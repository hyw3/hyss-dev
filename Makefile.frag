# HySS - Hyang Server Scripts
#
# Copyright (C) 2019-2020 Hyang Language Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Gear
#

$(builddir)/gear_language_scanner.lo: $(srcdir)/gear_language_parser.h
$(builddir)/gear_ics_scanner.lo: $(srcdir)/gear_ics_parser.h

$(srcdir)/gear_language_scanner.c: $(srcdir)/gear_language_scanner.l
	@(cd $(top_srcdir); $(RE2C) $(RE2C_FLAGS) --no-generation-date --case-inverted -cbdFt Gear/gear_language_scanner_defs.h -oGear/gear_language_scanner.c Gear/gear_language_scanner.l)

$(srcdir)/gear_language_parser.h: $(srcdir)/gear_language_parser.c
$(srcdir)/gear_language_parser.c: $(srcdir)/gear_language_parser.y
	@$(YACC) -p gear -v -d $(srcdir)/gear_language_parser.y -o $@

$(srcdir)/gear_ics_parser.h: $(srcdir)/gear_ics_parser.c
$(srcdir)/gear_ics_parser.c: $(srcdir)/gear_ics_parser.y
	@$(YACC) -p ics_ -v -d $(srcdir)/gear_ics_parser.y -o $@

$(srcdir)/gear_ics_scanner.c: $(srcdir)/gear_ics_scanner.l
	@(cd $(top_srcdir); $(RE2C) $(RE2C_FLAGS) --no-generation-date --case-inverted -cbdFt Gear/gear_ics_scanner_defs.h -oGear/gear_ics_scanner.c Gear/gear_ics_scanner.l)

$(builddir)/gear_highlight.lo $(builddir)/gear_compile.lo: $(srcdir)/gear_language_parser.h
