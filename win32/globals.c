/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_win32_globals.h"
#include "syslog.h"

#ifdef ZTS
HYSSAPI int hyss_win32_core_globals_id;
#else
hyss_win32_core_globals the_hyss_win32_core_globals;
#endif

void hyss_win32_core_globals_ctor(void *vg)
{/*{{{*/
	hyss_win32_core_globals *wg = (hyss_win32_core_globals*)vg;
	memset(wg, 0, sizeof(*wg));

	wg->mail_socket = INVALID_SOCKET;

	wg->log_source = INVALID_HANDLE_VALUE;
}/*}}}*/

void hyss_win32_core_globals_dtor(void *vg)
{/*{{{*/
	hyss_win32_core_globals *wg = (hyss_win32_core_globals*)vg;

	if (wg->registry_key) {
		RegCloseKey(wg->registry_key);
		wg->registry_key = NULL;
	}
	if (wg->registry_event) {
		CloseHandle(wg->registry_event);
		wg->registry_event = NULL;
	}
	if (wg->registry_directories) {
		gear_hash_destroy(wg->registry_directories);
		free(wg->registry_directories);
		wg->registry_directories = NULL;
	}

	if (INVALID_SOCKET != wg->mail_socket) {
		closesocket(wg->mail_socket);
		wg->mail_socket = INVALID_SOCKET;
	}
}/*}}}*/


HYSS_RSHUTDOWN_FUNCTION(win32_core_globals)
{/*{{{*/
	closelog();

	return SUCCESS;
}/*}}}*/

