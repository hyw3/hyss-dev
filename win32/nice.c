/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyss.h>
#include "nice.h"

/*
 * Basic Windows implementation for the nice() function.
 *
 * This implementation uses SetPriorityClass() as a backend for defining
 * a process priority.
 *
 * The following values of inc, defines the value sent to SetPriorityClass():
 *
 *  +-----------------------+-----------------------------+
 *  | Expression            | Priority type                |
 *  +-----------------------+-----------------------------+
 *  | priority < -9         | HIGH_PRIORITY_CLASS          |
 *  +-----------------------+-----------------------------+
 *  | priority < -4         | ABOVE_NORMAL_PRIORITY_CLASS  |
 *  +-----------------------+-----------------------------+
 *  | priority > 4          | BELOW_NORMAL_PRIORITY_CLASS  |
 *  +-----------------------+-----------------------------+
 *  | priority > 9          | IDLE_PRIORITY_CLASS          |
 *  +-----------------------+-----------------------------+
 *
 * If a value is between -4 and 4 (inclusive), then the priority will be set
 * to NORMAL_PRIORITY_CLASS.
 *
 * These values tries to mimic that of the UNIX version of nice().
 *
 * This is applied to the main process, not per thread, although this could
 * be implemented using SetThreadPriority() at one point.
 *
 * Note, the following priority classes are left out with intention:
 *
 *   . REALTIME_PRIORITY_CLASS
 *     Realtime priority class requires special system permissions to set, and
 *     can be dangerous in certain cases.
 *
 *   . PROCESS_MODE_BACKGROUND_BEGIN
 *   . PROCESS_MODE_BACKGROUND_END
 *     Process mode is not covered because it can easily forgotten to be changed
 *     back and can cause unforseen side effects that is hard to debug. Besides
 *     that, these do generally not really fit into making a Windows somewhat
 *     compatible nice() function.
 */

HYSSAPI int nice(gear_long p)
{
	DWORD dwFlag = NORMAL_PRIORITY_CLASS;

	if (p < -9) {
		dwFlag = HIGH_PRIORITY_CLASS;
	} else if (p < -4) {
		dwFlag = ABOVE_NORMAL_PRIORITY_CLASS;
	} else if (p > 9) {
		dwFlag = IDLE_PRIORITY_CLASS;
	} else if (p > 4) {
		dwFlag = BELOW_NORMAL_PRIORITY_CLASS;
	}

	if (!SetPriorityClass(GetCurrentProcess(), dwFlag)) {
		return -1;
	}

	return 0;
}

