/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAVE_GETRUSAGE_H
# define HAVE_GETRUSAGE_H

/*
 * Note
 *
 * RUSAGE_CHILDREN is not implemented, and the RUSAGE_THREAD will
 * therefore instead be used instead to emulate the behavior.
 */

# define RUSAGE_SELF		0
# define RUSAGE_CHILDREN	1

# define RUSAGE_THREAD		RUSAGE_CHILDREN

/*
 * Implementation support
 *
 *  RUSAGE_SELF
 *		ru_utime
 *		ru_stime
 *		ru_majflt
 *		ru_maxrss
 *
 *  RUSAGE_THREAD
 *		ru_utime
 *		ru_stime
 *
 * Not implemented:
 *		ru_ixrss		(unused)
 *		ru_idrss		(unused)
 *		ru_isrss		(unused)
 *		ru_minflt
 *		ru_nswap		(unused)
 *		ru_inblock
 *		ru_oublock
 *		ru_msgsnd		(unused)
 *		ru_msgrcv		(unused)
 *		ru_nsignals		(unused)
 *		ru_nvcsw
 *		ru_nivcsw
 */

struct rusage
{
	/* User time used */
	struct timeval ru_utime;

	/* System time used */
	struct timeval ru_stime;

	/* Integral max resident set size */
	gear_long ru_maxrss;

	/* Page faults */
	gear_long ru_majflt;
#if 0
	/* Integral shared text memory size */
	gear_long ru_ixrss;

	/* Integral unshared data size */
	gear_long ru_idrss;

	/* Integral unshared stack size */
	gear_long ru_isrss;

	/* Page reclaims */
	gear_long ru_minflt;

	/* Swaps */
	gear_long ru_nswap;

	/* Block input operations */
	gear_long ru_inblock;

	/* Block output operations */
	gear_long ru_oublock;

	/* Messages sent */
	gear_long ru_msgsnd;

	/* Messages received */
	gear_long ru_msgrcv;

	/* Signals received */
	gear_long ru_nsignals;

	/* Voluntary context switches */
	gear_long ru_nvcsw;

	/* Involuntary context switches */
	gear_long ru_nivcsw;
#endif
};

HYSSAPI int getrusage(int who, struct rusage *usage);

#endif

