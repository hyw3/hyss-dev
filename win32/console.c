/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "win32/console.h"


HYSS_WINUTIL_API BOOL hyss_win32_console_fileno_is_console(gear_long fileno)
{/*{{{*/
	BOOL result = FALSE;
	HANDLE handle = (HANDLE) _get_osfhandle(fileno);

	if (handle != INVALID_HANDLE_VALUE) {
        DWORD mode;
        if (GetConsoleMode(handle, &mode)) {
            result = TRUE;
		}
	}
	return result;
}/*}}}*/

HYSS_WINUTIL_API BOOL hyss_win32_console_fileno_has_vt100(gear_long fileno)
{/*{{{*/
	BOOL result = FALSE;
	HANDLE handle = (HANDLE) _get_osfhandle(fileno);

	if (handle != INVALID_HANDLE_VALUE) {
		DWORD events;

		if (fileno != 0 && !GetNumberOfConsoleInputEvents(handle, &events)) {
			// Not STDIN
			DWORD mode;

			if (GetConsoleMode(handle, &mode)) {
				if (mode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) {
					result = TRUE;
				}
			}
		}
	}
	return result;
}/*}}}*/

HYSS_WINUTIL_API BOOL hyss_win32_console_fileno_set_vt100(gear_long fileno, BOOL enable)
{/*{{{*/
	BOOL result = FALSE;
	HANDLE handle = (HANDLE) _get_osfhandle(fileno);

	if (handle != INVALID_HANDLE_VALUE) {
		DWORD events;

		if (fileno != 0 && !GetNumberOfConsoleInputEvents(handle, &events)) {
			// Not STDIN
			DWORD mode;

			if (GetConsoleMode(handle, &mode)) {
				DWORD newMode;

				if (enable) {
					newMode = mode | ENABLE_VIRTUAL_TERMINAL_PROCESSING;
				}
				else {
					newMode = mode & ~ENABLE_VIRTUAL_TERMINAL_PROCESSING;
				}
				if (newMode == mode) {
					result = TRUE;
				}
				else {
					if (SetConsoleMode(handle, newMode)) {
						result = TRUE;
					}
				}
			}
		}
	}
	return result;
}/*}}}*/

HYSS_WINUTIL_API BOOL hyss_win32_console_is_own(void)
{/*{{{*/
	if (!IsDebuggerPresent()) {
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		DWORD pl[1];
		BOOL ret0 = FALSE, ret1 = FALSE;

		if (GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi)) {
			ret0 = !csbi.dwCursorPosition.X && !csbi.dwCursorPosition.Y;
		}

		ret1 = GetConsoleProcessList(pl, 1) == 1;

		return ret0 && ret1;
	}

	return FALSE;
}/*}}}*/

