/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_WIN32_GLOBALS_H
#define HYSS_WIN32_GLOBALS_H

/* misc globals for thread-safety under win32 */

#include "win32/sendmail.h"

typedef struct _hyss_win32_core_globals hyss_win32_core_globals;

#ifdef ZTS
# define PW32G(v)		GEAR_PBCG(hyss_win32_core_globals_id, hyss_win32_core_globals*, v)
extern HYSSAPI int hyss_win32_core_globals_id;
#else
# define PW32G(v)		(the_hyss_win32_core_globals.v)
extern HYSSAPI struct _hyss_win32_core_globals the_hyss_win32_core_globals;
#endif

struct _hyss_win32_core_globals {
	/* syslog */
	char *log_header;
	HANDLE log_source;

	HKEY       registry_key;
	HANDLE     registry_event;
	HashTable *registry_directories;

	char   mail_buffer[MAIL_BUFFER_SIZE];
	SOCKET mail_socket;
	char   mail_host[HOST_NAME_LEN];
	char   mail_local_host[HOST_NAME_LEN];
};

void hyss_win32_core_globals_ctor(void *vg);
void hyss_win32_core_globals_dtor(void *vg);
HYSS_RSHUTDOWN_FUNCTION(win32_core_globals);

#endif

