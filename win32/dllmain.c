/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.w32.h>

#include <win32/time.h>
#include <win32/ioutil.h>
#include <hyss.h>

#ifdef HAVE_LIBXML
#include <libxml/threads.h>
#endif

/* TODO this file, or part of it, could be machine generated, to
	allow extensions and SAPIs adding their own init stuff.
	However expected is that MINIT is enough in most cases.
	This file is only useful for some really internal stuff,
	eq. initializing something before the DLL even is
	available to be called. */

BOOL WINAPI DllMain(HINSTANCE inst, DWORD reason, LPVOID dummy)
{
	BOOL ret = TRUE;

	switch (reason)
	{
		case DLL_PROCESS_ATTACH:
			/*
			 * We do not need to check the return value of hyss_win32_init_gettimeofday()
			 * because the symbol bare minimum symbol we need is always available on our
			 * lowest supported platform.
			 *
			 * On Windows 8 or greater, we use a more precise symbol to obtain the system
			 * time, which is dynamically. The fallback allows us to proper support
			 * Vista/7/Server 2003 R2/Server 2008/Server 2008 R2.
			 *
			 * Instead simply initialize the global in win32/time.c for gettimeofday()
			 * use later on
			 */
			hyss_win32_init_gettimeofday();

			ret = ret && hyss_win32_ioutil_init();
			if (!ret) {
				fprintf(stderr, "ioutil initialization failed");
				return ret;
			}
			break;
#if 0 /* prepared */
		case DLL_PROCESS_DETACH:
			/* pass */
			break;

		case DLL_THREAD_ATTACH:
			/* pass */
			break;

		case DLL_THREAD_DETACH:
			/* pass */
			break;
#endif
	}

#ifdef HAVE_LIBXML
	/* This imply that only LIBXML_STATIC_FOR_DLL is supported ATM.
		If that changes, this place will need some rework.
	   TODO Also this should be revisited as no initialization
		might be needed for TS build (libxml build with TLS
		support. */
	ret = ret && xmlDllMain(inst, reason, dummy);
#endif

	return ret;
}

