@echo off
SET HYSS_BUILDCONF_PATH=%~dp0
cscript /nologo %HYSS_BUILDCONF_PATH%\script\hyssize.js %*
IF NOT EXIST configure.bat (
	echo Error generating configure script, configure script was not copied
	exit /b 3
) ELSE (
	echo Now run 'configure --help'
)
SET HYSS_BUILDCONF_PATH=
