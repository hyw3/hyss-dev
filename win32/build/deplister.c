/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This little application will list the DLL dependencies for a PE
 * cAPI to it's stdout for use by distro/installer building tools */

#include <windows.h>
#include <stdio.h>
#include <imagehlp.h>

BOOL CALLBACK StatusRoutine(IMAGEHLP_STATUS_REASON reason,
		PSTR image_name, PSTR dll_name,
		ULONG va, ULONG param)
{
	switch (reason) {
		case BindImportcAPIFailed:
			printf("%s,NOTFOUND\n", dll_name);
			return TRUE;

		case BindImportcAPI:
			printf("%s,OK\n", dll_name);
			return TRUE;
	}
	return TRUE;
}

/* usage:
 * deplister.exe path\to\cAPI.exe path\to\symbols\root
 * */

int main(int argc, char *argv[])
{
	return BindImageEx(BIND_NO_BOUND_IMPORTS | BIND_NO_UPDATE | BIND_ALL_IMAGES,
		argv[1], NULL, argv[2], StatusRoutine);
}

