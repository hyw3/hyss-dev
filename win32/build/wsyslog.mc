MessageId=1
Severity=Success
SymbolicName=HYSS_SYSLOG_SUCCESS_TYPE
Language=English
%1 %2
.

MessageId=2
Severity=Informational
SymbolicName=HYSS_SYSLOG_INFO_TYPE
Language=English
%1 %2
.

MessageId=3
Severity=Warning
SymbolicName=HYSS_SYSLOG_WARNING_TYPE
Language=English
%1 %2
.

MessageId=4
Severity=Error
SymbolicName=HYSS_SYSLOG_ERROR_TYPE
Language=English
%1 %2
.
