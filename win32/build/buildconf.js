/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// This generates a configure script for win32 build

WScript.StdOut.WriteLine("Rebuilding configure.js");
var FSO = WScript.CreateObject("Scripting.FileSystemObject");
var C = FSO.CreateTextFile("configure.js", true);
var B = FSO.CreateTextFile("configure.bat", true);

var cAPIs = "";
var CAPIS = WScript.CreateObject("Scripting.Dictionary");
var capi_dirs = new Array();

function file_get_contents(filename)
{
	var F = FSO.OpenTextFile(filename, 1);
	var t = F.ReadAll();
	F.Close();
	return t;
}

function cAPI_Item(capi_name, config_path, dir_line, deps, content)
{
	this.capi_name = capi_name;
	this.config_path = config_path;
	this.dir_line = dir_line;
	this.deps = deps;
	this.content = content;
}

function find_config_w32(dirname)
{
	if (!FSO.FolderExists(dirname)) {
		return;
	}

	var f = FSO.GetFolder(dirname);
	var	fc = new Enumerator(f.SubFolders);
	var c, i, ok, n;
	var item = null;
	var re_dep_line = new RegExp("ADD_EXTENSION_DEP\\([^,]*\\s*,\\s*['\"]([^'\"]+)['\"].*\\)", "gm");

	for (; !fc.atEnd(); fc.moveNext())
	{
		ok = true;
		/* check if we already picked up a cAPI with the same dirname;
		 * if we have, don't include it here */
		n = FSO.GetFileName(fc.item());

		if (n == '.svn' || n == 'tests')
			continue;

	//	WScript.StdOut.WriteLine("checking " + dirname + "/" + n);
		if (CAPIS.Exists(n)) {
			WScript.StdOut.WriteLine("Skipping " + dirname + "/" + n + " -- already have a cAPI with that name");
			continue;
		}

		c = FSO.BuildPath(fc.item(), "config.w32");
		if (FSO.FileExists(c)) {
//			WScript.StdOut.WriteLine(c);

			var dir_line = "configure_capi_dirname = condense_path(FSO.GetParentFolderName('"
							   	+ c.replace(new RegExp('(["\\\\])', "g"), '\\$1') + "'));\r\n";
			var contents = file_get_contents(c);
			var deps = new Array();

			// parse out any deps from the file
			var calls = contents.match(re_dep_line);
			if (calls != null) {
				for (i = 0; i < calls.length; i++) {
					// now we need the extension name out of this thing
					if (calls[i].match(re_dep_line)) {
//						WScript.StdOut.WriteLine("n depends on " + RegExp.$1);
						deps[deps.length] = RegExp.$1;

					}
				}
			}

			item = new cAPI_Item(n, c, dir_line, deps, contents);
			CAPIS.Add(n, item);
		}
	}
}

// Emit core cAPIs array.  This is used by a snapshot
// build to override a default "yes" value so that external
// cAPIs don't break the build by becoming statically compiled
function emit_core_capi_list()
{
	var capi_names = (new VBArray(CAPIS.Keys())).toArray();
	var i, capi_name, j;
	var item;
	var output = "";

	C.WriteLine("core_capi_list = new Array(");

	// first, look for cAPIs with empty deps; emit those first
	for (i in capi_names) {
		capi_name = capi_names[i];
		C.WriteLine("\"" + capi_name.replace(/_/g, "-") + "\",");
	}

	C.WriteLine("false // dummy");

	C.WriteLine(");");
}


function emit_capi(item)
{
	return item.dir_line + item.content;
}

function emit_dep_capis(capi_names)
{
	var i, capi_name, j;
	var output = "";
	var item = null;

	for (i in capi_names) {
		capi_name = capi_names[i];

		if (CAPIS.Exists(capi_name)) {
			item = CAPIS.Item(capi_name);
			CAPIS.Remove(capi_name);
			if (item.deps.length) {
				output += emit_dep_capis(item.deps);
			}
			output += emit_capi(item);
		}
	}

	return output;
}

function gen_capis()
{
	var capi_names = (new VBArray(CAPIS.Keys())).toArray();
	var i, capi_name, j;
	var item;
	var output = "";

	// first, look for cAPIs with empty deps; emit those first
	for (i in capi_names) {
		capi_name = capi_names[i];
		item = CAPIS.Item(capi_name);
		if (item.deps.length == 0) {
			CAPIS.Remove(capi_name);
			output += emit_capi(item);
		}
	}

	// now we are left with cAPIs that have dependencies on other cAPIs
	capi_names = (new VBArray(CAPIS.Keys())).toArray();
	output += emit_dep_capis(capi_names);

	return output;
}

// Process buildconf arguments
function buildconf_process_args()
{
	args = WScript.Arguments;

	for (i = 0; i < args.length; i++) {
		arg = args(i);
		// If it is --foo=bar, split on the equals sign
		arg = arg.split("=", 2);
		argname = arg[0];
		if (arg.length > 1) {
			argval = arg[1];
		} else {
			argval = null;
		}

		if (argname == '--add-cAPIs-dir' && argval != null) {
			WScript.StdOut.WriteLine("Adding " + argval + " to the cAPI search path");
			capi_dirs[capi_dirs.length] = argval;
		}
	}
}

buildconf_process_args();

// Write the head of the configure script
C.WriteLine("/* This file automatically generated from win32/build/confutils.js */");
C.WriteLine("MODE_HYSSIZE=false;");
C.Write(file_get_contents("win32/build/confutils.js"));

// Pull in code from sapi and extensions
cAPIs = file_get_contents("win32/build/config.w32");

// Pick up confs from hypbc and Gear if present
find_config_w32(".");
find_config_w32("server");
find_config_w32("extslib");
emit_core_capi_list();

// If we have not specified any cAPI dirs let's add some defaults
if (capi_dirs.length == 0) {
	find_config_w32("pecl");
	find_config_w32("..\\pecl");
	find_config_w32("pecl\\rpc");
	find_config_w32("..\\pecl\\rpc");
} else {
	for (i = 0; i < capi_dirs.length; i++) {
		find_config_w32(capi_dirs[i]);
	}
}

// Now generate contents of cAPI based on CAPIS, chasing dependencies
// to ensure that dependent cAPIs are emitted first
cAPIs += gen_capis();

// Look for ARG_ENABLE or ARG_WITH calls
re = new RegExp("(ARG_(ENABLE|WITH)\([^;]+\);)", "gm");
calls = cAPIs.match(re);
for (i = 0; i < calls.length; i++) {
	item = calls[i];
	C.WriteLine("try {");
	C.WriteLine(item);
	C.WriteLine("} catch (e) {");
	C.WriteLine('\tSTDOUT.WriteLine("problem: " + e);');
	C.WriteLine("}");
}

C.WriteBlankLines(1);
C.WriteLine("check_binary_tools_sdk();");
C.WriteBlankLines(1);
C.WriteLine("STDOUT.WriteLine(\"HYSS Version: \" + HYSS_VERSION_STRING);");
C.WriteLine("STDOUT.WriteBlankLines(1);");
C.WriteLine("conf_process_args();");
C.WriteBlankLines(1);

// Comment out the calls from their original positions
cAPIs = cAPIs.replace(re, "/* $1 */");
C.Write(cAPIs);

C.WriteBlankLines(1);
C.Write(file_get_contents("win32/build/configure.tail"));

B.WriteLine("@echo off");
B.WriteLine("cscript /nologo configure.js %*");
