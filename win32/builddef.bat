@echo off
rem Generate hyssts.def file, which exports symbols from our dll that
rem are present in some of the libraries which are compiled statically
rem into HYSS
type ..\extslib\sqlite\hyss_sqlite.def
type ..\extslib\libxml\hyss_libxml2.def
