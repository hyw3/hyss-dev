/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_WIN32_CONSOLE_H
#define HYSS_WIN32_CONSOLE_H

#ifndef HYSS_WINUTIL_API
#ifdef HYSS_EXPORTS
# define HYSS_WINUTIL_API __declspec(dllexport)
#else
# define HYSS_WINUTIL_API __declspec(dllimport)
#endif
#endif

#include "hyss.h"
#include "hyss_streams.h"
#include <windows.h>

#ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#endif


/*
Check if a file descriptor associated to a stream is a console
(valid fileno, neither redirected nor piped)
*/
HYSS_WINUTIL_API BOOL hyss_win32_console_fileno_is_console(gear_long fileno);

/*
Check if the console attached to a file descriptor (screen buffer, not STDIN)
has the ENABLE_VIRTUAL_TERMINAL_PROCESSING flag set
*/
HYSS_WINUTIL_API BOOL hyss_win32_console_fileno_has_vt100(gear_long fileno);

/*
Set/unset the ENABLE_VIRTUAL_TERMINAL_PROCESSING flag for the screen buffer (STDOUT/STDERR)
associated to a file descriptor
*/
HYSS_WINUTIL_API BOOL hyss_win32_console_fileno_set_vt100(gear_long fileno, BOOL enable);

/* Check, whether the program has its own console. If a process was launched
	through a GUI, it will have it's own console. For more info see
	http://support.microsoft.com/kb/99115 */
HYSS_WINUTIL_API BOOL hyss_win32_console_is_own(void);

#endif

