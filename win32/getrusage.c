/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyss.h>
#include <psapi.h>
#include "getrusage.h"

/*
 * Parts of this file is based on code from the OpenVSwitch project,
 * copyright 2014 Nicira, Inc.
 * and have been modified to work with HYSS.
 */

static gear_always_inline void usage_to_timeval(FILETIME *ft, struct timeval *tv)
{
	ULARGE_INTEGER time;

	time.LowPart = ft->dwLowDateTime;
	time.HighPart = ft->dwHighDateTime;

	tv->tv_sec = (gear_long) (time.QuadPart / 10000000);
	tv->tv_usec = (gear_long) ((time.QuadPart % 10000000) / 10);
}

HYSSAPI int getrusage(int who, struct rusage *usage)
{
	FILETIME ctime, etime, stime, utime;

	memset(usage, 0, sizeof(struct rusage));

	if (who == RUSAGE_SELF) {
		PROCESS_MEMORY_COUNTERS pmc;
		HANDLE proc = GetCurrentProcess();

		if (!GetProcessTimes(proc, &ctime, &etime, &stime, &utime)) {
			return -1;
		} else if(!GetProcessMemoryInfo(proc, &pmc, sizeof(pmc))) {
			return -1;
		}

		usage_to_timeval(&stime, &usage->ru_stime);
		usage_to_timeval(&utime, &usage->ru_utime);

		usage->ru_majflt = pmc.PageFaultCount;
		usage->ru_maxrss = pmc.PeakWorkingSetSize / 1024;

		return 0;
	} else if (who == RUSAGE_THREAD) {
		if (!GetThreadTimes(GetCurrentThread(), &ctime, &etime, &stime, &utime)) {
			return -1;
		}

		usage_to_timeval(&stime, &usage->ru_stime);
		usage_to_timeval(&utime, &usage->ru_utime);

		return 0;
	} else {
		return -1;
	}
}

