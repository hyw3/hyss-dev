/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ipc.h"

#include <windows.h>
#include <sys/stat.h>

#include "ioutil.h"

HYSS_WIN32_IPC_API key_t
ftok(const char *pathname, int proj_id)
{/*{{{*/
	HANDLE fh;
	struct _stat st;
	BY_HANDLE_FILE_INFORMATION bhfi;
	key_t ret;
	HYSS_WIN32_IOUTIL_INIT_W(pathname)

	if (!pathw) {
		return (key_t)-1;
	}

	if (_wstat(pathw, &st) < 0) {
		HYSS_WIN32_IOUTIL_CLEANUP_W()
		return (key_t)-1;
	}

	if ((fh = CreateFileW(pathw, FILE_GENERIC_READ, HYSS_WIN32_IOUTIL_DEFAULT_SHARE_MODE, 0, OPEN_EXISTING, 0, 0)) == INVALID_HANDLE_VALUE) {
		HYSS_WIN32_IOUTIL_CLEANUP_W()
		return (key_t)-1;
	}

	if (!GetFileInformationByHandle(fh, &bhfi)) {
		HYSS_WIN32_IOUTIL_CLEANUP_W()
		CloseHandle(fh);
		return (key_t)-1;
	}

	ret = (key_t) ((proj_id & 0xff) << 24 | (st.st_dev & 0xff) << 16 | ((bhfi.nFileIndexLow | (__int64)bhfi.nFileIndexHigh << 32) & 0xffff));

	CloseHandle(fh);
	HYSS_WIN32_IOUTIL_CLEANUP_W()

	return ret;
}/*}}}*/

