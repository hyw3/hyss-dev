/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_WIN32_WINUTIL_H
#define HYSS_WIN32_WINUTIL_H

#ifdef HYSS_EXPORTS
# define HYSS_WINUTIL_API __declspec(dllexport)
#else
# define HYSS_WINUTIL_API __declspec(dllimport)
#endif

HYSS_WINUTIL_API char *hyss_win32_error_to_msg(HRESULT error);

#define hyss_win_err()	hyss_win32_error_to_msg(GetLastError())
int hyss_win32_check_trailing_space(const char * path, const size_t path_len);
HYSS_WINUTIL_API int hyss_win32_get_random_bytes(unsigned char *buf, size_t size);
#ifdef HYSS_EXPORTS
BOOL hyss_win32_init_random_bytes(void);
BOOL hyss_win32_shutdown_random_bytes(void);
#endif

#if !defined(ECURDIR)
# define ECURDIR        EACCES
#endif /* !ECURDIR */
#if !defined(ENOSYS)
# define ENOSYS         EPERM
#endif /* !ENOSYS */

HYSS_WINUTIL_API int hyss_win32_code_to_errno(unsigned long w32Err);

#define SET_ERRNO_FROM_WIN32_CODE(err) \
	do { \
	int ern = hyss_win32_code_to_errno(err); \
	SetLastError(err); \
	_set_errno(ern); \
	} while (0)

HYSS_WINUTIL_API char *hyss_win32_get_username(void);

#endif

