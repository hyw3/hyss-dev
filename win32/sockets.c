/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Code originally from extslib/sockets */

#include <stdio.h>
#include <fcntl.h>

#include "hyss.h"

HYSSAPI int socketpair(int domain, int type, int protocol, SOCKET sock[2])
{
	struct sockaddr_in address;
	SOCKET redirect;
	int size = sizeof(address);

	if(domain != AF_INET) {
		WSASetLastError(WSAENOPROTOOPT);
		return -1;
	}

	sock[0] = sock[1] = redirect = INVALID_SOCKET;


	sock[0]	= socket(domain, type, protocol);
	if (INVALID_SOCKET == sock[0]) {
		goto error;
	}

	address.sin_addr.s_addr	= INADDR_ANY;
	address.sin_family	= AF_INET;
	address.sin_port	= 0;

	if (bind(sock[0], (struct sockaddr*)&address, sizeof(address)) != 0) {
		goto error;
	}

	if(getsockname(sock[0], (struct sockaddr *)&address, &size) != 0) {
		goto error;
	}

	if (listen(sock[0], 2) != 0) {
		goto error;
	}

	sock[1] = socket(domain, type, protocol);
	if (INVALID_SOCKET == sock[1]) {
		goto error;
	}

	address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	if(connect(sock[1], (struct sockaddr*)&address, sizeof(address)) != 0) {
		goto error;
	}

	redirect = accept(sock[0],(struct sockaddr*)&address, &size);
	if (INVALID_SOCKET == redirect) {
		goto error;
	}

	closesocket(sock[0]);
	sock[0] = redirect;

	return 0;

error:
	closesocket(redirect);
	closesocket(sock[0]);
	closesocket(sock[1]);
	WSASetLastError(WSAECONNABORTED);
	return -1;
}

