/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_WIN32_CODEPAGE_H
#define HYSS_WIN32_CODEPAGE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef HYSS_EXPORTS
# define PW32CP __declspec(dllexport)
#else
# define PW32CP __declspec(dllimport)
#endif

#define HYSS_WIN32_CP_IGNORE_LEN (0)
#define HYSS_WIN32_CP_IGNORE_LEN_P ((size_t *)-1)

struct hyss_win32_cp {
	DWORD id;
	DWORD to_w_fl;
	DWORD from_w_fl;
	DWORD char_size;
	char *name;
	char *enc;
	char *desc;
};

PW32CP BOOL hyss_win32_cp_use_unicode(void);
PW32CP const struct hyss_win32_cp *hyss_win32_cp_do_setup(const char *);
#define hyss_win32_cp_setup() hyss_win32_cp_do_setup(NULL)
PW32CP const struct hyss_win32_cp *hyss_win32_cp_do_update(const char *);
#define hyss_win32_cp_update() hyss_win32_cp_do_update(NULL)
PW32CP const struct hyss_win32_cp *hyss_win32_cp_shutdown(void);
PW32CP const struct hyss_win32_cp *hyss_win32_cp_get_current(void);
PW32CP const struct hyss_win32_cp *hyss_win32_cp_get_orig(void);
PW32CP const struct hyss_win32_cp *hyss_win32_cp_get_by_id(DWORD id);
PW32CP const struct hyss_win32_cp *hyss_win32_cp_set_by_id(DWORD id);
PW32CP const struct hyss_win32_cp *hyss_win32_cp_get_by_enc(const char *enc);
PW32CP const struct hyss_win32_cp *hyss_win32_cp_cli_do_setup(DWORD);
#define hyss_win32_cp_cli_setup() hyss_win32_cp_cli_do_setup(0)
#define hyss_win32_cp_cli_update() hyss_win32_cp_cli_do_setup(0)
PW32CP const struct hyss_win32_cp *hyss_win32_cp_cli_do_restore(DWORD);
#define hyss_win32_cp_cli_restore() hyss_win32_cp_cli_do_restore(0)

/* This API is binary safe and expects a \0 terminated input.
   The returned out is \0 terminated, but the length doesn't count \0. */
PW32CP wchar_t *hyss_win32_cp_conv_to_w(DWORD in_cp, DWORD flags, const char* in, size_t in_len, size_t *out_len);
PW32CP wchar_t *hyss_win32_cp_conv_utf8_to_w(const char* in, size_t in_len, size_t *out_len);
#define hyss_win32_cp_utf8_to_w(in) hyss_win32_cp_conv_utf8_to_w(in, HYSS_WIN32_CP_IGNORE_LEN, HYSS_WIN32_CP_IGNORE_LEN_P)
PW32CP wchar_t *hyss_win32_cp_conv_cur_to_w(const char* in, size_t in_len, size_t *out_len);
#define hyss_win32_cp_cur_to_w(in) hyss_win32_cp_conv_cur_to_w(in, HYSS_WIN32_CP_IGNORE_LEN, HYSS_WIN32_CP_IGNORE_LEN_P)
PW32CP wchar_t *hyss_win32_cp_conv_ascii_to_w(const char* in, size_t in_len, size_t *out_len);
#define hyss_win32_cp_ascii_to_w(in) hyss_win32_cp_conv_ascii_to_w(in, HYSS_WIN32_CP_IGNORE_LEN, HYSS_WIN32_CP_IGNORE_LEN_P)
PW32CP char *hyss_win32_cp_conv_from_w(DWORD out_cp, DWORD flags, const wchar_t* in, size_t in_len, size_t *out_len);
PW32CP char *hyss_win32_cp_conv_w_to_utf8(const wchar_t* in, size_t in_len, size_t *out_len);
#define hyss_win32_cp_w_to_utf8(in) hyss_win32_cp_conv_w_to_utf8(in, HYSS_WIN32_CP_IGNORE_LEN, HYSS_WIN32_CP_IGNORE_LEN_P)
PW32CP char *hyss_win32_cp_conv_w_to_cur(const wchar_t* in, size_t in_len, size_t *out_len);
#define hyss_win32_cp_w_to_cur(in) hyss_win32_cp_conv_w_to_cur(in, HYSS_WIN32_CP_IGNORE_LEN, HYSS_WIN32_CP_IGNORE_LEN_P)
PW32CP wchar_t *hyss_win32_cp_env_any_to_w(const char* env);

/* This function tries to make the best guess to convert any
   given string to a wide char, also preferring the fastest code
   path to unicode. It returns NULL on fail. */
__forceinline static wchar_t *hyss_win32_cp_conv_any_to_w(const char* in, size_t in_len, size_t *out_len)
{/*{{{*/
	wchar_t *ret = NULL;

	if (hyss_win32_cp_use_unicode()) {
		/* First try the pure ascii conversion. This is the fastest way to do the
			thing. Only applicable if the source string is UTF-8 in general.
			While it could possibly be ok with European encodings, usage with
			Asian encodings can cause unintended side effects. Lookup the term
			"mojibake" if need more. */
		ret = hyss_win32_cp_conv_ascii_to_w(in, in_len, out_len);

		/* If that failed, try to convert to multibyte. */
		if (!ret) {
			ret = hyss_win32_cp_conv_utf8_to_w(in, in_len, out_len);

			/* Still need this fallback with regard to possible broken data
				in the existing scripts. Broken data might be hardcoded in
				the user scripts, as UTF-8 settings was de facto ignored in
				older HYSS versions. The fallback can be removed later for
				the sake of purity, keep now for BC reasons. */
			if (!ret) {
				const struct hyss_win32_cp *acp = hyss_win32_cp_get_by_id(GetACP());

				if (acp) {
					ret = hyss_win32_cp_conv_to_w(acp->id, acp->to_w_fl, in, in_len, out_len);
				}
			}
		}
	} else {
		/* No unicode, convert from the current thread cp. */
		ret = hyss_win32_cp_conv_cur_to_w(in, in_len, out_len);
	}

	return ret;
}/*}}}*/
#define hyss_win32_cp_any_to_w(in) hyss_win32_cp_conv_any_to_w(in, HYSS_WIN32_CP_IGNORE_LEN, HYSS_WIN32_CP_IGNORE_LEN_P)

/* This function converts from unicode function output back to HYSS. If
	the HYSS's current charset is not compatible with unicode, so the currently
	configured CP will be used. */
__forceinline static char *hyss_win32_cp_conv_w_to_any(const wchar_t* in, size_t in_len, size_t *out_len)
{/*{{{*/
	return hyss_win32_cp_conv_w_to_cur(in, in_len, out_len);
}/*}}}*/
#define hyss_win32_cp_w_to_any(in) hyss_win32_cp_conv_w_to_any(in, HYSS_WIN32_CP_IGNORE_LEN, HYSS_WIN32_CP_IGNORE_LEN_P)

#define HYSS_WIN32_CP_W_TO_ANY_ARRAY(aw, aw_len, aa, aa_len) do { \
	int i; \
	aa_len = aw_len; \
	aa = (char **) malloc(aw_len * sizeof(char *)); \
	if (!aa) { \
		break; \
	} \
	for (i = 0; i < aw_len; i++) { \
		aa[i] = hyss_win32_cp_w_to_any(aw[i]); \
	} \
} while (0);


#define HYSS_WIN32_CP_FREE_ARRAY(a, a_len) do { \
	int i; \
	for (i = 0; i < a_len; i++) { \
		free(a[i]); \
	} \
	free(a); \
} while (0);

#ifdef __cplusplus
}
#endif

#endif /* HYSS_WIN32_CODEPAGE_H */

