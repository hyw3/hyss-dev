/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_WIN32_SIGNAL_H
#define HYSS_WIN32_SIGNAL_H

#include <signal.h>

#define SIGALRM 13
#define	SIGVTALRM 26			/* virtual time alarm */
#define	SIGPROF	27			/* profiling time alarm */

#endif /* HYSS_WIN32_SIGNAL_H */

