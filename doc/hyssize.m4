dnl This file becomes configure.ac for self-contained extensions.

AC_PREREQ([2.68])
AC_INIT(config.m4)
ifdef([AC_PRESERVE_HELP_ORDER], [AC_PRESERVE_HELP_ORDER], [])

HYSS_PLV_UPGRADE(plv.upgrade)

dnl
AC_DEFUN([HYSS_EXT_BUILDDIR],[.])dnl
AC_DEFUN([HYSS_EXT_DIR],[""])dnl
AC_DEFUN([HYSS_EXT_SRCDIR],[$abs_srcdir])dnl
AC_DEFUN([HYSS_ALWAYS_SHARED],[
  ext_output="yes, shared"
  ext_shared=yes
  test "[$]$1" = "no" && $1=yes
])dnl
dnl

test -z "$CFLAGS" && auto_cflags=1

abs_srcdir=`(cd $srcdir && pwd)`
abs_builddir=`pwd`

AC_PROG_CC([cc gcc])
HYSS_DETECT_ICC
HYSS_DETECT_SUNCC
AC_PROG_CC_C_O

dnl Support systems with system libraries in e.g. /usr/lib64
HYSS_ARG_WITH(libdir, for system library directory,
[  --with-libdir=NAME      Look for libraries in .../NAME rather than .../lib], lib, no)

HYSS_RUNPATH_SWITCH
HYSS_SHLIB_SUFFIX_NAMES

dnl Find hyss-config script
HYSS_ARG_WITH(hyss-config,,
[  --with-hyss-config=PATH  Path to hyss-config [hyss-config]], hyss-config, no)

dnl For BC
HYSS_CONFIG=$HYSS_HYSS_CONFIG
prefix=`$HYSS_CONFIG --prefix 2>/dev/null`
hyssincludedir=`$HYSS_CONFIG --include-dir 2>/dev/null`
INCLUDES=`$HYSS_CONFIG --includes 2>/dev/null`
EXTENSION_DIR=`$HYSS_CONFIG --extension-dir 2>/dev/null`
HYSS_EXECUTABLE=`$HYSS_CONFIG --hyss-binary 2>/dev/null`

if test -z "$prefix"; then
  AC_MSG_ERROR([Cannot find hyss-config. Please use --with-hyss-config=PATH])
fi

hyss_shtool=$srcdir/build/shtool
HYSS_INIT_BUILD_SYSTEM

AC_MSG_CHECKING([for HYSS prefix])
AC_MSG_RESULT([$prefix])
AC_MSG_CHECKING([for HYSS includes])
AC_MSG_RESULT([$INCLUDES])
AC_MSG_CHECKING([for HYSS extension directory])
AC_MSG_RESULT([$EXTENSION_DIR])
AC_MSG_CHECKING([for HYSS installed headers prefix])
AC_MSG_RESULT([$hyssincludedir])

dnl Checks for HYSS_DEBUG / GEAR_DEBUG / ZTS
AC_MSG_CHECKING([if debug is enabled])
old_CPPFLAGS=$CPPFLAGS
CPPFLAGS="-I$hyssincludedir"
AC_EGREP_CPP(hyss_debug_is_enabled,[
#include <main/hyss_config.h>
#if GEAR_DEBUG
hyss_debug_is_enabled
#endif
],[
  HYSS_DEBUG=yes
],[
  HYSS_DEBUG=no
])
CPPFLAGS=$old_CPPFLAGS
AC_MSG_RESULT([$HYSS_DEBUG])

AC_MSG_CHECKING([if zts is enabled])
old_CPPFLAGS=$CPPFLAGS
CPPFLAGS="-I$hyssincludedir"
AC_EGREP_CPP(hyss_zts_is_enabled,[
#include <main/hyss_config.h>
#if ZTS
hyss_zts_is_enabled
#endif
],[
  HYSS_THREAD_SAFETY=yes
],[
  HYSS_THREAD_SAFETY=no
])
CPPFLAGS=$old_CPPFLAGS
AC_MSG_RESULT([$HYSS_THREAD_SAFETY])

dnl Support for building and testing Gear extensions
GEAR_EXT_TYPE="gear_extension"
HYSS_SUBST(GEAR_EXT_TYPE)

dnl Discard optimization flags when debugging is enabled
if test "$HYSS_DEBUG" = "yes"; then
  HYSS_DEBUG=1
  GEAR_DEBUG=yes
  changequote({,})
  CFLAGS=`echo "$CFLAGS" | $SED -e 's/-O[0-9s]*//g'`
  CXXFLAGS=`echo "$CXXFLAGS" | $SED -e 's/-O[0-9s]*//g'`
  changequote([,])
  dnl add -O0 only if GCC or ICC is used
  if test "$GCC" = "yes" || test "$ICC" = "yes"; then
    CFLAGS="$CFLAGS -O0"
    CXXFLAGS="$CXXFLAGS -g -O0"
  fi
  if test "$SUNCC" = "yes"; then
    if test -n "$auto_cflags"; then
      CFLAGS="-g"
      CXXFLAGS="-g"
    else
      CFLAGS="$CFLAGS -g"
      CXXFLAGS="$CFLAGS -g"
    fi
  fi
else
  HYSS_DEBUG=0
  GEAR_DEBUG=no
fi

dnl Always shared
HYSS_BUILD_SHARED

dnl Required programs
HYSS_PROG_RE2C
HYSS_PROG_AWK

sinclude(config.m4)

enable_static=no
enable_shared=yes

dnl Only allow AC_PROG_CXX and AC_PROG_CXXCPP if they are explicitly called (by HYSS_REQUIRE_CXX).
dnl Otherwise AC_PROG_LIBTOOL fails if there is no working C++ compiler.
AC_PROVIDE_IFELSE([HYSS_REQUIRE_CXX], [], [
  undefine([AC_PROG_CXX])
  AC_DEFUN([AC_PROG_CXX], [])
  undefine([AC_PROG_CXXCPP])
  AC_DEFUN([AC_PROG_CXXCPP], [hyss_prog_cxxcpp=disabled])
])
AC_PROG_LIBTOOL

all_targets='$(HYSS_CAPIS) $(HYSS_GEAR_EX)'
install_targets="install-cAPIs install-headers"
hysslibdir="`pwd`/cAPIs"
CPPFLAGS="$CPPFLAGS -DHAVE_CONFIG_H"
CFLAGS_CLEAN='$(CFLAGS)'
CXXFLAGS_CLEAN='$(CXXFLAGS)'

test "$prefix" = "NONE" && prefix="/usr/local"
test "$exec_prefix" = "NONE" && exec_prefix='$(prefix)'

HYSS_SUBST(HYSS_CAPIS)
HYSS_SUBST(HYSS_GEAR_EX)

HYSS_SUBST(all_targets)
HYSS_SUBST(install_targets)

HYSS_SUBST(prefix)
HYSS_SUBST(exec_prefix)
HYSS_SUBST(libdir)
HYSS_SUBST(prefix)
HYSS_SUBST(hysslibdir)
HYSS_SUBST(hyssincludedir)

HYSS_SUBST(CC)
HYSS_SUBST(CFLAGS)
HYSS_SUBST(CFLAGS_CLEAN)
HYSS_SUBST(CPP)
HYSS_SUBST(CPPFLAGS)
HYSS_SUBST(CXX)
HYSS_SUBST(CXXFLAGS)
HYSS_SUBST(CXXFLAGS_CLEAN)
HYSS_SUBST(EXTENSION_DIR)
HYSS_SUBST(HYSS_EXECUTABLE)
HYSS_SUBST(EXTRA_LDFLAGS)
HYSS_SUBST(EXTRA_LIBS)
HYSS_SUBST(INCLUDES)
HYSS_SUBST(LFLAGS)
HYSS_SUBST(LDFLAGS)
HYSS_SUBST(SHARED_LIBTOOL)
HYSS_SUBST(LIBTOOL)
HYSS_SUBST(SHELL)
HYSS_SUBST(INSTALL_HEADERS)

HYSS_GEN_BUILD_DIRS
HYSS_GEN_GLOBAL_MAKEFILE

test -d cAPIs || $hyss_shtool mkdir cAPIs

AC_CONFIG_HEADERS([config.h])

AC_OUTPUT
