/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_CLHY_H
#define HYSS_CLHY_H

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"

#include "hyss.h"
#include "main/hyss_streams.h"

/* Enable per-cAPI logging in cLHy */
#ifdef CLHYLOG_USE_CAPI
CLHYLOG_USE_CAPI(hyss);
#endif

/* Declare this so we can get to it from outside the sapi_clhydelman.c file */
extern cAPI CLHY_CAPI_DECLARE_DATA hyss_capi;

/* A way to specify the location of the hyss.ics dir in an clhy directive */
extern char *clhydelman_hyss_ics_path_override;

/* The server_context used by HYSS */
typedef struct hyss_struct {
	int state;
	request_rec *r;
	kuda_bucket_brigade *brigade;
	/* stat structure of the current file */
	gear_stat_t finfo;
	/* Whether or not we've processed HYSS in the output filters yet. */
	int request_processed;
	/* final content type */
	char *content_type;
} hyss_struct;

void *merge_hyss_config(kuda_pool_t *p, void *base_conf, void *new_conf);
void *create_hyss_config(kuda_pool_t *p, char *dummy);
char *get_hyss_config(void *conf, char *name, size_t name_len);
void apply_config(void *);
extern const command_rec hyss_dir_cmds[];
void hyss_clhy_register_hook(kuda_pool_t *p);

#define KUDA_ARRAY_FOREACH_OPEN(arr, key, val) 		\
{							\
	kuda_table_entry_t *elts;			\
	int i;						\
	elts = (kuda_table_entry_t *) arr->elts;	\
	for (i = 0; i < arr->nelts; i++) {		\
		key = elts[i].key;			\
		val = elts[i].val;

#define KUDA_ARRAY_FOREACH_CLOSE() }}

typedef struct {
	gear_bool engine;
	gear_bool xbithack;
	gear_bool last_modified;
} hyss_clhydelman_info_struct;

extern gear_capi_entry clhydelman_capi_entry;

#ifdef ZTS
extern int hyss_clhydelman_info_id;
#define CLHY(v) GEAR_PBCG(hyss_clhydelman_info_id, hyss_clhydelman_info_struct *, v)
GEAR_PBCLS_CACHE_EXTERN()
#else
extern hyss_clhydelman_info_struct hyss_clhydelman_info;
#define CLHY(v) (hyss_clhydelman_info.v)
#endif

/* fix for gcc4 visibility patch */
#ifndef HYSS_WIN32
# undef CLHY_CAPI_DECLARE_DATA
# define CLHY_CAPI_DECLARE_DATA HYSSAPI
#endif

#endif /* HYSS_CLHY_H */
