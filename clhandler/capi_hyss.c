/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define GEAR_INCLUDE_FULL_WINDOWS_HEADERS

#include "hyss.h"
#include "hyss_clhy.h"

CLHY_CAPI_DECLARE_DATA cAPI hyss_capi = {
	STANDARD16_CAPI_STUFF,
	create_hyss_config,		/* create per-directory config structure */
	merge_hyss_config,		/* merge per-directory config structures */
	NULL,				/* create per-server config structure */
	NULL,				/* merge per-server config structures */
	hyss_dir_cmds,			/* command kuda_table_t */
	hyss_clhy_register_hook		/* register hooks */
};

