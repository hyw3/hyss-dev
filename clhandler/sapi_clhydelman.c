/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define GEAR_INCLUDE_FULL_WINDOWS_HEADERS

#include "hyss.h"
#include "hyss_main.h"
#include "hyss_ics.h"
#include "hyss_variables.h"
#include "SAPI.h"

#include <fcntl.h>

#include "gear_smart_str.h"
#include "extslib/standard/hyss_standard.h"

#include "kuda_strings.h"
#include "clhy_config.h"
#include "util_filter.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_request.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_log.h"
#include "http_main.h"
#include "util_script.h"
#include "http_core.h"
#include "clhy_core.h"

#include "hyss_clhy.h"

#undef shutdown

#define HYSS_MAGIC_TYPE "application/x-wwhy-hyss"
#define HYSS_SOURCE_MAGIC_TYPE "application/x-wwhy-hyss-source"
#define HYSS_SCRIPT "hyss-script"

char *clhydelman_hyss_ics_path_override = NULL;
#if defined(HYSS_WIN32) && defined(ZTS)
GEAR_PBCLS_CACHE_DEFINE()
#endif

static size_t
hyss_clhy_sapi_ub_write(const char *str, size_t str_length)
{
	request_rec *r;
	hyss_struct *ctx;

	ctx = SG(server_context);
	r = ctx->r;

	if (clhy_rwrite(str, str_length, r) < 0) {
		hyss_handle_aborted_connection();
	}

	return str_length;
}

static int
hyss_clhy_sapi_header_handler(sapi_header_struct *sapi_header, sapi_header_op_enum op, sapi_headers_struct *sapi_headers)
{
	hyss_struct *ctx;
	char *val, *ptr;

	ctx = SG(server_context);

	switch (op) {
		case SAPI_HEADER_DELETE:
			kuda_table_unset(ctx->r->headers_out, sapi_header->header);
			return 0;

		case SAPI_HEADER_DELETE_ALL:
			kuda_table_clear(ctx->r->headers_out);
			return 0;

		case SAPI_HEADER_ADD:
		case SAPI_HEADER_REPLACE:
			val = strchr(sapi_header->header, ':');

			if (!val) {
				return 0;
			}
			ptr = val;

			*val = '\0';

			do {
				val++;
			} while (*val == ' ');

			if (!strcasecmp(sapi_header->header, "content-type")) {
				if (ctx->content_type) {
					efree(ctx->content_type);
				}
				ctx->content_type = estrdup(val);
			} else if (!strcasecmp(sapi_header->header, "content-length")) {
				kuda_off_t clen = 0;

				if (KUDA_SUCCESS != kuda_strtoff(&clen, val, (char **) NULL, 10)) {
					/* We'll fall back to strtol, since that's what we used to
					 * do anyway. */
					clen = (kuda_off_t) strtol(val, (char **) NULL, 10);
				}

				clhy_set_content_length(ctx->r, clen);
			} else if (op == SAPI_HEADER_REPLACE) {
				kuda_table_set(ctx->r->headers_out, sapi_header->header, val);
			} else {
				kuda_table_add(ctx->r->headers_out, sapi_header->header, val);
			}

			*ptr = ':';

			return SAPI_HEADER_ADD;

		default:
			return 0;
	}
}

static int
hyss_clhy_sapi_send_headers(sapi_headers_struct *sapi_headers)
{
	hyss_struct *ctx = SG(server_context);
	const char *sline = SG(sapi_headers).http_status_line;

	ctx->r->status = SG(sapi_headers).http_response_code;

	/* wwhy requires that r->status_line is set to the first digit of
	 * the status-code: */
	if (sline && strlen(sline) > 12 && strncmp(sline, "HTTP/1.", 7) == 0 && sline[8] == ' ') {
		ctx->r->status_line = kuda_pstrdup(ctx->r->pool, sline + 9);
		ctx->r->proto_num = 1000 + (sline[7]-'0');
		if ((sline[7]-'0') == 0) {
			kuda_table_set(ctx->r->subprocess_env, "force-response-1.0", "true");
		}
	}

	/*	call clhy_set_content_type only once, else each time we call it,
		configured output filters for that content type will be added */
	if (!ctx->content_type) {
		ctx->content_type = sapi_get_default_content_type();
	}
	clhy_set_content_type(ctx->r, kuda_pstrdup(ctx->r->pool, ctx->content_type));
	efree(ctx->content_type);
	ctx->content_type = NULL;

	return SAPI_HEADER_SENT_SUCCESSFULLY;
}

static kuda_size_t
hyss_clhy_sapi_read_post(char *buf, size_t count_bytes)
{
	kuda_size_t len, tlen=0;
	hyss_struct *ctx = SG(server_context);
	request_rec *r;
	kuda_bucket_brigade *brigade;

	r = ctx->r;
	brigade = ctx->brigade;
	len = count_bytes;

	/*
	 * This loop is needed because clhy_get_brigade() can return us partial data
	 * which would cause premature termination of request read. Therefor we
	 * need to make sure that if data is available we fill the buffer completely.
	 */

	while (clhy_get_brigade(r->input_filters, brigade, CLHY_MODE_READBYTES, KUDA_BLOCK_READ, len) == KUDA_SUCCESS) {
		kuda_brigade_flatten(brigade, buf, &len);
		kuda_brigade_cleanup(brigade);
		tlen += len;
		if (tlen == count_bytes || !len) {
			break;
		}
		buf += len;
		len = count_bytes - tlen;
	}

	return tlen;
}

static gear_stat_t*
hyss_clhy_sapi_get_stat(void)
{
	hyss_struct *ctx = SG(server_context);

#ifdef HYSS_WIN32
	ctx->finfo.st_uid = 0;
	ctx->finfo.st_gid = 0;
#else
	ctx->finfo.st_uid = ctx->r->finfo.user;
	ctx->finfo.st_gid = ctx->r->finfo.group;
#endif
	ctx->finfo.st_dev = ctx->r->finfo.device;
	ctx->finfo.st_ino = ctx->r->finfo.inode;
	ctx->finfo.st_atime = kuda_time_sec(ctx->r->finfo.atime);
	ctx->finfo.st_mtime = kuda_time_sec(ctx->r->finfo.mtime);
	ctx->finfo.st_ctime = kuda_time_sec(ctx->r->finfo.ctime);
	ctx->finfo.st_size = ctx->r->finfo.size;
	ctx->finfo.st_nlink = ctx->r->finfo.nlink;

	return &ctx->finfo;
}

static char *
hyss_clhy_sapi_read_cookies(void)
{
	hyss_struct *ctx = SG(server_context);
	const char *http_cookie;

	http_cookie = kuda_table_get(ctx->r->headers_in, "cookie");

	/* The SAPI interface should use 'const char *' */
	return (char *) http_cookie;
}

static char *
hyss_clhy_sapi_getenv(char *name, size_t name_len)
{
	hyss_struct *ctx = SG(server_context);
	const char *env_var;

	if (ctx == NULL) {
		return NULL;
	}

	env_var = kuda_table_get(ctx->r->subprocess_env, name);

	return (char *) env_var;
}

static void
hyss_clhy_sapi_register_variables(zval *track_vars_array)
{
	hyss_struct *ctx = SG(server_context);
	const kuda_array_header_t *arr = kuda_table_elts(ctx->r->subprocess_env);
	char *key, *val;
	size_t new_val_len;

	KUDA_ARRAY_FOREACH_OPEN(arr, key, val)
		if (!val) {
			val = "";
		}
		if (sapi_capi.input_filter(PARSE_SERVER, key, &val, strlen(val), &new_val_len)) {
			hyss_register_variable_safe(key, val, new_val_len, track_vars_array);
		}
	KUDA_ARRAY_FOREACH_CLOSE()

	if (sapi_capi.input_filter(PARSE_SERVER, "HYSS_SELF", &ctx->r->uri, strlen(ctx->r->uri), &new_val_len)) {
		hyss_register_variable_safe("HYSS_SELF", ctx->r->uri, new_val_len, track_vars_array);
	}
}

static void
hyss_clhy_sapi_flush(void *server_context)
{
	hyss_struct *ctx;
	request_rec *r;

	ctx = server_context;

	/* If we haven't registered a server_context yet,
	 * then don't bother flushing. */
	if (!server_context) {
		return;
	}

	r = ctx->r;

	sapi_send_headers();

	r->status = SG(sapi_headers).http_response_code;
	SG(headers_sent) = 1;

	if (clhy_rflush(r) < 0 || r->connection->aborted) {
		hyss_handle_aborted_connection();
	}
}

static void hyss_clhy_sapi_log_message(char *msg, int syslog_type_int)
{
	hyss_struct *ctx;
	int clhylog_type = CLHYLOG_ERR;

	ctx = SG(server_context);

	switch (syslog_type_int) {
#if LOG_EMERG != LOG_CRIT
		case LOG_EMERG:
			clhylog_type = CLHYLOG_EMERG;
			break;
#endif
#if LOG_ALERT != LOG_CRIT
		case LOG_ALERT:
			clhylog_type = CLHYLOG_ALERT;
			break;
#endif
		case LOG_CRIT:
			clhylog_type = CLHYLOG_CRIT;
			break;
		case LOG_ERR:
			clhylog_type = CLHYLOG_ERR;
			break;
		case LOG_WARNING:
			clhylog_type = CLHYLOG_WARNING;
			break;
		case LOG_NOTICE:
			clhylog_type = CLHYLOG_NOTICE;
			break;
#if LOG_INFO != LOG_NOTICE
		case LOG_INFO:
			clhylog_type = CLHYLOG_INFO;
			break;
#endif
#if LOG_NOTICE != LOG_DEBUG
		case LOG_DEBUG:
			clhylog_type = CLHYLOG_DEBUG;
			break;
#endif
	}

	if (ctx == NULL) {
		clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR | CLHYLOG_STARTUP, 0, NULL, "%s", msg);
	} else {
		clhy_log_rerror(CLHYLOG_MARK, clhylog_type, 0, ctx->r, "%s", msg);
	}
}

static void hyss_clhy_sapi_log_message_ex(char *msg, request_rec *r)
{
	if (r) {
		clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, msg, r->filename);
	} else {
		hyss_clhy_sapi_log_message(msg, -1);
	}
}

static double hyss_clhy_sapi_get_request_time(void)
{
	hyss_struct *ctx = SG(server_context);
	return ((double) kuda_time_as_msec(ctx->r->request_time)) / 1000.0;
}

extern gear_capi_entry hyss_clhy_capi;

static int hyss_clhydelman_startup(sapi_capi_struct *sapi_capi)
{
	if (hyss_capi_startup(sapi_capi, &hyss_clhy_capi, 1)==FAILURE) {
		return FAILURE;
	}
	return SUCCESS;
}

static sapi_capi_struct clhydelman_sapi_capi = {
	"clhandler",
	"cLHy Handler",

	hyss_clhydelman_startup,			/* startup */
	hyss_capi_shutdown_wrapper,			/* shutdown */

	NULL,						/* activate */
	NULL,						/* deactivate */

	hyss_clhy_sapi_ub_write,			/* unbuffered write */
	hyss_clhy_sapi_flush,				/* flush */
	hyss_clhy_sapi_get_stat,			/* get uid */
	hyss_clhy_sapi_getenv,				/* getenv */

	hyss_error,					/* error handler */

	hyss_clhy_sapi_header_handler,			/* header handler */
	hyss_clhy_sapi_send_headers,			/* send headers handler */
	NULL,						/* send header handler */

	hyss_clhy_sapi_read_post,			/* read POST data */
	hyss_clhy_sapi_read_cookies,			/* read Cookies */

	hyss_clhy_sapi_register_variables,
	hyss_clhy_sapi_log_message,			/* Log message */
	hyss_clhy_sapi_get_request_time,		/* Request Time */
	NULL,						/* Child Terminate */

	STANDARD_SAPI_CAPI_PROPERTIES
};

static kuda_status_t hyss_clhy_server_shutdown(void *tmp)
{
	clhydelman_sapi_capi.shutdown(&clhydelman_sapi_capi);
	sapi_shutdown();
#ifdef ZTS
	pbc_shutdown();
#endif
	return KUDA_SUCCESS;
}

static kuda_status_t hyss_clhy_child_shutdown(void *tmp)
{
	clhydelman_sapi_capi.shutdown(&clhydelman_sapi_capi);
#if defined(ZTS) && !defined(HYSS_WIN32)
	pbc_shutdown();
#endif
	return KUDA_SUCCESS;
}

static void hyss_clhy_add_version(kuda_pool_t *p)
{
	if (PG(expose_hyss)) {
		clhy_add_version_component(p, "HYSS/" HYSS_VERSION);
	}
}

static int hyss_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog, kuda_pool_t *ptemp)
{
#ifndef ZTS
	int threaded_clmp;

	clhy_clmp_query(CLHY_CLMPQ_IS_THREADED, &threaded_clmp);
	if(threaded_clmp) {
		clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, 0, "cLHy is running a threaded CLMP, but your HYSS cAPI is not compiled to be threadsafe.  You need to recompile HYSS.");
		return DONE;
	}
#endif
	/* When this is NULL, cLHy won't override the hard-coded default
	 * hyss.ics path setting. */
	clhydelman_hyss_ics_path_override = NULL;
	return OK;
}

static int
hyss_clhy_server_startup(kuda_pool_t *pconf, kuda_pool_t *plog, kuda_pool_t *ptemp, server_rec *s)
{
	void *data = NULL;
	const char *userdata_key = "clhydelmanhook_post_config";

	/* cLHy will load, unload and then reload a DSO cAPI. This
	 * prevents us from starting HYSS until the second load. */
	kuda_pool_userdata_get(&data, userdata_key, s->process->pool);
	if (data == NULL) {
		/* We must use set() here and *not* setn(), otherwise the
		 * static string pointed to by userdata_key will be mapped
		 * to a different location when the DSO is reloaded and the
		 * pointers won't match, causing get() to return NULL when
		 * we expected it to return non-NULL. */
		kuda_pool_userdata_set((const void *)1, userdata_key, kuda_pool_cleanup_null, s->process->pool);
		return OK;
	}

	/* Set up our overridden path. */
	if (clhydelman_hyss_ics_path_override) {
		clhydelman_sapi_capi.hyss_ics_path_override = clhydelman_hyss_ics_path_override;
	}
#ifdef ZTS
	pbc_startup(1, 1, 0, NULL);
	(void)ts_resource(0);
	GEAR_PBCLS_CACHE_UPDATE();
#endif

	gear_signal_startup();

	sapi_startup(&clhydelman_sapi_capi);
	clhydelman_sapi_capi.startup(&clhydelman_sapi_capi);
	kuda_pool_cleanup_register(pconf, NULL, hyss_clhy_server_shutdown, kuda_pool_cleanup_null);
	hyss_clhy_add_version(pconf);

	return OK;
}

static kuda_status_t hyss_server_context_cleanup(void *data_)
{
	void **data = data_;
	*data = NULL;
	return KUDA_SUCCESS;
}

static int hyss_clhy_request_ctor(request_rec *r, hyss_struct *ctx)
{
	char *content_length;
	const char *auth;

	SG(sapi_headers).http_response_code = !r->status ? HTTP_OK : r->status;
	SG(request_info).content_type = kuda_table_get(r->headers_in, "Content-Type");
	SG(request_info).query_string = kuda_pstrdup(r->pool, r->args);
	SG(request_info).request_method = r->method;
	SG(request_info).proto_num = r->proto_num;
	SG(request_info).request_uri = kuda_pstrdup(r->pool, r->uri);
	SG(request_info).path_translated = kuda_pstrdup(r->pool, r->filename);
	r->no_local_copy = 1;

	content_length = (char *) kuda_table_get(r->headers_in, "Content-Length");
	if (content_length) {
		GEAR_ATOL(SG(request_info).content_length, content_length);
	} else {
		SG(request_info).content_length = 0;
	}

	kuda_table_unset(r->headers_out, "Content-Length");
	kuda_table_unset(r->headers_out, "Last-Modified");
	kuda_table_unset(r->headers_out, "Expires");
	kuda_table_unset(r->headers_out, "ETag");

	auth = kuda_table_get(r->headers_in, "Authorization");
	hyss_handle_auth_data(auth);

	if (SG(request_info).auth_user == NULL && r->user) {
		SG(request_info).auth_user = estrdup(r->user);
	}

	ctx->r->user = kuda_pstrdup(ctx->r->pool, SG(request_info).auth_user);

	return hyss_request_startup();
}

static void hyss_clhy_request_dtor(request_rec *r)
{
	hyss_request_shutdown(NULL);
}

static void hyss_clhy_ics_dtor(request_rec *r, request_rec *p)
{
	if (strcmp(r->protocol, "INCLUDED")) {
		gear_try { gear_ics_deactivate(); } gear_end_try();
	} else {
typedef struct {
	HashTable config;
} hyss_conf_rec;
		gear_string *str;
		hyss_conf_rec *c = clhy_get_capi_config(r->per_dir_config, &hyss_capi);

		GEAR_HASH_FOREACH_STR_KEY(&c->config, str) {
			gear_restore_ics_entry(str, GEAR_ICS_STAGE_SHUTDOWN);
		} GEAR_HASH_FOREACH_END();
	}
	if (p) {
		((hyss_struct *)SG(server_context))->r = p;
	} else {
		kuda_pool_cleanup_run(r->pool, (void *)&SG(server_context), hyss_server_context_cleanup);
	}
}

static int hyss_handler(request_rec *r)
{
	hyss_struct * volatile ctx;
	void *conf;
	kuda_bucket_brigade * volatile brigade;
	kuda_bucket *bucket;
	kuda_status_t rv;
	request_rec * volatile parent_req = NULL;
#ifdef ZTS
	/* initial resource fetch */
	(void)ts_resource(0);
	GEAR_PBCLS_CACHE_UPDATE();
#endif

#define CLHYAP_ICS_OFF hyss_clhy_ics_dtor(r, parent_req);

	conf = clhy_get_capi_config(r->per_dir_config, &hyss_capi);

	/* apply_config() needs r in some cases, so allocate server_context early */
	ctx = SG(server_context);
	if (ctx == NULL || (ctx && ctx->request_processed && !strcmp(r->protocol, "INCLUDED"))) {
normal:
		ctx = SG(server_context) = kuda_pcalloc(r->pool, sizeof(*ctx));
		/* register a cleanup so we clear out the SG(server_context)
		 * after each request. Note: We pass in the pointer to the
		 * server_context in case this is handled by a different thread.
		 */
		kuda_pool_cleanup_register(r->pool, (void *)&SG(server_context), hyss_server_context_cleanup, kuda_pool_cleanup_null);
		ctx->r = r;
		ctx = NULL; /* May look weird to null it here, but it is to catch the right case in the first_try later on */
	} else {
		parent_req = ctx->r;
		ctx->r = r;
	}
	apply_config(conf);

	if (strcmp(r->handler, HYSS_MAGIC_TYPE) && strcmp(r->handler, HYSS_SOURCE_MAGIC_TYPE) && strcmp(r->handler, HYSS_SCRIPT)) {
		/* Check for xbithack in this case. */
		if (!CLHY(xbithack) || strcmp(r->handler, "text/html") || !(r->finfo.protection & KUDA_UEXECUTE)) {
			CLHYAP_ICS_OFF;
			return DECLINED;
		}
	}

	/* Give a 404 if PATH_INFO is used but is explicitly disabled in
	 * the configuration; default behaviour is to accept. */
	if (r->used_path_info == CLHY_REQ_REJECT_PATH_INFO
		&& r->path_info && r->path_info[0]) {
		CLHYAP_ICS_OFF;
		return HTTP_NOT_FOUND;
	}

	/* handle situations where user turns the engine off */
	if (!CLHY(engine)) {
		CLHYAP_ICS_OFF;
		return DECLINED;
	}

	if (r->finfo.filetype == 0) {
		hyss_clhy_sapi_log_message_ex("script '%s' not found or unable to stat", r);
		CLHYAP_ICS_OFF;
		return HTTP_NOT_FOUND;
	}
	if (r->finfo.filetype == KUDA_DIR) {
		hyss_clhy_sapi_log_message_ex("attempt to invoke directory '%s' as script", r);
		CLHYAP_ICS_OFF;
		return HTTP_FORBIDDEN;
	}

	/* Setup the CGI variables if this is the main request */
	if (r->main == NULL ||
		/* .. or if the sub-request environment differs from the main-request. */
		r->subprocess_env != r->main->subprocess_env
	) {
		/* setup standard CGI variables */
		clhy_add_common_vars(r);
		clhy_add_cgi_vars(r);
	}

gear_first_try {

	if (ctx == NULL) {
		brigade = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
		ctx = SG(server_context);
		ctx->brigade = brigade;

		if (hyss_clhy_request_ctor(r, ctx)!=SUCCESS) {
			gear_bailout();
		}
	} else {
		if (!parent_req) {
			parent_req = ctx->r;
		}
		if (parent_req && parent_req->handler &&
				strcmp(parent_req->handler, HYSS_MAGIC_TYPE) &&
				strcmp(parent_req->handler, HYSS_SOURCE_MAGIC_TYPE) &&
				strcmp(parent_req->handler, HYSS_SCRIPT)) {
			if (hyss_clhy_request_ctor(r, ctx)!=SUCCESS) {
				gear_bailout();
			}
		}

		if (parent_req && parent_req->status != HTTP_OK && parent_req->status != 413 && strcmp(r->protocol, "INCLUDED")) {
			parent_req = NULL;
			goto normal;
		}
		ctx->r = r;
		brigade = ctx->brigade;
	}

	if (CLHY(last_modified)) {
		clhy_update_mtime(r, r->finfo.mtime);
		clhy_set_last_modified(r);
	}

	/* Determine if we need to parse the file or show the source */
	if (strncmp(r->handler, HYSS_SOURCE_MAGIC_TYPE, sizeof(HYSS_SOURCE_MAGIC_TYPE) - 1) == 0) {
		gear_syntax_highlighter_ics syntax_highlighter_ics;
		hyss_get_highlight_struct(&syntax_highlighter_ics);
		highlight_file((char *)r->filename, &syntax_highlighter_ics);
	} else {
		gear_file_handle zfd;

		zfd.type = GEAR_HANDLE_FILENAME;
		zfd.filename = (char *) r->filename;
		zfd.free_filename = 0;
		zfd.opened_path = NULL;

		if (!parent_req) {
			hyss_execute_script(&zfd);
		} else {
			gear_execute_scripts(GEAR_INCLUDE, NULL, 1, &zfd);
		}

		kuda_table_set(r->notes, "capi_hyss_memory_usage",
			kuda_psprintf(ctx->r->pool, "%" KUDA_SIZE_T_FMT, gear_memory_peak_usage(1)));
	}

} gear_end_try();

	if (!parent_req) {
		hyss_clhy_request_dtor(r);
		ctx->request_processed = 1;
		kuda_brigade_cleanup(brigade);
		bucket = kuda_bucket_eos_create(r->connection->bucket_alloc);
		KUDA_BRIGADE_INSERT_TAIL(brigade, bucket);

		rv = clhy_pass_brigade(r->output_filters, brigade);
		if (rv != KUDA_SUCCESS || r->connection->aborted) {
gear_first_try {
			hyss_handle_aborted_connection();
} gear_end_try();
		}
		kuda_brigade_cleanup(brigade);
		kuda_pool_cleanup_run(r->pool, (void *)&SG(server_context), hyss_server_context_cleanup);
	} else {
		ctx->r = parent_req;
	}

	return OK;
}

static void hyss_clhy_child_init(kuda_pool_t *pchild, server_rec *s)
{
	kuda_pool_cleanup_register(pchild, NULL, hyss_clhy_child_shutdown, kuda_pool_cleanup_null);
}

#ifdef GEAR_SIGNALS
static void hyss_clhy_signal_init(kuda_pool_t *pchild, server_rec *s)
{
	gear_signal_init();
}
#endif

void hyss_clhy_register_hook(kuda_pool_t *p)
{
	clhy_hook_pre_config(hyss_pre_config, NULL, NULL, KUDA_HOOK_MIDDLE);
	clhy_hook_post_config(hyss_clhy_server_startup, NULL, NULL, KUDA_HOOK_MIDDLE);
	clhy_hook_handler(hyss_handler, NULL, NULL, KUDA_HOOK_MIDDLE);
#ifdef GEAR_SIGNALS
	clhy_hook_child_init(hyss_clhy_signal_init, NULL, NULL, KUDA_HOOK_MIDDLE);
#endif
	clhy_hook_child_init(hyss_clhy_child_init, NULL, NULL, KUDA_HOOK_MIDDLE);
}

