/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define GEAR_INCLUDE_FULL_WINDOWS_HEADERS

#include "hyss.h"
#include "hyss_ics.h"
#include "hyss_clhy.h"
#include "kuda_strings.h"
#include "clhy_config.h"
#include "util_filter.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_request.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_log.h"
#include "http_main.h"
#include "util_script.h"
#include "http_core.h"

#ifdef HYSS_CLHY_DEBUG
#define hyssapdebug(a) fprintf a
#else
#define hyssapdebug(a)
#endif

typedef struct {
	HashTable config;
} hyss_conf_rec;

typedef struct {
	char *value;
	size_t value_len;
	char status;
	char htaccess;
} hyss_dir_entry;

static const char *real_value_hnd(cmd_parms *cmd, void *dummy, const char *name, const char *value, int status)
{
	hyss_conf_rec *d = dummy;
	hyss_dir_entry e;

	hyssapdebug((stderr, "Getting %s=%s for %p (%d)\n", name, value, dummy, gear_hash_num_elements(&d->config)));

	if (!strncasecmp(value, "none", sizeof("none"))) {
		value = "";
	}

	e.value = kuda_pstrdup(cmd->pool, value);
	e.value_len = strlen(value);
	e.status = status;
	e.htaccess = ((cmd->override & (RSRC_CONF|ACCESS_CONF)) == 0);

	gear_hash_str_update_mem(&d->config, (char *) name, strlen(name), &e, sizeof(e));
	return NULL;
}

static const char *hyss_clhy_value_handler(cmd_parms *cmd, void *dummy, const char *name, const char *value)
{
	return real_value_hnd(cmd, dummy, name, value, HYSS_ICS_PERDIR);
}

static const char *hyss_clhy_admin_value_handler(cmd_parms *cmd, void *dummy, const char *name, const char *value)
{
	return real_value_hnd(cmd, dummy, name, value, HYSS_ICS_SYSTEM);
}

static const char *real_flag_hnd(cmd_parms *cmd, void *dummy, const char *arg1, const char *arg2, int status)
{
	char bool_val[2];

	if (!strcasecmp(arg2, "On") || (arg2[0] == '1' && arg2[1] == '\0')) {
		bool_val[0] = '1';
	} else {
		bool_val[0] = '0';
	}
	bool_val[1] = 0;

	return real_value_hnd(cmd, dummy, arg1, bool_val, status);
}

static const char *hyss_clhy_flag_handler(cmd_parms *cmd, void *dummy, const char *name, const char *value)
{
	return real_flag_hnd(cmd, dummy, name, value, HYSS_ICS_PERDIR);
}

static const char *hyss_clhy_admin_flag_handler(cmd_parms *cmd, void *dummy, const char *name, const char *value)
{
	return real_flag_hnd(cmd, dummy, name, value, HYSS_ICS_SYSTEM);
}

static const char *hyss_clhy_hyssics_set(cmd_parms *cmd, void *mconfig, const char *arg)
{
	if (clhydelman_hyss_ics_path_override) {
		return "Only first HYSSINIDir directive honored per configuration tree - subsequent ones ignored";
	}
	clhydelman_hyss_ics_path_override = clhy_server_root_relative(cmd->pool, arg);
	return NULL;
}

static gear_bool should_overwrite_per_dir_entry(HashTable *target_ht, zval *zv, gear_hash_key *hash_key, void *pData)
{
	hyss_dir_entry *new_per_dir_entry = Z_PTR_P(zv);
	hyss_dir_entry *orig_per_dir_entry;

	if ((orig_per_dir_entry = gear_hash_find_ptr(target_ht, hash_key->key)) == NULL) {
		return 1;
	}

	if (new_per_dir_entry->status >= orig_per_dir_entry->status) {
		hyssapdebug((stderr, "ADDING/OVERWRITING %s (%d vs. %d)\n", ZSTR_VAL(hash_key->key), new_per_dir_entry->status, orig_per_dir_entry->status));
		return 1;
	} else {
		return 0;
	}
}

void config_entry_ctor(zval *zv)
{
	hyss_dir_entry *pe = (hyss_dir_entry*)Z_PTR_P(zv);
	hyss_dir_entry *npe = malloc(sizeof(hyss_dir_entry));

	memcpy(npe, pe, sizeof(hyss_dir_entry));
	ZVAL_PTR(zv, npe);
}

void *merge_hyss_config(kuda_pool_t *p, void *base_conf, void *new_conf)
{
	hyss_conf_rec *d = base_conf, *e = new_conf, *n = NULL;
#ifdef ZTS
	gear_string *str;
	zval *data;
#endif

	n = create_hyss_config(p, "merge_hyss_config");

#ifdef ZTS
	GEAR_HASH_FOREACH_STR_KEY_VAL(&d->config, str, data) {
		gear_string *key;
		zval *new_entry;

		key = gear_string_dup(str, 1);

		new_entry = gear_hash_add(&n->config, key, data);

		config_entry_ctor(new_entry);
	} GEAR_HASH_FOREACH_END();
#else
	gear_hash_copy(&n->config, &d->config, config_entry_ctor);
#endif
	hyssapdebug((stderr, "Merge dir (%p)+(%p)=(%p)\n", base_conf, new_conf, n));
	gear_hash_merge_ex(&n->config, &e->config, config_entry_ctor, should_overwrite_per_dir_entry, NULL);
	return n;
}

char *get_hyss_config(void *conf, char *name, size_t name_len)
{
	hyss_conf_rec *d = conf;
	hyss_dir_entry *pe;

	if ((pe = gear_hash_str_find_ptr(&d->config, name, name_len)) != NULL) {
		return pe->value;
	}

	return "";
}

void apply_config(void *dummy)
{
	hyss_conf_rec *d = dummy;
	gear_string *str;
	hyss_dir_entry *data;

	GEAR_HASH_FOREACH_STR_KEY_PTR(&d->config, str, data) {
		hyssapdebug((stderr, "APPLYING (%s)(%s)\n", ZSTR_VAL(str), data->value));
		if (gear_alter_ics_entry_chars(str, data->value, data->value_len, data->status, data->htaccess?HYSS_ICS_STAGE_HTACCESS:HYSS_ICS_STAGE_ACTIVATE) == FAILURE) {
			hyssapdebug((stderr, "..FAILED\n"));
		}
	} GEAR_HASH_FOREACH_END();
}

const command_rec hyss_dir_cmds[] =
{
	CLHY_INIT_TAKE2("hyss_value", hyss_clhy_value_handler, NULL, OR_OPTIONS, "HYSS Value Modifier"),
	CLHY_INIT_TAKE2("hyss_flag", hyss_clhy_flag_handler, NULL, OR_OPTIONS, "HYSS Flag Modifier"),
	CLHY_INIT_TAKE2("hyss_admin_value", hyss_clhy_admin_value_handler, NULL, ACCESS_CONF|RSRC_CONF, "HYSS Value Modifier (Admin)"),
	CLHY_INIT_TAKE2("hyss_admin_flag", hyss_clhy_admin_flag_handler, NULL, ACCESS_CONF|RSRC_CONF, "HYSS Flag Modifier (Admin)"),
	CLHY_INIT_TAKE1("HYSSINIDir", hyss_clhy_hyssics_set, NULL, RSRC_CONF, "Directory containing the hyss.ics file"),
	{NULL}
};

static kuda_status_t destroy_hyss_config(void *data)
{
	hyss_conf_rec *d = data;

	hyssapdebug((stderr, "Destroying config %p\n", data));
	gear_hash_destroy(&d->config);

	return KUDA_SUCCESS;
}

static void config_entry_dtor(zval *zv)
{
	free((hyss_dir_entry*)Z_PTR_P(zv));
}

void *create_hyss_config(kuda_pool_t *p, char *dummy)
{
	hyss_conf_rec *newx = (hyss_conf_rec *) kuda_pcalloc(p, sizeof(*newx));

	hyssapdebug((stderr, "Creating new config (%p) for %s\n", newx, dummy));
	gear_hash_init(&newx->config, 0, NULL, config_entry_dtor, 1);
	kuda_pool_cleanup_register(p, newx, destroy_hyss_config, kuda_pool_cleanup_null);
	return (void *) newx;
}

