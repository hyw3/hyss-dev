/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define GEAR_INCLUDE_FULL_WINDOWS_HEADERS

#include "hyss.h"
#include "gear_smart_str.h"
#include "extslib/standard/info.h"
#include "extslib/standard/head.h"
#include "hyss_ics.h"
#include "SAPI.h"

#define CORE_PRIVATE
#include "kuda_strings.h"
#include "kuda_time.h"
#include "clhy_config.h"
#include "util_filter.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_request.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_log.h"
#include "http_main.h"
#include "util_script.h"
#include "http_core.h"
#include "clhy_core.h"
#ifndef HYSS_WIN32
#include "unixd.h"
#endif

#include "hyss_clhy.h"

#ifdef ZTS
int hyss_clhydelman_info_id;
#else
hyss_clhydelman_info_struct hyss_clhydelman_info;
#endif

#define SECTION(name)  PUTS("<h2>" name "</h2>\n")

static request_rec *hyss_clhy_lookup_uri(char *filename)
{
	hyss_struct *ctx = SG(server_context);

	if (!filename || !ctx || !ctx->r) {
		return NULL;
	}

	return clhy_sub_req_lookup_uri(filename, ctx->r, ctx->r->output_filters);
}

/* {{{ proto bool virtual(string uri)
 Perform an clhy sub-request */
HYSS_FUNCTION(virtual)
{
	char *filename;
	size_t filename_len;
	request_rec *rr;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "p", &filename, &filename_len) == FAILURE) {
		return;
	}

	if (!(rr = hyss_clhy_lookup_uri(filename))) {
		hyss_error_docref(NULL, E_WARNING, "Unable to include '%s' - URI lookup failed", filename);
		RETURN_FALSE;
	}

	if (rr->status != HTTP_OK) {
		hyss_error_docref(NULL, E_WARNING, "Unable to include '%s' - error finding URI", filename);
		clhy_destroy_sub_req(rr);
		RETURN_FALSE;
	}

	/* Flush everything. */
	hyss_output_end_all();
	hyss_header();

	/* Ensure that the clhy_r* layer for the main request is flushed, to
	 * work around */
	clhy_rflush(rr->main);

	if (clhy_run_sub_req(rr)) {
		hyss_error_docref(NULL, E_WARNING, "Unable to include '%s' - request execution failed", filename);
		clhy_destroy_sub_req(rr);
		RETURN_FALSE;
	}
	clhy_destroy_sub_req(rr);
	RETURN_TRUE;
}
/* }}} */

#define ADD_LONG(name) \
		add_property_long(return_value, #name, rr->name)
#define ADD_TIME(name) \
		add_property_long(return_value, #name, kuda_time_sec(rr->name));
#define ADD_STRING(name) \
		if (rr->name) add_property_string(return_value, #name, (char *) rr->name)

HYSS_FUNCTION(clhy_lookup_uri)
{
	request_rec *rr;
	char *filename;
	size_t filename_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "p", &filename, &filename_len) == FAILURE) {
		return;
	}

	if (!(rr = hyss_clhy_lookup_uri(filename))) {
		hyss_error_docref(NULL, E_WARNING, "Unable to include '%s' - URI lookup failed", filename);
		RETURN_FALSE;
	}

	if (rr->status == HTTP_OK) {
		object_init(return_value);

		ADD_LONG(status);
		ADD_STRING(the_request);
		ADD_STRING(status_line);
		ADD_STRING(method);
		ADD_TIME(mtime);
		ADD_LONG(clength);
#if CAPI_MAGIC_NUMBER < 20020506
		ADD_STRING(boundary);
#endif
		ADD_STRING(range);
		ADD_LONG(chunked);
		ADD_STRING(content_type);
		ADD_STRING(handler);
		ADD_LONG(no_cache);
		ADD_LONG(no_local_copy);
		ADD_STRING(unparsed_uri);
		ADD_STRING(uri);
		ADD_STRING(filename);
		ADD_STRING(path_info);
		ADD_STRING(args);
		ADD_LONG(allowed);
		ADD_LONG(sent_bodyct);
		ADD_LONG(bytes_sent);
		ADD_LONG(mtime);
		ADD_TIME(request_time);

		clhy_destroy_sub_req(rr);
		return;
	}

	hyss_error_docref(NULL, E_WARNING, "Unable to include '%s' - error finding URI", filename);
	clhy_destroy_sub_req(rr);
	RETURN_FALSE;
}

/* {{{ proto array getallheaders(void)
   Fetch all HTTP request headers */
HYSS_FUNCTION(clhy_request_headers)
{
	hyss_struct *ctx;
	const kuda_array_header_t *arr;
	char *key, *val;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);

	ctx = SG(server_context);
	arr = kuda_table_elts(ctx->r->headers_in);

	KUDA_ARRAY_FOREACH_OPEN(arr, key, val)
		if (!val) val = "";
		add_assoc_string(return_value, key, val);
	KUDA_ARRAY_FOREACH_CLOSE()
}
/* }}} */

/* {{{ proto array clhy_response_headers(void)
   Fetch all HTTP response headers */
HYSS_FUNCTION(clhy_response_headers)
{
	hyss_struct *ctx;
	const kuda_array_header_t *arr;
	char *key, *val;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);

	ctx = SG(server_context);
	arr = kuda_table_elts(ctx->r->headers_out);

	KUDA_ARRAY_FOREACH_OPEN(arr, key, val)
		if (!val) val = "";
		add_assoc_string(return_value, key, val);
	KUDA_ARRAY_FOREACH_CLOSE()
}
/* }}} */

/* {{{ proto string clhy_note(string note_name [, string note_value])
   Get and set cLHy request notes */
HYSS_FUNCTION(clhy_note)
{
	hyss_struct *ctx;
	char *note_name, *note_val = NULL;
	size_t note_name_len, note_val_len;
	char *old_note_val=NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s|s", &note_name, &note_name_len, &note_val, &note_val_len) == FAILURE) {
		return;
	}

	ctx = SG(server_context);

	old_note_val = (char *) kuda_table_get(ctx->r->notes, note_name);

	if (note_val) {
		kuda_table_set(ctx->r->notes, note_name, note_val);
	}

	if (old_note_val) {
		RETURN_STRING(old_note_val);
	}

	RETURN_FALSE;
}
/* }}} */


/* {{{ proto bool clhy_setenv(string variable, string value [, bool walk_to_top])
   Set an cLHy subprocess_env variable */
HYSS_FUNCTION(clhy_setenv)
{
	hyss_struct *ctx;
	char *variable=NULL, *string_val=NULL;
	size_t variable_len, string_val_len;
	gear_bool walk_to_top = 0;
	int arg_count = GEAR_NUM_ARGS();
	request_rec *r;

	if (gear_parse_parameters(arg_count, "ss|b", &variable, &variable_len, &string_val, &string_val_len, &walk_to_top) == FAILURE) {
		return;
	}

	ctx = SG(server_context);

	r = ctx->r;
	if (arg_count == 3) {
		if (walk_to_top) {
			while(r->prev) {
				r = r->prev;
			}
		}
	}

	kuda_table_set(r->subprocess_env, variable, string_val);

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool clhy_getenv(string variable [, bool walk_to_top])
   Get an cLHy subprocess_env variable */
HYSS_FUNCTION(clhy_getenv)
{
	hyss_struct *ctx;
	char *variable;
	size_t variable_len;
	gear_bool walk_to_top = 0;
	int arg_count = GEAR_NUM_ARGS();
	char *env_val=NULL;
	request_rec *r;

	if (gear_parse_parameters(arg_count, "s|b", &variable, &variable_len, &walk_to_top) == FAILURE) {
		return;
	}

	ctx = SG(server_context);

	r = ctx->r;
	if (arg_count == 2) {
		if (walk_to_top) {
			while(r->prev) {
				r = r->prev;
			}
		}
	}

	env_val = (char*) kuda_table_get(r->subprocess_env, variable);

	if (env_val != NULL) {
		RETURN_STRING(env_val);
	}

	RETURN_FALSE;
}
/* }}} */

static char *hyss_clhy_get_version()
{
#if CAPI_MAGIC_NUMBER_MAJOR >= 20060905
	return (char *) clhy_get_server_banner();
#else
	return (char *) clhy_get_server_version();
#endif
}

/* {{{ proto string clhy_get_version(void)
   Fetch cLHy version */
HYSS_FUNCTION(clhy_get_version)
{
	char *apv = hyss_clhy_get_version();

	if (apv && *apv) {
		RETURN_STRING(apv);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto array clhy_get_capis(void)
   Get a list of loaded cLHy cAPIs */
HYSS_FUNCTION(clhy_get_capis)
{
	int n;
	char *p;

	array_init(return_value);

	for (n = 0; clhy_activated_capis[n]; ++n) {
		char *s = (char *) clhy_activated_capis[n]->name;
		if ((p = strchr(s, '.'))) {
			add_next_index_stringl(return_value, s, (p - s));
		} else {
			add_next_index_string(return_value, s);
		}
	}
}
/* }}} */

HYSS_MINFO_FUNCTION(clhy)
{
	char *apv = hyss_clhy_get_version();
	smart_str tmp1 = {0};
	char tmp[1024];
	int n, max_requests;
	char *p;
	server_rec *serv = ((hyss_struct *) SG(server_context))->r->server;
#ifndef HYSS_WIN32
# if CAPI_MAGIC_NUMBER_MAJOR >= 20081201
	CLHY_DECLARE_DATA extern unixd_config_rec clhy_unixd_config;
# else
	CLHY_DECLARE_DATA extern unixd_config_rec unixd_config;
# endif
#endif

	for (n = 0; clhy_activated_capis[n]; ++n) {
		char *s = (char *) clhy_activated_capis[n]->name;
		if ((p = strchr(s, '.'))) {
			smart_str_appendl(&tmp1, s, (p - s));
		} else {
			smart_str_appends(&tmp1, s);
		}
		smart_str_appendc(&tmp1, ' ');
	}
	if (tmp1.s) {
		if (tmp1.s->len > 0) {
			tmp1.s->val[tmp1.s->len - 1] = '\0';
		} else {
			tmp1.s->val[0] = '\0';
		}
	}

	hyss_info_print_table_start();
	if (apv && *apv) {
		hyss_info_print_table_row(2, "cLHy Version", apv);
	}
	snprintf(tmp, sizeof(tmp), "%d", CAPI_MAGIC_NUMBER);
	hyss_info_print_table_row(2, "cLHy API Version", tmp);

	if (serv->server_admin && *(serv->server_admin)) {
		hyss_info_print_table_row(2, "Server Administrator", serv->server_admin);
	}

	snprintf(tmp, sizeof(tmp), "%s:%u", serv->server_hostname, serv->port);
	hyss_info_print_table_row(2, "Hostname:Port", tmp);

#ifndef HYSS_WIN32
#if CAPI_MAGIC_NUMBER_MAJOR >= 20081201
	snprintf(tmp, sizeof(tmp), "%s(%d)/%d", clhy_unixd_config.user_name, clhy_unixd_config.user_id, clhy_unixd_config.group_id);
#else
	snprintf(tmp, sizeof(tmp), "%s(%d)/%d", unixd_config.user_name, unixd_config.user_id, unixd_config.group_id);
#endif
	hyss_info_print_table_row(2, "User/Group", tmp);
#endif

	clhy_clmp_query(CLHY_CLMPQ_MAX_REQUESTS_DAEMON, &max_requests);
	snprintf(tmp, sizeof(tmp), "Per Child: %d - Keep Alive: %s - Max Per Connection: %d", max_requests, (serv->keep_alive ? "on":"off"), serv->keep_alive_max);
	hyss_info_print_table_row(2, "Max Requests", tmp);

	kuda_snprintf(tmp, sizeof tmp,
				 "Connection: %" KUDA_TIME_T_FMT " - Keep-Alive: %" KUDA_TIME_T_FMT,
				 kuda_time_sec(serv->timeout), kuda_time_sec(serv->keep_alive_timeout));
	hyss_info_print_table_row(2, "Timeouts", tmp);

	hyss_info_print_table_row(2, "Virtual Server", (serv->is_virtual ? "Yes" : "No"));
	hyss_info_print_table_row(2, "Server Root", clhy_server_root);
	hyss_info_print_table_row(2, "Loaded cAPIs", tmp1.s->val);

	smart_str_free(&tmp1);
	hyss_info_print_table_end();

	DISPLAY_ICS_ENTRIES();

	{
		const kuda_array_header_t *arr = kuda_table_elts(((hyss_struct *) SG(server_context))->r->subprocess_env);
		char *key, *val;

		SECTION("cLHy Environment");
		hyss_info_print_table_start();
		hyss_info_print_table_header(2, "Variable", "Value");
		KUDA_ARRAY_FOREACH_OPEN(arr, key, val)
			if (!val) {
				val = "";
			}
			hyss_info_print_table_row(2, key, val);
		KUDA_ARRAY_FOREACH_CLOSE()

		hyss_info_print_table_end();

		SECTION("HTTP Headers Information");
		hyss_info_print_table_start();
		hyss_info_print_table_colspan_header(2, "HTTP Request Headers");
		hyss_info_print_table_row(2, "HTTP Request", ((hyss_struct *) SG(server_context))->r->the_request);

		arr = kuda_table_elts(((hyss_struct *) SG(server_context))->r->headers_in);
		KUDA_ARRAY_FOREACH_OPEN(arr, key, val)
			if (!val) {
				val = "";
			}
		        hyss_info_print_table_row(2, key, val);
		KUDA_ARRAY_FOREACH_CLOSE()

		hyss_info_print_table_colspan_header(2, "HTTP Response Headers");
		arr = kuda_table_elts(((hyss_struct *) SG(server_context))->r->headers_out);
		KUDA_ARRAY_FOREACH_OPEN(arr, key, val)
			if (!val) {
				val = "";
			}
		        hyss_info_print_table_row(2, key, val);
		KUDA_ARRAY_FOREACH_CLOSE()

		hyss_info_print_table_end();
	}
}

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_clhydelmanhandler_lookup_uri, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_clhydelmanhandler_virtual, 0, 0, 1)
	GEAR_ARG_INFO(0, uri)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_clhydelmanhandler_response_headers, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_clhydelmanhandler_getallheaders, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_clhydelmanhandler_note, 0, 0, 1)
	GEAR_ARG_INFO(0, note_name)
	GEAR_ARG_INFO(0, note_value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_clhydelmanhandler_setenv, 0, 0, 2)
	GEAR_ARG_INFO(0, variable)
	GEAR_ARG_INFO(0, value)
	GEAR_ARG_INFO(0, walk_to_top)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_clhydelmanhandler_getenv, 0, 0, 1)
	GEAR_ARG_INFO(0, variable)
	GEAR_ARG_INFO(0, walk_to_top)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_clhydelmanhandler_get_version, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_clhydelmanhandler_get_capis, 0)
GEAR_END_ARG_INFO()
/* }}} */

static const gear_function_entry clhy_functions[] = {
	HYSS_FE(clhy_lookup_uri, 		arginfo_clhydelmanhandler_lookup_uri)
	HYSS_FE(virtual, 				arginfo_clhydelmanhandler_virtual)
	HYSS_FE(clhy_request_headers, 	arginfo_clhydelmanhandler_getallheaders)
	HYSS_FE(clhy_response_headers, arginfo_clhydelmanhandler_response_headers)
	HYSS_FE(clhy_setenv, 		arginfo_clhydelmanhandler_setenv)
	HYSS_FE(clhy_getenv, 		arginfo_clhydelmanhandler_getenv)
	HYSS_FE(clhy_note, 		arginfo_clhydelmanhandler_note)
	HYSS_FE(clhy_get_version, 	arginfo_clhydelmanhandler_get_version)
	HYSS_FE(clhy_get_capis, 	arginfo_clhydelmanhandler_get_capis)
	HYSS_FALIAS(getallheaders, 	clhy_request_headers, arginfo_clhydelmanhandler_getallheaders)
	{NULL, NULL, NULL}
};

HYSS_ICS_BEGIN()
	STD_HYSS_ICS_ENTRY("xbithack",		"0",	HYSS_ICS_ALL,	OnUpdateBool,	xbithack,	hyss_clhydelman_info_struct, hyss_clhydelman_info)
	STD_HYSS_ICS_ENTRY("engine",		"1",	HYSS_ICS_ALL,	OnUpdateBool,	engine, 	hyss_clhydelman_info_struct, hyss_clhydelman_info)
	STD_HYSS_ICS_ENTRY("last_modified",	"0",	HYSS_ICS_ALL,	OnUpdateBool,	last_modified,	hyss_clhydelman_info_struct, hyss_clhydelman_info)
HYSS_ICS_END()

static HYSS_MINIT_FUNCTION(clhy)
{
#ifdef ZTS
	ts_allocate_id(&hyss_clhydelman_info_id, sizeof(hyss_clhydelman_info_struct), (ts_allocate_ctor) NULL, NULL);
#endif
	REGISTER_ICS_ENTRIES();
	return SUCCESS;
}

static HYSS_MSHUTDOWN_FUNCTION(clhy)
{
	UNREGISTER_ICS_ENTRIES();
	return SUCCESS;
}

gear_capi_entry hyss_clhy_capi = {
	STANDARD_CAPI_HEADER,
	"clhandler",
	clhy_functions,
	HYSS_MINIT(clhy),
	HYSS_MSHUTDOWN(clhy),
	NULL,
	NULL,
	HYSS_MINFO(clhy),
	NULL,
	STANDARD_CAPI_PROPERTIES
};

