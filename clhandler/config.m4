dnl config.m4 for sapi clhandler

HYSS_ARG_WITH(clhyext,,
[  --with-clhyext[=FILE]       Build shared cLHy Handler cAPI. FILE is the optional
                          pathname to the cLHy clhyext tool [clhyext]], no, no)

AC_MSG_CHECKING([for cLHy handler-cAPI support via DSO through CLHYEXT])

if test "$HYSS_CLHYEXT" != "no"; then
  if test "$HYSS_CLHYEXT" = "yes"; then
    CLHYEXT=clhyext
    $CLHYEXT -q CFLAGS >/dev/null 2>&1
    if test "$?" != "0" && test -x /usr/sbin/clhyext; then
      CLHYEXT=/usr/sbin/clhyext
    fi
  else
    HYSS_EXPAND_PATH($HYSS_CLHYEXT, CLHYEXT)
  fi

  $CLHYEXT -q CFLAGS >/dev/null 2>&1
  if test "$?" != "0"; then
    AC_MSG_RESULT()
    AC_MSG_RESULT()
    AC_MSG_RESULT([Sorry, I cannot run clhyext.  Possible reasons follow:])
    AC_MSG_RESULT()
    AC_MSG_RESULT([1. Perl is not installed])
    AC_MSG_RESULT([2. clhyext was not found. Try to pass the path using --with-clhyext=/path/to/clhyext])
    AC_MSG_RESULT([3. cLHy was not built using --enable-so (the clhyext usage page is displayed)])
    AC_MSG_RESULT()
    AC_MSG_RESULT([The output of $CLHYEXT follows:])
    $CLHYEXT -q CFLAGS
    AC_MSG_ERROR([Aborting])
  fi

  CLHYEXT_INCLUDEDIR=`$CLHYEXT -q INCLUDEDIR`
  CLHYEXT_BINDIR=`$CLHYEXT -q BINDIR`
  CLHYEXT_WWHY=`$CLHYEXT -q SBINDIR`/`$CLHYEXT -q TARGET`
  CLHYEXT_CFLAGS=`$CLHYEXT -q CFLAGS`
  KUDELMAN_BINDIR=`$CLHYEXT -q KUDELMAN_BINDIR`
  KUDA_BINDIR=`$CLHYEXT -q KUDA_BINDIR`

  # Pick up ap[ru]-N-config if using wwhy >=2.1
  KUDA_CONFIG=`$CLHYEXT -q KUDA_CONFIG 2>/dev/null ||
    echo $KUDA_BINDIR/apr-config`
  KUDELMAN_CONFIG=`$CLHYEXT -q KUDELMAN_CONFIG 2>/dev/null ||
    echo $KUDELMAN_BINDIR/apu-config`

  KUDA_CFLAGS="`$KUDA_CONFIG --cppflags --includes`"
  KUDELMAN_CFLAGS="`$KUDELMAN_CONFIG --includes`"

  for flag in $CLHYEXT_CFLAGS; do
    case $flag in
    -D*) CLHY_CPPFLAGS="$CLHY_CPPFLAGS $flag";;
    esac
  done

  CLHY_CFLAGS="$CLHY_CPPFLAGS -I$CLHYEXT_INCLUDEDIR $KUDA_CFLAGS $KUDELMAN_CFLAGS -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1"

  # Test that we're trying to configure with cLHy
  HYSS_CLHY_EXTRACT_VERSION($CLHYEXT_WWHY)
  if test "$CLHY_VERSION" -le 2000000; then
    AC_MSG_ERROR([You have enabled cLHy support while your server is cLHy.  Please use the appropriate switch --with-clhyext (without the 2)])
  elif test "$CLHY_VERSION" -lt 2000044; then
    AC_MSG_ERROR([Please note that cLHy version >= 1.6 is required])
  fi

  CLHYEXT_LIBEXECDIR='$(INSTALL_ROOT)'`$CLHYEXT -q LIBEXECDIR`
  if test -z `$CLHYEXT -q SYSCONFDIR`; then
    INSTALL_IT="\$(mkinstalldirs) '$CLHYEXT_LIBEXECDIR' && \
                 $CLHYEXT -S LIBEXECDIR='$CLHYEXT_LIBEXECDIR' \
                       -i -n hyss"
  else
    CLHYEXT_SYSCONFDIR='$(INSTALL_ROOT)'`$CLHYEXT -q SYSCONFDIR`
    INSTALL_IT="\$(mkinstalldirs) '$CLHYEXT_LIBEXECDIR' && \
                \$(mkinstalldirs) '$CLHYEXT_SYSCONFDIR' && \
                 $CLHYEXT -S LIBEXECDIR='$CLHYEXT_LIBEXECDIR' \
                       -S SYSCONFDIR='$CLHYEXT_SYSCONFDIR' \
                       -i -a -n hyss"
  fi

  case $host_alias in
  *aix*)
    EXTRA_LDFLAGS="$EXTRA_LDFLAGS -Wl,-brtl -Wl,-bI:$CLHYEXT_LIBEXECDIR/wwhy.exp"
    HYSS_SELECT_SAPI(clhandler, shared, capi_hyss.c sapi_clhydelman.c clhy_config.c hyss_functions.c, $CLHY_CFLAGS)
    INSTALL_IT="$INSTALL_IT $SAPI_LIBTOOL"
    ;;
  *darwin*)
    dnl When using bundles on Darwin, we must resolve all symbols.  However,
    dnl the linker does not recursively look at the bundle loader and
    dnl pull in its dependencies.  Therefore, we must pull in the APR
    dnl and APR-util libraries.
    if test -x "$KUDA_CONFIG"; then
        MH_BUNDLE_FLAGS="`$KUDA_CONFIG --ldflags --link-ld --libs`"
    fi
    if test -x "$KUDELMAN_CONFIG"; then
        MH_BUNDLE_FLAGS="`$KUDELMAN_CONFIG --ldflags --link-ld --libs` $MH_BUNDLE_FLAGS"
    fi
    MH_BUNDLE_FLAGS="-bundle -bundle_loader $CLHYEXT_WWHY $MH_BUNDLE_FLAGS"
    HYSS_SUBST(MH_BUNDLE_FLAGS)
    HYSS_SELECT_SAPI(clhandler, bundle, capi_hyss.c sapi_clhydelman.c clhy_config.c hyss_functions.c, $CLHY_CFLAGS)
    SAPI_SHARED=libs/libhyss.so
    INSTALL_IT="$INSTALL_IT $SAPI_SHARED"
    ;;
  *)
    HYSS_SELECT_SAPI(clhandler, shared, capi_hyss.c sapi_clhydelman.c clhy_config.c hyss_functions.c, $CLHY_CFLAGS)
    INSTALL_IT="$INSTALL_IT $SAPI_LIBTOOL"
    ;;
  esac

  if test "$CLHY_VERSION" -lt 1006000; then
    CLHYEXT_CLMP=`$CLHYEXT -q CLMP_NAME`
    if test "$CLHYEXT_CLMP" != "prefork" && test "$CLHYEXT_CLMP" != "peruser" && test "$CLHYEXT_CLMP" != "itk"; then
      HYSS_BUILD_THREAD_SAFE
    fi
  else
    CLHY_THREADED_CLMP=`$CLHYEXT_WWHY -V | grep 'threaded:.*yes'`
    if test -n "$CLHY_THREADED_CLMP"; then
      HYSS_BUILD_THREAD_SAFE
    fi
  fi
  AC_MSG_RESULT(yes)
  HYSS_SUBST(CLHYEXT)
else
  AC_MSG_RESULT(no)
fi

dnl ## Local Variables:
dnl ## tab-width: 4
dnl ## End:
