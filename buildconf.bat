@echo off
cscript /nologo win32\build\buildconf.js %*
SET HYSS_BUILDCONF_PATH=%~dp0
copy %HYSS_BUILDCONF_PATH%\win32\build\configure.bat %HYSS_BUILDCONF_PATH% > nul
SET HYSS_SDK_SCRIPT_PATH=

IF NOT EXIST %HYSS_BUILDCONF_PATH% (echo Error generating configure script, configure script was not copied) ELSE (echo Now run 'configure --help')
