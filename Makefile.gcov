# HySS - Hyang Server Scripts
#
# Copyright (C) 2019-2020 Hyang Language Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# LCOV
#

LCOV_INCLUDE="."

lcov: lcov-html

lcov-test: lcov-clean-data test

hyss_lcov.info: lcov-test
	@echo "Generating data for $@"
	@rm -rf lcov_data/
	@$(mkinstalldirs) lcov_data/
	@echo
	-@files=`find . -name \*.gcda -o -name \*.gcno -o -name \*.da -o -name \*.c -o -name \*.h | sed -e 's/^\.\///' | sed -e 's/\.gcda//g' -e 's/\.gcno//g' -e 's/\.da//g' | $(EGREP) $(LCOV_INCLUDE) | sed -e 's/.libs/zzzz/g' | sort | sed -e 's/zzzz/.libs/g' | uniq` ;\
	for x in $$files; do \
		echo -n . ;\
		y=`echo $$x | sed -e 's!\.libs/!!'`; \
		dir=lcov_data/`dirname $$x`; \
		test -d "$$dir" || $(mkinstalldirs) "$$dir"; \
		if test -f "$(top_srcdir)/$$y.c"; then \
			ln -f -s $(top_srcdir)/$$y.c lcov_data/$$y.c; \
		fi; \
		if test -f "$(top_srcdir)/$$y.h"; then \
			ln -f -s $(top_srcdir)/$$y.h lcov_data/$$y.h; \
		fi; \
		if test -f "$(top_srcdir)/$$y.re"; then \
			ln -f -s $(top_srcdir)/$$y.re lcov_data/$$y.re; \
		fi; \
		if test -f "$(top_srcdir)/$$y.y"; then \
		        ln -f -s $(top_srcdir)/$$y.y lcov_data/$$y.y; \
		fi; \
		if test -f "$(top_srcdir)/$$y.l"; then \
		        ln -f -s $(top_srcdir)/$$y.l lcov_data/$$y.l; \
		fi; \
		if test -f "$(top_srcdir)/$$y"; then \
		        ln -f -s $(top_srcdir)/$$y lcov_data/$$y; \
		fi; \
		if test -f "$(top_builddir)/$$y.c"; then \
			ln -f -s $(top_builddir)/$$y.c lcov_data/$$y.c; \
		fi; \
		if test -f "$$x.gcno"; then \
			cp $$x.gcno lcov_data/$$y.gcno ; \
		fi; \
		if test -f "$$x.gcda"; then \
			cp $$x.gcda lcov_data/$$y.gcda ; \
		fi; \
		if test -f "$$x.da"; then \
			cp $$x.da   lcov_data/$$y.da ; \
		fi; \
		if test -f "$$x.bb"; then \
			cp $$x.bb   lcov_data/$$y.bb ; \
		fi; \
		if test -f "$$x.bbg"; then \
			cp $$x.bbg  lcov_data/$$y.bbg ; \
		fi; \
	done; \
	for dir in extslib/bcmath/libbcmath extslib/date/lib extslib/fileinfo/libmagic extslib/gd/libgd extslib/mbstring/libmbfl extslib/mbstring/oniguruma extslib/pcre/pcrelib extslib/pdo_sqlite/libsqlite extslib/sqlite3/libsqlite extslib/xmlrpc/libxmlrpc extslib/zip/lib; do \
		if test -d lcov_data/$$dir; then \
			rm -rf lcov_data/$$dir ; \
		fi; \
	done
	@echo
	@echo "Generating $@"
	@$(LTP) --directory lcov_data/ --capture --base-directory=lcov_data --output-file $@

lcov-html: hyss_lcov.info
	@echo "Generating lcov HTML"
	@$(LTP_GENHTML) --legend --output-directory lcov_html/ --title "HYSS Code Coverage" hyss_lcov.info

lcov-clean:
	rm -f hyss_lcov.info
	rm -rf lcov_data/
	rm -rf lcov_html/

lcov-clean-data:
	@find . -name \*.gcda -o -name \*.da -o -name \*.bbg? | xargs rm -f
