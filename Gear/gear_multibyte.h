/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_MULTIBYTE_H
#define GEAR_MULTIBYTE_H

typedef struct _gear_encoding gear_encoding;

typedef size_t (*gear_encoding_filter)(unsigned char **str, size_t *str_length, const unsigned char *buf, size_t length);

typedef const gear_encoding* (*gear_encoding_fetcher)(const char *encoding_name);
typedef const char* (*gear_encoding_name_getter)(const gear_encoding *encoding);
typedef int (*gear_encoding_lexer_compatibility_checker)(const gear_encoding *encoding);
typedef const gear_encoding *(*gear_encoding_detector)(const unsigned char *string, size_t length, const gear_encoding **list, size_t list_size);
typedef size_t (*gear_encoding_converter)(unsigned char **to, size_t *to_length, const unsigned char *from, size_t from_length, const gear_encoding *encoding_to, const gear_encoding *encoding_from);
typedef int (*gear_encoding_list_parser)(const char *encoding_list, size_t encoding_list_len, const gear_encoding ***return_list, size_t *return_size, int persistent);
typedef const gear_encoding *(*gear_encoding_internal_encoding_getter)(void);
typedef int (*gear_encoding_internal_encoding_setter)(const gear_encoding *encoding);

typedef struct _gear_multibyte_functions {
    const char *provider_name;
    gear_encoding_fetcher encoding_fetcher;
    gear_encoding_name_getter encoding_name_getter;
    gear_encoding_lexer_compatibility_checker lexer_compatibility_checker;
    gear_encoding_detector encoding_detector;
    gear_encoding_converter encoding_converter;
    gear_encoding_list_parser encoding_list_parser;
    gear_encoding_internal_encoding_getter internal_encoding_getter;
    gear_encoding_internal_encoding_setter internal_encoding_setter;
} gear_multibyte_functions;

/*
 * gear multibyte APIs
 */
BEGIN_EXTERN_C()

GEAR_API extern const gear_encoding *gear_multibyte_encoding_utf32be;
GEAR_API extern const gear_encoding *gear_multibyte_encoding_utf32le;
GEAR_API extern const gear_encoding *gear_multibyte_encoding_utf16be;
GEAR_API extern const gear_encoding *gear_multibyte_encoding_utf16le;
GEAR_API extern const gear_encoding *gear_multibyte_encoding_utf8;

/* multibyte utility functions */
GEAR_API int gear_multibyte_set_functions(const gear_multibyte_functions *functions);
GEAR_API void gear_multibyte_restore_functions(void);
GEAR_API const gear_multibyte_functions *gear_multibyte_get_functions(void);

GEAR_API const gear_encoding *gear_multibyte_fetch_encoding(const char *name);
GEAR_API const char *gear_multibyte_get_encoding_name(const gear_encoding *encoding);
GEAR_API int gear_multibyte_check_lexer_compatibility(const gear_encoding *encoding);
GEAR_API const gear_encoding *gear_multibyte_encoding_detector(const unsigned char *string, size_t length, const gear_encoding **list, size_t list_size);
GEAR_API size_t gear_multibyte_encoding_converter(unsigned char **to, size_t *to_length, const unsigned char *from, size_t from_length, const gear_encoding *encoding_to, const gear_encoding *encoding_from);
GEAR_API int gear_multibyte_parse_encoding_list(const char *encoding_list, size_t encoding_list_len, const gear_encoding ***return_list, size_t *return_size, int persistent);

GEAR_API const gear_encoding *gear_multibyte_get_internal_encoding(void);
GEAR_API const gear_encoding *gear_multibyte_get_script_encoding(void);
GEAR_API int gear_multibyte_set_script_encoding(const gear_encoding **encoding_list, size_t encoding_list_size);
GEAR_API int gear_multibyte_set_internal_encoding(const gear_encoding *encoding);
GEAR_API int gear_multibyte_set_script_encoding_by_string(const char *new_value, size_t new_value_length);

END_EXTERN_C()

#endif /* GEAR_MULTIBYTE_H */

