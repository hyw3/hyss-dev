/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "gear.h"
#include "gear_API.h"
#include "gear_ast.h"
#include "gear_globals.h"
#include "gear_constants.h"
#include "gear_list.h"

static void GEAR_FASTCALL gear_string_destroy(gear_string *str);
static void GEAR_FASTCALL gear_reference_destroy(gear_reference *ref);
static void GEAR_FASTCALL gear_empty_destroy(gear_reference *ref);

#if GEAR_DEBUG
static void GEAR_FASTCALL gear_array_destroy_wrapper(gear_array *arr);
static void GEAR_FASTCALL gear_object_destroy_wrapper(gear_object *obj);
static void GEAR_FASTCALL gear_resource_destroy_wrapper(gear_resource *res);
static void GEAR_FASTCALL gear_ast_ref_destroy_wrapper(gear_ast_ref *ast);
#else
# define gear_array_destroy_wrapper    gear_array_destroy
# define gear_object_destroy_wrapper   gear_objects_store_del
# define gear_resource_destroy_wrapper gear_list_free
# define gear_ast_ref_destroy_wrapper  gear_ast_ref_destroy
#endif

typedef void (GEAR_FASTCALL *gear_rc_dtor_func_t)(gear_refcounted *p);

static const gear_rc_dtor_func_t gear_rc_dtor_func[] = {
	/* IS_UNDEF        */ (gear_rc_dtor_func_t)gear_empty_destroy,
	/* IS_NULL         */ (gear_rc_dtor_func_t)gear_empty_destroy,
	/* IS_FALSE        */ (gear_rc_dtor_func_t)gear_empty_destroy,
	/* IS_TRUE         */ (gear_rc_dtor_func_t)gear_empty_destroy,
	/* IS_LONG         */ (gear_rc_dtor_func_t)gear_empty_destroy,
	/* IS_DOUBLE       */ (gear_rc_dtor_func_t)gear_empty_destroy,
	/* IS_STRING       */ (gear_rc_dtor_func_t)gear_string_destroy,
	/* IS_ARRAY        */ (gear_rc_dtor_func_t)gear_array_destroy_wrapper,
	/* IS_OBJECT       */ (gear_rc_dtor_func_t)gear_object_destroy_wrapper,
	/* IS_RESOURCE     */ (gear_rc_dtor_func_t)gear_resource_destroy_wrapper,
	/* IS_REFERENCE    */ (gear_rc_dtor_func_t)gear_reference_destroy,
	/* IS_CONSTANT_AST */ (gear_rc_dtor_func_t)gear_ast_ref_destroy_wrapper
};

GEAR_API void GEAR_FASTCALL rc_dtor_func(gear_refcounted *p)
{
	GEAR_ASSERT(GC_TYPE(p) <= IS_CONSTANT_AST);
	gear_rc_dtor_func[GC_TYPE(p)](p);
}

static void GEAR_FASTCALL gear_string_destroy(gear_string *str)
{
	CHECK_ZVAL_STRING(str);
	GEAR_ASSERT(!ZSTR_IS_INTERNED(str));
	GEAR_ASSERT(GC_REFCOUNT(str) == 0);
	GEAR_ASSERT(!(GC_FLAGS(str) & IS_STR_PERSISTENT));
	efree(str);
}

static void GEAR_FASTCALL gear_reference_destroy(gear_reference *ref)
{
	i_zval_ptr_dtor(&ref->val GEAR_FILE_LINE_CC);
	efree_size(ref, sizeof(gear_reference));
}

static void GEAR_FASTCALL gear_empty_destroy(gear_reference *ref)
{
}

#if GEAR_DEBUG
static void GEAR_FASTCALL gear_array_destroy_wrapper(gear_array *arr)
{
	gear_array_destroy(arr);
}

static void GEAR_FASTCALL gear_object_destroy_wrapper(gear_object *obj)
{
	gear_objects_store_del(obj);
}

static void GEAR_FASTCALL gear_resource_destroy_wrapper(gear_resource *res)
{
	gear_list_free(res);
}

static void GEAR_FASTCALL gear_ast_ref_destroy_wrapper(gear_ast_ref *ast)
{
	gear_ast_ref_destroy(ast);
}
#endif

GEAR_API void zval_ptr_dtor(zval *zval_ptr) /* {{{ */
{
	i_zval_ptr_dtor(zval_ptr GEAR_FILE_LINE_CC);
}
/* }}} */

GEAR_API void zval_internal_ptr_dtor(zval *zval_ptr) /* {{{ */
{
	if (Z_REFCOUNTED_P(zval_ptr)) {
		gear_refcounted *ref = Z_COUNTED_P(zval_ptr);

		if (GC_DELREF(ref) == 0) {
			if (Z_TYPE_P(zval_ptr) == IS_STRING) {
				gear_string *str = (gear_string*)ref;

				CHECK_ZVAL_STRING(str);
				GEAR_ASSERT(!ZSTR_IS_INTERNED(str));
				GEAR_ASSERT((GC_FLAGS(str) & IS_STR_PERSISTENT));
				free(str);
			} else {
				gear_error_noreturn(E_CORE_ERROR, "Internal zval's can't be arrays, objects, resources or reference");
			}
		}
	}
}
/* }}} */

/* This function should only be used as a copy constructor, i.e. it
 * should only be called AFTER a zval has been copied to another
 * location using ZVAL_COPY_VALUE. Do not call it before copying,
 * otherwise a reference may be leaked. */
GEAR_API void zval_add_ref(zval *p)
{
	if (Z_REFCOUNTED_P(p)) {
		if (Z_ISREF_P(p) && Z_REFCOUNT_P(p) == 1) {
			ZVAL_COPY(p, Z_REFVAL_P(p));
		} else {
			Z_ADDREF_P(p);
		}
	}
}

GEAR_API void GEAR_FASTCALL zval_copy_ctor_func(zval *zvalue)
{
	if (EXPECTED(Z_TYPE_P(zvalue) == IS_ARRAY)) {
		ZVAL_ARR(zvalue, gear_array_dup(Z_ARRVAL_P(zvalue)));
	} else if (EXPECTED(Z_TYPE_P(zvalue) == IS_STRING)) {
		GEAR_ASSERT(!ZSTR_IS_INTERNED(Z_STR_P(zvalue)));
		CHECK_ZVAL_STRING(Z_STR_P(zvalue));
		ZVAL_NEW_STR(zvalue, gear_string_dup(Z_STR_P(zvalue), 0));
	}
}

