/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_GENERATORS_H
#define GEAR_GENERATORS_H

BEGIN_EXTERN_C()

extern GEAR_API gear_class_entry *gear_ce_generator;
extern GEAR_API gear_class_entry *gear_ce_ClosedGeneratorException;

typedef struct _gear_generator_node gear_generator_node;
typedef struct _gear_generator gear_generator;

/* The concept of `yield from` exposes problems when accessed at different levels of the chain of delegated generators. We need to be able to reference the currently executed Generator in all cases and still being able to access the return values of finished Generators.
 * The solution to this problem is a doubly-linked tree, which all Generators referenced in maintain a reference to. It should be impossible to avoid walking the tree in all cases. This way, we only need tree walks from leaf to root in case where some part of the `yield from` chain is passed to another `yield from`. (Update of leaf node pointer and list of multi-children nodes needed when leaf gets a child in direct path from leaf to root node.) But only in that case, which should be a fairly rare case (which is then possible, but not totally cheap).
 * The root of the tree is then the currently executed Generator. The subnodes of the tree (all except the root node) are all Generators which do `yield from`. Each node of the tree knows a pointer to one leaf descendant node. Each node with multiple children needs a list of all leaf descendant nodes paired with pointers to their respective child node. (The stack is determined by leaf node pointers) Nodes with only one child just don't need a list, there it is enough to just have a pointer to the child node. Further, leaf nodes store a pointer to the root node.
 * That way, when we advance any generator, we just need to look up a leaf node (which all have a reference to a root node). Then we can see at the root node whether current Generator is finished. If it isn't, all is fine and we can just continue. If the Generator finished, there will be two cases. Either it is a simple node with just one child, then go down to child node. Or it has multiple children and we now will remove the current leaf node from the list of nodes (unnecessary, is microoptimization) and go down to the child node whose reference was paired with current leaf node. Child node is then removed its parent reference and becomes new top node. Or the current node references the Generator we're currently executing, then we can continue from the YIELD_FROM opcode. When a node referenced as root node in a leaf node has a parent, then we go the way up until we find a root node without parent.
 * In case we go into a new `yield from` level, a node is created on top of current root and becomes the new root. Leaf node needs to be updated with new root node then.
 * When a Generator referenced by a node of the tree is added to `yield from`, that node now gets a list of children (we need to walk the descendants of that node and nodes of the tree of the other Generator down to the first multi-children node and copy all the leaf node pointers from there). In case there was no multi-children node (linear tree), we just add a pair (pointer to leaf node, pointer to child node), with the child node being in a direct path from leaf to this node.
 */

struct _gear_generator_node {
	gear_generator *parent; /* NULL for root */
	uint32_t children;
	union {
		HashTable *ht; /* if multiple children */
		struct { /* if one child */
			gear_generator *leaf;
			gear_generator *child;
		} single;
	} child;
	union {
		gear_generator *leaf; /* if > 0 children */
		gear_generator *root; /* if 0 children */
	} ptr;
};

struct _gear_generator {
	gear_object std;

	gear_object_iterator *iterator;

	/* The suspended execution context. */
	gear_execute_data *execute_data;

	/* Frozen call stack for "yield" used in context of other calls */
	gear_execute_data *frozen_call_stack;

	/* Current value */
	zval value;
	/* Current key */
	zval key;
	/* Return value */
	zval retval;
	/* Variable to put sent value into */
	zval *send_target;
	/* Largest used integer key for auto-incrementing keys */
	gear_long largest_used_integer_key;

	/* Values specified by "yield from" to yield from this generator.
	 * This is only used for arrays or non-generator Traversables.
	 * This zval also uses the u2 structure in the same way as
	 * by-value foreach. */
	zval values;

	/* Node of waiting generators when multiple "yield from" expressions
	 * are nested. */
	gear_generator_node node;

	/* Fake execute_data for stacktraces */
	gear_execute_data execute_fake;

	/* GEAR_GENERATOR_* flags */
	gear_uchar flags;

	zval *gc_buffer;
	uint32_t gc_buffer_size;
};

static const gear_uchar GEAR_GENERATOR_CURRENTLY_RUNNING = 0x1;
static const gear_uchar GEAR_GENERATOR_FORCED_CLOSE      = 0x2;
static const gear_uchar GEAR_GENERATOR_AT_FIRST_YIELD    = 0x4;
static const gear_uchar GEAR_GENERATOR_DO_INIT           = 0x8;

void gear_register_generator_ce(void);
GEAR_API void gear_generator_close(gear_generator *generator, gear_bool finished_execution);
GEAR_API void gear_generator_resume(gear_generator *generator);

GEAR_API void gear_generator_restore_call_stack(gear_generator *generator);
GEAR_API gear_execute_data* gear_generator_freeze_call_stack(gear_execute_data *execute_data);

void gear_generator_yield_from(gear_generator *generator, gear_generator *from);
GEAR_API gear_execute_data *gear_generator_check_placeholder_frame(gear_execute_data *ptr);

GEAR_API gear_generator *gear_generator_update_current(gear_generator *generator, gear_generator *leaf);
static gear_always_inline gear_generator *gear_generator_get_current(gear_generator *generator)
{
	gear_generator *leaf;
	gear_generator *root;

	if (EXPECTED(generator->node.parent == NULL)) {
		/* we're not in yield from mode */
		return generator;
	}

	leaf = generator->node.children ? generator->node.ptr.leaf : generator;
	root = leaf->node.ptr.root;

	if (EXPECTED(root->execute_data && root->node.parent == NULL)) {
		/* generator still running */
		return root;
	}

	return gear_generator_update_current(generator, leaf);
}

END_EXTERN_C()

#endif

