/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_SMART_STR_H
#define GEAR_SMART_STR_H

#include <gear.h>
#include "gear_globals.h"
#include "gear_smart_str_public.h"

#define smart_str_appends_ex(dest, src, what) \
	smart_str_appendl_ex((dest), (src), strlen(src), (what))
#define smart_str_appends(dest, src) \
	smart_str_appendl((dest), (src), strlen(src))
#define smart_str_extend(dest, len) \
	smart_str_extend_ex((dest), (len), 0)
#define smart_str_appendc(dest, c) \
	smart_str_appendc_ex((dest), (c), 0)
#define smart_str_appendl(dest, src, len) \
	smart_str_appendl_ex((dest), (src), (len), 0)
#define smart_str_append(dest, src) \
	smart_str_append_ex((dest), (src), 0)
#define smart_str_append_smart_str(dest, src) \
	smart_str_append_smart_str_ex((dest), (src), 0)
#define smart_str_sets(dest, src) \
	smart_str_setl((dest), (src), strlen(src));
#define smart_str_append_long(dest, val) \
	smart_str_append_long_ex((dest), (val), 0)
#define smart_str_append_unsigned(dest, val) \
	smart_str_append_unsigned_ex((dest), (val), 0)
#define smart_str_free(dest) \
	smart_str_free_ex((dest), 0)

BEGIN_EXTERN_C()

GEAR_API void GEAR_FASTCALL smart_str_erealloc(smart_str *str, size_t len);
GEAR_API void GEAR_FASTCALL smart_str_realloc(smart_str *str, size_t len);
GEAR_API void GEAR_FASTCALL smart_str_append_escaped(smart_str *str, const char *s, size_t l);
GEAR_API void smart_str_append_printf(smart_str *dest, const char *format, ...)
	GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);

END_EXTERN_C()

static gear_always_inline size_t smart_str_alloc(smart_str *str, size_t len, gear_bool persistent) {
	if (UNEXPECTED(!str->s)) {
		goto do_smart_str_realloc;
	} else {
		len += ZSTR_LEN(str->s);
		if (UNEXPECTED(len >= str->a)) {
do_smart_str_realloc:
			if (persistent) {
				smart_str_realloc(str, len);
			} else {
				smart_str_erealloc(str, len);
			}
		}
	}
	return len;
}

static gear_always_inline char* smart_str_extend_ex(smart_str *dest, size_t len, gear_bool persistent) {
	size_t new_len = smart_str_alloc(dest, len, persistent);
	char *ret = ZSTR_VAL(dest->s) + ZSTR_LEN(dest->s);
	ZSTR_LEN(dest->s) = new_len;
	return ret;
}

static gear_always_inline void smart_str_free_ex(smart_str *str, gear_bool persistent) {
	if (str->s) {
		gear_string_release_ex(str->s, persistent);
		str->s = NULL;
	}
	str->a = 0;
}

static gear_always_inline void smart_str_0(smart_str *str) {
	if (str->s) {
		ZSTR_VAL(str->s)[ZSTR_LEN(str->s)] = '\0';
	}
}

static gear_always_inline size_t smart_str_get_len(smart_str *str) {
	return str->s ? ZSTR_LEN(str->s) : 0;
}

static gear_always_inline gear_string *smart_str_extract(smart_str *str) {
	if (str->s) {
		gear_string *res;
		smart_str_0(str);
		res = str->s;
		str->s = NULL;
		return res;
	} else {
		return ZSTR_EMPTY_ALLOC();
	}
}

static gear_always_inline void smart_str_appendc_ex(smart_str *dest, char ch, gear_bool persistent) {
	size_t new_len = smart_str_alloc(dest, 1, persistent);
	ZSTR_VAL(dest->s)[new_len - 1] = ch;
	ZSTR_LEN(dest->s) = new_len;
}

static gear_always_inline void smart_str_appendl_ex(smart_str *dest, const char *str, size_t len, gear_bool persistent) {
	size_t new_len = smart_str_alloc(dest, len, persistent);
	memcpy(ZSTR_VAL(dest->s) + ZSTR_LEN(dest->s), str, len);
	ZSTR_LEN(dest->s) = new_len;
}

static gear_always_inline void smart_str_append_ex(smart_str *dest, const gear_string *src, gear_bool persistent) {
	smart_str_appendl_ex(dest, ZSTR_VAL(src), ZSTR_LEN(src), persistent);
}

static gear_always_inline void smart_str_append_smart_str_ex(smart_str *dest, const smart_str *src, gear_bool persistent) {
	if (src->s && ZSTR_LEN(src->s)) {
		smart_str_append_ex(dest, src->s, persistent);
	}
}

static gear_always_inline void smart_str_append_long_ex(smart_str *dest, gear_long num, gear_bool persistent) {
	char buf[32];
	char *result = gear_print_long_to_buf(buf + sizeof(buf) - 1, num);
	smart_str_appendl_ex(dest, result, buf + sizeof(buf) - 1 - result, persistent);
}

static gear_always_inline void smart_str_append_unsigned_ex(smart_str *dest, gear_ulong num, gear_bool persistent) {
	char buf[32];
	char *result = gear_print_ulong_to_buf(buf + sizeof(buf) - 1, num);
	smart_str_appendl_ex(dest, result, buf + sizeof(buf) - 1 - result, persistent);
}

static gear_always_inline void smart_str_setl(smart_str *dest, const char *src, size_t len) {
	smart_str_free(dest);
	smart_str_appendl(dest, src, len);
}

#endif


