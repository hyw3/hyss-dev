/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_globals.h"
#include "gear_variables.h"
#include "gear_API.h"
#include "gear_objects_API.h"

GEAR_API void GEAR_FASTCALL gear_objects_store_init(gear_objects_store *objects, uint32_t init_size)
{
	objects->object_buckets = (gear_object **) emalloc(init_size * sizeof(gear_object*));
	objects->top = 1; /* Skip 0 so that handles are true */
	objects->size = init_size;
	objects->free_list_head = -1;
	memset(&objects->object_buckets[0], 0, sizeof(gear_object*));
}

GEAR_API void GEAR_FASTCALL gear_objects_store_destroy(gear_objects_store *objects)
{
	efree(objects->object_buckets);
	objects->object_buckets = NULL;
}

GEAR_API void GEAR_FASTCALL gear_objects_store_call_destructors(gear_objects_store *objects)
{
	EG(flags) |= EG_FLAGS_OBJECT_STORE_NO_REUSE;
	if (objects->top > 1) {
		uint32_t i;
		for (i = 1; i < objects->top; i++) {
			gear_object *obj = objects->object_buckets[i];
			if (IS_OBJ_VALID(obj)) {
				if (!(OBJ_FLAGS(obj) & IS_OBJ_DESTRUCTOR_CALLED)) {
					GC_ADD_FLAGS(obj, IS_OBJ_DESTRUCTOR_CALLED);

					if (obj->handlers->dtor_obj
					 && (obj->handlers->dtor_obj != gear_objects_destroy_object
					  || obj->ce->destructor)) {
						GC_ADDREF(obj);
						obj->handlers->dtor_obj(obj);
						GC_DELREF(obj);
					}
				}
			}
		}
	}
}

GEAR_API void GEAR_FASTCALL gear_objects_store_mark_destructed(gear_objects_store *objects)
{
	if (objects->object_buckets && objects->top > 1) {
		gear_object **obj_ptr = objects->object_buckets + 1;
		gear_object **end = objects->object_buckets + objects->top;

		do {
			gear_object *obj = *obj_ptr;

			if (IS_OBJ_VALID(obj)) {
				GC_ADD_FLAGS(obj, IS_OBJ_DESTRUCTOR_CALLED);
			}
			obj_ptr++;
		} while (obj_ptr != end);
	}
}

GEAR_API void GEAR_FASTCALL gear_objects_store_free_object_storage(gear_objects_store *objects, gear_bool fast_shutdown)
{
	gear_object **obj_ptr, **end, *obj;

	if (objects->top <= 1) {
		return;
	}

	/* Free object contents, but don't free objects themselves, so they show up as leaks */
	end = objects->object_buckets + 1;
	obj_ptr = objects->object_buckets + objects->top;

	if (fast_shutdown) {
		do {
			obj_ptr--;
			obj = *obj_ptr;
			if (IS_OBJ_VALID(obj)) {
				if (!(OBJ_FLAGS(obj) & IS_OBJ_FREE_CALLED)) {
					GC_ADD_FLAGS(obj, IS_OBJ_FREE_CALLED);
					if (obj->handlers->free_obj && obj->handlers->free_obj != gear_object_std_dtor) {
						GC_ADDREF(obj);
						obj->handlers->free_obj(obj);
						GC_DELREF(obj);
					}
				}
			}
		} while (obj_ptr != end);
	} else {
		do {
			obj_ptr--;
			obj = *obj_ptr;
			if (IS_OBJ_VALID(obj)) {
				if (!(OBJ_FLAGS(obj) & IS_OBJ_FREE_CALLED)) {
					GC_ADD_FLAGS(obj, IS_OBJ_FREE_CALLED);
					if (obj->handlers->free_obj) {
						GC_ADDREF(obj);
						obj->handlers->free_obj(obj);
						GC_DELREF(obj);
					}
				}
			}
		} while (obj_ptr != end);
	}
}


/* Store objects API */

GEAR_API void GEAR_FASTCALL gear_objects_store_put(gear_object *object)
{
	int handle;

	/* When in shutdown sequence - do not reuse previously freed handles, to make sure
	 * the dtors for newly created objects are called in gear_objects_store_call_destructors() loop
	 */
	if (EG(objects_store).free_list_head != -1 && EXPECTED(!(EG(flags) & EG_FLAGS_OBJECT_STORE_NO_REUSE))) {
		handle = EG(objects_store).free_list_head;
		EG(objects_store).free_list_head = GET_OBJ_BUCKET_NUMBER(EG(objects_store).object_buckets[handle]);
	} else {
		if (EG(objects_store).top == EG(objects_store).size) {
			uint32_t new_size = 2 * EG(objects_store).size;
			EG(objects_store).object_buckets = (gear_object **) erealloc(EG(objects_store).object_buckets, new_size * sizeof(gear_object*));
			/* Assign size after realloc, in case it fails */
			EG(objects_store).size = new_size;
		}
		handle = EG(objects_store).top++;
	}
	object->handle = handle;
	EG(objects_store).object_buckets[handle] = object;
}

GEAR_API void GEAR_FASTCALL gear_objects_store_del(gear_object *object) /* {{{ */
{
	GEAR_ASSERT(GC_REFCOUNT(object) == 0);

	/* GC might have released this object already. */
	if (UNEXPECTED(GC_TYPE(object) == IS_NULL)) {
		return;
	}

	/*	Make sure we hold a reference count during the destructor call
		otherwise, when the destructor ends the storage might be freed
		when the refcount reaches 0 a second time
	 */
	if (!(OBJ_FLAGS(object) & IS_OBJ_DESTRUCTOR_CALLED)) {
		GC_ADD_FLAGS(object, IS_OBJ_DESTRUCTOR_CALLED);

		if (object->handlers->dtor_obj
		 && (object->handlers->dtor_obj != gear_objects_destroy_object
		  || object->ce->destructor)) {
			GC_ADDREF(object);
			object->handlers->dtor_obj(object);
			GC_DELREF(object);
		}
	}

	if (GC_REFCOUNT(object) == 0) {
		uint32_t handle = object->handle;
		void *ptr;

		GEAR_ASSERT(EG(objects_store).object_buckets != NULL);
		GEAR_ASSERT(IS_OBJ_VALID(EG(objects_store).object_buckets[object->handle]));
		EG(objects_store).object_buckets[handle] = SET_OBJ_INVALID(object);
		if (!(OBJ_FLAGS(object) & IS_OBJ_FREE_CALLED)) {
			GC_ADD_FLAGS(object, IS_OBJ_FREE_CALLED);
			if (object->handlers->free_obj) {
				GC_ADDREF(object);
				object->handlers->free_obj(object);
				GC_DELREF(object);
			}
		}
		ptr = ((char*)object) - object->handlers->offset;
		GC_REMOVE_FROM_BUFFER(object);
		efree(ptr);
		GEAR_OBJECTS_STORE_ADD_TO_FREE_LIST(handle);
	}
}
/* }}} */


