/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <signal.h>

#include "gear.h"
#include "gear_compile.h"
#include "gear_execute.h"
#include "gear_API.h"
#include "gear_stack.h"
#include "gear_constants.h"
#include "gear_extensions.h"
#include "gear_exceptions.h"
#include "gear_closures.h"
#include "gear_generators.h"
#include "gear_vm.h"
#include "gear_float.h"
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

GEAR_API void (*gear_execute_ex)(gear_execute_data *execute_data);
GEAR_API void (*gear_execute_internal)(gear_execute_data *execute_data, zval *return_value);

/* true globals */
GEAR_API const gear_fcall_info empty_fcall_info = { 0, {{0}, {{0}}, {0}}, NULL, NULL, NULL, 0, 0 };
GEAR_API const gear_fcall_info_cache empty_fcall_info_cache = { NULL, NULL, NULL, NULL };

#ifdef GEAR_WIN32
GEAR_TLS HANDLE tq_timer = NULL;
#endif

#if 0&&GEAR_DEBUG
static void (*original_sigsegv_handler)(int);
static void gear_handle_sigsegv(int dummy) /* {{{ */
{
	fflush(stdout);
	fflush(stderr);
	if (original_sigsegv_handler == gear_handle_sigsegv) {
		signal(SIGSEGV, original_sigsegv_handler);
	} else {
		signal(SIGSEGV, SIG_DFL);
	}
	{

		fprintf(stderr, "SIGSEGV caught on opcode %d on opline %d of %s() at %s:%d\n\n",
				active_opline->opcode,
				active_opline-EG(active_op_array)->opcodes,
				get_active_function_name(),
				gear_get_executed_filename(),
				gear_get_executed_lineno());
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
		fflush(stderr);
#endif
	}
	if (original_sigsegv_handler!=gear_handle_sigsegv) {
		original_sigsegv_handler(dummy);
	}
}
/* }}} */
#endif

static void gear_extension_activator(gear_extension *extension) /* {{{ */
{
	if (extension->activate) {
		extension->activate();
	}
}
/* }}} */

static void gear_extension_deactivator(gear_extension *extension) /* {{{ */
{
	if (extension->deactivate) {
		extension->deactivate();
	}
}
/* }}} */

static int clean_non_persistent_constant_full(zval *zv) /* {{{ */
{
	gear_constant *c = Z_PTR_P(zv);
	return (GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT) ? GEAR_HASH_APPLY_KEEP : GEAR_HASH_APPLY_REMOVE;
}
/* }}} */

static int clean_non_persistent_function_full(zval *zv) /* {{{ */
{
	gear_function *function = Z_PTR_P(zv);
	return (function->type == GEAR_INTERNAL_FUNCTION) ? GEAR_HASH_APPLY_KEEP : GEAR_HASH_APPLY_REMOVE;
}
/* }}} */

static int clean_non_persistent_class_full(zval *zv) /* {{{ */
{
	gear_class_entry *ce = Z_PTR_P(zv);
	return (ce->type == GEAR_INTERNAL_CLASS) ? GEAR_HASH_APPLY_KEEP : GEAR_HASH_APPLY_REMOVE;
}
/* }}} */

void init_executor(void) /* {{{ */
{
	gear_init_fpu();

	ZVAL_NULL(&EG(uninitialized_zval));
	ZVAL_ERROR(&EG(error_zval));
/* destroys stack frame, therefore makes core dumps worthless */
#if 0&&GEAR_DEBUG
	original_sigsegv_handler = signal(SIGSEGV, gear_handle_sigsegv);
#endif

	EG(symtable_cache_ptr) = EG(symtable_cache) - 1;
	EG(symtable_cache_limit) = EG(symtable_cache) + SYMTABLE_CACHE_SIZE - 1;
	EG(no_extensions) = 0;

	EG(function_table) = CG(function_table);
	EG(class_table) = CG(class_table);

	EG(in_autoload) = NULL;
	EG(autoload_func) = NULL;
	EG(error_handling) = EH_NORMAL;
	EG(flags) = EG_FLAGS_INITIAL;

	gear_vm_stack_init();

	gear_hash_init(&EG(symbol_table), 64, NULL, ZVAL_PTR_DTOR, 0);

	gear_llist_apply(&gear_extensions, (llist_apply_func_t) gear_extension_activator);

	gear_hash_init(&EG(included_files), 8, NULL, NULL, 0);

	EG(ticks_count) = 0;

	ZVAL_UNDEF(&EG(user_error_handler));
	ZVAL_UNDEF(&EG(user_exception_handler));

	EG(current_execute_data) = NULL;

	gear_stack_init(&EG(user_error_handlers_error_reporting), sizeof(int));
	gear_stack_init(&EG(user_error_handlers), sizeof(zval));
	gear_stack_init(&EG(user_exception_handlers), sizeof(zval));

	gear_objects_store_init(&EG(objects_store), 1024);

	EG(full_tables_cleanup) = 0;
	EG(vm_interrupt) = 0;
	EG(timed_out) = 0;

	EG(exception) = NULL;
	EG(prev_exception) = NULL;

	EG(fake_scope) = NULL;

	EG(ht_iterators_count) = sizeof(EG(ht_iterators_slots)) / sizeof(HashTableIterator);
	EG(ht_iterators_used) = 0;
	EG(ht_iterators) = EG(ht_iterators_slots);
	memset(EG(ht_iterators), 0, sizeof(EG(ht_iterators_slots)));

	EG(each_deprecation_thrown) = 0;

	EG(persistent_constants_count) = EG(gear_constants)->nNumUsed;
	EG(persistent_functions_count) = EG(function_table)->nNumUsed;
	EG(persistent_classes_count)   = EG(class_table)->nNumUsed;

	EG(active) = 1;
}
/* }}} */

static int zval_call_destructor(zval *zv) /* {{{ */
{
	if (Z_TYPE_P(zv) == IS_INDIRECT) {
		zv = Z_INDIRECT_P(zv);
	}
	if (Z_TYPE_P(zv) == IS_OBJECT && Z_REFCOUNT_P(zv) == 1) {
		return GEAR_HASH_APPLY_REMOVE;
	} else {
		return GEAR_HASH_APPLY_KEEP;
	}
}
/* }}} */

static void gear_unclean_zval_ptr_dtor(zval *zv) /* {{{ */
{
	if (Z_TYPE_P(zv) == IS_INDIRECT) {
		zv = Z_INDIRECT_P(zv);
	}
	i_zval_ptr_dtor(zv GEAR_FILE_LINE_CC);
}
/* }}} */

static void gear_throw_or_error(int fetch_type, gear_class_entry *exception_ce, const char *format, ...) /* {{{ */
{
	va_list va;
	char *message = NULL;

	va_start(va, format);
	gear_vspprintf(&message, 0, format, va);

	if (fetch_type & GEAR_FETCH_CLASS_EXCEPTION) {
		gear_throw_error(exception_ce, "%s", message);
	} else {
		gear_error(E_ERROR, "%s", message);
	}

	efree(message);
	va_end(va);
}
/* }}} */

void shutdown_destructors(void) /* {{{ */
{
	if (CG(unclean_shutdown)) {
		EG(symbol_table).pDestructor = gear_unclean_zval_ptr_dtor;
	}
	gear_try {
		uint32_t symbols;
		do {
			symbols = gear_hash_num_elements(&EG(symbol_table));
			gear_hash_reverse_apply(&EG(symbol_table), (apply_func_t) zval_call_destructor);
		} while (symbols != gear_hash_num_elements(&EG(symbol_table)));
		gear_objects_store_call_destructors(&EG(objects_store));
	} gear_catch {
		/* if we couldn't destruct cleanly, mark all objects as destructed anyway */
		gear_objects_store_mark_destructed(&EG(objects_store));
	} gear_end_try();
}
/* }}} */

void shutdown_executor(void) /* {{{ */
{
	gear_string *key;
	zval *zv;
#if GEAR_DEBUG
	gear_bool fast_shutdown = 0;
#else
	gear_bool fast_shutdown = is_gear_mm() && !EG(full_tables_cleanup);
#endif

	gear_try {
		gear_llist_destroy(&CG(open_files));
	} gear_end_try();

	gear_try {
		gear_close_rsrc_list(&EG(regular_list));
	} gear_end_try();

	gear_objects_store_free_object_storage(&EG(objects_store), fast_shutdown);

	/* All resources and objects are destroyed. */
	/* No HYSS callback functions may be called after this point. */
	EG(active) = 0;

	gear_try {
		gear_llist_apply(&gear_extensions, (llist_apply_func_t) gear_extension_deactivator);
	} gear_end_try();

	if (fast_shutdown) {
		/* Fast Request Shutdown
		 * =====================
		 * Gear Memory Manager frees memory by its own. We don't have to free
		 * each allocated block separately.
		 */
		gear_hash_discard(EG(gear_constants), EG(persistent_constants_count));
		gear_hash_discard(EG(function_table), EG(persistent_functions_count));
		gear_hash_discard(EG(class_table), EG(persistent_classes_count));
		gear_cleanup_internal_classes();
	} else {
		gear_hash_graceful_reverse_destroy(&EG(symbol_table));

#if GEAR_DEBUG
		if (gc_enabled() && !CG(unclean_shutdown)) {
			gc_collect_cycles();
		}
#endif

		/* remove error handlers before destroying classes and functions,
		 * so that if handler used some class, crash would not happen */
		if (Z_TYPE(EG(user_error_handler)) != IS_UNDEF) {
			zval_ptr_dtor(&EG(user_error_handler));
			ZVAL_UNDEF(&EG(user_error_handler));
		}

		if (Z_TYPE(EG(user_exception_handler)) != IS_UNDEF) {
			zval_ptr_dtor(&EG(user_exception_handler));
			ZVAL_UNDEF(&EG(user_exception_handler));
		}

		gear_stack_clean(&EG(user_error_handlers_error_reporting), NULL, 1);
		gear_stack_clean(&EG(user_error_handlers), (void (*)(void *))ZVAL_PTR_DTOR, 1);
		gear_stack_clean(&EG(user_exception_handlers), (void (*)(void *))ZVAL_PTR_DTOR, 1);

		gear_vm_stack_destroy();

		if (EG(full_tables_cleanup)) {
			gear_hash_reverse_apply(EG(gear_constants), clean_non_persistent_constant_full);
			gear_hash_reverse_apply(EG(function_table), clean_non_persistent_function_full);
			gear_hash_reverse_apply(EG(class_table), clean_non_persistent_class_full);
		} else {
			GEAR_HASH_REVERSE_FOREACH_STR_KEY_VAL(EG(gear_constants), key, zv) {
				gear_constant *c = Z_PTR_P(zv);
				if (GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT) {
					break;
				}
				zval_ptr_dtor_nogc(&c->value);
				if (c->name) {
					gear_string_release_ex(c->name, 0);
				}
				efree(c);
				gear_string_release_ex(key, 0);
			} GEAR_HASH_FOREACH_END_DEL();
			GEAR_HASH_REVERSE_FOREACH_STR_KEY_VAL(EG(function_table), key, zv) {
				gear_function *func = Z_PTR_P(zv);
				if (func->type == GEAR_INTERNAL_FUNCTION) {
					break;
				}
				destroy_op_array(&func->op_array);
				gear_string_release_ex(key, 0);
			} GEAR_HASH_FOREACH_END_DEL();
			GEAR_HASH_REVERSE_FOREACH_STR_KEY_VAL(EG(class_table), key, zv) {
				gear_class_entry *ce = Z_PTR_P(zv);
				if (ce->type == GEAR_INTERNAL_CLASS) {
					break;
				}
				destroy_gear_class(zv);
				gear_string_release_ex(key, 0);
			} GEAR_HASH_FOREACH_END_DEL();
		}

		gear_cleanup_internal_classes();

		while (EG(symtable_cache_ptr)>=EG(symtable_cache)) {
			gear_hash_destroy(*EG(symtable_cache_ptr));
			FREE_HASHTABLE(*EG(symtable_cache_ptr));
			EG(symtable_cache_ptr)--;
		}

		gear_hash_destroy(&EG(included_files));

		gear_stack_destroy(&EG(user_error_handlers_error_reporting));
		gear_stack_destroy(&EG(user_error_handlers));
		gear_stack_destroy(&EG(user_exception_handlers));
		gear_objects_store_destroy(&EG(objects_store));
		if (EG(in_autoload)) {
			gear_hash_destroy(EG(in_autoload));
			FREE_HASHTABLE(EG(in_autoload));
		}

		if (EG(ht_iterators) != EG(ht_iterators_slots)) {
			efree(EG(ht_iterators));
		}
	}

#if GEAR_DEBUG
	if (EG(ht_iterators_used) && !CG(unclean_shutdown)) {
		gear_error(E_WARNING, "Leaked %" PRIu32 " hashtable iterators", EG(ht_iterators_used));
	}
#endif

	EG(ht_iterators_used) = 0;

	gear_shutdown_fpu();
}
/* }}} */

/* return class name and "::" or "". */
GEAR_API const char *get_active_class_name(const char **space) /* {{{ */
{
	gear_function *func;

	if (!gear_is_executing()) {
		if (space) {
			*space = "";
		}
		return "";
	}

	func = EG(current_execute_data)->func;
	switch (func->type) {
		case GEAR_USER_FUNCTION:
		case GEAR_INTERNAL_FUNCTION:
		{
			gear_class_entry *ce = func->common.scope;

			if (space) {
				*space = ce ? "::" : "";
			}
			return ce ? ZSTR_VAL(ce->name) : "";
		}
		default:
			if (space) {
				*space = "";
			}
			return "";
	}
}
/* }}} */

GEAR_API const char *get_active_function_name(void) /* {{{ */
{
	gear_function *func;

	if (!gear_is_executing()) {
		return NULL;
	}
	func = EG(current_execute_data)->func;
	switch (func->type) {
		case GEAR_USER_FUNCTION: {
				gear_string *function_name = func->common.function_name;

				if (function_name) {
					return ZSTR_VAL(function_name);
				} else {
					return "main";
				}
			}
			break;
		case GEAR_INTERNAL_FUNCTION:
			return ZSTR_VAL(func->common.function_name);
			break;
		default:
			return NULL;
	}
}
/* }}} */

GEAR_API const char *gear_get_executed_filename(void) /* {{{ */
{
	gear_execute_data *ex = EG(current_execute_data);

	while (ex && (!ex->func || !GEAR_USER_CODE(ex->func->type))) {
		ex = ex->prev_execute_data;
	}
	if (ex) {
		return ZSTR_VAL(ex->func->op_array.filename);
	} else {
		return "[no active file]";
	}
}
/* }}} */

GEAR_API gear_string *gear_get_executed_filename_ex(void) /* {{{ */
{
	gear_execute_data *ex = EG(current_execute_data);

	while (ex && (!ex->func || !GEAR_USER_CODE(ex->func->type))) {
		ex = ex->prev_execute_data;
	}
	if (ex) {
		return ex->func->op_array.filename;
	} else {
		return NULL;
	}
}
/* }}} */

GEAR_API uint32_t gear_get_executed_lineno(void) /* {{{ */
{
	gear_execute_data *ex = EG(current_execute_data);

	while (ex && (!ex->func || !GEAR_USER_CODE(ex->func->type))) {
		ex = ex->prev_execute_data;
	}
	if (ex) {
		if (EG(exception) && ex->opline->opcode == GEAR_HANDLE_EXCEPTION &&
		    ex->opline->lineno == 0 && EG(opline_before_exception)) {
			return EG(opline_before_exception)->lineno;
		}
		return ex->opline->lineno;
	} else {
		return 0;
	}
}
/* }}} */

GEAR_API gear_class_entry *gear_get_executed_scope(void) /* {{{ */
{
	gear_execute_data *ex = EG(current_execute_data);

	while (1) {
		if (!ex) {
			return NULL;
		} else if (ex->func && (GEAR_USER_CODE(ex->func->type) || ex->func->common.scope)) {
			return ex->func->common.scope;
		}
		ex = ex->prev_execute_data;
	}
}
/* }}} */

GEAR_API gear_bool gear_is_executing(void) /* {{{ */
{
	return EG(current_execute_data) != 0;
}
/* }}} */

GEAR_API int gear_use_undefined_constant(gear_string *name, gear_ast_attr attr, zval *result) /* {{{ */
{
	char *colon;

	if (UNEXPECTED(EG(exception))) {
		return FAILURE;
	} else if ((colon = (char*)gear_memrchr(ZSTR_VAL(name), ':', ZSTR_LEN(name)))) {
		gear_throw_error(NULL, "Undefined class constant '%s'", ZSTR_VAL(name));
		return FAILURE;
	} else if ((attr & IS_CONSTANT_UNQUALIFIED) == 0) {
		gear_throw_error(NULL, "Undefined constant '%s'", ZSTR_VAL(name));
		return FAILURE;
	} else {
		char *actual = ZSTR_VAL(name);
		size_t actual_len = ZSTR_LEN(name);
		char *slash = (char *) gear_memrchr(actual, '\\', actual_len);

		if (slash) {
			actual = slash + 1;
			actual_len -= (actual - ZSTR_VAL(name));
		}

		gear_error(E_WARNING, "Use of undefined constant %s - assumed '%s' (this will throw an Error in a future version of HYSS)", actual, actual);
		if (EG(exception)) {
			return FAILURE;
		} else {
			gear_string *result_str = gear_string_init(actual, actual_len, 0);
			zval_ptr_dtor_nogc(result);
			ZVAL_NEW_STR(result, result_str);
		}
	}
	return SUCCESS;
}
/* }}} */

GEAR_API int zval_update_constant_ex(zval *p, gear_class_entry *scope) /* {{{ */
{
	if (Z_TYPE_P(p) == IS_CONSTANT_AST) {
		gear_ast *ast = Z_ASTVAL_P(p);

		if (ast->kind == GEAR_AST_CONSTANT) {
			gear_string *name = gear_ast_get_constant_name(ast);
			zval *zv = gear_get_constant_ex(name, scope, ast->attr);

			if (UNEXPECTED(zv == NULL)) {
				return gear_use_undefined_constant(name, ast->attr, p);
			}
			zval_ptr_dtor_nogc(p);
			ZVAL_COPY_OR_DUP(p, zv);
		} else {
			zval tmp;

			if (UNEXPECTED(gear_ast_evaluate(&tmp, ast, scope) != SUCCESS)) {
				return FAILURE;
			}
			zval_ptr_dtor_nogc(p);
			ZVAL_COPY_VALUE(p, &tmp);
		}
	}
	return SUCCESS;
}
/* }}} */

GEAR_API int zval_update_constant(zval *pp) /* {{{ */
{
	return zval_update_constant_ex(pp, EG(current_execute_data) ? gear_get_executed_scope() : CG(active_class_entry));
}
/* }}} */

int _call_user_function_ex(zval *object, zval *function_name, zval *retval_ptr, uint32_t param_count, zval params[], int no_separation) /* {{{ */
{
	gear_fcall_info fci;

	fci.size = sizeof(fci);
	fci.object = object ? Z_OBJ_P(object) : NULL;
	ZVAL_COPY_VALUE(&fci.function_name, function_name);
	fci.retval = retval_ptr;
	fci.param_count = param_count;
	fci.params = params;
	fci.no_separation = (gear_bool) no_separation;

	return gear_call_function(&fci, NULL);
}
/* }}} */

int gear_call_function(gear_fcall_info *fci, gear_fcall_info_cache *fci_cache) /* {{{ */
{
	uint32_t i;
	gear_execute_data *call, dummy_execute_data;
	gear_fcall_info_cache fci_cache_local;
	gear_function *func;

	ZVAL_UNDEF(fci->retval);

	if (!EG(active)) {
		return FAILURE; /* executor is already inactive */
	}

	if (EG(exception)) {
		return FAILURE; /* we would result in an instable executor otherwise */
	}

	GEAR_ASSERT(fci->size == sizeof(gear_fcall_info));

	/* Initialize execute_data */
	if (!EG(current_execute_data)) {
		/* This only happens when we're called outside any execute()'s
		 * It shouldn't be strictly necessary to NULL execute_data out,
		 * but it may make bugs easier to spot
		 */
		memset(&dummy_execute_data, 0, sizeof(gear_execute_data));
		EG(current_execute_data) = &dummy_execute_data;
	} else if (EG(current_execute_data)->func &&
	           GEAR_USER_CODE(EG(current_execute_data)->func->common.type) &&
	           EG(current_execute_data)->opline->opcode != GEAR_DO_FCALL &&
	           EG(current_execute_data)->opline->opcode != GEAR_DO_ICALL &&
	           EG(current_execute_data)->opline->opcode != GEAR_DO_UCALL &&
	           EG(current_execute_data)->opline->opcode != GEAR_DO_FCALL_BY_NAME) {
		/* Insert fake frame in case of include or magic calls */
		dummy_execute_data = *EG(current_execute_data);
		dummy_execute_data.prev_execute_data = EG(current_execute_data);
		dummy_execute_data.call = NULL;
		dummy_execute_data.opline = NULL;
		dummy_execute_data.func = NULL;
		EG(current_execute_data) = &dummy_execute_data;
	}

	if (!fci_cache || !fci_cache->function_handler) {
		char *error = NULL;

		if (!fci_cache) {
			fci_cache = &fci_cache_local;
		}

		if (!gear_is_callable_ex(&fci->function_name, fci->object, IS_CALLABLE_CHECK_SILENT, NULL, fci_cache, &error)) {
			if (error) {
				gear_string *callable_name
					= gear_get_callable_name_ex(&fci->function_name, fci->object);
				gear_error(E_WARNING, "Invalid callback %s, %s", ZSTR_VAL(callable_name), error);
				efree(error);
				gear_string_release_ex(callable_name, 0);
			}
			if (EG(current_execute_data) == &dummy_execute_data) {
				EG(current_execute_data) = dummy_execute_data.prev_execute_data;
			}
			return FAILURE;
		} else if (error) {
			/* Capitalize the first latter of the error message */
			if (error[0] >= 'a' && error[0] <= 'z') {
				error[0] += ('A' - 'a');
			}
			gear_error(E_DEPRECATED, "%s", error);
			efree(error);
			if (UNEXPECTED(EG(exception))) {
				if (EG(current_execute_data) == &dummy_execute_data) {
					EG(current_execute_data) = dummy_execute_data.prev_execute_data;
				}
				return FAILURE;
			}
		}
	}

	func = fci_cache->function_handler;
	fci->object = (func->common.fn_flags & GEAR_ACC_STATIC) ?
		NULL : fci_cache->object;

	call = gear_vm_stack_push_call_frame(GEAR_CALL_TOP_FUNCTION | GEAR_CALL_DYNAMIC,
		func, fci->param_count, fci_cache->called_scope, fci->object);

	if (UNEXPECTED(func->common.fn_flags & GEAR_ACC_DEPRECATED)) {
		gear_error(E_DEPRECATED, "Function %s%s%s() is deprecated",
			func->common.scope ? ZSTR_VAL(func->common.scope->name) : "",
			func->common.scope ? "::" : "",
			ZSTR_VAL(func->common.function_name));
		if (UNEXPECTED(EG(exception))) {
			gear_vm_stack_free_call_frame(call);
			if (EG(current_execute_data) == &dummy_execute_data) {
				EG(current_execute_data) = dummy_execute_data.prev_execute_data;
			}
			return FAILURE;
		}
	}

	for (i=0; i<fci->param_count; i++) {
		zval *param;
		zval *arg = &fci->params[i];

		if (ARG_SHOULD_BE_SENT_BY_REF(func, i + 1)) {
			if (UNEXPECTED(!Z_ISREF_P(arg))) {
				if (!fci->no_separation) {
					/* Separation is enabled -- create a ref */
					ZVAL_NEW_REF(arg, arg);
				} else if (!ARG_MAY_BE_SENT_BY_REF(func, i + 1)) {
					/* By-value send is not allowed -- emit a warning,
					 * but still perform the call with a by-value send. */
					gear_error(E_WARNING,
						"Parameter %d to %s%s%s() expected to be a reference, value given", i+1,
						func->common.scope ? ZSTR_VAL(func->common.scope->name) : "",
						func->common.scope ? "::" : "",
						ZSTR_VAL(func->common.function_name));
					if (UNEXPECTED(EG(exception))) {
						GEAR_CALL_NUM_ARGS(call) = i;
						gear_vm_stack_free_args(call);
						gear_vm_stack_free_call_frame(call);
						if (EG(current_execute_data) == &dummy_execute_data) {
							EG(current_execute_data) = dummy_execute_data.prev_execute_data;
						}
						return FAILURE;
					}
				}
			}
		} else {
			if (Z_ISREF_P(arg) &&
			    !(func->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE)) {
				/* don't separate references for __call */
				arg = Z_REFVAL_P(arg);
			}
		}

		param = GEAR_CALL_ARG(call, i+1);
		ZVAL_COPY(param, arg);
	}

	if (UNEXPECTED(func->op_array.fn_flags & GEAR_ACC_CLOSURE)) {
		uint32_t call_info;

		GC_ADDREF(GEAR_CLOSURE_OBJECT(func));
		call_info = GEAR_CALL_CLOSURE;
		if (func->common.fn_flags & GEAR_ACC_FAKE_CLOSURE) {
			call_info |= GEAR_CALL_FAKE_CLOSURE;
		}
		GEAR_ADD_CALL_FLAG(call, call_info);
	}

	if (func->type == GEAR_USER_FUNCTION) {
		int call_via_handler = (func->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) != 0;
		const gear_op *current_opline_before_exception = EG(opline_before_exception);

		gear_init_func_execute_data(call, &func->op_array, fci->retval);
		gear_execute_ex(call);
		EG(opline_before_exception) = current_opline_before_exception;
		if (call_via_handler) {
			/* We must re-initialize function again */
			fci_cache->function_handler = NULL;
		}
	} else if (func->type == GEAR_INTERNAL_FUNCTION) {
		int call_via_handler = (func->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) != 0;
		ZVAL_NULL(fci->retval);
		call->prev_execute_data = EG(current_execute_data);
		call->return_value = NULL; /* this is not a constructor call */
		EG(current_execute_data) = call;
		if (EXPECTED(gear_execute_internal == NULL)) {
			/* saves one function call if gear_execute_internal is not used */
			func->internal_function.handler(call, fci->retval);
		} else {
			gear_execute_internal(call, fci->retval);
		}
		EG(current_execute_data) = call->prev_execute_data;
		gear_vm_stack_free_args(call);

		if (EG(exception)) {
			zval_ptr_dtor(fci->retval);
			ZVAL_UNDEF(fci->retval);
		}

		if (call_via_handler) {
			/* We must re-initialize function again */
			fci_cache->function_handler = NULL;
		}
	} else { /* GEAR_OVERLOADED_FUNCTION */
		ZVAL_NULL(fci->retval);

		/* Not sure what should be done here if it's a static method */
		if (fci->object) {
			call->prev_execute_data = EG(current_execute_data);
			EG(current_execute_data) = call;
			fci->object->handlers->call_method(func->common.function_name, fci->object, call, fci->retval);
			EG(current_execute_data) = call->prev_execute_data;
		} else {
			gear_throw_error(NULL, "Cannot call overloaded function for non-object");
		}

		gear_vm_stack_free_args(call);

		if (func->type == GEAR_OVERLOADED_FUNCTION_TEMPORARY) {
			gear_string_release_ex(func->common.function_name, 0);
		}
		efree(func);

		if (EG(exception)) {
			zval_ptr_dtor(fci->retval);
			ZVAL_UNDEF(fci->retval);
		}
	}

	gear_vm_stack_free_call_frame(call);

	if (EG(current_execute_data) == &dummy_execute_data) {
		EG(current_execute_data) = dummy_execute_data.prev_execute_data;
	}

	if (UNEXPECTED(EG(exception))) {
		if (UNEXPECTED(!EG(current_execute_data))) {
			gear_throw_exception_internal(NULL);
		} else if (EG(current_execute_data)->func &&
		           GEAR_USER_CODE(EG(current_execute_data)->func->common.type)) {
			gear_rethrow_exception(EG(current_execute_data));
		}
	}

	return SUCCESS;
}
/* }}} */

GEAR_API gear_class_entry *gear_lookup_class_ex(gear_string *name, const zval *key, int use_autoload) /* {{{ */
{
	gear_class_entry *ce = NULL;
	zval args[1], *zv;
	zval local_retval;
	gear_string *lc_name;
	gear_fcall_info fcall_info;
	gear_fcall_info_cache fcall_cache;

	if (key) {
		lc_name = Z_STR_P(key);
	} else {
		if (name == NULL || !ZSTR_LEN(name)) {
			return NULL;
		}

		if (ZSTR_VAL(name)[0] == '\\') {
			lc_name = gear_string_alloc(ZSTR_LEN(name) - 1, 0);
			gear_str_tolower_copy(ZSTR_VAL(lc_name), ZSTR_VAL(name) + 1, ZSTR_LEN(name) - 1);
		} else {
			lc_name = gear_string_tolower(name);
		}
	}

	zv = gear_hash_find(EG(class_table), lc_name);
	if (zv) {
		if (!key) {
			gear_string_release_ex(lc_name, 0);
		}
		return (gear_class_entry*)Z_PTR_P(zv);
	}

	/* The compiler is not-reentrant. Make sure we __autoload() only during run-time
	 * (doesn't impact functionality of __autoload()
	*/
	if (!use_autoload || gear_is_compiling()) {
		if (!key) {
			gear_string_release_ex(lc_name, 0);
		}
		return NULL;
	}

	if (!EG(autoload_func)) {
		gear_function *func = gear_fetch_function(ZSTR_KNOWN(GEAR_STR_MAGIC_AUTOLOAD));

		if (func) {
			EG(autoload_func) = func;
		} else {
			if (!key) {
				gear_string_release_ex(lc_name, 0);
			}
			return NULL;
		}

	}

	/* Verify class name before passing it to __autoload() */
	if (!key && strspn(ZSTR_VAL(name), "0123456789_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\200\201\202\203\204\205\206\207\210\211\212\213\214\215\216\217\220\221\222\223\224\225\226\227\230\231\232\233\234\235\236\237\240\241\242\243\244\245\246\247\250\251\252\253\254\255\256\257\260\261\262\263\264\265\266\267\270\271\272\273\274\275\276\277\300\301\302\303\304\305\306\307\310\311\312\313\314\315\316\317\320\321\322\323\324\325\326\327\330\331\332\333\334\335\336\337\340\341\342\343\344\345\346\347\350\351\352\353\354\355\356\357\360\361\362\363\364\365\366\367\370\371\372\373\374\375\376\377\\") != ZSTR_LEN(name)) {
		gear_string_release_ex(lc_name, 0);
		return NULL;
	}

	if (EG(in_autoload) == NULL) {
		ALLOC_HASHTABLE(EG(in_autoload));
		gear_hash_init(EG(in_autoload), 8, NULL, NULL, 0);
	}

	if (gear_hash_add_empty_element(EG(in_autoload), lc_name) == NULL) {
		if (!key) {
			gear_string_release_ex(lc_name, 0);
		}
		return NULL;
	}

	ZVAL_UNDEF(&local_retval);

	if (ZSTR_VAL(name)[0] == '\\') {
		ZVAL_STRINGL(&args[0], ZSTR_VAL(name) + 1, ZSTR_LEN(name) - 1);
	} else {
		ZVAL_STR_COPY(&args[0], name);
	}

	fcall_info.size = sizeof(fcall_info);
	ZVAL_STR_COPY(&fcall_info.function_name, EG(autoload_func)->common.function_name);
	fcall_info.retval = &local_retval;
	fcall_info.param_count = 1;
	fcall_info.params = args;
	fcall_info.object = NULL;
	fcall_info.no_separation = 1;

	fcall_cache.function_handler = EG(autoload_func);
	fcall_cache.called_scope = NULL;
	fcall_cache.object = NULL;

	gear_exception_save();
	if ((gear_call_function(&fcall_info, &fcall_cache) == SUCCESS) && !EG(exception)) {
		ce = gear_hash_find_ptr(EG(class_table), lc_name);
	}
	gear_exception_restore();

	zval_ptr_dtor(&args[0]);
	zval_ptr_dtor_str(&fcall_info.function_name);

	gear_hash_del(EG(in_autoload), lc_name);

	zval_ptr_dtor(&local_retval);

	if (!key) {
		gear_string_release_ex(lc_name, 0);
	}
	return ce;
}
/* }}} */

GEAR_API gear_class_entry *gear_lookup_class(gear_string *name) /* {{{ */
{
	return gear_lookup_class_ex(name, NULL, 1);
}
/* }}} */

GEAR_API gear_class_entry *gear_get_called_scope(gear_execute_data *ex) /* {{{ */
{
	while (ex) {
		if (Z_TYPE(ex->This) == IS_OBJECT) {
			return Z_OBJCE(ex->This);
		} else if (Z_CE(ex->This)) {
			return Z_CE(ex->This);
		} else if (ex->func) {
			if (ex->func->type != GEAR_INTERNAL_FUNCTION || ex->func->common.scope) {
				return NULL;
			}
		}
		ex = ex->prev_execute_data;
	}
	return NULL;
}
/* }}} */

GEAR_API gear_object *gear_get_this_object(gear_execute_data *ex) /* {{{ */
{
	while (ex) {
		if (Z_TYPE(ex->This) == IS_OBJECT) {
			return Z_OBJ(ex->This);
		} else if (ex->func) {
			if (ex->func->type != GEAR_INTERNAL_FUNCTION || ex->func->common.scope) {
				return NULL;
			}
		}
		ex = ex->prev_execute_data;
	}
	return NULL;
}
/* }}} */

GEAR_API int gear_eval_stringl(char *str, size_t str_len, zval *retval_ptr, char *string_name) /* {{{ */
{
	zval pv;
	gear_op_array *new_op_array;
	uint32_t original_compiler_options;
	int retval;

	if (retval_ptr) {
		ZVAL_NEW_STR(&pv, gear_string_alloc(str_len + sizeof("return ;")-1, 0));
		memcpy(Z_STRVAL(pv), "return ", sizeof("return ") - 1);
		memcpy(Z_STRVAL(pv) + sizeof("return ") - 1, str, str_len);
		Z_STRVAL(pv)[Z_STRLEN(pv) - 1] = ';';
		Z_STRVAL(pv)[Z_STRLEN(pv)] = '\0';
	} else {
		ZVAL_STRINGL(&pv, str, str_len);
	}

	/*printf("Evaluating '%s'\n", pv.value.str.val);*/

	original_compiler_options = CG(compiler_options);
	CG(compiler_options) = GEAR_COMPILE_DEFAULT_FOR_EVAL;
	new_op_array = gear_compile_string(&pv, string_name);
	CG(compiler_options) = original_compiler_options;

	if (new_op_array) {
		zval local_retval;

		EG(no_extensions)=1;

		new_op_array->scope = gear_get_executed_scope();

		gear_try {
			ZVAL_UNDEF(&local_retval);
			gear_execute(new_op_array, &local_retval);
		} gear_catch {
			destroy_op_array(new_op_array);
			efree_size(new_op_array, sizeof(gear_op_array));
			gear_bailout();
		} gear_end_try();

		if (Z_TYPE(local_retval) != IS_UNDEF) {
			if (retval_ptr) {
				ZVAL_COPY_VALUE(retval_ptr, &local_retval);
			} else {
				zval_ptr_dtor(&local_retval);
			}
		} else {
			if (retval_ptr) {
				ZVAL_NULL(retval_ptr);
			}
		}

		EG(no_extensions)=0;
		destroy_op_array(new_op_array);
		efree_size(new_op_array, sizeof(gear_op_array));
		retval = SUCCESS;
	} else {
		retval = FAILURE;
	}
	zval_ptr_dtor_str(&pv);
	return retval;
}
/* }}} */

GEAR_API int gear_eval_string(char *str, zval *retval_ptr, char *string_name) /* {{{ */
{
	return gear_eval_stringl(str, strlen(str), retval_ptr, string_name);
}
/* }}} */

GEAR_API int gear_eval_stringl_ex(char *str, size_t str_len, zval *retval_ptr, char *string_name, int handle_exceptions) /* {{{ */
{
	int result;

	result = gear_eval_stringl(str, str_len, retval_ptr, string_name);
	if (handle_exceptions && EG(exception)) {
		gear_exception_error(EG(exception), E_ERROR);
		result = FAILURE;
	}
	return result;
}
/* }}} */

GEAR_API int gear_eval_string_ex(char *str, zval *retval_ptr, char *string_name, int handle_exceptions) /* {{{ */
{
	return gear_eval_stringl_ex(str, strlen(str), retval_ptr, string_name, handle_exceptions);
}
/* }}} */

static void gear_set_timeout_ex(gear_long seconds, int reset_signals);

GEAR_API GEAR_NORETURN void gear_timeout(int dummy) /* {{{ */
{
#if defined(HYSS_WIN32)
# ifndef ZTS
	/* No action is needed if we're timed out because zero seconds are
	   just ignored. Also, the hard timeout needs to be respected. If the
	   timer is not restarted properly, it could hang in the shutdown
	   function. */
	if (EG(hard_timeout) > 0) {
		EG(timed_out) = 0;
		gear_set_timeout_ex(EG(hard_timeout), 1);
		/* XXX Abused, introduce an additional flag if the value needs to be kept. */
		EG(hard_timeout) = 0;
	}
# endif
#else
	EG(timed_out) = 0;
	gear_set_timeout_ex(0, 1);
#endif

	gear_error_noreturn(E_ERROR, "Maximum execution time of " GEAR_LONG_FMT " second%s exceeded", EG(timeout_seconds), EG(timeout_seconds) == 1 ? "" : "s");
}
/* }}} */

#ifndef GEAR_WIN32
static void gear_timeout_handler(int dummy) /* {{{ */
{
#ifndef ZTS
    if (EG(timed_out)) {
		/* Die on hard timeout */
		const char *error_filename = NULL;
		uint32_t error_lineno = 0;
		char log_buffer[2048];
		int output_len = 0;

		if (gear_is_compiling()) {
			error_filename = ZSTR_VAL(gear_get_compiled_filename());
			error_lineno = gear_get_compiled_lineno();
		} else if (gear_is_executing()) {
			error_filename = gear_get_executed_filename();
			if (error_filename[0] == '[') { /* [no active file] */
				error_filename = NULL;
				error_lineno = 0;
			} else {
				error_lineno = gear_get_executed_lineno();
			}
		}
		if (!error_filename) {
			error_filename = "Unknown";
		}

		output_len = snprintf(log_buffer, sizeof(log_buffer), "\nFatal error: Maximum execution time of " GEAR_LONG_FMT "+" GEAR_LONG_FMT " seconds exceeded (terminated) in %s on line %d\n", EG(timeout_seconds), EG(hard_timeout), error_filename, error_lineno);
		if (output_len > 0) {
			write(2, log_buffer, MIN(output_len, sizeof(log_buffer)));
		}
		_exit(124);
    }
#endif

	if (gear_on_timeout) {
#ifdef GEAR_SIGNALS
		/*
		   We got here because we got a timeout signal, so we are in a signal handler
		   at this point. However, we want to be able to timeout any user-supplied
		   shutdown functions, so pretend we are not in a signal handler while we are
		   calling these
		*/
		SIGG(running) = 0;
#endif
		gear_on_timeout(EG(timeout_seconds));
	}

	EG(timed_out) = 1;
	EG(vm_interrupt) = 1;

#ifndef ZTS
	if (EG(hard_timeout) > 0) {
		/* Set hard timeout */
		gear_set_timeout_ex(EG(hard_timeout), 1);
	}
#endif
}
/* }}} */
#endif

#ifdef GEAR_WIN32
VOID CALLBACK tq_timer_cb(PVOID arg, BOOLEAN timed_out)
{
	gear_executor_globals *eg;

	/* The doc states it'll be always true, however it theoretically
		could be FALSE when the thread was signaled. */
	if (!timed_out) {
		return;
	}

	eg = (gear_executor_globals *)arg;
	eg->timed_out = 1;
	eg->vm_interrupt = 1;
}
#endif

/* This one doesn't exists on QNX */
#ifndef SIGPROF
#define SIGPROF 27
#endif

static void gear_set_timeout_ex(gear_long seconds, int reset_signals) /* {{{ */
{
#ifdef GEAR_WIN32
	gear_executor_globals *eg;

	if(!seconds) {
		return;
	}

        /* Don't use ChangeTimerQueueTimer() as it will not restart an expired
		timer, so we could end up with just an ignored timeout. Instead
		delete and recreate. */
	if (NULL != tq_timer) {
		if (!DeleteTimerQueueTimer(NULL, tq_timer, NULL)) {
			tq_timer = NULL;
			gear_error_noreturn(E_ERROR, "Could not delete queued timer");
			return;
		}
		tq_timer = NULL;
	}

	/* XXX passing NULL means the default timer queue provided by the system is used */
	eg = GEAR_CAPI_GLOBALS_BULK(executor);
	if (!CreateTimerQueueTimer(&tq_timer, NULL, (WAITORTIMERCALLBACK)tq_timer_cb, (VOID*)eg, seconds*1000, 0, WT_EXECUTEONLYONCE)) {
		tq_timer = NULL;
		gear_error_noreturn(E_ERROR, "Could not queue new timer");
		return;
	}
#else
#	ifdef HAVE_SETITIMER
	{
		struct itimerval t_r;		/* timeout requested */
		int signo;

		if(seconds) {
			t_r.it_value.tv_sec = seconds;
			t_r.it_value.tv_usec = t_r.it_interval.tv_sec = t_r.it_interval.tv_usec = 0;

#	ifdef __CYGWIN__
			setitimer(ITIMER_REAL, &t_r, NULL);
		}
		signo = SIGALRM;
#	else
			setitimer(ITIMER_PROF, &t_r, NULL);
		}
		signo = SIGPROF;
#	endif

		if (reset_signals) {
#	ifdef GEAR_SIGNALS
			gear_signal(signo, gear_timeout_handler);
#	else
			sigset_t sigset;
#   ifdef HAVE_SIGACTION
			struct sigaction act;

			act.sa_handler = gear_timeout_handler;
			sigemptyset(&act.sa_mask);
			act.sa_flags = SA_RESETHAND | SA_NODEFER;
			sigaction(signo, &act, NULL);
#   else
			signal(signo, gear_timeout_handler);
#   endif /* HAVE_SIGACTION */
			sigemptyset(&sigset);
			sigaddset(&sigset, signo);
			sigprocmask(SIG_UNBLOCK, &sigset, NULL);
#	endif /* GEAR_SIGNALS */
		}
	}
#	endif /* HAVE_SETITIMER */
#endif
}
/* }}} */

void gear_set_timeout(gear_long seconds, int reset_signals) /* {{{ */
{

	EG(timeout_seconds) = seconds;
	gear_set_timeout_ex(seconds, reset_signals);
	EG(timed_out) = 0;
}
/* }}} */

void gear_unset_timeout(void) /* {{{ */
{
#ifdef GEAR_WIN32
	if (NULL != tq_timer) {
		if (!DeleteTimerQueueTimer(NULL, tq_timer, NULL)) {
			EG(timed_out) = 0;
			tq_timer = NULL;
			gear_error_noreturn(E_ERROR, "Could not delete queued timer");
			return;
		}
		tq_timer = NULL;
	}
	EG(timed_out) = 0;
#else
#	ifdef HAVE_SETITIMER
	if (EG(timeout_seconds)) {
		struct itimerval no_timeout;

		no_timeout.it_value.tv_sec = no_timeout.it_value.tv_usec = no_timeout.it_interval.tv_sec = no_timeout.it_interval.tv_usec = 0;

#ifdef __CYGWIN__
		setitimer(ITIMER_REAL, &no_timeout, NULL);
#else
		setitimer(ITIMER_PROF, &no_timeout, NULL);
#endif
	}
#	endif
	EG(timed_out) = 0;
#endif
}
/* }}} */

gear_class_entry *gear_fetch_class(gear_string *class_name, int fetch_type) /* {{{ */
{
	gear_class_entry *ce, *scope;
	int fetch_sub_type = fetch_type & GEAR_FETCH_CLASS_MASK;

check_fetch_type:
	switch (fetch_sub_type) {
		case GEAR_FETCH_CLASS_SELF:
			scope = gear_get_executed_scope();
			if (UNEXPECTED(!scope)) {
				gear_throw_or_error(fetch_type, NULL, "Cannot access self:: when no class scope is active");
			}
			return scope;
		case GEAR_FETCH_CLASS_PARENT:
			scope = gear_get_executed_scope();
			if (UNEXPECTED(!scope)) {
				gear_throw_or_error(fetch_type, NULL, "Cannot access parent:: when no class scope is active");
				return NULL;
			}
			if (UNEXPECTED(!scope->parent)) {
				gear_throw_or_error(fetch_type, NULL, "Cannot access parent:: when current class scope has no parent");
			}
			return scope->parent;
		case GEAR_FETCH_CLASS_STATIC:
			ce = gear_get_called_scope(EG(current_execute_data));
			if (UNEXPECTED(!ce)) {
				gear_throw_or_error(fetch_type, NULL, "Cannot access static:: when no class scope is active");
				return NULL;
			}
			return ce;
		case GEAR_FETCH_CLASS_AUTO: {
				fetch_sub_type = gear_get_class_fetch_type(class_name);
				if (UNEXPECTED(fetch_sub_type != GEAR_FETCH_CLASS_DEFAULT)) {
					goto check_fetch_type;
				}
			}
			break;
	}

	if (fetch_type & GEAR_FETCH_CLASS_NO_AUTOLOAD) {
		return gear_lookup_class_ex(class_name, NULL, 0);
	} else if ((ce = gear_lookup_class_ex(class_name, NULL, 1)) == NULL) {
		if (!(fetch_type & GEAR_FETCH_CLASS_SILENT) && !EG(exception)) {
			if (fetch_sub_type == GEAR_FETCH_CLASS_INTERFACE) {
				gear_throw_or_error(fetch_type, NULL, "Interface '%s' not found", ZSTR_VAL(class_name));
			} else if (fetch_sub_type == GEAR_FETCH_CLASS_TRAIT) {
				gear_throw_or_error(fetch_type, NULL, "Trait '%s' not found", ZSTR_VAL(class_name));
			} else {
				gear_throw_or_error(fetch_type, NULL, "Class '%s' not found", ZSTR_VAL(class_name));
			}
		}
		return NULL;
	}
	return ce;
}
/* }}} */

gear_class_entry *gear_fetch_class_by_name(gear_string *class_name, const zval *key, int fetch_type) /* {{{ */
{
	gear_class_entry *ce;

	if (fetch_type & GEAR_FETCH_CLASS_NO_AUTOLOAD) {
		return gear_lookup_class_ex(class_name, key, 0);
	} else if ((ce = gear_lookup_class_ex(class_name, key, 1)) == NULL) {
		if ((fetch_type & GEAR_FETCH_CLASS_SILENT) == 0 && !EG(exception)) {
			if ((fetch_type & GEAR_FETCH_CLASS_MASK) == GEAR_FETCH_CLASS_INTERFACE) {
				gear_throw_or_error(fetch_type, NULL, "Interface '%s' not found", ZSTR_VAL(class_name));
			} else if ((fetch_type & GEAR_FETCH_CLASS_MASK) == GEAR_FETCH_CLASS_TRAIT) {
				gear_throw_or_error(fetch_type, NULL, "Trait '%s' not found", ZSTR_VAL(class_name));
			} else {
				gear_throw_or_error(fetch_type, NULL, "Class '%s' not found", ZSTR_VAL(class_name));
			}
		}
		return NULL;
	}
	return ce;
}
/* }}} */

#define MAX_ABSTRACT_INFO_CNT 3
#define MAX_ABSTRACT_INFO_FMT "%s%s%s%s"
#define DISPLAY_ABSTRACT_FN(idx) \
	ai.afn[idx] ? GEAR_FN_SCOPE_NAME(ai.afn[idx]) : "", \
	ai.afn[idx] ? "::" : "", \
	ai.afn[idx] ? ZSTR_VAL(ai.afn[idx]->common.function_name) : "", \
	ai.afn[idx] && ai.afn[idx + 1] ? ", " : (ai.afn[idx] && ai.cnt > MAX_ABSTRACT_INFO_CNT ? ", ..." : "")

typedef struct _gear_abstract_info {
	gear_function *afn[MAX_ABSTRACT_INFO_CNT + 1];
	int cnt;
	int ctor;
} gear_abstract_info;

static void gear_verify_abstract_class_function(gear_function *fn, gear_abstract_info *ai) /* {{{ */
{
	if (fn->common.fn_flags & GEAR_ACC_ABSTRACT) {
		if (ai->cnt < MAX_ABSTRACT_INFO_CNT) {
			ai->afn[ai->cnt] = fn;
		}
		if (fn->common.fn_flags & GEAR_ACC_CTOR) {
			if (!ai->ctor) {
				ai->cnt++;
				ai->ctor = 1;
			} else {
				ai->afn[ai->cnt] = NULL;
			}
		} else {
			ai->cnt++;
		}
	}
}
/* }}} */

void gear_verify_abstract_class(gear_class_entry *ce) /* {{{ */
{
	gear_function *func;
	gear_abstract_info ai;

	if ((ce->ce_flags & GEAR_ACC_IMPLICIT_ABSTRACT_CLASS) && !(ce->ce_flags & (GEAR_ACC_TRAIT | GEAR_ACC_EXPLICIT_ABSTRACT_CLASS))) {
		memset(&ai, 0, sizeof(ai));

		GEAR_HASH_FOREACH_PTR(&ce->function_table, func) {
			gear_verify_abstract_class_function(func, &ai);
		} GEAR_HASH_FOREACH_END();

		if (ai.cnt) {
			gear_error_noreturn(E_ERROR, "Class %s contains %d abstract method%s and must therefore be declared abstract or implement the remaining methods (" MAX_ABSTRACT_INFO_FMT MAX_ABSTRACT_INFO_FMT MAX_ABSTRACT_INFO_FMT ")",
				ZSTR_VAL(ce->name), ai.cnt,
				ai.cnt > 1 ? "s" : "",
				DISPLAY_ABSTRACT_FN(0),
				DISPLAY_ABSTRACT_FN(1),
				DISPLAY_ABSTRACT_FN(2)
				);
		}
	}
}
/* }}} */

GEAR_API int gear_delete_global_variable(gear_string *name) /* {{{ */
{
    return gear_hash_del_ind(&EG(symbol_table), name);
}
/* }}} */

GEAR_API gear_array *gear_rebuild_symbol_table(void) /* {{{ */
{
	gear_execute_data *ex;
	gear_array *symbol_table;

	/* Search for last called user function */
	ex = EG(current_execute_data);
	while (ex && (!ex->func || !GEAR_USER_CODE(ex->func->common.type))) {
		ex = ex->prev_execute_data;
	}
	if (!ex) {
		return NULL;
	}
	if (GEAR_CALL_INFO(ex) & GEAR_CALL_HAS_SYMBOL_TABLE) {
		return ex->symbol_table;
	}

	GEAR_ADD_CALL_FLAG(ex, GEAR_CALL_HAS_SYMBOL_TABLE);
	if (EG(symtable_cache_ptr) >= EG(symtable_cache)) {
		/*printf("Cache hit!  Reusing %x\n", symtable_cache[symtable_cache_ptr]);*/
		symbol_table = ex->symbol_table = *(EG(symtable_cache_ptr)--);
		if (!ex->func->op_array.last_var) {
			return symbol_table;
		}
		gear_hash_extend(symbol_table, ex->func->op_array.last_var, 0);
	} else {
		symbol_table = ex->symbol_table = gear_new_array(ex->func->op_array.last_var);
		if (!ex->func->op_array.last_var) {
			return symbol_table;
		}
		gear_hash_real_init_mixed(symbol_table);
		/*printf("Cache miss!  Initialized %x\n", EG(active_symbol_table));*/
	}
	if (EXPECTED(ex->func->op_array.last_var)) {
		gear_string **str = ex->func->op_array.vars;
		gear_string **end = str + ex->func->op_array.last_var;
		zval *var = GEAR_CALL_VAR_NUM(ex, 0);

		do {
			_gear_hash_append_ind(symbol_table, *str, var);
			str++;
			var++;
		} while (str != end);
	}
	return symbol_table;
}
/* }}} */

GEAR_API void gear_attach_symbol_table(gear_execute_data *execute_data) /* {{{ */
{
	gear_op_array *op_array = &execute_data->func->op_array;
	HashTable *ht = execute_data->symbol_table;

	/* copy real values from symbol table into CV slots and create
	   INDIRECT references to CV in symbol table  */
	if (EXPECTED(op_array->last_var)) {
		gear_string **str = op_array->vars;
		gear_string **end = str + op_array->last_var;
		zval *var = EX_VAR_NUM(0);

		do {
			zval *zv = gear_hash_find_ex(ht, *str, 1);

			if (zv) {
				if (Z_TYPE_P(zv) == IS_INDIRECT) {
					zval *val = Z_INDIRECT_P(zv);

					ZVAL_COPY_VALUE(var, val);
				} else {
					ZVAL_COPY_VALUE(var, zv);
				}
			} else {
				ZVAL_UNDEF(var);
				zv = gear_hash_add_new(ht, *str, var);
			}
			ZVAL_INDIRECT(zv, var);
			str++;
			var++;
		} while (str != end);
	}
}
/* }}} */

GEAR_API void gear_detach_symbol_table(gear_execute_data *execute_data) /* {{{ */
{
	gear_op_array *op_array = &execute_data->func->op_array;
	HashTable *ht = execute_data->symbol_table;

	/* copy real values from CV slots into symbol table */
	if (EXPECTED(op_array->last_var)) {
		gear_string **str = op_array->vars;
		gear_string **end = str + op_array->last_var;
		zval *var = EX_VAR_NUM(0);

		do {
			if (Z_TYPE_P(var) == IS_UNDEF) {
				gear_hash_del(ht, *str);
			} else {
				gear_hash_update(ht, *str, var);
				ZVAL_UNDEF(var);
			}
			str++;
			var++;
		} while (str != end);
	}
}
/* }}} */

GEAR_API int gear_set_local_var(gear_string *name, zval *value, int force) /* {{{ */
{
	gear_execute_data *execute_data = EG(current_execute_data);

	while (execute_data && (!execute_data->func || !GEAR_USER_CODE(execute_data->func->common.type))) {
		execute_data = execute_data->prev_execute_data;
	}

	if (execute_data) {
		if (!(EX_CALL_INFO() & GEAR_CALL_HAS_SYMBOL_TABLE)) {
			gear_ulong h = gear_string_hash_val(name);
			gear_op_array *op_array = &execute_data->func->op_array;

			if (EXPECTED(op_array->last_var)) {
				gear_string **str = op_array->vars;
				gear_string **end = str + op_array->last_var;

				do {
					if (ZSTR_H(*str) == h &&
					    gear_string_equal_content(*str, name)) {
						zval *var = EX_VAR_NUM(str - op_array->vars);
						ZVAL_COPY_VALUE(var, value);
						return SUCCESS;
					}
					str++;
				} while (str != end);
			}
			if (force) {
				gear_array *symbol_table = gear_rebuild_symbol_table();
				if (symbol_table) {
					gear_hash_update(symbol_table, name, value);
					return SUCCESS;
				}
			}
		} else {
			gear_hash_update_ind(execute_data->symbol_table, name, value);
			return SUCCESS;
		}
	}
	return FAILURE;
}
/* }}} */

GEAR_API int gear_set_local_var_str(const char *name, size_t len, zval *value, int force) /* {{{ */
{
	gear_execute_data *execute_data = EG(current_execute_data);

	while (execute_data && (!execute_data->func || !GEAR_USER_CODE(execute_data->func->common.type))) {
		execute_data = execute_data->prev_execute_data;
	}

	if (execute_data) {
		if (!(EX_CALL_INFO() & GEAR_CALL_HAS_SYMBOL_TABLE)) {
			gear_ulong h = gear_hash_func(name, len);
			gear_op_array *op_array = &execute_data->func->op_array;
			if (EXPECTED(op_array->last_var)) {
				gear_string **str = op_array->vars;
				gear_string **end = str + op_array->last_var;

				do {
					if (ZSTR_H(*str) == h &&
					    ZSTR_LEN(*str) == len &&
					    memcmp(ZSTR_VAL(*str), name, len) == 0) {
						zval *var = EX_VAR_NUM(str - op_array->vars);
						zval_ptr_dtor(var);
						ZVAL_COPY_VALUE(var, value);
						return SUCCESS;
					}
					str++;
				} while (str != end);
			}
			if (force) {
				gear_array *symbol_table = gear_rebuild_symbol_table();
				if (symbol_table) {
					gear_hash_str_update(symbol_table, name, len, value);
					return SUCCESS;
				}
			}
		} else {
			gear_hash_str_update_ind(execute_data->symbol_table, name, len, value);
			return SUCCESS;
		}
	}
	return FAILURE;
}
/* }}} */

GEAR_API int gear_forbid_dynamic_call(const char *func_name) /* {{{ */
{
	gear_execute_data *ex = EG(current_execute_data);
	GEAR_ASSERT(ex != NULL && ex->func != NULL);

	if (GEAR_CALL_INFO(ex) & GEAR_CALL_DYNAMIC) {
		gear_error(E_WARNING, "Cannot call %s dynamically", func_name);
		return FAILURE;
	}

	return SUCCESS;
}
/* }}} */

