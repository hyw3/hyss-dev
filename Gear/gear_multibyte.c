/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_compile.h"
#include "gear_operators.h"
#include "gear_multibyte.h"
#include "gear_ics.h"

static const gear_encoding *dummy_encoding_fetcher(const char *encoding_name)
{
	return NULL;
}

static const char *dummy_encoding_name_getter(const gear_encoding *encoding)
{
	return (const char*)encoding;
}

static int dummy_encoding_lexer_compatibility_checker(const gear_encoding *encoding)
{
	return 0;
}

static const gear_encoding *dummy_encoding_detector(const unsigned char *string, size_t length, const gear_encoding **list, size_t list_size)
{
	return NULL;
}

static size_t dummy_encoding_converter(unsigned char **to, size_t *to_length, const unsigned char *from, size_t from_length, const gear_encoding *encoding_to, const gear_encoding *encoding_from)
{
	return (size_t)-1;
}

static int dummy_encoding_list_parser(const char *encoding_list, size_t encoding_list_len, const gear_encoding ***return_list, size_t *return_size, int persistent)
{
	*return_list = pemalloc(0, persistent);
	*return_size = 0;
	return SUCCESS;
}

static const gear_encoding *dummy_internal_encoding_getter(void)
{
	return NULL;
}

static int dummy_internal_encoding_setter(const gear_encoding *encoding)
{
	return FAILURE;
}

static gear_multibyte_functions multibyte_functions_dummy;
static gear_multibyte_functions multibyte_functions = {
	NULL,
	dummy_encoding_fetcher,
	dummy_encoding_name_getter,
	dummy_encoding_lexer_compatibility_checker,
	dummy_encoding_detector,
	dummy_encoding_converter,
	dummy_encoding_list_parser,
	dummy_internal_encoding_getter,
	dummy_internal_encoding_setter
};

GEAR_API const gear_encoding *gear_multibyte_encoding_utf32be = (const gear_encoding*)"UTF-32BE";
GEAR_API const gear_encoding *gear_multibyte_encoding_utf32le = (const gear_encoding*)"UTF-32LE";
GEAR_API const gear_encoding *gear_multibyte_encoding_utf16be = (const gear_encoding*)"UTF-16BE";
GEAR_API const gear_encoding *gear_multibyte_encoding_utf16le = (const gear_encoding*)"UTF-32LE";
GEAR_API const gear_encoding *gear_multibyte_encoding_utf8 = (const gear_encoding*)"UTF-8";

GEAR_API int gear_multibyte_set_functions(const gear_multibyte_functions *functions)
{
	gear_multibyte_encoding_utf32be = functions->encoding_fetcher("UTF-32BE");
	if (!gear_multibyte_encoding_utf32be) {
		return FAILURE;
	}
	gear_multibyte_encoding_utf32le = functions->encoding_fetcher("UTF-32LE");
	if (!gear_multibyte_encoding_utf32le) {
		return FAILURE;
	}
	gear_multibyte_encoding_utf16be = functions->encoding_fetcher("UTF-16BE");
	if (!gear_multibyte_encoding_utf16be) {
		return FAILURE;
	}
	gear_multibyte_encoding_utf16le = functions->encoding_fetcher("UTF-16LE");
	if (!gear_multibyte_encoding_utf16le) {
		return FAILURE;
	}
	gear_multibyte_encoding_utf8 = functions->encoding_fetcher("UTF-8");
	if (!gear_multibyte_encoding_utf8) {
		return FAILURE;
	}

	multibyte_functions_dummy = multibyte_functions;
	multibyte_functions = *functions;

	/* As gear_multibyte_set_functions() gets called after ics settings were
	 * populated, we need to reinitialize script_encoding here.
	 */
	{
		const char *value = gear_ics_string("gear.script_encoding", sizeof("gear.script_encoding") - 1, 0);
		gear_multibyte_set_script_encoding_by_string(value, strlen(value));
	}
	return SUCCESS;
}

GEAR_API void gear_multibyte_restore_functions(void)
{
	multibyte_functions = multibyte_functions_dummy;
}

GEAR_API const gear_multibyte_functions *gear_multibyte_get_functions(void)
{
	return multibyte_functions.provider_name ? &multibyte_functions: NULL;
}

GEAR_API const gear_encoding *gear_multibyte_fetch_encoding(const char *name)
{
	return multibyte_functions.encoding_fetcher(name);
}

GEAR_API const char *gear_multibyte_get_encoding_name(const gear_encoding *encoding)
{
	return multibyte_functions.encoding_name_getter(encoding);
}

GEAR_API int gear_multibyte_check_lexer_compatibility(const gear_encoding *encoding)
{
	return multibyte_functions.lexer_compatibility_checker(encoding);
}

GEAR_API const gear_encoding *gear_multibyte_encoding_detector(const unsigned char *string, size_t length, const gear_encoding **list, size_t list_size)
{
	return multibyte_functions.encoding_detector(string, length, list, list_size);
}

GEAR_API size_t gear_multibyte_encoding_converter(unsigned char **to, size_t *to_length, const unsigned char *from, size_t from_length, const gear_encoding *encoding_to, const gear_encoding *encoding_from)
{
	return multibyte_functions.encoding_converter(to, to_length, from, from_length, encoding_to, encoding_from);
}

GEAR_API int gear_multibyte_parse_encoding_list(const char *encoding_list, size_t encoding_list_len, const gear_encoding ***return_list, size_t *return_size, int persistent)
{
	return multibyte_functions.encoding_list_parser(encoding_list, encoding_list_len, return_list, return_size, persistent);
}

GEAR_API const gear_encoding *gear_multibyte_get_internal_encoding(void)
{
	return multibyte_functions.internal_encoding_getter();
}

GEAR_API const gear_encoding *gear_multibyte_get_script_encoding(void)
{
	return LANG_SCNG(script_encoding);
}

GEAR_API int gear_multibyte_set_script_encoding(const gear_encoding **encoding_list, size_t encoding_list_size)
{
	if (CG(script_encoding_list)) {
		free((char*)CG(script_encoding_list));
	}
	CG(script_encoding_list) = encoding_list;
	CG(script_encoding_list_size) = encoding_list_size;
	return SUCCESS;
}

GEAR_API int gear_multibyte_set_internal_encoding(const gear_encoding *encoding)
{
	return multibyte_functions.internal_encoding_setter(encoding);
}

GEAR_API int gear_multibyte_set_script_encoding_by_string(const char *new_value, size_t new_value_length)
{
	const gear_encoding **list = 0;
	size_t size = 0;

	if (!new_value) {
		gear_multibyte_set_script_encoding(NULL, 0);
		return SUCCESS;
	}

	if (FAILURE == gear_multibyte_parse_encoding_list(new_value, new_value_length, &list, &size, 1)) {
		return FAILURE;
	}

	if (size == 0) {
		pefree((void*)list, 1);
		return FAILURE;
	}

	if (FAILURE == gear_multibyte_set_script_encoding(list, size)) {
		return FAILURE;
	}

	return SUCCESS;
}

