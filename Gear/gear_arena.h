/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GEAR_ARENA_H_
#define _GEAR_ARENA_H_

#include "gear.h"

typedef struct _gear_arena gear_arena;

struct _gear_arena {
	char		*ptr;
	char		*end;
	gear_arena  *prev;
};

static gear_always_inline gear_arena* gear_arena_create(size_t size)
{
	gear_arena *arena = (gear_arena*)emalloc(size);

	arena->ptr = (char*) arena + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena));
	arena->end = (char*) arena + size;
	arena->prev = NULL;
	return arena;
}

static gear_always_inline void gear_arena_destroy(gear_arena *arena)
{
	do {
		gear_arena *prev = arena->prev;
		efree(arena);
		arena = prev;
	} while (arena);
}

#define GEAR_ARENA_ALIGNMENT 8U

static gear_always_inline void* gear_arena_alloc(gear_arena **arena_ptr, size_t size)
{
	gear_arena *arena = *arena_ptr;
	char *ptr = arena->ptr;

	size = GEAR_MM_ALIGNED_SIZE(size);

	if (EXPECTED(size <= (size_t)(arena->end - ptr))) {
		arena->ptr = ptr + size;
	} else {
		size_t arena_size =
			UNEXPECTED((size + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena))) > (size_t)(arena->end - (char*) arena)) ?
				(size + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena))) :
				(size_t)(arena->end - (char*) arena);
		gear_arena *new_arena = (gear_arena*)emalloc(arena_size);

		ptr = (char*) new_arena + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena));
		new_arena->ptr = (char*) new_arena + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena)) + size;
		new_arena->end = (char*) new_arena + arena_size;
		new_arena->prev = arena;
		*arena_ptr = new_arena;
	}

	return (void*) ptr;
}

static gear_always_inline void* gear_arena_calloc(gear_arena **arena_ptr, size_t count, size_t unit_size)
{
	int overflow;
	size_t size;
	void *ret;

	size = gear_safe_address(unit_size, count, 0, &overflow);
	if (UNEXPECTED(overflow)) {
		gear_error(E_ERROR, "Possible integer overflow in gear_arena_calloc() (%zu * %zu)", unit_size, count);
	}
	ret = gear_arena_alloc(arena_ptr, size);
	memset(ret, 0, size);
	return ret;
}

static gear_always_inline void* gear_arena_checkpoint(gear_arena *arena)
{
	return arena->ptr;
}

static gear_always_inline void gear_arena_release(gear_arena **arena_ptr, void *checkpoint)
{
	gear_arena *arena = *arena_ptr;

	while (UNEXPECTED((char*)checkpoint > arena->end) ||
	       UNEXPECTED((char*)checkpoint <= (char*)arena)) {
		gear_arena *prev = arena->prev;
		efree(arena);
		*arena_ptr = arena = prev;
	}
	GEAR_ASSERT((char*)checkpoint > (char*)arena && (char*)checkpoint <= arena->end);
	arena->ptr = (char*)checkpoint;
}

#endif /* _GEAR_ARENA_H_ */

