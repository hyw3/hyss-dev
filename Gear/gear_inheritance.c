/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"
#include "gear_compile.h"
#include "gear_execute.h"
#include "gear_inheritance.h"
#include "gear_smart_str.h"
#include "gear_operators.h"

static void overriden_ptr_dtor(zval *zv) /* {{{ */
{
	efree_size(Z_PTR_P(zv), sizeof(gear_function));
}
/* }}} */

static gear_property_info *gear_duplicate_property_info(gear_property_info *property_info) /* {{{ */
{
	gear_property_info* new_property_info;

	new_property_info = gear_arena_alloc(&CG(arena), sizeof(gear_property_info));
	memcpy(new_property_info, property_info, sizeof(gear_property_info));
	gear_string_addref(new_property_info->name);
	if (new_property_info->doc_comment) {
		gear_string_addref(new_property_info->doc_comment);
	}
	return new_property_info;
}
/* }}} */

static gear_property_info *gear_duplicate_property_info_internal(gear_property_info *property_info) /* {{{ */
{
	gear_property_info* new_property_info = pemalloc(sizeof(gear_property_info), 1);
	memcpy(new_property_info, property_info, sizeof(gear_property_info));
	gear_string_addref(new_property_info->name);
	return new_property_info;
}
/* }}} */

static gear_function *gear_duplicate_function(gear_function *func, gear_class_entry *ce) /* {{{ */
{
	gear_function *new_function;

	if (UNEXPECTED(func->type == GEAR_INTERNAL_FUNCTION)) {
		if (UNEXPECTED(ce->type & GEAR_INTERNAL_CLASS)) {
			new_function = pemalloc(sizeof(gear_internal_function), 1);
			memcpy(new_function, func, sizeof(gear_internal_function));
		} else {
			new_function = gear_arena_alloc(&CG(arena), sizeof(gear_internal_function));
			memcpy(new_function, func, sizeof(gear_internal_function));
			new_function->common.fn_flags |= GEAR_ACC_ARENA_ALLOCATED;
		}
		if (EXPECTED(new_function->common.function_name)) {
			gear_string_addref(new_function->common.function_name);
		}
	} else {
		if (func->op_array.refcount) {
			(*func->op_array.refcount)++;
		}
		if (EXPECTED(!func->op_array.static_variables)) {
			/* reuse the same op_array structure */
			return func;
		}
		if (!(GC_FLAGS(func->op_array.static_variables) & IS_ARRAY_IMMUTABLE)) {
			GC_ADDREF(func->op_array.static_variables);
		}
		new_function = gear_arena_alloc(&CG(arena), sizeof(gear_op_array));
		memcpy(new_function, func, sizeof(gear_op_array));
	}
	return new_function;
}
/* }}} */

static void do_inherit_parent_constructor(gear_class_entry *ce) /* {{{ */
{
	GEAR_ASSERT(ce->parent != NULL);

	/* You cannot change create_object */
	ce->create_object = ce->parent->create_object;

	/* Inherit special functions if needed */
	if (EXPECTED(!ce->get_iterator)) {
		ce->get_iterator = ce->parent->get_iterator;
	}
	if (EXPECTED(!ce->iterator_funcs_ptr) && UNEXPECTED(ce->parent->iterator_funcs_ptr)) {
		if (ce->type == GEAR_INTERNAL_CLASS) {
			ce->iterator_funcs_ptr = calloc(1, sizeof(gear_class_iterator_funcs));
			if (ce->parent->iterator_funcs_ptr->zf_new_iterator) {
				ce->iterator_funcs_ptr->zf_new_iterator = gear_hash_str_find_ptr(&ce->function_table, "getiterator", sizeof("getiterator") - 1);
			}
			if (ce->parent->iterator_funcs_ptr->zf_current) {
				ce->iterator_funcs_ptr->zf_rewind = gear_hash_str_find_ptr(&ce->function_table, "rewind", sizeof("rewind") - 1);
				ce->iterator_funcs_ptr->zf_valid = gear_hash_str_find_ptr(&ce->function_table, "valid", sizeof("valid") - 1);
				ce->iterator_funcs_ptr->zf_key = gear_hash_str_find_ptr(&ce->function_table, "key", sizeof("key") - 1);
				ce->iterator_funcs_ptr->zf_current = gear_hash_str_find_ptr(&ce->function_table, "current", sizeof("current") - 1);
				ce->iterator_funcs_ptr->zf_next = gear_hash_str_find_ptr(&ce->function_table, "next", sizeof("next") - 1);
			}
		} else {
			ce->iterator_funcs_ptr = gear_arena_alloc(&CG(arena), sizeof(gear_class_iterator_funcs));
			memset(ce->iterator_funcs_ptr, 0, sizeof(gear_class_iterator_funcs));
		}
	}
	if (EXPECTED(!ce->__get)) {
		ce->__get = ce->parent->__get;
	}
	if (EXPECTED(!ce->__set)) {
		ce->__set = ce->parent->__set;
	}
	if (EXPECTED(!ce->__unset)) {
		ce->__unset = ce->parent->__unset;
	}
	if (EXPECTED(!ce->__isset)) {
		ce->__isset = ce->parent->__isset;
	}
	if (EXPECTED(!ce->__call)) {
		ce->__call = ce->parent->__call;
	}
	if (EXPECTED(!ce->__callstatic)) {
		ce->__callstatic = ce->parent->__callstatic;
	}
	if (EXPECTED(!ce->__tostring)) {
		ce->__tostring = ce->parent->__tostring;
	}
	if (EXPECTED(!ce->clone)) {
		ce->clone = ce->parent->clone;
	}
	if (EXPECTED(!ce->serialize)) {
		ce->serialize = ce->parent->serialize;
	}
	if (EXPECTED(!ce->unserialize)) {
		ce->unserialize = ce->parent->unserialize;
	}
	if (!ce->destructor) {
		ce->destructor = ce->parent->destructor;
	}
	if (EXPECTED(!ce->__debugInfo)) {
		ce->__debugInfo = ce->parent->__debugInfo;
	}

	if (ce->constructor) {
		if (ce->parent->constructor && UNEXPECTED(ce->parent->constructor->common.fn_flags & GEAR_ACC_FINAL)) {
			gear_error_noreturn(E_ERROR, "Cannot override final %s::%s() with %s::%s()",
				ZSTR_VAL(ce->parent->name), ZSTR_VAL(ce->parent->constructor->common.function_name),
				ZSTR_VAL(ce->name), ZSTR_VAL(ce->constructor->common.function_name));
		}
		return;
	}

	ce->constructor = ce->parent->constructor;
}
/* }}} */

char *gear_visibility_string(uint32_t fn_flags) /* {{{ */
{
	if (fn_flags & GEAR_ACC_PRIVATE) {
		return "private";
	}
	if (fn_flags & GEAR_ACC_PROTECTED) {
		return "protected";
	}
	if (fn_flags & GEAR_ACC_PUBLIC) {
		return "public";
	}
	return "";
}
/* }}} */

static gear_always_inline gear_bool gear_iterable_compatibility_check(gear_arg_info *arg_info) /* {{{ */
{
	if (GEAR_TYPE_CODE(arg_info->type) == IS_ARRAY) {
		return 1;
	}

	if (GEAR_TYPE_IS_CLASS(arg_info->type) && gear_string_equals_literal_ci(GEAR_TYPE_NAME(arg_info->type), "Traversable")) {
		return 1;
	}

	return 0;
}
/* }}} */

static int gear_do_perform_type_hint_check(const gear_function *fe, gear_arg_info *fe_arg_info, const gear_function *proto, gear_arg_info *proto_arg_info) /* {{{ */
{
	GEAR_ASSERT(GEAR_TYPE_IS_SET(fe_arg_info->type) && GEAR_TYPE_IS_SET(proto_arg_info->type));

	if (GEAR_TYPE_IS_CLASS(fe_arg_info->type) && GEAR_TYPE_IS_CLASS(proto_arg_info->type)) {
		gear_string *fe_class_name, *proto_class_name;
		const char *class_name;
		size_t class_name_len;

		fe_class_name = GEAR_TYPE_NAME(fe_arg_info->type);
		class_name = ZSTR_VAL(fe_class_name);
		class_name_len = ZSTR_LEN(fe_class_name);
		if (class_name_len == sizeof("parent")-1 && !strcasecmp(class_name, "parent") && fe->common.scope && fe->common.scope->parent) {
			fe_class_name = gear_string_copy(fe->common.scope->parent->name);
		} else if (class_name_len == sizeof("self")-1 && !strcasecmp(class_name, "self") && fe->common.scope) {
			fe_class_name = gear_string_copy(fe->common.scope->name);
		} else {
			gear_string_addref(fe_class_name);
		}

		proto_class_name = GEAR_TYPE_NAME(proto_arg_info->type);
		class_name = ZSTR_VAL(proto_class_name);
		class_name_len = ZSTR_LEN(proto_class_name);
		if (class_name_len == sizeof("parent")-1 && !strcasecmp(class_name, "parent") && proto->common.scope && proto->common.scope->parent) {
			proto_class_name = gear_string_copy(proto->common.scope->parent->name);
		} else if (class_name_len == sizeof("self")-1 && !strcasecmp(class_name, "self") && proto->common.scope) {
			proto_class_name = gear_string_copy(proto->common.scope->name);
		} else {
			gear_string_addref(proto_class_name);
		}

		if (fe_class_name != proto_class_name && strcasecmp(ZSTR_VAL(fe_class_name), ZSTR_VAL(proto_class_name)) != 0) {
			if (fe->common.type != GEAR_USER_FUNCTION) {
				gear_string_release(proto_class_name);
				gear_string_release(fe_class_name);
				return 0;
			} else {
				gear_class_entry *fe_ce, *proto_ce;

				fe_ce = gear_lookup_class(fe_class_name);
				proto_ce = gear_lookup_class(proto_class_name);

				/* Check for class alias */
				if (!fe_ce || !proto_ce ||
						fe_ce->type == GEAR_INTERNAL_CLASS ||
						proto_ce->type == GEAR_INTERNAL_CLASS ||
						fe_ce != proto_ce) {
					gear_string_release(proto_class_name);
					gear_string_release(fe_class_name);
					return 0;
				}
			}
		}
		gear_string_release(proto_class_name);
		gear_string_release(fe_class_name);
	} else if (GEAR_TYPE_CODE(fe_arg_info->type) != GEAR_TYPE_CODE(proto_arg_info->type)) {
		/* Incompatible built-in types */
		return 0;
	}

	return 1;
}
/* }}} */

static int gear_do_perform_arg_type_hint_check(const gear_function *fe, gear_arg_info *fe_arg_info, const gear_function *proto, gear_arg_info *proto_arg_info) /* {{{ */
{
	if (!GEAR_TYPE_IS_SET(fe_arg_info->type)) {
		/* Child with no type is always compatible */
		return 1;
	}

	if (!GEAR_TYPE_IS_SET(proto_arg_info->type)) {
		/* Child defines a type, but parent doesn't, violates LSP */
		return 0;
	}

	return gear_do_perform_type_hint_check(fe, fe_arg_info, proto, proto_arg_info);
}
/* }}} */

static gear_bool gear_do_perform_implementation_check(const gear_function *fe, const gear_function *proto) /* {{{ */
{
	uint32_t i, num_args;

	/* If it's a user function then arg_info == NULL means we don't have any parameters but
	 * we still need to do the arg number checks.  We are only willing to ignore this for internal
	 * functions because extensions don't always define arg_info.
	 */
	if (!proto || (!proto->common.arg_info && proto->common.type != GEAR_USER_FUNCTION)) {
		return 1;
	}

	/* Checks for constructors only if they are declared in an interface,
	 * or explicitly marked as abstract
	 */
	if ((fe->common.fn_flags & GEAR_ACC_CTOR)
		&& ((proto->common.scope->ce_flags & GEAR_ACC_INTERFACE) == 0
			&& (proto->common.fn_flags & GEAR_ACC_ABSTRACT) == 0)) {
		return 1;
	}

	/* If the prototype method is private do not enforce a signature */
	if (proto->common.fn_flags & GEAR_ACC_PRIVATE) {
		return 1;
	}

	/* check number of arguments */
	if (proto->common.required_num_args < fe->common.required_num_args
		|| proto->common.num_args > fe->common.num_args) {
		return 0;
	}

	/* by-ref constraints on return values are covariant */
	if ((proto->common.fn_flags & GEAR_ACC_RETURN_REFERENCE)
		&& !(fe->common.fn_flags & GEAR_ACC_RETURN_REFERENCE)) {
		return 0;
	}

	if ((proto->common.fn_flags & GEAR_ACC_VARIADIC)
		&& !(fe->common.fn_flags & GEAR_ACC_VARIADIC)) {
		return 0;
	}

	/* For variadic functions any additional (optional) arguments that were added must be
	 * checked against the signature of the variadic argument, so in this case we have to
	 * go through all the parameters of the function and not just those present in the
	 * prototype. */
	num_args = proto->common.num_args;
	if (proto->common.fn_flags & GEAR_ACC_VARIADIC) {
		num_args++;
        if (fe->common.num_args >= proto->common.num_args) {
			num_args = fe->common.num_args;
			if (fe->common.fn_flags & GEAR_ACC_VARIADIC) {
				num_args++;
			}
		}
	}

	for (i = 0; i < num_args; i++) {
		gear_arg_info *fe_arg_info = &fe->common.arg_info[i];

		gear_arg_info *proto_arg_info;
		if (i < proto->common.num_args) {
			proto_arg_info = &proto->common.arg_info[i];
		} else {
			proto_arg_info = &proto->common.arg_info[proto->common.num_args];
		}

		if (!gear_do_perform_arg_type_hint_check(fe, fe_arg_info, proto, proto_arg_info)) {
			switch (GEAR_TYPE_CODE(fe_arg_info->type)) {
				case IS_ITERABLE:
					if (!gear_iterable_compatibility_check(proto_arg_info)) {
						return 0;
					}
					break;

				default:
					return 0;
			}
		}

		if (GEAR_TYPE_IS_SET(proto_arg_info->type) && GEAR_TYPE_ALLOW_NULL(proto_arg_info->type) && !GEAR_TYPE_ALLOW_NULL(fe_arg_info->type)) {
			/* incompatible nullability */
			return 0;
		}

		/* by-ref constraints on arguments are invariant */
		if (fe_arg_info->pass_by_reference != proto_arg_info->pass_by_reference) {
			return 0;
		}
	}

	/* Check return type compatibility, but only if the prototype already specifies
	 * a return type. Adding a new return type is always valid. */
	if (proto->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
		/* Removing a return type is not valid. */
		if (!(fe->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE)) {
			return 0;
		}

		if (!gear_do_perform_type_hint_check(fe, fe->common.arg_info - 1, proto, proto->common.arg_info - 1)) {
			switch (GEAR_TYPE_CODE(proto->common.arg_info[-1].type)) {
				case IS_ITERABLE:
					if (!gear_iterable_compatibility_check(fe->common.arg_info - 1)) {
						return 0;
					}
					break;

				default:
					return 0;
			}
		}

		if (GEAR_TYPE_ALLOW_NULL(fe->common.arg_info[-1].type) && !GEAR_TYPE_ALLOW_NULL(proto->common.arg_info[-1].type)) {
			return 0;
		}
	}
	return 1;
}
/* }}} */

static GEAR_COLD void gear_append_type_hint(smart_str *str, const gear_function *fptr, gear_arg_info *arg_info, int return_hint) /* {{{ */
{

	if (GEAR_TYPE_IS_SET(arg_info->type) && GEAR_TYPE_ALLOW_NULL(arg_info->type)) {
		smart_str_appendc(str, '?');
	}

	if (GEAR_TYPE_IS_CLASS(arg_info->type)) {
		const char *class_name;
		size_t class_name_len;

		class_name = ZSTR_VAL(GEAR_TYPE_NAME(arg_info->type));
		class_name_len = ZSTR_LEN(GEAR_TYPE_NAME(arg_info->type));

		if (!strcasecmp(class_name, "self") && fptr->common.scope) {
			class_name = ZSTR_VAL(fptr->common.scope->name);
			class_name_len = ZSTR_LEN(fptr->common.scope->name);
		} else if (!strcasecmp(class_name, "parent") && fptr->common.scope && fptr->common.scope->parent) {
			class_name = ZSTR_VAL(fptr->common.scope->parent->name);
			class_name_len = ZSTR_LEN(fptr->common.scope->parent->name);
		}

		smart_str_appendl(str, class_name, class_name_len);
		if (!return_hint) {
			smart_str_appendc(str, ' ');
		}
	} else if (GEAR_TYPE_IS_CODE(arg_info->type)) {
		const char *type_name = gear_get_type_by_const(GEAR_TYPE_CODE(arg_info->type));
		smart_str_appends(str, type_name);
		if (!return_hint) {
			smart_str_appendc(str, ' ');
		}
	}
}
/* }}} */

static GEAR_COLD gear_string *gear_get_function_declaration(const gear_function *fptr) /* {{{ */
{
	smart_str str = {0};

	if (fptr->op_array.fn_flags & GEAR_ACC_RETURN_REFERENCE) {
		smart_str_appends(&str, "& ");
	}

	if (fptr->common.scope) {
		/* cut off on NULL byte ... class@anonymous */
		smart_str_appendl(&str, ZSTR_VAL(fptr->common.scope->name), strlen(ZSTR_VAL(fptr->common.scope->name)));
		smart_str_appends(&str, "::");
	}

	smart_str_append(&str, fptr->common.function_name);
	smart_str_appendc(&str, '(');

	if (fptr->common.arg_info) {
		uint32_t i, num_args, required;
		gear_arg_info *arg_info = fptr->common.arg_info;

		required = fptr->common.required_num_args;
		num_args = fptr->common.num_args;
		if (fptr->common.fn_flags & GEAR_ACC_VARIADIC) {
			num_args++;
		}
		for (i = 0; i < num_args;) {
			gear_append_type_hint(&str, fptr, arg_info, 0);

			if (arg_info->pass_by_reference) {
				smart_str_appendc(&str, '&');
			}

			if (arg_info->is_variadic) {
				smart_str_appends(&str, "...");
			}

			smart_str_appendc(&str, '$');

			if (arg_info->name) {
				if (fptr->type == GEAR_INTERNAL_FUNCTION) {
					smart_str_appends(&str, ((gear_internal_arg_info*)arg_info)->name);
				} else {
					smart_str_appendl(&str, ZSTR_VAL(arg_info->name), ZSTR_LEN(arg_info->name));
				}
			} else {
				smart_str_appends(&str, "param");
				smart_str_append_unsigned(&str, i);
			}

			if (i >= required && !arg_info->is_variadic) {
				smart_str_appends(&str, " = ");
				if (fptr->type == GEAR_USER_FUNCTION) {
					gear_op *precv = NULL;
					{
						uint32_t idx  = i;
						gear_op *op = fptr->op_array.opcodes;
						gear_op *end = op + fptr->op_array.last;

						++idx;
						while (op < end) {
							if ((op->opcode == GEAR_RECV || op->opcode == GEAR_RECV_INIT)
									&& op->op1.num == (gear_ulong)idx)
							{
								precv = op;
							}
							++op;
						}
					}
					if (precv && precv->opcode == GEAR_RECV_INIT && precv->op2_type != IS_UNUSED) {
						zval *zv = RT_CONSTANT(precv, precv->op2);

						if (Z_TYPE_P(zv) == IS_FALSE) {
							smart_str_appends(&str, "false");
						} else if (Z_TYPE_P(zv) == IS_TRUE) {
							smart_str_appends(&str, "true");
						} else if (Z_TYPE_P(zv) == IS_NULL) {
							smart_str_appends(&str, "NULL");
						} else if (Z_TYPE_P(zv) == IS_STRING) {
							smart_str_appendc(&str, '\'');
							smart_str_appendl(&str, Z_STRVAL_P(zv), MIN(Z_STRLEN_P(zv), 10));
							if (Z_STRLEN_P(zv) > 10) {
								smart_str_appends(&str, "...");
							}
							smart_str_appendc(&str, '\'');
						} else if (Z_TYPE_P(zv) == IS_ARRAY) {
							smart_str_appends(&str, "Array");
						} else if (Z_TYPE_P(zv) == IS_CONSTANT_AST) {
							gear_ast *ast = Z_ASTVAL_P(zv);
							if (ast->kind == GEAR_AST_CONSTANT) {
								smart_str_append(&str, gear_ast_get_constant_name(ast));
							} else {
								smart_str_appends(&str, "<expression>");
							}
						} else {
							gear_string *tmp_zv_str;
							gear_string *zv_str = zval_get_tmp_string(zv, &tmp_zv_str);
							smart_str_append(&str, zv_str);
							gear_tmp_string_release(tmp_zv_str);
						}
					}
				} else {
					smart_str_appends(&str, "NULL");
				}
			}

			if (++i < num_args) {
				smart_str_appends(&str, ", ");
			}
			arg_info++;
		}
	}

	smart_str_appendc(&str, ')');

	if (fptr->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
		smart_str_appends(&str, ": ");
		gear_append_type_hint(&str, fptr, fptr->common.arg_info - 1, 1);
	}
	smart_str_0(&str);

	return str.s;
}
/* }}} */

static void do_inheritance_check_on_method(gear_function *child, gear_function *parent) /* {{{ */
{
	uint32_t child_flags;
	uint32_t parent_flags = parent->common.fn_flags;

	if (UNEXPECTED(parent_flags & GEAR_ACC_FINAL)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot override final method %s::%s()", GEAR_FN_SCOPE_NAME(parent), ZSTR_VAL(child->common.function_name));
	}

	child_flags	= child->common.fn_flags;
	/* You cannot change from static to non static and vice versa.
	 */
	if (UNEXPECTED((child_flags & GEAR_ACC_STATIC) != (parent_flags & GEAR_ACC_STATIC))) {
		if (child_flags & GEAR_ACC_STATIC) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot make non static method %s::%s() static in class %s", GEAR_FN_SCOPE_NAME(parent), ZSTR_VAL(child->common.function_name), GEAR_FN_SCOPE_NAME(child));
		} else {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot make static method %s::%s() non static in class %s", GEAR_FN_SCOPE_NAME(parent), ZSTR_VAL(child->common.function_name), GEAR_FN_SCOPE_NAME(child));
		}
	}

	/* Disallow making an inherited method abstract. */
	if (UNEXPECTED((child_flags & GEAR_ACC_ABSTRACT) > (parent_flags & GEAR_ACC_ABSTRACT))) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot make non abstract method %s::%s() abstract in class %s", GEAR_FN_SCOPE_NAME(parent), ZSTR_VAL(child->common.function_name), GEAR_FN_SCOPE_NAME(child));
	}

	/* Prevent derived classes from restricting access that was available in parent classes (except deriving from non-abstract ctors) */
	if (UNEXPECTED((!(child_flags & GEAR_ACC_CTOR) || (parent_flags & (GEAR_ACC_ABSTRACT | GEAR_ACC_IMPLEMENTED_ABSTRACT))) &&
		(child_flags & GEAR_ACC_PPP_MASK) > (parent_flags & GEAR_ACC_PPP_MASK))) {
		gear_error_noreturn(E_COMPILE_ERROR, "Access level to %s::%s() must be %s (as in class %s)%s", GEAR_FN_SCOPE_NAME(child), ZSTR_VAL(child->common.function_name), gear_visibility_string(parent_flags), GEAR_FN_SCOPE_NAME(parent), (parent_flags&GEAR_ACC_PUBLIC) ? "" : " or weaker");
	}

	if ((child_flags & GEAR_ACC_PRIVATE) < (parent_flags & (GEAR_ACC_PRIVATE|GEAR_ACC_CHANGED))) {
		child->common.fn_flags |= GEAR_ACC_CHANGED;
	}

	if (parent_flags & GEAR_ACC_PRIVATE) {
		child->common.prototype = NULL;
	} else if (parent_flags & GEAR_ACC_ABSTRACT) {
		child->common.fn_flags |= GEAR_ACC_IMPLEMENTED_ABSTRACT;
		child->common.prototype = parent;
	} else if (!(parent->common.fn_flags & GEAR_ACC_CTOR)) {
		child->common.prototype = parent->common.prototype ? parent->common.prototype : parent;
	} else if (parent->common.prototype && (parent->common.prototype->common.scope->ce_flags & GEAR_ACC_INTERFACE)) {
		/* ctors only have a prototype if it comes from an interface */
		child->common.prototype = parent->common.prototype ? parent->common.prototype : parent;
		/* and if that is the case, we want to check inheritance against it */
		parent = child->common.prototype;
	}

	if (UNEXPECTED(!gear_do_perform_implementation_check(child, parent))) {
		int error_level;
		const char *error_verb;
		gear_string *method_prototype = gear_get_function_declaration(parent);
		gear_string *child_prototype = gear_get_function_declaration(child);

		if (child->common.prototype && (
			child->common.prototype->common.fn_flags & GEAR_ACC_ABSTRACT
		)) {
			error_level = E_COMPILE_ERROR;
			error_verb = "must";
		} else if ((parent->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) &&
                   (!(child->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) ||
		            !gear_do_perform_type_hint_check(child, child->common.arg_info - 1, parent, parent->common.arg_info - 1) ||
		            (GEAR_TYPE_ALLOW_NULL(child->common.arg_info[-1].type) && !GEAR_TYPE_ALLOW_NULL(parent->common.arg_info[-1].type)))) {
			error_level = E_COMPILE_ERROR;
			error_verb = "must";
		} else {
			error_level = E_WARNING;
			error_verb = "should";
		}
		gear_error(error_level, "Declaration of %s %s be compatible with %s", ZSTR_VAL(child_prototype), error_verb, ZSTR_VAL(method_prototype));
		gear_string_efree(child_prototype);
		gear_string_efree(method_prototype);
	}
}
/* }}} */

static gear_function *do_inherit_method(gear_string *key, gear_function *parent, gear_class_entry *ce) /* {{{ */
{
	zval *child = gear_hash_find_ex(&ce->function_table, key, 1);

	if (child) {
		gear_function *func = (gear_function*)Z_PTR_P(child);
		gear_function *orig_prototype = func->common.prototype;

		do_inheritance_check_on_method(func, parent);
		if (func->common.prototype != orig_prototype &&
		    func->type == GEAR_USER_FUNCTION &&
		    func->common.scope != ce &&
		    !func->op_array.static_variables) {
			/* Lazy duplication */
			gear_function *new_function = gear_arena_alloc(&CG(arena), sizeof(gear_op_array));
			memcpy(new_function, func, sizeof(gear_op_array));
			Z_PTR_P(child) = new_function;
			func->common.prototype = orig_prototype;
		}
		return NULL;
	}

	if (parent->common.fn_flags & (GEAR_ACC_ABSTRACT)) {
		ce->ce_flags |= GEAR_ACC_IMPLICIT_ABSTRACT_CLASS;
	}

	return gear_duplicate_function(parent, ce);
}
/* }}} */

static void do_inherit_property(gear_property_info *parent_info, gear_string *key, gear_class_entry *ce) /* {{{ */
{
	zval *child = gear_hash_find_ex(&ce->properties_info, key, 1);
	gear_property_info *child_info;

	if (UNEXPECTED(child)) {
		child_info = Z_PTR_P(child);
		if (UNEXPECTED(parent_info->flags & (GEAR_ACC_PRIVATE|GEAR_ACC_SHADOW))) {
			child_info->flags |= GEAR_ACC_CHANGED;
		} else {
			if (UNEXPECTED((parent_info->flags & GEAR_ACC_STATIC) != (child_info->flags & GEAR_ACC_STATIC))) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot redeclare %s%s::$%s as %s%s::$%s",
					(parent_info->flags & GEAR_ACC_STATIC) ? "static " : "non static ", ZSTR_VAL(ce->parent->name), ZSTR_VAL(key),
					(child_info->flags & GEAR_ACC_STATIC) ? "static " : "non static ", ZSTR_VAL(ce->name), ZSTR_VAL(key));
			}

			if (parent_info->flags & GEAR_ACC_CHANGED) {
				child_info->flags |= GEAR_ACC_CHANGED;
			}

			if (UNEXPECTED((child_info->flags & GEAR_ACC_PPP_MASK) > (parent_info->flags & GEAR_ACC_PPP_MASK))) {
				gear_error_noreturn(E_COMPILE_ERROR, "Access level to %s::$%s must be %s (as in class %s)%s", ZSTR_VAL(ce->name), ZSTR_VAL(key), gear_visibility_string(parent_info->flags), ZSTR_VAL(ce->parent->name), (parent_info->flags&GEAR_ACC_PUBLIC) ? "" : " or weaker");
			} else if ((child_info->flags & GEAR_ACC_STATIC) == 0) {
				int parent_num = OBJ_PROP_TO_NUM(parent_info->offset);
				int child_num = OBJ_PROP_TO_NUM(child_info->offset);

				/* Don't keep default properties in GC (they may be freed by opcache) */
				zval_ptr_dtor_nogc(&(ce->default_properties_table[parent_num]));
				ce->default_properties_table[parent_num] = ce->default_properties_table[child_num];
				ZVAL_UNDEF(&ce->default_properties_table[child_num]);
				child_info->offset = parent_info->offset;
			}
		}
	} else {
		if (UNEXPECTED(parent_info->flags & GEAR_ACC_PRIVATE)) {
			if (UNEXPECTED(ce->type & GEAR_INTERNAL_CLASS)) {
				child_info = gear_duplicate_property_info_internal(parent_info);
			} else {
				child_info = gear_duplicate_property_info(parent_info);
			}
			child_info->flags &= ~GEAR_ACC_PRIVATE; /* it's not private anymore */
			child_info->flags |= GEAR_ACC_SHADOW; /* but it's a shadow of private */
		} else {
			if (UNEXPECTED(ce->type & GEAR_INTERNAL_CLASS)) {
				child_info = gear_duplicate_property_info_internal(parent_info);
			} else {
				child_info = parent_info;
			}
		}
		_gear_hash_append_ptr(&ce->properties_info, key, child_info);
	}
}
/* }}} */

static inline void do_implement_interface(gear_class_entry *ce, gear_class_entry *iface) /* {{{ */
{
	if (!(ce->ce_flags & GEAR_ACC_INTERFACE) && iface->interface_gets_implemented && iface->interface_gets_implemented(iface, ce) == FAILURE) {
		gear_error_noreturn(E_CORE_ERROR, "Class %s could not implement interface %s", ZSTR_VAL(ce->name), ZSTR_VAL(iface->name));
	}
	if (UNEXPECTED(ce == iface)) {
		gear_error_noreturn(E_ERROR, "Interface %s cannot implement itself", ZSTR_VAL(ce->name));
	}
}
/* }}} */

GEAR_API void gear_do_inherit_interfaces(gear_class_entry *ce, const gear_class_entry *iface) /* {{{ */
{
	/* expects interface to be contained in ce's interface list already */
	uint32_t i, ce_num, if_num = iface->num_interfaces;
	gear_class_entry *entry;

	if (if_num==0) {
		return;
	}
	ce_num = ce->num_interfaces;

	if (ce->type == GEAR_INTERNAL_CLASS) {
		ce->interfaces = (gear_class_entry **) realloc(ce->interfaces, sizeof(gear_class_entry *) * (ce_num + if_num));
	} else {
		ce->interfaces = (gear_class_entry **) erealloc(ce->interfaces, sizeof(gear_class_entry *) * (ce_num + if_num));
	}

	/* Inherit the interfaces, only if they're not already inherited by the class */
	while (if_num--) {
		entry = iface->interfaces[if_num];
		for (i = 0; i < ce_num; i++) {
			if (ce->interfaces[i] == entry) {
				break;
			}
		}
		if (i == ce_num) {
			ce->interfaces[ce->num_interfaces++] = entry;
		}
	}

	/* and now call the implementing handlers */
	while (ce_num < ce->num_interfaces) {
		do_implement_interface(ce, ce->interfaces[ce_num++]);
	}
}
/* }}} */

static void do_inherit_class_constant(gear_string *name, gear_class_constant *parent_const, gear_class_entry *ce) /* {{{ */
{
	zval *zv = gear_hash_find_ex(&ce->constants_table, name, 1);
	gear_class_constant *c;

	if (zv != NULL) {
		c = (gear_class_constant*)Z_PTR_P(zv);
		if (UNEXPECTED((Z_ACCESS_FLAGS(c->value) & GEAR_ACC_PPP_MASK) > (Z_ACCESS_FLAGS(parent_const->value) & GEAR_ACC_PPP_MASK))) {
			gear_error_noreturn(E_COMPILE_ERROR, "Access level to %s::%s must be %s (as in class %s)%s",
				ZSTR_VAL(ce->name), ZSTR_VAL(name), gear_visibility_string(Z_ACCESS_FLAGS(parent_const->value)), ZSTR_VAL(ce->parent->name), (Z_ACCESS_FLAGS(parent_const->value) & GEAR_ACC_PUBLIC) ? "" : " or weaker");
		}
	} else if (!(Z_ACCESS_FLAGS(parent_const->value) & GEAR_ACC_PRIVATE)) {
		if (Z_TYPE(parent_const->value) == IS_CONSTANT_AST) {
			ce->ce_flags &= ~GEAR_ACC_CONSTANTS_UPDATED;
		}
		if (ce->type & GEAR_INTERNAL_CLASS) {
			c = pemalloc(sizeof(gear_class_constant), 1);
			memcpy(c, parent_const, sizeof(gear_class_constant));
			parent_const = c;
		}
		_gear_hash_append_ptr(&ce->constants_table, name, parent_const);
	}
}
/* }}} */

GEAR_API void gear_do_inheritance(gear_class_entry *ce, gear_class_entry *parent_ce) /* {{{ */
{
	gear_property_info *property_info;
	gear_function *func;
	gear_string *key;

	if (UNEXPECTED(ce->ce_flags & GEAR_ACC_INTERFACE)) {
		/* Interface can only inherit other interfaces */
		if (UNEXPECTED(!(parent_ce->ce_flags & GEAR_ACC_INTERFACE))) {
			gear_error_noreturn(E_COMPILE_ERROR, "Interface %s may not inherit from class (%s)", ZSTR_VAL(ce->name), ZSTR_VAL(parent_ce->name));
		}
	} else if (UNEXPECTED(parent_ce->ce_flags & (GEAR_ACC_INTERFACE|GEAR_ACC_TRAIT|GEAR_ACC_FINAL))) {
		/* Class declaration must not extend traits or interfaces */
		if (parent_ce->ce_flags & GEAR_ACC_INTERFACE) {
			gear_error_noreturn(E_COMPILE_ERROR, "Class %s cannot extend from interface %s", ZSTR_VAL(ce->name), ZSTR_VAL(parent_ce->name));
		} else if (parent_ce->ce_flags & GEAR_ACC_TRAIT) {
			gear_error_noreturn(E_COMPILE_ERROR, "Class %s cannot extend from trait %s", ZSTR_VAL(ce->name), ZSTR_VAL(parent_ce->name));
		}

		/* Class must not extend a final class */
		if (parent_ce->ce_flags & GEAR_ACC_FINAL) {
			gear_error_noreturn(E_COMPILE_ERROR, "Class %s may not inherit from final class (%s)", ZSTR_VAL(ce->name), ZSTR_VAL(parent_ce->name));
		}
	}

	ce->parent = parent_ce;

	/* Inherit interfaces */
	gear_do_inherit_interfaces(ce, parent_ce);

	/* Inherit properties */
	if (parent_ce->default_properties_count) {
		zval *src, *dst, *end;

		if (ce->default_properties_count) {
			zval *table = pemalloc(sizeof(zval) * (ce->default_properties_count + parent_ce->default_properties_count), ce->type == GEAR_INTERNAL_CLASS);
			src = ce->default_properties_table + ce->default_properties_count;
			end = table + parent_ce->default_properties_count;
			dst = end + ce->default_properties_count;
			ce->default_properties_table = table;
			do {
				dst--;
				src--;
				ZVAL_COPY_VALUE(dst, src);
			} while (dst != end);
			pefree(src, ce->type == GEAR_INTERNAL_CLASS);
			end = ce->default_properties_table;
		} else {
			end = pemalloc(sizeof(zval) * parent_ce->default_properties_count, ce->type == GEAR_INTERNAL_CLASS);
			dst = end + parent_ce->default_properties_count;
			ce->default_properties_table = end;
		}
		src = parent_ce->default_properties_table + parent_ce->default_properties_count;
		if (UNEXPECTED(parent_ce->type != ce->type)) {
			/* User class extends internal */
			do {
				dst--;
				src--;
				ZVAL_COPY_OR_DUP(dst, src);
				if (Z_OPT_TYPE_P(dst) == IS_CONSTANT_AST) {
					ce->ce_flags &= ~GEAR_ACC_CONSTANTS_UPDATED;
				}
				continue;
			} while (dst != end);
		} else {
			do {
				dst--;
				src--;
				ZVAL_COPY(dst, src);
				if (Z_OPT_TYPE_P(dst) == IS_CONSTANT_AST) {
					ce->ce_flags &= ~GEAR_ACC_CONSTANTS_UPDATED;
				}
				continue;
			} while (dst != end);
		}
		ce->default_properties_count += parent_ce->default_properties_count;
	}

	if (parent_ce->default_static_members_count) {
		zval *src, *dst, *end;

		if (ce->default_static_members_count) {
			zval *table = pemalloc(sizeof(zval) * (ce->default_static_members_count + parent_ce->default_static_members_count), ce->type == GEAR_INTERNAL_CLASS);
			src = ce->default_static_members_table + ce->default_static_members_count;
			end = table + parent_ce->default_static_members_count;
			dst = end + ce->default_static_members_count;
			ce->default_static_members_table = table;
			do {
				dst--;
				src--;
				ZVAL_COPY_VALUE(dst, src);
			} while (dst != end);
			pefree(src, ce->type == GEAR_INTERNAL_CLASS);
			end = ce->default_static_members_table;
		} else {
			end = pemalloc(sizeof(zval) * parent_ce->default_static_members_count, ce->type == GEAR_INTERNAL_CLASS);
			dst = end + parent_ce->default_static_members_count;
			ce->default_static_members_table = end;
		}
		if (UNEXPECTED(parent_ce->type != ce->type)) {
			/* User class extends internal */
			if (CE_STATIC_MEMBERS(parent_ce) == NULL) {
				gear_class_init_statics(parent_ce);
			}
			if (UNEXPECTED(gear_update_class_constants(parent_ce) != SUCCESS)) {
				GEAR_ASSERT(0);
			}
			src = CE_STATIC_MEMBERS(parent_ce) + parent_ce->default_static_members_count;
			do {
				dst--;
				src--;
				if (Z_TYPE_P(src) == IS_INDIRECT) {
					ZVAL_INDIRECT(dst, Z_INDIRECT_P(src));
				} else {
					ZVAL_INDIRECT(dst, src);
				}
			} while (dst != end);
		} else if (ce->type == GEAR_USER_CLASS) {
			src = parent_ce->default_static_members_table + parent_ce->default_static_members_count;
			do {
				dst--;
				src--;
				if (Z_TYPE_P(src) == IS_INDIRECT) {
					ZVAL_INDIRECT(dst, Z_INDIRECT_P(src));
				} else {
					ZVAL_INDIRECT(dst, src);
				}
				if (Z_TYPE_P(Z_INDIRECT_P(dst)) == IS_CONSTANT_AST) {
					ce->ce_flags &= ~GEAR_ACC_CONSTANTS_UPDATED;
				}
			} while (dst != end);
		} else {
			src = parent_ce->default_static_members_table + parent_ce->default_static_members_count;
			do {
				dst--;
				src--;
				if (Z_TYPE_P(src) == IS_INDIRECT) {
					ZVAL_INDIRECT(dst, Z_INDIRECT_P(src));
				} else {
					ZVAL_INDIRECT(dst, src);
				}
			} while (dst != end);
		}
		ce->default_static_members_count += parent_ce->default_static_members_count;
		if (ce->type == GEAR_USER_CLASS) {
			ce->static_members_table = ce->default_static_members_table;
		}
	}

	GEAR_HASH_FOREACH_PTR(&ce->properties_info, property_info) {
		if (property_info->ce == ce) {
			if (property_info->flags & GEAR_ACC_STATIC) {
				property_info->offset += parent_ce->default_static_members_count;
			} else {
				property_info->offset += parent_ce->default_properties_count * sizeof(zval);
			}
		}
	} GEAR_HASH_FOREACH_END();

	if (gear_hash_num_elements(&parent_ce->properties_info)) {
		gear_hash_extend(&ce->properties_info,
			gear_hash_num_elements(&ce->properties_info) +
			gear_hash_num_elements(&parent_ce->properties_info), 0);

		GEAR_HASH_FOREACH_STR_KEY_PTR(&parent_ce->properties_info, key, property_info) {
			do_inherit_property(property_info, key, ce);
		} GEAR_HASH_FOREACH_END();
	}

	if (gear_hash_num_elements(&parent_ce->constants_table)) {
		gear_class_constant *c;

		gear_hash_extend(&ce->constants_table,
			gear_hash_num_elements(&ce->constants_table) +
			gear_hash_num_elements(&parent_ce->constants_table), 0);

		GEAR_HASH_FOREACH_STR_KEY_PTR(&parent_ce->constants_table, key, c) {
			do_inherit_class_constant(key, c, ce);
		} GEAR_HASH_FOREACH_END();
	}

	if (gear_hash_num_elements(&parent_ce->function_table)) {
		gear_hash_extend(&ce->function_table,
			gear_hash_num_elements(&ce->function_table) +
			gear_hash_num_elements(&parent_ce->function_table), 0);

		GEAR_HASH_FOREACH_STR_KEY_PTR(&parent_ce->function_table, key, func) {
			gear_function *new_func = do_inherit_method(key, func, ce);

			if (new_func) {
				_gear_hash_append_ptr(&ce->function_table, key, new_func);
			}
		} GEAR_HASH_FOREACH_END();
	}

	do_inherit_parent_constructor(ce);

	if (ce->ce_flags & GEAR_ACC_IMPLICIT_ABSTRACT_CLASS && ce->type == GEAR_INTERNAL_CLASS) {
		ce->ce_flags |= GEAR_ACC_EXPLICIT_ABSTRACT_CLASS;
	} else if (!(ce->ce_flags & (GEAR_ACC_IMPLEMENT_INTERFACES|GEAR_ACC_IMPLEMENT_TRAITS))) {
		/* The verification will be done in runtime by GEAR_VERIFY_ABSTRACT_CLASS */
		gear_verify_abstract_class(ce);
	}
	ce->ce_flags |= parent_ce->ce_flags & (GEAR_HAS_STATIC_IN_METHODS | GEAR_ACC_USE_GUARDS);
}
/* }}} */

static gear_bool do_inherit_constant_check(HashTable *child_constants_table, gear_class_constant *parent_constant, gear_string *name, const gear_class_entry *iface) /* {{{ */
{
	zval *zv = gear_hash_find_ex(child_constants_table, name, 1);
	gear_class_constant *old_constant;

	if (zv != NULL) {
		old_constant = (gear_class_constant*)Z_PTR_P(zv);
		if (old_constant->ce != parent_constant->ce) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot inherit previously-inherited or override constant %s from interface %s", ZSTR_VAL(name), ZSTR_VAL(iface->name));
		}
		return 0;
	}
	return 1;
}
/* }}} */

static void do_inherit_iface_constant(gear_string *name, gear_class_constant *c, gear_class_entry *ce, gear_class_entry *iface) /* {{{ */
{
	if (do_inherit_constant_check(&ce->constants_table, c, name, iface)) {
		gear_class_constant *ct;
		if (Z_TYPE(c->value) == IS_CONSTANT_AST) {
			ce->ce_flags &= ~GEAR_ACC_CONSTANTS_UPDATED;
		}
		if (ce->type & GEAR_INTERNAL_CLASS) {
			ct = pemalloc(sizeof(gear_class_constant), 1);
			memcpy(ct, c, sizeof(gear_class_constant));
			c = ct;
		}
		gear_hash_update_ptr(&ce->constants_table, name, c);
	}
}
/* }}} */

GEAR_API void gear_do_implement_interface(gear_class_entry *ce, gear_class_entry *iface) /* {{{ */
{
	uint32_t i, ignore = 0;
	uint32_t current_iface_num = ce->num_interfaces;
	uint32_t parent_iface_num  = ce->parent ? ce->parent->num_interfaces : 0;
	gear_function *func;
	gear_string *key;
	gear_class_constant *c;

	for (i = 0; i < ce->num_interfaces; i++) {
		if (ce->interfaces[i] == NULL) {
			memmove(ce->interfaces + i, ce->interfaces + i + 1, sizeof(gear_class_entry*) * (--ce->num_interfaces - i));
			i--;
		} else if (ce->interfaces[i] == iface) {
			if (EXPECTED(i < parent_iface_num)) {
				ignore = 1;
			} else {
				gear_error_noreturn(E_COMPILE_ERROR, "Class %s cannot implement previously implemented interface %s", ZSTR_VAL(ce->name), ZSTR_VAL(iface->name));
			}
		}
	}
	if (ignore) {
		/* Check for attempt to redeclare interface constants */
		GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->constants_table, key, c) {
			do_inherit_constant_check(&iface->constants_table, c, key, iface);
		} GEAR_HASH_FOREACH_END();
	} else {
		if (ce->num_interfaces >= current_iface_num) {
			if (ce->type == GEAR_INTERNAL_CLASS) {
				ce->interfaces = (gear_class_entry **) realloc(ce->interfaces, sizeof(gear_class_entry *) * (++current_iface_num));
			} else {
				ce->interfaces = (gear_class_entry **) erealloc(ce->interfaces, sizeof(gear_class_entry *) * (++current_iface_num));
			}
		}
		ce->interfaces[ce->num_interfaces++] = iface;

		GEAR_HASH_FOREACH_STR_KEY_PTR(&iface->constants_table, key, c) {
			do_inherit_iface_constant(key, c, ce, iface);
		} GEAR_HASH_FOREACH_END();

		GEAR_HASH_FOREACH_STR_KEY_PTR(&iface->function_table, key, func) {
			gear_function *new_func = do_inherit_method(key, func, ce);

			if (new_func) {
				gear_hash_add_new_ptr(&ce->function_table, key, new_func);
			}
		} GEAR_HASH_FOREACH_END();

		do_implement_interface(ce, iface);
		gear_do_inherit_interfaces(ce, iface);
	}
}
/* }}} */

GEAR_API void gear_do_implement_trait(gear_class_entry *ce, gear_class_entry *trait) /* {{{ */
{
	uint32_t i, ignore = 0;
	uint32_t current_trait_num = ce->num_traits;
	uint32_t parent_trait_num  = ce->parent ? ce->parent->num_traits : 0;

	for (i = 0; i < ce->num_traits; i++) {
		if (ce->traits[i] == NULL) {
			memmove(ce->traits + i, ce->traits + i + 1, sizeof(gear_class_entry*) * (--ce->num_traits - i));
			i--;
		} else if (ce->traits[i] == trait) {
			if (i < parent_trait_num) {
				ignore = 1;
			}
		}
	}
	if (!ignore) {
		if (ce->num_traits >= current_trait_num) {
			if (ce->type == GEAR_INTERNAL_CLASS) {
				ce->traits = (gear_class_entry **) realloc(ce->traits, sizeof(gear_class_entry *) * (++current_trait_num));
			} else {
				ce->traits = (gear_class_entry **) erealloc(ce->traits, sizeof(gear_class_entry *) * (++current_trait_num));
			}
		}
		ce->traits[ce->num_traits++] = trait;
	}
}
/* }}} */

static gear_bool gear_traits_method_compatibility_check(gear_function *fn, gear_function *other_fn) /* {{{ */
{
	uint32_t    fn_flags = fn->common.scope->ce_flags;
	uint32_t other_flags = other_fn->common.scope->ce_flags;

	return gear_do_perform_implementation_check(fn, other_fn)
		&& ((fn_flags & (GEAR_ACC_FINAL|GEAR_ACC_STATIC)) ==
		    (other_flags & (GEAR_ACC_FINAL|GEAR_ACC_STATIC))); /* equal final and static qualifier */
}
/* }}} */

static void gear_add_magic_methods(gear_class_entry* ce, gear_string* mname, gear_function* fe) /* {{{ */
{
	if (ZSTR_LEN(ce->name) != ZSTR_LEN(mname) && (ZSTR_VAL(mname)[0] != '_' || ZSTR_VAL(mname)[1] != '_')) {
		/* pass */
	} else if (gear_string_equals_literal(mname, GEAR_CLONE_FUNC_NAME)) {
		ce->clone = fe;
	} else if (gear_string_equals_literal(mname, GEAR_CONSTRUCTOR_FUNC_NAME)) {
		if (ce->constructor && (!ce->parent || ce->constructor != ce->parent->constructor)) {
			gear_error_noreturn(E_COMPILE_ERROR, "%s has colliding constructor definitions coming from traits", ZSTR_VAL(ce->name));
		}
		ce->constructor = fe; fe->common.fn_flags |= GEAR_ACC_CTOR;
	} else if (gear_string_equals_literal(mname, GEAR_DESTRUCTOR_FUNC_NAME)) {
		ce->destructor = fe; fe->common.fn_flags |= GEAR_ACC_DTOR;
	} else if (gear_string_equals_literal(mname, GEAR_GET_FUNC_NAME)) {
		ce->__get = fe;
		ce->ce_flags |= GEAR_ACC_USE_GUARDS;
	} else if (gear_string_equals_literal(mname, GEAR_SET_FUNC_NAME)) {
		ce->__set = fe;
		ce->ce_flags |= GEAR_ACC_USE_GUARDS;
	} else if (gear_string_equals_literal(mname, GEAR_CALL_FUNC_NAME)) {
		ce->__call = fe;
	} else if (gear_string_equals_literal(mname, GEAR_UNSET_FUNC_NAME)) {
		ce->__unset = fe;
		ce->ce_flags |= GEAR_ACC_USE_GUARDS;
	} else if (gear_string_equals_literal(mname, GEAR_ISSET_FUNC_NAME)) {
		ce->__isset = fe;
		ce->ce_flags |= GEAR_ACC_USE_GUARDS;
	} else if (gear_string_equals_literal(mname, GEAR_CALLSTATIC_FUNC_NAME)) {
		ce->__callstatic = fe;
	} else if (gear_string_equals_literal(mname, GEAR_TOSTRING_FUNC_NAME)) {
		ce->__tostring = fe;
	} else if (gear_string_equals_literal(mname, GEAR_DEBUGINFO_FUNC_NAME)) {
		ce->__debugInfo = fe;
	} else if (ZSTR_LEN(ce->name) == ZSTR_LEN(mname)) {
		gear_string *lowercase_name = gear_string_tolower(ce->name);
		lowercase_name = gear_new_interned_string(lowercase_name);
		if (!memcmp(ZSTR_VAL(mname), ZSTR_VAL(lowercase_name), ZSTR_LEN(mname))) {
			if (ce->constructor  && (!ce->parent || ce->constructor != ce->parent->constructor)) {
				gear_error_noreturn(E_COMPILE_ERROR, "%s has colliding constructor definitions coming from traits", ZSTR_VAL(ce->name));
			}
			ce->constructor = fe;
			fe->common.fn_flags |= GEAR_ACC_CTOR;
		}
		gear_string_release_ex(lowercase_name, 0);
	}
}
/* }}} */

static void gear_add_trait_method(gear_class_entry *ce, const char *name, gear_string *key, gear_function *fn, HashTable **overriden) /* {{{ */
{
	gear_function *existing_fn = NULL;
	gear_function *new_fn;

	if ((existing_fn = gear_hash_find_ptr(&ce->function_table, key)) != NULL) {
		/* if it is the same function with the same visibility and has not been assigned a class scope yet, regardless
		 * of where it is coming from there is no conflict and we do not need to add it again */
		if (existing_fn->op_array.opcodes == fn->op_array.opcodes &&
			(existing_fn->common.fn_flags & GEAR_ACC_PPP_MASK) == (fn->common.fn_flags & GEAR_ACC_PPP_MASK) &&
			(existing_fn->common.scope->ce_flags & GEAR_ACC_TRAIT) == GEAR_ACC_TRAIT) {
			return;
		}

		if (existing_fn->common.scope == ce) {
			/* members from the current class override trait methods */
			/* use temporary *overriden HashTable to detect hidden conflict */
			if (*overriden) {
				if ((existing_fn = gear_hash_find_ptr(*overriden, key)) != NULL) {
					if (existing_fn->common.fn_flags & GEAR_ACC_ABSTRACT) {
						/* Make sure the trait method is compatible with previosly declared abstract method */
						if (UNEXPECTED(!gear_traits_method_compatibility_check(fn, existing_fn))) {
							gear_error_noreturn(E_COMPILE_ERROR, "Declaration of %s must be compatible with %s",
								ZSTR_VAL(gear_get_function_declaration(fn)),
								ZSTR_VAL(gear_get_function_declaration(existing_fn)));
						}
					}
					if (fn->common.fn_flags & GEAR_ACC_ABSTRACT) {
						/* Make sure the abstract declaration is compatible with previous declaration */
						if (UNEXPECTED(!gear_traits_method_compatibility_check(existing_fn, fn))) {
							gear_error_noreturn(E_COMPILE_ERROR, "Declaration of %s must be compatible with %s",
								ZSTR_VAL(gear_get_function_declaration(existing_fn)),
								ZSTR_VAL(gear_get_function_declaration(fn)));
						}
						return;
					}
				}
			} else {
				ALLOC_HASHTABLE(*overriden);
				gear_hash_init_ex(*overriden, 8, NULL, overriden_ptr_dtor, 0, 0);
			}
			gear_hash_update_mem(*overriden, key, fn, sizeof(gear_function));
			return;
		} else if (existing_fn->common.fn_flags & GEAR_ACC_ABSTRACT &&
				(existing_fn->common.scope->ce_flags & GEAR_ACC_INTERFACE) == 0) {
			/* Make sure the trait method is compatible with previosly declared abstract method */
			if (UNEXPECTED(!gear_traits_method_compatibility_check(fn, existing_fn))) {
				gear_error_noreturn(E_COMPILE_ERROR, "Declaration of %s must be compatible with %s",
					ZSTR_VAL(gear_get_function_declaration(fn)),
					ZSTR_VAL(gear_get_function_declaration(existing_fn)));
			}
		} else if (fn->common.fn_flags & GEAR_ACC_ABSTRACT) {
			/* Make sure the abstract declaration is compatible with previous declaration */
			if (UNEXPECTED(!gear_traits_method_compatibility_check(existing_fn, fn))) {
				gear_error_noreturn(E_COMPILE_ERROR, "Declaration of %s must be compatible with %s",
					ZSTR_VAL(gear_get_function_declaration(existing_fn)),
					ZSTR_VAL(gear_get_function_declaration(fn)));
			}
			return;
		} else if (UNEXPECTED(existing_fn->common.scope->ce_flags & GEAR_ACC_TRAIT)) {
			/* two traits can't define the same non-abstract method */
#if 1
			gear_error_noreturn(E_COMPILE_ERROR, "Trait method %s has not been applied, because there are collisions with other trait methods on %s",
				name, ZSTR_VAL(ce->name));
#else		/* TODO: better error message */
			gear_error_noreturn(E_COMPILE_ERROR, "Trait method %s::%s has not been applied as %s::%s, because of collision with %s::%s",
				ZSTR_VAL(fn->common.scope->name), ZSTR_VAL(fn->common.function_name),
				ZSTR_VAL(ce->name), name,
				ZSTR_VAL(existing_fn->common.scope->name), ZSTR_VAL(existing_fn->common.function_name));
#endif
		} else {
			/* inherited members are overridden by members inserted by traits */
			/* check whether the trait method fulfills the inheritance requirements */
			do_inheritance_check_on_method(fn, existing_fn);
			fn->common.prototype = NULL;
		}
	}

	function_add_ref(fn);
	if (UNEXPECTED(fn->type == GEAR_INTERNAL_FUNCTION)) {
		new_fn = gear_arena_alloc(&CG(arena), sizeof(gear_internal_function));
		memcpy(new_fn, fn, sizeof(gear_internal_function));
		new_fn->common.fn_flags |= GEAR_ACC_ARENA_ALLOCATED;
	} else {
		new_fn = gear_arena_alloc(&CG(arena), sizeof(gear_op_array));
		memcpy(new_fn, fn, sizeof(gear_op_array));
	}
	fn = gear_hash_update_ptr(&ce->function_table, key, new_fn);
	gear_add_magic_methods(ce, key, fn);
}
/* }}} */

static void gear_fixup_trait_method(gear_function *fn, gear_class_entry *ce) /* {{{ */
{
	if ((fn->common.scope->ce_flags & GEAR_ACC_TRAIT) == GEAR_ACC_TRAIT) {

		fn->common.scope = ce;

		if (fn->common.fn_flags & GEAR_ACC_ABSTRACT) {
			ce->ce_flags |= GEAR_ACC_IMPLICIT_ABSTRACT_CLASS;
		}
		if (fn->type == GEAR_USER_FUNCTION && fn->op_array.static_variables) {
			ce->ce_flags |= GEAR_HAS_STATIC_IN_METHODS;
		}
	}
}
/* }}} */

static int gear_traits_copy_functions(gear_string *fnname, gear_function *fn, gear_class_entry *ce, HashTable **overriden, HashTable *exclude_table, gear_class_entry **aliases) /* {{{ */
{
	gear_trait_alias  *alias, **alias_ptr;
	gear_string       *lcname;
	gear_function      fn_copy;
	int                i;

	/* apply aliases which are qualified with a class name, there should not be any ambiguity */
	if (ce->trait_aliases) {
		alias_ptr = ce->trait_aliases;
		alias = *alias_ptr;
		i = 0;
		while (alias) {
			/* Scope unset or equal to the function we compare to, and the alias applies to fn */
			if (alias->alias != NULL
				&& (!aliases[i] || fn->common.scope == aliases[i])
				&& ZSTR_LEN(alias->trait_method.method_name) == ZSTR_LEN(fnname)
				&& (gear_binary_strcasecmp(ZSTR_VAL(alias->trait_method.method_name), ZSTR_LEN(alias->trait_method.method_name), ZSTR_VAL(fnname), ZSTR_LEN(fnname)) == 0)) {
				fn_copy = *fn;

				/* if it is 0, no modifieres has been changed */
				if (alias->modifiers) {
					fn_copy.common.fn_flags = alias->modifiers | (fn->common.fn_flags ^ (fn->common.fn_flags & GEAR_ACC_PPP_MASK));
				}

				lcname = gear_string_tolower(alias->alias);
				gear_add_trait_method(ce, ZSTR_VAL(alias->alias), lcname, &fn_copy, overriden);
				gear_string_release_ex(lcname, 0);

				/* Record the trait from which this alias was resolved. */
				if (!aliases[i]) {
					aliases[i] = fn->common.scope;
				}
				if (!alias->trait_method.class_name) {
					/* TODO: try to avoid this assignment (it's necessary only for reflection) */
					alias->trait_method.class_name = gear_string_copy(fn->common.scope->name);
				}
			}
			alias_ptr++;
			alias = *alias_ptr;
			i++;
		}
	}

	if (exclude_table == NULL || gear_hash_find(exclude_table, fnname) == NULL) {
		/* is not in hashtable, thus, function is not to be excluded */
		/* And how about GEAR_OVERLOADED_FUNCTION? */
		memcpy(&fn_copy, fn, fn->type == GEAR_USER_FUNCTION? sizeof(gear_op_array) : sizeof(gear_internal_function));

		/* apply aliases which have not alias name, just setting visibility */
		if (ce->trait_aliases) {
			alias_ptr = ce->trait_aliases;
			alias = *alias_ptr;
			i = 0;
			while (alias) {
				/* Scope unset or equal to the function we compare to, and the alias applies to fn */
				if (alias->alias == NULL && alias->modifiers != 0
					&& (!aliases[i] || fn->common.scope == aliases[i])
					&& (ZSTR_LEN(alias->trait_method.method_name) == ZSTR_LEN(fnname))
					&& (gear_binary_strcasecmp(ZSTR_VAL(alias->trait_method.method_name), ZSTR_LEN(alias->trait_method.method_name), ZSTR_VAL(fnname), ZSTR_LEN(fnname)) == 0)) {

					fn_copy.common.fn_flags = alias->modifiers | (fn->common.fn_flags ^ (fn->common.fn_flags & GEAR_ACC_PPP_MASK));

					/** Record the trait from which this alias was resolved. */
					if (!aliases[i]) {
						aliases[i] = fn->common.scope;
					}
					if (!alias->trait_method.class_name) {
						/* TODO: try to avoid this assignment (it's necessary only for reflection) */
						alias->trait_method.class_name = gear_string_copy(fn->common.scope->name);
					}
				}
				alias_ptr++;
				alias = *alias_ptr;
				i++;
			}
		}

		gear_add_trait_method(ce, ZSTR_VAL(fn->common.function_name), fnname, &fn_copy, overriden);
	}

	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

static uint32_t gear_check_trait_usage(gear_class_entry *ce, gear_class_entry *trait) /* {{{ */
{
	uint32_t i;

	if (UNEXPECTED((trait->ce_flags & GEAR_ACC_TRAIT) != GEAR_ACC_TRAIT)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Class %s is not a trait, Only traits may be used in 'as' and 'insteadof' statements", ZSTR_VAL(trait->name));
		return 0;
	}

	for (i = 0; i < ce->num_traits; i++) {
		if (ce->traits[i] == trait) {
			return i;
		}
	}
	gear_error_noreturn(E_COMPILE_ERROR, "Required Trait %s wasn't added to %s", ZSTR_VAL(trait->name), ZSTR_VAL(ce->name));
	return 0;
}
/* }}} */

static void gear_traits_init_trait_structures(gear_class_entry *ce, HashTable ***exclude_tables_ptr, gear_class_entry ***aliases_ptr) /* {{{ */
{
	size_t i, j = 0;
	gear_trait_precedence **precedences;
	gear_trait_precedence *cur_precedence;
	gear_trait_method_reference *cur_method_ref;
	gear_string *lcname;
	gear_bool method_exists;
	HashTable **exclude_tables = NULL;
	gear_class_entry **aliases = NULL;
	gear_class_entry *trait;

	/* resolve class references */
	if (ce->trait_precedences) {
		exclude_tables = ecalloc(ce->num_traits, sizeof(HashTable*));
		i = 0;
		precedences = ce->trait_precedences;
		ce->trait_precedences = NULL;
		while ((cur_precedence = precedences[i])) {
			/** Resolve classes for all precedence operations. */
			cur_method_ref = &cur_precedence->trait_method;
			trait = gear_fetch_class(cur_method_ref->class_name,
							GEAR_FETCH_CLASS_TRAIT|GEAR_FETCH_CLASS_NO_AUTOLOAD);
			if (!trait) {
				gear_error_noreturn(E_COMPILE_ERROR, "Could not find trait %s", ZSTR_VAL(cur_method_ref->class_name));
			}
			gear_check_trait_usage(ce, trait);

			/** Ensure that the preferred method is actually available. */
			lcname = gear_string_tolower(cur_method_ref->method_name);
			method_exists = gear_hash_exists(&trait->function_table, lcname);
			if (!method_exists) {
				gear_error_noreturn(E_COMPILE_ERROR,
						   "A precedence rule was defined for %s::%s but this method does not exist",
						   ZSTR_VAL(trait->name),
						   ZSTR_VAL(cur_method_ref->method_name));
			}

			/** With the other traits, we are more permissive.
				We do not give errors for those. This allows to be more
				defensive in such definitions.
				However, we want to make sure that the insteadof declaration
				is consistent in itself.
			 */

			for (j = 0; j < cur_precedence->num_excludes; j++) {
				gear_string* class_name = cur_precedence->exclude_class_names[j];
				gear_class_entry *exclude_ce = gear_fetch_class(class_name, GEAR_FETCH_CLASS_TRAIT |GEAR_FETCH_CLASS_NO_AUTOLOAD);
				uint32_t trait_num;

				if (!exclude_ce) {
					gear_error_noreturn(E_COMPILE_ERROR, "Could not find trait %s", ZSTR_VAL(class_name));
				}
				trait_num = gear_check_trait_usage(ce, exclude_ce);
				if (!exclude_tables[trait_num]) {
					ALLOC_HASHTABLE(exclude_tables[trait_num]);
					gear_hash_init(exclude_tables[trait_num], 0, NULL, NULL, 0);
				}
				if (gear_hash_add_empty_element(exclude_tables[trait_num], lcname) == NULL) {
					gear_error_noreturn(E_COMPILE_ERROR, "Failed to evaluate a trait precedence (%s). Method of trait %s was defined to be excluded multiple times", ZSTR_VAL(precedences[i]->trait_method.method_name), ZSTR_VAL(exclude_ce->name));
				}

				/* make sure that the trait method is not from a class mentioned in
				 exclude_from_classes, for consistency */
				if (trait == exclude_ce) {
					gear_error_noreturn(E_COMPILE_ERROR,
							   "Inconsistent insteadof definition. "
							   "The method %s is to be used from %s, but %s is also on the exclude list",
							   ZSTR_VAL(cur_method_ref->method_name),
							   ZSTR_VAL(trait->name),
							   ZSTR_VAL(trait->name));
				}
			}
			gear_string_release_ex(lcname, 0);
			i++;
		}
		ce->trait_precedences = precedences;
	}

	if (ce->trait_aliases) {
		i = 0;
		while (ce->trait_aliases[i]) {
			i++;
		}
		aliases = ecalloc(i, sizeof(gear_class_entry*));
		i = 0;
		while (ce->trait_aliases[i]) {
			/** For all aliases with an explicit class name, resolve the class now. */
			if (ce->trait_aliases[i]->trait_method.class_name) {
				cur_method_ref = &ce->trait_aliases[i]->trait_method;
				trait = gear_fetch_class(cur_method_ref->class_name, GEAR_FETCH_CLASS_TRAIT|GEAR_FETCH_CLASS_NO_AUTOLOAD);
				if (!trait) {
					gear_error_noreturn(E_COMPILE_ERROR, "Could not find trait %s", ZSTR_VAL(cur_method_ref->class_name));
				}
				gear_check_trait_usage(ce, trait);
				aliases[i] = trait;

				/** And, ensure that the referenced method is resolvable, too. */
				lcname = gear_string_tolower(cur_method_ref->method_name);
				method_exists = gear_hash_exists(&trait->function_table, lcname);
				gear_string_release_ex(lcname, 0);

				if (!method_exists) {
					gear_error_noreturn(E_COMPILE_ERROR, "An alias was defined for %s::%s but this method does not exist", ZSTR_VAL(trait->name), ZSTR_VAL(cur_method_ref->method_name));
				}
			}
			i++;
		}
	}

	*exclude_tables_ptr = exclude_tables;
	*aliases_ptr = aliases;
}
/* }}} */

static void gear_do_traits_method_binding(gear_class_entry *ce, HashTable **exclude_tables, gear_class_entry **aliases) /* {{{ */
{
	uint32_t i;
	HashTable *overriden = NULL;
	gear_string *key;
	gear_function *fn;

	if (exclude_tables) {
		for (i = 0; i < ce->num_traits; i++) {
			/* copies functions, applies defined aliasing, and excludes unused trait methods */
			GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->traits[i]->function_table, key, fn) {
				gear_traits_copy_functions(key, fn, ce, &overriden, exclude_tables[i], aliases);
			} GEAR_HASH_FOREACH_END();

			if (exclude_tables[i]) {
				gear_hash_destroy(exclude_tables[i]);
				FREE_HASHTABLE(exclude_tables[i]);
				exclude_tables[i] = NULL;
			}
		}
	} else {
		for (i = 0; i < ce->num_traits; i++) {
			GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->traits[i]->function_table, key, fn) {
				gear_traits_copy_functions(key, fn, ce, &overriden, NULL, aliases);
			} GEAR_HASH_FOREACH_END();
		}
	}

	GEAR_HASH_FOREACH_PTR(&ce->function_table, fn) {
		gear_fixup_trait_method(fn, ce);
	} GEAR_HASH_FOREACH_END();

	if (overriden) {
		gear_hash_destroy(overriden);
		FREE_HASHTABLE(overriden);
	}
}
/* }}} */

static gear_class_entry* find_first_definition(gear_class_entry *ce, size_t current_trait, gear_string *prop_name, gear_class_entry *coliding_ce) /* {{{ */
{
	size_t i;

	if (coliding_ce == ce) {
		for (i = 0; i < current_trait; i++) {
			if (gear_hash_exists(&ce->traits[i]->properties_info, prop_name)) {
				return ce->traits[i];
			}
		}
	}

	return coliding_ce;
}
/* }}} */

static void gear_do_traits_property_binding(gear_class_entry *ce) /* {{{ */
{
	size_t i;
	gear_property_info *property_info;
	gear_property_info *coliding_prop;
	gear_string* prop_name;
	const char* class_name_unused;
	gear_bool not_compatible;
	zval* prop_value;
	uint32_t flags;
	gear_string *doc_comment;

	/* In the following steps the properties are inserted into the property table
	 * for that, a very strict approach is applied:
	 * - check for compatibility, if not compatible with any property in class -> fatal
	 * - if compatible, then strict notice
	 */
	for (i = 0; i < ce->num_traits; i++) {
		GEAR_HASH_FOREACH_PTR(&ce->traits[i]->properties_info, property_info) {
			/* first get the unmangeld name if necessary,
			 * then check whether the property is already there
			 */
			flags = property_info->flags;
			if (flags & GEAR_ACC_PUBLIC) {
				prop_name = gear_string_copy(property_info->name);
			} else {
				const char *pname;
				size_t pname_len;

				/* for private and protected we need to unmangle the names */
				gear_unmangle_property_name_ex(property_info->name,
											&class_name_unused, &pname, &pname_len);
				prop_name = gear_string_init(pname, pname_len, 0);
			}

			/* next: check for conflicts with current class */
			if ((coliding_prop = gear_hash_find_ptr(&ce->properties_info, prop_name)) != NULL) {
				if (coliding_prop->flags & GEAR_ACC_SHADOW) {
					gear_string_release_ex(coliding_prop->name, 0);
					if (coliding_prop->doc_comment) {
						gear_string_release_ex(coliding_prop->doc_comment, 0);
                    }
					gear_hash_del(&ce->properties_info, prop_name);
					flags |= GEAR_ACC_CHANGED;
				} else {
					not_compatible = 1;

					if ((coliding_prop->flags & (GEAR_ACC_PPP_MASK | GEAR_ACC_STATIC))
						== (flags & (GEAR_ACC_PPP_MASK | GEAR_ACC_STATIC))) {
						/* the flags are identical, thus, the properties may be compatible */
						zval *op1, *op2;
						zval op1_tmp, op2_tmp;

						if (flags & GEAR_ACC_STATIC) {
							op1 = &ce->default_static_members_table[coliding_prop->offset];
							op2 = &ce->traits[i]->default_static_members_table[property_info->offset];
							ZVAL_DEINDIRECT(op1);
							ZVAL_DEINDIRECT(op2);
						} else {
							op1 = &ce->default_properties_table[OBJ_PROP_TO_NUM(coliding_prop->offset)];
							op2 = &ce->traits[i]->default_properties_table[OBJ_PROP_TO_NUM(property_info->offset)];
						}

						/* if any of the values is a constant, we try to resolve it */
						if (UNEXPECTED(Z_TYPE_P(op1) == IS_CONSTANT_AST)) {
							ZVAL_COPY_OR_DUP(&op1_tmp, op1);
							zval_update_constant_ex(&op1_tmp, ce);
							op1 = &op1_tmp;
						}
						if (UNEXPECTED(Z_TYPE_P(op2) == IS_CONSTANT_AST)) {
							ZVAL_COPY_OR_DUP(&op2_tmp, op2);
							zval_update_constant_ex(&op2_tmp, ce);
							op2 = &op2_tmp;
						}

						not_compatible = fast_is_not_identical_function(op1, op2);

						if (op1 == &op1_tmp) {
							zval_ptr_dtor_nogc(&op1_tmp);
						}
						if (op2 == &op2_tmp) {
							zval_ptr_dtor_nogc(&op2_tmp);
						}
					}

					if (not_compatible) {
						gear_error_noreturn(E_COMPILE_ERROR,
							   "%s and %s define the same property ($%s) in the composition of %s. However, the definition differs and is considered incompatible. Class was composed",
								ZSTR_VAL(find_first_definition(ce, i, prop_name, coliding_prop->ce)->name),
								ZSTR_VAL(property_info->ce->name),
								ZSTR_VAL(prop_name),
								ZSTR_VAL(ce->name));
					}

					gear_string_release_ex(prop_name, 0);
					continue;
				}
			}

			/* property not found, so lets add it */
			if (flags & GEAR_ACC_STATIC) {
				prop_value = &ce->traits[i]->default_static_members_table[property_info->offset];
				GEAR_ASSERT(Z_TYPE_P(prop_value) != IS_INDIRECT);
			} else {
				prop_value = &ce->traits[i]->default_properties_table[OBJ_PROP_TO_NUM(property_info->offset)];
			}

			Z_TRY_ADDREF_P(prop_value);
			doc_comment = property_info->doc_comment ? gear_string_copy(property_info->doc_comment) : NULL;
			gear_declare_property_ex(ce, prop_name,
									 prop_value, flags,
								     doc_comment);
			gear_string_release_ex(prop_name, 0);
		} GEAR_HASH_FOREACH_END();
	}
}
/* }}} */

static void gear_do_check_for_inconsistent_traits_aliasing(gear_class_entry *ce, gear_class_entry **aliases) /* {{{ */
{
	int i = 0;
	gear_trait_alias* cur_alias;
	gear_string* lc_method_name;

	if (ce->trait_aliases) {
		while (ce->trait_aliases[i]) {
			cur_alias = ce->trait_aliases[i];
			/** The trait for this alias has not been resolved, this means, this
				alias was not applied. Abort with an error. */
			if (!aliases[i]) {
				if (cur_alias->alias) {
					/** Plain old inconsistency/typo/bug */
					gear_error_noreturn(E_COMPILE_ERROR,
							   "An alias (%s) was defined for method %s(), but this method does not exist",
							   ZSTR_VAL(cur_alias->alias),
							   ZSTR_VAL(cur_alias->trait_method.method_name));
				} else {
					/** Here are two possible cases:
						1) this is an attempt to modifiy the visibility
						   of a method introduce as part of another alias.
						   Since that seems to violate the DRY principle,
						   we check against it and abort.
						2) it is just a plain old inconsitency/typo/bug
						   as in the case where alias is set. */

					lc_method_name = gear_string_tolower(
						cur_alias->trait_method.method_name);
					if (gear_hash_exists(&ce->function_table,
										 lc_method_name)) {
						gear_string_release_ex(lc_method_name, 0);
						gear_error_noreturn(E_COMPILE_ERROR,
								   "The modifiers for the trait alias %s() need to be changed in the same statement in which the alias is defined. Error",
								   ZSTR_VAL(cur_alias->trait_method.method_name));
					} else {
						gear_string_release_ex(lc_method_name, 0);
						gear_error_noreturn(E_COMPILE_ERROR,
								   "The modifiers of the trait method %s() are changed, but this method does not exist. Error",
								   ZSTR_VAL(cur_alias->trait_method.method_name));

					}
				}
			}
			i++;
		}
	}
}
/* }}} */

GEAR_API void gear_do_bind_traits(gear_class_entry *ce) /* {{{ */
{
	HashTable **exclude_tables;
	gear_class_entry **aliases;

	if (ce->num_traits == 0) {
		return;
	}

	/* complete initialization of trait strutures in ce */
	gear_traits_init_trait_structures(ce, &exclude_tables, &aliases);

	/* first care about all methods to be flattened into the class */
	gear_do_traits_method_binding(ce, exclude_tables, aliases);

	/* Aliases which have not been applied indicate typos/bugs. */
	gear_do_check_for_inconsistent_traits_aliasing(ce, aliases);

	if (aliases) {
		efree(aliases);
	}

	if (exclude_tables) {
		efree(exclude_tables);
	}

	/* then flatten the properties into it, to, mostly to notfiy developer about problems */
	gear_do_traits_property_binding(ce);

	/* verify that all abstract methods from traits have been implemented */
	gear_verify_abstract_class(ce);

	/* Emit E_DEPRECATED for HYSS constructors */
	gear_check_deprecated_constructor(ce);

	/* now everything should be fine and an added GEAR_ACC_IMPLICIT_ABSTRACT_CLASS should be removed */
	if (ce->ce_flags & GEAR_ACC_IMPLICIT_ABSTRACT_CLASS) {
		ce->ce_flags -= GEAR_ACC_IMPLICIT_ABSTRACT_CLASS;
	}
}
/* }}} */


static gear_bool gear_has_deprecated_constructor(const gear_class_entry *ce) /* {{{ */
{
	const gear_string *constructor_name;
	if (!ce->constructor) {
		return 0;
	}
	constructor_name = ce->constructor->common.function_name;
	return !gear_binary_strcasecmp(
		ZSTR_VAL(ce->name), ZSTR_LEN(ce->name),
		ZSTR_VAL(constructor_name), ZSTR_LEN(constructor_name)
	);
}
/* }}} */

void gear_check_deprecated_constructor(const gear_class_entry *ce) /* {{{ */
{
	if (gear_has_deprecated_constructor(ce)) {
		gear_error(E_DEPRECATED, "Methods with the same name as their class will not be constructors in a future version of HYSS; %s has a deprecated constructor", ZSTR_VAL(ce->name));
	}
}
/* }}} */

