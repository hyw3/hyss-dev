/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear_extensions.h"

GEAR_API gear_llist gear_extensions;
GEAR_API uint32_t gear_extension_flags = 0;
static int last_resource_number;

int gear_load_extension(const char *path)
{
#if GEAR_EXTENSIONS_SUPPORT
	DL_HANDLE handle;

	handle = DL_LOAD(path);
	if (!handle) {
#ifndef GEAR_WIN32
		fprintf(stderr, "Failed loading %s:  %s\n", path, DL_ERROR());
#else
		fprintf(stderr, "Failed loading %s\n", path);
		/* See http://support.microsoft.com/kb/190351 */
		fflush(stderr);
#endif
		return FAILURE;
	}
	return gear_load_extension_handle(handle, path);
#else
	fprintf(stderr, "Extensions are not supported on this platform.\n");
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
	fflush(stderr);
#endif
	return FAILURE;
#endif
}

int gear_load_extension_handle(DL_HANDLE handle, const char *path)
{
#if GEAR_EXTENSIONS_SUPPORT
	gear_extension *new_extension;
	gear_extension_version_info *extension_version_info;

	extension_version_info = (gear_extension_version_info *) DL_FETCH_SYMBOL(handle, "extension_version_info");
	if (!extension_version_info) {
		extension_version_info = (gear_extension_version_info *) DL_FETCH_SYMBOL(handle, "_extension_version_info");
	}
	new_extension = (gear_extension *) DL_FETCH_SYMBOL(handle, "gear_extension_entry");
	if (!new_extension) {
		new_extension = (gear_extension *) DL_FETCH_SYMBOL(handle, "_gear_extension_entry");
	}
	if (!extension_version_info || !new_extension) {
		fprintf(stderr, "%s doesn't appear to be a valid Gear extension\n", path);
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
		fflush(stderr);
#endif
		DL_UNLOAD(handle);
		return FAILURE;
	}

	/* allow extension to proclaim compatibility with any Gear version */
	if (extension_version_info->gear_extension_api_no != GEAR_EXTENSION_API_NO &&(!new_extension->api_no_check || new_extension->api_no_check(GEAR_EXTENSION_API_NO) != SUCCESS)) {
		if (extension_version_info->gear_extension_api_no > GEAR_EXTENSION_API_NO) {
			fprintf(stderr, "%s requires Gear Engine API version %d.\n"
					"The Gear Engine API version %d which is installed, is outdated.\n\n",
					new_extension->name,
					extension_version_info->gear_extension_api_no,
					GEAR_EXTENSION_API_NO);
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
			fflush(stderr);
#endif
			DL_UNLOAD(handle);
			return FAILURE;
		} else if (extension_version_info->gear_extension_api_no < GEAR_EXTENSION_API_NO) {
			fprintf(stderr, "%s requires Gear Engine API version %d.\n"
					"The Gear Engine API version %d which is installed, is newer.\n"
					"Contact %s at %s for a later version of %s.\n\n",
					new_extension->name,
					extension_version_info->gear_extension_api_no,
					GEAR_EXTENSION_API_NO,
					new_extension->author,
					new_extension->URL,
					new_extension->name);
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
			fflush(stderr);
#endif
			DL_UNLOAD(handle);
			return FAILURE;
		}
	} else if (strcmp(GEAR_EXTENSION_BUILD_ID, extension_version_info->build_id) &&
	           (!new_extension->build_id_check || new_extension->build_id_check(GEAR_EXTENSION_BUILD_ID) != SUCCESS)) {
		fprintf(stderr, "Cannot load %s - it was built with configuration %s, whereas running engine is %s\n",
					new_extension->name, extension_version_info->build_id, GEAR_EXTENSION_BUILD_ID);
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
		fflush(stderr);
#endif
		DL_UNLOAD(handle);
		return FAILURE;
	} else if (gear_get_extension(new_extension->name)) {
		fprintf(stderr, "Cannot load %s - it was already loaded\n", new_extension->name);
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
		fflush(stderr);
#endif
		DL_UNLOAD(handle);
		return FAILURE;
	}

	return gear_register_extension(new_extension, handle);
#else
	fprintf(stderr, "Extensions are not supported on this platform.\n");
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
	fflush(stderr);
#endif
	return FAILURE;
#endif
}


int gear_register_extension(gear_extension *new_extension, DL_HANDLE handle)
{
#if GEAR_EXTENSIONS_SUPPORT
	gear_extension extension;

	extension = *new_extension;
	extension.handle = handle;

	gear_extension_dispatch_message(GEAR_EXTMSG_NEW_EXTENSION, &extension);

	gear_llist_add_element(&gear_extensions, &extension);

	if (extension.op_array_ctor) {
		gear_extension_flags |= GEAR_EXTENSIONS_HAVE_OP_ARRAY_CTOR;
	}
	if (extension.op_array_dtor) {
		gear_extension_flags |= GEAR_EXTENSIONS_HAVE_OP_ARRAY_DTOR;
	}
	if (extension.op_array_handler) {
		gear_extension_flags |= GEAR_EXTENSIONS_HAVE_OP_ARRAY_HANDLER;
	}
	if (extension.op_array_persist_calc) {
		gear_extension_flags |= GEAR_EXTENSIONS_HAVE_OP_ARRAY_PERSIST_CALC;
	}
	if (extension.op_array_persist) {
		gear_extension_flags |= GEAR_EXTENSIONS_HAVE_OP_ARRAY_PERSIST;
	}
	/*fprintf(stderr, "Loaded %s, version %s\n", extension.name, extension.version);*/
#endif

	return SUCCESS;
}


static void gear_extension_shutdown(gear_extension *extension)
{
#if GEAR_EXTENSIONS_SUPPORT
	if (extension->shutdown) {
		extension->shutdown(extension);
	}
#endif
}

static int gear_extension_startup(gear_extension *extension)
{
#if GEAR_EXTENSIONS_SUPPORT
	if (extension->startup) {
		if (extension->startup(extension)!=SUCCESS) {
			return 1;
		}
		gear_append_version_info(extension);
	}
#endif
	return 0;
}


int gear_startup_extensions_mechanism()
{
	/* Startup extensions mechanism */
	gear_llist_init(&gear_extensions, sizeof(gear_extension), (void (*)(void *)) gear_extension_dtor, 1);
	last_resource_number = 0;
	return SUCCESS;
}


int gear_startup_extensions()
{
	gear_llist_apply_with_del(&gear_extensions, (int (*)(void *)) gear_extension_startup);
	return SUCCESS;
}


void gear_shutdown_extensions(void)
{
	gear_llist_apply(&gear_extensions, (llist_apply_func_t) gear_extension_shutdown);
	gear_llist_destroy(&gear_extensions);
}


void gear_extension_dtor(gear_extension *extension)
{
#if GEAR_EXTENSIONS_SUPPORT && !GEAR_DEBUG
	if (extension->handle && !getenv("GEAR_DONT_UNLOAD_CAPIS")) {
		DL_UNLOAD(extension->handle);
	}
#endif
}


static void gear_extension_message_dispatcher(const gear_extension *extension, int num_args, va_list args)
{
	int message;
	void *arg;

	if (!extension->message_handler || num_args!=2) {
		return;
	}
	message = va_arg(args, int);
	arg = va_arg(args, void *);
	extension->message_handler(message, arg);
}


GEAR_API void gear_extension_dispatch_message(int message, void *arg)
{
	gear_llist_apply_with_arguments(&gear_extensions, (llist_apply_with_args_func_t) gear_extension_message_dispatcher, 2, message, arg);
}


GEAR_API int gear_get_resource_handle(gear_extension *extension)
{
	if (last_resource_number<GEAR_MAX_RESERVED_RESOURCES) {
		extension->resource_number = last_resource_number;
		return last_resource_number++;
	} else {
		return -1;
	}
}


GEAR_API gear_extension *gear_get_extension(const char *extension_name)
{
	gear_llist_element *element;

	for (element = gear_extensions.head; element; element = element->next) {
		gear_extension *extension = (gear_extension *) element->data;

		if (!strcmp(extension->name, extension_name)) {
			return extension;
		}
	}
	return NULL;
}

typedef struct _gear_extension_persist_data {
	gear_op_array *op_array;
	size_t         size;
	char          *mem;
} gear_extension_persist_data;

static void gear_extension_op_array_persist_calc_handler(gear_extension *extension, gear_extension_persist_data *data)
{
	if (extension->op_array_persist_calc) {
		data->size += extension->op_array_persist_calc(data->op_array);
	}
}

static void gear_extension_op_array_persist_handler(gear_extension *extension, gear_extension_persist_data *data)
{
	if (extension->op_array_persist) {
		size_t size = extension->op_array_persist(data->op_array, data->mem);
		if (size) {
			data->mem = (void*)((char*)data->mem + size);
			data->size += size;
		}
	}
}

GEAR_API size_t gear_extensions_op_array_persist_calc(gear_op_array *op_array)
{
	if (gear_extension_flags & GEAR_EXTENSIONS_HAVE_OP_ARRAY_PERSIST_CALC) {
		gear_extension_persist_data data;

		data.op_array = op_array;
		data.size = 0;
		data.mem  = NULL;
		gear_llist_apply_with_argument(&gear_extensions, (llist_apply_with_arg_func_t) gear_extension_op_array_persist_calc_handler, &data);
		return data.size;
	}
	return 0;
}

GEAR_API size_t gear_extensions_op_array_persist(gear_op_array *op_array, void *mem)
{
	if (gear_extension_flags & GEAR_EXTENSIONS_HAVE_OP_ARRAY_PERSIST) {
		gear_extension_persist_data data;

		data.op_array = op_array;
		data.size = 0;
		data.mem  = mem;
		gear_llist_apply_with_argument(&gear_extensions, (llist_apply_with_arg_func_t) gear_extension_op_array_persist_handler, &data);
		return data.size;
	}
	return 0;
}

