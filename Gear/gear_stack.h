/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_STACK_H
#define GEAR_STACK_H

typedef struct _gear_stack {
	int size, top, max;
	void *elements;
} gear_stack;


#define STACK_BLOCK_SIZE 16

BEGIN_EXTERN_C()
GEAR_API int gear_stack_init(gear_stack *stack, int size);
GEAR_API int gear_stack_push(gear_stack *stack, const void *element);
GEAR_API void *gear_stack_top(const gear_stack *stack);
GEAR_API int gear_stack_del_top(gear_stack *stack);
GEAR_API int gear_stack_int_top(const gear_stack *stack);
GEAR_API int gear_stack_is_empty(const gear_stack *stack);
GEAR_API int gear_stack_destroy(gear_stack *stack);
GEAR_API void *gear_stack_base(const gear_stack *stack);
GEAR_API int gear_stack_count(const gear_stack *stack);
GEAR_API void gear_stack_apply(gear_stack *stack, int type, int (*apply_function)(void *element));
GEAR_API void gear_stack_apply_with_argument(gear_stack *stack, int type, int (*apply_function)(void *element, void *arg), void *arg);
GEAR_API void gear_stack_clean(gear_stack *stack, void (*func)(void *), gear_bool free_elements);
END_EXTERN_C()

#define GEAR_STACK_APPLY_TOPDOWN	1
#define GEAR_STACK_APPLY_BOTTOMUP	2

#endif /* GEAR_STACK_H */

