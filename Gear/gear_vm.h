/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_VM_H
#define GEAR_VM_H

BEGIN_EXTERN_C()

GEAR_API void GEAR_FASTCALL gear_vm_set_opcode_handler(gear_op* opcode);
GEAR_API void GEAR_FASTCALL gear_vm_set_opcode_handler_ex(gear_op* opcode, uint32_t op1_info, uint32_t op2_info, uint32_t res_info);
GEAR_API void GEAR_FASTCALL gear_serialize_opcode_handler(gear_op *op);
GEAR_API void GEAR_FASTCALL gear_deserialize_opcode_handler(gear_op *op);
GEAR_API const void* GEAR_FASTCALL gear_get_opcode_handler_func(const gear_op *op);
GEAR_API const gear_op *gear_get_halt_op(void);
GEAR_API int GEAR_FASTCALL gear_vm_call_opcode_handler(gear_execute_data *ex);
GEAR_API int gear_vm_kind(void);

void gear_vm_init(void);
void gear_vm_dtor(void);

END_EXTERN_C()

#define GEAR_VM_SET_OPCODE_HANDLER(opline) gear_vm_set_opcode_handler(opline)

#endif

