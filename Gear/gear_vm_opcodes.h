/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_VM_OPCODES_H
#define GEAR_VM_OPCODES_H

#define GEAR_VM_SPEC		1
#define GEAR_VM_LINES		0
#define GEAR_VM_KIND_CALL	1
#define GEAR_VM_KIND_SWITCH	2
#define GEAR_VM_KIND_GOTO	3
#define GEAR_VM_KIND_HYBRID	4
/* HYBRID requires support for computed GOTO and global register variables*/
#if (defined(__GNUC__) && defined(HAVE_GCC_GLOBAL_REGS))
# define GEAR_VM_KIND		GEAR_VM_KIND_HYBRID
#else
# define GEAR_VM_KIND		GEAR_VM_KIND_CALL
#endif

#define GEAR_VM_OP_SPEC          0x00000001
#define GEAR_VM_OP_CONST         0x00000002
#define GEAR_VM_OP_TMPVAR        0x00000004
#define GEAR_VM_OP_TMPVARCV      0x00000008
#define GEAR_VM_OP_MASK          0x000000f0
#define GEAR_VM_OP_NUM           0x00000010
#define GEAR_VM_OP_JMP_ADDR      0x00000020
#define GEAR_VM_OP_TRY_CATCH     0x00000030
#define GEAR_VM_OP_THIS          0x00000050
#define GEAR_VM_OP_NEXT          0x00000060
#define GEAR_VM_OP_CLASS_FETCH   0x00000070
#define GEAR_VM_OP_CONSTRUCTOR   0x00000080
#define GEAR_VM_OP_CONST_FETCH   0x00000090
#define GEAR_VM_OP_CACHE_SLOT    0x000000a0
#define GEAR_VM_EXT_VAR_FETCH    0x00010000
#define GEAR_VM_EXT_ISSET        0x00020000
#define GEAR_VM_EXT_CACHE_SLOT   0x00040000
#define GEAR_VM_EXT_ARRAY_INIT   0x00080000
#define GEAR_VM_EXT_REF          0x00100000
#define GEAR_VM_EXT_MASK         0x0f000000
#define GEAR_VM_EXT_NUM          0x01000000
#define GEAR_VM_EXT_LAST_CATCH   0x02000000
#define GEAR_VM_EXT_JMP_ADDR     0x03000000
#define GEAR_VM_EXT_DIM_OBJ      0x04000000
#define GEAR_VM_EXT_TYPE         0x07000000
#define GEAR_VM_EXT_EVAL         0x08000000
#define GEAR_VM_EXT_TYPE_MASK    0x09000000
#define GEAR_VM_EXT_SRC          0x0b000000
#define GEAR_VM_NO_CONST_CONST   0x40000000
#define GEAR_VM_COMMUTATIVE      0x80000000
#define GEAR_VM_OP1_FLAGS(flags) (flags & 0xff)
#define GEAR_VM_OP2_FLAGS(flags) ((flags >> 8) & 0xff)

BEGIN_EXTERN_C()

GEAR_API const char* GEAR_FASTCALL gear_get_opcode_name(gear_uchar opcode);
GEAR_API uint32_t GEAR_FASTCALL gear_get_opcode_flags(gear_uchar opcode);

END_EXTERN_C()

#define GEAR_NOP                               0
#define GEAR_ADD                               1
#define GEAR_SUB                               2
#define GEAR_MUL                               3
#define GEAR_DIV                               4
#define GEAR_MOD                               5
#define GEAR_SL                                6
#define GEAR_SR                                7
#define GEAR_CONCAT                            8
#define GEAR_BW_OR                             9
#define GEAR_BW_AND                           10
#define GEAR_BW_XOR                           11
#define GEAR_BW_NOT                           12
#define GEAR_BOOL_NOT                         13
#define GEAR_BOOL_XOR                         14
#define GEAR_IS_IDENTICAL                     15
#define GEAR_IS_NOT_IDENTICAL                 16
#define GEAR_IS_EQUAL                         17
#define GEAR_IS_NOT_EQUAL                     18
#define GEAR_IS_SMALLER                       19
#define GEAR_IS_SMALLER_OR_EQUAL              20
#define GEAR_CAST                             21
#define GEAR_QM_ASSIGN                        22
#define GEAR_ASSIGN_ADD                       23
#define GEAR_ASSIGN_SUB                       24
#define GEAR_ASSIGN_MUL                       25
#define GEAR_ASSIGN_DIV                       26
#define GEAR_ASSIGN_MOD                       27
#define GEAR_ASSIGN_SL                        28
#define GEAR_ASSIGN_SR                        29
#define GEAR_ASSIGN_CONCAT                    30
#define GEAR_ASSIGN_BW_OR                     31
#define GEAR_ASSIGN_BW_AND                    32
#define GEAR_ASSIGN_BW_XOR                    33
#define GEAR_PRE_INC                          34
#define GEAR_PRE_DEC                          35
#define GEAR_POST_INC                         36
#define GEAR_POST_DEC                         37
#define GEAR_ASSIGN                           38
#define GEAR_ASSIGN_REF                       39
#define GEAR_ECHO                             40
#define GEAR_GENERATOR_CREATE                 41
#define GEAR_JMP                              42
#define GEAR_JMPZ                             43
#define GEAR_JMPNZ                            44
#define GEAR_JMPZNZ                           45
#define GEAR_JMPZ_EX                          46
#define GEAR_JMPNZ_EX                         47
#define GEAR_CASE                             48
#define GEAR_CHECK_VAR                        49
#define GEAR_SEND_VAR_NO_REF_EX               50
#define GEAR_MAKE_REF                         51
#define GEAR_BOOL                             52
#define GEAR_FAST_CONCAT                      53
#define GEAR_ROPE_INIT                        54
#define GEAR_ROPE_ADD                         55
#define GEAR_ROPE_END                         56
#define GEAR_BEGIN_SILENCE                    57
#define GEAR_END_SILENCE                      58
#define GEAR_INIT_FCALL_BY_NAME               59
#define GEAR_DO_FCALL                         60
#define GEAR_INIT_FCALL                       61
#define GEAR_RETURN                           62
#define GEAR_RECV                             63
#define GEAR_RECV_INIT                        64
#define GEAR_SEND_VAL                         65
#define GEAR_SEND_VAR_EX                      66
#define GEAR_SEND_REF                         67
#define GEAR_NEW                              68
#define GEAR_INIT_NS_FCALL_BY_NAME            69
#define GEAR_FREE                             70
#define GEAR_INIT_ARRAY                       71
#define GEAR_ADD_ARRAY_ELEMENT                72
#define GEAR_INCLUDE_OR_EVAL                  73
#define GEAR_UNSET_VAR                        74
#define GEAR_UNSET_DIM                        75
#define GEAR_UNSET_OBJ                        76
#define GEAR_FE_RESET_R                       77
#define GEAR_FE_FETCH_R                       78
#define GEAR_EXIT                             79
#define GEAR_FETCH_R                          80
#define GEAR_FETCH_DIM_R                      81
#define GEAR_FETCH_OBJ_R                      82
#define GEAR_FETCH_W                          83
#define GEAR_FETCH_DIM_W                      84
#define GEAR_FETCH_OBJ_W                      85
#define GEAR_FETCH_RW                         86
#define GEAR_FETCH_DIM_RW                     87
#define GEAR_FETCH_OBJ_RW                     88
#define GEAR_FETCH_IS                         89
#define GEAR_FETCH_DIM_IS                     90
#define GEAR_FETCH_OBJ_IS                     91
#define GEAR_FETCH_FUNC_ARG                   92
#define GEAR_FETCH_DIM_FUNC_ARG               93
#define GEAR_FETCH_OBJ_FUNC_ARG               94
#define GEAR_FETCH_UNSET                      95
#define GEAR_FETCH_DIM_UNSET                  96
#define GEAR_FETCH_OBJ_UNSET                  97
#define GEAR_FETCH_LIST_R                     98
#define GEAR_FETCH_CONSTANT                   99
#define GEAR_CHECK_FUNC_ARG                  100
#define GEAR_EXT_STMT                        101
#define GEAR_EXT_FCALL_BEGIN                 102
#define GEAR_EXT_FCALL_END                   103
#define GEAR_EXT_NOP                         104
#define GEAR_TICKS                           105
#define GEAR_SEND_VAR_NO_REF                 106
#define GEAR_CATCH                           107
#define GEAR_THROW                           108
#define GEAR_FETCH_CLASS                     109
#define GEAR_CLONE                           110
#define GEAR_RETURN_BY_REF                   111
#define GEAR_INIT_METHOD_CALL                112
#define GEAR_INIT_STATIC_METHOD_CALL         113
#define GEAR_ISSET_ISEMPTY_VAR               114
#define GEAR_ISSET_ISEMPTY_DIM_OBJ           115
#define GEAR_SEND_VAL_EX                     116
#define GEAR_SEND_VAR                        117
#define GEAR_INIT_USER_CALL                  118
#define GEAR_SEND_ARRAY                      119
#define GEAR_SEND_USER                       120
#define GEAR_STRLEN                          121
#define GEAR_DEFINED                         122
#define GEAR_TYPE_CHECK                      123
#define GEAR_VERIFY_RETURN_TYPE              124
#define GEAR_FE_RESET_RW                     125
#define GEAR_FE_FETCH_RW                     126
#define GEAR_FE_FREE                         127
#define GEAR_INIT_DYNAMIC_CALL               128
#define GEAR_DO_ICALL                        129
#define GEAR_DO_UCALL                        130
#define GEAR_DO_FCALL_BY_NAME                131
#define GEAR_PRE_INC_OBJ                     132
#define GEAR_PRE_DEC_OBJ                     133
#define GEAR_POST_INC_OBJ                    134
#define GEAR_POST_DEC_OBJ                    135
#define GEAR_ASSIGN_OBJ                      136
#define GEAR_OP_DATA                         137
#define GEAR_INSTANCEOF                      138
#define GEAR_DECLARE_CLASS                   139
#define GEAR_DECLARE_INHERITED_CLASS         140
#define GEAR_DECLARE_FUNCTION                141
#define GEAR_YIELD_FROM                      142
#define GEAR_DECLARE_CONST                   143
#define GEAR_ADD_INTERFACE                   144
#define GEAR_DECLARE_INHERITED_CLASS_DELAYED 145
#define GEAR_VERIFY_ABSTRACT_CLASS           146
#define GEAR_ASSIGN_DIM                      147
#define GEAR_ISSET_ISEMPTY_PROP_OBJ          148
#define GEAR_HANDLE_EXCEPTION                149
#define GEAR_USER_OPCODE                     150
#define GEAR_ASSERT_CHECK                    151
#define GEAR_JMP_SET                         152
#define GEAR_DECLARE_LAMBDA_FUNCTION         153
#define GEAR_ADD_TRAIT                       154
#define GEAR_BIND_TRAITS                     155
#define GEAR_SEPARATE                        156
#define GEAR_FETCH_CLASS_NAME                157
#define GEAR_CALL_TRAMPOLINE                 158
#define GEAR_DISCARD_EXCEPTION               159
#define GEAR_YIELD                           160
#define GEAR_GENERATOR_RETURN                161
#define GEAR_FAST_CALL                       162
#define GEAR_FAST_RET                        163
#define GEAR_RECV_VARIADIC                   164
#define GEAR_SEND_UNPACK                     165
#define GEAR_POW                             166
#define GEAR_ASSIGN_POW                      167
#define GEAR_BIND_GLOBAL                     168
#define GEAR_COALESCE                        169
#define GEAR_SPACESHIP                       170
#define GEAR_DECLARE_ANON_CLASS              171
#define GEAR_DECLARE_ANON_INHERITED_CLASS    172
#define GEAR_FETCH_STATIC_PROP_R             173
#define GEAR_FETCH_STATIC_PROP_W             174
#define GEAR_FETCH_STATIC_PROP_RW            175
#define GEAR_FETCH_STATIC_PROP_IS            176
#define GEAR_FETCH_STATIC_PROP_FUNC_ARG      177
#define GEAR_FETCH_STATIC_PROP_UNSET         178
#define GEAR_UNSET_STATIC_PROP               179
#define GEAR_ISSET_ISEMPTY_STATIC_PROP       180
#define GEAR_FETCH_CLASS_CONSTANT            181
#define GEAR_BIND_LEXICAL                    182
#define GEAR_BIND_STATIC                     183
#define GEAR_FETCH_THIS                      184
#define GEAR_SEND_FUNC_ARG                   185
#define GEAR_ISSET_ISEMPTY_THIS              186
#define GEAR_SWITCH_LONG                     187
#define GEAR_SWITCH_STRING                   188
#define GEAR_IN_ARRAY                        189
#define GEAR_COUNT                           190
#define GEAR_GET_CLASS                       191
#define GEAR_GET_CALLED_CLASS                192
#define GEAR_GET_TYPE                        193
#define GEAR_FUNC_NUM_ARGS                   194
#define GEAR_FUNC_GET_ARGS                   195
#define GEAR_UNSET_CV                        196
#define GEAR_ISSET_ISEMPTY_CV                197
#define GEAR_FETCH_LIST_W                    198

#define GEAR_VM_LAST_OPCODE                  198

#endif
