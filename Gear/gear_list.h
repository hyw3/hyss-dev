/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_LIST_H
#define GEAR_LIST_H

#include "gear_hash.h"
#include "gear_globals.h"

BEGIN_EXTERN_C()

typedef void (*rsrc_dtor_func_t)(gear_resource *res);
#define GEAR_RSRC_DTOR_FUNC(name) void name(gear_resource *res)

typedef struct _gear_rsrc_list_dtors_entry {
	rsrc_dtor_func_t list_dtor_ex;
	rsrc_dtor_func_t plist_dtor_ex;

	const char *type_name;

	int capi_number;
	int resource_id;
} gear_rsrc_list_dtors_entry;


GEAR_API int gear_register_list_destructors_ex(rsrc_dtor_func_t ld, rsrc_dtor_func_t pld, const char *type_name, int capi_number);

void list_entry_destructor(zval *ptr);
void plist_entry_destructor(zval *ptr);

void gear_clean_capi_rsrc_dtors(int capi_number);
int gear_init_rsrc_list(void);
int gear_init_rsrc_plist(void);
void gear_close_rsrc_list(HashTable *ht);
void gear_destroy_rsrc_list(HashTable *ht);
int gear_init_rsrc_list_dtors(void);
void gear_destroy_rsrc_list_dtors(void);

GEAR_API zval* GEAR_FASTCALL gear_list_insert(void *ptr, int type);
GEAR_API int GEAR_FASTCALL gear_list_free(gear_resource *res);
GEAR_API int GEAR_FASTCALL gear_list_delete(gear_resource *res);
GEAR_API int GEAR_FASTCALL gear_list_close(gear_resource *res);

GEAR_API gear_resource *gear_register_resource(void *rsrc_pointer, int rsrc_type);
GEAR_API void *gear_fetch_resource(gear_resource *res, const char *resource_type_name, int resource_type);
GEAR_API void *gear_fetch_resource2(gear_resource *res, const char *resource_type_name, int resource_type, int resource_type2);
GEAR_API void *gear_fetch_resource_ex(zval *res, const char *resource_type_name, int resource_type);
GEAR_API void *gear_fetch_resource2_ex(zval *res, const char *resource_type_name, int resource_type, int resource_type2);

GEAR_API const char *gear_rsrc_list_get_rsrc_type(gear_resource *res);
GEAR_API int gear_fetch_list_dtor_id(const char *type_name);

GEAR_API gear_resource* gear_register_persistent_resource(const char *key, size_t key_len, void *rsrc_pointer, int rsrc_type);
GEAR_API gear_resource* gear_register_persistent_resource_ex(gear_string *key, void *rsrc_pointer, int rsrc_type);

extern GEAR_API int le_index_ptr;  /* list entry type for index pointers */

END_EXTERN_C()

#endif

