/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_VARIABLES_H
#define GEAR_VARIABLES_H

#include "gear_types.h"
#include "gear_gc.h"

BEGIN_EXTERN_C()

GEAR_API void GEAR_FASTCALL rc_dtor_func(gear_refcounted *p);
GEAR_API void GEAR_FASTCALL zval_copy_ctor_func(zval *zvalue);

static gear_always_inline void zval_ptr_dtor_nogc(zval *zval_ptr)
{
	if (Z_REFCOUNTED_P(zval_ptr) && !Z_DELREF_P(zval_ptr)) {
		rc_dtor_func(Z_COUNTED_P(zval_ptr));
	}
}

static gear_always_inline void i_zval_ptr_dtor(zval *zval_ptr GEAR_FILE_LINE_DC)
{
	if (Z_REFCOUNTED_P(zval_ptr)) {
		gear_refcounted *ref = Z_COUNTED_P(zval_ptr);
		if (!GC_DELREF(ref)) {
			rc_dtor_func(ref);
		} else {
			gc_check_possible_root(ref);
		}
	}
}

static gear_always_inline void zval_copy_ctor(zval *zvalue)
{
	if (Z_TYPE_P(zvalue) == IS_ARRAY) {
		ZVAL_ARR(zvalue, gear_array_dup(Z_ARR_P(zvalue)));
	} else if (Z_REFCOUNTED_P(zvalue)) {
		Z_ADDREF_P(zvalue);
	}
}

static gear_always_inline void zval_opt_copy_ctor(zval *zvalue)
{
	if (Z_OPT_TYPE_P(zvalue) == IS_ARRAY) {
		ZVAL_ARR(zvalue, gear_array_dup(Z_ARR_P(zvalue)));
	} else if (Z_OPT_REFCOUNTED_P(zvalue)) {
		Z_ADDREF_P(zvalue);
	}
}

static gear_always_inline void zval_ptr_dtor_str(zval *zval_ptr)
{
	if (Z_REFCOUNTED_P(zval_ptr) && !Z_DELREF_P(zval_ptr)) {
		GEAR_ASSERT(Z_TYPE_P(zval_ptr) == IS_STRING);
		GEAR_ASSERT(!ZSTR_IS_INTERNED(Z_STR_P(zval_ptr)));
		GEAR_ASSERT(!(GC_FLAGS(Z_STR_P(zval_ptr)) & IS_STR_PERSISTENT));
		efree(Z_STR_P(zval_ptr));
	}
}

GEAR_API void zval_ptr_dtor(zval *zval_ptr);
GEAR_API void zval_internal_ptr_dtor(zval *zvalue);

/* Kept for compatibility */
#define zval_dtor(zvalue) zval_ptr_dtor_nogc(zvalue)
#define zval_internal_dtor(zvalue) zval_internal_ptr_dtor(zvalue)
#define zval_dtor_func rc_dtor_func
#define zval_ptr_dtor_wrapper zval_ptr_dtor
#define zval_internal_ptr_dtor_wrapper zval_internal_ptr_dtor

GEAR_API void zval_add_ref(zval *p);

END_EXTERN_C()

#define ZVAL_PTR_DTOR zval_ptr_dtor
#define ZVAL_INTERNAL_PTR_DTOR zval_internal_ptr_dtor

#endif

