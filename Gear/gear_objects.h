/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_OBJECTS_H
#define GEAR_OBJECTS_H

#include "gear.h"

BEGIN_EXTERN_C()
GEAR_API void GEAR_FASTCALL gear_object_std_init(gear_object *object, gear_class_entry *ce);
GEAR_API gear_object* GEAR_FASTCALL gear_objects_new(gear_class_entry *ce);
GEAR_API void GEAR_FASTCALL gear_objects_clone_members(gear_object *new_object, gear_object *old_object);

GEAR_API void gear_object_std_dtor(gear_object *object);
GEAR_API void gear_objects_destroy_object(gear_object *object);
GEAR_API gear_object *gear_objects_clone_obj(zval *object);
END_EXTERN_C()

#endif /* GEAR_OBJECTS_H */

