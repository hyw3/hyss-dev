/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* If you change this file, please regenerate the gear_vm_execute.h and
 * gear_vm_opcodes.h files by running:
 * hyss gear_vm_gen.hyss
 */

GEAR_VM_COLD_CONSTCONST_HANDLER(1, GEAR_ADD, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)) {
		if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
			result = EX_VAR(opline->result.var);
			fast_long_add_function(result, op1, op2);
			GEAR_VM_NEXT_OPCODE();
		} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, ((double)Z_LVAL_P(op1)) + Z_DVAL_P(op2));
			GEAR_VM_NEXT_OPCODE();
		}
	} else if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_DOUBLE)) {
		if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, Z_DVAL_P(op1) + Z_DVAL_P(op2));
			GEAR_VM_NEXT_OPCODE();
		} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, Z_DVAL_P(op1) + ((double)Z_LVAL_P(op2)));
			GEAR_VM_NEXT_OPCODE();
		}
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	add_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(2, GEAR_SUB, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)) {
		if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
			result = EX_VAR(opline->result.var);
			fast_long_sub_function(result, op1, op2);
			GEAR_VM_NEXT_OPCODE();
		} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, ((double)Z_LVAL_P(op1)) - Z_DVAL_P(op2));
			GEAR_VM_NEXT_OPCODE();
		}
	} else if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_DOUBLE)) {
		if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, Z_DVAL_P(op1) - Z_DVAL_P(op2));
			GEAR_VM_NEXT_OPCODE();
		} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, Z_DVAL_P(op1) - ((double)Z_LVAL_P(op2)));
			GEAR_VM_NEXT_OPCODE();
		}
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	sub_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(3, GEAR_MUL, CONST|TMPVAR|CV, CONST|TMPVAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)) {
		if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
			gear_long overflow;

			result = EX_VAR(opline->result.var);
			GEAR_SIGNED_MULTIPLY_LONG(Z_LVAL_P(op1), Z_LVAL_P(op2), Z_LVAL_P(result), Z_DVAL_P(result), overflow);
			Z_TYPE_INFO_P(result) = overflow ? IS_DOUBLE : IS_LONG;
			GEAR_VM_NEXT_OPCODE();
		} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, ((double)Z_LVAL_P(op1)) * Z_DVAL_P(op2));
			GEAR_VM_NEXT_OPCODE();
		}
	} else if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_DOUBLE)) {
		if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, Z_DVAL_P(op1) * Z_DVAL_P(op2));
			GEAR_VM_NEXT_OPCODE();
		} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
			result = EX_VAR(opline->result.var);
			ZVAL_DOUBLE(result, Z_DVAL_P(op1) * ((double)Z_LVAL_P(op2)));
			GEAR_VM_NEXT_OPCODE();
		}
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	mul_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(4, GEAR_DIV, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR(BP_VAR_R);
	fast_div_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_HELPER(gear_capi_by_zero_helper, ANY, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	gear_throw_exception_ex(gear_ce_division_by_zero_error, 0, "Modulo by zero");
	ZVAL_UNDEF(EX_VAR(opline->result.var));
	HANDLE_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(5, GEAR_MOD, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)) {
		if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
			result = EX_VAR(opline->result.var);
			if (UNEXPECTED(Z_LVAL_P(op2) == 0)) {
				GEAR_VM_DISPATCH_TO_HELPER(gear_capi_by_zero_helper);
			} else if (UNEXPECTED(Z_LVAL_P(op2) == -1)) {
				/* Prevent overflow error/crash if op1==GEAR_LONG_MIN */
				ZVAL_LONG(result, 0);
			} else {
				ZVAL_LONG(result, Z_LVAL_P(op1) % Z_LVAL_P(op2));
			}
			GEAR_VM_NEXT_OPCODE();
		}
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	capi_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(6, GEAR_SL, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)
			&& EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)
			&& EXPECTED((gear_ulong)Z_LVAL_P(op2) < SIZEOF_GEAR_LONG * 8)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(op1) << Z_LVAL_P(op2));
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	shift_left_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(7, GEAR_SR, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)
			&& EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)
			&& EXPECTED((gear_ulong)Z_LVAL_P(op2) < SIZEOF_GEAR_LONG * 8)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(op1) >> Z_LVAL_P(op2));
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	shift_right_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(166, GEAR_POW, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR(BP_VAR_R);
	pow_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(8, GEAR_CONCAT, CONST|TMPVAR|CV, CONST|TMPVAR|CV, SPEC(NO_CONST_CONST))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);

	if ((OP1_TYPE == IS_CONST || EXPECTED(Z_TYPE_P(op1) == IS_STRING)) &&
	    (OP2_TYPE == IS_CONST || EXPECTED(Z_TYPE_P(op2) == IS_STRING))) {
		gear_string *op1_str = Z_STR_P(op1);
		gear_string *op2_str = Z_STR_P(op2);
		gear_string *str;

		if (OP1_TYPE != IS_CONST && UNEXPECTED(ZSTR_LEN(op1_str) == 0)) {
			if (OP2_TYPE == IS_CONST || OP2_TYPE == IS_CV) {
				ZVAL_STR_COPY(EX_VAR(opline->result.var), op2_str);
			} else {
				ZVAL_STR(EX_VAR(opline->result.var), op2_str);
			}
			FREE_OP1();
		} else if (OP2_TYPE != IS_CONST && UNEXPECTED(ZSTR_LEN(op2_str) == 0)) {
			if (OP1_TYPE == IS_CONST || OP1_TYPE == IS_CV) {
				ZVAL_STR_COPY(EX_VAR(opline->result.var), op1_str);
			} else {
				ZVAL_STR(EX_VAR(opline->result.var), op1_str);
			}
			FREE_OP2();
		} else if (OP1_TYPE != IS_CONST && OP1_TYPE != IS_CV &&
		    !ZSTR_IS_INTERNED(op1_str) && GC_REFCOUNT(op1_str) == 1) {
		    size_t len = ZSTR_LEN(op1_str);

			str = gear_string_extend(op1_str, len + ZSTR_LEN(op2_str), 0);
			memcpy(ZSTR_VAL(str) + len, ZSTR_VAL(op2_str), ZSTR_LEN(op2_str)+1);
			ZVAL_NEW_STR(EX_VAR(opline->result.var), str);
			FREE_OP2();
		} else {
			str = gear_string_alloc(ZSTR_LEN(op1_str) + ZSTR_LEN(op2_str), 0);
			memcpy(ZSTR_VAL(str), ZSTR_VAL(op1_str), ZSTR_LEN(op1_str));
			memcpy(ZSTR_VAL(str) + ZSTR_LEN(op1_str), ZSTR_VAL(op2_str), ZSTR_LEN(op2_str)+1);
			ZVAL_NEW_STR(EX_VAR(opline->result.var), str);
			FREE_OP1();
			FREE_OP2();
		}
		GEAR_VM_NEXT_OPCODE();
	} else {
		SAVE_OPLINE();

		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op1) == IS_UNDEF)) {
			op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
		}
		if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op2) == IS_UNDEF)) {
			op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
		}
		concat_function(EX_VAR(opline->result.var), op1, op2);
		FREE_OP1();
		FREE_OP2();
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
}

GEAR_VM_COLD_CONSTCONST_HANDLER(15, GEAR_IS_IDENTICAL, CONST|TMP|VAR|CV, CONST|TMP|VAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;
	int result;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_DEREF(BP_VAR_R);
	result = fast_is_identical_function(op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_SMART_BRANCH(result, 1);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(16, GEAR_IS_NOT_IDENTICAL, CONST|TMP|VAR|CV, CONST|TMP|VAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;
	int result;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_DEREF(BP_VAR_R);
	result = fast_is_not_identical_function(op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_SMART_BRANCH(result, 1);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(17, GEAR_IS_EQUAL, CONST|TMPVAR|CV, CONST|TMPVAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	do {
		int result;

		if (EXPECTED(Z_TYPE_P(op1) == IS_LONG)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_LONG)) {
				result = (Z_LVAL_P(op1) == Z_LVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_P(op2) == IS_DOUBLE)) {
				result = ((double)Z_LVAL_P(op1) == Z_DVAL_P(op2));
			} else {
				break;
			}
		} else if (EXPECTED(Z_TYPE_P(op1) == IS_DOUBLE)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_DOUBLE)) {
				result = (Z_DVAL_P(op1) == Z_DVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_P(op2) == IS_LONG)) {
				result = (Z_DVAL_P(op1) == ((double)Z_LVAL_P(op2)));
			} else {
				break;
			}
		} else if (EXPECTED(Z_TYPE_P(op1) == IS_STRING)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_STRING)) {
				result = gear_fast_equal_strings(Z_STR_P(op1), Z_STR_P(op2));
				FREE_OP1();
				FREE_OP2();
			} else {
				break;
			}
		} else {
			break;
		}
		GEAR_VM_SMART_BRANCH(result, 0);
		ZVAL_BOOL(EX_VAR(opline->result.var), result);
		GEAR_VM_NEXT_OPCODE();
	} while (0);

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	result = EX_VAR(opline->result.var);
	compare_function(result, op1, op2);
	ZVAL_BOOL(result, Z_LVAL_P(result) == 0);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(18, GEAR_IS_NOT_EQUAL, CONST|TMPVAR|CV, CONST|TMPVAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	do {
		int result;

		if (EXPECTED(Z_TYPE_P(op1) == IS_LONG)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_LONG)) {
				result = (Z_LVAL_P(op1) != Z_LVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_P(op2) == IS_DOUBLE)) {
				result = ((double)Z_LVAL_P(op1) != Z_DVAL_P(op2));
			} else {
				break;
			}
		} else if (EXPECTED(Z_TYPE_P(op1) == IS_DOUBLE)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_DOUBLE)) {
				result = (Z_DVAL_P(op1) != Z_DVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_P(op2) == IS_LONG)) {
				result = (Z_DVAL_P(op1) != ((double)Z_LVAL_P(op2)));
			} else {
				break;
			}
		} else if (EXPECTED(Z_TYPE_P(op1) == IS_STRING)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_STRING)) {
				result = !gear_fast_equal_strings(Z_STR_P(op1), Z_STR_P(op2));
				FREE_OP1();
				FREE_OP2();
			} else {
				break;
			}
		} else {
			break;
		}
		GEAR_VM_SMART_BRANCH(result, 0);
		ZVAL_BOOL(EX_VAR(opline->result.var), result);
		GEAR_VM_NEXT_OPCODE();
	} while (0);

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	result = EX_VAR(opline->result.var);
	compare_function(result, op1, op2);
	ZVAL_BOOL(result, Z_LVAL_P(result) != 0);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(19, GEAR_IS_SMALLER, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	do {
		int result;

		if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)) {
			if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
				result = (Z_LVAL_P(op1) < Z_LVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
				result = ((double)Z_LVAL_P(op1) < Z_DVAL_P(op2));
			} else {
				break;
			}
		} else if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_DOUBLE)) {
			if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
				result = (Z_DVAL_P(op1) < Z_DVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
				result = (Z_DVAL_P(op1) < ((double)Z_LVAL_P(op2)));
			} else {
				break;
			}
		} else {
			break;
		}
		GEAR_VM_SMART_BRANCH(result, 0);
		ZVAL_BOOL(EX_VAR(opline->result.var), result);
		GEAR_VM_NEXT_OPCODE();
	} while (0);

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	result = EX_VAR(opline->result.var);
	compare_function(result, op1, op2);
	ZVAL_BOOL(result, Z_LVAL_P(result) < 0);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(20, GEAR_IS_SMALLER_OR_EQUAL, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	do {
		int result;

		if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)) {
			if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
				result = (Z_LVAL_P(op1) <= Z_LVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
				result = ((double)Z_LVAL_P(op1) <= Z_DVAL_P(op2));
			} else {
				break;
			}
		} else if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_DOUBLE)) {
			if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_DOUBLE)) {
				result = (Z_DVAL_P(op1) <= Z_DVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
				result = (Z_DVAL_P(op1) <= ((double)Z_LVAL_P(op2)));
			} else {
				break;
			}
		} else {
			break;
		}
		GEAR_VM_SMART_BRANCH(result, 0);
		ZVAL_BOOL(EX_VAR(opline->result.var), result);
		GEAR_VM_NEXT_OPCODE();
	} while (0);

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	result = EX_VAR(opline->result.var);
	compare_function(result, op1, op2);
	ZVAL_BOOL(result, Z_LVAL_P(result) <= 0);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(170, GEAR_SPACESHIP, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR(BP_VAR_R);
	compare_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(9, GEAR_BW_OR, CONST|TMPVAR|CV, CONST|TMPVAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)
			&& EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(op1) | Z_LVAL_P(op2));
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	bitwise_or_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(10, GEAR_BW_AND, CONST|TMPVAR|CV, CONST|TMPVAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)
			&& EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(op1) & Z_LVAL_P(op2));
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	bitwise_and_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(11, GEAR_BW_XOR, CONST|TMPVAR|CV, CONST|TMPVAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)
			&& EXPECTED(Z_TYPE_INFO_P(op2) == IS_LONG)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(op1) ^ Z_LVAL_P(op2));
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	bitwise_xor_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(14, GEAR_BOOL_XOR, CONST|TMPVAR|CV, CONST|TMPVAR|CV, SPEC(COMMUTATIVE))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR(BP_VAR_R);
	boolean_xor_function(EX_VAR(opline->result.var), op1, op2);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONST_HANDLER(12, GEAR_BW_NOT, CONST|TMPVAR|CV, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *op1;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_INFO_P(op1) == IS_LONG)) {
		ZVAL_LONG(EX_VAR(opline->result.var), ~Z_LVAL_P(op1));
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	bitwise_not_function(EX_VAR(opline->result.var),
		GET_OP1_ZVAL_PTR(BP_VAR_R));
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONST_HANDLER(13, GEAR_BOOL_NOT, CONST|TMPVAR|CV, ANY)
{
	USE_OPLINE
	zval *val;
	gear_free_op free_op1;

	val = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (Z_TYPE_INFO_P(val) == IS_TRUE) {
		ZVAL_FALSE(EX_VAR(opline->result.var));
	} else if (EXPECTED(Z_TYPE_INFO_P(val) <= IS_TRUE)) {
		ZVAL_TRUE(EX_VAR(opline->result.var));
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(val) == IS_UNDEF)) {
			SAVE_OPLINE();
			GET_OP1_UNDEF_CV(val, BP_VAR_R);
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		}
	} else {
		SAVE_OPLINE();
		ZVAL_BOOL(EX_VAR(opline->result.var), !i_gear_is_true(val));
		FREE_OP1();
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_HELPER(gear_this_not_in_object_context_helper, ANY, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	gear_throw_error(NULL, "Using $this when not in object context");
	if ((opline+1)->opcode == GEAR_OP_DATA) {
		FREE_UNFETCHED_OP_DATA();
	}
	FREE_UNFETCHED_OP2();
	UNDEF_RESULT();
	HANDLE_EXCEPTION();
}

GEAR_VM_COLD_HELPER(gear_abstract_method_helper, ANY, ANY, gear_function *fbc)
{
	USE_OPLINE

	SAVE_OPLINE();
	gear_throw_error(NULL, "Cannot call abstract method %s::%s()", ZSTR_VAL(fbc->common.scope->name), ZSTR_VAL(fbc->common.function_name));
	UNDEF_RESULT();
	HANDLE_EXCEPTION();
}

GEAR_VM_COLD_HELPER(gear_undefined_function_helper, ANY, ANY, zval *function_name)
{
	SAVE_OPLINE();
	gear_throw_error(NULL, "Call to undefined function %s()", Z_STRVAL_P(function_name));
	HANDLE_EXCEPTION();
}

GEAR_VM_HELPER(gear_binary_assign_op_obj_helper, VAR|UNUSED|CV, CONST|TMPVAR|CV, binary_op_type binary_op)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2, free_op_data1;
	zval *object;
	zval *property;
	zval *value;
	zval *zptr;

	SAVE_OPLINE();
	object = GET_OP1_OBJ_ZVAL_PTR_PTR(BP_VAR_RW);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	property = GET_OP2_ZVAL_PTR(BP_VAR_R);

	do {
		value = get_op_data_zval_ptr_r((opline+1)->op1_type, (opline+1)->op1, &free_op_data1);

		if (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) != IS_OBJECT)) {
			if (Z_ISREF_P(object)) {
				object = Z_REFVAL_P(object);
				if (EXPECTED(Z_TYPE_P(object) == IS_OBJECT)) {
					GEAR_VM_C_GOTO(assign_op_object);
				}
			}
			if (UNEXPECTED(!make_real_object(object, property OPLINE_CC EXECUTE_DATA_CC))) {
				break;
			}
		}

		/* here we are sure we are dealing with an object */
GEAR_VM_C_LABEL(assign_op_object):
		if (EXPECTED(Z_OBJ_HT_P(object)->get_property_ptr_ptr)
			&& EXPECTED((zptr = Z_OBJ_HT_P(object)->get_property_ptr_ptr(object, property, BP_VAR_RW, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR((opline+1)->extended_value) : NULL))) != NULL)) {
			if (UNEXPECTED(Z_ISERROR_P(zptr))) {
				if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
					ZVAL_NULL(EX_VAR(opline->result.var));
				}
			} else {
				ZVAL_DEREF(zptr);

				binary_op(zptr, zptr, value);
				if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
					ZVAL_COPY(EX_VAR(opline->result.var), zptr);
				}
			}
		} else {
			gear_assign_op_overloaded_property(object, property, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR((opline+1)->extended_value) : NULL), value, binary_op OPLINE_CC EXECUTE_DATA_CC);
		}
	} while (0);

	FREE_OP(free_op_data1);
	FREE_OP2();
	FREE_OP1_VAR_PTR();
	/* assign_obj has two opcodes! */
	GEAR_VM_NEXT_OPCODE_EX(1, 2);
}

GEAR_VM_HELPER(gear_binary_assign_op_dim_helper, VAR|CV, CONST|TMPVAR|UNUSED|CV, binary_op_type binary_op)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2, free_op_data1;
	zval *var_ptr;
	zval *value, *container, *dim;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);

	if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
GEAR_VM_C_LABEL(assign_dim_op_array):
		SEPARATE_ARRAY(container);
GEAR_VM_C_LABEL(assign_dim_op_new_array):
		dim = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
		if (OP2_TYPE == IS_UNUSED) {
			var_ptr = gear_hash_next_index_insert(Z_ARRVAL_P(container), &EG(uninitialized_zval));
			if (UNEXPECTED(!var_ptr)) {
				gear_cannot_add_element();
				GEAR_VM_C_GOTO(assign_dim_op_ret_null);
			}
		} else {
			if (OP2_TYPE == IS_CONST) {
				var_ptr = gear_fetch_dimension_address_inner_RW_CONST(Z_ARRVAL_P(container), dim EXECUTE_DATA_CC);
			} else {
				var_ptr = gear_fetch_dimension_address_inner_RW(Z_ARRVAL_P(container), dim EXECUTE_DATA_CC);
			}
			if (UNEXPECTED(!var_ptr)) {
				GEAR_VM_C_GOTO(assign_dim_op_ret_null);
			}
			ZVAL_DEREF(var_ptr);
		}

		value = get_op_data_zval_ptr_r((opline+1)->op1_type, (opline+1)->op1, &free_op_data1);

		binary_op(var_ptr, var_ptr, value);

		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_COPY(EX_VAR(opline->result.var), var_ptr);
		}
	} else {
		if (EXPECTED(Z_ISREF_P(container))) {
			container = Z_REFVAL_P(container);
			if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
				GEAR_VM_C_GOTO(assign_dim_op_array);
			}
		} else if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(container) == IS_UNDEF)) {
			container = GET_OP1_UNDEF_CV(container, BP_VAR_RW);
GEAR_VM_C_LABEL(assign_dim_op_convert_to_array):
			ZVAL_ARR(container, gear_new_array(8));
			GEAR_VM_C_GOTO(assign_dim_op_new_array);
		}

		dim = GET_OP2_ZVAL_PTR(BP_VAR_R);

		if (EXPECTED(Z_TYPE_P(container) == IS_OBJECT)) {
			value = get_op_data_zval_ptr_r((opline+1)->op1_type, (opline+1)->op1, &free_op_data1);
			if (OP2_TYPE == IS_CONST && Z_EXTRA_P(dim) == GEAR_EXTRA_VALUE) {
				dim++;
			}
			gear_binary_assign_op_obj_dim(container, dim, value, UNEXPECTED(RETURN_VALUE_USED(opline)) ? EX_VAR(opline->result.var) : NULL, binary_op EXECUTE_DATA_CC);
		} else {
			if (UNEXPECTED(Z_TYPE_P(container) == IS_STRING)) {
				if (OP2_TYPE == IS_UNUSED) {
					gear_use_new_element_for_string();
				} else {
					gear_check_string_offset(dim, BP_VAR_RW EXECUTE_DATA_CC);
					gear_wrong_string_offset(EXECUTE_DATA_C);
				}
				UNDEF_RESULT();
			} else if (EXPECTED(Z_TYPE_P(container) <= IS_FALSE)) {
				GEAR_VM_C_GOTO(assign_dim_op_convert_to_array);
			} else {
				if (UNEXPECTED(OP1_TYPE != IS_VAR || EXPECTED(!Z_ISERROR_P(container)))) {
					gear_use_scalar_as_array();
				}
GEAR_VM_C_LABEL(assign_dim_op_ret_null):
				if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
					ZVAL_NULL(EX_VAR(opline->result.var));
				}
			}
			value = get_op_data_zval_ptr_r((opline+1)->op1_type, (opline+1)->op1, &free_op_data1);
		}
	}

	FREE_OP2();
	FREE_OP(free_op_data1);
	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_EX(1, 2);
}

GEAR_VM_HELPER(gear_binary_assign_op_simple_helper, VAR|CV, CONST|TMPVAR|CV, binary_op_type binary_op)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *var_ptr;
	zval *value;

	SAVE_OPLINE();
	value = GET_OP2_ZVAL_PTR(BP_VAR_R);
	var_ptr = GET_OP1_ZVAL_PTR_PTR(BP_VAR_RW);

	if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(var_ptr))) {
		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_NULL(EX_VAR(opline->result.var));
		}
	} else {
		ZVAL_DEREF(var_ptr);

		binary_op(var_ptr, var_ptr, value);

		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_COPY(EX_VAR(opline->result.var), var_ptr);
		}
	}

	FREE_OP2();
	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_INLINE_HELPER(gear_binary_assign_op_helper, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, SPEC(DIM_OBJ), binary_op_type binary_op)
{
#if defined(GEAR_VM_SPEC) && OP2_TYPE == IS_UNUSED
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_dim_helper, binary_op, binary_op);
#else
# if !defined(GEAR_VM_SPEC) || OP1_TYPE != IS_UNUSED
#  if !defined(GEAR_VM_SPEC)
	/* opline->extended_value checks are specialized, don't need opline */
	USE_OPLINE
#  endif

	if (EXPECTED(opline->extended_value == 0)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_simple_helper, binary_op, binary_op);
	}
	if (EXPECTED(opline->extended_value == GEAR_ASSIGN_DIM)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_dim_helper, binary_op, binary_op);
	}
# endif

	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_obj_helper, binary_op, binary_op);
#endif
}

GEAR_VM_HANDLER(23, GEAR_ASSIGN_ADD, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, add_function);
}

GEAR_VM_HANDLER(24, GEAR_ASSIGN_SUB, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, sub_function);
}

GEAR_VM_HANDLER(25, GEAR_ASSIGN_MUL, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, mul_function);
}

GEAR_VM_HANDLER(26, GEAR_ASSIGN_DIV, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, div_function);
}

GEAR_VM_HANDLER(27, GEAR_ASSIGN_MOD, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, capi_function);
}

GEAR_VM_HANDLER(28, GEAR_ASSIGN_SL, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, shift_left_function);
}

GEAR_VM_HANDLER(29, GEAR_ASSIGN_SR, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, shift_right_function);
}

GEAR_VM_HANDLER(30, GEAR_ASSIGN_CONCAT, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, concat_function);
}

GEAR_VM_HANDLER(31, GEAR_ASSIGN_BW_OR, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, bitwise_or_function);
}

GEAR_VM_HANDLER(32, GEAR_ASSIGN_BW_AND, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, bitwise_and_function);
}

GEAR_VM_HANDLER(33, GEAR_ASSIGN_BW_XOR, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, bitwise_xor_function);
}

GEAR_VM_HANDLER(167, GEAR_ASSIGN_POW, VAR|UNUSED|THIS|CV, CONST|TMPVAR|UNUSED|NEXT|CV, DIM_OBJ|CACHE_SLOT, SPEC(DIM_OBJ))
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_binary_assign_op_helper, binary_op, pow_function);
}

GEAR_VM_HELPER(gear_pre_incdec_property_helper, VAR|UNUSED|CV, CONST|TMPVAR|CV, int inc)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *object;
	zval *property;
	zval *zptr;

	SAVE_OPLINE();
	object = GET_OP1_OBJ_ZVAL_PTR_PTR(BP_VAR_RW);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	property = GET_OP2_ZVAL_PTR(BP_VAR_R);

	do {
		if (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) != IS_OBJECT)) {
			if (Z_ISREF_P(object)) {
				object = Z_REFVAL_P(object);
				if (EXPECTED(Z_TYPE_P(object) == IS_OBJECT)) {
					GEAR_VM_C_GOTO(pre_incdec_object);
				}
			}
			if (UNEXPECTED(!make_real_object(object, property OPLINE_CC EXECUTE_DATA_CC))) {
				break;
			}
		}

		/* here we are sure we are dealing with an object */
GEAR_VM_C_LABEL(pre_incdec_object):
		if (EXPECTED(Z_OBJ_HT_P(object)->get_property_ptr_ptr)
			&& EXPECTED((zptr = Z_OBJ_HT_P(object)->get_property_ptr_ptr(object, property, BP_VAR_RW, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL))) != NULL)) {
			if (UNEXPECTED(Z_ISERROR_P(zptr))) {
				if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
					ZVAL_NULL(EX_VAR(opline->result.var));
				}
			} else {
				if (EXPECTED(Z_TYPE_P(zptr) == IS_LONG)) {
					if (inc) {
						fast_long_increment_function(zptr);
					} else {
						fast_long_decrement_function(zptr);
					}
				} else {
					ZVAL_DEREF(zptr);

					if (inc) {
						increment_function(zptr);
					} else {
						decrement_function(zptr);
					}
				}
				if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
					ZVAL_COPY(EX_VAR(opline->result.var), zptr);
				}
			}
		} else {
			gear_pre_incdec_overloaded_property(object, property, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL), inc OPLINE_CC EXECUTE_DATA_CC);
		}
	} while (0);

	FREE_OP2();
	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(132, GEAR_PRE_INC_OBJ, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_pre_incdec_property_helper, inc, 1);
}

GEAR_VM_HANDLER(133, GEAR_PRE_DEC_OBJ, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_pre_incdec_property_helper, inc, 0);
}

GEAR_VM_HELPER(gear_post_incdec_property_helper, VAR|UNUSED|CV, CONST|TMPVAR|CV, int inc)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *object;
	zval *property;
	zval *zptr;

	SAVE_OPLINE();
	object = GET_OP1_OBJ_ZVAL_PTR_PTR(BP_VAR_RW);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	property = GET_OP2_ZVAL_PTR(BP_VAR_R);

	do {
		if (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) != IS_OBJECT)) {
			if (Z_ISREF_P(object)) {
				object = Z_REFVAL_P(object);
				if (EXPECTED(Z_TYPE_P(object) == IS_OBJECT)) {
					GEAR_VM_C_GOTO(post_incdec_object);
				}
			}
			if (UNEXPECTED(!make_real_object(object, property OPLINE_CC EXECUTE_DATA_CC))) {
				break;
			}
		}

		/* here we are sure we are dealing with an object */
GEAR_VM_C_LABEL(post_incdec_object):
		if (EXPECTED(Z_OBJ_HT_P(object)->get_property_ptr_ptr)
			&& EXPECTED((zptr = Z_OBJ_HT_P(object)->get_property_ptr_ptr(object, property, BP_VAR_RW, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL))) != NULL)) {
			if (UNEXPECTED(Z_ISERROR_P(zptr))) {
				ZVAL_NULL(EX_VAR(opline->result.var));
			} else {
				if (EXPECTED(Z_TYPE_P(zptr) == IS_LONG)) {
					ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(zptr));
					if (inc) {
						fast_long_increment_function(zptr);
					} else {
						fast_long_decrement_function(zptr);
					}
				} else {
					ZVAL_DEREF(zptr);
					ZVAL_COPY(EX_VAR(opline->result.var), zptr);
					if (inc) {
						increment_function(zptr);
					} else {
						decrement_function(zptr);
					}
				}
			}
		} else {
			gear_post_incdec_overloaded_property(object, property, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL), inc OPLINE_CC EXECUTE_DATA_CC);
		}
	} while (0);

	FREE_OP2();
	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(134, GEAR_POST_INC_OBJ, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_post_incdec_property_helper, inc, 1);
}

GEAR_VM_HANDLER(135, GEAR_POST_DEC_OBJ, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_post_incdec_property_helper, inc, 0);
}

GEAR_VM_HANDLER(34, GEAR_PRE_INC, VAR|CV, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);

	if (EXPECTED(Z_TYPE_P(var_ptr) == IS_LONG)) {
		fast_long_increment_function(var_ptr);
		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_COPY_VALUE(EX_VAR(opline->result.var), var_ptr);
		}
		GEAR_VM_NEXT_OPCODE();
	}

	if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(var_ptr))) {
		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_NULL(EX_VAR(opline->result.var));
		}
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(var_ptr) == IS_UNDEF)) {
		var_ptr = GET_OP1_UNDEF_CV(var_ptr, BP_VAR_RW);
	}
	ZVAL_DEREF(var_ptr);

	increment_function(var_ptr);

	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_COPY(EX_VAR(opline->result.var), var_ptr);
	}

	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(35, GEAR_PRE_DEC, VAR|CV, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);

	if (EXPECTED(Z_TYPE_P(var_ptr) == IS_LONG)) {
		fast_long_decrement_function(var_ptr);
		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_COPY_VALUE(EX_VAR(opline->result.var), var_ptr);
		}
		GEAR_VM_NEXT_OPCODE();
	}

	if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(var_ptr))) {
		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_NULL(EX_VAR(opline->result.var));
		}
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(var_ptr) == IS_UNDEF)) {
		var_ptr = GET_OP1_UNDEF_CV(var_ptr, BP_VAR_RW);
	}
	ZVAL_DEREF(var_ptr);

	decrement_function(var_ptr);

	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_COPY(EX_VAR(opline->result.var), var_ptr);
	}

	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(36, GEAR_POST_INC, VAR|CV, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);

	if (EXPECTED(Z_TYPE_P(var_ptr) == IS_LONG)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
		fast_long_increment_function(var_ptr);
		GEAR_VM_NEXT_OPCODE();
	}

	if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(var_ptr))) {
		ZVAL_NULL(EX_VAR(opline->result.var));
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(var_ptr) == IS_UNDEF)) {
		var_ptr = GET_OP1_UNDEF_CV(var_ptr, BP_VAR_RW);
	}
	ZVAL_DEREF(var_ptr);
	ZVAL_COPY(EX_VAR(opline->result.var), var_ptr);

	increment_function(var_ptr);

	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(37, GEAR_POST_DEC, VAR|CV, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);

	if (EXPECTED(Z_TYPE_P(var_ptr) == IS_LONG)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
		fast_long_decrement_function(var_ptr);
		GEAR_VM_NEXT_OPCODE();
	}

	if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(var_ptr))) {
		ZVAL_NULL(EX_VAR(opline->result.var));
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(var_ptr) == IS_UNDEF)) {
		var_ptr = GET_OP1_UNDEF_CV(var_ptr, BP_VAR_RW);
	}
	ZVAL_DEREF(var_ptr);
	ZVAL_COPY(EX_VAR(opline->result.var), var_ptr);

	decrement_function(var_ptr);

	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(40, GEAR_ECHO, CONST|TMPVAR|CV, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *z;

	SAVE_OPLINE();
	z = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (Z_TYPE_P(z) == IS_STRING) {
		gear_string *str = Z_STR_P(z);

		if (ZSTR_LEN(str) != 0) {
			gear_write(ZSTR_VAL(str), ZSTR_LEN(str));
		}
	} else {
		gear_string *str = zval_get_string_func(z);

		if (ZSTR_LEN(str) != 0) {
			gear_write(ZSTR_VAL(str), ZSTR_LEN(str));
		} else if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(z) == IS_UNDEF)) {
			GET_OP1_UNDEF_CV(z, BP_VAR_R);
		}
		gear_string_release_ex(str, 0);
	}

	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HELPER(gear_fetch_var_address_helper, CONST|TMPVAR|CV, UNUSED, int type)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *varname;
	zval *retval;
	gear_string *name, *tmp_name;
	HashTable *target_symbol_table;

	SAVE_OPLINE();
	varname = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (OP1_TYPE == IS_CONST) {
		name = Z_STR_P(varname);
	} else if (EXPECTED(Z_TYPE_P(varname) == IS_STRING)) {
		name = Z_STR_P(varname);
		tmp_name = NULL;
	} else {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(varname) == IS_UNDEF)) {
			GET_OP1_UNDEF_CV(varname, BP_VAR_R);
		}
		name = zval_get_tmp_string(varname, &tmp_name);
	}

	target_symbol_table = gear_get_target_symbol_table(opline->extended_value EXECUTE_DATA_CC);
	retval = gear_hash_find_ex(target_symbol_table, name, OP1_TYPE == IS_CONST);
	if (retval == NULL) {
		if (UNEXPECTED(gear_string_equals(name, ZSTR_KNOWN(GEAR_STR_THIS)))) {
GEAR_VM_C_LABEL(fetch_this):
			gear_fetch_this_var(type OPLINE_CC EXECUTE_DATA_CC);
			if (OP1_TYPE != IS_CONST) {
				gear_tmp_string_release(tmp_name);
			}
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		}
		if (type == BP_VAR_W) {
			retval = gear_hash_add_new(target_symbol_table, name, &EG(uninitialized_zval));
		} else if (type == BP_VAR_IS) {
			retval = &EG(uninitialized_zval);
		} else {
			gear_error(E_NOTICE,"Undefined variable: %s", ZSTR_VAL(name));
			if (type == BP_VAR_RW) {
				retval = gear_hash_update(target_symbol_table, name, &EG(uninitialized_zval));
			} else {
				retval = &EG(uninitialized_zval);
			}
		}
	/* GLOBAL or $$name variable may be an INDIRECT pointer to CV */
	} else if (Z_TYPE_P(retval) == IS_INDIRECT) {
		retval = Z_INDIRECT_P(retval);
		if (Z_TYPE_P(retval) == IS_UNDEF) {
			if (UNEXPECTED(gear_string_equals(name, ZSTR_KNOWN(GEAR_STR_THIS)))) {
				GEAR_VM_C_GOTO(fetch_this);
			}
			if (type == BP_VAR_W) {
				ZVAL_NULL(retval);
			} else if (type == BP_VAR_IS) {
				retval = &EG(uninitialized_zval);
			} else {
				gear_error(E_NOTICE,"Undefined variable: %s", ZSTR_VAL(name));
				if (type == BP_VAR_RW) {
					ZVAL_NULL(retval);
				} else {
					retval = &EG(uninitialized_zval);
				}
			}
		}
	}

	if (!(opline->extended_value & GEAR_FETCH_GLOBAL_LOCK)) {
		FREE_OP1();
	}

	if (OP1_TYPE != IS_CONST) {
		gear_tmp_string_release(tmp_name);
	}

	GEAR_ASSERT(retval != NULL);
	if (type == BP_VAR_R || type == BP_VAR_IS) {
		ZVAL_COPY_DEREF(EX_VAR(opline->result.var), retval);
	} else {
		ZVAL_INDIRECT(EX_VAR(opline->result.var), retval);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(80, GEAR_FETCH_R, CONST|TMPVAR|CV, UNUSED, VAR_FETCH)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_var_address_helper, type, BP_VAR_R);
}

GEAR_VM_HANDLER(83, GEAR_FETCH_W, CONST|TMPVAR|CV, UNUSED, VAR_FETCH)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_var_address_helper, type, BP_VAR_W);
}

GEAR_VM_HANDLER(86, GEAR_FETCH_RW, CONST|TMPVAR|CV, UNUSED, VAR_FETCH)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_var_address_helper, type, BP_VAR_RW);
}

GEAR_VM_HANDLER(92, GEAR_FETCH_FUNC_ARG, CONST|TMPVAR|CV, UNUSED, VAR_FETCH)
{
	int fetch_type =
		(UNEXPECTED(GEAR_CALL_INFO(EX(call)) & GEAR_CALL_SEND_ARG_BY_REF)) ?
			BP_VAR_W : BP_VAR_R;
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_var_address_helper, type, fetch_type);
}

GEAR_VM_HANDLER(95, GEAR_FETCH_UNSET, CONST|TMPVAR|CV, UNUSED, VAR_FETCH)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_var_address_helper, type, BP_VAR_UNSET);
}

GEAR_VM_HANDLER(89, GEAR_FETCH_IS, CONST|TMPVAR|CV, UNUSED, VAR_FETCH)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_var_address_helper, type, BP_VAR_IS);
}

GEAR_VM_HELPER(gear_fetch_static_prop_helper, CONST|TMPVAR|CV, UNUSED|CONST|VAR, int type)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *varname;
	zval *retval;
	gear_string *name, *tmp_name;
	gear_class_entry *ce;

	SAVE_OPLINE();

	do {
		if (OP2_TYPE == IS_CONST) {
			if (OP1_TYPE == IS_CONST && EXPECTED((ce = CACHED_PTR(opline->extended_value)) != NULL)) {
				retval = CACHED_PTR(opline->extended_value + sizeof(void*));
				break;
			} else {
				zval *class_name = RT_CONSTANT(opline, opline->op2);

				if (UNEXPECTED((ce = CACHED_PTR(opline->extended_value)) == NULL)) {
					ce = gear_fetch_class_by_name(Z_STR_P(class_name), class_name + 1, GEAR_FETCH_CLASS_DEFAULT | GEAR_FETCH_CLASS_EXCEPTION);
					if (UNEXPECTED(ce == NULL)) {
						FREE_UNFETCHED_OP1();
						retval = NULL;
						break;
					}
					if (OP1_TYPE != IS_CONST) {
						CACHE_PTR(opline->extended_value, ce);
					}
				}
			}
		} else {
			if (OP2_TYPE == IS_UNUSED) {
				ce = gear_fetch_class(NULL, opline->op2.num);
				if (UNEXPECTED(ce == NULL)) {
					FREE_UNFETCHED_OP1();
					retval = NULL;
					break;
				}
			} else {
				ce = Z_CE_P(EX_VAR(opline->op2.var));
			}
			if (OP1_TYPE == IS_CONST &&
			    EXPECTED(CACHED_PTR(opline->extended_value) == ce)) {
				retval = CACHED_PTR(opline->extended_value + sizeof(void*));
				break;
			}
		}

		varname = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
		if (OP1_TYPE == IS_CONST) {
			name = Z_STR_P(varname);
		} else if (EXPECTED(Z_TYPE_P(varname) == IS_STRING)) {
			name = Z_STR_P(varname);
			tmp_name = NULL;
		} else {
			if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(varname) == IS_UNDEF)) {
				zval_undefined_cv(EX(opline)->op1.var EXECUTE_DATA_CC);
			}
			name = zval_get_tmp_string(varname, &tmp_name);
		}

		retval = gear_std_get_static_property(ce, name, type == BP_VAR_IS);

		if (OP1_TYPE != IS_CONST) {
			gear_tmp_string_release(tmp_name);
		}

		if (OP1_TYPE == IS_CONST && EXPECTED(retval)) {
			CACHE_POLYMORPHIC_PTR(opline->extended_value, ce, retval);
		}

		FREE_OP1();
	} while (0);

	if (UNEXPECTED(retval == NULL)) {
		if (EG(exception)) {
			ZVAL_UNDEF(EX_VAR(opline->result.var));
			HANDLE_EXCEPTION();
		} else {
			GEAR_ASSERT(type == BP_VAR_IS);
			retval = &EG(uninitialized_zval);
		}
	}

	if (type == BP_VAR_R || type == BP_VAR_IS) {
		ZVAL_COPY_DEREF(EX_VAR(opline->result.var), retval);
	} else {
		ZVAL_INDIRECT(EX_VAR(opline->result.var), retval);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(173, GEAR_FETCH_STATIC_PROP_R, CONST|TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_static_prop_helper, type, BP_VAR_R);
}

GEAR_VM_HANDLER(174, GEAR_FETCH_STATIC_PROP_W, CONST|TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_static_prop_helper, type, BP_VAR_W);
}

GEAR_VM_HANDLER(175, GEAR_FETCH_STATIC_PROP_RW, CONST|TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_static_prop_helper, type, BP_VAR_RW);
}

GEAR_VM_HANDLER(177, GEAR_FETCH_STATIC_PROP_FUNC_ARG, CONST|TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, CACHE_SLOT)
{
	int fetch_type =
		(UNEXPECTED(GEAR_CALL_INFO(EX(call)) & GEAR_CALL_SEND_ARG_BY_REF)) ?
			BP_VAR_W : BP_VAR_R;
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_static_prop_helper, type, fetch_type);
}

GEAR_VM_HANDLER(178, GEAR_FETCH_STATIC_PROP_UNSET, CONST|TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_static_prop_helper, type, BP_VAR_UNSET);
}

GEAR_VM_HANDLER(176, GEAR_FETCH_STATIC_PROP_IS, CONST|TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, CACHE_SLOT)
{
	GEAR_VM_DISPATCH_TO_HELPER(gear_fetch_static_prop_helper, type, BP_VAR_IS);
}

GEAR_VM_COLD_CONSTCONST_HANDLER(81, GEAR_FETCH_DIM_R, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container, *dim, *value, *result;

	SAVE_OPLINE();
	container = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	dim = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (OP1_TYPE != IS_CONST) {
		if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
GEAR_VM_C_LABEL(fetch_dim_r_array):
			value = gear_fetch_dimension_address_inner(Z_ARRVAL_P(container), dim, OP2_TYPE, BP_VAR_R EXECUTE_DATA_CC);
			result = EX_VAR(opline->result.var);
			ZVAL_COPY_DEREF(result, value);
		} else if (EXPECTED(Z_TYPE_P(container) == IS_REFERENCE)) {
			container = Z_REFVAL_P(container);
			if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
				GEAR_VM_C_GOTO(fetch_dim_r_array);
			} else {
				GEAR_VM_C_GOTO(fetch_dim_r_slow);
			}
		} else {
GEAR_VM_C_LABEL(fetch_dim_r_slow):
			if (OP2_TYPE == IS_CONST && Z_EXTRA_P(dim) == GEAR_EXTRA_VALUE) {
				dim++;
			}
			gear_fetch_dimension_address_read_R_slow(container, dim OPLINE_CC EXECUTE_DATA_CC);
		}
	} else {
		gear_fetch_dimension_address_read_R(container, dim, OP2_TYPE OPLINE_CC EXECUTE_DATA_CC);
	}
	FREE_OP2();
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(84, GEAR_FETCH_DIM_W, VAR|CV, CONST|TMPVAR|UNUSED|NEXT|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;

	SAVE_OPLINE();
	container = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);
	gear_fetch_dimension_address_W(container, GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R), OP2_TYPE OPLINE_CC EXECUTE_DATA_CC);
	FREE_OP2();
	if (OP1_TYPE == IS_VAR) {
		zval *result = EX_VAR(opline->result.var);
		FREE_VAR_PTR_AND_EXTRACT_RESULT_IF_NECESSARY(free_op1, result);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(87, GEAR_FETCH_DIM_RW, VAR|CV, CONST|TMPVAR|UNUSED|NEXT|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;

	SAVE_OPLINE();
	container = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	gear_fetch_dimension_address_RW(container, GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R), OP2_TYPE OPLINE_CC EXECUTE_DATA_CC);
	FREE_OP2();
	if (OP1_TYPE == IS_VAR) {
		zval *result = EX_VAR(opline->result.var);
		FREE_VAR_PTR_AND_EXTRACT_RESULT_IF_NECESSARY(free_op1, result);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(90, GEAR_FETCH_DIM_IS, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;

	SAVE_OPLINE();
	container = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_IS);
	gear_fetch_dimension_address_read_IS(container, GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R), OP2_TYPE OPLINE_CC EXECUTE_DATA_CC);
	FREE_OP2();
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_HELPER(gear_use_tmp_in_write_context_helper, ANY, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	gear_throw_error(NULL, "Cannot use temporary expression in write context");
	FREE_UNFETCHED_OP2();
	FREE_UNFETCHED_OP1();
	ZVAL_UNDEF(EX_VAR(opline->result.var));
	HANDLE_EXCEPTION();
}

GEAR_VM_COLD_HELPER(gear_use_undef_in_read_context_helper, ANY, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	gear_throw_error(NULL, "Cannot use [] for reading");
	FREE_UNFETCHED_OP2();
	FREE_UNFETCHED_OP1();
	ZVAL_UNDEF(EX_VAR(opline->result.var));
	HANDLE_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(93, GEAR_FETCH_DIM_FUNC_ARG, CONST|TMP|VAR|CV, CONST|TMPVAR|UNUSED|NEXT|CV)
{
	if (UNEXPECTED(GEAR_CALL_INFO(EX(call)) & GEAR_CALL_SEND_ARG_BY_REF)) {
        if ((OP1_TYPE & (IS_CONST|IS_TMP_VAR))) {
			GEAR_VM_DISPATCH_TO_HELPER(gear_use_tmp_in_write_context_helper);
        }
		GEAR_VM_DISPATCH_TO_HANDLER(GEAR_FETCH_DIM_W);
	} else {
		if (OP2_TYPE == IS_UNUSED) {
			GEAR_VM_DISPATCH_TO_HELPER(gear_use_undef_in_read_context_helper);
		}
		GEAR_VM_DISPATCH_TO_HANDLER(GEAR_FETCH_DIM_R);
	}
}

GEAR_VM_HANDLER(96, GEAR_FETCH_DIM_UNSET, VAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;

	SAVE_OPLINE();
	container = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_UNSET);
	gear_fetch_dimension_address_UNSET(container, GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R), OP2_TYPE OPLINE_CC EXECUTE_DATA_CC);
	FREE_OP2();
	if (OP1_TYPE == IS_VAR) {
		zval *result = EX_VAR(opline->result.var);
		FREE_VAR_PTR_AND_EXTRACT_RESULT_IF_NECESSARY(free_op1, result);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_OBJ_HANDLER(82, GEAR_FETCH_OBJ_R, CONST|TMPVAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *container;
	gear_free_op free_op2;
	zval *offset;
	void **cache_slot = NULL;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	offset = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (OP1_TYPE == IS_CONST ||
	    (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) != IS_OBJECT))) {
	    do {
			if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(container)) {
				container = Z_REFVAL_P(container);
				if (EXPECTED(Z_TYPE_P(container) == IS_OBJECT)) {
					break;
				}
			}
			if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
				GET_OP1_UNDEF_CV(container, BP_VAR_R);
			}
			if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(offset) == IS_UNDEF)) {
				GET_OP2_UNDEF_CV(offset, BP_VAR_R);
			}
			GEAR_VM_C_GOTO(fetch_obj_r_no_object);
		} while (0);
	}

	/* here we are sure we are dealing with an object */
	do {
		gear_object *zobj = Z_OBJ_P(container);
		zval *retval;

		if (OP2_TYPE == IS_CONST) {
			cache_slot = CACHE_ADDR(opline->extended_value);

			if (EXPECTED(zobj->ce == CACHED_PTR_EX(cache_slot))) {
				uintptr_t prop_offset = (uintptr_t)CACHED_PTR_EX(cache_slot + 1);

				if (EXPECTED(IS_VALID_PROPERTY_OFFSET(prop_offset))) {
					retval = OBJ_PROP(zobj, prop_offset);
					if (EXPECTED(Z_TYPE_INFO_P(retval) != IS_UNDEF)) {
						ZVAL_COPY_DEREF(EX_VAR(opline->result.var), retval);
						break;
					}
				} else if (EXPECTED(zobj->properties != NULL)) {
					if (!IS_UNKNOWN_DYNAMIC_PROPERTY_OFFSET(prop_offset)) {
						uintptr_t idx = GEAR_DECODE_DYN_PROP_OFFSET(prop_offset);

						if (EXPECTED(idx < zobj->properties->nNumUsed * sizeof(Bucket))) {
							Bucket *p = (Bucket*)((char*)zobj->properties->arData + idx);

							if (EXPECTED(Z_TYPE(p->val) != IS_UNDEF) &&
						        (EXPECTED(p->key == Z_STR_P(offset)) ||
						         (EXPECTED(p->h == ZSTR_H(Z_STR_P(offset))) &&
						          EXPECTED(p->key != NULL) &&
						          EXPECTED(gear_string_equal_content(p->key, Z_STR_P(offset)))))) {
								ZVAL_COPY_DEREF(EX_VAR(opline->result.var), &p->val);
								break;
							}
						}
						CACHE_PTR_EX(cache_slot + 1, (void*)GEAR_DYNAMIC_PROPERTY_OFFSET);
					}
					retval = gear_hash_find_ex(zobj->properties, Z_STR_P(offset), 1);
					if (EXPECTED(retval)) {
						uintptr_t idx = (char*)retval - (char*)zobj->properties->arData;
						CACHE_PTR_EX(cache_slot + 1, (void*)GEAR_ENCODE_DYN_PROP_OFFSET(idx));
						ZVAL_COPY_DEREF(EX_VAR(opline->result.var), retval);
						break;
					}
				}
			}
		} else if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(offset) == IS_UNDEF)) {
			GET_OP2_UNDEF_CV(offset, BP_VAR_R);
		}

		if (UNEXPECTED(zobj->handlers->read_property == NULL)) {
GEAR_VM_C_LABEL(fetch_obj_r_no_object):
			gear_wrong_property_read(offset);
			ZVAL_NULL(EX_VAR(opline->result.var));
		} else {
			retval = zobj->handlers->read_property(container, offset, BP_VAR_R, cache_slot, EX_VAR(opline->result.var));

			if (retval != EX_VAR(opline->result.var)) {
				ZVAL_COPY_DEREF(EX_VAR(opline->result.var), retval);
			} else if (UNEXPECTED(Z_ISREF_P(retval))) {
				gear_unwrap_reference(retval);
			}
		}
	} while (0);

	FREE_OP2();
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(85, GEAR_FETCH_OBJ_W, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *property, *container, *result;

	SAVE_OPLINE();

	container = GET_OP1_OBJ_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);
	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	property = GET_OP2_ZVAL_PTR(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	gear_fetch_property_address(result, container, OP1_TYPE, property, OP2_TYPE, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL), BP_VAR_W OPLINE_CC);
	FREE_OP2();
	if (OP1_TYPE == IS_VAR) {
		FREE_VAR_PTR_AND_EXTRACT_RESULT_IF_NECESSARY(free_op1, result);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(88, GEAR_FETCH_OBJ_RW, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *property, *container, *result;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR_PTR(BP_VAR_RW);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}
	property = GET_OP2_ZVAL_PTR(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	gear_fetch_property_address(result, container, OP1_TYPE, property, OP2_TYPE, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL), BP_VAR_RW OPLINE_CC);
	FREE_OP2();
	if (OP1_TYPE == IS_VAR) {
		FREE_VAR_PTR_AND_EXTRACT_RESULT_IF_NECESSARY(free_op1, result);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONST_HANDLER(91, GEAR_FETCH_OBJ_IS, CONST|TMPVAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *container;
	gear_free_op free_op2;
	zval *offset;
	void **cache_slot = NULL;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR(BP_VAR_IS);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	offset = GET_OP2_ZVAL_PTR(BP_VAR_R);

	if (OP1_TYPE == IS_CONST ||
	    (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) != IS_OBJECT))) {
		do {
			if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(container)) {
				container = Z_REFVAL_P(container);
				if (EXPECTED(Z_TYPE_P(container) == IS_OBJECT)) {
					break;
				}
			}
			GEAR_VM_C_GOTO(fetch_obj_is_no_object);
		} while (0);
	}

	/* here we are sure we are dealing with an object */
	do {
		gear_object *zobj = Z_OBJ_P(container);
		zval *retval;

		if (OP2_TYPE == IS_CONST) {
			cache_slot = CACHE_ADDR(opline->extended_value);

			if (EXPECTED(zobj->ce == CACHED_PTR_EX(cache_slot))) {
				uintptr_t prop_offset = (uintptr_t)CACHED_PTR_EX(cache_slot + 1);

				if (EXPECTED(IS_VALID_PROPERTY_OFFSET(prop_offset))) {
					retval = OBJ_PROP(zobj, prop_offset);
					if (EXPECTED(Z_TYPE_P(retval) != IS_UNDEF)) {
						ZVAL_COPY(EX_VAR(opline->result.var), retval);
						break;
					}
				} else if (EXPECTED(zobj->properties != NULL)) {
					if (!IS_UNKNOWN_DYNAMIC_PROPERTY_OFFSET(prop_offset)) {
						uintptr_t idx = GEAR_DECODE_DYN_PROP_OFFSET(prop_offset);

						if (EXPECTED(idx < zobj->properties->nNumUsed * sizeof(Bucket))) {
							Bucket *p = (Bucket*)((char*)zobj->properties->arData + idx);

							if (EXPECTED(Z_TYPE(p->val) != IS_UNDEF) &&
						        (EXPECTED(p->key == Z_STR_P(offset)) ||
						         (EXPECTED(p->h == ZSTR_H(Z_STR_P(offset))) &&
						          EXPECTED(p->key != NULL) &&
						          EXPECTED(gear_string_equal_content(p->key, Z_STR_P(offset)))))) {
								ZVAL_COPY(EX_VAR(opline->result.var), &p->val);
								break;
							}
						}
						CACHE_PTR_EX(cache_slot + 1, (void*)GEAR_DYNAMIC_PROPERTY_OFFSET);
					}
					retval = gear_hash_find_ex(zobj->properties, Z_STR_P(offset), 1);
					if (EXPECTED(retval)) {
						uintptr_t idx = (char*)retval - (char*)zobj->properties->arData;
						CACHE_PTR_EX(cache_slot + 1, (void*)GEAR_ENCODE_DYN_PROP_OFFSET(idx));
						ZVAL_COPY(EX_VAR(opline->result.var), retval);
						break;
					}
				}
			}
		}

		if (UNEXPECTED(zobj->handlers->read_property == NULL)) {
GEAR_VM_C_LABEL(fetch_obj_is_no_object):
			ZVAL_NULL(EX_VAR(opline->result.var));
		} else {

			retval = zobj->handlers->read_property(container, offset, BP_VAR_IS, cache_slot, EX_VAR(opline->result.var));

			if (retval != EX_VAR(opline->result.var)) {
				ZVAL_COPY(EX_VAR(opline->result.var), retval);
			}
		}
	} while (0);

	FREE_OP2();
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONST_HANDLER(94, GEAR_FETCH_OBJ_FUNC_ARG, CONST|TMP|VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	if (UNEXPECTED(GEAR_CALL_INFO(EX(call)) & GEAR_CALL_SEND_ARG_BY_REF)) {
		/* Behave like FETCH_OBJ_W */
		if ((OP1_TYPE & (IS_CONST|IS_TMP_VAR))) {
			GEAR_VM_DISPATCH_TO_HELPER(gear_use_tmp_in_write_context_helper);
		}

		GEAR_VM_DISPATCH_TO_HANDLER(GEAR_FETCH_OBJ_W);
	} else {
		GEAR_VM_DISPATCH_TO_HANDLER(GEAR_FETCH_OBJ_R);
	}
}

GEAR_VM_HANDLER(97, GEAR_FETCH_OBJ_UNSET, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container, *property, *result;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR_PTR(BP_VAR_UNSET);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	property = GET_OP2_ZVAL_PTR(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	gear_fetch_property_address(result, container, OP1_TYPE, property, OP2_TYPE, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL), BP_VAR_UNSET OPLINE_CC);
	FREE_OP2();
	if (OP1_TYPE == IS_VAR) {
		FREE_VAR_PTR_AND_EXTRACT_RESULT_IF_NECESSARY(free_op1, result);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(98, GEAR_FETCH_LIST_R, CONST|TMPVARCV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;

	SAVE_OPLINE();
	container = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	gear_fetch_dimension_address_LIST_r(container, GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R), OP2_TYPE OPLINE_CC EXECUTE_DATA_CC);
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(198, GEAR_FETCH_LIST_W, VAR, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container, *dim;

	SAVE_OPLINE();
	container = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);
	dim = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (OP1_TYPE == IS_VAR
		&& Z_TYPE_P(EX_VAR(opline->op1.var)) != IS_INDIRECT
		&& UNEXPECTED(!Z_ISREF_P(container))
	) {
		gear_error(E_NOTICE, "Attempting to set reference to non referenceable value");
		gear_fetch_dimension_address_LIST_r(container, dim, OP2_TYPE OPLINE_CC EXECUTE_DATA_CC);
	} else {
		gear_fetch_dimension_address_W(container, dim, OP2_TYPE OPLINE_CC EXECUTE_DATA_CC);
	}

	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(136, GEAR_ASSIGN_OBJ, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT, SPEC(OP_DATA=CONST|TMP|VAR|CV))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2, free_op_data;
	zval *object, *property, *value, tmp;

	SAVE_OPLINE();
	object = GET_OP1_OBJ_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	property = GET_OP2_ZVAL_PTR(BP_VAR_R);
	value = GET_OP_DATA_ZVAL_PTR(BP_VAR_R);

	if (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) != IS_OBJECT)) {
		if (Z_ISREF_P(object)) {
			object = Z_REFVAL_P(object);
			if (EXPECTED(Z_TYPE_P(object) == IS_OBJECT)) {
				GEAR_VM_C_GOTO(assign_object);
			}
		}
		if (UNEXPECTED(!make_real_object(object, property OPLINE_CC EXECUTE_DATA_CC))) {
			FREE_OP_DATA();
			GEAR_VM_C_GOTO(exit_assign_obj);
		}
	}

GEAR_VM_C_LABEL(assign_object):
	if (OP2_TYPE == IS_CONST &&
	    EXPECTED(Z_OBJCE_P(object) == CACHED_PTR(opline->extended_value))) {
		uintptr_t prop_offset = (uintptr_t)CACHED_PTR(opline->extended_value + sizeof(void*));
		gear_object *zobj = Z_OBJ_P(object);
		zval *property_val;

		if (EXPECTED(IS_VALID_PROPERTY_OFFSET(prop_offset))) {
			property_val = OBJ_PROP(zobj, prop_offset);
			if (Z_TYPE_P(property_val) != IS_UNDEF) {
GEAR_VM_C_LABEL(fast_assign_obj):
				value = gear_assign_to_variable(property_val, value, OP_DATA_TYPE);
				if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
					ZVAL_COPY(EX_VAR(opline->result.var), value);
				}
				GEAR_VM_C_GOTO(exit_assign_obj);
			}
		} else {
			if (EXPECTED(zobj->properties != NULL)) {
				if (UNEXPECTED(GC_REFCOUNT(zobj->properties) > 1)) {
					if (EXPECTED(!(GC_FLAGS(zobj->properties) & IS_ARRAY_IMMUTABLE))) {
						GC_DELREF(zobj->properties);
					}
					zobj->properties = gear_array_dup(zobj->properties);
				}
				property_val = gear_hash_find_ex(zobj->properties, Z_STR_P(property), 1);
				if (property_val) {
					GEAR_VM_C_GOTO(fast_assign_obj);
				}
			}

			if (!zobj->ce->__set) {

				if (EXPECTED(zobj->properties == NULL)) {
					rebuild_object_properties(zobj);
				}
				if (OP_DATA_TYPE == IS_CONST) {
					if (UNEXPECTED(Z_OPT_REFCOUNTED_P(value))) {
						Z_ADDREF_P(value);
					}
				} else if (OP_DATA_TYPE != IS_TMP_VAR) {
					if (Z_ISREF_P(value)) {
						if (OP_DATA_TYPE == IS_VAR) {
							gear_reference *ref = Z_REF_P(value);
							if (GC_DELREF(ref) == 0) {
								ZVAL_COPY_VALUE(&tmp, Z_REFVAL_P(value));
								efree_size(ref, sizeof(gear_reference));
								value = &tmp;
							} else {
								value = Z_REFVAL_P(value);
								Z_TRY_ADDREF_P(value);
							}
						} else {
							value = Z_REFVAL_P(value);
							Z_TRY_ADDREF_P(value);
						}
					} else if (OP_DATA_TYPE == IS_CV) {
						Z_TRY_ADDREF_P(value);
					}
				}
				gear_hash_add_new(zobj->properties, Z_STR_P(property), value);
				if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
					ZVAL_COPY(EX_VAR(opline->result.var), value);
				}
				GEAR_VM_C_GOTO(exit_assign_obj);
			}
		}
	}

	if (!Z_OBJ_HT_P(object)->write_property) {
		gear_wrong_property_assignment(property OPLINE_CC EXECUTE_DATA_CC);
		FREE_OP_DATA();
		GEAR_VM_C_GOTO(exit_assign_obj);
	}

	if (OP_DATA_TYPE == IS_CV || OP_DATA_TYPE == IS_VAR) {
		ZVAL_DEREF(value);
	}

	Z_OBJ_HT_P(object)->write_property(object, property, value, (OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL);

	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_COPY(EX_VAR(opline->result.var), value);
	}
	FREE_OP_DATA();
GEAR_VM_C_LABEL(exit_assign_obj):
	FREE_OP2();
	FREE_OP1_VAR_PTR();
	/* assign_obj has two opcodes! */
	GEAR_VM_NEXT_OPCODE_EX(1, 2);
}

GEAR_VM_HANDLER(147, GEAR_ASSIGN_DIM, VAR|CV, CONST|TMPVAR|UNUSED|NEXT|CV, SPEC(OP_DATA=CONST|TMP|VAR|CV))
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *object_ptr;
	gear_free_op free_op2, free_op_data;
	zval *value;
	zval *variable_ptr;
	zval *dim;

	SAVE_OPLINE();
	object_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);

	if (EXPECTED(Z_TYPE_P(object_ptr) == IS_ARRAY)) {
GEAR_VM_C_LABEL(try_assign_dim_array):
		SEPARATE_ARRAY(object_ptr);
		if (OP2_TYPE == IS_UNUSED) {
			value = GET_OP_DATA_ZVAL_PTR(BP_VAR_R);
			if (OP_DATA_TYPE == IS_CV || OP_DATA_TYPE == IS_VAR) {
				ZVAL_DEREF(value);
			}
			variable_ptr = gear_hash_next_index_insert(Z_ARRVAL_P(object_ptr), value);
			if (UNEXPECTED(variable_ptr == NULL)) {
				FREE_OP_DATA();
				gear_cannot_add_element();
				GEAR_VM_C_GOTO(assign_dim_error);
			} else if (OP_DATA_TYPE == IS_CV) {
				if (Z_REFCOUNTED_P(value)) {
					Z_ADDREF_P(value);
				}
			} else if (OP_DATA_TYPE == IS_VAR) {
				if (value != free_op_data) {
					if (Z_REFCOUNTED_P(value)) {
						Z_ADDREF_P(value);
					}
					FREE_OP_DATA();
				}
			} else if (OP_DATA_TYPE == IS_CONST) {
				if (UNEXPECTED(Z_REFCOUNTED_P(value))) {
					Z_ADDREF_P(value);
				}
			}
		} else {
			dim = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
			if (OP2_TYPE == IS_CONST) {
				variable_ptr = gear_fetch_dimension_address_inner_W_CONST(Z_ARRVAL_P(object_ptr), dim EXECUTE_DATA_CC);
			} else {
				variable_ptr = gear_fetch_dimension_address_inner_W(Z_ARRVAL_P(object_ptr), dim EXECUTE_DATA_CC);
			}
			if (UNEXPECTED(variable_ptr == NULL)) {
				GEAR_VM_C_GOTO(assign_dim_error);
			}
			value = GET_OP_DATA_ZVAL_PTR(BP_VAR_R);
			value = gear_assign_to_variable(variable_ptr, value, OP_DATA_TYPE);
		}
		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_COPY(EX_VAR(opline->result.var), value);
		}
	} else {
		if (EXPECTED(Z_ISREF_P(object_ptr))) {
			object_ptr = Z_REFVAL_P(object_ptr);
			if (EXPECTED(Z_TYPE_P(object_ptr) == IS_ARRAY)) {
				GEAR_VM_C_GOTO(try_assign_dim_array);
			}
		}
		if (EXPECTED(Z_TYPE_P(object_ptr) == IS_OBJECT)) {
			dim = GET_OP2_ZVAL_PTR(BP_VAR_R);
			value = GET_OP_DATA_ZVAL_PTR_DEREF(BP_VAR_R);

			if (OP2_TYPE == IS_CONST && Z_EXTRA_P(dim) == GEAR_EXTRA_VALUE) {
				dim++;
			}
			gear_assign_to_object_dim(object_ptr, dim, value OPLINE_CC EXECUTE_DATA_CC);

			FREE_OP_DATA();
		} else if (EXPECTED(Z_TYPE_P(object_ptr) == IS_STRING)) {
			if (OP2_TYPE == IS_UNUSED) {
				gear_use_new_element_for_string();
				FREE_UNFETCHED_OP_DATA();
				FREE_OP1_VAR_PTR();
				UNDEF_RESULT();
				HANDLE_EXCEPTION();
			} else {
				dim = GET_OP2_ZVAL_PTR(BP_VAR_R);
				value = GET_OP_DATA_ZVAL_PTR_DEREF(BP_VAR_R);
				gear_assign_to_string_offset(object_ptr, dim, value OPLINE_CC EXECUTE_DATA_CC);
				FREE_OP_DATA();
			}
		} else if (EXPECTED(Z_TYPE_P(object_ptr) <= IS_FALSE)) {
			ZVAL_ARR(object_ptr, gear_new_array(8));
			GEAR_VM_C_GOTO(try_assign_dim_array);
		} else {
			if (OP1_TYPE != IS_VAR || EXPECTED(!Z_ISERROR_P(object_ptr))) {
				gear_use_scalar_as_array();
			}
			dim = GET_OP2_ZVAL_PTR(BP_VAR_R);
GEAR_VM_C_LABEL(assign_dim_error):
			FREE_UNFETCHED_OP_DATA();
			if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
				ZVAL_NULL(EX_VAR(opline->result.var));
			}
		}
	}
	if (OP2_TYPE != IS_UNUSED) {
		FREE_OP2();
	}
	FREE_OP1_VAR_PTR();
	/* assign_dim has two opcodes! */
	GEAR_VM_NEXT_OPCODE_EX(1, 2);
}

GEAR_VM_HANDLER(38, GEAR_ASSIGN, VAR|CV, CONST|TMP|VAR|CV, SPEC(RETVAL))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *value;
	zval *variable_ptr;

	SAVE_OPLINE();
	value = GET_OP2_ZVAL_PTR(BP_VAR_R);
	variable_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);

	if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(variable_ptr))) {
		FREE_OP2();
		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_NULL(EX_VAR(opline->result.var));
		}
	} else {
		value = gear_assign_to_variable(variable_ptr, value, OP2_TYPE);
		if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
			ZVAL_COPY(EX_VAR(opline->result.var), value);
		}
		FREE_OP1_VAR_PTR();
		/* gear_assign_to_variable() always takes care of op2, never free it! */
	}

	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(39, GEAR_ASSIGN_REF, VAR|CV, VAR|CV, SRC)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *variable_ptr;
	zval *value_ptr;

	SAVE_OPLINE();
	value_ptr = GET_OP2_ZVAL_PTR_PTR(BP_VAR_W);
	variable_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);

	if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(variable_ptr))) {
		variable_ptr = &EG(uninitialized_zval);
	} else if (OP1_TYPE == IS_VAR &&
	           UNEXPECTED(Z_TYPE_P(EX_VAR(opline->op1.var)) != IS_INDIRECT)) {

		gear_throw_error(NULL, "Cannot assign by reference to overloaded object");
		FREE_OP1_VAR_PTR();
		FREE_OP2_VAR_PTR();
		UNDEF_RESULT();
		HANDLE_EXCEPTION();
	} else if (OP2_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(value_ptr))) {
		variable_ptr = &EG(uninitialized_zval);
	} else if (OP2_TYPE == IS_VAR &&
	           opline->extended_value == GEAR_RETURNS_FUNCTION &&
			   UNEXPECTED(!Z_ISREF_P(value_ptr))) {

		if (UNEXPECTED(!gear_wrong_assign_to_variable_reference(variable_ptr, value_ptr, OP2_TYPE OPLINE_CC EXECUTE_DATA_CC))) {
			FREE_OP2_VAR_PTR();
			UNDEF_RESULT();
			HANDLE_EXCEPTION();
		}

		/* op2 freed by assign_to_variable */
		FREE_OP1_VAR_PTR();
		GEAR_VM_NEXT_OPCODE();
	} else {
		gear_assign_to_variable_reference(variable_ptr, value_ptr);
	}

	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_COPY(EX_VAR(opline->result.var), variable_ptr);
	}

	FREE_OP2_VAR_PTR();
	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_HELPER(gear_leave_helper, ANY, ANY)
{
	gear_execute_data *old_execute_data;
	uint32_t call_info = EX_CALL_INFO();

	if (EXPECTED((call_info & (GEAR_CALL_CODE|GEAR_CALL_TOP|GEAR_CALL_HAS_SYMBOL_TABLE|GEAR_CALL_FREE_EXTRA_ARGS|GEAR_CALL_ALLOCATED)) == 0)) {
		i_free_compiled_variables(execute_data);

		EG(current_execute_data) = EX(prev_execute_data);
		if (UNEXPECTED(call_info & GEAR_CALL_RELEASE_THIS)) {
			gear_object *object = Z_OBJ(execute_data->This);
#if 0
			if (UNEXPECTED(EG(exception) != NULL) && (EX(opline)->op1.num & GEAR_CALL_CTOR)) {
#else
			if (UNEXPECTED(EG(exception) != NULL) && (call_info & GEAR_CALL_CTOR)) {
#endif
				GC_DELREF(object);
				gear_object_store_ctor_failed(object);
			}
			OBJ_RELEASE(object);
		} else if (UNEXPECTED(call_info & GEAR_CALL_CLOSURE)) {
			OBJ_RELEASE(GEAR_CLOSURE_OBJECT(EX(func)));
		}
		EG(vm_stack_top) = (zval*)execute_data;
		execute_data = EX(prev_execute_data);

		if (UNEXPECTED(EG(exception) != NULL)) {
			gear_rethrow_exception(execute_data);
			HANDLE_EXCEPTION_LEAVE();
		}

		LOAD_NEXT_OPLINE();
		GEAR_VM_LEAVE();
	} else if (EXPECTED((call_info & (GEAR_CALL_CODE|GEAR_CALL_TOP)) == 0)) {
		i_free_compiled_variables(execute_data);

		if (UNEXPECTED(call_info & GEAR_CALL_HAS_SYMBOL_TABLE)) {
			gear_clean_and_cache_symbol_table(EX(symbol_table));
		}
		EG(current_execute_data) = EX(prev_execute_data);
		if (UNEXPECTED(call_info & GEAR_CALL_RELEASE_THIS)) {
			gear_object *object = Z_OBJ(execute_data->This);
#if 0
			if (UNEXPECTED(EG(exception) != NULL) && (EX(opline)->op1.num & GEAR_CALL_CTOR)) {
#else
			if (UNEXPECTED(EG(exception) != NULL) && (call_info & GEAR_CALL_CTOR)) {
#endif
				GC_DELREF(object);
				gear_object_store_ctor_failed(object);
			}
			OBJ_RELEASE(object);
		} else if (UNEXPECTED(call_info & GEAR_CALL_CLOSURE)) {
			OBJ_RELEASE(GEAR_CLOSURE_OBJECT(EX(func)));
		}

		gear_vm_stack_free_extra_args_ex(call_info, execute_data);
		old_execute_data = execute_data;
		execute_data = EX(prev_execute_data);
		gear_vm_stack_free_call_frame_ex(call_info, old_execute_data);

		if (UNEXPECTED(EG(exception) != NULL)) {
			gear_rethrow_exception(execute_data);
			HANDLE_EXCEPTION_LEAVE();
		}

		LOAD_NEXT_OPLINE();
		GEAR_VM_LEAVE();
	} else if (EXPECTED((call_info & GEAR_CALL_TOP) == 0)) {
		gear_detach_symbol_table(execute_data);
		destroy_op_array(&EX(func)->op_array);
		efree_size(EX(func), sizeof(gear_op_array));
		old_execute_data = execute_data;
		execute_data = EG(current_execute_data) = EX(prev_execute_data);
		gear_vm_stack_free_call_frame_ex(call_info, old_execute_data);

		gear_attach_symbol_table(execute_data);
		if (UNEXPECTED(EG(exception) != NULL)) {
			gear_rethrow_exception(execute_data);
			HANDLE_EXCEPTION_LEAVE();
		}

		LOAD_NEXT_OPLINE();
		GEAR_VM_LEAVE();
	} else {
		if (EXPECTED((call_info & GEAR_CALL_CODE) == 0)) {
			i_free_compiled_variables(execute_data);
			if (UNEXPECTED(call_info & (GEAR_CALL_HAS_SYMBOL_TABLE|GEAR_CALL_FREE_EXTRA_ARGS))) {
				if (UNEXPECTED(call_info & GEAR_CALL_HAS_SYMBOL_TABLE)) {
					gear_clean_and_cache_symbol_table(EX(symbol_table));
				}
				gear_vm_stack_free_extra_args_ex(call_info, execute_data);
			}
			EG(current_execute_data) = EX(prev_execute_data);
			if (UNEXPECTED(call_info & GEAR_CALL_CLOSURE)) {
				OBJ_RELEASE(GEAR_CLOSURE_OBJECT(EX(func)));
			}
			GEAR_VM_RETURN();
		} else /* if (call_kind == GEAR_CALL_TOP_CODE) */ {
			gear_array *symbol_table = EX(symbol_table);

			gear_detach_symbol_table(execute_data);
			old_execute_data = EX(prev_execute_data);
			while (old_execute_data) {
				if (old_execute_data->func && (GEAR_CALL_INFO(old_execute_data) & GEAR_CALL_HAS_SYMBOL_TABLE)) {
					if (old_execute_data->symbol_table == symbol_table) {
						gear_attach_symbol_table(old_execute_data);
					}
					break;
				}
				old_execute_data = old_execute_data->prev_execute_data;
			}
			EG(current_execute_data) = EX(prev_execute_data);
			GEAR_VM_RETURN();
		}
	}
}

GEAR_VM_HOT_HANDLER(42, GEAR_JMP, JMP_ADDR, ANY)
{
	USE_OPLINE

	GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op1), 0);
}

GEAR_VM_HOT_NOCONST_HANDLER(43, GEAR_JMPZ, CONST|TMPVAR|CV, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *val;

	val = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (Z_TYPE_INFO_P(val) == IS_TRUE) {
		GEAR_VM_NEXT_OPCODE();
	} else if (EXPECTED(Z_TYPE_INFO_P(val) <= IS_TRUE)) {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(val) == IS_UNDEF)) {
			SAVE_OPLINE();
			GET_OP1_UNDEF_CV(val, BP_VAR_R);
			if (UNEXPECTED(EG(exception))) {
				HANDLE_EXCEPTION();
			}
		}
		GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
	}

	SAVE_OPLINE();
	if (i_gear_is_true(val)) {
		opline++;
	} else {
		opline = OP_JMP_ADDR(opline, opline->op2);
	}
	FREE_OP1();
	GEAR_VM_JMP(opline);
}

GEAR_VM_HOT_NOCONST_HANDLER(44, GEAR_JMPNZ, CONST|TMPVAR|CV, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *val;

	val = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (Z_TYPE_INFO_P(val) == IS_TRUE) {
		GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
	} else if (EXPECTED(Z_TYPE_INFO_P(val) <= IS_TRUE)) {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(val) == IS_UNDEF)) {
			SAVE_OPLINE();
			GET_OP1_UNDEF_CV(val, BP_VAR_R);
			if (UNEXPECTED(EG(exception))) {
				HANDLE_EXCEPTION();
			}
		}
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (i_gear_is_true(val)) {
		opline = OP_JMP_ADDR(opline, opline->op2);
	} else {
		opline++;
	}
	FREE_OP1();
	GEAR_VM_JMP(opline);
}

GEAR_VM_HOT_NOCONST_HANDLER(45, GEAR_JMPZNZ, CONST|TMPVAR|CV, JMP_ADDR, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *val;

	val = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (EXPECTED(Z_TYPE_INFO_P(val) == IS_TRUE)) {
		GEAR_VM_SET_RELATIVE_OPCODE(opline, opline->extended_value);
		GEAR_VM_CONTINUE();
	} else if (EXPECTED(Z_TYPE_INFO_P(val) <= IS_TRUE)) {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(val) == IS_UNDEF)) {
			SAVE_OPLINE();
			GET_OP1_UNDEF_CV(val, BP_VAR_R);
			if (UNEXPECTED(EG(exception))) {
				HANDLE_EXCEPTION();
			}
		}
		GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
	}

	SAVE_OPLINE();
	if (i_gear_is_true(val)) {
		opline = GEAR_OFFSET_TO_OPLINE(opline, opline->extended_value);
	} else {
		opline = OP_JMP_ADDR(opline, opline->op2);
	}
	FREE_OP1();
	GEAR_VM_JMP(opline);
}

GEAR_VM_COLD_CONST_HANDLER(46, GEAR_JMPZ_EX, CONST|TMPVAR|CV, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *val;
	int ret;

	val = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (Z_TYPE_INFO_P(val) == IS_TRUE) {
		ZVAL_TRUE(EX_VAR(opline->result.var));
		GEAR_VM_NEXT_OPCODE();
	} else if (EXPECTED(Z_TYPE_INFO_P(val) <= IS_TRUE)) {
		ZVAL_FALSE(EX_VAR(opline->result.var));
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(val) == IS_UNDEF)) {
			SAVE_OPLINE();
			GET_OP1_UNDEF_CV(val, BP_VAR_R);
			if (UNEXPECTED(EG(exception))) {
				HANDLE_EXCEPTION();
			}
		}
		GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
	}

	SAVE_OPLINE();
	ret = i_gear_is_true(val);
	FREE_OP1();
	if (ret) {
		ZVAL_TRUE(EX_VAR(opline->result.var));
		opline++;
	} else {
		ZVAL_FALSE(EX_VAR(opline->result.var));
		opline = OP_JMP_ADDR(opline, opline->op2);
	}
	GEAR_VM_JMP(opline);
}

GEAR_VM_COLD_CONST_HANDLER(47, GEAR_JMPNZ_EX, CONST|TMPVAR|CV, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *val;
	int ret;

	val = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (Z_TYPE_INFO_P(val) == IS_TRUE) {
		ZVAL_TRUE(EX_VAR(opline->result.var));
		GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
	} else if (EXPECTED(Z_TYPE_INFO_P(val) <= IS_TRUE)) {
		ZVAL_FALSE(EX_VAR(opline->result.var));
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(val) == IS_UNDEF)) {
			SAVE_OPLINE();
			GET_OP1_UNDEF_CV(val, BP_VAR_R);
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		} else {
			GEAR_VM_NEXT_OPCODE();
		}
	}

	SAVE_OPLINE();
	ret = i_gear_is_true(val);
	FREE_OP1();
	if (ret) {
		ZVAL_TRUE(EX_VAR(opline->result.var));
		opline = OP_JMP_ADDR(opline, opline->op2);
	} else {
		ZVAL_FALSE(EX_VAR(opline->result.var));
		opline++;
	}
	GEAR_VM_JMP(opline);
}

GEAR_VM_HANDLER(70, GEAR_FREE, TMPVAR, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	zval_ptr_dtor_nogc(EX_VAR(opline->op1.var));
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_HANDLER(127, GEAR_FE_FREE, TMPVAR, ANY)
{
	zval *var;
	USE_OPLINE

	SAVE_OPLINE();
	var = EX_VAR(opline->op1.var);
	if (Z_TYPE_P(var) != IS_ARRAY && Z_FE_ITER_P(var) != (uint32_t)-1) {
		gear_hash_iterator_del(Z_FE_ITER_P(var));
	}
	zval_ptr_dtor_nogc(var);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(53, GEAR_FAST_CONCAT, CONST|TMPVAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2;
	gear_string *op1_str, *op2_str, *str;


	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if ((OP1_TYPE == IS_CONST || EXPECTED(Z_TYPE_P(op1) == IS_STRING)) &&
	    (OP2_TYPE == IS_CONST || EXPECTED(Z_TYPE_P(op2) == IS_STRING))) {
		gear_string *op1_str = Z_STR_P(op1);
		gear_string *op2_str = Z_STR_P(op2);
		gear_string *str;

		if (OP1_TYPE != IS_CONST && UNEXPECTED(ZSTR_LEN(op1_str) == 0)) {
			if (OP2_TYPE == IS_CONST || OP2_TYPE == IS_CV) {
				ZVAL_STR_COPY(EX_VAR(opline->result.var), op2_str);
			} else {
				ZVAL_STR(EX_VAR(opline->result.var), op2_str);
			}
			FREE_OP1();
		} else if (OP2_TYPE != IS_CONST && UNEXPECTED(ZSTR_LEN(op2_str) == 0)) {
			if (OP1_TYPE == IS_CONST || OP1_TYPE == IS_CV) {
				ZVAL_STR_COPY(EX_VAR(opline->result.var), op1_str);
			} else {
				ZVAL_STR(EX_VAR(opline->result.var), op1_str);
			}
			FREE_OP2();
		} else if (OP1_TYPE != IS_CONST && OP1_TYPE != IS_CV &&
		    !ZSTR_IS_INTERNED(op1_str) && GC_REFCOUNT(op1_str) == 1) {
		    size_t len = ZSTR_LEN(op1_str);

			str = gear_string_extend(op1_str, len + ZSTR_LEN(op2_str), 0);
			memcpy(ZSTR_VAL(str) + len, ZSTR_VAL(op2_str), ZSTR_LEN(op2_str)+1);
			ZVAL_NEW_STR(EX_VAR(opline->result.var), str);
			FREE_OP2();
		} else {
			str = gear_string_alloc(ZSTR_LEN(op1_str) + ZSTR_LEN(op2_str), 0);
			memcpy(ZSTR_VAL(str), ZSTR_VAL(op1_str), ZSTR_LEN(op1_str));
			memcpy(ZSTR_VAL(str) + ZSTR_LEN(op1_str), ZSTR_VAL(op2_str), ZSTR_LEN(op2_str)+1);
			ZVAL_NEW_STR(EX_VAR(opline->result.var), str);
			FREE_OP1();
			FREE_OP2();
		}
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CONST) {
		op1_str = Z_STR_P(op1);
	} else if (EXPECTED(Z_TYPE_P(op1) == IS_STRING)) {
		op1_str = gear_string_copy(Z_STR_P(op1));
	} else {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op1) == IS_UNDEF)) {
			GET_OP1_UNDEF_CV(op1, BP_VAR_R);
		}
		op1_str = zval_get_string_func(op1);
	}
	if (OP2_TYPE == IS_CONST) {
		op2_str = Z_STR_P(op2);
	} else if (EXPECTED(Z_TYPE_P(op2) == IS_STRING)) {
		op2_str = gear_string_copy(Z_STR_P(op2));
	} else {
		if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op2) == IS_UNDEF)) {
			GET_OP2_UNDEF_CV(op2, BP_VAR_R);
		}
		op2_str = zval_get_string_func(op2);
	}
	do {
		if (OP1_TYPE != IS_CONST) {
			if (UNEXPECTED(ZSTR_LEN(op1_str) == 0)) {
				if (OP2_TYPE == IS_CONST) {
					if (UNEXPECTED(Z_REFCOUNTED_P(op2))) {
						GC_ADDREF(op2_str);
					}
				}
				ZVAL_STR(EX_VAR(opline->result.var), op2_str);
				gear_string_release_ex(op1_str, 0);
				break;
			}
		}
		if (OP2_TYPE != IS_CONST) {
			if (UNEXPECTED(ZSTR_LEN(op2_str) == 0)) {
				if (OP1_TYPE == IS_CONST) {
					if (UNEXPECTED(Z_REFCOUNTED_P(op1))) {
						GC_ADDREF(op1_str);
					}
				}
				ZVAL_STR(EX_VAR(opline->result.var), op1_str);
				gear_string_release_ex(op2_str, 0);
				break;
			}
		}
		str = gear_string_alloc(ZSTR_LEN(op1_str) + ZSTR_LEN(op2_str), 0);
		memcpy(ZSTR_VAL(str), ZSTR_VAL(op1_str), ZSTR_LEN(op1_str));
		memcpy(ZSTR_VAL(str) + ZSTR_LEN(op1_str), ZSTR_VAL(op2_str), ZSTR_LEN(op2_str)+1);
		ZVAL_NEW_STR(EX_VAR(opline->result.var), str);
		if (OP1_TYPE != IS_CONST) {
			gear_string_release_ex(op1_str, 0);
		}
		if (OP2_TYPE != IS_CONST) {
			gear_string_release_ex(op2_str, 0);
		}
	} while (0);
	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(54, GEAR_ROPE_INIT, UNUSED, CONST|TMPVAR|CV, NUM)
{
	USE_OPLINE
	gear_free_op free_op2;
	gear_string **rope;
	zval *var;

	/* Compiler allocates the necessary number of zval slots to keep the rope */
	rope = (gear_string**)EX_VAR(opline->result.var);
	if (OP2_TYPE == IS_CONST) {
		var = GET_OP2_ZVAL_PTR(BP_VAR_R);
		rope[0] = Z_STR_P(var);
		if (UNEXPECTED(Z_REFCOUNTED_P(var))) {
			Z_ADDREF_P(var);
		}
	} else {
		var = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
		if (EXPECTED(Z_TYPE_P(var) == IS_STRING)) {
			if (OP2_TYPE == IS_CV) {
				rope[0] = gear_string_copy(Z_STR_P(var));
			} else {
				rope[0] = Z_STR_P(var);
			}
		} else {
			SAVE_OPLINE();
			if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(var) == IS_UNDEF)) {
				GET_OP2_UNDEF_CV(var, BP_VAR_R);
			}
			rope[0] = zval_get_string_func(var);
			FREE_OP2();
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		}
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(55, GEAR_ROPE_ADD, TMP, CONST|TMPVAR|CV, NUM)
{
	USE_OPLINE
	gear_free_op free_op2;
	gear_string **rope;
	zval *var;

	/* op1 and result are the same */
	rope = (gear_string**)EX_VAR(opline->op1.var);
	if (OP2_TYPE == IS_CONST) {
		var = GET_OP2_ZVAL_PTR(BP_VAR_R);
		rope[opline->extended_value] = Z_STR_P(var);
		if (UNEXPECTED(Z_REFCOUNTED_P(var))) {
			Z_ADDREF_P(var);
		}
	} else {
		var = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
		if (EXPECTED(Z_TYPE_P(var) == IS_STRING)) {
			if (OP2_TYPE == IS_CV) {
				rope[opline->extended_value] = gear_string_copy(Z_STR_P(var));
			} else {
				rope[opline->extended_value] = Z_STR_P(var);
			}
		} else {
			SAVE_OPLINE();
			if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(var) == IS_UNDEF)) {
				GET_OP2_UNDEF_CV(var, BP_VAR_R);
			}
			rope[opline->extended_value] = zval_get_string_func(var);
			FREE_OP2();
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		}
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(56, GEAR_ROPE_END, TMP, CONST|TMPVAR|CV, NUM)
{
	USE_OPLINE
	gear_free_op free_op2;
	gear_string **rope;
	zval *var, *ret;
	uint32_t i;
	size_t len = 0;
	char *target;

	rope = (gear_string**)EX_VAR(opline->op1.var);
	if (OP2_TYPE == IS_CONST) {
		var = GET_OP2_ZVAL_PTR(BP_VAR_R);
		rope[opline->extended_value] = Z_STR_P(var);
		if (UNEXPECTED(Z_REFCOUNTED_P(var))) {
			Z_ADDREF_P(var);
		}
	} else {
		var = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
		if (EXPECTED(Z_TYPE_P(var) == IS_STRING)) {
			if (OP2_TYPE == IS_CV) {
				rope[opline->extended_value] = gear_string_copy(Z_STR_P(var));
			} else {
				rope[opline->extended_value] = Z_STR_P(var);
			}
		} else {
			SAVE_OPLINE();
			if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(var) == IS_UNDEF)) {
				GET_OP2_UNDEF_CV(var, BP_VAR_R);
			}
			rope[opline->extended_value] = zval_get_string_func(var);
			FREE_OP2();
			if (UNEXPECTED(EG(exception))) {
				for (i = 0; i <= opline->extended_value; i++) {
					gear_string_release_ex(rope[i], 0);
				}
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
		}
	}
	for (i = 0; i <= opline->extended_value; i++) {
		len += ZSTR_LEN(rope[i]);
	}
	ret = EX_VAR(opline->result.var);
	ZVAL_STR(ret, gear_string_alloc(len, 0));
	target = Z_STRVAL_P(ret);
	for (i = 0; i <= opline->extended_value; i++) {
		memcpy(target, ZSTR_VAL(rope[i]), ZSTR_LEN(rope[i]));
		target += ZSTR_LEN(rope[i]);
		gear_string_release_ex(rope[i], 0);
	}
	*target = '\0';

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(109, GEAR_FETCH_CLASS, UNUSED|CLASS_FETCH, CONST|TMPVAR|UNUSED|CV, CACHE_SLOT)
{
	gear_free_op free_op2;
	zval *class_name;
	USE_OPLINE

	SAVE_OPLINE();
	if (OP2_TYPE == IS_UNUSED) {
		Z_CE_P(EX_VAR(opline->result.var)) = gear_fetch_class(NULL, opline->op1.num);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	} else if (OP2_TYPE == IS_CONST) {
		gear_class_entry *ce = CACHED_PTR(opline->extended_value);

		if (UNEXPECTED(ce == NULL)) {
			class_name = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
			ce = gear_fetch_class_by_name(Z_STR_P(class_name), class_name + 1, opline->op1.num);
			CACHE_PTR(opline->extended_value, ce);
		}
		Z_CE_P(EX_VAR(opline->result.var)) = ce;
	} else {
		class_name = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
GEAR_VM_C_LABEL(try_class_name):
		if (Z_TYPE_P(class_name) == IS_OBJECT) {
			Z_CE_P(EX_VAR(opline->result.var)) = Z_OBJCE_P(class_name);
		} else if (Z_TYPE_P(class_name) == IS_STRING) {
			Z_CE_P(EX_VAR(opline->result.var)) = gear_fetch_class(Z_STR_P(class_name), opline->op1.num);
		} else if ((OP2_TYPE & (IS_VAR|IS_CV)) && Z_TYPE_P(class_name) == IS_REFERENCE) {
			class_name = Z_REFVAL_P(class_name);
			GEAR_VM_C_GOTO(try_class_name);
		} else {
			if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(class_name) == IS_UNDEF)) {
				GET_OP2_UNDEF_CV(class_name, BP_VAR_R);
				if (UNEXPECTED(EG(exception) != NULL)) {
					HANDLE_EXCEPTION();
				}
			}
			gear_throw_error(NULL, "Class name must be a valid object or a string");
		}
	}

	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_OBJ_HANDLER(112, GEAR_INIT_METHOD_CALL, CONST|TMPVAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, NUM|CACHE_SLOT)
{
	USE_OPLINE
	zval *function_name;
	gear_free_op free_op1, free_op2;
	zval *object;
	gear_function *fbc;
	gear_class_entry *called_scope;
	gear_object *obj;
	gear_execute_data *call;
	uint32_t call_info;

	SAVE_OPLINE();

	object = GET_OP1_OBJ_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(object) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	if (OP2_TYPE != IS_CONST) {
		function_name = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	}

	if (OP2_TYPE != IS_CONST &&
	    UNEXPECTED(Z_TYPE_P(function_name) != IS_STRING)) {
		do {
			if ((OP2_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(function_name)) {
				function_name = Z_REFVAL_P(function_name);
				if (EXPECTED(Z_TYPE_P(function_name) == IS_STRING)) {
					break;
				}
			} else if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(function_name) == IS_UNDEF)) {
				GET_OP2_UNDEF_CV(function_name, BP_VAR_R);
				if (UNEXPECTED(EG(exception) != NULL)) {
					FREE_OP1();
					HANDLE_EXCEPTION();
				}
			}
			gear_throw_error(NULL, "Method name must be a string");
			FREE_OP2();
			FREE_OP1();
			HANDLE_EXCEPTION();
		} while (0);
	}

	if (OP1_TYPE != IS_UNUSED) {
		do {
			if (OP1_TYPE == IS_CONST || UNEXPECTED(Z_TYPE_P(object) != IS_OBJECT)) {
				if ((OP1_TYPE & (IS_VAR|IS_CV)) && EXPECTED(Z_ISREF_P(object))) {
					object = Z_REFVAL_P(object);
					if (EXPECTED(Z_TYPE_P(object) == IS_OBJECT)) {
						break;
					}
				}
				if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(object) == IS_UNDEF)) {
					object = GET_OP1_UNDEF_CV(object, BP_VAR_R);
					if (UNEXPECTED(EG(exception) != NULL)) {
						if (OP2_TYPE != IS_CONST) {
							FREE_OP2();
						}
						HANDLE_EXCEPTION();
					}
				}
				if (OP2_TYPE == IS_CONST) {
					function_name = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
				}
				gear_invalid_method_call(object, function_name);
				FREE_OP2();
				FREE_OP1();
				HANDLE_EXCEPTION();
			}
		} while (0);
	}

	obj = Z_OBJ_P(object);
	called_scope = obj->ce;

	if (OP2_TYPE == IS_CONST &&
	    EXPECTED(CACHED_PTR(opline->result.num) == called_scope)) {
	    fbc = CACHED_PTR(opline->result.num + sizeof(void*));
	} else {
	    gear_object *orig_obj = obj;

		if (UNEXPECTED(obj->handlers->get_method == NULL)) {
			gear_throw_error(NULL, "Object does not support method calls");
			FREE_OP2();
			FREE_OP1();
			HANDLE_EXCEPTION();
		}

		if (OP2_TYPE == IS_CONST) {
			function_name = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
		}

		/* First, locate the function. */
		fbc = obj->handlers->get_method(&obj, Z_STR_P(function_name), ((OP2_TYPE == IS_CONST) ? (RT_CONSTANT(opline, opline->op2) + 1) : NULL));
		if (UNEXPECTED(fbc == NULL)) {
			if (EXPECTED(!EG(exception))) {
				gear_undefined_method(obj->ce, Z_STR_P(function_name));
			}
			FREE_OP2();
			FREE_OP1();
			HANDLE_EXCEPTION();
		}
		if (OP2_TYPE == IS_CONST &&
		    EXPECTED(fbc->type <= GEAR_USER_FUNCTION) &&
		    EXPECTED(!(fbc->common.fn_flags & (GEAR_ACC_CALL_VIA_TRAMPOLINE|GEAR_ACC_NEVER_CACHE))) &&
		    EXPECTED(obj == orig_obj)) {
			CACHE_POLYMORPHIC_PTR(opline->result.num, called_scope, fbc);
		}
		if ((OP1_TYPE & (IS_VAR|IS_TMP_VAR)) && UNEXPECTED(obj != orig_obj)) {
			/* Reset "object" to trigger reference counting */
			object = NULL;
		}
		if (EXPECTED(fbc->type == GEAR_USER_FUNCTION) && UNEXPECTED(!fbc->op_array.run_time_cache)) {
			init_func_run_time_cache(&fbc->op_array);
		}
	}

	if (OP2_TYPE != IS_CONST) {
		FREE_OP2();
	}

	call_info = GEAR_CALL_NESTED_FUNCTION;
	if (UNEXPECTED((fbc->common.fn_flags & GEAR_ACC_STATIC) != 0)) {
		obj = NULL;
		FREE_OP1();

		if ((OP1_TYPE & (IS_VAR|IS_TMP_VAR)) && UNEXPECTED(EG(exception))) {
			HANDLE_EXCEPTION();
		}
	} else if (OP1_TYPE & (IS_VAR|IS_TMP_VAR|IS_CV)) {
		/* CV may be changed indirectly (e.g. when it's a reference) */
		call_info = GEAR_CALL_NESTED_FUNCTION | GEAR_CALL_RELEASE_THIS;
		if (OP1_TYPE == IS_CV) {
			GC_ADDREF(obj); /* For $this pointer */
		} else if (free_op1 != object) {
			GC_ADDREF(obj); /* For $this pointer */
			FREE_OP1();
		}
	}

	call = gear_vm_stack_push_call_frame(call_info,
		fbc, opline->extended_value, called_scope, obj);
	call->prev_execute_data = EX(call);
	EX(call) = call;

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(113, GEAR_INIT_STATIC_METHOD_CALL, UNUSED|CLASS_FETCH|CONST|VAR, CONST|TMPVAR|UNUSED|CONSTRUCTOR|CV, NUM|CACHE_SLOT)
{
	USE_OPLINE
	zval *function_name;
	gear_class_entry *ce;
	gear_object *object;
	gear_function *fbc;
	gear_execute_data *call;

	SAVE_OPLINE();

	if (OP1_TYPE == IS_CONST) {
		/* no function found. try a static method in class */
		ce = CACHED_PTR(opline->result.num);
		if (UNEXPECTED(ce == NULL)) {
			ce = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op1)), RT_CONSTANT(opline, opline->op1) + 1, GEAR_FETCH_CLASS_DEFAULT |  GEAR_FETCH_CLASS_EXCEPTION);
			if (UNEXPECTED(ce == NULL)) {
				GEAR_ASSERT(EG(exception));
				HANDLE_EXCEPTION();
			}
			if (OP2_TYPE != IS_CONST) {
				CACHE_PTR(opline->result.num, ce);
			}
		}
	} else if (OP1_TYPE == IS_UNUSED) {
		ce = gear_fetch_class(NULL, opline->op1.num);
		if (UNEXPECTED(ce == NULL)) {
			GEAR_ASSERT(EG(exception));
			FREE_UNFETCHED_OP2();
			HANDLE_EXCEPTION();
		}
	} else {
		ce = Z_CE_P(EX_VAR(opline->op1.var));
	}

	if (OP1_TYPE == IS_CONST &&
	    OP2_TYPE == IS_CONST &&
	    EXPECTED((fbc = CACHED_PTR(opline->result.num + sizeof(void*))) != NULL)) {
		/* nothing to do */
	} else if (OP1_TYPE != IS_CONST &&
	           OP2_TYPE == IS_CONST &&
	           EXPECTED(CACHED_PTR(opline->result.num) == ce)) {
		fbc = CACHED_PTR(opline->result.num + sizeof(void*));
	} else if (OP2_TYPE != IS_UNUSED) {
		gear_free_op free_op2;

		function_name = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
		if (OP2_TYPE != IS_CONST) {
			if (UNEXPECTED(Z_TYPE_P(function_name) != IS_STRING)) {
				do {
					if (OP2_TYPE & (IS_VAR|IS_CV) && Z_ISREF_P(function_name)) {
						function_name = Z_REFVAL_P(function_name);
						if (EXPECTED(Z_TYPE_P(function_name) == IS_STRING)) {
							break;
						}
					} else if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(function_name) == IS_UNDEF)) {
						GET_OP2_UNDEF_CV(function_name, BP_VAR_R);
						if (UNEXPECTED(EG(exception) != NULL)) {
							HANDLE_EXCEPTION();
						}
					}
					gear_throw_error(NULL, "Function name must be a string");
					FREE_OP2();
					HANDLE_EXCEPTION();
				} while (0);
			}
		}

		if (ce->get_static_method) {
			fbc = ce->get_static_method(ce, Z_STR_P(function_name));
		} else {
			fbc = gear_std_get_static_method(ce, Z_STR_P(function_name), ((OP2_TYPE == IS_CONST) ? (RT_CONSTANT(opline, opline->op2) + 1) : NULL));
		}
		if (UNEXPECTED(fbc == NULL)) {
			if (EXPECTED(!EG(exception))) {
				gear_undefined_method(ce, Z_STR_P(function_name));
			}
			FREE_OP2();
			HANDLE_EXCEPTION();
		}
		if (OP2_TYPE == IS_CONST &&
		    EXPECTED(fbc->type <= GEAR_USER_FUNCTION) &&
		    EXPECTED(!(fbc->common.fn_flags & (GEAR_ACC_CALL_VIA_TRAMPOLINE|GEAR_ACC_NEVER_CACHE)))) {
			CACHE_POLYMORPHIC_PTR(opline->result.num, ce, fbc);
		}
		if (EXPECTED(fbc->type == GEAR_USER_FUNCTION) && UNEXPECTED(!fbc->op_array.run_time_cache)) {
			init_func_run_time_cache(&fbc->op_array);
		}
		if (OP2_TYPE != IS_CONST) {
			FREE_OP2();
		}
	} else {
		if (UNEXPECTED(ce->constructor == NULL)) {
			gear_throw_error(NULL, "Cannot call constructor");
			HANDLE_EXCEPTION();
		}
		if (Z_TYPE(EX(This)) == IS_OBJECT && Z_OBJ(EX(This))->ce != ce->constructor->common.scope && (ce->constructor->common.fn_flags & GEAR_ACC_PRIVATE)) {
			gear_throw_error(NULL, "Cannot call private %s::__construct()", ZSTR_VAL(ce->name));
			HANDLE_EXCEPTION();
		}
		fbc = ce->constructor;
		if (EXPECTED(fbc->type == GEAR_USER_FUNCTION) && UNEXPECTED(!fbc->op_array.run_time_cache)) {
			init_func_run_time_cache(&fbc->op_array);
		}
	}

	object = NULL;
	if (!(fbc->common.fn_flags & GEAR_ACC_STATIC)) {
		if (Z_TYPE(EX(This)) == IS_OBJECT && instanceof_function(Z_OBJCE(EX(This)), ce)) {
			object = Z_OBJ(EX(This));
			ce = object->ce;
		} else {
			gear_non_static_method_call(fbc);
			if (UNEXPECTED(EG(exception) != NULL)) {
				HANDLE_EXCEPTION();
			}
		}
	}

	if (OP1_TYPE == IS_UNUSED) {
		/* previous opcode is GEAR_FETCH_CLASS */
		if ((opline->op1.num & GEAR_FETCH_CLASS_MASK) == GEAR_FETCH_CLASS_PARENT ||
		    (opline->op1.num & GEAR_FETCH_CLASS_MASK) == GEAR_FETCH_CLASS_SELF) {
			if (Z_TYPE(EX(This)) == IS_OBJECT) {
				ce = Z_OBJCE(EX(This));
			} else {
				ce = Z_CE(EX(This));
			}
		}
	}

	call = gear_vm_stack_push_call_frame(GEAR_CALL_NESTED_FUNCTION,
		fbc, opline->extended_value, ce, object);
	call->prev_execute_data = EX(call);
	EX(call) = call;

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(59, GEAR_INIT_FCALL_BY_NAME, ANY, CONST, NUM|CACHE_SLOT)
{
	USE_OPLINE
	gear_function *fbc;
	zval *function_name, *func;
	gear_execute_data *call;

	fbc = CACHED_PTR(opline->result.num);
	if (UNEXPECTED(fbc == NULL)) {
		function_name = (zval*)RT_CONSTANT(opline, opline->op2);
		func = gear_hash_find_ex(EG(function_table), Z_STR_P(function_name+1), 1);
		if (UNEXPECTED(func == NULL)) {
			GEAR_VM_DISPATCH_TO_HELPER(gear_undefined_function_helper, function_name, function_name);
		}
		fbc = Z_FUNC_P(func);
		if (EXPECTED(fbc->type == GEAR_USER_FUNCTION) && UNEXPECTED(!fbc->op_array.run_time_cache)) {
			fbc = init_func_run_time_cache_ex(func);
		}
		CACHE_PTR(opline->result.num, fbc);
	}
	call = gear_vm_stack_push_call_frame(GEAR_CALL_NESTED_FUNCTION,
		fbc, opline->extended_value, NULL, NULL);
	call->prev_execute_data = EX(call);
	EX(call) = call;

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(128, GEAR_INIT_DYNAMIC_CALL, ANY, CONST|TMPVAR|CV, NUM)
{
	USE_OPLINE
	gear_free_op free_op2;
	zval *function_name;
	gear_execute_data *call;

	SAVE_OPLINE();
	function_name = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);

GEAR_VM_C_LABEL(try_function_name):
	if (OP2_TYPE != IS_CONST && EXPECTED(Z_TYPE_P(function_name) == IS_STRING)) {
		call = gear_init_dynamic_call_string(Z_STR_P(function_name), opline->extended_value);
	} else if (OP2_TYPE != IS_CONST && EXPECTED(Z_TYPE_P(function_name) == IS_OBJECT)) {
		call = gear_init_dynamic_call_object(function_name, opline->extended_value);
	} else if (EXPECTED(Z_TYPE_P(function_name) == IS_ARRAY)) {
		call = gear_init_dynamic_call_array(Z_ARRVAL_P(function_name), opline->extended_value);
	} else if ((OP2_TYPE & (IS_VAR|IS_CV)) && EXPECTED(Z_TYPE_P(function_name) == IS_REFERENCE)) {
		function_name = Z_REFVAL_P(function_name);
		GEAR_VM_C_GOTO(try_function_name);
	} else {
		if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(function_name) == IS_UNDEF)) {
			GET_OP2_UNDEF_CV(function_name, BP_VAR_R);
			if (UNEXPECTED(EG(exception) != NULL)) {
				HANDLE_EXCEPTION();
			}
		}
		gear_throw_error(NULL, "Function name must be a string");
		call = NULL;
	}

	if (UNEXPECTED(!call)) {
		HANDLE_EXCEPTION();
	}

	FREE_OP2();
	if (OP2_TYPE & (IS_VAR|IS_TMP_VAR)) {
		if (UNEXPECTED(EG(exception))) {
			if (call) {
				 if (call->func->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) {
					gear_string_release_ex(call->func->common.function_name, 0);
					gear_free_trampoline(call->func);
				}
				gear_vm_stack_free_call_frame(call);
			}
			HANDLE_EXCEPTION();
		}
	} else if (UNEXPECTED(!call)) {
		HANDLE_EXCEPTION();
	}

	call->prev_execute_data = EX(call);
	EX(call) = call;

	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(118, GEAR_INIT_USER_CALL, CONST, CONST|TMPVAR|CV, NUM)
{
	USE_OPLINE
	gear_free_op free_op2;
	zval *function_name;
	gear_fcall_info_cache fcc;
	char *error = NULL;
	gear_function *func;
	gear_class_entry *called_scope;
	gear_object *object;
	gear_execute_data *call;
	uint32_t call_info = GEAR_CALL_NESTED_FUNCTION | GEAR_CALL_DYNAMIC;

	SAVE_OPLINE();
	function_name = GET_OP2_ZVAL_PTR(BP_VAR_R);
	if (gear_is_callable_ex(function_name, NULL, 0, NULL, &fcc, &error)) {
		func = fcc.function_handler;
		called_scope = fcc.called_scope;
		object = fcc.object;
		if (error) {
			efree(error);
			/* This is the only soft error is_callable() can generate */
			gear_non_static_method_call(func);
			if (UNEXPECTED(EG(exception) != NULL)) {
				FREE_OP2();
				HANDLE_EXCEPTION();
			}
		}
		if (func->common.fn_flags & GEAR_ACC_CLOSURE) {
			/* Delay closure destruction until its invocation */
			GC_ADDREF(GEAR_CLOSURE_OBJECT(func));
			call_info |= GEAR_CALL_CLOSURE;
			if (func->common.fn_flags & GEAR_ACC_FAKE_CLOSURE) {
				call_info |= GEAR_CALL_FAKE_CLOSURE;
			}
		} else if (object) {
			call_info |= GEAR_CALL_RELEASE_THIS;
			GC_ADDREF(object); /* For $this pointer */
		}

		FREE_OP2();
		if ((OP2_TYPE & (IS_TMP_VAR|IS_VAR)) && UNEXPECTED(EG(exception))) {
			if (call_info & GEAR_CALL_CLOSURE) {
				gear_object_release(GEAR_CLOSURE_OBJECT(func));
			}
			if (call_info & GEAR_CALL_RELEASE_THIS) {
				gear_object_release(object);
			}
			HANDLE_EXCEPTION();
		}

		if (EXPECTED(func->type == GEAR_USER_FUNCTION) && UNEXPECTED(!func->op_array.run_time_cache)) {
			init_func_run_time_cache(&func->op_array);
		}
	} else {
		gear_internal_type_error(EX_USES_STRICT_TYPES(), "%s() expects parameter 1 to be a valid callback, %s", Z_STRVAL_P(RT_CONSTANT(opline, opline->op1)), error);
		efree(error);
		FREE_OP2();
		if (UNEXPECTED(EG(exception))) {
			HANDLE_EXCEPTION();
		}
		func = (gear_function*)&gear_pass_function;
		called_scope = NULL;
		object = NULL;
	}

	call = gear_vm_stack_push_call_frame(call_info,
		func, opline->extended_value, called_scope, object);
	call->prev_execute_data = EX(call);
	EX(call) = call;

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(69, GEAR_INIT_NS_FCALL_BY_NAME, ANY, CONST, NUM|CACHE_SLOT)
{
	USE_OPLINE
	zval *func_name;
	zval *func;
	gear_function *fbc;
	gear_execute_data *call;

	fbc = CACHED_PTR(opline->result.num);
	if (UNEXPECTED(fbc == NULL)) {
		func_name = (zval *)RT_CONSTANT(opline, opline->op2);
		func = gear_hash_find_ex(EG(function_table), Z_STR_P(func_name + 1), 1);
		if (func == NULL) {
			func = gear_hash_find_ex(EG(function_table), Z_STR_P(func_name + 2), 1);
			if (UNEXPECTED(func == NULL)) {
				GEAR_VM_DISPATCH_TO_HELPER(gear_undefined_function_helper, function_name, func_name);
			}
		}
		fbc = Z_FUNC_P(func);
		if (EXPECTED(fbc->type == GEAR_USER_FUNCTION) && UNEXPECTED(!fbc->op_array.run_time_cache)) {
			fbc = init_func_run_time_cache_ex(func);
		}
		CACHE_PTR(opline->result.num, fbc);
	}

	call = gear_vm_stack_push_call_frame(GEAR_CALL_NESTED_FUNCTION,
		fbc, opline->extended_value, NULL, NULL);
	call->prev_execute_data = EX(call);
	EX(call) = call;

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(61, GEAR_INIT_FCALL, NUM, CONST, NUM|CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op2;
	zval *fname;
	zval *func;
	gear_function *fbc;
	gear_execute_data *call;

	fbc = CACHED_PTR(opline->result.num);
	if (UNEXPECTED(fbc == NULL)) {
		fname = GET_OP2_ZVAL_PTR(BP_VAR_R);
		func = gear_hash_find_ex(EG(function_table), Z_STR_P(fname), 1);
		if (UNEXPECTED(func == NULL)) {
			GEAR_VM_DISPATCH_TO_HELPER(gear_undefined_function_helper, function_name, fname);
		}
		fbc = Z_FUNC_P(func);
		if (EXPECTED(fbc->type == GEAR_USER_FUNCTION) && UNEXPECTED(!fbc->op_array.run_time_cache)) {
			fbc = init_func_run_time_cache_ex(func);
		}
		CACHE_PTR(opline->result.num, fbc);
	}

	call = gear_vm_stack_push_call_frame_ex(
		opline->op1.num, GEAR_CALL_NESTED_FUNCTION,
		fbc, opline->extended_value, NULL, NULL);
	call->prev_execute_data = EX(call);
	EX(call) = call;

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(129, GEAR_DO_ICALL, ANY, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	gear_execute_data *call = EX(call);
	gear_function *fbc = call->func;
	zval *ret;
	zval retval;

	SAVE_OPLINE();
	EX(call) = call->prev_execute_data;

	call->prev_execute_data = execute_data;
	EG(current_execute_data) = call;

	ret = RETURN_VALUE_USED(opline) ? EX_VAR(opline->result.var) : &retval;
	ZVAL_NULL(ret);

	fbc->internal_function.handler(call, ret);

#if GEAR_DEBUG
	if (!EG(exception) && call->func) {
		GEAR_ASSERT(!(call->func->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) ||
			gear_verify_internal_return_type(call->func, ret));
		GEAR_ASSERT((call->func->common.fn_flags & GEAR_ACC_RETURN_REFERENCE)
			? Z_ISREF_P(ret) : !Z_ISREF_P(ret));
	}
#endif

	EG(current_execute_data) = execute_data;
	gear_vm_stack_free_args(call);
	gear_vm_stack_free_call_frame(call);

	if (!RETURN_VALUE_USED(opline)) {
		zval_ptr_dtor(ret);
	}

	if (UNEXPECTED(EG(exception) != NULL)) {
		gear_rethrow_exception(execute_data);
		HANDLE_EXCEPTION();
	}

	GEAR_VM_SET_OPCODE(opline + 1);
	GEAR_VM_CONTINUE();
}

GEAR_VM_HOT_HANDLER(130, GEAR_DO_UCALL, ANY, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	gear_execute_data *call = EX(call);
	gear_function *fbc = call->func;
	zval *ret;

	SAVE_OPLINE();
	EX(call) = call->prev_execute_data;

	ret = NULL;
	if (RETURN_VALUE_USED(opline)) {
		ret = EX_VAR(opline->result.var);
		ZVAL_NULL(ret);
	}

	call->prev_execute_data = execute_data;
	execute_data = call;
	i_init_func_execute_data(&fbc->op_array, ret, 0 EXECUTE_DATA_CC);
	LOAD_OPLINE();

	GEAR_VM_ENTER_EX();
}

GEAR_VM_HOT_HANDLER(131, GEAR_DO_FCALL_BY_NAME, ANY, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	gear_execute_data *call = EX(call);
	gear_function *fbc = call->func;
	zval *ret;

	SAVE_OPLINE();
	EX(call) = call->prev_execute_data;

	if (EXPECTED(fbc->type == GEAR_USER_FUNCTION)) {
		ret = NULL;
		if (RETURN_VALUE_USED(opline)) {
			ret = EX_VAR(opline->result.var);
			ZVAL_NULL(ret);
		}

		call->prev_execute_data = execute_data;
		execute_data = call;
		i_init_func_execute_data(&fbc->op_array, ret, 0 EXECUTE_DATA_CC);
		LOAD_OPLINE();

		GEAR_VM_ENTER_EX();
	} else {
		zval retval;
		GEAR_ASSERT(fbc->type == GEAR_INTERNAL_FUNCTION);

		if (UNEXPECTED((fbc->common.fn_flags & GEAR_ACC_DEPRECATED) != 0)) {
			gear_deprecated_function(fbc);
			if (UNEXPECTED(EG(exception) != NULL)) {
			    UNDEF_RESULT();
				HANDLE_EXCEPTION();
			}
		}

		call->prev_execute_data = execute_data;
		EG(current_execute_data) = call;

		if (UNEXPECTED(fbc->common.fn_flags & GEAR_ACC_HAS_TYPE_HINTS)
		 && UNEXPECTED(!gear_verify_internal_arg_types(fbc, call))) {
			gear_vm_stack_free_call_frame(call);
			gear_rethrow_exception(execute_data);
		    UNDEF_RESULT();
			HANDLE_EXCEPTION();
		}

		ret = RETURN_VALUE_USED(opline) ? EX_VAR(opline->result.var) : &retval;
		ZVAL_NULL(ret);

		fbc->internal_function.handler(call, ret);

#if GEAR_DEBUG
		if (!EG(exception) && call->func) {
			GEAR_ASSERT(!(call->func->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) ||
				gear_verify_internal_return_type(call->func, ret));
			GEAR_ASSERT((call->func->common.fn_flags & GEAR_ACC_RETURN_REFERENCE)
				? Z_ISREF_P(ret) : !Z_ISREF_P(ret));
		}
#endif

		EG(current_execute_data) = execute_data;
		gear_vm_stack_free_args(call);
		gear_vm_stack_free_call_frame(call);

		if (!RETURN_VALUE_USED(opline)) {
			zval_ptr_dtor(ret);
		}
	}

	if (UNEXPECTED(EG(exception) != NULL)) {
		gear_rethrow_exception(execute_data);
		HANDLE_EXCEPTION();
	}
	GEAR_VM_SET_OPCODE(opline + 1);
	GEAR_VM_CONTINUE();
}

GEAR_VM_HOT_HANDLER(60, GEAR_DO_FCALL, ANY, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	gear_execute_data *call = EX(call);
	gear_function *fbc = call->func;
	gear_object *object;
	zval *ret;

	SAVE_OPLINE();
	EX(call) = call->prev_execute_data;
	if (UNEXPECTED((fbc->common.fn_flags & (GEAR_ACC_ABSTRACT|GEAR_ACC_DEPRECATED)) != 0)) {
		if (UNEXPECTED((fbc->common.fn_flags & GEAR_ACC_ABSTRACT) != 0)) {
			GEAR_VM_DISPATCH_TO_HELPER(gear_abstract_method_helper, fbc, fbc);
		}
		if (UNEXPECTED((fbc->common.fn_flags & GEAR_ACC_DEPRECATED) != 0)) {
			gear_deprecated_function(fbc);
			if (UNEXPECTED(EG(exception) != NULL)) {
				UNDEF_RESULT();
				HANDLE_EXCEPTION();
			}
		}
	}

	if (EXPECTED(fbc->type == GEAR_USER_FUNCTION)) {
		ret = NULL;
		if (RETURN_VALUE_USED(opline)) {
			ret = EX_VAR(opline->result.var);
			ZVAL_NULL(ret);
		}

		call->prev_execute_data = execute_data;
		execute_data = call;
		i_init_func_execute_data(&fbc->op_array, ret, 1 EXECUTE_DATA_CC);

		if (EXPECTED(gear_execute_ex == execute_ex)) {
			LOAD_OPLINE();
			GEAR_VM_ENTER_EX();
		} else {
			execute_data = EX(prev_execute_data);
			LOAD_OPLINE();
			GEAR_ADD_CALL_FLAG(call, GEAR_CALL_TOP);
			gear_execute_ex(call);
		}
	} else if (EXPECTED(fbc->type < GEAR_USER_FUNCTION)) {
		zval retval;

		call->prev_execute_data = execute_data;
		EG(current_execute_data) = call;

		if (UNEXPECTED(fbc->common.fn_flags & GEAR_ACC_HAS_TYPE_HINTS)
		  && UNEXPECTED(!gear_verify_internal_arg_types(fbc, call))) {
			UNDEF_RESULT();
			GEAR_VM_C_GOTO(fcall_end);
		}

		ret = RETURN_VALUE_USED(opline) ? EX_VAR(opline->result.var) : &retval;
		ZVAL_NULL(ret);

		if (!gear_execute_internal) {
			/* saves one function call if gear_execute_internal is not used */
			fbc->internal_function.handler(call, ret);
		} else {
			gear_execute_internal(call, ret);
		}

#if GEAR_DEBUG
		if (!EG(exception) && call->func) {
			GEAR_ASSERT(!(call->func->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) ||
				gear_verify_internal_return_type(call->func, ret));
			GEAR_ASSERT((call->func->common.fn_flags & GEAR_ACC_RETURN_REFERENCE)
				? Z_ISREF_P(ret) : !Z_ISREF_P(ret));
		}
#endif

		EG(current_execute_data) = execute_data;
		gear_vm_stack_free_args(call);

		if (!RETURN_VALUE_USED(opline)) {
			zval_ptr_dtor(ret);
		}

	} else { /* GEAR_OVERLOADED_FUNCTION */
		zval retval;

		ret = RETURN_VALUE_USED(opline) ? EX_VAR(opline->result.var) : &retval;

		call->prev_execute_data = execute_data;

		if (UNEXPECTED(!gear_do_fcall_overloaded(call, ret))) {
			UNDEF_RESULT();
			HANDLE_EXCEPTION();
		}

		if (!RETURN_VALUE_USED(opline)) {
			zval_ptr_dtor(ret);
		}
	}

GEAR_VM_C_LABEL(fcall_end):
	if (UNEXPECTED(GEAR_CALL_INFO(call) & GEAR_CALL_RELEASE_THIS)) {
		object = Z_OBJ(call->This);
#if 0
		if (UNEXPECTED(EG(exception) != NULL) && (opline->op1.num & GEAR_CALL_CTOR)) {
#else
		if (UNEXPECTED(EG(exception) != NULL) && (GEAR_CALL_INFO(call) & GEAR_CALL_CTOR)) {
#endif
			GC_DELREF(object);
			gear_object_store_ctor_failed(object);
		}
		OBJ_RELEASE(object);
	}

	gear_vm_stack_free_call_frame(call);
	if (UNEXPECTED(EG(exception) != NULL)) {
		gear_rethrow_exception(execute_data);
		HANDLE_EXCEPTION();
	}

	GEAR_VM_SET_OPCODE(opline + 1);
	GEAR_VM_CONTINUE();
}

GEAR_VM_COLD_CONST_HANDLER(124, GEAR_VERIFY_RETURN_TYPE, CONST|TMP|VAR|UNUSED|CV, UNUSED|CACHE_SLOT)
{
	USE_OPLINE

	SAVE_OPLINE();
	if (OP1_TYPE == IS_UNUSED) {
		gear_verify_missing_return_type(EX(func), CACHE_ADDR(opline->op2.num));
	} else {
/* prevents "undefined variable opline" errors */
#if !defined(GEAR_VM_SPEC) || (OP1_TYPE != IS_UNUSED)
		zval *retval_ref, *retval_ptr;
		gear_free_op free_op1;
		gear_arg_info *ret_info = EX(func)->common.arg_info - 1;

		retval_ref = retval_ptr = GET_OP1_ZVAL_PTR(BP_VAR_R);

		if (OP1_TYPE == IS_CONST) {
			ZVAL_COPY(EX_VAR(opline->result.var), retval_ptr);
			retval_ref = retval_ptr = EX_VAR(opline->result.var);
		} else if (OP1_TYPE == IS_VAR) {
			if (UNEXPECTED(Z_TYPE_P(retval_ptr) == IS_INDIRECT)) {
				retval_ptr = Z_INDIRECT_P(retval_ptr);
			}
			ZVAL_DEREF(retval_ptr);
		} else if (OP1_TYPE == IS_CV) {
			ZVAL_DEREF(retval_ptr);
		}

		if (UNEXPECTED(!GEAR_TYPE_IS_CLASS(ret_info->type)
			&& GEAR_TYPE_CODE(ret_info->type) != IS_CALLABLE
			&& GEAR_TYPE_CODE(ret_info->type) != IS_ITERABLE
			&& !GEAR_SAME_FAKE_TYPE(GEAR_TYPE_CODE(ret_info->type), Z_TYPE_P(retval_ptr))
			&& !(EX(func)->op_array.fn_flags & GEAR_ACC_RETURN_REFERENCE)
			&& retval_ref != retval_ptr)
		) {
			/* A cast might happen - unwrap the reference if this is a by-value return */
			if (Z_REFCOUNT_P(retval_ref) == 1) {
				ZVAL_UNREF(retval_ref);
			} else {
				Z_DELREF_P(retval_ref);
				ZVAL_COPY(retval_ref, retval_ptr);
			}
			retval_ptr = retval_ref;
		}
		gear_verify_return_type(EX(func), retval_ptr, CACHE_ADDR(opline->op2.num));
#endif
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_HANDLER(62, GEAR_RETURN, CONST|TMP|VAR|CV, ANY)
{
	USE_OPLINE
	zval *retval_ptr;
	zval *return_value;
	gear_free_op free_op1;

	retval_ptr = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	return_value = EX(return_value);
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(retval_ptr) == IS_UNDEF)) {
		SAVE_OPLINE();
		retval_ptr = GET_OP1_UNDEF_CV(retval_ptr, BP_VAR_R);
		if (return_value) {
			ZVAL_NULL(return_value);
		}
	} else if (!return_value) {
		if (OP1_TYPE & (IS_VAR|IS_TMP_VAR)) {
			if (Z_REFCOUNTED_P(free_op1) && !Z_DELREF_P(free_op1)) {
				SAVE_OPLINE();
				rc_dtor_func(Z_COUNTED_P(free_op1));
			}
		}
	} else {
		if ((OP1_TYPE & (IS_CONST|IS_TMP_VAR))) {
			ZVAL_COPY_VALUE(return_value, retval_ptr);
			if (OP1_TYPE == IS_CONST) {
				if (UNEXPECTED(Z_OPT_REFCOUNTED_P(return_value))) {
					Z_ADDREF_P(return_value);
				}
			}
		} else if (OP1_TYPE == IS_CV) {
			if (Z_OPT_REFCOUNTED_P(retval_ptr)) {
				if (EXPECTED(!Z_OPT_ISREF_P(retval_ptr))) {
					ZVAL_COPY_VALUE(return_value, retval_ptr);
					if (EXPECTED(!(EX_CALL_INFO() & GEAR_CALL_CODE))) {
						ZVAL_NULL(retval_ptr);
					} else {
						Z_ADDREF_P(return_value);
					}
				} else {
					retval_ptr = Z_REFVAL_P(retval_ptr);
					ZVAL_COPY(return_value, retval_ptr);
				}
			} else {
				ZVAL_COPY_VALUE(return_value, retval_ptr);
			}
		} else /* if (OP1_TYPE == IS_VAR) */ {
			if (UNEXPECTED(Z_ISREF_P(retval_ptr))) {
				gear_refcounted *ref = Z_COUNTED_P(retval_ptr);

				retval_ptr = Z_REFVAL_P(retval_ptr);
				ZVAL_COPY_VALUE(return_value, retval_ptr);
				if (UNEXPECTED(GC_DELREF(ref) == 0)) {
					efree_size(ref, sizeof(gear_reference));
				} else if (Z_OPT_REFCOUNTED_P(retval_ptr)) {
					Z_ADDREF_P(retval_ptr);
				}
			} else {
				ZVAL_COPY_VALUE(return_value, retval_ptr);
			}
		}
	}
	GEAR_VM_DISPATCH_TO_HELPER(gear_leave_helper);
}

GEAR_VM_COLD_CONST_HANDLER(111, GEAR_RETURN_BY_REF, CONST|TMP|VAR|CV, ANY, SRC)
{
	USE_OPLINE
	zval *retval_ptr;
	gear_free_op free_op1;

	SAVE_OPLINE();

	do {
		if ((OP1_TYPE & (IS_CONST|IS_TMP_VAR)) ||
		    (OP1_TYPE == IS_VAR && opline->extended_value == GEAR_RETURNS_VALUE)) {
			/* Not supposed to happen, but we'll allow it */
			gear_error(E_NOTICE, "Only variable references should be returned by reference");

			retval_ptr = GET_OP1_ZVAL_PTR(BP_VAR_R);
			if (!EX(return_value)) {
				FREE_OP1();
			} else {
				if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISREF_P(retval_ptr))) {
					ZVAL_COPY_VALUE(EX(return_value), retval_ptr);
					break;
				}

				ZVAL_NEW_REF(EX(return_value), retval_ptr);
				if (OP1_TYPE == IS_CONST) {
					Z_TRY_ADDREF_P(retval_ptr);
				}
			}
			break;
		}

		retval_ptr = GET_OP1_ZVAL_PTR_PTR(BP_VAR_W);

		if (OP1_TYPE == IS_VAR) {
			if (retval_ptr == &EG(uninitialized_zval) ||
			    (opline->extended_value == GEAR_RETURNS_FUNCTION && !Z_ISREF_P(retval_ptr))) {
				gear_error(E_NOTICE, "Only variable references should be returned by reference");
				if (EX(return_value)) {
					ZVAL_NEW_REF(EX(return_value), retval_ptr);
				} else {
					FREE_OP1_VAR_PTR();
				}
				break;
			}
		}

		if (EX(return_value)) {
			if (Z_ISREF_P(retval_ptr)) {
				Z_ADDREF_P(retval_ptr);
			} else {
				ZVAL_MAKE_REF_EX(retval_ptr, 2);
			}
			ZVAL_REF(EX(return_value), Z_REF_P(retval_ptr));
		}

		FREE_OP1_VAR_PTR();
	} while (0);

	GEAR_VM_DISPATCH_TO_HELPER(gear_leave_helper);
}

GEAR_VM_HANDLER(41, GEAR_GENERATOR_CREATE, ANY, ANY)
{
	zval *return_value = EX(return_value);

	if (EXPECTED(return_value)) {
		USE_OPLINE
		gear_generator *generator;
		gear_execute_data *gen_execute_data;
		uint32_t num_args, used_stack, call_info;

		object_init_ex(return_value, gear_ce_generator);

		/*
		 * Normally the execute_data is allocated on the VM stack (because it does
		 * not actually do any allocation and thus is faster). For generators
		 * though this behavior would be suboptimal, because the (rather large)
		 * structure would have to be copied back and forth every time execution is
		 * suspended or resumed. That's why for generators the execution context
		 * is allocated on heap.
		 */
		num_args = EX_NUM_ARGS();
		if (EXPECTED(num_args <= EX(func)->op_array.num_args)) {
			used_stack = (GEAR_CALL_FRAME_SLOT + EX(func)->op_array.last_var + EX(func)->op_array.T) * sizeof(zval);
			gen_execute_data = (gear_execute_data*)emalloc(used_stack);
			used_stack = (GEAR_CALL_FRAME_SLOT + EX(func)->op_array.last_var) * sizeof(zval);
		} else {
			used_stack = (GEAR_CALL_FRAME_SLOT + num_args + EX(func)->op_array.last_var + EX(func)->op_array.T - EX(func)->op_array.num_args) * sizeof(zval);
			gen_execute_data = (gear_execute_data*)emalloc(used_stack);
		}
		memcpy(gen_execute_data, execute_data, used_stack);

		/* Save execution context in generator object. */
		generator = (gear_generator *) Z_OBJ_P(EX(return_value));
		generator->execute_data = gen_execute_data;
		generator->frozen_call_stack = NULL;
		generator->execute_fake.opline = NULL;
		generator->execute_fake.func = NULL;
		generator->execute_fake.prev_execute_data = NULL;
		ZVAL_OBJ(&generator->execute_fake.This, (gear_object *) generator);

		gen_execute_data->opline = opline + 1;
		/* EX(return_value) keeps pointer to gear_object (not a real zval) */
		gen_execute_data->return_value = (zval*)generator;
		call_info = Z_TYPE_INFO(EX(This));
		if ((call_info & Z_TYPE_MASK) == IS_OBJECT
		 && (!(call_info & ((GEAR_CALL_CLOSURE|GEAR_CALL_RELEASE_THIS) << GEAR_CALL_INFO_SHIFT))
			 /* Bug #72523 */
			|| UNEXPECTED(gear_execute_ex != execute_ex))) {
			GEAR_ADD_CALL_FLAG_EX(call_info, GEAR_CALL_RELEASE_THIS);
			Z_ADDREF(gen_execute_data->This);
		}
		GEAR_ADD_CALL_FLAG_EX(call_info, (GEAR_CALL_TOP_FUNCTION | GEAR_CALL_ALLOCATED | GEAR_CALL_GENERATOR));
		Z_TYPE_INFO(gen_execute_data->This) = call_info;
		gen_execute_data->prev_execute_data = NULL;

		call_info = EX_CALL_INFO();
		EG(current_execute_data) = EX(prev_execute_data);
		if (EXPECTED(!(call_info & (GEAR_CALL_TOP|GEAR_CALL_ALLOCATED)))) {
			EG(vm_stack_top) = (zval*)execute_data;
			execute_data = EX(prev_execute_data);
			LOAD_NEXT_OPLINE();
			GEAR_VM_LEAVE();
		} else if (EXPECTED(!(call_info & GEAR_CALL_TOP))) {
			gear_execute_data *old_execute_data = execute_data;
			execute_data = EX(prev_execute_data);
			gear_vm_stack_free_call_frame_ex(call_info, old_execute_data);
			LOAD_NEXT_OPLINE();
			GEAR_VM_LEAVE();
		} else {
			GEAR_VM_RETURN();
		}
	} else {
		GEAR_VM_DISPATCH_TO_HELPER(gear_leave_helper);
	}
}

GEAR_VM_HANDLER(161, GEAR_GENERATOR_RETURN, CONST|TMP|VAR|CV, ANY)
{
	USE_OPLINE
	zval *retval;
	gear_free_op free_op1;

	gear_generator *generator = gear_get_running_generator(EXECUTE_DATA_C);

	SAVE_OPLINE();
	retval = GET_OP1_ZVAL_PTR(BP_VAR_R);

	/* Copy return value into generator->retval */
	if ((OP1_TYPE & (IS_CONST|IS_TMP_VAR))) {
		ZVAL_COPY_VALUE(&generator->retval, retval);
		if (OP1_TYPE == IS_CONST) {
			if (UNEXPECTED(Z_OPT_REFCOUNTED(generator->retval))) {
				Z_ADDREF(generator->retval);
			}
		}
	} else if (OP1_TYPE == IS_CV) {
		ZVAL_COPY_DEREF(&generator->retval, retval);
	} else /* if (OP1_TYPE == IS_VAR) */ {
		if (UNEXPECTED(Z_ISREF_P(retval))) {
			gear_refcounted *ref = Z_COUNTED_P(retval);

			retval = Z_REFVAL_P(retval);
			ZVAL_COPY_VALUE(&generator->retval, retval);
			if (UNEXPECTED(GC_DELREF(ref) == 0)) {
				efree_size(ref, sizeof(gear_reference));
			} else if (Z_OPT_REFCOUNTED_P(retval)) {
				Z_ADDREF_P(retval);
			}
		} else {
			ZVAL_COPY_VALUE(&generator->retval, retval);
		}
	}

	/* Close the generator to free up resources */
	gear_generator_close(generator, 1);

	/* Pass execution back to handling code */
	GEAR_VM_RETURN();
}

GEAR_VM_COLD_CONST_HANDLER(108, GEAR_THROW, CONST|TMP|VAR|CV, ANY)
{
	USE_OPLINE
	zval *value;
	gear_free_op free_op1;

	SAVE_OPLINE();
	value = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	do {
		if (OP1_TYPE == IS_CONST || UNEXPECTED(Z_TYPE_P(value) != IS_OBJECT)) {
			if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(value)) {
				value = Z_REFVAL_P(value);
				if (EXPECTED(Z_TYPE_P(value) == IS_OBJECT)) {
					break;
				}
			}
			if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(value) == IS_UNDEF)) {
				GET_OP1_UNDEF_CV(value, BP_VAR_R);
				if (UNEXPECTED(EG(exception) != NULL)) {
					HANDLE_EXCEPTION();
				}
			}
			gear_throw_error(NULL, "Can only throw objects");
			FREE_OP1();
			HANDLE_EXCEPTION();
		}
	} while (0);

	gear_exception_save();
	if (OP1_TYPE != IS_TMP_VAR) {
		Z_TRY_ADDREF_P(value);
	}

	gear_throw_exception_object(value);
	gear_exception_restore();
	FREE_OP1_IF_VAR();
	HANDLE_EXCEPTION();
}

GEAR_VM_HANDLER(107, GEAR_CATCH, CONST, JMP_ADDR, LAST_CATCH|CACHE_SLOT)
{
	USE_OPLINE
	gear_class_entry *ce, *catch_ce;
	gear_object *exception;
	zval *ex;

	SAVE_OPLINE();
	/* Check whether an exception has been thrown, if not, jump over code */
	gear_exception_restore();
	if (EG(exception) == NULL) {
		GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
	}
	catch_ce = CACHED_PTR(opline->extended_value & ~GEAR_LAST_CATCH);
	if (UNEXPECTED(catch_ce == NULL)) {
		catch_ce = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op1)), RT_CONSTANT(opline, opline->op1) + 1, GEAR_FETCH_CLASS_NO_AUTOLOAD);

		CACHE_PTR(opline->extended_value & ~GEAR_LAST_CATCH, catch_ce);
	}
	ce = EG(exception)->ce;

#ifdef HAVE_DTRACE
	if (DTRACE_EXCEPTION_CAUGHT_ENABLED()) {
		DTRACE_EXCEPTION_CAUGHT((char *)ce->name);
	}
#endif /* HAVE_DTRACE */

	if (ce != catch_ce) {
		if (!catch_ce || !instanceof_function(ce, catch_ce)) {
			if (opline->extended_value & GEAR_LAST_CATCH) {
				gear_rethrow_exception(execute_data);
				HANDLE_EXCEPTION();
			}
			GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
		}
	}

	exception = EG(exception);
	ex = EX_VAR(opline->result.var);
	if (UNEXPECTED(Z_ISREF_P(ex))) {
		ex = Z_REFVAL_P(ex);
	}
	zval_ptr_dtor(ex);
	ZVAL_OBJ(ex, EG(exception));
	if (UNEXPECTED(EG(exception) != exception)) {
		GC_ADDREF(EG(exception));
		HANDLE_EXCEPTION();
	} else {
		EG(exception) = NULL;
		GEAR_VM_NEXT_OPCODE();
	}
}

GEAR_VM_HOT_HANDLER(65, GEAR_SEND_VAL, CONST|TMPVAR, NUM)
{
	USE_OPLINE
	zval *value, *arg;
	gear_free_op free_op1;

	value = GET_OP1_ZVAL_PTR(BP_VAR_R);
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);
	ZVAL_COPY_VALUE(arg, value);
	if (OP1_TYPE == IS_CONST) {
		if (UNEXPECTED(Z_OPT_REFCOUNTED_P(arg))) {
			Z_ADDREF_P(arg);
		}
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_HELPER(gear_cannot_pass_by_ref_helper, ANY, ANY)
{
	USE_OPLINE
	zval *arg;
	uint32_t arg_num = opline->op2.num;

	SAVE_OPLINE();
	gear_throw_error(NULL, "Cannot pass parameter %d by reference", arg_num);
	FREE_UNFETCHED_OP1();
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);
	ZVAL_UNDEF(arg);
	HANDLE_EXCEPTION();
}

GEAR_VM_HOT_SEND_HANDLER(116, GEAR_SEND_VAL_EX, CONST|TMP, NUM, SPEC(QUICK_ARG))
{
	USE_OPLINE
	zval *value, *arg;
	gear_free_op free_op1;
	uint32_t arg_num = opline->op2.num;

	if (EXPECTED(arg_num <= MAX_ARG_FLAG_NUM)) {
		if (QUICK_ARG_MUST_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
			GEAR_VM_C_GOTO(send_val_by_ref);
		}
	} else if (ARG_MUST_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
GEAR_VM_C_LABEL(send_val_by_ref):
		GEAR_VM_DISPATCH_TO_HELPER(gear_cannot_pass_by_ref_helper);
	}
	value = GET_OP1_ZVAL_PTR(BP_VAR_R);
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);
	ZVAL_COPY_VALUE(arg, value);
	if (OP1_TYPE == IS_CONST) {
		if (UNEXPECTED(Z_OPT_REFCOUNTED_P(arg))) {
			Z_ADDREF_P(arg);
		}
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(117, GEAR_SEND_VAR, VAR|CV, NUM)
{
	USE_OPLINE
	zval *varptr, *arg;
	gear_free_op free_op1;

	varptr = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(varptr) == IS_UNDEF)) {
		SAVE_OPLINE();
		GET_OP1_UNDEF_CV(varptr, BP_VAR_R);
		arg = GEAR_CALL_VAR(EX(call), opline->result.var);
		ZVAL_NULL(arg);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}

	arg = GEAR_CALL_VAR(EX(call), opline->result.var);

	if (OP1_TYPE == IS_CV) {
		ZVAL_COPY_DEREF(arg, varptr);
	} else /* if (OP1_TYPE == IS_VAR) */ {
		if (UNEXPECTED(Z_ISREF_P(varptr))) {
			gear_refcounted *ref = Z_COUNTED_P(varptr);

			varptr = Z_REFVAL_P(varptr);
			ZVAL_COPY_VALUE(arg, varptr);
			if (UNEXPECTED(GC_DELREF(ref) == 0)) {
				efree_size(ref, sizeof(gear_reference));
			} else if (Z_OPT_REFCOUNTED_P(arg)) {
				Z_ADDREF_P(arg);
			}
		} else {
			ZVAL_COPY_VALUE(arg, varptr);
		}
	}

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(106, GEAR_SEND_VAR_NO_REF, VAR, NUM)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *varptr, *arg;

	varptr = GET_OP1_ZVAL_PTR(BP_VAR_R);
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);
	ZVAL_COPY_VALUE(arg, varptr);

	if (EXPECTED(Z_ISREF_P(varptr))) {
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	gear_error(E_NOTICE, "Only variables should be passed by reference");
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_SEND_HANDLER(50, GEAR_SEND_VAR_NO_REF_EX, VAR, NUM, SPEC(QUICK_ARG))
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *varptr, *arg;
	uint32_t arg_num = opline->op2.num;

	if (EXPECTED(arg_num <= MAX_ARG_FLAG_NUM)) {
		if (!QUICK_ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
			GEAR_VM_DISPATCH_TO_HANDLER(GEAR_SEND_VAR);
		}

		varptr = GET_OP1_ZVAL_PTR(BP_VAR_R);
		arg = GEAR_CALL_VAR(EX(call), opline->result.var);
		ZVAL_COPY_VALUE(arg, varptr);

		if (EXPECTED(Z_ISREF_P(varptr) ||
		    QUICK_ARG_MAY_BE_SENT_BY_REF(EX(call)->func, arg_num))) {
			GEAR_VM_NEXT_OPCODE();
		}
	} else {
		if (!ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
			GEAR_VM_DISPATCH_TO_HANDLER(GEAR_SEND_VAR);
		}

		varptr = GET_OP1_ZVAL_PTR(BP_VAR_R);
		arg = GEAR_CALL_VAR(EX(call), opline->result.var);
		ZVAL_COPY_VALUE(arg, varptr);

		if (EXPECTED(Z_ISREF_P(varptr) ||
		    ARG_MAY_BE_SENT_BY_REF(EX(call)->func, arg_num))) {
			GEAR_VM_NEXT_OPCODE();
		}
	}

	SAVE_OPLINE();
	gear_error(E_NOTICE, "Only variables should be passed by reference");
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(67, GEAR_SEND_REF, VAR|CV, NUM)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *varptr, *arg;

	SAVE_OPLINE();
	varptr = GET_OP1_ZVAL_PTR_PTR(BP_VAR_W);

	arg = GEAR_CALL_VAR(EX(call), opline->result.var);
	if (OP1_TYPE == IS_VAR && UNEXPECTED(Z_ISERROR_P(varptr))) {
		ZVAL_NEW_EMPTY_REF(arg);
		ZVAL_NULL(Z_REFVAL_P(arg));
		GEAR_VM_NEXT_OPCODE();
	}

	if (Z_ISREF_P(varptr)) {
		Z_ADDREF_P(varptr);
	} else {
		ZVAL_MAKE_REF_EX(varptr, 2);
	}
	ZVAL_REF(arg, Z_REF_P(varptr));

	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_SEND_HANDLER(66, GEAR_SEND_VAR_EX, VAR|CV, NUM, SPEC(QUICK_ARG))
{
	USE_OPLINE
	zval *varptr, *arg;
	gear_free_op free_op1;
	uint32_t arg_num = opline->op2.num;

	if (EXPECTED(arg_num <= MAX_ARG_FLAG_NUM)) {
		if (QUICK_ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
			GEAR_VM_C_GOTO(send_var_by_ref);
		}
	} else if (ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
GEAR_VM_C_LABEL(send_var_by_ref):
		GEAR_VM_DISPATCH_TO_HANDLER(GEAR_SEND_REF);
	}

	varptr = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(varptr) == IS_UNDEF)) {
		SAVE_OPLINE();
		GET_OP1_UNDEF_CV(varptr, BP_VAR_R);
		arg = GEAR_CALL_VAR(EX(call), opline->result.var);
		ZVAL_NULL(arg);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}

	arg = GEAR_CALL_VAR(EX(call), opline->result.var);

	if (OP1_TYPE == IS_CV) {
		ZVAL_COPY_DEREF(arg, varptr);
	} else /* if (OP1_TYPE == IS_VAR) */ {
		if (UNEXPECTED(Z_ISREF_P(varptr))) {
			gear_refcounted *ref = Z_COUNTED_P(varptr);

			varptr = Z_REFVAL_P(varptr);
			ZVAL_COPY_VALUE(arg, varptr);
			if (UNEXPECTED(GC_DELREF(ref) == 0)) {
				efree_size(ref, sizeof(gear_reference));
			} else if (Z_OPT_REFCOUNTED_P(arg)) {
				Z_ADDREF_P(arg);
			}
		} else {
			ZVAL_COPY_VALUE(arg, varptr);
		}
	}

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_SEND_HANDLER(100, GEAR_CHECK_FUNC_ARG, UNUSED, NUM, SPEC(QUICK_ARG))
{
	USE_OPLINE
	uint32_t arg_num = opline->op2.num;

	if (EXPECTED(arg_num <= MAX_ARG_FLAG_NUM)) {
		if (QUICK_ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
			GEAR_ADD_CALL_FLAG(EX(call), GEAR_CALL_SEND_ARG_BY_REF);
		} else {
			GEAR_DEL_CALL_FLAG(EX(call), GEAR_CALL_SEND_ARG_BY_REF);
		}
	} else if (ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
		GEAR_ADD_CALL_FLAG(EX(call), GEAR_CALL_SEND_ARG_BY_REF);
	} else {
		GEAR_DEL_CALL_FLAG(EX(call), GEAR_CALL_SEND_ARG_BY_REF);
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(185, GEAR_SEND_FUNC_ARG, VAR, NUM)
{
	USE_OPLINE
	zval *varptr, *arg;
	gear_free_op free_op1;

	if (UNEXPECTED(GEAR_CALL_INFO(EX(call)) & GEAR_CALL_SEND_ARG_BY_REF)) {
		GEAR_VM_DISPATCH_TO_HANDLER(GEAR_SEND_REF);
	}

	varptr = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);

	if (UNEXPECTED(Z_ISREF_P(varptr))) {
		gear_refcounted *ref = Z_COUNTED_P(varptr);

		varptr = Z_REFVAL_P(varptr);
		ZVAL_COPY_VALUE(arg, varptr);
		if (UNEXPECTED(GC_DELREF(ref) == 0)) {
			efree_size(ref, sizeof(gear_reference));
		} else if (Z_OPT_REFCOUNTED_P(arg)) {
			Z_ADDREF_P(arg);
		}
	} else {
		ZVAL_COPY_VALUE(arg, varptr);
	}

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(165, GEAR_SEND_UNPACK, ANY, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *args;
	int arg_num;

	SAVE_OPLINE();
	args = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	arg_num = GEAR_CALL_NUM_ARGS(EX(call)) + 1;

GEAR_VM_C_LABEL(send_again):
	if (EXPECTED(Z_TYPE_P(args) == IS_ARRAY)) {
		HashTable *ht = Z_ARRVAL_P(args);
		zval *arg, *top;
		gear_string *name;

		gear_vm_stack_extend_call_frame(&EX(call), arg_num - 1, gear_hash_num_elements(ht));

		if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_REFCOUNT_P(args) > 1) {
			uint32_t i;
			int separate = 0;

			/* check if any of arguments are going to be passed by reference */
			for (i = 0; i < gear_hash_num_elements(ht); i++) {
				if (ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num + i)) {
					separate = 1;
					break;
				}
			}
			if (separate) {
				SEPARATE_ARRAY(args);
				ht = Z_ARRVAL_P(args);
			}
		}

		GEAR_HASH_FOREACH_STR_KEY_VAL(ht, name, arg) {
			if (name) {
				gear_throw_error(NULL, "Cannot unpack array with string keys");
				FREE_OP1();
				HANDLE_EXCEPTION();
			}

			top = GEAR_CALL_ARG(EX(call), arg_num);
			if (ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
				if (Z_ISREF_P(arg)) {
					Z_ADDREF_P(arg);
					ZVAL_REF(top, Z_REF_P(arg));
				} else if (OP1_TYPE & (IS_VAR|IS_CV)) {
					/* array is already separated above */
					ZVAL_MAKE_REF_EX(arg, 2);
					ZVAL_REF(top, Z_REF_P(arg));
				} else {
					Z_TRY_ADDREF_P(arg);
					ZVAL_NEW_REF(top, arg);
				}
			} else {
				ZVAL_COPY_DEREF(top, arg);
			}

			GEAR_CALL_NUM_ARGS(EX(call))++;
			arg_num++;
		} GEAR_HASH_FOREACH_END();

	} else if (EXPECTED(Z_TYPE_P(args) == IS_OBJECT)) {
		gear_class_entry *ce = Z_OBJCE_P(args);
		gear_object_iterator *iter;

		if (!ce || !ce->get_iterator) {
			gear_error(E_WARNING, "Only arrays and Traversables can be unpacked");
		} else {

			iter = ce->get_iterator(ce, args, 0);
			if (UNEXPECTED(!iter)) {
				FREE_OP1();
				if (!EG(exception)) {
					gear_throw_exception_ex(
						NULL, 0, "Object of type %s did not create an Iterator", ZSTR_VAL(ce->name)
					);
				}
				HANDLE_EXCEPTION();
			}

			if (iter->funcs->rewind) {
				iter->funcs->rewind(iter);
			}

			for (; iter->funcs->valid(iter) == SUCCESS; ++arg_num) {
				zval *arg, *top;

				if (UNEXPECTED(EG(exception) != NULL)) {
					break;
				}

				arg = iter->funcs->get_current_data(iter);
				if (UNEXPECTED(EG(exception) != NULL)) {
					break;
				}

				if (iter->funcs->get_current_key) {
					zval key;
					iter->funcs->get_current_key(iter, &key);
					if (UNEXPECTED(EG(exception) != NULL)) {
						break;
					}

					if (UNEXPECTED(Z_TYPE(key) != IS_LONG)) {
						gear_throw_error(NULL,
							(Z_TYPE(key) == IS_STRING) ?
								"Cannot unpack Traversable with string keys" :
								"Cannot unpack Traversable with non-integer keys");
						zval_ptr_dtor(&key);
						break;
					}
				}

				if (ARG_MUST_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
					gear_error(
						E_WARNING, "Cannot pass by-reference argument %d of %s%s%s()"
						" by unpacking a Traversable, passing by-value instead", arg_num,
						EX(call)->func->common.scope ? ZSTR_VAL(EX(call)->func->common.scope->name) : "",
						EX(call)->func->common.scope ? "::" : "",
						ZSTR_VAL(EX(call)->func->common.function_name)
					);
				}

				ZVAL_DEREF(arg);
				Z_TRY_ADDREF_P(arg);

				gear_vm_stack_extend_call_frame(&EX(call), arg_num - 1, 1);
				top = GEAR_CALL_ARG(EX(call), arg_num);
				ZVAL_COPY_VALUE(top, arg);
				GEAR_CALL_NUM_ARGS(EX(call))++;

				iter->funcs->move_forward(iter);
			}

			gear_iterator_dtor(iter);
		}
	} else if (EXPECTED(Z_ISREF_P(args))) {
		args = Z_REFVAL_P(args);
		GEAR_VM_C_GOTO(send_again);
	} else {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(args) == IS_UNDEF)) {
			GET_OP1_UNDEF_CV(args, BP_VAR_R);
		}
		gear_error(E_WARNING, "Only arrays and Traversables can be unpacked");
	}

	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(119, GEAR_SEND_ARRAY, ANY, ANY, NUM)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *args;

	SAVE_OPLINE();
	args = GET_OP1_ZVAL_PTR(BP_VAR_R);

	if (UNEXPECTED(Z_TYPE_P(args) != IS_ARRAY)) {
		if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(args)) {
			args = Z_REFVAL_P(args);
			if (EXPECTED(Z_TYPE_P(args) == IS_ARRAY)) {
				GEAR_VM_C_GOTO(send_array);
			}
		}
		gear_internal_type_error(EX_USES_STRICT_TYPES(), "call_user_func_array() expects parameter 2 to be array, %s given", gear_get_type_by_const(Z_TYPE_P(args)));
		if (GEAR_CALL_INFO(EX(call)) & GEAR_CALL_CLOSURE) {
			OBJ_RELEASE(GEAR_CLOSURE_OBJECT(EX(call)->func));
		}
		if (Z_TYPE(EX(call)->This) == IS_OBJECT) {
			OBJ_RELEASE(Z_OBJ(EX(call)->This));
		}
		EX(call)->func = (gear_function*)&gear_pass_function;
		Z_OBJ(EX(call)->This) = NULL;
		GEAR_SET_CALL_INFO(EX(call), 0, GEAR_CALL_INFO(EX(call)) & ~GEAR_CALL_RELEASE_THIS);
		FREE_UNFETCHED_OP2();
	} else {
		uint32_t arg_num;
		HashTable *ht;
		zval *arg, *param;


GEAR_VM_C_LABEL(send_array):
		ht = Z_ARRVAL_P(args);
		if (OP2_TYPE != IS_UNUSED) {
			gear_free_op free_op2;
			zval *op2 = GET_OP2_ZVAL_PTR_DEREF(BP_VAR_R);
			uint32_t skip = opline->extended_value;
			uint32_t count = gear_hash_num_elements(ht);
			gear_long len = zval_get_long(op2);

			if (len < 0) {
				len += (gear_long)(count - skip);
			}
			if (skip < count && len > 0) {
				if (len > (gear_long)(count - skip)) {
					len = (gear_long)(count - skip);
				}
				gear_vm_stack_extend_call_frame(&EX(call), 0, len);
				arg_num = 1;
				param = GEAR_CALL_ARG(EX(call), 1);
				GEAR_HASH_FOREACH_VAL(ht, arg) {
					if (skip > 0) {
						skip--;
						continue;
					} else if ((gear_long)(arg_num - 1) >= len) {
						break;
					} else if (ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
						if (UNEXPECTED(!Z_ISREF_P(arg))) {
							if (!ARG_MAY_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
								/* By-value send is not allowed -- emit a warning,
								 * but still perform the call. */
								gear_param_must_be_ref(EX(call)->func, arg_num);
							}
						}
					} else {
						if (Z_ISREF_P(arg) &&
						    !(EX(call)->func->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE)) {
							/* don't separate references for __call */
							arg = Z_REFVAL_P(arg);
						}
					}
					ZVAL_COPY(param, arg);
					GEAR_CALL_NUM_ARGS(EX(call))++;
					arg_num++;
					param++;
				} GEAR_HASH_FOREACH_END();
			}
			FREE_OP2();
		} else {
			gear_vm_stack_extend_call_frame(&EX(call), 0, gear_hash_num_elements(ht));
			arg_num = 1;
			param = GEAR_CALL_ARG(EX(call), 1);
			GEAR_HASH_FOREACH_VAL(ht, arg) {
				if (ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
					if (UNEXPECTED(!Z_ISREF_P(arg))) {
						if (!ARG_MAY_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
							/* By-value send is not allowed -- emit a warning,
							 * but still perform the call. */
							gear_param_must_be_ref(EX(call)->func, arg_num);
						}
					}
				} else {
					if (Z_ISREF_P(arg) &&
					    !(EX(call)->func->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE)) {
						/* don't separate references for __call */
						arg = Z_REFVAL_P(arg);
					}
				}
				ZVAL_COPY(param, arg);
				GEAR_CALL_NUM_ARGS(EX(call))++;
				arg_num++;
				param++;
			} GEAR_HASH_FOREACH_END();
		}
	}
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(120, GEAR_SEND_USER, CONST|TMP|VAR|CV, NUM)
{
	USE_OPLINE
	zval *arg, *param;
	gear_free_op free_op1;

	SAVE_OPLINE();
	arg = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);
	param = GEAR_CALL_VAR(EX(call), opline->result.var);

	if (UNEXPECTED(ARG_MUST_BE_SENT_BY_REF(EX(call)->func, opline->op2.num))) {
		gear_param_must_be_ref(EX(call)->func, opline->op2.num);
	}

	ZVAL_COPY(param, arg);

	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_HANDLER(63, GEAR_RECV, NUM, UNUSED|CACHE_SLOT)
{
	USE_OPLINE
	uint32_t arg_num = opline->op1.num;

	if (UNEXPECTED(arg_num > EX_NUM_ARGS())) {
		SAVE_OPLINE();
		gear_missing_arg_error(execute_data);
		HANDLE_EXCEPTION();
	} else if (UNEXPECTED((EX(func)->op_array.fn_flags & GEAR_ACC_HAS_TYPE_HINTS) != 0)) {
		zval *param = EX_VAR(opline->result.var);

		SAVE_OPLINE();
		if (UNEXPECTED(!gear_verify_arg_type(EX(func), arg_num, param, NULL, CACHE_ADDR(opline->op2.num)) || EG(exception))) {
			HANDLE_EXCEPTION();
		}
	}

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(64, GEAR_RECV_INIT, NUM, CONST, CACHE_SLOT)
{
	USE_OPLINE
	uint32_t arg_num;
	zval *param;

	GEAR_VM_REPEATABLE_OPCODE

	arg_num = opline->op1.num;
	param = EX_VAR(opline->result.var);
	if (arg_num > EX_NUM_ARGS()) {
		zval *default_value = RT_CONSTANT(opline, opline->op2);

		if (Z_OPT_TYPE_P(default_value) == IS_CONSTANT_AST) {
			zval *cache_val = (zval*)CACHE_ADDR(Z_CACHE_SLOT_P(default_value));

			/* we keep in cache only not refcounted values */
			if (Z_TYPE_P(cache_val) != IS_UNDEF) {
				ZVAL_COPY_VALUE(param, cache_val);
			} else {
				SAVE_OPLINE();
				ZVAL_COPY(param, default_value);
				if (UNEXPECTED(zval_update_constant_ex(param, EX(func)->op_array.scope) != SUCCESS)) {
					zval_ptr_dtor_nogc(param);
					ZVAL_UNDEF(param);
					HANDLE_EXCEPTION();
				}
				if (!Z_REFCOUNTED_P(param)) {
					ZVAL_COPY_VALUE(cache_val, param);
				}
			}
		} else {
			ZVAL_COPY(param, default_value);
		}
	}

	if (UNEXPECTED((EX(func)->op_array.fn_flags & GEAR_ACC_HAS_TYPE_HINTS) != 0)) {
		zval *default_value = RT_CONSTANT(opline, opline->op2);

		SAVE_OPLINE();
		if (UNEXPECTED(!gear_verify_arg_type(EX(func), arg_num, param, default_value, CACHE_ADDR(opline->extended_value)) || EG(exception))) {
			HANDLE_EXCEPTION();
		}
	}

	GEAR_VM_REPEAT_OPCODE(GEAR_RECV_INIT);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(164, GEAR_RECV_VARIADIC, NUM, UNUSED|CACHE_SLOT)
{
	USE_OPLINE
	uint32_t arg_num = opline->op1.num;
	uint32_t arg_count = EX_NUM_ARGS();
	zval *params;

	SAVE_OPLINE();

	params = EX_VAR(opline->result.var);

	if (arg_num <= arg_count) {
		zval *param;

		array_init_size(params, arg_count - arg_num + 1);
		gear_hash_real_init_packed(Z_ARRVAL_P(params));
		GEAR_HASH_FILL_PACKED(Z_ARRVAL_P(params)) {
			param = EX_VAR_NUM(EX(func)->op_array.last_var + EX(func)->op_array.T);
			if (UNEXPECTED((EX(func)->op_array.fn_flags & GEAR_ACC_HAS_TYPE_HINTS) != 0)) {
				do {
					gear_verify_arg_type(EX(func), arg_num, param, NULL, CACHE_ADDR(opline->op2.num));
					if (Z_OPT_REFCOUNTED_P(param)) Z_ADDREF_P(param);
					GEAR_HASH_FILL_ADD(param);
					param++;
				} while (++arg_num <= arg_count);
			} else {
				do {
					if (Z_OPT_REFCOUNTED_P(param)) Z_ADDREF_P(param);
					GEAR_HASH_FILL_ADD(param);
					param++;
				} while (++arg_num <= arg_count);
			}
		} GEAR_HASH_FILL_END();
	} else {
		ZVAL_EMPTY_ARRAY(params);
	}

	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONST_HANDLER(52, GEAR_BOOL, CONST|TMPVAR|CV, ANY)
{
	USE_OPLINE
	zval *val;
	gear_free_op free_op1;

	val = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (Z_TYPE_INFO_P(val) == IS_TRUE) {
		ZVAL_TRUE(EX_VAR(opline->result.var));
	} else if (EXPECTED(Z_TYPE_INFO_P(val) <= IS_TRUE)) {
		ZVAL_FALSE(EX_VAR(opline->result.var));
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_INFO_P(val) == IS_UNDEF)) {
			SAVE_OPLINE();
			GET_OP1_UNDEF_CV(val, BP_VAR_R);
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		}
	} else {
		SAVE_OPLINE();
		ZVAL_BOOL(EX_VAR(opline->result.var), i_gear_is_true(val));
		FREE_OP1();
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(48, GEAR_CASE, TMPVAR, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	do {
		int result;

		if (EXPECTED(Z_TYPE_P(op1) == IS_LONG)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_LONG)) {
				result = (Z_LVAL_P(op1) == Z_LVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_P(op2) == IS_DOUBLE)) {
				result = ((double)Z_LVAL_P(op1) == Z_DVAL_P(op2));
			} else {
				break;
			}
		} else if (EXPECTED(Z_TYPE_P(op1) == IS_DOUBLE)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_DOUBLE)) {
				result = (Z_DVAL_P(op1) == Z_DVAL_P(op2));
			} else if (EXPECTED(Z_TYPE_P(op2) == IS_LONG)) {
				result = (Z_DVAL_P(op1) == ((double)Z_LVAL_P(op2)));
			} else {
				break;
			}
		} else if (EXPECTED(Z_TYPE_P(op1) == IS_STRING)) {
			if (EXPECTED(Z_TYPE_P(op2) == IS_STRING)) {
				result = gear_fast_equal_strings(Z_STR_P(op1), Z_STR_P(op2));
				FREE_OP2();
			} else {
				break;
			}
		} else {
			break;
		}
		GEAR_VM_SMART_BRANCH(result, 0);
		ZVAL_BOOL(EX_VAR(opline->result.var), result);
		GEAR_VM_NEXT_OPCODE();
	} while (0);

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op1) == IS_UNDEF)) {
		op1 = GET_OP1_UNDEF_CV(op1, BP_VAR_R);
	}
	if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(op2) == IS_UNDEF)) {
		op2 = GET_OP2_UNDEF_CV(op2, BP_VAR_R);
	}
	result = EX_VAR(opline->result.var);
	compare_function(result, op1, op2);
	ZVAL_BOOL(result, Z_LVAL_P(result) == 0);
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(68, GEAR_NEW, UNUSED|CLASS_FETCH|CONST|VAR, UNUSED|CACHE_SLOT, NUM)
{
	USE_OPLINE
	zval *result;
	gear_function *constructor;
	gear_class_entry *ce;
	gear_execute_data *call;

	SAVE_OPLINE();
	if (OP1_TYPE == IS_CONST) {
		ce = CACHED_PTR(opline->op2.num);
		if (UNEXPECTED(ce == NULL)) {
			ce = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op1)), RT_CONSTANT(opline, opline->op1) + 1, GEAR_FETCH_CLASS_DEFAULT | GEAR_FETCH_CLASS_EXCEPTION);
			if (UNEXPECTED(ce == NULL)) {
				GEAR_ASSERT(EG(exception));
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
			CACHE_PTR(opline->op2.num, ce);
		}
	} else if (OP1_TYPE == IS_UNUSED) {
		ce = gear_fetch_class(NULL, opline->op1.num);
		if (UNEXPECTED(ce == NULL)) {
			GEAR_ASSERT(EG(exception));
			ZVAL_UNDEF(EX_VAR(opline->result.var));
			HANDLE_EXCEPTION();
		}
	} else {
		ce = Z_CE_P(EX_VAR(opline->op1.var));
	}

	result = EX_VAR(opline->result.var);
	if (UNEXPECTED(object_init_ex(result, ce) != SUCCESS)) {
		ZVAL_UNDEF(result);
		HANDLE_EXCEPTION();
	}

	constructor = Z_OBJ_HT_P(result)->get_constructor(Z_OBJ_P(result));
	if (constructor == NULL) {
		if (UNEXPECTED(EG(exception))) {
			HANDLE_EXCEPTION();
		}

		/* If there are no arguments, skip over the DO_FCALL opcode. We check if the next
		 * opcode is DO_FCALL in case EXT instructions are used. */
		if (EXPECTED(opline->extended_value == 0 && (opline+1)->opcode == GEAR_DO_FCALL)) {
			GEAR_VM_NEXT_OPCODE_EX(1, 2);
		}

		/* Perform a dummy function call */
		call = gear_vm_stack_push_call_frame(
			GEAR_CALL_FUNCTION, (gear_function *) &gear_pass_function,
			opline->extended_value, NULL, NULL);
	} else {
		if (EXPECTED(constructor->type == GEAR_USER_FUNCTION) && UNEXPECTED(!constructor->op_array.run_time_cache)) {
			init_func_run_time_cache(&constructor->op_array);
		}
		/* We are not handling overloaded classes right now */
		call = gear_vm_stack_push_call_frame(
			GEAR_CALL_FUNCTION | GEAR_CALL_RELEASE_THIS | GEAR_CALL_CTOR,
			constructor,
			opline->extended_value,
			ce,
			Z_OBJ_P(result));
		Z_ADDREF_P(result);
	}

	call->prev_execute_data = EX(call);
	EX(call) = call;
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_CONST_HANDLER(110, GEAR_CLONE, CONST|TMPVAR|UNUSED|THIS|CV, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *obj;
	gear_class_entry *ce, *scope;
	gear_function *clone;
	gear_object_clone_obj_t clone_call;

	SAVE_OPLINE();
	obj = GET_OP1_OBJ_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(obj) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	do {
		if (OP1_TYPE == IS_CONST ||
		    (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(obj) != IS_OBJECT))) {
		    if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(obj)) {
				obj = Z_REFVAL_P(obj);
				if (EXPECTED(Z_TYPE_P(obj) == IS_OBJECT)) {
					break;
				}
			}
			ZVAL_UNDEF(EX_VAR(opline->result.var));
			if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(obj) == IS_UNDEF)) {
				GET_OP1_UNDEF_CV(obj, BP_VAR_R);
				if (UNEXPECTED(EG(exception) != NULL)) {
					HANDLE_EXCEPTION();
				}
			}
			gear_throw_error(NULL, "__clone method called on non-object");
			FREE_OP1();
			HANDLE_EXCEPTION();
		}
	} while (0);

	ce = Z_OBJCE_P(obj);
	clone = ce->clone;
	clone_call = Z_OBJ_HT_P(obj)->clone_obj;
	if (UNEXPECTED(clone_call == NULL)) {
		gear_throw_error(NULL, "Trying to clone an uncloneable object of class %s", ZSTR_VAL(ce->name));
		FREE_OP1();
		ZVAL_UNDEF(EX_VAR(opline->result.var));
		HANDLE_EXCEPTION();
	}

	if (clone) {
		if (clone->op_array.fn_flags & GEAR_ACC_PRIVATE) {
			/* Ensure that if we're calling a private function, we're allowed to do so.
			 */
			scope = EX(func)->op_array.scope;
			if (!gear_check_private(clone, scope, clone->common.function_name)) {
				gear_throw_error(NULL, "Call to private %s::__clone() from context '%s'", ZSTR_VAL(clone->common.scope->name), scope ? ZSTR_VAL(scope->name) : "");
				FREE_OP1();
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
		} else if ((clone->common.fn_flags & GEAR_ACC_PROTECTED)) {
			/* Ensure that if we're calling a protected function, we're allowed to do so.
			 */
			scope = EX(func)->op_array.scope;
			if (UNEXPECTED(!gear_check_protected(gear_get_function_root_class(clone), scope))) {
				gear_throw_error(NULL, "Call to protected %s::__clone() from context '%s'", ZSTR_VAL(clone->common.scope->name), scope ? ZSTR_VAL(scope->name) : "");
				FREE_OP1();
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
		}
	}

	ZVAL_OBJ(EX_VAR(opline->result.var), clone_call(obj));

	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_HANDLER(99, GEAR_FETCH_CONSTANT, UNUSED|CONST_FETCH, CONST, CACHE_SLOT)
{
	USE_OPLINE
	gear_constant *c;

	c = CACHED_PTR(opline->extended_value);
	if (EXPECTED(c != NULL) && EXPECTED(!IS_SPECIAL_CACHE_VAL(c))) {
		ZVAL_COPY_OR_DUP(EX_VAR(opline->result.var), &c->value);
		GEAR_VM_NEXT_OPCODE();
	}

	SAVE_OPLINE();
	gear_quick_get_constant(RT_CONSTANT(opline, opline->op2) + 1, opline->op1.num OPLINE_CC EXECUTE_DATA_CC);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(181, GEAR_FETCH_CLASS_CONSTANT, VAR|CONST|UNUSED|CLASS_FETCH, CONST, CACHE_SLOT)
{
	gear_class_entry *ce, *scope;
	gear_class_constant *c;
	zval *value, *zv;
	USE_OPLINE

	SAVE_OPLINE();

	do {
		if (OP1_TYPE == IS_CONST) {
			if (EXPECTED(CACHED_PTR(opline->extended_value + sizeof(void*)))) {
				value = CACHED_PTR(opline->extended_value + sizeof(void*));
				break;
			} else if (EXPECTED(CACHED_PTR(opline->extended_value))) {
				ce = CACHED_PTR(opline->extended_value);
			} else {
				ce = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op1)), RT_CONSTANT(opline, opline->op1) + 1, GEAR_FETCH_CLASS_DEFAULT | GEAR_FETCH_CLASS_EXCEPTION);
				if (UNEXPECTED(ce == NULL)) {
					GEAR_ASSERT(EG(exception));
					ZVAL_UNDEF(EX_VAR(opline->result.var));
					HANDLE_EXCEPTION();
				}
			}
		} else {
			if (OP1_TYPE == IS_UNUSED) {
				ce = gear_fetch_class(NULL, opline->op1.num);
				if (UNEXPECTED(ce == NULL)) {
					GEAR_ASSERT(EG(exception));
					ZVAL_UNDEF(EX_VAR(opline->result.var));
					HANDLE_EXCEPTION();
				}
			} else {
				ce = Z_CE_P(EX_VAR(opline->op1.var));
			}
			if (EXPECTED(CACHED_PTR(opline->extended_value) == ce)) {
				value = CACHED_PTR(opline->extended_value + sizeof(void*));
				break;
			}
		}

		zv = gear_hash_find_ex(&ce->constants_table, Z_STR_P(RT_CONSTANT(opline, opline->op2)), 1);
		if (EXPECTED(zv != NULL)) {
			c = Z_PTR_P(zv);
			scope = EX(func)->op_array.scope;
			if (!gear_verify_const_access(c, scope)) {
				gear_throw_error(NULL, "Cannot access %s const %s::%s", gear_visibility_string(Z_ACCESS_FLAGS(c->value)), ZSTR_VAL(ce->name), Z_STRVAL_P(RT_CONSTANT(opline, opline->op2)));
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
			value = &c->value;
			if (Z_TYPE_P(value) == IS_CONSTANT_AST) {
				zval_update_constant_ex(value, c->ce);
				if (UNEXPECTED(EG(exception) != NULL)) {
					ZVAL_UNDEF(EX_VAR(opline->result.var));
					HANDLE_EXCEPTION();
				}
			}
			CACHE_POLYMORPHIC_PTR(opline->extended_value, ce, value);
		} else {
			gear_throw_error(NULL, "Undefined class constant '%s'", Z_STRVAL_P(RT_CONSTANT(opline, opline->op2)));
			ZVAL_UNDEF(EX_VAR(opline->result.var));
			HANDLE_EXCEPTION();
		}
	} while (0);

	ZVAL_COPY_OR_DUP(EX_VAR(opline->result.var), value);

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(72, GEAR_ADD_ARRAY_ELEMENT, CONST|TMP|VAR|CV, CONST|TMPVAR|UNUSED|NEXT|CV, REF)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *expr_ptr, new_expr;

	SAVE_OPLINE();
	if ((OP1_TYPE == IS_VAR || OP1_TYPE == IS_CV) &&
	    UNEXPECTED(opline->extended_value & GEAR_ARRAY_ELEMENT_REF)) {
		expr_ptr = GET_OP1_ZVAL_PTR_PTR(BP_VAR_W);
		if (Z_ISREF_P(expr_ptr)) {
			Z_ADDREF_P(expr_ptr);
		} else {
			ZVAL_MAKE_REF_EX(expr_ptr, 2);
		}
		FREE_OP1_VAR_PTR();
	} else {
		expr_ptr = GET_OP1_ZVAL_PTR(BP_VAR_R);
		if (OP1_TYPE == IS_TMP_VAR) {
			/* pass */
		} else if (OP1_TYPE == IS_CONST) {
			Z_TRY_ADDREF_P(expr_ptr);
		} else if (OP1_TYPE == IS_CV) {
			ZVAL_DEREF(expr_ptr);
			Z_TRY_ADDREF_P(expr_ptr);
		} else /* if (OP1_TYPE == IS_VAR) */ {
			if (UNEXPECTED(Z_ISREF_P(expr_ptr))) {
				gear_refcounted *ref = Z_COUNTED_P(expr_ptr);

				expr_ptr = Z_REFVAL_P(expr_ptr);
				if (UNEXPECTED(GC_DELREF(ref) == 0)) {
					ZVAL_COPY_VALUE(&new_expr, expr_ptr);
					expr_ptr = &new_expr;
					efree_size(ref, sizeof(gear_reference));
				} else if (Z_OPT_REFCOUNTED_P(expr_ptr)) {
					Z_ADDREF_P(expr_ptr);
				}
			}
		}
	}

	if (OP2_TYPE != IS_UNUSED) {
		gear_free_op free_op2;
		zval *offset = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
		gear_string *str;
		gear_ulong hval;

GEAR_VM_C_LABEL(add_again):
		if (EXPECTED(Z_TYPE_P(offset) == IS_STRING)) {
			str = Z_STR_P(offset);
			if (OP2_TYPE != IS_CONST) {
				if (GEAR_HANDLE_NUMERIC(str, hval)) {
					GEAR_VM_C_GOTO(num_index);
				}
			}
GEAR_VM_C_LABEL(str_index):
			gear_hash_update(Z_ARRVAL_P(EX_VAR(opline->result.var)), str, expr_ptr);
		} else if (EXPECTED(Z_TYPE_P(offset) == IS_LONG)) {
			hval = Z_LVAL_P(offset);
GEAR_VM_C_LABEL(num_index):
			gear_hash_index_update(Z_ARRVAL_P(EX_VAR(opline->result.var)), hval, expr_ptr);
		} else if ((OP2_TYPE & (IS_VAR|IS_CV)) && EXPECTED(Z_TYPE_P(offset) == IS_REFERENCE)) {
			offset = Z_REFVAL_P(offset);
			GEAR_VM_C_GOTO(add_again);
		} else if (Z_TYPE_P(offset) == IS_NULL) {
			str = ZSTR_EMPTY_ALLOC();
			GEAR_VM_C_GOTO(str_index);
		} else if (Z_TYPE_P(offset) == IS_DOUBLE) {
			hval = gear_dval_to_lval(Z_DVAL_P(offset));
			GEAR_VM_C_GOTO(num_index);
		} else if (Z_TYPE_P(offset) == IS_FALSE) {
			hval = 0;
			GEAR_VM_C_GOTO(num_index);
		} else if (Z_TYPE_P(offset) == IS_TRUE) {
			hval = 1;
			GEAR_VM_C_GOTO(num_index);
		} else if (OP2_TYPE == IS_CV && Z_TYPE_P(offset) == IS_UNDEF) {
			GET_OP2_UNDEF_CV(offset, BP_VAR_R);
			str = ZSTR_EMPTY_ALLOC();
			GEAR_VM_C_GOTO(str_index);
		} else {
			gear_illegal_offset();
			zval_ptr_dtor_nogc(expr_ptr);
		}
		FREE_OP2();
	} else {
		if (!gear_hash_next_index_insert(Z_ARRVAL_P(EX_VAR(opline->result.var)), expr_ptr)) {
			gear_cannot_add_element();
			zval_ptr_dtor_nogc(expr_ptr);
		}
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(71, GEAR_INIT_ARRAY, CONST|TMP|VAR|CV, CONST|TMPVAR|UNUSED|NEXT|CV, ARRAY_INIT|REF)
{
	zval *array;
	uint32_t size;
	USE_OPLINE

	array = EX_VAR(opline->result.var);
	if (OP1_TYPE != IS_UNUSED) {
		size = opline->extended_value >> GEAR_ARRAY_SIZE_SHIFT;
		ZVAL_ARR(array, gear_new_array(size));
		/* Explicitly initialize array as not-packed if flag is set */
		if (opline->extended_value & GEAR_ARRAY_NOT_PACKED) {
			gear_hash_real_init_mixed(Z_ARRVAL_P(array));
		}
		GEAR_VM_DISPATCH_TO_HANDLER(GEAR_ADD_ARRAY_ELEMENT);
	} else {
		ZVAL_EMPTY_ARRAY(array);
		GEAR_VM_NEXT_OPCODE();
	}
}

GEAR_VM_COLD_CONST_HANDLER(21, GEAR_CAST, CONST|TMP|VAR|CV, ANY, TYPE)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *expr;
	zval *result = EX_VAR(opline->result.var);
	HashTable *ht;

	SAVE_OPLINE();
	expr = GET_OP1_ZVAL_PTR(BP_VAR_R);

	switch (opline->extended_value) {
		case IS_NULL:
			ZVAL_NULL(result);
			break;
		case _IS_BOOL:
			ZVAL_BOOL(result, gear_is_true(expr));
			break;
		case IS_LONG:
			ZVAL_LONG(result, zval_get_long(expr));
			break;
		case IS_DOUBLE:
			ZVAL_DOUBLE(result, zval_get_double(expr));
			break;
		case IS_STRING:
			ZVAL_STR(result, zval_get_string(expr));
			break;
		default:
			if (OP1_TYPE & (IS_VAR|IS_CV)) {
				ZVAL_DEREF(expr);
			}
			/* If value is already of correct type, return it directly */
			if (Z_TYPE_P(expr) == opline->extended_value) {
				ZVAL_COPY_VALUE(result, expr);
				if (OP1_TYPE == IS_CONST) {
					if (UNEXPECTED(Z_OPT_REFCOUNTED_P(result))) Z_ADDREF_P(result);
				} else if (OP1_TYPE != IS_TMP_VAR) {
					if (Z_OPT_REFCOUNTED_P(result)) Z_ADDREF_P(result);
				}

				FREE_OP1_IF_VAR();
				GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
			}

			if (opline->extended_value == IS_ARRAY) {
				if (OP1_TYPE == IS_CONST || Z_TYPE_P(expr) != IS_OBJECT || Z_OBJCE_P(expr) == gear_ce_closure) {
					if (Z_TYPE_P(expr) != IS_NULL) {
						ZVAL_ARR(result, gear_new_array(1));
						expr = gear_hash_index_add_new(Z_ARRVAL_P(result), 0, expr);
						if (OP1_TYPE == IS_CONST) {
							if (UNEXPECTED(Z_OPT_REFCOUNTED_P(expr))) Z_ADDREF_P(expr);
						} else {
							if (Z_OPT_REFCOUNTED_P(expr)) Z_ADDREF_P(expr);
						}
					} else {
						ZVAL_EMPTY_ARRAY(result);
					}
				} else if (Z_OBJ_HT_P(expr)->get_properties) {
					HashTable *obj_ht = Z_OBJ_HT_P(expr)->get_properties(expr);
					if (obj_ht) {
						/* fast copy */
						obj_ht = gear_proptable_to_symtable(obj_ht,
							(Z_OBJCE_P(expr)->default_properties_count ||
							 Z_OBJ_P(expr)->handlers != &std_object_handlers ||
							 GC_IS_RECURSIVE(obj_ht)));
						ZVAL_ARR(result, obj_ht);
					} else {
						ZVAL_EMPTY_ARRAY(result);
					}
				} else {
					ZVAL_COPY_VALUE(result, expr);
					Z_ADDREF_P(result);
					convert_to_array(result);
				}
			} else {
				ZVAL_OBJ(result, gear_objects_new(gear_standard_class_def));
				if (Z_TYPE_P(expr) == IS_ARRAY) {
					ht = gear_symtable_to_proptable(Z_ARR_P(expr));
					if (GC_FLAGS(ht) & IS_ARRAY_IMMUTABLE) {
						/* TODO: try not to duplicate immutable arrays as well ??? */
						ht = gear_array_dup(ht);
					}
					Z_OBJ_P(result)->properties = ht;
				} else if (Z_TYPE_P(expr) != IS_NULL) {
					Z_OBJ_P(result)->properties = ht = gear_new_array(1);
					expr = gear_hash_add_new(ht, ZSTR_KNOWN(GEAR_STR_SCALAR), expr);
					if (OP1_TYPE == IS_CONST) {
						if (UNEXPECTED(Z_OPT_REFCOUNTED_P(expr))) Z_ADDREF_P(expr);
					} else {
						if (Z_OPT_REFCOUNTED_P(expr)) Z_ADDREF_P(expr);
					}
				}
			}
	}

	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(73, GEAR_INCLUDE_OR_EVAL, CONST|TMPVAR|CV, ANY, EVAL)
{
	USE_OPLINE
	gear_op_array *new_op_array;
	gear_free_op free_op1;
	zval *inc_filename;

	SAVE_OPLINE();
	inc_filename = GET_OP1_ZVAL_PTR(BP_VAR_R);
	new_op_array = gear_include_or_eval(inc_filename, opline->extended_value);
	FREE_OP1();
	if (UNEXPECTED(EG(exception) != NULL)) {
		if (new_op_array != GEAR_FAKE_OP_ARRAY && new_op_array != NULL) {
			destroy_op_array(new_op_array);
			efree_size(new_op_array, sizeof(gear_op_array));
		}
		UNDEF_RESULT();
		HANDLE_EXCEPTION();
	} else if (new_op_array == GEAR_FAKE_OP_ARRAY) {
		if (RETURN_VALUE_USED(opline)) {
			ZVAL_TRUE(EX_VAR(opline->result.var));
		}
	} else if (EXPECTED(new_op_array != NULL)) {
		zval *return_value = NULL;
		gear_execute_data *call;

		if (RETURN_VALUE_USED(opline)) {
			return_value = EX_VAR(opline->result.var);
			ZVAL_NULL(return_value);
		}

		new_op_array->scope = EX(func)->op_array.scope;

		call = gear_vm_stack_push_call_frame(GEAR_CALL_NESTED_CODE | GEAR_CALL_HAS_SYMBOL_TABLE,
			(gear_function*)new_op_array, 0,
			Z_TYPE(EX(This)) != IS_OBJECT ? Z_CE(EX(This)) : NULL,
			Z_TYPE(EX(This)) == IS_OBJECT ? Z_OBJ(EX(This)) : NULL);

		if (EX_CALL_INFO() & GEAR_CALL_HAS_SYMBOL_TABLE) {
			call->symbol_table = EX(symbol_table);
		} else {
			call->symbol_table = gear_rebuild_symbol_table();
		}

		call->prev_execute_data = execute_data;
		i_init_code_execute_data(call, new_op_array, return_value);
		if (EXPECTED(gear_execute_ex == execute_ex)) {
			GEAR_VM_ENTER();
		} else {
			GEAR_ADD_CALL_FLAG(call, GEAR_CALL_TOP);
			gear_execute_ex(call);
			gear_vm_stack_free_call_frame(call);
		}

		destroy_op_array(new_op_array);
		efree_size(new_op_array, sizeof(gear_op_array));
		if (UNEXPECTED(EG(exception) != NULL)) {
			gear_rethrow_exception(execute_data);
			UNDEF_RESULT();
			HANDLE_EXCEPTION();
		}
	} else if (RETURN_VALUE_USED(opline)) {
		ZVAL_FALSE(EX_VAR(opline->result.var));
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(196, GEAR_UNSET_CV, CV, UNUSED)
{
	USE_OPLINE
	zval *var = EX_VAR(opline->op1.var);

	if (Z_REFCOUNTED_P(var)) {
		gear_refcounted *garbage = Z_COUNTED_P(var);

		ZVAL_UNDEF(var);
		SAVE_OPLINE();
		if (!GC_DELREF(garbage)) {
			rc_dtor_func(garbage);
		} else {
			gc_check_possible_root(garbage);
		}
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	} else {
		ZVAL_UNDEF(var);
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(74, GEAR_UNSET_VAR, CONST|TMPVAR|CV, UNUSED, VAR_FETCH)
{
	USE_OPLINE
	zval *varname;
	gear_string *name, *tmp_name;
	HashTable *target_symbol_table;
	gear_free_op free_op1;

	SAVE_OPLINE();

	varname = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (OP1_TYPE == IS_CONST) {
		name = Z_STR_P(varname);
	} else if (EXPECTED(Z_TYPE_P(varname) == IS_STRING)) {
		name = Z_STR_P(varname);
		tmp_name = NULL;
	} else {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(varname) == IS_UNDEF)) {
			varname = GET_OP1_UNDEF_CV(varname, BP_VAR_R);
		}
		name = zval_get_tmp_string(varname, &tmp_name);
	}

	target_symbol_table = gear_get_target_symbol_table(opline->extended_value EXECUTE_DATA_CC);
	gear_hash_del_ind(target_symbol_table, name);

	if (OP1_TYPE != IS_CONST) {
		gear_tmp_string_release(tmp_name);
	}
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_HANDLER(179, GEAR_UNSET_STATIC_PROP, CONST|TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, CACHE_SLOT)
{
	USE_OPLINE
	zval *varname;
	gear_string *name, *tmp_name;
	gear_class_entry *ce;
	gear_free_op free_op1;

	SAVE_OPLINE();

	if (OP2_TYPE == IS_CONST) {
		ce = CACHED_PTR(opline->extended_value);
		if (UNEXPECTED(ce == NULL)) {
			ce = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op2)), RT_CONSTANT(opline, opline->op2) + 1, GEAR_FETCH_CLASS_DEFAULT | GEAR_FETCH_CLASS_EXCEPTION);
			if (UNEXPECTED(ce == NULL)) {
				GEAR_ASSERT(EG(exception));
				FREE_UNFETCHED_OP1();
				HANDLE_EXCEPTION();
			}
			/*CACHE_PTR(opline->extended_value, ce);*/
		}
	} else if (OP2_TYPE == IS_UNUSED) {
		ce = gear_fetch_class(NULL, opline->op2.num);
		if (UNEXPECTED(ce == NULL)) {
			GEAR_ASSERT(EG(exception));
			FREE_UNFETCHED_OP1();
			HANDLE_EXCEPTION();
		}
	} else {
		ce = Z_CE_P(EX_VAR(opline->op2.var));
	}

	varname = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (OP1_TYPE == IS_CONST) {
		name = Z_STR_P(varname);
	} else if (EXPECTED(Z_TYPE_P(varname) == IS_STRING)) {
		name = Z_STR_P(varname);
		tmp_name = NULL;
	} else {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(varname) == IS_UNDEF)) {
			varname = GET_OP1_UNDEF_CV(varname, BP_VAR_R);
		}
		name = zval_get_tmp_string(varname, &tmp_name);
	}

	gear_std_unset_static_property(ce, name);

	if (OP1_TYPE != IS_CONST) {
		gear_tmp_string_release(tmp_name);
	}
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(75, GEAR_UNSET_DIM, VAR|CV, CONST|TMPVAR|CV)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;
	zval *offset;
	gear_ulong hval;
	gear_string *key;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR_PTR_UNDEF(BP_VAR_UNSET);
	offset = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);

	do {
		if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
			HashTable *ht;

GEAR_VM_C_LABEL(unset_dim_array):
			SEPARATE_ARRAY(container);
			ht = Z_ARRVAL_P(container);
GEAR_VM_C_LABEL(offset_again):
			if (EXPECTED(Z_TYPE_P(offset) == IS_STRING)) {
				key = Z_STR_P(offset);
				if (OP2_TYPE != IS_CONST) {
					if (GEAR_HANDLE_NUMERIC(key, hval)) {
						GEAR_VM_C_GOTO(num_index_dim);
					}
				}
GEAR_VM_C_LABEL(str_index_dim):
				if (ht == &EG(symbol_table)) {
					gear_delete_global_variable(key);
				} else {
					gear_hash_del(ht, key);
				}
			} else if (EXPECTED(Z_TYPE_P(offset) == IS_LONG)) {
				hval = Z_LVAL_P(offset);
GEAR_VM_C_LABEL(num_index_dim):
				gear_hash_index_del(ht, hval);
			} else if ((OP2_TYPE & (IS_VAR|IS_CV)) && EXPECTED(Z_TYPE_P(offset) == IS_REFERENCE)) {
				offset = Z_REFVAL_P(offset);
				GEAR_VM_C_GOTO(offset_again);
			} else if (Z_TYPE_P(offset) == IS_DOUBLE) {
				hval = gear_dval_to_lval(Z_DVAL_P(offset));
				GEAR_VM_C_GOTO(num_index_dim);
			} else if (Z_TYPE_P(offset) == IS_NULL) {
				key = ZSTR_EMPTY_ALLOC();
				GEAR_VM_C_GOTO(str_index_dim);
			} else if (Z_TYPE_P(offset) == IS_FALSE) {
				hval = 0;
				GEAR_VM_C_GOTO(num_index_dim);
			} else if (Z_TYPE_P(offset) == IS_TRUE) {
				hval = 1;
				GEAR_VM_C_GOTO(num_index_dim);
			} else if (Z_TYPE_P(offset) == IS_RESOURCE) {
				hval = Z_RES_HANDLE_P(offset);
				GEAR_VM_C_GOTO(num_index_dim);
			} else if (OP2_TYPE == IS_CV && Z_TYPE_P(offset) == IS_UNDEF) {
				GET_OP2_UNDEF_CV(offset, BP_VAR_R);
				key = ZSTR_EMPTY_ALLOC();
				GEAR_VM_C_GOTO(str_index_dim);
			} else {
				gear_error(E_WARNING, "Illegal offset type in unset");
			}
			break;
		} else if (Z_ISREF_P(container)) {
			container = Z_REFVAL_P(container);
			if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
				GEAR_VM_C_GOTO(unset_dim_array);
			}
		}
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
			container = GET_OP1_UNDEF_CV(container, BP_VAR_R);
		}
		if (OP2_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(offset) == IS_UNDEF)) {
			offset = GET_OP2_UNDEF_CV(offset, BP_VAR_R);
		}
		if (EXPECTED(Z_TYPE_P(container) == IS_OBJECT)) {
			if (UNEXPECTED(Z_OBJ_HT_P(container)->unset_dimension == NULL)) {
				gear_use_object_as_array();
			} else {
				if (OP2_TYPE == IS_CONST && Z_EXTRA_P(offset) == GEAR_EXTRA_VALUE) {
					offset++;
				}
				Z_OBJ_HT_P(container)->unset_dimension(container, offset);
			}
		} else if (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) == IS_STRING)) {
			gear_throw_error(NULL, "Cannot unset string offsets");
		}
	} while (0);

	FREE_OP2();
	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(76, GEAR_UNSET_OBJ, VAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;
	zval *offset;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR_PTR(BP_VAR_UNSET);
	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}
	offset = GET_OP2_ZVAL_PTR(BP_VAR_R);

	do {
		if (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) != IS_OBJECT)) {
			if (Z_ISREF_P(container)) {
				container = Z_REFVAL_P(container);
				if (Z_TYPE_P(container) != IS_OBJECT) {
					break;
				}
			} else {
				break;
			}
		}
		if (Z_OBJ_HT_P(container)->unset_property) {
			Z_OBJ_HT_P(container)->unset_property(container, offset, ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value) : NULL));
		} else {
			gear_wrong_property_unset(offset);
		}
	} while (0);

	FREE_OP2();
	FREE_OP1_VAR_PTR();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(77, GEAR_FE_RESET_R, CONST|TMP|VAR|CV, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *array_ptr, *result;

	SAVE_OPLINE();

	array_ptr = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_P(array_ptr) == IS_ARRAY)) {
		result = EX_VAR(opline->result.var);
		ZVAL_COPY_VALUE(result, array_ptr);
		if (OP1_TYPE != IS_TMP_VAR && Z_OPT_REFCOUNTED_P(result)) {
			Z_ADDREF_P(array_ptr);
		}
		Z_FE_POS_P(result) = 0;

		FREE_OP1_IF_VAR();
		GEAR_VM_NEXT_OPCODE();
	} else if (OP1_TYPE != IS_CONST && EXPECTED(Z_TYPE_P(array_ptr) == IS_OBJECT)) {
		if (!Z_OBJCE_P(array_ptr)->get_iterator) {
			result = EX_VAR(opline->result.var);
			ZVAL_COPY_VALUE(result, array_ptr);
			if (OP1_TYPE != IS_TMP_VAR) {
				Z_ADDREF_P(array_ptr);
			}
			if (Z_OBJ_P(array_ptr)->properties
			 && UNEXPECTED(GC_REFCOUNT(Z_OBJ_P(array_ptr)->properties) > 1)) {
				if (EXPECTED(!(GC_FLAGS(Z_OBJ_P(array_ptr)->properties) & IS_ARRAY_IMMUTABLE))) {
					GC_DELREF(Z_OBJ_P(array_ptr)->properties);
				}
				Z_OBJ_P(array_ptr)->properties = gear_array_dup(Z_OBJ_P(array_ptr)->properties);
			}
			Z_FE_ITER_P(EX_VAR(opline->result.var)) = gear_hash_iterator_add(Z_OBJPROP_P(array_ptr), 0);

			FREE_OP1_IF_VAR();
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		} else {
			gear_bool is_empty = gear_fe_reset_iterator(array_ptr, 0 OPLINE_CC EXECUTE_DATA_CC);

			FREE_OP1();
			if (UNEXPECTED(EG(exception))) {
				HANDLE_EXCEPTION();
			} else if (is_empty) {
				GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
			} else {
				GEAR_VM_NEXT_OPCODE();
			}
		}
	} else {
		gear_error(E_WARNING, "Invalid argument supplied for foreach()");
		ZVAL_UNDEF(EX_VAR(opline->result.var));
		Z_FE_ITER_P(EX_VAR(opline->result.var)) = (uint32_t)-1;
		FREE_OP1();
		GEAR_VM_JMP(OP_JMP_ADDR(opline, opline->op2));
	}
}

GEAR_VM_COLD_CONST_HANDLER(125, GEAR_FE_RESET_RW, CONST|TMP|VAR|CV, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *array_ptr, *array_ref;

	SAVE_OPLINE();

	if (OP1_TYPE == IS_VAR || OP1_TYPE == IS_CV) {
		array_ref = array_ptr = GET_OP1_ZVAL_PTR_PTR(BP_VAR_R);
		if (Z_ISREF_P(array_ref)) {
			array_ptr = Z_REFVAL_P(array_ref);
		}
	} else {
		array_ref = array_ptr = GET_OP1_ZVAL_PTR(BP_VAR_R);
	}

	if (EXPECTED(Z_TYPE_P(array_ptr) == IS_ARRAY)) {
		if (OP1_TYPE == IS_VAR || OP1_TYPE == IS_CV) {
			if (array_ptr == array_ref) {
				ZVAL_NEW_REF(array_ref, array_ref);
				array_ptr = Z_REFVAL_P(array_ref);
			}
			Z_ADDREF_P(array_ref);
			ZVAL_COPY_VALUE(EX_VAR(opline->result.var), array_ref);
		} else {
			array_ref = EX_VAR(opline->result.var);
			ZVAL_NEW_REF(array_ref, array_ptr);
			array_ptr = Z_REFVAL_P(array_ref);
		}
		if (OP1_TYPE == IS_CONST) {
			ZVAL_ARR(array_ptr, gear_array_dup(Z_ARRVAL_P(array_ptr)));
		} else {
			SEPARATE_ARRAY(array_ptr);
		}
		Z_FE_ITER_P(EX_VAR(opline->result.var)) = gear_hash_iterator_add(Z_ARRVAL_P(array_ptr), 0);

		if (OP1_TYPE == IS_VAR) {
			FREE_OP1_VAR_PTR();
		}
		GEAR_VM_NEXT_OPCODE();
	} else if (OP1_TYPE != IS_CONST && EXPECTED(Z_TYPE_P(array_ptr) == IS_OBJECT)) {
		if (!Z_OBJCE_P(array_ptr)->get_iterator) {
			if (OP1_TYPE == IS_VAR || OP1_TYPE == IS_CV) {
				if (array_ptr == array_ref) {
					ZVAL_NEW_REF(array_ref, array_ref);
					array_ptr = Z_REFVAL_P(array_ref);
				}
				Z_ADDREF_P(array_ref);
				ZVAL_COPY_VALUE(EX_VAR(opline->result.var), array_ref);
			} else {
				array_ptr = EX_VAR(opline->result.var);
				ZVAL_COPY_VALUE(array_ptr, array_ref);
			}
			if (Z_OBJ_P(array_ptr)->properties
			 && UNEXPECTED(GC_REFCOUNT(Z_OBJ_P(array_ptr)->properties) > 1)) {
				if (EXPECTED(!(GC_FLAGS(Z_OBJ_P(array_ptr)->properties) & IS_ARRAY_IMMUTABLE))) {
					GC_DELREF(Z_OBJ_P(array_ptr)->properties);
				}
				Z_OBJ_P(array_ptr)->properties = gear_array_dup(Z_OBJ_P(array_ptr)->properties);
			}
			Z_FE_ITER_P(EX_VAR(opline->result.var)) = gear_hash_iterator_add(Z_OBJPROP_P(array_ptr), 0);

			FREE_OP1_VAR_PTR();
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		} else {
			gear_bool is_empty = gear_fe_reset_iterator(array_ptr, 1 OPLINE_CC EXECUTE_DATA_CC);

			if (OP1_TYPE == IS_VAR) {
				FREE_OP1_VAR_PTR();
			} else {
				FREE_OP1();
			}
			if (UNEXPECTED(EG(exception))) {
				HANDLE_EXCEPTION();
			} else if (is_empty) {
				GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
			} else {
				GEAR_VM_NEXT_OPCODE();
			}
		}
	} else {
		gear_error(E_WARNING, "Invalid argument supplied for foreach()");
		ZVAL_UNDEF(EX_VAR(opline->result.var));
		Z_FE_ITER_P(EX_VAR(opline->result.var)) = (uint32_t)-1;
		if (OP1_TYPE == IS_VAR) {
			FREE_OP1_VAR_PTR();
		} else {
			FREE_OP1();
		}
		GEAR_VM_JMP(OP_JMP_ADDR(opline, opline->op2));
	}
}

GEAR_VM_HANDLER(78, GEAR_FE_FETCH_R, VAR, ANY, JMP_ADDR)
{
	USE_OPLINE
	zval *array;
	zval *value;
	uint32_t value_type;
	HashTable *fe_ht;
	HashPosition pos;
	Bucket *p;

	array = EX_VAR(opline->op1.var);
	SAVE_OPLINE();
	if (EXPECTED(Z_TYPE_P(array) == IS_ARRAY)) {
		fe_ht = Z_ARRVAL_P(array);
		pos = Z_FE_POS_P(array);
		p = fe_ht->arData + pos;
		while (1) {
			if (UNEXPECTED(pos >= fe_ht->nNumUsed)) {
				/* reached end of iteration */
GEAR_VM_C_LABEL(fe_fetch_r_exit):
				GEAR_VM_SET_RELATIVE_OPCODE(opline, opline->extended_value);
				GEAR_VM_CONTINUE();
			}
			value = &p->val;
			value_type = Z_TYPE_INFO_P(value);
			if (EXPECTED(value_type != IS_UNDEF)) {
				if (UNEXPECTED(value_type == IS_INDIRECT)) {
					value = Z_INDIRECT_P(value);
					value_type = Z_TYPE_INFO_P(value);
					if (EXPECTED(value_type != IS_UNDEF)) {
						break;
					}
				} else {
					break;
				}
			}
			pos++;
			p++;
		}
		Z_FE_POS_P(array) = pos + 1;
		if (RETURN_VALUE_USED(opline)) {
			if (!p->key) {
				ZVAL_LONG(EX_VAR(opline->result.var), p->h);
			} else {
				ZVAL_STR_COPY(EX_VAR(opline->result.var), p->key);
			}
		}
	} else {
		gear_object_iterator *iter;

		GEAR_ASSERT(Z_TYPE_P(array) == IS_OBJECT);
		if ((iter = gear_iterator_unwrap(array)) == NULL) {
			/* plain object */

			fe_ht = Z_OBJPROP_P(array);
			pos = gear_hash_iterator_pos(Z_FE_ITER_P(array), fe_ht);
			p = fe_ht->arData + pos;
			while (1) {
				if (UNEXPECTED(pos >= fe_ht->nNumUsed)) {
					/* reached end of iteration */
					GEAR_VM_C_GOTO(fe_fetch_r_exit);
				}

				value = &p->val;
				value_type = Z_TYPE_INFO_P(value);
				if (EXPECTED(value_type != IS_UNDEF)) {
					if (UNEXPECTED(value_type == IS_INDIRECT)) {
						value = Z_INDIRECT_P(value);
						value_type = Z_TYPE_INFO_P(value);
						if (EXPECTED(value_type != IS_UNDEF)
						 && EXPECTED(gear_check_property_access(Z_OBJ_P(array), p->key) == SUCCESS)) {
							break;
						}
					} else {
						break;
					}
				}
				pos++;
				p++;
			}
			if (RETURN_VALUE_USED(opline)) {
				if (UNEXPECTED(!p->key)) {
					ZVAL_LONG(EX_VAR(opline->result.var), p->h);
				} else if (ZSTR_VAL(p->key)[0]) {
					ZVAL_STR_COPY(EX_VAR(opline->result.var), p->key);
				} else {
					const char *class_name, *prop_name;
					size_t prop_name_len;
					gear_unmangle_property_name_ex(
						p->key, &class_name, &prop_name, &prop_name_len);
					ZVAL_STRINGL(EX_VAR(opline->result.var), prop_name, prop_name_len);
				}
			}
			EG(ht_iterators)[Z_FE_ITER_P(array)].pos = pos + 1;
		} else {
			if (EXPECTED(++iter->index > 0)) {
				/* This could cause an endless loop if index becomes zero again.
				 * In case that ever happens we need an additional flag. */
				iter->funcs->move_forward(iter);
				if (UNEXPECTED(EG(exception) != NULL)) {
					UNDEF_RESULT();
					HANDLE_EXCEPTION();
				}
				if (UNEXPECTED(iter->funcs->valid(iter) == FAILURE)) {
					/* reached end of iteration */
					if (UNEXPECTED(EG(exception) != NULL)) {
						UNDEF_RESULT();
						HANDLE_EXCEPTION();
					}
					GEAR_VM_C_GOTO(fe_fetch_r_exit);
				}
			}
			value = iter->funcs->get_current_data(iter);
			if (UNEXPECTED(EG(exception) != NULL)) {
				UNDEF_RESULT();
				HANDLE_EXCEPTION();
			}
			if (!value) {
				/* failure in get_current_data */
				GEAR_VM_C_GOTO(fe_fetch_r_exit);
			}
			if (RETURN_VALUE_USED(opline)) {
				if (iter->funcs->get_current_key) {
					iter->funcs->get_current_key(iter, EX_VAR(opline->result.var));
					if (UNEXPECTED(EG(exception) != NULL)) {
						UNDEF_RESULT();
						HANDLE_EXCEPTION();
					}
				} else {
					ZVAL_LONG(EX_VAR(opline->result.var), iter->index);
				}
			}
			value_type = Z_TYPE_INFO_P(value);
		}
	}

	if (EXPECTED(OP2_TYPE == IS_CV)) {
		zval *variable_ptr = EX_VAR(opline->op2.var);
		gear_assign_to_variable(variable_ptr, value, IS_CV);
	} else {
		zval *res = EX_VAR(opline->op2.var);
		gear_refcounted *gc = Z_COUNTED_P(value);

		ZVAL_COPY_VALUE_EX(res, value, gc, value_type);
		if (Z_TYPE_INFO_REFCOUNTED(value_type)) {
			GC_ADDREF(gc);
		}
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(126, GEAR_FE_FETCH_RW, VAR, ANY, JMP_ADDR)
{
	USE_OPLINE
	zval *array;
	zval *value;
	uint32_t value_type;
	HashTable *fe_ht;
	HashPosition pos;
	Bucket *p;

	array = EX_VAR(opline->op1.var);
	SAVE_OPLINE();

	ZVAL_DEREF(array);
	if (EXPECTED(Z_TYPE_P(array) == IS_ARRAY)) {
		pos = gear_hash_iterator_pos_ex(Z_FE_ITER_P(EX_VAR(opline->op1.var)), array);
		fe_ht = Z_ARRVAL_P(array);
		p = fe_ht->arData + pos;
		while (1) {
			if (UNEXPECTED(pos >= fe_ht->nNumUsed)) {
				/* reached end of iteration */
				GEAR_VM_C_GOTO(fe_fetch_w_exit);
			}
			value = &p->val;
			value_type = Z_TYPE_INFO_P(value);
			if (EXPECTED(value_type != IS_UNDEF)) {
				if (UNEXPECTED(value_type == IS_INDIRECT)) {
					value = Z_INDIRECT_P(value);
					value_type = Z_TYPE_INFO_P(value);
					if (EXPECTED(value_type != IS_UNDEF)) {
						break;
					}
				} else {
					break;
				}
			}
			pos++;
			p++;
		}
		if (RETURN_VALUE_USED(opline)) {
			if (!p->key) {
				ZVAL_LONG(EX_VAR(opline->result.var), p->h);
			} else {
				ZVAL_STR_COPY(EX_VAR(opline->result.var), p->key);
			}
		}
		EG(ht_iterators)[Z_FE_ITER_P(EX_VAR(opline->op1.var))].pos = pos + 1;
	} else if (EXPECTED(Z_TYPE_P(array) == IS_OBJECT)) {
		gear_object_iterator *iter;

		if ((iter = gear_iterator_unwrap(array)) == NULL) {
			/* plain object */

			fe_ht = Z_OBJPROP_P(array);
			pos = gear_hash_iterator_pos(Z_FE_ITER_P(EX_VAR(opline->op1.var)), fe_ht);
			p = fe_ht->arData + pos;
			while (1) {
				if (UNEXPECTED(pos >= fe_ht->nNumUsed)) {
					/* reached end of iteration */
					GEAR_VM_C_GOTO(fe_fetch_w_exit);
				}

				value = &p->val;
				value_type = Z_TYPE_INFO_P(value);
				if (EXPECTED(value_type != IS_UNDEF)) {
					if (UNEXPECTED(value_type == IS_INDIRECT)) {
						value = Z_INDIRECT_P(value);
						value_type = Z_TYPE_INFO_P(value);
						if (EXPECTED(value_type != IS_UNDEF)
						 && EXPECTED(gear_check_property_access(Z_OBJ_P(array), p->key) == SUCCESS)) {
							break;
						}
					} else {
						break;
					}
				}
				pos++;
				p++;
			}
			if (RETURN_VALUE_USED(opline)) {
				if (UNEXPECTED(!p->key)) {
					ZVAL_LONG(EX_VAR(opline->result.var), p->h);
				} else if (ZSTR_VAL(p->key)[0]) {
					ZVAL_STR_COPY(EX_VAR(opline->result.var), p->key);
				} else {
					const char *class_name, *prop_name;
					size_t prop_name_len;
					gear_unmangle_property_name_ex(
						p->key, &class_name, &prop_name, &prop_name_len);
					ZVAL_STRINGL(EX_VAR(opline->result.var), prop_name, prop_name_len);
				}
			}
			EG(ht_iterators)[Z_FE_ITER_P(EX_VAR(opline->op1.var))].pos = pos + 1;
		} else {
			if (++iter->index > 0) {
				/* This could cause an endless loop if index becomes zero again.
				 * In case that ever happens we need an additional flag. */
				iter->funcs->move_forward(iter);
				if (UNEXPECTED(EG(exception) != NULL)) {
					UNDEF_RESULT();
					HANDLE_EXCEPTION();
				}
				if (UNEXPECTED(iter->funcs->valid(iter) == FAILURE)) {
					/* reached end of iteration */
					if (UNEXPECTED(EG(exception) != NULL)) {
						UNDEF_RESULT();
						HANDLE_EXCEPTION();
					}
					GEAR_VM_C_GOTO(fe_fetch_w_exit);
				}
			}
			value = iter->funcs->get_current_data(iter);
			if (UNEXPECTED(EG(exception) != NULL)) {
				UNDEF_RESULT();
				HANDLE_EXCEPTION();
			}
			if (!value) {
				/* failure in get_current_data */
				GEAR_VM_C_GOTO(fe_fetch_w_exit);
			}
			if (RETURN_VALUE_USED(opline)) {
				if (iter->funcs->get_current_key) {
					iter->funcs->get_current_key(iter, EX_VAR(opline->result.var));
					if (UNEXPECTED(EG(exception) != NULL)) {
						UNDEF_RESULT();
						HANDLE_EXCEPTION();
					}
				} else {
					ZVAL_LONG(EX_VAR(opline->result.var), iter->index);
				}
			}
			value_type = Z_TYPE_INFO_P(value);
		}
	} else {
		gear_error(E_WARNING, "Invalid argument supplied for foreach()");
		if (UNEXPECTED(EG(exception))) {
			UNDEF_RESULT();
			HANDLE_EXCEPTION();
		}
GEAR_VM_C_LABEL(fe_fetch_w_exit):
		GEAR_VM_SET_RELATIVE_OPCODE(opline, opline->extended_value);
		GEAR_VM_CONTINUE();
	}

	if (EXPECTED((value_type & Z_TYPE_MASK) != IS_REFERENCE)) {
		gear_refcounted *gc = Z_COUNTED_P(value);
		zval *ref;
		ZVAL_NEW_EMPTY_REF(value);
		ref = Z_REFVAL_P(value);
		ZVAL_COPY_VALUE_EX(ref, value, gc, value_type);
	}
	if (EXPECTED(OP2_TYPE == IS_CV)) {
		zval *variable_ptr = EX_VAR(opline->op2.var);
		if (EXPECTED(variable_ptr != value)) {
			gear_reference *ref;

			ref = Z_REF_P(value);
			GC_ADDREF(ref);
			zval_ptr_dtor(variable_ptr);
			ZVAL_REF(variable_ptr, ref);
		}
	} else {
		Z_ADDREF_P(value);
		ZVAL_REF(EX_VAR(opline->op2.var), Z_REF_P(value));
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_HANDLER(197, GEAR_ISSET_ISEMPTY_CV, CV, UNUSED, ISSET, SPEC(ISSET))
{
	USE_OPLINE
	zval *value;
	int result;

	value = EX_VAR(opline->op1.var);
	if (!(opline->extended_value & GEAR_ISEMPTY)) {
		result =
			Z_TYPE_P(value) > IS_NULL &&
		    (!Z_ISREF_P(value) || Z_TYPE_P(Z_REFVAL_P(value)) != IS_NULL);
	} else {
		SAVE_OPLINE();
		result = !i_gear_is_true(value);
		if (UNEXPECTED(EG(exception))) {
			ZVAL_UNDEF(EX_VAR(opline->result.var));
			HANDLE_EXCEPTION();
		}
	}
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(114, GEAR_ISSET_ISEMPTY_VAR, CONST|TMPVAR|CV, UNUSED, VAR_FETCH|ISSET)
{
	USE_OPLINE
	zval *value;
	int result;
	gear_free_op free_op1;
	zval *varname;
	gear_string *name, *tmp_name;
	HashTable *target_symbol_table;

	SAVE_OPLINE();
	varname = GET_OP1_ZVAL_PTR(BP_VAR_IS);
	if (OP1_TYPE == IS_CONST) {
		name = Z_STR_P(varname);
	} else {
		name = zval_get_tmp_string(varname, &tmp_name);
	}

	target_symbol_table = gear_get_target_symbol_table(opline->extended_value EXECUTE_DATA_CC);
	value = gear_hash_find_ex(target_symbol_table, name, OP1_TYPE == IS_CONST);

	if (OP1_TYPE != IS_CONST) {
		gear_tmp_string_release(tmp_name);
	}
	FREE_OP1();

	if (!value) {
		result = (opline->extended_value & GEAR_ISEMPTY);
	} else {
		if (Z_TYPE_P(value) == IS_INDIRECT) {
			value = Z_INDIRECT_P(value);
		}
		if (!(opline->extended_value & GEAR_ISEMPTY)) {
			if (Z_ISREF_P(value)) {
				value = Z_REFVAL_P(value);
			}
			result = Z_TYPE_P(value) > IS_NULL;
		} else {
			result = !i_gear_is_true(value);
		}
	}

	GEAR_VM_SMART_BRANCH(result, 1);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(180, GEAR_ISSET_ISEMPTY_STATIC_PROP, CONST|TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, ISSET|CACHE_SLOT)
{
	USE_OPLINE
	zval *value;
	int result;
	gear_free_op free_op1;
	zval *varname;
	gear_string *name, *tmp_name;
	gear_class_entry *ce;

	SAVE_OPLINE();
	if (OP2_TYPE == IS_CONST) {
		if (OP1_TYPE == IS_CONST && EXPECTED((ce = CACHED_PTR(opline->extended_value & ~GEAR_ISEMPTY)) != NULL)) {
			value = CACHED_PTR((opline->extended_value & ~GEAR_ISEMPTY) + sizeof(void*));
			GEAR_VM_C_GOTO(is_static_prop_return);
		} else if (UNEXPECTED((ce = CACHED_PTR(opline->extended_value & ~GEAR_ISEMPTY)) == NULL)) {
			ce = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op2)), RT_CONSTANT(opline, opline->op2) + 1, GEAR_FETCH_CLASS_DEFAULT | GEAR_FETCH_CLASS_EXCEPTION);
			if (UNEXPECTED(ce == NULL)) {
				GEAR_ASSERT(EG(exception));
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
			if (OP1_TYPE != IS_CONST) {
				CACHE_PTR(opline->extended_value & ~GEAR_ISEMPTY, ce);
			}
		}
	} else {
		if (OP2_TYPE == IS_UNUSED) {
			ce = gear_fetch_class(NULL, opline->op2.num);
			if (UNEXPECTED(ce == NULL)) {
				GEAR_ASSERT(EG(exception));
				FREE_UNFETCHED_OP1();
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
		} else {
			ce = Z_CE_P(EX_VAR(opline->op2.var));
		}
		if (OP1_TYPE == IS_CONST &&
		    EXPECTED(CACHED_PTR(opline->extended_value & ~GEAR_ISEMPTY) == ce)) {

			value = CACHED_PTR((opline->extended_value & ~GEAR_ISEMPTY) + sizeof(void*));
			GEAR_VM_C_GOTO(is_static_prop_return);
		}
	}

	varname = GET_OP1_ZVAL_PTR(BP_VAR_IS);
	if (OP1_TYPE == IS_CONST) {
		name = Z_STR_P(varname);
	} else {
		name = zval_get_tmp_string(varname, &tmp_name);
	}

	value = gear_std_get_static_property(ce, name, 1);

	if (OP1_TYPE == IS_CONST && value) {
		CACHE_POLYMORPHIC_PTR(opline->extended_value & ~GEAR_ISEMPTY, ce, value);
	}

	if (OP1_TYPE != IS_CONST) {
		gear_tmp_string_release(tmp_name);
	}
	FREE_OP1();

GEAR_VM_C_LABEL(is_static_prop_return):
	if (!(opline->extended_value & GEAR_ISEMPTY)) {
		result = value && Z_TYPE_P(value) > IS_NULL &&
		    (!Z_ISREF_P(value) || Z_TYPE_P(Z_REFVAL_P(value)) != IS_NULL);
	} else {
		result = !value || !i_gear_is_true(value);
	}

	GEAR_VM_SMART_BRANCH(result, 1);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(115, GEAR_ISSET_ISEMPTY_DIM_OBJ, CONST|TMPVAR|CV, CONST|TMPVAR|CV, ISSET)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;
	int result;
	gear_ulong hval;
	zval *offset;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR_UNDEF(BP_VAR_IS);
	offset = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);

	if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
		HashTable *ht;
		zval *value;
		gear_string *str;

GEAR_VM_C_LABEL(isset_dim_obj_array):
		ht = Z_ARRVAL_P(container);
GEAR_VM_C_LABEL(isset_again):
		if (EXPECTED(Z_TYPE_P(offset) == IS_STRING)) {
			str = Z_STR_P(offset);
			if (OP2_TYPE != IS_CONST) {
				if (GEAR_HANDLE_NUMERIC(str, hval)) {
					GEAR_VM_C_GOTO(num_index_prop);
				}
			}
			value = gear_hash_find_ex_ind(ht, str, OP2_TYPE == IS_CONST);
		} else if (EXPECTED(Z_TYPE_P(offset) == IS_LONG)) {
			hval = Z_LVAL_P(offset);
GEAR_VM_C_LABEL(num_index_prop):
			value = gear_hash_index_find(ht, hval);
		} else if ((OP2_TYPE & (IS_VAR|IS_CV)) && EXPECTED(Z_ISREF_P(offset))) {
			offset = Z_REFVAL_P(offset);
			GEAR_VM_C_GOTO(isset_again);
		} else {
			value = gear_find_array_dim_slow(ht, offset EXECUTE_DATA_CC);
		}

		if (!(opline->extended_value & GEAR_ISEMPTY)) {
			/* > IS_NULL means not IS_UNDEF and not IS_NULL */
			result = value != NULL && Z_TYPE_P(value) > IS_NULL &&
			    (!Z_ISREF_P(value) || Z_TYPE_P(Z_REFVAL_P(value)) != IS_NULL);
		} else {
			result = (value == NULL || !i_gear_is_true(value));
		}
		GEAR_VM_C_GOTO(isset_dim_obj_exit);
	} else if ((OP1_TYPE & (IS_VAR|IS_CV)) && EXPECTED(Z_ISREF_P(container))) {
		container = Z_REFVAL_P(container);
		if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
			GEAR_VM_C_GOTO(isset_dim_obj_array);
		}
	}

	if (OP2_TYPE == IS_CONST && Z_EXTRA_P(offset) == GEAR_EXTRA_VALUE) {
		offset++;
	}
	if (!(opline->extended_value & GEAR_ISEMPTY)) {
		result = gear_isset_dim_slow(container, offset EXECUTE_DATA_CC);
	} else {
		result = gear_isempty_dim_slow(container, offset EXECUTE_DATA_CC);
	}

GEAR_VM_C_LABEL(isset_dim_obj_exit):
	FREE_OP2();
	FREE_OP1();
	GEAR_VM_SMART_BRANCH(result, 1);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONST_HANDLER(148, GEAR_ISSET_ISEMPTY_PROP_OBJ, CONST|TMPVAR|UNUSED|THIS|CV, CONST|TMPVAR|CV, ISSET|CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container;
	int result;
	zval *offset;

	SAVE_OPLINE();
	container = GET_OP1_OBJ_ZVAL_PTR(BP_VAR_IS);

	if (OP1_TYPE == IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) == IS_UNDEF)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}

	offset = GET_OP2_ZVAL_PTR(BP_VAR_R);

	if (OP1_TYPE == IS_CONST ||
	    (OP1_TYPE != IS_UNUSED && UNEXPECTED(Z_TYPE_P(container) != IS_OBJECT))) {
		if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(container)) {
			container = Z_REFVAL_P(container);
			if (UNEXPECTED(Z_TYPE_P(container) != IS_OBJECT)) {
				GEAR_VM_C_GOTO(isset_no_object);
			}
		} else {
			GEAR_VM_C_GOTO(isset_no_object);
		}
	}
	if (UNEXPECTED(!Z_OBJ_HT_P(container)->has_property)) {
		gear_wrong_property_check(offset);
GEAR_VM_C_LABEL(isset_no_object):
		result = (opline->extended_value & GEAR_ISEMPTY);
	} else {
		result =
			(opline->extended_value & GEAR_ISEMPTY) ^
			Z_OBJ_HT_P(container)->has_property(container, offset, (opline->extended_value & GEAR_ISEMPTY), ((OP2_TYPE == IS_CONST) ? CACHE_ADDR(opline->extended_value & ~GEAR_ISEMPTY) : NULL));
	}

	FREE_OP2();
	FREE_OP1();
	GEAR_VM_SMART_BRANCH(result, 1);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_HANDLER(79, GEAR_EXIT, CONST|TMPVAR|UNUSED|CV, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	if (OP1_TYPE != IS_UNUSED) {
		gear_free_op free_op1;
		zval *ptr = GET_OP1_ZVAL_PTR(BP_VAR_R);

		do {
			if (Z_TYPE_P(ptr) == IS_LONG) {
				EG(exit_status) = Z_LVAL_P(ptr);
			} else {
				if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(ptr)) {
					ptr = Z_REFVAL_P(ptr);
					if (Z_TYPE_P(ptr) == IS_LONG) {
						EG(exit_status) = Z_LVAL_P(ptr);
						break;
					}
				}
				gear_print_zval(ptr, 0);
			}
		} while (0);
		FREE_OP1();
	}
	gear_bailout();
	GEAR_VM_NEXT_OPCODE(); /* Never reached */
}

GEAR_VM_HANDLER(57, GEAR_BEGIN_SILENCE, ANY, ANY)
{
	USE_OPLINE

	ZVAL_LONG(EX_VAR(opline->result.var), EG(error_reporting));

	if (EG(error_reporting)) {
		do {
			EG(error_reporting) = 0;
			if (!EG(error_reporting_ics_entry)) {
				zval *zv = gear_hash_find_ex(EG(ics_directives), ZSTR_KNOWN(GEAR_STR_ERROR_REPORTING), 1);
				if (zv) {
					EG(error_reporting_ics_entry) = (gear_ics_entry *)Z_PTR_P(zv);
				} else {
					break;
				}
			}
			if (!EG(error_reporting_ics_entry)->modified) {
				if (!EG(modified_ics_directives)) {
					ALLOC_HASHTABLE(EG(modified_ics_directives));
					gear_hash_init(EG(modified_ics_directives), 8, NULL, NULL, 0);
				}
				if (EXPECTED(gear_hash_add_ptr(EG(modified_ics_directives), ZSTR_KNOWN(GEAR_STR_ERROR_REPORTING), EG(error_reporting_ics_entry)) != NULL)) {
					EG(error_reporting_ics_entry)->orig_value = EG(error_reporting_ics_entry)->value;
					EG(error_reporting_ics_entry)->orig_modifiable = EG(error_reporting_ics_entry)->modifiable;
					EG(error_reporting_ics_entry)->modified = 1;
				}
			}
		} while (0);
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(58, GEAR_END_SILENCE, TMP, ANY)
{
	USE_OPLINE

	if (!EG(error_reporting) && Z_LVAL_P(EX_VAR(opline->op1.var)) != 0) {
		EG(error_reporting) = Z_LVAL_P(EX_VAR(opline->op1.var));
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_CONST_HANDLER(152, GEAR_JMP_SET, CONST|TMP|VAR|CV, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *value;
	zval *ref = NULL;
	int ret;

	SAVE_OPLINE();
	value = GET_OP1_ZVAL_PTR(BP_VAR_R);

	if ((OP1_TYPE == IS_VAR || OP1_TYPE == IS_CV) && Z_ISREF_P(value)) {
		if (OP1_TYPE == IS_VAR) {
			ref = value;
		}
		value = Z_REFVAL_P(value);
	}

	ret = i_gear_is_true(value);

	if (UNEXPECTED(EG(exception))) {
		FREE_OP1();
		ZVAL_UNDEF(EX_VAR(opline->result.var));
		HANDLE_EXCEPTION();
	}

	if (ret) {
		zval *result = EX_VAR(opline->result.var);

		ZVAL_COPY_VALUE(result, value);
		if (OP1_TYPE == IS_CONST) {
			if (UNEXPECTED(Z_OPT_REFCOUNTED_P(result))) Z_ADDREF_P(result);
		} else if (OP1_TYPE == IS_CV) {
			if (Z_OPT_REFCOUNTED_P(result)) Z_ADDREF_P(result);
		} else if (OP1_TYPE == IS_VAR && ref) {
			gear_reference *r = Z_REF_P(ref);

			if (UNEXPECTED(GC_DELREF(r) == 0)) {
				efree_size(r, sizeof(gear_reference));
			} else if (Z_OPT_REFCOUNTED_P(result)) {
				Z_ADDREF_P(result);
			}
		}
		GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
	}

	FREE_OP1();
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_CONST_HANDLER(169, GEAR_COALESCE, CONST|TMPVAR|CV, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *value;
	zval *ref = NULL;

	SAVE_OPLINE();
	value = GET_OP1_ZVAL_PTR(BP_VAR_IS);

	if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(value)) {
		if (OP1_TYPE & IS_VAR) {
			ref = value;
		}
		value = Z_REFVAL_P(value);
	}

	if (Z_TYPE_P(value) > IS_NULL) {
		zval *result = EX_VAR(opline->result.var);
		ZVAL_COPY_VALUE(result, value);
		if (OP1_TYPE == IS_CONST) {
			if (UNEXPECTED(Z_OPT_REFCOUNTED_P(result))) Z_ADDREF_P(result);
		} else if (OP1_TYPE == IS_CV) {
			if (Z_OPT_REFCOUNTED_P(result)) Z_ADDREF_P(result);
		} else if ((OP1_TYPE & IS_VAR) && ref) {
			gear_reference *r = Z_REF_P(ref);

			if (UNEXPECTED(GC_DELREF(r) == 0)) {
				efree_size(r, sizeof(gear_reference));
			} else if (Z_OPT_REFCOUNTED_P(result)) {
				Z_ADDREF_P(result);
			}
		}
		GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op2), 0);
	}

	FREE_OP1();
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(22, GEAR_QM_ASSIGN, CONST|TMP|VAR|CV, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *value;
	zval *result = EX_VAR(opline->result.var);

	value = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(value) == IS_UNDEF)) {
		SAVE_OPLINE();
		GET_OP1_UNDEF_CV(value, BP_VAR_R);
		ZVAL_NULL(result);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}

	if (OP1_TYPE == IS_CV) {
		ZVAL_COPY_DEREF(result, value);
	} else if (OP1_TYPE == IS_VAR) {
		if (UNEXPECTED(Z_ISREF_P(value))) {
			ZVAL_COPY_VALUE(result, Z_REFVAL_P(value));
			if (UNEXPECTED(Z_DELREF_P(value) == 0)) {
				efree_size(Z_REF_P(value), sizeof(gear_reference));
			} else if (Z_OPT_REFCOUNTED_P(result)) {
				Z_ADDREF_P(result);
			}
		} else {
			ZVAL_COPY_VALUE(result, value);
		}
	} else {
		ZVAL_COPY_VALUE(result, value);
		if (OP1_TYPE == IS_CONST) {
			if (UNEXPECTED(Z_OPT_REFCOUNTED_P(result))) {
				Z_ADDREF_P(result);
			}
		}
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_HANDLER(101, GEAR_EXT_STMT, ANY, ANY)
{
	USE_OPLINE

	if (!EG(no_extensions)) {
		SAVE_OPLINE();
		gear_llist_apply_with_argument(&gear_extensions, (llist_apply_with_arg_func_t) gear_extension_statement_handler, execute_data);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_HANDLER(102, GEAR_EXT_FCALL_BEGIN, ANY, ANY)
{
	USE_OPLINE

	if (!EG(no_extensions)) {
		SAVE_OPLINE();
		gear_llist_apply_with_argument(&gear_extensions, (llist_apply_with_arg_func_t) gear_extension_fcall_begin_handler, execute_data);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_HANDLER(103, GEAR_EXT_FCALL_END, ANY, ANY)
{
	USE_OPLINE

	if (!EG(no_extensions)) {
		SAVE_OPLINE();
		gear_llist_apply_with_argument(&gear_extensions, (llist_apply_with_arg_func_t) gear_extension_fcall_end_handler, execute_data);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(139, GEAR_DECLARE_CLASS, CONST, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	Z_CE_P(EX_VAR(opline->result.var)) = do_bind_class(&EX(func)->op_array, opline, EG(class_table), 0);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(140, GEAR_DECLARE_INHERITED_CLASS, CONST, CONST)
{
	gear_class_entry *parent;
	USE_OPLINE

	SAVE_OPLINE();
	parent = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op2)),
	                                 RT_CONSTANT(opline, opline->op2) + 1,
	                                 GEAR_FETCH_CLASS_EXCEPTION);
	if (UNEXPECTED(parent == NULL)) {
		GEAR_ASSERT(EG(exception));
		HANDLE_EXCEPTION();
	}
	Z_CE_P(EX_VAR(opline->result.var)) = do_bind_inherited_class(&EX(func)->op_array, opline, EG(class_table), parent, 0);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(145, GEAR_DECLARE_INHERITED_CLASS_DELAYED, CONST, CONST)
{
	USE_OPLINE
	zval *zce, *orig_zce;
	gear_class_entry *parent;

	SAVE_OPLINE();
	if ((zce = gear_hash_find_ex(EG(class_table), Z_STR_P(RT_CONSTANT(opline, opline->op1)), 1)) == NULL ||
	    ((orig_zce = gear_hash_find_ex(EG(class_table), Z_STR_P(RT_CONSTANT(opline, opline->op1)+1), 1)) != NULL &&
	     Z_CE_P(zce) != Z_CE_P(orig_zce))) {
		parent = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op2)),
	                                 RT_CONSTANT(opline, opline->op2) + 1,
	                                 GEAR_FETCH_CLASS_EXCEPTION);
		if (UNEXPECTED(parent == NULL)) {
			GEAR_ASSERT(EG(exception));
			HANDLE_EXCEPTION();
		}
		do_bind_inherited_class(&EX(func)->op_array, opline, EG(class_table), parent, 0);
	}
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(171, GEAR_DECLARE_ANON_CLASS, ANY, ANY, JMP_ADDR)
{
	zval *zv;
	gear_class_entry *ce;
	USE_OPLINE

	SAVE_OPLINE();
	zv = gear_hash_find_ex(EG(class_table), Z_STR_P(RT_CONSTANT(opline, opline->op1)), 1);
	GEAR_ASSERT(zv != NULL);
	ce = Z_CE_P(zv);
	Z_CE_P(EX_VAR(opline->result.var)) = ce;

	if (ce->ce_flags & GEAR_ACC_ANON_BOUND) {
		GEAR_VM_SET_RELATIVE_OPCODE(opline, opline->extended_value);
		GEAR_VM_CONTINUE();
	}

	if (!(ce->ce_flags & (GEAR_ACC_INTERFACE|GEAR_ACC_IMPLEMENT_INTERFACES|GEAR_ACC_IMPLEMENT_TRAITS))) {
		gear_verify_abstract_class(ce);
	}
	ce->ce_flags |= GEAR_ACC_ANON_BOUND;
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(172, GEAR_DECLARE_ANON_INHERITED_CLASS, CONST, CONST, JMP_ADDR)
{
	zval *zv;
	gear_class_entry *ce, *parent;
	USE_OPLINE

	SAVE_OPLINE();
	zv = gear_hash_find_ex(EG(class_table), Z_STR_P(RT_CONSTANT(opline, opline->op1)), 1);
	GEAR_ASSERT(zv != NULL);
	ce = Z_CE_P(zv);
	Z_CE_P(EX_VAR(opline->result.var)) = ce;

	if (ce->ce_flags & GEAR_ACC_ANON_BOUND) {
		GEAR_VM_SET_RELATIVE_OPCODE(opline, opline->extended_value);
		GEAR_VM_CONTINUE();
	}

	parent = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op2)),
	                                 RT_CONSTANT(opline, opline->op2) + 1,
	                                 GEAR_FETCH_CLASS_EXCEPTION);
	if (UNEXPECTED(parent == NULL)) {
		GEAR_ASSERT(EG(exception));
		HANDLE_EXCEPTION();
	}

	gear_do_inheritance(ce, parent);
	ce->ce_flags |= GEAR_ACC_ANON_BOUND;
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(141, GEAR_DECLARE_FUNCTION, ANY, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	do_bind_function(&EX(func)->op_array, opline, EG(function_table), 0);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(105, GEAR_TICKS, ANY, ANY, NUM)
{
	USE_OPLINE

	if ((uint32_t)++EG(ticks_count) >= opline->extended_value) {
		EG(ticks_count) = 0;
		if (gear_ticks_function) {
			SAVE_OPLINE();
			gear_ticks_function(opline->extended_value);
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		}
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(138, GEAR_INSTANCEOF, TMPVAR|CV, UNUSED|CLASS_FETCH|CONST|VAR, CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *expr;
	gear_bool result;

	SAVE_OPLINE();
	expr = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);

GEAR_VM_C_LABEL(try_instanceof):
	if (Z_TYPE_P(expr) == IS_OBJECT) {
		gear_class_entry *ce;

		if (OP2_TYPE == IS_CONST) {
			ce = CACHED_PTR(opline->extended_value);
			if (UNEXPECTED(ce == NULL)) {
				ce = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op2)), RT_CONSTANT(opline, opline->op2) + 1, GEAR_FETCH_CLASS_NO_AUTOLOAD);
				if (EXPECTED(ce)) {
					CACHE_PTR(opline->extended_value, ce);
				}
			}
		} else if (OP2_TYPE == IS_UNUSED) {
			ce = gear_fetch_class(NULL, opline->op2.num);
			if (UNEXPECTED(ce == NULL)) {
				GEAR_ASSERT(EG(exception));
				FREE_OP1();
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
		} else {
			ce = Z_CE_P(EX_VAR(opline->op2.var));
		}
		result = ce && instanceof_function(Z_OBJCE_P(expr), ce);
	} else if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_TYPE_P(expr) == IS_REFERENCE) {
		expr = Z_REFVAL_P(expr);
		GEAR_VM_C_GOTO(try_instanceof);
	} else {
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(expr) == IS_UNDEF)) {
			GET_OP1_UNDEF_CV(expr, BP_VAR_R);
		}
		result = 0;
	}
	FREE_OP1();
	GEAR_VM_SMART_BRANCH(result, 1);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_HANDLER(104, GEAR_EXT_NOP, ANY, ANY)
{
	USE_OPLINE

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_HANDLER(0, GEAR_NOP, ANY, ANY)
{
	USE_OPLINE

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(144, GEAR_ADD_INTERFACE, ANY, CONST, CACHE_SLOT)
{
	USE_OPLINE
	gear_class_entry *ce = Z_CE_P(EX_VAR(opline->op1.var));
	gear_class_entry *iface;

	SAVE_OPLINE();
	iface = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op2)), RT_CONSTANT(opline, opline->op2) + 1, GEAR_FETCH_CLASS_INTERFACE);
	if (UNEXPECTED(iface == NULL)) {
		GEAR_ASSERT(EG(exception));
		HANDLE_EXCEPTION();
	}

	if (UNEXPECTED((iface->ce_flags & GEAR_ACC_INTERFACE) == 0)) {
		gear_error_noreturn(E_ERROR, "%s cannot implement %s - it is not an interface", ZSTR_VAL(ce->name), ZSTR_VAL(iface->name));
	}
	gear_do_implement_interface(ce, iface);

	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(154, GEAR_ADD_TRAIT, ANY, ANY, CACHE_SLOT)
{
	USE_OPLINE
	gear_class_entry *ce = Z_CE_P(EX_VAR(opline->op1.var));
	gear_class_entry *trait;

	SAVE_OPLINE();
	trait = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(opline, opline->op2)),
	                                 RT_CONSTANT(opline, opline->op2) + 1,
	                                 GEAR_FETCH_CLASS_TRAIT);
	if (UNEXPECTED(trait == NULL)) {
		GEAR_ASSERT(EG(exception));
		HANDLE_EXCEPTION();
	}
	if (!(trait->ce_flags & GEAR_ACC_TRAIT)) {
		gear_error_noreturn(E_ERROR, "%s cannot use %s - it is not a trait", ZSTR_VAL(ce->name), ZSTR_VAL(trait->name));
	}

	gear_do_implement_trait(ce, trait);

	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(155, GEAR_BIND_TRAITS, ANY, ANY)
{
	USE_OPLINE
	gear_class_entry *ce = Z_CE_P(EX_VAR(opline->op1.var));

	SAVE_OPLINE();
	gear_do_bind_traits(ce);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HELPER(gear_dispatch_try_catch_finally_helper, ANY, ANY, uint32_t try_catch_offset, uint32_t op_num)
{
	/* May be NULL during generator closing (only finally blocks are executed) */
	gear_object *ex = EG(exception);

	/* Walk try/catch/finally structures upwards, performing the necessary actions */
	while (try_catch_offset != (uint32_t) -1) {
		gear_try_catch_element *try_catch =
			&EX(func)->op_array.try_catch_array[try_catch_offset];

		if (op_num < try_catch->catch_op && ex) {
			/* Go to catch block */
			cleanup_live_vars(execute_data, op_num, try_catch->catch_op);
			GEAR_VM_JMP_EX(&EX(func)->op_array.opcodes[try_catch->catch_op], 0);

		} else if (op_num < try_catch->finally_op) {
			/* Go to finally block */
			zval *fast_call = EX_VAR(EX(func)->op_array.opcodes[try_catch->finally_end].op1.var);
			cleanup_live_vars(execute_data, op_num, try_catch->finally_op);
			Z_OBJ_P(fast_call) = EG(exception);
			EG(exception) = NULL;
			Z_OPLINE_NUM_P(fast_call) = (uint32_t)-1;
			GEAR_VM_JMP_EX(&EX(func)->op_array.opcodes[try_catch->finally_op], 0);

		} else if (op_num < try_catch->finally_end) {
			zval *fast_call = EX_VAR(EX(func)->op_array.opcodes[try_catch->finally_end].op1.var);

			/* cleanup incomplete RETURN statement */
			if (Z_OPLINE_NUM_P(fast_call) != (uint32_t)-1
			 && (EX(func)->op_array.opcodes[Z_OPLINE_NUM_P(fast_call)].op2_type & (IS_TMP_VAR | IS_VAR))) {
				zval *return_value = EX_VAR(EX(func)->op_array.opcodes[Z_OPLINE_NUM_P(fast_call)].op2.var);

				zval_ptr_dtor(return_value);
			}

			/* Chain potential exception from wrapping finally block */
			if (Z_OBJ_P(fast_call)) {
				if (ex) {
					gear_exception_set_previous(ex, Z_OBJ_P(fast_call));
				} else {
					EG(exception) = Z_OBJ_P(fast_call);
				}
				ex = Z_OBJ_P(fast_call);
			}
		}

		try_catch_offset--;
	}

	/* Uncaught exception */
	cleanup_live_vars(execute_data, op_num, 0);
	if (UNEXPECTED((EX_CALL_INFO() & GEAR_CALL_GENERATOR) != 0)) {
		gear_generator *generator = gear_get_running_generator(EXECUTE_DATA_C);
		gear_generator_close(generator, 1);
		GEAR_VM_RETURN();
	} else {
		GEAR_VM_DISPATCH_TO_HELPER(gear_leave_helper);
	}
}

GEAR_VM_HANDLER(149, GEAR_HANDLE_EXCEPTION, ANY, ANY)
{
	const gear_op *throw_op = EG(opline_before_exception);
	uint32_t throw_op_num = throw_op - EX(func)->op_array.opcodes;
	int i, current_try_catch_offset = -1;

	if ((throw_op->opcode == GEAR_FREE || throw_op->opcode == GEAR_FE_FREE)
		&& throw_op->extended_value & GEAR_FREE_ON_RETURN) {
		/* exceptions thrown because of loop var destruction on return/break/...
		 * are logically thrown at the end of the foreach loop, so adjust the
		 * throw_op_num.
		 */
		const gear_live_range *range = find_live_range(
			&EX(func)->op_array, throw_op_num, throw_op->op1.var);
		throw_op_num = range->end;
	}

	/* Find the innermost try/catch/finally the exception was thrown in */
	for (i = 0; i < EX(func)->op_array.last_try_catch; i++) {
		gear_try_catch_element *try_catch = &EX(func)->op_array.try_catch_array[i];
		if (try_catch->try_op > throw_op_num) {
			/* further blocks will not be relevant... */
			break;
		}
		if (throw_op_num < try_catch->catch_op || throw_op_num < try_catch->finally_end) {
			current_try_catch_offset = i;
		}
	}

	cleanup_unfinished_calls(execute_data, throw_op_num);

	if (throw_op->result_type & (IS_VAR | IS_TMP_VAR)) {
		switch (throw_op->opcode) {
			case GEAR_ADD_ARRAY_ELEMENT:
			case GEAR_ROPE_INIT:
			case GEAR_ROPE_ADD:
				break; /* exception while building structures, live range handling will free those */

			case GEAR_FETCH_CLASS:
			case GEAR_DECLARE_CLASS:
			case GEAR_DECLARE_INHERITED_CLASS:
			case GEAR_DECLARE_ANON_CLASS:
			case GEAR_DECLARE_ANON_INHERITED_CLASS:
				break; /* return value is gear_class_entry pointer */

			default:
				zval_ptr_dtor_nogc(EX_VAR(throw_op->result.var));
		}
	}

	GEAR_VM_DISPATCH_TO_HELPER(gear_dispatch_try_catch_finally_helper, try_catch_offset, current_try_catch_offset, op_num, throw_op_num);
}

GEAR_VM_HANDLER(146, GEAR_VERIFY_ABSTRACT_CLASS, ANY, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	gear_verify_abstract_class(Z_CE_P(EX_VAR(opline->op1.var)));
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(150, GEAR_USER_OPCODE, ANY, ANY)
{
	USE_OPLINE
	int ret;

	SAVE_OPLINE();
	ret = gear_user_opcode_handlers[opline->opcode](execute_data);
	opline = EX(opline);

	switch (ret) {
		case GEAR_USER_OPCODE_CONTINUE:
			GEAR_VM_CONTINUE();
		case GEAR_USER_OPCODE_RETURN:
			if (UNEXPECTED((EX_CALL_INFO() & GEAR_CALL_GENERATOR) != 0)) {
				gear_generator *generator = gear_get_running_generator(EXECUTE_DATA_C);
				gear_generator_close(generator, 1);
				GEAR_VM_RETURN();
			} else {
				GEAR_VM_DISPATCH_TO_HELPER(gear_leave_helper);
			}
		case GEAR_USER_OPCODE_ENTER:
			GEAR_VM_ENTER();
		case GEAR_USER_OPCODE_LEAVE:
			GEAR_VM_LEAVE();
		case GEAR_USER_OPCODE_DISPATCH:
			GEAR_VM_DISPATCH(opline->opcode, opline);
		default:
			GEAR_VM_DISPATCH((gear_uchar)(ret & 0xff), opline);
	}
}

GEAR_VM_HANDLER(143, GEAR_DECLARE_CONST, CONST, CONST)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *name;
	zval *val;
	gear_constant c;

	SAVE_OPLINE();
	name  = GET_OP1_ZVAL_PTR(BP_VAR_R);
	val   = GET_OP2_ZVAL_PTR(BP_VAR_R);

	ZVAL_COPY(&c.value, val);
	if (Z_OPT_CONSTANT(c.value)) {
		if (UNEXPECTED(zval_update_constant_ex(&c.value, EX(func)->op_array.scope) != SUCCESS)) {
			zval_ptr_dtor_nogc(&c.value);
			FREE_OP1();
			FREE_OP2();
			HANDLE_EXCEPTION();
		}
	}
	/* non persistent, case sensitive */
	GEAR_CONSTANT_SET_FLAGS(&c, CONST_CS, HYSS_USER_CONSTANT);
	c.name = gear_string_copy(Z_STR_P(name));

	if (gear_register_constant(&c) == FAILURE) {
	}

	FREE_OP1();
	FREE_OP2();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(153, GEAR_DECLARE_LAMBDA_FUNCTION, CONST, UNUSED)
{
	USE_OPLINE
	zval *zfunc;
	zval *object;
	gear_class_entry *called_scope;
	gear_function *fbc;

	zfunc = gear_hash_find_ex(EG(function_table), Z_STR_P(RT_CONSTANT(opline, opline->op1)), 1);
	GEAR_ASSERT(zfunc != NULL && Z_FUNC_P(zfunc)->type == GEAR_USER_FUNCTION);

	fbc = Z_PTR_P(zfunc);
	if (fbc->common.fn_flags & GEAR_ACC_IMMUTABLE) {
		gear_function *new_func = gear_arena_alloc(&CG(arena), sizeof(gear_op_array));

		memcpy(new_func, fbc, sizeof(gear_op_array));
		new_func->common.fn_flags &= ~GEAR_ACC_IMMUTABLE;
		Z_PTR_P(zfunc) = fbc = new_func;
	}

	if (Z_TYPE(EX(This)) == IS_OBJECT) {
		called_scope = Z_OBJCE(EX(This));
		if (UNEXPECTED((Z_FUNC_P(zfunc)->common.fn_flags & GEAR_ACC_STATIC) ||
				(EX(func)->common.fn_flags & GEAR_ACC_STATIC))) {
			object = NULL;
		} else {
			object = &EX(This);
		}
	} else {
		called_scope = Z_CE(EX(This));
		object = NULL;
	}
	gear_create_closure(EX_VAR(opline->result.var), Z_FUNC_P(zfunc),
		EX(func)->op_array.scope, called_scope, object);

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(156, GEAR_SEPARATE, VAR, UNUSED)
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = EX_VAR(opline->op1.var);
	if (UNEXPECTED(Z_ISREF_P(var_ptr))) {
		if (UNEXPECTED(Z_REFCOUNT_P(var_ptr) == 1)) {
			ZVAL_UNREF(var_ptr);
		}
	}

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_HELPER(gear_yield_in_closed_generator_helper, ANY, ANY)
{
	USE_OPLINE

	SAVE_OPLINE();
	gear_throw_error(NULL, "Cannot yield from finally in a force-closed generator");
	FREE_UNFETCHED_OP2();
	FREE_UNFETCHED_OP1();
	UNDEF_RESULT();
	HANDLE_EXCEPTION();
}

GEAR_VM_HANDLER(160, GEAR_YIELD, CONST|TMP|VAR|CV|UNUSED, CONST|TMP|VAR|CV|UNUSED, SRC)
{
	USE_OPLINE

	gear_generator *generator = gear_get_running_generator(EXECUTE_DATA_C);

	SAVE_OPLINE();
	if (UNEXPECTED(generator->flags & GEAR_GENERATOR_FORCED_CLOSE)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_yield_in_closed_generator_helper);
	}

	/* Destroy the previously yielded value */
	zval_ptr_dtor(&generator->value);

	/* Destroy the previously yielded key */
	zval_ptr_dtor(&generator->key);

	/* Set the new yielded value */
	if (OP1_TYPE != IS_UNUSED) {
		gear_free_op free_op1;

		if (UNEXPECTED(EX(func)->op_array.fn_flags & GEAR_ACC_RETURN_REFERENCE)) {
			/* Constants and temporary variables aren't yieldable by reference,
			 * but we still allow them with a notice. */
			if (OP1_TYPE & (IS_CONST|IS_TMP_VAR)) {
				zval *value;

				gear_error(E_NOTICE, "Only variable references should be yielded by reference");

				value = GET_OP1_ZVAL_PTR(BP_VAR_R);
				ZVAL_COPY_VALUE(&generator->value, value);
				if (OP1_TYPE == IS_CONST) {
					if (UNEXPECTED(Z_OPT_REFCOUNTED(generator->value))) {
						Z_ADDREF(generator->value);
					}
				}
			} else {
				zval *value_ptr = GET_OP1_ZVAL_PTR_PTR(BP_VAR_W);

				/* If a function call result is yielded and the function did
				 * not return by reference we throw a notice. */
				if (OP1_TYPE == IS_VAR &&
				    (value_ptr == &EG(uninitialized_zval) ||
				     (opline->extended_value == GEAR_RETURNS_FUNCTION &&
				      !Z_ISREF_P(value_ptr)))) {
					gear_error(E_NOTICE, "Only variable references should be yielded by reference");
					ZVAL_COPY(&generator->value, value_ptr);
				} else {
					if (Z_ISREF_P(value_ptr)) {
						Z_ADDREF_P(value_ptr);
					} else {
						ZVAL_MAKE_REF_EX(value_ptr, 2);
					}
					ZVAL_REF(&generator->value, Z_REF_P(value_ptr));
				}

				FREE_OP1_VAR_PTR();
			}
		} else {
			zval *value = GET_OP1_ZVAL_PTR(BP_VAR_R);

			/* Consts, temporary variables and references need copying */
			if (OP1_TYPE == IS_CONST) {
				ZVAL_COPY_VALUE(&generator->value, value);
				if (UNEXPECTED(Z_OPT_REFCOUNTED(generator->value))) {
					Z_ADDREF(generator->value);
				}
			} else if (OP1_TYPE == IS_TMP_VAR) {
				ZVAL_COPY_VALUE(&generator->value, value);
            } else if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(value)) {
				ZVAL_COPY(&generator->value, Z_REFVAL_P(value));
				FREE_OP1_IF_VAR();
			} else {
				ZVAL_COPY_VALUE(&generator->value, value);
				if (OP1_TYPE == IS_CV) {
					if (Z_OPT_REFCOUNTED_P(value)) Z_ADDREF_P(value);
				}
			}
		}
	} else {
		/* If no value was specified yield null */
		ZVAL_NULL(&generator->value);
	}

	/* Set the new yielded key */
	if (OP2_TYPE != IS_UNUSED) {
		gear_free_op free_op2;
		zval *key = GET_OP2_ZVAL_PTR(BP_VAR_R);

		/* Consts, temporary variables and references need copying */
		if (OP2_TYPE == IS_CONST) {
			ZVAL_COPY_VALUE(&generator->key, key);
			if (UNEXPECTED(Z_OPT_REFCOUNTED(generator->key))) {
				Z_ADDREF(generator->key);
			}
		} else if (OP2_TYPE == IS_TMP_VAR) {
			ZVAL_COPY_VALUE(&generator->key, key);
		} else if ((OP2_TYPE & (IS_VAR|IS_CV)) && Z_ISREF_P(key)) {
			ZVAL_COPY(&generator->key, Z_REFVAL_P(key));
			FREE_OP2_IF_VAR();
		} else {
			ZVAL_COPY_VALUE(&generator->key, key);
			if (OP2_TYPE == IS_CV) {
				if (Z_OPT_REFCOUNTED_P(key)) Z_ADDREF_P(key);
			}
		}

		if (Z_TYPE(generator->key) == IS_LONG
		    && Z_LVAL(generator->key) > generator->largest_used_integer_key
		) {
			generator->largest_used_integer_key = Z_LVAL(generator->key);
		}
	} else {
		/* If no key was specified we use auto-increment keys */
		generator->largest_used_integer_key++;
		ZVAL_LONG(&generator->key, generator->largest_used_integer_key);
	}

	if (RETURN_VALUE_USED(opline)) {
		/* If the return value of yield is used set the send
		 * target and initialize it to NULL */
		generator->send_target = EX_VAR(opline->result.var);
		ZVAL_NULL(generator->send_target);
	} else {
		generator->send_target = NULL;
	}

	/* We increment to the next op, so we are at the correct position when the
	 * generator is resumed. */
	GEAR_VM_INC_OPCODE();

	/* The GOTO VM uses a local opline variable. We need to set the opline
	 * variable in execute_data so we don't resume at an old position. */
	SAVE_OPLINE();

	GEAR_VM_RETURN();
}

GEAR_VM_HANDLER(142, GEAR_YIELD_FROM, CONST|TMP|VAR|CV, ANY)
{
	USE_OPLINE

	gear_generator *generator = gear_get_running_generator(EXECUTE_DATA_C);

	zval *val;
	gear_free_op free_op1;

	SAVE_OPLINE();
	val = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);

	if (UNEXPECTED(generator->flags & GEAR_GENERATOR_FORCED_CLOSE)) {
		gear_throw_error(NULL, "Cannot use \"yield from\" in a force-closed generator");
		FREE_OP1();
		UNDEF_RESULT();
		HANDLE_EXCEPTION();
	}

	if (Z_TYPE_P(val) == IS_ARRAY) {
		ZVAL_COPY_VALUE(&generator->values, val);
		if (OP1_TYPE != IS_TMP_VAR && Z_OPT_REFCOUNTED_P(val)) {
			Z_ADDREF_P(val);
		}
		Z_FE_POS(generator->values) = 0;

		FREE_OP1_IF_VAR();
	} else if (OP1_TYPE != IS_CONST && Z_TYPE_P(val) == IS_OBJECT && Z_OBJCE_P(val)->get_iterator) {
		gear_class_entry *ce = Z_OBJCE_P(val);
		if (ce == gear_ce_generator) {
			gear_generator *new_gen = (gear_generator *) Z_OBJ_P(val);

			if (OP1_TYPE != IS_TMP_VAR) {
				Z_ADDREF_P(val);
			}
			FREE_OP1_IF_VAR();

			if (Z_ISUNDEF(new_gen->retval)) {
				if (UNEXPECTED(gear_generator_get_current(new_gen) == generator)) {
					gear_throw_error(NULL, "Impossible to yield from the Generator being currently run");
					zval_ptr_dtor(val);
					UNDEF_RESULT();
					HANDLE_EXCEPTION();
				} else {
					gear_generator_yield_from(generator, new_gen);
				}
			} else if (UNEXPECTED(new_gen->execute_data == NULL)) {
				gear_throw_error(NULL, "Generator passed to yield from was aborted without proper return and is unable to continue");
				zval_ptr_dtor(val);
				UNDEF_RESULT();
				HANDLE_EXCEPTION();
			} else {
				if (RETURN_VALUE_USED(opline)) {
					ZVAL_COPY(EX_VAR(opline->result.var), &new_gen->retval);
				}
				GEAR_VM_NEXT_OPCODE();
			}
		} else {
			gear_object_iterator *iter = ce->get_iterator(ce, val, 0);
			FREE_OP1();

			if (UNEXPECTED(!iter) || UNEXPECTED(EG(exception))) {
				if (!EG(exception)) {
					gear_throw_error(NULL, "Object of type %s did not create an Iterator", ZSTR_VAL(ce->name));
				}
				UNDEF_RESULT();
				HANDLE_EXCEPTION();
			}

			iter->index = 0;
			if (iter->funcs->rewind) {
				iter->funcs->rewind(iter);
				if (UNEXPECTED(EG(exception) != NULL)) {
					OBJ_RELEASE(&iter->std);
					UNDEF_RESULT();
					HANDLE_EXCEPTION();
				}
			}

			ZVAL_OBJ(&generator->values, &iter->std);
		}
	} else {
		gear_throw_error(NULL, "Can use \"yield from\" only with arrays and Traversables");
		UNDEF_RESULT();
		HANDLE_EXCEPTION();
	}

	/* This is the default return value
	 * when the expression is a Generator, it will be overwritten in gear_generator_resume() */
	if (RETURN_VALUE_USED(opline)) {
		ZVAL_NULL(EX_VAR(opline->result.var));
	}

	/* This generator has no send target (though the generator we delegate to might have one) */
	generator->send_target = NULL;

	/* We increment to the next op, so we are at the correct position when the
	 * generator is resumed. */
	GEAR_VM_INC_OPCODE();

	/* The GOTO VM uses a local opline variable. We need to set the opline
	 * variable in execute_data so we don't resume at an old position. */
	SAVE_OPLINE();

	GEAR_VM_RETURN();
}

GEAR_VM_HANDLER(159, GEAR_DISCARD_EXCEPTION, ANY, ANY)
{
	USE_OPLINE
	zval *fast_call = EX_VAR(opline->op1.var);
	SAVE_OPLINE();

	/* cleanup incomplete RETURN statement */
	if (Z_OPLINE_NUM_P(fast_call) != (uint32_t)-1
	 && (EX(func)->op_array.opcodes[Z_OPLINE_NUM_P(fast_call)].op2_type & (IS_TMP_VAR | IS_VAR))) {
		zval *return_value = EX_VAR(EX(func)->op_array.opcodes[Z_OPLINE_NUM_P(fast_call)].op2.var);

		zval_ptr_dtor(return_value);
	}

	/* cleanup delayed exception */
	if (Z_OBJ_P(fast_call) != NULL) {
		/* discard the previously thrown exception */
		OBJ_RELEASE(Z_OBJ_P(fast_call));
		Z_OBJ_P(fast_call) = NULL;
	}

	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(162, GEAR_FAST_CALL, JMP_ADDR, ANY)
{
	USE_OPLINE
	zval *fast_call = EX_VAR(opline->result.var);

	Z_OBJ_P(fast_call) = NULL;
	/* set return address */
	Z_OPLINE_NUM_P(fast_call) = opline - EX(func)->op_array.opcodes;
	GEAR_VM_JMP_EX(OP_JMP_ADDR(opline, opline->op1), 0);
}

GEAR_VM_HANDLER(163, GEAR_FAST_RET, ANY, TRY_CATCH)
{
	USE_OPLINE
	zval *fast_call = EX_VAR(opline->op1.var);
	uint32_t current_try_catch_offset, current_op_num;

	if (Z_OPLINE_NUM_P(fast_call) != (uint32_t)-1) {
		const gear_op *fast_ret = EX(func)->op_array.opcodes + Z_OPLINE_NUM_P(fast_call);

		GEAR_VM_JMP_EX(fast_ret + 1, 0);
	}

	/* special case for unhandled exceptions */
	EG(exception) = Z_OBJ_P(fast_call);
	Z_OBJ_P(fast_call) = NULL;
	current_try_catch_offset = opline->op2.num;
	current_op_num = opline - EX(func)->op_array.opcodes;
	GEAR_VM_DISPATCH_TO_HELPER(gear_dispatch_try_catch_finally_helper, try_catch_offset, current_try_catch_offset, op_num, current_op_num);
}

GEAR_VM_HOT_HANDLER(168, GEAR_BIND_GLOBAL, CV, CONST, CACHE_SLOT)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *varname;
	zval *value;
	zval *variable_ptr;
	uintptr_t idx;
	gear_reference *ref;

	GEAR_VM_REPEATABLE_OPCODE

	varname = GET_OP2_ZVAL_PTR(BP_VAR_R);

	/* We store "hash slot index" + 1 (NULL is a mark of uninitialized cache slot) */
	idx = (uintptr_t)CACHED_PTR(opline->extended_value) - 1;
	if (EXPECTED(idx < EG(symbol_table).nNumUsed * sizeof(Bucket))) {
		Bucket *p = (Bucket*)((char*)EG(symbol_table).arData + idx);

		if (EXPECTED(Z_TYPE(p->val) != IS_UNDEF) &&
	        (EXPECTED(p->key == Z_STR_P(varname)) ||
	         (EXPECTED(p->h == ZSTR_H(Z_STR_P(varname))) &&
	          EXPECTED(p->key != NULL) &&
	          EXPECTED(gear_string_equal_content(p->key, Z_STR_P(varname)))))) {

			value = (zval*)p; /* value = &p->val; */
			GEAR_VM_C_GOTO(check_indirect);
		}
	}

	value = gear_hash_find_ex(&EG(symbol_table), Z_STR_P(varname), 1);
	if (UNEXPECTED(value == NULL)) {
		value = gear_hash_add_new(&EG(symbol_table), Z_STR_P(varname), &EG(uninitialized_zval));
		idx = (char*)value - (char*)EG(symbol_table).arData;
		/* Store "hash slot index" + 1 (NULL is a mark of uninitialized cache slot) */
		CACHE_PTR(opline->extended_value, (void*)(idx + 1));
	} else {
		idx = (char*)value - (char*)EG(symbol_table).arData;
		/* Store "hash slot index" + 1 (NULL is a mark of uninitialized cache slot) */
		CACHE_PTR(opline->extended_value, (void*)(idx + 1));
GEAR_VM_C_LABEL(check_indirect):
		/* GLOBAL variable may be an INDIRECT pointer to CV */
		if (UNEXPECTED(Z_TYPE_P(value) == IS_INDIRECT)) {
			value = Z_INDIRECT_P(value);
			if (UNEXPECTED(Z_TYPE_P(value) == IS_UNDEF)) {
				ZVAL_NULL(value);
			}
		}
	}

	if (UNEXPECTED(!Z_ISREF_P(value))) {
		ZVAL_MAKE_REF_EX(value, 2);
		ref = Z_REF_P(value);
	} else {
		ref = Z_REF_P(value);
		GC_ADDREF(ref);
	}

	variable_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);

	if (UNEXPECTED(Z_REFCOUNTED_P(variable_ptr))) {
		gear_refcounted *ref = Z_COUNTED_P(variable_ptr);
		uint32_t refcnt = GC_DELREF(ref);

		if (EXPECTED(variable_ptr != value)) {
			if (refcnt == 0) {
				SAVE_OPLINE();
				rc_dtor_func(ref);
				if (UNEXPECTED(EG(exception))) {
					ZVAL_NULL(variable_ptr);
					HANDLE_EXCEPTION();
				}
			} else {
				gc_check_possible_root(ref);
			}
		}
	}
	ZVAL_REF(variable_ptr, ref);

	GEAR_VM_REPEAT_OPCODE(GEAR_BIND_GLOBAL);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_CONST_HANDLER(121, GEAR_STRLEN, CONST|TMPVAR|CV, ANY)
{
	USE_OPLINE
	zval *value;
	gear_free_op free_op1;

	value = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_P(value) == IS_STRING)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_STRLEN_P(value));
		FREE_OP1();
		GEAR_VM_NEXT_OPCODE();
	} else {
		gear_bool strict;

		if ((OP1_TYPE & (IS_VAR|IS_CV)) && Z_TYPE_P(value) == IS_REFERENCE) {
			value = Z_REFVAL_P(value);
			if (EXPECTED(Z_TYPE_P(value) == IS_STRING)) {
				ZVAL_LONG(EX_VAR(opline->result.var), Z_STRLEN_P(value));
				FREE_OP1();
				GEAR_VM_NEXT_OPCODE();
			}
		}

		SAVE_OPLINE();
		if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(value) == IS_UNDEF)) {
			value = GET_OP1_UNDEF_CV(value, BP_VAR_R);
		}
		strict = EX_USES_STRICT_TYPES();
		do {
			if (EXPECTED(!strict)) {
				gear_string *str;
				zval tmp;

				ZVAL_COPY(&tmp, value);
				if (gear_parse_arg_str_weak(&tmp, &str)) {
					ZVAL_LONG(EX_VAR(opline->result.var), ZSTR_LEN(str));
					zval_ptr_dtor(&tmp);
					break;
				}
				zval_ptr_dtor(&tmp);
			}
			gear_internal_type_error(strict, "strlen() expects parameter 1 to be string, %s given", gear_get_type_by_const(Z_TYPE_P(value)));
			ZVAL_NULL(EX_VAR(opline->result.var));
		} while (0);
	}
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_NOCONST_HANDLER(123, GEAR_TYPE_CHECK, CONST|TMPVAR|CV, ANY, TYPE_MASK)
{
	USE_OPLINE
	zval *value;
	int result = 0;
	gear_free_op free_op1;

	value = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	if ((opline->extended_value >> (uint32_t)Z_TYPE_P(value)) & 1) {
GEAR_VM_C_LABEL(type_check_resource):
		if (EXPECTED(Z_TYPE_P(value) != IS_RESOURCE)
		 || EXPECTED(NULL != gear_rsrc_list_get_rsrc_type(Z_RES_P(value)))) {
			result = 1;
		}
	} else if ((OP1_TYPE & (IS_CV|IS_VAR)) && Z_ISREF_P(value)) {
		value = Z_REFVAL_P(value);
		if ((opline->extended_value >> (uint32_t)Z_TYPE_P(value)) & 1) {
			GEAR_VM_C_GOTO(type_check_resource);
		}
	} else if (OP1_TYPE == IS_CV && UNEXPECTED(Z_TYPE_P(value) == IS_UNDEF)) {
		result = ((1 << IS_NULL) & opline->extended_value) != 0;
		SAVE_OPLINE();
		GET_OP1_UNDEF_CV(value, BP_VAR_R);
		GEAR_VM_SMART_BRANCH(result, 1);
		ZVAL_BOOL(EX_VAR(opline->result.var), result);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
	if (OP1_TYPE & (IS_TMP_VAR|IS_VAR)) {
		SAVE_OPLINE();
		FREE_OP1();
		GEAR_VM_SMART_BRANCH(result, 1);
		ZVAL_BOOL(EX_VAR(opline->result.var), result);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	} else {
		GEAR_VM_SMART_BRANCH(result, 0);
		ZVAL_BOOL(EX_VAR(opline->result.var), result);
		GEAR_VM_NEXT_OPCODE();
	}
}

GEAR_VM_HOT_HANDLER(122, GEAR_DEFINED, CONST, ANY, CACHE_SLOT)
{
	USE_OPLINE
	gear_constant *c;
	int result;

	c = CACHED_PTR(opline->extended_value);
	do {
		if (EXPECTED(c != NULL)) {
			if (!IS_SPECIAL_CACHE_VAL(c)) {
				result = 1;
				break;
			} else if (EXPECTED(gear_hash_num_elements(EG(gear_constants)) == DECODE_SPECIAL_CACHE_NUM(c))) {
				result = 0;
				break;
			}
		}
		if (gear_quick_check_constant(RT_CONSTANT(opline, opline->op1) OPLINE_CC EXECUTE_DATA_CC) != SUCCESS) {
			CACHE_PTR(opline->extended_value, ENCODE_SPECIAL_CACHE_NUM(gear_hash_num_elements(EG(gear_constants))));
			result = 0;
		} else {
			result = 1;
		}
	} while (0);
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(151, GEAR_ASSERT_CHECK, ANY, JMP_ADDR)
{
	USE_OPLINE

	if (EG(assertions) <= 0) {
		gear_op *target = OP_JMP_ADDR(opline, opline->op2);
		if (RETURN_VALUE_USED(opline)) {
			ZVAL_TRUE(EX_VAR(opline->result.var));
		}
		GEAR_VM_JMP_EX(target, 0);
	} else {
		GEAR_VM_NEXT_OPCODE();
	}
}

GEAR_VM_HANDLER(157, GEAR_FETCH_CLASS_NAME, UNUSED|CLASS_FETCH, ANY)
{
	uint32_t fetch_type;
	gear_class_entry *called_scope, *scope;
	USE_OPLINE

	fetch_type = opline->op1.num;

	scope = EX(func)->op_array.scope;
	if (UNEXPECTED(scope == NULL)) {
		SAVE_OPLINE();
		gear_throw_error(NULL, "Cannot use \"%s\" when no class scope is active",
			fetch_type == GEAR_FETCH_CLASS_SELF ? "self" :
			fetch_type == GEAR_FETCH_CLASS_PARENT ? "parent" : "static");
		ZVAL_UNDEF(EX_VAR(opline->result.var));
		HANDLE_EXCEPTION();
	}

	switch (fetch_type) {
		case GEAR_FETCH_CLASS_SELF:
			ZVAL_STR_COPY(EX_VAR(opline->result.var), scope->name);
			break;
		case GEAR_FETCH_CLASS_PARENT:
			if (UNEXPECTED(scope->parent == NULL)) {
				SAVE_OPLINE();
				gear_throw_error(NULL,
					"Cannot use \"parent\" when current class scope has no parent");
				ZVAL_UNDEF(EX_VAR(opline->result.var));
				HANDLE_EXCEPTION();
			}
			ZVAL_STR_COPY(EX_VAR(opline->result.var), scope->parent->name);
			break;
		case GEAR_FETCH_CLASS_STATIC:
			if (Z_TYPE(EX(This)) == IS_OBJECT) {
				called_scope = Z_OBJCE(EX(This));
			} else {
				called_scope = Z_CE(EX(This));
			}
			ZVAL_STR_COPY(EX_VAR(opline->result.var), called_scope->name);
			break;
		EMPTY_SWITCH_DEFAULT_CASE()
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(158, GEAR_CALL_TRAMPOLINE, ANY, ANY)
{
	gear_array *args;
	gear_function *fbc = EX(func);
	zval *ret = EX(return_value);
	uint32_t call_info = EX_CALL_INFO() & (GEAR_CALL_NESTED | GEAR_CALL_TOP | GEAR_CALL_RELEASE_THIS);
	uint32_t num_args = EX_NUM_ARGS();
	gear_execute_data *call;

	SAVE_OPLINE();

	if (num_args) {
		zval *p = GEAR_CALL_ARG(execute_data, 1);
		zval *end = p + num_args;

		args = gear_new_array(num_args);
		gear_hash_real_init_packed(args);
		GEAR_HASH_FILL_PACKED(args) {
			do {
				GEAR_HASH_FILL_ADD(p);
				p++;
			} while (p != end);
		} GEAR_HASH_FILL_END();
	}

	call = execute_data;
	execute_data = EG(current_execute_data) = EX(prev_execute_data);

	call->func = (fbc->op_array.fn_flags & GEAR_ACC_STATIC) ? fbc->op_array.scope->__callstatic : fbc->op_array.scope->__call;
	GEAR_ASSERT(gear_vm_calc_used_stack(2, call->func) <= (size_t)(((char*)EG(vm_stack_end)) - (char*)call));
	GEAR_CALL_NUM_ARGS(call) = 2;

	ZVAL_STR(GEAR_CALL_ARG(call, 1), fbc->common.function_name);
	if (num_args) {
		ZVAL_ARR(GEAR_CALL_ARG(call, 2), args);
	} else {
		ZVAL_EMPTY_ARRAY(GEAR_CALL_ARG(call, 2));
	}
	gear_free_trampoline(fbc);
	fbc = call->func;

	if (EXPECTED(fbc->type == GEAR_USER_FUNCTION)) {
		if (UNEXPECTED(!fbc->op_array.run_time_cache)) {
			init_func_run_time_cache(&fbc->op_array);
		}
		execute_data = call;
		i_init_func_execute_data(&fbc->op_array, ret, 0 EXECUTE_DATA_CC);
		if (EXPECTED(gear_execute_ex == execute_ex)) {
			LOAD_OPLINE();
			GEAR_VM_ENTER_EX();
		} else {
			execute_data = EX(prev_execute_data);
			LOAD_OPLINE();
			GEAR_ADD_CALL_FLAG(call, GEAR_CALL_TOP);
			gear_execute_ex(call);
		}
	} else {
		zval retval;

		GEAR_ASSERT(fbc->type == GEAR_INTERNAL_FUNCTION);

		EG(current_execute_data) = call;

		if (UNEXPECTED(fbc->common.fn_flags & GEAR_ACC_HAS_TYPE_HINTS)
		 && UNEXPECTED(!gear_verify_internal_arg_types(fbc, call))) {
			gear_vm_stack_free_call_frame(call);
			if (ret) {
				ZVAL_UNDEF(ret);
			}
			GEAR_VM_C_GOTO(call_trampoline_end);
		}

		if (ret == NULL) {
			ZVAL_NULL(&retval);
			ret = &retval;
		}

		if (!gear_execute_internal) {
			/* saves one function call if gear_execute_internal is not used */
			fbc->internal_function.handler(call, ret);
		} else {
			gear_execute_internal(call, ret);
		}

#if GEAR_DEBUG
		if (!EG(exception) && call->func) {
			GEAR_ASSERT(!(call->func->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) ||
				gear_verify_internal_return_type(call->func, ret));
			GEAR_ASSERT((call->func->common.fn_flags & GEAR_ACC_RETURN_REFERENCE)
				? Z_ISREF_P(ret) : !Z_ISREF_P(ret));
		}
#endif

		EG(current_execute_data) = call->prev_execute_data;

		gear_vm_stack_free_args(call);

		if (ret == &retval) {
			zval_ptr_dtor(ret);
		}
	}

GEAR_VM_C_LABEL(call_trampoline_end):
	execute_data = EG(current_execute_data);

	if (!EX(func) || !GEAR_USER_CODE(EX(func)->type) || (call_info & GEAR_CALL_TOP)) {
		GEAR_VM_RETURN();
	}

	if (UNEXPECTED(call_info & GEAR_CALL_RELEASE_THIS)) {
		gear_object *object = Z_OBJ(call->This);
		OBJ_RELEASE(object);
	}
	gear_vm_stack_free_call_frame(call);

	if (UNEXPECTED(EG(exception) != NULL)) {
		gear_rethrow_exception(execute_data);
		HANDLE_EXCEPTION_LEAVE();
	}

	LOAD_OPLINE();
	GEAR_VM_INC_OPCODE();
	GEAR_VM_LEAVE();
}

GEAR_VM_HANDLER(182, GEAR_BIND_LEXICAL, TMP, CV, REF)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *closure, *var;

	closure = GET_OP1_ZVAL_PTR(BP_VAR_R);
	if (opline->extended_value & GEAR_BIND_REF) {
		/* By-ref binding */
		var = GET_OP2_ZVAL_PTR(BP_VAR_W);
		if (Z_ISREF_P(var)) {
			Z_ADDREF_P(var);
		} else {
			ZVAL_MAKE_REF_EX(var, 2);
		}
	} else {
		var = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
		if (UNEXPECTED(Z_ISUNDEF_P(var))) {
			SAVE_OPLINE();
			var = GET_OP2_UNDEF_CV(var, BP_VAR_R);
			if (UNEXPECTED(EG(exception))) {
				HANDLE_EXCEPTION();
			}
		}
		ZVAL_DEREF(var);
		Z_TRY_ADDREF_P(var);
	}

	gear_closure_bind_var_ex(closure, (opline->extended_value & ~GEAR_BIND_REF), var);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(183, GEAR_BIND_STATIC, CV, CONST, REF)
{
	USE_OPLINE
	gear_free_op free_op1;
	HashTable *ht;
	zval *value;
	zval *variable_ptr;

	variable_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_W);
	zval_ptr_dtor(variable_ptr);

	ht = EX(func)->op_array.static_variables;
	GEAR_ASSERT(ht != NULL);
	if (GC_REFCOUNT(ht) > 1) {
		if (!(GC_FLAGS(ht) & IS_ARRAY_IMMUTABLE)) {
			GC_DELREF(ht);
		}
		EX(func)->op_array.static_variables = ht = gear_array_dup(ht);
	}

	value = (zval*)((char*)ht->arData + (opline->extended_value & ~GEAR_BIND_REF));

	if (opline->extended_value & GEAR_BIND_REF) {
		if (Z_TYPE_P(value) == IS_CONSTANT_AST) {
			SAVE_OPLINE();
			if (UNEXPECTED(zval_update_constant_ex(value, EX(func)->op_array.scope) != SUCCESS)) {
				ZVAL_NULL(variable_ptr);
				HANDLE_EXCEPTION();
			}
		}
		if (UNEXPECTED(!Z_ISREF_P(value))) {
			gear_reference *ref = (gear_reference*)emalloc(sizeof(gear_reference));
			GC_SET_REFCOUNT(ref, 2);
			GC_TYPE_INFO(ref) = IS_REFERENCE;
			ZVAL_COPY_VALUE(&ref->val, value);
			Z_REF_P(value) = ref;
			Z_TYPE_INFO_P(value) = IS_REFERENCE_EX;
			ZVAL_REF(variable_ptr, ref);
		} else {
			Z_ADDREF_P(value);
			ZVAL_REF(variable_ptr, Z_REF_P(value));
		}
	} else {
		ZVAL_COPY(variable_ptr, value);
	}

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(184, GEAR_FETCH_THIS, UNUSED, UNUSED)
{
	USE_OPLINE

	if (EXPECTED(Z_TYPE(EX(This)) == IS_OBJECT)) {
		zval *result = EX_VAR(opline->result.var);

		ZVAL_OBJ(result, Z_OBJ(EX(This)));
		Z_ADDREF_P(result);
		GEAR_VM_NEXT_OPCODE();
	} else {
		GEAR_VM_DISPATCH_TO_HELPER(gear_this_not_in_object_context_helper);
	}
}

GEAR_VM_HANDLER(186, GEAR_ISSET_ISEMPTY_THIS, UNUSED, UNUSED)
{
	USE_OPLINE

	ZVAL_BOOL(EX_VAR(opline->result.var),
		(opline->extended_value & GEAR_ISEMPTY) ^
		 (Z_TYPE(EX(This)) == IS_OBJECT));
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(49, GEAR_CHECK_VAR, CV, UNUSED)
{
	USE_OPLINE
	zval *op1 = EX_VAR(opline->op1.var);

	if (UNEXPECTED(Z_TYPE_INFO_P(op1) == IS_UNDEF)) {
		SAVE_OPLINE();
		GET_OP1_UNDEF_CV(op1, BP_VAR_R);
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(51, GEAR_MAKE_REF, VAR|CV, UNUSED)
{
	USE_OPLINE
	zval *op1 = EX_VAR(opline->op1.var);

	if (OP1_TYPE == IS_CV) {
		if (UNEXPECTED(Z_TYPE_P(op1) == IS_UNDEF)) {
			ZVAL_NEW_EMPTY_REF(op1);
			Z_SET_REFCOUNT_P(op1, 2);
			ZVAL_NULL(Z_REFVAL_P(op1));
			ZVAL_REF(EX_VAR(opline->result.var), Z_REF_P(op1));
		} else {
			if (Z_ISREF_P(op1)) {
				Z_ADDREF_P(op1);
			} else {
				ZVAL_MAKE_REF_EX(op1, 2);
			}
			ZVAL_REF(EX_VAR(opline->result.var), Z_REF_P(op1));
		}
	} else if (EXPECTED(Z_TYPE_P(op1) == IS_INDIRECT)) {
		op1 = Z_INDIRECT_P(op1);
		if (EXPECTED(!Z_ISREF_P(op1))) {
			ZVAL_MAKE_REF_EX(op1, 2);
		} else {
			GC_ADDREF(Z_REF_P(op1));
		}
		ZVAL_REF(EX_VAR(opline->result.var), Z_REF_P(op1));
	} else {
		ZVAL_COPY_VALUE(EX_VAR(opline->result.var), op1);
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_CONSTCONST_HANDLER(187, GEAR_SWITCH_LONG, CONST|TMPVARCV, CONST, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op, *jump_zv;
	HashTable *jumptable;

	op = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	jumptable = Z_ARRVAL_P(GET_OP2_ZVAL_PTR(BP_VAR_R));

	if (Z_TYPE_P(op) != IS_LONG) {
		ZVAL_DEREF(op);
		if (Z_TYPE_P(op) != IS_LONG) {
			/* Wrong type, fall back to GEAR_CASE chain */
			GEAR_VM_NEXT_OPCODE();
		}
	}

	jump_zv = gear_hash_index_find(jumptable, Z_LVAL_P(op));
	if (jump_zv != NULL) {
		GEAR_VM_SET_RELATIVE_OPCODE(opline, Z_LVAL_P(jump_zv));
		GEAR_VM_CONTINUE();
	} else {
		/* default */
		GEAR_VM_SET_RELATIVE_OPCODE(opline, opline->extended_value);
		GEAR_VM_CONTINUE();
	}
}

GEAR_VM_COLD_CONSTCONST_HANDLER(188, GEAR_SWITCH_STRING, CONST|TMPVARCV, CONST, JMP_ADDR)
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *op, *jump_zv;
	HashTable *jumptable;

	op = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	jumptable = Z_ARRVAL_P(GET_OP2_ZVAL_PTR(BP_VAR_R));

	if (Z_TYPE_P(op) != IS_STRING) {
		if (OP1_TYPE == IS_CONST) {
			/* Wrong type, fall back to GEAR_CASE chain */
			GEAR_VM_NEXT_OPCODE();
		} else {
			ZVAL_DEREF(op);
			if (Z_TYPE_P(op) != IS_STRING) {
				/* Wrong type, fall back to GEAR_CASE chain */
				GEAR_VM_NEXT_OPCODE();
			}
		}
	}

	jump_zv = gear_hash_find_ex(jumptable, Z_STR_P(op), OP1_TYPE == IS_CONST);
	if (jump_zv != NULL) {
		GEAR_VM_SET_RELATIVE_OPCODE(opline, Z_LVAL_P(jump_zv));
		GEAR_VM_CONTINUE();
	} else {
		/* default */
		GEAR_VM_SET_RELATIVE_OPCODE(opline, opline->extended_value);
		GEAR_VM_CONTINUE();
	}
}

GEAR_VM_COLD_CONSTCONST_HANDLER(189, GEAR_IN_ARRAY, CONST|TMP|VAR|CV, CONST, NUM)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *op1;
	HashTable *ht = Z_ARRVAL_P(RT_CONSTANT(opline, opline->op2));
	int result;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_P(op1) == IS_STRING)) {
		result = gear_hash_exists(ht, Z_STR_P(op1));
	} else if (opline->extended_value) {
		if (EXPECTED(Z_TYPE_P(op1) == IS_LONG)) {
			result = gear_hash_index_exists(ht, Z_LVAL_P(op1));
		} else {
			result = 0;
		}
	} else if (Z_TYPE_P(op1) <= IS_FALSE) {
		result = gear_hash_exists(ht, ZSTR_EMPTY_ALLOC());
	} else {
		gear_string *key;
		zval key_tmp, result_tmp;

		result = 0;
		GEAR_HASH_FOREACH_STR_KEY(ht, key) {
			ZVAL_STR(&key_tmp, key);
			compare_function(&result_tmp, op1, &key_tmp);
			if (Z_LVAL(result_tmp) == 0) {
				result = 1;
				break;
			}
		} GEAR_HASH_FOREACH_END();
	}
	FREE_OP1();
	GEAR_VM_SMART_BRANCH(result, 1);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONST_HANDLER(190, GEAR_COUNT, CONST|TMP|VAR|CV, UNUSED)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *op1;
	gear_long count;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);
	do {
		if (Z_TYPE_P(op1) == IS_ARRAY) {
			count = gear_array_count(Z_ARRVAL_P(op1));
			break;
		} else if (Z_TYPE_P(op1) == IS_OBJECT) {
			/* first, we check if the handler is defined */
			if (Z_OBJ_HT_P(op1)->count_elements) {
				if (SUCCESS == Z_OBJ_HT_P(op1)->count_elements(op1, &count)) {
					break;
				}
			}

			/* if not and the object implements Countable we call its count() method */
			if (instanceof_function(Z_OBJCE_P(op1), gear_ce_countable)) {
				zval retval;

				gear_call_method_with_0_params(op1, NULL, NULL, "count", &retval);
				count = zval_get_long(&retval);
				zval_ptr_dtor(&retval);
				break;
			}

			/* If There's no handler and it doesn't implement Countable then add a warning */
			count = 1;
		} else if (Z_TYPE_P(op1) == IS_NULL) {
			count = 0;
		} else {
			count = 1;
		}
		gear_error(E_WARNING, "count(): Parameter must be an array or an object that implements Countable");
	} while (0);

	ZVAL_LONG(EX_VAR(opline->result.var), count);
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_COLD_CONST_HANDLER(191, GEAR_GET_CLASS, UNUSED|CONST|TMP|VAR|CV, UNUSED)
{
	USE_OPLINE

	if (OP1_TYPE == IS_UNUSED) {
		if (UNEXPECTED(!EX(func)->common.scope)) {
			SAVE_OPLINE();
			gear_error(E_WARNING, "get_class() called without object from outside a class");
			ZVAL_FALSE(EX_VAR(opline->result.var));
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		} else {
			ZVAL_STR_COPY(EX_VAR(opline->result.var), EX(func)->common.scope->name);
			GEAR_VM_NEXT_OPCODE();
		}
	} else {
		gear_free_op free_op1;
		zval *op1;

		SAVE_OPLINE();
		op1 = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);
		if (Z_TYPE_P(op1) == IS_OBJECT) {
			ZVAL_STR_COPY(EX_VAR(opline->result.var), Z_OBJCE_P(op1)->name);
		} else {
			gear_error(E_WARNING, "get_class() expects parameter 1 to be object, %s given", gear_get_type_by_const(Z_TYPE_P(op1)));
			ZVAL_FALSE(EX_VAR(opline->result.var));
		}
		FREE_OP1();
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}
}

GEAR_VM_HANDLER(192, GEAR_GET_CALLED_CLASS, UNUSED, UNUSED)
{
	USE_OPLINE

	if (Z_TYPE(EX(This)) == IS_OBJECT) {
		ZVAL_STR_COPY(EX_VAR(opline->result.var), Z_OBJCE(EX(This))->name);
	} else if (Z_CE(EX(This))) {
		ZVAL_STR_COPY(EX_VAR(opline->result.var), Z_CE(EX(This))->name);
	} else {
		ZVAL_FALSE(EX_VAR(opline->result.var));
		if (UNEXPECTED(!EX(func)->common.scope)) {
			SAVE_OPLINE();
			gear_error(E_WARNING, "get_called_class() called from outside a class");
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		}
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_COLD_CONST_HANDLER(193, GEAR_GET_TYPE, CONST|TMP|VAR|CV, UNUSED)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *op1;
	gear_string *type;

	SAVE_OPLINE();
	op1 = GET_OP1_ZVAL_PTR_DEREF(BP_VAR_R);
	type = gear_zval_get_type(op1);
	if (EXPECTED(type)) {
		ZVAL_INTERNED_STR(EX_VAR(opline->result.var), type);
	} else {
		ZVAL_STRING(EX_VAR(opline->result.var), "unknown type");
	}
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HANDLER(194, GEAR_FUNC_NUM_ARGS, UNUSED, UNUSED)
{
	USE_OPLINE

	ZVAL_LONG(EX_VAR(opline->result.var), EX_NUM_ARGS());
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HANDLER(195, GEAR_FUNC_GET_ARGS, UNUSED|CONST, UNUSED)
{
	USE_OPLINE
	gear_array *ht;
	uint32_t arg_count, result_size, skip;

	arg_count = EX_NUM_ARGS();
	if (OP1_TYPE == IS_CONST) {
		skip = Z_LVAL_P(RT_CONSTANT(opline, opline->op1));
		if (arg_count < skip) {
			result_size = 0;
		} else {
			result_size = arg_count - skip;
		}
	} else {
		skip = 0;
		result_size = arg_count;
	}

	if (result_size) {
		uint32_t first_extra_arg = EX(func)->op_array.num_args;

		ht = gear_new_array(result_size);
		ZVAL_ARR(EX_VAR(opline->result.var), ht);
		gear_hash_real_init_packed(ht);
		GEAR_HASH_FILL_PACKED(ht) {
			zval *p, *q;
			uint32_t i = skip;
			p = EX_VAR_NUM(i);
			if (arg_count > first_extra_arg) {
				while (i < first_extra_arg) {
					q = p;
					if (EXPECTED(Z_TYPE_INFO_P(q) != IS_UNDEF)) {
						ZVAL_DEREF(q);
						if (Z_OPT_REFCOUNTED_P(q)) {
							Z_ADDREF_P(q);
						}
					} else {
						q = &EG(uninitialized_zval);
					}
					GEAR_HASH_FILL_ADD(q);
					p++;
					i++;
				}
				if (skip < first_extra_arg) {
					skip = 0;
				} else {
					skip -= first_extra_arg;
				}
				p = EX_VAR_NUM(EX(func)->op_array.last_var + EX(func)->op_array.T + skip);
			}
			while (i < arg_count) {
				q = p;
				if (EXPECTED(Z_TYPE_INFO_P(q) != IS_UNDEF)) {
					ZVAL_DEREF(q);
					if (Z_OPT_REFCOUNTED_P(q)) {
						Z_ADDREF_P(q);
					}
				} else {
					q = &EG(uninitialized_zval);
				}
				GEAR_HASH_FILL_ADD(q);
				p++;
				i++;
			}
		} GEAR_HASH_FILL_END();
		ht->nNumOfElements = result_size;
	} else {
		ZVAL_EMPTY_ARRAY(EX_VAR(opline->result.var));
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_JMP, (OP_JMP_ADDR(op, op->op1) > op), GEAR_JMP_FORWARD, JMP_ADDR, ANY)
{
	USE_OPLINE

	OPLINE = OP_JMP_ADDR(opline, opline->op1);
	GEAR_VM_CONTINUE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_ADD, (res_info == MAY_BE_LONG && op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_ADD_LONG_NO_OVERFLOW, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	ZVAL_LONG(result, Z_LVAL_P(op1) + Z_LVAL_P(op2));
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_ADD, (op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_ADD_LONG, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	fast_long_add_function(result, op1, op2);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_ADD, (op1_info == MAY_BE_DOUBLE && op2_info == MAY_BE_DOUBLE), GEAR_ADD_DOUBLE, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	ZVAL_DOUBLE(result, Z_DVAL_P(op1) + Z_DVAL_P(op2));
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_SUB, (res_info == MAY_BE_LONG && op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_SUB_LONG_NO_OVERFLOW, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST))
{
	USE_OPLINE
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	ZVAL_LONG(result, Z_LVAL_P(op1) - Z_LVAL_P(op2));
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_SUB, (op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_SUB_LONG, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST))
{
	USE_OPLINE
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	fast_long_sub_function(result, op1, op2);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_SUB, (op1_info == MAY_BE_DOUBLE && op2_info == MAY_BE_DOUBLE), GEAR_SUB_DOUBLE, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST))
{
	USE_OPLINE
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	ZVAL_DOUBLE(result, Z_DVAL_P(op1) - Z_DVAL_P(op2));
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_MUL, (res_info == MAY_BE_LONG && op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_MUL_LONG_NO_OVERFLOW, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	ZVAL_LONG(result, Z_LVAL_P(op1) * Z_LVAL_P(op2));
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_MUL, (op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_MUL_LONG, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2, *result;
	gear_long overflow;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	GEAR_SIGNED_MULTIPLY_LONG(Z_LVAL_P(op1), Z_LVAL_P(op2), Z_LVAL_P(result), Z_DVAL_P(result), overflow);
	Z_TYPE_INFO_P(result) = overflow ? IS_DOUBLE : IS_LONG;
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_MUL, (op1_info == MAY_BE_DOUBLE && op2_info == MAY_BE_DOUBLE), GEAR_MUL_DOUBLE, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2, *result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = EX_VAR(opline->result.var);
	ZVAL_DOUBLE(result, Z_DVAL_P(op1) * Z_DVAL_P(op2));
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_IS_EQUAL, (op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_IS_EQUAL_LONG, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(SMART_BRANCH,NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2;
	int result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = (Z_LVAL_P(op1) == Z_LVAL_P(op2));
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_IS_EQUAL, (op1_info == MAY_BE_DOUBLE && op2_info == MAY_BE_DOUBLE), GEAR_IS_EQUAL_DOUBLE, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(SMART_BRANCH,NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2;
	int result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = (Z_DVAL_P(op1) == Z_DVAL_P(op2));
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_IS_NOT_EQUAL, (op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_IS_NOT_EQUAL_LONG, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(SMART_BRANCH,NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2;
	int result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = (Z_LVAL_P(op1) != Z_LVAL_P(op2));
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_IS_NOT_EQUAL, (op1_info == MAY_BE_DOUBLE && op2_info == MAY_BE_DOUBLE), GEAR_IS_NOT_EQUAL_DOUBLE, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(SMART_BRANCH,NO_CONST_CONST,COMMUTATIVE))
{
	USE_OPLINE
	zval *op1, *op2;
	int result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = (Z_DVAL_P(op1) != Z_DVAL_P(op2));
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_IS_SMALLER, (op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_IS_SMALLER_LONG, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(SMART_BRANCH,NO_CONST_CONST))
{
	USE_OPLINE
	zval *op1, *op2;
	int result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = (Z_LVAL_P(op1) < Z_LVAL_P(op2));
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_IS_SMALLER, (op1_info == MAY_BE_DOUBLE && op2_info == MAY_BE_DOUBLE), GEAR_IS_SMALLER_DOUBLE, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(SMART_BRANCH,NO_CONST_CONST))
{
	USE_OPLINE
	zval *op1, *op2;
	int result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = (Z_DVAL_P(op1) < Z_DVAL_P(op2));
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_IS_SMALLER_OR_EQUAL, (op1_info == MAY_BE_LONG && op2_info == MAY_BE_LONG), GEAR_IS_SMALLER_OR_EQUAL_LONG, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(SMART_BRANCH,NO_CONST_CONST))
{
	USE_OPLINE
	zval *op1, *op2;
	int result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = (Z_LVAL_P(op1) <= Z_LVAL_P(op2));
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_IS_SMALLER_OR_EQUAL, (op1_info == MAY_BE_DOUBLE && op2_info == MAY_BE_DOUBLE), GEAR_IS_SMALLER_OR_EQUAL_DOUBLE, CONST|TMPVARCV, CONST|TMPVARCV, SPEC(SMART_BRANCH,NO_CONST_CONST))
{
	USE_OPLINE
	zval *op1, *op2;
	int result;

	op1 = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	op2 = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	result = (Z_DVAL_P(op1) <= Z_DVAL_P(op2));
	GEAR_VM_SMART_BRANCH(result, 0);
	ZVAL_BOOL(EX_VAR(opline->result.var), result);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_PRE_INC, (res_info == MAY_BE_LONG && op1_info == MAY_BE_LONG), GEAR_PRE_INC_LONG_NO_OVERFLOW, TMPVARCV, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	Z_LVAL_P(var_ptr)++;
	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_PRE_INC, (op1_info == MAY_BE_LONG), GEAR_PRE_INC_LONG, TMPVARCV, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	fast_long_increment_function(var_ptr);
	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_COPY_VALUE(EX_VAR(opline->result.var), var_ptr);
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_PRE_INC, (op1_info == (MAY_BE_LONG|MAY_BE_DOUBLE)), GEAR_PRE_INC_LONG_OR_DOUBLE, TMPVARCV, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	if (EXPECTED(Z_TYPE_P(var_ptr) == IS_LONG)) {
		fast_long_increment_function(var_ptr);
	} else {
		Z_DVAL_P(var_ptr)++;
	}
	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_COPY_VALUE(EX_VAR(opline->result.var), var_ptr);
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_PRE_DEC, (res_info == MAY_BE_LONG && op1_info == MAY_BE_LONG), GEAR_PRE_DEC_LONG_NO_OVERFLOW, TMPVARCV, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	Z_LVAL_P(var_ptr)--;
	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_PRE_DEC, (op1_info == MAY_BE_LONG), GEAR_PRE_DEC_LONG, TMPVARCV, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	fast_long_decrement_function(var_ptr);
	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_COPY_VALUE(EX_VAR(opline->result.var), var_ptr);
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_PRE_DEC, (op1_info == (MAY_BE_LONG|MAY_BE_DOUBLE)), GEAR_PRE_DEC_LONG_OR_DOUBLE, TMPVARCV, ANY, SPEC(RETVAL))
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	if (EXPECTED(Z_TYPE_P(var_ptr) == IS_LONG)) {
		fast_long_decrement_function(var_ptr);
	} else {
		Z_DVAL_P(var_ptr)--;
	}
	if (UNEXPECTED(RETURN_VALUE_USED(opline))) {
		ZVAL_COPY_VALUE(EX_VAR(opline->result.var), var_ptr);
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_POST_INC, (res_info == MAY_BE_LONG && op1_info == MAY_BE_LONG), GEAR_POST_INC_LONG_NO_OVERFLOW, TMPVARCV, ANY)
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
	Z_LVAL_P(var_ptr)++;
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_POST_INC, (op1_info == MAY_BE_LONG), GEAR_POST_INC_LONG, TMPVARCV, ANY)
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
	fast_long_increment_function(var_ptr);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_POST_INC, (op1_info == (MAY_BE_LONG|MAY_BE_DOUBLE)), GEAR_POST_INC_LONG_OR_DOUBLE, TMPVARCV, ANY)
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	if (EXPECTED(Z_TYPE_P(var_ptr) == IS_LONG)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
		fast_long_increment_function(var_ptr);
	} else {
		ZVAL_DOUBLE(EX_VAR(opline->result.var), Z_DVAL_P(var_ptr));
		Z_DVAL_P(var_ptr)++;
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_POST_DEC, (res_info == MAY_BE_LONG && op1_info == MAY_BE_LONG), GEAR_POST_DEC_LONG_NO_OVERFLOW, TMPVARCV, ANY)
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
	Z_LVAL_P(var_ptr)--;
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_POST_DEC, (op1_info == MAY_BE_LONG), GEAR_POST_DEC_LONG, TMPVARCV, ANY)
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
	fast_long_decrement_function(var_ptr);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_POST_DEC, (op1_info == (MAY_BE_LONG|MAY_BE_DOUBLE)), GEAR_POST_DEC_LONG_OR_DOUBLE, TMPVARCV, ANY)
{
	USE_OPLINE
	zval *var_ptr;

	var_ptr = GET_OP1_ZVAL_PTR_PTR_UNDEF(BP_VAR_RW);
	if (EXPECTED(Z_TYPE_P(var_ptr) == IS_LONG)) {
		ZVAL_LONG(EX_VAR(opline->result.var), Z_LVAL_P(var_ptr));
		fast_long_decrement_function(var_ptr);
	} else {
		ZVAL_DOUBLE(EX_VAR(opline->result.var), Z_DVAL_P(var_ptr));
		Z_DVAL_P(var_ptr)--;
	}
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_QM_ASSIGN, (op1_info == MAY_BE_DOUBLE), GEAR_QM_ASSIGN_DOUBLE, CONST|TMPVARCV, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *value;

	value = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	ZVAL_DOUBLE(EX_VAR(opline->result.var), Z_DVAL_P(value));
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_QM_ASSIGN, ((op->op1_type == IS_CONST) ? !Z_REFCOUNTED_P(RT_CONSTANT(op, op->op1)) : (!(op1_info & ((MAY_BE_ANY|MAY_BE_UNDEF)-(MAY_BE_NULL|MAY_BE_FALSE|MAY_BE_TRUE|MAY_BE_LONG|MAY_BE_DOUBLE))))), GEAR_QM_ASSIGN_NOREF, CONST|TMPVARCV, ANY)
{
	USE_OPLINE
	gear_free_op free_op1;
	zval *value;

	value = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	ZVAL_COPY_VALUE(EX_VAR(opline->result.var), value);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_FETCH_DIM_R, (!(op2_info & (MAY_BE_UNDEF|MAY_BE_NULL|MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))), GEAR_FETCH_DIM_R_INDEX, CONST|TMPVAR|CV, CONST|TMPVARCV, SPEC(NO_CONST_CONST))
{
	USE_OPLINE
	gear_free_op free_op1, free_op2;
	zval *container, *dim, *value;
	gear_long offset;
	HashTable *ht;

	container = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	dim = GET_OP2_ZVAL_PTR_UNDEF(BP_VAR_R);
	if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
GEAR_VM_C_LABEL(fetch_dim_r_index_array):
		if (EXPECTED(Z_TYPE_P(dim) == IS_LONG)) {
			offset = Z_LVAL_P(dim);
		} else {
			offset = zval_get_long(dim);
		}
		ht = Z_ARRVAL_P(container);
		GEAR_HASH_INDEX_FIND(ht, offset, value, GEAR_VM_C_LABEL(fetch_dim_r_index_undef));
		ZVAL_COPY_DEREF(EX_VAR(opline->result.var), value);
		if (OP1_TYPE & (IS_TMP_VAR|IS_VAR)) {
			SAVE_OPLINE();
			FREE_OP1();
			GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
		} else {
			GEAR_VM_NEXT_OPCODE();
		}
	} else if (OP1_TYPE != IS_CONST && EXPECTED(Z_TYPE_P(container) == IS_REFERENCE)) {
		container = Z_REFVAL_P(container);
		if (EXPECTED(Z_TYPE_P(container) == IS_ARRAY)) {
			GEAR_VM_C_GOTO(fetch_dim_r_index_array);
		} else {
			GEAR_VM_C_GOTO(fetch_dim_r_index_slow);
		}
	} else {
GEAR_VM_C_LABEL(fetch_dim_r_index_slow):
		SAVE_OPLINE();
		if (OP2_TYPE == IS_CONST && Z_EXTRA_P(dim) == GEAR_EXTRA_VALUE) {
			dim++;
		}
		gear_fetch_dimension_address_read_R_slow(container, dim OPLINE_CC EXECUTE_DATA_CC);
		FREE_OP1();
		GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
	}

GEAR_VM_C_LABEL(fetch_dim_r_index_undef):
	ZVAL_NULL(EX_VAR(opline->result.var));
	SAVE_OPLINE();
	gear_undefined_offset(offset);
	FREE_OP1();
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_SEND_VAR, (op1_info & (MAY_BE_UNDEF|MAY_BE_REF)) == 0, GEAR_SEND_VAR_SIMPLE, CV|VAR, NUM)
{
	USE_OPLINE
	zval *varptr, *arg;
	gear_free_op free_op1;

	varptr = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);

	if (OP1_TYPE == IS_CV) {
		ZVAL_COPY(arg, varptr);
	} else /* if (OP1_TYPE == IS_VAR) */ {
		ZVAL_COPY_VALUE(arg, varptr);
	}

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_SEND_VAR_EX, op->op2.num <= MAX_ARG_FLAG_NUM && (op1_info & (MAY_BE_UNDEF|MAY_BE_REF)) == 0, GEAR_SEND_VAR_EX_SIMPLE, CV|VAR, NUM)
{
	USE_OPLINE
	zval *varptr, *arg;
	gear_free_op free_op1;
	uint32_t arg_num = opline->op2.num;

	if (QUICK_ARG_SHOULD_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
		GEAR_VM_DISPATCH_TO_HANDLER(GEAR_SEND_REF);
	}

	varptr = GET_OP1_ZVAL_PTR_UNDEF(BP_VAR_R);
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);

	if (OP1_TYPE == IS_CV) {
		ZVAL_COPY(arg, varptr);
	} else /* if (OP1_TYPE == IS_VAR) */ {
		ZVAL_COPY_VALUE(arg, varptr);
	}

	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_SEND_VAL, op->op1_type == IS_CONST && !Z_REFCOUNTED_P(RT_CONSTANT(op, op->op1)), GEAR_SEND_VAL_SIMPLE, CONST, NUM)
{
	USE_OPLINE
	zval *value, *arg;
	gear_free_op free_op1;

	value = GET_OP1_ZVAL_PTR(BP_VAR_R);
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);
	ZVAL_COPY_VALUE(arg, value);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_SEND_VAL_EX, op->op2.num <= MAX_ARG_FLAG_NUM && op->op1_type == IS_CONST && !Z_REFCOUNTED_P(RT_CONSTANT(op, op->op1)), GEAR_SEND_VAL_EX_SIMPLE, CONST, NUM)
{
	USE_OPLINE
	zval *value, *arg;
	gear_free_op free_op1;
	uint32_t arg_num = opline->op2.num;

	if (QUICK_ARG_MUST_BE_SENT_BY_REF(EX(call)->func, arg_num)) {
		GEAR_VM_DISPATCH_TO_HELPER(gear_cannot_pass_by_ref_helper);
	}
	value = GET_OP1_ZVAL_PTR(BP_VAR_R);
	arg = GEAR_CALL_VAR(EX(call), opline->result.var);
	ZVAL_COPY_VALUE(arg, value);
	GEAR_VM_NEXT_OPCODE();
}

GEAR_VM_HOT_TYPE_SPEC_HANDLER(GEAR_FE_FETCH_R, op->op2_type == IS_CV && (op1_info & (MAY_BE_UNDEF|MAY_BE_ANY|MAY_BE_REF)) == MAY_BE_ARRAY, GEAR_FE_FETCH_R_SIMPLE, VAR, CV, JMP_ADDR, SPEC(RETVAL))
{
	USE_OPLINE
	zval *array;
	zval *value, *variable_ptr;
	uint32_t value_type;
	HashTable *fe_ht;
	HashPosition pos;
	Bucket *p;

	array = EX_VAR(opline->op1.var);
	SAVE_OPLINE();
	fe_ht = Z_ARRVAL_P(array);
	pos = Z_FE_POS_P(array);
	p = fe_ht->arData + pos;
	while (1) {
		if (UNEXPECTED(pos >= fe_ht->nNumUsed)) {
			/* reached end of iteration */
			GEAR_VM_SET_RELATIVE_OPCODE(opline, opline->extended_value);
			GEAR_VM_CONTINUE();
		}
		value = &p->val;
		value_type = Z_TYPE_INFO_P(value);
		if (EXPECTED(value_type != IS_UNDEF)) {
			if (UNEXPECTED(value_type == IS_INDIRECT)) {
				value = Z_INDIRECT_P(value);
				value_type = Z_TYPE_INFO_P(value);
				if (EXPECTED(value_type != IS_UNDEF)) {
					break;
				}
			} else {
				break;
			}
		}
		pos++;
		p++;
	}
	Z_FE_POS_P(array) = pos + 1;
	if (RETURN_VALUE_USED(opline)) {
		if (!p->key) {
			ZVAL_LONG(EX_VAR(opline->result.var), p->h);
		} else {
			ZVAL_STR_COPY(EX_VAR(opline->result.var), p->key);
		}
	}

	variable_ptr = EX_VAR(opline->op2.var);
	gear_assign_to_variable(variable_ptr, value, IS_CV);
	GEAR_VM_NEXT_OPCODE_CHECK_EXCEPTION();
}

GEAR_VM_DEFINE_OP(137, GEAR_OP_DATA);

GEAR_VM_HELPER(gear_interrupt_helper, ANY, ANY)
{
	EG(vm_interrupt) = 0;
	if (EG(timed_out)) {
		gear_timeout(0);
	} else if (gear_interrupt_function) {
		SAVE_OPLINE();
		gear_interrupt_function(execute_data);
		GEAR_VM_ENTER();
	}
	GEAR_VM_CONTINUE();
}
