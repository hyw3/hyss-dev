/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* These iterators were designed to operate within the foreach()
 * structures provided by the engine, but could be extended for use
 * with other iterative engine opcodes.
 * These methods have similar semantics to the gear_hash API functions
 * with similar names.
 * */

typedef struct _gear_object_iterator gear_object_iterator;

typedef struct _gear_object_iterator_funcs {
	/* release all resources associated with this iterator instance */
	void (*dtor)(gear_object_iterator *iter);

	/* check for end of iteration (FAILURE or SUCCESS if data is valid) */
	int (*valid)(gear_object_iterator *iter);

	/* fetch the item data for the current element */
	zval *(*get_current_data)(gear_object_iterator *iter);

	/* fetch the key for the current element (optional, may be NULL). The key
	 * should be written into the provided zval* using the ZVAL_* macros. If
	 * this handler is not provided auto-incrementing integer keys will be
	 * used. */
	void (*get_current_key)(gear_object_iterator *iter, zval *key);

	/* step forwards to next element */
	void (*move_forward)(gear_object_iterator *iter);

	/* rewind to start of data (optional, may be NULL) */
	void (*rewind)(gear_object_iterator *iter);

	/* invalidate current value/key (optional, may be NULL) */
	void (*invalidate_current)(gear_object_iterator *iter);
} gear_object_iterator_funcs;

struct _gear_object_iterator {
	gear_object std;
	zval data;
	const gear_object_iterator_funcs *funcs;
	gear_ulong index; /* private to fe_reset/fe_fetch opcodes */
};

typedef struct _gear_class_iterator_funcs {
	union _gear_function *zf_new_iterator;
	union _gear_function *zf_valid;
	union _gear_function *zf_current;
	union _gear_function *zf_key;
	union _gear_function *zf_next;
	union _gear_function *zf_rewind;
} gear_class_iterator_funcs;

BEGIN_EXTERN_C()
/* given a zval, returns stuff that can be used to iterate it. */
GEAR_API gear_object_iterator* gear_iterator_unwrap(zval *array_ptr);

/* given an iterator, wrap it up as a zval for use by the engine opcodes */
GEAR_API void gear_iterator_init(gear_object_iterator *iter);
GEAR_API void gear_iterator_dtor(gear_object_iterator *iter);

GEAR_API void gear_register_iterator_wrapper(void);
END_EXTERN_C()

