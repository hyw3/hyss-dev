/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * gear_alloc is designed to be a modern CPU cache friendly memory manager
 * for HYSS. Most ideas are taken from jemalloc and tcmalloc implementations.
 *
 * All allocations are split into 3 categories:
 *
 * Huge  - the size is greater than CHUNK size (~2M by default), allocation is
 *         performed using mmap(). The result is aligned on 2M boundary.
 *
 * Large - a number of 4096K pages inside a CHUNK. Large blocks
 *         are always aligned on page boundary.
 *
 * Small - less than 3/4 of page size. Small sizes are rounded up to nearest
 *         greater predefined small size (there are 30 predefined sizes:
 *         8, 16, 24, 32, ... 3072). Small blocks are allocated from
 *         RUNs. Each RUN is allocated as a single or few following pages.
 *         Allocation inside RUNs implemented using linked list of free
 *         elements. The result is aligned to 8 bytes.
 *
 * gear_alloc allocates memory from OS by CHUNKs, these CHUNKs and huge memory
 * blocks are always aligned to CHUNK boundary. So it's very easy to determine
 * the CHUNK owning the certain pointer. Regular CHUNKs reserve a single
 * page at start for special purpose. It contains bitset of free pages,
 * few bitset for available runs of predefined small sizes, map of pages that
 * keeps information about usage of each page in this CHUNK, etc.
 *
 * gear_alloc provides familiar emalloc/efree/erealloc API, but in addition it
 * provides specialized and optimized routines to allocate blocks of predefined
 * sizes (e.g. emalloc_2(), emallc_4(), ..., emalloc_large(), etc)
 * The library uses C preprocessor tricks that substitute calls to emalloc()
 * with more specialized routines when the requested size is known.
 */

#include "gear.h"
#include "gear_alloc.h"
#include "gear_globals.h"
#include "gear_operators.h"
#include "gear_multiply.h"
#include "gear_bitset.h"

#ifdef HAVE_SIGNAL_H
# include <signal.h>
#endif
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef GEAR_WIN32
# include <wincrypt.h>
# include <process.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#if HAVE_LIMITS_H
#include <limits.h>
#endif
#include <fcntl.h>
#include <errno.h>

#ifndef _WIN32
# ifdef HAVE_MREMAP
#  ifndef _GNU_SOURCE
#   define _GNU_SOURCE
#  endif
#  ifndef __USE_GNU
#   define __USE_GNU
#  endif
# endif
# include <sys/mman.h>
# ifndef MAP_ANON
#  ifdef MAP_ANONYMOUS
#   define MAP_ANON MAP_ANONYMOUS
#  endif
# endif
# ifndef MREMAP_MAYMOVE
#  define MREMAP_MAYMOVE 0
# endif
# ifndef MAP_FAILED
#  define MAP_FAILED ((void*)-1)
# endif
# ifndef MAP_POPULATE
#  define MAP_POPULATE 0
# endif
#  if defined(_SC_PAGESIZE) || (_SC_PAGE_SIZE)
#    define REAL_PAGE_SIZE _real_page_size
static size_t _real_page_size = GEAR_MM_PAGE_SIZE;
#  endif
#endif

#ifndef REAL_PAGE_SIZE
# define REAL_PAGE_SIZE GEAR_MM_PAGE_SIZE
#endif

#ifndef GEAR_MM_STAT
# define GEAR_MM_STAT 1    /* track current and peak memory usage            */
#endif
#ifndef GEAR_MM_LIMIT
# define GEAR_MM_LIMIT 1   /* support for user-defined memory limit          */
#endif
#ifndef GEAR_MM_CUSTOM
# define GEAR_MM_CUSTOM 1  /* support for custom memory allocator            */
                           /* USE_GEAR_ALLOC=0 may switch to system malloc() */
#endif
#ifndef GEAR_MM_STORAGE
# define GEAR_MM_STORAGE 1 /* support for custom memory storage              */
#endif
#ifndef GEAR_MM_ERROR
# define GEAR_MM_ERROR 1   /* report system errors                           */
#endif

#ifndef GEAR_MM_CHECK
# define GEAR_MM_CHECK(condition, message)  do { \
		if (UNEXPECTED(!(condition))) { \
			gear_mm_panic(message); \
		} \
	} while (0)
#endif

typedef uint32_t   gear_mm_page_info; /* 4-byte integer */
typedef gear_ulong gear_mm_bitset;    /* 4-byte or 8-byte integer */

#define GEAR_MM_ALIGNED_OFFSET(size, alignment) \
	(((size_t)(size)) & ((alignment) - 1))
#define GEAR_MM_ALIGNED_BASE(size, alignment) \
	(((size_t)(size)) & ~((alignment) - 1))
#define GEAR_MM_SIZE_TO_NUM(size, alignment) \
	(((size_t)(size) + ((alignment) - 1)) / (alignment))

#define GEAR_MM_BITSET_LEN		(sizeof(gear_mm_bitset) * 8)       /* 32 or 64 */
#define GEAR_MM_PAGE_MAP_LEN	(GEAR_MM_PAGES / GEAR_MM_BITSET_LEN) /* 16 or 8 */

typedef gear_mm_bitset gear_mm_page_map[GEAR_MM_PAGE_MAP_LEN];     /* 64B */

#define GEAR_MM_IS_FRUN                  0x00000000
#define GEAR_MM_IS_LRUN                  0x40000000
#define GEAR_MM_IS_SRUN                  0x80000000

#define GEAR_MM_LRUN_PAGES_MASK          0x000003ff
#define GEAR_MM_LRUN_PAGES_OFFSET        0

#define GEAR_MM_SRUN_BIN_NUM_MASK        0x0000001f
#define GEAR_MM_SRUN_BIN_NUM_OFFSET      0

#define GEAR_MM_SRUN_FREE_COUNTER_MASK   0x01ff0000
#define GEAR_MM_SRUN_FREE_COUNTER_OFFSET 16

#define GEAR_MM_NRUN_OFFSET_MASK         0x01ff0000
#define GEAR_MM_NRUN_OFFSET_OFFSET       16

#define GEAR_MM_LRUN_PAGES(info)         (((info) & GEAR_MM_LRUN_PAGES_MASK) >> GEAR_MM_LRUN_PAGES_OFFSET)
#define GEAR_MM_SRUN_BIN_NUM(info)       (((info) & GEAR_MM_SRUN_BIN_NUM_MASK) >> GEAR_MM_SRUN_BIN_NUM_OFFSET)
#define GEAR_MM_SRUN_FREE_COUNTER(info)  (((info) & GEAR_MM_SRUN_FREE_COUNTER_MASK) >> GEAR_MM_SRUN_FREE_COUNTER_OFFSET)
#define GEAR_MM_NRUN_OFFSET(info)        (((info) & GEAR_MM_NRUN_OFFSET_MASK) >> GEAR_MM_NRUN_OFFSET_OFFSET)

#define GEAR_MM_FRUN()                   GEAR_MM_IS_FRUN
#define GEAR_MM_LRUN(count)              (GEAR_MM_IS_LRUN | ((count) << GEAR_MM_LRUN_PAGES_OFFSET))
#define GEAR_MM_SRUN(bin_num)            (GEAR_MM_IS_SRUN | ((bin_num) << GEAR_MM_SRUN_BIN_NUM_OFFSET))
#define GEAR_MM_SRUN_EX(bin_num, count)  (GEAR_MM_IS_SRUN | ((bin_num) << GEAR_MM_SRUN_BIN_NUM_OFFSET) | ((count) << GEAR_MM_SRUN_FREE_COUNTER_OFFSET))
#define GEAR_MM_NRUN(bin_num, offset)    (GEAR_MM_IS_SRUN | GEAR_MM_IS_LRUN | ((bin_num) << GEAR_MM_SRUN_BIN_NUM_OFFSET) | ((offset) << GEAR_MM_NRUN_OFFSET_OFFSET))

#define GEAR_MM_BINS 30

typedef struct  _gear_mm_page      gear_mm_page;
typedef struct  _gear_mm_bin       gear_mm_bin;
typedef struct  _gear_mm_free_slot gear_mm_free_slot;
typedef struct  _gear_mm_chunk     gear_mm_chunk;
typedef struct  _gear_mm_huge_list gear_mm_huge_list;

int gear_mm_use_huge_pages = 0;

/*
 * Memory is retrived from OS by chunks of fixed size 2MB.
 * Inside chunk it's managed by pages of fixed size 4096B.
 * So each chunk consists from 512 pages.
 * The first page of each chunk is reseved for chunk header.
 * It contains service information about all pages.
 *
 * free_pages - current number of free pages in this chunk
 *
 * free_tail  - number of continuous free pages at the end of chunk
 *
 * free_map   - bitset (a bit for each page). The bit is set if the corresponding
 *              page is allocated. Allocator for "lage sizes" may easily find a
 *              free page (or a continuous number of pages) searching for zero
 *              bits.
 *
 * map        - contains service information for each page. (32-bits for each
 *              page).
 *    usage:
 *				(2 bits)
 * 				FRUN - free page,
 *              LRUN - first page of "large" allocation
 *              SRUN - first page of a bin used for "small" allocation
 *
 *    lrun_pages:
 *              (10 bits) number of allocated pages
 *
 *    srun_bin_num:
 *              (5 bits) bin number (e.g. 0 for sizes 0-2, 1 for 3-4,
 *               2 for 5-8, 3 for 9-16 etc) see gear_alloc_sizes.h
 */

struct _gear_mm_heap {
#if GEAR_MM_CUSTOM
	int                use_custom_heap;
#endif
#if GEAR_MM_STORAGE
	gear_mm_storage   *storage;
#endif
#if GEAR_MM_STAT
	size_t             size;                    /* current memory usage */
	size_t             peak;                    /* peak memory usage */
#endif
	gear_mm_free_slot *free_slot[GEAR_MM_BINS]; /* free lists for small sizes */
#if GEAR_MM_STAT || GEAR_MM_LIMIT
	size_t             real_size;               /* current size of allocated pages */
#endif
#if GEAR_MM_STAT
	size_t             real_peak;               /* peak size of allocated pages */
#endif
#if GEAR_MM_LIMIT
	size_t             limit;                   /* memory limit */
	int                overflow;                /* memory overflow flag */
#endif

	gear_mm_huge_list *huge_list;               /* list of huge allocated blocks */

	gear_mm_chunk     *main_chunk;
	gear_mm_chunk     *cached_chunks;			/* list of unused chunks */
	int                chunks_count;			/* number of alocated chunks */
	int                peak_chunks_count;		/* peak number of allocated chunks for current request */
	int                cached_chunks_count;		/* number of cached chunks */
	double             avg_chunks_count;		/* average number of chunks allocated per request */
	int                last_chunks_delete_boundary; /* numer of chunks after last deletion */
	int                last_chunks_delete_count;    /* number of deletion over the last boundary */
#if GEAR_MM_CUSTOM
	union {
		struct {
			void      *(*_malloc)(size_t);
			void       (*_free)(void*);
			void      *(*_realloc)(void*, size_t);
		} std;
		struct {
			void      *(*_malloc)(size_t GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
			void       (*_free)(void*  GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
			void      *(*_realloc)(void*, size_t  GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
		} debug;
	} custom_heap;
#endif
};

struct _gear_mm_chunk {
	gear_mm_heap      *heap;
	gear_mm_chunk     *next;
	gear_mm_chunk     *prev;
	uint32_t           free_pages;				/* number of free pages */
	uint32_t           free_tail;               /* number of free pages at the end of chunk */
	uint32_t           num;
	char               reserve[64 - (sizeof(void*) * 3 + sizeof(uint32_t) * 3)];
	gear_mm_heap       heap_slot;               /* used only in main chunk */
	gear_mm_page_map   free_map;                /* 512 bits or 64 bytes */
	gear_mm_page_info  map[GEAR_MM_PAGES];      /* 2 KB = 512 * 4 */
};

struct _gear_mm_page {
	char               bytes[GEAR_MM_PAGE_SIZE];
};

/*
 * bin - is one or few continuous pages (up to 8) used for allocation of
 * a particular "small size".
 */
struct _gear_mm_bin {
	char               bytes[GEAR_MM_PAGE_SIZE * 8];
};

struct _gear_mm_free_slot {
	gear_mm_free_slot *next_free_slot;
};

struct _gear_mm_huge_list {
	void              *ptr;
	size_t             size;
	gear_mm_huge_list *next;
#if GEAR_DEBUG
	gear_mm_debug_info dbg;
#endif
};

#define GEAR_MM_PAGE_ADDR(chunk, page_num) \
	((void*)(((gear_mm_page*)(chunk)) + (page_num)))

#define _BIN_DATA_SIZE(num, size, elements, pages, x, y) size,
static const uint32_t bin_data_size[] = {
  GEAR_MM_BINS_INFO(_BIN_DATA_SIZE, x, y)
};

#define _BIN_DATA_ELEMENTS(num, size, elements, pages, x, y) elements,
static const uint32_t bin_elements[] = {
  GEAR_MM_BINS_INFO(_BIN_DATA_ELEMENTS, x, y)
};

#define _BIN_DATA_PAGES(num, size, elements, pages, x, y) pages,
static const uint32_t bin_pages[] = {
  GEAR_MM_BINS_INFO(_BIN_DATA_PAGES, x, y)
};

#if GEAR_DEBUG
GEAR_COLD void gear_debug_alloc_output(char *format, ...)
{
	char output_buf[256];
	va_list args;

	va_start(args, format);
	vsprintf(output_buf, format, args);
	va_end(args);

#ifdef GEAR_WIN32
	OutputDebugString(output_buf);
#else
	fprintf(stderr, "%s", output_buf);
#endif
}
#endif

static GEAR_COLD GEAR_NORETURN void gear_mm_panic(const char *message)
{
	fprintf(stderr, "%s\n", message);
/* See http://support.microsoft.com/kb/190351 */
#ifdef GEAR_WIN32
	fflush(stderr);
#endif
#if GEAR_DEBUG && defined(HAVE_KILL) && defined(HAVE_GETPID)
	kill(getpid(), SIGSEGV);
#endif
	exit(1);
}

static GEAR_COLD GEAR_NORETURN void gear_mm_safe_error(gear_mm_heap *heap,
	const char *format,
	size_t limit,
#if GEAR_DEBUG
	const char *filename,
	uint32_t lineno,
#endif
	size_t size)
{

	heap->overflow = 1;
	gear_try {
		gear_error_noreturn(E_ERROR,
			format,
			limit,
#if GEAR_DEBUG
			filename,
			lineno,
#endif
			size);
	} gear_catch {
	}  gear_end_try();
	heap->overflow = 0;
	gear_bailout();
	exit(1);
}

#ifdef _WIN32
void
stderr_last_error(char *msg)
{
	LPSTR buf = NULL;
	DWORD err = GetLastError();

	if (!FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			err,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPSTR)&buf,
		0, NULL)) {
		fprintf(stderr, "\n%s: [0x%08lx]\n", msg, err);
	}
	else {
		fprintf(stderr, "\n%s: [0x%08lx] %s\n", msg, err, buf);
	}
}
#endif

/*****************/
/* OS Allocation */
/*****************/

static void *gear_mm_mmap_fixed(void *addr, size_t size)
{
#ifdef _WIN32
	return VirtualAlloc(addr, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#else
	int flags = MAP_PRIVATE | MAP_ANON;
#if defined(MAP_EXCL)
	flags |= MAP_FIXED | MAP_EXCL;
#endif
	/* MAP_FIXED leads to discarding of the old mapping, so it can't be used. */
	void *ptr = mmap(addr, size, PROT_READ | PROT_WRITE, flags /*| MAP_POPULATE | MAP_HUGETLB*/, -1, 0);

	if (ptr == MAP_FAILED) {
#if GEAR_MM_ERROR && !defined(MAP_EXCL)
		fprintf(stderr, "\nmmap() failed: [%d] %s\n", errno, strerror(errno));
#endif
		return NULL;
	} else if (ptr != addr) {
		if (munmap(ptr, size) != 0) {
#if GEAR_MM_ERROR
			fprintf(stderr, "\nmunmap() failed: [%d] %s\n", errno, strerror(errno));
#endif
		}
		return NULL;
	}
	return ptr;
#endif
}

static void *gear_mm_mmap(size_t size)
{
#ifdef _WIN32
	void *ptr = VirtualAlloc(NULL, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

	if (ptr == NULL) {
#if GEAR_MM_ERROR
		stderr_last_error("VirtualAlloc() failed");
#endif
		return NULL;
	}
	return ptr;
#else
	void *ptr;

#ifdef MAP_HUGETLB
	if (gear_mm_use_huge_pages && size == GEAR_MM_CHUNK_SIZE) {
		ptr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON | MAP_HUGETLB, -1, 0);
		if (ptr != MAP_FAILED) {
			return ptr;
		}
	}
#endif

	ptr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);

	if (ptr == MAP_FAILED) {
#if GEAR_MM_ERROR
		fprintf(stderr, "\nmmap() failed: [%d] %s\n", errno, strerror(errno));
#endif
		return NULL;
	}
	return ptr;
#endif
}

static void gear_mm_munmap(void *addr, size_t size)
{
#ifdef _WIN32
	if (VirtualFree(addr, 0, MEM_RELEASE) == 0) {
#if GEAR_MM_ERROR
		stderr_last_error("VirtualFree() failed");
#endif
	}
#else
	if (munmap(addr, size) != 0) {
#if GEAR_MM_ERROR
		fprintf(stderr, "\nmunmap() failed: [%d] %s\n", errno, strerror(errno));
#endif
	}
#endif
}

/***********/
/* Bitmask */
/***********/

/* number of trailing set (1) bits */
static gear_always_inline int gear_mm_bitset_nts(gear_mm_bitset bitset)
{
#if (defined(__GNUC__) || __has_builtin(__builtin_ctzl)) && SIZEOF_GEAR_LONG == SIZEOF_LONG && defined(HYSS_HAVE_BUILTIN_CTZL)
	return __builtin_ctzl(~bitset);
#elif (defined(__GNUC__) || __has_builtin(__builtin_ctzll)) && defined(HYSS_HAVE_BUILTIN_CTZLL)
	return __builtin_ctzll(~bitset);
#elif defined(_WIN32)
	unsigned long index;

#if defined(_WIN64)
	if (!BitScanForward64(&index, ~bitset)) {
#else
	if (!BitScanForward(&index, ~bitset)) {
#endif
		/* undefined behavior */
		return 32;
	}

	return (int)index;
#else
	int n;

	if (bitset == (gear_mm_bitset)-1) return GEAR_MM_BITSET_LEN;

	n = 0;
#if SIZEOF_GEAR_LONG == 8
	if (sizeof(gear_mm_bitset) == 8) {
		if ((bitset & 0xffffffff) == 0xffffffff) {n += 32; bitset = bitset >> Z_UL(32);}
	}
#endif
	if ((bitset & 0x0000ffff) == 0x0000ffff) {n += 16; bitset = bitset >> 16;}
	if ((bitset & 0x000000ff) == 0x000000ff) {n +=  8; bitset = bitset >>  8;}
	if ((bitset & 0x0000000f) == 0x0000000f) {n +=  4; bitset = bitset >>  4;}
	if ((bitset & 0x00000003) == 0x00000003) {n +=  2; bitset = bitset >>  2;}
	return n + (bitset & 1);
#endif
}

static gear_always_inline int gear_mm_bitset_find_zero(gear_mm_bitset *bitset, int size)
{
	int i = 0;

	do {
		gear_mm_bitset tmp = bitset[i];
		if (tmp != (gear_mm_bitset)-1) {
			return i * GEAR_MM_BITSET_LEN + gear_mm_bitset_nts(tmp);
		}
		i++;
	} while (i < size);
	return -1;
}

static gear_always_inline int gear_mm_bitset_find_one(gear_mm_bitset *bitset, int size)
{
	int i = 0;

	do {
		gear_mm_bitset tmp = bitset[i];
		if (tmp != 0) {
			return i * GEAR_MM_BITSET_LEN + gear_ulong_ntz(tmp);
		}
		i++;
	} while (i < size);
	return -1;
}

static gear_always_inline int gear_mm_bitset_find_zero_and_set(gear_mm_bitset *bitset, int size)
{
	int i = 0;

	do {
		gear_mm_bitset tmp = bitset[i];
		if (tmp != (gear_mm_bitset)-1) {
			int n = gear_mm_bitset_nts(tmp);
			bitset[i] |= Z_UL(1) << n;
			return i * GEAR_MM_BITSET_LEN + n;
		}
		i++;
	} while (i < size);
	return -1;
}

static gear_always_inline int gear_mm_bitset_is_set(gear_mm_bitset *bitset, int bit)
{
	return GEAR_BIT_TEST(bitset, bit);
}

static gear_always_inline void gear_mm_bitset_set_bit(gear_mm_bitset *bitset, int bit)
{
	bitset[bit / GEAR_MM_BITSET_LEN] |= (Z_L(1) << (bit & (GEAR_MM_BITSET_LEN-1)));
}

static gear_always_inline void gear_mm_bitset_reset_bit(gear_mm_bitset *bitset, int bit)
{
	bitset[bit / GEAR_MM_BITSET_LEN] &= ~(Z_L(1) << (bit & (GEAR_MM_BITSET_LEN-1)));
}

static gear_always_inline void gear_mm_bitset_set_range(gear_mm_bitset *bitset, int start, int len)
{
	if (len == 1) {
		gear_mm_bitset_set_bit(bitset, start);
	} else {
		int pos = start / GEAR_MM_BITSET_LEN;
		int end = (start + len - 1) / GEAR_MM_BITSET_LEN;
		int bit = start & (GEAR_MM_BITSET_LEN - 1);
		gear_mm_bitset tmp;

		if (pos != end) {
			/* set bits from "bit" to GEAR_MM_BITSET_LEN-1 */
			tmp = (gear_mm_bitset)-1 << bit;
			bitset[pos++] |= tmp;
			while (pos != end) {
				/* set all bits */
				bitset[pos++] = (gear_mm_bitset)-1;
			}
			end = (start + len - 1) & (GEAR_MM_BITSET_LEN - 1);
			/* set bits from "0" to "end" */
			tmp = (gear_mm_bitset)-1 >> ((GEAR_MM_BITSET_LEN - 1) - end);
			bitset[pos] |= tmp;
		} else {
			end = (start + len - 1) & (GEAR_MM_BITSET_LEN - 1);
			/* set bits from "bit" to "end" */
			tmp = (gear_mm_bitset)-1 << bit;
			tmp &= (gear_mm_bitset)-1 >> ((GEAR_MM_BITSET_LEN - 1) - end);
			bitset[pos] |= tmp;
		}
	}
}

static gear_always_inline void gear_mm_bitset_reset_range(gear_mm_bitset *bitset, int start, int len)
{
	if (len == 1) {
		gear_mm_bitset_reset_bit(bitset, start);
	} else {
		int pos = start / GEAR_MM_BITSET_LEN;
		int end = (start + len - 1) / GEAR_MM_BITSET_LEN;
		int bit = start & (GEAR_MM_BITSET_LEN - 1);
		gear_mm_bitset tmp;

		if (pos != end) {
			/* reset bits from "bit" to GEAR_MM_BITSET_LEN-1 */
			tmp = ~((Z_L(1) << bit) - 1);
			bitset[pos++] &= ~tmp;
			while (pos != end) {
				/* set all bits */
				bitset[pos++] = 0;
			}
			end = (start + len - 1) & (GEAR_MM_BITSET_LEN - 1);
			/* reset bits from "0" to "end" */
			tmp = (gear_mm_bitset)-1 >> ((GEAR_MM_BITSET_LEN - 1) - end);
			bitset[pos] &= ~tmp;
		} else {
			end = (start + len - 1) & (GEAR_MM_BITSET_LEN - 1);
			/* reset bits from "bit" to "end" */
			tmp = (gear_mm_bitset)-1 << bit;
			tmp &= (gear_mm_bitset)-1 >> ((GEAR_MM_BITSET_LEN - 1) - end);
			bitset[pos] &= ~tmp;
		}
	}
}

static gear_always_inline int gear_mm_bitset_is_free_range(gear_mm_bitset *bitset, int start, int len)
{
	if (len == 1) {
		return !gear_mm_bitset_is_set(bitset, start);
	} else {
		int pos = start / GEAR_MM_BITSET_LEN;
		int end = (start + len - 1) / GEAR_MM_BITSET_LEN;
		int bit = start & (GEAR_MM_BITSET_LEN - 1);
		gear_mm_bitset tmp;

		if (pos != end) {
			/* set bits from "bit" to GEAR_MM_BITSET_LEN-1 */
			tmp = (gear_mm_bitset)-1 << bit;
			if ((bitset[pos++] & tmp) != 0) {
				return 0;
			}
			while (pos != end) {
				/* set all bits */
				if (bitset[pos++] != 0) {
					return 0;
				}
			}
			end = (start + len - 1) & (GEAR_MM_BITSET_LEN - 1);
			/* set bits from "0" to "end" */
			tmp = (gear_mm_bitset)-1 >> ((GEAR_MM_BITSET_LEN - 1) - end);
			return (bitset[pos] & tmp) == 0;
		} else {
			end = (start + len - 1) & (GEAR_MM_BITSET_LEN - 1);
			/* set bits from "bit" to "end" */
			tmp = (gear_mm_bitset)-1 << bit;
			tmp &= (gear_mm_bitset)-1 >> ((GEAR_MM_BITSET_LEN - 1) - end);
			return (bitset[pos] & tmp) == 0;
		}
	}
}

/**********/
/* Chunks */
/**********/

static void *gear_mm_chunk_alloc_int(size_t size, size_t alignment)
{
	void *ptr = gear_mm_mmap(size);

	if (ptr == NULL) {
		return NULL;
	} else if (GEAR_MM_ALIGNED_OFFSET(ptr, alignment) == 0) {
#ifdef MADV_HUGEPAGE
		if (gear_mm_use_huge_pages) {
			madvise(ptr, size, MADV_HUGEPAGE);
		}
#endif
		return ptr;
	} else {
		size_t offset;

		/* chunk has to be aligned */
		gear_mm_munmap(ptr, size);
		ptr = gear_mm_mmap(size + alignment - REAL_PAGE_SIZE);
#ifdef _WIN32
		offset = GEAR_MM_ALIGNED_OFFSET(ptr, alignment);
		gear_mm_munmap(ptr, size + alignment - REAL_PAGE_SIZE);
		ptr = gear_mm_mmap_fixed((void*)((char*)ptr + (alignment - offset)), size);
		offset = GEAR_MM_ALIGNED_OFFSET(ptr, alignment);
		if (offset != 0) {
			gear_mm_munmap(ptr, size);
			return NULL;
		}
		return ptr;
#else
		offset = GEAR_MM_ALIGNED_OFFSET(ptr, alignment);
		if (offset != 0) {
			offset = alignment - offset;
			gear_mm_munmap(ptr, offset);
			ptr = (char*)ptr + offset;
			alignment -= offset;
		}
		if (alignment > REAL_PAGE_SIZE) {
			gear_mm_munmap((char*)ptr + size, alignment - REAL_PAGE_SIZE);
		}
# ifdef MADV_HUGEPAGE
		if (gear_mm_use_huge_pages) {
			madvise(ptr, size, MADV_HUGEPAGE);
		}
# endif
#endif
		return ptr;
	}
}

static void *gear_mm_chunk_alloc(gear_mm_heap *heap, size_t size, size_t alignment)
{
#if GEAR_MM_STORAGE
	if (UNEXPECTED(heap->storage)) {
		void *ptr = heap->storage->handlers.chunk_alloc(heap->storage, size, alignment);
		GEAR_ASSERT(((gear_uintptr_t)((char*)ptr + (alignment-1)) & (alignment-1)) == (gear_uintptr_t)ptr);
		return ptr;
	}
#endif
	return gear_mm_chunk_alloc_int(size, alignment);
}

static void gear_mm_chunk_free(gear_mm_heap *heap, void *addr, size_t size)
{
#if GEAR_MM_STORAGE
	if (UNEXPECTED(heap->storage)) {
		heap->storage->handlers.chunk_free(heap->storage, addr, size);
		return;
	}
#endif
	gear_mm_munmap(addr, size);
}

static int gear_mm_chunk_truncate(gear_mm_heap *heap, void *addr, size_t old_size, size_t new_size)
{
#if GEAR_MM_STORAGE
	if (UNEXPECTED(heap->storage)) {
		if (heap->storage->handlers.chunk_truncate) {
			return heap->storage->handlers.chunk_truncate(heap->storage, addr, old_size, new_size);
		} else {
			return 0;
		}
	}
#endif
#ifndef _WIN32
	gear_mm_munmap((char*)addr + new_size, old_size - new_size);
	return 1;
#else
	return 0;
#endif
}

static int gear_mm_chunk_extend(gear_mm_heap *heap, void *addr, size_t old_size, size_t new_size)
{
#if GEAR_MM_STORAGE
	if (UNEXPECTED(heap->storage)) {
		if (heap->storage->handlers.chunk_extend) {
			return heap->storage->handlers.chunk_extend(heap->storage, addr, old_size, new_size);
		} else {
			return 0;
		}
	}
#endif
#ifndef _WIN32
	return (gear_mm_mmap_fixed((char*)addr + old_size, new_size - old_size) != NULL);
#else
	return 0;
#endif
}

static gear_always_inline void gear_mm_chunk_init(gear_mm_heap *heap, gear_mm_chunk *chunk)
{
	chunk->heap = heap;
	chunk->next = heap->main_chunk;
	chunk->prev = heap->main_chunk->prev;
	chunk->prev->next = chunk;
	chunk->next->prev = chunk;
	/* mark first pages as allocated */
	chunk->free_pages = GEAR_MM_PAGES - GEAR_MM_FIRST_PAGE;
	chunk->free_tail = GEAR_MM_FIRST_PAGE;
	/* the younger chunks have bigger number */
	chunk->num = chunk->prev->num + 1;
	/* mark first pages as allocated */
	chunk->free_map[0] = (1L << GEAR_MM_FIRST_PAGE) - 1;
	chunk->map[0] = GEAR_MM_LRUN(GEAR_MM_FIRST_PAGE);
}

/***********************/
/* Huge Runs (forward) */
/***********************/

static size_t gear_mm_get_huge_block_size(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
static void *gear_mm_alloc_huge(gear_mm_heap *heap, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
static void gear_mm_free_huge(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);

#if GEAR_DEBUG
static void gear_mm_change_huge_block_size(gear_mm_heap *heap, void *ptr, size_t size, size_t dbg_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
#else
static void gear_mm_change_huge_block_size(gear_mm_heap *heap, void *ptr, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
#endif

/**************/
/* Large Runs */
/**************/

#if GEAR_DEBUG
static void *gear_mm_alloc_pages(gear_mm_heap *heap, uint32_t pages_count, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
#else
static void *gear_mm_alloc_pages(gear_mm_heap *heap, uint32_t pages_count GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
#endif
{
	gear_mm_chunk *chunk = heap->main_chunk;
	uint32_t page_num, len;
	int steps = 0;

	while (1) {
		if (UNEXPECTED(chunk->free_pages < pages_count)) {
			goto not_found;
#if 0
		} else if (UNEXPECTED(chunk->free_pages + chunk->free_tail == GEAR_MM_PAGES)) {
			if (UNEXPECTED(GEAR_MM_PAGES - chunk->free_tail < pages_count)) {
				goto not_found;
			} else {
				page_num = chunk->free_tail;
				goto found;
			}
		} else if (0) {
			/* First-Fit Search */
			int free_tail = chunk->free_tail;
			gear_mm_bitset *bitset = chunk->free_map;
			gear_mm_bitset tmp = *(bitset++);
			int i = 0;

			while (1) {
				/* skip allocated blocks */
				while (tmp == (gear_mm_bitset)-1) {
					i += GEAR_MM_BITSET_LEN;
					if (i == GEAR_MM_PAGES) {
						goto not_found;
					}
					tmp = *(bitset++);
				}
				/* find first 0 bit */
				page_num = i + gear_mm_bitset_nts(tmp);
				/* reset bits from 0 to "bit" */
				tmp &= tmp + 1;
				/* skip free blocks */
				while (tmp == 0) {
					i += GEAR_MM_BITSET_LEN;
					len = i - page_num;
					if (len >= pages_count) {
						goto found;
					} else if (i >= free_tail) {
						goto not_found;
					}
					tmp = *(bitset++);
				}
				/* find first 1 bit */
				len = (i + gear_ulong_ntz(tmp)) - page_num;
				if (len >= pages_count) {
					goto found;
				}
				/* set bits from 0 to "bit" */
				tmp |= tmp - 1;
			}
#endif
		} else {
			/* Best-Fit Search */
			int best = -1;
			uint32_t best_len = GEAR_MM_PAGES;
			uint32_t free_tail = chunk->free_tail;
			gear_mm_bitset *bitset = chunk->free_map;
			gear_mm_bitset tmp = *(bitset++);
			uint32_t i = 0;

			while (1) {
				/* skip allocated blocks */
				while (tmp == (gear_mm_bitset)-1) {
					i += GEAR_MM_BITSET_LEN;
					if (i == GEAR_MM_PAGES) {
						if (best > 0) {
							page_num = best;
							goto found;
						} else {
							goto not_found;
						}
					}
					tmp = *(bitset++);
				}
				/* find first 0 bit */
				page_num = i + gear_mm_bitset_nts(tmp);
				/* reset bits from 0 to "bit" */
				tmp &= tmp + 1;
				/* skip free blocks */
				while (tmp == 0) {
					i += GEAR_MM_BITSET_LEN;
					if (i >= free_tail || i == GEAR_MM_PAGES) {
						len = GEAR_MM_PAGES - page_num;
						if (len >= pages_count && len < best_len) {
							chunk->free_tail = page_num + pages_count;
							goto found;
						} else {
							/* set accurate value */
							chunk->free_tail = page_num;
							if (best > 0) {
								page_num = best;
								goto found;
							} else {
								goto not_found;
							}
						}
					}
					tmp = *(bitset++);
				}
				/* find first 1 bit */
				len = i + gear_ulong_ntz(tmp) - page_num;
				if (len >= pages_count) {
					if (len == pages_count) {
						goto found;
					} else if (len < best_len) {
						best_len = len;
						best = page_num;
					}
				}
				/* set bits from 0 to "bit" */
				tmp |= tmp - 1;
			}
		}

not_found:
		if (chunk->next == heap->main_chunk) {
get_chunk:
			if (heap->cached_chunks) {
				heap->cached_chunks_count--;
				chunk = heap->cached_chunks;
				heap->cached_chunks = chunk->next;
			} else {
#if GEAR_MM_LIMIT
				if (UNEXPECTED(heap->real_size + GEAR_MM_CHUNK_SIZE > heap->limit)) {
					if (gear_mm_gc(heap)) {
						goto get_chunk;
					} else if (heap->overflow == 0) {
#if GEAR_DEBUG
						gear_mm_safe_error(heap, "Allowed memory size of %zu bytes exhausted at %s:%d (tried to allocate %zu bytes)", heap->limit, __gear_filename, __gear_lineno, size);
#else
						gear_mm_safe_error(heap, "Allowed memory size of %zu bytes exhausted (tried to allocate %zu bytes)", heap->limit, GEAR_MM_PAGE_SIZE * pages_count);
#endif
						return NULL;
					}
				}
#endif
				chunk = (gear_mm_chunk*)gear_mm_chunk_alloc(heap, GEAR_MM_CHUNK_SIZE, GEAR_MM_CHUNK_SIZE);
				if (UNEXPECTED(chunk == NULL)) {
					/* insufficient memory */
					if (gear_mm_gc(heap) &&
					    (chunk = (gear_mm_chunk*)gear_mm_chunk_alloc(heap, GEAR_MM_CHUNK_SIZE, GEAR_MM_CHUNK_SIZE)) != NULL) {
						/* pass */
					} else {
#if !GEAR_MM_LIMIT
						gear_mm_safe_error(heap, "Out of memory");
#elif GEAR_DEBUG
						gear_mm_safe_error(heap, "Out of memory (allocated %zu) at %s:%d (tried to allocate %zu bytes)", heap->real_size, __gear_filename, __gear_lineno, size);
#else
						gear_mm_safe_error(heap, "Out of memory (allocated %zu) (tried to allocate %zu bytes)", heap->real_size, GEAR_MM_PAGE_SIZE * pages_count);
#endif
						return NULL;
					}
				}
#if GEAR_MM_STAT
				do {
					size_t size = heap->real_size + GEAR_MM_CHUNK_SIZE;
					size_t peak = MAX(heap->real_peak, size);
					heap->real_size = size;
					heap->real_peak = peak;
				} while (0);
#elif GEAR_MM_LIMIT
				heap->real_size += GEAR_MM_CHUNK_SIZE;

#endif
			}
			heap->chunks_count++;
			if (heap->chunks_count > heap->peak_chunks_count) {
				heap->peak_chunks_count = heap->chunks_count;
			}
			gear_mm_chunk_init(heap, chunk);
			page_num = GEAR_MM_FIRST_PAGE;
			len = GEAR_MM_PAGES - GEAR_MM_FIRST_PAGE;
			goto found;
		} else {
			chunk = chunk->next;
			steps++;
		}
	}

found:
	if (steps > 2 && pages_count < 8) {
		/* move chunk into the head of the linked-list */
		chunk->prev->next = chunk->next;
		chunk->next->prev = chunk->prev;
		chunk->next = heap->main_chunk->next;
		chunk->prev = heap->main_chunk;
		chunk->prev->next = chunk;
		chunk->next->prev = chunk;
	}
	/* mark run as allocated */
	chunk->free_pages -= pages_count;
	gear_mm_bitset_set_range(chunk->free_map, page_num, pages_count);
	chunk->map[page_num] = GEAR_MM_LRUN(pages_count);
	if (page_num == chunk->free_tail) {
		chunk->free_tail = page_num + pages_count;
	}
	return GEAR_MM_PAGE_ADDR(chunk, page_num);
}

static gear_always_inline void *gear_mm_alloc_large_ex(gear_mm_heap *heap, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	int pages_count = (int)GEAR_MM_SIZE_TO_NUM(size, GEAR_MM_PAGE_SIZE);
#if GEAR_DEBUG
	void *ptr = gear_mm_alloc_pages(heap, pages_count, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#else
	void *ptr = gear_mm_alloc_pages(heap, pages_count GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#endif
#if GEAR_MM_STAT
	do {
		size_t size = heap->size + pages_count * GEAR_MM_PAGE_SIZE;
		size_t peak = MAX(heap->peak, size);
		heap->size = size;
		heap->peak = peak;
	} while (0);
#endif
	return ptr;
}

#if GEAR_DEBUG
static gear_never_inline void *gear_mm_alloc_large(gear_mm_heap *heap, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	return gear_mm_alloc_large_ex(heap, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}
#else
static gear_never_inline void *gear_mm_alloc_large(gear_mm_heap *heap, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	return gear_mm_alloc_large_ex(heap, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}
#endif

static gear_always_inline void gear_mm_delete_chunk(gear_mm_heap *heap, gear_mm_chunk *chunk)
{
	chunk->next->prev = chunk->prev;
	chunk->prev->next = chunk->next;
	heap->chunks_count--;
	if (heap->chunks_count + heap->cached_chunks_count < heap->avg_chunks_count + 0.1
	 || (heap->chunks_count == heap->last_chunks_delete_boundary
	  && heap->last_chunks_delete_count >= 4)) {
		/* delay deletion */
		heap->cached_chunks_count++;
		chunk->next = heap->cached_chunks;
		heap->cached_chunks = chunk;
	} else {
#if GEAR_MM_STAT || GEAR_MM_LIMIT
		heap->real_size -= GEAR_MM_CHUNK_SIZE;
#endif
		if (!heap->cached_chunks) {
			if (heap->chunks_count != heap->last_chunks_delete_boundary) {
				heap->last_chunks_delete_boundary = heap->chunks_count;
				heap->last_chunks_delete_count = 0;
			} else {
				heap->last_chunks_delete_count++;
			}
		}
		if (!heap->cached_chunks || chunk->num > heap->cached_chunks->num) {
			gear_mm_chunk_free(heap, chunk, GEAR_MM_CHUNK_SIZE);
		} else {
//TODO: select the best chunk to delete???
			chunk->next = heap->cached_chunks->next;
			gear_mm_chunk_free(heap, heap->cached_chunks, GEAR_MM_CHUNK_SIZE);
			heap->cached_chunks = chunk;
		}
	}
}

static gear_always_inline void gear_mm_free_pages_ex(gear_mm_heap *heap, gear_mm_chunk *chunk, uint32_t page_num, uint32_t pages_count, int free_chunk)
{
	chunk->free_pages += pages_count;
	gear_mm_bitset_reset_range(chunk->free_map, page_num, pages_count);
	chunk->map[page_num] = 0;
	if (chunk->free_tail == page_num + pages_count) {
		/* this setting may be not accurate */
		chunk->free_tail = page_num;
	}
	if (free_chunk && chunk->free_pages == GEAR_MM_PAGES - GEAR_MM_FIRST_PAGE) {
		gear_mm_delete_chunk(heap, chunk);
	}
}

static gear_never_inline void gear_mm_free_pages(gear_mm_heap *heap, gear_mm_chunk *chunk, int page_num, int pages_count)
{
	gear_mm_free_pages_ex(heap, chunk, page_num, pages_count, 1);
}

static gear_always_inline void gear_mm_free_large(gear_mm_heap *heap, gear_mm_chunk *chunk, int page_num, int pages_count)
{
#if GEAR_MM_STAT
	heap->size -= pages_count * GEAR_MM_PAGE_SIZE;
#endif
	gear_mm_free_pages(heap, chunk, page_num, pages_count);
}

/**************/
/* Small Runs */
/**************/

/* higher set bit number (0->N/A, 1->1, 2->2, 4->3, 8->4, 127->7, 128->8 etc) */
static gear_always_inline int gear_mm_small_size_to_bit(int size)
{
#if (defined(__GNUC__) || __has_builtin(__builtin_clz))  && defined(HYSS_HAVE_BUILTIN_CLZ)
	return (__builtin_clz(size) ^ 0x1f) + 1;
#elif defined(_WIN32)
	unsigned long index;

	if (!BitScanReverse(&index, (unsigned long)size)) {
		/* undefined behavior */
		return 64;
	}

	return (((31 - (int)index) ^ 0x1f) + 1);
#else
	int n = 16;
	if (size <= 0x00ff) {n -= 8; size = size << 8;}
	if (size <= 0x0fff) {n -= 4; size = size << 4;}
	if (size <= 0x3fff) {n -= 2; size = size << 2;}
	if (size <= 0x7fff) {n -= 1;}
	return n;
#endif
}

#ifndef MAX
# define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
# define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

static gear_always_inline int gear_mm_small_size_to_bin(size_t size)
{
#if 0
	int n;
                            /*0,  1,  2,  3,  4,  5,  6,  7,  8,  9  10, 11, 12*/
	static const int f1[] = { 3,  3,  3,  3,  3,  3,  3,  4,  5,  6,  7,  8,  9};
	static const int f2[] = { 0,  0,  0,  0,  0,  0,  0,  4,  8, 12, 16, 20, 24};

	if (UNEXPECTED(size <= 2)) return 0;
	n = gear_mm_small_size_to_bit(size - 1);
	return ((size-1) >> f1[n]) + f2[n];
#else
	unsigned int t1, t2;

	if (size <= 64) {
		/* we need to support size == 0 ... */
		return (size - !!size) >> 3;
	} else {
		t1 = size - 1;
		t2 = gear_mm_small_size_to_bit(t1) - 3;
		t1 = t1 >> t2;
		t2 = t2 - 3;
		t2 = t2 << 2;
		return (int)(t1 + t2);
	}
#endif
}

#define GEAR_MM_SMALL_SIZE_TO_BIN(size)  gear_mm_small_size_to_bin(size)

static gear_never_inline void *gear_mm_alloc_small_slow(gear_mm_heap *heap, uint32_t bin_num GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
    gear_mm_chunk *chunk;
    int page_num;
	gear_mm_bin *bin;
	gear_mm_free_slot *p, *end;

#if GEAR_DEBUG
	bin = (gear_mm_bin*)gear_mm_alloc_pages(heap, bin_pages[bin_num], bin_data_size[bin_num] GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#else
	bin = (gear_mm_bin*)gear_mm_alloc_pages(heap, bin_pages[bin_num] GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#endif
	if (UNEXPECTED(bin == NULL)) {
		/* insufficient memory */
		return NULL;
	}

	chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(bin, GEAR_MM_CHUNK_SIZE);
	page_num = GEAR_MM_ALIGNED_OFFSET(bin, GEAR_MM_CHUNK_SIZE) / GEAR_MM_PAGE_SIZE;
	chunk->map[page_num] = GEAR_MM_SRUN(bin_num);
	if (bin_pages[bin_num] > 1) {
		uint32_t i = 1;

		do {
			chunk->map[page_num+i] = GEAR_MM_NRUN(bin_num, i);
			i++;
		} while (i < bin_pages[bin_num]);
	}

	/* create a linked list of elements from 1 to last */
	end = (gear_mm_free_slot*)((char*)bin + (bin_data_size[bin_num] * (bin_elements[bin_num] - 1)));
	heap->free_slot[bin_num] = p = (gear_mm_free_slot*)((char*)bin + bin_data_size[bin_num]);
	do {
		p->next_free_slot = (gear_mm_free_slot*)((char*)p + bin_data_size[bin_num]);
#if GEAR_DEBUG
		do {
			gear_mm_debug_info *dbg = (gear_mm_debug_info*)((char*)p + bin_data_size[bin_num] - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));
			dbg->size = 0;
		} while (0);
#endif
		p = (gear_mm_free_slot*)((char*)p + bin_data_size[bin_num]);
	} while (p != end);

	/* terminate list using NULL */
	p->next_free_slot = NULL;
#if GEAR_DEBUG
		do {
			gear_mm_debug_info *dbg = (gear_mm_debug_info*)((char*)p + bin_data_size[bin_num] - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));
			dbg->size = 0;
		} while (0);
#endif

	/* return first element */
	return (char*)bin;
}

static gear_always_inline void *gear_mm_alloc_small(gear_mm_heap *heap, size_t size, int bin_num GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
#if GEAR_MM_STAT
	do {
		size_t size = heap->size + bin_data_size[bin_num];
		size_t peak = MAX(heap->peak, size);
		heap->size = size;
		heap->peak = peak;
	} while (0);
#endif

	if (EXPECTED(heap->free_slot[bin_num] != NULL)) {
		gear_mm_free_slot *p = heap->free_slot[bin_num];
		heap->free_slot[bin_num] = p->next_free_slot;
		return (void*)p;
	} else {
		return gear_mm_alloc_small_slow(heap, bin_num GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
	}
}

static gear_always_inline void gear_mm_free_small(gear_mm_heap *heap, void *ptr, int bin_num)
{
	gear_mm_free_slot *p;

#if GEAR_MM_STAT
	heap->size -= bin_data_size[bin_num];
#endif

#if GEAR_DEBUG
	do {
		gear_mm_debug_info *dbg = (gear_mm_debug_info*)((char*)ptr + bin_data_size[bin_num] - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));
		dbg->size = 0;
	} while (0);
#endif

    p = (gear_mm_free_slot*)ptr;
    p->next_free_slot = heap->free_slot[bin_num];
    heap->free_slot[bin_num] = p;
}

/********/
/* Heap */
/********/

#if GEAR_DEBUG
static gear_always_inline gear_mm_debug_info *gear_mm_get_debug_info(gear_mm_heap *heap, void *ptr)
{
	size_t page_offset = GEAR_MM_ALIGNED_OFFSET(ptr, GEAR_MM_CHUNK_SIZE);
	gear_mm_chunk *chunk;
	int page_num;
	gear_mm_page_info info;

	GEAR_MM_CHECK(page_offset != 0, "gear_mm_heap corrupted");
	chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(ptr, GEAR_MM_CHUNK_SIZE);
	page_num = (int)(page_offset / GEAR_MM_PAGE_SIZE);
	info = chunk->map[page_num];
	GEAR_MM_CHECK(chunk->heap == heap, "gear_mm_heap corrupted");
	if (EXPECTED(info & GEAR_MM_IS_SRUN)) {
		int bin_num = GEAR_MM_SRUN_BIN_NUM(info);
		return (gear_mm_debug_info*)((char*)ptr + bin_data_size[bin_num] - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));
	} else /* if (info & GEAR_MM_IS_LRUN) */ {
		int pages_count = GEAR_MM_LRUN_PAGES(info);

		return (gear_mm_debug_info*)((char*)ptr + GEAR_MM_PAGE_SIZE * pages_count - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));
	}
}
#endif

static gear_always_inline void *gear_mm_alloc_heap(gear_mm_heap *heap, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	void *ptr;
#if GEAR_DEBUG
	size_t real_size = size;
	gear_mm_debug_info *dbg;

	/* special handling for zero-size allocation */
	size = MAX(size, 1);
	size = GEAR_MM_ALIGNED_SIZE(size) + GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info));
	if (UNEXPECTED(size < real_size)) {
		gear_error_noreturn(E_ERROR, "Possible integer overflow in memory allocation (%zu + %zu)", GEAR_MM_ALIGNED_SIZE(real_size), GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));
		return NULL;
	}
#endif
	if (EXPECTED(size <= GEAR_MM_MAX_SMALL_SIZE)) {
		ptr = gear_mm_alloc_small(heap, size, GEAR_MM_SMALL_SIZE_TO_BIN(size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#if GEAR_DEBUG
		dbg = gear_mm_get_debug_info(heap, ptr);
		dbg->size = real_size;
		dbg->filename = __gear_filename;
		dbg->orig_filename = __gear_orig_filename;
		dbg->lineno = __gear_lineno;
		dbg->orig_lineno = __gear_orig_lineno;
#endif
		return ptr;
	} else if (EXPECTED(size <= GEAR_MM_MAX_LARGE_SIZE)) {
		ptr = gear_mm_alloc_large(heap, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#if GEAR_DEBUG
		dbg = gear_mm_get_debug_info(heap, ptr);
		dbg->size = real_size;
		dbg->filename = __gear_filename;
		dbg->orig_filename = __gear_orig_filename;
		dbg->lineno = __gear_lineno;
		dbg->orig_lineno = __gear_orig_lineno;
#endif
		return ptr;
	} else {
#if GEAR_DEBUG
		size = real_size;
#endif
		return gear_mm_alloc_huge(heap, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
	}
}

static gear_always_inline void gear_mm_free_heap(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	size_t page_offset = GEAR_MM_ALIGNED_OFFSET(ptr, GEAR_MM_CHUNK_SIZE);

	if (UNEXPECTED(page_offset == 0)) {
		if (ptr != NULL) {
			gear_mm_free_huge(heap, ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
		}
	} else {
		gear_mm_chunk *chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(ptr, GEAR_MM_CHUNK_SIZE);
		int page_num = (int)(page_offset / GEAR_MM_PAGE_SIZE);
		gear_mm_page_info info = chunk->map[page_num];

		GEAR_MM_CHECK(chunk->heap == heap, "gear_mm_heap corrupted");
		if (EXPECTED(info & GEAR_MM_IS_SRUN)) {
			gear_mm_free_small(heap, ptr, GEAR_MM_SRUN_BIN_NUM(info));
		} else /* if (info & GEAR_MM_IS_LRUN) */ {
			int pages_count = GEAR_MM_LRUN_PAGES(info);

			GEAR_MM_CHECK(GEAR_MM_ALIGNED_OFFSET(page_offset, GEAR_MM_PAGE_SIZE) == 0, "gear_mm_heap corrupted");
			gear_mm_free_large(heap, chunk, page_num, pages_count);
		}
	}
}

static size_t gear_mm_size(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	size_t page_offset = GEAR_MM_ALIGNED_OFFSET(ptr, GEAR_MM_CHUNK_SIZE);

	if (UNEXPECTED(page_offset == 0)) {
		return gear_mm_get_huge_block_size(heap, ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
	} else {
		gear_mm_chunk *chunk;
#if 0 && GEAR_DEBUG
		gear_mm_debug_info *dbg = gear_mm_get_debug_info(heap, ptr);
		return dbg->size;
#else
		int page_num;
		gear_mm_page_info info;

		chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(ptr, GEAR_MM_CHUNK_SIZE);
		page_num = (int)(page_offset / GEAR_MM_PAGE_SIZE);
		info = chunk->map[page_num];
		GEAR_MM_CHECK(chunk->heap == heap, "gear_mm_heap corrupted");
		if (EXPECTED(info & GEAR_MM_IS_SRUN)) {
			return bin_data_size[GEAR_MM_SRUN_BIN_NUM(info)];
		} else /* if (info & GEAR_MM_IS_LARGE_RUN) */ {
			return GEAR_MM_LRUN_PAGES(info) * GEAR_MM_PAGE_SIZE;
		}
#endif
	}
}

static gear_never_inline void *gear_mm_realloc_slow(gear_mm_heap *heap, void *ptr, size_t size, size_t copy_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	void *ret;

#if GEAR_MM_STAT
	do {
		size_t orig_peak = heap->peak;
		size_t orig_real_peak = heap->real_peak;
#endif
		ret = gear_mm_alloc_heap(heap, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
		memcpy(ret, ptr, copy_size);
		gear_mm_free_heap(heap, ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#if GEAR_MM_STAT
		heap->peak = MAX(orig_peak, heap->size);
		heap->real_peak = MAX(orig_real_peak, heap->real_size);
	} while (0);
#endif
	return ret;
}

static gear_never_inline void *gear_mm_realloc_huge(gear_mm_heap *heap, void *ptr, size_t size, size_t copy_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	size_t old_size;
	size_t new_size;
#if GEAR_DEBUG
	size_t real_size;
#endif

	old_size = gear_mm_get_huge_block_size(heap, ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#if GEAR_DEBUG
	real_size = size;
	size = GEAR_MM_ALIGNED_SIZE(size) + GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info));
#endif
	if (size > GEAR_MM_MAX_LARGE_SIZE) {
#if GEAR_DEBUG
		size = real_size;
#endif
#ifdef GEAR_WIN32
		/* On Windows we don't have ability to extend huge blocks in-place.
		 * We allocate them with 2MB size granularity, to avoid many
		 * reallocations when they are extended by small pieces
		 */
		new_size = GEAR_MM_ALIGNED_SIZE_EX(size, MAX(REAL_PAGE_SIZE, GEAR_MM_CHUNK_SIZE));
#else
		new_size = GEAR_MM_ALIGNED_SIZE_EX(size, REAL_PAGE_SIZE);
#endif
		if (new_size == old_size) {
#if GEAR_DEBUG
			gear_mm_change_huge_block_size(heap, ptr, new_size, real_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#else
			gear_mm_change_huge_block_size(heap, ptr, new_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#endif
			return ptr;
		} else if (new_size < old_size) {
			/* unmup tail */
			if (gear_mm_chunk_truncate(heap, ptr, old_size, new_size)) {
#if GEAR_MM_STAT || GEAR_MM_LIMIT
				heap->real_size -= old_size - new_size;
#endif
#if GEAR_MM_STAT
				heap->size -= old_size - new_size;
#endif
#if GEAR_DEBUG
				gear_mm_change_huge_block_size(heap, ptr, new_size, real_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#else
				gear_mm_change_huge_block_size(heap, ptr, new_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#endif
				return ptr;
			}
		} else /* if (new_size > old_size) */ {
#if GEAR_MM_LIMIT
			if (UNEXPECTED(heap->real_size + (new_size - old_size) > heap->limit)) {
				if (gear_mm_gc(heap) && heap->real_size + (new_size - old_size) <= heap->limit) {
					/* pass */
				} else if (heap->overflow == 0) {
#if GEAR_DEBUG
					gear_mm_safe_error(heap, "Allowed memory size of %zu bytes exhausted at %s:%d (tried to allocate %zu bytes)", heap->limit, __gear_filename, __gear_lineno, size);
#else
					gear_mm_safe_error(heap, "Allowed memory size of %zu bytes exhausted (tried to allocate %zu bytes)", heap->limit, size);
#endif
					return NULL;
				}
			}
#endif
			/* try to map tail right after this block */
			if (gear_mm_chunk_extend(heap, ptr, old_size, new_size)) {
#if GEAR_MM_STAT || GEAR_MM_LIMIT
				heap->real_size += new_size - old_size;
#endif
#if GEAR_MM_STAT
				heap->real_peak = MAX(heap->real_peak, heap->real_size);
				heap->size += new_size - old_size;
				heap->peak = MAX(heap->peak, heap->size);
#endif
#if GEAR_DEBUG
				gear_mm_change_huge_block_size(heap, ptr, new_size, real_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#else
				gear_mm_change_huge_block_size(heap, ptr, new_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#endif
				return ptr;
			}
		}
	}

	return gear_mm_realloc_slow(heap, ptr, size, MIN(old_size, copy_size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

static gear_always_inline void *gear_mm_realloc_heap(gear_mm_heap *heap, void *ptr, size_t size, gear_bool use_copy_size, size_t copy_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	size_t page_offset;
	size_t old_size;
	size_t new_size;
	void *ret;
#if GEAR_DEBUG
	gear_mm_debug_info *dbg;
#endif

	page_offset = GEAR_MM_ALIGNED_OFFSET(ptr, GEAR_MM_CHUNK_SIZE);
	if (UNEXPECTED(page_offset == 0)) {
		if (EXPECTED(ptr == NULL)) {
			return _gear_mm_alloc(heap, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
		} else {
			return gear_mm_realloc_huge(heap, ptr, size, copy_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
		}
	} else {
		gear_mm_chunk *chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(ptr, GEAR_MM_CHUNK_SIZE);
		int page_num = (int)(page_offset / GEAR_MM_PAGE_SIZE);
		gear_mm_page_info info = chunk->map[page_num];
#if GEAR_DEBUG
		size_t real_size = size;

		size = GEAR_MM_ALIGNED_SIZE(size) + GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info));
#endif

		GEAR_MM_CHECK(chunk->heap == heap, "gear_mm_heap corrupted");
		if (info & GEAR_MM_IS_SRUN) {
			int old_bin_num = GEAR_MM_SRUN_BIN_NUM(info);

			do {
				old_size = bin_data_size[old_bin_num];

				/* Check if requested size fits into current bin */
				if (size <= old_size) {
					/* Check if truncation is necessary */
					if (old_bin_num > 0 && size < bin_data_size[old_bin_num - 1]) {
						/* truncation */
						ret = gear_mm_alloc_small(heap, size, GEAR_MM_SMALL_SIZE_TO_BIN(size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
						copy_size = use_copy_size ? MIN(size, copy_size) : size;
						memcpy(ret, ptr, copy_size);
						gear_mm_free_small(heap, ptr, old_bin_num);
					} else {
						/* reallocation in-place */
						ret = ptr;
					}
				} else if (size <= GEAR_MM_MAX_SMALL_SIZE) {
					/* small extension */

#if GEAR_MM_STAT
					do {
						size_t orig_peak = heap->peak;
						size_t orig_real_peak = heap->real_peak;
#endif
						ret = gear_mm_alloc_small(heap, size, GEAR_MM_SMALL_SIZE_TO_BIN(size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
						copy_size = use_copy_size ? MIN(old_size, copy_size) : old_size;
						memcpy(ret, ptr, copy_size);
						gear_mm_free_small(heap, ptr, old_bin_num);
#if GEAR_MM_STAT
						heap->peak = MAX(orig_peak, heap->size);
						heap->real_peak = MAX(orig_real_peak, heap->real_size);
					} while (0);
#endif
				} else {
					/* slow reallocation */
					break;
				}

#if GEAR_DEBUG
				dbg = gear_mm_get_debug_info(heap, ret);
				dbg->size = real_size;
				dbg->filename = __gear_filename;
				dbg->orig_filename = __gear_orig_filename;
				dbg->lineno = __gear_lineno;
				dbg->orig_lineno = __gear_orig_lineno;
#endif
				return ret;
			}  while (0);

		} else /* if (info & GEAR_MM_IS_LARGE_RUN) */ {
			GEAR_MM_CHECK(GEAR_MM_ALIGNED_OFFSET(page_offset, GEAR_MM_PAGE_SIZE) == 0, "gear_mm_heap corrupted");
			old_size = GEAR_MM_LRUN_PAGES(info) * GEAR_MM_PAGE_SIZE;
			if (size > GEAR_MM_MAX_SMALL_SIZE && size <= GEAR_MM_MAX_LARGE_SIZE) {
				new_size = GEAR_MM_ALIGNED_SIZE_EX(size, GEAR_MM_PAGE_SIZE);
				if (new_size == old_size) {
#if GEAR_DEBUG
					dbg = gear_mm_get_debug_info(heap, ptr);
					dbg->size = real_size;
					dbg->filename = __gear_filename;
					dbg->orig_filename = __gear_orig_filename;
					dbg->lineno = __gear_lineno;
					dbg->orig_lineno = __gear_orig_lineno;
#endif
					return ptr;
				} else if (new_size < old_size) {
					/* free tail pages */
					int new_pages_count = (int)(new_size / GEAR_MM_PAGE_SIZE);
					int rest_pages_count = (int)((old_size - new_size) / GEAR_MM_PAGE_SIZE);

#if GEAR_MM_STAT
					heap->size -= rest_pages_count * GEAR_MM_PAGE_SIZE;
#endif
					chunk->map[page_num] = GEAR_MM_LRUN(new_pages_count);
					chunk->free_pages += rest_pages_count;
					gear_mm_bitset_reset_range(chunk->free_map, page_num + new_pages_count, rest_pages_count);
#if GEAR_DEBUG
					dbg = gear_mm_get_debug_info(heap, ptr);
					dbg->size = real_size;
					dbg->filename = __gear_filename;
					dbg->orig_filename = __gear_orig_filename;
					dbg->lineno = __gear_lineno;
					dbg->orig_lineno = __gear_orig_lineno;
#endif
					return ptr;
				} else /* if (new_size > old_size) */ {
					int new_pages_count = (int)(new_size / GEAR_MM_PAGE_SIZE);
					int old_pages_count = (int)(old_size / GEAR_MM_PAGE_SIZE);

					/* try to allocate tail pages after this block */
					if (page_num + new_pages_count <= GEAR_MM_PAGES &&
					    gear_mm_bitset_is_free_range(chunk->free_map, page_num + old_pages_count, new_pages_count - old_pages_count)) {
#if GEAR_MM_STAT
						do {
							size_t size = heap->size + (new_size - old_size);
							size_t peak = MAX(heap->peak, size);
							heap->size = size;
							heap->peak = peak;
						} while (0);
#endif
						chunk->free_pages -= new_pages_count - old_pages_count;
						gear_mm_bitset_set_range(chunk->free_map, page_num + old_pages_count, new_pages_count - old_pages_count);
						chunk->map[page_num] = GEAR_MM_LRUN(new_pages_count);
#if GEAR_DEBUG
						dbg = gear_mm_get_debug_info(heap, ptr);
						dbg->size = real_size;
						dbg->filename = __gear_filename;
						dbg->orig_filename = __gear_orig_filename;
						dbg->lineno = __gear_lineno;
						dbg->orig_lineno = __gear_orig_lineno;
#endif
						return ptr;
					}
				}
			}
		}
#if GEAR_DEBUG
		size = real_size;
#endif
	}

	copy_size = MIN(old_size, copy_size);
	return gear_mm_realloc_slow(heap, ptr, size, copy_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

/*********************/
/* Huge Runs (again) */
/*********************/

#if GEAR_DEBUG
static void gear_mm_add_huge_block(gear_mm_heap *heap, void *ptr, size_t size, size_t dbg_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
#else
static void gear_mm_add_huge_block(gear_mm_heap *heap, void *ptr, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
#endif
{
	gear_mm_huge_list *list = (gear_mm_huge_list*)gear_mm_alloc_heap(heap, sizeof(gear_mm_huge_list) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
	list->ptr = ptr;
	list->size = size;
	list->next = heap->huge_list;
#if GEAR_DEBUG
	list->dbg.size = dbg_size;
	list->dbg.filename = __gear_filename;
	list->dbg.orig_filename = __gear_orig_filename;
	list->dbg.lineno = __gear_lineno;
	list->dbg.orig_lineno = __gear_orig_lineno;
#endif
	heap->huge_list = list;
}

static size_t gear_mm_del_huge_block(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	gear_mm_huge_list *prev = NULL;
	gear_mm_huge_list *list = heap->huge_list;
	while (list != NULL) {
		if (list->ptr == ptr) {
			size_t size;

			if (prev) {
				prev->next = list->next;
			} else {
				heap->huge_list = list->next;
			}
			size = list->size;
			gear_mm_free_heap(heap, list GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
			return size;
		}
		prev = list;
		list = list->next;
	}
	GEAR_MM_CHECK(0, "gear_mm_heap corrupted");
	return 0;
}

static size_t gear_mm_get_huge_block_size(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	gear_mm_huge_list *list = heap->huge_list;
	while (list != NULL) {
		if (list->ptr == ptr) {
			return list->size;
		}
		list = list->next;
	}
	GEAR_MM_CHECK(0, "gear_mm_heap corrupted");
	return 0;
}

#if GEAR_DEBUG
static void gear_mm_change_huge_block_size(gear_mm_heap *heap, void *ptr, size_t size, size_t dbg_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
#else
static void gear_mm_change_huge_block_size(gear_mm_heap *heap, void *ptr, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
#endif
{
	gear_mm_huge_list *list = heap->huge_list;
	while (list != NULL) {
		if (list->ptr == ptr) {
			list->size = size;
#if GEAR_DEBUG
			list->dbg.size = dbg_size;
			list->dbg.filename = __gear_filename;
			list->dbg.orig_filename = __gear_orig_filename;
			list->dbg.lineno = __gear_lineno;
			list->dbg.orig_lineno = __gear_orig_lineno;
#endif
			return;
		}
		list = list->next;
	}
}

static void *gear_mm_alloc_huge(gear_mm_heap *heap, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
#ifdef GEAR_WIN32
	/* On Windows we don't have ability to extend huge blocks in-place.
	 * We allocate them with 2MB size granularity, to avoid many
	 * reallocations when they are extended by small pieces
	 */
	size_t new_size = GEAR_MM_ALIGNED_SIZE_EX(size, MAX(REAL_PAGE_SIZE, GEAR_MM_CHUNK_SIZE));
#else
	size_t new_size = GEAR_MM_ALIGNED_SIZE_EX(size, REAL_PAGE_SIZE);
#endif
	void *ptr;

#if GEAR_MM_LIMIT
	if (UNEXPECTED(heap->real_size + new_size > heap->limit)) {
		if (gear_mm_gc(heap) && heap->real_size + new_size <= heap->limit) {
			/* pass */
		} else if (heap->overflow == 0) {
#if GEAR_DEBUG
			gear_mm_safe_error(heap, "Allowed memory size of %zu bytes exhausted at %s:%d (tried to allocate %zu bytes)", heap->limit, __gear_filename, __gear_lineno, size);
#else
			gear_mm_safe_error(heap, "Allowed memory size of %zu bytes exhausted (tried to allocate %zu bytes)", heap->limit, size);
#endif
			return NULL;
		}
	}
#endif
	ptr = gear_mm_chunk_alloc(heap, new_size, GEAR_MM_CHUNK_SIZE);
	if (UNEXPECTED(ptr == NULL)) {
		/* insufficient memory */
		if (gear_mm_gc(heap) &&
		    (ptr = gear_mm_chunk_alloc(heap, new_size, GEAR_MM_CHUNK_SIZE)) != NULL) {
			/* pass */
		} else {
#if !GEAR_MM_LIMIT
			gear_mm_safe_error(heap, "Out of memory");
#elif GEAR_DEBUG
			gear_mm_safe_error(heap, "Out of memory (allocated %zu) at %s:%d (tried to allocate %zu bytes)", heap->real_size, __gear_filename, __gear_lineno, size);
#else
			gear_mm_safe_error(heap, "Out of memory (allocated %zu) (tried to allocate %zu bytes)", heap->real_size, size);
#endif
			return NULL;
		}
	}
#if GEAR_DEBUG
	gear_mm_add_huge_block(heap, ptr, new_size, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#else
	gear_mm_add_huge_block(heap, ptr, new_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
#endif
#if GEAR_MM_STAT
	do {
		size_t size = heap->real_size + new_size;
		size_t peak = MAX(heap->real_peak, size);
		heap->real_size = size;
		heap->real_peak = peak;
	} while (0);
	do {
		size_t size = heap->size + new_size;
		size_t peak = MAX(heap->peak, size);
		heap->size = size;
		heap->peak = peak;
	} while (0);
#elif GEAR_MM_LIMIT
	heap->real_size += new_size;
#endif
	return ptr;
}

static void gear_mm_free_huge(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	size_t size;

	GEAR_MM_CHECK(GEAR_MM_ALIGNED_OFFSET(ptr, GEAR_MM_CHUNK_SIZE) == 0, "gear_mm_heap corrupted");
	size = gear_mm_del_huge_block(heap, ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
	gear_mm_chunk_free(heap, ptr, size);
#if GEAR_MM_STAT || GEAR_MM_LIMIT
	heap->real_size -= size;
#endif
#if GEAR_MM_STAT
	heap->size -= size;
#endif
}

/******************/
/* Initialization */
/******************/

static gear_mm_heap *gear_mm_init(void)
{
	gear_mm_chunk *chunk = (gear_mm_chunk*)gear_mm_chunk_alloc_int(GEAR_MM_CHUNK_SIZE, GEAR_MM_CHUNK_SIZE);
	gear_mm_heap *heap;

	if (UNEXPECTED(chunk == NULL)) {
#if GEAR_MM_ERROR
#ifdef _WIN32
		stderr_last_error("Can't initialize heap");
#else
		fprintf(stderr, "\nCan't initialize heap: [%d] %s\n", errno, strerror(errno));
#endif
#endif
		return NULL;
	}
	heap = &chunk->heap_slot;
	chunk->heap = heap;
	chunk->next = chunk;
	chunk->prev = chunk;
	chunk->free_pages = GEAR_MM_PAGES - GEAR_MM_FIRST_PAGE;
	chunk->free_tail = GEAR_MM_FIRST_PAGE;
	chunk->num = 0;
	chunk->free_map[0] = (Z_L(1) << GEAR_MM_FIRST_PAGE) - 1;
	chunk->map[0] = GEAR_MM_LRUN(GEAR_MM_FIRST_PAGE);
	heap->main_chunk = chunk;
	heap->cached_chunks = NULL;
	heap->chunks_count = 1;
	heap->peak_chunks_count = 1;
	heap->cached_chunks_count = 0;
	heap->avg_chunks_count = 1.0;
	heap->last_chunks_delete_boundary = 0;
	heap->last_chunks_delete_count = 0;
#if GEAR_MM_STAT || GEAR_MM_LIMIT
	heap->real_size = GEAR_MM_CHUNK_SIZE;
#endif
#if GEAR_MM_STAT
	heap->real_peak = GEAR_MM_CHUNK_SIZE;
	heap->size = 0;
	heap->peak = 0;
#endif
#if GEAR_MM_LIMIT
	heap->limit = ((size_t)Z_L(-1) >> (size_t)Z_L(1));
	heap->overflow = 0;
#endif
#if GEAR_MM_CUSTOM
	heap->use_custom_heap = GEAR_MM_CUSTOM_HEAP_NONE;
#endif
#if GEAR_MM_STORAGE
	heap->storage = NULL;
#endif
	heap->huge_list = NULL;
	return heap;
}

GEAR_API size_t gear_mm_gc(gear_mm_heap *heap)
{
	gear_mm_free_slot *p, **q;
	gear_mm_chunk *chunk;
	size_t page_offset;
	int page_num;
	gear_mm_page_info info;
	uint32_t i, free_counter;
	int has_free_pages;
	size_t collected = 0;

#if GEAR_MM_CUSTOM
	if (heap->use_custom_heap) {
		return 0;
	}
#endif

	for (i = 0; i < GEAR_MM_BINS; i++) {
		has_free_pages = 0;
		p = heap->free_slot[i];
		while (p != NULL) {
			chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(p, GEAR_MM_CHUNK_SIZE);
			GEAR_MM_CHECK(chunk->heap == heap, "gear_mm_heap corrupted");
			page_offset = GEAR_MM_ALIGNED_OFFSET(p, GEAR_MM_CHUNK_SIZE);
			GEAR_ASSERT(page_offset != 0);
			page_num = (int)(page_offset / GEAR_MM_PAGE_SIZE);
			info = chunk->map[page_num];
			GEAR_ASSERT(info & GEAR_MM_IS_SRUN);
			if (info & GEAR_MM_IS_LRUN) {
				page_num -= GEAR_MM_NRUN_OFFSET(info);
				info = chunk->map[page_num];
				GEAR_ASSERT(info & GEAR_MM_IS_SRUN);
				GEAR_ASSERT(!(info & GEAR_MM_IS_LRUN));
			}
			GEAR_ASSERT(GEAR_MM_SRUN_BIN_NUM(info) == i);
			free_counter = GEAR_MM_SRUN_FREE_COUNTER(info) + 1;
			if (free_counter == bin_elements[i]) {
				has_free_pages = 1;
			}
			chunk->map[page_num] = GEAR_MM_SRUN_EX(i, free_counter);
			p = p->next_free_slot;
		}

		if (!has_free_pages) {
			continue;
		}

		q = &heap->free_slot[i];
		p = *q;
		while (p != NULL) {
			chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(p, GEAR_MM_CHUNK_SIZE);
			GEAR_MM_CHECK(chunk->heap == heap, "gear_mm_heap corrupted");
			page_offset = GEAR_MM_ALIGNED_OFFSET(p, GEAR_MM_CHUNK_SIZE);
			GEAR_ASSERT(page_offset != 0);
			page_num = (int)(page_offset / GEAR_MM_PAGE_SIZE);
			info = chunk->map[page_num];
			GEAR_ASSERT(info & GEAR_MM_IS_SRUN);
			if (info & GEAR_MM_IS_LRUN) {
				page_num -= GEAR_MM_NRUN_OFFSET(info);
				info = chunk->map[page_num];
				GEAR_ASSERT(info & GEAR_MM_IS_SRUN);
				GEAR_ASSERT(!(info & GEAR_MM_IS_LRUN));
			}
			GEAR_ASSERT(GEAR_MM_SRUN_BIN_NUM(info) == i);
			if (GEAR_MM_SRUN_FREE_COUNTER(info) == bin_elements[i]) {
				/* remove from cache */
				p = p->next_free_slot;
				*q = p;
			} else {
				q = &p->next_free_slot;
				p = *q;
			}
		}
	}

	chunk = heap->main_chunk;
	do {
		i = GEAR_MM_FIRST_PAGE;
		while (i < chunk->free_tail) {
			if (gear_mm_bitset_is_set(chunk->free_map, i)) {
				info = chunk->map[i];
				if (info & GEAR_MM_IS_SRUN) {
					int bin_num = GEAR_MM_SRUN_BIN_NUM(info);
					int pages_count = bin_pages[bin_num];

					if (GEAR_MM_SRUN_FREE_COUNTER(info) == bin_elements[bin_num]) {
						/* all elemens are free */
						gear_mm_free_pages_ex(heap, chunk, i, pages_count, 0);
						collected += pages_count;
					} else {
						/* reset counter */
						chunk->map[i] = GEAR_MM_SRUN(bin_num);
					}
					i += bin_pages[bin_num];
				} else /* if (info & GEAR_MM_IS_LRUN) */ {
					i += GEAR_MM_LRUN_PAGES(info);
				}
			} else {
				i++;
			}
		}
		if (chunk->free_pages == GEAR_MM_PAGES - GEAR_MM_FIRST_PAGE) {
			gear_mm_chunk *next_chunk = chunk->next;

			gear_mm_delete_chunk(heap, chunk);
			chunk = next_chunk;
		} else {
			chunk = chunk->next;
		}
	} while (chunk != heap->main_chunk);

	return collected * GEAR_MM_PAGE_SIZE;
}

#if GEAR_DEBUG
/******************/
/* Leak detection */
/******************/

static gear_long gear_mm_find_leaks_small(gear_mm_chunk *p, uint32_t i, uint32_t j, gear_leak_info *leak)
{
    int empty = 1;
	gear_long count = 0;
	int bin_num = GEAR_MM_SRUN_BIN_NUM(p->map[i]);
	gear_mm_debug_info *dbg = (gear_mm_debug_info*)((char*)p + GEAR_MM_PAGE_SIZE * i + bin_data_size[bin_num] * (j + 1) - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));

	while (j < bin_elements[bin_num]) {
		if (dbg->size != 0) {
			if (dbg->filename == leak->filename && dbg->lineno == leak->lineno) {
				count++;
				dbg->size = 0;
				dbg->filename = NULL;
				dbg->lineno = 0;
			} else {
				empty = 0;
			}
		}
		j++;
		dbg = (gear_mm_debug_info*)((char*)dbg + bin_data_size[bin_num]);
	}
	if (empty) {
		gear_mm_bitset_reset_range(p->free_map, i, bin_pages[bin_num]);
	}
	return count;
}

static gear_long gear_mm_find_leaks(gear_mm_heap *heap, gear_mm_chunk *p, uint32_t i, gear_leak_info *leak)
{
	gear_long count = 0;

	do {
		while (i < p->free_tail) {
			if (gear_mm_bitset_is_set(p->free_map, i)) {
				if (p->map[i] & GEAR_MM_IS_SRUN) {
					int bin_num = GEAR_MM_SRUN_BIN_NUM(p->map[i]);
					count += gear_mm_find_leaks_small(p, i, 0, leak);
					i += bin_pages[bin_num];
				} else /* if (p->map[i] & GEAR_MM_IS_LRUN) */ {
					int pages_count = GEAR_MM_LRUN_PAGES(p->map[i]);
					gear_mm_debug_info *dbg = (gear_mm_debug_info*)((char*)p + GEAR_MM_PAGE_SIZE * (i + pages_count) - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));

					if (dbg->filename == leak->filename && dbg->lineno == leak->lineno) {
						count++;
					}
					gear_mm_bitset_reset_range(p->free_map, i, pages_count);
					i += pages_count;
				}
			} else {
				i++;
			}
		}
		p = p->next;
	} while (p != heap->main_chunk);
	return count;
}

static gear_long gear_mm_find_leaks_huge(gear_mm_heap *heap, gear_mm_huge_list *list)
{
	gear_long count = 0;
	gear_mm_huge_list *prev = list;
	gear_mm_huge_list *p = list->next;

	while (p) {
		if (p->dbg.filename == list->dbg.filename && p->dbg.lineno == list->dbg.lineno) {
			prev->next = p->next;
			gear_mm_chunk_free(heap, p->ptr, p->size);
			gear_mm_free_heap(heap, p, NULL, 0, NULL, 0);
			count++;
		} else {
			prev = p;
		}
		p = prev->next;
	}

	return count;
}

static void gear_mm_check_leaks(gear_mm_heap *heap)
{
	gear_mm_huge_list *list;
	gear_mm_chunk *p;
	gear_leak_info leak;
	gear_long repeated = 0;
	uint32_t total = 0;
	uint32_t i, j;

	/* find leaked huge blocks and free them */
	list = heap->huge_list;
	while (list) {
		gear_mm_huge_list *q = list;

		leak.addr = list->ptr;
		leak.size = list->dbg.size;
		leak.filename = list->dbg.filename;
		leak.orig_filename = list->dbg.orig_filename;
		leak.lineno = list->dbg.lineno;
		leak.orig_lineno = list->dbg.orig_lineno;

		gear_message_dispatcher(ZMSG_LOG_SCRIPT_NAME, NULL);
		gear_message_dispatcher(ZMSG_MEMORY_LEAK_DETECTED, &leak);
		repeated = gear_mm_find_leaks_huge(heap, list);
		total += 1 + repeated;
		if (repeated) {
			gear_message_dispatcher(ZMSG_MEMORY_LEAK_REPEATED, (void *)(gear_uintptr_t)repeated);
		}

		heap->huge_list = list = list->next;
		gear_mm_chunk_free(heap, q->ptr, q->size);
		gear_mm_free_heap(heap, q, NULL, 0, NULL, 0);
	}

	/* for each chunk */
	p = heap->main_chunk;
	do {
		i = GEAR_MM_FIRST_PAGE;
		while (i < p->free_tail) {
			if (gear_mm_bitset_is_set(p->free_map, i)) {
				if (p->map[i] & GEAR_MM_IS_SRUN) {
					int bin_num = GEAR_MM_SRUN_BIN_NUM(p->map[i]);
					gear_mm_debug_info *dbg = (gear_mm_debug_info*)((char*)p + GEAR_MM_PAGE_SIZE * i + bin_data_size[bin_num] - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));

					j = 0;
					while (j < bin_elements[bin_num]) {
						if (dbg->size != 0) {
							leak.addr = (gear_mm_debug_info*)((char*)p + GEAR_MM_PAGE_SIZE * i + bin_data_size[bin_num] * j);
							leak.size = dbg->size;
							leak.filename = dbg->filename;
							leak.orig_filename = dbg->orig_filename;
							leak.lineno = dbg->lineno;
							leak.orig_lineno = dbg->orig_lineno;

							gear_message_dispatcher(ZMSG_LOG_SCRIPT_NAME, NULL);
							gear_message_dispatcher(ZMSG_MEMORY_LEAK_DETECTED, &leak);

							dbg->size = 0;
							dbg->filename = NULL;
							dbg->lineno = 0;

							repeated = gear_mm_find_leaks_small(p, i, j + 1, &leak) +
							           gear_mm_find_leaks(heap, p, i + bin_pages[bin_num], &leak);
							total += 1 + repeated;
							if (repeated) {
								gear_message_dispatcher(ZMSG_MEMORY_LEAK_REPEATED, (void *)(gear_uintptr_t)repeated);
							}
						}
						dbg = (gear_mm_debug_info*)((char*)dbg + bin_data_size[bin_num]);
						j++;
					}
					i += bin_pages[bin_num];
				} else /* if (p->map[i] & GEAR_MM_IS_LRUN) */ {
					int pages_count = GEAR_MM_LRUN_PAGES(p->map[i]);
					gear_mm_debug_info *dbg = (gear_mm_debug_info*)((char*)p + GEAR_MM_PAGE_SIZE * (i + pages_count) - GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info)));

					leak.addr = (void*)((char*)p + GEAR_MM_PAGE_SIZE * i);
					leak.size = dbg->size;
					leak.filename = dbg->filename;
					leak.orig_filename = dbg->orig_filename;
					leak.lineno = dbg->lineno;
					leak.orig_lineno = dbg->orig_lineno;

					gear_message_dispatcher(ZMSG_LOG_SCRIPT_NAME, NULL);
					gear_message_dispatcher(ZMSG_MEMORY_LEAK_DETECTED, &leak);

					gear_mm_bitset_reset_range(p->free_map, i, pages_count);

					repeated = gear_mm_find_leaks(heap, p, i + pages_count, &leak);
					total += 1 + repeated;
					if (repeated) {
						gear_message_dispatcher(ZMSG_MEMORY_LEAK_REPEATED, (void *)(gear_uintptr_t)repeated);
					}
					i += pages_count;
				}
			} else {
				i++;
			}
		}
		p = p->next;
	} while (p != heap->main_chunk);
	if (total) {
		gear_message_dispatcher(ZMSG_MEMORY_LEAKS_GRAND_TOTAL, &total);
	}
}
#endif

void gear_mm_shutdown(gear_mm_heap *heap, int full, int silent)
{
	gear_mm_chunk *p;
	gear_mm_huge_list *list;

#if GEAR_MM_CUSTOM
	if (heap->use_custom_heap) {
		if (full) {
			if (GEAR_DEBUG && heap->use_custom_heap == GEAR_MM_CUSTOM_HEAP_DEBUG) {
				heap->custom_heap.debug._free(heap GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC);
			} else {
				heap->custom_heap.std._free(heap);
			}
		}
		return;
	}
#endif

#if GEAR_DEBUG
	if (!silent) {
		gear_mm_check_leaks(heap);
	}
#endif

	/* free huge blocks */
	list = heap->huge_list;
	heap->huge_list = NULL;
	while (list) {
		gear_mm_huge_list *q = list;
		list = list->next;
		gear_mm_chunk_free(heap, q->ptr, q->size);
	}

	/* move all chunks except of the first one into the cache */
	p = heap->main_chunk->next;
	while (p != heap->main_chunk) {
		gear_mm_chunk *q = p->next;
		p->next = heap->cached_chunks;
		heap->cached_chunks = p;
		p = q;
		heap->chunks_count--;
		heap->cached_chunks_count++;
	}

	if (full) {
		/* free all cached chunks */
		while (heap->cached_chunks) {
			p = heap->cached_chunks;
			heap->cached_chunks = p->next;
			gear_mm_chunk_free(heap, p, GEAR_MM_CHUNK_SIZE);
		}
		/* free the first chunk */
		gear_mm_chunk_free(heap, heap->main_chunk, GEAR_MM_CHUNK_SIZE);
	} else {
		gear_mm_heap old_heap;

		/* free some cached chunks to keep average count */
		heap->avg_chunks_count = (heap->avg_chunks_count + (double)heap->peak_chunks_count) / 2.0;
		while ((double)heap->cached_chunks_count + 0.9 > heap->avg_chunks_count &&
		       heap->cached_chunks) {
			p = heap->cached_chunks;
			heap->cached_chunks = p->next;
			gear_mm_chunk_free(heap, p, GEAR_MM_CHUNK_SIZE);
			heap->cached_chunks_count--;
		}
		/* clear cached chunks */
		p = heap->cached_chunks;
		while (p != NULL) {
			gear_mm_chunk *q = p->next;
			memset(p, 0, sizeof(gear_mm_chunk));
			p->next = q;
			p = q;
		}

		/* reinitialize the first chunk and heap */
		old_heap = *heap;
		p = heap->main_chunk;
		memset(p, 0, GEAR_MM_FIRST_PAGE * GEAR_MM_PAGE_SIZE);
		*heap = old_heap;
		memset(heap->free_slot, 0, sizeof(heap->free_slot));
		heap->main_chunk = p;
		p->heap = &p->heap_slot;
		p->next = p;
		p->prev = p;
		p->free_pages = GEAR_MM_PAGES - GEAR_MM_FIRST_PAGE;
		p->free_tail = GEAR_MM_FIRST_PAGE;
		p->free_map[0] = (1L << GEAR_MM_FIRST_PAGE) - 1;
		p->map[0] = GEAR_MM_LRUN(GEAR_MM_FIRST_PAGE);
		heap->chunks_count = 1;
		heap->peak_chunks_count = 1;
		heap->last_chunks_delete_boundary = 0;
		heap->last_chunks_delete_count = 0;
#if GEAR_MM_STAT || GEAR_MM_LIMIT
		heap->real_size = GEAR_MM_CHUNK_SIZE;
#endif
#if GEAR_MM_STAT
		heap->real_peak = GEAR_MM_CHUNK_SIZE;
		heap->size = heap->peak = 0;
#endif
	}
}

/**************/
/* PUBLIC API */
/**************/

GEAR_API void* GEAR_FASTCALL _gear_mm_alloc(gear_mm_heap *heap, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	return gear_mm_alloc_heap(heap, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

GEAR_API void GEAR_FASTCALL _gear_mm_free(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	gear_mm_free_heap(heap, ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

void* GEAR_FASTCALL _gear_mm_realloc(gear_mm_heap *heap, void *ptr, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	return gear_mm_realloc_heap(heap, ptr, size, 0, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

void* GEAR_FASTCALL _gear_mm_realloc2(gear_mm_heap *heap, void *ptr, size_t size, size_t copy_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	return gear_mm_realloc_heap(heap, ptr, size, 1, copy_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

GEAR_API size_t GEAR_FASTCALL _gear_mm_block_size(gear_mm_heap *heap, void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	return gear_mm_size(heap, ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

/**********************/
/* Allocation Manager */
/**********************/

typedef struct _gear_alloc_globals {
	gear_mm_heap *mm_heap;
} gear_alloc_globals;

#ifdef ZTS
static int alloc_globals_id;
# define AG(v) GEAR_PBCG(alloc_globals_id, gear_alloc_globals *, v)
#else
# define AG(v) (alloc_globals.v)
static gear_alloc_globals alloc_globals;
#endif

GEAR_API int is_gear_mm(void)
{
#if GEAR_MM_CUSTOM
	return !AG(mm_heap)->use_custom_heap;
#else
	return 1;
#endif
}

#if !GEAR_DEBUG && defined(HAVE_BUILTIN_CONSTANT_P)
#undef _emalloc

#if GEAR_MM_CUSTOM
# define GEAR_MM_CUSTOM_ALLOCATOR(size) do { \
		if (UNEXPECTED(AG(mm_heap)->use_custom_heap)) { \
			if (GEAR_DEBUG && AG(mm_heap)->use_custom_heap == GEAR_MM_CUSTOM_HEAP_DEBUG) { \
				return AG(mm_heap)->custom_heap.debug._malloc(size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC); \
			} else { \
				return AG(mm_heap)->custom_heap.std._malloc(size); \
			} \
		} \
	} while (0)
# define GEAR_MM_CUSTOM_DEALLOCATOR(ptr) do { \
		if (UNEXPECTED(AG(mm_heap)->use_custom_heap)) { \
			if (GEAR_DEBUG && AG(mm_heap)->use_custom_heap == GEAR_MM_CUSTOM_HEAP_DEBUG) { \
				AG(mm_heap)->custom_heap.debug._free(ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC); \
			} else { \
				AG(mm_heap)->custom_heap.std._free(ptr); \
			} \
			return; \
		} \
	} while (0)
#else
# define GEAR_MM_CUSTOM_ALLOCATOR(size)
# define GEAR_MM_CUSTOM_DEALLOCATOR(ptr)
#endif

# define _GEAR_BIN_ALLOCATOR(_num, _size, _elements, _pages, x, y) \
	GEAR_API void* GEAR_FASTCALL _emalloc_ ## _size(void) { \
		GEAR_MM_CUSTOM_ALLOCATOR(_size); \
		return gear_mm_alloc_small(AG(mm_heap), _size, _num GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC); \
	}

GEAR_MM_BINS_INFO(_GEAR_BIN_ALLOCATOR, x, y)

GEAR_API void* GEAR_FASTCALL _emalloc_large(size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	GEAR_MM_CUSTOM_ALLOCATOR(size);
	return gear_mm_alloc_large_ex(AG(mm_heap), size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

GEAR_API void* GEAR_FASTCALL _emalloc_huge(size_t size)
{
	GEAR_MM_CUSTOM_ALLOCATOR(size);
	return gear_mm_alloc_huge(AG(mm_heap), size);
}

#if GEAR_DEBUG
# define _GEAR_BIN_FREE(_num, _size, _elements, _pages, x, y) \
	GEAR_API void GEAR_FASTCALL _efree_ ## _size(void *ptr) { \
		GEAR_MM_CUSTOM_DEALLOCATOR(ptr); \
		{ \
			size_t page_offset = GEAR_MM_ALIGNED_OFFSET(ptr, GEAR_MM_CHUNK_SIZE); \
			gear_mm_chunk *chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(ptr, GEAR_MM_CHUNK_SIZE); \
			int page_num = page_offset / GEAR_MM_PAGE_SIZE; \
			GEAR_MM_CHECK(chunk->heap == AG(mm_heap), "gear_mm_heap corrupted"); \
			GEAR_ASSERT(chunk->map[page_num] & GEAR_MM_IS_SRUN); \
			GEAR_ASSERT(GEAR_MM_SRUN_BIN_NUM(chunk->map[page_num]) == _num); \
			gear_mm_free_small(AG(mm_heap), ptr, _num); \
		} \
	}
#else
# define _GEAR_BIN_FREE(_num, _size, _elements, _pages, x, y) \
	GEAR_API void GEAR_FASTCALL _efree_ ## _size(void *ptr) { \
		GEAR_MM_CUSTOM_DEALLOCATOR(ptr); \
		{ \
			gear_mm_chunk *chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(ptr, GEAR_MM_CHUNK_SIZE); \
			GEAR_MM_CHECK(chunk->heap == AG(mm_heap), "gear_mm_heap corrupted"); \
			gear_mm_free_small(AG(mm_heap), ptr, _num); \
		} \
	}
#endif

GEAR_MM_BINS_INFO(_GEAR_BIN_FREE, x, y)

GEAR_API void GEAR_FASTCALL _efree_large(void *ptr, size_t size)
{
	GEAR_MM_CUSTOM_DEALLOCATOR(ptr);
	{
		size_t page_offset = GEAR_MM_ALIGNED_OFFSET(ptr, GEAR_MM_CHUNK_SIZE);
		gear_mm_chunk *chunk = (gear_mm_chunk*)GEAR_MM_ALIGNED_BASE(ptr, GEAR_MM_CHUNK_SIZE);
		int page_num = page_offset / GEAR_MM_PAGE_SIZE;
		uint32_t pages_count = GEAR_MM_ALIGNED_SIZE_EX(size, GEAR_MM_PAGE_SIZE) / GEAR_MM_PAGE_SIZE;

		GEAR_MM_CHECK(chunk->heap == AG(mm_heap) && GEAR_MM_ALIGNED_OFFSET(page_offset, GEAR_MM_PAGE_SIZE) == 0, "gear_mm_heap corrupted");
		GEAR_ASSERT(chunk->map[page_num] & GEAR_MM_IS_LRUN);
		GEAR_ASSERT(GEAR_MM_LRUN_PAGES(chunk->map[page_num]) == pages_count);
		gear_mm_free_large(AG(mm_heap), chunk, page_num, pages_count);
	}
}

GEAR_API void GEAR_FASTCALL _efree_huge(void *ptr, size_t size)
{

	GEAR_MM_CUSTOM_DEALLOCATOR(ptr);
	gear_mm_free_huge(AG(mm_heap), ptr);
}
#endif

GEAR_API void* GEAR_FASTCALL _emalloc(size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
#if GEAR_MM_CUSTOM
	if (UNEXPECTED(AG(mm_heap)->use_custom_heap)) {
		if (GEAR_DEBUG && AG(mm_heap)->use_custom_heap == GEAR_MM_CUSTOM_HEAP_DEBUG) {
			return AG(mm_heap)->custom_heap.debug._malloc(size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
		} else {
			return AG(mm_heap)->custom_heap.std._malloc(size);
		}
	}
#endif
	return gear_mm_alloc_heap(AG(mm_heap), size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

GEAR_API void GEAR_FASTCALL _efree(void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
#if GEAR_MM_CUSTOM
	if (UNEXPECTED(AG(mm_heap)->use_custom_heap)) {
		if (GEAR_DEBUG && AG(mm_heap)->use_custom_heap == GEAR_MM_CUSTOM_HEAP_DEBUG) {
			AG(mm_heap)->custom_heap.debug._free(ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
		} else {
			AG(mm_heap)->custom_heap.std._free(ptr);
	    }
		return;
	}
#endif
	gear_mm_free_heap(AG(mm_heap), ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

GEAR_API void* GEAR_FASTCALL _erealloc(void *ptr, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
#if GEAR_MM_CUSTOM
	if (UNEXPECTED(AG(mm_heap)->use_custom_heap)) {
		if (GEAR_DEBUG && AG(mm_heap)->use_custom_heap == GEAR_MM_CUSTOM_HEAP_DEBUG) {
			return AG(mm_heap)->custom_heap.debug._realloc(ptr, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
		} else {
			return AG(mm_heap)->custom_heap.std._realloc(ptr, size);
		}
	}
#endif
	return gear_mm_realloc_heap(AG(mm_heap), ptr, size, 0, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

GEAR_API void* GEAR_FASTCALL _erealloc2(void *ptr, size_t size, size_t copy_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
#if GEAR_MM_CUSTOM
	if (UNEXPECTED(AG(mm_heap)->use_custom_heap)) {
		if (GEAR_DEBUG && AG(mm_heap)->use_custom_heap == GEAR_MM_CUSTOM_HEAP_DEBUG) {
			return AG(mm_heap)->custom_heap.debug._realloc(ptr, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
		} else {
			return AG(mm_heap)->custom_heap.std._realloc(ptr, size);
		}
	}
#endif
	return gear_mm_realloc_heap(AG(mm_heap), ptr, size, 1, copy_size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

GEAR_API size_t GEAR_FASTCALL _gear_mem_block_size(void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
#if GEAR_MM_CUSTOM
	if (UNEXPECTED(AG(mm_heap)->use_custom_heap)) {
		return 0;
	}
#endif
	return gear_mm_size(AG(mm_heap), ptr GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
}

GEAR_API void* GEAR_FASTCALL _safe_emalloc(size_t nmemb, size_t size, size_t offset GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	return emalloc_rel(gear_safe_address_guarded(nmemb, size, offset));
}

GEAR_API void* GEAR_FASTCALL _safe_malloc(size_t nmemb, size_t size, size_t offset)
{
	return pemalloc(gear_safe_address_guarded(nmemb, size, offset), 1);
}

GEAR_API void* GEAR_FASTCALL _safe_erealloc(void *ptr, size_t nmemb, size_t size, size_t offset GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	return erealloc_rel(ptr, gear_safe_address_guarded(nmemb, size, offset));
}

GEAR_API void* GEAR_FASTCALL _safe_realloc(void *ptr, size_t nmemb, size_t size, size_t offset)
{
	return perealloc(ptr, gear_safe_address_guarded(nmemb, size, offset), 1);
}

GEAR_API void* GEAR_FASTCALL _ecalloc(size_t nmemb, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	void *p;

	size = gear_safe_address_guarded(nmemb, size, 0);
	p = emalloc_rel(size);
	memset(p, 0, size);
	return p;
}

GEAR_API char* GEAR_FASTCALL _estrdup(const char *s GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	size_t length;
	char *p;

	length = strlen(s);
	if (UNEXPECTED(length + 1 == 0)) {
		gear_error_noreturn(E_ERROR, "Possible integer overflow in memory allocation (1 * %zu + 1)", length);
	}
	p = (char *) _emalloc(length + 1 GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
	memcpy(p, s, length+1);
	return p;
}

GEAR_API char* GEAR_FASTCALL _estrndup(const char *s, size_t length GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC)
{
	char *p;

	if (UNEXPECTED(length + 1 == 0)) {
		gear_error_noreturn(E_ERROR, "Possible integer overflow in memory allocation (1 * %zu + 1)", length);
	}
	p = (char *) _emalloc(length + 1 GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
	memcpy(p, s, length);
	p[length] = 0;
	return p;
}


GEAR_API char* GEAR_FASTCALL gear_strndup(const char *s, size_t length)
{
	char *p;

	if (UNEXPECTED(length + 1 == 0)) {
		gear_error_noreturn(E_ERROR, "Possible integer overflow in memory allocation (1 * %zu + 1)", length);
	}
	p = (char *) malloc(length + 1);
	if (UNEXPECTED(p == NULL)) {
		return p;
	}
	if (EXPECTED(length)) {
		memcpy(p, s, length);
	}
	p[length] = 0;
	return p;
}


GEAR_API int gear_set_memory_limit(size_t memory_limit)
{
#if GEAR_MM_LIMIT
	AG(mm_heap)->limit = (memory_limit >= GEAR_MM_CHUNK_SIZE) ? memory_limit : GEAR_MM_CHUNK_SIZE;
#endif
	return SUCCESS;
}

GEAR_API size_t gear_memory_usage(int real_usage)
{
#if GEAR_MM_STAT
	if (real_usage) {
		return AG(mm_heap)->real_size;
	} else {
		size_t usage = AG(mm_heap)->size;
		return usage;
	}
#endif
	return 0;
}

GEAR_API size_t gear_memory_peak_usage(int real_usage)
{
#if GEAR_MM_STAT
	if (real_usage) {
		return AG(mm_heap)->real_peak;
	} else {
		return AG(mm_heap)->peak;
	}
#endif
	return 0;
}

GEAR_API void shutdown_memory_manager(int silent, int full_shutdown)
{
	gear_mm_shutdown(AG(mm_heap), full_shutdown, silent);
}

static void alloc_globals_ctor(gear_alloc_globals *alloc_globals)
{
	char *tmp;

#if GEAR_MM_CUSTOM
	tmp = getenv("USE_GEAR_ALLOC");
	if (tmp && !gear_atoi(tmp, 0)) {
		alloc_globals->mm_heap = malloc(sizeof(gear_mm_heap));
		memset(alloc_globals->mm_heap, 0, sizeof(gear_mm_heap));
		alloc_globals->mm_heap->use_custom_heap = GEAR_MM_CUSTOM_HEAP_STD;
		alloc_globals->mm_heap->custom_heap.std._malloc = __gear_malloc;
		alloc_globals->mm_heap->custom_heap.std._free = free;
		alloc_globals->mm_heap->custom_heap.std._realloc = __gear_realloc;
		return;
	}
#endif

	tmp = getenv("USE_GEAR_ALLOC_HUGE_PAGES");
	if (tmp && gear_atoi(tmp, 0)) {
		gear_mm_use_huge_pages = 1;
	}
	GEAR_PBCLS_CACHE_UPDATE();
	alloc_globals->mm_heap = gear_mm_init();
}

#ifdef ZTS
static void alloc_globals_dtor(gear_alloc_globals *alloc_globals)
{
	gear_mm_shutdown(alloc_globals->mm_heap, 1, 1);
}
#endif

GEAR_API void start_memory_manager(void)
{
#ifdef ZTS
	ts_allocate_id(&alloc_globals_id, sizeof(gear_alloc_globals), (ts_allocate_ctor) alloc_globals_ctor, (ts_allocate_dtor) alloc_globals_dtor);
#else
	alloc_globals_ctor(&alloc_globals);
#endif
#ifndef _WIN32
#  if defined(_SC_PAGESIZE)
	REAL_PAGE_SIZE = sysconf(_SC_PAGESIZE);
#  elif defined(_SC_PAGE_SIZE)
	REAL_PAGE_SIZE = sysconf(_SC_PAGE_SIZE);
#  endif
#endif
}

GEAR_API gear_mm_heap *gear_mm_set_heap(gear_mm_heap *new_heap)
{
	gear_mm_heap *old_heap;

	old_heap = AG(mm_heap);
	AG(mm_heap) = (gear_mm_heap*)new_heap;
	return (gear_mm_heap*)old_heap;
}

GEAR_API gear_mm_heap *gear_mm_get_heap(void)
{
	return AG(mm_heap);
}

GEAR_API int gear_mm_is_custom_heap(gear_mm_heap *new_heap)
{
#if GEAR_MM_CUSTOM
	return AG(mm_heap)->use_custom_heap;
#else
	return 0;
#endif
}

GEAR_API void gear_mm_set_custom_handlers(gear_mm_heap *heap,
                                          void* (*_malloc)(size_t),
                                          void  (*_free)(void*),
                                          void* (*_realloc)(void*, size_t))
{
#if GEAR_MM_CUSTOM
	gear_mm_heap *_heap = (gear_mm_heap*)heap;

	if (!_malloc && !_free && !_realloc) {
		_heap->use_custom_heap = GEAR_MM_CUSTOM_HEAP_NONE;
	} else {
		_heap->use_custom_heap = GEAR_MM_CUSTOM_HEAP_STD;
		_heap->custom_heap.std._malloc = _malloc;
		_heap->custom_heap.std._free = _free;
		_heap->custom_heap.std._realloc = _realloc;
	}
#endif
}

GEAR_API void gear_mm_get_custom_handlers(gear_mm_heap *heap,
                                          void* (**_malloc)(size_t),
                                          void  (**_free)(void*),
                                          void* (**_realloc)(void*, size_t))
{
#if GEAR_MM_CUSTOM
	gear_mm_heap *_heap = (gear_mm_heap*)heap;

	if (heap->use_custom_heap) {
		*_malloc = _heap->custom_heap.std._malloc;
		*_free = _heap->custom_heap.std._free;
		*_realloc = _heap->custom_heap.std._realloc;
	} else {
		*_malloc = NULL;
		*_free = NULL;
		*_realloc = NULL;
	}
#else
	*_malloc = NULL;
	*_free = NULL;
	*_realloc = NULL;
#endif
}

#if GEAR_DEBUG
GEAR_API void gear_mm_set_custom_debug_handlers(gear_mm_heap *heap,
                                          void* (*_malloc)(size_t GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC),
                                          void  (*_free)(void* GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC),
                                          void* (*_realloc)(void*, size_t GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC))
{
#if GEAR_MM_CUSTOM
	gear_mm_heap *_heap = (gear_mm_heap*)heap;

	_heap->use_custom_heap = GEAR_MM_CUSTOM_HEAP_DEBUG;
	_heap->custom_heap.debug._malloc = _malloc;
	_heap->custom_heap.debug._free = _free;
	_heap->custom_heap.debug._realloc = _realloc;
#endif
}
#endif

GEAR_API gear_mm_storage *gear_mm_get_storage(gear_mm_heap *heap)
{
#if GEAR_MM_STORAGE
	return heap->storage;
#else
	return NULL
#endif
}

GEAR_API gear_mm_heap *gear_mm_startup(void)
{
	return gear_mm_init();
}

GEAR_API gear_mm_heap *gear_mm_startup_ex(const gear_mm_handlers *handlers, void *data, size_t data_size)
{
#if GEAR_MM_STORAGE
	gear_mm_storage tmp_storage, *storage;
	gear_mm_chunk *chunk;
	gear_mm_heap *heap;

	memcpy((gear_mm_handlers*)&tmp_storage.handlers, handlers, sizeof(gear_mm_handlers));
	tmp_storage.data = data;
	chunk = (gear_mm_chunk*)handlers->chunk_alloc(&tmp_storage, GEAR_MM_CHUNK_SIZE, GEAR_MM_CHUNK_SIZE);
	if (UNEXPECTED(chunk == NULL)) {
#if GEAR_MM_ERROR
#ifdef _WIN32
		stderr_last_error("Can't initialize heap");
#else
		fprintf(stderr, "\nCan't initialize heap: [%d] %s\n", errno, strerror(errno));
#endif
#endif
		return NULL;
	}
	heap = &chunk->heap_slot;
	chunk->heap = heap;
	chunk->next = chunk;
	chunk->prev = chunk;
	chunk->free_pages = GEAR_MM_PAGES - GEAR_MM_FIRST_PAGE;
	chunk->free_tail = GEAR_MM_FIRST_PAGE;
	chunk->num = 0;
	chunk->free_map[0] = (Z_L(1) << GEAR_MM_FIRST_PAGE) - 1;
	chunk->map[0] = GEAR_MM_LRUN(GEAR_MM_FIRST_PAGE);
	heap->main_chunk = chunk;
	heap->cached_chunks = NULL;
	heap->chunks_count = 1;
	heap->peak_chunks_count = 1;
	heap->cached_chunks_count = 0;
	heap->avg_chunks_count = 1.0;
	heap->last_chunks_delete_boundary = 0;
	heap->last_chunks_delete_count = 0;
#if GEAR_MM_STAT || GEAR_MM_LIMIT
	heap->real_size = GEAR_MM_CHUNK_SIZE;
#endif
#if GEAR_MM_STAT
	heap->real_peak = GEAR_MM_CHUNK_SIZE;
	heap->size = 0;
	heap->peak = 0;
#endif
#if GEAR_MM_LIMIT
	heap->limit = (Z_L(-1) >> Z_L(1));
	heap->overflow = 0;
#endif
#if GEAR_MM_CUSTOM
	heap->use_custom_heap = 0;
#endif
	heap->storage = &tmp_storage;
	heap->huge_list = NULL;
	memset(heap->free_slot, 0, sizeof(heap->free_slot));
	storage = _gear_mm_alloc(heap, sizeof(gear_mm_storage) + data_size GEAR_FILE_LINE_CC GEAR_FILE_LINE_CC);
	if (!storage) {
		handlers->chunk_free(&tmp_storage, chunk, GEAR_MM_CHUNK_SIZE);
#if GEAR_MM_ERROR
#ifdef _WIN32
		stderr_last_error("Can't initialize heap");
#else
		fprintf(stderr, "\nCan't initialize heap: [%d] %s\n", errno, strerror(errno));
#endif
#endif
		return NULL;
	}
	memcpy(storage, &tmp_storage, sizeof(gear_mm_storage));
	if (data) {
		storage->data = (void*)(((char*)storage + sizeof(gear_mm_storage)));
		memcpy(storage->data, data, data_size);
	}
	heap->storage = storage;
	return heap;
#else
	return NULL;
#endif
}

static GEAR_COLD GEAR_NORETURN void gear_out_of_memory(void)
{
	fprintf(stderr, "Out of memory\n");
	exit(1);
}

GEAR_API void * __gear_malloc(size_t len)
{
	void *tmp = malloc(len);
	if (EXPECTED(tmp || !len)) {
		return tmp;
	}
	gear_out_of_memory();
}

GEAR_API void * __gear_calloc(size_t nmemb, size_t len)
{
	void *tmp;

	len = gear_safe_address_guarded(nmemb, len, 0);
	tmp = __gear_malloc(len);
	memset(tmp, 0, len);
	return tmp;
}

GEAR_API void * __gear_realloc(void *p, size_t len)
{
	p = realloc(p, len);
	if (EXPECTED(p || !len)) {
		return p;
	}
	gear_out_of_memory();
}

