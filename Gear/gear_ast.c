/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear_ast.h"
#include "gear_API.h"
#include "gear_operators.h"
#include "gear_language_parser.h"
#include "gear_smart_str.h"
#include "gear_exceptions.h"
#include "gear_constants.h"

GEAR_API gear_ast_process_t gear_ast_process = NULL;

static inline void *gear_ast_alloc(size_t size) {
	return gear_arena_alloc(&CG(ast_arena), size);
}

static inline void *gear_ast_realloc(void *old, size_t old_size, size_t new_size) {
	void *new = gear_ast_alloc(new_size);
	memcpy(new, old, old_size);
	return new;
}

static inline size_t gear_ast_size(uint32_t children) {
	return sizeof(gear_ast) - sizeof(gear_ast *) + sizeof(gear_ast *) * children;
}

static inline size_t gear_ast_list_size(uint32_t children) {
	return sizeof(gear_ast_list) - sizeof(gear_ast *) + sizeof(gear_ast *) * children;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_znode(znode *node) {
	gear_ast_znode *ast;

	ast = gear_ast_alloc(sizeof(gear_ast_znode));
	ast->kind = GEAR_AST_ZNODE;
	ast->attr = 0;
	ast->lineno = CG(gear_lineno);
	ast->node = *node;
	return (gear_ast *) ast;
}

static gear_always_inline gear_ast * gear_ast_create_zval_int(zval *zv, uint32_t attr, uint32_t lineno) {
	gear_ast_zval *ast;

	ast = gear_ast_alloc(sizeof(gear_ast_zval));
	ast->kind = GEAR_AST_ZVAL;
	ast->attr = attr;
	ZVAL_COPY_VALUE(&ast->val, zv);
	Z_LINENO(ast->val) = lineno;
	return (gear_ast *) ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval_with_lineno(zval *zv, uint32_t lineno) {
	return gear_ast_create_zval_int(zv, 0, lineno);
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval_ex(zval *zv, gear_ast_attr attr) {
	return gear_ast_create_zval_int(zv, attr, CG(gear_lineno));
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval(zval *zv) {
	return gear_ast_create_zval_int(zv, 0, CG(gear_lineno));
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval_from_str(gear_string *str) {
	zval zv;
	ZVAL_STR(&zv, str);
	return gear_ast_create_zval_int(&zv, 0, CG(gear_lineno));
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval_from_long(gear_long lval) {
	zval zv;
	ZVAL_LONG(&zv, lval);
	return gear_ast_create_zval_int(&zv, 0, CG(gear_lineno));
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_constant(gear_string *name, gear_ast_attr attr) {
	gear_ast_zval *ast;

	ast = gear_ast_alloc(sizeof(gear_ast_zval));
	ast->kind = GEAR_AST_CONSTANT;
	ast->attr = attr;
	ZVAL_STR(&ast->val, name);
	Z_LINENO(ast->val) = CG(gear_lineno);
	return (gear_ast *) ast;
}

GEAR_API gear_ast *gear_ast_create_decl(
	gear_ast_kind kind, uint32_t flags, uint32_t start_lineno, gear_string *doc_comment,
	gear_string *name, gear_ast *child0, gear_ast *child1, gear_ast *child2, gear_ast *child3
) {
	gear_ast_decl *ast;

	ast = gear_ast_alloc(sizeof(gear_ast_decl));
	ast->kind = kind;
	ast->attr = 0;
	ast->start_lineno = start_lineno;
	ast->end_lineno = CG(gear_lineno);
	ast->flags = flags;
	ast->lex_pos = LANG_SCNG(yy_text);
	ast->doc_comment = doc_comment;
	ast->name = name;
	ast->child[0] = child0;
	ast->child[1] = child1;
	ast->child[2] = child2;
	ast->child[3] = child3;

	return (gear_ast *) ast;
}

#if GEAR_AST_SPEC
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_0(gear_ast_kind kind) {
	gear_ast *ast;

	GEAR_ASSERT(kind >> GEAR_AST_NUM_CHILDREN_SHIFT == 0);
	ast = gear_ast_alloc(gear_ast_size(0));
	ast->kind = kind;
	ast->attr = 0;
	ast->lineno = CG(gear_lineno);

	return ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_1(gear_ast_kind kind, gear_ast *child) {
	gear_ast *ast;
	uint32_t lineno;

	GEAR_ASSERT(kind >> GEAR_AST_NUM_CHILDREN_SHIFT == 1);
	ast = gear_ast_alloc(gear_ast_size(1));
	ast->kind = kind;
	ast->attr = 0;
	ast->child[0] = child;
	if (child) {
		lineno = gear_ast_get_lineno(child);
	} else {
		lineno = CG(gear_lineno);
	}
	ast->lineno = lineno;
	ast->lineno = lineno;

	return ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_2(gear_ast_kind kind, gear_ast *child1, gear_ast *child2) {
	gear_ast *ast;
	uint32_t lineno;

	GEAR_ASSERT(kind >> GEAR_AST_NUM_CHILDREN_SHIFT == 2);
	ast = gear_ast_alloc(gear_ast_size(2));
	ast->kind = kind;
	ast->attr = 0;
	ast->child[0] = child1;
	ast->child[1] = child2;
	if (child1) {
		lineno = gear_ast_get_lineno(child1);
	} else if (child2) {
		lineno = gear_ast_get_lineno(child2);
	} else {
		lineno = CG(gear_lineno);
	}
	ast->lineno = lineno;

	return ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_3(gear_ast_kind kind, gear_ast *child1, gear_ast *child2, gear_ast *child3) {
	gear_ast *ast;
	uint32_t lineno;

	GEAR_ASSERT(kind >> GEAR_AST_NUM_CHILDREN_SHIFT == 3);
	ast = gear_ast_alloc(gear_ast_size(3));
	ast->kind = kind;
	ast->attr = 0;
	ast->child[0] = child1;
	ast->child[1] = child2;
	ast->child[2] = child3;
	if (child1) {
		lineno = gear_ast_get_lineno(child1);
	} else if (child2) {
		lineno = gear_ast_get_lineno(child2);
	} else if (child3) {
		lineno = gear_ast_get_lineno(child3);
	} else {
		lineno = CG(gear_lineno);
	}
	ast->lineno = lineno;

	return ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_4(gear_ast_kind kind, gear_ast *child1, gear_ast *child2, gear_ast *child3, gear_ast *child4) {
	gear_ast *ast;
	uint32_t lineno;

	GEAR_ASSERT(kind >> GEAR_AST_NUM_CHILDREN_SHIFT == 4);
	ast = gear_ast_alloc(gear_ast_size(4));
	ast->kind = kind;
	ast->attr = 0;
	ast->child[0] = child1;
	ast->child[1] = child2;
	ast->child[2] = child3;
	ast->child[3] = child4;
	if (child1) {
		lineno = gear_ast_get_lineno(child1);
	} else if (child2) {
		lineno = gear_ast_get_lineno(child2);
	} else if (child3) {
		lineno = gear_ast_get_lineno(child3);
	} else if (child4) {
		lineno = gear_ast_get_lineno(child4);
	} else {
		lineno = CG(gear_lineno);
	}
	ast->lineno = lineno;

	return ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_list_0(gear_ast_kind kind) {
	gear_ast *ast;
	gear_ast_list *list;

	ast = gear_ast_alloc(gear_ast_list_size(4));
	list = (gear_ast_list *) ast;
	list->kind = kind;
	list->attr = 0;
	list->lineno = CG(gear_lineno);
	list->children = 0;

	return ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_list_1(gear_ast_kind kind, gear_ast *child) {
	gear_ast *ast;
	gear_ast_list *list;
	uint32_t lineno;

	ast = gear_ast_alloc(gear_ast_list_size(4));
	list = (gear_ast_list *) ast;
	list->kind = kind;
	list->attr = 0;
	list->children = 1;
	list->child[0] = child;
	if (child) {
		lineno = gear_ast_get_lineno(child);
		if (lineno > CG(gear_lineno)) {
			lineno = CG(gear_lineno);
		}
	} else {
		lineno = CG(gear_lineno);
	}
	list->lineno = lineno;

	return ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_list_2(gear_ast_kind kind, gear_ast *child1, gear_ast *child2) {
	gear_ast *ast;
	gear_ast_list *list;
	uint32_t lineno;

	ast = gear_ast_alloc(gear_ast_list_size(4));
	list = (gear_ast_list *) ast;
	list->kind = kind;
	list->attr = 0;
	list->children = 2;
	list->child[0] = child1;
	list->child[1] = child2;
	if (child1) {
		lineno = gear_ast_get_lineno(child1);
		if (lineno > CG(gear_lineno)) {
			lineno = CG(gear_lineno);
		}
	} else if (child2) {
		lineno = gear_ast_get_lineno(child2);
		if (lineno > CG(gear_lineno)) {
			lineno = CG(gear_lineno);
		}
	} else {
		list->children = 0;
		lineno = CG(gear_lineno);
	}
	list->lineno = lineno;

	return ast;
}
#else
static gear_ast *gear_ast_create_from_va_list(gear_ast_kind kind, gear_ast_attr attr, va_list va) {
	uint32_t i, children = kind >> GEAR_AST_NUM_CHILDREN_SHIFT;
	gear_ast *ast;

	ast = gear_ast_alloc(gear_ast_size(children));
	ast->kind = kind;
	ast->attr = attr;
	ast->lineno = (uint32_t) -1;

	for (i = 0; i < children; ++i) {
		ast->child[i] = va_arg(va, gear_ast *);
		if (ast->child[i] != NULL) {
			uint32_t lineno = gear_ast_get_lineno(ast->child[i]);
			if (lineno < ast->lineno) {
				ast->lineno = lineno;
			}
		}
	}

	if (ast->lineno == UINT_MAX) {
		ast->lineno = CG(gear_lineno);
	}

	return ast;
}

GEAR_API gear_ast *gear_ast_create_ex(gear_ast_kind kind, gear_ast_attr attr, ...) {
	va_list va;
	gear_ast *ast;

	va_start(va, attr);
	ast = gear_ast_create_from_va_list(kind, attr, va);
	va_end(va);

	return ast;
}

GEAR_API gear_ast *gear_ast_create(gear_ast_kind kind, ...) {
	va_list va;
	gear_ast *ast;

	va_start(va, kind);
	ast = gear_ast_create_from_va_list(kind, 0, va);
	va_end(va);

	return ast;
}

GEAR_API gear_ast *gear_ast_create_list(uint32_t init_children, gear_ast_kind kind, ...) {
	gear_ast *ast;
	gear_ast_list *list;

	ast = gear_ast_alloc(gear_ast_list_size(4));
	list = (gear_ast_list *) ast;
	list->kind = kind;
	list->attr = 0;
	list->lineno = CG(gear_lineno);
	list->children = 0;

	{
		va_list va;
		uint32_t i;
		va_start(va, kind);
		for (i = 0; i < init_children; ++i) {
			gear_ast *child = va_arg(va, gear_ast *);
			ast = gear_ast_list_add(ast, child);
			if (child != NULL) {
				uint32_t lineno = gear_ast_get_lineno(child);
				if (lineno < ast->lineno) {
					ast->lineno = lineno;
				}
			}
		}
		va_end(va);
	}

	return ast;
}
#endif

static inline gear_bool is_power_of_two(uint32_t n) {
	return ((n != 0) && (n == (n & (~n + 1))));
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_list_add(gear_ast *ast, gear_ast *op) {
	gear_ast_list *list = gear_ast_get_list(ast);
	if (list->children >= 4 && is_power_of_two(list->children)) {
			list = gear_ast_realloc(list,
			gear_ast_list_size(list->children), gear_ast_list_size(list->children * 2));
	}
	list->child[list->children++] = op;
	return (gear_ast *) list;
}

static int gear_ast_add_array_element(zval *result, zval *offset, zval *expr)
{
	switch (Z_TYPE_P(offset)) {
		case IS_UNDEF:
			if (!gear_hash_next_index_insert(Z_ARRVAL_P(result), expr)) {
				gear_error(E_WARNING,
					"Cannot add element to the array as the next element is already occupied");
				zval_ptr_dtor_nogc(expr);
			}
			break;
		case IS_STRING:
			gear_symtable_update(Z_ARRVAL_P(result), Z_STR_P(offset), expr);
			zval_ptr_dtor_str(offset);
			break;
		case IS_NULL:
			gear_symtable_update(Z_ARRVAL_P(result), ZSTR_EMPTY_ALLOC(), expr);
			break;
		case IS_LONG:
			gear_hash_index_update(Z_ARRVAL_P(result), Z_LVAL_P(offset), expr);
			break;
		case IS_FALSE:
			gear_hash_index_update(Z_ARRVAL_P(result), 0, expr);
			break;
		case IS_TRUE:
			gear_hash_index_update(Z_ARRVAL_P(result), 1, expr);
			break;
		case IS_DOUBLE:
			gear_hash_index_update(Z_ARRVAL_P(result), gear_dval_to_lval(Z_DVAL_P(offset)), expr);
			break;
		case IS_RESOURCE:
			gear_error(E_NOTICE, "Resource ID#%d used as offset, casting to integer (%d)", Z_RES_HANDLE_P(offset), Z_RES_HANDLE_P(offset));
			gear_hash_index_update(Z_ARRVAL_P(result), Z_RES_HANDLE_P(offset), expr);
			break;
		default:
			gear_throw_error(NULL, "Illegal offset type");
			return FAILURE;
 	}
	return SUCCESS;
}

GEAR_API int GEAR_FASTCALL gear_ast_evaluate(zval *result, gear_ast *ast, gear_class_entry *scope)
{
	zval op1, op2;
	int ret = SUCCESS;

	switch (ast->kind) {
		case GEAR_AST_BINARY_OP:
			if (UNEXPECTED(gear_ast_evaluate(&op1, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
			} else if (UNEXPECTED(gear_ast_evaluate(&op2, ast->child[1], scope) != SUCCESS)) {
				zval_ptr_dtor_nogc(&op1);
				ret = FAILURE;
			} else {
				binary_op_type op = get_binary_op(ast->attr);
				ret = op(result, &op1, &op2);
				zval_ptr_dtor_nogc(&op1);
				zval_ptr_dtor_nogc(&op2);
			}
			break;
		case GEAR_AST_GREATER:
		case GEAR_AST_GREATER_EQUAL:
			if (UNEXPECTED(gear_ast_evaluate(&op1, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
			} else if (UNEXPECTED(gear_ast_evaluate(&op2, ast->child[1], scope) != SUCCESS)) {
				zval_ptr_dtor_nogc(&op1);
				ret = FAILURE;
			} else {
				/* op1 > op2 is the same as op2 < op1 */
				binary_op_type op = ast->kind == GEAR_AST_GREATER
					? is_smaller_function : is_smaller_or_equal_function;
				ret = op(result, &op2, &op1);
				zval_ptr_dtor_nogc(&op1);
				zval_ptr_dtor_nogc(&op2);
			}
			break;
		case GEAR_AST_UNARY_OP:
			if (UNEXPECTED(gear_ast_evaluate(&op1, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
			} else {
				unary_op_type op = get_unary_op(ast->attr);
				ret = op(result, &op1);
				zval_ptr_dtor_nogc(&op1);
			}
			break;
		case GEAR_AST_ZVAL:
		{
			zval *zv = gear_ast_get_zval(ast);

			ZVAL_COPY(result, zv);
			break;
		}
		case GEAR_AST_CONSTANT:
		{
			gear_string *name = gear_ast_get_constant_name(ast);
			zval *zv = gear_get_constant_ex(name, scope, ast->attr);

			if (UNEXPECTED(zv == NULL)) {
				ZVAL_UNDEF(result);
				ret = gear_use_undefined_constant(name, ast->attr, result);
				break;
			}
			ZVAL_COPY_OR_DUP(result, zv);
			break;
		}
		case GEAR_AST_CONSTANT_CLASS:
			GEAR_ASSERT(EG(current_execute_data));
			if (scope && scope->name) {
				ZVAL_STR_COPY(result, scope->name);
			} else {
				ZVAL_EMPTY_STRING(result);
			}
			break;
		case GEAR_AST_AND:
			if (UNEXPECTED(gear_ast_evaluate(&op1, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
				break;
			}
			if (gear_is_true(&op1)) {
				if (UNEXPECTED(gear_ast_evaluate(&op2, ast->child[1], scope) != SUCCESS)) {
					zval_ptr_dtor_nogc(&op1);
					ret = FAILURE;
					break;
				}
				ZVAL_BOOL(result, gear_is_true(&op2));
				zval_ptr_dtor_nogc(&op2);
			} else {
				ZVAL_FALSE(result);
			}
			zval_ptr_dtor_nogc(&op1);
			break;
		case GEAR_AST_OR:
			if (UNEXPECTED(gear_ast_evaluate(&op1, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
				break;
			}
			if (gear_is_true(&op1)) {
				ZVAL_TRUE(result);
			} else {
				if (UNEXPECTED(gear_ast_evaluate(&op2, ast->child[1], scope) != SUCCESS)) {
					zval_ptr_dtor_nogc(&op1);
					ret = FAILURE;
					break;
				}
				ZVAL_BOOL(result, gear_is_true(&op2));
				zval_ptr_dtor_nogc(&op2);
			}
			zval_ptr_dtor_nogc(&op1);
			break;
		case GEAR_AST_CONDITIONAL:
			if (UNEXPECTED(gear_ast_evaluate(&op1, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
				break;
			}
			if (gear_is_true(&op1)) {
				if (!ast->child[1]) {
					*result = op1;
				} else {
					if (UNEXPECTED(gear_ast_evaluate(result, ast->child[1], scope) != SUCCESS)) {
						zval_ptr_dtor_nogc(&op1);
						ret = FAILURE;
						break;
					}
					zval_ptr_dtor_nogc(&op1);
				}
			} else {
				if (UNEXPECTED(gear_ast_evaluate(result, ast->child[2], scope) != SUCCESS)) {
					zval_ptr_dtor_nogc(&op1);
					ret = FAILURE;
					break;
				}
				zval_ptr_dtor_nogc(&op1);
			}
			break;
		case GEAR_AST_COALESCE:
			if (UNEXPECTED(gear_ast_evaluate(&op1, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
				break;
			}
			if (Z_TYPE(op1) > IS_NULL) {
				*result = op1;
			} else {
				if (UNEXPECTED(gear_ast_evaluate(result, ast->child[1], scope) != SUCCESS)) {
					zval_ptr_dtor_nogc(&op1);
					ret = FAILURE;
					break;
				}
				zval_ptr_dtor_nogc(&op1);
			}
			break;
		case GEAR_AST_UNARY_PLUS:
			if (UNEXPECTED(gear_ast_evaluate(&op2, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
			} else {
				ZVAL_LONG(&op1, 0);
				ret = add_function(result, &op1, &op2);
				zval_ptr_dtor_nogc(&op2);
			}
			break;
		case GEAR_AST_UNARY_MINUS:
			if (UNEXPECTED(gear_ast_evaluate(&op2, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
			} else {
				ZVAL_LONG(&op1, 0);
				ret = sub_function(result, &op1, &op2);
				zval_ptr_dtor_nogc(&op2);
			}
			break;
		case GEAR_AST_ARRAY:
			{
				uint32_t i;
				gear_ast_list *list = gear_ast_get_list(ast);

				if (!list->children) {
					ZVAL_EMPTY_ARRAY(result);
					break;
				}
				array_init(result);
				for (i = 0; i < list->children; i++) {
					gear_ast *elem = list->child[i];
					if (elem->child[1]) {
						if (UNEXPECTED(gear_ast_evaluate(&op1, elem->child[1], scope) != SUCCESS)) {
							zval_ptr_dtor_nogc(result);
							return FAILURE;
						}
					} else {
						ZVAL_UNDEF(&op1);
					}
					if (UNEXPECTED(gear_ast_evaluate(&op2, elem->child[0], scope) != SUCCESS)) {
						zval_ptr_dtor_nogc(&op1);
						zval_ptr_dtor_nogc(result);
						return FAILURE;
					}
					if (UNEXPECTED(gear_ast_add_array_element(result, &op1, &op2) != SUCCESS)) {
						zval_ptr_dtor_nogc(&op1);
						zval_ptr_dtor_nogc(&op2);
						zval_ptr_dtor_nogc(result);
						return FAILURE;
					}
				}
			}
			break;
		case GEAR_AST_DIM:
			if (ast->child[1] == NULL) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot use [] for reading");
			}

			if (UNEXPECTED(gear_ast_evaluate(&op1, ast->child[0], scope) != SUCCESS)) {
				ret = FAILURE;
			} else if (UNEXPECTED(gear_ast_evaluate(&op2, ast->child[1], scope) != SUCCESS)) {
				zval_ptr_dtor_nogc(&op1);
				ret = FAILURE;
			} else {
				gear_fetch_dimension_const(result, &op1, &op2, (ast->attr == GEAR_DIM_IS) ? BP_VAR_IS : BP_VAR_R);

				zval_ptr_dtor_nogc(&op1);
				zval_ptr_dtor_nogc(&op2);
			}
			break;
		default:
			gear_throw_error(NULL, "Unsupported constant expression");
			ret = FAILURE;
	}
	return ret;
}

static size_t GEAR_FASTCALL gear_ast_tree_size(gear_ast *ast)
{
	size_t size;

	if (ast->kind == GEAR_AST_ZVAL || ast->kind == GEAR_AST_CONSTANT) {
		size = sizeof(gear_ast_zval);
	} else if (gear_ast_is_list(ast)) {
		uint32_t i;
		gear_ast_list *list = gear_ast_get_list(ast);

		size = gear_ast_list_size(list->children);
		for (i = 0; i < list->children; i++) {
			if (list->child[i]) {
				size += gear_ast_tree_size(list->child[i]);
			}
		}
	} else {
		uint32_t i, children = gear_ast_get_num_children(ast);

		size = gear_ast_size(children);
		for (i = 0; i < children; i++) {
			if (ast->child[i]) {
				size += gear_ast_tree_size(ast->child[i]);
			}
		}
	}
	return size;
}

static void* GEAR_FASTCALL gear_ast_tree_copy(gear_ast *ast, void *buf)
{
	if (ast->kind == GEAR_AST_ZVAL) {
		gear_ast_zval *new = (gear_ast_zval*)buf;
		new->kind = GEAR_AST_ZVAL;
		new->attr = ast->attr;
		ZVAL_COPY(&new->val, gear_ast_get_zval(ast));
		buf = (void*)((char*)buf + sizeof(gear_ast_zval));
	} else if (ast->kind == GEAR_AST_CONSTANT) {
		gear_ast_zval *new = (gear_ast_zval*)buf;
		new->kind = GEAR_AST_CONSTANT;
		new->attr = ast->attr;
		ZVAL_STR_COPY(&new->val, gear_ast_get_constant_name(ast));
		buf = (void*)((char*)buf + sizeof(gear_ast_zval));
	} else if (gear_ast_is_list(ast)) {
		gear_ast_list *list = gear_ast_get_list(ast);
		gear_ast_list *new = (gear_ast_list*)buf;
		uint32_t i;
		new->kind = list->kind;
		new->attr = list->attr;
		new->children = list->children;
		buf = (void*)((char*)buf + gear_ast_list_size(list->children));
		for (i = 0; i < list->children; i++) {
			if (list->child[i]) {
				new->child[i] = (gear_ast*)buf;
				buf = gear_ast_tree_copy(list->child[i], buf);
			} else {
				new->child[i] = NULL;
			}
		}
	} else {
		uint32_t i, children = gear_ast_get_num_children(ast);
		gear_ast *new = (gear_ast*)buf;
		new->kind = ast->kind;
		new->attr = ast->attr;
		buf = (void*)((char*)buf + gear_ast_size(children));
		for (i = 0; i < children; i++) {
			if (ast->child[i]) {
				new->child[i] = (gear_ast*)buf;
				buf = gear_ast_tree_copy(ast->child[i], buf);
			} else {
				new->child[i] = NULL;
			}
		}
	}
	return buf;
}

GEAR_API gear_ast_ref * GEAR_FASTCALL gear_ast_copy(gear_ast *ast)
{
	size_t tree_size;
	gear_ast_ref *ref;

	GEAR_ASSERT(ast != NULL);
	tree_size = gear_ast_tree_size(ast) + sizeof(gear_ast_ref);
	ref = emalloc(tree_size);
	gear_ast_tree_copy(ast, GC_AST(ref));
	GC_SET_REFCOUNT(ref, 1);
	GC_TYPE_INFO(ref) = IS_CONSTANT_AST;
	return ref;
}

GEAR_API void GEAR_FASTCALL gear_ast_destroy(gear_ast *ast)
{
tail_call:
	if (!ast) {
		return;
	}

	if (EXPECTED(ast->kind >= GEAR_AST_VAR)) {
		uint32_t i, children = gear_ast_get_num_children(ast);

		for (i = 1; i < children; i++) {
			gear_ast_destroy(ast->child[i]);
		}
		ast = ast->child[0];
		goto tail_call;
	} else if (EXPECTED(ast->kind == GEAR_AST_ZVAL)) {
		zval_ptr_dtor_nogc(gear_ast_get_zval(ast));
	} else if (EXPECTED(gear_ast_is_list(ast))) {
		gear_ast_list *list = gear_ast_get_list(ast);
		if (list->children) {
			uint32_t i;

			for (i = 1; i < list->children; i++) {
				gear_ast_destroy(list->child[i]);
			}
			ast = list->child[0];
			goto tail_call;
		}
	} else if (EXPECTED(ast->kind == GEAR_AST_CONSTANT)) {
		gear_string_release_ex(gear_ast_get_constant_name(ast), 0);
	} else if (EXPECTED(ast->kind >= GEAR_AST_FUNC_DECL)) {
		gear_ast_decl *decl = (gear_ast_decl *) ast;

		if (decl->name) {
		    gear_string_release_ex(decl->name, 0);
		}
		if (decl->doc_comment) {
			gear_string_release_ex(decl->doc_comment, 0);
		}
		gear_ast_destroy(decl->child[0]);
		gear_ast_destroy(decl->child[1]);
		gear_ast_destroy(decl->child[2]);
		ast = decl->child[3];
		goto tail_call;
	}
}

GEAR_API void GEAR_FASTCALL gear_ast_ref_destroy(gear_ast_ref *ast)
{
	gear_ast_destroy(GC_AST(ast));
	efree(ast);
}

GEAR_API void gear_ast_apply(gear_ast *ast, gear_ast_apply_func fn) {
	if (gear_ast_is_list(ast)) {
		gear_ast_list *list = gear_ast_get_list(ast);
		uint32_t i;
		for (i = 0; i < list->children; ++i) {
			fn(&list->child[i]);
		}
	} else {
		uint32_t i, children = gear_ast_get_num_children(ast);
		for (i = 0; i < children; ++i) {
			fn(&ast->child[i]);
		}
	}
}

/*
 * Operator Precendence
 * ====================
 * priority  associativity  operators
 * ----------------------------------
 *   10     left            include, include_once, eval, require, require_once
 *   20     left            ,
 *   30     left            or
 *   40     left            xor
 *   50     left            and
 *   60     right           print
 *   70     right           yield
 *   80     right           =>
 *   85     right           yield from
 *   90     right           = += -= *= /= .= %= &= |= ^= <<= >>= **=
 *  100     left            ? :
 *  110     right           ??
 *  120     left            ||
 *  130     left            &&
 *  140     left            |
 *  150     left            ^
 *  160     left            &
 *  170     non-associative == != === !==
 *  180     non-associative < <= > >= <=>
 *  190     left            << >>
 *  200     left            + - .
 *  210     left            * / %
 *  220     right           !
 *  230     non-associative instanceof
 *  240     right           + - ++ -- ~ (type) @
 *  250     right           **
 *  260     left            [
 *  270     non-associative clone new
 */

static GEAR_COLD void gear_ast_export_ex(smart_str *str, gear_ast *ast, int priority, int indent);

static GEAR_COLD void gear_ast_export_str(smart_str *str, gear_string *s)
{
	size_t i;

	for (i = 0; i < ZSTR_LEN(s); i++) {
		unsigned char c = ZSTR_VAL(s)[i];
		if (c == '\'' || c == '\\') {
			smart_str_appendc(str, '\\');
			smart_str_appendc(str, c);
		} else {
			smart_str_appendc(str, c);
		}
	}
}

static GEAR_COLD void gear_ast_export_qstr(smart_str *str, char quote, gear_string *s)
{
	size_t i;

	for (i = 0; i < ZSTR_LEN(s); i++) {
		unsigned char c = ZSTR_VAL(s)[i];
		if (c < ' ') {
			switch (c) {
				case '\n':
					smart_str_appends(str, "\\n");
					break;
				case '\r':
					smart_str_appends(str, "\\r");
					break;
				case '\t':
					smart_str_appends(str, "\\t");
					break;
				case '\f':
					smart_str_appends(str, "\\f");
					break;
				case '\v':
					smart_str_appends(str, "\\v");
					break;
#ifdef GEAR_WIN32
				case VK_ESCAPE:
#else
				case '\e':
#endif
					smart_str_appends(str, "\\e");
					break;
				default:
					smart_str_appends(str, "\\0");
					smart_str_appendc(str, '0' + (c / 8));
					smart_str_appendc(str, '0' + (c % 8));
					break;
			}
		} else {
			if (c == quote || c == '$' || c == '\\') {
				smart_str_appendc(str, '\\');
			}
			smart_str_appendc(str, c);
		}
	}
}

static GEAR_COLD void gear_ast_export_indent(smart_str *str, int indent)
{
	while (indent > 0) {
		smart_str_appends(str, "    ");
		indent--;
	}
}

static GEAR_COLD void gear_ast_export_name(smart_str *str, gear_ast *ast, int priority, int indent)
{
	if (ast->kind == GEAR_AST_ZVAL) {
		zval *zv = gear_ast_get_zval(ast);

		if (Z_TYPE_P(zv) == IS_STRING) {
			smart_str_append(str, Z_STR_P(zv));
			return;
		}
	}
	gear_ast_export_ex(str, ast, priority, indent);
}

static GEAR_COLD void gear_ast_export_ns_name(smart_str *str, gear_ast *ast, int priority, int indent)
{
	if (ast->kind == GEAR_AST_ZVAL) {
		zval *zv = gear_ast_get_zval(ast);

		if (Z_TYPE_P(zv) == IS_STRING) {
		    if (ast->attr == GEAR_NAME_FQ) {
				smart_str_appendc(str, '\\');
		    } else if (ast->attr == GEAR_NAME_RELATIVE) {
				smart_str_appends(str, "namespace\\");
		    }
			smart_str_append(str, Z_STR_P(zv));
			return;
		}
	}
	gear_ast_export_ex(str, ast, priority, indent);
}

static GEAR_COLD int gear_ast_valid_var_char(char ch)
{
	unsigned char c = (unsigned char)ch;

	if (c != '_' && c < 127 &&
	    (c < '0' || c > '9') &&
	    (c < 'A' || c > 'Z') &&
	    (c < 'a' || c > 'z')) {
		return 0;
	}
	return 1;
}

static GEAR_COLD int gear_ast_valid_var_name(const char *s, size_t len)
{
	unsigned char c;
	size_t i;

	if (len == 0) {
		return 0;
	}
	c = (unsigned char)s[0];
	if (c != '_' && c < 127 &&
	    (c < 'A' || c > 'Z') &&
	    (c < 'a' || c > 'z')) {
		return 0;
	}
	for (i = 1; i < len; i++) {
		c = (unsigned char)s[i];
		if (c != '_' && c < 127 &&
		    (c < '0' || c > '9') &&
		    (c < 'A' || c > 'Z') &&
		    (c < 'a' || c > 'z')) {
			return 0;
		}
	}
	return 1;
}

static GEAR_COLD void gear_ast_export_var(smart_str *str, gear_ast *ast, int priority, int indent)
{
	if (ast->kind == GEAR_AST_ZVAL) {
		zval *zv = gear_ast_get_zval(ast);
		if (Z_TYPE_P(zv) == IS_STRING &&
		    gear_ast_valid_var_name(Z_STRVAL_P(zv), Z_STRLEN_P(zv))) {
			smart_str_append(str, Z_STR_P(zv));
			return;
		}
	} else if (ast->kind == GEAR_AST_VAR) {
		gear_ast_export_ex(str, ast, 0, indent);
		return;
	}
	smart_str_appendc(str, '{');
	gear_ast_export_name(str, ast, 0, indent);
	smart_str_appendc(str, '}');
}

static GEAR_COLD void gear_ast_export_list(smart_str *str, gear_ast_list *list, int separator, int priority, int indent)
{
	uint32_t i = 0;

	while (i < list->children) {
		if (i != 0 && separator) {
			smart_str_appends(str, ", ");
		}
		gear_ast_export_ex(str, list->child[i], priority, indent);
		i++;
	}
}

static GEAR_COLD void gear_ast_export_encaps_list(smart_str *str, char quote, gear_ast_list *list, int indent)
{
	uint32_t i = 0;
	gear_ast *ast;

	while (i < list->children) {
		ast = list->child[i];
		if (ast->kind == GEAR_AST_ZVAL) {
			zval *zv = gear_ast_get_zval(ast);

			GEAR_ASSERT(Z_TYPE_P(zv) == IS_STRING);
			gear_ast_export_qstr(str, quote, Z_STR_P(zv));
		} else if (ast->kind == GEAR_AST_VAR &&
		           ast->child[0]->kind == GEAR_AST_ZVAL &&
		           (i + 1 == list->children ||
		            list->child[i + 1]->kind != GEAR_AST_ZVAL ||
		            !gear_ast_valid_var_char(
		                *Z_STRVAL_P(
		                    gear_ast_get_zval(list->child[i + 1]))))) {
			gear_ast_export_ex(str, ast, 0, indent);
		} else {
			smart_str_appendc(str, '{');
			gear_ast_export_ex(str, ast, 0, indent);
			smart_str_appendc(str, '}');
		}
		i++;
	}
}

static GEAR_COLD void gear_ast_export_name_list_ex(smart_str *str, gear_ast_list *list, int indent, const char *separator)
{
	uint32_t i = 0;

	while (i < list->children) {
		if (i != 0) {
			smart_str_appends(str, separator);
		}
		gear_ast_export_name(str, list->child[i], 0, indent);
		i++;
	}
}

#define gear_ast_export_name_list(s, l, i) gear_ast_export_name_list_ex(s, l, i, ", ")
#define gear_ast_export_catch_name_list(s, l, i) gear_ast_export_name_list_ex(s, l, i, "|")

static GEAR_COLD void gear_ast_export_var_list(smart_str *str, gear_ast_list *list, int indent)
{
	uint32_t i = 0;

	while (i < list->children) {
		if (i != 0) {
			smart_str_appends(str, ", ");
		}
		if (list->child[i]->attr) {
			smart_str_appendc(str, '&');
		}
		smart_str_appendc(str, '$');
		gear_ast_export_name(str, list->child[i], 20, indent);
		i++;
	}
}

static GEAR_COLD void gear_ast_export_stmt(smart_str *str, gear_ast *ast, int indent)
{
	if (!ast) {
		return;
	}

	if (ast->kind == GEAR_AST_STMT_LIST ||
	    ast->kind == GEAR_AST_TRAIT_ADAPTATIONS) {
		gear_ast_list *list = (gear_ast_list*)ast;
		uint32_t i = 0;

		while (i < list->children) {
			ast = list->child[i];
			gear_ast_export_stmt(str, ast, indent);
			i++;
		}
	} else {
		gear_ast_export_indent(str, indent);
		gear_ast_export_ex(str, ast, 0, indent);
		switch (ast->kind) {
			case GEAR_AST_LABEL:
			case GEAR_AST_IF:
			case GEAR_AST_SWITCH:
			case GEAR_AST_WHILE:
			case GEAR_AST_TRY:
			case GEAR_AST_FOR:
			case GEAR_AST_FOREACH:
			case GEAR_AST_FUNC_DECL:
			case GEAR_AST_METHOD:
			case GEAR_AST_CLASS:
			case GEAR_AST_USE_TRAIT:
			case GEAR_AST_NAMESPACE:
			case GEAR_AST_DECLARE:
				break;
			default:
				smart_str_appendc(str, ';');
				break;
		}
		smart_str_appendc(str, '\n');
	}
}

static GEAR_COLD void gear_ast_export_if_stmt(smart_str *str, gear_ast_list *list, int indent)
{
	uint32_t i;
	gear_ast *ast;

tail_call:
	i = 0;
	while (i < list->children) {
		ast = list->child[i];
		GEAR_ASSERT(ast->kind == GEAR_AST_IF_ELEM);
		if (ast->child[0]) {
			if (i == 0) {
				smart_str_appends(str, "if (");
			} else {
				gear_ast_export_indent(str, indent);
				smart_str_appends(str, "} elseif (");
			}
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appends(str, ") {\n");
			gear_ast_export_stmt(str, ast->child[1], indent + 1);
		} else {
			gear_ast_export_indent(str, indent);
			smart_str_appends(str, "} else ");
			if (ast->child[1]->kind == GEAR_AST_IF) {
				list = (gear_ast_list*)ast->child[1];
				goto tail_call;
			} else {
				smart_str_appends(str, "{\n");
				gear_ast_export_stmt(str, ast->child[1], indent + 1);
			}
		}
		i++;
	}
	gear_ast_export_indent(str, indent);
	smart_str_appendc(str, '}');
}

static GEAR_COLD void gear_ast_export_zval(smart_str *str, zval *zv, int priority, int indent)
{
	gear_long idx;
	gear_string *key;
	zval *val;
	int first;

	ZVAL_DEREF(zv);
	switch (Z_TYPE_P(zv)) {
		case IS_NULL:
			smart_str_appends(str, "null");
			break;
		case IS_FALSE:
			smart_str_appends(str, "false");
			break;
		case IS_TRUE:
			smart_str_appends(str, "true");
			break;
		case IS_LONG:
			smart_str_append_long(str, Z_LVAL_P(zv));
			break;
		case IS_DOUBLE:
			key = gear_strpprintf(0, "%.*G", (int) EG(precision), Z_DVAL_P(zv));
			smart_str_appendl(str, ZSTR_VAL(key), ZSTR_LEN(key));
			gear_string_release_ex(key, 0);
			break;
		case IS_STRING:
			smart_str_appendc(str, '\'');
			gear_ast_export_str(str, Z_STR_P(zv));
			smart_str_appendc(str, '\'');
			break;
		case IS_ARRAY:
			smart_str_appendc(str, '[');
			first = 1;
			GEAR_HASH_FOREACH_KEY_VAL(Z_ARRVAL_P(zv), idx, key, val) {
				if (first) {
					first = 0;
				} else {
					smart_str_appends(str, ", ");
				}
				if (key) {
					smart_str_appendc(str, '\'');
					gear_ast_export_str(str, key);
					smart_str_appends(str, "' => ");
				} else {
					smart_str_append_long(str, idx);
					smart_str_appends(str, " => ");
				}
				gear_ast_export_zval(str, val, 0, indent);
			} GEAR_HASH_FOREACH_END();
			smart_str_appendc(str, ']');
			break;
		case IS_CONSTANT_AST:
			gear_ast_export_ex(str, Z_ASTVAL_P(zv), priority, indent);
			break;
		EMPTY_SWITCH_DEFAULT_CASE();
	}
}

static GEAR_COLD void gear_ast_export_class_no_header(smart_str *str, gear_ast_decl *decl, int indent) {
	if (decl->child[0]) {
		smart_str_appends(str, " extends ");
		gear_ast_export_ns_name(str, decl->child[0], 0, indent);
	}
	if (decl->child[1]) {
		smart_str_appends(str, " implements ");
		gear_ast_export_ex(str, decl->child[1], 0, indent);
	}
	smart_str_appends(str, " {\n");
	gear_ast_export_stmt(str, decl->child[2], indent + 1);
	gear_ast_export_indent(str, indent);
	smart_str_appends(str, "}");
}

#define BINARY_OP(_op, _p, _pl, _pr) do { \
		op = _op; \
		p = _p; \
		pl = _pl; \
		pr = _pr; \
		goto binary_op; \
	} while (0)

#define PREFIX_OP(_op, _p, _pl) do { \
		op = _op; \
		p = _p; \
		pl = _pl; \
		goto prefix_op; \
	} while (0)

#define FUNC_OP(_op) do { \
		op = _op; \
		goto func_op; \
	} while (0)

#define POSTFIX_OP(_op, _p, _pl) do { \
		op = _op; \
		p = _p; \
		pl = _pl; \
		goto postfix_op; \
	} while (0)

#define APPEND_NODE_1(_op) do { \
		op = _op; \
		goto append_node_1; \
	} while (0)

#define APPEND_STR(_op) do { \
		op = _op; \
		goto append_str; \
	} while (0)

#define APPEND_DEFAULT_VALUE(n) do { \
		p = n; \
		goto append_default_value; \
	} while (0)

static GEAR_COLD void gear_ast_export_ex(smart_str *str, gear_ast *ast, int priority, int indent)
{
	gear_ast_decl *decl;
	int p, pl, pr;
	const char *op;

tail_call:
	if (!ast) {
		return;
	}
	switch (ast->kind) {
		/* special nodes */
		case GEAR_AST_ZVAL:
			gear_ast_export_zval(str, gear_ast_get_zval(ast), priority, indent);
			break;
		case GEAR_AST_CONSTANT: {
			gear_string *name = gear_ast_get_constant_name(ast);
			smart_str_appendl(str, ZSTR_VAL(name), ZSTR_LEN(name));
			break;
		}
		case GEAR_AST_CONSTANT_CLASS:
			smart_str_appendl(str, "__CLASS__", sizeof("__CLASS__")-1);
			break;
		case GEAR_AST_ZNODE:
			/* This AST kind is only used for temporary nodes during compilation */
			GEAR_ASSERT(0);
			break;

		/* declaration nodes */
		case GEAR_AST_FUNC_DECL:
		case GEAR_AST_CLOSURE:
		case GEAR_AST_METHOD:
			decl = (gear_ast_decl *) ast;
			if (decl->flags & GEAR_ACC_PUBLIC) {
				smart_str_appends(str, "public ");
			} else if (decl->flags & GEAR_ACC_PROTECTED) {
				smart_str_appends(str, "protected ");
			} else if (decl->flags & GEAR_ACC_PRIVATE) {
				smart_str_appends(str, "private ");
			}
			if (decl->flags & GEAR_ACC_STATIC) {
				smart_str_appends(str, "static ");
			}
			if (decl->flags & GEAR_ACC_ABSTRACT) {
				smart_str_appends(str, "abstract ");
			}
			if (decl->flags & GEAR_ACC_FINAL) {
				smart_str_appends(str, "final ");
			}
			smart_str_appends(str, "function ");
			if (decl->flags & GEAR_ACC_RETURN_REFERENCE) {
				smart_str_appendc(str, '&');
			}
			if (ast->kind != GEAR_AST_CLOSURE) {
				smart_str_appendl(str, ZSTR_VAL(decl->name), ZSTR_LEN(decl->name));
			}
			smart_str_appendc(str, '(');
			gear_ast_export_ex(str, decl->child[0], 0, indent);
			smart_str_appendc(str, ')');
			gear_ast_export_ex(str, decl->child[1], 0, indent);
			if (decl->child[3]) {
				smart_str_appends(str, ": ");
				if (decl->child[3]->attr & GEAR_TYPE_NULLABLE) {
					smart_str_appendc(str, '?');
				}
				gear_ast_export_ns_name(str, decl->child[3], 0, indent);
			}
			if (decl->child[2]) {
				smart_str_appends(str, " {\n");
				gear_ast_export_stmt(str, decl->child[2], indent + 1);
				gear_ast_export_indent(str, indent);
				smart_str_appendc(str, '}');
				if (ast->kind != GEAR_AST_CLOSURE) {
					smart_str_appendc(str, '\n');
				}
			} else {
				smart_str_appends(str, ";\n");
			}
			break;
		case GEAR_AST_CLASS:
			decl = (gear_ast_decl *) ast;
			if (decl->flags & GEAR_ACC_INTERFACE) {
				smart_str_appends(str, "interface ");
			} else if (decl->flags & GEAR_ACC_TRAIT) {
				smart_str_appends(str, "trait ");
			} else {
				if (decl->flags & GEAR_ACC_EXPLICIT_ABSTRACT_CLASS) {
					smart_str_appends(str, "abstract ");
				}
				if (decl->flags & GEAR_ACC_FINAL) {
					smart_str_appends(str, "final ");
				}
				smart_str_appends(str, "class ");
			}
			smart_str_appendl(str, ZSTR_VAL(decl->name), ZSTR_LEN(decl->name));
			gear_ast_export_class_no_header(str, decl, indent);
			smart_str_appendc(str, '\n');
			break;

		/* list nodes */
		case GEAR_AST_ARG_LIST:
		case GEAR_AST_EXPR_LIST:
		case GEAR_AST_PARAM_LIST:
simple_list:
			gear_ast_export_list(str, (gear_ast_list*)ast, 1, 20, indent);
			break;
		case GEAR_AST_ARRAY:
			smart_str_appendc(str, '[');
			gear_ast_export_list(str, (gear_ast_list*)ast, 1, 20, indent);
			smart_str_appendc(str, ']');
			break;
		case GEAR_AST_ENCAPS_LIST:
			smart_str_appendc(str, '"');
			gear_ast_export_encaps_list(str, '"', (gear_ast_list*)ast, indent);
			smart_str_appendc(str, '"');
			break;
		case GEAR_AST_STMT_LIST:
		case GEAR_AST_TRAIT_ADAPTATIONS:
			gear_ast_export_stmt(str, ast, indent);
			break;
		case GEAR_AST_IF:
			gear_ast_export_if_stmt(str, (gear_ast_list*)ast, indent);
			break;
		case GEAR_AST_SWITCH_LIST:
		case GEAR_AST_CATCH_LIST:
			gear_ast_export_list(str, (gear_ast_list*)ast, 0, 0, indent);
			break;
		case GEAR_AST_CLOSURE_USES:
			smart_str_appends(str, " use(");
			gear_ast_export_var_list(str, (gear_ast_list*)ast, indent);
			smart_str_appendc(str, ')');
			break;
		case GEAR_AST_PROP_DECL:
			if (ast->attr & GEAR_ACC_PUBLIC) {
				smart_str_appends(str, "public ");
			} else if (ast->attr & GEAR_ACC_PROTECTED) {
				smart_str_appends(str, "protected ");
			} else if (ast->attr & GEAR_ACC_PRIVATE) {
				smart_str_appends(str, "private ");
			}
			if (ast->attr & GEAR_ACC_STATIC) {
				smart_str_appends(str, "static ");
			}
			goto simple_list;
		case GEAR_AST_CONST_DECL:
		case GEAR_AST_CLASS_CONST_DECL:
			smart_str_appends(str, "const ");
			goto simple_list;
		case GEAR_AST_NAME_LIST:
			gear_ast_export_name_list(str, (gear_ast_list*)ast, indent);
			break;
		case GEAR_AST_USE:
			smart_str_appends(str, "use ");
			if (ast->attr == T_FUNCTION) {
				smart_str_appends(str, "function ");
			} else if (ast->attr == T_CONST) {
				smart_str_appends(str, "const ");
			}
			goto simple_list;

		/* 0 child nodes */
		case GEAR_AST_MAGIC_CONST:
			switch (ast->attr) {
				case T_LINE:     APPEND_STR("__LINE__");
				case T_FILE:     APPEND_STR("__FILE__");
				case T_DIR:      APPEND_STR("__DIR__");
				case T_TRAIT_C:  APPEND_STR("__TRAIT__");
				case T_METHOD_C: APPEND_STR("__METHOD__");
				case T_FUNC_C:   APPEND_STR("__FUNCTION__");
				case T_NS_C:     APPEND_STR("__NAMESPACE__");
				case T_CLASS_C:  APPEND_STR("__CLASS__");
				EMPTY_SWITCH_DEFAULT_CASE();
			}
			break;
		case GEAR_AST_TYPE:
			switch (ast->attr) {
				case IS_ARRAY:    APPEND_STR("array");
				case IS_CALLABLE: APPEND_STR("callable");
				EMPTY_SWITCH_DEFAULT_CASE();
			}
			break;

		/* 1 child node */
		case GEAR_AST_VAR:
			smart_str_appendc(str, '$');
			gear_ast_export_var(str, ast->child[0], 0, indent);
			break;
		case GEAR_AST_CONST:
			gear_ast_export_ns_name(str, ast->child[0], 0, indent);
			break;
		case GEAR_AST_UNPACK:
			smart_str_appends(str, "...");
			ast = ast->child[0];
			goto tail_call;
		case GEAR_AST_UNARY_PLUS:  PREFIX_OP("+", 240, 241);
		case GEAR_AST_UNARY_MINUS: PREFIX_OP("-", 240, 241);
		case GEAR_AST_CAST:
			switch (ast->attr) {
				case IS_NULL:      PREFIX_OP("(unset)",  240, 241);
				case _IS_BOOL:     PREFIX_OP("(bool)",   240, 241);
				case IS_LONG:      PREFIX_OP("(int)",    240, 241);
				case IS_DOUBLE:    PREFIX_OP("(double)", 240, 241);
				case IS_STRING:    PREFIX_OP("(string)", 240, 241);
				case IS_ARRAY:     PREFIX_OP("(array)",  240, 241);
				case IS_OBJECT:    PREFIX_OP("(object)", 240, 241);
				EMPTY_SWITCH_DEFAULT_CASE();
			}
			break;
		case GEAR_AST_EMPTY:
			FUNC_OP("empty");
		case GEAR_AST_ISSET:
			FUNC_OP("isset");
		case GEAR_AST_SILENCE:
			PREFIX_OP("@", 240, 241);
		case GEAR_AST_SHELL_EXEC:
			smart_str_appendc(str, '`');
			if (ast->child[0]->kind == GEAR_AST_ENCAPS_LIST) {
				gear_ast_export_encaps_list(str, '`', (gear_ast_list*)ast->child[0], indent);
			} else {
				zval *zv;
				GEAR_ASSERT(ast->child[0]->kind == GEAR_AST_ZVAL);
				zv = gear_ast_get_zval(ast->child[0]);
				GEAR_ASSERT(Z_TYPE_P(zv) == IS_STRING);
				gear_ast_export_qstr(str, '`', Z_STR_P(zv));
			}
			smart_str_appendc(str, '`');
			break;
		case GEAR_AST_CLONE:
			PREFIX_OP("clone ", 270, 271);
		case GEAR_AST_EXIT:
			if (ast->child[0]) {
				FUNC_OP("exit");
			} else {
				APPEND_STR("exit");
			}
			break;
		case GEAR_AST_PRINT:
			PREFIX_OP("print ", 60, 61);
		case GEAR_AST_INCLUDE_OR_EVAL:
			switch (ast->attr) {
				case GEAR_INCLUDE_ONCE: FUNC_OP("include_once");
				case GEAR_INCLUDE:      FUNC_OP("include");
				case GEAR_REQUIRE_ONCE: FUNC_OP("require_once");
				case GEAR_REQUIRE:      FUNC_OP("require");
				case GEAR_EVAL:         FUNC_OP("eval");
				EMPTY_SWITCH_DEFAULT_CASE();
			}
			break;
		case GEAR_AST_UNARY_OP:
			switch (ast->attr) {
				case GEAR_BW_NOT:   PREFIX_OP("~", 240, 241);
				case GEAR_BOOL_NOT: PREFIX_OP("!", 240, 241);
				EMPTY_SWITCH_DEFAULT_CASE();
			}
			break;
		case GEAR_AST_PRE_INC:
			PREFIX_OP("++", 240, 241);
		case GEAR_AST_PRE_DEC:
			PREFIX_OP("--", 240, 241);
		case GEAR_AST_POST_INC:
			POSTFIX_OP("++", 240, 241);
		case GEAR_AST_POST_DEC:
			POSTFIX_OP("--", 240, 241);

		case GEAR_AST_GLOBAL:
			APPEND_NODE_1("global");
		case GEAR_AST_UNSET:
			FUNC_OP("unset");
		case GEAR_AST_RETURN:
			APPEND_NODE_1("return");
		case GEAR_AST_LABEL:
			gear_ast_export_name(str, ast->child[0], 0, indent);
			smart_str_appendc(str, ':');
			break;
		case GEAR_AST_REF:
			smart_str_appendc(str, '&');
			ast = ast->child[0];
			goto tail_call;
		case GEAR_AST_HALT_COMPILER:
			APPEND_STR("__HALT_COMPILER()");
		case GEAR_AST_ECHO:
			APPEND_NODE_1("echo");
		case GEAR_AST_THROW:
			APPEND_NODE_1("throw");
		case GEAR_AST_GOTO:
			smart_str_appends(str, "goto ");
			gear_ast_export_name(str, ast->child[0], 0, indent);
			break;
		case GEAR_AST_BREAK:
			APPEND_NODE_1("break");
		case GEAR_AST_CONTINUE:
			APPEND_NODE_1("continue");

		/* 2 child nodes */
		case GEAR_AST_DIM:
			gear_ast_export_ex(str, ast->child[0], 260, indent);
			smart_str_appendc(str, '[');
			if (ast->child[1]) {
				gear_ast_export_ex(str, ast->child[1], 0, indent);
			}
			smart_str_appendc(str, ']');
			break;
		case GEAR_AST_PROP:
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appends(str, "->");
			gear_ast_export_var(str, ast->child[1], 0, indent);
			break;
		case GEAR_AST_STATIC_PROP:
			gear_ast_export_ns_name(str, ast->child[0], 0, indent);
			smart_str_appends(str, "::$");
			gear_ast_export_var(str, ast->child[1], 0, indent);
			break;
		case GEAR_AST_CALL:
			gear_ast_export_ns_name(str, ast->child[0], 0, indent);
			smart_str_appendc(str, '(');
			gear_ast_export_ex(str, ast->child[1], 0, indent);
			smart_str_appendc(str, ')');
			break;
		case GEAR_AST_CLASS_CONST:
			gear_ast_export_ns_name(str, ast->child[0], 0, indent);
			smart_str_appends(str, "::");
			gear_ast_export_name(str, ast->child[1], 0, indent);
			break;
		case GEAR_AST_ASSIGN:            BINARY_OP(" = ",   90, 91, 90);
		case GEAR_AST_ASSIGN_REF:        BINARY_OP(" =& ",  90, 91, 90);
		case GEAR_AST_ASSIGN_OP:
			switch (ast->attr) {
				case GEAR_ASSIGN_ADD:    BINARY_OP(" += ",  90, 91, 90);
				case GEAR_ASSIGN_SUB:    BINARY_OP(" -= ",  90, 91, 90);
				case GEAR_ASSIGN_MUL:    BINARY_OP(" *= ",  90, 91, 90);
				case GEAR_ASSIGN_DIV:    BINARY_OP(" /= ",  90, 91, 90);
				case GEAR_ASSIGN_MOD:    BINARY_OP(" %= ",  90, 91, 90);
				case GEAR_ASSIGN_SL:     BINARY_OP(" <<= ", 90, 91, 90);
				case GEAR_ASSIGN_SR:     BINARY_OP(" >>= ", 90, 91, 90);
				case GEAR_ASSIGN_CONCAT: BINARY_OP(" .= ",  90, 91, 90);
				case GEAR_ASSIGN_BW_OR:  BINARY_OP(" |= ",  90, 91, 90);
				case GEAR_ASSIGN_BW_AND: BINARY_OP(" &= ",  90, 91, 90);
				case GEAR_ASSIGN_BW_XOR: BINARY_OP(" ^= ",  90, 91, 90);
				case GEAR_ASSIGN_POW:    BINARY_OP(" **= ", 90, 91, 90);
				EMPTY_SWITCH_DEFAULT_CASE();
			}
			break;
		case GEAR_AST_BINARY_OP:
			switch (ast->attr) {
				case GEAR_ADD:                 BINARY_OP(" + ",   200, 200, 201);
				case GEAR_SUB:                 BINARY_OP(" - ",   200, 200, 201);
				case GEAR_MUL:                 BINARY_OP(" * ",   210, 210, 211);
				case GEAR_DIV:                 BINARY_OP(" / ",   210, 210, 211);
				case GEAR_MOD:                 BINARY_OP(" % ",   210, 210, 211);
				case GEAR_SL:                  BINARY_OP(" << ",  190, 190, 191);
				case GEAR_SR:                  BINARY_OP(" >> ",  190, 190, 191);
				case GEAR_CONCAT:              BINARY_OP(" . ",   200, 200, 201);
				case GEAR_BW_OR:               BINARY_OP(" | ",   140, 140, 141);
				case GEAR_BW_AND:              BINARY_OP(" & ",   160, 160, 161);
				case GEAR_BW_XOR:              BINARY_OP(" ^ ",   150, 150, 151);
				case GEAR_IS_IDENTICAL:        BINARY_OP(" === ", 170, 171, 171);
				case GEAR_IS_NOT_IDENTICAL:    BINARY_OP(" !== ", 170, 171, 171);
				case GEAR_IS_EQUAL:            BINARY_OP(" == ",  170, 171, 171);
				case GEAR_IS_NOT_EQUAL:        BINARY_OP(" != ",  170, 171, 171);
				case GEAR_IS_SMALLER:          BINARY_OP(" < ",   180, 181, 181);
				case GEAR_IS_SMALLER_OR_EQUAL: BINARY_OP(" <= ",  180, 181, 181);
				case GEAR_POW:                 BINARY_OP(" ** ",  250, 251, 250);
				case GEAR_BOOL_XOR:            BINARY_OP(" xor ",  40,  40,  41);
				case GEAR_SPACESHIP:           BINARY_OP(" <=> ", 180, 181, 181);
				EMPTY_SWITCH_DEFAULT_CASE();
			}
			break;
		case GEAR_AST_GREATER:                 BINARY_OP(" > ",   180, 181, 181);
		case GEAR_AST_GREATER_EQUAL:           BINARY_OP(" >= ",  180, 181, 181);
		case GEAR_AST_AND:                     BINARY_OP(" && ",  130, 130, 131);
		case GEAR_AST_OR:                      BINARY_OP(" || ",  120, 120, 121);
		case GEAR_AST_ARRAY_ELEM:
			if (ast->child[1]) {
				gear_ast_export_ex(str, ast->child[1], 80, indent);
				smart_str_appends(str, " => ");
			}
			gear_ast_export_ex(str, ast->child[0], 80, indent);
			break;
		case GEAR_AST_NEW:
			smart_str_appends(str, "new ");
			if (ast->child[0]->kind == GEAR_AST_CLASS) {
				smart_str_appends(str, "class");
				if (gear_ast_get_list(ast->child[1])->children) {
					smart_str_appendc(str, '(');
					gear_ast_export_ex(str, ast->child[1], 0, indent);
					smart_str_appendc(str, ')');
				}
				gear_ast_export_class_no_header(str, (gear_ast_decl *) ast->child[0], indent);
			} else {
				gear_ast_export_ns_name(str, ast->child[0], 0, indent);
				smart_str_appendc(str, '(');
				gear_ast_export_ex(str, ast->child[1], 0, indent);
				smart_str_appendc(str, ')');
			}
			break;
		case GEAR_AST_INSTANCEOF:
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appends(str, " instanceof ");
			gear_ast_export_ns_name(str, ast->child[1], 0, indent);
			break;
		case GEAR_AST_YIELD:
			if (priority > 70) smart_str_appendc(str, '(');
			smart_str_appends(str, "yield ");
			if (ast->child[0]) {
				if (ast->child[1]) {
					gear_ast_export_ex(str, ast->child[1], 70, indent);
					smart_str_appends(str, " => ");
				}
				gear_ast_export_ex(str, ast->child[0], 70, indent);
			}
			if (priority > 70) smart_str_appendc(str, ')');
			break;
		case GEAR_AST_YIELD_FROM:
			PREFIX_OP("yield from ", 85, 86);
		case GEAR_AST_COALESCE: BINARY_OP(" ?? ", 110, 111, 110);
		case GEAR_AST_STATIC:
			smart_str_appends(str, "static $");
			gear_ast_export_name(str, ast->child[0], 0, indent);
			APPEND_DEFAULT_VALUE(1);
		case GEAR_AST_WHILE:
			smart_str_appends(str, "while (");
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appends(str, ") {\n");
			gear_ast_export_stmt(str, ast->child[1], indent + 1);
			gear_ast_export_indent(str, indent);
			smart_str_appendc(str, '}');
			break;
		case GEAR_AST_DO_WHILE:
			smart_str_appends(str, "do {\n");
			gear_ast_export_stmt(str, ast->child[0], indent + 1);
			gear_ast_export_indent(str, indent);
			smart_str_appends(str, "} while (");
			gear_ast_export_ex(str, ast->child[1], 0, indent);
			smart_str_appendc(str, ')');
			break;

		case GEAR_AST_IF_ELEM:
			if (ast->child[0]) {
				smart_str_appends(str, "if (");
				gear_ast_export_ex(str, ast->child[0], 0, indent);
				smart_str_appends(str, ") {\n");
				gear_ast_export_stmt(str, ast->child[1], indent + 1);
			} else {
				smart_str_appends(str, "else {\n");
				gear_ast_export_stmt(str, ast->child[1], indent + 1);
			}
			gear_ast_export_indent(str, indent);
			smart_str_appendc(str, '}');
			break;
		case GEAR_AST_SWITCH:
			smart_str_appends(str, "switch (");
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appends(str, ") {\n");
			gear_ast_export_ex(str, ast->child[1], 0, indent + 1);
			gear_ast_export_indent(str, indent);
			smart_str_appendc(str, '}');
			break;
		case GEAR_AST_SWITCH_CASE:
			gear_ast_export_indent(str, indent);
			if (ast->child[0]) {
				smart_str_appends(str, "case ");
				gear_ast_export_ex(str, ast->child[0], 0, indent);
				smart_str_appends(str, ":\n");
			} else {
				smart_str_appends(str, "default:\n");
			}
			gear_ast_export_stmt(str, ast->child[1], indent + 1);
			break;
		case GEAR_AST_DECLARE:
			smart_str_appends(str, "declare(");
			GEAR_ASSERT(ast->child[0]->kind == GEAR_AST_CONST_DECL);
			gear_ast_export_list(str, (gear_ast_list*)ast->child[0], 1, 0, indent);
			smart_str_appendc(str, ')');
			if (ast->child[1]) {
				smart_str_appends(str, " {\n");
				gear_ast_export_stmt(str, ast->child[1], indent + 1);
				gear_ast_export_indent(str, indent);
				smart_str_appendc(str, '}');
			} else {
				smart_str_appendc(str, ';');
			}
			break;
		case GEAR_AST_PROP_ELEM:
			smart_str_appendc(str, '$');
			/* break missing intentionally */
		case GEAR_AST_CONST_ELEM:
			gear_ast_export_name(str, ast->child[0], 0, indent);
			APPEND_DEFAULT_VALUE(1);
		case GEAR_AST_USE_TRAIT:
			smart_str_appends(str, "use ");
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			if (ast->child[1]) {
				smart_str_appends(str, " {\n");
				gear_ast_export_ex(str, ast->child[1], 0, indent + 1);
				gear_ast_export_indent(str, indent);
				smart_str_appends(str, "}");
			} else {
				smart_str_appends(str, ";");
			}
			break;
		case GEAR_AST_TRAIT_PRECEDENCE:
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appends(str, " insteadof ");
			gear_ast_export_ex(str, ast->child[1], 0, indent);
			break;
		case GEAR_AST_METHOD_REFERENCE:
			if (ast->child[0]) {
				gear_ast_export_name(str, ast->child[0], 0, indent);
				smart_str_appends(str, "::");
			}
			gear_ast_export_name(str, ast->child[1], 0, indent);
			break;
		case GEAR_AST_NAMESPACE:
			smart_str_appends(str, "namespace");
			if (ast->child[0]) {
				smart_str_appendc(str, ' ');
				gear_ast_export_name(str, ast->child[0], 0, indent);
			}
			if (ast->child[1]) {
				smart_str_appends(str, " {\n");
				gear_ast_export_stmt(str, ast->child[1], indent + 1);
				gear_ast_export_indent(str, indent);
				smart_str_appends(str, "}\n");
			} else {
				smart_str_appendc(str, ';');
			}
			break;
		case GEAR_AST_USE_ELEM:
		case GEAR_AST_TRAIT_ALIAS:
			gear_ast_export_name(str, ast->child[0], 0, indent);
			if (ast->attr & GEAR_ACC_PUBLIC) {
				smart_str_appends(str, " as public");
			} else if (ast->attr & GEAR_ACC_PROTECTED) {
				smart_str_appends(str, " as protected");
			} else if (ast->attr & GEAR_ACC_PRIVATE) {
				smart_str_appends(str, " as private");
			} else if (ast->child[1]) {
				smart_str_appends(str, " as");
			}
			if (ast->child[1]) {
				smart_str_appendc(str, ' ');
				gear_ast_export_name(str, ast->child[1], 0, indent);
			}
			break;

		/* 3 child nodes */
		case GEAR_AST_METHOD_CALL:
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appends(str, "->");
			gear_ast_export_var(str, ast->child[1], 0, indent);
			smart_str_appendc(str, '(');
			gear_ast_export_ex(str, ast->child[2], 0, indent);
			smart_str_appendc(str, ')');
			break;
		case GEAR_AST_STATIC_CALL:
			gear_ast_export_ns_name(str, ast->child[0], 0, indent);
			smart_str_appends(str, "::");
			gear_ast_export_var(str, ast->child[1], 0, indent);
			smart_str_appendc(str, '(');
			gear_ast_export_ex(str, ast->child[2], 0, indent);
			smart_str_appendc(str, ')');
			break;
		case GEAR_AST_CONDITIONAL:
			if (priority > 100) smart_str_appendc(str, '(');
			gear_ast_export_ex(str, ast->child[0], 100, indent);
			if (ast->child[1]) {
				smart_str_appends(str, " ? ");
				gear_ast_export_ex(str, ast->child[1], 101, indent);
				smart_str_appends(str, " : ");
			} else {
				smart_str_appends(str, " ?: ");
			}
			gear_ast_export_ex(str, ast->child[2], 101, indent);
			if (priority > 100) smart_str_appendc(str, ')');
			break;

		case GEAR_AST_TRY:
			smart_str_appends(str, "try {\n");
			gear_ast_export_stmt(str, ast->child[0], indent + 1);
			gear_ast_export_indent(str, indent);
			gear_ast_export_ex(str, ast->child[1], 0, indent);
			if (ast->child[2]) {
				smart_str_appends(str, "} finally {\n");
				gear_ast_export_stmt(str, ast->child[2], indent + 1);
				gear_ast_export_indent(str, indent);
			}
			smart_str_appendc(str, '}');
			break;
		case GEAR_AST_CATCH:
			smart_str_appends(str, "} catch (");
			gear_ast_export_catch_name_list(str, gear_ast_get_list(ast->child[0]), indent);
			smart_str_appends(str, " $");
			gear_ast_export_var(str, ast->child[1], 0, indent);
			smart_str_appends(str, ") {\n");
			gear_ast_export_stmt(str, ast->child[2], indent + 1);
			gear_ast_export_indent(str, indent);
			break;
		case GEAR_AST_PARAM:
			if (ast->child[0]) {
				if (ast->child[0]->attr & GEAR_TYPE_NULLABLE) {
					smart_str_appendc(str, '?');
				}
				gear_ast_export_ns_name(str, ast->child[0], 0, indent);
				smart_str_appendc(str, ' ');
			}
			if (ast->attr & GEAR_PARAM_REF) {
				smart_str_appendc(str, '&');
			}
			if (ast->attr & GEAR_PARAM_VARIADIC) {
				smart_str_appends(str, "...");
			}
			smart_str_appendc(str, '$');
			gear_ast_export_name(str, ast->child[1], 0, indent);
			APPEND_DEFAULT_VALUE(2);

		/* 4 child nodes */
		case GEAR_AST_FOR:
			smart_str_appends(str, "for (");
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appendc(str, ';');
			if (ast->child[1]) {
				smart_str_appendc(str, ' ');
				gear_ast_export_ex(str, ast->child[1], 0, indent);
			}
			smart_str_appendc(str, ';');
			if (ast->child[2]) {
				smart_str_appendc(str, ' ');
				gear_ast_export_ex(str, ast->child[2], 0, indent);
			}
			smart_str_appends(str, ") {\n");
			gear_ast_export_stmt(str, ast->child[3], indent + 1);
			gear_ast_export_indent(str, indent);
			smart_str_appendc(str, '}');
			break;
		case GEAR_AST_FOREACH:
			smart_str_appends(str, "foreach (");
			gear_ast_export_ex(str, ast->child[0], 0, indent);
			smart_str_appends(str, " as ");
			if (ast->child[2]) {
				gear_ast_export_ex(str, ast->child[2], 0, indent);
				smart_str_appends(str, " => ");
			}
			gear_ast_export_ex(str, ast->child[1], 0, indent);
			smart_str_appends(str, ") {\n");
			gear_ast_export_stmt(str, ast->child[3], indent + 1);
			gear_ast_export_indent(str, indent);
			smart_str_appendc(str, '}');
			break;
		EMPTY_SWITCH_DEFAULT_CASE();
	}
	return;

binary_op:
	if (priority > p) smart_str_appendc(str, '(');
	gear_ast_export_ex(str, ast->child[0], pl, indent);
	smart_str_appends(str, op);
	gear_ast_export_ex(str, ast->child[1], pr, indent);
	if (priority > p) smart_str_appendc(str, ')');
	return;

prefix_op:
	if (priority > p) smart_str_appendc(str, '(');
	smart_str_appends(str, op);
	gear_ast_export_ex(str, ast->child[0], pl, indent);
	if (priority > p) smart_str_appendc(str, ')');
	return;

postfix_op:
	if (priority > p) smart_str_appendc(str, '(');
	gear_ast_export_ex(str, ast->child[0], pl, indent);
	smart_str_appends(str, op);
	if (priority > p) smart_str_appendc(str, ')');
	return;

func_op:
	smart_str_appends(str, op);
	smart_str_appendc(str, '(');
	gear_ast_export_ex(str, ast->child[0], 0, indent);
	smart_str_appendc(str, ')');
	return;

append_node_1:
	smart_str_appends(str, op);
	if (ast->child[0]) {
		smart_str_appendc(str, ' ');
		ast = ast->child[0];
		goto tail_call;
	}
	return;

append_str:
	smart_str_appends(str, op);
	return;

append_default_value:
	if (ast->child[p]) {
		smart_str_appends(str, " = ");
		ast = ast->child[p];
		goto tail_call;
	}
	return;
}

GEAR_API GEAR_COLD gear_string *gear_ast_export(const char *prefix, gear_ast *ast, const char *suffix)
{
	smart_str str = {0};

	smart_str_appends(&str, prefix);
	gear_ast_export_ex(&str, ast, 0, 0);
	smart_str_appends(&str, suffix);
	smart_str_0(&str);
	return str.s;
}

