/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear_cpuinfo.h"

typedef struct _gear_cpu_info {
	uint32_t eax;
	uint32_t ebx;
	uint32_t ecx;
	uint32_t edx;
	uint32_t initialized;
} gear_cpu_info;

static gear_cpu_info cpuinfo = {0};

#if defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__))
# if defined(HAVE_CPUID_H) && defined(HAVE_CPUID_COUNT)
# include <cpuid.h>
static void __gear_cpuid(uint32_t func, uint32_t subfunc, gear_cpu_info *cpuinfo) {
	__cpuid_count(func, subfunc, cpuinfo->eax, cpuinfo->ebx, cpuinfo->ecx, cpuinfo->edx);
}
# else
static void __gear_cpuid(uint32_t func, uint32_t subfunc, gear_cpu_info *cpuinfo) {
#if defined(__i386__) && (defined(__pic__) || defined(__PIC__))
	/* PIC on i386 uses %ebx, so preserve it. */
	__asm__ __volatile__ (
		"pushl  %%ebx\n"
		"cpuid\n"
		"mov    %%ebx,%1\n"
		"popl   %%ebx"
		: "=a"(cpuinfo->eax), "=r"(cpuinfo->ebx), "=c"(cpuinfo->ecx), "=d"(cpuinfo->edx)
		: "a"(func), "c"(subfunc)
	);
#else
	__asm__ __volatile__ (
		"cpuid"
		: "=a"(cpuinfo->eax), "=b"(cpuinfo->ebx), "=c"(cpuinfo->ecx), "=d"(cpuinfo->edx)
		: "a"(func), "c"(subfunc)
	);
#endif
}
# endif
#elif defined(GEAR_WIN32) && !defined(__clang__)
# include <intrin.h>
static void __gear_cpuid(uint32_t func, uint32_t subfunc, gear_cpu_info *cpuinfo) {
	int regs[4];

	__cpuidex(regs, func, subfunc);

	cpuinfo->eax = regs[0];
	cpuinfo->ebx = regs[1];
	cpuinfo->ecx = regs[2];
	cpuinfo->edx = regs[3];
}
#else
static void __gear_cpuid(uint32_t func, uint32_t subfunc, gear_cpu_info *cpuinfo) {
	cpuinfo->eax = 0;
}
#endif

void gear_cpu_startup(void)
{
	if (!cpuinfo.initialized) {
		gear_cpu_info ebx;
		int max_feature;

		cpuinfo.initialized = 1;
		__gear_cpuid(0, 0, &cpuinfo);
		max_feature = cpuinfo.eax;
		if (max_feature == 0) {
			return;
		}

		__gear_cpuid(1, 0, &cpuinfo);

		/* for avx2 */
		if (max_feature >= 7) {
			__gear_cpuid(7, 0, &ebx);
			cpuinfo.ebx = ebx.ebx;
		} else {
			cpuinfo.ebx = 0;
		}
	}
}

GEAR_API int gear_cpu_supports(gear_cpu_feature feature) {
	if (feature & GEAR_CPU_EDX_MASK) {
		return (cpuinfo.edx & (feature & ~GEAR_CPU_EDX_MASK));
	} else if (feature & GEAR_CPU_EBX_MASK) {
		return (cpuinfo.ebx & (feature & ~GEAR_CPU_EBX_MASK));
	} else {
		return (cpuinfo.ecx & feature);
	}
}

