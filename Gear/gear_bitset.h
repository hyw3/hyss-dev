/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GEAR_BITSET_H_
#define _GEAR_BITSET_H_

typedef gear_ulong *gear_bitset;

#define GEAR_BITSET_ELM_SIZE sizeof(gear_ulong)

#if SIZEOF_GEAR_LONG == 4
# define GEAR_BITSET_ELM_NUM(n)		((n) >> 5)
# define GEAR_BITSET_BIT_NUM(n)		((gear_ulong)(n) & Z_UL(0x1f))
#elif SIZEOF_GEAR_LONG == 8
# define GEAR_BITSET_ELM_NUM(n)		((n) >> 6)
# define GEAR_BITSET_BIT_NUM(n)		((gear_ulong)(n) & Z_UL(0x3f))
#else
# define GEAR_BITSET_ELM_NUM(n)		((n) / (sizeof(gear_long) * 8))
# define GEAR_BITSET_BIT_NUM(n)		((n) % (sizeof(gear_long) * 8))
#endif

#define GEAR_BITSET_ALLOCA(n, use_heap) \
	(gear_bitset)do_alloca((n) * GEAR_BITSET_ELM_SIZE, use_heap)

/* Number of trailing zero bits (0x01 -> 0; 0x40 -> 6; 0x00 -> LEN) */
static gear_always_inline int gear_ulong_ntz(gear_ulong num)
{
#if (defined(__GNUC__) || __has_builtin(__builtin_ctzl)) \
	&& SIZEOF_GEAR_LONG == SIZEOF_LONG && defined(HYSS_HAVE_BUILTIN_CTZL)
	return __builtin_ctzl(num);
#elif (defined(__GNUC__) || __has_builtin(__builtin_ctzll)) && defined(HYSS_HAVE_BUILTIN_CTZLL)
	return __builtin_ctzll(num);
#elif defined(_WIN32)
	unsigned long index;

#if defined(_WIN64)
	if (!BitScanForward64(&index, num)) {
#else
	if (!BitScanForward(&index, num)) {
#endif
		/* undefined behavior */
		return SIZEOF_GEAR_LONG * 8;
	}

	return (int) index;
#else
	int n;

	if (num == Z_UL(0)) return SIZEOF_GEAR_LONG * 8;

	n = 1;
#if SIZEOF_GEAR_LONG == 8
	if ((num & 0xffffffff) == 0) {n += 32; num = num >> Z_UL(32);}
#endif
	if ((num & 0x0000ffff) == 0) {n += 16; num = num >> 16;}
	if ((num & 0x000000ff) == 0) {n +=  8; num = num >>  8;}
	if ((num & 0x0000000f) == 0) {n +=  4; num = num >>  4;}
	if ((num & 0x00000003) == 0) {n +=  2; num = num >>  2;}
	return n - (num & 1);
#endif
}

/* Returns the number of gear_ulong words needed to store a bitset that is N
   bits long.  */
static inline uint32_t gear_bitset_len(uint32_t n)
{
	return (n + ((sizeof(gear_long) * 8) - 1)) / (sizeof(gear_long) * 8);
}

static inline gear_bool gear_bitset_in(gear_bitset set, uint32_t n)
{
	return GEAR_BIT_TEST(set, n);
}

static inline void gear_bitset_incl(gear_bitset set, uint32_t n)
{
	set[GEAR_BITSET_ELM_NUM(n)] |= Z_UL(1) << GEAR_BITSET_BIT_NUM(n);
}

static inline void gear_bitset_excl(gear_bitset set, uint32_t n)
{
	set[GEAR_BITSET_ELM_NUM(n)] &= ~(Z_UL(1) << GEAR_BITSET_BIT_NUM(n));
}

static inline void gear_bitset_clear(gear_bitset set, uint32_t len)
{
	memset(set, 0, len * GEAR_BITSET_ELM_SIZE);
}

static inline int gear_bitset_empty(gear_bitset set, uint32_t len)
{
	uint32_t i;
	for (i = 0; i < len; i++) {
		if (set[i]) {
			return 0;
		}
	}
	return 1;
}

static inline void gear_bitset_fill(gear_bitset set, uint32_t len)
{
	memset(set, 0xff, len * GEAR_BITSET_ELM_SIZE);
}

static inline gear_bool gear_bitset_equal(gear_bitset set1, gear_bitset set2, uint32_t len)
{
    return memcmp(set1, set2, len * GEAR_BITSET_ELM_SIZE) == 0;
}

static inline void gear_bitset_copy(gear_bitset set1, gear_bitset set2, uint32_t len)
{
    memcpy(set1, set2, len * GEAR_BITSET_ELM_SIZE);
}

static inline void gear_bitset_intersection(gear_bitset set1, gear_bitset set2, uint32_t len)
{
    uint32_t i;

    for (i = 0; i < len; i++) {
		set1[i] &= set2[i];
	}
}

static inline void gear_bitset_union(gear_bitset set1, gear_bitset set2, uint32_t len)
{
	uint32_t i;

	for (i = 0; i < len; i++) {
		set1[i] |= set2[i];
	}
}

static inline void gear_bitset_difference(gear_bitset set1, gear_bitset set2, uint32_t len)
{
	uint32_t i;

	for (i = 0; i < len; i++) {
		set1[i] = set1[i] & ~set2[i];
	}
}

static inline void gear_bitset_union_with_intersection(gear_bitset set1, gear_bitset set2, gear_bitset set3, gear_bitset set4, uint32_t len)
{
	uint32_t i;

	for (i = 0; i < len; i++) {
		set1[i] = set2[i] | (set3[i] & set4[i]);
	}
}

static inline void gear_bitset_union_with_difference(gear_bitset set1, gear_bitset set2, gear_bitset set3, gear_bitset set4, uint32_t len)
{
	uint32_t i;

	for (i = 0; i < len; i++) {
		set1[i] = set2[i] | (set3[i] & ~set4[i]);
	}
}

static inline gear_bool gear_bitset_subset(gear_bitset set1, gear_bitset set2, uint32_t len)
{
	uint32_t i;

	for (i = 0; i < len; i++) {
		if (set1[i] & ~set2[i]) {
			return 0;
		}
	}
	return 1;
}

static inline int gear_bitset_first(gear_bitset set, uint32_t len)
{
	uint32_t i;

	for (i = 0; i < len; i++) {
		if (set[i]) {
			return GEAR_BITSET_ELM_SIZE * 8 * i + gear_ulong_ntz(set[i]);
		}
	}
	return -1; /* empty set */
}

static inline int gear_bitset_last(gear_bitset set, uint32_t len)
{
	uint32_t i = len;

	while (i > 0) {
		i--;
		if (set[i]) {
			int j = GEAR_BITSET_ELM_SIZE * 8 * i - 1;
			gear_ulong x = set[i];
			while (x != Z_UL(0)) {
				x = x >> Z_UL(1);
				j++;
			}
			return j;
		}
	}
	return -1; /* empty set */
}

#define GEAR_BITSET_FOREACH(set, len, bit) do { \
	gear_bitset _set = (set); \
	uint32_t _i, _len = (len); \
	for (_i = 0; _i < _len; _i++) { \
		gear_ulong _x = _set[_i]; \
		if (_x) { \
			(bit) = GEAR_BITSET_ELM_SIZE * 8 * _i; \
			for (; _x != 0; _x >>= Z_UL(1), (bit)++) { \
				if (!(_x & Z_UL(1))) continue;

#define GEAR_BITSET_REVERSE_FOREACH(set, len, bit) do { \
	gear_bitset _set = (set); \
	uint32_t _i = (len); \
	gear_ulong _test = Z_UL(1) << (GEAR_BITSET_ELM_SIZE * 8 - 1); \
	while (_i-- > 0) { \
		gear_ulong _x = _set[_i]; \
		if (_x) { \
			(bit) = GEAR_BITSET_ELM_SIZE * 8 * (_i + 1) - 1; \
			for (; _x != 0; _x <<= Z_UL(1), (bit)--) { \
				if (!(_x & _test)) continue; \

#define GEAR_BITSET_FOREACH_END() \
			} \
		} \
	} \
} while (0)

static inline int gear_bitset_pop_first(gear_bitset set, uint32_t len) {
	int i = gear_bitset_first(set, len);
	if (i >= 0) {
		gear_bitset_excl(set, i);
	}
	return i;
}

#endif /* _GEAR_BITSET_H_ */

