/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_INTERFACES_H
#define GEAR_INTERFACES_H

#include "gear.h"
#include "gear_API.h"

BEGIN_EXTERN_C()

extern GEAR_API gear_class_entry *gear_ce_traversable;
extern GEAR_API gear_class_entry *gear_ce_aggregate;
extern GEAR_API gear_class_entry *gear_ce_iterator;
extern GEAR_API gear_class_entry *gear_ce_arrayaccess;
extern GEAR_API gear_class_entry *gear_ce_serializable;
extern GEAR_API gear_class_entry *gear_ce_countable;

typedef struct _gear_user_iterator {
	gear_object_iterator     it;
	gear_class_entry         *ce;
	zval                     value;
} gear_user_iterator;

GEAR_API zval* gear_call_method(zval *object_pp, gear_class_entry *obj_ce, gear_function **fn_proxy, const char *function_name, size_t function_name_len, zval *retval, int param_count, zval* arg1, zval* arg2);

#define gear_call_method_with_0_params(obj, obj_ce, fn_proxy, function_name, retval) \
	gear_call_method(obj, obj_ce, fn_proxy, function_name, sizeof(function_name)-1, retval, 0, NULL, NULL)

#define gear_call_method_with_1_params(obj, obj_ce, fn_proxy, function_name, retval, arg1) \
	gear_call_method(obj, obj_ce, fn_proxy, function_name, sizeof(function_name)-1, retval, 1, arg1, NULL)

#define gear_call_method_with_2_params(obj, obj_ce, fn_proxy, function_name, retval, arg1, arg2) \
	gear_call_method(obj, obj_ce, fn_proxy, function_name, sizeof(function_name)-1, retval, 2, arg1, arg2)

#define REGISTER_MAGIC_INTERFACE(class_name, class_name_str) \
	{\
		gear_class_entry ce;\
		INIT_CLASS_ENTRY(ce, # class_name_str, gear_funcs_ ## class_name) \
		gear_ce_ ## class_name = gear_register_internal_interface(&ce);\
		gear_ce_ ## class_name->interface_gets_implemented = gear_implement_ ## class_name;\
	}

#define REGISTER_MAGIC_IMPLEMENT(class_name, interface_name) \
	gear_class_implements(gear_ce_ ## class_name, 1, gear_ce_ ## interface_name)

GEAR_API void gear_user_it_rewind(gear_object_iterator *_iter);
GEAR_API int gear_user_it_valid(gear_object_iterator *_iter);
GEAR_API void gear_user_it_get_current_key(gear_object_iterator *_iter, zval *key);
GEAR_API zval *gear_user_it_get_current_data(gear_object_iterator *_iter);
GEAR_API void gear_user_it_move_forward(gear_object_iterator *_iter);
GEAR_API void gear_user_it_invalidate_current(gear_object_iterator *_iter);

GEAR_API void gear_user_it_new_iterator(gear_class_entry *ce, zval *object, zval *iterator);
GEAR_API gear_object_iterator *gear_user_it_get_new_iterator(gear_class_entry *ce, zval *object, int by_ref);

GEAR_API void gear_register_interfaces(void);

GEAR_API int gear_user_serialize(zval *object, unsigned char **buffer, size_t *buf_len, gear_serialize_data *data);
GEAR_API int gear_user_unserialize(zval *object, gear_class_entry *ce, const unsigned char *buf, size_t buf_len, gear_unserialize_data *data);

GEAR_API int gear_class_serialize_deny(zval *object, unsigned char **buffer, size_t *buf_len, gear_serialize_data *data);
GEAR_API int gear_class_unserialize_deny(zval *object, gear_class_entry *ce, const unsigned char *buf, size_t buf_len, gear_unserialize_data *data);

END_EXTERN_C()

#endif /* GEAR_INTERFACES_H */

