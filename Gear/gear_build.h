/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_BUILD_H
#define GEAR_BUILD_H

#define GEAR_TOSTR_(x) #x
#define GEAR_TOSTR(x) GEAR_TOSTR_(x)

#ifdef ZTS
#define GEAR_BUILD_TS ",TS"
#else
#define GEAR_BUILD_TS ",NTS"
#endif

#if GEAR_DEBUG
#define GEAR_BUILD_DEBUG ",debug"
#else
#define GEAR_BUILD_DEBUG
#endif

#if defined(GEAR_WIN32) && defined(HYSS_COMPILER_ID)
#define GEAR_BUILD_SYSTEM "," HYSS_COMPILER_ID
#else
#define GEAR_BUILD_SYSTEM
#endif

/* for private applications */
#define GEAR_BUILD_EXTRA

#endif

