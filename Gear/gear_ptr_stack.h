/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_PTR_STACK_H
#define GEAR_PTR_STACK_H

typedef struct _gear_ptr_stack {
	int top, max;
	void **elements;
	void **top_element;
	gear_bool persistent;
} gear_ptr_stack;


#define PTR_STACK_BLOCK_SIZE 64

BEGIN_EXTERN_C()
GEAR_API void gear_ptr_stack_init(gear_ptr_stack *stack);
GEAR_API void gear_ptr_stack_init_ex(gear_ptr_stack *stack, gear_bool persistent);
GEAR_API void gear_ptr_stack_n_push(gear_ptr_stack *stack, int count, ...);
GEAR_API void gear_ptr_stack_n_pop(gear_ptr_stack *stack, int count, ...);
GEAR_API void gear_ptr_stack_destroy(gear_ptr_stack *stack);
GEAR_API void gear_ptr_stack_apply(gear_ptr_stack *stack, void (*func)(void *));
GEAR_API void gear_ptr_stack_reverse_apply(gear_ptr_stack *stack, void (*func)(void *));
GEAR_API void gear_ptr_stack_clean(gear_ptr_stack *stack, void (*func)(void *), gear_bool free_elements);
GEAR_API int gear_ptr_stack_num_elements(gear_ptr_stack *stack);
END_EXTERN_C()

#define GEAR_PTR_STACK_RESIZE_IF_NEEDED(stack, count)		\
	if (stack->top+count > stack->max) {					\
		/* we need to allocate more memory */				\
		do {												\
			stack->max += PTR_STACK_BLOCK_SIZE;				\
		} while (stack->top+count > stack->max);			\
		stack->elements = (void **) perealloc(stack->elements, (sizeof(void *) * (stack->max)), stack->persistent);	\
		stack->top_element = stack->elements+stack->top;	\
	}

/*	Not doing this with a macro because of the loop unrolling in the element assignment.
	Just using a macro for 3 in the body for readability sake. */
static gear_always_inline void gear_ptr_stack_3_push(gear_ptr_stack *stack, void *a, void *b, void *c)
{
#define GEAR_PTR_STACK_NUM_ARGS 3

	GEAR_PTR_STACK_RESIZE_IF_NEEDED(stack, GEAR_PTR_STACK_NUM_ARGS)

	stack->top += GEAR_PTR_STACK_NUM_ARGS;
	*(stack->top_element++) = a;
	*(stack->top_element++) = b;
	*(stack->top_element++) = c;

#undef GEAR_PTR_STACK_NUM_ARGS
}

static gear_always_inline void gear_ptr_stack_2_push(gear_ptr_stack *stack, void *a, void *b)
{
#define GEAR_PTR_STACK_NUM_ARGS 2

	GEAR_PTR_STACK_RESIZE_IF_NEEDED(stack, GEAR_PTR_STACK_NUM_ARGS)

	stack->top += GEAR_PTR_STACK_NUM_ARGS;
	*(stack->top_element++) = a;
	*(stack->top_element++) = b;

#undef GEAR_PTR_STACK_NUM_ARGS
}

static gear_always_inline void gear_ptr_stack_3_pop(gear_ptr_stack *stack, void **a, void **b, void **c)
{
	*a = *(--stack->top_element);
	*b = *(--stack->top_element);
	*c = *(--stack->top_element);
	stack->top -= 3;
}

static gear_always_inline void gear_ptr_stack_2_pop(gear_ptr_stack *stack, void **a, void **b)
{
	*a = *(--stack->top_element);
	*b = *(--stack->top_element);
	stack->top -= 2;
}

static gear_always_inline void gear_ptr_stack_push(gear_ptr_stack *stack, void *ptr)
{
	GEAR_PTR_STACK_RESIZE_IF_NEEDED(stack, 1)

	stack->top++;
	*(stack->top_element++) = ptr;
}

static gear_always_inline void *gear_ptr_stack_pop(gear_ptr_stack *stack)
{
	stack->top--;
	return *(--stack->top_element);
}

static gear_always_inline void *gear_ptr_stack_top(gear_ptr_stack *stack)
{
    return stack->elements[stack->top - 1];
}

#endif /* GEAR_PTR_STACK_H */

