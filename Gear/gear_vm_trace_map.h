/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear_vm_handlers.h"
#include "gear_sort.h"

#define GEN_MAP(n, name) do { \
		ZVAL_LONG(&tmp, (gear_long)(uintptr_t)gear_opcode_handlers[n]); \
		gear_hash_str_add(&vm_trace_ht, #name, sizeof(#name) - 1, &tmp); \
	} while (0);

#define VM_TRACE_START() do { \
		zval tmp; \
		gear_hash_init(&vm_trace_ht, 0, NULL, NULL, 1); \
		VM_HANDLERS(GEN_MAP) \
		gear_vm_trace_init(); \
	} while (0)

#ifdef _WIN64
# define ADDR_FMT "%016I64x"
#elif SIZEOF_SIZE_T == 4
# define ADDR_FMT "%08zx"
#elif SIZEOF_SIZE_T == 8
# define ADDR_FMT "%016zx"
#else
# error "Unknown SIZEOF_SIZE_T"
#endif

static HashTable vm_trace_ht;

static int gear_vm_trace_compare(const Bucket *p1, const Bucket *p2)
{
	if (Z_LVAL(p1->val) > Z_LVAL(p2->val)) {
		return 1;
	} else if (Z_LVAL(p1->val) < Z_LVAL(p2->val)) {
		return -1;
	} else {
		return 0;
	}
}

static void gear_vm_trace_init(void)
{
	FILE *f;
	gear_string *key, *prev_key;
	zval *val;
	gear_long prev_addr;

	f = fopen("gear_vm.map", "w+");
	if (f) {
		gear_hash_sort(&vm_trace_ht, (compare_func_t)gear_vm_trace_compare, 0);
		prev_key = NULL;
		GEAR_HASH_FOREACH_STR_KEY_VAL(&vm_trace_ht, key, val) {
			if (prev_key) {
				fprintf(f, ADDR_FMT" "ADDR_FMT" t %s\n", prev_addr, Z_LVAL_P(val) - prev_addr, ZSTR_VAL(prev_key));
			}
			prev_key  = key;
			prev_addr = Z_LVAL_P(val);
		} GEAR_HASH_FOREACH_END();
		if (prev_key) {
			fprintf(f, ADDR_FMT" "ADDR_FMT" t %s\n", prev_addr, 0, ZSTR_VAL(prev_key));
		}
		fclose(f);
	}
	gear_hash_destroy(&vm_trace_ht);
}
