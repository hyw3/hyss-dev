/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"

static gear_class_entry gear_iterator_class_entry;

static void iter_wrapper_free(gear_object *object);
static void iter_wrapper_dtor(gear_object *object);

static const gear_object_handlers iterator_object_handlers = {
	0,
	iter_wrapper_free,
	iter_wrapper_dtor,
	NULL,
	NULL, /* prop read */
	NULL, /* prop write */
	NULL, /* read dim */
	NULL, /* write dim */
	NULL,
	NULL, /* get */
	NULL, /* set */
	NULL, /* has prop */
	NULL, /* unset prop */
	NULL, /* has dim */
	NULL, /* unset dim */
	NULL, /* props get */
	NULL, /* method get */
	NULL, /* call */
	NULL, /* get ctor */
	NULL, /* get class name */
	NULL, /* compare */
	NULL, /* cast */
	NULL, /* count */
	NULL, /* get_debug_info */
	NULL, /* get_closure */
	NULL, /* get_gc */
	NULL, /* do_operation */
	NULL  /* compare */
};

GEAR_API void gear_register_iterator_wrapper(void)
{
	INIT_CLASS_ENTRY(gear_iterator_class_entry, "__iterator_wrapper", NULL);
}

static void iter_wrapper_free(gear_object *object)
{
	gear_object_iterator *iter = (gear_object_iterator*)object;
	iter->funcs->dtor(iter);
}

static void iter_wrapper_dtor(gear_object *object)
{
}

GEAR_API void gear_iterator_init(gear_object_iterator *iter)
{
	gear_object_std_init(&iter->std, &gear_iterator_class_entry);
	iter->std.handlers = &iterator_object_handlers;
}

GEAR_API void gear_iterator_dtor(gear_object_iterator *iter)
{
	if (GC_DELREF(&iter->std) > 0) {
		return;
	}

	gear_objects_store_del(&iter->std);
}

GEAR_API gear_object_iterator* gear_iterator_unwrap(zval *array_ptr)
{
	GEAR_ASSERT(Z_TYPE_P(array_ptr) == IS_OBJECT);
	if (Z_OBJ_HT_P(array_ptr) == &iterator_object_handlers) {
		return (gear_object_iterator *)Z_OBJ_P(array_ptr);
	}
	return NULL;
}

