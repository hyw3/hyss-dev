/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_GLOBALS_H
#define GEAR_GLOBALS_H


#include <setjmp.h>

#include "gear_globals_macros.h"

#include "gear_stack.h"
#include "gear_ptr_stack.h"
#include "gear_hash.h"
#include "gear_llist.h"
#include "gear_objects.h"
#include "gear_objects_API.h"
#include "gear_capis.h"
#include "gear_float.h"
#include "gear_multibyte.h"
#include "gear_multiply.h"
#include "gear_arena.h"

/* Define ZTS if you want a thread-safe Gear */
/*#undef ZTS*/

#ifdef ZTS

BEGIN_EXTERN_C()
GEAR_API extern int compiler_globals_id;
GEAR_API extern int executor_globals_id;
END_EXTERN_C()

#endif

#define SYMTABLE_CACHE_SIZE 32


#include "gear_compile.h"

/* excpt.h on Digital Unix 4.0 defines function_table */
#undef function_table

#define GEAR_EARLY_BINDING_COMPILE_TIME 0
#define GEAR_EARLY_BINDING_DELAYED      1
#define GEAR_EARLY_BINDING_DELAYED_ALL  2

typedef struct _gear_vm_stack *gear_vm_stack;
typedef struct _gear_ics_entry gear_ics_entry;


struct _gear_compiler_globals {
	gear_stack loop_var_stack;

	gear_class_entry *active_class_entry;

	gear_string *compiled_filename;

	int gear_lineno;

	gear_op_array *active_op_array;

	HashTable *function_table;	/* function symbol table */
	HashTable *class_table;		/* class table */

	HashTable filenames_table;

	HashTable *auto_globals;

	gear_bool parse_error;
	gear_bool in_compilation;
	gear_bool short_tags;

	gear_bool unclean_shutdown;

	gear_bool ics_parser_unbuffered_errors;

	gear_llist open_files;

	struct _gear_ics_parser_param *ics_parser_param;

	uint32_t start_lineno;
	gear_bool increment_lineno;

	gear_string *doc_comment;
	uint32_t extra_fn_flags;

	uint32_t compiler_options; /* set of GEAR_COMPILE_* constants */

	gear_oparray_context context;
	gear_file_context file_context;

	gear_arena *arena;

	HashTable interned_strings;

	const gear_encoding **script_encoding_list;
	size_t script_encoding_list_size;
	gear_bool multibyte;
	gear_bool detect_unicode;
	gear_bool encoding_declared;

	gear_ast *ast;
	gear_arena *ast_arena;

	gear_stack delayed_oplines_stack;

#ifdef ZTS
	zval **static_members_table;
	int last_static_member;
#endif
};


struct _gear_executor_globals {
	zval uninitialized_zval;
	zval error_zval;

	/* symbol table cache */
	gear_array *symtable_cache[SYMTABLE_CACHE_SIZE];
	gear_array **symtable_cache_limit;
	gear_array **symtable_cache_ptr;

	gear_array symbol_table;		/* main symbol table */

	HashTable included_files;	/* files already included */

	JMP_BUF *bailout;

	int error_reporting;
	int exit_status;

	HashTable *function_table;	/* function symbol table */
	HashTable *class_table;		/* class table */
	HashTable *gear_constants;	/* constants table */

	zval          *vm_stack_top;
	zval          *vm_stack_end;
	gear_vm_stack  vm_stack;
	size_t         vm_stack_page_size;

	struct _gear_execute_data *current_execute_data;
	gear_class_entry *fake_scope; /* used to avoid checks accessing properties */

	gear_long precision;

	int ticks_count;

	uint32_t persistent_constants_count;
	uint32_t persistent_functions_count;
	uint32_t persistent_classes_count;

	HashTable *in_autoload;
	gear_function *autoload_func;
	gear_bool full_tables_cleanup;

	/* for extended information support */
	gear_bool no_extensions;

	gear_bool vm_interrupt;
	gear_bool timed_out;
	gear_long hard_timeout;

#ifdef GEAR_WIN32
	OSVERSIONINFOEX windows_version_info;
#endif

	HashTable regular_list;
	HashTable persistent_list;

	int user_error_handler_error_reporting;
	zval user_error_handler;
	zval user_exception_handler;
	gear_stack user_error_handlers_error_reporting;
	gear_stack user_error_handlers;
	gear_stack user_exception_handlers;

	gear_error_handling_t  error_handling;
	gear_class_entry      *exception_class;

	/* timeout support */
	gear_long timeout_seconds;

	int lambda_count;

	HashTable *ics_directives;
	HashTable *modified_ics_directives;
	gear_ics_entry *error_reporting_ics_entry;

	gear_objects_store objects_store;
	gear_object *exception, *prev_exception;
	const gear_op *opline_before_exception;
	gear_op exception_op[3];

	struct _gear_capi_entry *current_capi;

	gear_bool active;
	gear_uchar flags;

	gear_long assertions;

	uint32_t           ht_iterators_count;     /* number of allocatd slots */
	uint32_t           ht_iterators_used;      /* number of used slots */
	HashTableIterator *ht_iterators;
	HashTableIterator  ht_iterators_slots[16];

	void *saved_fpu_cw_ptr;
#if XPFPA_HAVE_CW
	XPFPA_CW_DATATYPE saved_fpu_cw;
#endif

	gear_function trampoline;
	gear_op       call_trampoline_op;

	gear_bool each_deprecation_thrown;

	void *reserved[GEAR_MAX_RESERVED_RESOURCES];
};

#define EG_FLAGS_INITIAL				(0)
#define EG_FLAGS_IN_SHUTDOWN			(1<<0)
#define EG_FLAGS_OBJECT_STORE_NO_REUSE	(1<<1)

struct _gear_ics_scanner_globals {
	gear_file_handle *yy_in;
	gear_file_handle *yy_out;

	unsigned int yy_leng;
	unsigned char *yy_start;
	unsigned char *yy_text;
	unsigned char *yy_cursor;
	unsigned char *yy_marker;
	unsigned char *yy_limit;
	int yy_state;
	gear_stack state_stack;

	char *filename;
	int lineno;

	/* Modes are: GEAR_ICS_SCANNER_NORMAL, GEAR_ICS_SCANNER_RAW, GEAR_ICS_SCANNER_TYPED */
	int scanner_mode;
};

typedef enum {
	ON_TOKEN,
	ON_FEEDBACK,
	ON_STOP
} gear_hyss_scanner_event;

struct _gear_hyss_scanner_globals {
	gear_file_handle *yy_in;
	gear_file_handle *yy_out;

	unsigned int yy_leng;
	unsigned char *yy_start;
	unsigned char *yy_text;
	unsigned char *yy_cursor;
	unsigned char *yy_marker;
	unsigned char *yy_limit;
	int yy_state;
	gear_stack state_stack;
	gear_ptr_stack heredoc_label_stack;
	gear_bool heredoc_scan_ahead;
	int heredoc_indentation;
	gear_bool heredoc_indentation_uses_spaces;

	/* original (unfiltered) script */
	unsigned char *script_org;
	size_t script_org_size;

	/* filtered script */
	unsigned char *script_filtered;
	size_t script_filtered_size;

	/* input/output filters */
	gear_encoding_filter input_filter;
	gear_encoding_filter output_filter;
	const gear_encoding *script_encoding;

	/* initial string length after scanning to first variable */
	int scanned_string_len;

	/* hooks */
	void (*on_event)(gear_hyss_scanner_event event, int token, int line, void *context);
	void *on_event_context;
};

#endif /* GEAR_GLOBALS_H */

