/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_COMPILE_H
#define GEAR_COMPILE_H

#include "gear.h"
#include "gear_ast.h"

#ifdef HAVE_STDARG_H
# include <stdarg.h>
#endif

#include "gear_llist.h"

#define SET_UNUSED(op)  op ## _type = IS_UNUSED

#define MAKE_NOP(opline) do { \
	(opline)->op1.num = 0; \
	(opline)->op2.num = 0; \
	(opline)->result.num = 0; \
	(opline)->opcode = GEAR_NOP; \
	(opline)->op1_type =  IS_UNUSED; \
	(opline)->op2_type = IS_UNUSED; \
	(opline)->result_type = IS_UNUSED; \
} while (0)

#define RESET_DOC_COMMENT() do { \
	if (CG(doc_comment)) { \
		gear_string_release_ex(CG(doc_comment), 0); \
		CG(doc_comment) = NULL; \
	} \
} while (0)

typedef struct _gear_op_array gear_op_array;
typedef struct _gear_op gear_op;

/* On 64-bit systems less optimal, but more compact VM code leads to better
 * performance. So on 32-bit systems we use absolute addresses for jump
 * targets and constants, but on 64-bit systems realtive 32-bit offsets */
#if SIZEOF_SIZE_T == 4
# define GEAR_USE_ABS_JMP_ADDR      1
# define GEAR_USE_ABS_CONST_ADDR    1
# define GEAR_EX_USE_RUN_TIME_CACHE 1
#else
# define GEAR_USE_ABS_JMP_ADDR      0
# define GEAR_USE_ABS_CONST_ADDR    0
# define GEAR_EX_USE_RUN_TIME_CACHE 1
#endif

typedef union _znode_op {
	uint32_t      constant;
	uint32_t      var;
	uint32_t      num;
	uint32_t      opline_num; /*  Needs to be signed */
#if GEAR_USE_ABS_JMP_ADDR
	gear_op       *jmp_addr;
#else
	uint32_t      jmp_offset;
#endif
#if GEAR_USE_ABS_CONST_ADDR
	zval          *zv;
#endif
} znode_op;

typedef struct _znode { /* used only during compilation */
	gear_uchar op_type;
	gear_uchar flag;
	union {
		znode_op op;
		zval constant; /* replaced by literal/zv */
	} u;
} znode;

/* Temporarily defined here, to avoid header ordering issues */
typedef struct _gear_ast_znode {
	gear_ast_kind kind;
	gear_ast_attr attr;
	uint32_t lineno;
	znode node;
} gear_ast_znode;

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_znode(znode *node);

static gear_always_inline znode *gear_ast_get_znode(gear_ast *ast) {
	return &((gear_ast_znode *) ast)->node;
}

typedef struct _gear_declarables {
	gear_long ticks;
} gear_declarables;

/* Compilation context that is different for each file, but shared between op arrays. */
typedef struct _gear_file_context {
	gear_declarables declarables;
	znode implementing_class;

	gear_string *current_namespace;
	gear_bool in_namespace;
	gear_bool has_bracketed_namespaces;

	HashTable *imports;
	HashTable *imports_function;
	HashTable *imports_const;

	HashTable seen_symbols;
} gear_file_context;

typedef union _gear_parser_stack_elem {
	gear_ast *ast;
	gear_string *str;
	gear_ulong num;
} gear_parser_stack_elem;

void gear_compile_top_stmt(gear_ast *ast);
void gear_compile_stmt(gear_ast *ast);
void gear_compile_expr(znode *node, gear_ast *ast);
void gear_compile_var(znode *node, gear_ast *ast, uint32_t type);
void gear_eval_const_expr(gear_ast **ast_ptr);
void gear_const_expr_to_zval(zval *result, gear_ast *ast);

typedef int (*user_opcode_handler_t) (gear_execute_data *execute_data);

struct _gear_op {
	const void *handler;
	znode_op op1;
	znode_op op2;
	znode_op result;
	uint32_t extended_value;
	uint32_t lineno;
	gear_uchar opcode;
	gear_uchar op1_type;
	gear_uchar op2_type;
	gear_uchar result_type;
};


typedef struct _gear_brk_cont_element {
	int start;
	int cont;
	int brk;
	int parent;
	gear_bool is_switch;
} gear_brk_cont_element;

typedef struct _gear_label {
	int brk_cont;
	uint32_t opline_num;
} gear_label;

typedef struct _gear_try_catch_element {
	uint32_t try_op;
	uint32_t catch_op;  /* ketchup! */
	uint32_t finally_op;
	uint32_t finally_end;
} gear_try_catch_element;

#define GEAR_LIVE_TMPVAR  0
#define GEAR_LIVE_LOOP    1
#define GEAR_LIVE_SILENCE 2
#define GEAR_LIVE_ROPE    3
#define GEAR_LIVE_MASK    3

typedef struct _gear_live_range {
	uint32_t var; /* low bits are used for variable type (GEAR_LIVE_* macros) */
	uint32_t start;
	uint32_t end;
} gear_live_range;

/* Compilation context that is different for each op array. */
typedef struct _gear_oparray_context {
	uint32_t   opcodes_size;
	int        vars_size;
	int        literals_size;
	int        backpatch_count;
	uint32_t   fast_call_var;
	uint32_t   try_catch_offset;
	int        current_brk_cont;
	int        last_brk_cont;
	gear_brk_cont_element *brk_cont_array;
	HashTable *labels;
} gear_oparray_context;

/* Class, property and method flags                  class|meth.|prop.|const*/
/*                                                        |     |     |     */
/* Common flags                                           |     |     |     */
/* ============                                           |     |     |     */
/*                                                        |     |     |     */
/* Staic method or property                               |     |     |     */
#define GEAR_ACC_STATIC                  (1 <<  0) /*     |  X  |  X  |     */
/*                                                        |     |     |     */
/* Final class or method                                  |     |     |     */
#define GEAR_ACC_FINAL                   (1 <<  2) /*  X  |  X  |     |     */
/*                                                        |     |     |     */
/* Visibility flags (public < protected < private)        |     |     |     */
#define GEAR_ACC_PUBLIC                  (1 <<  8) /*     |  X  |  X  |  X  */
#define GEAR_ACC_PROTECTED               (1 <<  9) /*     |  X  |  X  |  X  */
#define GEAR_ACC_PRIVATE                 (1 << 10) /*     |  X  |  X  |  X  */
/*                                                        |     |     |     */
/* TODO: explain the name ???                             |     |     |     */
#define GEAR_ACC_CHANGED                 (1 << 11) /*     |  X  |  X  |     */
/*                                                        |     |     |     */
/* TODO: used only by extslib/reflection ???                  |     |     |     */
#define GEAR_ACC_IMPLICIT_PUBLIC         (1 << 12) /*     |  ?  |  ?  |  ?  */
/*                                                        |     |     |     */
/* Shadow of parent's private method/property             |     |     |     */
#define GEAR_ACC_SHADOW                  (1 << 17) /*     |  ?  |  X  |     */
/*                                                        |     |     |     */
/* Class Flags (unused: 0, 1, 3, 11-18, 21, 25...)        |     |     |     */
/* ===========                                            |     |     |     */
/*                                                        |     |     |     */
/* class is abstarct, since it is set by any              |     |     |     */
/* abstract method                                        |     |     |     */
#define GEAR_ACC_IMPLICIT_ABSTRACT_CLASS (1 <<  4) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Class is explicitly defined as abstract by using       |     |     |     */
/* the keyword.                                           |     |     |     */
#define GEAR_ACC_EXPLICIT_ABSTRACT_CLASS (1 <<  5) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Special class types                                    |     |     |     */
#define GEAR_ACC_INTERFACE               (1 <<  6) /*  X  |     |     |     */
#define GEAR_ACC_TRAIT                   (1 <<  7) /*  X  |     |     |     */
#define GEAR_ACC_ANON_CLASS              (1 <<  8) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Bound anonymous class                                  |     |     |     */
#define GEAR_ACC_ANON_BOUND              (1 <<  9) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Class extends another class                            |     |     |     */
#define GEAR_ACC_INHERITED               (1 << 10) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Class implements interface(s)                          |     |     |     */
#define GEAR_ACC_IMPLEMENT_INTERFACES    (1 << 19) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Class constants updated                                |     |     |     */
#define GEAR_ACC_CONSTANTS_UPDATED       (1 << 20) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Class uses trait(s)                                    |     |     |     */
#define GEAR_ACC_IMPLEMENT_TRAITS        (1 << 22) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* User class has methods with static variables           |     |     |     */
#define GEAR_HAS_STATIC_IN_METHODS       (1 << 23) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Class has magic methods __get/__set/__unset/           |     |     |     */
/* __isset that use guards                                |     |     |     */
#define GEAR_ACC_USE_GUARDS              (1 << 24) /*  X  |     |     |     */
/*                                                        |     |     |     */
/* Function Flags (unused: 4, 5, 17?)                     |     |     |     */
/* ==============                                         |     |     |     */
/*                                                        |     |     |     */
/* Abstarct method                                        |     |     |     */
#define GEAR_ACC_ABSTRACT                (1 <<  1) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* TODO: used only during inheritance ???                 |     |     |     */
#define GEAR_ACC_IMPLEMENTED_ABSTRACT    (1 <<  3) /*     |  X  |     |     */
/*                                                        |     |     |     */
#define GEAR_ACC_FAKE_CLOSURE            (1 <<  6) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* method flag used by Closure::__invoke()                |     |     |     */
#define GEAR_ACC_USER_ARG_INFO           (1 <<  7) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* method flags (special method detection)                |     |     |     */
#define GEAR_ACC_CTOR                    (1 << 13) /*     |  X  |     |     */
#define GEAR_ACC_DTOR                    (1 << 14) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* "main" op_array with                                   |     |     |     */
/* GEAR_DECLARE_INHERITED_CLASS_DELAYED opcodes           |     |     |     */
#define GEAR_ACC_EARLY_BINDING           (1 << 15) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* method flag (bc only), any method that has this        |     |     |     */
/* flag can be used statically and non statically.        |     |     |     */
#define GEAR_ACC_ALLOW_STATIC            (1 << 16) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* deprecation flag                                       |     |     |     */
#define GEAR_ACC_DEPRECATED              (1 << 18) /*     |  X  |     |     */
/*                                                        |     |     |     */
#define GEAR_ACC_NO_RT_ARENA             (1 << 19) /*     |  X  |     |     */
/*                                                        |     |     |     */
#define GEAR_ACC_CLOSURE                 (1 << 20) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* call through user function trampoline. e.g.            |     |     |     */
/* __call, __callstatic                                   |     |     |     */
#define GEAR_ACC_CALL_VIA_TRAMPOLINE     (1 << 21) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* disable inline caching                                 |     |     |     */
#define GEAR_ACC_NEVER_CACHE             (1 << 22) /*     |  X  |     |     */
/*                                                        |     |     |     */
#define GEAR_ACC_GENERATOR               (1 << 23) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* Function with varable number of arguments              |     |     |     */
#define GEAR_ACC_VARIADIC                (1 << 24) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* Immutable op_array (lazy loading)                      |     |     |     */
#define GEAR_ACC_IMMUTABLE               (1 << 25) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* Function returning by reference                        |     |     |     */
#define GEAR_ACC_RETURN_REFERENCE        (1 << 26) /*     |  X  |     |     */
/*                                                        |     |     |     */
#define GEAR_ACC_DONE_PASS_TWO           (1 << 27) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* Function has typed arguments                           |     |     |     */
#define GEAR_ACC_HAS_TYPE_HINTS          (1 << 28) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* op_array has finally blocks (user only)                |     |     |     */
#define GEAR_ACC_HAS_FINALLY_BLOCK       (1 << 29) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* internal function is allocated at arena (int only)     |     |     |     */
#define GEAR_ACC_ARENA_ALLOCATED         (1 << 29) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* Function has a return type                             |     |     |     */
#define GEAR_ACC_HAS_RETURN_TYPE         (1 << 30) /*     |  X  |     |     */
/*                                                        |     |     |     */
/* op_array uses strict mode types                        |     |     |     */
#define GEAR_ACC_STRICT_TYPES            (1 << 31) /*     |  X  |     |     */


#define GEAR_ACC_PPP_MASK  (GEAR_ACC_PUBLIC | GEAR_ACC_PROTECTED | GEAR_ACC_PRIVATE)

/* call through internal function handler. e.g. Closure::invoke() */
#define GEAR_ACC_CALL_VIA_HANDLER     GEAR_ACC_CALL_VIA_TRAMPOLINE

char *gear_visibility_string(uint32_t fn_flags);

typedef struct _gear_property_info {
	uint32_t offset; /* property offset for object properties or
	                      property index for static properties */
	uint32_t flags;
	gear_string *name;
	gear_string *doc_comment;
	gear_class_entry *ce;
} gear_property_info;

#define OBJ_PROP(obj, offset) \
	((zval*)((char*)(obj) + offset))
#define OBJ_PROP_NUM(obj, num) \
	(&(obj)->properties_table[(num)])
#define OBJ_PROP_TO_OFFSET(num) \
	((uint32_t)(gear_uintptr_t)OBJ_PROP_NUM(((gear_object*)NULL), num))
#define OBJ_PROP_TO_NUM(offset) \
	((offset - OBJ_PROP_TO_OFFSET(0)) / sizeof(zval))

typedef struct _gear_class_constant {
	zval value; /* access flags are stored in reserved: zval.u2.access_flags */
	gear_string *doc_comment;
	gear_class_entry *ce;
} gear_class_constant;

/* arg_info for internal functions */
typedef struct _gear_internal_arg_info {
	const char *name;
	gear_type type;
	gear_uchar pass_by_reference;
	gear_bool is_variadic;
} gear_internal_arg_info;

/* arg_info for user functions */
typedef struct _gear_arg_info {
	gear_string *name;
	gear_type type;
	gear_uchar pass_by_reference;
	gear_bool is_variadic;
} gear_arg_info;

/* the following structure repeats the layout of gear_internal_arg_info,
 * but its fields have different meaning. It's used as the first element of
 * arg_info array to define properties of internal functions.
 * It's also used for the return type.
 */
typedef struct _gear_internal_function_info {
	gear_uintptr_t required_num_args;
	gear_type type;
	gear_bool return_reference;
	gear_bool _is_variadic;
} gear_internal_function_info;

struct _gear_op_array {
	/* Common elements */
	gear_uchar type;
	gear_uchar arg_flags[3]; /* bitset of arg_info.pass_by_reference */
	uint32_t fn_flags;
	gear_string *function_name;
	gear_class_entry *scope;
	gear_function *prototype;
	uint32_t num_args;
	uint32_t required_num_args;
	gear_arg_info *arg_info;
	/* END of common elements */

	int cache_size;     /* number of run_time_cache_slots * sizeof(void*) */
	int last_var;       /* number of CV variables */
	uint32_t T;         /* number of temporary variables */
	uint32_t last;      /* number of opcodes */

	gear_op *opcodes;
	void **run_time_cache;
	HashTable *static_variables;
	gear_string **vars; /* names of CV variables */

	uint32_t *refcount;

	int last_live_range;
	int last_try_catch;
	gear_live_range *live_range;
	gear_try_catch_element *try_catch_array;

	gear_string *filename;
	uint32_t line_start;
	uint32_t line_end;
	gear_string *doc_comment;

	int last_literal;
	zval *literals;

	void *reserved[GEAR_MAX_RESERVED_RESOURCES];
};


#define GEAR_RETURN_VALUE				0
#define GEAR_RETURN_REFERENCE			1

/* gear_internal_function_handler */
typedef void (GEAR_FASTCALL *zif_handler)(INTERNAL_FUNCTION_PARAMETERS);

typedef struct _gear_internal_function {
	/* Common elements */
	gear_uchar type;
	gear_uchar arg_flags[3]; /* bitset of arg_info.pass_by_reference */
	uint32_t fn_flags;
	gear_string* function_name;
	gear_class_entry *scope;
	gear_function *prototype;
	uint32_t num_args;
	uint32_t required_num_args;
	gear_internal_arg_info *arg_info;
	/* END of common elements */

	zif_handler handler;
	struct _gear_capi_entry *cAPI;
	void *reserved[GEAR_MAX_RESERVED_RESOURCES];
} gear_internal_function;

#define GEAR_FN_SCOPE_NAME(function)  ((function) && (function)->common.scope ? ZSTR_VAL((function)->common.scope->name) : "")

union _gear_function {
	gear_uchar type;	/* MUST be the first element of this struct! */
	uint32_t   quick_arg_flags;

	struct {
		gear_uchar type;  /* never used */
		gear_uchar arg_flags[3]; /* bitset of arg_info.pass_by_reference */
		uint32_t fn_flags;
		gear_string *function_name;
		gear_class_entry *scope;
		union _gear_function *prototype;
		uint32_t num_args;
		uint32_t required_num_args;
		gear_arg_info *arg_info;
	} common;

	gear_op_array op_array;
	gear_internal_function internal_function;
};

typedef enum _gear_call_kind {
	GEAR_CALL_NESTED_FUNCTION,	/* stackless VM call to function */
	GEAR_CALL_NESTED_CODE,		/* stackless VM call to include/require/eval */
	GEAR_CALL_TOP_FUNCTION,		/* direct VM call to function from external C code */
	GEAR_CALL_TOP_CODE			/* direct VM call to "main" code from external C code */
} gear_call_kind;

struct _gear_execute_data {
	const gear_op       *opline;           /* executed opline                */
	gear_execute_data   *call;             /* current call                   */
	zval                *return_value;
	gear_function       *func;             /* executed function              */
	zval                 This;             /* this + call_info + num_args    */
	gear_execute_data   *prev_execute_data;
	gear_array          *symbol_table;
#if GEAR_EX_USE_RUN_TIME_CACHE
	void               **run_time_cache;   /* cache op_array->run_time_cache */
#endif
};

#define GEAR_CALL_FUNCTION           (0 << 0)
#define GEAR_CALL_CODE               (1 << 0)
#define GEAR_CALL_NESTED             (0 << 1)
#define GEAR_CALL_TOP                (1 << 1)
#define GEAR_CALL_FREE_EXTRA_ARGS    (1 << 2)
#define GEAR_CALL_CTOR               (1 << 3)
#define GEAR_CALL_HAS_SYMBOL_TABLE   (1 << 4)
#define GEAR_CALL_CLOSURE            (1 << 5)
#define GEAR_CALL_RELEASE_THIS       (1 << 6)
#define GEAR_CALL_ALLOCATED          (1 << 7)
#define GEAR_CALL_GENERATOR          (1 << 8)
#define GEAR_CALL_DYNAMIC            (1 << 9)
#define GEAR_CALL_FAKE_CLOSURE       (1 << 10)
#define GEAR_CALL_SEND_ARG_BY_REF    (1 << 11)

#define GEAR_CALL_INFO_SHIFT         16

#define GEAR_CALL_INFO(call) \
	(Z_TYPE_INFO((call)->This) >> GEAR_CALL_INFO_SHIFT)

#define GEAR_CALL_KIND_EX(call_info) \
	(call_info & (GEAR_CALL_CODE | GEAR_CALL_TOP))

#define GEAR_CALL_KIND(call) \
	GEAR_CALL_KIND_EX(GEAR_CALL_INFO(call))

#define GEAR_SET_CALL_INFO(call, object, info) do { \
		Z_TYPE_INFO((call)->This) = ((object) ? IS_OBJECT_EX : IS_UNDEF) | ((info) << GEAR_CALL_INFO_SHIFT); \
	} while (0)

#define GEAR_ADD_CALL_FLAG_EX(call_info, flag) do { \
		call_info |= ((flag) << GEAR_CALL_INFO_SHIFT); \
	} while (0)

#define GEAR_DEL_CALL_FLAG_EX(call_info, flag) do { \
		call_info &= ~((flag) << GEAR_CALL_INFO_SHIFT); \
	} while (0)

#define GEAR_ADD_CALL_FLAG(call, flag) do { \
		GEAR_ADD_CALL_FLAG_EX(Z_TYPE_INFO((call)->This), flag); \
	} while (0)

#define GEAR_DEL_CALL_FLAG(call, flag) do { \
		GEAR_DEL_CALL_FLAG_EX(Z_TYPE_INFO((call)->This), flag); \
	} while (0)

#define GEAR_CALL_NUM_ARGS(call) \
	(call)->This.u2.num_args

#define GEAR_CALL_FRAME_SLOT \
	((int)((GEAR_MM_ALIGNED_SIZE(sizeof(gear_execute_data)) + GEAR_MM_ALIGNED_SIZE(sizeof(zval)) - 1) / GEAR_MM_ALIGNED_SIZE(sizeof(zval))))

#define GEAR_CALL_VAR(call, n) \
	((zval*)(((char*)(call)) + ((int)(n))))

#define GEAR_CALL_VAR_NUM(call, n) \
	(((zval*)(call)) + (GEAR_CALL_FRAME_SLOT + ((int)(n))))

#define GEAR_CALL_ARG(call, n) \
	GEAR_CALL_VAR_NUM(call, ((int)(n)) - 1)

#define EX(element) 			((execute_data)->element)

#define EX_CALL_INFO()			GEAR_CALL_INFO(execute_data)
#define EX_CALL_KIND()			GEAR_CALL_KIND(execute_data)
#define EX_NUM_ARGS()			GEAR_CALL_NUM_ARGS(execute_data)

#define GEAR_CALL_USES_STRICT_TYPES(call) \
	(((call)->func->common.fn_flags & GEAR_ACC_STRICT_TYPES) != 0)

#define EX_USES_STRICT_TYPES() \
	GEAR_CALL_USES_STRICT_TYPES(execute_data)

#define GEAR_ARG_USES_STRICT_TYPES() \
	(EG(current_execute_data)->prev_execute_data && \
	 EG(current_execute_data)->prev_execute_data->func && \
	 GEAR_CALL_USES_STRICT_TYPES(EG(current_execute_data)->prev_execute_data))

#define GEAR_RET_USES_STRICT_TYPES() \
	GEAR_CALL_USES_STRICT_TYPES(EG(current_execute_data))

#define EX_VAR(n)				GEAR_CALL_VAR(execute_data, n)
#define EX_VAR_NUM(n)			GEAR_CALL_VAR_NUM(execute_data, n)

#define EX_VAR_TO_NUM(n)		((uint32_t)(GEAR_CALL_VAR(NULL, n) - GEAR_CALL_VAR_NUM(NULL, 0)))

#define GEAR_OPLINE_TO_OFFSET(opline, target) \
	((char*)(target) - (char*)(opline))

#define GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, opline_num) \
	((char*)&(op_array)->opcodes[opline_num] - (char*)(opline))

#define GEAR_OFFSET_TO_OPLINE(base, offset) \
	((gear_op*)(((char*)(base)) + (int)offset))

#define GEAR_OFFSET_TO_OPLINE_NUM(op_array, base, offset) \
	(GEAR_OFFSET_TO_OPLINE(base, offset) - op_array->opcodes)

#if GEAR_USE_ABS_JMP_ADDR

/* run-time jump target */
# define OP_JMP_ADDR(opline, node) \
	(node).jmp_addr

# define GEAR_SET_OP_JMP_ADDR(opline, node, val) do { \
		(node).jmp_addr = (val); \
	} while (0)

/* convert jump target from compile-time to run-time */
# define GEAR_PASS_TWO_UPDATE_JMP_TARGET(op_array, opline, node) do { \
		(node).jmp_addr = (op_array)->opcodes + (node).opline_num; \
	} while (0)

/* convert jump target back from run-time to compile-time */
# define GEAR_PASS_TWO_UNDO_JMP_TARGET(op_array, opline, node) do { \
		(node).opline_num = (node).jmp_addr - (op_array)->opcodes; \
	} while (0)

#else

/* run-time jump target */
# define OP_JMP_ADDR(opline, node) \
	GEAR_OFFSET_TO_OPLINE(opline, (node).jmp_offset)

# define GEAR_SET_OP_JMP_ADDR(opline, node, val) do { \
		(node).jmp_offset = GEAR_OPLINE_TO_OFFSET(opline, val); \
	} while (0)

/* convert jump target from compile-time to run-time */
# define GEAR_PASS_TWO_UPDATE_JMP_TARGET(op_array, opline, node) do { \
		(node).jmp_offset = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, (node).opline_num); \
	} while (0)

/* convert jump target back from run-time to compile-time */
# define GEAR_PASS_TWO_UNDO_JMP_TARGET(op_array, opline, node) do { \
		(node).opline_num = GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, (node).jmp_offset); \
	} while (0)

#endif

/* constant-time constant */
# define CT_CONSTANT_EX(op_array, num) \
	((op_array)->literals + (num))

# define CT_CONSTANT(node) \
	CT_CONSTANT_EX(CG(active_op_array), (node).constant)

#if GEAR_USE_ABS_CONST_ADDR

/* run-time constant */
# define RT_CONSTANT(opline, node) \
	(node).zv

/* convert constant from compile-time to run-time */
# define GEAR_PASS_TWO_UPDATE_CONSTANT(op_array, opline, node) do { \
		(node).zv = CT_CONSTANT_EX(op_array, (node).constant); \
	} while (0)

#else

/* At run-time, constants are allocated together with op_array->opcodes
 * and addressed relatively to current opline.
 */

/* run-time constant */
# define RT_CONSTANT(opline, node) \
	((zval*)(((char*)(opline)) + (int32_t)(node).constant))

/* convert constant from compile-time to run-time */
# define GEAR_PASS_TWO_UPDATE_CONSTANT(op_array, opline, node) do { \
		(node).constant = \
			(((char*)CT_CONSTANT_EX(op_array, (node).constant)) - \
			((char*)opline)); \
	} while (0)

#endif

/* convert constant back from run-time to compile-time */
#define GEAR_PASS_TWO_UNDO_CONSTANT(op_array, opline, node) do { \
		(node).constant = RT_CONSTANT(opline, node) - (op_array)->literals; \
	} while (0)

#if GEAR_EX_USE_RUN_TIME_CACHE

# define EX_RUN_TIME_CACHE() \
	EX(run_time_cache)

# define EX_LOAD_RUN_TIME_CACHE(op_array) do { \
		EX(run_time_cache) = (op_array)->run_time_cache; \
	} while (0)

#else

# define EX_RUN_TIME_CACHE() \
	EX(func)->op_array.run_time_cache

# define EX_LOAD_RUN_TIME_CACHE(op_array) do { \
	} while (0)

#endif

#define IS_UNUSED	0		/* Unused operand */
#define IS_CONST	(1<<0)
#define IS_TMP_VAR	(1<<1)
#define IS_VAR		(1<<2)
#define IS_CV		(1<<3)	/* Compiled variable */

#define GEAR_EXTRA_VALUE 1

#include "gear_globals.h"

BEGIN_EXTERN_C()

void init_compiler(void);
void shutdown_compiler(void);
void gear_init_compiler_data_structures(void);

void gear_oparray_context_begin(gear_oparray_context *prev_context);
void gear_oparray_context_end(gear_oparray_context *prev_context);
void gear_file_context_begin(gear_file_context *prev_context);
void gear_file_context_end(gear_file_context *prev_context);

extern GEAR_API gear_op_array *(*gear_compile_file)(gear_file_handle *file_handle, int type);
extern GEAR_API gear_op_array *(*gear_compile_string)(zval *source_string, char *filename);

GEAR_API int GEAR_FASTCALL lex_scan(zval *gearlval, gear_parser_stack_elem *elem);
void startup_scanner(void);
void shutdown_scanner(void);

GEAR_API gear_string *gear_set_compiled_filename(gear_string *new_compiled_filename);
GEAR_API void gear_restore_compiled_filename(gear_string *original_compiled_filename);
GEAR_API gear_string *gear_get_compiled_filename(void);
GEAR_API int gear_get_compiled_lineno(void);
GEAR_API size_t gear_get_scanned_file_offset(void);

GEAR_API gear_string *gear_get_compiled_variable_name(const gear_op_array *op_array, uint32_t var);

#ifdef ZTS
const char *gear_get_geartext(void);
int gear_get_gearleng(void);
#endif

typedef int (GEAR_FASTCALL *unary_op_type)(zval *, zval *);
typedef int (GEAR_FASTCALL *binary_op_type)(zval *, zval *, zval *);

GEAR_API unary_op_type get_unary_op(int opcode);
GEAR_API binary_op_type get_binary_op(int opcode);

void gear_stop_lexing(void);
void gear_emit_final_return(int return_one);

/* Used during AST construction */
gear_ast *gear_ast_append_str(gear_ast *left, gear_ast *right);
gear_ast *gear_negate_num_string(gear_ast *ast);
uint32_t gear_add_class_modifier(uint32_t flags, uint32_t new_flag);
uint32_t gear_add_member_modifier(uint32_t flags, uint32_t new_flag);
gear_bool gear_handle_encoding_declaration(gear_ast *ast);

/* parser-driven code generators */
void gear_do_free(znode *op1);

GEAR_API int do_bind_function(const gear_op_array *op_array, const gear_op *opline, HashTable *function_table, gear_bool compile_time);
GEAR_API gear_class_entry *do_bind_class(const gear_op_array *op_array, const gear_op *opline, HashTable *class_table, gear_bool compile_time);
GEAR_API gear_class_entry *do_bind_inherited_class(const gear_op_array *op_array, const gear_op *opline, HashTable *class_table, gear_class_entry *parent_ce, gear_bool compile_time);
GEAR_API uint32_t gear_build_delayed_early_binding_list(const gear_op_array *op_array);
GEAR_API void gear_do_delayed_early_binding(const gear_op_array *op_array, uint32_t first_early_binding_opline);

void gear_do_extended_info(void);
void gear_do_extended_fcall_begin(void);
void gear_do_extended_fcall_end(void);

void gear_verify_namespace(void);

void gear_resolve_goto_label(gear_op_array *op_array, gear_op *opline);

GEAR_API void function_add_ref(gear_function *function);

#define INITIAL_OP_ARRAY_SIZE 64


/* helper functions in gear_language_scanner.l */
GEAR_API gear_op_array *compile_file(gear_file_handle *file_handle, int type);
GEAR_API gear_op_array *compile_string(zval *source_string, char *filename);
GEAR_API gear_op_array *compile_filename(int type, zval *filename);
GEAR_API void gear_try_exception_handler();
GEAR_API int gear_execute_scripts(int type, zval *retval, int file_count, ...);
GEAR_API int open_file_for_scanning(gear_file_handle *file_handle);
GEAR_API void init_op_array(gear_op_array *op_array, gear_uchar type, int initial_ops_size);
GEAR_API void destroy_op_array(gear_op_array *op_array);
GEAR_API void gear_destroy_file_handle(gear_file_handle *file_handle);
GEAR_API void gear_cleanup_internal_class_data(gear_class_entry *ce);
GEAR_API void gear_cleanup_internal_classes(void);

GEAR_API void destroy_gear_function(gear_function *function);
GEAR_API void gear_function_dtor(zval *zv);
GEAR_API void destroy_gear_class(zval *zv);
void gear_class_add_ref(zval *zv);

GEAR_API gear_string *gear_mangle_property_name(const char *src1, size_t src1_length, const char *src2, size_t src2_length, int internal);
#define gear_unmangle_property_name(mangled_property, class_name, prop_name) \
        gear_unmangle_property_name_ex(mangled_property, class_name, prop_name, NULL)
GEAR_API int gear_unmangle_property_name_ex(const gear_string *name, const char **class_name, const char **prop_name, size_t *prop_len);

#define GEAR_FUNCTION_DTOR gear_function_dtor
#define GEAR_CLASS_DTOR destroy_gear_class

GEAR_API int pass_two(gear_op_array *op_array);
GEAR_API gear_bool gear_is_compiling(void);
GEAR_API char *gear_make_compiled_string_description(const char *name);
GEAR_API void gear_initialize_class_data(gear_class_entry *ce, gear_bool nullify_handlers);
uint32_t gear_get_class_fetch_type(gear_string *name);
GEAR_API gear_uchar gear_get_call_op(const gear_op *init_op, gear_function *fbc);
GEAR_API int gear_is_smart_branch(gear_op *opline);

static gear_always_inline uint32_t get_next_op_number(gear_op_array *op_array)
{
	return op_array->last;
}

typedef gear_bool (*gear_auto_global_callback)(gear_string *name);
typedef struct _gear_auto_global {
	gear_string *name;
	gear_auto_global_callback auto_global_callback;
	gear_bool jit;
	gear_bool armed;
} gear_auto_global;

GEAR_API int gear_register_auto_global(gear_string *name, gear_bool jit, gear_auto_global_callback auto_global_callback);
GEAR_API void gear_activate_auto_globals(void);
GEAR_API gear_bool gear_is_auto_global(gear_string *name);
GEAR_API gear_bool gear_is_auto_global_str(char *name, size_t len);
GEAR_API size_t gear_dirname(char *path, size_t len);
GEAR_API void gear_set_function_arg_flags(gear_function *func);

int GEAR_FASTCALL gearlex(gear_parser_stack_elem *elem);

int gear_add_literal(gear_op_array *op_array, zval *zv);

void gear_assert_valid_class_name(const gear_string *const_name);

/* BEGIN: OPCODES */

#include "gear_vm_opcodes.h"

/* END: OPCODES */

/* class fetches */
#define GEAR_FETCH_CLASS_DEFAULT	0
#define GEAR_FETCH_CLASS_SELF		1
#define GEAR_FETCH_CLASS_PARENT		2
#define GEAR_FETCH_CLASS_STATIC		3
#define GEAR_FETCH_CLASS_AUTO		4
#define GEAR_FETCH_CLASS_INTERFACE	5
#define GEAR_FETCH_CLASS_TRAIT		6
#define GEAR_FETCH_CLASS_MASK        0x0f
#define GEAR_FETCH_CLASS_NO_AUTOLOAD 0x80
#define GEAR_FETCH_CLASS_SILENT      0x0100
#define GEAR_FETCH_CLASS_EXCEPTION   0x0200

#define GEAR_PARAM_REF      (1<<0)
#define GEAR_PARAM_VARIADIC (1<<1)

#define GEAR_NAME_FQ       0
#define GEAR_NAME_NOT_FQ   1
#define GEAR_NAME_RELATIVE 2

#define GEAR_TYPE_NULLABLE (1<<8)

#define GEAR_ARRAY_SYNTAX_LIST 1  /* list() */
#define GEAR_ARRAY_SYNTAX_LONG 2  /* array() */
#define GEAR_ARRAY_SYNTAX_SHORT 3 /* [] */

/* var status for backpatching */
#define BP_VAR_R			0
#define BP_VAR_W			1
#define BP_VAR_RW			2
#define BP_VAR_IS			3
#define BP_VAR_FUNC_ARG		4
#define BP_VAR_UNSET		5

#define GEAR_INTERNAL_FUNCTION				1
#define GEAR_USER_FUNCTION					2
#define GEAR_OVERLOADED_FUNCTION			3
#define	GEAR_EVAL_CODE						4
#define GEAR_OVERLOADED_FUNCTION_TEMPORARY	5

/* A quick check (type == GEAR_USER_FUNCTION || type == GEAR_EVAL_CODE) */
#define GEAR_USER_CODE(type) ((type & 1) == 0)

#define GEAR_INTERNAL_CLASS         1
#define GEAR_USER_CLASS             2

#define GEAR_EVAL				(1<<0)
#define GEAR_INCLUDE			(1<<1)
#define GEAR_INCLUDE_ONCE		(1<<2)
#define GEAR_REQUIRE			(1<<3)
#define GEAR_REQUIRE_ONCE		(1<<4)

#define GEAR_CT	(1<<0)
#define GEAR_RT (1<<1)

/* global/local fetches */
#define GEAR_FETCH_GLOBAL		(1<<1)
#define GEAR_FETCH_LOCAL		(1<<2)
#define GEAR_FETCH_GLOBAL_LOCK	(1<<3)

#define GEAR_FETCH_TYPE_MASK	0xe

#define GEAR_ISEMPTY			(1<<0)

#define GEAR_LAST_CATCH			(1<<0)

#define GEAR_FREE_ON_RETURN     (1<<0)

#define GEAR_SEND_BY_VAL     0
#define GEAR_SEND_BY_REF     1
#define GEAR_SEND_PREFER_REF 2

#define GEAR_DIM_IS 1

#define IS_CONSTANT_UNQUALIFIED     0x010
#define IS_CONSTANT_CLASS           0x080  /* __CLASS__ in trait */
#define IS_CONSTANT_IN_NAMESPACE    0x100

static gear_always_inline int gear_check_arg_send_type(const gear_function *zf, uint32_t arg_num, uint32_t mask)
{
	arg_num--;
	if (UNEXPECTED(arg_num >= zf->common.num_args)) {
		if (EXPECTED((zf->common.fn_flags & GEAR_ACC_VARIADIC) == 0)) {
			return 0;
		}
		arg_num = zf->common.num_args;
	}
	return UNEXPECTED((zf->common.arg_info[arg_num].pass_by_reference & mask) != 0);
}

#define ARG_MUST_BE_SENT_BY_REF(zf, arg_num) \
	gear_check_arg_send_type(zf, arg_num, GEAR_SEND_BY_REF)

#define ARG_SHOULD_BE_SENT_BY_REF(zf, arg_num) \
	gear_check_arg_send_type(zf, arg_num, GEAR_SEND_BY_REF|GEAR_SEND_PREFER_REF)

#define ARG_MAY_BE_SENT_BY_REF(zf, arg_num) \
	gear_check_arg_send_type(zf, arg_num, GEAR_SEND_PREFER_REF)

/* Quick API to check firat 12 arguments */
#define MAX_ARG_FLAG_NUM 12

#ifdef WORDS_BIGENDIAN
# define GEAR_SET_ARG_FLAG(zf, arg_num, mask) do { \
		(zf)->quick_arg_flags |= ((mask) << ((arg_num) - 1) * 2); \
	} while (0)
# define GEAR_CHECK_ARG_FLAG(zf, arg_num, mask) \
	(((zf)->quick_arg_flags >> (((arg_num) - 1) * 2)) & (mask))
#else
# define GEAR_SET_ARG_FLAG(zf, arg_num, mask) do { \
		(zf)->quick_arg_flags |= (((mask) << 6) << (arg_num) * 2); \
	} while (0)
# define GEAR_CHECK_ARG_FLAG(zf, arg_num, mask) \
	(((zf)->quick_arg_flags >> (((arg_num) + 3) * 2)) & (mask))
#endif

#define QUICK_ARG_MUST_BE_SENT_BY_REF(zf, arg_num) \
	GEAR_CHECK_ARG_FLAG(zf, arg_num, GEAR_SEND_BY_REF)

#define QUICK_ARG_SHOULD_BE_SENT_BY_REF(zf, arg_num) \
	GEAR_CHECK_ARG_FLAG(zf, arg_num, GEAR_SEND_BY_REF|GEAR_SEND_PREFER_REF)

#define QUICK_ARG_MAY_BE_SENT_BY_REF(zf, arg_num) \
	GEAR_CHECK_ARG_FLAG(zf, arg_num, GEAR_SEND_PREFER_REF)

#define GEAR_RETURN_VAL 0
#define GEAR_RETURN_REF 1

#define GEAR_BIND_VAL 0
#define GEAR_BIND_REF 1

#define GEAR_RETURNS_FUNCTION (1<<0)
#define GEAR_RETURNS_VALUE    (1<<1)

#define GEAR_ARRAY_ELEMENT_REF		(1<<0)
#define GEAR_ARRAY_NOT_PACKED		(1<<1)
#define GEAR_ARRAY_SIZE_SHIFT		2

/* For "use" AST nodes and the seen symbol table */
#define GEAR_SYMBOL_CLASS    (1<<0)
#define GEAR_SYMBOL_FUNCTION (1<<1)
#define GEAR_SYMBOL_CONST    (1<<2)

/* Pseudo-opcodes that are used only temporarily during compilation */
#define GEAR_GOTO  253
#define GEAR_BRK   254
#define GEAR_CONT  255


END_EXTERN_C()

#define GEAR_CLONE_FUNC_NAME		"__clone"
#define GEAR_CONSTRUCTOR_FUNC_NAME	"__construct"
#define GEAR_DESTRUCTOR_FUNC_NAME	"__destruct"
#define GEAR_GET_FUNC_NAME          "__get"
#define GEAR_SET_FUNC_NAME          "__set"
#define GEAR_UNSET_FUNC_NAME        "__unset"
#define GEAR_ISSET_FUNC_NAME        "__isset"
#define GEAR_CALL_FUNC_NAME         "__call"
#define GEAR_CALLSTATIC_FUNC_NAME   "__callstatic"
#define GEAR_TOSTRING_FUNC_NAME     "__tostring"
#define GEAR_AUTOLOAD_FUNC_NAME     "__autoload"
#define GEAR_INVOKE_FUNC_NAME       "__invoke"
#define GEAR_DEBUGINFO_FUNC_NAME    "__debuginfo"

/* The following constants may be combined in CG(compiler_options)
 * to change the default compiler behavior */

/* generate extended debug information */
#define GEAR_COMPILE_EXTENDED_INFO              (1<<0)

/* call op_array handler of extendions */
#define GEAR_COMPILE_HANDLE_OP_ARRAY            (1<<1)

/* generate GEAR_INIT_FCALL_BY_NAME for internal functions instead of GEAR_INIT_FCALL */
#define GEAR_COMPILE_IGNORE_INTERNAL_FUNCTIONS  (1<<2)

/* don't perform early binding for classes inherited form internal ones;
 * in namespaces assume that internal class that doesn't exist at compile-time
 * may apper in run-time */
#define GEAR_COMPILE_IGNORE_INTERNAL_CLASSES    (1<<3)

/* generate GEAR_DECLARE_INHERITED_CLASS_DELAYED opcode to delay early binding */
#define GEAR_COMPILE_DELAYED_BINDING            (1<<4)

/* disable constant substitution at compile-time */
#define GEAR_COMPILE_NO_CONSTANT_SUBSTITUTION   (1<<5)

/* disable usage of builtin instruction for strlen() */
#define GEAR_COMPILE_NO_BUILTIN_STRLEN          (1<<6)

/* disable substitution of persistent constants at compile-time */
#define GEAR_COMPILE_NO_PERSISTENT_CONSTANT_SUBSTITUTION	(1<<7)

/* generate GEAR_INIT_FCALL_BY_NAME for userland functions instead of GEAR_INIT_FCALL */
#define GEAR_COMPILE_IGNORE_USER_FUNCTIONS      (1<<8)

/* force GEAR_ACC_USE_GUARDS for all classes */
#define GEAR_COMPILE_GUARDS						(1<<9)

/* disable builtin special case function calls */
#define GEAR_COMPILE_NO_BUILTINS				(1<<10)

/* result of compilation may be stored in file cache */
#define GEAR_COMPILE_WITH_FILE_CACHE			(1<<11)

/* disable jumptable optimization for switch statements */
#define GEAR_COMPILE_NO_JUMPTABLES				(1<<12)

/* The default value for CG(compiler_options) */
#define GEAR_COMPILE_DEFAULT					GEAR_COMPILE_HANDLE_OP_ARRAY

/* The default value for CG(compiler_options) during eval() */
#define GEAR_COMPILE_DEFAULT_FOR_EVAL			0

GEAR_API gear_bool gear_binary_op_produces_numeric_string_error(uint32_t opcode, zval *op1, zval *op2);

#endif /* GEAR_COMPILE_H */

