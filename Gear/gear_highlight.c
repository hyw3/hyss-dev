/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include <gear_language_parser.h>
#include "gear_compile.h"
#include "gear_highlight.h"
#include "gear_ptr_stack.h"
#include "gear_globals.h"
#include "gear_exceptions.h"

GEAR_API void gear_html_putc(char c)
{
	switch (c) {
		case '\n':
			GEAR_PUTS("<br />");
			break;
		case '<':
			GEAR_PUTS("&lt;");
			break;
		case '>':
			GEAR_PUTS("&gt;");
			break;
		case '&':
			GEAR_PUTS("&amp;");
			break;
		case ' ':
			GEAR_PUTS("&nbsp;");
			break;
		case '\t':
			GEAR_PUTS("&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		default:
			GEAR_PUTC(c);
			break;
	}
}


GEAR_API void gear_html_puts(const char *s, size_t len)
{
	const unsigned char *ptr = (const unsigned char*)s, *end = ptr + len;
	unsigned char *filtered = NULL;
	size_t filtered_len;

	if (LANG_SCNG(output_filter)) {
		LANG_SCNG(output_filter)(&filtered, &filtered_len, ptr, len);
		ptr = filtered;
		end = filtered + filtered_len;
	}

	while (ptr<end) {
		if (*ptr==' ') {
			do {
				gear_html_putc(*ptr);
			} while ((++ptr < end) && (*ptr==' '));
		} else {
			gear_html_putc(*ptr++);
		}
	}

	if (LANG_SCNG(output_filter)) {
		efree(filtered);
	}
}


GEAR_API void gear_highlight(gear_syntax_highlighter_ics *syntax_highlighter_ics)
{
	zval token;
	int token_type;
	char *last_color = syntax_highlighter_ics->highlight_html;
	char *next_color;

	gear_printf("<code>");
	gear_printf("<span style=\"color: %s\">\n", last_color);
	/* highlight stuff coming back from gearlex() */
	while ((token_type=lex_scan(&token, NULL))) {
		switch (token_type) {
			case T_INLINE_HTML:
				next_color = syntax_highlighter_ics->highlight_html;
				break;
			case T_COMMENT:
			case T_DOC_COMMENT:
				next_color = syntax_highlighter_ics->highlight_comment;
				break;
			case T_OPEN_TAG:
			case T_OPEN_TAG_WITH_ECHO:
			case T_CLOSE_TAG:
			case T_LINE:
			case T_FILE:
			case T_DIR:
			case T_TRAIT_C:
			case T_METHOD_C:
			case T_FUNC_C:
			case T_NS_C:
			case T_CLASS_C:
				next_color = syntax_highlighter_ics->highlight_default;
				break;
			case '"':
			case T_ENCAPSED_AND_WHITESPACE:
			case T_CONSTANT_ENCAPSED_STRING:
				next_color = syntax_highlighter_ics->highlight_string;
				break;
			case T_WHITESPACE:
				gear_html_puts((char*)LANG_SCNG(yy_text), LANG_SCNG(yy_leng));  /* no color needed */
				ZVAL_UNDEF(&token);
				continue;
				break;
			default:
				if (Z_TYPE(token) == IS_UNDEF) {
					next_color = syntax_highlighter_ics->highlight_keyword;
				} else {
					next_color = syntax_highlighter_ics->highlight_default;
				}
				break;
		}

		if (last_color != next_color) {
			if (last_color != syntax_highlighter_ics->highlight_html) {
				gear_printf("</span>");
			}
			last_color = next_color;
			if (last_color != syntax_highlighter_ics->highlight_html) {
				gear_printf("<span style=\"color: %s\">", last_color);
			}
		}

		gear_html_puts((char*)LANG_SCNG(yy_text), LANG_SCNG(yy_leng));

		if (Z_TYPE(token) == IS_STRING) {
			switch (token_type) {
				case T_OPEN_TAG:
				case T_OPEN_TAG_WITH_ECHO:
				case T_CLOSE_TAG:
				case T_WHITESPACE:
				case T_COMMENT:
				case T_DOC_COMMENT:
					break;
				default:
					zval_ptr_dtor_str(&token);
					break;
			}
		}
		ZVAL_UNDEF(&token);
	}

	if (last_color != syntax_highlighter_ics->highlight_html) {
		gear_printf("</span>\n");
	}
	gear_printf("</span>\n");
	gear_printf("</code>");

	/* Discard parse errors thrown during tokenization */
	gear_clear_exception();
}

GEAR_API void gear_strip(void)
{
	zval token;
	int token_type;
	int prev_space = 0;

	while ((token_type=lex_scan(&token, NULL))) {
		switch (token_type) {
			case T_WHITESPACE:
				if (!prev_space) {
					gear_write(" ", sizeof(" ") - 1);
					prev_space = 1;
				}
						/* lack of break; is intentional */
			case T_COMMENT:
			case T_DOC_COMMENT:
				ZVAL_UNDEF(&token);
				continue;

			case T_END_HEREDOC:
				gear_write((char*)LANG_SCNG(yy_text), LANG_SCNG(yy_leng));
				/* read the following character, either newline or ; */
				if (lex_scan(&token, NULL) != T_WHITESPACE) {
					gear_write((char*)LANG_SCNG(yy_text), LANG_SCNG(yy_leng));
				}
				gear_write("\n", sizeof("\n") - 1);
				prev_space = 1;
				ZVAL_UNDEF(&token);
				continue;

			default:
				gear_write((char*)LANG_SCNG(yy_text), LANG_SCNG(yy_leng));
				break;
		}

		if (Z_TYPE(token) == IS_STRING) {
			switch (token_type) {
				case T_OPEN_TAG:
				case T_OPEN_TAG_WITH_ECHO:
				case T_CLOSE_TAG:
				case T_WHITESPACE:
				case T_COMMENT:
				case T_DOC_COMMENT:
					break;

				default:
					zval_ptr_dtor_str(&token);
					break;
			}
		}
		prev_space = 0;
		ZVAL_UNDEF(&token);
	}

	/* Discard parse errors thrown during tokenization */
	gear_clear_exception();
}

