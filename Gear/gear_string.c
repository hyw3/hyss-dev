/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_globals.h"

#ifdef HAVE_VALGRIND
# include "valgrind/callgrind.h"
#endif

GEAR_API gear_new_interned_string_func_t gear_new_interned_string;
GEAR_API gear_string_init_interned_func_t gear_string_init_interned;

static gear_string* GEAR_FASTCALL gear_new_interned_string_permanent(gear_string *str);
static gear_string* GEAR_FASTCALL gear_new_interned_string_request(gear_string *str);
static gear_string* GEAR_FASTCALL gear_string_init_interned_permanent(const char *str, size_t size, int permanent);
static gear_string* GEAR_FASTCALL gear_string_init_interned_request(const char *str, size_t size, int permanent);

/* Any strings interned in the startup phase. Common to all the threads,
   won't be free'd until process exit. If we want an ability to
   add permanent strings even after startup, it would be still
   possible on costs of locking in the thread safe builds. */
static HashTable interned_strings_permanent;

static gear_new_interned_string_func_t interned_string_request_handler = gear_new_interned_string_request;
static gear_string_init_interned_func_t interned_string_init_request_handler = gear_string_init_interned_request;
static gear_string_copy_storage_func_t interned_string_copy_storage = NULL;
static gear_string_copy_storage_func_t interned_string_restore_storage = NULL;

GEAR_API gear_string  *gear_empty_string = NULL;
GEAR_API gear_string  *gear_one_char_string[256];
GEAR_API gear_string **gear_known_strings = NULL;

GEAR_API gear_ulong GEAR_FASTCALL gear_string_hash_func(gear_string *str)
{
	return ZSTR_H(str) = gear_hash_func(ZSTR_VAL(str), ZSTR_LEN(str));
}

GEAR_API gear_ulong GEAR_FASTCALL gear_hash_func(const char *str, size_t len)
{
	return gear_inline_hash_func(str, len);
}

static void _str_dtor(zval *zv)
{
	gear_string *str = Z_STR_P(zv);
	pefree(str, GC_FLAGS(str) & IS_STR_PERSISTENT);
}

static const char *known_strings[] = {
#define _GEAR_STR_DSC(id, str) str,
GEAR_KNOWN_STRINGS(_GEAR_STR_DSC)
#undef _GEAR_STR_DSC
	NULL
};

static void gear_init_interned_strings_ht(HashTable *interned_strings, int permanent)
{
	gear_hash_init(interned_strings, 1024, NULL, _str_dtor, permanent);
	gear_hash_real_init_mixed(interned_strings);
}

GEAR_API void gear_interned_strings_init(void)
{
	char s[2];
	int i;
	gear_string *str;

	interned_string_request_handler = gear_new_interned_string_request;
	interned_string_init_request_handler = gear_string_init_interned_request;
	interned_string_copy_storage = NULL;
	interned_string_restore_storage = NULL;

	gear_empty_string = NULL;
	gear_known_strings = NULL;

	gear_init_interned_strings_ht(&interned_strings_permanent, 1);

	gear_new_interned_string = gear_new_interned_string_permanent;
	gear_string_init_interned = gear_string_init_interned_permanent;

	/* interned empty string */
	str = gear_string_alloc(sizeof("")-1, 1);
	ZSTR_VAL(str)[0] = '\000';
	gear_empty_string = gear_new_interned_string_permanent(str);

	s[1] = 0;
	for (i = 0; i < 256; i++) {
		s[0] = i;
		gear_one_char_string[i] = gear_new_interned_string_permanent(gear_string_init(s, 1, 1));
	}

	/* known strings */
	gear_known_strings = pemalloc(sizeof(gear_string*) * ((sizeof(known_strings) / sizeof(known_strings[0]) - 1)), 1);
	for (i = 0; i < (sizeof(known_strings) / sizeof(known_strings[0])) - 1; i++) {
		str = gear_string_init(known_strings[i], strlen(known_strings[i]), 1);
		gear_known_strings[i] = gear_new_interned_string_permanent(str);
	}
}

GEAR_API void gear_interned_strings_dtor(void)
{
	gear_hash_destroy(&interned_strings_permanent);

	free(gear_known_strings);
	gear_known_strings = NULL;
}

static gear_always_inline gear_string *gear_interned_string_ht_lookup_ex(gear_ulong h, const char *str, size_t size, HashTable *interned_strings)
{
	uint32_t nIndex;
	uint32_t idx;
	Bucket *p;

	nIndex = h | interned_strings->nTableMask;
	idx = HT_HASH(interned_strings, nIndex);
	while (idx != HT_INVALID_IDX) {
		p = HT_HASH_TO_BUCKET(interned_strings, idx);
		if ((p->h == h) && (ZSTR_LEN(p->key) == size)) {
			if (!memcmp(ZSTR_VAL(p->key), str, size)) {
				return p->key;
			}
		}
		idx = Z_NEXT(p->val);
	}

	return NULL;
}

static gear_always_inline gear_string *gear_interned_string_ht_lookup(gear_string *str, HashTable *interned_strings)
{
	gear_ulong h = ZSTR_H(str);
	uint32_t nIndex;
	uint32_t idx;
	Bucket *p;

	nIndex = h | interned_strings->nTableMask;
	idx = HT_HASH(interned_strings, nIndex);
	while (idx != HT_INVALID_IDX) {
		p = HT_HASH_TO_BUCKET(interned_strings, idx);
		if ((p->h == h) && gear_string_equal_content(p->key, str)) {
			return p->key;
		}
		idx = Z_NEXT(p->val);
	}

	return NULL;
}

/* This function might be not thread safe at least because it would update the
   hash val in the passed string. Be sure it is called in the appropriate context. */
static gear_always_inline gear_string *gear_add_interned_string(gear_string *str, HashTable *interned_strings, uint32_t flags)
{
	zval val;

	GC_SET_REFCOUNT(str, 1);
	GC_ADD_FLAGS(str, IS_STR_INTERNED | flags);

	ZVAL_INTERNED_STR(&val, str);

	gear_hash_add_new(interned_strings, str, &val);

	return str;
}

GEAR_API gear_string* GEAR_FASTCALL gear_interned_string_find_permanent(gear_string *str)
{
	gear_string_hash_val(str);
	return gear_interned_string_ht_lookup(str, &interned_strings_permanent);
}

static gear_string* GEAR_FASTCALL gear_new_interned_string_permanent(gear_string *str)
{
	gear_string *ret;

	if (ZSTR_IS_INTERNED(str)) {
		return str;
	}

	gear_string_hash_val(str);
	ret = gear_interned_string_ht_lookup(str, &interned_strings_permanent);
	if (ret) {
		gear_string_release(str);
		return ret;
	}

	GEAR_ASSERT(GC_FLAGS(str) & GC_PERSISTENT);
	if (GC_REFCOUNT(str) > 1) {
		gear_ulong h = ZSTR_H(str);
		gear_string_delref(str);
		str = gear_string_init(ZSTR_VAL(str), ZSTR_LEN(str), 1);
		ZSTR_H(str) = h;
	}

	return gear_add_interned_string(str, &interned_strings_permanent, IS_STR_PERMANENT);
}

static gear_string* GEAR_FASTCALL gear_new_interned_string_request(gear_string *str)
{
	gear_string *ret;

	if (ZSTR_IS_INTERNED(str)) {
		return str;
	}

	gear_string_hash_val(str);

	/* Check for permanent strings, the table is readonly at this point. */
	ret = gear_interned_string_ht_lookup(str, &interned_strings_permanent);
	if (ret) {
		gear_string_release(str);
		return ret;
	}

	ret = gear_interned_string_ht_lookup(str, &CG(interned_strings));
	if (ret) {
		gear_string_release(str);
		return ret;
	}

	/* Create a short living interned, freed after the request. */
#if GEAR_RC_DEBUG
	if (gear_rc_debug) {
		/* HYSS shouldn't create persistent interned string during request,
		 * but at least dl() may do this */
		GEAR_ASSERT(!(GC_FLAGS(str) & GC_PERSISTENT));
	}
#endif
	if (GC_REFCOUNT(str) > 1) {
		gear_ulong h = ZSTR_H(str);
		gear_string_delref(str);
		str = gear_string_init(ZSTR_VAL(str), ZSTR_LEN(str), 0);
		ZSTR_H(str) = h;
	}

	ret = gear_add_interned_string(str, &CG(interned_strings), 0);

	return ret;
}

static gear_string* GEAR_FASTCALL gear_string_init_interned_permanent(const char *str, size_t size, int permanent)
{
	gear_string *ret;
	gear_ulong h = gear_inline_hash_func(str, size);

	ret = gear_interned_string_ht_lookup_ex(h, str, size, &interned_strings_permanent);
	if (ret) {
		return ret;
	}

	GEAR_ASSERT(permanent);
	ret = gear_string_init(str, size, permanent);
	ZSTR_H(ret) = h;
	return gear_add_interned_string(ret, &interned_strings_permanent, IS_STR_PERMANENT);
}

static gear_string* GEAR_FASTCALL gear_string_init_interned_request(const char *str, size_t size, int permanent)
{
	gear_string *ret;
	gear_ulong h = gear_inline_hash_func(str, size);

	/* Check for permanent strings, the table is readonly at this point. */
	ret = gear_interned_string_ht_lookup_ex(h, str, size, &interned_strings_permanent);
	if (ret) {
		return ret;
	}

	ret = gear_interned_string_ht_lookup_ex(h, str, size, &CG(interned_strings));
	if (ret) {
		return ret;
	}

#if GEAR_RC_DEBUG
	if (gear_rc_debug) {
		/* HYSS shouldn't create persistent interned string during request,
		 * but at least dl() may do this */
		GEAR_ASSERT(!permanent);
	}
#endif
	ret = gear_string_init(str, size, permanent);
	ZSTR_H(ret) = h;

	/* Create a short living interned, freed after the request. */
	return gear_add_interned_string(ret, &CG(interned_strings), 0);
}

GEAR_API void gear_interned_strings_activate(void)
{
	gear_init_interned_strings_ht(&CG(interned_strings), 0);
}

GEAR_API void gear_interned_strings_deactivate(void)
{
	gear_hash_destroy(&CG(interned_strings));
}

GEAR_API void gear_interned_strings_set_request_storage_handlers(gear_new_interned_string_func_t handler, gear_string_init_interned_func_t init_handler)
{
	interned_string_request_handler = handler;
	interned_string_init_request_handler = init_handler;
}

GEAR_API void gear_interned_strings_set_permanent_storage_copy_handlers(gear_string_copy_storage_func_t copy_handler, gear_string_copy_storage_func_t restore_handler)
{
	interned_string_copy_storage = copy_handler;
	interned_string_restore_storage = restore_handler;
}

GEAR_API void gear_interned_strings_switch_storage(gear_bool request)
{
	if (request) {
		if (interned_string_copy_storage) {
			interned_string_copy_storage();
		}
		gear_new_interned_string = interned_string_request_handler;
		gear_string_init_interned = interned_string_init_request_handler;
	} else {
		gear_new_interned_string = gear_new_interned_string_permanent;
		gear_string_init_interned = gear_string_init_interned_permanent;
		if (interned_string_restore_storage) {
			interned_string_restore_storage();
		}
	}
}

#if defined(__GNUC__) && defined(__i386__)
GEAR_API gear_bool GEAR_FASTCALL gear_string_equal_val(gear_string *s1, gear_string *s2)
{
	char *ptr = ZSTR_VAL(s1);
	size_t delta = (char*)s2 - (char*)s1;
	size_t len = ZSTR_LEN(s1);
	gear_ulong ret;

	__asm__ (
		".LL0%=:\n\t"
		"movl (%2,%3), %0\n\t"
		"xorl (%2), %0\n\t"
		"jne .LL1%=\n\t"
		"addl $0x4, %2\n\t"
		"subl $0x4, %1\n\t"
		"ja .LL0%=\n\t"
		"movl $0x1, %0\n\t"
		"jmp .LL3%=\n\t"
		".LL1%=:\n\t"
		"cmpl $0x4,%1\n\t"
		"jb .LL2%=\n\t"
		"xorl %0, %0\n\t"
		"jmp .LL3%=\n\t"
		".LL2%=:\n\t"
		"negl %1\n\t"
		"lea 0x20(,%1,8), %1\n\t"
		"shll %b1, %0\n\t"
		"sete %b0\n\t"
		"movzbl %b0, %0\n\t"
		".LL3%=:\n"
		: "=&a"(ret),
		  "+c"(len),
		  "+r"(ptr)
		: "r"(delta)
		: "cc");
	return ret;
}

#ifdef HAVE_VALGRIND
GEAR_API gear_bool GEAR_FASTCALL I_WRAP_SONAME_FNNAME_ZU(NONE,gear_string_equal_val)(gear_string *s1, gear_string *s2)
{
	size_t len = ZSTR_LEN(s1);
	char *ptr1 = ZSTR_VAL(s1);
	char *ptr2 = ZSTR_VAL(s2);
	gear_ulong ret;

	__asm__ (
		"test %1, %1\n\t"
		"jnz .LL1%=\n\t"
		"movl $0x1, %0\n\t"
		"jmp .LL2%=\n\t"
		".LL1%=:\n\t"
		"cld\n\t"
		"rep\n\t"
		"cmpsb\n\t"
		"sete %b0\n\t"
		"movzbl %b0, %0\n\t"
		".LL2%=:\n"
		: "=a"(ret),
		  "+c"(len),
		  "+D"(ptr1),
		  "+S"(ptr2)
		:
		: "cc");
	return ret;
}
#endif

#elif defined(__GNUC__) && defined(__x86_64__) && !defined(__ILP32__)
GEAR_API gear_bool GEAR_FASTCALL gear_string_equal_val(gear_string *s1, gear_string *s2)
{
	char *ptr = ZSTR_VAL(s1);
	size_t delta = (char*)s2 - (char*)s1;
	size_t len = ZSTR_LEN(s1);
	gear_ulong ret;

	__asm__ (
		".LL0%=:\n\t"
		"movq (%2,%3), %0\n\t"
		"xorq (%2), %0\n\t"
		"jne .LL1%=\n\t"
		"addq $0x8, %2\n\t"
		"subq $0x8, %1\n\t"
		"ja .LL0%=\n\t"
		"movq $0x1, %0\n\t"
		"jmp .LL3%=\n\t"
		".LL1%=:\n\t"
		"cmpq $0x8,%1\n\t"
		"jb .LL2%=\n\t"
		"xorq %0, %0\n\t"
		"jmp .LL3%=\n\t"
		".LL2%=:\n\t"
		"negq %1\n\t"
		"lea 0x40(,%1,8), %1\n\t"
		"shlq %b1, %0\n\t"
		"sete %b0\n\t"
		"movzbq %b0, %0\n\t"
		".LL3%=:\n"
		: "=&a"(ret),
		  "+c"(len),
		  "+r"(ptr)
		: "r"(delta)
		: "cc");
	return ret;
}

#ifdef HAVE_VALGRIND
GEAR_API gear_bool GEAR_FASTCALL I_WRAP_SONAME_FNNAME_ZU(NONE,gear_string_equal_val)(gear_string *s1, gear_string *s2)
{
	size_t len = ZSTR_LEN(s1);
	char *ptr1 = ZSTR_VAL(s1);
	char *ptr2 = ZSTR_VAL(s2);
	gear_ulong ret;

	__asm__ (
		"test %1, %1\n\t"
		"jnz .LL1%=\n\t"
		"movq $0x1, %0\n\t"
		"jmp .LL2%=\n\t"
		".LL1%=:\n\t"
		"cld\n\t"
		"rep\n\t"
		"cmpsb\n\t"
		"sete %b0\n\t"
		"movzbq %b0, %0\n\t"
		".LL2%=:\n"
		: "=a"(ret),
		  "+c"(len),
		  "+D"(ptr1),
		  "+S"(ptr2)
		:
		: "cc");
	return ret;
}
#endif

#endif

