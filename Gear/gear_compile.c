/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gear_language_parser.h>
#include "gear.h"
#include "gear_compile.h"
#include "gear_constants.h"
#include "gear_llist.h"
#include "gear_API.h"
#include "gear_exceptions.h"
#include "gear_interfaces.h"
#include "gear_virtual_cwd.h"
#include "gear_multibyte.h"
#include "gear_language_scanner.h"
#include "gear_inheritance.h"
#include "gear_vm.h"

#define SET_NODE(target, src) do { \
		target ## _type = (src)->op_type; \
		if ((src)->op_type == IS_CONST) { \
			target.constant = gear_add_literal(CG(active_op_array), &(src)->u.constant); \
		} else { \
			target = (src)->u.op; \
		} \
	} while (0)

#define GET_NODE(target, src) do { \
		(target)->op_type = src ## _type; \
		if ((target)->op_type == IS_CONST) { \
			ZVAL_COPY_VALUE(&(target)->u.constant, CT_CONSTANT(src)); \
		} else { \
			(target)->u.op = src; \
		} \
	} while (0)

#define FC(member) (CG(file_context).member)

typedef struct _gear_loop_var {
	gear_uchar opcode;
	gear_uchar var_type;
	uint32_t   var_num;
	union {
		uint32_t try_catch_offset;
		uint32_t live_range_offset;
	} u;
} gear_loop_var;

static inline uint32_t gear_alloc_cache_slot(void) {
	gear_op_array *op_array = CG(active_op_array);
	uint32_t ret = op_array->cache_size;
	op_array->cache_size += sizeof(void*);
	return ret;
}

#define POLYMORPHIC_CACHE_SLOT_SIZE 2

static inline uint32_t gear_alloc_polymorphic_cache_slot(void) {
	gear_op_array *op_array = CG(active_op_array);
	uint32_t ret = op_array->cache_size;
	op_array->cache_size += POLYMORPHIC_CACHE_SLOT_SIZE * sizeof(void*);
	return ret;
}

GEAR_API gear_op_array *(*gear_compile_file)(gear_file_handle *file_handle, int type);
GEAR_API gear_op_array *(*gear_compile_string)(zval *source_string, char *filename);

#ifndef ZTS
GEAR_API gear_compiler_globals compiler_globals;
GEAR_API gear_executor_globals executor_globals;
#endif

static gear_op *gear_emit_op(znode *result, gear_uchar opcode, znode *op1, znode *op2);
static gear_bool gear_try_ct_eval_array(zval *result, gear_ast *ast);

static void init_op(gear_op *op)
{
	MAKE_NOP(op);
	op->extended_value = 0;
	op->lineno = CG(gear_lineno);
}

static gear_op *get_next_op(gear_op_array *op_array)
{
	uint32_t next_op_num = op_array->last++;
	gear_op *next_op;

	if (UNEXPECTED(next_op_num >= CG(context).opcodes_size)) {
		CG(context).opcodes_size *= 4;
		op_array->opcodes = erealloc(op_array->opcodes, CG(context).opcodes_size * sizeof(gear_op));
	}

	next_op = &(op_array->opcodes[next_op_num]);

	init_op(next_op);

	return next_op;
}

static gear_brk_cont_element *get_next_brk_cont_element(void)
{
	CG(context).last_brk_cont++;
	CG(context).brk_cont_array = erealloc(CG(context).brk_cont_array, sizeof(gear_brk_cont_element) * CG(context).last_brk_cont);
	return &CG(context).brk_cont_array[CG(context).last_brk_cont-1];
}

static void gear_destroy_property_info_internal(zval *zv) /* {{{ */
{
	gear_property_info *property_info = Z_PTR_P(zv);

	gear_string_release_ex(property_info->name, 1);
	free(property_info);
}
/* }}} */

static gear_string *gear_build_runtime_definition_key(gear_string *name, unsigned char *lex_pos) /* {{{ */
{
	gear_string *result;
	char char_pos_buf[32];
	size_t char_pos_len = gear_sprintf(char_pos_buf, "%p", lex_pos);
	gear_string *filename = CG(active_op_array)->filename;

	/* NULL, name length, filename length, last accepting char position length */
	result = gear_string_alloc(1 + ZSTR_LEN(name) + ZSTR_LEN(filename) + char_pos_len, 0);
 	sprintf(ZSTR_VAL(result), "%c%s%s%s", '\0', ZSTR_VAL(name), ZSTR_VAL(filename), char_pos_buf);
	return gear_new_interned_string(result);
}
/* }}} */

static gear_bool gear_get_unqualified_name(const gear_string *name, const char **result, size_t *result_len) /* {{{ */
{
	const char *ns_separator = gear_memrchr(ZSTR_VAL(name), '\\', ZSTR_LEN(name));
	if (ns_separator != NULL) {
		*result = ns_separator + 1;
		*result_len = ZSTR_VAL(name) + ZSTR_LEN(name) - *result;
		return 1;
	}

	return 0;
}
/* }}} */

struct reserved_class_name {
	const char *name;
	size_t len;
};
static const struct reserved_class_name reserved_class_names[] = {
	{GEAR_STRL("bool")},
	{GEAR_STRL("false")},
	{GEAR_STRL("float")},
	{GEAR_STRL("int")},
	{GEAR_STRL("null")},
	{GEAR_STRL("parent")},
	{GEAR_STRL("self")},
	{GEAR_STRL("static")},
	{GEAR_STRL("string")},
	{GEAR_STRL("true")},
	{GEAR_STRL("void")},
	{GEAR_STRL("iterable")},
	{GEAR_STRL("object")},
	{NULL, 0}
};

static gear_bool gear_is_reserved_class_name(const gear_string *name) /* {{{ */
{
	const struct reserved_class_name *reserved = reserved_class_names;

	const char *uqname = ZSTR_VAL(name);
	size_t uqname_len = ZSTR_LEN(name);
	gear_get_unqualified_name(name, &uqname, &uqname_len);

	for (; reserved->name; ++reserved) {
		if (uqname_len == reserved->len
			&& gear_binary_strcasecmp(uqname, uqname_len, reserved->name, reserved->len) == 0
		) {
			return 1;
		}
	}

	return 0;
}
/* }}} */

void gear_assert_valid_class_name(const gear_string *name) /* {{{ */
{
	if (gear_is_reserved_class_name(name)) {
		gear_error_noreturn(E_COMPILE_ERROR,
			"Cannot use '%s' as class name as it is reserved", ZSTR_VAL(name));
	}
}
/* }}} */

typedef struct _builtin_type_info {
	const char* name;
	const size_t name_len;
	const gear_uchar type;
} builtin_type_info;

static const builtin_type_info builtin_types[] = {
	{GEAR_STRL("int"), IS_LONG},
	{GEAR_STRL("float"), IS_DOUBLE},
	{GEAR_STRL("string"), IS_STRING},
	{GEAR_STRL("bool"), _IS_BOOL},
	{GEAR_STRL("void"), IS_VOID},
	{GEAR_STRL("iterable"), IS_ITERABLE},
	{GEAR_STRL("object"), IS_OBJECT},
	{NULL, 0, IS_UNDEF}
};


static gear_always_inline gear_uchar gear_lookup_builtin_type_by_name(const gear_string *name) /* {{{ */
{
	const builtin_type_info *info = &builtin_types[0];

	for (; info->name; ++info) {
		if (ZSTR_LEN(name) == info->name_len
			&& gear_binary_strcasecmp(ZSTR_VAL(name), ZSTR_LEN(name), info->name, info->name_len) == 0
		) {
			return info->type;
		}
	}

	return 0;
}
/* }}} */


void gear_oparray_context_begin(gear_oparray_context *prev_context) /* {{{ */
{
	*prev_context = CG(context);
	CG(context).opcodes_size = INITIAL_OP_ARRAY_SIZE;
	CG(context).vars_size = 0;
	CG(context).literals_size = 0;
	CG(context).backpatch_count = 0;
	CG(context).fast_call_var = -1;
	CG(context).try_catch_offset = -1;
	CG(context).current_brk_cont = -1;
	CG(context).last_brk_cont = 0;
	CG(context).brk_cont_array = NULL;
	CG(context).labels = NULL;
}
/* }}} */

void gear_oparray_context_end(gear_oparray_context *prev_context) /* {{{ */
{
	if (CG(context).brk_cont_array) {
		efree(CG(context).brk_cont_array);
		CG(context).brk_cont_array = NULL;
	}
	if (CG(context).labels) {
		gear_hash_destroy(CG(context).labels);
		FREE_HASHTABLE(CG(context).labels);
		CG(context).labels = NULL;
	}
	CG(context) = *prev_context;
}
/* }}} */

static void gear_reset_import_tables(void) /* {{{ */
{
	if (FC(imports)) {
		gear_hash_destroy(FC(imports));
		efree(FC(imports));
		FC(imports) = NULL;
	}

	if (FC(imports_function)) {
		gear_hash_destroy(FC(imports_function));
		efree(FC(imports_function));
		FC(imports_function) = NULL;
	}

	if (FC(imports_const)) {
		gear_hash_destroy(FC(imports_const));
		efree(FC(imports_const));
		FC(imports_const) = NULL;
	}
}
/* }}} */

static void gear_end_namespace(void) /* {{{ */ {
	FC(in_namespace) = 0;
	gear_reset_import_tables();
	if (FC(current_namespace)) {
		gear_string_release_ex(FC(current_namespace), 0);
		FC(current_namespace) = NULL;
	}
}
/* }}} */

void gear_file_context_begin(gear_file_context *prev_context) /* {{{ */
{
	*prev_context = CG(file_context);
	FC(imports) = NULL;
	FC(imports_function) = NULL;
	FC(imports_const) = NULL;
	FC(current_namespace) = NULL;
	FC(in_namespace) = 0;
	FC(has_bracketed_namespaces) = 0;
	FC(declarables).ticks = 0;
	gear_hash_init(&FC(seen_symbols), 8, NULL, NULL, 0);
}
/* }}} */

void gear_file_context_end(gear_file_context *prev_context) /* {{{ */
{
	gear_end_namespace();
	gear_hash_destroy(&FC(seen_symbols));
	CG(file_context) = *prev_context;
}
/* }}} */

void gear_init_compiler_data_structures(void) /* {{{ */
{
	gear_stack_init(&CG(loop_var_stack), sizeof(gear_loop_var));
	gear_stack_init(&CG(delayed_oplines_stack), sizeof(gear_op));
	CG(active_class_entry) = NULL;
	CG(in_compilation) = 0;
	CG(start_lineno) = 0;

	CG(encoding_declared) = 0;
}
/* }}} */

static void gear_register_seen_symbol(gear_string *name, uint32_t kind) {
	zval *zv = gear_hash_find(&FC(seen_symbols), name);
	if (zv) {
		Z_LVAL_P(zv) |= kind;
	} else {
		zval tmp;
		ZVAL_LONG(&tmp, kind);
		gear_hash_add_new(&FC(seen_symbols), name, &tmp);
	}
}

static gear_bool gear_have_seen_symbol(gear_string *name, uint32_t kind) {
	zval *zv = gear_hash_find(&FC(seen_symbols), name);
	return zv && (Z_LVAL_P(zv) & kind) != 0;
}

GEAR_API void file_handle_dtor(gear_file_handle *fh) /* {{{ */
{

	gear_file_handle_dtor(fh);
}
/* }}} */

void init_compiler(void) /* {{{ */
{
	CG(arena) = gear_arena_create(64 * 1024);
	CG(active_op_array) = NULL;
	memset(&CG(context), 0, sizeof(CG(context)));
	gear_init_compiler_data_structures();
	gear_init_rsrc_list();
	gear_hash_init(&CG(filenames_table), 8, NULL, ZVAL_PTR_DTOR, 0);
	gear_llist_init(&CG(open_files), sizeof(gear_file_handle), (void (*)(void *)) file_handle_dtor, 0);
	CG(unclean_shutdown) = 0;
}
/* }}} */

void shutdown_compiler(void) /* {{{ */
{
	gear_stack_destroy(&CG(loop_var_stack));
	gear_stack_destroy(&CG(delayed_oplines_stack));
	gear_hash_destroy(&CG(filenames_table));
	gear_arena_destroy(CG(arena));
}
/* }}} */

GEAR_API gear_string *gear_set_compiled_filename(gear_string *new_compiled_filename) /* {{{ */
{
	zval *p, rv;

	if ((p = gear_hash_find(&CG(filenames_table), new_compiled_filename))) {
		GEAR_ASSERT(Z_TYPE_P(p) == IS_STRING);
		CG(compiled_filename) = Z_STR_P(p);
		return Z_STR_P(p);
	}

	new_compiled_filename = gear_new_interned_string(gear_string_copy(new_compiled_filename));
	ZVAL_STR(&rv, new_compiled_filename);
	gear_hash_add_new(&CG(filenames_table), new_compiled_filename, &rv);

	CG(compiled_filename) = new_compiled_filename;
	return new_compiled_filename;
}
/* }}} */

GEAR_API void gear_restore_compiled_filename(gear_string *original_compiled_filename) /* {{{ */
{
	CG(compiled_filename) = original_compiled_filename;
}
/* }}} */

GEAR_API gear_string *gear_get_compiled_filename(void) /* {{{ */
{
	return CG(compiled_filename);
}
/* }}} */

GEAR_API int gear_get_compiled_lineno(void) /* {{{ */
{
	return CG(gear_lineno);
}
/* }}} */

GEAR_API gear_bool gear_is_compiling(void) /* {{{ */
{
	return CG(in_compilation);
}
/* }}} */

static uint32_t get_temporary_variable(gear_op_array *op_array) /* {{{ */
{
	return (uint32_t)op_array->T++;
}
/* }}} */

static int lookup_cv(gear_op_array *op_array, gear_string *name) /* {{{ */{
	int i = 0;
	gear_ulong hash_value = gear_string_hash_val(name);

	while (i < op_array->last_var) {
		if (ZSTR_VAL(op_array->vars[i]) == ZSTR_VAL(name) ||
		    (ZSTR_H(op_array->vars[i]) == hash_value &&
		     gear_string_equal_content(op_array->vars[i], name))) {
			return (int)(gear_intptr_t)GEAR_CALL_VAR_NUM(NULL, i);
		}
		i++;
	}
	i = op_array->last_var;
	op_array->last_var++;
	if (op_array->last_var > CG(context).vars_size) {
		CG(context).vars_size += 16; /* FIXME */
		op_array->vars = erealloc(op_array->vars, CG(context).vars_size * sizeof(gear_string*));
	}

	op_array->vars[i] = gear_string_copy(name);
	return (int)(gear_intptr_t)GEAR_CALL_VAR_NUM(NULL, i);
}
/* }}} */

void gear_del_literal(gear_op_array *op_array, int n) /* {{{ */
{
	zval_ptr_dtor_nogc(CT_CONSTANT_EX(op_array, n));
	if (n + 1 == op_array->last_literal) {
		op_array->last_literal--;
	} else {
		ZVAL_UNDEF(CT_CONSTANT_EX(op_array, n));
	}
}
/* }}} */

static inline gear_string *zval_make_interned_string(zval *zv) /* {{{ */
{
	GEAR_ASSERT(Z_TYPE_P(zv) == IS_STRING);
	Z_STR_P(zv) = gear_new_interned_string(Z_STR_P(zv));
	if (ZSTR_IS_INTERNED(Z_STR_P(zv))) {
		Z_TYPE_FLAGS_P(zv) = 0;
	}
	return Z_STR_P(zv);
}

/* Common part of gear_add_literal and gear_append_individual_literal */
static inline void gear_insert_literal(gear_op_array *op_array, zval *zv, int literal_position) /* {{{ */
{
	zval *lit = CT_CONSTANT_EX(op_array, literal_position);
	if (Z_TYPE_P(zv) == IS_STRING) {
		zval_make_interned_string(zv);
	}
	ZVAL_COPY_VALUE(lit, zv);
	Z_EXTRA_P(lit) = 0;
}
/* }}} */

/* Is used while compiling a function, using the context to keep track
   of an approximate size to avoid to relocate to often.
   Literals are truncated to actual size in the second compiler pass (pass_two()). */
int gear_add_literal(gear_op_array *op_array, zval *zv) /* {{{ */
{
	int i = op_array->last_literal;
	op_array->last_literal++;
	if (i >= CG(context).literals_size) {
		while (i >= CG(context).literals_size) {
			CG(context).literals_size += 16; /* FIXME */
		}
		op_array->literals = (zval*)erealloc(op_array->literals, CG(context).literals_size * sizeof(zval));
	}
	gear_insert_literal(op_array, zv, i);
	return i;
}
/* }}} */

static inline int gear_add_literal_string(gear_op_array *op_array, gear_string **str) /* {{{ */
{
	int ret;
	zval zv;
	ZVAL_STR(&zv, *str);
	ret = gear_add_literal(op_array, &zv);
	*str = Z_STR(zv);
	return ret;
}
/* }}} */

static int gear_add_func_name_literal(gear_op_array *op_array, gear_string *name) /* {{{ */
{
	/* Original name */
	int ret = gear_add_literal_string(op_array, &name);

	/* Lowercased name */
	gear_string *lc_name = gear_string_tolower(name);
	gear_add_literal_string(op_array, &lc_name);

	return ret;
}
/* }}} */

static int gear_add_ns_func_name_literal(gear_op_array *op_array, gear_string *name) /* {{{ */
{
	const char *unqualified_name;
	size_t unqualified_name_len;

	/* Original name */
	int ret = gear_add_literal_string(op_array, &name);

	/* Lowercased name */
	gear_string *lc_name = gear_string_tolower(name);
	gear_add_literal_string(op_array, &lc_name);

	/* Lowercased unqualfied name */
	if (gear_get_unqualified_name(name, &unqualified_name, &unqualified_name_len)) {
		lc_name = gear_string_alloc(unqualified_name_len, 0);
		gear_str_tolower_copy(ZSTR_VAL(lc_name), unqualified_name, unqualified_name_len);
		gear_add_literal_string(op_array, &lc_name);
	}

	return ret;
}
/* }}} */

static int gear_add_class_name_literal(gear_op_array *op_array, gear_string *name) /* {{{ */
{
	/* Original name */
	int ret = gear_add_literal_string(op_array, &name);

	/* Lowercased name */
	gear_string *lc_name = gear_string_tolower(name);
	gear_add_literal_string(op_array, &lc_name);

	return ret;
}
/* }}} */

static int gear_add_const_name_literal(gear_op_array *op_array, gear_string *name, gear_bool unqualified) /* {{{ */
{
	gear_string *tmp_name;

	int ret = gear_add_literal_string(op_array, &name);

	size_t ns_len = 0, after_ns_len = ZSTR_LEN(name);
	const char *after_ns = gear_memrchr(ZSTR_VAL(name), '\\', ZSTR_LEN(name));
	if (after_ns) {
		after_ns += 1;
		ns_len = after_ns - ZSTR_VAL(name) - 1;
		after_ns_len = ZSTR_LEN(name) - ns_len - 1;

		/* lowercased namespace name & original constant name */
		tmp_name = gear_string_init(ZSTR_VAL(name), ZSTR_LEN(name), 0);
		gear_str_tolower(ZSTR_VAL(tmp_name), ns_len);
		gear_add_literal_string(op_array, &tmp_name);

		/* lowercased namespace name & lowercased constant name */
		tmp_name = gear_string_tolower(name);
		gear_add_literal_string(op_array, &tmp_name);

		if (!unqualified) {
			return ret;
		}
	} else {
		after_ns = ZSTR_VAL(name);
	}

	/* original unqualified constant name */
	tmp_name = gear_string_init(after_ns, after_ns_len, 0);
	gear_add_literal_string(op_array, &tmp_name);

	/* lowercased unqualified constant name */
	tmp_name = gear_string_alloc(after_ns_len, 0);
	gear_str_tolower_copy(ZSTR_VAL(tmp_name), after_ns, after_ns_len);
	gear_add_literal_string(op_array, &tmp_name);

	return ret;
}
/* }}} */

#define LITERAL_STR(op, str) do { \
		zval _c; \
		ZVAL_STR(&_c, str); \
		op.constant = gear_add_literal(CG(active_op_array), &_c); \
	} while (0)

void gear_stop_lexing(void)
{
	if (LANG_SCNG(on_event)) {
		LANG_SCNG(on_event)(ON_STOP, END, 0, LANG_SCNG(on_event_context));
	}

	LANG_SCNG(yy_cursor) = LANG_SCNG(yy_limit);
}

static uint32_t gear_start_live_range(gear_op_array *op_array, uint32_t start) /* {{{ */
{
	gear_live_range *range;

	op_array->last_live_range++;
	op_array->live_range = erealloc(op_array->live_range, sizeof(gear_live_range) * op_array->last_live_range);
	range = op_array->live_range + op_array->last_live_range - 1;
	range->start = start;
	return op_array->last_live_range - 1;
}
/* }}} */

static void gear_end_live_range(gear_op_array *op_array, uint32_t offset, uint32_t end, uint32_t kind, uint32_t var) /* {{{ */
{
	gear_live_range *range = op_array->live_range + offset;

	if (range->start == end && offset == (uint32_t)op_array->last_live_range - 1) {
		op_array->last_live_range--;
	} else {
		range->end = end;
		range->var = (var * sizeof(zval)) | kind;
	}
}
/* }}} */

static inline void gear_begin_loop(
		gear_uchar free_opcode, const znode *loop_var, gear_bool is_switch) /* {{{ */
{
	gear_brk_cont_element *brk_cont_element;
	int parent = CG(context).current_brk_cont;
	gear_loop_var info = {0};

	CG(context).current_brk_cont = CG(context).last_brk_cont;
	brk_cont_element = get_next_brk_cont_element();
	brk_cont_element->parent = parent;
	brk_cont_element->is_switch = is_switch;

	if (loop_var && (loop_var->op_type & (IS_VAR|IS_TMP_VAR))) {
		uint32_t start = get_next_op_number(CG(active_op_array));

		info.opcode = free_opcode;
		info.var_type = loop_var->op_type;
		info.var_num = loop_var->u.op.var;
		info.u.live_range_offset = gear_start_live_range(CG(active_op_array), start);
		brk_cont_element->start = start;
	} else {
		info.opcode = GEAR_NOP;
		/* The start field is used to free temporary variables in case of exceptions.
		 * We won't try to free something of we don't have loop variable.  */
		brk_cont_element->start = -1;
	}

	gear_stack_push(&CG(loop_var_stack), &info);
}
/* }}} */

static inline void gear_end_loop(int cont_addr, const znode *var_node) /* {{{ */
{
	uint32_t end = get_next_op_number(CG(active_op_array));
	gear_brk_cont_element *brk_cont_element
		= &CG(context).brk_cont_array[CG(context).current_brk_cont];
	brk_cont_element->cont = cont_addr;
	brk_cont_element->brk = end;
	CG(context).current_brk_cont = brk_cont_element->parent;

	if (brk_cont_element->start != -1) {
		gear_loop_var *loop_var = gear_stack_top(&CG(loop_var_stack));
		gear_end_live_range(CG(active_op_array), loop_var->u.live_range_offset, end,
			loop_var->opcode == GEAR_FE_FREE ? GEAR_LIVE_LOOP : GEAR_LIVE_TMPVAR,
			var_node->u.op.var);
	}

	gear_stack_del_top(&CG(loop_var_stack));
}
/* }}} */

void gear_do_free(znode *op1) /* {{{ */
{
	if (op1->op_type == IS_TMP_VAR) {
		gear_op *opline = &CG(active_op_array)->opcodes[CG(active_op_array)->last-1];

		while (opline->opcode == GEAR_END_SILENCE) {
			opline--;
		}

		if (opline->result_type == IS_TMP_VAR && opline->result.var == op1->u.op.var) {
			if (opline->opcode == GEAR_BOOL || opline->opcode == GEAR_BOOL_NOT) {
				return;
			}
		}

		gear_emit_op(NULL, GEAR_FREE, op1, NULL);
	} else if (op1->op_type == IS_VAR) {
		gear_op *opline = &CG(active_op_array)->opcodes[CG(active_op_array)->last-1];
		while (opline->opcode == GEAR_END_SILENCE ||
				opline->opcode == GEAR_EXT_FCALL_END ||
				opline->opcode == GEAR_OP_DATA) {
			opline--;
		}
		if (opline->result_type == IS_VAR
			&& opline->result.var == op1->u.op.var) {
			if (opline->opcode == GEAR_FETCH_THIS) {
				opline->opcode = GEAR_NOP;
				opline->result_type = IS_UNUSED;
			} else {
				opline->result_type = IS_UNUSED;
			}
		} else {
			while (opline >= CG(active_op_array)->opcodes) {
				if ((opline->opcode == GEAR_FETCH_LIST_R ||
                     opline->opcode == GEAR_FETCH_LIST_W) &&
				    opline->op1_type == IS_VAR &&
				    opline->op1.var == op1->u.op.var) {
					gear_emit_op(NULL, GEAR_FREE, op1, NULL);
					return;
				}
				if (opline->result_type == IS_VAR
					&& opline->result.var == op1->u.op.var) {
					if (opline->opcode == GEAR_NEW) {
						gear_emit_op(NULL, GEAR_FREE, op1, NULL);
					}
					break;
				}
				opline--;
			}
		}
	} else if (op1->op_type == IS_CONST) {
		/* Destroy value without using GC: When opcache moves arrays into SHM it will
		 * free the gear_array structure, so references to it from outside the op array
		 * become invalid. GC would cause such a reference in the root buffer. */
		zval_ptr_dtor_nogc(&op1->u.constant);
	}
}
/* }}} */

uint32_t gear_add_class_modifier(uint32_t flags, uint32_t new_flag) /* {{{ */
{
	uint32_t new_flags = flags | new_flag;
	if ((flags & GEAR_ACC_EXPLICIT_ABSTRACT_CLASS) && (new_flag & GEAR_ACC_EXPLICIT_ABSTRACT_CLASS)) {
		gear_throw_exception(gear_ce_compile_error,
			"Multiple abstract modifiers are not allowed", 0);
		return 0;
	}
	if ((flags & GEAR_ACC_FINAL) && (new_flag & GEAR_ACC_FINAL)) {
		gear_throw_exception(gear_ce_compile_error, "Multiple final modifiers are not allowed", 0);
		return 0;
	}
	if ((new_flags & GEAR_ACC_EXPLICIT_ABSTRACT_CLASS) && (new_flags & GEAR_ACC_FINAL)) {
		gear_throw_exception(gear_ce_compile_error,
			"Cannot use the final modifier on an abstract class", 0);
		return 0;
	}
	return new_flags;
}
/* }}} */

uint32_t gear_add_member_modifier(uint32_t flags, uint32_t new_flag) /* {{{ */
{
	uint32_t new_flags = flags | new_flag;
	if ((flags & GEAR_ACC_PPP_MASK) && (new_flag & GEAR_ACC_PPP_MASK)) {
		gear_throw_exception(gear_ce_compile_error,
			"Multiple access type modifiers are not allowed", 0);
		return 0;
	}
	if ((flags & GEAR_ACC_ABSTRACT) && (new_flag & GEAR_ACC_ABSTRACT)) {
		gear_throw_exception(gear_ce_compile_error, "Multiple abstract modifiers are not allowed", 0);
		return 0;
	}
	if ((flags & GEAR_ACC_STATIC) && (new_flag & GEAR_ACC_STATIC)) {
		gear_throw_exception(gear_ce_compile_error, "Multiple static modifiers are not allowed", 0);
		return 0;
	}
	if ((flags & GEAR_ACC_FINAL) && (new_flag & GEAR_ACC_FINAL)) {
		gear_throw_exception(gear_ce_compile_error, "Multiple final modifiers are not allowed", 0);
		return 0;
	}
	if ((new_flags & GEAR_ACC_ABSTRACT) && (new_flags & GEAR_ACC_FINAL)) {
		gear_throw_exception(gear_ce_compile_error,
			"Cannot use the final modifier on an abstract class member", 0);
		return 0;
	}
	return new_flags;
}
/* }}} */

gear_string *gear_concat3(char *str1, size_t str1_len, char *str2, size_t str2_len, char *str3, size_t str3_len) /* {{{ */
{
	size_t len = str1_len + str2_len + str3_len;
	gear_string *res = gear_string_alloc(len, 0);

	memcpy(ZSTR_VAL(res), str1, str1_len);
	memcpy(ZSTR_VAL(res) + str1_len, str2, str2_len);
	memcpy(ZSTR_VAL(res) + str1_len + str2_len, str3, str3_len);
	ZSTR_VAL(res)[len] = '\0';

	return res;
}

gear_string *gear_concat_names(char *name1, size_t name1_len, char *name2, size_t name2_len) {
	return gear_concat3(name1, name1_len, "\\", 1, name2, name2_len);
}

gear_string *gear_prefix_with_ns(gear_string *name) {
	if (FC(current_namespace)) {
		gear_string *ns = FC(current_namespace);
		return gear_concat_names(ZSTR_VAL(ns), ZSTR_LEN(ns), ZSTR_VAL(name), ZSTR_LEN(name));
	} else {
		return gear_string_copy(name);
	}
}

void *gear_hash_find_ptr_lc(HashTable *ht, const char *str, size_t len) {
	void *result;
	gear_string *lcname;
	ALLOCA_FLAG(use_heap);

	ZSTR_ALLOCA_ALLOC(lcname, len, use_heap);
	gear_str_tolower_copy(ZSTR_VAL(lcname), str, len);
	result = gear_hash_find_ptr(ht, lcname);
	ZSTR_ALLOCA_FREE(lcname, use_heap);

	return result;
}

gear_string *gear_resolve_non_class_name(
	gear_string *name, uint32_t type, gear_bool *is_fully_qualified,
	gear_bool case_sensitive, HashTable *current_import_sub
) {
	char *compound;
	*is_fully_qualified = 0;

	if (ZSTR_VAL(name)[0] == '\\') {
		/* Remove \ prefix (only relevant if this is a string rather than a label) */
		*is_fully_qualified = 1;
		return gear_string_init(ZSTR_VAL(name) + 1, ZSTR_LEN(name) - 1, 0);
	}

	if (type == GEAR_NAME_FQ) {
		*is_fully_qualified = 1;
		return gear_string_copy(name);
	}

	if (type == GEAR_NAME_RELATIVE) {
		*is_fully_qualified = 1;
		return gear_prefix_with_ns(name);
	}

	if (current_import_sub) {
		/* If an unqualified name is a function/const alias, replace it. */
		gear_string *import_name;
		if (case_sensitive) {
			import_name = gear_hash_find_ptr(current_import_sub, name);
		} else {
			import_name = gear_hash_find_ptr_lc(current_import_sub, ZSTR_VAL(name), ZSTR_LEN(name));
		}

		if (import_name) {
			*is_fully_qualified = 1;
			return gear_string_copy(import_name);
		}
	}

	compound = memchr(ZSTR_VAL(name), '\\', ZSTR_LEN(name));
	if (compound) {
		*is_fully_qualified = 1;
	}

	if (compound && FC(imports)) {
		/* If the first part of a qualified name is an alias, substitute it. */
		size_t len = compound - ZSTR_VAL(name);
		gear_string *import_name = gear_hash_find_ptr_lc(FC(imports), ZSTR_VAL(name), len);

		if (import_name) {
			return gear_concat_names(
				ZSTR_VAL(import_name), ZSTR_LEN(import_name), ZSTR_VAL(name) + len + 1, ZSTR_LEN(name) - len - 1);
		}
	}

	return gear_prefix_with_ns(name);
}
/* }}} */

gear_string *gear_resolve_function_name(gear_string *name, uint32_t type, gear_bool *is_fully_qualified) /* {{{ */
{
	return gear_resolve_non_class_name(
		name, type, is_fully_qualified, 0, FC(imports_function));
}
/* }}} */

gear_string *gear_resolve_const_name(gear_string *name, uint32_t type, gear_bool *is_fully_qualified) /* {{{ */ {
	return gear_resolve_non_class_name(
		name, type, is_fully_qualified, 1, FC(imports_const));
}
/* }}} */

gear_string *gear_resolve_class_name(gear_string *name, uint32_t type) /* {{{ */
{
	char *compound;

	if (type == GEAR_NAME_RELATIVE) {
		return gear_prefix_with_ns(name);
	}

	if (type == GEAR_NAME_FQ || ZSTR_VAL(name)[0] == '\\') {
		/* Remove \ prefix (only relevant if this is a string rather than a label) */
		if (ZSTR_VAL(name)[0] == '\\') {
			name = gear_string_init(ZSTR_VAL(name) + 1, ZSTR_LEN(name) - 1, 0);
		} else {
			gear_string_addref(name);
		}
		/* Ensure that \self, \parent and \static are not used */
		if (GEAR_FETCH_CLASS_DEFAULT != gear_get_class_fetch_type(name)) {
			gear_error_noreturn(E_COMPILE_ERROR, "'\\%s' is an invalid class name", ZSTR_VAL(name));
		}
		return name;
	}

	if (FC(imports)) {
		compound = memchr(ZSTR_VAL(name), '\\', ZSTR_LEN(name));
		if (compound) {
			/* If the first part of a qualified name is an alias, substitute it. */
			size_t len = compound - ZSTR_VAL(name);
			gear_string *import_name =
				gear_hash_find_ptr_lc(FC(imports), ZSTR_VAL(name), len);

			if (import_name) {
				return gear_concat_names(
					ZSTR_VAL(import_name), ZSTR_LEN(import_name), ZSTR_VAL(name) + len + 1, ZSTR_LEN(name) - len - 1);
			}
		} else {
			/* If an unqualified name is an alias, replace it. */
			gear_string *import_name
				= gear_hash_find_ptr_lc(FC(imports), ZSTR_VAL(name), ZSTR_LEN(name));

			if (import_name) {
				return gear_string_copy(import_name);
			}
		}
	}

	/* If not fully qualified and not an alias, prepend the current namespace */
	return gear_prefix_with_ns(name);
}
/* }}} */

gear_string *gear_resolve_class_name_ast(gear_ast *ast) /* {{{ */
{
	zval *class_name = gear_ast_get_zval(ast);
	if (Z_TYPE_P(class_name) != IS_STRING) {
		gear_error_noreturn(E_COMPILE_ERROR, "Illegal class name");
	}
	return gear_resolve_class_name(Z_STR_P(class_name), ast->attr);
}
/* }}} */

static void label_ptr_dtor(zval *zv) /* {{{ */
{
	efree_size(Z_PTR_P(zv), sizeof(gear_label));
}
/* }}} */

static void str_dtor(zval *zv)  /* {{{ */ {
	gear_string_release_ex(Z_STR_P(zv), 0);
}
/* }}} */

static gear_bool gear_is_call(gear_ast *ast);

static uint32_t gear_add_try_element(uint32_t try_op) /* {{{ */
{
	gear_op_array *op_array = CG(active_op_array);
	uint32_t try_catch_offset = op_array->last_try_catch++;
	gear_try_catch_element *elem;

	op_array->try_catch_array = safe_erealloc(
		op_array->try_catch_array, sizeof(gear_try_catch_element), op_array->last_try_catch, 0);

	elem = &op_array->try_catch_array[try_catch_offset];
	elem->try_op = try_op;
	elem->catch_op = 0;
	elem->finally_op = 0;
	elem->finally_end = 0;

	return try_catch_offset;
}
/* }}} */

GEAR_API void function_add_ref(gear_function *function) /* {{{ */
{
	if (function->type == GEAR_USER_FUNCTION) {
		gear_op_array *op_array = &function->op_array;

		if (op_array->refcount) {
			(*op_array->refcount)++;
		}
		if (op_array->static_variables) {
			if (!(GC_FLAGS(op_array->static_variables) & IS_ARRAY_IMMUTABLE)) {
				GC_ADDREF(op_array->static_variables);
			}
		}
		op_array->run_time_cache = NULL;
	} else if (function->type == GEAR_INTERNAL_FUNCTION) {
		if (function->common.function_name) {
			gear_string_addref(function->common.function_name);
		}
	}
}
/* }}} */

GEAR_API int do_bind_function(const gear_op_array *op_array, const gear_op *opline, HashTable *function_table, gear_bool compile_time) /* {{{ */
{
	gear_function *function, *new_function;
	zval *lcname, *rtd_key, *zv;

	if (compile_time) {
		lcname = CT_CONSTANT_EX(op_array, opline->op1.constant);
		rtd_key = lcname + 1;
	} else {
		lcname = RT_CONSTANT(opline, opline->op1);
		rtd_key = lcname + 1;
	}

	zv = gear_hash_find_ex(function_table, Z_STR_P(rtd_key), 1);
	function = (gear_function*)Z_PTR_P(zv);
	new_function = gear_arena_alloc(&CG(arena), sizeof(gear_op_array));
	memcpy(new_function, function, sizeof(gear_op_array));
	if (gear_hash_add_ptr(function_table, Z_STR_P(lcname), new_function) == NULL) {
		int error_level = compile_time ? E_COMPILE_ERROR : E_ERROR;
		gear_function *old_function;

		zv = gear_hash_find_ex(function_table, Z_STR_P(lcname), 1);
		GEAR_ASSERT(zv != NULL);
		old_function = (gear_function*)Z_PTR_P(zv);
		if (old_function->type == GEAR_USER_FUNCTION
			&& old_function->op_array.last > 0) {
			gear_error_noreturn(error_level, "Cannot redeclare %s() (previously declared in %s:%d)",
						ZSTR_VAL(function->common.function_name),
						ZSTR_VAL(old_function->op_array.filename),
						old_function->op_array.opcodes[0].lineno);
		} else {
			gear_error_noreturn(error_level, "Cannot redeclare %s()", ZSTR_VAL(function->common.function_name));
		}
		return FAILURE;
	} else {
		if (function->op_array.refcount) {
			(*function->op_array.refcount)++;
		}
		if (!(function->op_array.fn_flags & GEAR_ACC_IMMUTABLE)) {
			function->op_array.static_variables = NULL; /* NULL out the unbound function */
		}
		return SUCCESS;
	}
}
/* }}} */

GEAR_API gear_class_entry *do_bind_class(const gear_op_array* op_array, const gear_op *opline, HashTable *class_table, gear_bool compile_time) /* {{{ */
{
	gear_class_entry *ce;
	zval *lcname, *rtd_key, *zv;

	if (compile_time) {
		lcname = CT_CONSTANT_EX(op_array, opline->op1.constant);
		rtd_key = lcname + 1;
	} else {
		lcname = RT_CONSTANT(opline, opline->op1);
		rtd_key = lcname + 1;
	}
	zv = gear_hash_find_ex(class_table, Z_STR_P(rtd_key), 1);
	GEAR_ASSERT(zv);
	ce = (gear_class_entry*)Z_PTR_P(zv);
	ce->refcount++;
	if (gear_hash_add_ptr(class_table, Z_STR_P(lcname), ce) == NULL) {
		ce->refcount--;
		if (!compile_time) {
			/* If we're in compile time, in practice, it's quite possible
			 * that we'll never reach this class declaration at runtime,
			 * so we shut up about it.  This allows the if (!defined('FOO')) { return; }
			 * approach to work.
			 */
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot declare %s %s, because the name is already in use", gear_get_object_type(ce), ZSTR_VAL(ce->name));
		}
		return NULL;
	} else {
		if (!(ce->ce_flags & (GEAR_ACC_INTERFACE|GEAR_ACC_IMPLEMENT_INTERFACES|GEAR_ACC_IMPLEMENT_TRAITS))) {
			gear_verify_abstract_class(ce);
		}
		return ce;
	}
}
/* }}} */

GEAR_API gear_class_entry *do_bind_inherited_class(const gear_op_array *op_array, const gear_op *opline, HashTable *class_table, gear_class_entry *parent_ce, gear_bool compile_time) /* {{{ */
{
	gear_class_entry *ce;
	zval *lcname, *rtd_key, *zv;

	if (compile_time) {
		lcname = CT_CONSTANT_EX(op_array, opline->op1.constant);
		rtd_key = lcname + 1;
	} else {
		lcname = RT_CONSTANT(opline, opline->op1);
		rtd_key = lcname + 1;
	}

	zv = gear_hash_find_ex(class_table, Z_STR_P(rtd_key), 1);

	if (!zv) {
		if (!compile_time) {
			/* If we're in compile time, in practice, it's quite possible
			 * that we'll never reach this class declaration at runtime,
			 * so we shut up about it.  This allows the if (!defined('FOO')) { return; }
			 * approach to work.
			 */
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot declare  %s, because the name is already in use", gear_get_object_type(Z_OBJCE_P(lcname)));
		}
		return NULL;
	}

	ce = (gear_class_entry*)Z_PTR_P(zv);

	if (gear_hash_exists(class_table, Z_STR_P(lcname))) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot declare %s %s, because the name is already in use", gear_get_object_type(ce), ZSTR_VAL(ce->name));
	}

	gear_do_inheritance(ce, parent_ce);

	ce->refcount++;

	/* Register the derived class */
	if (gear_hash_add_ptr(class_table, Z_STR_P(lcname), ce) == NULL) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot declare %s %s, because the name is already in use", gear_get_object_type(ce), ZSTR_VAL(ce->name));
	}
	return ce;
}
/* }}} */

void gear_do_early_binding(void) /* {{{ */
{
	gear_op *opline = &CG(active_op_array)->opcodes[CG(active_op_array)->last-1];
	HashTable *table;

	while (opline->opcode == GEAR_TICKS && opline > CG(active_op_array)->opcodes) {
		opline--;
	}

	switch (opline->opcode) {
		case GEAR_DECLARE_FUNCTION:
			if (do_bind_function(CG(active_op_array), opline, CG(function_table), 1) == FAILURE) {
				return;
			}
			table = CG(function_table);
			break;
		case GEAR_DECLARE_CLASS:
			if (do_bind_class(CG(active_op_array), opline, CG(class_table), 1) == NULL) {
				return;
			}
			table = CG(class_table);
			break;
		case GEAR_DECLARE_INHERITED_CLASS:
			{
				zval *parent_name;
				gear_class_entry *ce;

				parent_name = CT_CONSTANT(opline->op2);
				if (((ce = gear_lookup_class_ex(Z_STR_P(parent_name), parent_name + 1, 0)) == NULL) ||
				    ((CG(compiler_options) & GEAR_COMPILE_IGNORE_INTERNAL_CLASSES) &&
				     (ce->type == GEAR_INTERNAL_CLASS))) {
					if (CG(compiler_options) & GEAR_COMPILE_DELAYED_BINDING) {
						CG(active_op_array)->fn_flags |= GEAR_ACC_EARLY_BINDING;
						opline->opcode = GEAR_DECLARE_INHERITED_CLASS_DELAYED;
						opline->result_type = IS_UNUSED;
						opline->result.opline_num = -1;
					}
					return;
				}
				if (do_bind_inherited_class(CG(active_op_array), opline, CG(class_table), ce, 1) == NULL) {
					return;
				}
				gear_del_literal(CG(active_op_array), opline->op2.constant+1);
				gear_del_literal(CG(active_op_array), opline->op2.constant);

				table = CG(class_table);
				break;
			}
		case GEAR_VERIFY_ABSTRACT_CLASS:
		case GEAR_ADD_INTERFACE:
		case GEAR_ADD_TRAIT:
		case GEAR_BIND_TRAITS:
			/* We currently don't early-bind classes that implement interfaces */
			/* Classes with traits are handled exactly the same, no early-bind here */
			return;
		default:
			gear_error_noreturn(E_COMPILE_ERROR, "Invalid binding type");
			return;
	}

	gear_hash_del(table, Z_STR_P(CT_CONSTANT(opline->op1)+1));
	gear_del_literal(CG(active_op_array), opline->op1.constant+1);
	gear_del_literal(CG(active_op_array), opline->op1.constant);
	MAKE_NOP(opline);
}
/* }}} */

static void gear_mark_function_as_generator() /* {{{ */
{
	if (!CG(active_op_array)->function_name) {
		gear_error_noreturn(E_COMPILE_ERROR,
			"The \"yield\" expression can only be used inside a function");
	}

	if (CG(active_op_array)->fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
		gear_arg_info return_info = CG(active_op_array)->arg_info[-1];

		if (GEAR_TYPE_CODE(return_info.type) != IS_ITERABLE) {
			const char *msg = "Generators may only declare a return type of Generator, Iterator, Traversable, or iterable, %s is not permitted";

			if (!GEAR_TYPE_IS_CLASS(return_info.type)) {
				gear_error_noreturn(E_COMPILE_ERROR, msg, gear_get_type_by_const(GEAR_TYPE_CODE(return_info.type)));
			}

			if (!gear_string_equals_literal_ci(GEAR_TYPE_NAME(return_info.type), "Traversable")
				&& !gear_string_equals_literal_ci(GEAR_TYPE_NAME(return_info.type), "Iterator")
				&& !gear_string_equals_literal_ci(GEAR_TYPE_NAME(return_info.type), "Generator")) {
				gear_error_noreturn(E_COMPILE_ERROR, msg, ZSTR_VAL(GEAR_TYPE_NAME(return_info.type)));
			}
		}
	}

	CG(active_op_array)->fn_flags |= GEAR_ACC_GENERATOR;
}
/* }}} */

GEAR_API uint32_t gear_build_delayed_early_binding_list(const gear_op_array *op_array) /* {{{ */
{
	if (op_array->fn_flags & GEAR_ACC_EARLY_BINDING) {
		uint32_t  first_early_binding_opline = (uint32_t)-1;
		uint32_t *prev_opline_num = &first_early_binding_opline;
		gear_op  *opline = op_array->opcodes;
		gear_op  *end = opline + op_array->last;

		while (opline < end) {
			if (opline->opcode == GEAR_DECLARE_INHERITED_CLASS_DELAYED) {
				*prev_opline_num = opline - op_array->opcodes;
				prev_opline_num = &opline->result.opline_num;
			}
			++opline;
		}
		*prev_opline_num = -1;
		return first_early_binding_opline;
	}
	return (uint32_t)-1;
}
/* }}} */

GEAR_API void gear_do_delayed_early_binding(const gear_op_array *op_array, uint32_t first_early_binding_opline) /* {{{ */
{
	if (first_early_binding_opline != (uint32_t)-1) {
		gear_bool orig_in_compilation = CG(in_compilation);
		uint32_t opline_num = first_early_binding_opline;
		gear_class_entry *ce;

		CG(in_compilation) = 1;
		while (opline_num != (uint32_t)-1) {
			const gear_op *opline = &op_array->opcodes[opline_num];
			zval *parent_name = RT_CONSTANT(opline, opline->op2);
			if ((ce = gear_lookup_class_ex(Z_STR_P(parent_name), parent_name + 1, 0)) != NULL) {
				do_bind_inherited_class(op_array, &op_array->opcodes[opline_num], EG(class_table), ce, 0);
			}
			opline_num = op_array->opcodes[opline_num].result.opline_num;
		}
		CG(in_compilation) = orig_in_compilation;
	}
}
/* }}} */

GEAR_API gear_string *gear_mangle_property_name(const char *src1, size_t src1_length, const char *src2, size_t src2_length, int internal) /* {{{ */
{
	size_t prop_name_length = 1 + src1_length + 1 + src2_length;
	gear_string *prop_name = gear_string_alloc(prop_name_length, internal);

	ZSTR_VAL(prop_name)[0] = '\0';
	memcpy(ZSTR_VAL(prop_name) + 1, src1, src1_length+1);
	memcpy(ZSTR_VAL(prop_name) + 1 + src1_length + 1, src2, src2_length+1);
	return prop_name;
}
/* }}} */

static gear_always_inline size_t gear_strnlen(const char* s, size_t maxlen) /* {{{ */
{
	size_t len = 0;
	while (*s++ && maxlen--) len++;
	return len;
}
/* }}} */

GEAR_API int gear_unmangle_property_name_ex(const gear_string *name, const char **class_name, const char **prop_name, size_t *prop_len) /* {{{ */
{
	size_t class_name_len;
	size_t anonclass_src_len;

	*class_name = NULL;

	if (!ZSTR_LEN(name) || ZSTR_VAL(name)[0] != '\0') {
		*prop_name = ZSTR_VAL(name);
		if (prop_len) {
			*prop_len = ZSTR_LEN(name);
		}
		return SUCCESS;
	}
	if (ZSTR_LEN(name) < 3 || ZSTR_VAL(name)[1] == '\0') {
		gear_error(E_NOTICE, "Illegal member variable name");
		*prop_name = ZSTR_VAL(name);
		if (prop_len) {
			*prop_len = ZSTR_LEN(name);
		}
		return FAILURE;
	}

	class_name_len = gear_strnlen(ZSTR_VAL(name) + 1, ZSTR_LEN(name) - 2);
	if (class_name_len >= ZSTR_LEN(name) - 2 || ZSTR_VAL(name)[class_name_len + 1] != '\0') {
		gear_error(E_NOTICE, "Corrupt member variable name");
		*prop_name = ZSTR_VAL(name);
		if (prop_len) {
			*prop_len = ZSTR_LEN(name);
		}
		return FAILURE;
	}

	*class_name = ZSTR_VAL(name) + 1;
	anonclass_src_len = gear_strnlen(*class_name + class_name_len + 1, ZSTR_LEN(name) - class_name_len - 2);
	if (class_name_len + anonclass_src_len + 2 != ZSTR_LEN(name)) {
		class_name_len += anonclass_src_len + 1;
	}
	*prop_name = ZSTR_VAL(name) + class_name_len + 2;
	if (prop_len) {
		*prop_len = ZSTR_LEN(name) - class_name_len - 2;
	}
	return SUCCESS;
}
/* }}} */

static gear_constant *gear_lookup_reserved_const(const char *name, size_t len) /* {{{ */
{
	gear_constant *c = gear_hash_find_ptr_lc(EG(gear_constants), name, len);
	if (c && !(GEAR_CONSTANT_FLAGS(c) & CONST_CS) && (GEAR_CONSTANT_FLAGS(c) & CONST_CT_SUBST)) {
		return c;
	}
	return NULL;
}
/* }}} */

static gear_bool gear_try_ct_eval_const(zval *zv, gear_string *name, gear_bool is_fully_qualified) /* {{{ */
{
	gear_constant *c;

	/* Substitute case-sensitive (or lowercase) constants */
	c = gear_hash_find_ptr(EG(gear_constants), name);
	if (c && (
	      ((GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT)
	      && !(CG(compiler_options) & GEAR_COMPILE_NO_PERSISTENT_CONSTANT_SUBSTITUTION)
	      && (!(GEAR_CONSTANT_FLAGS(c) & CONST_NO_FILE_CACHE) || !(CG(compiler_options) & GEAR_COMPILE_WITH_FILE_CACHE)))
	   || (Z_TYPE(c->value) < IS_OBJECT && !(CG(compiler_options) & GEAR_COMPILE_NO_CONSTANT_SUBSTITUTION))
	)) {
		ZVAL_COPY_OR_DUP(zv, &c->value);
		return 1;
	}

	{
		/* Substitute true, false and null (including unqualified usage in namespaces) */
		const char *lookup_name = ZSTR_VAL(name);
		size_t lookup_len = ZSTR_LEN(name);

		if (!is_fully_qualified) {
			gear_get_unqualified_name(name, &lookup_name, &lookup_len);
		}

		c = gear_lookup_reserved_const(lookup_name, lookup_len);
		if (c) {
			ZVAL_COPY_OR_DUP(zv, &c->value);
			return 1;
		}
	}

	return 0;
}
/* }}} */

static inline gear_bool gear_is_scope_known() /* {{{ */
{
	if (CG(active_op_array)->fn_flags & GEAR_ACC_CLOSURE) {
		/* Closures can be rebound to a different scope */
		return 0;
	}

	if (!CG(active_class_entry)) {
		/* The scope is known if we're in a free function (no scope), but not if we're in
		 * a file/eval (which inherits including/eval'ing scope). */
		return CG(active_op_array)->function_name != NULL;
	}

	/* For traits self etc refers to the using class, not the trait itself */
	return (CG(active_class_entry)->ce_flags & GEAR_ACC_TRAIT) == 0;
}
/* }}} */

static inline gear_bool class_name_refers_to_active_ce(gear_string *class_name, uint32_t fetch_type) /* {{{ */
{
	if (!CG(active_class_entry)) {
		return 0;
	}
	if (fetch_type == GEAR_FETCH_CLASS_SELF && gear_is_scope_known()) {
		return 1;
	}
	return fetch_type == GEAR_FETCH_CLASS_DEFAULT
		&& gear_string_equals_ci(class_name, CG(active_class_entry)->name);
}
/* }}} */

uint32_t gear_get_class_fetch_type(gear_string *name) /* {{{ */
{
	if (gear_string_equals_literal_ci(name, "self")) {
		return GEAR_FETCH_CLASS_SELF;
	} else if (gear_string_equals_literal_ci(name, "parent")) {
		return GEAR_FETCH_CLASS_PARENT;
	} else if (gear_string_equals_literal_ci(name, "static")) {
		return GEAR_FETCH_CLASS_STATIC;
	} else {
		return GEAR_FETCH_CLASS_DEFAULT;
	}
}
/* }}} */

static uint32_t gear_get_class_fetch_type_ast(gear_ast *name_ast) /* {{{ */
{
	/* Fully qualified names are always default refs */
	if (name_ast->attr == GEAR_NAME_FQ) {
		return GEAR_FETCH_CLASS_DEFAULT;
	}

	return gear_get_class_fetch_type(gear_ast_get_str(name_ast));
}
/* }}} */

static void gear_ensure_valid_class_fetch_type(uint32_t fetch_type) /* {{{ */
{
	if (fetch_type != GEAR_FETCH_CLASS_DEFAULT && !CG(active_class_entry) && gear_is_scope_known()) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use \"%s\" when no class scope is active",
			fetch_type == GEAR_FETCH_CLASS_SELF ? "self" :
			fetch_type == GEAR_FETCH_CLASS_PARENT ? "parent" : "static");
	}
}
/* }}} */

static gear_bool gear_try_compile_const_expr_resolve_class_name(zval *zv, gear_ast *class_ast, gear_ast *name_ast, gear_bool constant) /* {{{ */
{
	uint32_t fetch_type;
	zval *class_name;

	if (name_ast->kind != GEAR_AST_ZVAL) {
		return 0;
	}

	if (!gear_string_equals_literal_ci(gear_ast_get_str(name_ast), "class")) {
		return 0;
	}

	if (class_ast->kind != GEAR_AST_ZVAL) {
		gear_error_noreturn(E_COMPILE_ERROR,
			"Dynamic class names are not allowed in compile-time ::class fetch");
	}

	class_name = gear_ast_get_zval(class_ast);

	if (Z_TYPE_P(class_name) != IS_STRING) {
		gear_error_noreturn(E_COMPILE_ERROR, "Illegal class name");
	}

	fetch_type = gear_get_class_fetch_type(Z_STR_P(class_name));
	gear_ensure_valid_class_fetch_type(fetch_type);

	switch (fetch_type) {
		case GEAR_FETCH_CLASS_SELF:
			if (CG(active_class_entry) && gear_is_scope_known()) {
				ZVAL_STR_COPY(zv, CG(active_class_entry)->name);
			} else {
				ZVAL_NULL(zv);
			}
			return 1;
		case GEAR_FETCH_CLASS_STATIC:
		case GEAR_FETCH_CLASS_PARENT:
			if (constant) {
				gear_error_noreturn(E_COMPILE_ERROR,
					"%s::class cannot be used for compile-time class name resolution",
					fetch_type == GEAR_FETCH_CLASS_STATIC ? "static" : "parent"
				);
			} else {
				ZVAL_NULL(zv);
			}
			return 1;
		case GEAR_FETCH_CLASS_DEFAULT:
			ZVAL_STR(zv, gear_resolve_class_name_ast(class_ast));
			return 1;
		EMPTY_SWITCH_DEFAULT_CASE()
	}
}
/* }}} */

static gear_bool gear_try_ct_eval_class_const(zval *zv, gear_string *class_name, gear_string *name) /* {{{ */
{
	uint32_t fetch_type = gear_get_class_fetch_type(class_name);
	gear_class_constant *cc;
	zval *c;

	if (class_name_refers_to_active_ce(class_name, fetch_type)) {
		cc = gear_hash_find_ptr(&CG(active_class_entry)->constants_table, name);
	} else if (fetch_type == GEAR_FETCH_CLASS_DEFAULT && !(CG(compiler_options) & GEAR_COMPILE_NO_CONSTANT_SUBSTITUTION)) {
		gear_class_entry *ce = gear_hash_find_ptr_lc(CG(class_table), ZSTR_VAL(class_name), ZSTR_LEN(class_name));
		if (ce) {
			cc = gear_hash_find_ptr(&ce->constants_table, name);
		} else {
			return 0;
		}
	} else {
		return 0;
	}

	if (CG(compiler_options) & GEAR_COMPILE_NO_PERSISTENT_CONSTANT_SUBSTITUTION) {
		return 0;
	}

	if (!cc || !gear_verify_const_access(cc, CG(active_class_entry))) {
		return 0;
	}

	c = &cc->value;

	/* Substitute case-sensitive (or lowercase) persistent class constants */
	if (Z_TYPE_P(c) < IS_OBJECT) {
		ZVAL_COPY_OR_DUP(zv, c);
		return 1;
	}

	return 0;
}
/* }}} */

static void gear_add_to_list(void *result, void *item) /* {{{ */
{
	void** list = *(void**)result;
	size_t n = 0;

	if (list) {
		while (list[n]) {
			n++;
		}
	}

	list = erealloc(list, sizeof(void*) * (n+2));

	list[n]   = item;
	list[n+1] = NULL;

	*(void**)result = list;
}
/* }}} */

void gear_do_extended_info(void) /* {{{ */
{
	gear_op *opline;

	if (!(CG(compiler_options) & GEAR_COMPILE_EXTENDED_INFO)) {
		return;
	}

	opline = get_next_op(CG(active_op_array));

	opline->opcode = GEAR_EXT_STMT;
}
/* }}} */

void gear_do_extended_fcall_begin(void) /* {{{ */
{
	gear_op *opline;

	if (!(CG(compiler_options) & GEAR_COMPILE_EXTENDED_INFO)) {
		return;
	}

	opline = get_next_op(CG(active_op_array));

	opline->opcode = GEAR_EXT_FCALL_BEGIN;
}
/* }}} */

void gear_do_extended_fcall_end(void) /* {{{ */
{
	gear_op *opline;

	if (!(CG(compiler_options) & GEAR_COMPILE_EXTENDED_INFO)) {
		return;
	}

	opline = get_next_op(CG(active_op_array));

	opline->opcode = GEAR_EXT_FCALL_END;
}
/* }}} */

gear_bool gear_is_auto_global_str(char *name, size_t len) /* {{{ */ {
	gear_auto_global *auto_global;

	if ((auto_global = gear_hash_str_find_ptr(CG(auto_globals), name, len)) != NULL) {
		if (auto_global->armed) {
			auto_global->armed = auto_global->auto_global_callback(auto_global->name);
		}
		return 1;
	}
	return 0;
}
/* }}} */

gear_bool gear_is_auto_global(gear_string *name) /* {{{ */
{
	gear_auto_global *auto_global;

	if ((auto_global = gear_hash_find_ptr(CG(auto_globals), name)) != NULL) {
		if (auto_global->armed) {
			auto_global->armed = auto_global->auto_global_callback(auto_global->name);
		}
		return 1;
	}
	return 0;
}
/* }}} */

int gear_register_auto_global(gear_string *name, gear_bool jit, gear_auto_global_callback auto_global_callback) /* {{{ */
{
	gear_auto_global auto_global;
	int retval;

	auto_global.name = name;
	auto_global.auto_global_callback = auto_global_callback;
	auto_global.jit = jit;

	retval = gear_hash_add_mem(CG(auto_globals), auto_global.name, &auto_global, sizeof(gear_auto_global)) != NULL ? SUCCESS : FAILURE;

	return retval;
}
/* }}} */

GEAR_API void gear_activate_auto_globals(void) /* {{{ */
{
	gear_auto_global *auto_global;

	GEAR_HASH_FOREACH_PTR(CG(auto_globals), auto_global) {
		if (auto_global->jit) {
			auto_global->armed = 1;
		} else if (auto_global->auto_global_callback) {
			auto_global->armed = auto_global->auto_global_callback(auto_global->name);
		} else {
			auto_global->armed = 0;
		}
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

int GEAR_FASTCALL gearlex(gear_parser_stack_elem *elem) /* {{{ */
{
	zval zv;

	if (CG(increment_lineno)) {
		CG(gear_lineno)++;
		CG(increment_lineno) = 0;
	}

	return lex_scan(&zv, elem);
}
/* }}} */

GEAR_API void gear_initialize_class_data(gear_class_entry *ce, gear_bool nullify_handlers) /* {{{ */
{
	gear_bool persistent_hashes = (ce->type == GEAR_INTERNAL_CLASS) ? 1 : 0;

	ce->refcount = 1;
	ce->ce_flags = GEAR_ACC_CONSTANTS_UPDATED;

	if (CG(compiler_options) & GEAR_COMPILE_GUARDS) {
		ce->ce_flags |= GEAR_ACC_USE_GUARDS;
	}

	ce->default_properties_table = NULL;
	ce->default_static_members_table = NULL;
	gear_hash_init_ex(&ce->properties_info, 8, NULL, (persistent_hashes ? gear_destroy_property_info_internal : NULL), persistent_hashes, 0);
	gear_hash_init_ex(&ce->constants_table, 8, NULL, NULL, persistent_hashes, 0);
	gear_hash_init_ex(&ce->function_table, 8, NULL, GEAR_FUNCTION_DTOR, persistent_hashes, 0);

	if (ce->type == GEAR_INTERNAL_CLASS) {
#ifdef ZTS
		int n = gear_hash_num_elements(CG(class_table));

		if (CG(static_members_table) && n >= CG(last_static_member)) {
			/* Support for run-time declaration: dl() */
			CG(last_static_member) = n+1;
			CG(static_members_table) = realloc(CG(static_members_table), (n+1)*sizeof(zval*));
			CG(static_members_table)[n] = NULL;
		}
		ce->static_members_table = (zval*)(gear_intptr_t)n;
#else
		ce->static_members_table = NULL;
#endif
	} else {
		ce->static_members_table = ce->default_static_members_table;
		ce->info.user.doc_comment = NULL;
	}

	ce->default_properties_count = 0;
	ce->default_static_members_count = 0;

	if (nullify_handlers) {
		ce->constructor = NULL;
		ce->destructor = NULL;
		ce->clone = NULL;
		ce->__get = NULL;
		ce->__set = NULL;
		ce->__unset = NULL;
		ce->__isset = NULL;
		ce->__call = NULL;
		ce->__callstatic = NULL;
		ce->__tostring = NULL;
		ce->create_object = NULL;
		ce->get_iterator = NULL;
		ce->iterator_funcs_ptr = NULL;
		ce->get_static_method = NULL;
		ce->parent = NULL;
		ce->num_interfaces = 0;
		ce->interfaces = NULL;
		ce->num_traits = 0;
		ce->traits = NULL;
		ce->trait_aliases = NULL;
		ce->trait_precedences = NULL;
		ce->serialize = NULL;
		ce->unserialize = NULL;
		ce->serialize_func = NULL;
		ce->unserialize_func = NULL;
		ce->__debugInfo = NULL;
		if (ce->type == GEAR_INTERNAL_CLASS) {
			ce->info.internal.cAPI = NULL;
			ce->info.internal.builtin_functions = NULL;
		}
	}
}
/* }}} */

GEAR_API gear_string *gear_get_compiled_variable_name(const gear_op_array *op_array, uint32_t var) /* {{{ */
{
	return op_array->vars[EX_VAR_TO_NUM(var)];
}
/* }}} */

gear_ast *gear_ast_append_str(gear_ast *left_ast, gear_ast *right_ast) /* {{{ */
{
	zval *left_zv = gear_ast_get_zval(left_ast);
	gear_string *left = Z_STR_P(left_zv);
	gear_string *right = gear_ast_get_str(right_ast);

	gear_string *result;
	size_t left_len = ZSTR_LEN(left);
	size_t len = left_len + ZSTR_LEN(right) + 1; /* left\right */

	result = gear_string_extend(left, len, 0);
	ZSTR_VAL(result)[left_len] = '\\';
	memcpy(&ZSTR_VAL(result)[left_len + 1], ZSTR_VAL(right), ZSTR_LEN(right));
	ZSTR_VAL(result)[len] = '\0';
	gear_string_release_ex(right, 0);

	ZVAL_STR(left_zv, result);
	return left_ast;
}
/* }}} */

gear_ast *gear_negate_num_string(gear_ast *ast) /* {{{ */
{
	zval *zv = gear_ast_get_zval(ast);
	if (Z_TYPE_P(zv) == IS_LONG) {
		if (Z_LVAL_P(zv) == 0) {
			ZVAL_NEW_STR(zv, gear_string_init("-0", sizeof("-0")-1, 0));
		} else {
			GEAR_ASSERT(Z_LVAL_P(zv) > 0);
			Z_LVAL_P(zv) *= -1;
		}
	} else if (Z_TYPE_P(zv) == IS_STRING) {
		size_t orig_len = Z_STRLEN_P(zv);
		Z_STR_P(zv) = gear_string_extend(Z_STR_P(zv), orig_len + 1, 0);
		memmove(Z_STRVAL_P(zv) + 1, Z_STRVAL_P(zv), orig_len + 1);
		Z_STRVAL_P(zv)[0] = '-';
	} else {
		GEAR_ASSERT(0);
	}
	return ast;
}
/* }}} */

void gear_verify_namespace(void) /* {{{ */
{
	if (FC(has_bracketed_namespaces) && !FC(in_namespace)) {
		gear_error_noreturn(E_COMPILE_ERROR, "No code may exist outside of namespace {}");
	}
}
/* }}} */

/* {{{ gear_dirname
   Returns directory name component of path */
GEAR_API size_t gear_dirname(char *path, size_t len)
{
	register char *end = path + len - 1;
	unsigned int len_adjust = 0;

#ifdef GEAR_WIN32
	/* Note that on Win32 CWD is per drive (heritage from CP/M).
	 * This means dirname("c:foo") maps to "c:." or "c:" - which means CWD on C: drive.
	 */
	if ((2 <= len) && isalpha((int)((unsigned char *)path)[0]) && (':' == path[1])) {
		/* Skip over the drive spec (if any) so as not to change */
		path += 2;
		len_adjust += 2;
		if (2 == len) {
			/* Return "c:" on Win32 for dirname("c:").
			 * It would be more consistent to return "c:."
			 * but that would require making the string *longer*.
			 */
			return len;
		}
	}
#endif

	if (len == 0) {
		/* Illegal use of this function */
		return 0;
	}

	/* Strip trailing slashes */
	while (end >= path && IS_SLASH_P(end)) {
		end--;
	}
	if (end < path) {
		/* The path only contained slashes */
		path[0] = DEFAULT_SLASH;
		path[1] = '\0';
		return 1 + len_adjust;
	}

	/* Strip filename */
	while (end >= path && !IS_SLASH_P(end)) {
		end--;
	}
	if (end < path) {
		/* No slash found, therefore return '.' */
		path[0] = '.';
		path[1] = '\0';
		return 1 + len_adjust;
	}

	/* Strip slashes which came before the file name */
	while (end >= path && IS_SLASH_P(end)) {
		end--;
	}
	if (end < path) {
		path[0] = DEFAULT_SLASH;
		path[1] = '\0';
		return 1 + len_adjust;
	}
	*(end+1) = '\0';

	return (size_t)(end + 1 - path) + len_adjust;
}
/* }}} */

static void gear_adjust_for_fetch_type(gear_op *opline, znode *result, uint32_t type) /* {{{ */
{
	gear_uchar factor = (opline->opcode == GEAR_FETCH_STATIC_PROP_R) ? 1 : 3;

	switch (type) {
		case BP_VAR_R:
			opline->result_type = IS_TMP_VAR;
			result->op_type = IS_TMP_VAR;
			return;
		case BP_VAR_W:
			opline->opcode += 1 * factor;
			return;
		case BP_VAR_RW:
			opline->opcode += 2 * factor;
			return;
		case BP_VAR_IS:
			opline->result_type = IS_TMP_VAR;
			result->op_type = IS_TMP_VAR;
			opline->opcode += 3 * factor;
			return;
		case BP_VAR_FUNC_ARG:
			opline->opcode += 4 * factor;
			return;
		case BP_VAR_UNSET:
			opline->opcode += 5 * factor;
			return;
		EMPTY_SWITCH_DEFAULT_CASE()
	}
}
/* }}} */

static inline void gear_make_var_result(znode *result, gear_op *opline) /* {{{ */
{
	opline->result_type = IS_VAR;
	opline->result.var = get_temporary_variable(CG(active_op_array));
	GET_NODE(result, opline->result);
}
/* }}} */

static inline void gear_make_tmp_result(znode *result, gear_op *opline) /* {{{ */
{
	opline->result_type = IS_TMP_VAR;
	opline->result.var = get_temporary_variable(CG(active_op_array));
	GET_NODE(result, opline->result);
}
/* }}} */

static void gear_find_live_range(gear_op *opline, gear_uchar type, uint32_t var) /* {{{ */
{
	gear_op *def = opline;

	while (def != CG(active_op_array)->opcodes) {
		def--;
		if (def->result_type == type && def->result.var == var) {
			if (def->opcode == GEAR_ADD_ARRAY_ELEMENT ||
			    def->opcode == GEAR_ROPE_ADD) {
			    /* not a real definition */
				continue;
			} else if (def->opcode == GEAR_JMPZ_EX ||
			           def->opcode == GEAR_JMPNZ_EX ||
			           def->opcode == GEAR_BOOL ||
			           def->opcode == GEAR_BOOL_NOT) {
				/* result IS_BOOL, it does't have to be destroyed */
				break;
			} else if (def->opcode == GEAR_DECLARE_CLASS ||
			           def->opcode == GEAR_DECLARE_INHERITED_CLASS ||
			           def->opcode == GEAR_DECLARE_INHERITED_CLASS_DELAYED ||
			           def->opcode == GEAR_DECLARE_ANON_CLASS ||
			           def->opcode == GEAR_DECLARE_ANON_INHERITED_CLASS) {
				/* classes don't have to be destroyed */
				break;
			} else if (def->opcode == GEAR_FAST_CALL) {
				/* fast_calls don't have to be destroyed */
				break;
			} else if (def->opcode == GEAR_NEW) {
				/* Objects created via GEAR_NEW are only fully initialized
				 * after the DO_FCALL (constructor call) */
				int level = 0;
				while (def + 1 != opline) {
					def++;
					if (def->opcode == GEAR_DO_FCALL) {
						if (level == 0) {
							break;
						}
						level--;
					} else {
						switch(def->opcode) {
							case GEAR_INIT_FCALL:
							case GEAR_INIT_FCALL_BY_NAME:
							case GEAR_INIT_NS_FCALL_BY_NAME:
							case GEAR_INIT_DYNAMIC_CALL:
							case GEAR_INIT_USER_CALL:
							case GEAR_INIT_METHOD_CALL:
							case GEAR_INIT_STATIC_METHOD_CALL:
							case GEAR_NEW:
								level++;
								break;
							case GEAR_DO_ICALL:
							case GEAR_DO_UCALL:
							case GEAR_DO_FCALL_BY_NAME:
								level--;
								break;
						}
					}
				}
				if (def + 1 == opline) {
					break;
				}
			}

	        gear_end_live_range(CG(active_op_array),
				gear_start_live_range(CG(active_op_array),
					def + 1 - CG(active_op_array)->opcodes),
				opline - CG(active_op_array)->opcodes,
				GEAR_LIVE_TMPVAR, var);
		    break;
		}
	}
}
/* }}} */

static gear_always_inline int gear_is_def_range(gear_op *opline, gear_uchar type, uint32_t var) /* {{{ */
{
	while (1) {
		if (opline->result_type == type && opline->result.var == var) {
			return opline->opcode != GEAR_ADD_ARRAY_ELEMENT &&
				opline->opcode != GEAR_ROPE_ADD;
		} else if (opline->opcode == GEAR_OP_DATA) {
			return (opline-1)->result_type == type &&
				(opline-1)->result.var == var;
		} else if (opline->opcode == GEAR_END_SILENCE ||
		           opline->opcode == GEAR_NOP ||
	    	       opline->opcode == GEAR_EXT_NOP ||
	        	   opline->opcode == GEAR_EXT_STMT ||
	        	   opline->opcode == GEAR_EXT_FCALL_BEGIN ||
	        	   opline->opcode == GEAR_EXT_FCALL_END ||
	        	   opline->opcode == GEAR_TICKS) {
			opline--;
		} else {
			return 0;
		}
	}
}
/* }}} */

static void gear_check_live_ranges(gear_op *opline) /* {{{ */
{
	if ((opline->op1_type & (IS_VAR|IS_TMP_VAR)) &&
		!gear_is_def_range(opline - 1, opline->op1_type, opline->op1.var)) {

		if (opline->opcode == GEAR_OP_DATA) {
			if (!gear_is_def_range(opline - 2, opline->op1_type, opline->op1.var)) {
				gear_find_live_range(opline - 1, opline->op1_type, opline->op1.var);
			}
		} else if (opline->opcode == GEAR_INIT_STATIC_METHOD_CALL ||
		           opline->opcode == GEAR_NEW ||
		           opline->opcode == GEAR_FETCH_CLASS_CONSTANT ||
		           opline->opcode == GEAR_ADD_INTERFACE ||
		           opline->opcode == GEAR_ADD_TRAIT ||
		           opline->opcode == GEAR_BIND_TRAITS ||
		           opline->opcode == GEAR_VERIFY_ABSTRACT_CLASS) {
			/* classes don't have to be destroyed */
		} else if (opline->opcode == GEAR_FAST_RET) {
			/* fast_calls don't have to be destroyed */
		} else if (opline->opcode == GEAR_CASE ||
		           opline->opcode == GEAR_SWITCH_LONG ||
		           opline->opcode == GEAR_SWITCH_STRING ||
		           opline->opcode == GEAR_FE_FETCH_R ||
		           opline->opcode == GEAR_FE_FETCH_RW ||
			       opline->opcode == GEAR_FE_FREE ||
			       opline->opcode == GEAR_ROPE_ADD ||
			       opline->opcode == GEAR_ROPE_END ||
			       opline->opcode == GEAR_END_SILENCE ||
			       opline->opcode == GEAR_FETCH_LIST_R ||
			       opline->opcode == GEAR_FETCH_LIST_W ||
			       opline->opcode == GEAR_VERIFY_RETURN_TYPE ||
			       opline->opcode == GEAR_BIND_LEXICAL) {
			/* these opcodes are handled separately */
		} else {
			gear_find_live_range(opline, opline->op1_type, opline->op1.var);
		}
	}

	if ((opline->op2_type & (IS_VAR|IS_TMP_VAR)) &&
		!gear_is_def_range(opline - 1, opline->op2_type, opline->op2.var)) {

		if (opline->opcode == GEAR_OP_DATA) {
			if (!gear_is_def_range(opline - 2, opline->op2_type, opline->op2.var)) {
				gear_find_live_range(opline-1, opline->op2_type, opline->op2.var);
			}
		} else if (opline->opcode == GEAR_FETCH_STATIC_PROP_R ||
		           opline->opcode == GEAR_FETCH_STATIC_PROP_W ||
		           opline->opcode == GEAR_FETCH_STATIC_PROP_RW ||
		           opline->opcode == GEAR_FETCH_STATIC_PROP_IS ||
		           opline->opcode == GEAR_FETCH_STATIC_PROP_FUNC_ARG ||
		           opline->opcode == GEAR_FETCH_STATIC_PROP_UNSET ||
		           opline->opcode == GEAR_UNSET_STATIC_PROP ||
		           opline->opcode == GEAR_ISSET_ISEMPTY_STATIC_PROP ||
		           opline->opcode == GEAR_INSTANCEOF) {
			/* classes don't have to be destroyed */
		} else {
			gear_find_live_range(opline, opline->op2_type, opline->op2.var);
		}
	}
}
/* }}} */

static gear_op *gear_emit_op(znode *result, gear_uchar opcode, znode *op1, znode *op2) /* {{{ */
{
	gear_op *opline = get_next_op(CG(active_op_array));
	opline->opcode = opcode;

	if (op1 != NULL) {
		SET_NODE(opline->op1, op1);
	}

	if (op2 != NULL) {
		SET_NODE(opline->op2, op2);
	}

	gear_check_live_ranges(opline);

	if (result) {
		gear_make_var_result(result, opline);
	}
	return opline;
}
/* }}} */

static gear_op *gear_emit_op_tmp(znode *result, gear_uchar opcode, znode *op1, znode *op2) /* {{{ */
{
	gear_op *opline = get_next_op(CG(active_op_array));
	opline->opcode = opcode;

	if (op1 != NULL) {
		SET_NODE(opline->op1, op1);
	}

	if (op2 != NULL) {
		SET_NODE(opline->op2, op2);
	}

	gear_check_live_ranges(opline);

	if (result) {
		gear_make_tmp_result(result, opline);
	}

	return opline;
}
/* }}} */

static void gear_emit_tick(void) /* {{{ */
{
	gear_op *opline;

	/* This prevents a double TICK generated by the parser statement of "declare()" */
	if (CG(active_op_array)->last && CG(active_op_array)->opcodes[CG(active_op_array)->last - 1].opcode == GEAR_TICKS) {
		return;
	}

	opline = get_next_op(CG(active_op_array));

	opline->opcode = GEAR_TICKS;
	opline->extended_value = FC(declarables).ticks;
}
/* }}} */

static inline gear_op *gear_emit_op_data(znode *value) /* {{{ */
{
	return gear_emit_op(NULL, GEAR_OP_DATA, value, NULL);
}
/* }}} */

static inline uint32_t gear_emit_jump(uint32_t opnum_target) /* {{{ */
{
	uint32_t opnum = get_next_op_number(CG(active_op_array));
	gear_op *opline = gear_emit_op(NULL, GEAR_JMP, NULL, NULL);
	opline->op1.opline_num = opnum_target;
	return opnum;
}
/* }}} */

GEAR_API int gear_is_smart_branch(gear_op *opline) /* {{{ */
{
	switch (opline->opcode) {
		case GEAR_IS_IDENTICAL:
		case GEAR_IS_NOT_IDENTICAL:
		case GEAR_IS_EQUAL:
		case GEAR_IS_NOT_EQUAL:
		case GEAR_IS_SMALLER:
		case GEAR_IS_SMALLER_OR_EQUAL:
		case GEAR_CASE:
		case GEAR_ISSET_ISEMPTY_CV:
		case GEAR_ISSET_ISEMPTY_VAR:
		case GEAR_ISSET_ISEMPTY_DIM_OBJ:
		case GEAR_ISSET_ISEMPTY_PROP_OBJ:
		case GEAR_ISSET_ISEMPTY_STATIC_PROP:
		case GEAR_INSTANCEOF:
		case GEAR_TYPE_CHECK:
		case GEAR_DEFINED:
		case GEAR_IN_ARRAY:
			return 1;
		default:
			return 0;
	}
}
/* }}} */

static inline uint32_t gear_emit_cond_jump(gear_uchar opcode, znode *cond, uint32_t opnum_target) /* {{{ */
{
	uint32_t opnum = get_next_op_number(CG(active_op_array));
	gear_op *opline;

	if ((cond->op_type & (IS_CV|IS_CONST))
	 && opnum > 0
	 && gear_is_smart_branch(CG(active_op_array)->opcodes + opnum - 1)) {
		/* emit extra NOP to avoid incorrect SMART_BRANCH in very rare cases */
		gear_emit_op(NULL, GEAR_NOP, NULL, NULL);
		opnum = get_next_op_number(CG(active_op_array));
	}
	opline = gear_emit_op(NULL, opcode, cond, NULL);
	opline->op2.opline_num = opnum_target;
	return opnum;
}
/* }}} */

static inline void gear_update_jump_target(uint32_t opnum_jump, uint32_t opnum_target) /* {{{ */
{
	gear_op *opline = &CG(active_op_array)->opcodes[opnum_jump];
	switch (opline->opcode) {
		case GEAR_JMP:
			opline->op1.opline_num = opnum_target;
			break;
		case GEAR_JMPZ:
		case GEAR_JMPNZ:
		case GEAR_JMPZ_EX:
		case GEAR_JMPNZ_EX:
		case GEAR_JMP_SET:
			opline->op2.opline_num = opnum_target;
			break;
		EMPTY_SWITCH_DEFAULT_CASE()
	}
}
/* }}} */

static inline void gear_update_jump_target_to_next(uint32_t opnum_jump) /* {{{ */
{
	gear_update_jump_target(opnum_jump, get_next_op_number(CG(active_op_array)));
}
/* }}} */

static inline gear_op *gear_delayed_emit_op(znode *result, gear_uchar opcode, znode *op1, znode *op2) /* {{{ */
{
	gear_op tmp_opline;

	init_op(&tmp_opline);

	tmp_opline.opcode = opcode;
	if (op1 != NULL) {
		SET_NODE(tmp_opline.op1, op1);
	}
	if (op2 != NULL) {
		SET_NODE(tmp_opline.op2, op2);
	}
	if (result) {
		gear_make_var_result(result, &tmp_opline);
	}

	gear_stack_push(&CG(delayed_oplines_stack), &tmp_opline);
	return gear_stack_top(&CG(delayed_oplines_stack));
}
/* }}} */

static inline uint32_t gear_delayed_compile_begin(void) /* {{{ */
{
	return gear_stack_count(&CG(delayed_oplines_stack));
}
/* }}} */

static gear_op *gear_delayed_compile_end(uint32_t offset) /* {{{ */
{
	gear_op *opline = NULL, *oplines = gear_stack_base(&CG(delayed_oplines_stack));
	uint32_t i, count = gear_stack_count(&CG(delayed_oplines_stack));

	GEAR_ASSERT(count >= offset);
	for (i = offset; i < count; ++i) {
		opline = get_next_op(CG(active_op_array));
		memcpy(opline, &oplines[i], sizeof(gear_op));
		gear_check_live_ranges(opline);
	}
	CG(delayed_oplines_stack).top = offset;
	return opline;
}
/* }}} */

static void gear_emit_return_type_check(
		znode *expr, gear_arg_info *return_info, gear_bool implicit) /* {{{ */
{
	if (GEAR_TYPE_IS_SET(return_info->type)) {
		gear_op *opline;

		/* `return ...;` is illegal in a void function (but `return;` isn't) */
		if (GEAR_TYPE_CODE(return_info->type) == IS_VOID) {
			if (expr) {
				if (expr->op_type == IS_CONST && Z_TYPE(expr->u.constant) == IS_NULL) {
					gear_error_noreturn(E_COMPILE_ERROR,
						"A void function must not return a value "
						"(did you mean \"return;\" instead of \"return null;\"?)");
				} else {
					gear_error_noreturn(E_COMPILE_ERROR, "A void function must not return a value");
				}
			}
			/* we don't need run-time check */
			return;
		}

		if (!expr && !implicit) {
			if (GEAR_TYPE_ALLOW_NULL(return_info->type)) {
				gear_error_noreturn(E_COMPILE_ERROR,
					"A function with return type must return a value "
					"(did you mean \"return null;\" instead of \"return;\"?)");
			} else {
				gear_error_noreturn(E_COMPILE_ERROR,
					"A function with return type must return a value");
			}
		}

		if (expr && expr->op_type == IS_CONST) {
			if ((GEAR_TYPE_CODE(return_info->type) == Z_TYPE(expr->u.constant))
			 ||((GEAR_TYPE_CODE(return_info->type) == _IS_BOOL)
			  && (Z_TYPE(expr->u.constant) == IS_FALSE
			   || Z_TYPE(expr->u.constant) == IS_TRUE))
			 || (GEAR_TYPE_ALLOW_NULL(return_info->type)
			  && Z_TYPE(expr->u.constant) == IS_NULL)) {
				/* we don't need run-time check */
				return;
			}
		}

		opline = gear_emit_op(NULL, GEAR_VERIFY_RETURN_TYPE, expr, NULL);
		if (expr && expr->op_type == IS_CONST) {
			opline->result_type = expr->op_type = IS_TMP_VAR;
			opline->result.var = expr->u.op.var = get_temporary_variable(CG(active_op_array));
		}
		if (GEAR_TYPE_IS_CLASS(return_info->type)) {
			opline->op2.num = CG(active_op_array)->cache_size;
			CG(active_op_array)->cache_size += sizeof(void*);
		} else {
			opline->op2.num = -1;
		}
	}
}
/* }}} */

void gear_emit_final_return(int return_one) /* {{{ */
{
	znode zn;
	gear_op *ret;
	gear_bool returns_reference = (CG(active_op_array)->fn_flags & GEAR_ACC_RETURN_REFERENCE) != 0;

	if (CG(active_op_array)->fn_flags & GEAR_ACC_HAS_RETURN_TYPE
			&& !(CG(active_op_array)->fn_flags & GEAR_ACC_GENERATOR)) {
		gear_emit_return_type_check(NULL, CG(active_op_array)->arg_info - 1, 1);
	}

	zn.op_type = IS_CONST;
	if (return_one) {
		ZVAL_LONG(&zn.u.constant, 1);
	} else {
		ZVAL_NULL(&zn.u.constant);
	}

	ret = gear_emit_op(NULL, returns_reference ? GEAR_RETURN_BY_REF : GEAR_RETURN, &zn, NULL);
	ret->extended_value = -1;
}
/* }}} */

static inline gear_bool gear_is_variable(gear_ast *ast) /* {{{ */
{
	return ast->kind == GEAR_AST_VAR || ast->kind == GEAR_AST_DIM
		|| ast->kind == GEAR_AST_PROP || ast->kind == GEAR_AST_STATIC_PROP
		|| ast->kind == GEAR_AST_CALL || ast->kind == GEAR_AST_METHOD_CALL
		|| ast->kind == GEAR_AST_STATIC_CALL;
}
/* }}} */

static inline gear_bool gear_is_call(gear_ast *ast) /* {{{ */
{
	return ast->kind == GEAR_AST_CALL
		|| ast->kind == GEAR_AST_METHOD_CALL
		|| ast->kind == GEAR_AST_STATIC_CALL;
}
/* }}} */

static inline gear_bool gear_is_unticked_stmt(gear_ast *ast) /* {{{ */
{
	return ast->kind == GEAR_AST_STMT_LIST || ast->kind == GEAR_AST_LABEL
		|| ast->kind == GEAR_AST_PROP_DECL || ast->kind == GEAR_AST_CLASS_CONST_DECL
		|| ast->kind == GEAR_AST_USE_TRAIT || ast->kind == GEAR_AST_METHOD;
}
/* }}} */

static inline gear_bool gear_can_write_to_variable(gear_ast *ast) /* {{{ */
{
	while (ast->kind == GEAR_AST_DIM || ast->kind == GEAR_AST_PROP) {
		ast = ast->child[0];
	}

	return gear_is_variable(ast);
}
/* }}} */

static inline gear_bool gear_is_const_default_class_ref(gear_ast *name_ast) /* {{{ */
{
	if (name_ast->kind != GEAR_AST_ZVAL) {
		return 0;
	}

	return GEAR_FETCH_CLASS_DEFAULT == gear_get_class_fetch_type_ast(name_ast);
}
/* }}} */

static inline void gear_handle_numeric_op(znode *node) /* {{{ */
{
	if (node->op_type == IS_CONST && Z_TYPE(node->u.constant) == IS_STRING) {
		gear_ulong index;

		if (GEAR_HANDLE_NUMERIC(Z_STR(node->u.constant), index)) {
			zval_ptr_dtor(&node->u.constant);
			ZVAL_LONG(&node->u.constant, index);
		}
	}
}
/* }}} */

static inline void gear_handle_numeric_dim(gear_op *opline, znode *dim_node) /* {{{ */
{
	if (Z_TYPE(dim_node->u.constant) == IS_STRING) {
		gear_ulong index;

		if (GEAR_HANDLE_NUMERIC(Z_STR(dim_node->u.constant), index)) {
			/* For numeric indexs we also keep the original value to use by ArrayAccess
			 * See bug #63217
			 */
			int c = gear_add_literal(CG(active_op_array), &dim_node->u.constant);
			GEAR_ASSERT(opline->op2.constant + 1 == c);
			ZVAL_LONG(CT_CONSTANT(opline->op2), index);
			Z_EXTRA_P(CT_CONSTANT(opline->op2)) = GEAR_EXTRA_VALUE;
			return;
		}
	}
}
/* }}} */

static inline void gear_set_class_name_op1(gear_op *opline, znode *class_node) /* {{{ */
{
	if (class_node->op_type == IS_CONST) {
		opline->op1_type = IS_CONST;
		opline->op1.constant = gear_add_class_name_literal(
			CG(active_op_array), Z_STR(class_node->u.constant));
	} else {
		SET_NODE(opline->op1, class_node);
	}
}
/* }}} */

static void gear_compile_class_ref_ex(znode *result, gear_ast *name_ast, uint32_t fetch_flags) /* {{{ */
{
	uint32_t fetch_type;

	if (name_ast->kind != GEAR_AST_ZVAL) {
		znode name_node;

		gear_compile_expr(&name_node, name_ast);

		if (name_node.op_type == IS_CONST) {
			gear_string *name;

			if (Z_TYPE(name_node.u.constant) != IS_STRING) {
				gear_error_noreturn(E_COMPILE_ERROR, "Illegal class name");
			}

			name = Z_STR(name_node.u.constant);
			fetch_type = gear_get_class_fetch_type(name);

			if (fetch_type == GEAR_FETCH_CLASS_DEFAULT) {
				result->op_type = IS_CONST;
				ZVAL_STR(&result->u.constant, gear_resolve_class_name(name, GEAR_NAME_FQ));
			} else {
				gear_ensure_valid_class_fetch_type(fetch_type);
				result->op_type = IS_UNUSED;
				result->u.op.num = fetch_type | fetch_flags;
			}

			gear_string_release_ex(name, 0);
		} else {
			gear_op *opline = gear_emit_op(result, GEAR_FETCH_CLASS, NULL, &name_node);
			opline->op1.num = GEAR_FETCH_CLASS_DEFAULT | fetch_flags;
		}
		return;
	}

	/* Fully qualified names are always default refs */
	if (name_ast->attr == GEAR_NAME_FQ) {
		result->op_type = IS_CONST;
		ZVAL_STR(&result->u.constant, gear_resolve_class_name_ast(name_ast));
		return;
	}

	fetch_type = gear_get_class_fetch_type(gear_ast_get_str(name_ast));
	if (GEAR_FETCH_CLASS_DEFAULT == fetch_type) {
		result->op_type = IS_CONST;
		ZVAL_STR(&result->u.constant, gear_resolve_class_name_ast(name_ast));
	} else {
		gear_ensure_valid_class_fetch_type(fetch_type);
		result->op_type = IS_UNUSED;
		result->u.op.num = fetch_type | fetch_flags;
	}
}
/* }}} */

static int gear_try_compile_cv(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *name_ast = ast->child[0];
	if (name_ast->kind == GEAR_AST_ZVAL) {
		zval *zv = gear_ast_get_zval(name_ast);
		gear_string *name;

		if (EXPECTED(Z_TYPE_P(zv) == IS_STRING)) {
			name = zval_make_interned_string(zv);
		} else {
			name = gear_new_interned_string(zval_get_string_func(zv));
		}

		if (gear_is_auto_global(name)) {
			return FAILURE;
		}

		result->op_type = IS_CV;
		result->u.op.var = lookup_cv(CG(active_op_array), name);

		if (UNEXPECTED(Z_TYPE_P(zv) != IS_STRING)) {
			gear_string_release_ex(name, 0);
		}

		return SUCCESS;
	}

	return FAILURE;
}
/* }}} */

static gear_op *gear_compile_simple_var_no_cv(znode *result, gear_ast *ast, uint32_t type, int delayed) /* {{{ */
{
	gear_ast *name_ast = ast->child[0];
	znode name_node;
	gear_op *opline;

	gear_compile_expr(&name_node, name_ast);
	if (name_node.op_type == IS_CONST) {
		convert_to_string(&name_node.u.constant);
	}

	if (delayed) {
		opline = gear_delayed_emit_op(result, GEAR_FETCH_R, &name_node, NULL);
	} else {
		opline = gear_emit_op(result, GEAR_FETCH_R, &name_node, NULL);
	}

	if (name_node.op_type == IS_CONST &&
	    gear_is_auto_global(Z_STR(name_node.u.constant))) {

		opline->extended_value = GEAR_FETCH_GLOBAL;
	} else {
		opline->extended_value = GEAR_FETCH_LOCAL;
	}

	gear_adjust_for_fetch_type(opline, result, type);
	return opline;
}
/* }}} */

static gear_bool is_this_fetch(gear_ast *ast) /* {{{ */
{
	if (ast->kind == GEAR_AST_VAR && ast->child[0]->kind == GEAR_AST_ZVAL) {
		zval *name = gear_ast_get_zval(ast->child[0]);
		return Z_TYPE_P(name) == IS_STRING && gear_string_equals_literal(Z_STR_P(name), "this");
	}

	return 0;
}
/* }}} */

static void gear_compile_simple_var(znode *result, gear_ast *ast, uint32_t type, int delayed) /* {{{ */
{
	if (is_this_fetch(ast)) {
		gear_op *opline = gear_emit_op(result, GEAR_FETCH_THIS, NULL, NULL);
		if ((type == BP_VAR_R) || (type == BP_VAR_IS)) {
			opline->result_type = IS_TMP_VAR;
			result->op_type = IS_TMP_VAR;
		}
	} else if (gear_try_compile_cv(result, ast) == FAILURE) {
		gear_compile_simple_var_no_cv(result, ast, type, delayed);
	}
}
/* }}} */

static void gear_separate_if_call_and_write(znode *node, gear_ast *ast, uint32_t type) /* {{{ */
{
	if (type != BP_VAR_R && type != BP_VAR_IS && gear_is_call(ast)) {
		if (node->op_type == IS_VAR) {
			gear_op *opline = gear_emit_op(NULL, GEAR_SEPARATE, node, NULL);
			opline->result_type = IS_VAR;
			opline->result.var = opline->op1.var;
		} else {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use result of built-in function in write context");
		}
	}
}
/* }}} */

void gear_delayed_compile_var(znode *result, gear_ast *ast, uint32_t type);
void gear_compile_assign(znode *result, gear_ast *ast);

static inline void gear_emit_assign_znode(gear_ast *var_ast, znode *value_node) /* {{{ */
{
	znode dummy_node;
	gear_ast *assign_ast = gear_ast_create(GEAR_AST_ASSIGN, var_ast,
		gear_ast_create_znode(value_node));
	gear_compile_assign(&dummy_node, assign_ast);
	gear_do_free(&dummy_node);
}
/* }}} */

static gear_op *gear_delayed_compile_dim(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];
	gear_ast *dim_ast = ast->child[1];
	gear_op *opline;

	znode var_node, dim_node;

	gear_delayed_compile_var(&var_node, var_ast, type);
	gear_separate_if_call_and_write(&var_node, var_ast, type);

	if (dim_ast == NULL) {
		if (type == BP_VAR_R || type == BP_VAR_IS) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use [] for reading");
		}
		if (type == BP_VAR_UNSET) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use [] for unsetting");
		}
		dim_node.op_type = IS_UNUSED;
	} else {
		gear_compile_expr(&dim_node, dim_ast);
	}

	opline = gear_delayed_emit_op(result, GEAR_FETCH_DIM_R, &var_node, &dim_node);
	gear_adjust_for_fetch_type(opline, result, type);

	if (dim_node.op_type == IS_CONST) {
		gear_handle_numeric_dim(opline, &dim_node);
	}
	return opline;
}
/* }}} */

static gear_op *gear_compile_dim(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	uint32_t offset = gear_delayed_compile_begin();
	gear_delayed_compile_dim(result, ast, type);
	return gear_delayed_compile_end(offset);
}
/* }}} */

static gear_op *gear_delayed_compile_prop(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	gear_ast *obj_ast = ast->child[0];
	gear_ast *prop_ast = ast->child[1];

	znode obj_node, prop_node;
	gear_op *opline;

	if (is_this_fetch(obj_ast)) {
		obj_node.op_type = IS_UNUSED;
	} else {
		gear_delayed_compile_var(&obj_node, obj_ast, type);
		gear_separate_if_call_and_write(&obj_node, obj_ast, type);
	}
	gear_compile_expr(&prop_node, prop_ast);

	opline = gear_delayed_emit_op(result, GEAR_FETCH_OBJ_R, &obj_node, &prop_node);
	if (opline->op2_type == IS_CONST) {
		convert_to_string(CT_CONSTANT(opline->op2));
		opline->extended_value = gear_alloc_polymorphic_cache_slot();
	}

	gear_adjust_for_fetch_type(opline, result, type);
	return opline;
}
/* }}} */

static gear_op *gear_compile_prop(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	uint32_t offset = gear_delayed_compile_begin();
	gear_delayed_compile_prop(result, ast, type);
	return gear_delayed_compile_end(offset);
}
/* }}} */

gear_op *gear_compile_static_prop(znode *result, gear_ast *ast, uint32_t type, int delayed) /* {{{ */
{
	gear_ast *class_ast = ast->child[0];
	gear_ast *prop_ast = ast->child[1];

	znode class_node, prop_node;
	gear_op *opline;

	gear_compile_class_ref_ex(&class_node, class_ast, GEAR_FETCH_CLASS_EXCEPTION);

	gear_compile_expr(&prop_node, prop_ast);

	if (delayed) {
		opline = gear_delayed_emit_op(result, GEAR_FETCH_STATIC_PROP_R, &prop_node, NULL);
	} else {
		opline = gear_emit_op(result, GEAR_FETCH_STATIC_PROP_R, &prop_node, NULL);
	}
	if (opline->op1_type == IS_CONST) {
		convert_to_string(CT_CONSTANT(opline->op1));
		opline->extended_value = gear_alloc_polymorphic_cache_slot();
	}
	if (class_node.op_type == IS_CONST) {
		opline->op2_type = IS_CONST;
		opline->op2.constant = gear_add_class_name_literal(
			CG(active_op_array), Z_STR(class_node.u.constant));
		if (opline->op1_type != IS_CONST) {
			opline->extended_value = gear_alloc_cache_slot();
		}
	} else {
		SET_NODE(opline->op2, &class_node);
	}

	gear_adjust_for_fetch_type(opline, result, type);
	return opline;
}
/* }}} */

static void gear_verify_list_assign_target(gear_ast *var_ast, gear_bool old_style) /* {{{ */ {
	if (var_ast->kind == GEAR_AST_ARRAY) {
		if (var_ast->attr == GEAR_ARRAY_SYNTAX_LONG) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot assign to array(), use [] instead");
		}
		if (old_style != var_ast->attr) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot mix [] and list()");
		}
	} else if (!gear_can_write_to_variable(var_ast)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Assignments can only happen to writable values");
	}
}
/* }}} */

static inline void gear_emit_assign_ref_znode(gear_ast *var_ast, znode *value_node);

/* Propagate refs used on leaf elements to the surrounding list() structures. */
static gear_bool gear_propagate_list_refs(gear_ast *ast) { /* {{{ */
	gear_ast_list *list = gear_ast_get_list(ast);
	gear_bool has_refs = 0;
	uint32_t i;

	for (i = 0; i < list->children; ++i) {
		gear_ast *elem_ast = list->child[i];

		if (elem_ast) {
			gear_ast *var_ast = elem_ast->child[0];
			if (var_ast->kind == GEAR_AST_ARRAY) {
				elem_ast->attr = gear_propagate_list_refs(var_ast);
			}
			has_refs |= elem_ast->attr;
		}
	}

	return has_refs;
}
/* }}} */

static void gear_compile_list_assign(
		znode *result, gear_ast *ast, znode *expr_node, gear_bool old_style) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t i;
	gear_bool has_elems = 0;
	gear_bool is_keyed =
		list->children > 0 && list->child[0] != NULL && list->child[0]->child[1] != NULL;

	if (list->children && expr_node->op_type == IS_CONST && Z_TYPE(expr_node->u.constant) == IS_STRING) {
		zval_make_interned_string(&expr_node->u.constant);
	}

	for (i = 0; i < list->children; ++i) {
		gear_ast *elem_ast = list->child[i];
		gear_ast *var_ast, *key_ast;
		znode fetch_result, dim_node;
		gear_op *opline;

		if (elem_ast == NULL) {
			if (is_keyed) {
				gear_error(E_COMPILE_ERROR,
					"Cannot use empty array entries in keyed array assignment");
			} else {
				continue;
			}
		}

		var_ast = elem_ast->child[0];
		key_ast = elem_ast->child[1];
		has_elems = 1;

		if (is_keyed) {
			if (key_ast == NULL) {
				gear_error(E_COMPILE_ERROR,
					"Cannot mix keyed and unkeyed array entries in assignments");
			}

			gear_compile_expr(&dim_node, key_ast);
		} else {
			if (key_ast != NULL) {
				gear_error(E_COMPILE_ERROR,
					"Cannot mix keyed and unkeyed array entries in assignments");
			}

			dim_node.op_type = IS_CONST;
			ZVAL_LONG(&dim_node.u.constant, i);
		}

		if (expr_node->op_type == IS_CONST) {
			Z_TRY_ADDREF(expr_node->u.constant);
		}

		gear_verify_list_assign_target(var_ast, old_style);

		opline = gear_emit_op(&fetch_result,
			elem_ast->attr ? (expr_node->op_type == IS_CV ? GEAR_FETCH_DIM_W : GEAR_FETCH_LIST_W) : GEAR_FETCH_LIST_R, expr_node, &dim_node);

		if (dim_node.op_type == IS_CONST) {
			gear_handle_numeric_dim(opline, &dim_node);
		}

		if (var_ast->kind == GEAR_AST_ARRAY) {
			if (elem_ast->attr) {
				gear_emit_op(&fetch_result, GEAR_MAKE_REF, &fetch_result, NULL);
			}
			gear_compile_list_assign(NULL, var_ast, &fetch_result, var_ast->attr);
		} else if (elem_ast->attr) {
			gear_emit_assign_ref_znode(var_ast, &fetch_result);
		} else {
			gear_emit_assign_znode(var_ast, &fetch_result);
		}
	}

	if (has_elems == 0) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use empty list");
	}

	if (result) {
		*result = *expr_node;
	} else {
		gear_do_free(expr_node);
	}
}
/* }}} */

static void gear_ensure_writable_variable(const gear_ast *ast) /* {{{ */
{
	if (ast->kind == GEAR_AST_CALL) {
		gear_error_noreturn(E_COMPILE_ERROR, "Can't use function return value in write context");
	}
	if (ast->kind == GEAR_AST_METHOD_CALL || ast->kind == GEAR_AST_STATIC_CALL) {
		gear_error_noreturn(E_COMPILE_ERROR, "Can't use method return value in write context");
	}
}
/* }}} */

/* Detects $a... = $a pattern */
gear_bool gear_is_assign_to_self(gear_ast *var_ast, gear_ast *expr_ast) /* {{{ */
{
	if (expr_ast->kind != GEAR_AST_VAR || expr_ast->child[0]->kind != GEAR_AST_ZVAL) {
		return 0;
	}

	while (gear_is_variable(var_ast) && var_ast->kind != GEAR_AST_VAR) {
		var_ast = var_ast->child[0];
	}

	if (var_ast->kind != GEAR_AST_VAR || var_ast->child[0]->kind != GEAR_AST_ZVAL) {
		return 0;
	}

	{
		gear_string *name1 = zval_get_string(gear_ast_get_zval(var_ast->child[0]));
		gear_string *name2 = zval_get_string(gear_ast_get_zval(expr_ast->child[0]));
		gear_bool result = gear_string_equals(name1, name2);
		gear_string_release_ex(name1, 0);
		gear_string_release_ex(name2, 0);
		return result;
	}
}
/* }}} */

/* Detects if list($a, $b, $c) contains variable with given name */
gear_bool gear_list_has_assign_to(gear_ast *list_ast, gear_string *name) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(list_ast);
	uint32_t i;
	for (i = 0; i < list->children; i++) {
		gear_ast *elem_ast = list->child[i];
		gear_ast *var_ast;

		if (!elem_ast) {
			continue;
		}
		var_ast = elem_ast->child[0];

		/* Recursively check nested list()s */
		if (var_ast->kind == GEAR_AST_ARRAY && gear_list_has_assign_to(var_ast, name)) {
			return 1;
		}

		if (var_ast->kind == GEAR_AST_VAR && var_ast->child[0]->kind == GEAR_AST_ZVAL) {
			gear_string *var_name = zval_get_string(gear_ast_get_zval(var_ast->child[0]));
			gear_bool result = gear_string_equals(var_name, name);
			gear_string_release_ex(var_name, 0);
			if (result) {
				return 1;
			}
		}
	}

	return 0;
}
/* }}} */

/* Detects patterns like list($a, $b, $c) = $a */
gear_bool gear_list_has_assign_to_self(gear_ast *list_ast, gear_ast *expr_ast) /* {{{ */
{
	/* Only check simple variables on the RHS, as only CVs cause issues with this. */
	if (expr_ast->kind == GEAR_AST_VAR && expr_ast->child[0]->kind == GEAR_AST_ZVAL) {
		gear_string *name = zval_get_string(gear_ast_get_zval(expr_ast->child[0]));
		gear_bool result = gear_list_has_assign_to(list_ast, name);
		gear_string_release_ex(name, 0);
		return result;
	}
	return 0;
}
/* }}} */

void gear_compile_assign(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];
	gear_ast *expr_ast = ast->child[1];

	znode var_node, expr_node;
	gear_op *opline;
	uint32_t offset;

	if (is_this_fetch(var_ast)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot re-assign $this");
	}

	gear_ensure_writable_variable(var_ast);

	switch (var_ast->kind) {
		case GEAR_AST_VAR:
		case GEAR_AST_STATIC_PROP:
			offset = gear_delayed_compile_begin();
			gear_delayed_compile_var(&var_node, var_ast, BP_VAR_W);
			gear_compile_expr(&expr_node, expr_ast);
			gear_delayed_compile_end(offset);
			gear_emit_op(result, GEAR_ASSIGN, &var_node, &expr_node);
			return;
		case GEAR_AST_DIM:
			offset = gear_delayed_compile_begin();
			gear_delayed_compile_dim(result, var_ast, BP_VAR_W);

			if (gear_is_assign_to_self(var_ast, expr_ast)
			 && !is_this_fetch(expr_ast)) {
				/* $a[0] = $a should evaluate the right $a first */
				znode cv_node;

				if (gear_try_compile_cv(&cv_node, expr_ast) == FAILURE) {
					gear_compile_simple_var_no_cv(&expr_node, expr_ast, BP_VAR_R, 0);
				} else {
					gear_emit_op(&expr_node, GEAR_QM_ASSIGN, &cv_node, NULL);
				}
			} else {
				gear_compile_expr(&expr_node, expr_ast);
			}

			opline = gear_delayed_compile_end(offset);
			opline->opcode = GEAR_ASSIGN_DIM;

			opline = gear_emit_op_data(&expr_node);
			return;
		case GEAR_AST_PROP:
			offset = gear_delayed_compile_begin();
			gear_delayed_compile_prop(result, var_ast, BP_VAR_W);
			gear_compile_expr(&expr_node, expr_ast);

			opline = gear_delayed_compile_end(offset);
			opline->opcode = GEAR_ASSIGN_OBJ;

			gear_emit_op_data(&expr_node);
			return;
		case GEAR_AST_ARRAY:
			if (gear_propagate_list_refs(var_ast)) {
				if (!gear_is_variable(expr_ast)) {
					gear_error_noreturn(E_COMPILE_ERROR,
						"Cannot assign reference to non referencable value");
				}

				gear_compile_var(&expr_node, expr_ast, BP_VAR_W);
				/* MAKE_REF is usually not necessary for CVs. However, if there are
				 * self-assignments, this forces the RHS to evaluate first. */
				if (expr_node.op_type != IS_CV
						|| gear_list_has_assign_to_self(var_ast, expr_ast)) {
					gear_emit_op(&expr_node, GEAR_MAKE_REF, &expr_node, NULL);
				}
			} else {
				if (gear_list_has_assign_to_self(var_ast, expr_ast)) {
					/* list($a, $b) = $a should evaluate the right $a first */
					znode cv_node;

					if (gear_try_compile_cv(&cv_node, expr_ast) == FAILURE) {
						gear_compile_simple_var_no_cv(&expr_node, expr_ast, BP_VAR_R, 0);
					} else {
						gear_emit_op(&expr_node, GEAR_QM_ASSIGN, &cv_node, NULL);
					}
				} else {
					gear_compile_expr(&expr_node, expr_ast);
				}
			}

			gear_compile_list_assign(result, var_ast, &expr_node, var_ast->attr);
			return;
		EMPTY_SWITCH_DEFAULT_CASE();
	}
}
/* }}} */

void gear_compile_assign_ref(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *target_ast = ast->child[0];
	gear_ast *source_ast = ast->child[1];

	znode target_node, source_node;
	gear_op *opline;
	uint32_t offset;

	if (is_this_fetch(target_ast)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot re-assign $this");
	}
	gear_ensure_writable_variable(target_ast);

	offset = gear_delayed_compile_begin();
	gear_delayed_compile_var(&target_node, target_ast, BP_VAR_W);
	gear_compile_var(&source_node, source_ast, BP_VAR_W);

	if ((target_ast->kind != GEAR_AST_VAR
	  || target_ast->child[0]->kind != GEAR_AST_ZVAL)
	 && source_node.op_type != IS_CV) {
		/* Both LHS and RHS expressions may modify the same data structure,
		 * and the modification during RHS evaluation may dangle the pointer
		 * to the result of the LHS evaluation.
		 * Use MAKE_REF instruction to replace direct pointer with REFERENCE.
		 * See: Bug #71539
		 */
		gear_emit_op(&source_node, GEAR_MAKE_REF, &source_node, NULL);
	}

	gear_delayed_compile_end(offset);

	if (source_node.op_type != IS_VAR && gear_is_call(source_ast)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use result of built-in function in write context");
	}

	opline = gear_emit_op(result, GEAR_ASSIGN_REF, &target_node, &source_node);

	if (gear_is_call(source_ast)) {
		opline->extended_value = GEAR_RETURNS_FUNCTION;
	}
}
/* }}} */

static inline void gear_emit_assign_ref_znode(gear_ast *var_ast, znode *value_node) /* {{{ */
{
	gear_ast *assign_ast = gear_ast_create(GEAR_AST_ASSIGN_REF, var_ast,
		gear_ast_create_znode(value_node));
	gear_compile_assign_ref(NULL, assign_ast);
}
/* }}} */

void gear_compile_compound_assign(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];
	gear_ast *expr_ast = ast->child[1];
	uint32_t opcode = ast->attr;

	znode var_node, expr_node;
	gear_op *opline;
	uint32_t offset, cache_slot;

	gear_ensure_writable_variable(var_ast);

	switch (var_ast->kind) {
		case GEAR_AST_VAR:
		case GEAR_AST_STATIC_PROP:
			offset = gear_delayed_compile_begin();
			gear_delayed_compile_var(&var_node, var_ast, BP_VAR_RW);
			gear_compile_expr(&expr_node, expr_ast);
			gear_delayed_compile_end(offset);
			gear_emit_op(result, opcode, &var_node, &expr_node);
			return;
		case GEAR_AST_DIM:
			offset = gear_delayed_compile_begin();
			gear_delayed_compile_dim(result, var_ast, BP_VAR_RW);
			gear_compile_expr(&expr_node, expr_ast);

			opline = gear_delayed_compile_end(offset);
			opline->opcode = opcode;
			opline->extended_value = GEAR_ASSIGN_DIM;

			opline = gear_emit_op_data(&expr_node);
			return;
		case GEAR_AST_PROP:
			offset = gear_delayed_compile_begin();
			gear_delayed_compile_prop(result, var_ast, BP_VAR_RW);
			gear_compile_expr(&expr_node, expr_ast);

			opline = gear_delayed_compile_end(offset);
			cache_slot = opline->extended_value;
			opline->opcode = opcode;
			opline->extended_value = GEAR_ASSIGN_OBJ;

			opline = gear_emit_op_data(&expr_node);
			opline->extended_value = cache_slot;
			return;
		EMPTY_SWITCH_DEFAULT_CASE()
	}
}
/* }}} */

uint32_t gear_compile_args(gear_ast *ast, gear_function *fbc) /* {{{ */
{
	gear_ast_list *args = gear_ast_get_list(ast);
	uint32_t i;
	gear_bool uses_arg_unpack = 0;
	uint32_t arg_count = 0; /* number of arguments not including unpacks */

	for (i = 0; i < args->children; ++i) {
		gear_ast *arg = args->child[i];
		uint32_t arg_num = i + 1;

		znode arg_node;
		gear_op *opline;
		gear_uchar opcode;

		if (arg->kind == GEAR_AST_UNPACK) {
			uses_arg_unpack = 1;
			fbc = NULL;

			gear_compile_expr(&arg_node, arg->child[0]);
			opline = gear_emit_op(NULL, GEAR_SEND_UNPACK, &arg_node, NULL);
			opline->op2.num = arg_count;
			opline->result.var = (uint32_t)(gear_intptr_t)GEAR_CALL_ARG(NULL, arg_count);
			continue;
		}

		if (uses_arg_unpack) {
			gear_error_noreturn(E_COMPILE_ERROR,
				"Cannot use positional argument after argument unpacking");
		}

		arg_count++;
		if (gear_is_variable(arg)) {
			if (gear_is_call(arg)) {
				gear_compile_var(&arg_node, arg, BP_VAR_R);
				if (arg_node.op_type & (IS_CONST|IS_TMP_VAR)) {
					/* Function call was converted into builtin instruction */
					opcode = GEAR_SEND_VAL;
				} else {
					if (fbc) {
						if (ARG_MUST_BE_SENT_BY_REF(fbc, arg_num)) {
							opcode = GEAR_SEND_VAR_NO_REF;
						} else if (ARG_MAY_BE_SENT_BY_REF(fbc, arg_num)) {
							opcode = GEAR_SEND_VAL;
						} else {
							opcode = GEAR_SEND_VAR;
						}
					} else {
						opcode = GEAR_SEND_VAR_NO_REF_EX;
					}
				}
			} else if (fbc) {
				if (ARG_SHOULD_BE_SENT_BY_REF(fbc, arg_num)) {
					gear_compile_var(&arg_node, arg, BP_VAR_W);
					opcode = GEAR_SEND_REF;
				} else {
					gear_compile_var(&arg_node, arg, BP_VAR_R);
					opcode = (arg_node.op_type == IS_TMP_VAR) ? GEAR_SEND_VAL : GEAR_SEND_VAR;
				}
			} else {
				do {
					if (arg->kind == GEAR_AST_VAR) {
						CG(gear_lineno) = gear_ast_get_lineno(ast);
						if (is_this_fetch(arg)) {
							gear_emit_op(&arg_node, GEAR_FETCH_THIS, NULL, NULL);
							opcode = GEAR_SEND_VAR_EX;
							break;
						} else if (gear_try_compile_cv(&arg_node, arg) == SUCCESS) {
							opcode = GEAR_SEND_VAR_EX;
							break;
						}
					}
					opline = gear_emit_op(NULL, GEAR_CHECK_FUNC_ARG, NULL, NULL);
					opline->op2.num = arg_num;
					gear_compile_var(&arg_node, arg, BP_VAR_FUNC_ARG);
					opcode = GEAR_SEND_FUNC_ARG;
				} while (0);
			}
		} else {
			gear_compile_expr(&arg_node, arg);
			if (arg_node.op_type == IS_VAR) {
				/* pass ++$a or something similar */
				if (fbc) {
					if (ARG_MUST_BE_SENT_BY_REF(fbc, arg_num)) {
						opcode = GEAR_SEND_VAR_NO_REF;
					} else if (ARG_MAY_BE_SENT_BY_REF(fbc, arg_num)) {
						opcode = GEAR_SEND_VAL;
					} else {
						opcode = GEAR_SEND_VAR;
					}
				} else {
					opcode = GEAR_SEND_VAR_NO_REF_EX;
				}
			} else if (arg_node.op_type == IS_CV) {
				if (fbc) {
					if (ARG_SHOULD_BE_SENT_BY_REF(fbc, arg_num)) {
						opcode = GEAR_SEND_REF;
					} else {
						opcode = GEAR_SEND_VAR;
					}
				} else {
					opcode = GEAR_SEND_VAR_EX;
				}
			} else {
				if (fbc) {
					opcode = GEAR_SEND_VAL;
					if (ARG_MUST_BE_SENT_BY_REF(fbc, arg_num)) {
						gear_error_noreturn(E_COMPILE_ERROR, "Only variables can be passed by reference");
					}
				} else {
					opcode = GEAR_SEND_VAL_EX;
				}
			}
		}

		opline = gear_emit_op(NULL, opcode, &arg_node, NULL);
		opline->op2.opline_num = arg_num;
		opline->result.var = (uint32_t)(gear_intptr_t)GEAR_CALL_ARG(NULL, arg_num);
	}

	return arg_count;
}
/* }}} */

GEAR_API gear_uchar gear_get_call_op(const gear_op *init_op, gear_function *fbc) /* {{{ */
{
	if (fbc) {
		if (fbc->type == GEAR_INTERNAL_FUNCTION) {
			if (init_op->opcode == GEAR_INIT_FCALL && !gear_execute_internal) {
				if (!(fbc->common.fn_flags & (GEAR_ACC_ABSTRACT|GEAR_ACC_DEPRECATED|GEAR_ACC_HAS_TYPE_HINTS|GEAR_ACC_RETURN_REFERENCE))) {
					return GEAR_DO_ICALL;
				} else {
					return GEAR_DO_FCALL_BY_NAME;
				}
			}
		} else {
			if (gear_execute_ex == execute_ex && !(fbc->common.fn_flags & GEAR_ACC_ABSTRACT)) {
				return GEAR_DO_UCALL;
			}
		}
	} else if (gear_execute_ex == execute_ex &&
	           !gear_execute_internal &&
	           (init_op->opcode == GEAR_INIT_FCALL_BY_NAME ||
	            init_op->opcode == GEAR_INIT_NS_FCALL_BY_NAME)) {
		return GEAR_DO_FCALL_BY_NAME;
	}
	return GEAR_DO_FCALL;
}
/* }}} */

void gear_compile_call_common(znode *result, gear_ast *args_ast, gear_function *fbc) /* {{{ */
{
	gear_op *opline;
	uint32_t opnum_init = get_next_op_number(CG(active_op_array)) - 1;
	uint32_t arg_count;
	uint32_t call_flags;

	gear_do_extended_fcall_begin();

	arg_count = gear_compile_args(args_ast, fbc);

	opline = &CG(active_op_array)->opcodes[opnum_init];
	opline->extended_value = arg_count;

	if (opline->opcode == GEAR_INIT_FCALL) {
		opline->op1.num = gear_vm_calc_used_stack(arg_count, fbc);
	}

	call_flags = (opline->opcode == GEAR_NEW ? GEAR_CALL_CTOR : 0);
	opline = gear_emit_op(result, gear_get_call_op(opline, fbc), NULL, NULL);
	opline->op1.num = call_flags;

	gear_do_extended_fcall_end();
}
/* }}} */

gear_bool gear_compile_function_name(znode *name_node, gear_ast *name_ast) /* {{{ */
{
	gear_string *orig_name = gear_ast_get_str(name_ast);
	gear_bool is_fully_qualified;

	name_node->op_type = IS_CONST;
	ZVAL_STR(&name_node->u.constant, gear_resolve_function_name(
		orig_name, name_ast->attr, &is_fully_qualified));

	return !is_fully_qualified && FC(current_namespace);
}
/* }}} */

void gear_compile_ns_call(znode *result, znode *name_node, gear_ast *args_ast) /* {{{ */
{
	gear_op *opline = get_next_op(CG(active_op_array));
	opline->opcode = GEAR_INIT_NS_FCALL_BY_NAME;
	opline->op2_type = IS_CONST;
	opline->op2.constant = gear_add_ns_func_name_literal(
		CG(active_op_array), Z_STR(name_node->u.constant));
	opline->result.num = gear_alloc_cache_slot();

	gear_compile_call_common(result, args_ast, NULL);
}
/* }}} */

void gear_compile_dynamic_call(znode *result, znode *name_node, gear_ast *args_ast) /* {{{ */
{
	if (name_node->op_type == IS_CONST && Z_TYPE(name_node->u.constant) == IS_STRING) {
		const char *colon;
		gear_string *str = Z_STR(name_node->u.constant);
		if ((colon = gear_memrchr(ZSTR_VAL(str), ':', ZSTR_LEN(str))) != NULL && colon > ZSTR_VAL(str) && *(colon - 1) == ':') {
			gear_string *class = gear_string_init(ZSTR_VAL(str), colon - ZSTR_VAL(str) - 1, 0);
			gear_string *method = gear_string_init(colon + 1, ZSTR_LEN(str) - (colon - ZSTR_VAL(str)) - 1, 0);
			gear_op *opline = get_next_op(CG(active_op_array));

			opline->opcode = GEAR_INIT_STATIC_METHOD_CALL;
			opline->op1_type = IS_CONST;
			opline->op1.constant = gear_add_class_name_literal(CG(active_op_array), class);
			opline->op2_type = IS_CONST;
			opline->op2.constant = gear_add_func_name_literal(CG(active_op_array), method);
			/* 2 slots, for class and method */
			opline->result.num = gear_alloc_polymorphic_cache_slot();
			zval_ptr_dtor(&name_node->u.constant);
		} else {
			gear_op *opline = get_next_op(CG(active_op_array));

			opline->opcode = GEAR_INIT_FCALL_BY_NAME;
			opline->op2_type = IS_CONST;
			opline->op2.constant = gear_add_func_name_literal(CG(active_op_array), str);
			opline->result.num = gear_alloc_cache_slot();
		}
	} else {
		gear_emit_op(NULL, GEAR_INIT_DYNAMIC_CALL, NULL, name_node);
	}

	gear_compile_call_common(result, args_ast, NULL);
}
/* }}} */

static gear_bool gear_args_contain_unpack(gear_ast_list *args) /* {{{ */
{
	uint32_t i;
	for (i = 0; i < args->children; ++i) {
		if (args->child[i]->kind == GEAR_AST_UNPACK) {
			return 1;
		}
	}
	return 0;
}
/* }}} */

int gear_compile_func_strlen(znode *result, gear_ast_list *args) /* {{{ */
{
	znode arg_node;

	if ((CG(compiler_options) & GEAR_COMPILE_NO_BUILTIN_STRLEN)
		|| args->children != 1 || args->child[0]->kind == GEAR_AST_UNPACK
	) {
		return FAILURE;
	}

	gear_compile_expr(&arg_node, args->child[0]);
	if (arg_node.op_type == IS_CONST && Z_TYPE(arg_node.u.constant) == IS_STRING) {
		result->op_type = IS_CONST;
		ZVAL_LONG(&result->u.constant, Z_STRLEN(arg_node.u.constant));
		zval_ptr_dtor_str(&arg_node.u.constant);
	} else {
		gear_emit_op_tmp(result, GEAR_STRLEN, &arg_node, NULL);
	}
	return SUCCESS;
}
/* }}} */

int gear_compile_func_typecheck(znode *result, gear_ast_list *args, uint32_t type) /* {{{ */
{
	znode arg_node;
	gear_op *opline;

	if (args->children != 1 || args->child[0]->kind == GEAR_AST_UNPACK) {
		return FAILURE;
	}

	gear_compile_expr(&arg_node, args->child[0]);
	opline = gear_emit_op_tmp(result, GEAR_TYPE_CHECK, &arg_node, NULL);
	if (type != _IS_BOOL) {
		opline->extended_value = (1 << type);
	} else {
		opline->extended_value = (1 << IS_FALSE) | (1 << IS_TRUE);
	}
	return SUCCESS;
}
/* }}} */

int gear_compile_func_cast(znode *result, gear_ast_list *args, uint32_t type) /* {{{ */
{
	znode arg_node;
	gear_op *opline;

	if (args->children != 1 || args->child[0]->kind == GEAR_AST_UNPACK) {
		return FAILURE;
	}

	gear_compile_expr(&arg_node, args->child[0]);
	opline = gear_emit_op_tmp(result, GEAR_CAST, &arg_node, NULL);
	opline->extended_value = type;
	return SUCCESS;
}
/* }}} */

int gear_compile_func_defined(znode *result, gear_ast_list *args) /* {{{ */
{
	gear_string *name;
	gear_op *opline;

	if (args->children != 1 || args->child[0]->kind != GEAR_AST_ZVAL) {
		return FAILURE;
	}

	name = zval_get_string(gear_ast_get_zval(args->child[0]));
	if (gear_memrchr(ZSTR_VAL(name), '\\', ZSTR_LEN(name)) || gear_memrchr(ZSTR_VAL(name), ':', ZSTR_LEN(name))) {
		gear_string_release_ex(name, 0);
		return FAILURE;
	}

	if (gear_try_ct_eval_const(&result->u.constant, name, 0)) {
		gear_string_release_ex(name, 0);
		zval_ptr_dtor(&result->u.constant);
		ZVAL_TRUE(&result->u.constant);
		result->op_type = IS_CONST;
		return SUCCESS;
	}

	opline = gear_emit_op_tmp(result, GEAR_DEFINED, NULL, NULL);
	opline->op1_type = IS_CONST;
	LITERAL_STR(opline->op1, name);
	opline->extended_value = gear_alloc_cache_slot();

	/* Lowercase constant name in a separate literal */
	{
		zval c;
		gear_string *lcname = gear_string_tolower(name);
		ZVAL_NEW_STR(&c, lcname);
		gear_add_literal(CG(active_op_array), &c);
	}
	return SUCCESS;
}
/* }}} */

int gear_compile_func_chr(znode *result, gear_ast_list *args) /* {{{ */
{

	if (args->children == 1 &&
	    args->child[0]->kind == GEAR_AST_ZVAL &&
	    Z_TYPE_P(gear_ast_get_zval(args->child[0])) == IS_LONG) {

		gear_long c = Z_LVAL_P(gear_ast_get_zval(args->child[0])) & 0xff;

		result->op_type = IS_CONST;
		ZVAL_INTERNED_STR(&result->u.constant, ZSTR_CHAR(c));
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */

int gear_compile_func_ord(znode *result, gear_ast_list *args) /* {{{ */
{
	if (args->children == 1 &&
	    args->child[0]->kind == GEAR_AST_ZVAL &&
	    Z_TYPE_P(gear_ast_get_zval(args->child[0])) == IS_STRING) {

		result->op_type = IS_CONST;
		ZVAL_LONG(&result->u.constant, (unsigned char)Z_STRVAL_P(gear_ast_get_zval(args->child[0]))[0]);
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */


static int gear_try_compile_ct_bound_init_user_func(gear_ast *name_ast, uint32_t num_args) /* {{{ */
{
	gear_string *name, *lcname;
	gear_function *fbc;
	gear_op *opline;

	if (name_ast->kind != GEAR_AST_ZVAL || Z_TYPE_P(gear_ast_get_zval(name_ast)) != IS_STRING) {
		return FAILURE;
	}

	name = gear_ast_get_str(name_ast);
	lcname = gear_string_tolower(name);

	fbc = gear_hash_find_ptr(CG(function_table), lcname);
	if (!fbc
	 || (fbc->type == GEAR_INTERNAL_FUNCTION && (CG(compiler_options) & GEAR_COMPILE_IGNORE_INTERNAL_FUNCTIONS))
	 || (fbc->type == GEAR_USER_FUNCTION && (CG(compiler_options) & GEAR_COMPILE_IGNORE_USER_FUNCTIONS))
	) {
		gear_string_release_ex(lcname, 0);
		return FAILURE;
	}

	opline = gear_emit_op(NULL, GEAR_INIT_FCALL, NULL, NULL);
	opline->extended_value = num_args;
	opline->op1.num = gear_vm_calc_used_stack(num_args, fbc);
	opline->op2_type = IS_CONST;
	LITERAL_STR(opline->op2, lcname);
	opline->result.num = gear_alloc_cache_slot();

	return SUCCESS;
}
/* }}} */

static void gear_compile_init_user_func(gear_ast *name_ast, uint32_t num_args, gear_string *orig_func_name) /* {{{ */
{
	gear_op *opline;
	znode name_node;

	if (gear_try_compile_ct_bound_init_user_func(name_ast, num_args) == SUCCESS) {
		return;
	}

	gear_compile_expr(&name_node, name_ast);

	opline = gear_emit_op(NULL, GEAR_INIT_USER_CALL, NULL, &name_node);
	opline->op1_type = IS_CONST;
	LITERAL_STR(opline->op1, gear_string_copy(orig_func_name));
	opline->extended_value = num_args;
}
/* }}} */

/* cufa = call_user_func_array */
int gear_compile_func_cufa(znode *result, gear_ast_list *args, gear_string *lcname) /* {{{ */
{
	znode arg_node;

	if (args->children != 2 || gear_args_contain_unpack(args)) {
		return FAILURE;
	}

	gear_compile_init_user_func(args->child[0], 0, lcname);
	if (args->child[1]->kind == GEAR_AST_CALL
	 && args->child[1]->child[0]->kind == GEAR_AST_ZVAL
	 && Z_TYPE_P(gear_ast_get_zval(args->child[1]->child[0])) == IS_STRING
	 && args->child[1]->child[1]->kind == GEAR_AST_ARG_LIST) {
		gear_string *orig_name = gear_ast_get_str(args->child[1]->child[0]);
		gear_ast_list *list = gear_ast_get_list(args->child[1]->child[1]);
		gear_bool is_fully_qualified;
		gear_string *name = gear_resolve_function_name(orig_name, args->child[1]->child[0]->attr, &is_fully_qualified);

		if (gear_string_equals_literal_ci(name, "array_slice")
		 && list->children == 3
		 && list->child[1]->kind == GEAR_AST_ZVAL) {
			zval *zv = gear_ast_get_zval(list->child[1]);

			if (Z_TYPE_P(zv) == IS_LONG
			 && Z_LVAL_P(zv) >= 0
			 && Z_LVAL_P(zv) <= 0x7fffffff) {
				gear_op *opline;
				znode len_node;

				gear_compile_expr(&arg_node, list->child[0]);
				gear_compile_expr(&len_node, list->child[2]);
				opline = gear_emit_op(NULL, GEAR_SEND_ARRAY, &arg_node, &len_node);
				opline->extended_value = Z_LVAL_P(zv);
				gear_emit_op(result, GEAR_DO_FCALL, NULL, NULL);
				gear_string_release_ex(name, 0);
				return SUCCESS;
			}
		}
		gear_string_release_ex(name, 0);
	}
	gear_compile_expr(&arg_node, args->child[1]);
	gear_emit_op(NULL, GEAR_SEND_ARRAY, &arg_node, NULL);
	gear_emit_op(result, GEAR_DO_FCALL, NULL, NULL);

	return SUCCESS;
}
/* }}} */

/* cuf = call_user_func */
int gear_compile_func_cuf(znode *result, gear_ast_list *args, gear_string *lcname) /* {{{ */
{
	uint32_t i;

	if (args->children < 1 || gear_args_contain_unpack(args)) {
		return FAILURE;
	}

	gear_compile_init_user_func(args->child[0], args->children - 1, lcname);
	for (i = 1; i < args->children; ++i) {
		gear_ast *arg_ast = args->child[i];
		znode arg_node;
		gear_op *opline;

		gear_compile_expr(&arg_node, arg_ast);

		opline = gear_emit_op(NULL, GEAR_SEND_USER, &arg_node, NULL);
		opline->op2.num = i;
		opline->result.var = (uint32_t)(gear_intptr_t)GEAR_CALL_ARG(NULL, i);
	}
	gear_emit_op(result, GEAR_DO_FCALL, NULL, NULL);

	return SUCCESS;
}
/* }}} */

static void gear_compile_assert(znode *result, gear_ast_list *args, gear_string *name, gear_function *fbc) /* {{{ */
{
	if (EG(assertions) >= 0) {
		znode name_node;
		gear_op *opline;
		uint32_t check_op_number = get_next_op_number(CG(active_op_array));

		gear_emit_op(NULL, GEAR_ASSERT_CHECK, NULL, NULL);

		if (fbc) {
			name_node.op_type = IS_CONST;
			ZVAL_STR_COPY(&name_node.u.constant, name);

			opline = gear_emit_op(NULL, GEAR_INIT_FCALL, NULL, &name_node);
		} else {
			opline = gear_emit_op(NULL, GEAR_INIT_NS_FCALL_BY_NAME, NULL, NULL);
			opline->op2_type = IS_CONST;
			opline->op2.constant = gear_add_ns_func_name_literal(
				CG(active_op_array), name);
		}
		opline->result.num = gear_alloc_cache_slot();

		if (args->children == 1 &&
		    (args->child[0]->kind != GEAR_AST_ZVAL ||
		     Z_TYPE_P(gear_ast_get_zval(args->child[0])) != IS_STRING)) {
			/* add "assert(condition) as assertion message */
			gear_ast_list_add((gear_ast*)args,
				gear_ast_create_zval_from_str(
					gear_ast_export("assert(", args->child[0], ")")));
		}

		gear_compile_call_common(result, (gear_ast*)args, fbc);

		opline = &CG(active_op_array)->opcodes[check_op_number];
		opline->op2.opline_num = get_next_op_number(CG(active_op_array));
		SET_NODE(opline->result, result);
	} else {
		if (!fbc) {
			gear_string_release_ex(name, 0);
		}
		result->op_type = IS_CONST;
		ZVAL_TRUE(&result->u.constant);
	}
}
/* }}} */

static int gear_compile_func_in_array(znode *result, gear_ast_list *args) /* {{{ */
{
	gear_bool strict = 0;
	znode array, needly;
	gear_op *opline;

	if (args->children == 3) {
		if (args->child[2]->kind == GEAR_AST_ZVAL) {
			strict = gear_is_true(gear_ast_get_zval(args->child[2]));
		} else if (args->child[2]->kind == GEAR_AST_CONST) {
			zval value;
			gear_ast *name_ast = args->child[2]->child[0];
			gear_bool is_fully_qualified;
			gear_string *resolved_name = gear_resolve_const_name(
				gear_ast_get_str(name_ast), name_ast->attr, &is_fully_qualified);

			if (!gear_try_ct_eval_const(&value, resolved_name, is_fully_qualified)) {
				gear_string_release_ex(resolved_name, 0);
				return FAILURE;
			}

			gear_string_release_ex(resolved_name, 0);
			strict = gear_is_true(&value);
			zval_ptr_dtor(&value);
		} else {
			return FAILURE;
		}
	} else if (args->children != 2) {
		return FAILURE;
	}

	if (args->child[1]->kind != GEAR_AST_ARRAY
	 || !gear_try_ct_eval_array(&array.u.constant, args->child[1])) {
		return FAILURE;
	}

	if (gear_hash_num_elements(Z_ARRVAL(array.u.constant)) > 0) {
		gear_bool ok = 1;
		zval *val, tmp;
		HashTable *src = Z_ARRVAL(array.u.constant);
		HashTable *dst = gear_new_array(gear_hash_num_elements(src));

		ZVAL_TRUE(&tmp);

		if (strict) {
			GEAR_HASH_FOREACH_VAL(src, val) {
				if (Z_TYPE_P(val) == IS_STRING) {
					gear_hash_add(dst, Z_STR_P(val), &tmp);
				} else if (Z_TYPE_P(val) == IS_LONG) {
					gear_hash_index_add(dst, Z_LVAL_P(val), &tmp);
				} else {
					gear_array_destroy(dst);
					ok = 0;
					break;
				}
			} GEAR_HASH_FOREACH_END();
		} else {
			GEAR_HASH_FOREACH_VAL(src, val) {
				if (Z_TYPE_P(val) != IS_STRING
				 || is_numeric_string(Z_STRVAL_P(val), Z_STRLEN_P(val), NULL, NULL, 0)) {
					gear_array_destroy(dst);
					ok = 0;
					break;
				}
				gear_hash_add(dst, Z_STR_P(val), &tmp);
			} GEAR_HASH_FOREACH_END();
		}

		gear_array_destroy(src);
		if (!ok) {
			return FAILURE;
		}
		Z_ARRVAL(array.u.constant) = dst;
	}
	array.op_type = IS_CONST;

	gear_compile_expr(&needly, args->child[0]);

	opline = gear_emit_op_tmp(result, GEAR_IN_ARRAY, &needly, &array);
	opline->extended_value = strict;

	return SUCCESS;
}
/* }}} */

int gear_compile_func_count(znode *result, gear_ast_list *args) /* {{{ */
{
	znode arg_node;

	if (args->children != 1 || args->child[0]->kind == GEAR_AST_UNPACK) {
		return FAILURE;
	}

	gear_compile_expr(&arg_node, args->child[0]);
	gear_emit_op_tmp(result, GEAR_COUNT, &arg_node, NULL);
	return SUCCESS;
}
/* }}} */

int gear_compile_func_get_class(znode *result, gear_ast_list *args) /* {{{ */
{
	if (args->children == 0) {
		gear_emit_op_tmp(result, GEAR_GET_CLASS, NULL, NULL);
	} else {
		znode arg_node;

		if (args->children != 1 || args->child[0]->kind == GEAR_AST_UNPACK) {
			return FAILURE;
		}

		gear_compile_expr(&arg_node, args->child[0]);
		gear_emit_op_tmp(result, GEAR_GET_CLASS, &arg_node, NULL);
	}
	return SUCCESS;
}
/* }}} */

int gear_compile_func_get_called_class(znode *result, gear_ast_list *args) /* {{{ */
{
	if (args->children != 0) {
		return FAILURE;
	}

	gear_emit_op_tmp(result, GEAR_GET_CALLED_CLASS, NULL, NULL);
	return SUCCESS;
}
/* }}} */

int gear_compile_func_gettype(znode *result, gear_ast_list *args) /* {{{ */
{
	znode arg_node;

	if (args->children != 1 || args->child[0]->kind == GEAR_AST_UNPACK) {
		return FAILURE;
	}

	gear_compile_expr(&arg_node, args->child[0]);
	gear_emit_op_tmp(result, GEAR_GET_TYPE, &arg_node, NULL);
	return SUCCESS;
}
/* }}} */

int gear_compile_func_num_args(znode *result, gear_ast_list *args) /* {{{ */
{
	if (CG(active_op_array)->function_name && args->children == 0) {
		gear_emit_op_tmp(result, GEAR_FUNC_NUM_ARGS, NULL, NULL);
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */

int gear_compile_func_get_args(znode *result, gear_ast_list *args) /* {{{ */
{
	if (CG(active_op_array)->function_name && args->children == 0) {
		gear_emit_op_tmp(result, GEAR_FUNC_GET_ARGS, NULL, NULL);
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */

int gear_compile_func_array_slice(znode *result, gear_ast_list *args) /* {{{ */
{
	if (CG(active_op_array)->function_name
	 && args->children == 2
	 && args->child[0]->kind == GEAR_AST_CALL
	 && args->child[0]->child[0]->kind == GEAR_AST_ZVAL
	 && Z_TYPE_P(gear_ast_get_zval(args->child[0]->child[0])) == IS_STRING
	 && args->child[0]->child[1]->kind == GEAR_AST_ARG_LIST
	 && args->child[1]->kind == GEAR_AST_ZVAL) {

		gear_string *orig_name = gear_ast_get_str(args->child[0]->child[0]);
		gear_bool is_fully_qualified;
		gear_string *name = gear_resolve_function_name(orig_name, args->child[0]->child[0]->attr, &is_fully_qualified);
		gear_ast_list *list = gear_ast_get_list(args->child[0]->child[1]);
		zval *zv = gear_ast_get_zval(args->child[1]);
		znode first;

		if (gear_string_equals_literal_ci(name, "func_get_args")
		 && list->children == 0
		 && Z_TYPE_P(zv) == IS_LONG
		 && Z_LVAL_P(zv) >= 0) {
			first.op_type = IS_CONST;
			ZVAL_LONG(&first.u.constant, Z_LVAL_P(zv));
			gear_emit_op_tmp(result, GEAR_FUNC_GET_ARGS, &first, NULL);
			gear_string_release_ex(name, 0);
			return SUCCESS;
		}
		gear_string_release_ex(name, 0);
	}
	return FAILURE;
}
/* }}} */

int gear_try_compile_special_func(znode *result, gear_string *lcname, gear_ast_list *args, gear_function *fbc, uint32_t type) /* {{{ */
{
	if (fbc->internal_function.handler == GEAR_FN(display_disabled_function)) {
		return FAILURE;
	}

	if (CG(compiler_options) & GEAR_COMPILE_NO_BUILTINS) {
		return FAILURE;
	}

	if (gear_string_equals_literal(lcname, "strlen")) {
		return gear_compile_func_strlen(result, args);
	} else if (gear_string_equals_literal(lcname, "is_null")) {
		return gear_compile_func_typecheck(result, args, IS_NULL);
	} else if (gear_string_equals_literal(lcname, "is_bool")) {
		return gear_compile_func_typecheck(result, args, _IS_BOOL);
	} else if (gear_string_equals_literal(lcname, "is_long")
		|| gear_string_equals_literal(lcname, "is_int")
		|| gear_string_equals_literal(lcname, "is_integer")
	) {
		return gear_compile_func_typecheck(result, args, IS_LONG);
	} else if (gear_string_equals_literal(lcname, "is_float")
		|| gear_string_equals_literal(lcname, "is_double")
		|| gear_string_equals_literal(lcname, "is_real")
	) {
		return gear_compile_func_typecheck(result, args, IS_DOUBLE);
	} else if (gear_string_equals_literal(lcname, "is_string")) {
		return gear_compile_func_typecheck(result, args, IS_STRING);
	} else if (gear_string_equals_literal(lcname, "is_array")) {
		return gear_compile_func_typecheck(result, args, IS_ARRAY);
	} else if (gear_string_equals_literal(lcname, "is_object")) {
		return gear_compile_func_typecheck(result, args, IS_OBJECT);
	} else if (gear_string_equals_literal(lcname, "is_resource")) {
		return gear_compile_func_typecheck(result, args, IS_RESOURCE);
	} else if (gear_string_equals_literal(lcname, "boolval")) {
		return gear_compile_func_cast(result, args, _IS_BOOL);
	} else if (gear_string_equals_literal(lcname, "intval")) {
		return gear_compile_func_cast(result, args, IS_LONG);
	} else if (gear_string_equals_literal(lcname, "floatval")
		|| gear_string_equals_literal(lcname, "doubleval")
	) {
		return gear_compile_func_cast(result, args, IS_DOUBLE);
	} else if (gear_string_equals_literal(lcname, "strval")) {
		return gear_compile_func_cast(result, args, IS_STRING);
	} else if (gear_string_equals_literal(lcname, "defined")) {
		return gear_compile_func_defined(result, args);
	} else if (gear_string_equals_literal(lcname, "chr") && type == BP_VAR_R) {
		return gear_compile_func_chr(result, args);
	} else if (gear_string_equals_literal(lcname, "ord") && type == BP_VAR_R) {
		return gear_compile_func_ord(result, args);
	} else if (gear_string_equals_literal(lcname, "call_user_func_array")) {
		return gear_compile_func_cufa(result, args, lcname);
	} else if (gear_string_equals_literal(lcname, "call_user_func")) {
		return gear_compile_func_cuf(result, args, lcname);
	} else if (gear_string_equals_literal(lcname, "in_array")) {
		return gear_compile_func_in_array(result, args);
	} else if (gear_string_equals_literal(lcname, "count")) {
		return gear_compile_func_count(result, args);
	} else if (gear_string_equals_literal(lcname, "get_class")) {
		return gear_compile_func_get_class(result, args);
	} else if (gear_string_equals_literal(lcname, "get_called_class")) {
		return gear_compile_func_get_called_class(result, args);
	} else if (gear_string_equals_literal(lcname, "gettype")) {
		return gear_compile_func_gettype(result, args);
	} else if (gear_string_equals_literal(lcname, "func_num_args")) {
		return gear_compile_func_num_args(result, args);
	} else if (gear_string_equals_literal(lcname, "func_get_args")) {
		return gear_compile_func_get_args(result, args);
	} else if (gear_string_equals_literal(lcname, "array_slice")) {
		return gear_compile_func_array_slice(result, args);
	} else {
		return FAILURE;
	}
}
/* }}} */

void gear_compile_call(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	gear_ast *name_ast = ast->child[0];
	gear_ast *args_ast = ast->child[1];

	znode name_node;

	if (name_ast->kind != GEAR_AST_ZVAL || Z_TYPE_P(gear_ast_get_zval(name_ast)) != IS_STRING) {
		gear_compile_expr(&name_node, name_ast);
		gear_compile_dynamic_call(result, &name_node, args_ast);
		return;
	}

	{
		gear_bool runtime_resolution = gear_compile_function_name(&name_node, name_ast);
		if (runtime_resolution) {
			if (gear_string_equals_literal_ci(gear_ast_get_str(name_ast), "assert")) {
				gear_compile_assert(result, gear_ast_get_list(args_ast), Z_STR(name_node.u.constant), NULL);
			} else {
				gear_compile_ns_call(result, &name_node, args_ast);
			}
			return;
		}
	}

	{
		zval *name = &name_node.u.constant;
		gear_string *lcname;
		gear_function *fbc;
		gear_op *opline;

		lcname = gear_string_tolower(Z_STR_P(name));
		fbc = gear_hash_find_ptr(CG(function_table), lcname);

		/* Special assert() handling should apply independently of compiler flags. */
		if (fbc && gear_string_equals_literal(lcname, "assert")) {
			gear_compile_assert(result, gear_ast_get_list(args_ast), lcname, fbc);
			gear_string_release(lcname);
			zval_ptr_dtor(&name_node.u.constant);
			return;
		}

		if (!fbc
		 || (fbc->type == GEAR_INTERNAL_FUNCTION && (CG(compiler_options) & GEAR_COMPILE_IGNORE_INTERNAL_FUNCTIONS))
		 || (fbc->type == GEAR_USER_FUNCTION && (CG(compiler_options) & GEAR_COMPILE_IGNORE_USER_FUNCTIONS))
		) {
			gear_string_release_ex(lcname, 0);
			gear_compile_dynamic_call(result, &name_node, args_ast);
			return;
		}

		if (gear_try_compile_special_func(result, lcname,
				gear_ast_get_list(args_ast), fbc, type) == SUCCESS
		) {
			gear_string_release_ex(lcname, 0);
			zval_ptr_dtor(&name_node.u.constant);
			return;
		}

		zval_ptr_dtor(&name_node.u.constant);
		ZVAL_NEW_STR(&name_node.u.constant, lcname);

		opline = gear_emit_op(NULL, GEAR_INIT_FCALL, NULL, &name_node);
		opline->result.num = gear_alloc_cache_slot();

		gear_compile_call_common(result, args_ast, fbc);
	}
}
/* }}} */

void gear_compile_method_call(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	gear_ast *obj_ast = ast->child[0];
	gear_ast *method_ast = ast->child[1];
	gear_ast *args_ast = ast->child[2];

	znode obj_node, method_node;
	gear_op *opline;
	gear_function *fbc = NULL;

	if (is_this_fetch(obj_ast)) {
		obj_node.op_type = IS_UNUSED;
	} else {
		gear_compile_expr(&obj_node, obj_ast);
	}

	gear_compile_expr(&method_node, method_ast);
	opline = gear_emit_op(NULL, GEAR_INIT_METHOD_CALL, &obj_node, NULL);

	if (method_node.op_type == IS_CONST) {
		if (Z_TYPE(method_node.u.constant) != IS_STRING) {
			gear_error_noreturn(E_COMPILE_ERROR, "Method name must be a string");
		}

		opline->op2_type = IS_CONST;
		opline->op2.constant = gear_add_func_name_literal(CG(active_op_array),
			Z_STR(method_node.u.constant));
		opline->result.num = gear_alloc_polymorphic_cache_slot();
	} else {
		SET_NODE(opline->op2, &method_node);
	}

	/* Check if this calls a known method on $this */
	if (opline->op1_type == IS_UNUSED && opline->op2_type == IS_CONST &&
			CG(active_class_entry) && gear_is_scope_known()) {
		gear_string *lcname = Z_STR_P(CT_CONSTANT(opline->op2) + 1);
		fbc = gear_hash_find_ptr(&CG(active_class_entry)->function_table, lcname);

		/* We only know the exact method that is being called if it is either private or final.
		 * Otherwise an overriding method in a child class may be called. */
		if (fbc && !(fbc->common.fn_flags & (GEAR_ACC_PRIVATE|GEAR_ACC_FINAL))) {
			fbc = NULL;
		}
	}

	gear_compile_call_common(result, args_ast, fbc);
}
/* }}} */

static gear_bool gear_is_constructor(gear_string *name) /* {{{ */
{
	return gear_string_equals_literal_ci(name, GEAR_CONSTRUCTOR_FUNC_NAME);
}
/* }}} */

void gear_compile_static_call(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	gear_ast *class_ast = ast->child[0];
	gear_ast *method_ast = ast->child[1];
	gear_ast *args_ast = ast->child[2];

	znode class_node, method_node;
	gear_op *opline;
	gear_function *fbc = NULL;

	gear_compile_class_ref_ex(&class_node, class_ast, GEAR_FETCH_CLASS_EXCEPTION);

	gear_compile_expr(&method_node, method_ast);
	if (method_node.op_type == IS_CONST) {
		zval *name = &method_node.u.constant;
		if (Z_TYPE_P(name) != IS_STRING) {
			gear_error_noreturn(E_COMPILE_ERROR, "Method name must be a string");
		}
		if (gear_is_constructor(Z_STR_P(name))) {
			zval_ptr_dtor(name);
			method_node.op_type = IS_UNUSED;
		}
	}

	opline = get_next_op(CG(active_op_array));
	opline->opcode = GEAR_INIT_STATIC_METHOD_CALL;

	gear_set_class_name_op1(opline, &class_node);

	if (method_node.op_type == IS_CONST) {
		opline->op2_type = IS_CONST;
		opline->op2.constant = gear_add_func_name_literal(CG(active_op_array),
			Z_STR(method_node.u.constant));
		opline->result.num = gear_alloc_polymorphic_cache_slot();
	} else {
		if (opline->op1_type == IS_CONST) {
			opline->result.num = gear_alloc_cache_slot();
		}
		SET_NODE(opline->op2, &method_node);
	}
	gear_check_live_ranges(opline);

	/* Check if we already know which method we're calling */
	if (opline->op2_type == IS_CONST) {
		gear_class_entry *ce = NULL;
		if (opline->op1_type == IS_CONST) {
			gear_string *lcname = Z_STR_P(CT_CONSTANT(opline->op1) + 1);
			ce = gear_hash_find_ptr(CG(class_table), lcname);
			if (!ce && CG(active_class_entry)
					&& gear_string_equals_ci(CG(active_class_entry)->name, lcname)) {
				ce = CG(active_class_entry);
			}
		} else if (opline->op1_type == IS_UNUSED
				&& (opline->op1.num & GEAR_FETCH_CLASS_MASK) == GEAR_FETCH_CLASS_SELF
				&& gear_is_scope_known()) {
			ce = CG(active_class_entry);
		}
		if (ce) {
			gear_string *lcname = Z_STR_P(CT_CONSTANT(opline->op2) + 1);
			fbc = gear_hash_find_ptr(&ce->function_table, lcname);
			if (fbc && !(fbc->common.fn_flags & GEAR_ACC_PUBLIC)) {
				if (ce != CG(active_class_entry)
				 &&((fbc->common.fn_flags & GEAR_ACC_PRIVATE)
				  || !gear_check_protected(gear_get_function_root_class(fbc), CG(active_class_entry)))) {
					/* incompatibe function */
					fbc = NULL;
				}
			}
		}
	}

	gear_compile_call_common(result, args_ast, fbc);
}
/* }}} */

void gear_compile_class_decl(gear_ast *ast);

void gear_compile_new(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *class_ast = ast->child[0];
	gear_ast *args_ast = ast->child[1];

	znode class_node, ctor_result;
	gear_op *opline;

	if (class_ast->kind == GEAR_AST_CLASS) {
		uint32_t dcl_opnum = get_next_op_number(CG(active_op_array));
		gear_compile_class_decl(class_ast);
		/* jump over anon class declaration */
		opline = &CG(active_op_array)->opcodes[dcl_opnum];
		if (opline->opcode == GEAR_FETCH_CLASS) {
			opline++;
		}
		class_node.op_type = opline->result_type;
		class_node.u.op.var = opline->result.var;
		opline->extended_value = get_next_op_number(CG(active_op_array));
	} else {
		gear_compile_class_ref_ex(&class_node, class_ast, GEAR_FETCH_CLASS_EXCEPTION);
	}

	opline = gear_emit_op(result, GEAR_NEW, NULL, NULL);

	if (class_node.op_type == IS_CONST) {
		opline->op1_type = IS_CONST;
		opline->op1.constant = gear_add_class_name_literal(
			CG(active_op_array), Z_STR(class_node.u.constant));
		opline->op2.num = gear_alloc_cache_slot();
	} else {
		SET_NODE(opline->op1, &class_node);
	}

	gear_compile_call_common(&ctor_result, args_ast, NULL);
	gear_do_free(&ctor_result);
}
/* }}} */

void gear_compile_clone(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *obj_ast = ast->child[0];

	znode obj_node;
	gear_compile_expr(&obj_node, obj_ast);

	gear_emit_op_tmp(result, GEAR_CLONE, &obj_node, NULL);
}
/* }}} */

void gear_compile_global_var(gear_ast *ast) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];
	gear_ast *name_ast = var_ast->child[0];

	znode name_node, result;

	gear_compile_expr(&name_node, name_ast);
	if (name_node.op_type == IS_CONST) {
		convert_to_string(&name_node.u.constant);
	}

	if (is_this_fetch(var_ast)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use $this as global variable");
	} else if (gear_try_compile_cv(&result, var_ast) == SUCCESS) {
		gear_op *opline = gear_emit_op(NULL, GEAR_BIND_GLOBAL, &result, &name_node);
		opline->extended_value = gear_alloc_cache_slot();
	} else {
		/* name_ast should be evaluated only. FETCH_GLOBAL_LOCK instructs FETCH_W
		 * to not free the name_node operand, so it can be reused in the following
		 * ASSIGN_REF, which then frees it. */
		gear_op *opline = gear_emit_op(&result, GEAR_FETCH_W, &name_node, NULL);
		opline->extended_value = GEAR_FETCH_GLOBAL_LOCK;

		if (name_node.op_type == IS_CONST) {
			gear_string_addref(Z_STR(name_node.u.constant));
		}

		gear_emit_assign_ref_znode(
			gear_ast_create(GEAR_AST_VAR, gear_ast_create_znode(&name_node)),
			&result
		);
	}
}
/* }}} */

static void gear_compile_static_var_common(gear_ast *var_ast, zval *value, uint32_t by_ref) /* {{{ */
{
	znode var_node;
	gear_op *opline;
	gear_string *var_name;

	if (var_ast->kind == GEAR_AST_ZVAL) {
		var_name = zval_make_interned_string(gear_ast_get_zval(var_ast));
		gear_compile_expr(&var_node, var_ast);
	} else {
		gear_compile_expr(&var_node, var_ast);
		var_name = zval_make_interned_string(&var_node.u.constant);
	}

	if (!CG(active_op_array)->static_variables) {
		if (CG(active_op_array)->scope) {
			CG(active_op_array)->scope->ce_flags |= GEAR_HAS_STATIC_IN_METHODS;
		}
		CG(active_op_array)->static_variables = gear_new_array(8);
	}

	if (GC_REFCOUNT(CG(active_op_array)->static_variables) > 1) {
		if (!(GC_FLAGS(CG(active_op_array)->static_variables) & IS_ARRAY_IMMUTABLE)) {
			GC_DELREF(CG(active_op_array)->static_variables);
		}
		CG(active_op_array)->static_variables = gear_array_dup(CG(active_op_array)->static_variables);
	}
	value = gear_hash_update(CG(active_op_array)->static_variables, var_name, value);

	if (gear_string_equals_literal(var_name, "this")) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use $this as static variable");
	}

	opline = gear_emit_op(NULL, GEAR_BIND_STATIC, NULL, &var_node);
	opline->op1_type = IS_CV;
	opline->op1.var = lookup_cv(CG(active_op_array), var_name);
	opline->extended_value = (uint32_t)((char*)value - (char*)CG(active_op_array)->static_variables->arData) | by_ref;
}
/* }}} */

void gear_compile_static_var(gear_ast *ast) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];
	gear_ast *value_ast = ast->child[1];
	zval value_zv;

	if (value_ast) {
		gear_const_expr_to_zval(&value_zv, value_ast);
	} else {
		ZVAL_NULL(&value_zv);
	}

	gear_compile_static_var_common(var_ast, &value_zv, GEAR_BIND_REF);
}
/* }}} */

void gear_compile_unset(gear_ast *ast) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];
	znode var_node;
	gear_op *opline;

	gear_ensure_writable_variable(var_ast);

	switch (var_ast->kind) {
		case GEAR_AST_VAR:
			if (is_this_fetch(var_ast)) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot unset $this");
			} else if (gear_try_compile_cv(&var_node, var_ast) == SUCCESS) {
				opline = gear_emit_op(NULL, GEAR_UNSET_CV, &var_node, NULL);
			} else {
				opline = gear_compile_simple_var_no_cv(NULL, var_ast, BP_VAR_UNSET, 0);
				opline->opcode = GEAR_UNSET_VAR;
			}
			return;
		case GEAR_AST_DIM:
			opline = gear_compile_dim(NULL, var_ast, BP_VAR_UNSET);
			opline->opcode = GEAR_UNSET_DIM;
			return;
		case GEAR_AST_PROP:
			opline = gear_compile_prop(NULL, var_ast, BP_VAR_UNSET);
			opline->opcode = GEAR_UNSET_OBJ;
			return;
		case GEAR_AST_STATIC_PROP:
			opline = gear_compile_static_prop(NULL, var_ast, BP_VAR_UNSET, 0);
			opline->opcode = GEAR_UNSET_STATIC_PROP;
			return;
		EMPTY_SWITCH_DEFAULT_CASE()
	}
}
/* }}} */

static int gear_handle_loops_and_finally_ex(gear_long depth, znode *return_value) /* {{{ */
{
	gear_loop_var *base;
	gear_loop_var *loop_var = gear_stack_top(&CG(loop_var_stack));

	if (!loop_var) {
		return 1;
	}
	base = gear_stack_base(&CG(loop_var_stack));
	for (; loop_var >= base; loop_var--) {
		if (loop_var->opcode == GEAR_FAST_CALL) {
			gear_op *opline = get_next_op(CG(active_op_array));

			opline->opcode = GEAR_FAST_CALL;
			opline->result_type = IS_TMP_VAR;
			opline->result.var = loop_var->var_num;
			if (return_value) {
				SET_NODE(opline->op2, return_value);
			}
			opline->op1.num = loop_var->u.try_catch_offset;
		} else if (loop_var->opcode == GEAR_DISCARD_EXCEPTION) {
			gear_op *opline = get_next_op(CG(active_op_array));
			opline->opcode = GEAR_DISCARD_EXCEPTION;
			opline->op1_type = IS_TMP_VAR;
			opline->op1.var = loop_var->var_num;
		} else if (loop_var->opcode == GEAR_RETURN) {
			/* Stack separator */
			break;
		} else if (depth <= 1) {
			return 1;
		} else if (loop_var->opcode == GEAR_NOP) {
			/* Loop doesn't have freeable variable */
			depth--;
		} else {
			gear_op *opline;

			GEAR_ASSERT(loop_var->var_type & (IS_VAR|IS_TMP_VAR));
			opline = get_next_op(CG(active_op_array));
			opline->opcode = loop_var->opcode;
			opline->op1_type = loop_var->var_type;
			opline->op1.var = loop_var->var_num;
			opline->extended_value = GEAR_FREE_ON_RETURN;
			depth--;
	    }
	}
	return (depth == 0);
}
/* }}} */

static int gear_handle_loops_and_finally(znode *return_value) /* {{{ */
{
	return gear_handle_loops_and_finally_ex(gear_stack_count(&CG(loop_var_stack)) + 1, return_value);
}
/* }}} */

static int gear_has_finally_ex(gear_long depth) /* {{{ */
{
	gear_loop_var *base;
	gear_loop_var *loop_var = gear_stack_top(&CG(loop_var_stack));

	if (!loop_var) {
		return 0;
	}
	base = gear_stack_base(&CG(loop_var_stack));
	for (; loop_var >= base; loop_var--) {
		if (loop_var->opcode == GEAR_FAST_CALL) {
			return 1;
		} else if (loop_var->opcode == GEAR_DISCARD_EXCEPTION) {
		} else if (loop_var->opcode == GEAR_RETURN) {
			/* Stack separator */
			return 0;
		} else if (depth <= 1) {
			return 0;
		} else {
			depth--;
	    }
	}
	return 0;
}
/* }}} */

static int gear_has_finally(void) /* {{{ */
{
	return gear_has_finally_ex(gear_stack_count(&CG(loop_var_stack)) + 1);
}
/* }}} */

void gear_compile_return(gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	gear_bool is_generator = (CG(active_op_array)->fn_flags & GEAR_ACC_GENERATOR) != 0;
	gear_bool by_ref = (CG(active_op_array)->fn_flags & GEAR_ACC_RETURN_REFERENCE) != 0;

	znode expr_node;
	gear_op *opline;

	if (is_generator) {
		/* For generators the by-ref flag refers to yields, not returns */
		by_ref = 0;
	}

	if (!expr_ast) {
		expr_node.op_type = IS_CONST;
		ZVAL_NULL(&expr_node.u.constant);
	} else if (by_ref && gear_is_variable(expr_ast) && !gear_is_call(expr_ast)) {
		gear_compile_var(&expr_node, expr_ast, BP_VAR_W);
	} else {
		gear_compile_expr(&expr_node, expr_ast);
	}

	if ((CG(active_op_array)->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK)
	 && (expr_node.op_type == IS_CV || (by_ref && expr_node.op_type == IS_VAR))
	 && gear_has_finally()) {
		/* Copy return value into temporary VAR to avoid modification in finally code */
		if (by_ref) {
			gear_emit_op(&expr_node, GEAR_MAKE_REF, &expr_node, NULL);
		} else {
			gear_emit_op_tmp(&expr_node, GEAR_QM_ASSIGN, &expr_node, NULL);
		}
	}

	/* Generator return types are handled separately */
	if (!is_generator && CG(active_op_array)->fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
		gear_emit_return_type_check(
			expr_ast ? &expr_node : NULL, CG(active_op_array)->arg_info - 1, 0);
	}

	gear_handle_loops_and_finally((expr_node.op_type & (IS_TMP_VAR | IS_VAR)) ? &expr_node : NULL);

	opline = gear_emit_op(NULL, by_ref ? GEAR_RETURN_BY_REF : GEAR_RETURN,
		&expr_node, NULL);

	if (by_ref && expr_ast) {
		if (gear_is_call(expr_ast)) {
			opline->extended_value = GEAR_RETURNS_FUNCTION;
		} else if (!gear_is_variable(expr_ast)) {
			opline->extended_value = GEAR_RETURNS_VALUE;
		}
	}
}
/* }}} */

void gear_compile_echo(gear_ast *ast) /* {{{ */
{
	gear_op *opline;
	gear_ast *expr_ast = ast->child[0];

	znode expr_node;
	gear_compile_expr(&expr_node, expr_ast);

	opline = gear_emit_op(NULL, GEAR_ECHO, &expr_node, NULL);
	opline->extended_value = 0;
}
/* }}} */

void gear_compile_throw(gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];

	znode expr_node;
	gear_compile_expr(&expr_node, expr_ast);

	gear_emit_op(NULL, GEAR_THROW, &expr_node, NULL);
}
/* }}} */

void gear_compile_break_continue(gear_ast *ast) /* {{{ */
{
	gear_ast *depth_ast = ast->child[0];

	gear_op *opline;
	gear_long depth;

	GEAR_ASSERT(ast->kind == GEAR_AST_BREAK || ast->kind == GEAR_AST_CONTINUE);

	if (depth_ast) {
		zval *depth_zv;
		if (depth_ast->kind != GEAR_AST_ZVAL) {
			gear_error_noreturn(E_COMPILE_ERROR, "'%s' operator with non-integer operand "
				"is no longer supported", ast->kind == GEAR_AST_BREAK ? "break" : "continue");
		}

		depth_zv = gear_ast_get_zval(depth_ast);
		if (Z_TYPE_P(depth_zv) != IS_LONG || Z_LVAL_P(depth_zv) < 1) {
			gear_error_noreturn(E_COMPILE_ERROR, "'%s' operator accepts only positive integers",
				ast->kind == GEAR_AST_BREAK ? "break" : "continue");
		}

		depth = Z_LVAL_P(depth_zv);
	} else {
		depth = 1;
	}

	if (CG(context).current_brk_cont == -1) {
		gear_error_noreturn(E_COMPILE_ERROR, "'%s' not in the 'loop' or 'switch' context",
			ast->kind == GEAR_AST_BREAK ? "break" : "continue");
	} else {
		if (!gear_handle_loops_and_finally_ex(depth, NULL)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot '%s' " GEAR_LONG_FMT " level%s",
				ast->kind == GEAR_AST_BREAK ? "break" : "continue",
				depth, depth == 1 ? "" : "s");
		}
	}

	if (ast->kind == GEAR_AST_CONTINUE) {
		int d, cur = CG(context).current_brk_cont;
		for (d = depth - 1; d > 0; d--) {
			cur = CG(context).brk_cont_array[cur].parent;
			GEAR_ASSERT(cur != -1);
		}

		if (CG(context).brk_cont_array[cur].is_switch) {
			if (depth == 1) {
				gear_error(E_WARNING,
					"\"continue\" targeting switch is equivalent to \"break\". " \
					"Did you mean to use \"continue " GEAR_LONG_FMT "\"?",
					depth + 1);
			} else {
				gear_error(E_WARNING,
					"\"continue " GEAR_LONG_FMT "\" targeting switch is equivalent to \"break " GEAR_LONG_FMT "\". " \
					"Did you mean to use \"continue " GEAR_LONG_FMT "\"?",
					depth, depth, depth + 1);
			}
		}
	}

	opline = gear_emit_op(NULL, ast->kind == GEAR_AST_BREAK ? GEAR_BRK : GEAR_CONT, NULL, NULL);
	opline->op1.num = CG(context).current_brk_cont;
	opline->op2.num = depth;
}
/* }}} */

void gear_resolve_goto_label(gear_op_array *op_array, gear_op *opline) /* {{{ */
{
	gear_label *dest;
	int current, remove_oplines = opline->op1.num;
	zval *label;
	uint32_t opnum = opline - op_array->opcodes;

	label = CT_CONSTANT_EX(op_array, opline->op2.constant);
	if (CG(context).labels == NULL ||
	    (dest = gear_hash_find_ptr(CG(context).labels, Z_STR_P(label))) == NULL
	) {
		CG(in_compilation) = 1;
		CG(active_op_array) = op_array;
		CG(gear_lineno) = opline->lineno;
		gear_error_noreturn(E_COMPILE_ERROR, "'goto' to undefined label '%s'", Z_STRVAL_P(label));
	}

	zval_ptr_dtor_str(label);
	ZVAL_NULL(label);

	current = opline->extended_value;
	for (; current != dest->brk_cont; current = CG(context).brk_cont_array[current].parent) {
		if (current == -1) {
			CG(in_compilation) = 1;
			CG(active_op_array) = op_array;
			CG(gear_lineno) = opline->lineno;
			gear_error_noreturn(E_COMPILE_ERROR, "'goto' into loop or switch statement is disallowed");
		}
		if (CG(context).brk_cont_array[current].start >= 0) {
			remove_oplines--;
		}
	}

	for (current = 0; current < op_array->last_try_catch; ++current) {
		gear_try_catch_element *elem = &op_array->try_catch_array[current];
		if (elem->try_op > opnum) {
			break;
		}
		if (elem->finally_op && opnum < elem->finally_op - 1
			&& (dest->opline_num > elem->finally_end || dest->opline_num < elem->try_op)
		) {
			remove_oplines--;
		}
	}

	opline->opcode = GEAR_JMP;
	opline->op1.opline_num = dest->opline_num;
	opline->extended_value = 0;
	SET_UNUSED(opline->op1);
	SET_UNUSED(opline->op2);
	SET_UNUSED(opline->result);

	GEAR_ASSERT(remove_oplines >= 0);
	while (remove_oplines--) {
		opline--;
		MAKE_NOP(opline);
		GEAR_VM_SET_OPCODE_HANDLER(opline);
	}
}
/* }}} */

void gear_compile_goto(gear_ast *ast) /* {{{ */
{
	gear_ast *label_ast = ast->child[0];
	znode label_node;
	gear_op *opline;
	uint32_t opnum_start = get_next_op_number(CG(active_op_array));

	gear_compile_expr(&label_node, label_ast);

	/* Label resolution and unwinding adjustments happen in pass two. */
	gear_handle_loops_and_finally(NULL);
	opline = gear_emit_op(NULL, GEAR_GOTO, NULL, &label_node);
	opline->op1.num = get_next_op_number(CG(active_op_array)) - opnum_start - 1;
	opline->extended_value = CG(context).current_brk_cont;
}
/* }}} */

void gear_compile_label(gear_ast *ast) /* {{{ */
{
	gear_string *label = gear_ast_get_str(ast->child[0]);
	gear_label dest;

	if (!CG(context).labels) {
		ALLOC_HASHTABLE(CG(context).labels);
		gear_hash_init(CG(context).labels, 8, NULL, label_ptr_dtor, 0);
	}

	dest.brk_cont = CG(context).current_brk_cont;
	dest.opline_num = get_next_op_number(CG(active_op_array));

	if (!gear_hash_add_mem(CG(context).labels, label, &dest, sizeof(gear_label))) {
		gear_error_noreturn(E_COMPILE_ERROR, "Label '%s' already defined", ZSTR_VAL(label));
	}
}
/* }}} */

void gear_compile_while(gear_ast *ast) /* {{{ */
{
	gear_ast *cond_ast = ast->child[0];
	gear_ast *stmt_ast = ast->child[1];
	znode cond_node;
	uint32_t opnum_start, opnum_jmp, opnum_cond;

	opnum_jmp = gear_emit_jump(0);

	gear_begin_loop(GEAR_NOP, NULL, 0);

	opnum_start = get_next_op_number(CG(active_op_array));
	gear_compile_stmt(stmt_ast);

	opnum_cond = get_next_op_number(CG(active_op_array));
	gear_update_jump_target(opnum_jmp, opnum_cond);
	gear_compile_expr(&cond_node, cond_ast);

	gear_emit_cond_jump(GEAR_JMPNZ, &cond_node, opnum_start);

	gear_end_loop(opnum_cond, NULL);
}
/* }}} */

void gear_compile_do_while(gear_ast *ast) /* {{{ */
{
	gear_ast *stmt_ast = ast->child[0];
	gear_ast *cond_ast = ast->child[1];

	znode cond_node;
	uint32_t opnum_start, opnum_cond;

	gear_begin_loop(GEAR_NOP, NULL, 0);

	opnum_start = get_next_op_number(CG(active_op_array));
	gear_compile_stmt(stmt_ast);

	opnum_cond = get_next_op_number(CG(active_op_array));
	gear_compile_expr(&cond_node, cond_ast);

	gear_emit_cond_jump(GEAR_JMPNZ, &cond_node, opnum_start);

	gear_end_loop(opnum_cond, NULL);
}
/* }}} */

void gear_compile_expr_list(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast_list *list;
	uint32_t i;

	result->op_type = IS_CONST;
	ZVAL_TRUE(&result->u.constant);

	if (!ast) {
		return;
	}

	list = gear_ast_get_list(ast);
	for (i = 0; i < list->children; ++i) {
		gear_ast *expr_ast = list->child[i];

		gear_do_free(result);
		gear_compile_expr(result, expr_ast);
	}
}
/* }}} */

void gear_compile_for(gear_ast *ast) /* {{{ */
{
	gear_ast *init_ast = ast->child[0];
	gear_ast *cond_ast = ast->child[1];
	gear_ast *loop_ast = ast->child[2];
	gear_ast *stmt_ast = ast->child[3];

	znode result;
	uint32_t opnum_start, opnum_jmp, opnum_loop;

	gear_compile_expr_list(&result, init_ast);
	gear_do_free(&result);

	opnum_jmp = gear_emit_jump(0);

	gear_begin_loop(GEAR_NOP, NULL, 0);

	opnum_start = get_next_op_number(CG(active_op_array));
	gear_compile_stmt(stmt_ast);

	opnum_loop = get_next_op_number(CG(active_op_array));
	gear_compile_expr_list(&result, loop_ast);
	gear_do_free(&result);

	gear_update_jump_target_to_next(opnum_jmp);
	gear_compile_expr_list(&result, cond_ast);
	gear_do_extended_info();

	gear_emit_cond_jump(GEAR_JMPNZ, &result, opnum_start);

	gear_end_loop(opnum_loop, NULL);
}
/* }}} */

void gear_compile_foreach(gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	gear_ast *value_ast = ast->child[1];
	gear_ast *key_ast = ast->child[2];
	gear_ast *stmt_ast = ast->child[3];
	gear_bool by_ref = value_ast->kind == GEAR_AST_REF;
	gear_bool is_variable = gear_is_variable(expr_ast) && !gear_is_call(expr_ast)
		&& gear_can_write_to_variable(expr_ast);

	znode expr_node, reset_node, value_node, key_node;
	gear_op *opline;
	uint32_t opnum_reset, opnum_fetch;

	if (key_ast) {
		if (key_ast->kind == GEAR_AST_REF) {
			gear_error_noreturn(E_COMPILE_ERROR, "Key element cannot be a reference");
		}
		if (key_ast->kind == GEAR_AST_ARRAY) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use list as key element");
		}
	}

	if (by_ref) {
		value_ast = value_ast->child[0];
	}

	if (value_ast->kind == GEAR_AST_ARRAY && gear_propagate_list_refs(value_ast)) {
		by_ref = 1;
	}

	if (by_ref && is_variable) {
		gear_compile_var(&expr_node, expr_ast, BP_VAR_W);
	} else {
		gear_compile_expr(&expr_node, expr_ast);
	}

	if (by_ref) {
		gear_separate_if_call_and_write(&expr_node, expr_ast, BP_VAR_W);
	}

	opnum_reset = get_next_op_number(CG(active_op_array));
	opline = gear_emit_op(&reset_node, by_ref ? GEAR_FE_RESET_RW : GEAR_FE_RESET_R, &expr_node, NULL);

	gear_begin_loop(GEAR_FE_FREE, &reset_node, 0);

	opnum_fetch = get_next_op_number(CG(active_op_array));
	opline = gear_emit_op(NULL, by_ref ? GEAR_FE_FETCH_RW : GEAR_FE_FETCH_R, &reset_node, NULL);

	if (is_this_fetch(value_ast)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot re-assign $this");
	} else if (value_ast->kind == GEAR_AST_VAR &&
		gear_try_compile_cv(&value_node, value_ast) == SUCCESS) {
		SET_NODE(opline->op2, &value_node);
	} else {
		opline->op2_type = IS_VAR;
		opline->op2.var = get_temporary_variable(CG(active_op_array));
		GET_NODE(&value_node, opline->op2);
		if (value_ast->kind == GEAR_AST_ARRAY) {
			gear_compile_list_assign(NULL, value_ast, &value_node, value_ast->attr);
		} else if (by_ref) {
			gear_emit_assign_ref_znode(value_ast, &value_node);
		} else {
			gear_emit_assign_znode(value_ast, &value_node);
		}
	}

	if (key_ast) {
		opline = &CG(active_op_array)->opcodes[opnum_fetch];
		gear_make_tmp_result(&key_node, opline);
		gear_emit_assign_znode(key_ast, &key_node);
	}

	gear_compile_stmt(stmt_ast);

	/* Place JMP and FE_FREE on the line where foreach starts. It would be
	 * better to use the end line, but this information is not available
	 * currently. */
	CG(gear_lineno) = ast->lineno;
	gear_emit_jump(opnum_fetch);

	opline = &CG(active_op_array)->opcodes[opnum_reset];
	opline->op2.opline_num = get_next_op_number(CG(active_op_array));

	opline = &CG(active_op_array)->opcodes[opnum_fetch];
	opline->extended_value = get_next_op_number(CG(active_op_array));

	gear_end_loop(opnum_fetch, &reset_node);

	opline = gear_emit_op(NULL, GEAR_FE_FREE, &reset_node, NULL);
}
/* }}} */

void gear_compile_if(gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t i;
	uint32_t *jmp_opnums = NULL;

	if (list->children > 1) {
		jmp_opnums = safe_emalloc(sizeof(uint32_t), list->children - 1, 0);
	}

	for (i = 0; i < list->children; ++i) {
		gear_ast *elem_ast = list->child[i];
		gear_ast *cond_ast = elem_ast->child[0];
		gear_ast *stmt_ast = elem_ast->child[1];

		znode cond_node;
		uint32_t opnum_jmpz;
		if (cond_ast) {
			gear_compile_expr(&cond_node, cond_ast);
			opnum_jmpz = gear_emit_cond_jump(GEAR_JMPZ, &cond_node, 0);
		}

		gear_compile_stmt(stmt_ast);

		if (i != list->children - 1) {
			jmp_opnums[i] = gear_emit_jump(0);
		}

		if (cond_ast) {
			gear_update_jump_target_to_next(opnum_jmpz);
		}
	}

	if (list->children > 1) {
		for (i = 0; i < list->children - 1; ++i) {
			gear_update_jump_target_to_next(jmp_opnums[i]);
		}
		efree(jmp_opnums);
	}
}
/* }}} */

static gear_uchar determine_switch_jumptable_type(gear_ast_list *cases) {
	uint32_t i;
	gear_uchar common_type = IS_UNDEF;
	for (i = 0; i < cases->children; i++) {
		gear_ast *case_ast = cases->child[i];
		gear_ast **cond_ast = &case_ast->child[0];
		zval *cond_zv;
		if (!case_ast->child[0]) {
			/* Skip default clause */
			continue;
		}

		gear_eval_const_expr(cond_ast);
		if ((*cond_ast)->kind != GEAR_AST_ZVAL) {
			/* Non-constant case */
			return IS_UNDEF;
		}

		cond_zv = gear_ast_get_zval(case_ast->child[0]);
		if (Z_TYPE_P(cond_zv) != IS_LONG && Z_TYPE_P(cond_zv) != IS_STRING) {
			/* We only optimize switched on integers and strings */
			return IS_UNDEF;
		}

		if (common_type == IS_UNDEF) {
			common_type = Z_TYPE_P(cond_zv);
		} else if (common_type != Z_TYPE_P(cond_zv)) {
			/* Non-uniform case types */
			return IS_UNDEF;
		}

		if (Z_TYPE_P(cond_zv) == IS_STRING
				&& is_numeric_string(Z_STRVAL_P(cond_zv), Z_STRLEN_P(cond_zv), NULL, NULL, 0)) {
			/* Numeric strings cannot be compared with a simple hash lookup */
			return IS_UNDEF;
		}
	}

	return common_type;
}

static gear_bool should_use_jumptable(gear_ast_list *cases, gear_uchar jumptable_type) {
	if (CG(compiler_options) & GEAR_COMPILE_NO_JUMPTABLES) {
		return 0;
	}

	/* Thresholds are chosen based on when the average switch time for equidistributed
	 * input becomes smaller when using the jumptable optimization. */
	if (jumptable_type == IS_LONG) {
		return cases->children >= 5;
	} else {
		GEAR_ASSERT(jumptable_type == IS_STRING);
		return cases->children >= 2;
	}
}

void gear_compile_switch(gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	gear_ast_list *cases = gear_ast_get_list(ast->child[1]);

	uint32_t i;
	gear_bool has_default_case = 0;

	znode expr_node, case_node;
	gear_op *opline;
	uint32_t *jmpnz_opnums, opnum_default_jmp, opnum_switch;
	gear_uchar jumptable_type;
	HashTable *jumptable = NULL;

	gear_compile_expr(&expr_node, expr_ast);

	gear_begin_loop(GEAR_FREE, &expr_node, 1);

	case_node.op_type = IS_TMP_VAR;
	case_node.u.op.var = get_temporary_variable(CG(active_op_array));

	jumptable_type = determine_switch_jumptable_type(cases);
	if (jumptable_type != IS_UNDEF && should_use_jumptable(cases, jumptable_type)) {
		znode jumptable_op;

		ALLOC_HASHTABLE(jumptable);
		gear_hash_init(jumptable, cases->children, NULL, NULL, 0);
		jumptable_op.op_type = IS_CONST;
		ZVAL_ARR(&jumptable_op.u.constant, jumptable);

		opline = gear_emit_op(NULL,
			jumptable_type == IS_LONG ? GEAR_SWITCH_LONG : GEAR_SWITCH_STRING,
			&expr_node, &jumptable_op);
		if (opline->op1_type == IS_CONST) {
			Z_TRY_ADDREF_P(CT_CONSTANT(opline->op1));
		}
		opnum_switch = opline - CG(active_op_array)->opcodes;
	}

	jmpnz_opnums = safe_emalloc(sizeof(uint32_t), cases->children, 0);
	for (i = 0; i < cases->children; ++i) {
		gear_ast *case_ast = cases->child[i];
		gear_ast *cond_ast = case_ast->child[0];
		znode cond_node;

		if (!cond_ast) {
			if (has_default_case) {
				CG(gear_lineno) = case_ast->lineno;
				gear_error_noreturn(E_COMPILE_ERROR,
					"Switch statements may only contain one default clause");
			}
			has_default_case = 1;
			continue;
		}

		gear_compile_expr(&cond_node, cond_ast);

		if (expr_node.op_type == IS_CONST
			&& Z_TYPE(expr_node.u.constant) == IS_FALSE) {
			jmpnz_opnums[i] = gear_emit_cond_jump(GEAR_JMPZ, &cond_node, 0);
		} else if (expr_node.op_type == IS_CONST
			&& Z_TYPE(expr_node.u.constant) == IS_TRUE) {
			jmpnz_opnums[i] = gear_emit_cond_jump(GEAR_JMPNZ, &cond_node, 0);
		} else {
			opline = gear_emit_op(NULL,
				(expr_node.op_type & (IS_VAR|IS_TMP_VAR)) ? GEAR_CASE : GEAR_IS_EQUAL,
				&expr_node, &cond_node);
			SET_NODE(opline->result, &case_node);
			if (opline->op1_type == IS_CONST) {
				Z_TRY_ADDREF_P(CT_CONSTANT(opline->op1));
			}

			jmpnz_opnums[i] = gear_emit_cond_jump(GEAR_JMPNZ, &case_node, 0);
		}
	}

	opnum_default_jmp = gear_emit_jump(0);

	for (i = 0; i < cases->children; ++i) {
		gear_ast *case_ast = cases->child[i];
		gear_ast *cond_ast = case_ast->child[0];
		gear_ast *stmt_ast = case_ast->child[1];

		if (cond_ast) {
			gear_update_jump_target_to_next(jmpnz_opnums[i]);

			if (jumptable) {
				zval *cond_zv = gear_ast_get_zval(cond_ast);
				zval jmp_target;
				ZVAL_LONG(&jmp_target, get_next_op_number(CG(active_op_array)));

				GEAR_ASSERT(Z_TYPE_P(cond_zv) == jumptable_type);
				if (Z_TYPE_P(cond_zv) == IS_LONG) {
					gear_hash_index_add(jumptable, Z_LVAL_P(cond_zv), &jmp_target);
				} else {
					GEAR_ASSERT(Z_TYPE_P(cond_zv) == IS_STRING);
					gear_hash_add(jumptable, Z_STR_P(cond_zv), &jmp_target);
				}
			}
		} else {
			gear_update_jump_target_to_next(opnum_default_jmp);

			if (jumptable) {
				opline = &CG(active_op_array)->opcodes[opnum_switch];
				opline->extended_value = get_next_op_number(CG(active_op_array));
			}
		}

		gear_compile_stmt(stmt_ast);
	}

	if (!has_default_case) {
		gear_update_jump_target_to_next(opnum_default_jmp);

		if (jumptable) {
			opline = &CG(active_op_array)->opcodes[opnum_switch];
			opline->extended_value = get_next_op_number(CG(active_op_array));
		}
	}

	gear_end_loop(get_next_op_number(CG(active_op_array)), &expr_node);

	if (expr_node.op_type & (IS_VAR|IS_TMP_VAR)) {
		/* don't use emit_op() to prevent automatic live-range construction */
		opline = get_next_op(CG(active_op_array));
		opline->opcode = GEAR_FREE;
		SET_NODE(opline->op1, &expr_node);
	} else if (expr_node.op_type == IS_CONST) {
		zval_ptr_dtor_nogc(&expr_node.u.constant);
	}

	efree(jmpnz_opnums);
}
/* }}} */

void gear_compile_try(gear_ast *ast) /* {{{ */
{
	gear_ast *try_ast = ast->child[0];
	gear_ast_list *catches = gear_ast_get_list(ast->child[1]);
	gear_ast *finally_ast = ast->child[2];

	uint32_t i, j;
	gear_op *opline;
	uint32_t try_catch_offset;
	uint32_t *jmp_opnums = safe_emalloc(sizeof(uint32_t), catches->children, 0);
	uint32_t orig_fast_call_var = CG(context).fast_call_var;
	uint32_t orig_try_catch_offset = CG(context).try_catch_offset;

	if (catches->children == 0 && !finally_ast) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use try without catch or finally");
	}

	/* label: try { } must not be equal to try { label: } */
	if (CG(context).labels) {
		gear_label *label;
		GEAR_HASH_REVERSE_FOREACH_PTR(CG(context).labels, label) {
			if (label->opline_num == get_next_op_number(CG(active_op_array))) {
				gear_emit_op(NULL, GEAR_NOP, NULL, NULL);
			}
			break;
		} GEAR_HASH_FOREACH_END();
	}

	try_catch_offset = gear_add_try_element(get_next_op_number(CG(active_op_array)));

	if (finally_ast) {
		gear_loop_var fast_call;
		if (!(CG(active_op_array)->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK)) {
			CG(active_op_array)->fn_flags |= GEAR_ACC_HAS_FINALLY_BLOCK;
		}
		CG(context).fast_call_var = get_temporary_variable(CG(active_op_array));

		/* Push FAST_CALL on unwind stack */
		fast_call.opcode = GEAR_FAST_CALL;
		fast_call.var_type = IS_TMP_VAR;
		fast_call.var_num = CG(context).fast_call_var;
		fast_call.u.try_catch_offset = try_catch_offset;
		gear_stack_push(&CG(loop_var_stack), &fast_call);
	}

	CG(context).try_catch_offset = try_catch_offset;

	gear_compile_stmt(try_ast);

	if (catches->children != 0) {
		jmp_opnums[0] = gear_emit_jump(0);
	}

	for (i = 0; i < catches->children; ++i) {
		gear_ast *catch_ast = catches->child[i];
		gear_ast_list *classes = gear_ast_get_list(catch_ast->child[0]);
		gear_ast *var_ast = catch_ast->child[1];
		gear_ast *stmt_ast = catch_ast->child[2];
		gear_string *var_name = zval_make_interned_string(gear_ast_get_zval(var_ast));
		gear_bool is_last_catch = (i + 1 == catches->children);

		uint32_t *jmp_multicatch = safe_emalloc(sizeof(uint32_t), classes->children - 1, 0);
		uint32_t opnum_catch;

		CG(gear_lineno) = catch_ast->lineno;

		for (j = 0; j < classes->children; j++) {

			gear_ast *class_ast = classes->child[j];
			gear_bool is_last_class = (j + 1 == classes->children);

			if (!gear_is_const_default_class_ref(class_ast)) {
				gear_error_noreturn(E_COMPILE_ERROR, "Bad class name in the catch statement");
			}

			opnum_catch = get_next_op_number(CG(active_op_array));
			if (i == 0 && j == 0) {
				CG(active_op_array)->try_catch_array[try_catch_offset].catch_op = opnum_catch;
			}

			opline = get_next_op(CG(active_op_array));
			opline->opcode = GEAR_CATCH;
			opline->op1_type = IS_CONST;
			opline->op1.constant = gear_add_class_name_literal(CG(active_op_array),
					gear_resolve_class_name_ast(class_ast));
			opline->extended_value = gear_alloc_cache_slot();

			if (gear_string_equals_literal(var_name, "this")) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot re-assign $this");
			}

			opline->result_type = IS_CV;
			opline->result.var = lookup_cv(CG(active_op_array), var_name);

			if (is_last_catch && is_last_class) {
				opline->extended_value |= GEAR_LAST_CATCH;
			}

			if (!is_last_class) {
				jmp_multicatch[j] = gear_emit_jump(0);
				opline = &CG(active_op_array)->opcodes[opnum_catch];
				opline->op2.opline_num = get_next_op_number(CG(active_op_array));
			}
		}

		for (j = 0; j < classes->children - 1; j++) {
			gear_update_jump_target_to_next(jmp_multicatch[j]);
		}

		efree(jmp_multicatch);

		gear_compile_stmt(stmt_ast);

		if (!is_last_catch) {
			jmp_opnums[i + 1] = gear_emit_jump(0);
		}

		opline = &CG(active_op_array)->opcodes[opnum_catch];
		if (!is_last_catch) {
			opline->op2.opline_num = get_next_op_number(CG(active_op_array));
		}
	}

	for (i = 0; i < catches->children; ++i) {
		gear_update_jump_target_to_next(jmp_opnums[i]);
	}

	if (finally_ast) {
		gear_loop_var discard_exception;
		uint32_t opnum_jmp = get_next_op_number(CG(active_op_array)) + 1;

		/* Pop FAST_CALL from unwind stack */
		gear_stack_del_top(&CG(loop_var_stack));

		/* Push DISCARD_EXCEPTION on unwind stack */
		discard_exception.opcode = GEAR_DISCARD_EXCEPTION;
		discard_exception.var_type = IS_TMP_VAR;
		discard_exception.var_num = CG(context).fast_call_var;
		gear_stack_push(&CG(loop_var_stack), &discard_exception);

		CG(gear_lineno) = finally_ast->lineno;

		opline = gear_emit_op(NULL, GEAR_FAST_CALL, NULL, NULL);
		opline->op1.num = try_catch_offset;
		opline->result_type = IS_TMP_VAR;
		opline->result.var = CG(context).fast_call_var;

		gear_emit_op(NULL, GEAR_JMP, NULL, NULL);

		gear_compile_stmt(finally_ast);

		CG(active_op_array)->try_catch_array[try_catch_offset].finally_op = opnum_jmp + 1;
		CG(active_op_array)->try_catch_array[try_catch_offset].finally_end
			= get_next_op_number(CG(active_op_array));

		opline = gear_emit_op(NULL, GEAR_FAST_RET, NULL, NULL);
		opline->op1_type = IS_TMP_VAR;
		opline->op1.var = CG(context).fast_call_var;
		opline->op2.num = orig_try_catch_offset;

		gear_update_jump_target_to_next(opnum_jmp);

		CG(context).fast_call_var = orig_fast_call_var;

		/* Pop DISCARD_EXCEPTION from unwind stack */
		gear_stack_del_top(&CG(loop_var_stack));
	}

	CG(context).try_catch_offset = orig_try_catch_offset;

	efree(jmp_opnums);
}
/* }}} */

/* Encoding declarations must already be handled during parsing */
gear_bool gear_handle_encoding_declaration(gear_ast *ast) /* {{{ */
{
	gear_ast_list *declares = gear_ast_get_list(ast);
	uint32_t i;
	for (i = 0; i < declares->children; ++i) {
		gear_ast *declare_ast = declares->child[i];
		gear_ast *name_ast = declare_ast->child[0];
		gear_ast *value_ast = declare_ast->child[1];
		gear_string *name = gear_ast_get_str(name_ast);

		if (gear_string_equals_literal_ci(name, "encoding")) {
			if (value_ast->kind != GEAR_AST_ZVAL) {
				gear_throw_exception(gear_ce_compile_error, "Encoding must be a literal", 0);
				return 0;
			}

			if (CG(multibyte)) {
				gear_string *encoding_name = zval_get_string(gear_ast_get_zval(value_ast));

				const gear_encoding *new_encoding, *old_encoding;
				gear_encoding_filter old_input_filter;

				CG(encoding_declared) = 1;

				new_encoding = gear_multibyte_fetch_encoding(ZSTR_VAL(encoding_name));
				if (!new_encoding) {
					gear_error(E_COMPILE_WARNING, "Unsupported encoding [%s]", ZSTR_VAL(encoding_name));
				} else {
					old_input_filter = LANG_SCNG(input_filter);
					old_encoding = LANG_SCNG(script_encoding);
					gear_multibyte_set_filter(new_encoding);

					/* need to re-scan if input filter changed */
					if (old_input_filter != LANG_SCNG(input_filter) ||
						 (old_input_filter && new_encoding != old_encoding)) {
						gear_multibyte_yyinput_again(old_input_filter, old_encoding);
					}
				}

				gear_string_release_ex(encoding_name, 0);
			} else {
				gear_error(E_COMPILE_WARNING, "declare(encoding=...) ignored because "
					"Gear multibyte feature is turned off by settings");
			}
		}
	}

	return 1;
}
/* }}} */

static int gear_declare_is_first_statement(gear_ast *ast) /* {{{ */
{
	uint32_t i = 0;
	gear_ast_list *file_ast = gear_ast_get_list(CG(ast));

	/* Check to see if this declare is preceded only by declare statements */
	while (i < file_ast->children) {
		if (file_ast->child[i] == ast) {
			return SUCCESS;
		} else if (file_ast->child[i] == NULL) {
			/* Empty statements are not allowed prior to a declare */
			return FAILURE;
		} else if (file_ast->child[i]->kind != GEAR_AST_DECLARE) {
			/* declares can only be preceded by other declares */
			return FAILURE;
		}
		i++;
	}
	return FAILURE;
}
/* }}} */

void gear_compile_declare(gear_ast *ast) /* {{{ */
{
	gear_ast_list *declares = gear_ast_get_list(ast->child[0]);
	gear_ast *stmt_ast = ast->child[1];
	gear_declarables orig_declarables = FC(declarables);
	uint32_t i;

	for (i = 0; i < declares->children; ++i) {
		gear_ast *declare_ast = declares->child[i];
		gear_ast *name_ast = declare_ast->child[0];
		gear_ast *value_ast = declare_ast->child[1];
		gear_string *name = gear_ast_get_str(name_ast);

		if (value_ast->kind != GEAR_AST_ZVAL) {
			gear_error_noreturn(E_COMPILE_ERROR, "declare(%s) value must be a literal", ZSTR_VAL(name));
		}

		if (gear_string_equals_literal_ci(name, "ticks")) {
			zval value_zv;
			gear_const_expr_to_zval(&value_zv, value_ast);
			FC(declarables).ticks = zval_get_long(&value_zv);
			zval_ptr_dtor_nogc(&value_zv);
		} else if (gear_string_equals_literal_ci(name, "encoding")) {

			if (FAILURE == gear_declare_is_first_statement(ast)) {
				gear_error_noreturn(E_COMPILE_ERROR, "Encoding declaration pragma must be "
					"the very first statement in the script");
			}
		} else if (gear_string_equals_literal_ci(name, "strict_types")) {
			zval value_zv;

			if (FAILURE == gear_declare_is_first_statement(ast)) {
				gear_error_noreturn(E_COMPILE_ERROR, "strict_types declaration must be "
					"the very first statement in the script");
			}

			if (ast->child[1] != NULL) {
				gear_error_noreturn(E_COMPILE_ERROR, "strict_types declaration must not "
					"use block mode");
			}

			gear_const_expr_to_zval(&value_zv, value_ast);

			if (Z_TYPE(value_zv) != IS_LONG || (Z_LVAL(value_zv) != 0 && Z_LVAL(value_zv) != 1)) {
				gear_error_noreturn(E_COMPILE_ERROR, "strict_types declaration must have 0 or 1 as its value");
			}

			if (Z_LVAL(value_zv) == 1) {
				CG(active_op_array)->fn_flags |= GEAR_ACC_STRICT_TYPES;
			}

		} else {
			gear_error(E_COMPILE_WARNING, "Unsupported declare '%s'", ZSTR_VAL(name));
		}
	}

	if (stmt_ast) {
		gear_compile_stmt(stmt_ast);

		FC(declarables) = orig_declarables;
	}
}
/* }}} */

void gear_compile_stmt_list(gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t i;
	for (i = 0; i < list->children; ++i) {
		gear_compile_stmt(list->child[i]);
	}
}
/* }}} */

GEAR_API void gear_set_function_arg_flags(gear_function *func) /* {{{ */
{
	uint32_t i, n;

	func->common.arg_flags[0] = 0;
	func->common.arg_flags[1] = 0;
	func->common.arg_flags[2] = 0;
	if (func->common.arg_info) {
		n = MIN(func->common.num_args, MAX_ARG_FLAG_NUM);
		i = 0;
		while (i < n) {
			GEAR_SET_ARG_FLAG(func, i + 1, func->common.arg_info[i].pass_by_reference);
			i++;
		}
		if (UNEXPECTED(func->common.fn_flags & GEAR_ACC_VARIADIC && func->common.arg_info[i].pass_by_reference)) {
			uint32_t pass_by_reference = func->common.arg_info[i].pass_by_reference;
			while (i < MAX_ARG_FLAG_NUM) {
				GEAR_SET_ARG_FLAG(func, i + 1, pass_by_reference);
				i++;
			}
		}
	}
}
/* }}} */

static void gear_compile_typename(gear_ast *ast, gear_arg_info *arg_info, gear_bool allow_null) /* {{{ */
{
	if (ast->kind == GEAR_AST_TYPE) {
		arg_info->type = GEAR_TYPE_ENCODE(ast->attr, allow_null);
	} else {
		gear_string *class_name = gear_ast_get_str(ast);
		gear_uchar type = gear_lookup_builtin_type_by_name(class_name);

		if (type != 0) {
			if ((ast->attr & GEAR_NAME_NOT_FQ) != GEAR_NAME_NOT_FQ) {
				gear_error_noreturn(E_COMPILE_ERROR,
					"Type declaration '%s' must be unqualified",
					ZSTR_VAL(gear_string_tolower(class_name)));
			}
			arg_info->type = GEAR_TYPE_ENCODE(type, allow_null);
		} else {
			uint32_t fetch_type = gear_get_class_fetch_type_ast(ast);
			if (fetch_type == GEAR_FETCH_CLASS_DEFAULT) {
				class_name = gear_resolve_class_name_ast(ast);
				gear_assert_valid_class_name(class_name);
			} else {
				gear_ensure_valid_class_fetch_type(fetch_type);
				gear_string_addref(class_name);
			}

			arg_info->type = GEAR_TYPE_ENCODE_CLASS(class_name, allow_null);
		}
	}
}
/* }}} */

void gear_compile_params(gear_ast *ast, gear_ast *return_type_ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t i;
	gear_op_array *op_array = CG(active_op_array);
	gear_arg_info *arg_infos;

	if (return_type_ast) {
		gear_bool allow_null = 0;

		/* Use op_array->arg_info[-1] for return type */
		arg_infos = safe_emalloc(sizeof(gear_arg_info), list->children + 1, 0);
		arg_infos->name = NULL;
		arg_infos->pass_by_reference = (op_array->fn_flags & GEAR_ACC_RETURN_REFERENCE) != 0;
		arg_infos->is_variadic = 0;
		arg_infos->type = 0;

		if (return_type_ast->attr & GEAR_TYPE_NULLABLE) {
			allow_null = 1;
			return_type_ast->attr &= ~GEAR_TYPE_NULLABLE;
		}

		gear_compile_typename(return_type_ast, arg_infos, allow_null);

		if (GEAR_TYPE_CODE(arg_infos->type) == IS_VOID && GEAR_TYPE_ALLOW_NULL(arg_infos->type)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Void type cannot be nullable");
		}

		arg_infos++;
		op_array->fn_flags |= GEAR_ACC_HAS_RETURN_TYPE;
	} else {
		if (list->children == 0) {
			return;
		}
		arg_infos = safe_emalloc(sizeof(gear_arg_info), list->children, 0);
	}

	for (i = 0; i < list->children; ++i) {
		gear_ast *param_ast = list->child[i];
		gear_ast *type_ast = param_ast->child[0];
		gear_ast *var_ast = param_ast->child[1];
		gear_ast *default_ast = param_ast->child[2];
		gear_string *name = zval_make_interned_string(gear_ast_get_zval(var_ast));
		gear_bool is_ref = (param_ast->attr & GEAR_PARAM_REF) != 0;
		gear_bool is_variadic = (param_ast->attr & GEAR_PARAM_VARIADIC) != 0;

		znode var_node, default_node;
		gear_uchar opcode;
		gear_op *opline;
		gear_arg_info *arg_info;

		if (gear_is_auto_global(name)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot re-assign auto-global variable %s",
				ZSTR_VAL(name));
		}

		var_node.op_type = IS_CV;
		var_node.u.op.var = lookup_cv(CG(active_op_array), name);

		if (EX_VAR_TO_NUM(var_node.u.op.var) != i) {
			gear_error_noreturn(E_COMPILE_ERROR, "Redefinition of parameter $%s",
				ZSTR_VAL(name));
		} else if (gear_string_equals_literal(name, "this")) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use $this as parameter");
		}

		if (op_array->fn_flags & GEAR_ACC_VARIADIC) {
			gear_error_noreturn(E_COMPILE_ERROR, "Only the last parameter can be variadic");
		}

		if (is_variadic) {
			opcode = GEAR_RECV_VARIADIC;
			default_node.op_type = IS_UNUSED;
			op_array->fn_flags |= GEAR_ACC_VARIADIC;

			if (default_ast) {
				gear_error_noreturn(E_COMPILE_ERROR,
					"Variadic parameter cannot have a default value");
			}
		} else if (default_ast) {
			/* we cannot substitute constants here or it will break ReflectionParameter::getDefaultValueConstantName() and ReflectionParameter::isDefaultValueConstant() */
			uint32_t cops = CG(compiler_options);
			CG(compiler_options) |= GEAR_COMPILE_NO_CONSTANT_SUBSTITUTION | GEAR_COMPILE_NO_PERSISTENT_CONSTANT_SUBSTITUTION;
			opcode = GEAR_RECV_INIT;
			default_node.op_type = IS_CONST;
			gear_const_expr_to_zval(&default_node.u.constant, default_ast);
			CG(compiler_options) = cops;
		} else {
			opcode = GEAR_RECV;
			default_node.op_type = IS_UNUSED;
			op_array->required_num_args = i + 1;
		}

		opline = gear_emit_op(NULL, opcode, NULL, &default_node);
		SET_NODE(opline->result, &var_node);
		opline->op1.num = i + 1;

		arg_info = &arg_infos[i];
		arg_info->name = gear_string_copy(name);
		arg_info->pass_by_reference = is_ref;
		arg_info->is_variadic = is_variadic;
		/* TODO: Keep compatibility, but may be better reset "allow_null" ??? */
		arg_info->type = GEAR_TYPE_ENCODE(0, 1);

		if (type_ast) {
			gear_bool allow_null;
			gear_bool has_null_default = default_ast
				&& (Z_TYPE(default_node.u.constant) == IS_NULL
					|| (Z_TYPE(default_node.u.constant) == IS_CONSTANT_AST
						&& Z_ASTVAL(default_node.u.constant)->kind == GEAR_AST_CONSTANT
						&& strcasecmp(ZSTR_VAL(gear_ast_get_constant_name(Z_ASTVAL(default_node.u.constant))), "NULL") == 0));
			gear_bool is_explicitly_nullable = (type_ast->attr & GEAR_TYPE_NULLABLE) == GEAR_TYPE_NULLABLE;

			op_array->fn_flags |= GEAR_ACC_HAS_TYPE_HINTS;
			allow_null = has_null_default || is_explicitly_nullable;

			type_ast->attr &= ~GEAR_TYPE_NULLABLE;
			gear_compile_typename(type_ast, arg_info, allow_null);

			if (GEAR_TYPE_CODE(arg_info->type) == IS_VOID) {
				gear_error_noreturn(E_COMPILE_ERROR, "void cannot be used as a parameter type");
			}

			if (type_ast->kind == GEAR_AST_TYPE) {
				if (GEAR_TYPE_CODE(arg_info->type) == IS_ARRAY) {
					if (default_ast && !has_null_default
						&& Z_TYPE(default_node.u.constant) != IS_ARRAY
						&& Z_TYPE(default_node.u.constant) != IS_CONSTANT_AST
					) {
						gear_error_noreturn(E_COMPILE_ERROR, "Default value for parameters "
							"with array type can only be an array or NULL");
					}
				} else if (GEAR_TYPE_CODE(arg_info->type) == IS_CALLABLE && default_ast) {
					if (!has_null_default && Z_TYPE(default_node.u.constant) != IS_CONSTANT_AST) {
						gear_error_noreturn(E_COMPILE_ERROR, "Default value for parameters "
							"with callable type can only be NULL");
					}
				}
			} else {
				if (default_ast && !has_null_default && Z_TYPE(default_node.u.constant) != IS_CONSTANT_AST) {
					if (GEAR_TYPE_IS_CLASS(arg_info->type)) {
						gear_error_noreturn(E_COMPILE_ERROR, "Default value for parameters "
							"with a class type can only be NULL");
					} else switch (GEAR_TYPE_CODE(arg_info->type)) {
						case IS_DOUBLE:
							if (Z_TYPE(default_node.u.constant) != IS_DOUBLE && Z_TYPE(default_node.u.constant) != IS_LONG) {
								gear_error_noreturn(E_COMPILE_ERROR, "Default value for parameters "
									"with a float type can only be float, integer, or NULL");
							}
							break;

						case IS_ITERABLE:
							if (Z_TYPE(default_node.u.constant) != IS_ARRAY) {
								gear_error_noreturn(E_COMPILE_ERROR, "Default value for parameters "
									"with iterable type can only be an array or NULL");
							}
							break;

						case IS_OBJECT:
							gear_error_noreturn(E_COMPILE_ERROR, "Default value for parameters "
								"with an object type can only be NULL");
							break;

						default:
							if (!GEAR_SAME_FAKE_TYPE(GEAR_TYPE_CODE(arg_info->type), Z_TYPE(default_node.u.constant))) {
								gear_error_noreturn(E_COMPILE_ERROR, "Default value for parameters "
									"with a %s type can only be %s or NULL",
									gear_get_type_by_const(GEAR_TYPE_CODE(arg_info->type)), gear_get_type_by_const(GEAR_TYPE_CODE(arg_info->type)));
							}
							break;
					}
				}
			}

			/* Allocate cache slot to speed-up run-time class resolution */
			if (opline->opcode == GEAR_RECV_INIT) {
				if (GEAR_TYPE_IS_CLASS(arg_info->type)) {
					opline->extended_value = gear_alloc_cache_slot();
				}
			} else {
				if (GEAR_TYPE_IS_CLASS(arg_info->type)) {
					opline->op2.num = op_array->cache_size;
					op_array->cache_size += sizeof(void*);
				} else {
					opline->op2.num = -1;
				}
			}
		} else {
			if (opline->opcode != GEAR_RECV_INIT) {
				opline->op2.num = -1;
			}
		}
	}

	/* These are assigned at the end to avoid uninitialized memory in case of an error */
	op_array->num_args = list->children;
	op_array->arg_info = arg_infos;

	/* Don't count the variadic argument */
	if (op_array->fn_flags & GEAR_ACC_VARIADIC) {
		op_array->num_args--;
	}
	gear_set_function_arg_flags((gear_function*)op_array);
}
/* }}} */

static void gear_compile_closure_binding(znode *closure, gear_op_array *op_array, gear_ast *uses_ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(uses_ast);
	uint32_t i;

	if (!list->children) {
		return;
	}

	if (!op_array->static_variables) {
		op_array->static_variables = gear_new_array(8);
	}

	if (GC_REFCOUNT(op_array->static_variables) > 1) {
		if (!(GC_FLAGS(op_array->static_variables) & IS_ARRAY_IMMUTABLE)) {
			GC_DELREF(op_array->static_variables);
		}
		op_array->static_variables = gear_array_dup(op_array->static_variables);
	}

	for (i = 0; i < list->children; ++i) {
		gear_ast *var_name_ast = list->child[i];
		gear_string *var_name = zval_make_interned_string(gear_ast_get_zval(var_name_ast));
		uint32_t by_ref = var_name_ast->attr;
		gear_op *opline;
		zval *value;

		if (gear_string_equals_literal(var_name, "this")) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use $this as lexical variable");
		}

		if (gear_is_auto_global(var_name)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use auto-global as lexical variable");
		}

		value = gear_hash_add(op_array->static_variables, var_name, &EG(uninitialized_zval));
		if (!value) {
			gear_error_noreturn(E_COMPILE_ERROR,
				"Cannot use variable $%s twice", ZSTR_VAL(var_name));
		}

		opline = gear_emit_op(NULL, GEAR_BIND_LEXICAL, closure, NULL);
		opline->op2_type = IS_CV;
		opline->op2.var = lookup_cv(CG(active_op_array), var_name);
		opline->extended_value = (uint32_t)((char*)value - (char*)op_array->static_variables->arData) | by_ref;
	}
}
/* }}} */

void gear_compile_closure_uses(gear_ast *ast) /* {{{ */
{
	gear_op_array *op_array = CG(active_op_array);
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t i;

	for (i = 0; i < list->children; ++i) {
		gear_ast *var_ast = list->child[i];
		gear_string *var_name = gear_ast_get_str(var_ast);
		uint32_t by_ref = var_ast->attr;
		zval zv;
		ZVAL_NULL(&zv);

		{
			int i;
			for (i = 0; i < op_array->last_var; i++) {
				if (gear_string_equals(op_array->vars[i], var_name)) {
					gear_error_noreturn(E_COMPILE_ERROR,
						"Cannot use lexical variable $%s as a parameter name", ZSTR_VAL(var_name));
				}
			}
		}

		gear_compile_static_var_common(var_ast, &zv, by_ref);
	}
}
/* }}} */

void gear_begin_method_decl(gear_op_array *op_array, gear_string *name, gear_bool has_body) /* {{{ */
{
	gear_class_entry *ce = CG(active_class_entry);
	gear_bool in_interface = (ce->ce_flags & GEAR_ACC_INTERFACE) != 0;
	gear_bool in_trait = (ce->ce_flags & GEAR_ACC_TRAIT) != 0;
	gear_bool is_public = (op_array->fn_flags & GEAR_ACC_PUBLIC) != 0;
	gear_bool is_static = (op_array->fn_flags & GEAR_ACC_STATIC) != 0;

	gear_string *lcname;

	if (in_interface) {
		if (!is_public || (op_array->fn_flags & (GEAR_ACC_FINAL|GEAR_ACC_ABSTRACT))) {
			gear_error_noreturn(E_COMPILE_ERROR, "Access type for interface method "
				"%s::%s() must be omitted", ZSTR_VAL(ce->name), ZSTR_VAL(name));
		}
		op_array->fn_flags |= GEAR_ACC_ABSTRACT;
	}

	if (op_array->fn_flags & GEAR_ACC_ABSTRACT) {
		if (op_array->fn_flags & GEAR_ACC_PRIVATE) {
			gear_error_noreturn(E_COMPILE_ERROR, "%s function %s::%s() cannot be declared private",
				in_interface ? "Interface" : "Abstract", ZSTR_VAL(ce->name), ZSTR_VAL(name));
		}

		if (has_body) {
			gear_error_noreturn(E_COMPILE_ERROR, "%s function %s::%s() cannot contain body",
				in_interface ? "Interface" : "Abstract", ZSTR_VAL(ce->name), ZSTR_VAL(name));
		}

		ce->ce_flags |= GEAR_ACC_IMPLICIT_ABSTRACT_CLASS;
	} else if (!has_body) {
		gear_error_noreturn(E_COMPILE_ERROR, "Non-abstract method %s::%s() must contain body",
			ZSTR_VAL(ce->name), ZSTR_VAL(name));
	}

	op_array->scope = ce;
	op_array->function_name = gear_string_copy(name);

	lcname = gear_string_tolower(name);
	lcname = gear_new_interned_string(lcname);

	if (gear_hash_add_ptr(&ce->function_table, lcname, op_array) == NULL) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot redeclare %s::%s()",
			ZSTR_VAL(ce->name), ZSTR_VAL(name));
	}

	if (in_interface) {
		if (ZSTR_VAL(lcname)[0] != '_' || ZSTR_VAL(lcname)[1] != '_') {
			/* pass */
		} else if (gear_string_equals_literal(lcname, GEAR_CALL_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __call() must have "
					"public visibility and cannot be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_CALLSTATIC_FUNC_NAME)) {
			if (!is_public || !is_static) {
				gear_error(E_WARNING, "The magic method __callStatic() must have "
					"public visibility and be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_GET_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __get() must have "
					"public visibility and cannot be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_SET_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __set() must have "
					"public visibility and cannot be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_UNSET_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __unset() must have "
					"public visibility and cannot be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_ISSET_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __isset() must have "
					"public visibility and cannot be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_TOSTRING_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __toString() must have "
					"public visibility and cannot be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_INVOKE_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __invoke() must have "
					"public visibility and cannot be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_DEBUGINFO_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __debugInfo() must have "
					"public visibility and cannot be static");
			}
		}
	} else {
		if (!in_trait && gear_string_equals_ci(lcname, ce->name)) {
			if (!ce->constructor) {
				ce->constructor = (gear_function *) op_array;
			}
		} else if (ZSTR_VAL(lcname)[0] != '_' || ZSTR_VAL(lcname)[1] != '_') {
			if (!is_static) {
				op_array->fn_flags |= GEAR_ACC_ALLOW_STATIC;
			}
		} else if (gear_string_equals_literal(lcname, GEAR_CONSTRUCTOR_FUNC_NAME)) {
			ce->constructor = (gear_function *) op_array;
		} else if (gear_string_equals_literal(lcname, GEAR_DESTRUCTOR_FUNC_NAME)) {
			ce->destructor = (gear_function *) op_array;
		} else if (gear_string_equals_literal(lcname, GEAR_CLONE_FUNC_NAME)) {
			ce->clone = (gear_function *) op_array;
		} else if (gear_string_equals_literal(lcname, GEAR_CALL_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __call() must have "
					"public visibility and cannot be static");
			}
			ce->__call = (gear_function *) op_array;
		} else if (gear_string_equals_literal(lcname, GEAR_CALLSTATIC_FUNC_NAME)) {
			if (!is_public || !is_static) {
				gear_error(E_WARNING, "The magic method __callStatic() must have "
					"public visibility and be static");
			}
			ce->__callstatic = (gear_function *) op_array;
		} else if (gear_string_equals_literal(lcname, GEAR_GET_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __get() must have "
					"public visibility and cannot be static");
			}
			ce->__get = (gear_function *) op_array;
			ce->ce_flags |= GEAR_ACC_USE_GUARDS;
		} else if (gear_string_equals_literal(lcname, GEAR_SET_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __set() must have "
					"public visibility and cannot be static");
			}
			ce->__set = (gear_function *) op_array;
			ce->ce_flags |= GEAR_ACC_USE_GUARDS;
		} else if (gear_string_equals_literal(lcname, GEAR_UNSET_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __unset() must have "
					"public visibility and cannot be static");
			}
			ce->__unset = (gear_function *) op_array;
			ce->ce_flags |= GEAR_ACC_USE_GUARDS;
		} else if (gear_string_equals_literal(lcname, GEAR_ISSET_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __isset() must have "
					"public visibility and cannot be static");
			}
			ce->__isset = (gear_function *) op_array;
			ce->ce_flags |= GEAR_ACC_USE_GUARDS;
		} else if (gear_string_equals_literal(lcname, GEAR_TOSTRING_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __toString() must have "
					"public visibility and cannot be static");
			}
			ce->__tostring = (gear_function *) op_array;
		} else if (gear_string_equals_literal(lcname, GEAR_INVOKE_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __invoke() must have "
					"public visibility and cannot be static");
			}
		} else if (gear_string_equals_literal(lcname, GEAR_DEBUGINFO_FUNC_NAME)) {
			if (!is_public || is_static) {
				gear_error(E_WARNING, "The magic method __debugInfo() must have "
					"public visibility and cannot be static");
			}
			ce->__debugInfo = (gear_function *) op_array;
		} else if (!is_static) {
			op_array->fn_flags |= GEAR_ACC_ALLOW_STATIC;
		}
	}

	gear_string_release_ex(lcname, 0);
}
/* }}} */

static void gear_begin_func_decl(znode *result, gear_op_array *op_array, gear_ast_decl *decl) /* {{{ */
{
	gear_ast *params_ast = decl->child[0];
	gear_string *unqualified_name, *name, *lcname, *key;
	gear_op *opline;

	unqualified_name = decl->name;
	op_array->function_name = name = gear_prefix_with_ns(unqualified_name);
	lcname = gear_string_tolower(name);

	if (FC(imports_function)) {
		gear_string *import_name = gear_hash_find_ptr_lc(
			FC(imports_function), ZSTR_VAL(unqualified_name), ZSTR_LEN(unqualified_name));
		if (import_name && !gear_string_equals_ci(lcname, import_name)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot declare function %s "
				"because the name is already in use", ZSTR_VAL(name));
		}
	}

	if (gear_string_equals_literal(lcname, GEAR_AUTOLOAD_FUNC_NAME)) {
		if (gear_ast_get_list(params_ast)->children != 1) {
			gear_error_noreturn(E_COMPILE_ERROR, "%s() must take exactly 1 argument",
				GEAR_AUTOLOAD_FUNC_NAME);
		}

		gear_error(E_DEPRECATED, "__autoload() is deprecated, use spl_autoload_register() instead");
	}

	if (gear_string_equals_literal_ci(unqualified_name, "assert")) {
		gear_error(E_DEPRECATED,
			"Defining a custom assert() function is deprecated, "
			"as the function has special semantics");
	}

	key = gear_build_runtime_definition_key(lcname, decl->lex_pos);
	gear_hash_update_ptr(CG(function_table), key, op_array);
	gear_register_seen_symbol(lcname, GEAR_SYMBOL_FUNCTION);

	if (op_array->fn_flags & GEAR_ACC_CLOSURE) {
		opline = gear_emit_op_tmp(result, GEAR_DECLARE_LAMBDA_FUNCTION, NULL, NULL);
		opline->op1_type = IS_CONST;
		LITERAL_STR(opline->op1, key);
	} else {
		opline = get_next_op(CG(active_op_array));
		opline->opcode = GEAR_DECLARE_FUNCTION;
		opline->op1_type = IS_CONST;
		LITERAL_STR(opline->op1, gear_string_copy(lcname));
		/* RTD key is placed after lcname literal in op1 */
		gear_add_literal_string(CG(active_op_array), &key);
	}

	gear_string_release_ex(lcname, 0);
}
/* }}} */

void gear_compile_func_decl(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast_decl *decl = (gear_ast_decl *) ast;
	gear_ast *params_ast = decl->child[0];
	gear_ast *uses_ast = decl->child[1];
	gear_ast *stmt_ast = decl->child[2];
	gear_ast *return_type_ast = decl->child[3];
	gear_bool is_method = decl->kind == GEAR_AST_METHOD;

	gear_op_array *orig_op_array = CG(active_op_array);
	gear_op_array *op_array = gear_arena_alloc(&CG(arena), sizeof(gear_op_array));
	gear_oparray_context orig_oparray_context;

	init_op_array(op_array, GEAR_USER_FUNCTION, INITIAL_OP_ARRAY_SIZE);

	op_array->fn_flags |= (orig_op_array->fn_flags & GEAR_ACC_STRICT_TYPES);
	op_array->fn_flags |= decl->flags;
	op_array->line_start = decl->start_lineno;
	op_array->line_end = decl->end_lineno;
	if (decl->doc_comment) {
		op_array->doc_comment = gear_string_copy(decl->doc_comment);
	}
	if (decl->kind == GEAR_AST_CLOSURE) {
		op_array->fn_flags |= GEAR_ACC_CLOSURE;
	}

	if (is_method) {
		gear_bool has_body = stmt_ast != NULL;
		gear_begin_method_decl(op_array, decl->name, has_body);
	} else {
		gear_begin_func_decl(result, op_array, decl);
		if (uses_ast) {
			gear_compile_closure_binding(result, op_array, uses_ast);
		}
	}

	CG(active_op_array) = op_array;

	gear_oparray_context_begin(&orig_oparray_context);

	if (CG(compiler_options) & GEAR_COMPILE_EXTENDED_INFO) {
		gear_op *opline_ext = gear_emit_op(NULL, GEAR_EXT_NOP, NULL, NULL);
		opline_ext->lineno = decl->start_lineno;
	}

	{
		/* Push a separator to the loop variable stack */
		gear_loop_var dummy_var;
		dummy_var.opcode = GEAR_RETURN;

		gear_stack_push(&CG(loop_var_stack), (void *) &dummy_var);
	}

	gear_compile_params(params_ast, return_type_ast);
	if (CG(active_op_array)->fn_flags & GEAR_ACC_GENERATOR) {
		gear_mark_function_as_generator();
		gear_emit_op(NULL, GEAR_GENERATOR_CREATE, NULL, NULL);
	}
	if (uses_ast) {
		gear_compile_closure_uses(uses_ast);
	}
	gear_compile_stmt(stmt_ast);

	if (is_method) {
		gear_check_magic_method_implementation(
			CG(active_class_entry), (gear_function *) op_array, E_COMPILE_ERROR);
	}

	/* put the implicit return on the really last line */
	CG(gear_lineno) = decl->end_lineno;

	gear_do_extended_info();
	gear_emit_final_return(0);

	pass_two(CG(active_op_array));
	gear_oparray_context_end(&orig_oparray_context);

	/* Pop the loop variable stack separator */
	gear_stack_del_top(&CG(loop_var_stack));

	CG(active_op_array) = orig_op_array;
}
/* }}} */

void gear_compile_prop_decl(gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t flags = list->attr;
	gear_class_entry *ce = CG(active_class_entry);
	uint32_t i, children = list->children;

	if (ce->ce_flags & GEAR_ACC_INTERFACE) {
		gear_error_noreturn(E_COMPILE_ERROR, "Interfaces may not include member variables");
	}

	if (flags & GEAR_ACC_ABSTRACT) {
		gear_error_noreturn(E_COMPILE_ERROR, "Properties cannot be declared abstract");
	}

	for (i = 0; i < children; ++i) {
		gear_ast *prop_ast = list->child[i];
		gear_ast *name_ast = prop_ast->child[0];
		gear_ast *value_ast = prop_ast->child[1];
		gear_ast *doc_comment_ast = prop_ast->child[2];
		gear_string *name = zval_make_interned_string(gear_ast_get_zval(name_ast));
		gear_string *doc_comment = NULL;
		zval value_zv;

		/* Doc comment has been appended as last element in GEAR_AST_PROP_ELEM ast */
		if (doc_comment_ast) {
			doc_comment = gear_string_copy(gear_ast_get_str(doc_comment_ast));
		}

		if (flags & GEAR_ACC_FINAL) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot declare property %s::$%s final, "
				"the final modifier is allowed only for methods and classes",
				ZSTR_VAL(ce->name), ZSTR_VAL(name));
		}

		if (gear_hash_exists(&ce->properties_info, name)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot redeclare %s::$%s",
				ZSTR_VAL(ce->name), ZSTR_VAL(name));
		}

		if (value_ast) {
			gear_const_expr_to_zval(&value_zv, value_ast);
		} else {
			ZVAL_NULL(&value_zv);
		}

		gear_declare_property_ex(ce, name, &value_zv, flags, doc_comment);
	}
}
/* }}} */

void gear_compile_class_const_decl(gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	gear_class_entry *ce = CG(active_class_entry);
	uint32_t i;

	if ((ce->ce_flags & GEAR_ACC_TRAIT) != 0) {
		gear_error_noreturn(E_COMPILE_ERROR, "Traits cannot have constants");
		return;
	}

	for (i = 0; i < list->children; ++i) {
		gear_ast *const_ast = list->child[i];
		gear_ast *name_ast = const_ast->child[0];
		gear_ast *value_ast = const_ast->child[1];
		gear_ast *doc_comment_ast = const_ast->child[2];
		gear_string *name = zval_make_interned_string(gear_ast_get_zval(name_ast));
		gear_string *doc_comment = doc_comment_ast ? gear_string_copy(gear_ast_get_str(doc_comment_ast)) : NULL;
		zval value_zv;

		if (UNEXPECTED(ast->attr & (GEAR_ACC_STATIC|GEAR_ACC_ABSTRACT|GEAR_ACC_FINAL))) {
			if (ast->attr & GEAR_ACC_STATIC) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot use 'static' as constant modifier");
			} else if (ast->attr & GEAR_ACC_ABSTRACT) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot use 'abstract' as constant modifier");
			} else if (ast->attr & GEAR_ACC_FINAL) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot use 'final' as constant modifier");
			}
		}

		gear_const_expr_to_zval(&value_zv, value_ast);
		gear_declare_class_constant_ex(ce, name, &value_zv, ast->attr, doc_comment);
	}
}
/* }}} */

static void gear_compile_method_ref(gear_ast *ast, gear_trait_method_reference *method_ref) /* {{{ */
{
	gear_ast *class_ast = ast->child[0];
	gear_ast *method_ast = ast->child[1];

	method_ref->method_name = gear_string_copy(gear_ast_get_str(method_ast));

	if (class_ast) {
		method_ref->class_name = gear_resolve_class_name_ast(class_ast);
	} else {
		method_ref->class_name = NULL;
	}
}
/* }}} */

static void gear_compile_trait_precedence(gear_ast *ast) /* {{{ */
{
	gear_ast *method_ref_ast = ast->child[0];
	gear_ast *insteadof_ast = ast->child[1];
	gear_ast_list *insteadof_list = gear_ast_get_list(insteadof_ast);
	uint32_t i;

	gear_trait_precedence *precedence = emalloc(sizeof(gear_trait_precedence) + (insteadof_list->children - 1) * sizeof(gear_string*));
	gear_compile_method_ref(method_ref_ast, &precedence->trait_method);
	precedence->num_excludes = insteadof_list->children;

	for (i = 0; i < insteadof_list->children; ++i) {
		gear_ast *name_ast = insteadof_list->child[i];
		precedence->exclude_class_names[i] = gear_resolve_class_name_ast(name_ast);
	}

	gear_add_to_list(&CG(active_class_entry)->trait_precedences, precedence);
}
/* }}} */

static void gear_compile_trait_alias(gear_ast *ast) /* {{{ */
{
	gear_ast *method_ref_ast = ast->child[0];
	gear_ast *alias_ast = ast->child[1];
	uint32_t modifiers = ast->attr;

	gear_trait_alias *alias;

	if (modifiers == GEAR_ACC_STATIC) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use 'static' as method modifier");
	} else if (modifiers == GEAR_ACC_ABSTRACT) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use 'abstract' as method modifier");
	} else if (modifiers == GEAR_ACC_FINAL) {
		gear_error_noreturn(E_COMPILE_ERROR, "Cannot use 'final' as method modifier");
	}

	alias = emalloc(sizeof(gear_trait_alias));
	gear_compile_method_ref(method_ref_ast, &alias->trait_method);
	alias->modifiers = modifiers;

	if (alias_ast) {
		alias->alias = gear_string_copy(gear_ast_get_str(alias_ast));
	} else {
		alias->alias = NULL;
	}

	gear_add_to_list(&CG(active_class_entry)->trait_aliases, alias);
}
/* }}} */

void gear_compile_use_trait(gear_ast *ast) /* {{{ */
{
	gear_ast_list *traits = gear_ast_get_list(ast->child[0]);
	gear_ast_list *adaptations = ast->child[1] ? gear_ast_get_list(ast->child[1]) : NULL;
	gear_class_entry *ce = CG(active_class_entry);
	gear_op *opline;
	uint32_t i;

	for (i = 0; i < traits->children; ++i) {
		gear_ast *trait_ast = traits->child[i];
		gear_string *name = gear_ast_get_str(trait_ast);

		if (ce->ce_flags & GEAR_ACC_INTERFACE) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use traits inside of interfaces. "
				"%s is used in %s", ZSTR_VAL(name), ZSTR_VAL(ce->name));
		}

		switch (gear_get_class_fetch_type(name)) {
			case GEAR_FETCH_CLASS_SELF:
			case GEAR_FETCH_CLASS_PARENT:
			case GEAR_FETCH_CLASS_STATIC:
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot use '%s' as trait name "
					"as it is reserved", ZSTR_VAL(name));
				break;
		}

		opline = get_next_op(CG(active_op_array));
		opline->opcode = GEAR_ADD_TRAIT;
		SET_NODE(opline->op1, &FC(implementing_class));
		opline->op2_type = IS_CONST;
		opline->op2.constant = gear_add_class_name_literal(CG(active_op_array),
			gear_resolve_class_name_ast(trait_ast));

		ce->num_traits++;
	}

	if (!adaptations) {
		return;
	}

	for (i = 0; i < adaptations->children; ++i) {
		gear_ast *adaptation_ast = adaptations->child[i];
		switch (adaptation_ast->kind) {
			case GEAR_AST_TRAIT_PRECEDENCE:
				gear_compile_trait_precedence(adaptation_ast);
				break;
			case GEAR_AST_TRAIT_ALIAS:
				gear_compile_trait_alias(adaptation_ast);
				break;
			EMPTY_SWITCH_DEFAULT_CASE()
		}
	}
}
/* }}} */

void gear_compile_implements(znode *class_node, gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t i;
	for (i = 0; i < list->children; ++i) {
		gear_ast *class_ast = list->child[i];
		gear_string *name = gear_ast_get_str(class_ast);

		gear_op *opline;

		if (!gear_is_const_default_class_ref(class_ast)) {
			gear_error_noreturn(E_COMPILE_ERROR,
				"Cannot use '%s' as interface name as it is reserved", ZSTR_VAL(name));
		}

		opline = gear_emit_op(NULL, GEAR_ADD_INTERFACE, class_node, NULL);
		opline->op2_type = IS_CONST;
		opline->op2.constant = gear_add_class_name_literal(CG(active_op_array),
			gear_resolve_class_name_ast(class_ast));

		CG(active_class_entry)->num_interfaces++;
	}
}
/* }}} */

static gear_string *gear_generate_anon_class_name(unsigned char *lex_pos) /* {{{ */
{
	gear_string *result;
	char char_pos_buf[32];
	size_t char_pos_len = gear_sprintf(char_pos_buf, "%p", lex_pos);
	gear_string *filename = CG(active_op_array)->filename;

	/* NULL, name length, filename length, last accepting char position length */
	result = gear_string_alloc(sizeof("class@anonymous") + ZSTR_LEN(filename) + char_pos_len, 0);
	sprintf(ZSTR_VAL(result), "class@anonymous%c%s%s", '\0', ZSTR_VAL(filename), char_pos_buf);
	return gear_new_interned_string(result);
}
/* }}} */

void gear_compile_class_decl(gear_ast *ast) /* {{{ */
{
	gear_ast_decl *decl = (gear_ast_decl *) ast;
	gear_ast *extends_ast = decl->child[0];
	gear_ast *implements_ast = decl->child[1];
	gear_ast *stmt_ast = decl->child[2];
	gear_string *name, *lcname;
	gear_class_entry *ce = gear_arena_alloc(&CG(arena), sizeof(gear_class_entry));
	gear_op *opline;
	znode declare_node;
	int extends_const;

	gear_class_entry *original_ce = CG(active_class_entry);
	znode original_implementing_class = FC(implementing_class);

	if (EXPECTED((decl->flags & GEAR_ACC_ANON_CLASS) == 0)) {
		gear_string *unqualified_name = decl->name;

		if (CG(active_class_entry)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Class declarations may not be nested");
		}

		gear_assert_valid_class_name(unqualified_name);
		name = gear_prefix_with_ns(unqualified_name);
		name = gear_new_interned_string(name);
		lcname = gear_string_tolower(name);

		if (FC(imports)) {
			gear_string *import_name = gear_hash_find_ptr_lc(
				FC(imports), ZSTR_VAL(unqualified_name), ZSTR_LEN(unqualified_name));
			if (import_name && !gear_string_equals_ci(lcname, import_name)) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot declare class %s "
						"because the name is already in use", ZSTR_VAL(name));
			}
		}

		gear_register_seen_symbol(lcname, GEAR_SYMBOL_CLASS);
	} else {
		name = gear_generate_anon_class_name(decl->lex_pos);
		lcname = gear_string_tolower(name);
	}
	lcname = gear_new_interned_string(lcname);

	ce->type = GEAR_USER_CLASS;
	ce->name = name;
	gear_initialize_class_data(ce, 1);

	ce->ce_flags |= decl->flags;
	ce->info.user.filename = gear_get_compiled_filename();
	ce->info.user.line_start = decl->start_lineno;
	ce->info.user.line_end = decl->end_lineno;

	if (decl->doc_comment) {
		ce->info.user.doc_comment = gear_string_copy(decl->doc_comment);
	}

	if (UNEXPECTED((decl->flags & GEAR_ACC_ANON_CLASS))) {
		/* Serialization is not supported for anonymous classes */
		ce->serialize = gear_class_serialize_deny;
		ce->unserialize = gear_class_unserialize_deny;
	}

	if (extends_ast) {
		znode extends_node;
		gear_string *extends_name;

		if (!gear_is_const_default_class_ref(extends_ast)) {
			extends_name = gear_ast_get_str(extends_ast);
			gear_error_noreturn(E_COMPILE_ERROR,
				"Cannot use '%s' as class name as it is reserved", ZSTR_VAL(extends_name));
		}

		gear_compile_expr(&extends_node, extends_ast);
		if (extends_node.op_type != IS_CONST || Z_TYPE(extends_node.u.constant) != IS_STRING) {
			gear_error_noreturn(E_COMPILE_ERROR, "Illegal class name");
		}
		extends_name = Z_STR(extends_node.u.constant);
		extends_const = gear_add_class_name_literal(CG(active_op_array),
				gear_resolve_class_name(extends_name,
					extends_ast->kind == GEAR_AST_ZVAL ? extends_ast->attr : GEAR_NAME_FQ));
		gear_string_release_ex(extends_name, 0);
		ce->ce_flags |= GEAR_ACC_INHERITED;
	}

	opline = get_next_op(CG(active_op_array));
	gear_make_var_result(&declare_node, opline);

	GET_NODE(&FC(implementing_class), opline->result);

	opline->op1_type = IS_CONST;
	LITERAL_STR(opline->op1, lcname);

	if (decl->flags & GEAR_ACC_ANON_CLASS) {
		if (extends_ast) {
			opline->opcode = GEAR_DECLARE_ANON_INHERITED_CLASS;
			opline->op2_type = IS_CONST;
			opline->op2.constant = extends_const;
		} else {
			opline->opcode = GEAR_DECLARE_ANON_CLASS;
		}

		if (!gear_hash_exists(CG(class_table), lcname)) {
			gear_hash_add_ptr(CG(class_table), lcname, ce);
		} else {
			/* This anonymous class has been included, reuse the existing definition.
			 * NB: This behavior is buggy, and this should always result in a separate
			 * class declaration. However, until the problem of RTD key collisions is
			 * solved, this gives a behavior close to what is expected. */
			zval zv;
			ZVAL_PTR(&zv, ce);
			destroy_gear_class(&zv);
			ce = gear_hash_find_ptr(CG(class_table), lcname);

			/* Manually replicate emission of necessary inheritance opcodes here. We cannot
			 * reuse the general code, as we only want to emit the opcodes, without modifying
			 * the reused class definition. */
			if (ce->ce_flags & GEAR_ACC_IMPLEMENT_TRAITS) {
				gear_emit_op(NULL, GEAR_BIND_TRAITS, &declare_node, NULL);
			}
			if (implements_ast) {
				gear_ast_list *iface_list = gear_ast_get_list(implements_ast);
				uint32_t i;
				for (i = 0; i < iface_list->children; i++) {
					opline = gear_emit_op(NULL, GEAR_ADD_INTERFACE, &declare_node, NULL);
					opline->op2_type = IS_CONST;
					opline->op2.constant = gear_add_class_name_literal(CG(active_op_array),
						gear_resolve_class_name_ast(iface_list->child[i]));
				}
				gear_emit_op(NULL, GEAR_VERIFY_ABSTRACT_CLASS, &declare_node, NULL);
			}
			return;
		}
	} else {
		gear_string *key;

		if (extends_ast) {
			opline->opcode = GEAR_DECLARE_INHERITED_CLASS;
			opline->op2_type = IS_CONST;
			opline->op2.constant = extends_const;
		} else {
			opline->opcode = GEAR_DECLARE_CLASS;
		}

		key = gear_build_runtime_definition_key(lcname, decl->lex_pos);
		/* RTD key is placed after lcname literal in op1 */
		gear_add_literal_string(CG(active_op_array), &key);

		gear_hash_update_ptr(CG(class_table), key, ce);
	}

	CG(active_class_entry) = ce;

	gear_compile_stmt(stmt_ast);

	/* Reset lineno for final opcodes and errors */
	CG(gear_lineno) = ast->lineno;

	if (ce->num_traits == 0) {
		/* For traits this check is delayed until after trait binding */
		gear_check_deprecated_constructor(ce);
	}

	if (ce->constructor) {
		ce->constructor->common.fn_flags |= GEAR_ACC_CTOR;
		if (ce->constructor->common.fn_flags & GEAR_ACC_STATIC) {
			gear_error_noreturn(E_COMPILE_ERROR, "Constructor %s::%s() cannot be static",
				ZSTR_VAL(ce->name), ZSTR_VAL(ce->constructor->common.function_name));
		}
		if (ce->constructor->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
			gear_error_noreturn(E_COMPILE_ERROR,
				"Constructor %s::%s() cannot declare a return type",
				ZSTR_VAL(ce->name), ZSTR_VAL(ce->constructor->common.function_name));
		}
	}
	if (ce->destructor) {
		ce->destructor->common.fn_flags |= GEAR_ACC_DTOR;
		if (ce->destructor->common.fn_flags & GEAR_ACC_STATIC) {
			gear_error_noreturn(E_COMPILE_ERROR, "Destructor %s::%s() cannot be static",
				ZSTR_VAL(ce->name), ZSTR_VAL(ce->destructor->common.function_name));
		} else if (ce->destructor->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
			gear_error_noreturn(E_COMPILE_ERROR,
				"Destructor %s::%s() cannot declare a return type",
				ZSTR_VAL(ce->name), ZSTR_VAL(ce->destructor->common.function_name));
		}
	}
	if (ce->clone) {
		if (ce->clone->common.fn_flags & GEAR_ACC_STATIC) {
			gear_error_noreturn(E_COMPILE_ERROR, "Clone method %s::%s() cannot be static",
				ZSTR_VAL(ce->name), ZSTR_VAL(ce->clone->common.function_name));
		} else if (ce->clone->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
			gear_error_noreturn(E_COMPILE_ERROR,
				"%s::%s() cannot declare a return type",
				ZSTR_VAL(ce->name), ZSTR_VAL(ce->clone->common.function_name));
		}
	}

	/* Check for traits and proceed like with interfaces.
	 * The only difference will be a combined handling of them in the end.
	 * Thus, we need another opcode here. */
	if (ce->num_traits > 0) {
		ce->traits = NULL;
		ce->num_traits = 0;
		ce->ce_flags |= GEAR_ACC_IMPLEMENT_TRAITS;

		gear_emit_op(NULL, GEAR_BIND_TRAITS, &declare_node, NULL);
	}

	if (implements_ast) {
		gear_compile_implements(&declare_node, implements_ast);
	}

	if (!(ce->ce_flags & (GEAR_ACC_INTERFACE|GEAR_ACC_EXPLICIT_ABSTRACT_CLASS))
		&& (extends_ast || implements_ast)
	) {
		gear_verify_abstract_class(ce);
		if (implements_ast) {
			gear_emit_op(NULL, GEAR_VERIFY_ABSTRACT_CLASS, &declare_node, NULL);
		}
	}

	/* Inherit interfaces; reset number to zero, we need it for above check and
	 * will restore it during actual implementation.
	 * The GEAR_ACC_IMPLEMENT_INTERFACES flag disables double call to
	 * gear_verify_abstract_class() */
	if (ce->num_interfaces > 0) {
		ce->interfaces = NULL;
		ce->num_interfaces = 0;
		ce->ce_flags |= GEAR_ACC_IMPLEMENT_INTERFACES;
	}

	FC(implementing_class) = original_implementing_class;
	CG(active_class_entry) = original_ce;
}
/* }}} */

static HashTable *gear_get_import_ht(uint32_t type) /* {{{ */
{
	switch (type) {
		case GEAR_SYMBOL_CLASS:
			if (!FC(imports)) {
				FC(imports) = emalloc(sizeof(HashTable));
				gear_hash_init(FC(imports), 8, NULL, str_dtor, 0);
			}
			return FC(imports);
		case GEAR_SYMBOL_FUNCTION:
			if (!FC(imports_function)) {
				FC(imports_function) = emalloc(sizeof(HashTable));
				gear_hash_init(FC(imports_function), 8, NULL, str_dtor, 0);
			}
			return FC(imports_function);
		case GEAR_SYMBOL_CONST:
			if (!FC(imports_const)) {
				FC(imports_const) = emalloc(sizeof(HashTable));
				gear_hash_init(FC(imports_const), 8, NULL, str_dtor, 0);
			}
			return FC(imports_const);
		EMPTY_SWITCH_DEFAULT_CASE()
	}

	return NULL;
}
/* }}} */

static char *gear_get_use_type_str(uint32_t type) /* {{{ */
{
	switch (type) {
		case GEAR_SYMBOL_CLASS:
			return "";
		case GEAR_SYMBOL_FUNCTION:
			return " function";
		case GEAR_SYMBOL_CONST:
			return " const";
		EMPTY_SWITCH_DEFAULT_CASE()
	}

	return " unknown";
}
/* }}} */

static void gear_check_already_in_use(uint32_t type, gear_string *old_name, gear_string *new_name, gear_string *check_name) /* {{{ */
{
	if (gear_string_equals_ci(old_name, check_name)) {
		return;
	}

	gear_error_noreturn(E_COMPILE_ERROR, "Cannot use%s %s as %s because the name "
		"is already in use", gear_get_use_type_str(type), ZSTR_VAL(old_name), ZSTR_VAL(new_name));
}
/* }}} */

void gear_compile_use(gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t i;
	gear_string *current_ns = FC(current_namespace);
	uint32_t type = ast->attr;
	HashTable *current_import = gear_get_import_ht(type);
	gear_bool case_sensitive = type == GEAR_SYMBOL_CONST;

	for (i = 0; i < list->children; ++i) {
		gear_ast *use_ast = list->child[i];
		gear_ast *old_name_ast = use_ast->child[0];
		gear_ast *new_name_ast = use_ast->child[1];
		gear_string *old_name = gear_ast_get_str(old_name_ast);
		gear_string *new_name, *lookup_name;

		if (new_name_ast) {
			new_name = gear_string_copy(gear_ast_get_str(new_name_ast));
		} else {
			const char *unqualified_name;
			size_t unqualified_name_len;
			if (gear_get_unqualified_name(old_name, &unqualified_name, &unqualified_name_len)) {
				/* The form "use A\B" is equivalent to "use A\B as B" */
				new_name = gear_string_init(unqualified_name, unqualified_name_len, 0);
			} else {
				new_name = gear_string_copy(old_name);

				if (!current_ns) {
					if (type == T_CLASS && gear_string_equals_literal(new_name, "strict")) {
						gear_error_noreturn(E_COMPILE_ERROR,
							"You seem to be trying to use a different language...");
					}

					gear_error(E_WARNING, "The use statement with non-compound name '%s' "
						"has no effect", ZSTR_VAL(new_name));
				}
			}
		}

		if (case_sensitive) {
			lookup_name = gear_string_copy(new_name);
		} else {
			lookup_name = gear_string_tolower(new_name);
		}

		if (type == GEAR_SYMBOL_CLASS && gear_is_reserved_class_name(new_name)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use %s as %s because '%s' "
				"is a special class name", ZSTR_VAL(old_name), ZSTR_VAL(new_name), ZSTR_VAL(new_name));
		}

		if (current_ns) {
			gear_string *ns_name = gear_string_alloc(ZSTR_LEN(current_ns) + 1 + ZSTR_LEN(new_name), 0);
			gear_str_tolower_copy(ZSTR_VAL(ns_name), ZSTR_VAL(current_ns), ZSTR_LEN(current_ns));
			ZSTR_VAL(ns_name)[ZSTR_LEN(current_ns)] = '\\';
			memcpy(ZSTR_VAL(ns_name) + ZSTR_LEN(current_ns) + 1, ZSTR_VAL(lookup_name), ZSTR_LEN(lookup_name) + 1);

			if (gear_have_seen_symbol(ns_name, type)) {
				gear_check_already_in_use(type, old_name, new_name, ns_name);
			}

			gear_string_efree(ns_name);
		} else {
			if (gear_have_seen_symbol(lookup_name, type)) {
				gear_check_already_in_use(type, old_name, new_name, lookup_name);
			}
		}

		gear_string_addref(old_name);
		if (!gear_hash_add_ptr(current_import, lookup_name, old_name)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use%s %s as %s because the name "
				"is already in use", gear_get_use_type_str(type), ZSTR_VAL(old_name), ZSTR_VAL(new_name));
		}

		gear_string_release_ex(lookup_name, 0);
		gear_string_release_ex(new_name, 0);
	}
}
/* }}} */

void gear_compile_group_use(gear_ast *ast) /* {{{ */
{
	uint32_t i;
	gear_string *ns = gear_ast_get_str(ast->child[0]);
	gear_ast_list *list = gear_ast_get_list(ast->child[1]);

	for (i = 0; i < list->children; i++) {
		gear_ast *inline_use, *use = list->child[i];
		zval *name_zval = gear_ast_get_zval(use->child[0]);
		gear_string *name = Z_STR_P(name_zval);
		gear_string *compound_ns = gear_concat_names(ZSTR_VAL(ns), ZSTR_LEN(ns), ZSTR_VAL(name), ZSTR_LEN(name));
		gear_string_release_ex(name, 0);
		ZVAL_STR(name_zval, compound_ns);
		inline_use = gear_ast_create_list(1, GEAR_AST_USE, use);
		inline_use->attr = ast->attr ? ast->attr : use->attr;
		gear_compile_use(inline_use);
	}
}
/* }}} */

void gear_compile_const_decl(gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	uint32_t i;
	for (i = 0; i < list->children; ++i) {
		gear_ast *const_ast = list->child[i];
		gear_ast *name_ast = const_ast->child[0];
		gear_ast *value_ast = const_ast->child[1];
		gear_string *unqualified_name = gear_ast_get_str(name_ast);

		gear_string *name;
		znode name_node, value_node;
		zval *value_zv = &value_node.u.constant;

		value_node.op_type = IS_CONST;
		gear_const_expr_to_zval(value_zv, value_ast);

		if (gear_lookup_reserved_const(ZSTR_VAL(unqualified_name), ZSTR_LEN(unqualified_name))) {
			gear_error_noreturn(E_COMPILE_ERROR,
				"Cannot redeclare constant '%s'", ZSTR_VAL(unqualified_name));
		}

		name = gear_prefix_with_ns(unqualified_name);
		name = gear_new_interned_string(name);

		if (FC(imports_const)) {
			gear_string *import_name = gear_hash_find_ptr(FC(imports_const), unqualified_name);
			if (import_name && !gear_string_equals(import_name, name)) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot declare const %s because "
					"the name is already in use", ZSTR_VAL(name));
			}
		}

		name_node.op_type = IS_CONST;
		ZVAL_STR(&name_node.u.constant, name);

		gear_emit_op(NULL, GEAR_DECLARE_CONST, &name_node, &value_node);

		gear_register_seen_symbol(name, GEAR_SYMBOL_CONST);
	}
}
/* }}}*/

void gear_compile_namespace(gear_ast *ast) /* {{{ */
{
	gear_ast *name_ast = ast->child[0];
	gear_ast *stmt_ast = ast->child[1];
	gear_string *name;
	gear_bool with_bracket = stmt_ast != NULL;

	/* handle mixed syntax declaration or nested namespaces */
	if (!FC(has_bracketed_namespaces)) {
		if (FC(current_namespace)) {
			/* previous namespace declarations were unbracketed */
			if (with_bracket) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot mix bracketed namespace declarations "
					"with unbracketed namespace declarations");
			}
		}
	} else {
		/* previous namespace declarations were bracketed */
		if (!with_bracket) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot mix bracketed namespace declarations "
				"with unbracketed namespace declarations");
		} else if (FC(current_namespace) || FC(in_namespace)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Namespace declarations cannot be nested");
		}
	}

	if (((!with_bracket && !FC(current_namespace))
		 || (with_bracket && !FC(has_bracketed_namespaces))) && CG(active_op_array)->last > 0
	) {
		/* ignore GEAR_EXT_STMT and GEAR_TICKS */
		uint32_t num = CG(active_op_array)->last;
		while (num > 0 &&
		       (CG(active_op_array)->opcodes[num-1].opcode == GEAR_EXT_STMT ||
		        CG(active_op_array)->opcodes[num-1].opcode == GEAR_TICKS)) {
			--num;
		}
		if (num > 0) {
			gear_error_noreturn(E_COMPILE_ERROR, "Namespace declaration statement has to be "
				"the very first statement or after any declare call in the script");
		}
	}

	if (FC(current_namespace)) {
		gear_string_release_ex(FC(current_namespace), 0);
	}

	if (name_ast) {
		name = gear_ast_get_str(name_ast);

		if (GEAR_FETCH_CLASS_DEFAULT != gear_get_class_fetch_type(name)) {
			gear_error_noreturn(E_COMPILE_ERROR, "Cannot use '%s' as namespace name", ZSTR_VAL(name));
		}

		FC(current_namespace) = gear_string_copy(name);
	} else {
		FC(current_namespace) = NULL;
	}

	gear_reset_import_tables();

	FC(in_namespace) = 1;
	if (with_bracket) {
		FC(has_bracketed_namespaces) = 1;
	}

	if (stmt_ast) {
		gear_compile_top_stmt(stmt_ast);
		gear_end_namespace();
	}
}
/* }}} */

void gear_compile_halt_compiler(gear_ast *ast) /* {{{ */
{
	gear_ast *offset_ast = ast->child[0];
	gear_long offset = Z_LVAL_P(gear_ast_get_zval(offset_ast));

	gear_string *filename, *name;
	const char const_name[] = "__COMPILER_HALT_OFFSET__";

	if (FC(has_bracketed_namespaces) && FC(in_namespace)) {
		gear_error_noreturn(E_COMPILE_ERROR,
			"__HALT_COMPILER() can only be used from the outermost scope");
	}

	filename = gear_get_compiled_filename();
	name = gear_mangle_property_name(const_name, sizeof(const_name) - 1,
		ZSTR_VAL(filename), ZSTR_LEN(filename), 0);

	gear_register_long_constant(ZSTR_VAL(name), ZSTR_LEN(name), offset, CONST_CS, 0);
	gear_string_release_ex(name, 0);
}
/* }}} */

static gear_bool gear_try_ct_eval_magic_const(zval *zv, gear_ast *ast) /* {{{ */
{
	gear_op_array *op_array = CG(active_op_array);
	gear_class_entry *ce = CG(active_class_entry);

	switch (ast->attr) {
		case T_LINE:
			ZVAL_LONG(zv, ast->lineno);
			break;
		case T_FILE:
			ZVAL_STR_COPY(zv, CG(compiled_filename));
			break;
		case T_DIR:
		{
			gear_string *filename = CG(compiled_filename);
			gear_string *dirname = gear_string_init(ZSTR_VAL(filename), ZSTR_LEN(filename), 0);
#ifdef GEAR_WIN32
			ZSTR_LEN(dirname) = hyss_win32_ioutil_dirname(ZSTR_VAL(dirname), ZSTR_LEN(dirname));
#else
			ZSTR_LEN(dirname) = gear_dirname(ZSTR_VAL(dirname), ZSTR_LEN(dirname));
#endif

			if (strcmp(ZSTR_VAL(dirname), ".") == 0) {
				dirname = gear_string_extend(dirname, MAXPATHLEN, 0);
#if HAVE_GETCWD
				GEAR_IGNORE_VALUE(VCWD_GETCWD(ZSTR_VAL(dirname), MAXPATHLEN));
#elif HAVE_GETWD
				GEAR_IGNORE_VALUE(VCWD_GETWD(ZSTR_VAL(dirname)));
#endif
				ZSTR_LEN(dirname) = strlen(ZSTR_VAL(dirname));
			}

			ZVAL_STR(zv, dirname);
			break;
		}
		case T_FUNC_C:
			if (op_array && op_array->function_name) {
				ZVAL_STR_COPY(zv, op_array->function_name);
			} else {
				ZVAL_EMPTY_STRING(zv);
			}
			break;
		case T_METHOD_C:
			if ((op_array && !op_array->scope && op_array->function_name) || (op_array->fn_flags & GEAR_ACC_CLOSURE)) {
				ZVAL_STR_COPY(zv, op_array->function_name);
			} else if (ce) {
				if (op_array && op_array->function_name) {
					ZVAL_NEW_STR(zv, gear_concat3(ZSTR_VAL(ce->name), ZSTR_LEN(ce->name), "::", 2,
						ZSTR_VAL(op_array->function_name), ZSTR_LEN(op_array->function_name)));
				} else {
					ZVAL_STR_COPY(zv, ce->name);
				}
			} else if (op_array && op_array->function_name) {
				ZVAL_STR_COPY(zv, op_array->function_name);
			} else {
				ZVAL_EMPTY_STRING(zv);
			}
			break;
		case T_CLASS_C:
			if (ce) {
				if ((ce->ce_flags & GEAR_ACC_TRAIT) != 0) {
					return 0;
				} else {
					ZVAL_STR_COPY(zv, ce->name);
				}
			} else {
				ZVAL_EMPTY_STRING(zv);
			}
			break;
		case T_TRAIT_C:
			if (ce && (ce->ce_flags & GEAR_ACC_TRAIT) != 0) {
				ZVAL_STR_COPY(zv, ce->name);
			} else {
				ZVAL_EMPTY_STRING(zv);
			}
			break;
		case T_NS_C:
			if (FC(current_namespace)) {
				ZVAL_STR_COPY(zv, FC(current_namespace));
			} else {
				ZVAL_EMPTY_STRING(zv);
			}
			break;
		EMPTY_SWITCH_DEFAULT_CASE()
	}

	return 1;
}
/* }}} */

GEAR_API gear_bool gear_binary_op_produces_numeric_string_error(uint32_t opcode, zval *op1, zval *op2) /* {{{ */
{
	if (!(opcode == GEAR_ADD || opcode == GEAR_SUB || opcode == GEAR_MUL || opcode == GEAR_DIV
		|| opcode == GEAR_POW || opcode == GEAR_MOD || opcode == GEAR_SL || opcode == GEAR_SR
		|| opcode == GEAR_BW_OR || opcode == GEAR_BW_AND || opcode == GEAR_BW_XOR)) {
		return 0;
	}

	/* While basic arithmetic operators always produce numeric string errors,
	 * bitwise operators don't produce errors if both operands are strings */
	if ((opcode == GEAR_BW_OR || opcode == GEAR_BW_AND || opcode == GEAR_BW_XOR)
		&& Z_TYPE_P(op1) == IS_STRING && Z_TYPE_P(op2) == IS_STRING) {
		return 0;
	}

	if (Z_TYPE_P(op1) == IS_STRING
		&& !is_numeric_string(Z_STRVAL_P(op1), Z_STRLEN_P(op1), NULL, NULL, 0)) {
		return 1;
	}

	if (Z_TYPE_P(op2) == IS_STRING
		&& !is_numeric_string(Z_STRVAL_P(op2), Z_STRLEN_P(op2), NULL, NULL, 0)) {
		return 1;
	}

	return 0;
}
/* }}} */

static inline gear_bool gear_try_ct_eval_binary_op(zval *result, uint32_t opcode, zval *op1, zval *op2) /* {{{ */
{
	binary_op_type fn = get_binary_op(opcode);

	/* don't evaluate division by zero at compile-time */
	if ((opcode == GEAR_DIV || opcode == GEAR_MOD) &&
	    zval_get_long(op2) == 0) {
		return 0;
	} else if ((opcode == GEAR_SL || opcode == GEAR_SR) &&
	    zval_get_long(op2) < 0) {
		return 0;
	}

	/* don't evaluate numeric string error-producing operations at compile-time */
	if (gear_binary_op_produces_numeric_string_error(opcode, op1, op2)) {
		return 0;
	}

	fn(result, op1, op2);
	return 1;
}
/* }}} */

static inline void gear_ct_eval_unary_op(zval *result, uint32_t opcode, zval *op) /* {{{ */
{
	unary_op_type fn = get_unary_op(opcode);
	fn(result, op);
}
/* }}} */

static inline gear_bool gear_try_ct_eval_unary_pm(zval *result, gear_ast_kind kind, zval *op) /* {{{ */
{
	zval left;
	ZVAL_LONG(&left, (kind == GEAR_AST_UNARY_PLUS) ? 1 : -1);
	return gear_try_ct_eval_binary_op(result, GEAR_MUL, &left, op);
}
/* }}} */

static inline void gear_ct_eval_greater(zval *result, gear_ast_kind kind, zval *op1, zval *op2) /* {{{ */
{
	binary_op_type fn = kind == GEAR_AST_GREATER
		? is_smaller_function : is_smaller_or_equal_function;
	fn(result, op2, op1);
}
/* }}} */

static gear_bool gear_try_ct_eval_array(zval *result, gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	gear_ast *last_elem_ast = NULL;
	uint32_t i;
	gear_bool is_constant = 1;

	if (ast->attr == GEAR_ARRAY_SYNTAX_LIST) {
		gear_error(E_COMPILE_ERROR, "Cannot use list() as standalone expression");
	}

	/* First ensure that *all* child nodes are constant and by-val */
	for (i = 0; i < list->children; ++i) {
		gear_ast *elem_ast = list->child[i];

		if (elem_ast == NULL) {
			/* Report error at line of last non-empty element */
			if (last_elem_ast) {
				CG(gear_lineno) = gear_ast_get_lineno(last_elem_ast);
			}
			gear_error(E_COMPILE_ERROR, "Cannot use empty array elements in arrays");
		}

		gear_eval_const_expr(&elem_ast->child[0]);
		gear_eval_const_expr(&elem_ast->child[1]);

		if (elem_ast->attr /* by_ref */ || elem_ast->child[0]->kind != GEAR_AST_ZVAL
			|| (elem_ast->child[1] && elem_ast->child[1]->kind != GEAR_AST_ZVAL)
		) {
			is_constant = 0;
		}

		last_elem_ast = elem_ast;
	}

	if (!is_constant) {
		return 0;
	}

	if (!list->children) {
		ZVAL_EMPTY_ARRAY(result);
		return 1;
	}

	array_init_size(result, list->children);
	for (i = 0; i < list->children; ++i) {
		gear_ast *elem_ast = list->child[i];
		gear_ast *value_ast = elem_ast->child[0];
		gear_ast *key_ast = elem_ast->child[1];

		zval *value = gear_ast_get_zval(value_ast);
		Z_TRY_ADDREF_P(value);

		if (key_ast) {
			zval *key = gear_ast_get_zval(key_ast);
			switch (Z_TYPE_P(key)) {
				case IS_LONG:
					gear_hash_index_update(Z_ARRVAL_P(result), Z_LVAL_P(key), value);
					break;
				case IS_STRING:
					gear_symtable_update(Z_ARRVAL_P(result), Z_STR_P(key), value);
					break;
				case IS_DOUBLE:
					gear_hash_index_update(Z_ARRVAL_P(result),
						gear_dval_to_lval(Z_DVAL_P(key)), value);
					break;
				case IS_FALSE:
					gear_hash_index_update(Z_ARRVAL_P(result), 0, value);
					break;
				case IS_TRUE:
					gear_hash_index_update(Z_ARRVAL_P(result), 1, value);
					break;
				case IS_NULL:
					gear_hash_update(Z_ARRVAL_P(result), ZSTR_EMPTY_ALLOC(), value);
					break;
				default:
					gear_error_noreturn(E_COMPILE_ERROR, "Illegal offset type");
					break;
			}
		} else {
			if (!gear_hash_next_index_insert(Z_ARRVAL_P(result), value)) {
				zval_ptr_dtor_nogc(value);
				zval_ptr_dtor(result);
				return 0;
			}
		}
	}

	return 1;
}
/* }}} */

void gear_compile_binary_op(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *left_ast = ast->child[0];
	gear_ast *right_ast = ast->child[1];
	uint32_t opcode = ast->attr;

	znode left_node, right_node;
	gear_compile_expr(&left_node, left_ast);
	gear_compile_expr(&right_node, right_ast);

	if (left_node.op_type == IS_CONST && right_node.op_type == IS_CONST) {
		if (gear_try_ct_eval_binary_op(&result->u.constant, opcode,
				&left_node.u.constant, &right_node.u.constant)
		) {
			result->op_type = IS_CONST;
			zval_ptr_dtor(&left_node.u.constant);
			zval_ptr_dtor(&right_node.u.constant);
			return;
		}
	}

	do {
		if (opcode == GEAR_IS_EQUAL || opcode == GEAR_IS_NOT_EQUAL) {
			if (left_node.op_type == IS_CONST) {
				if (Z_TYPE(left_node.u.constant) == IS_FALSE) {
					opcode = (opcode == GEAR_IS_NOT_EQUAL) ? GEAR_BOOL : GEAR_BOOL_NOT;
					gear_emit_op_tmp(result, opcode, &right_node, NULL);
					break;
				} else if (Z_TYPE(left_node.u.constant) == IS_TRUE) {
					opcode = (opcode == GEAR_IS_EQUAL) ? GEAR_BOOL : GEAR_BOOL_NOT;
					gear_emit_op_tmp(result, opcode, &right_node, NULL);
					break;
				}
			} else if (right_node.op_type == IS_CONST) {
				if (Z_TYPE(right_node.u.constant) == IS_FALSE) {
					opcode = (opcode == GEAR_IS_NOT_EQUAL) ? GEAR_BOOL : GEAR_BOOL_NOT;
					gear_emit_op_tmp(result, opcode, &left_node, NULL);
					break;
				} else if (Z_TYPE(right_node.u.constant) == IS_TRUE) {
					opcode = (opcode == GEAR_IS_EQUAL) ? GEAR_BOOL : GEAR_BOOL_NOT;
					gear_emit_op_tmp(result, opcode, &left_node, NULL);
					break;
				}
			}
		}
		if (opcode == GEAR_CONCAT) {
			/* convert constant operands to strings at compile-time */
			if (left_node.op_type == IS_CONST) {
				convert_to_string(&left_node.u.constant);
			}
			if (right_node.op_type == IS_CONST) {
				convert_to_string(&right_node.u.constant);
			}
			if (left_node.op_type == IS_CONST && right_node.op_type == IS_CONST) {
				opcode = GEAR_FAST_CONCAT;
			}
		}
		gear_emit_op_tmp(result, opcode, &left_node, &right_node);
	} while (0);
}
/* }}} */

/* We do not use gear_compile_binary_op for this because we want to retain the left-to-right
 * evaluation order. */
void gear_compile_greater(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *left_ast = ast->child[0];
	gear_ast *right_ast = ast->child[1];
	znode left_node, right_node;

	GEAR_ASSERT(ast->kind == GEAR_AST_GREATER || ast->kind == GEAR_AST_GREATER_EQUAL);

	gear_compile_expr(&left_node, left_ast);
	gear_compile_expr(&right_node, right_ast);

	if (left_node.op_type == IS_CONST && right_node.op_type == IS_CONST) {
		result->op_type = IS_CONST;
		gear_ct_eval_greater(&result->u.constant, ast->kind,
			&left_node.u.constant, &right_node.u.constant);
		zval_ptr_dtor(&left_node.u.constant);
		zval_ptr_dtor(&right_node.u.constant);
		return;
	}

	gear_emit_op_tmp(result,
		ast->kind == GEAR_AST_GREATER ? GEAR_IS_SMALLER : GEAR_IS_SMALLER_OR_EQUAL,
		&right_node, &left_node);
}
/* }}} */

void gear_compile_unary_op(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	uint32_t opcode = ast->attr;

	znode expr_node;
	gear_compile_expr(&expr_node, expr_ast);

	if (expr_node.op_type == IS_CONST) {
		result->op_type = IS_CONST;
		gear_ct_eval_unary_op(&result->u.constant, opcode,
			&expr_node.u.constant);
		zval_ptr_dtor(&expr_node.u.constant);
		return;
	}

	gear_emit_op_tmp(result, opcode, &expr_node, NULL);
}
/* }}} */

void gear_compile_unary_pm(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	znode expr_node;
	znode lefthand_node;

	GEAR_ASSERT(ast->kind == GEAR_AST_UNARY_PLUS || ast->kind == GEAR_AST_UNARY_MINUS);

	gear_compile_expr(&expr_node, expr_ast);

	if (expr_node.op_type == IS_CONST) {
		if (gear_try_ct_eval_unary_pm(&result->u.constant, ast->kind, &expr_node.u.constant)) {
			result->op_type = IS_CONST;
			zval_ptr_dtor(&expr_node.u.constant);
			return;
		}
	}

	lefthand_node.op_type = IS_CONST;
	ZVAL_LONG(&lefthand_node.u.constant, (ast->kind == GEAR_AST_UNARY_PLUS) ? 1 : -1);
	gear_emit_op_tmp(result, GEAR_MUL, &lefthand_node, &expr_node);
}
/* }}} */

void gear_compile_short_circuiting(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *left_ast = ast->child[0];
	gear_ast *right_ast = ast->child[1];

	znode left_node, right_node;
	gear_op *opline_jmpz, *opline_bool;
	uint32_t opnum_jmpz;

	GEAR_ASSERT(ast->kind == GEAR_AST_AND || ast->kind == GEAR_AST_OR);

	gear_compile_expr(&left_node, left_ast);

	if (left_node.op_type == IS_CONST) {
		if ((ast->kind == GEAR_AST_AND && !gear_is_true(&left_node.u.constant))
		 || (ast->kind == GEAR_AST_OR && gear_is_true(&left_node.u.constant))) {
			result->op_type = IS_CONST;
			ZVAL_BOOL(&result->u.constant, gear_is_true(&left_node.u.constant));
		} else {
			gear_compile_expr(&right_node, right_ast);

			if (right_node.op_type == IS_CONST) {
				result->op_type = IS_CONST;
				ZVAL_BOOL(&result->u.constant, gear_is_true(&right_node.u.constant));

				zval_ptr_dtor(&right_node.u.constant);
			} else {
				gear_emit_op_tmp(result, GEAR_BOOL, &right_node, NULL);
			}
		}

		zval_ptr_dtor(&left_node.u.constant);
		return;
	}

	opnum_jmpz = get_next_op_number(CG(active_op_array));
	opline_jmpz = gear_emit_op(NULL, ast->kind == GEAR_AST_AND ? GEAR_JMPZ_EX : GEAR_JMPNZ_EX,
		&left_node, NULL);

	if (left_node.op_type == IS_TMP_VAR) {
		SET_NODE(opline_jmpz->result, &left_node);
	} else {
		opline_jmpz->result.var = get_temporary_variable(CG(active_op_array));
		opline_jmpz->result_type = IS_TMP_VAR;
	}

	GET_NODE(result, opline_jmpz->result);
	gear_compile_expr(&right_node, right_ast);

	opline_bool = gear_emit_op(NULL, GEAR_BOOL, &right_node, NULL);
	SET_NODE(opline_bool->result, result);

	gear_update_jump_target_to_next(opnum_jmpz);
}
/* }}} */

void gear_compile_post_incdec(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];
	GEAR_ASSERT(ast->kind == GEAR_AST_POST_INC || ast->kind == GEAR_AST_POST_DEC);

	gear_ensure_writable_variable(var_ast);

	if (var_ast->kind == GEAR_AST_PROP) {
		gear_op *opline = gear_compile_prop(NULL, var_ast, BP_VAR_RW);
		opline->opcode = ast->kind == GEAR_AST_POST_INC ? GEAR_POST_INC_OBJ : GEAR_POST_DEC_OBJ;
		gear_make_tmp_result(result, opline);
	} else {
		znode var_node;
		gear_compile_var(&var_node, var_ast, BP_VAR_RW);
		gear_emit_op_tmp(result, ast->kind == GEAR_AST_POST_INC ? GEAR_POST_INC : GEAR_POST_DEC,
			&var_node, NULL);
	}
}
/* }}} */

void gear_compile_pre_incdec(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];
	GEAR_ASSERT(ast->kind == GEAR_AST_PRE_INC || ast->kind == GEAR_AST_PRE_DEC);

	gear_ensure_writable_variable(var_ast);

	if (var_ast->kind == GEAR_AST_PROP) {
		gear_op *opline = gear_compile_prop(result, var_ast, BP_VAR_RW);
		opline->opcode = ast->kind == GEAR_AST_PRE_INC ? GEAR_PRE_INC_OBJ : GEAR_PRE_DEC_OBJ;
	} else {
		znode var_node;
		gear_compile_var(&var_node, var_ast, BP_VAR_RW);
		gear_emit_op(result, ast->kind == GEAR_AST_PRE_INC ? GEAR_PRE_INC : GEAR_PRE_DEC,
			&var_node, NULL);
	}
}
/* }}} */

void gear_compile_cast(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	znode expr_node;
	gear_op *opline;

	gear_compile_expr(&expr_node, expr_ast);

	opline = gear_emit_op_tmp(result, GEAR_CAST, &expr_node, NULL);
	opline->extended_value = ast->attr;

	if (ast->attr == IS_NULL) {
		gear_error(E_DEPRECATED, "The (unset) cast is deprecated");
	}
}
/* }}} */

static void gear_compile_shorthand_conditional(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *cond_ast = ast->child[0];
	gear_ast *false_ast = ast->child[2];

	znode cond_node, false_node;
	gear_op *opline_qm_assign;
	uint32_t opnum_jmp_set;

	GEAR_ASSERT(ast->child[1] == NULL);

	gear_compile_expr(&cond_node, cond_ast);

	opnum_jmp_set = get_next_op_number(CG(active_op_array));
	gear_emit_op_tmp(result, GEAR_JMP_SET, &cond_node, NULL);

	gear_compile_expr(&false_node, false_ast);

	opline_qm_assign = gear_emit_op_tmp(NULL, GEAR_QM_ASSIGN, &false_node, NULL);
	SET_NODE(opline_qm_assign->result, result);

	gear_update_jump_target_to_next(opnum_jmp_set);
}
/* }}} */

void gear_compile_conditional(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *cond_ast = ast->child[0];
	gear_ast *true_ast = ast->child[1];
	gear_ast *false_ast = ast->child[2];

	znode cond_node, true_node, false_node;
	gear_op *opline_qm_assign2;
	uint32_t opnum_jmpz, opnum_jmp;

	if (!true_ast) {
		gear_compile_shorthand_conditional(result, ast);
		return;
	}

	gear_compile_expr(&cond_node, cond_ast);

	opnum_jmpz = gear_emit_cond_jump(GEAR_JMPZ, &cond_node, 0);

	gear_compile_expr(&true_node, true_ast);

	gear_emit_op_tmp(result, GEAR_QM_ASSIGN, &true_node, NULL);

	opnum_jmp = gear_emit_jump(0);

	gear_update_jump_target_to_next(opnum_jmpz);

	gear_compile_expr(&false_node, false_ast);

	opline_qm_assign2 = gear_emit_op(NULL, GEAR_QM_ASSIGN, &false_node, NULL);
	SET_NODE(opline_qm_assign2->result, result);

	gear_update_jump_target_to_next(opnum_jmp);
}
/* }}} */

void gear_compile_coalesce(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	gear_ast *default_ast = ast->child[1];

	znode expr_node, default_node;
	gear_op *opline;
	uint32_t opnum;

	gear_compile_var(&expr_node, expr_ast, BP_VAR_IS);

	opnum = get_next_op_number(CG(active_op_array));
	gear_emit_op_tmp(result, GEAR_COALESCE, &expr_node, NULL);

	gear_compile_expr(&default_node, default_ast);

	opline = gear_emit_op_tmp(NULL, GEAR_QM_ASSIGN, &default_node, NULL);
	SET_NODE(opline->result, result);

	opline = &CG(active_op_array)->opcodes[opnum];
	opline->op2.opline_num = get_next_op_number(CG(active_op_array));
}
/* }}} */

void gear_compile_print(znode *result, gear_ast *ast) /* {{{ */
{
	gear_op *opline;
	gear_ast *expr_ast = ast->child[0];

	znode expr_node;
	gear_compile_expr(&expr_node, expr_ast);

	opline = gear_emit_op(NULL, GEAR_ECHO, &expr_node, NULL);
	opline->extended_value = 1;

	result->op_type = IS_CONST;
	ZVAL_LONG(&result->u.constant, 1);
}
/* }}} */

void gear_compile_exit(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];

	if (expr_ast) {
		znode expr_node;
		gear_compile_expr(&expr_node, expr_ast);
		gear_emit_op(NULL, GEAR_EXIT, &expr_node, NULL);
	} else {
		gear_emit_op(NULL, GEAR_EXIT, NULL, NULL);
	}

	result->op_type = IS_CONST;
	ZVAL_BOOL(&result->u.constant, 1);
}
/* }}} */

void gear_compile_yield(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *value_ast = ast->child[0];
	gear_ast *key_ast = ast->child[1];

	znode value_node, key_node;
	znode *value_node_ptr = NULL, *key_node_ptr = NULL;
	gear_op *opline;
	gear_bool returns_by_ref = (CG(active_op_array)->fn_flags & GEAR_ACC_RETURN_REFERENCE) != 0;

	gear_mark_function_as_generator();

	if (key_ast) {
		gear_compile_expr(&key_node, key_ast);
		key_node_ptr = &key_node;
	}

	if (value_ast) {
		if (returns_by_ref && gear_is_variable(value_ast) && !gear_is_call(value_ast)) {
			gear_compile_var(&value_node, value_ast, BP_VAR_W);
		} else {
			gear_compile_expr(&value_node, value_ast);
		}
		value_node_ptr = &value_node;
	}

	opline = gear_emit_op(result, GEAR_YIELD, value_node_ptr, key_node_ptr);

	if (value_ast && returns_by_ref && gear_is_call(value_ast)) {
		opline->extended_value = GEAR_RETURNS_FUNCTION;
	}
}
/* }}} */

void gear_compile_yield_from(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	znode expr_node;

	gear_mark_function_as_generator();

	if (CG(active_op_array)->fn_flags & GEAR_ACC_RETURN_REFERENCE) {
		gear_error_noreturn(E_COMPILE_ERROR,
			"Cannot use \"yield from\" inside a by-reference generator");
	}

	gear_compile_expr(&expr_node, expr_ast);
	gear_emit_op_tmp(result, GEAR_YIELD_FROM, &expr_node, NULL);
}
/* }}} */

void gear_compile_instanceof(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *obj_ast = ast->child[0];
	gear_ast *class_ast = ast->child[1];

	znode obj_node, class_node;
	gear_op *opline;

	gear_compile_expr(&obj_node, obj_ast);
	if (obj_node.op_type == IS_CONST) {
		gear_do_free(&obj_node);
		result->op_type = IS_CONST;
		ZVAL_FALSE(&result->u.constant);
		return;
	}

	gear_compile_class_ref_ex(&class_node, class_ast,
		GEAR_FETCH_CLASS_NO_AUTOLOAD | GEAR_FETCH_CLASS_EXCEPTION);

	opline = gear_emit_op_tmp(result, GEAR_INSTANCEOF, &obj_node, NULL);

	if (class_node.op_type == IS_CONST) {
		opline->op2_type = IS_CONST;
		opline->op2.constant = gear_add_class_name_literal(
			CG(active_op_array), Z_STR(class_node.u.constant));
		opline->extended_value = gear_alloc_cache_slot();
	} else {
		SET_NODE(opline->op2, &class_node);
	}
}
/* }}} */

void gear_compile_include_or_eval(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	znode expr_node;
	gear_op *opline;

	gear_do_extended_fcall_begin();
	gear_compile_expr(&expr_node, expr_ast);

	opline = gear_emit_op(result, GEAR_INCLUDE_OR_EVAL, &expr_node, NULL);
	opline->extended_value = ast->attr;

	gear_do_extended_fcall_end();
}
/* }}} */

void gear_compile_isset_or_empty(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *var_ast = ast->child[0];

	znode var_node;
	gear_op *opline = NULL;

	GEAR_ASSERT(ast->kind == GEAR_AST_ISSET || ast->kind == GEAR_AST_EMPTY);

	if (!gear_is_variable(var_ast) || gear_is_call(var_ast)) {
		if (ast->kind == GEAR_AST_EMPTY) {
			/* empty(expr) can be transformed to !expr */
			gear_ast *not_ast = gear_ast_create_ex(GEAR_AST_UNARY_OP, GEAR_BOOL_NOT, var_ast);
			gear_compile_expr(result, not_ast);
			return;
		} else {
			gear_error_noreturn(E_COMPILE_ERROR,
				"Cannot use isset() on the result of an expression "
				"(you can use \"null !== expression\" instead)");
		}
	}

	switch (var_ast->kind) {
		case GEAR_AST_VAR:
			if (is_this_fetch(var_ast)) {
				opline = gear_emit_op(result, GEAR_ISSET_ISEMPTY_THIS, NULL, NULL);
			} else if (gear_try_compile_cv(&var_node, var_ast) == SUCCESS) {
				opline = gear_emit_op(result, GEAR_ISSET_ISEMPTY_CV, &var_node, NULL);
			} else {
				opline = gear_compile_simple_var_no_cv(result, var_ast, BP_VAR_IS, 0);
				opline->opcode = GEAR_ISSET_ISEMPTY_VAR;
			}
			break;
		case GEAR_AST_DIM:
			opline = gear_compile_dim(result, var_ast, BP_VAR_IS);
			opline->opcode = GEAR_ISSET_ISEMPTY_DIM_OBJ;
			break;
		case GEAR_AST_PROP:
			opline = gear_compile_prop(result, var_ast, BP_VAR_IS);
			opline->opcode = GEAR_ISSET_ISEMPTY_PROP_OBJ;
			break;
		case GEAR_AST_STATIC_PROP:
			opline = gear_compile_static_prop(result, var_ast, BP_VAR_IS, 0);
			opline->opcode = GEAR_ISSET_ISEMPTY_STATIC_PROP;
			break;
		EMPTY_SWITCH_DEFAULT_CASE()
	}

	result->op_type = opline->result_type = IS_TMP_VAR;
	if (!(ast->kind == GEAR_AST_ISSET)) {
		opline->extended_value |= GEAR_ISEMPTY;
	}
}
/* }}} */

void gear_compile_silence(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];
	znode silence_node;
	uint32_t range;

	range = gear_start_live_range(CG(active_op_array), get_next_op_number(CG(active_op_array)));
	gear_emit_op_tmp(&silence_node, GEAR_BEGIN_SILENCE, NULL, NULL);

	if (expr_ast->kind == GEAR_AST_VAR) {
		/* For @$var we need to force a FETCH instruction, otherwise the CV access will
		 * happen outside the silenced section. */
		gear_compile_simple_var_no_cv(result, expr_ast, BP_VAR_R, 0 );
	} else {
		gear_compile_expr(result, expr_ast);
	}

	/* Store BEGIN_SILENCE/END_SILENCE pair to restore previous
	 * EG(error_reporting) value on exception */
	gear_end_live_range(CG(active_op_array), range, get_next_op_number(CG(active_op_array)),
		GEAR_LIVE_SILENCE, silence_node.u.op.var);

	gear_emit_op(NULL, GEAR_END_SILENCE, &silence_node, NULL);
}
/* }}} */

void gear_compile_shell_exec(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *expr_ast = ast->child[0];

	zval fn_name;
	gear_ast *name_ast, *args_ast, *call_ast;

	ZVAL_STRING(&fn_name, "shell_exec");
	name_ast = gear_ast_create_zval(&fn_name);
	args_ast = gear_ast_create_list(1, GEAR_AST_ARG_LIST, expr_ast);
	call_ast = gear_ast_create(GEAR_AST_CALL, name_ast, args_ast);

	gear_compile_expr(result, call_ast);

	zval_ptr_dtor(&fn_name);
}
/* }}} */

void gear_compile_array(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast_list *list = gear_ast_get_list(ast);
	gear_op *opline;
	uint32_t i, opnum_init = -1;
	gear_bool packed = 1;

	if (gear_try_ct_eval_array(&result->u.constant, ast)) {
		result->op_type = IS_CONST;
		return;
	}

	/* Empty arrays are handled at compile-time */
	GEAR_ASSERT(list->children > 0);

	for (i = 0; i < list->children; ++i) {
		gear_ast *elem_ast = list->child[i];
		gear_ast *value_ast, *key_ast;
		gear_bool by_ref;
		znode value_node, key_node, *key_node_ptr = NULL;

		if (elem_ast == NULL) {
			gear_error(E_COMPILE_ERROR, "Cannot use empty array elements in arrays");
		}

		value_ast = elem_ast->child[0];
		key_ast = elem_ast->child[1];
		by_ref = elem_ast->attr;

		if (key_ast) {
			gear_compile_expr(&key_node, key_ast);
			gear_handle_numeric_op(&key_node);
			key_node_ptr = &key_node;
		}

		if (by_ref) {
			gear_ensure_writable_variable(value_ast);
			gear_compile_var(&value_node, value_ast, BP_VAR_W);
		} else {
			gear_compile_expr(&value_node, value_ast);
		}

		if (i == 0) {
			opnum_init = get_next_op_number(CG(active_op_array));
			opline = gear_emit_op_tmp(result, GEAR_INIT_ARRAY, &value_node, key_node_ptr);
			opline->extended_value = list->children << GEAR_ARRAY_SIZE_SHIFT;
		} else {
			opline = gear_emit_op(NULL, GEAR_ADD_ARRAY_ELEMENT,
				&value_node, key_node_ptr);
			SET_NODE(opline->result, result);
		}
		opline->extended_value |= by_ref;

		if (key_ast && key_node.op_type == IS_CONST && Z_TYPE(key_node.u.constant) == IS_STRING) {
			packed = 0;
		}
	}

	/* Add a flag to INIT_ARRAY if we know this array cannot be packed */
	if (!packed) {
		GEAR_ASSERT(opnum_init != (uint32_t)-1);
		opline = &CG(active_op_array)->opcodes[opnum_init];
		opline->extended_value |= GEAR_ARRAY_NOT_PACKED;
	}
}
/* }}} */

void gear_compile_const(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *name_ast = ast->child[0];

	gear_op *opline;

	gear_bool is_fully_qualified;
	gear_string *orig_name = gear_ast_get_str(name_ast);
	gear_string *resolved_name = gear_resolve_const_name(orig_name, name_ast->attr, &is_fully_qualified);

	if (gear_string_equals_literal(resolved_name, "__COMPILER_HALT_OFFSET__") || (name_ast->attr != GEAR_NAME_RELATIVE && gear_string_equals_literal(orig_name, "__COMPILER_HALT_OFFSET__"))) {
		gear_ast *last = CG(ast);

		while (last && last->kind == GEAR_AST_STMT_LIST) {
			gear_ast_list *list = gear_ast_get_list(last);
			last = list->child[list->children-1];
		}
		if (last && last->kind == GEAR_AST_HALT_COMPILER) {
			result->op_type = IS_CONST;
			ZVAL_LONG(&result->u.constant, Z_LVAL_P(gear_ast_get_zval(last->child[0])));
			gear_string_release_ex(resolved_name, 0);
			return;
		}
	}

	if (gear_try_ct_eval_const(&result->u.constant, resolved_name, is_fully_qualified)) {
		result->op_type = IS_CONST;
		gear_string_release_ex(resolved_name, 0);
		return;
	}

	opline = gear_emit_op_tmp(result, GEAR_FETCH_CONSTANT, NULL, NULL);
	opline->op2_type = IS_CONST;

	if (is_fully_qualified) {
		opline->op2.constant = gear_add_const_name_literal(
			CG(active_op_array), resolved_name, 0);
	} else {
		opline->op1.num = IS_CONSTANT_UNQUALIFIED;
		if (FC(current_namespace)) {
			opline->op1.num |= IS_CONSTANT_IN_NAMESPACE;
			opline->op2.constant = gear_add_const_name_literal(
				CG(active_op_array), resolved_name, 1);
		} else {
			opline->op2.constant = gear_add_const_name_literal(
				CG(active_op_array), resolved_name, 0);
		}
	}
	opline->extended_value = gear_alloc_cache_slot();
}
/* }}} */

void gear_compile_class_const(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *class_ast = ast->child[0];
	gear_ast *const_ast = ast->child[1];

	znode class_node, const_node;
	gear_op *opline;

	if (gear_try_compile_const_expr_resolve_class_name(&result->u.constant, class_ast, const_ast, 0)) {
		if (Z_TYPE(result->u.constant) == IS_NULL) {
			gear_op *opline = gear_emit_op_tmp(result, GEAR_FETCH_CLASS_NAME, NULL, NULL);
			opline->op1.num = gear_get_class_fetch_type(gear_ast_get_str(class_ast));
		} else {
			result->op_type = IS_CONST;
		}
		return;
	}

	gear_eval_const_expr(&ast->child[0]);
	gear_eval_const_expr(&ast->child[1]);

	class_ast = ast->child[0];
	const_ast = ast->child[1];

	if (class_ast->kind == GEAR_AST_ZVAL) {
		gear_string *resolved_name;

		resolved_name = gear_resolve_class_name_ast(class_ast);
		if (const_ast->kind == GEAR_AST_ZVAL && gear_try_ct_eval_class_const(&result->u.constant, resolved_name, gear_ast_get_str(const_ast))) {
			result->op_type = IS_CONST;
			gear_string_release_ex(resolved_name, 0);
			return;
		}
		gear_string_release_ex(resolved_name, 0);
	}
	if (const_ast->kind == GEAR_AST_ZVAL && gear_string_equals_literal_ci(gear_ast_get_str(const_ast), "class")) {
		gear_error_noreturn(E_COMPILE_ERROR,
			"Dynamic class names are not allowed in compile-time ::class fetch");
	}

	gear_compile_class_ref_ex(&class_node, class_ast, GEAR_FETCH_CLASS_EXCEPTION);

	gear_compile_expr(&const_node, const_ast);

	opline = gear_emit_op_tmp(result, GEAR_FETCH_CLASS_CONSTANT, NULL, &const_node);

	gear_set_class_name_op1(opline, &class_node);

	opline->extended_value = gear_alloc_polymorphic_cache_slot();
}
/* }}} */

void gear_compile_resolve_class_name(znode *result, gear_ast *ast) /* {{{ */
{
	gear_ast *name_ast = ast->child[0];
	uint32_t fetch_type = gear_get_class_fetch_type(gear_ast_get_str(name_ast));
	gear_ensure_valid_class_fetch_type(fetch_type);

	switch (fetch_type) {
		case GEAR_FETCH_CLASS_SELF:
			if (CG(active_class_entry) && gear_is_scope_known()) {
				result->op_type = IS_CONST;
				ZVAL_STR_COPY(&result->u.constant, CG(active_class_entry)->name);
			} else {
				gear_op *opline = gear_emit_op_tmp(result, GEAR_FETCH_CLASS_NAME, NULL, NULL);
				opline->op1.num = fetch_type;
			}
			break;
		case GEAR_FETCH_CLASS_STATIC:
		case GEAR_FETCH_CLASS_PARENT:
			{
				gear_op *opline = gear_emit_op_tmp(result, GEAR_FETCH_CLASS_NAME, NULL, NULL);
				opline->op1.num = fetch_type;
			}
			break;
		case GEAR_FETCH_CLASS_DEFAULT:
			result->op_type = IS_CONST;
			ZVAL_STR(&result->u.constant, gear_resolve_class_name_ast(name_ast));
			break;
		EMPTY_SWITCH_DEFAULT_CASE()
	}
}
/* }}} */

static gear_op *gear_compile_rope_add(znode *result, uint32_t num, znode *elem_node) /* {{{ */
{
	gear_op *opline = get_next_op(CG(active_op_array));

	if (num == 0) {
		result->op_type = IS_TMP_VAR;
		result->u.op.var = -1;
		opline->opcode = GEAR_ROPE_INIT;
	} else {
		opline->opcode = GEAR_ROPE_ADD;
		SET_NODE(opline->op1, result);
	}
	SET_NODE(opline->op2, elem_node);
	SET_NODE(opline->result, result);
	opline->extended_value = num;
	return opline;
}
/* }}} */

static void gear_compile_encaps_list(znode *result, gear_ast *ast) /* {{{ */
{
	uint32_t i, j;
	uint32_t rope_init_lineno = -1;
	gear_op *opline = NULL, *init_opline;
	znode elem_node, last_const_node;
	gear_ast_list *list = gear_ast_get_list(ast);

	GEAR_ASSERT(list->children > 0);

	j = 0;
	last_const_node.op_type = IS_UNUSED;
	for (i = 0; i < list->children; i++) {
		gear_compile_expr(&elem_node, list->child[i]);

		if (elem_node.op_type == IS_CONST) {
			convert_to_string(&elem_node.u.constant);

			if (Z_STRLEN(elem_node.u.constant) == 0) {
				zval_ptr_dtor(&elem_node.u.constant);
			} else if (last_const_node.op_type == IS_CONST) {
				concat_function(&last_const_node.u.constant, &last_const_node.u.constant, &elem_node.u.constant);
				zval_ptr_dtor(&elem_node.u.constant);
			} else {
				last_const_node.op_type = IS_CONST;
				ZVAL_COPY_VALUE(&last_const_node.u.constant, &elem_node.u.constant);
			}
			continue;
		} else {
			if (j == 0) {
				rope_init_lineno = get_next_op_number(CG(active_op_array));
			}
			if (last_const_node.op_type == IS_CONST) {
				gear_compile_rope_add(result, j++, &last_const_node);
				last_const_node.op_type = IS_UNUSED;
			}
			opline = gear_compile_rope_add(result, j++, &elem_node);
		}
	}

	if (j == 0) {
		result->op_type = IS_CONST;
		if (last_const_node.op_type == IS_CONST) {
			ZVAL_COPY_VALUE(&result->u.constant, &last_const_node.u.constant);
		} else {
			ZVAL_EMPTY_STRING(&result->u.constant);
			/* empty string */
		}
		return;
	} else if (last_const_node.op_type == IS_CONST) {
		opline = gear_compile_rope_add(result, j++, &last_const_node);
	}
	init_opline = CG(active_op_array)->opcodes + rope_init_lineno;
	if (j == 1) {
		if (opline->op2_type == IS_CONST) {
			GET_NODE(result, opline->op2);
			MAKE_NOP(opline);
		} else {
			opline->opcode = GEAR_CAST;
			opline->extended_value = IS_STRING;
			opline->op1_type = opline->op2_type;
			opline->op1 = opline->op2;
			opline->result_type = IS_TMP_VAR;
			opline->result.var = get_temporary_variable(CG(active_op_array));
			SET_UNUSED(opline->op2);
			GET_NODE(result, opline->result);
		}
	} else if (j == 2) {
		opline->opcode = GEAR_FAST_CONCAT;
		opline->extended_value = 0;
		opline->op1_type = init_opline->op2_type;
		opline->op1 = init_opline->op2;
		opline->result_type = IS_TMP_VAR;
		opline->result.var = get_temporary_variable(CG(active_op_array));
		MAKE_NOP(init_opline);
		GET_NODE(result, opline->result);
	} else {
		uint32_t var;
		uint32_t range = gear_start_live_range(CG(active_op_array), rope_init_lineno);

		init_opline->extended_value = j;
		opline->opcode = GEAR_ROPE_END;
		opline->result.var = get_temporary_variable(CG(active_op_array));
		var = opline->op1.var = get_temporary_variable(CG(active_op_array));
		GET_NODE(result, opline->result);

		/* Allocates the necessary number of zval slots to keep the rope */
		i = ((j * sizeof(gear_string*)) + (sizeof(zval) - 1)) / sizeof(zval);
		while (i > 1) {
			get_temporary_variable(CG(active_op_array));
			i--;
		}

		gear_end_live_range(CG(active_op_array), range, opline - CG(active_op_array)->opcodes,
			GEAR_LIVE_ROPE, var);

		/* Update all the previous opcodes to use the same variable */
		while (opline != init_opline) {
			opline--;
			if (opline->opcode == GEAR_ROPE_ADD &&
			    opline->result.var == (uint32_t)-1) {
				opline->op1.var = var;
				opline->result.var = var;
			} else if (opline->opcode == GEAR_ROPE_INIT &&
			           opline->result.var == (uint32_t)-1) {
				opline->result.var = var;
			}
		}
	}
}
/* }}} */

void gear_compile_magic_const(znode *result, gear_ast *ast) /* {{{ */
{
	gear_op *opline;

	if (gear_try_ct_eval_magic_const(&result->u.constant, ast)) {
		result->op_type = IS_CONST;
		return;
	}

	GEAR_ASSERT(ast->attr == T_CLASS_C &&
	            CG(active_class_entry) &&
	            (CG(active_class_entry)->ce_flags & GEAR_ACC_TRAIT) != 0);

	opline = gear_emit_op_tmp(result, GEAR_FETCH_CLASS_NAME, NULL, NULL);
	opline->op1.num = GEAR_FETCH_CLASS_SELF;
}
/* }}} */

gear_bool gear_is_allowed_in_const_expr(gear_ast_kind kind) /* {{{ */
{
	return kind == GEAR_AST_ZVAL || kind == GEAR_AST_BINARY_OP
		|| kind == GEAR_AST_GREATER || kind == GEAR_AST_GREATER_EQUAL
		|| kind == GEAR_AST_AND || kind == GEAR_AST_OR
		|| kind == GEAR_AST_UNARY_OP
		|| kind == GEAR_AST_UNARY_PLUS || kind == GEAR_AST_UNARY_MINUS
		|| kind == GEAR_AST_CONDITIONAL || kind == GEAR_AST_DIM
		|| kind == GEAR_AST_ARRAY || kind == GEAR_AST_ARRAY_ELEM
		|| kind == GEAR_AST_CONST || kind == GEAR_AST_CLASS_CONST
		|| kind == GEAR_AST_MAGIC_CONST || kind == GEAR_AST_COALESCE;
}
/* }}} */

void gear_compile_const_expr_class_const(gear_ast **ast_ptr) /* {{{ */
{
	gear_ast *ast = *ast_ptr;
	gear_ast *class_ast = ast->child[0];
	gear_ast *const_ast = ast->child[1];
	gear_string *class_name;
	gear_string *const_name = gear_ast_get_str(const_ast);
	gear_string *name;
	zval result;
	int fetch_type;

	if (class_ast->kind != GEAR_AST_ZVAL) {
		gear_error_noreturn(E_COMPILE_ERROR,
			"Dynamic class names are not allowed in compile-time class constant references");
	}

	if (gear_try_compile_const_expr_resolve_class_name(&result, class_ast, const_ast, 1)) {
		*ast_ptr = gear_ast_create_zval(&result);
		return;
	}

	class_name = gear_ast_get_str(class_ast);
	fetch_type = gear_get_class_fetch_type(class_name);

	if (GEAR_FETCH_CLASS_STATIC == fetch_type) {
		gear_error_noreturn(E_COMPILE_ERROR,
			"\"static::\" is not allowed in compile-time constants");
	}

	if (GEAR_FETCH_CLASS_DEFAULT == fetch_type) {
		class_name = gear_resolve_class_name_ast(class_ast);
	} else {
		gear_string_addref(class_name);
	}

	name = gear_concat3(
		ZSTR_VAL(class_name), ZSTR_LEN(class_name), "::", 2, ZSTR_VAL(const_name), ZSTR_LEN(const_name));

	gear_ast_destroy(ast);
	gear_string_release_ex(class_name, 0);

	*ast_ptr = gear_ast_create_constant(name, fetch_type | GEAR_FETCH_CLASS_EXCEPTION);
}
/* }}} */

void gear_compile_const_expr_const(gear_ast **ast_ptr) /* {{{ */
{
	gear_ast *ast = *ast_ptr;
	gear_ast *name_ast = ast->child[0];
	gear_string *orig_name = gear_ast_get_str(name_ast);
	gear_bool is_fully_qualified;
	zval result;
	gear_string *resolved_name;

	resolved_name = gear_resolve_const_name(
		orig_name, name_ast->attr, &is_fully_qualified);

	if (gear_try_ct_eval_const(&result, resolved_name, is_fully_qualified)) {
		gear_string_release_ex(resolved_name, 0);
		gear_ast_destroy(ast);
		*ast_ptr = gear_ast_create_zval(&result);
		return;
	}

	gear_ast_destroy(ast);
	*ast_ptr = gear_ast_create_constant(resolved_name, !is_fully_qualified ? IS_CONSTANT_UNQUALIFIED : 0);
}
/* }}} */

void gear_compile_const_expr_magic_const(gear_ast **ast_ptr) /* {{{ */
{
	gear_ast *ast = *ast_ptr;

	/* Other cases already resolved by constant folding */
	GEAR_ASSERT(ast->attr == T_CLASS_C);

	gear_ast_destroy(ast);
	*ast_ptr = gear_ast_create(GEAR_AST_CONSTANT_CLASS);
}
/* }}} */

void gear_compile_const_expr(gear_ast **ast_ptr) /* {{{ */
{
	gear_ast *ast = *ast_ptr;
	if (ast == NULL || ast->kind == GEAR_AST_ZVAL) {
		return;
	}

	if (!gear_is_allowed_in_const_expr(ast->kind)) {
		gear_error_noreturn(E_COMPILE_ERROR, "Constant expression contains invalid operations");
	}

	switch (ast->kind) {
		case GEAR_AST_CLASS_CONST:
			gear_compile_const_expr_class_const(ast_ptr);
			break;
		case GEAR_AST_CONST:
			gear_compile_const_expr_const(ast_ptr);
			break;
		case GEAR_AST_MAGIC_CONST:
			gear_compile_const_expr_magic_const(ast_ptr);
			break;
		default:
			gear_ast_apply(ast, gear_compile_const_expr);
			break;
	}
}
/* }}} */

void gear_const_expr_to_zval(zval *result, gear_ast *ast) /* {{{ */
{
	gear_ast *orig_ast = ast;
	gear_eval_const_expr(&ast);
	gear_compile_const_expr(&ast);
	if (ast->kind == GEAR_AST_ZVAL) {
		ZVAL_COPY_VALUE(result, gear_ast_get_zval(ast));
	} else {
		ZVAL_AST(result, gear_ast_copy(ast));
		/* destroy the ast here, it might have been replaced */
		gear_ast_destroy(ast);
	}

	/* Kill this branch of the original AST, as it was already destroyed.
	 * It would be nice to find a better solution to this problem in the
	 * future. */
	orig_ast->kind = 0;
}
/* }}} */

/* Same as compile_stmt, but with early binding */
void gear_compile_top_stmt(gear_ast *ast) /* {{{ */
{
	if (!ast) {
		return;
	}

	if (ast->kind == GEAR_AST_STMT_LIST) {
		gear_ast_list *list = gear_ast_get_list(ast);
		uint32_t i;
		for (i = 0; i < list->children; ++i) {
			gear_compile_top_stmt(list->child[i]);
		}
		return;
	}

	gear_compile_stmt(ast);

	if (ast->kind != GEAR_AST_NAMESPACE && ast->kind != GEAR_AST_HALT_COMPILER) {
		gear_verify_namespace();
	}
	if (ast->kind == GEAR_AST_FUNC_DECL || ast->kind == GEAR_AST_CLASS) {
		CG(gear_lineno) = ((gear_ast_decl *) ast)->end_lineno;
		gear_do_early_binding();
	}
}
/* }}} */

void gear_compile_stmt(gear_ast *ast) /* {{{ */
{
	if (!ast) {
		return;
	}

	CG(gear_lineno) = ast->lineno;

	if ((CG(compiler_options) & GEAR_COMPILE_EXTENDED_INFO) && !gear_is_unticked_stmt(ast)) {
		gear_do_extended_info();
	}

	switch (ast->kind) {
		case GEAR_AST_STMT_LIST:
			gear_compile_stmt_list(ast);
			break;
		case GEAR_AST_GLOBAL:
			gear_compile_global_var(ast);
			break;
		case GEAR_AST_STATIC:
			gear_compile_static_var(ast);
			break;
		case GEAR_AST_UNSET:
			gear_compile_unset(ast);
			break;
		case GEAR_AST_RETURN:
			gear_compile_return(ast);
			break;
		case GEAR_AST_ECHO:
			gear_compile_echo(ast);
			break;
		case GEAR_AST_THROW:
			gear_compile_throw(ast);
			break;
		case GEAR_AST_BREAK:
		case GEAR_AST_CONTINUE:
			gear_compile_break_continue(ast);
			break;
		case GEAR_AST_GOTO:
			gear_compile_goto(ast);
			break;
		case GEAR_AST_LABEL:
			gear_compile_label(ast);
			break;
		case GEAR_AST_WHILE:
			gear_compile_while(ast);
			break;
		case GEAR_AST_DO_WHILE:
			gear_compile_do_while(ast);
			break;
		case GEAR_AST_FOR:
			gear_compile_for(ast);
			break;
		case GEAR_AST_FOREACH:
			gear_compile_foreach(ast);
			break;
		case GEAR_AST_IF:
			gear_compile_if(ast);
			break;
		case GEAR_AST_SWITCH:
			gear_compile_switch(ast);
			break;
		case GEAR_AST_TRY:
			gear_compile_try(ast);
			break;
		case GEAR_AST_DECLARE:
			gear_compile_declare(ast);
			break;
		case GEAR_AST_FUNC_DECL:
		case GEAR_AST_METHOD:
			gear_compile_func_decl(NULL, ast);
			break;
		case GEAR_AST_PROP_DECL:
			gear_compile_prop_decl(ast);
			break;
		case GEAR_AST_CLASS_CONST_DECL:
			gear_compile_class_const_decl(ast);
			break;
		case GEAR_AST_USE_TRAIT:
			gear_compile_use_trait(ast);
			break;
		case GEAR_AST_CLASS:
			gear_compile_class_decl(ast);
			break;
		case GEAR_AST_GROUP_USE:
			gear_compile_group_use(ast);
			break;
		case GEAR_AST_USE:
			gear_compile_use(ast);
			break;
		case GEAR_AST_CONST_DECL:
			gear_compile_const_decl(ast);
			break;
		case GEAR_AST_NAMESPACE:
			gear_compile_namespace(ast);
			break;
		case GEAR_AST_HALT_COMPILER:
			gear_compile_halt_compiler(ast);
			break;
		default:
		{
			znode result;
			gear_compile_expr(&result, ast);
			gear_do_free(&result);
		}
	}

	if (FC(declarables).ticks && !gear_is_unticked_stmt(ast)) {
		gear_emit_tick();
	}
}
/* }}} */

void gear_compile_expr(znode *result, gear_ast *ast) /* {{{ */
{
	/* CG(gear_lineno) = ast->lineno; */
	CG(gear_lineno) = gear_ast_get_lineno(ast);

	switch (ast->kind) {
		case GEAR_AST_ZVAL:
			ZVAL_COPY(&result->u.constant, gear_ast_get_zval(ast));
			result->op_type = IS_CONST;
			return;
		case GEAR_AST_ZNODE:
			*result = *gear_ast_get_znode(ast);
			return;
		case GEAR_AST_VAR:
		case GEAR_AST_DIM:
		case GEAR_AST_PROP:
		case GEAR_AST_STATIC_PROP:
		case GEAR_AST_CALL:
		case GEAR_AST_METHOD_CALL:
		case GEAR_AST_STATIC_CALL:
			gear_compile_var(result, ast, BP_VAR_R);
			return;
		case GEAR_AST_ASSIGN:
			gear_compile_assign(result, ast);
			return;
		case GEAR_AST_ASSIGN_REF:
			gear_compile_assign_ref(result, ast);
			return;
		case GEAR_AST_NEW:
			gear_compile_new(result, ast);
			return;
		case GEAR_AST_CLONE:
			gear_compile_clone(result, ast);
			return;
		case GEAR_AST_ASSIGN_OP:
			gear_compile_compound_assign(result, ast);
			return;
		case GEAR_AST_BINARY_OP:
			gear_compile_binary_op(result, ast);
			return;
		case GEAR_AST_GREATER:
		case GEAR_AST_GREATER_EQUAL:
			gear_compile_greater(result, ast);
			return;
		case GEAR_AST_UNARY_OP:
			gear_compile_unary_op(result, ast);
			return;
		case GEAR_AST_UNARY_PLUS:
		case GEAR_AST_UNARY_MINUS:
			gear_compile_unary_pm(result, ast);
			return;
		case GEAR_AST_AND:
		case GEAR_AST_OR:
			gear_compile_short_circuiting(result, ast);
			return;
		case GEAR_AST_POST_INC:
		case GEAR_AST_POST_DEC:
			gear_compile_post_incdec(result, ast);
			return;
		case GEAR_AST_PRE_INC:
		case GEAR_AST_PRE_DEC:
			gear_compile_pre_incdec(result, ast);
			return;
		case GEAR_AST_CAST:
			gear_compile_cast(result, ast);
			return;
		case GEAR_AST_CONDITIONAL:
			gear_compile_conditional(result, ast);
			return;
		case GEAR_AST_COALESCE:
			gear_compile_coalesce(result, ast);
			return;
		case GEAR_AST_PRINT:
			gear_compile_print(result, ast);
			return;
		case GEAR_AST_EXIT:
			gear_compile_exit(result, ast);
			return;
		case GEAR_AST_YIELD:
			gear_compile_yield(result, ast);
			return;
		case GEAR_AST_YIELD_FROM:
			gear_compile_yield_from(result, ast);
			return;
		case GEAR_AST_INSTANCEOF:
			gear_compile_instanceof(result, ast);
			return;
		case GEAR_AST_INCLUDE_OR_EVAL:
			gear_compile_include_or_eval(result, ast);
			return;
		case GEAR_AST_ISSET:
		case GEAR_AST_EMPTY:
			gear_compile_isset_or_empty(result, ast);
			return;
		case GEAR_AST_SILENCE:
			gear_compile_silence(result, ast);
			return;
		case GEAR_AST_SHELL_EXEC:
			gear_compile_shell_exec(result, ast);
			return;
		case GEAR_AST_ARRAY:
			gear_compile_array(result, ast);
			return;
		case GEAR_AST_CONST:
			gear_compile_const(result, ast);
			return;
		case GEAR_AST_CLASS_CONST:
			gear_compile_class_const(result, ast);
			return;
		case GEAR_AST_ENCAPS_LIST:
			gear_compile_encaps_list(result, ast);
			return;
		case GEAR_AST_MAGIC_CONST:
			gear_compile_magic_const(result, ast);
			return;
		case GEAR_AST_CLOSURE:
			gear_compile_func_decl(result, ast);
			return;
		default:
			GEAR_ASSERT(0 /* not supported */);
	}
}
/* }}} */

void gear_compile_var(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	CG(gear_lineno) = gear_ast_get_lineno(ast);

	switch (ast->kind) {
		case GEAR_AST_VAR:
			gear_compile_simple_var(result, ast, type, 0);
			return;
		case GEAR_AST_DIM:
			gear_compile_dim(result, ast, type);
			return;
		case GEAR_AST_PROP:
			gear_compile_prop(result, ast, type);
			return;
		case GEAR_AST_STATIC_PROP:
			gear_compile_static_prop(result, ast, type, 0);
			return;
		case GEAR_AST_CALL:
			gear_compile_call(result, ast, type);
			return;
		case GEAR_AST_METHOD_CALL:
			gear_compile_method_call(result, ast, type);
			return;
		case GEAR_AST_STATIC_CALL:
			gear_compile_static_call(result, ast, type);
			return;
		case GEAR_AST_ZNODE:
			*result = *gear_ast_get_znode(ast);
			return;
		default:
			if (type == BP_VAR_W || type == BP_VAR_RW || type == BP_VAR_UNSET) {
				gear_error_noreturn(E_COMPILE_ERROR,
					"Cannot use temporary expression in write context");
			}

			gear_compile_expr(result, ast);
			return;
	}
}
/* }}} */

void gear_delayed_compile_var(znode *result, gear_ast *ast, uint32_t type) /* {{{ */
{
	switch (ast->kind) {
		case GEAR_AST_VAR:
			gear_compile_simple_var(result, ast, type, 1);
			return;
		case GEAR_AST_DIM:
			gear_delayed_compile_dim(result, ast, type);
			return;
		case GEAR_AST_PROP:
			gear_delayed_compile_prop(result, ast, type);
			return;
		case GEAR_AST_STATIC_PROP:
			gear_compile_static_prop(result, ast, type, 1);
			return;
		default:
			gear_compile_var(result, ast, type);
			return;
	}
}
/* }}} */

void gear_eval_const_expr(gear_ast **ast_ptr) /* {{{ */
{
	gear_ast *ast = *ast_ptr;
	zval result;

	if (!ast) {
		return;
	}

	switch (ast->kind) {
		case GEAR_AST_BINARY_OP:
			gear_eval_const_expr(&ast->child[0]);
			gear_eval_const_expr(&ast->child[1]);
			if (ast->child[0]->kind != GEAR_AST_ZVAL || ast->child[1]->kind != GEAR_AST_ZVAL) {
				return;
			}

			if (!gear_try_ct_eval_binary_op(&result, ast->attr,
					gear_ast_get_zval(ast->child[0]), gear_ast_get_zval(ast->child[1]))
			) {
				return;
			}
			break;
		case GEAR_AST_GREATER:
		case GEAR_AST_GREATER_EQUAL:
			gear_eval_const_expr(&ast->child[0]);
			gear_eval_const_expr(&ast->child[1]);
			if (ast->child[0]->kind != GEAR_AST_ZVAL || ast->child[1]->kind != GEAR_AST_ZVAL) {
				return;
			}

			gear_ct_eval_greater(&result, ast->kind,
				gear_ast_get_zval(ast->child[0]), gear_ast_get_zval(ast->child[1]));
			break;
		case GEAR_AST_AND:
		case GEAR_AST_OR:
		{
			int i;
			for (i = 0; i <= 1; i++) {
				gear_eval_const_expr(&ast->child[i]);
				if (ast->child[i]->kind == GEAR_AST_ZVAL) {
					if (gear_is_true(gear_ast_get_zval(ast->child[i])) == (ast->kind == GEAR_AST_OR)) {
						ZVAL_BOOL(&result, ast->kind == GEAR_AST_OR);
						return;
					}
				}
			}

			if (ast->child[0]->kind != GEAR_AST_ZVAL || ast->child[1]->kind != GEAR_AST_ZVAL) {
				return;
			}

			if (ast->kind == GEAR_AST_OR) {
				ZVAL_BOOL(&result, gear_is_true(gear_ast_get_zval(ast->child[0])) || gear_is_true(gear_ast_get_zval(ast->child[1])));
			} else {
				ZVAL_BOOL(&result, gear_is_true(gear_ast_get_zval(ast->child[0])) && gear_is_true(gear_ast_get_zval(ast->child[1])));
			}
			break;
		}
		case GEAR_AST_UNARY_OP:
			gear_eval_const_expr(&ast->child[0]);
			if (ast->child[0]->kind != GEAR_AST_ZVAL) {
				return;
			}

			gear_ct_eval_unary_op(&result, ast->attr, gear_ast_get_zval(ast->child[0]));
			break;
		case GEAR_AST_UNARY_PLUS:
		case GEAR_AST_UNARY_MINUS:
			gear_eval_const_expr(&ast->child[0]);
			if (ast->child[0]->kind != GEAR_AST_ZVAL) {
				return;
			}

			if (!gear_try_ct_eval_unary_pm(&result, ast->kind, gear_ast_get_zval(ast->child[0]))) {
				return;
			}
			break;
		case GEAR_AST_COALESCE:
			/* Set isset fetch indicator here, opcache disallows runtime altering of the AST */
			if (ast->child[0]->kind == GEAR_AST_DIM) {
				ast->child[0]->attr = GEAR_DIM_IS;
			}
			gear_eval_const_expr(&ast->child[0]);

			if (ast->child[0]->kind != GEAR_AST_ZVAL) {
				/* ensure everything was compile-time evaluated at least once */
				gear_eval_const_expr(&ast->child[1]);
				return;
			}

			if (Z_TYPE_P(gear_ast_get_zval(ast->child[0])) == IS_NULL) {
				gear_eval_const_expr(&ast->child[1]);
				*ast_ptr = ast->child[1];
				ast->child[1] = NULL;
				gear_ast_destroy(ast);
			} else {
				*ast_ptr = ast->child[0];
				ast->child[0] = NULL;
				gear_ast_destroy(ast);
			}
			return;
		case GEAR_AST_CONDITIONAL:
		{
			gear_ast **child, *child_ast;
			gear_eval_const_expr(&ast->child[0]);
			if (ast->child[0]->kind != GEAR_AST_ZVAL) {
				/* ensure everything was compile-time evaluated at least once */
				if (ast->child[1]) {
					gear_eval_const_expr(&ast->child[1]);
				}
				gear_eval_const_expr(&ast->child[2]);
				return;
			}

			child = &ast->child[2 - gear_is_true(gear_ast_get_zval(ast->child[0]))];
			if (*child == NULL) {
				child--;
			}
			child_ast = *child;
			*child = NULL;
			gear_ast_destroy(ast);
			*ast_ptr = child_ast;
			gear_eval_const_expr(ast_ptr);
			return;
		}
		case GEAR_AST_DIM:
		{
			/* constant expression should be always read context ... */
			zval *container, *dim;

			if (ast->child[1] == NULL) {
				gear_error_noreturn(E_COMPILE_ERROR, "Cannot use [] for reading");
			}

			/* Set isset fetch indicator here, opcache disallows runtime altering of the AST */
			if (ast->attr == GEAR_DIM_IS && ast->child[0]->kind == GEAR_AST_DIM) {
				ast->child[0]->attr = GEAR_DIM_IS;
			}

			gear_eval_const_expr(&ast->child[0]);
			gear_eval_const_expr(&ast->child[1]);
			if (ast->child[0]->kind != GEAR_AST_ZVAL || ast->child[1]->kind != GEAR_AST_ZVAL) {
				return;
			}

			container = gear_ast_get_zval(ast->child[0]);
			dim = gear_ast_get_zval(ast->child[1]);

			if (Z_TYPE_P(container) == IS_ARRAY) {
				zval *el;
				if (Z_TYPE_P(dim) == IS_LONG) {
					el = gear_hash_index_find(Z_ARR_P(container), Z_LVAL_P(dim));
					if (el) {
						ZVAL_COPY(&result, el);
					} else {
						return;
					}
				} else if (Z_TYPE_P(dim) == IS_STRING) {
					el = gear_symtable_find(Z_ARR_P(container), Z_STR_P(dim));
					if (el) {
						ZVAL_COPY(&result, el);
					} else {
						return;
					}
				} else {
					return; /* warning... handle at runtime */
				}
			} else if (Z_TYPE_P(container) == IS_STRING) {
				gear_long offset;
				gear_uchar c;
				if (Z_TYPE_P(dim) == IS_LONG) {
					offset = Z_LVAL_P(dim);
				} else if (Z_TYPE_P(dim) != IS_STRING || is_numeric_string(Z_STRVAL_P(dim), Z_STRLEN_P(dim), &offset, NULL, 1) != IS_LONG) {
					return;
				}
				if (offset < 0 || (size_t)offset >= Z_STRLEN_P(container)) {
					return;
				}
				c = (gear_uchar) Z_STRVAL_P(container)[offset];
				ZVAL_INTERNED_STR(&result, ZSTR_CHAR(c));
			} else if (Z_TYPE_P(container) <= IS_FALSE) {
				ZVAL_NULL(&result);
			} else {
				return;
			}
			break;
		}
		case GEAR_AST_ARRAY:
			if (!gear_try_ct_eval_array(&result, ast)) {
				return;
			}
			break;
		case GEAR_AST_MAGIC_CONST:
			if (!gear_try_ct_eval_magic_const(&result, ast)) {
				return;
			}
			break;
		case GEAR_AST_CONST:
		{
			gear_ast *name_ast = ast->child[0];
			gear_bool is_fully_qualified;
			gear_string *resolved_name = gear_resolve_const_name(
				gear_ast_get_str(name_ast), name_ast->attr, &is_fully_qualified);

			if (!gear_try_ct_eval_const(&result, resolved_name, is_fully_qualified)) {
				gear_string_release_ex(resolved_name, 0);
				return;
			}

			gear_string_release_ex(resolved_name, 0);
			break;
		}
		case GEAR_AST_CLASS_CONST:
		{
			gear_ast *class_ast = ast->child[0];
			gear_ast *name_ast = ast->child[1];
			gear_string *resolved_name;

			if (gear_try_compile_const_expr_resolve_class_name(&result, class_ast, name_ast, 0)) {
				if (Z_TYPE(result) == IS_NULL) {
					if (gear_get_class_fetch_type(gear_ast_get_str(class_ast)) == GEAR_FETCH_CLASS_SELF) {
						gear_ast_destroy(ast);
						*ast_ptr = gear_ast_create_ex(GEAR_AST_MAGIC_CONST, T_CLASS_C);
					}
					return;
				}
				break;
			}

			gear_eval_const_expr(&ast->child[0]);
			gear_eval_const_expr(&ast->child[1]);

			class_ast = ast->child[0];
			name_ast = ast->child[1];

			if (name_ast->kind == GEAR_AST_ZVAL && gear_string_equals_literal_ci(gear_ast_get_str(name_ast), "class")) {
				gear_error_noreturn(E_COMPILE_ERROR,
					"Dynamic class names are not allowed in compile-time ::class fetch");
			}

			if (class_ast->kind != GEAR_AST_ZVAL || name_ast->kind != GEAR_AST_ZVAL) {
				return;
			}

			resolved_name = gear_resolve_class_name_ast(class_ast);

			if (!gear_try_ct_eval_class_const(&result, resolved_name, gear_ast_get_str(name_ast))) {
				gear_string_release_ex(resolved_name, 0);
				return;
			}

			gear_string_release_ex(resolved_name, 0);
			break;
		}

		default:
			return;
	}

	gear_ast_destroy(ast);
	*ast_ptr = gear_ast_create_zval(&result);
}
/* }}} */

