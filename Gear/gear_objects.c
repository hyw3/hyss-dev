/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_globals.h"
#include "gear_variables.h"
#include "gear_API.h"
#include "gear_interfaces.h"
#include "gear_exceptions.h"

GEAR_API void GEAR_FASTCALL gear_object_std_init(gear_object *object, gear_class_entry *ce)
{
	GC_SET_REFCOUNT(object, 1);
	GC_TYPE_INFO(object) = IS_OBJECT | (GC_COLLECTABLE << GC_FLAGS_SHIFT);
	object->ce = ce;
	object->properties = NULL;
	gear_objects_store_put(object);
	if (UNEXPECTED(ce->ce_flags & GEAR_ACC_USE_GUARDS)) {
		ZVAL_UNDEF(object->properties_table + object->ce->default_properties_count);
	}
}

GEAR_API void gear_object_std_dtor(gear_object *object)
{
	zval *p, *end;

	if (object->properties) {
		if (EXPECTED(!(GC_FLAGS(object->properties) & IS_ARRAY_IMMUTABLE))) {
			if (EXPECTED(GC_DELREF(object->properties) == 0)) {
				gear_array_destroy(object->properties);
			}
		}
	}
	p = object->properties_table;
	if (EXPECTED(object->ce->default_properties_count)) {
		end = p + object->ce->default_properties_count;
		do {
			i_zval_ptr_dtor(p GEAR_FILE_LINE_CC);
			p++;
		} while (p != end);
	}
	if (UNEXPECTED(object->ce->ce_flags & GEAR_ACC_USE_GUARDS)) {
		if (EXPECTED(Z_TYPE_P(p) == IS_STRING)) {
			zval_ptr_dtor_str(p);
		} else if (Z_TYPE_P(p) == IS_ARRAY) {
			HashTable *guards;

			guards = Z_ARRVAL_P(p);
			GEAR_ASSERT(guards != NULL);
			gear_hash_destroy(guards);
			FREE_HASHTABLE(guards);
		}
	}
}

GEAR_API void gear_objects_destroy_object(gear_object *object)
{
	gear_function *destructor = object->ce->destructor;

	if (destructor) {
		gear_object *old_exception;
		gear_class_entry *orig_fake_scope;
		gear_fcall_info fci;
		gear_fcall_info_cache fcic;
		zval ret;

		if (destructor->op_array.fn_flags & (GEAR_ACC_PRIVATE|GEAR_ACC_PROTECTED)) {
			if (destructor->op_array.fn_flags & GEAR_ACC_PRIVATE) {
				/* Ensure that if we're calling a private function, we're allowed to do so.
				 */
				if (EG(current_execute_data)) {
					gear_class_entry *scope = gear_get_executed_scope();

					if (object->ce != scope) {
						gear_throw_error(NULL,
							"Call to private %s::__destruct() from context '%s'",
							ZSTR_VAL(object->ce->name),
							scope ? ZSTR_VAL(scope->name) : "");
						return;
					}
				} else {
					gear_error(E_WARNING,
						"Call to private %s::__destruct() from context '' during shutdown ignored",
						ZSTR_VAL(object->ce->name));
					return;
				}
			} else {
				/* Ensure that if we're calling a protected function, we're allowed to do so.
				 */
				if (EG(current_execute_data)) {
					gear_class_entry *scope = gear_get_executed_scope();

					if (!gear_check_protected(gear_get_function_root_class(destructor), scope)) {
						gear_throw_error(NULL,
							"Call to protected %s::__destruct() from context '%s'",
							ZSTR_VAL(object->ce->name),
							scope ? ZSTR_VAL(scope->name) : "");
						return;
					}
				} else {
					gear_error(E_WARNING,
						"Call to protected %s::__destruct() from context '' during shutdown ignored",
						ZSTR_VAL(object->ce->name));
					return;
				}
			}
		}

		GC_ADDREF(object);

		/* Make sure that destructors are protected from previously thrown exceptions.
		 * For example, if an exception was thrown in a function and when the function's
		 * local variable destruction results in a destructor being called.
		 */
		old_exception = NULL;
		if (EG(exception)) {
			if (EG(exception) == object) {
				gear_error_noreturn(E_CORE_ERROR, "Attempt to destruct pending exception");
			} else {
				old_exception = EG(exception);
				EG(exception) = NULL;
			}
		}
		orig_fake_scope = EG(fake_scope);
		EG(fake_scope) = NULL;

		ZVAL_UNDEF(&ret);

		fci.size = sizeof(fci);
		fci.object = object;
		fci.retval = &ret;
		fci.param_count = 0;
		fci.params = NULL;
		fci.no_separation = 1;
		ZVAL_UNDEF(&fci.function_name); /* Unused */

		fcic.function_handler = destructor;
		fcic.called_scope = object->ce;
		fcic.object = object;

		gear_call_function(&fci, &fcic);
		zval_ptr_dtor(&ret);

		if (old_exception) {
			if (EG(exception)) {
				gear_exception_set_previous(EG(exception), old_exception);
			} else {
				EG(exception) = old_exception;
			}
		}
		OBJ_RELEASE(object);
		EG(fake_scope) = orig_fake_scope;
	}
}

GEAR_API gear_object* GEAR_FASTCALL gear_objects_new(gear_class_entry *ce)
{
	gear_object *object = emalloc(sizeof(gear_object) + gear_object_properties_size(ce));

	gear_object_std_init(object, ce);
	object->handlers = &std_object_handlers;
	return object;
}

GEAR_API void GEAR_FASTCALL gear_objects_clone_members(gear_object *new_object, gear_object *old_object)
{
	if (old_object->ce->default_properties_count) {
		zval *src = old_object->properties_table;
		zval *dst = new_object->properties_table;
		zval *end = src + old_object->ce->default_properties_count;

		do {
			i_zval_ptr_dtor(dst GEAR_FILE_LINE_CC);
			ZVAL_COPY_VALUE(dst, src);
			zval_add_ref(dst);
			src++;
			dst++;
		} while (src != end);
	} else if (old_object->properties && !old_object->ce->clone) {
		/* fast copy */
		if (EXPECTED(old_object->handlers == &std_object_handlers)) {
			if (EXPECTED(!(GC_FLAGS(old_object->properties) & IS_ARRAY_IMMUTABLE))) {
				GC_ADDREF(old_object->properties);
			}
			new_object->properties = old_object->properties;
			return;
		}
	}

	if (old_object->properties &&
	    EXPECTED(gear_hash_num_elements(old_object->properties))) {
		zval *prop, new_prop;
		gear_ulong num_key;
		gear_string *key;

		if (!new_object->properties) {
			new_object->properties = gear_new_array(gear_hash_num_elements(old_object->properties));
			gear_hash_real_init_mixed(new_object->properties);
		} else {
			gear_hash_extend(new_object->properties, new_object->properties->nNumUsed + gear_hash_num_elements(old_object->properties), 0);
		}

		HT_FLAGS(new_object->properties) |=
			HT_FLAGS(old_object->properties) & HASH_FLAG_HAS_EMPTY_IND;

		GEAR_HASH_FOREACH_KEY_VAL(old_object->properties, num_key, key, prop) {
			if (Z_TYPE_P(prop) == IS_INDIRECT) {
				ZVAL_INDIRECT(&new_prop, new_object->properties_table + (Z_INDIRECT_P(prop) - old_object->properties_table));
			} else {
				ZVAL_COPY_VALUE(&new_prop, prop);
				zval_add_ref(&new_prop);
			}
			if (EXPECTED(key)) {
				_gear_hash_append(new_object->properties, key, &new_prop);
			} else {
				gear_hash_index_add_new(new_object->properties, num_key, &new_prop);
			}
		} GEAR_HASH_FOREACH_END();
	}

	if (old_object->ce->clone) {
		gear_fcall_info fci;
		gear_fcall_info_cache fcic;
		zval ret;

		GC_ADDREF(new_object);

		ZVAL_UNDEF(&ret);

		fci.size = sizeof(fci);
		fci.object = new_object;
		fci.retval = &ret;
		fci.param_count = 0;
		fci.params = NULL;
		fci.no_separation = 1;
		ZVAL_UNDEF(&fci.function_name); /* Unused */

		fcic.function_handler = new_object->ce->clone;
		fcic.called_scope = new_object->ce;
		fcic.object = new_object;

		gear_call_function(&fci, &fcic);
		zval_ptr_dtor(&ret);
		OBJ_RELEASE(new_object);
	}
}

GEAR_API gear_object *gear_objects_clone_obj(zval *zobject)
{
	gear_object *old_object;
	gear_object *new_object;

	/* assume that create isn't overwritten, so when clone depends on the
	 * overwritten one then it must itself be overwritten */
	old_object = Z_OBJ_P(zobject);
	new_object = gear_objects_new(old_object->ce);

	/* gear_objects_clone_members() expect the properties to be initialized. */
	if (new_object->ce->default_properties_count) {
		zval *p = new_object->properties_table;
		zval *end = p + new_object->ce->default_properties_count;
		do {
			ZVAL_UNDEF(p);
			p++;
		} while (p != end);
	}

	gear_objects_clone_members(new_object, old_object);

	return new_object;
}

