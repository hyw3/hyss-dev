/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_RANGE_CHECK_H
#define GEAR_RANGE_CHECK_H

#include "gear_long.h"

/* Flag macros for basic range recognition. Notable is that
   always sizeof(signed) == sizeof(unsigned), so no need to
   overcomplicate things. */
#if SIZEOF_INT < SIZEOF_GEAR_LONG
# define GEAR_LONG_CAN_OVFL_INT 1
# define GEAR_LONG_CAN_OVFL_UINT 1
#endif

#if SIZEOF_INT < SIZEOF_SIZE_T
/* size_t can always overflow signed int on the same platform.
   Furthermore, by the current design, size_t can always
   overflow gear_long. */
# define GEAR_SIZE_T_CAN_OVFL_UINT 1
#endif


/* gear_long vs. (unsigned) int checks. */
#ifdef GEAR_LONG_CAN_OVFL_INT
# define GEAR_LONG_INT_OVFL(zlong) UNEXPECTED((zlong) > (gear_long)INT_MAX)
# define GEAR_LONG_INT_UDFL(zlong) UNEXPECTED((zlong) < (gear_long)INT_MIN)
# define GEAR_LONG_EXCEEDS_INT(zlong) UNEXPECTED(GEAR_LONG_INT_OVFL(zlong) || GEAR_LONG_INT_UDFL(zlong))
# define GEAR_LONG_UINT_OVFL(zlong) UNEXPECTED((zlong) < 0 || (zlong) > (gear_long)UINT_MAX)
#else
# define GEAR_LONG_INT_OVFL(zl) (0)
# define GEAR_LONG_INT_UDFL(zl) (0)
# define GEAR_LONG_EXCEEDS_INT(zlong) (0)
# define GEAR_LONG_UINT_OVFL(zl) (0)
#endif

/* size_t vs (unsigned) int checks. */
#define GEAR_SIZE_T_INT_OVFL(size) 	UNEXPECTED((size) > (size_t)INT_MAX)
#ifdef GEAR_SIZE_T_CAN_OVFL_UINT
# define GEAR_SIZE_T_UINT_OVFL(size) UNEXPECTED((size) > (size_t)UINT_MAX)
#else
# define GEAR_SIZE_T_UINT_OVFL(size) (0)
#endif

/* Comparison gear_long vs size_t */
#define GEAR_SIZE_T_GT_GEAR_LONG(size, zlong) ((zlong) < 0 || (size) > (size_t)(zlong))
#define GEAR_SIZE_T_GTE_GEAR_LONG(size, zlong) ((zlong) < 0 || (size) >= (size_t)(zlong))
#define GEAR_SIZE_T_LT_GEAR_LONG(size, zlong) ((zlong) >= 0 && (size) < (size_t)(zlong))
#define GEAR_SIZE_T_LTE_GEAR_LONG(size, zlong) ((zlong) >= 0 && (size) <= (size_t)(zlong))

#endif /* GEAR_RANGE_CHECK_H */

