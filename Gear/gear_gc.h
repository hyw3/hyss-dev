/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_GC_H
#define GEAR_GC_H

BEGIN_EXTERN_C()

typedef struct _gear_gc_status {
	uint32_t runs;
	uint32_t collected;
	uint32_t threshold;
	uint32_t num_roots;
} gear_gc_status;

GEAR_API extern int (*gc_collect_cycles)(void);

GEAR_API void GEAR_FASTCALL gc_possible_root(gear_refcounted *ref);
GEAR_API void GEAR_FASTCALL gc_remove_from_buffer(gear_refcounted *ref);

/* enable/disable automatic start of GC collection */
GEAR_API gear_bool gc_enable(gear_bool enable);
GEAR_API gear_bool gc_enabled(void);

/* enable/disable possible root additions */
GEAR_API gear_bool gc_protect(gear_bool protect);
GEAR_API gear_bool gc_protected(void);

/* The default implementation of the gc_collect_cycles callback. */
GEAR_API int  gear_gc_collect_cycles(void);

GEAR_API void gear_gc_get_status(gear_gc_status *status);

void gc_globals_ctor(void);
void gc_globals_dtor(void);
void gc_reset(void);

END_EXTERN_C()

#define GC_REMOVE_FROM_BUFFER(p) do { \
		gear_refcounted *_p = (gear_refcounted*)(p); \
		if (GC_TYPE_INFO(_p) & GC_INFO_MASK) { \
			gc_remove_from_buffer(_p); \
		} \
	} while (0)

#define GC_MAY_LEAK(ref) \
	((GC_TYPE_INFO(ref) & \
		(GC_INFO_MASK | (GC_COLLECTABLE << GC_FLAGS_SHIFT))) == \
	(GC_COLLECTABLE << GC_FLAGS_SHIFT))

static gear_always_inline void gc_check_possible_root(gear_refcounted *ref)
{
	if (GC_TYPE_INFO(ref) == IS_REFERENCE) {
		zval *zv = &((gear_reference*)ref)->val;

		if (!Z_REFCOUNTED_P(zv)) {
			return;
		}
		ref = Z_COUNTED_P(zv);
	}
	if (UNEXPECTED(GC_MAY_LEAK(ref))) {
		gc_possible_root(ref);
	}
}

#endif /* GEAR_GC_H */

