/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gear.h>
#include "gear_smart_str.h"
#include "gear_smart_string.h"

#define SMART_STR_OVERHEAD   (GEAR_MM_OVERHEAD + _ZSTR_HEADER_SIZE + 1)
#define SMART_STR_START_SIZE 256
#define SMART_STR_START_LEN  (SMART_STR_START_SIZE - SMART_STR_OVERHEAD)
#define SMART_STR_PAGE       4096

#define SMART_STR_NEW_LEN(len) \
	(GEAR_MM_ALIGNED_SIZE_EX(len + SMART_STR_OVERHEAD, SMART_STR_PAGE) - SMART_STR_OVERHEAD)

GEAR_API void GEAR_FASTCALL smart_str_erealloc(smart_str *str, size_t len)
{
	if (UNEXPECTED(!str->s)) {
		str->a = len <= SMART_STR_START_LEN
				? SMART_STR_START_LEN
				: SMART_STR_NEW_LEN(len);
		str->s = gear_string_alloc(str->a, 0);
		ZSTR_LEN(str->s) = 0;
	} else {
		str->a = SMART_STR_NEW_LEN(len);
		str->s = (gear_string *) erealloc2(str->s, str->a + _ZSTR_HEADER_SIZE + 1, _ZSTR_HEADER_SIZE + ZSTR_LEN(str->s));
	}
}

GEAR_API void GEAR_FASTCALL smart_str_realloc(smart_str *str, size_t len)
{
	if (UNEXPECTED(!str->s)) {
		str->a = len <= SMART_STR_START_SIZE
				? SMART_STR_START_LEN
				: SMART_STR_NEW_LEN(len);
		str->s = gear_string_alloc(str->a, 1);
		ZSTR_LEN(str->s) = 0;
	} else {
		str->a = SMART_STR_NEW_LEN(len);
		str->s = (gear_string *) perealloc(str->s, str->a + _ZSTR_HEADER_SIZE + 1, 1);
	}
}

/* Windows uses VK_ESCAPE instead of \e */
#ifndef VK_ESCAPE
#define VK_ESCAPE '\e'
#endif

static size_t gear_compute_escaped_string_len(const char *s, size_t l) {
	size_t i, len = l;
	for (i = 0; i < l; ++i) {
		char c = s[i];
		if (c == '\n' || c == '\r' || c == '\t' ||
			c == '\f' || c == '\v' || c == '\\' || c == VK_ESCAPE) {
			len += 1;
		} else if (c < 32 || c > 126) {
			len += 3;
		}
	}
	return len;
}

GEAR_API void GEAR_FASTCALL smart_str_append_escaped(smart_str *str, const char *s, size_t l) {
	char *res;
	size_t i, len = gear_compute_escaped_string_len(s, l);

	smart_str_alloc(str, len, 0);
	res = &ZSTR_VAL(str->s)[ZSTR_LEN(str->s)];
	ZSTR_LEN(str->s) += len;

	for (i = 0; i < l; ++i) {
		unsigned char c = s[i];
		if (c < 32 || c == '\\' || c > 126) {
			*res++ = '\\';
			switch (c) {
				case '\n': *res++ = 'n'; break;
				case '\r': *res++ = 'r'; break;
				case '\t': *res++ = 't'; break;
				case '\f': *res++ = 'f'; break;
				case '\v': *res++ = 'v'; break;
				case '\\': *res++ = '\\'; break;
				case VK_ESCAPE: *res++ = 'e'; break;
				default:
					*res++ = 'x';
					if ((c >> 4) < 10) {
						*res++ = (c >> 4) + '0';
					} else {
						*res++ = (c >> 4) + 'A' - 10;
					}
					if ((c & 0xf) < 10) {
						*res++ = (c & 0xf) + '0';
					} else {
						*res++ = (c & 0xf) + 'A' - 10;
					}
			}
		} else {
			*res++ = c;
		}
	}
}

GEAR_API void smart_str_append_printf(smart_str *dest, const char *format, ...) {
	va_list arg;
	va_start(arg, format);
	gear_printf_to_smart_str(dest, format, arg);
	va_end(arg);
}

#define SMART_STRING_OVERHEAD   (GEAR_MM_OVERHEAD + 1)
#define SMART_STRING_START_SIZE 256
#define SMART_STRING_START_LEN  (SMART_STRING_START_SIZE - SMART_STRING_OVERHEAD)
#define SMART_STRING_PAGE       4096

GEAR_API void GEAR_FASTCALL _smart_string_alloc_persistent(smart_string *str, size_t len)
{
	if (!str->c) {
		str->len = 0;
		if (len <= SMART_STRING_START_LEN) {
			str->a = SMART_STRING_START_LEN;
		} else {
			str->a = GEAR_MM_ALIGNED_SIZE_EX(len + SMART_STRING_OVERHEAD, SMART_STRING_PAGE) - SMART_STRING_OVERHEAD;
		}
		str->c = pemalloc(str->a + 1, 1);
	} else {
		if (UNEXPECTED((size_t) len > SIZE_MAX - str->len)) {
			gear_error(E_ERROR, "String size overflow");
		}
		len += str->len;
		str->a = GEAR_MM_ALIGNED_SIZE_EX(len + SMART_STRING_OVERHEAD, SMART_STRING_PAGE) - SMART_STRING_OVERHEAD;
		str->c = perealloc(str->c, str->a + 1, 1);
	}
}

GEAR_API void GEAR_FASTCALL _smart_string_alloc(smart_string *str, size_t len)
{
	if (!str->c) {
		str->len = 0;
		if (len <= SMART_STRING_START_LEN) {
			str->a = SMART_STRING_START_LEN;
			str->c = emalloc(SMART_STRING_START_LEN + 1);
		} else {
			str->a = GEAR_MM_ALIGNED_SIZE_EX(len + SMART_STRING_OVERHEAD, SMART_STRING_PAGE) - SMART_STRING_OVERHEAD;
			if (EXPECTED(str->a < (GEAR_MM_CHUNK_SIZE - SMART_STRING_OVERHEAD))) {
				str->c = emalloc_large(str->a + 1);
			} else {
				/* allocate a huge chunk */
				str->c = emalloc(str->a + 1);
			}
		}
	} else {
		if (UNEXPECTED((size_t) len > SIZE_MAX - str->len)) {
			gear_error(E_ERROR, "String size overflow");
		}
		len += str->len;
		str->a = GEAR_MM_ALIGNED_SIZE_EX(len + SMART_STRING_OVERHEAD, SMART_STRING_PAGE) - SMART_STRING_OVERHEAD;
		str->c = erealloc2(str->c, str->a + 1, str->len);
	}
}

