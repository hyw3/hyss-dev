/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_OBJECT_HANDLERS_H
#define GEAR_OBJECT_HANDLERS_H

struct _gear_property_info;

#define GEAR_WRONG_PROPERTY_INFO \
	((struct _gear_property_info*)((intptr_t)-1))

#define GEAR_DYNAMIC_PROPERTY_OFFSET               ((uintptr_t)(intptr_t)(-1))

#define IS_VALID_PROPERTY_OFFSET(offset)           ((intptr_t)(offset) > 0)
#define IS_WRONG_PROPERTY_OFFSET(offset)           ((intptr_t)(offset) == 0)
#define IS_DYNAMIC_PROPERTY_OFFSET(offset)         ((intptr_t)(offset) < 0)

#define IS_UNKNOWN_DYNAMIC_PROPERTY_OFFSET(offset) (offset == GEAR_DYNAMIC_PROPERTY_OFFSET)
#define GEAR_DECODE_DYN_PROP_OFFSET(offset)        ((uintptr_t)(-(intptr_t)(offset) - 2))
#define GEAR_ENCODE_DYN_PROP_OFFSET(offset)        ((uintptr_t)(-((intptr_t)(offset) + 2)))


/* The following rule applies to read_property() and read_dimension() implementations:
   If you return a zval which is not otherwise referenced by the extension or the engine's
   symbol table, its reference count should be 0.
*/
/* Used to fetch property from the object, read-only */
typedef zval *(*gear_object_read_property_t)(zval *object, zval *member, int type, void **cache_slot, zval *rv);

/* Used to fetch dimension from the object, read-only */
typedef zval *(*gear_object_read_dimension_t)(zval *object, zval *offset, int type, zval *rv);


/* The following rule applies to write_property() and write_dimension() implementations:
   If you receive a value zval in write_property/write_dimension, you may only modify it if
   its reference count is 1.  Otherwise, you must create a copy of that zval before making
   any changes.  You should NOT modify the reference count of the value passed to you.
*/
/* Used to set property of the object */
typedef void (*gear_object_write_property_t)(zval *object, zval *member, zval *value, void **cache_slot);

/* Used to set dimension of the object */
typedef void (*gear_object_write_dimension_t)(zval *object, zval *offset, zval *value);


/* Used to create pointer to the property of the object, for future direct r/w access */
typedef zval *(*gear_object_get_property_ptr_ptr_t)(zval *object, zval *member, int type, void **cache_slot);

/* Used to set object value. Can be used to override assignments and scalar
   write ops (like ++, +=) on the object */
typedef void (*gear_object_set_t)(zval *object, zval *value);

/* Used to get object value. Can be used when converting object value to
 * one of the basic types and when using scalar ops (like ++, +=) on the object
 */
typedef zval* (*gear_object_get_t)(zval *object, zval *rv);

/* Used to check if a property of the object exists */
/* param has_set_exists:
 * 0 (has) whether property exists and is not NULL
 * 1 (set) whether property exists and is true
 * 2 (exists) whether property exists
 */
typedef int (*gear_object_has_property_t)(zval *object, zval *member, int has_set_exists, void **cache_slot);

/* Used to check if a dimension of the object exists */
typedef int (*gear_object_has_dimension_t)(zval *object, zval *member, int check_empty);

/* Used to remove a property of the object */
typedef void (*gear_object_unset_property_t)(zval *object, zval *member, void **cache_slot);

/* Used to remove a dimension of the object */
typedef void (*gear_object_unset_dimension_t)(zval *object, zval *offset);

/* Used to get hash of the properties of the object, as hash of zval's */
typedef HashTable *(*gear_object_get_properties_t)(zval *object);

typedef HashTable *(*gear_object_get_debug_info_t)(zval *object, int *is_temp);

/* Used to call methods */
/* args on stack! */
/* Andi - EX(fbc) (function being called) needs to be initialized already in the INIT fcall opcode so that the parameters can be parsed the right way. We need to add another callback for this.
 */
typedef int (*gear_object_call_method_t)(gear_string *method, gear_object *object, INTERNAL_FUNCTION_PARAMETERS);
typedef gear_function *(*gear_object_get_method_t)(gear_object **object, gear_string *method, const zval *key);
typedef gear_function *(*gear_object_get_constructor_t)(gear_object *object);

/* Object maintenance/destruction */
typedef void (*gear_object_dtor_obj_t)(gear_object *object);
typedef void (*gear_object_free_obj_t)(gear_object *object);
typedef gear_object* (*gear_object_clone_obj_t)(zval *object);

/* Get class name for display in var_dump and other debugging functions.
 * Must be defined and must return a non-NULL value. */
typedef gear_string *(*gear_object_get_class_name_t)(const gear_object *object);

typedef int (*gear_object_compare_t)(zval *object1, zval *object2);
typedef int (*gear_object_compare_zvals_t)(zval *resul, zval *op1, zval *op2);

/* Cast an object to some other type.
 * readobj and retval must point to distinct zvals.
 */
typedef int (*gear_object_cast_t)(zval *readobj, zval *retval, int type);

/* updates *count to hold the number of elements present and returns SUCCESS.
 * Returns FAILURE if the object does not have any sense of overloaded dimensions */
typedef int (*gear_object_count_elements_t)(zval *object, gear_long *count);

typedef int (*gear_object_get_closure_t)(zval *obj, gear_class_entry **ce_ptr, gear_function **fptr_ptr, gear_object **obj_ptr);

typedef HashTable *(*gear_object_get_gc_t)(zval *object, zval **table, int *n);

typedef int (*gear_object_do_operation_t)(gear_uchar opcode, zval *result, zval *op1, zval *op2);

struct _gear_object_handlers {
	/* offset of real object header (usually zero) */
	int										offset;
	/* general object functions */
	gear_object_free_obj_t					free_obj;
	gear_object_dtor_obj_t					dtor_obj;
	gear_object_clone_obj_t					clone_obj;
	/* individual object functions */
	gear_object_read_property_t				read_property;
	gear_object_write_property_t			write_property;
	gear_object_read_dimension_t			read_dimension;
	gear_object_write_dimension_t			write_dimension;
	gear_object_get_property_ptr_ptr_t		get_property_ptr_ptr;
	gear_object_get_t						get;
	gear_object_set_t						set;
	gear_object_has_property_t				has_property;
	gear_object_unset_property_t			unset_property;
	gear_object_has_dimension_t				has_dimension;
	gear_object_unset_dimension_t			unset_dimension;
	gear_object_get_properties_t			get_properties;
	gear_object_get_method_t				get_method;
	gear_object_call_method_t				call_method;
	gear_object_get_constructor_t			get_constructor;
	gear_object_get_class_name_t			get_class_name;
	gear_object_compare_t					compare_objects;
	gear_object_cast_t						cast_object;
	gear_object_count_elements_t			count_elements;
	gear_object_get_debug_info_t			get_debug_info;
	gear_object_get_closure_t				get_closure;
	gear_object_get_gc_t					get_gc;
	gear_object_do_operation_t				do_operation;
	gear_object_compare_zvals_t				compare;
};

BEGIN_EXTERN_C()
extern const GEAR_API gear_object_handlers std_object_handlers;

#define gear_get_std_object_handlers() \
	(&std_object_handlers)

#define gear_get_function_root_class(fbc) \
	((fbc)->common.prototype ? (fbc)->common.prototype->common.scope : (fbc)->common.scope)

#define GEAR_PROPERTY_ISSET     0x0          /* Property exists and is not NULL */
#define GEAR_PROPERTY_NOT_EMPTY GEAR_ISEMPTY /* Property is not empty */
#define GEAR_PROPERTY_EXISTS    0x2          /* Property exists */

GEAR_API void gear_class_init_statics(gear_class_entry *ce);
GEAR_API gear_function *gear_std_get_static_method(gear_class_entry *ce, gear_string *function_name_strval, const zval *key);
GEAR_API zval *gear_std_get_static_property(gear_class_entry *ce, gear_string *property_name, gear_bool silent);
GEAR_API GEAR_COLD gear_bool gear_std_unset_static_property(gear_class_entry *ce, gear_string *property_name);
GEAR_API gear_function *gear_std_get_constructor(gear_object *object);
GEAR_API struct _gear_property_info *gear_get_property_info(gear_class_entry *ce, gear_string *member, int silent);
GEAR_API HashTable *gear_std_get_properties(zval *object);
GEAR_API HashTable *gear_std_get_gc(zval *object, zval **table, int *n);
GEAR_API HashTable *gear_std_get_debug_info(zval *object, int *is_temp);
GEAR_API int gear_std_cast_object_tostring(zval *readobj, zval *writeobj, int type);
GEAR_API zval *gear_std_get_property_ptr_ptr(zval *object, zval *member, int type, void **cache_slot);
GEAR_API zval *gear_std_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv);
GEAR_API void gear_std_write_property(zval *object, zval *member, zval *value, void **cache_slot);
GEAR_API int gear_std_has_property(zval *object, zval *member, int has_set_exists, void **cache_slot);
GEAR_API void gear_std_unset_property(zval *object, zval *member, void **cache_slot);
GEAR_API zval *gear_std_read_dimension(zval *object, zval *offset, int type, zval *rv);
GEAR_API void gear_std_write_dimension(zval *object, zval *offset, zval *value);
GEAR_API int gear_std_has_dimension(zval *object, zval *offset, int check_empty);
GEAR_API void gear_std_unset_dimension(zval *object, zval *offset);
GEAR_API gear_function *gear_std_get_method(gear_object **obj_ptr, gear_string *method_name, const zval *key);
GEAR_API gear_string *gear_std_get_class_name(const gear_object *zobj);
GEAR_API int gear_std_compare_objects(zval *o1, zval *o2);
GEAR_API int gear_std_get_closure(zval *obj, gear_class_entry **ce_ptr, gear_function **fptr_ptr, gear_object **obj_ptr);
GEAR_API void rebuild_object_properties(gear_object *zobj);

GEAR_API int gear_check_private(gear_function *fbc, gear_class_entry *ce, gear_string *function_name);

GEAR_API int gear_check_protected(gear_class_entry *ce, gear_class_entry *scope);

GEAR_API int gear_check_property_access(gear_object *zobj, gear_string *prop_info_name);

GEAR_API gear_function *gear_get_call_trampoline_func(gear_class_entry *ce, gear_string *method_name, int is_static);

GEAR_API uint32_t *gear_get_property_guard(gear_object *zobj, gear_string *member);

#define gear_free_trampoline(func) do { \
		if ((func) == &EG(trampoline)) { \
			EG(trampoline).common.function_name = NULL; \
		} else { \
			efree(func); \
		} \
	} while (0)

END_EXTERN_C()

#endif

