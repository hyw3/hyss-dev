/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_ICS_H
#define GEAR_ICS_H

#define GEAR_ICS_USER	(1<<0)
#define GEAR_ICS_PERDIR	(1<<1)
#define GEAR_ICS_SYSTEM	(1<<2)

#define GEAR_ICS_ALL (GEAR_ICS_USER|GEAR_ICS_PERDIR|GEAR_ICS_SYSTEM)

#define GEAR_ICS_MH(name) int name(gear_ics_entry *entry, gear_string *new_value, void *mh_arg1, void *mh_arg2, void *mh_arg3, int stage)
#define GEAR_ICS_DISP(name) void name(gear_ics_entry *ics_entry, int type)

typedef struct _gear_ics_entry_def {
	const char *name;
	GEAR_ICS_MH((*on_modify));
	void *mh_arg1;
	void *mh_arg2;
	void *mh_arg3;
	const char *value;
	void (*displayer)(gear_ics_entry *ics_entry, int type);

	uint32_t value_length;
	uint16_t name_length;
	uint8_t modifiable;
} gear_ics_entry_def;

struct _gear_ics_entry {
	gear_string *name;
	GEAR_ICS_MH((*on_modify));
	void *mh_arg1;
	void *mh_arg2;
	void *mh_arg3;
	gear_string *value;
	gear_string *orig_value;
	void (*displayer)(gear_ics_entry *ics_entry, int type);

	int capi_number;

	uint8_t modifiable;
	uint8_t orig_modifiable;
	uint8_t modified;

};

BEGIN_EXTERN_C()
GEAR_API int gear_ics_startup(void);
GEAR_API int gear_ics_shutdown(void);
GEAR_API int gear_ics_global_shutdown(void);
GEAR_API int gear_ics_deactivate(void);
GEAR_API void gear_ics_dtor(HashTable *ics_directives);

GEAR_API int gear_copy_ics_directives(void);

GEAR_API void gear_ics_sort_entries(void);

GEAR_API int gear_register_ics_entries(const gear_ics_entry_def *ics_entry, int capi_number);
GEAR_API void gear_unregister_ics_entries(int capi_number);
GEAR_API void gear_ics_refresh_caches(int stage);
GEAR_API int gear_alter_ics_entry(gear_string *name, gear_string *new_value, int modify_type, int stage);
GEAR_API int gear_alter_ics_entry_ex(gear_string *name, gear_string *new_value, int modify_type, int stage, int force_change);
GEAR_API int gear_alter_ics_entry_chars(gear_string *name, const char *value, size_t value_length, int modify_type, int stage);
GEAR_API int gear_alter_ics_entry_chars_ex(gear_string *name, const char *value, size_t value_length, int modify_type, int stage, int force_change);
GEAR_API int gear_restore_ics_entry(gear_string *name, int stage);
GEAR_API void display_ics_entries(gear_capi_entry *cAPI);

GEAR_API gear_long gear_ics_long(char *name, size_t name_length, int orig);
GEAR_API double gear_ics_double(char *name, size_t name_length, int orig);
GEAR_API char *gear_ics_string(char *name, size_t name_length, int orig);
GEAR_API char *gear_ics_string_ex(char *name, size_t name_length, int orig, gear_bool *exists);
GEAR_API gear_string *gear_ics_get_value(gear_string *name);
GEAR_API gear_bool gear_ics_parse_bool(gear_string *str);

GEAR_API int gear_ics_register_displayer(char *name, uint32_t name_length, void (*displayer)(gear_ics_entry *ics_entry, int type));

GEAR_API GEAR_ICS_DISP(gear_ics_boolean_displayer_cb);
GEAR_API GEAR_ICS_DISP(gear_ics_color_displayer_cb);
GEAR_API GEAR_ICS_DISP(display_link_numbers);
END_EXTERN_C()

#define GEAR_ICS_BEGIN()		static const gear_ics_entry_def ics_entries[] = {
#define GEAR_ICS_END()		{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0} };

#define GEAR_ICS_ENTRY3_EX(name, default_value, modifiable, on_modify, arg1, arg2, arg3, displayer) \
	{ name, on_modify, arg1, arg2, arg3, default_value, displayer, sizeof(default_value)-1, sizeof(name)-1, modifiable },

#define GEAR_ICS_ENTRY3(name, default_value, modifiable, on_modify, arg1, arg2, arg3) \
	GEAR_ICS_ENTRY3_EX(name, default_value, modifiable, on_modify, arg1, arg2, arg3, NULL)

#define GEAR_ICS_ENTRY2_EX(name, default_value, modifiable, on_modify, arg1, arg2, displayer) \
	GEAR_ICS_ENTRY3_EX(name, default_value, modifiable, on_modify, arg1, arg2, NULL, displayer)

#define GEAR_ICS_ENTRY2(name, default_value, modifiable, on_modify, arg1, arg2) \
	GEAR_ICS_ENTRY2_EX(name, default_value, modifiable, on_modify, arg1, arg2, NULL)

#define GEAR_ICS_ENTRY1_EX(name, default_value, modifiable, on_modify, arg1, displayer) \
	GEAR_ICS_ENTRY3_EX(name, default_value, modifiable, on_modify, arg1, NULL, NULL, displayer)

#define GEAR_ICS_ENTRY1(name, default_value, modifiable, on_modify, arg1) \
	GEAR_ICS_ENTRY1_EX(name, default_value, modifiable, on_modify, arg1, NULL)

#define GEAR_ICS_ENTRY_EX(name, default_value, modifiable, on_modify, displayer) \
	GEAR_ICS_ENTRY3_EX(name, default_value, modifiable, on_modify, NULL, NULL, NULL, displayer)

#define GEAR_ICS_ENTRY(name, default_value, modifiable, on_modify) \
	GEAR_ICS_ENTRY_EX(name, default_value, modifiable, on_modify, NULL)

#ifdef ZTS
#define STD_GEAR_ICS_ENTRY(name, default_value, modifiable, on_modify, property_name, struct_type, struct_ptr) \
	GEAR_ICS_ENTRY2(name, default_value, modifiable, on_modify, (void *) XtOffsetOf(struct_type, property_name), (void *) &struct_ptr##_id)
#define STD_GEAR_ICS_ENTRY_EX(name, default_value, modifiable, on_modify, property_name, struct_type, struct_ptr, displayer) \
	GEAR_ICS_ENTRY2_EX(name, default_value, modifiable, on_modify, (void *) XtOffsetOf(struct_type, property_name), (void *) &struct_ptr##_id, displayer)
#define STD_GEAR_ICS_BOOLEAN(name, default_value, modifiable, on_modify, property_name, struct_type, struct_ptr) \
	GEAR_ICS_ENTRY3_EX(name, default_value, modifiable, on_modify, (void *) XtOffsetOf(struct_type, property_name), (void *) &struct_ptr##_id, NULL, gear_ics_boolean_displayer_cb)
#else
#define STD_GEAR_ICS_ENTRY(name, default_value, modifiable, on_modify, property_name, struct_type, struct_ptr) \
	GEAR_ICS_ENTRY2(name, default_value, modifiable, on_modify, (void *) XtOffsetOf(struct_type, property_name), (void *) &struct_ptr)
#define STD_GEAR_ICS_ENTRY_EX(name, default_value, modifiable, on_modify, property_name, struct_type, struct_ptr, displayer) \
	GEAR_ICS_ENTRY2_EX(name, default_value, modifiable, on_modify, (void *) XtOffsetOf(struct_type, property_name), (void *) &struct_ptr, displayer)
#define STD_GEAR_ICS_BOOLEAN(name, default_value, modifiable, on_modify, property_name, struct_type, struct_ptr) \
	GEAR_ICS_ENTRY3_EX(name, default_value, modifiable, on_modify, (void *) XtOffsetOf(struct_type, property_name), (void *) &struct_ptr, NULL, gear_ics_boolean_displayer_cb)
#endif

#define ICS_INT(name) gear_ics_long((name), sizeof(name)-1, 0)
#define ICS_FLT(name) gear_ics_double((name), sizeof(name)-1, 0)
#define ICS_STR(name) gear_ics_string_ex((name), sizeof(name)-1, 0, NULL)
#define ICS_BOOL(name) ((gear_bool) ICS_INT(name))

#define ICS_ORIG_INT(name)	gear_ics_long((name), sizeof(name)-1, 1)
#define ICS_ORIG_FLT(name)	gear_ics_double((name), sizeof(name)-1, 1)
#define ICS_ORIG_STR(name)	gear_ics_string((name), sizeof(name)-1, 1)
#define ICS_ORIG_BOOL(name) ((gear_bool) ICS_ORIG_INT(name))

#define REGISTER_ICS_ENTRIES() gear_register_ics_entries(ics_entries, capi_number)
#define UNREGISTER_ICS_ENTRIES() gear_unregister_ics_entries(capi_number)
#define DISPLAY_ICS_ENTRIES() display_ics_entries(gear_capi)

#define REGISTER_ICS_DISPLAYER(name, displayer) gear_ics_register_displayer((name), sizeof(name)-1, displayer)
#define REGISTER_ICS_BOOLEAN(name) REGISTER_ICS_DISPLAYER(name, gear_ics_boolean_displayer_cb)

/* Standard message handlers */
BEGIN_EXTERN_C()
GEAR_API GEAR_ICS_MH(OnUpdateBool);
GEAR_API GEAR_ICS_MH(OnUpdateLong);
GEAR_API GEAR_ICS_MH(OnUpdateLongGEZero);
GEAR_API GEAR_ICS_MH(OnUpdateReal);
GEAR_API GEAR_ICS_MH(OnUpdateString);
GEAR_API GEAR_ICS_MH(OnUpdateStringUnempty);
END_EXTERN_C()

#define GEAR_ICS_DISPLAY_ORIG	1
#define GEAR_ICS_DISPLAY_ACTIVE	2

#define GEAR_ICS_STAGE_STARTUP		(1<<0)
#define GEAR_ICS_STAGE_SHUTDOWN		(1<<1)
#define GEAR_ICS_STAGE_ACTIVATE		(1<<2)
#define GEAR_ICS_STAGE_DEACTIVATE	(1<<3)
#define GEAR_ICS_STAGE_RUNTIME		(1<<4)
#define GEAR_ICS_STAGE_HTACCESS		(1<<5)

#define GEAR_ICS_STAGE_IN_REQUEST   (GEAR_ICS_STAGE_ACTIVATE|GEAR_ICS_STAGE_DEACTIVATE|GEAR_ICS_STAGE_RUNTIME|GEAR_ICS_STAGE_HTACCESS)

/* ICS parsing engine */
typedef void (*gear_ics_parser_cb_t)(zval *arg1, zval *arg2, zval *arg3, int callback_type, void *arg);
BEGIN_EXTERN_C()
GEAR_API int gear_parse_ics_file(gear_file_handle *fh, gear_bool unbuffered_errors, int scanner_mode, gear_ics_parser_cb_t ics_parser_cb, void *arg);
GEAR_API int gear_parse_ics_string(char *str, gear_bool unbuffered_errors, int scanner_mode, gear_ics_parser_cb_t ics_parser_cb, void *arg);
END_EXTERN_C()

/* ICS entries */
#define GEAR_ICS_PARSER_ENTRY     1 /* Normal entry: foo = bar */
#define GEAR_ICS_PARSER_SECTION	  2 /* Section: [foobar] */
#define GEAR_ICS_PARSER_POP_ENTRY 3 /* Offset entry: foo[] = bar */

typedef struct _gear_ics_parser_param {
	gear_ics_parser_cb_t ics_parser_cb;
	void *arg;
} gear_ics_parser_param;

#endif /* GEAR_ICS_H */

