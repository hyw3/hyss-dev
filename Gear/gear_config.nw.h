/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_CONFIG_NW_H
#define GEAR_CONFIG_NW_H


#include <string.h>
#include <float.h>

typedef unsigned long ulong;
typedef unsigned int uint;

#define HAVE_ALLOCA 1
#define HAVE_LIMITS_H 1
/* #include <malloc.h> */

#define HAVE_STRING_H 1
#define HAVE_SYS_SELECT_H 1
#define HAVE_STDLIB_H 1
#undef HAVE_KILL
#define HAVE_GETPID 1
/* #define HAVE_ALLOCA_H 1 */
#define HAVE_MEMCPY 1
#define HAVE_STRDUP 1
#define HAVE_SYS_TYPES_H 1
/* #define HAVE_STDIOSTR_H 1 */
#define HAVE_CLASS_ISTDIOSTREAM
#define istdiostream stdiostream
#define HAVE_STDARG_H	1
#define HAVE_DLFCN_H	1
/* #define HAVE_LIBDL 1 */
#define HAVE_SNPRINTF	1
#define HAVE_VSNPRINTF	1

/*
#define snprintf _snprintf
#define vsnprintf _vsnprintf
#define gear_isinf(a)	0
#define gear_finite(x)	_finite(x)
#define gear_isnan(x)	_isnan(x)
*/

#define gear_sprintf sprintf

/* This will cause the compilation process to be MUCH longer, but will generate
 * a much quicker HYSS binary
 */
/*
#undef inline
#ifdef GEAR_WIN32_FORCE_INLINE
# define inline __forceinline
#else
# define inline
#endif
*/

/*
#define gear_finite(A) _finite(A)
#define gear_isnan(A) _isnan(A)
*/

#endif /* GEAR_CONFIG_NW_H */

