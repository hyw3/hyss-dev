/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * gear_gc_collect_cycles
 * ======================
 *
 * Colors and its meaning
 * ----------------------
 *
 * BLACK  (GC_BLACK)   - In use or free.
 * GREY   (GC_GREY)    - Possible member of cycle.
 * WHITE  (GC_WHITE)   - Member of garbage cycle.
 * PURPLE (GC_PURPLE)  - Possible root of cycle.
 *
 * Colors described in the paper but not used
 * ------------------------------------------
 *
 * GREEN - Acyclic
 * RED   - Candidate cycle underogin
 * ORANGE - Candidate cycle awaiting epoch boundary.
 *
 *
 * Flow
 * =====
 *
 * The garbage collect cycle starts from 'gc_mark_roots', which traverses the
 * possible roots, and calls mark_grey for roots are marked purple with
 * depth-first traverse.
 *
 * After all possible roots are traversed and marked,
 * gc_scan_roots will be called, and each root will be called with
 * gc_scan(root->ref)
 *
 * gc_scan checks the colors of possible members.
 *
 * If the node is marked as grey and the refcount > 0
 *    gc_scan_black will be called on that node to scan it's subgraph.
 * otherwise (refcount == 0), it marks the node white.
 *
 * A node MAY be added to possbile roots when GEAR_UNSET_VAR happens or
 * gear_assign_to_variable is called only when possible garbage node is
 * produced.
 * gc_possible_root() will be called to add the nodes to possible roots.
 *
 *
 * For objects, we call their get_gc handler (by default 'gear_std_get_gc') to
 * get the object properties to scan.
 *
 *
 * @see http://researcher.watson.ibm.com/researcher/files/us-bacon/Bacon01Concurrent.pdf
 */
#include "gear.h"
#include "gear_API.h"

#ifndef GC_BENCH
# define GC_BENCH 0
#endif

#ifndef GEAR_GC_DEBUG
# define GEAR_GC_DEBUG 0
#endif

/* GC_INFO layout */
#define GC_ADDRESS  0x0fffff
#define GC_COLOR    0x300000

#define GC_BLACK    0x000000 /* must be zero */
#define GC_WHITE    0x100000
#define GC_GREY     0x200000
#define GC_PURPLE   0x300000

/* GC_INFO access */
#define GC_REF_ADDRESS(ref) \
	(((GC_TYPE_INFO(ref)) & (GC_ADDRESS << GC_INFO_SHIFT)) >> GC_INFO_SHIFT)

#define GC_REF_COLOR(ref) \
	(((GC_TYPE_INFO(ref)) & (GC_COLOR << GC_INFO_SHIFT)) >> GC_INFO_SHIFT)

#define GC_REF_CHECK_COLOR(ref, color) \
	((GC_TYPE_INFO(ref) & (GC_COLOR << GC_INFO_SHIFT)) == ((color) << GC_INFO_SHIFT))

#define GC_REF_SET_INFO(ref, info) do { \
		GC_TYPE_INFO(ref) = \
			(GC_TYPE_INFO(ref) & (GC_TYPE_MASK | GC_FLAGS_MASK)) | \
			((info) << GC_INFO_SHIFT); \
	} while (0)

#define GC_REF_SET_COLOR(ref, c) do { \
		GC_TYPE_INFO(ref) = \
			(GC_TYPE_INFO(ref) & ~(GC_COLOR << GC_INFO_SHIFT)) | \
			((c) << GC_INFO_SHIFT); \
	} while (0)

#define GC_REF_SET_BLACK(ref) do { \
		GC_TYPE_INFO(ref) &= ~(GC_COLOR << GC_INFO_SHIFT); \
	} while (0)

#define GC_REF_SET_PURPLE(ref) do { \
		GC_TYPE_INFO(ref) |= (GC_COLOR << GC_INFO_SHIFT); \
	} while (0)

/* bit stealing tags for gc_root_buffer.ref */
#define GC_BITS    0x3

#define GC_ROOT    0x0 /* possible root of circular garbage     */
#define GC_UNUSED  0x1 /* part of linked list of unused buffers */
#define GC_GARBAGE 0x2 /* garbage to delete                     */

#define GC_GET_PTR(ptr) \
	((void*)(((uintptr_t)(ptr)) & ~GC_BITS))

#define GC_IS_ROOT(ptr) \
	((((uintptr_t)(ptr)) & GC_BITS) == GC_ROOT)
#define GC_IS_UNUSED(ptr) \
	((((uintptr_t)(ptr)) & GC_BITS) == GC_UNUSED)
#define GC_IS_GARBAGE(ptr) \
	((((uintptr_t)(ptr)) & GC_BITS) == GC_GARBAGE)

#define GC_MAKE_GARBAGE(ptr) \
	((void*)(((uintptr_t)(ptr)) | GC_GARBAGE))

/* GC address conversion */
#define GC_IDX2PTR(idx)      (GC_G(buf) + (idx))
#define GC_PTR2IDX(ptr)      ((ptr) - GC_G(buf))

#define GC_IDX2LIST(idx)     ((void*)(uintptr_t)(((idx) * sizeof(void*)) | GC_UNUSED))
#define GC_LIST2IDX(list)    (((uint32_t)(uintptr_t)(list)) / sizeof(void*))

/* GC buffers */
#define GC_INVALID           0
#define GC_FIRST_ROOT        1

#define GC_DEFAULT_BUF_SIZE  (16 * 1024)
#define GC_BUF_GROW_STEP     (128 * 1024)

#define GC_MAX_UNCOMPRESSED  (1024 * 1024)
#define GC_MAX_BUF_SIZE      0x40000000

#define GC_THRESHOLD_DEFAULT 10000
#define GC_THRESHOLD_STEP    10000
#define GC_THRESHOLD_MAX     1000000000
#define GC_THRESHOLD_TRIGGER 100

/* GC flags */
#define GC_HAS_DESTRUCTORS  (1<<0)

/* unused buffers */
#define GC_HAS_UNUSED() \
	(GC_G(unused) != GC_INVALID)
#define GC_FETCH_UNUSED() \
	gc_fetch_unused()
#define GC_LINK_UNUSED(root) \
	gc_link_unused(root)

#define GC_HAS_NEXT_UNUSED_UNDER_THRESHOLD() \
	(GC_G(first_unused) < GC_G(gc_threshold))
#define GC_HAS_NEXT_UNUSED() \
	(GC_G(first_unused) != GC_G(buf_size))
#define GC_FETCH_NEXT_UNUSED() \
	gc_fetch_next_unused()

GEAR_API int (*gc_collect_cycles)(void);

typedef struct _gc_root_buffer {
	gear_refcounted  *ref;
} gc_root_buffer;

typedef struct _gear_gc_globals {
	gear_bool         gc_enabled;
	gear_bool         gc_active;        /* GC currently running, forbid nested GC */
	gear_bool         gc_protected;     /* GC protected, forbid root additions */
	gear_bool         gc_full;

	gc_root_buffer   *buf;				/* preallocated arrays of buffers   */
	uint32_t          unused;			/* linked list of unused buffers    */
	uint32_t          first_unused;		/* first unused buffer              */
	uint32_t          gc_threshold;     /* GC collection threshold          */
	uint32_t          buf_size;			/* size of the GC buffer            */
	uint32_t          num_roots;		/* number of roots in GC buffer     */

	uint32_t gc_runs;
	uint32_t collected;

#if GC_BENCH
	uint32_t root_buf_length;
	uint32_t root_buf_peak;
	uint32_t zval_possible_root;
	uint32_t zval_buffered;
	uint32_t zval_remove_from_buffer;
	uint32_t zval_marked_grey;
#endif
} gear_gc_globals;

#ifdef ZTS
static int gc_globals_id;
#define GC_G(v) GEAR_PBCG(gc_globals_id, gear_gc_globals *, v)
#else
#define GC_G(v) (gc_globals.v)
static gear_gc_globals gc_globals;
#endif

#if GC_BENCH
# define GC_BENCH_INC(counter) GC_G(counter)++
# define GC_BENCH_DEC(counter) GC_G(counter)--
# define GC_BENCH_PEAK(peak, counter) do {		\
		if (GC_G(counter) > GC_G(peak)) {		\
			GC_G(peak) = GC_G(counter);			\
		}										\
	} while (0)
#else
# define GC_BENCH_INC(counter)
# define GC_BENCH_DEC(counter)
# define GC_BENCH_PEAK(peak, counter)
#endif

#if GEAR_GC_DEBUG > 1
# define GC_TRACE(format, ...) fprintf(stderr, format "\n", ##__VA_ARGS__);
# define GC_TRACE_REF(ref, format, ...) \
	do { \
		gc_trace_ref((gear_refcounted *) ref); \
		fprintf(stderr, format "\n", ##__VA_ARGS__); \
	} while (0)
# define GC_TRACE_SET_COLOR(ref, color) \
	GC_TRACE_REF(ref, "->%s", gc_color_name(color))
#else
# define GC_TRACE_REF(ref, format, ...)
# define GC_TRACE_SET_COLOR(ref, new_color)
# define GC_TRACE(str)
#endif


#define GC_STACK_SEGMENT_SIZE (((4096 - GEAR_MM_OVERHEAD) / sizeof(void*)) - 2)

typedef struct _gc_stack gc_stack;

struct _gc_stack {
	gc_stack        *prev;
	gc_stack        *next;
	gear_refcounted *data[GC_STACK_SEGMENT_SIZE];
};

#define GC_STACK_DCL(init) \
	gc_stack *_stack = init; \
	size_t    _top = 0;

#define GC_STACK_PUSH(ref) \
	gc_stack_push(&_stack, &_top, ref);

#define GC_STACK_POP() \
	gc_stack_pop(&_stack, &_top)

static gear_never_inline gc_stack* gc_stack_next(gc_stack *stack)
{
	if (UNEXPECTED(!stack->next)) {
		gc_stack *segment = emalloc(sizeof(gc_stack));
		segment->prev = stack;
		segment->next = NULL;
		stack->next = segment;
	}
	return stack->next;
}

static gear_always_inline void gc_stack_push(gc_stack **stack, size_t *top, gear_refcounted *ref)
{
	if (UNEXPECTED(*top == GC_STACK_SEGMENT_SIZE)) {
		(*stack) = gc_stack_next(*stack);
		(*top) = 0;
	}
	(*stack)->data[(*top)++] = ref;
}

static gear_always_inline gear_refcounted* gc_stack_pop(gc_stack **stack, size_t *top)
{
	if (UNEXPECTED((*top) == 0)) {
		if (!(*stack)->prev) {
			return NULL;
		} else {
			(*stack) = (*stack)->prev;
			(*top) = GC_STACK_SEGMENT_SIZE - 1;
			return (*stack)->data[GC_STACK_SEGMENT_SIZE - 1];
		}
	} else {
		return (*stack)->data[--(*top)];
	}
}

static void gc_stack_free(gc_stack *stack)
{
	gc_stack *p = stack->next;

	while (p) {
		stack = p->next;
		efree(p);
		p = stack;
	}
}

static gear_always_inline uint32_t gc_compress(uint32_t idx)
{
	return idx % GC_MAX_UNCOMPRESSED;
}

static gear_always_inline gc_root_buffer* gc_decompress(gear_refcounted *ref, uint32_t idx)
{
	gc_root_buffer *root = GC_IDX2PTR(idx);

	if (EXPECTED(GC_GET_PTR(root->ref) == ref)) {
		return root;
	}

	while (1) {
		idx += GC_MAX_UNCOMPRESSED;
		GEAR_ASSERT(idx < GC_G(first_unused));
		root = GC_IDX2PTR(idx);
		if (GC_GET_PTR(root->ref) == ref) {
			return root;
		}
	}
}

static gear_always_inline uint32_t gc_fetch_unused(void)
{
	uint32_t idx;
	gc_root_buffer *root;

	GEAR_ASSERT(GC_HAS_UNUSED());
	idx = GC_G(unused);
	root = GC_IDX2PTR(idx);
	GEAR_ASSERT(GC_IS_UNUSED(root->ref));
	GC_G(unused) = GC_LIST2IDX(root->ref);
	return idx;
}

static gear_always_inline void gc_link_unused(gc_root_buffer *root)
{
	root->ref = GC_IDX2LIST(GC_G(unused));
	GC_G(unused) = GC_PTR2IDX(root);
}

static gear_always_inline uint32_t gc_fetch_next_unused(void)
{
	uint32_t idx;

	GEAR_ASSERT(GC_HAS_NEXT_UNUSED());
	idx = GC_G(first_unused);
	GC_G(first_unused) = GC_G(first_unused) + 1;
	return idx;
}

#if GEAR_GC_DEBUG > 1
static const char *gc_color_name(uint32_t color) {
	switch (color) {
		case GC_BLACK: return "black";
		case GC_WHITE: return "white";
		case GC_GREY: return "grey";
		case GC_PURPLE: return "purple";
		default: return "unknown";
	}
}
static void gc_trace_ref(gear_refcounted *ref) {
	if (GC_TYPE(ref) == IS_OBJECT) {
		gear_object *obj = (gear_object *) ref;
		fprintf(stderr, "[%p] rc=%d addr=%d %s object(%s)#%d ",
			ref, GC_REFCOUNT(ref), GC_REF_ADDRESS(ref),
			gc_color_name(GC_REF_COLOR(ref)),
			obj->ce->name->val, obj->handle);
	} else if (GC_TYPE(ref) == IS_ARRAY) {
		gear_array *arr = (gear_array *) ref;
		fprintf(stderr, "[%p] rc=%d addr=%d %s array(%d) ",
			ref, GC_REFCOUNT(ref), GC_REF_ADDRESS(ref),
			gc_color_name(GC_REF_COLOR(ref)),
			gear_hash_num_elements(arr));
	} else {
		fprintf(stderr, "[%p] rc=%d addr=%d %s %s ",
			ref, GC_REFCOUNT(ref), GC_REF_ADDRESS(ref),
			gc_color_name(GC_REF_COLOR(ref)),
			gear_get_type_by_const(GC_TYPE(ref)));
	}
}
#endif

static gear_always_inline void gc_remove_from_roots(gc_root_buffer *root)
{
	GC_LINK_UNUSED(root);
	GC_G(num_roots)--;
	GC_BENCH_DEC(root_buf_length);
}

static void root_buffer_dtor(gear_gc_globals *gc_globals)
{
	if (gc_globals->buf) {
		free(gc_globals->buf);
		gc_globals->buf = NULL;
	}
}

static void gc_globals_ctor_ex(gear_gc_globals *gc_globals)
{
	gc_globals->gc_enabled = 0;
	gc_globals->gc_active = 0;
	gc_globals->gc_protected = 1;
	gc_globals->gc_full = 0;

	gc_globals->buf = NULL;
	gc_globals->unused = GC_INVALID;
	gc_globals->first_unused = GC_INVALID;
	gc_globals->gc_threshold = GC_INVALID;
	gc_globals->buf_size = GC_INVALID;
	gc_globals->num_roots = 0;

	gc_globals->gc_runs = 0;
	gc_globals->collected = 0;

#if GC_BENCH
	gc_globals->root_buf_length = 0;
	gc_globals->root_buf_peak = 0;
	gc_globals->zval_possible_root = 0;
	gc_globals->zval_buffered = 0;
	gc_globals->zval_remove_from_buffer = 0;
	gc_globals->zval_marked_grey = 0;
#endif
}

void gc_globals_ctor(void)
{
#ifdef ZTS
	ts_allocate_id(&gc_globals_id, sizeof(gear_gc_globals), (ts_allocate_ctor) gc_globals_ctor_ex, (ts_allocate_dtor) root_buffer_dtor);
#else
	gc_globals_ctor_ex(&gc_globals);
#endif
}

void gc_globals_dtor(void)
{
#ifndef ZTS
	root_buffer_dtor(&gc_globals);
#endif
}

void gc_reset(void)
{
	if (GC_G(buf)) {
		GC_G(gc_active) = 0;
		GC_G(gc_protected) = 0;
		GC_G(gc_full) = 0;
		GC_G(unused) = GC_INVALID;
		GC_G(first_unused) = GC_FIRST_ROOT;
		GC_G(num_roots) = 0;

		GC_G(gc_runs) = 0;
		GC_G(collected) = 0;

#if GC_BENCH
		GC_G(root_buf_length) = 0;
		GC_G(root_buf_peak) = 0;
		GC_G(zval_possible_root) = 0;
		GC_G(zval_buffered) = 0;
		GC_G(zval_remove_from_buffer) = 0;
		GC_G(zval_marked_grey) = 0;
#endif
	}
}

GEAR_API gear_bool gc_enable(gear_bool enable)
{
	gear_bool old_enabled = GC_G(gc_enabled);
	GC_G(gc_enabled) = enable;
	if (enable && !old_enabled && GC_G(buf) == NULL) {
		GC_G(buf) = (gc_root_buffer*) pemalloc(sizeof(gc_root_buffer) * GC_DEFAULT_BUF_SIZE, 1);
		GC_G(buf)[0].ref = NULL;
		GC_G(buf_size) = GC_DEFAULT_BUF_SIZE;
		GC_G(gc_threshold) = GC_THRESHOLD_DEFAULT + GC_FIRST_ROOT;
		gc_reset();
	}
	return old_enabled;
}

GEAR_API gear_bool gc_enabled(void)
{
	return GC_G(gc_enabled);
}

GEAR_API gear_bool gc_protect(gear_bool protect)
{
	gear_bool old_protected = GC_G(gc_protected);
	GC_G(gc_protected) = protect;
	return old_protected;
}

GEAR_API gear_bool gc_protected(void)
{
	return GC_G(gc_protected);
}

static void gc_grow_root_buffer(void)
{
	size_t new_size;

	if (GC_G(buf_size) >= GC_MAX_BUF_SIZE) {
		if (!GC_G(gc_full)) {
			gear_error(E_WARNING, "GC buffer overflow (GC disabled)\n");
			GC_G(gc_active) = 1;
			GC_G(gc_protected) = 1;
			GC_G(gc_full) = 1;
			return;
		}
	}
	if (GC_G(buf_size) < GC_BUF_GROW_STEP) {
		new_size = GC_G(buf_size) * 2;
	} else {
		new_size = GC_G(buf_size) + GC_BUF_GROW_STEP;
	}
	if (new_size > GC_MAX_BUF_SIZE) {
		new_size = GC_MAX_BUF_SIZE;
	}
	GC_G(buf) = perealloc(GC_G(buf), sizeof(gc_root_buffer) * new_size, 1);
	GC_G(buf_size) = new_size;
}

static void gc_adjust_threshold(int count)
{
	uint32_t new_threshold;

	/* TODO Very simple heuristic for dynamic GC buffer resizing:
	 * If there are "too few" collections, increase the collection threshold
	 * by a fixed step */
	if (count < GC_THRESHOLD_TRIGGER) {
		/* increase */
		if (GC_G(gc_threshold) < GC_THRESHOLD_MAX) {
			new_threshold = GC_G(gc_threshold) + GC_THRESHOLD_STEP;
			if (new_threshold > GC_THRESHOLD_MAX) {
				new_threshold = GC_THRESHOLD_MAX;
			}
			if (new_threshold > GC_G(buf_size)) {
				gc_grow_root_buffer();
			}
			if (new_threshold <= GC_G(buf_size)) {
				GC_G(gc_threshold) = new_threshold;
			}
		}
	} else if (GC_G(gc_threshold) > GC_THRESHOLD_DEFAULT) {
		new_threshold = GC_G(gc_threshold) - GC_THRESHOLD_STEP;
		if (new_threshold < GC_THRESHOLD_DEFAULT) {
			new_threshold = GC_THRESHOLD_DEFAULT;
		}
		GC_G(gc_threshold) = new_threshold;
	}
}

static gear_never_inline void GEAR_FASTCALL gc_possible_root_when_full(gear_refcounted *ref)
{
	uint32_t idx;
	gc_root_buffer *newRoot;

	GEAR_ASSERT(GC_TYPE(ref) == IS_ARRAY || GC_TYPE(ref) == IS_OBJECT);
	GEAR_ASSERT(GC_INFO(ref) == 0);

	if (GC_G(gc_enabled) && !GC_G(gc_active)) {
		GC_ADDREF(ref);
		gc_adjust_threshold(gc_collect_cycles());
		if (UNEXPECTED(GC_DELREF(ref)) == 0) {
			rc_dtor_func(ref);
			return;
		} else if (UNEXPECTED(GC_INFO(ref))) {
			return;
		}
	}

	if (GC_HAS_UNUSED()) {
		idx = GC_FETCH_UNUSED();
	} else if (EXPECTED(GC_HAS_NEXT_UNUSED())) {
		idx = GC_FETCH_NEXT_UNUSED();
	} else {
		gc_grow_root_buffer();
		if (UNEXPECTED(!GC_HAS_NEXT_UNUSED())) {
			return;
		}
		idx = GC_FETCH_NEXT_UNUSED();
	}

	newRoot = GC_IDX2PTR(idx);
	newRoot->ref = ref; /* GC_ROOT tag is 0 */
	GC_TRACE_SET_COLOR(ref, GC_PURPLE);

	idx = gc_compress(idx);
	GC_REF_SET_INFO(ref, idx | GC_PURPLE);
	GC_G(num_roots)++;

	GC_BENCH_INC(zval_buffered);
	GC_BENCH_INC(root_buf_length);
	GC_BENCH_PEAK(root_buf_peak, root_buf_length);
}

GEAR_API void GEAR_FASTCALL gc_possible_root(gear_refcounted *ref)
{
	uint32_t idx;
	gc_root_buffer *newRoot;

	if (UNEXPECTED(GC_G(gc_protected))) {
		return;
	}

	GC_BENCH_INC(zval_possible_root);

	if (EXPECTED(GC_HAS_UNUSED())) {
		idx = GC_FETCH_UNUSED();
	} else if (EXPECTED(GC_HAS_NEXT_UNUSED_UNDER_THRESHOLD())) {
		idx = GC_FETCH_NEXT_UNUSED();
	} else {
		gc_possible_root_when_full(ref);
		return;
	}

	GEAR_ASSERT(GC_TYPE(ref) == IS_ARRAY || GC_TYPE(ref) == IS_OBJECT);
	GEAR_ASSERT(GC_INFO(ref) == 0);

	newRoot = GC_IDX2PTR(idx);
	newRoot->ref = ref; /* GC_ROOT tag is 0 */
	GC_TRACE_SET_COLOR(ref, GC_PURPLE);

	idx = gc_compress(idx);
	GC_REF_SET_INFO(ref, idx | GC_PURPLE);
	GC_G(num_roots)++;

	GC_BENCH_INC(zval_buffered);
	GC_BENCH_INC(root_buf_length);
	GC_BENCH_PEAK(root_buf_peak, root_buf_length);
}

static gear_never_inline void GEAR_FASTCALL gc_remove_compressed(gear_refcounted *ref, uint32_t idx)
{
	gc_root_buffer *root = gc_decompress(ref, idx);
	gc_remove_from_roots(root);
}

GEAR_API void GEAR_FASTCALL gc_remove_from_buffer(gear_refcounted *ref)
{
	gc_root_buffer *root;
	uint32_t idx = GC_REF_ADDRESS(ref);

	GC_BENCH_INC(zval_remove_from_buffer);

	if (!GC_REF_CHECK_COLOR(ref, GC_BLACK)) {
		GC_TRACE_SET_COLOR(ref, GC_BLACK);
	}
	GC_REF_SET_INFO(ref, 0);

	/* Perform decompression only in case of large buffers */
	if (UNEXPECTED(GC_G(first_unused) >= GC_MAX_UNCOMPRESSED)) {
		gc_remove_compressed(ref, idx);
		return;
	}

	GEAR_ASSERT(idx);
	root = GC_IDX2PTR(idx);
	gc_remove_from_roots(root);
}

static void gc_scan_black(gear_refcounted *ref, gc_stack *stack)
{
	HashTable *ht = NULL;
	Bucket *p, *end;
	zval *zv;
	GC_STACK_DCL(stack);

tail_call:
	if (GC_TYPE(ref) == IS_OBJECT) {
		gear_object_get_gc_t get_gc;
		gear_object *obj = (gear_object*)ref;

		if (EXPECTED(!(OBJ_FLAGS(ref) & IS_OBJ_FREE_CALLED) &&
		             (get_gc = obj->handlers->get_gc) != NULL)) {
			int n;
			zval *zv, *end;
			zval tmp;

			ZVAL_OBJ(&tmp, obj);
			ht = get_gc(&tmp, &zv, &n);
			end = zv + n;
			if (EXPECTED(!ht)) {
				if (!n) goto next;
				while (!Z_REFCOUNTED_P(--end)) {
					if (zv == end) goto next;
				}
			}
			while (zv != end) {
				if (Z_REFCOUNTED_P(zv)) {
					ref = Z_COUNTED_P(zv);
					GC_ADDREF(ref);
					if (!GC_REF_CHECK_COLOR(ref, GC_BLACK)) {
						GC_REF_SET_BLACK(ref);
						GC_STACK_PUSH(ref);
					}
				}
				zv++;
			}
			if (EXPECTED(!ht)) {
				ref = Z_COUNTED_P(zv);
				GC_ADDREF(ref);
				if (!GC_REF_CHECK_COLOR(ref, GC_BLACK)) {
					GC_REF_SET_BLACK(ref);
					goto tail_call;
				}
				goto next;
			}
		} else {
			goto next;
		}
	} else if (GC_TYPE(ref) == IS_ARRAY) {
		if ((gear_array*)ref != &EG(symbol_table)) {
			ht = (gear_array*)ref;
		} else {
			goto next;
		}
	} else if (GC_TYPE(ref) == IS_REFERENCE) {
		if (Z_REFCOUNTED(((gear_reference*)ref)->val)) {
			ref = Z_COUNTED(((gear_reference*)ref)->val);
			GC_ADDREF(ref);
			if (!GC_REF_CHECK_COLOR(ref, GC_BLACK)) {
				GC_REF_SET_BLACK(ref);
				goto tail_call;
			}
		}
		goto next;
	} else {
		goto next;
	}

	if (!ht->nNumUsed) goto next;
	p = ht->arData;
	end = p + ht->nNumUsed;
	while (1) {
		end--;
		zv = &end->val;
		if (Z_TYPE_P(zv) == IS_INDIRECT) {
			zv = Z_INDIRECT_P(zv);
		}
		if (Z_REFCOUNTED_P(zv)) {
			break;
		}
		if (p == end) goto next;
	}
	while (p != end) {
		zv = &p->val;
		if (Z_TYPE_P(zv) == IS_INDIRECT) {
			zv = Z_INDIRECT_P(zv);
		}
		if (Z_REFCOUNTED_P(zv)) {
			ref = Z_COUNTED_P(zv);
			GC_ADDREF(ref);
			if (!GC_REF_CHECK_COLOR(ref, GC_BLACK)) {
				GC_REF_SET_BLACK(ref);
				GC_STACK_PUSH(ref);
			}
		}
		p++;
	}
	zv = &p->val;
	if (Z_TYPE_P(zv) == IS_INDIRECT) {
		zv = Z_INDIRECT_P(zv);
	}
	ref = Z_COUNTED_P(zv);
	GC_ADDREF(ref);
	if (!GC_REF_CHECK_COLOR(ref, GC_BLACK)) {
		GC_REF_SET_BLACK(ref);
		goto tail_call;
	}

next:
	ref = GC_STACK_POP();
	if (ref) {
		goto tail_call;
	}
}

static void gc_mark_grey(gear_refcounted *ref, gc_stack *stack)
{
	HashTable *ht = NULL;
	Bucket *p, *end;
	zval *zv;
	GC_STACK_DCL(stack);

	do {
		GC_BENCH_INC(zval_marked_grey);

		if (GC_TYPE(ref) == IS_OBJECT) {
			gear_object_get_gc_t get_gc;
			gear_object *obj = (gear_object*)ref;

			if (EXPECTED(!(OBJ_FLAGS(ref) & IS_OBJ_FREE_CALLED) &&
						 (get_gc = obj->handlers->get_gc) != NULL)) {
				int n;
				zval *zv, *end;
				zval tmp;

				ZVAL_OBJ(&tmp, obj);
				ht = get_gc(&tmp, &zv, &n);
				end = zv + n;
				if (EXPECTED(!ht)) {
					if (!n) goto next;
					while (!Z_REFCOUNTED_P(--end)) {
						if (zv == end) goto next;
					}
				}
				while (zv != end) {
					if (Z_REFCOUNTED_P(zv)) {
						ref = Z_COUNTED_P(zv);
						GC_DELREF(ref);
						if (!GC_REF_CHECK_COLOR(ref, GC_GREY)) {
							GC_REF_SET_COLOR(ref, GC_GREY);
							GC_STACK_PUSH(ref);
						}
					}
					zv++;
				}
				if (EXPECTED(!ht)) {
					ref = Z_COUNTED_P(zv);
					GC_DELREF(ref);
					if (!GC_REF_CHECK_COLOR(ref, GC_GREY)) {
						GC_REF_SET_COLOR(ref, GC_GREY);
						continue;
					}
					goto next;
				}
			} else {
				goto next;
			}
		} else if (GC_TYPE(ref) == IS_ARRAY) {
			if (((gear_array*)ref) == &EG(symbol_table)) {
				GC_REF_SET_BLACK(ref);
				goto next;
			} else {
				ht = (gear_array*)ref;
			}
		} else if (GC_TYPE(ref) == IS_REFERENCE) {
			if (Z_REFCOUNTED(((gear_reference*)ref)->val)) {
				ref = Z_COUNTED(((gear_reference*)ref)->val);
				GC_DELREF(ref);
				if (!GC_REF_CHECK_COLOR(ref, GC_GREY)) {
					GC_REF_SET_COLOR(ref, GC_GREY);
					continue;
				}
			}
			goto next;
		} else {
			goto next;
		}

		if (!ht->nNumUsed) goto next;
		p = ht->arData;
		end = p + ht->nNumUsed;
		while (1) {
			end--;
			zv = &end->val;
			if (Z_TYPE_P(zv) == IS_INDIRECT) {
				zv = Z_INDIRECT_P(zv);
			}
			if (Z_REFCOUNTED_P(zv)) {
				break;
			}
			if (p == end) goto next;
		}
		while (p != end) {
			zv = &p->val;
			if (Z_TYPE_P(zv) == IS_INDIRECT) {
				zv = Z_INDIRECT_P(zv);
			}
			if (Z_REFCOUNTED_P(zv)) {
				ref = Z_COUNTED_P(zv);
				GC_DELREF(ref);
				if (!GC_REF_CHECK_COLOR(ref, GC_GREY)) {
					GC_REF_SET_COLOR(ref, GC_GREY);
					GC_STACK_PUSH(ref);
				}
			}
			p++;
		}
		zv = &p->val;
		if (Z_TYPE_P(zv) == IS_INDIRECT) {
			zv = Z_INDIRECT_P(zv);
		}
		ref = Z_COUNTED_P(zv);
		GC_DELREF(ref);
		if (!GC_REF_CHECK_COLOR(ref, GC_GREY)) {
			GC_REF_SET_COLOR(ref, GC_GREY);
			continue;
		}

next:
		ref = GC_STACK_POP();
	} while (ref);
}

/* Two-Finger compaction algorithm */
static void gc_compact(void)
{
	if (GC_G(num_roots) + GC_FIRST_ROOT != GC_G(first_unused)) {
		if (GC_G(num_roots)) {
			gc_root_buffer *free = GC_IDX2PTR(GC_FIRST_ROOT);
			gc_root_buffer *scan = GC_IDX2PTR(GC_G(first_unused) - 1);
			gc_root_buffer *end  = GC_IDX2PTR(GC_G(num_roots));
			uint32_t idx;
			gear_refcounted *p;

			while (free < scan) {
				while (!GC_IS_UNUSED(free->ref)) {
					free++;
				}
				while (GC_IS_UNUSED(scan->ref)) {
					scan--;
				}
				if (scan > free) {
					p = scan->ref;
					free->ref = p;
					p = GC_GET_PTR(p);
					idx = gc_compress(GC_PTR2IDX(free));
					GC_REF_SET_INFO(p, idx | GC_REF_COLOR(p));
					free++;
					scan--;
					if (scan <= end) {
						break;
					}
				}
			}
		}
		GC_G(unused) = GC_INVALID;
		GC_G(first_unused) = GC_G(num_roots) + GC_FIRST_ROOT;
	}
}

static void gc_mark_roots(gc_stack *stack)
{
	gc_root_buffer *current, *last;

	gc_compact();

	current = GC_IDX2PTR(GC_FIRST_ROOT);
	last = GC_IDX2PTR(GC_G(first_unused));
	while (current != last) {
		if (GC_IS_ROOT(current->ref)) {
			if (GC_REF_CHECK_COLOR(current->ref, GC_PURPLE)) {
				GC_REF_SET_COLOR(current->ref, GC_GREY);
				gc_mark_grey(current->ref, stack);
			}
		}
		current++;
	}
}

static void gc_scan(gear_refcounted *ref, gc_stack *stack)
{
	HashTable *ht = NULL;
	Bucket *p, *end;
	zval *zv;
	GC_STACK_DCL(stack);

tail_call:
	if (GC_REF_CHECK_COLOR(ref, GC_WHITE)) {
		if (GC_REFCOUNT(ref) > 0) {
			if (!GC_REF_CHECK_COLOR(ref, GC_BLACK)) {
				GC_REF_SET_BLACK(ref);
				if (UNEXPECTED(!_stack->next)) {
					gc_stack_next(_stack);
				}
				/* Split stack and reuse the tail */
				_stack->next->prev = NULL;
				gc_scan_black(ref, _stack->next);
				_stack->next->prev = _stack;
			}
		} else {
			if (GC_TYPE(ref) == IS_OBJECT) {
				gear_object_get_gc_t get_gc;
				gear_object *obj = (gear_object*)ref;

				if (EXPECTED(!(OBJ_FLAGS(ref) & IS_OBJ_FREE_CALLED) &&
							 (get_gc = obj->handlers->get_gc) != NULL)) {
					int n;
					zval *zv, *end;
					zval tmp;

					ZVAL_OBJ(&tmp, obj);
					ht = get_gc(&tmp, &zv, &n);
					end = zv + n;
					if (EXPECTED(!ht)) {
						if (!n) goto next;
						while (!Z_REFCOUNTED_P(--end)) {
							if (zv == end) goto next;
						}
					}
					while (zv != end) {
						if (Z_REFCOUNTED_P(zv)) {
							ref = Z_COUNTED_P(zv);
							if (GC_REF_CHECK_COLOR(ref, GC_GREY)) {
								GC_REF_SET_COLOR(ref, GC_WHITE);
								GC_STACK_PUSH(ref);
							}
						}
						zv++;
					}
					if (EXPECTED(!ht)) {
						ref = Z_COUNTED_P(zv);
						if (GC_REF_CHECK_COLOR(ref, GC_GREY)) {
							GC_REF_SET_COLOR(ref, GC_WHITE);
							goto tail_call;
						}
						goto next;
					}
				} else {
					goto next;
				}
			} else if (GC_TYPE(ref) == IS_ARRAY) {
				if ((gear_array*)ref == &EG(symbol_table)) {
					GC_REF_SET_BLACK(ref);
					goto next;
				} else {
					ht = (gear_array*)ref;
				}
			} else if (GC_TYPE(ref) == IS_REFERENCE) {
				if (Z_REFCOUNTED(((gear_reference*)ref)->val)) {
					ref = Z_COUNTED(((gear_reference*)ref)->val);
					if (GC_REF_CHECK_COLOR(ref, GC_GREY)) {
						GC_REF_SET_COLOR(ref, GC_WHITE);
						goto tail_call;
					}
				}
				goto next;
			} else {
				goto next;
			}

			if (!ht->nNumUsed) goto next;
			p = ht->arData;
			end = p + ht->nNumUsed;
			while (1) {
				end--;
				zv = &end->val;
				if (Z_TYPE_P(zv) == IS_INDIRECT) {
					zv = Z_INDIRECT_P(zv);
				}
				if (Z_REFCOUNTED_P(zv)) {
					break;
				}
				if (p == end) goto next;
			}
			while (p != end) {
				zv = &p->val;
				if (Z_TYPE_P(zv) == IS_INDIRECT) {
					zv = Z_INDIRECT_P(zv);
				}
				if (Z_REFCOUNTED_P(zv)) {
					ref = Z_COUNTED_P(zv);
					if (GC_REF_CHECK_COLOR(ref, GC_GREY)) {
						GC_REF_SET_COLOR(ref, GC_WHITE);
						GC_STACK_PUSH(ref);
					}
				}
				p++;
			}
			zv = &p->val;
			if (Z_TYPE_P(zv) == IS_INDIRECT) {
				zv = Z_INDIRECT_P(zv);
			}
			ref = Z_COUNTED_P(zv);
			if (GC_REF_CHECK_COLOR(ref, GC_GREY)) {
				GC_REF_SET_COLOR(ref, GC_WHITE);
				goto tail_call;
			}
		}
	}

next:
	ref = GC_STACK_POP();
	if (ref) {
		goto tail_call;
	}
}

static void gc_scan_roots(gc_stack *stack)
{
	gc_root_buffer *current = GC_IDX2PTR(GC_FIRST_ROOT);
	gc_root_buffer *last = GC_IDX2PTR(GC_G(first_unused));

	while (current != last) {
		if (GC_IS_ROOT(current->ref)) {
			if (GC_REF_CHECK_COLOR(current->ref, GC_GREY)) {
				GC_REF_SET_COLOR(current->ref, GC_WHITE);
				gc_scan(current->ref, stack);
			}
		}
		current++;
	}
}

static void gc_add_garbage(gear_refcounted *ref)
{
	uint32_t idx;
	gc_root_buffer *buf;

	if (GC_HAS_UNUSED()) {
		idx = GC_FETCH_UNUSED();
	} else if (GC_HAS_NEXT_UNUSED()) {
		idx = GC_FETCH_NEXT_UNUSED();
	} else {
		gc_grow_root_buffer();
		if (UNEXPECTED(!GC_HAS_NEXT_UNUSED())) {
			return;
		}
		idx = GC_FETCH_NEXT_UNUSED();
	}

	buf = GC_IDX2PTR(idx);
	buf->ref = GC_MAKE_GARBAGE(ref);

	idx = gc_compress(idx);
	GC_REF_SET_INFO(ref, idx | GC_BLACK);
	GC_G(num_roots)++;
}

static int gc_collect_white(gear_refcounted *ref, uint32_t *flags, gc_stack *stack)
{
	int count = 0;
	HashTable *ht = NULL;
	Bucket *p, *end;
	zval *zv;
	GC_STACK_DCL(stack);

	do {
		/* don't count references for compatibility ??? */
		if (GC_TYPE(ref) != IS_REFERENCE) {
			count++;
		}

		if (GC_TYPE(ref) == IS_OBJECT) {
			gear_object_get_gc_t get_gc;
			gear_object *obj = (gear_object*)ref;

			if (EXPECTED(!(OBJ_FLAGS(ref) & IS_OBJ_FREE_CALLED) &&
						 (get_gc = obj->handlers->get_gc) != NULL)) {
				int n;
				zval *zv, *end;
				zval tmp;

				/* optimization: color is GC_BLACK (0) */
				if (!GC_INFO(ref)) {
					gc_add_garbage(ref);
				}
				if (obj->handlers->dtor_obj &&
					((obj->handlers->dtor_obj != gear_objects_destroy_object) ||
					 (obj->ce->destructor != NULL))) {
					*flags |= GC_HAS_DESTRUCTORS;
				}
				ZVAL_OBJ(&tmp, obj);
				ht = get_gc(&tmp, &zv, &n);
				end = zv + n;
				if (EXPECTED(!ht)) {
					if (!n) goto next;
					while (!Z_REFCOUNTED_P(--end)) {
						/* count non-refcounted for compatibility ??? */
						if (Z_TYPE_P(zv) != IS_UNDEF) {
							count++;
						}
						if (zv == end) goto next;
					}
				}
				while (zv != end) {
					if (Z_REFCOUNTED_P(zv)) {
						ref = Z_COUNTED_P(zv);
						GC_ADDREF(ref);
						if (GC_REF_CHECK_COLOR(ref, GC_WHITE)) {
							GC_REF_SET_BLACK(ref);
							GC_STACK_PUSH(ref);
						}
					/* count non-refcounted for compatibility ??? */
					} else if (Z_TYPE_P(zv) != IS_UNDEF) {
						count++;
					}
					zv++;
				}
				if (EXPECTED(!ht)) {
					ref = Z_COUNTED_P(zv);
					GC_ADDREF(ref);
					if (GC_REF_CHECK_COLOR(ref, GC_WHITE)) {
						GC_REF_SET_BLACK(ref);
						continue;
					}
					goto next;
				}
			} else {
				goto next;
			}
		} else if (GC_TYPE(ref) == IS_ARRAY) {
			/* optimization: color is GC_BLACK (0) */
			if (!GC_INFO(ref)) {
				gc_add_garbage(ref);
			}
			ht = (gear_array*)ref;
		} else if (GC_TYPE(ref) == IS_REFERENCE) {
			if (Z_REFCOUNTED(((gear_reference*)ref)->val)) {
				ref = Z_COUNTED(((gear_reference*)ref)->val);
				GC_ADDREF(ref);
				if (GC_REF_CHECK_COLOR(ref, GC_WHITE)) {
					GC_REF_SET_BLACK(ref);
					continue;
				}
			}
			goto next;
		} else {
			goto next;
		}

		if (!ht->nNumUsed) goto next;
		p = ht->arData;
		end = p + ht->nNumUsed;
		while (1) {
			end--;
			zv = &end->val;
			if (Z_TYPE_P(zv) == IS_INDIRECT) {
				zv = Z_INDIRECT_P(zv);
			}
			if (Z_REFCOUNTED_P(zv)) {
				break;
			}
			/* count non-refcounted for compatibility ??? */
			if (Z_TYPE_P(zv) != IS_UNDEF) {
				count++;
			}
			if (p == end) goto next;
		}
		while (p != end) {
			zv = &p->val;
			if (Z_TYPE_P(zv) == IS_INDIRECT) {
				zv = Z_INDIRECT_P(zv);
			}
			if (Z_REFCOUNTED_P(zv)) {
				ref = Z_COUNTED_P(zv);
				GC_ADDREF(ref);
				if (GC_REF_CHECK_COLOR(ref, GC_WHITE)) {
					GC_REF_SET_BLACK(ref);
					GC_STACK_PUSH(ref);
				}
				/* count non-refcounted for compatibility ??? */
			} else if (Z_TYPE_P(zv) != IS_UNDEF) {
				count++;
			}
			p++;
		}
		zv = &p->val;
		if (Z_TYPE_P(zv) == IS_INDIRECT) {
			zv = Z_INDIRECT_P(zv);
		}
		ref = Z_COUNTED_P(zv);
		GC_ADDREF(ref);
		if (GC_REF_CHECK_COLOR(ref, GC_WHITE)) {
			GC_REF_SET_BLACK(ref);
			continue;
		}

next:
		ref = GC_STACK_POP();
	} while (ref);

	return count;
}

static int gc_collect_roots(uint32_t *flags, gc_stack *stack)
{
	uint32_t idx, end;
	gear_refcounted *ref;
	int count = 0;
	gc_root_buffer *current = GC_IDX2PTR(GC_FIRST_ROOT);
	gc_root_buffer *last = GC_IDX2PTR(GC_G(first_unused));

	/* remove non-garbage from the list */
	while (current != last) {
		if (GC_IS_ROOT(current->ref)) {
			if (GC_REF_CHECK_COLOR(current->ref, GC_BLACK)) {
				GC_REF_SET_INFO(current->ref, 0); /* reset GC_ADDRESS() and keep GC_BLACK */
				gc_remove_from_roots(current);
			}
		}
		current++;
	}

	gc_compact();

	/* Root buffer might be reallocated during gc_collect_white,
	 * make sure to reload pointers. */
	idx = GC_FIRST_ROOT;
	end = GC_G(first_unused);
	while (idx != end) {
		current = GC_IDX2PTR(idx);
		ref = current->ref;
		GEAR_ASSERT(GC_IS_ROOT(ref));
		current->ref = GC_MAKE_GARBAGE(ref);
		if (GC_REF_CHECK_COLOR(ref, GC_WHITE)) {
			GC_REF_SET_BLACK(ref);
			count += gc_collect_white(ref, flags, stack);
		}
		idx++;
	}

	return count;
}

static void gc_remove_nested_data_from_buffer(gear_refcounted *ref, gc_root_buffer *root)
{
	HashTable *ht = NULL;
	Bucket *p, *end;
	zval *zv;

tail_call:
	if (root ||
	    (GC_REF_ADDRESS(ref) != 0 &&
	     GC_REF_CHECK_COLOR(ref, GC_BLACK))) {
		GC_TRACE_REF(ref, "removing from buffer");
		if (root) {
			gc_remove_from_roots(root);
			GC_REF_SET_INFO(ref, 0);
			root = NULL;
		} else {
			GC_REMOVE_FROM_BUFFER(ref);
		}

		if (GC_TYPE(ref) == IS_OBJECT) {
			gear_object_get_gc_t get_gc;
			gear_object *obj = (gear_object*)ref;

			if (EXPECTED(!(OBJ_FLAGS(ref) & IS_OBJ_FREE_CALLED) &&
		                 (get_gc = obj->handlers->get_gc) != NULL)) {
				int n;
				zval *zv, *end;
				zval tmp;

				ZVAL_OBJ(&tmp, obj);
				ht = get_gc(&tmp, &zv, &n);
				end = zv + n;
				if (EXPECTED(!ht)) {
					if (!n) return;
					while (!Z_REFCOUNTED_P(--end)) {
						if (zv == end) return;
					}
				}
				while (zv != end) {
					if (Z_REFCOUNTED_P(zv)) {
						ref = Z_COUNTED_P(zv);
						gc_remove_nested_data_from_buffer(ref, NULL);
					}
					zv++;
				}
				if (EXPECTED(!ht)) {
					ref = Z_COUNTED_P(zv);
					goto tail_call;
				}
			} else {
				return;
			}
		} else if (GC_TYPE(ref) == IS_ARRAY) {
			ht = (gear_array*)ref;
		} else if (GC_TYPE(ref) == IS_REFERENCE) {
			if (Z_REFCOUNTED(((gear_reference*)ref)->val)) {
				ref = Z_COUNTED(((gear_reference*)ref)->val);
				goto tail_call;
			}
			return;
		} else {
			return;
		}

		if (!ht->nNumUsed) return;
		p = ht->arData;
		end = p + ht->nNumUsed;
		while (1) {
			end--;
			zv = &end->val;
			if (Z_TYPE_P(zv) == IS_INDIRECT) {
				zv = Z_INDIRECT_P(zv);
			}
			if (Z_REFCOUNTED_P(zv)) {
				break;
			}
			if (p == end) return;
		}
		while (p != end) {
			zv = &p->val;
			if (Z_TYPE_P(zv) == IS_INDIRECT) {
				zv = Z_INDIRECT_P(zv);
			}
			if (Z_REFCOUNTED_P(zv)) {
				ref = Z_COUNTED_P(zv);
				gc_remove_nested_data_from_buffer(ref, NULL);
			}
			p++;
		}
		zv = &p->val;
		if (Z_TYPE_P(zv) == IS_INDIRECT) {
			zv = Z_INDIRECT_P(zv);
		}
		ref = Z_COUNTED_P(zv);
		goto tail_call;
	}
}

GEAR_API int gear_gc_collect_cycles(void)
{
	int count = 0;

	if (GC_G(num_roots)) {
		gc_root_buffer *current, *last;
		gear_refcounted *p;
		uint32_t gc_flags = 0;
		uint32_t idx, end;
		gc_stack stack;

		stack.prev = NULL;
		stack.next = NULL;

		if (GC_G(gc_active)) {
			return 0;
		}

		GC_TRACE("Collecting cycles");
		GC_G(gc_runs)++;
		GC_G(gc_active) = 1;

		GC_TRACE("Marking roots");
		gc_mark_roots(&stack);
		GC_TRACE("Scanning roots");
		gc_scan_roots(&stack);

		GC_TRACE("Collecting roots");
		count = gc_collect_roots(&gc_flags, &stack);

		gc_stack_free(&stack);

		if (!GC_G(num_roots)) {
			/* nothing to free */
			GC_TRACE("Nothing to free");
			GC_G(gc_active) = 0;
			return 0;
		}

		end = GC_G(first_unused);

		if (gc_flags & GC_HAS_DESTRUCTORS) {
			uint32_t *refcounts;

			GC_TRACE("Calling destructors");

			// TODO: may be use emalloc() ???
			refcounts = pemalloc(sizeof(uint32_t) * end, 1);

			/* Remember reference counters before calling destructors */
			idx = GC_FIRST_ROOT;
			current = GC_IDX2PTR(GC_FIRST_ROOT);
			while (idx != end) {
				if (GC_IS_GARBAGE(current->ref)) {
					p = GC_GET_PTR(current->ref);
					refcounts[idx] = GC_REFCOUNT(p);
				}
				current++;
				idx++;
			}

			/* Call destructors
			 *
			 * The root buffer might be reallocated during destructors calls,
			 * make sure to reload pointers as necessary. */
			idx = GC_FIRST_ROOT;
			while (idx != end) {
				current = GC_IDX2PTR(idx);
				if (GC_IS_GARBAGE(current->ref)) {
					p = GC_GET_PTR(current->ref);
					if (GC_TYPE(p) == IS_OBJECT
					 && !(OBJ_FLAGS(p) & IS_OBJ_DESTRUCTOR_CALLED)) {
						gear_object *obj = (gear_object*)p;

						GC_TRACE_REF(obj, "calling destructor");
						GC_ADD_FLAGS(obj, IS_OBJ_DESTRUCTOR_CALLED);
						if (obj->handlers->dtor_obj
						 && (obj->handlers->dtor_obj != gear_objects_destroy_object
						  || obj->ce->destructor)) {
							GC_ADDREF(obj);
							obj->handlers->dtor_obj(obj);
							GC_DELREF(obj);
						}
					}
				}
				idx++;
			}

			/* Remove values captured in destructors */
			idx = GC_FIRST_ROOT;
			current = GC_IDX2PTR(GC_FIRST_ROOT);
			while (idx != end) {
				if (GC_IS_GARBAGE(current->ref)) {
					p = GC_GET_PTR(current->ref);
					if (GC_REFCOUNT(p) > refcounts[idx]) {
						gc_remove_nested_data_from_buffer(p, current);
					}
				}
				current++;
				idx++;
			}

			pefree(refcounts, 1);

			if (GC_G(gc_protected)) {
				/* something went wrong */
				return 0;
			}
		}

		/* Destroy zvals */
		GC_TRACE("Destroying zvals");
		GC_G(gc_protected) = 1;
		current = GC_IDX2PTR(GC_FIRST_ROOT);
		last = GC_IDX2PTR(GC_G(first_unused));
		while (current != last) {
			if (GC_IS_GARBAGE(current->ref)) {
				p = GC_GET_PTR(current->ref);
				GC_TRACE_REF(p, "destroying");
				if (GC_TYPE(p) == IS_OBJECT) {
					gear_object *obj = (gear_object*)p;

					EG(objects_store).object_buckets[obj->handle] = SET_OBJ_INVALID(obj);
					GC_TYPE_INFO(obj) = IS_NULL |
						(GC_TYPE_INFO(obj) & ~GC_TYPE_MASK);
					if (!(OBJ_FLAGS(obj) & IS_OBJ_FREE_CALLED)) {
						GC_ADD_FLAGS(obj, IS_OBJ_FREE_CALLED);
						if (obj->handlers->free_obj) {
							GC_ADDREF(obj);
							obj->handlers->free_obj(obj);
							GC_DELREF(obj);
						}
					}

					GEAR_OBJECTS_STORE_ADD_TO_FREE_LIST(obj->handle);
					current->ref = GC_MAKE_GARBAGE(((char*)obj) - obj->handlers->offset);
				} else if (GC_TYPE(p) == IS_ARRAY) {
					gear_array *arr = (gear_array*)p;

					GC_TYPE_INFO(arr) = IS_NULL |
						(GC_TYPE_INFO(arr) & ~GC_TYPE_MASK);

					/* GC may destroy arrays with rc>1. This is valid and safe. */
					HT_ALLOW_COW_VIOLATION(arr);

					gear_hash_destroy(arr);
				}
			}
			current++;
		}

		/* Free objects */
		current = GC_IDX2PTR(GC_FIRST_ROOT);
		while (current != last) {
			if (GC_IS_GARBAGE(current->ref)) {
				p = GC_GET_PTR(current->ref);
				GC_LINK_UNUSED(current);
				GC_G(num_roots)--;
				efree(p);
			}
			current++;
		}

		GC_TRACE("Collection finished");
		GC_G(collected) += count;
		GC_G(gc_protected) = 0;
		GC_G(gc_active) = 0;
	}

	gc_compact();

	return count;
}

GEAR_API void gear_gc_get_status(gear_gc_status *status)
{
	status->runs = GC_G(gc_runs);
	status->collected = GC_G(collected);
	status->threshold = GC_G(gc_threshold);
	status->num_roots = GC_G(num_roots);
}

