/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_EXECUTE_H
#define GEAR_EXECUTE_H

#include "gear_compile.h"
#include "gear_hash.h"
#include "gear_operators.h"
#include "gear_variables.h"

BEGIN_EXTERN_C()
struct _gear_fcall_info;
GEAR_API extern void (*gear_execute_ex)(gear_execute_data *execute_data);
GEAR_API extern void (*gear_execute_internal)(gear_execute_data *execute_data, zval *return_value);

void init_executor(void);
void shutdown_executor(void);
void shutdown_destructors(void);
GEAR_API void gear_init_execute_data(gear_execute_data *execute_data, gear_op_array *op_array, zval *return_value);
GEAR_API void gear_init_func_execute_data(gear_execute_data *execute_data, gear_op_array *op_array, zval *return_value);
GEAR_API void gear_init_code_execute_data(gear_execute_data *execute_data, gear_op_array *op_array, zval *return_value);
GEAR_API void gear_execute(gear_op_array *op_array, zval *return_value);
GEAR_API void execute_ex(gear_execute_data *execute_data);
GEAR_API void execute_internal(gear_execute_data *execute_data, zval *return_value);
GEAR_API gear_class_entry *gear_lookup_class(gear_string *name);
GEAR_API gear_class_entry *gear_lookup_class_ex(gear_string *name, const zval *key, int use_autoload);
GEAR_API gear_class_entry *gear_get_called_scope(gear_execute_data *ex);
GEAR_API gear_object *gear_get_this_object(gear_execute_data *ex);
GEAR_API int gear_eval_string(char *str, zval *retval_ptr, char *string_name);
GEAR_API int gear_eval_stringl(char *str, size_t str_len, zval *retval_ptr, char *string_name);
GEAR_API int gear_eval_string_ex(char *str, zval *retval_ptr, char *string_name, int handle_exceptions);
GEAR_API int gear_eval_stringl_ex(char *str, size_t str_len, zval *retval_ptr, char *string_name, int handle_exceptions);

/* export gear_pass_function to allow comparisons against it */
extern GEAR_API const gear_internal_function gear_pass_function;

GEAR_API void GEAR_FASTCALL gear_check_internal_arg_type(gear_function *zf, uint32_t arg_num, zval *arg);
GEAR_API int  GEAR_FASTCALL gear_check_arg_type(gear_function *zf, uint32_t arg_num, zval *arg, zval *default_value, void **cache_slot);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_missing_arg_error(gear_execute_data *execute_data);

static gear_always_inline zval* gear_assign_to_variable(zval *variable_ptr, zval *value, gear_uchar value_type)
{
	gear_refcounted *ref = NULL;

	if (GEAR_CONST_COND(value_type & (IS_VAR|IS_CV), 1) && Z_ISREF_P(value)) {
		ref = Z_COUNTED_P(value);
		value = Z_REFVAL_P(value);
	}

	do {
		if (UNEXPECTED(Z_REFCOUNTED_P(variable_ptr))) {
			gear_refcounted *garbage;

			if (Z_ISREF_P(variable_ptr)) {
				variable_ptr = Z_REFVAL_P(variable_ptr);
				if (EXPECTED(!Z_REFCOUNTED_P(variable_ptr))) {
					break;
				}
			}
			if (Z_TYPE_P(variable_ptr) == IS_OBJECT &&
	    		UNEXPECTED(Z_OBJ_HANDLER_P(variable_ptr, set) != NULL)) {
				Z_OBJ_HANDLER_P(variable_ptr, set)(variable_ptr, value);
				return variable_ptr;
			}
			if (GEAR_CONST_COND(value_type & (IS_VAR|IS_CV), 1) && variable_ptr == value) {
				if (value_type == IS_VAR && ref) {
					GEAR_ASSERT(GC_REFCOUNT(ref) > 1);
					GC_DELREF(ref);
				}
				return variable_ptr;
			}
			garbage = Z_COUNTED_P(variable_ptr);
			if (GC_DELREF(garbage) == 0) {
				ZVAL_COPY_VALUE(variable_ptr, value);
				if (GEAR_CONST_COND(value_type  == IS_CONST, 0)) {
					if (UNEXPECTED(Z_OPT_REFCOUNTED_P(variable_ptr))) {
						Z_ADDREF_P(variable_ptr);
					}
				} else if (value_type & (IS_CONST|IS_CV)) {
					if (Z_OPT_REFCOUNTED_P(variable_ptr)) {
						Z_ADDREF_P(variable_ptr);
					}
				} else if (GEAR_CONST_COND(value_type == IS_VAR, 1) && UNEXPECTED(ref)) {
					if (UNEXPECTED(GC_DELREF(ref) == 0)) {
						efree_size(ref, sizeof(gear_reference));
					} else if (Z_OPT_REFCOUNTED_P(variable_ptr)) {
						Z_ADDREF_P(variable_ptr);
					}
				}
				rc_dtor_func(garbage);
				return variable_ptr;
			} else { /* we need to split */
				/* optimized version of GC_ZVAL_CHECK_POSSIBLE_ROOT(variable_ptr) */
				if (UNEXPECTED(GC_MAY_LEAK(garbage))) {
					gc_possible_root(garbage);
				}
			}
		}
	} while (0);

	ZVAL_COPY_VALUE(variable_ptr, value);
	if (GEAR_CONST_COND(value_type == IS_CONST, 0)) {
		if (UNEXPECTED(Z_OPT_REFCOUNTED_P(variable_ptr))) {
			Z_ADDREF_P(variable_ptr);
		}
	} else if (value_type & (IS_CONST|IS_CV)) {
		if (Z_OPT_REFCOUNTED_P(variable_ptr)) {
			Z_ADDREF_P(variable_ptr);
		}
	} else if (GEAR_CONST_COND(value_type == IS_VAR, 1) && UNEXPECTED(ref)) {
		if (UNEXPECTED(GC_DELREF(ref) == 0)) {
			efree_size(ref, sizeof(gear_reference));
		} else if (Z_OPT_REFCOUNTED_P(variable_ptr)) {
			Z_ADDREF_P(variable_ptr);
		}
	}
	return variable_ptr;
}

GEAR_API int zval_update_constant(zval *pp);
GEAR_API int zval_update_constant_ex(zval *pp, gear_class_entry *scope);
GEAR_API int gear_use_undefined_constant(gear_string *name, gear_ast_attr attr, zval *result);

/* dedicated Gear executor functions - do not use! */
struct _gear_vm_stack {
	zval *top;
	zval *end;
	gear_vm_stack prev;
};

#define GEAR_VM_STACK_HEADER_SLOTS \
	((GEAR_MM_ALIGNED_SIZE(sizeof(struct _gear_vm_stack)) + GEAR_MM_ALIGNED_SIZE(sizeof(zval)) - 1) / GEAR_MM_ALIGNED_SIZE(sizeof(zval)))

#define GEAR_VM_STACK_ELEMENTS(stack) \
	(((zval*)(stack)) + GEAR_VM_STACK_HEADER_SLOTS)

/*
 * In general in RELEASE build GEAR_ASSERT() must be zero-cost, but for some
 * reason, GCC generated worse code, performing CSE on assertion code and the
 * following "slow path" and moving memory read operatins from slow path into
 * common header. This made a degradation for the fast path.
 * The following "#if GEAR_DEBUG" eliminates it.
 */
#if GEAR_DEBUG
# define GEAR_ASSERT_VM_STACK(stack) GEAR_ASSERT(stack->top > (zval *) stack && stack->end > (zval *) stack && stack->top <= stack->end)
# define GEAR_ASSERT_VM_STACK_GLOBAL GEAR_ASSERT(EG(vm_stack_top) > (zval *) EG(vm_stack) && EG(vm_stack_end) > (zval *) EG(vm_stack) && EG(vm_stack_top) <= EG(vm_stack_end))
#else
# define GEAR_ASSERT_VM_STACK(stack)
# define GEAR_ASSERT_VM_STACK_GLOBAL
#endif

GEAR_API void gear_vm_stack_init(void);
GEAR_API void gear_vm_stack_init_ex(size_t page_size);
GEAR_API void gear_vm_stack_destroy(void);
GEAR_API void* gear_vm_stack_extend(size_t size);

static gear_always_inline void gear_vm_init_call_frame(gear_execute_data *call, uint32_t call_info, gear_function *func, uint32_t num_args, gear_class_entry *called_scope, gear_object *object)
{
	call->func = func;
	if (object) {
		Z_OBJ(call->This) = object;
		GEAR_SET_CALL_INFO(call, 1, call_info);
	} else {
		Z_CE(call->This) = called_scope;
		GEAR_SET_CALL_INFO(call, 0, call_info);
	}
	GEAR_CALL_NUM_ARGS(call) = num_args;
}

static gear_always_inline gear_execute_data *gear_vm_stack_push_call_frame_ex(uint32_t used_stack, uint32_t call_info, gear_function *func, uint32_t num_args, gear_class_entry *called_scope, gear_object *object)
{
	gear_execute_data *call = (gear_execute_data*)EG(vm_stack_top);

	GEAR_ASSERT_VM_STACK_GLOBAL;

	if (UNEXPECTED(used_stack > (size_t)(((char*)EG(vm_stack_end)) - (char*)call))) {
		call = (gear_execute_data*)gear_vm_stack_extend(used_stack);
		GEAR_ASSERT_VM_STACK_GLOBAL;
		gear_vm_init_call_frame(call, call_info | GEAR_CALL_ALLOCATED, func, num_args, called_scope, object);
		return call;
	} else {
		EG(vm_stack_top) = (zval*)((char*)call + used_stack);
		gear_vm_init_call_frame(call, call_info, func, num_args, called_scope, object);
		return call;
	}
}

static gear_always_inline uint32_t gear_vm_calc_used_stack(uint32_t num_args, gear_function *func)
{
	uint32_t used_stack = GEAR_CALL_FRAME_SLOT + num_args;

	if (EXPECTED(GEAR_USER_CODE(func->type))) {
		used_stack += func->op_array.last_var + func->op_array.T - MIN(func->op_array.num_args, num_args);
	}
	return used_stack * sizeof(zval);
}

static gear_always_inline gear_execute_data *gear_vm_stack_push_call_frame(uint32_t call_info, gear_function *func, uint32_t num_args, gear_class_entry *called_scope, gear_object *object)
{
	uint32_t used_stack = gear_vm_calc_used_stack(num_args, func);

	return gear_vm_stack_push_call_frame_ex(used_stack, call_info,
		func, num_args, called_scope, object);
}

static gear_always_inline void gear_vm_stack_free_extra_args_ex(uint32_t call_info, gear_execute_data *call)
{
	if (UNEXPECTED(call_info & GEAR_CALL_FREE_EXTRA_ARGS)) {
		uint32_t count = GEAR_CALL_NUM_ARGS(call) - call->func->op_array.num_args;
		zval *p = GEAR_CALL_VAR_NUM(call, call->func->op_array.last_var + call->func->op_array.T);
		do {
			if (Z_REFCOUNTED_P(p)) {
				gear_refcounted *r = Z_COUNTED_P(p);
				if (!GC_DELREF(r)) {
					ZVAL_NULL(p);
					rc_dtor_func(r);
				} else {
					gc_check_possible_root(r);
				}
			}
			p++;
		} while (--count);
 	}
}

static gear_always_inline void gear_vm_stack_free_extra_args(gear_execute_data *call)
{
	gear_vm_stack_free_extra_args_ex(GEAR_CALL_INFO(call), call);
}

static gear_always_inline void gear_vm_stack_free_args(gear_execute_data *call)
{
	uint32_t num_args = GEAR_CALL_NUM_ARGS(call);

	if (EXPECTED(num_args > 0)) {
		zval *p = GEAR_CALL_ARG(call, 1);

		do {
			if (Z_REFCOUNTED_P(p)) {
				gear_refcounted *r = Z_COUNTED_P(p);
				if (!GC_DELREF(r)) {
					ZVAL_NULL(p);
					rc_dtor_func(r);
				}
			}
			p++;
		} while (--num_args);
	}
}

static gear_always_inline void gear_vm_stack_free_call_frame_ex(uint32_t call_info, gear_execute_data *call)
{
	GEAR_ASSERT_VM_STACK_GLOBAL;

	if (UNEXPECTED(call_info & GEAR_CALL_ALLOCATED)) {
		gear_vm_stack p = EG(vm_stack);
		gear_vm_stack prev = p->prev;

		GEAR_ASSERT(call == (gear_execute_data*)GEAR_VM_STACK_ELEMENTS(EG(vm_stack)));
		EG(vm_stack_top) = prev->top;
		EG(vm_stack_end) = prev->end;
		EG(vm_stack) = prev;
		efree(p);
	} else {
		EG(vm_stack_top) = (zval*)call;
	}

	GEAR_ASSERT_VM_STACK_GLOBAL;
}

static gear_always_inline void gear_vm_stack_free_call_frame(gear_execute_data *call)
{
	gear_vm_stack_free_call_frame_ex(GEAR_CALL_INFO(call), call);
}

/* services */
GEAR_API const char *get_active_class_name(const char **space);
GEAR_API const char *get_active_function_name(void);
GEAR_API const char *gear_get_executed_filename(void);
GEAR_API gear_string *gear_get_executed_filename_ex(void);
GEAR_API uint32_t gear_get_executed_lineno(void);
GEAR_API gear_class_entry *gear_get_executed_scope(void);
GEAR_API gear_bool gear_is_executing(void);

GEAR_API void gear_set_timeout(gear_long seconds, int reset_signals);
GEAR_API void gear_unset_timeout(void);
GEAR_API GEAR_NORETURN void gear_timeout(int dummy);
GEAR_API gear_class_entry *gear_fetch_class(gear_string *class_name, int fetch_type);
GEAR_API gear_class_entry *gear_fetch_class_by_name(gear_string *class_name, const zval *key, int fetch_type);
void gear_verify_abstract_class(gear_class_entry *ce);

GEAR_API gear_function * GEAR_FASTCALL gear_fetch_function(gear_string *name);
GEAR_API gear_function * GEAR_FASTCALL gear_fetch_function_str(const char *name, size_t len);

GEAR_API void gear_fetch_dimension_const(zval *result, zval *container, zval *dim, int type);

GEAR_API zval* gear_get_compiled_variable_value(const gear_execute_data *execute_data_ptr, uint32_t var);

#define GEAR_USER_OPCODE_CONTINUE   0 /* execute next opcode */
#define GEAR_USER_OPCODE_RETURN     1 /* exit from executor (return from function) */
#define GEAR_USER_OPCODE_DISPATCH   2 /* call original opcode handler */
#define GEAR_USER_OPCODE_ENTER      3 /* enter into new op_array without recursion */
#define GEAR_USER_OPCODE_LEAVE      4 /* return to calling op_array within the same executor */

#define GEAR_USER_OPCODE_DISPATCH_TO 0x100 /* call original handler of returned opcode */

GEAR_API int gear_set_user_opcode_handler(gear_uchar opcode, user_opcode_handler_t handler);
GEAR_API user_opcode_handler_t gear_get_user_opcode_handler(gear_uchar opcode);

/* former gear_execute_locks.h */
typedef zval* gear_free_op;

GEAR_API zval *gear_get_zval_ptr(const gear_op *opline, int op_type, const znode_op *node, const gear_execute_data *execute_data, gear_free_op *should_free, int type);

GEAR_API void gear_clean_and_cache_symbol_table(gear_array *symbol_table);
GEAR_API void gear_free_compiled_variables(gear_execute_data *execute_data);
GEAR_API void gear_cleanup_unfinished_execution(gear_execute_data *execute_data, uint32_t op_num, uint32_t catch_op_num);

GEAR_API int GEAR_FASTCALL gear_do_fcall_overloaded(gear_execute_data *call, zval *ret);

#define CACHE_ADDR(num) \
	((void**)((char*)EX_RUN_TIME_CACHE() + (num)))

#define CACHED_PTR(num) \
	((void**)((char*)EX_RUN_TIME_CACHE() + (num)))[0]

#define CACHE_PTR(num, ptr) do { \
		((void**)((char*)EX_RUN_TIME_CACHE() + (num)))[0] = (ptr); \
	} while (0)

#define CACHED_POLYMORPHIC_PTR(num, ce) \
	(EXPECTED(((void**)((char*)EX_RUN_TIME_CACHE() + (num)))[0] == (void*)(ce)) ? \
		((void**)((char*)EX_RUN_TIME_CACHE() + (num)))[1] : \
		NULL)

#define CACHE_POLYMORPHIC_PTR(num, ce, ptr) do { \
		void **slot = (void**)((char*)EX_RUN_TIME_CACHE() + (num)); \
		slot[0] = (ce); \
		slot[1] = (ptr); \
	} while (0)

#define CACHED_PTR_EX(slot) \
	(slot)[0]

#define CACHE_PTR_EX(slot, ptr) do { \
		(slot)[0] = (ptr); \
	} while (0)

#define CACHED_POLYMORPHIC_PTR_EX(slot, ce) \
	(EXPECTED((slot)[0] == (ce)) ? (slot)[1] : NULL)

#define CACHE_POLYMORPHIC_PTR_EX(slot, ce, ptr) do { \
		(slot)[0] = (ce); \
		(slot)[1] = (ptr); \
	} while (0)

#define CACHE_SPECIAL (1<<0)

#define IS_SPECIAL_CACHE_VAL(ptr) \
	(((uintptr_t)(ptr)) & CACHE_SPECIAL)

#define ENCODE_SPECIAL_CACHE_NUM(num) \
	((void*)((((uintptr_t)(num)) << 1) | CACHE_SPECIAL))

#define DECODE_SPECIAL_CACHE_NUM(ptr) \
	(((uintptr_t)(ptr)) >> 1)

#define ENCODE_SPECIAL_CACHE_PTR(ptr) \
	((void*)(((uintptr_t)(ptr)) | CACHE_SPECIAL))

#define DECODE_SPECIAL_CACHE_PTR(ptr) \
	((void*)(((uintptr_t)(ptr)) & ~CACHE_SPECIAL))

#define SKIP_EXT_OPLINE(opline) do { \
		while (UNEXPECTED((opline)->opcode >= GEAR_EXT_STMT \
			&& (opline)->opcode <= GEAR_TICKS)) {     \
			(opline)--;                                  \
		}                                                \
	} while (0)

END_EXTERN_C()

#endif /* GEAR_EXECUTE_H */

