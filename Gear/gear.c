/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_extensions.h"
#include "gear_capis.h"
#include "gear_constants.h"
#include "gear_list.h"
#include "gear_API.h"
#include "gear_exceptions.h"
#include "gear_builtin_functions.h"
#include "gear_ics.h"
#include "gear_vm.h"
#include "gear_dtrace.h"
#include "gear_virtual_cwd.h"
#include "gear_smart_str.h"
#include "gear_smart_string.h"
#include "gear_cpuinfo.h"

#ifdef ZTS
GEAR_API int compiler_globals_id;
GEAR_API int executor_globals_id;
static HashTable *global_function_table = NULL;
static HashTable *global_class_table = NULL;
static HashTable *global_constants_table = NULL;
static HashTable *global_auto_globals_table = NULL;
static HashTable *global_persistent_list = NULL;
GEAR_PBCLS_CACHE_DEFINE()
# define GLOBAL_FUNCTION_TABLE		global_function_table
# define GLOBAL_CLASS_TABLE			global_class_table
# define GLOBAL_CONSTANTS_TABLE		global_constants_table
# define GLOBAL_AUTO_GLOBALS_TABLE	global_auto_globals_table
#else
# define GLOBAL_FUNCTION_TABLE		CG(function_table)
# define GLOBAL_CLASS_TABLE			CG(class_table)
# define GLOBAL_AUTO_GLOBALS_TABLE	CG(auto_globals)
# define GLOBAL_CONSTANTS_TABLE		EG(gear_constants)
#endif

GEAR_API gear_utility_values gear_uv;
GEAR_API gear_bool gear_dtrace_enabled;

/* version information */
static char *gear_version_info;
static uint32_t gear_version_info_length;
#define GEAR_CORE_VERSION_INFO	"Gear Engine v" GEAR_VERSION ", Copyright (c) 2017-2019 The Hyang Language Foundation\n"
#define PRINT_ZVAL_INDENT 4

/* true multithread-shared globals */
GEAR_API gear_class_entry *gear_standard_class_def = NULL;
GEAR_API size_t (*gear_printf)(const char *format, ...);
GEAR_API gear_write_func_t gear_write;
GEAR_API FILE *(*gear_fopen)(const char *filename, gear_string **opened_path);
GEAR_API int (*gear_stream_open_function)(const char *filename, gear_file_handle *handle);
GEAR_API void (*gear_ticks_function)(int ticks);
GEAR_API void (*gear_interrupt_function)(gear_execute_data *execute_data);
GEAR_API void (*gear_error_cb)(int type, const char *error_filename, const uint32_t error_lineno, const char *format, va_list args);
void (*gear_printf_to_smart_string)(smart_string *buf, const char *format, va_list ap);
void (*gear_printf_to_smart_str)(smart_str *buf, const char *format, va_list ap);
GEAR_API char *(*gear_getenv)(char *name, size_t name_len);
GEAR_API gear_string *(*gear_resolve_path)(const char *filename, size_t filename_len);
GEAR_API int (*gear_post_startup_cb)(void) = NULL;

void (*gear_on_timeout)(int seconds);

static void (*gear_message_dispatcher_p)(gear_long message, const void *data);
static zval *(*gear_get_configuration_directive_p)(gear_string *name);

#if GEAR_RC_DEBUG
GEAR_API gear_bool gear_rc_debug = 0;
#endif

static GEAR_ICS_MH(OnUpdateErrorReporting) /* {{{ */
{
	if (!new_value) {
		EG(error_reporting) = E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED;
	} else {
		EG(error_reporting) = atoi(ZSTR_VAL(new_value));
	}
	return SUCCESS;
}
/* }}} */

static GEAR_ICS_MH(OnUpdateGCEnabled) /* {{{ */
{
	gear_bool val;

	val = gear_ics_parse_bool(new_value);
	gc_enable(val);

	return SUCCESS;
}
/* }}} */

static GEAR_ICS_DISP(gear_gc_enabled_displayer_cb) /* {{{ */
{
	if (gc_enabled()) {
		GEAR_PUTS("On");
	} else {
		GEAR_PUTS("Off");
	}
}
/* }}} */


static GEAR_ICS_MH(OnUpdateScriptEncoding) /* {{{ */
{
	if (!CG(multibyte)) {
		return FAILURE;
	}
	if (!gear_multibyte_get_functions()) {
		return SUCCESS;
	}
	return gear_multibyte_set_script_encoding_by_string(new_value ? ZSTR_VAL(new_value) : NULL, new_value ? ZSTR_LEN(new_value) : 0);
}
/* }}} */

static GEAR_ICS_MH(OnUpdateAssertions) /* {{{ */
{
	gear_long *p, val;
#ifndef ZTS
	char *base = (char *) mh_arg2;
#else
	char *base;

	base = (char *) ts_resource(*((int *) mh_arg2));
#endif

	p = (gear_long *) (base+(size_t) mh_arg1);

	val = gear_atol(ZSTR_VAL(new_value), ZSTR_LEN(new_value));

	if (stage != GEAR_ICS_STAGE_STARTUP &&
	    stage != GEAR_ICS_STAGE_SHUTDOWN &&
	    *p != val &&
	    (*p < 0 || val < 0)) {
		gear_error(E_WARNING, "gear.assertions may be completely enabled or disabled only in hyss.ics");
		return FAILURE;
	}

	*p = val;
	return SUCCESS;
}
/* }}} */

GEAR_ICS_BEGIN()
	GEAR_ICS_ENTRY("error_reporting",				NULL,		GEAR_ICS_ALL,		OnUpdateErrorReporting)
	STD_GEAR_ICS_ENTRY("gear.assertions",				"1",    GEAR_ICS_ALL,       OnUpdateAssertions,           assertions,   gear_executor_globals,  executor_globals)
	GEAR_ICS_ENTRY3_EX("gear.enable_gc",				"1",	GEAR_ICS_ALL,		OnUpdateGCEnabled, NULL, NULL, NULL, gear_gc_enabled_displayer_cb)
 	STD_GEAR_ICS_BOOLEAN("gear.multibyte", "0", GEAR_ICS_PERDIR, OnUpdateBool, multibyte,      gear_compiler_globals, compiler_globals)
 	GEAR_ICS_ENTRY("gear.script_encoding",			NULL,		GEAR_ICS_ALL,		OnUpdateScriptEncoding)
 	STD_GEAR_ICS_BOOLEAN("gear.detect_unicode",			"1",	GEAR_ICS_ALL,		OnUpdateBool, detect_unicode, gear_compiler_globals, compiler_globals)
#ifdef GEAR_SIGNALS
	STD_GEAR_ICS_BOOLEAN("gear.signal_check", "0", GEAR_ICS_SYSTEM, OnUpdateBool, check, gear_signal_globals_t, gear_signal_globals)
#endif
GEAR_ICS_END()

GEAR_API size_t gear_vspprintf(char **pbuf, size_t max_len, const char *format, va_list ap) /* {{{ */
{
	smart_string buf = {0};

	/* since there are places where (v)spprintf called without checking for null,
	   a bit of defensive coding here */
	if (!pbuf) {
		return 0;
	}

	gear_printf_to_smart_string(&buf, format, ap);

	if (max_len && buf.len > max_len) {
		buf.len = max_len;
	}

	smart_string_0(&buf);

	if (buf.c) {
		*pbuf = buf.c;
		return buf.len;
	} else {
		*pbuf = estrndup("", 0);
		return 0;
	}
}
/* }}} */

GEAR_API size_t gear_spprintf(char **message, size_t max_len, const char *format, ...) /* {{{ */
{
	va_list arg;
	size_t len;

	va_start(arg, format);
	len = gear_vspprintf(message, max_len, format, arg);
	va_end(arg);
	return len;
}
/* }}} */

GEAR_API size_t gear_spprintf_unchecked(char **message, size_t max_len, const char *format, ...) /* {{{ */
{
	va_list arg;
	size_t len;

	va_start(arg, format);
	len = gear_vspprintf(message, max_len, format, arg);
	va_end(arg);
	return len;
}
/* }}} */

GEAR_API gear_string *gear_vstrpprintf(size_t max_len, const char *format, va_list ap) /* {{{ */
{
	smart_str buf = {0};

	gear_printf_to_smart_str(&buf, format, ap);

	if (!buf.s) {
		return ZSTR_EMPTY_ALLOC();
	}

	if (max_len && ZSTR_LEN(buf.s) > max_len) {
		ZSTR_LEN(buf.s) = max_len;
	}

	smart_str_0(&buf);
	return buf.s;
}
/* }}} */

GEAR_API gear_string *gear_strpprintf(size_t max_len, const char *format, ...) /* {{{ */
{
	va_list arg;
	gear_string *str;

	va_start(arg, format);
	str = gear_vstrpprintf(max_len, format, arg);
	va_end(arg);
	return str;
}
/* }}} */

GEAR_API gear_string *gear_strpprintf_unchecked(size_t max_len, const char *format, ...) /* {{{ */
{
	va_list arg;
	gear_string *str;

	va_start(arg, format);
	str = gear_vstrpprintf(max_len, format, arg);
	va_end(arg);
	return str;
}
/* }}} */

static void gear_print_zval_r_to_buf(smart_str *buf, zval *expr, int indent);

static void print_hash(smart_str *buf, HashTable *ht, int indent, gear_bool is_object) /* {{{ */
{
	zval *tmp;
	gear_string *string_key;
	gear_ulong num_key;
	int i;

	for (i = 0; i < indent; i++) {
		smart_str_appendc(buf, ' ');
	}
	smart_str_appends(buf, "(\n");
	indent += PRINT_ZVAL_INDENT;
	GEAR_HASH_FOREACH_KEY_VAL_IND(ht, num_key, string_key, tmp) {
		for (i = 0; i < indent; i++) {
			smart_str_appendc(buf, ' ');
		}
		smart_str_appendc(buf, '[');
		if (string_key) {
			if (is_object) {
				const char *prop_name, *class_name;
				size_t prop_len;
				int mangled = gear_unmangle_property_name_ex(string_key, &class_name, &prop_name, &prop_len);

				smart_str_appendl(buf, prop_name, prop_len);
				if (class_name && mangled == SUCCESS) {
					if (class_name[0] == '*') {
						smart_str_appends(buf, ":protected");
					} else {
						smart_str_appends(buf, ":");
						smart_str_appends(buf, class_name);
						smart_str_appends(buf, ":private");
					}
				}
			} else {
				smart_str_append(buf, string_key);
			}
		} else {
			smart_str_append_long(buf, num_key);
		}
		smart_str_appends(buf, "] => ");
		gear_print_zval_r_to_buf(buf, tmp, indent+PRINT_ZVAL_INDENT);
		smart_str_appends(buf, "\n");
	} GEAR_HASH_FOREACH_END();
	indent -= PRINT_ZVAL_INDENT;
	for (i = 0; i < indent; i++) {
		smart_str_appendc(buf, ' ');
	}
	smart_str_appends(buf, ")\n");
}
/* }}} */

static void print_flat_hash(HashTable *ht) /* {{{ */
{
	zval *tmp;
	gear_string *string_key;
	gear_ulong num_key;
	int i = 0;

	GEAR_HASH_FOREACH_KEY_VAL_IND(ht, num_key, string_key, tmp) {
		if (i++ > 0) {
			GEAR_PUTS(",");
		}
		GEAR_PUTS("[");
		if (string_key) {
			GEAR_WRITE(ZSTR_VAL(string_key), ZSTR_LEN(string_key));
		} else {
			gear_printf(GEAR_ULONG_FMT, num_key);
		}
		GEAR_PUTS("] => ");
		gear_print_flat_zval_r(tmp);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

GEAR_API int gear_make_printable_zval(zval *expr, zval *expr_copy) /* {{{ */
{
	if (Z_TYPE_P(expr) == IS_STRING) {
		return 0;
	} else {
		ZVAL_STR(expr_copy, zval_get_string_func(expr));
		return 1;
	}
}
/* }}} */

GEAR_API size_t gear_print_zval(zval *expr, int indent) /* {{{ */
{
	gear_string *tmp_str;
	gear_string *str = zval_get_tmp_string(expr, &tmp_str);
	size_t len = ZSTR_LEN(str);

	if (len != 0) {
		gear_write(ZSTR_VAL(str), len);
	}

	gear_tmp_string_release(tmp_str);
	return len;
}
/* }}} */

GEAR_API void gear_print_flat_zval_r(zval *expr) /* {{{ */
{
	switch (Z_TYPE_P(expr)) {
		case IS_ARRAY:
			GEAR_PUTS("Array (");
			if (!(GC_FLAGS(Z_ARRVAL_P(expr)) & GC_IMMUTABLE)) {
				if (GC_IS_RECURSIVE(Z_ARRVAL_P(expr))) {
					GEAR_PUTS(" *RECURSION*");
					return;
				}
				GC_PROTECT_RECURSION(Z_ARRVAL_P(expr));
			}
			print_flat_hash(Z_ARRVAL_P(expr));
			GEAR_PUTS(")");
			if (!(GC_FLAGS(Z_ARRVAL_P(expr)) & GC_IMMUTABLE)) {
				GC_UNPROTECT_RECURSION(Z_ARRVAL_P(expr));
			}
			break;
		case IS_OBJECT:
		{
			HashTable *properties = NULL;
			gear_string *class_name = Z_OBJ_HANDLER_P(expr, get_class_name)(Z_OBJ_P(expr));
			gear_printf("%s Object (", ZSTR_VAL(class_name));
			gear_string_release_ex(class_name, 0);

			if (GC_IS_RECURSIVE(Z_OBJ_P(expr))) {
				GEAR_PUTS(" *RECURSION*");
				return;
			}

			if (Z_OBJ_HANDLER_P(expr, get_properties)) {
				properties = Z_OBJPROP_P(expr);
			}
			if (properties) {
				GC_PROTECT_RECURSION(Z_OBJ_P(expr));
				print_flat_hash(properties);
				GC_UNPROTECT_RECURSION(Z_OBJ_P(expr));
			}
			GEAR_PUTS(")");
			break;
		}
		case IS_REFERENCE:
			gear_print_flat_zval_r(Z_REFVAL_P(expr));
			break;
		default:
			gear_print_zval(expr, 0);
			break;
	}
}
/* }}} */

static void gear_print_zval_r_to_buf(smart_str *buf, zval *expr, int indent) /* {{{ */
{
	switch (Z_TYPE_P(expr)) {
		case IS_ARRAY:
			smart_str_appends(buf, "Array\n");
			if (!(GC_FLAGS(Z_ARRVAL_P(expr)) & GC_IMMUTABLE)) {
				if (GC_IS_RECURSIVE(Z_ARRVAL_P(expr))) {
					smart_str_appends(buf, " *RECURSION*");
					return;
				}
				GC_PROTECT_RECURSION(Z_ARRVAL_P(expr));
			}
			print_hash(buf, Z_ARRVAL_P(expr), indent, 0);
			if (!(GC_FLAGS(Z_ARRVAL_P(expr)) & GC_IMMUTABLE)) {
				GC_UNPROTECT_RECURSION(Z_ARRVAL_P(expr));
			}
			break;
		case IS_OBJECT:
			{
				HashTable *properties;
				int is_temp;

				gear_string *class_name = Z_OBJ_HANDLER_P(expr, get_class_name)(Z_OBJ_P(expr));
				smart_str_appends(buf, ZSTR_VAL(class_name));
				gear_string_release_ex(class_name, 0);

				smart_str_appends(buf, " Object\n");
				if (GC_IS_RECURSIVE(Z_OBJ_P(expr))) {
					smart_str_appends(buf, " *RECURSION*");
					return;
				}
				if ((properties = Z_OBJDEBUG_P(expr, is_temp)) == NULL) {
					break;
				}

				GC_PROTECT_RECURSION(Z_OBJ_P(expr));
				print_hash(buf, properties, indent, 1);
				GC_UNPROTECT_RECURSION(Z_OBJ_P(expr));

				if (is_temp) {
					gear_hash_destroy(properties);
					FREE_HASHTABLE(properties);
				}
				break;
			}
		case IS_LONG:
			smart_str_append_long(buf, Z_LVAL_P(expr));
			break;
		case IS_REFERENCE:
			gear_print_zval_r_to_buf(buf, Z_REFVAL_P(expr), indent);
			break;
		case IS_STRING:
			smart_str_append(buf, Z_STR_P(expr));
			break;
		default:
			{
				gear_string *str = zval_get_string_func(expr);
				smart_str_append(buf, str);
				gear_string_release_ex(str, 0);
			}
			break;
	}
}
/* }}} */

GEAR_API gear_string *gear_print_zval_r_to_str(zval *expr, int indent) /* {{{ */
{
	smart_str buf = {0};
	gear_print_zval_r_to_buf(&buf, expr, indent);
	smart_str_0(&buf);
	return buf.s;
}
/* }}} */

GEAR_API void gear_print_zval_r(zval *expr, int indent) /* {{{ */
{
	gear_string *str = gear_print_zval_r_to_str(expr, indent);
	gear_write(ZSTR_VAL(str), ZSTR_LEN(str));
	gear_string_release_ex(str, 0);
}
/* }}} */

static FILE *gear_fopen_wrapper(const char *filename, gear_string **opened_path) /* {{{ */
{
	if (opened_path) {
		*opened_path = gear_string_init(filename, strlen(filename), 0);
	}
	return fopen(filename, "rb");
}
/* }}} */

#ifdef ZTS
static gear_bool short_tags_default      = 1;
static uint32_t compiler_options_default = GEAR_COMPILE_DEFAULT;
#else
# define short_tags_default			1
# define compiler_options_default	GEAR_COMPILE_DEFAULT
#endif

static void gear_set_default_compile_time_values(void) /* {{{ */
{
	/* default compile-time values */
	CG(short_tags) = short_tags_default;
	CG(compiler_options) = compiler_options_default;
}
/* }}} */

#ifdef GEAR_WIN32
static void gear_get_windows_version_info(OSVERSIONINFOEX *osvi) /* {{{ */
{
	ZeroMemory(osvi, sizeof(OSVERSIONINFOEX));
	osvi->dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	if(!GetVersionEx((OSVERSIONINFO *) osvi)) {
		GEAR_ASSERT(0); /* Should not happen as sizeof is used. */
	}
}
/* }}} */
#endif

static void gear_init_exception_op(void) /* {{{ */
{
	memset(EG(exception_op), 0, sizeof(EG(exception_op)));
	EG(exception_op)[0].opcode = GEAR_HANDLE_EXCEPTION;
	GEAR_VM_SET_OPCODE_HANDLER(EG(exception_op));
	EG(exception_op)[1].opcode = GEAR_HANDLE_EXCEPTION;
	GEAR_VM_SET_OPCODE_HANDLER(EG(exception_op)+1);
	EG(exception_op)[2].opcode = GEAR_HANDLE_EXCEPTION;
	GEAR_VM_SET_OPCODE_HANDLER(EG(exception_op)+2);
}
/* }}} */

static void gear_init_call_trampoline_op(void) /* {{{ */
{
	memset(&EG(call_trampoline_op), 0, sizeof(EG(call_trampoline_op)));
	EG(call_trampoline_op).opcode = GEAR_CALL_TRAMPOLINE;
	GEAR_VM_SET_OPCODE_HANDLER(&EG(call_trampoline_op));
}
/* }}} */

static void auto_global_dtor(zval *zv) /* {{{ */
{
	free(Z_PTR_P(zv));
}
/* }}} */

#ifdef ZTS
static void function_copy_ctor(zval *zv) /* {{{ */
{
	gear_function *old_func = Z_FUNC_P(zv);
	gear_function *func = pemalloc(sizeof(gear_internal_function), 1);

	Z_FUNC_P(zv) = func;
	memcpy(func, old_func, sizeof(gear_internal_function));
	function_add_ref(func);
	if ((old_func->common.fn_flags & (GEAR_ACC_HAS_RETURN_TYPE|GEAR_ACC_HAS_TYPE_HINTS))
	 && old_func->common.arg_info) {
		uint32_t i;
		uint32_t num_args = old_func->common.num_args + 1;
		gear_arg_info *arg_info = old_func->common.arg_info - 1;
		gear_arg_info *new_arg_info;

		if (old_func->common.fn_flags & GEAR_ACC_VARIADIC) {
			num_args++;
		}
		new_arg_info = pemalloc(sizeof(gear_arg_info) * num_args, 1);
		memcpy(new_arg_info, arg_info, sizeof(gear_arg_info) * num_args);
		for (i = 0 ; i < num_args; i++) {
			if (GEAR_TYPE_IS_CLASS(arg_info[i].type)) {
				gear_string *name = gear_string_dup(GEAR_TYPE_NAME(arg_info[i].type), 1);

				new_arg_info[i].type =
					GEAR_TYPE_ENCODE_CLASS(
						name, GEAR_TYPE_ALLOW_NULL(arg_info[i].type));
			}
		}
		func->common.arg_info = new_arg_info + 1;
	}
}
/* }}} */

static void auto_global_copy_ctor(zval *zv) /* {{{ */
{
	gear_auto_global *old_ag = (gear_auto_global *) Z_PTR_P(zv);
	gear_auto_global *new_ag = pemalloc(sizeof(gear_auto_global), 1);

	new_ag->name = old_ag->name;
	new_ag->auto_global_callback = old_ag->auto_global_callback;
	new_ag->jit = old_ag->jit;

	Z_PTR_P(zv) = new_ag;
}
/* }}} */

static void compiler_globals_ctor(gear_compiler_globals *compiler_globals) /* {{{ */
{
	compiler_globals->compiled_filename = NULL;

	compiler_globals->function_table = (HashTable *) malloc(sizeof(HashTable));
	gear_hash_init_ex(compiler_globals->function_table, 1024, NULL, GEAR_FUNCTION_DTOR, 1, 0);
	gear_hash_copy(compiler_globals->function_table, global_function_table, function_copy_ctor);

	compiler_globals->class_table = (HashTable *) malloc(sizeof(HashTable));
	gear_hash_init_ex(compiler_globals->class_table, 64, NULL, GEAR_CLASS_DTOR, 1, 0);
	gear_hash_copy(compiler_globals->class_table, global_class_table, gear_class_add_ref);

	gear_set_default_compile_time_values();

	compiler_globals->auto_globals = (HashTable *) malloc(sizeof(HashTable));
	gear_hash_init_ex(compiler_globals->auto_globals, 8, NULL, auto_global_dtor, 1, 0);
	gear_hash_copy(compiler_globals->auto_globals, global_auto_globals_table, auto_global_copy_ctor);

	compiler_globals->last_static_member = gear_hash_num_elements(compiler_globals->class_table);
	if (compiler_globals->last_static_member) {
		compiler_globals->static_members_table = calloc(compiler_globals->last_static_member, sizeof(zval*));
	} else {
		compiler_globals->static_members_table = NULL;
	}
	compiler_globals->script_encoding_list = NULL;
}
/* }}} */

static void compiler_globals_dtor(gear_compiler_globals *compiler_globals) /* {{{ */
{
	if (compiler_globals->function_table != GLOBAL_FUNCTION_TABLE) {
		gear_hash_destroy(compiler_globals->function_table);
		free(compiler_globals->function_table);
	}
	if (compiler_globals->class_table != GLOBAL_CLASS_TABLE) {
		gear_hash_destroy(compiler_globals->class_table);
		free(compiler_globals->class_table);
	}
	if (compiler_globals->auto_globals != GLOBAL_AUTO_GLOBALS_TABLE) {
		gear_hash_destroy(compiler_globals->auto_globals);
		free(compiler_globals->auto_globals);
	}
	if (compiler_globals->static_members_table) {
		free(compiler_globals->static_members_table);
	}
	if (compiler_globals->script_encoding_list) {
		pefree((char*)compiler_globals->script_encoding_list, 1);
	}
	compiler_globals->last_static_member = 0;
}
/* }}} */

static void executor_globals_ctor(gear_executor_globals *executor_globals) /* {{{ */
{
	GEAR_PBCLS_CACHE_UPDATE();

	gear_startup_constants();
	gear_copy_constants(executor_globals->gear_constants, GLOBAL_CONSTANTS_TABLE);
	gear_init_rsrc_plist();
	gear_init_exception_op();
	gear_init_call_trampoline_op();
	memset(&executor_globals->trampoline, 0, sizeof(gear_op_array));
	executor_globals->lambda_count = 0;
	ZVAL_UNDEF(&executor_globals->user_error_handler);
	ZVAL_UNDEF(&executor_globals->user_exception_handler);
	executor_globals->in_autoload = NULL;
	executor_globals->current_execute_data = NULL;
	executor_globals->current_capi = NULL;
	executor_globals->exit_status = 0;
#if XPFPA_HAVE_CW
	executor_globals->saved_fpu_cw = 0;
#endif
	executor_globals->saved_fpu_cw_ptr = NULL;
	executor_globals->active = 0;
	executor_globals->bailout = NULL;
	executor_globals->error_handling  = EH_NORMAL;
	executor_globals->exception_class = NULL;
	executor_globals->exception = NULL;
	executor_globals->objects_store.object_buckets = NULL;
#ifdef GEAR_WIN32
	gear_get_windows_version_info(&executor_globals->windows_version_info);
#endif
	executor_globals->flags = EG_FLAGS_INITIAL;
}
/* }}} */

static void executor_globals_dtor(gear_executor_globals *executor_globals) /* {{{ */
{
	gear_ics_dtor(executor_globals->ics_directives);

	if (&executor_globals->persistent_list != global_persistent_list) {
		gear_destroy_rsrc_list(&executor_globals->persistent_list);
	}
	if (executor_globals->gear_constants != GLOBAL_CONSTANTS_TABLE) {
		gear_hash_destroy(executor_globals->gear_constants);
		free(executor_globals->gear_constants);
	}
}
/* }}} */

static void gear_new_thread_end_handler(THREAD_T thread_id) /* {{{ */
{
	if (gear_copy_ics_directives() == SUCCESS) {
		gear_ics_refresh_caches(GEAR_ICS_STAGE_STARTUP);
	}
}
/* }}} */
#endif

#if defined(__FreeBSD__) || defined(__DragonFly__)
/* FreeBSD and DragonFly floating point precision fix */
#include <floatingpoint.h>
#endif

static void ics_scanner_globals_ctor(gear_ics_scanner_globals *scanner_globals_p) /* {{{ */
{
	memset(scanner_globals_p, 0, sizeof(*scanner_globals_p));
}
/* }}} */

static void hyss_scanner_globals_ctor(gear_hyss_scanner_globals *scanner_globals_p) /* {{{ */
{
	memset(scanner_globals_p, 0, sizeof(*scanner_globals_p));
}
/* }}} */

static void capi_destructor_zval(zval *zv) /* {{{ */
{
	gear_capi_entry *cAPI = (gear_capi_entry*)Z_PTR_P(zv);

	capi_destructor(cAPI);
	free(cAPI);
}
/* }}} */

static gear_bool hyss_auto_globals_create_globals(gear_string *name) /* {{{ */
{
	zval globals;

	/* IS_ARRAY, but with ref-counter 1 and not IS_TYPE_REFCOUNTED */
	ZVAL_ARR(&globals, &EG(symbol_table));
	Z_TYPE_FLAGS_P(&globals) = 0;
	ZVAL_NEW_REF(&globals, &globals);
	gear_hash_update(&EG(symbol_table), name, &globals);
	return 0;
}
/* }}} */

int gear_startup(gear_utility_functions *utility_functions, char **extensions) /* {{{ */
{
#ifdef ZTS
	gear_compiler_globals *compiler_globals;
	gear_executor_globals *executor_globals;
	extern GEAR_API ts_rsrc_id ics_scanner_globals_id;
	extern GEAR_API ts_rsrc_id language_scanner_globals_id;
	GEAR_PBCLS_CACHE_UPDATE();
#else
	extern gear_ics_scanner_globals ics_scanner_globals;
	extern gear_hyss_scanner_globals language_scanner_globals;
#endif

	gear_cpu_startup();

#ifdef GEAR_WIN32
	hyss_win32_cp_set_by_id(65001);
#endif

	start_memory_manager();

	virtual_cwd_startup(); /* Could use shutdown to free the main cwd but it would just slow it down for CGI */

#if defined(__FreeBSD__) || defined(__DragonFly__)
	/* FreeBSD and DragonFly floating point precision fix */
	fpsetmask(0);
#endif

	gear_startup_strtod();
	gear_startup_extensions_mechanism();

	/* Set up utility functions and values */
	gear_error_cb = utility_functions->error_function;
	gear_printf = utility_functions->printf_function;
	gear_write = (gear_write_func_t) utility_functions->write_function;
	gear_fopen = utility_functions->fopen_function;
	if (!gear_fopen) {
		gear_fopen = gear_fopen_wrapper;
	}
	gear_stream_open_function = utility_functions->stream_open_function;
	gear_message_dispatcher_p = utility_functions->message_handler;
	gear_get_configuration_directive_p = utility_functions->get_configuration_directive;
	gear_ticks_function = utility_functions->ticks_function;
	gear_on_timeout = utility_functions->on_timeout;
	gear_printf_to_smart_string = utility_functions->printf_to_smart_string_function;
	gear_printf_to_smart_str = utility_functions->printf_to_smart_str_function;
	gear_getenv = utility_functions->getenv_function;
	gear_resolve_path = utility_functions->resolve_path_function;

	gear_interrupt_function = NULL;

#if HAVE_DTRACE
/* build with dtrace support */
	{
		char *tmp = getenv("USE_GEAR_DTRACE");

		if (tmp && gear_atoi(tmp, 0)) {
			gear_dtrace_enabled = 1;
			gear_compile_file = dtrace_compile_file;
			gear_execute_ex = dtrace_execute_ex;
			gear_execute_internal = dtrace_execute_internal;
		} else {
			gear_compile_file = compile_file;
			gear_execute_ex = execute_ex;
			gear_execute_internal = NULL;
		}
	}
#else
	gear_compile_file = compile_file;
	gear_execute_ex = execute_ex;
	gear_execute_internal = NULL;
#endif /* HAVE_DTRACE */
	gear_compile_string = compile_string;
	gear_throw_exception_hook = NULL;

	/* Set up the default garbage collection implementation. */
	gc_collect_cycles = gear_gc_collect_cycles;

	gear_vm_init();

	/* set up version */
	gear_version_info = strdup(GEAR_CORE_VERSION_INFO);
	gear_version_info_length = sizeof(GEAR_CORE_VERSION_INFO) - 1;

	GLOBAL_FUNCTION_TABLE = (HashTable *) malloc(sizeof(HashTable));
	GLOBAL_CLASS_TABLE = (HashTable *) malloc(sizeof(HashTable));
	GLOBAL_AUTO_GLOBALS_TABLE = (HashTable *) malloc(sizeof(HashTable));
	GLOBAL_CONSTANTS_TABLE = (HashTable *) malloc(sizeof(HashTable));

	gear_hash_init_ex(GLOBAL_FUNCTION_TABLE, 1024, NULL, GEAR_FUNCTION_DTOR, 1, 0);
	gear_hash_init_ex(GLOBAL_CLASS_TABLE, 64, NULL, GEAR_CLASS_DTOR, 1, 0);
	gear_hash_init_ex(GLOBAL_AUTO_GLOBALS_TABLE, 8, NULL, auto_global_dtor, 1, 0);
	gear_hash_init_ex(GLOBAL_CONSTANTS_TABLE, 128, NULL, GEAR_CONSTANT_DTOR, 1, 0);

	gear_hash_init_ex(&capi_registry, 32, NULL, capi_destructor_zval, 1, 0);
	gear_init_rsrc_list_dtors();

#ifdef ZTS
	ts_allocate_id(&compiler_globals_id, sizeof(gear_compiler_globals), (ts_allocate_ctor) compiler_globals_ctor, (ts_allocate_dtor) compiler_globals_dtor);
	ts_allocate_id(&executor_globals_id, sizeof(gear_executor_globals), (ts_allocate_ctor) executor_globals_ctor, (ts_allocate_dtor) executor_globals_dtor);
	ts_allocate_id(&language_scanner_globals_id, sizeof(gear_hyss_scanner_globals), (ts_allocate_ctor) hyss_scanner_globals_ctor, NULL);
	ts_allocate_id(&ics_scanner_globals_id, sizeof(gear_ics_scanner_globals), (ts_allocate_ctor) ics_scanner_globals_ctor, NULL);
	compiler_globals = ts_resource(compiler_globals_id);
	executor_globals = ts_resource(executor_globals_id);

	compiler_globals_dtor(compiler_globals);
	compiler_globals->in_compilation = 0;
	compiler_globals->function_table = (HashTable *) malloc(sizeof(HashTable));
	compiler_globals->class_table = (HashTable *) malloc(sizeof(HashTable));

	*compiler_globals->function_table = *GLOBAL_FUNCTION_TABLE;
	*compiler_globals->class_table = *GLOBAL_CLASS_TABLE;
	compiler_globals->auto_globals = GLOBAL_AUTO_GLOBALS_TABLE;

	gear_hash_destroy(executor_globals->gear_constants);
	*executor_globals->gear_constants = *GLOBAL_CONSTANTS_TABLE;
#else
	ics_scanner_globals_ctor(&ics_scanner_globals);
	hyss_scanner_globals_ctor(&language_scanner_globals);
	gear_set_default_compile_time_values();
#ifdef GEAR_WIN32
	gear_get_windows_version_info(&EG(windows_version_info));
#endif
#endif
	EG(error_reporting) = E_ALL & ~E_NOTICE;

	gear_interned_strings_init();
	gear_startup_builtin_functions();
	gear_register_standard_constants();
	gear_register_auto_global(gear_string_init_interned("GLOBALS", sizeof("GLOBALS") - 1, 1), 1, hyss_auto_globals_create_globals);

#ifndef ZTS
	gear_init_rsrc_plist();
	gear_init_exception_op();
	gear_init_call_trampoline_op();
#endif

	gear_ics_startup();

#ifdef GEAR_WIN32
	/* Uses ICS settings, so needs to be run after it. */
	hyss_win32_cp_setup();
#endif

#ifdef ZTS
	pbc_set_new_thread_end_handler(gear_new_thread_end_handler);
	pbc_set_shutdown_handler(gear_interned_strings_dtor);
#endif

	return SUCCESS;
}
/* }}} */

void gear_register_standard_ics_entries(void) /* {{{ */
{
	int capi_number = 0;

	REGISTER_ICS_ENTRIES();
}
/* }}} */

/* Unlink the global (r/o) copies of the class, function and constant tables,
 * and use a fresh r/w copy for the startup thread
 */
int gear_post_startup(void) /* {{{ */
{
#ifdef ZTS
	gear_encoding **script_encoding_list;

	gear_compiler_globals *compiler_globals = ts_resource(compiler_globals_id);
	gear_executor_globals *executor_globals = ts_resource(executor_globals_id);

	*GLOBAL_FUNCTION_TABLE = *compiler_globals->function_table;
	*GLOBAL_CLASS_TABLE = *compiler_globals->class_table;
	*GLOBAL_CONSTANTS_TABLE = *executor_globals->gear_constants;

	short_tags_default = CG(short_tags);
	compiler_options_default = CG(compiler_options);

	gear_destroy_rsrc_list(&EG(persistent_list));
	free(compiler_globals->function_table);
	free(compiler_globals->class_table);
	if ((script_encoding_list = (gear_encoding **)compiler_globals->script_encoding_list)) {
		compiler_globals_ctor(compiler_globals);
		compiler_globals->script_encoding_list = (const gear_encoding **)script_encoding_list;
	} else {
		compiler_globals_ctor(compiler_globals);
	}
	free(EG(gear_constants));

	executor_globals_ctor(executor_globals);
	global_persistent_list = &EG(persistent_list);
	gear_copy_ics_directives();
#endif

	if (gear_post_startup_cb) {
		int (*cb)(void) = gear_post_startup_cb;

		gear_post_startup_cb = NULL;
		if (cb() != SUCCESS) {
			return FAILURE;
		}
	}

	return SUCCESS;
}
/* }}} */

void gear_shutdown(void) /* {{{ */
{
	gear_vm_dtor();

	gear_destroy_rsrc_list(&EG(persistent_list));
	gear_destroy_capis();

	virtual_cwd_deactivate();
	virtual_cwd_shutdown();

	gear_hash_destroy(GLOBAL_FUNCTION_TABLE);
	gear_hash_destroy(GLOBAL_CLASS_TABLE);

	gear_hash_destroy(GLOBAL_AUTO_GLOBALS_TABLE);
	free(GLOBAL_AUTO_GLOBALS_TABLE);

	gear_shutdown_extensions();
	free(gear_version_info);

	free(GLOBAL_FUNCTION_TABLE);
	free(GLOBAL_CLASS_TABLE);

	gear_hash_destroy(GLOBAL_CONSTANTS_TABLE);
	free(GLOBAL_CONSTANTS_TABLE);
	gear_shutdown_strtod();

#ifdef ZTS
	GLOBAL_FUNCTION_TABLE = NULL;
	GLOBAL_CLASS_TABLE = NULL;
	GLOBAL_AUTO_GLOBALS_TABLE = NULL;
	GLOBAL_CONSTANTS_TABLE = NULL;
#endif
	gear_destroy_rsrc_list_dtors();
}
/* }}} */

void gear_set_utility_values(gear_utility_values *utility_values) /* {{{ */
{
	gear_uv = *utility_values;
	gear_uv.import_use_extension_length = (uint32_t)strlen(gear_uv.import_use_extension);
}
/* }}} */

/* this should be compatible with the standard gearerror */
GEAR_COLD void gearerror(const char *error) /* {{{ */
{
	CG(parse_error) = 0;

	if (EG(exception)) {
		/* An exception was thrown in the lexer, don't throw another in the parser. */
		return;
	}

	gear_throw_exception(gear_ce_parse_error, error, 0);
}
/* }}} */

BEGIN_EXTERN_C()
GEAR_API GEAR_COLD void _gear_bailout(const char *filename, uint32_t lineno) /* {{{ */
{

	if (!EG(bailout)) {
		gear_output_debug_string(1, "%s(%d) : Bailed out without a bailout address!", filename, lineno);
		exit(-1);
	}
	gc_protect(1);
	CG(unclean_shutdown) = 1;
	CG(active_class_entry) = NULL;
	CG(in_compilation) = 0;
	EG(current_execute_data) = NULL;
	LONGJMP(*EG(bailout), FAILURE);
}
/* }}} */
END_EXTERN_C()

GEAR_API void gear_append_version_info(const gear_extension *extension) /* {{{ */
{
	char *new_info;
	uint32_t new_info_length;

	new_info_length = (uint32_t)(sizeof("    with  v, , by \n")
						+ strlen(extension->name)
						+ strlen(extension->version)
						+ strlen(extension->copyright)
						+ strlen(extension->author));

	new_info = (char *) malloc(new_info_length + 1);

	snprintf(new_info, new_info_length, "    with %s v%s, %s, by %s\n", extension->name, extension->version, extension->copyright, extension->author);

	gear_version_info = (char *) realloc(gear_version_info, gear_version_info_length+new_info_length + 1);
	strncat(gear_version_info, new_info, new_info_length);
	gear_version_info_length += new_info_length;
	free(new_info);
}
/* }}} */

GEAR_API char *get_gear_version(void) /* {{{ */
{
	return gear_version_info;
}
/* }}} */

GEAR_API void gear_activate(void) /* {{{ */
{
#ifdef ZTS
	virtual_cwd_activate();
#endif
	gc_reset();
	init_compiler();
	init_executor();
	startup_scanner();
}
/* }}} */

void gear_call_destructors(void) /* {{{ */
{
	gear_try {
		shutdown_destructors();
	} gear_end_try();
}
/* }}} */

GEAR_API void gear_deactivate(void) /* {{{ */
{
	/* we're no longer executing anything */
	EG(current_execute_data) = NULL;

	gear_try {
		shutdown_scanner();
	} gear_end_try();

	/* shutdown_executor() takes care of its own bailout handling */
	shutdown_executor();

	gear_try {
		gear_ics_deactivate();
	} gear_end_try();

	gear_try {
		shutdown_compiler();
	} gear_end_try();

	gear_destroy_rsrc_list(&EG(regular_list));

#if GC_BENCH
	fprintf(stderr, "GC Statistics\n");
	fprintf(stderr, "-------------\n");
	fprintf(stderr, "Runs:               %d\n", GC_G(gc_runs));
	fprintf(stderr, "Collected:          %d\n", GC_G(collected));
	fprintf(stderr, "Root buffer length: %d\n", GC_G(root_buf_length));
	fprintf(stderr, "Root buffer peak:   %d\n\n", GC_G(root_buf_peak));
	fprintf(stderr, "      Possible            Remove from  Marked\n");
	fprintf(stderr, "        Root    Buffered     buffer     grey\n");
	fprintf(stderr, "      --------  --------  -----------  ------\n");
	fprintf(stderr, "ZVAL  %8d  %8d  %9d  %8d\n", GC_G(zval_possible_root), GC_G(zval_buffered), GC_G(zval_remove_from_buffer), GC_G(zval_marked_grey));
#endif
}
/* }}} */

BEGIN_EXTERN_C()
GEAR_API void gear_message_dispatcher(gear_long message, const void *data) /* {{{ */
{
	if (gear_message_dispatcher_p) {
		gear_message_dispatcher_p(message, data);
	}
}
/* }}} */
END_EXTERN_C()

GEAR_API zval *gear_get_configuration_directive(gear_string *name) /* {{{ */
{
	if (gear_get_configuration_directive_p) {
		return gear_get_configuration_directive_p(name);
	} else {
		return NULL;
	}
}
/* }}} */

#define SAVE_STACK(stack) do { \
		if (CG(stack).top) { \
			memcpy(&stack, &CG(stack), sizeof(gear_stack)); \
			CG(stack).top = CG(stack).max = 0; \
			CG(stack).elements = NULL; \
		} else { \
			stack.top = 0; \
		} \
	} while (0)

#define RESTORE_STACK(stack) do { \
		if (stack.top) { \
			gear_stack_destroy(&CG(stack)); \
			memcpy(&CG(stack), &stack, sizeof(gear_stack)); \
		} \
	} while (0)

#if !defined(HAVE_NORETURN) || defined(HAVE_NORETURN_ALIAS)
GEAR_API GEAR_COLD void gear_error(int type, const char *format, ...) /* {{{ */
#else
static GEAR_COLD void gear_error_va_list(int type, const char *format, va_list args)
#endif
{
#if !defined(HAVE_NORETURN) || defined(HAVE_NORETURN_ALIAS)
	va_list args;
#endif
	va_list usr_copy;
	zval params[5];
	zval retval;
	const char *error_filename;
	uint32_t error_lineno = 0;
	zval orig_user_error_handler;
	gear_bool in_compilation;
	gear_class_entry *saved_class_entry;
	gear_stack loop_var_stack;
	gear_stack delayed_oplines_stack;
	gear_array *symbol_table;
	gear_class_entry *orig_fake_scope;

	/* Report about uncaught exception in case of fatal errors */
	if (EG(exception)) {
		gear_execute_data *ex;
		const gear_op *opline;

		switch (type) {
			case E_CORE_ERROR:
			case E_ERROR:
			case E_RECOVERABLE_ERROR:
			case E_PARSE:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
				ex = EG(current_execute_data);
				opline = NULL;
				while (ex && (!ex->func || !GEAR_USER_CODE(ex->func->type))) {
					ex = ex->prev_execute_data;
				}
				if (ex && ex->opline->opcode == GEAR_HANDLE_EXCEPTION &&
				    EG(opline_before_exception)) {
					opline = EG(opline_before_exception);
				}
				gear_exception_error(EG(exception), E_WARNING);
				EG(exception) = NULL;
				if (opline) {
					ex->opline = opline;
				}
				break;
			default:
				break;
		}
	}

	/* Obtain relevant filename and lineno */
	switch (type) {
		case E_CORE_ERROR:
		case E_CORE_WARNING:
			error_filename = NULL;
			error_lineno = 0;
			break;
		case E_PARSE:
		case E_COMPILE_ERROR:
		case E_COMPILE_WARNING:
		case E_ERROR:
		case E_NOTICE:
		case E_STRICT:
		case E_DEPRECATED:
		case E_WARNING:
		case E_USER_ERROR:
		case E_USER_WARNING:
		case E_USER_NOTICE:
		case E_USER_DEPRECATED:
		case E_RECOVERABLE_ERROR:
			if (gear_is_compiling()) {
				error_filename = ZSTR_VAL(gear_get_compiled_filename());
				error_lineno = gear_get_compiled_lineno();
			} else if (gear_is_executing()) {
				error_filename = gear_get_executed_filename();
				if (error_filename[0] == '[') { /* [no active file] */
					error_filename = NULL;
					error_lineno = 0;
				} else {
					error_lineno = gear_get_executed_lineno();
				}
			} else {
				error_filename = NULL;
				error_lineno = 0;
			}
			break;
		default:
			error_filename = NULL;
			error_lineno = 0;
			break;
	}
	if (!error_filename) {
		error_filename = "Unknown";
	}

#ifdef HAVE_DTRACE
	if (DTRACE_ERROR_ENABLED()) {
		char *dtrace_error_buffer;
#if !defined(HAVE_NORETURN) || defined(HAVE_NORETURN_ALIAS)
		va_start(args, format);
#endif
		gear_vspprintf(&dtrace_error_buffer, 0, format, args);
		DTRACE_ERROR(dtrace_error_buffer, (char *)error_filename, error_lineno);
		efree(dtrace_error_buffer);
#if !defined(HAVE_NORETURN) || defined(HAVE_NORETURN_ALIAS)
		va_end(args);
#endif
	}
#endif /* HAVE_DTRACE */

#if !defined(HAVE_NORETURN) || defined(HAVE_NORETURN_ALIAS)
	va_start(args, format);
#endif

	/* if we don't have a user defined error handler */
	if (Z_TYPE(EG(user_error_handler)) == IS_UNDEF
		|| !(EG(user_error_handler_error_reporting) & type)
		|| EG(error_handling) != EH_NORMAL) {
		gear_error_cb(type, error_filename, error_lineno, format, args);
	} else switch (type) {
		case E_ERROR:
		case E_PARSE:
		case E_CORE_ERROR:
		case E_CORE_WARNING:
		case E_COMPILE_ERROR:
		case E_COMPILE_WARNING:
			/* The error may not be safe to handle in user-space */
			gear_error_cb(type, error_filename, error_lineno, format, args);
			break;
		default:
			/* Handle the error in user space */
			va_copy(usr_copy, args);
			ZVAL_STR(&params[1], gear_vstrpprintf(0, format, usr_copy));
			va_end(usr_copy);

			ZVAL_LONG(&params[0], type);

			if (error_filename) {
				ZVAL_STRING(&params[2], error_filename);
			} else {
				ZVAL_NULL(&params[2]);
			}

			ZVAL_LONG(&params[3], error_lineno);

			symbol_table = gear_rebuild_symbol_table();

			/* during shutdown the symbol table table can be still null */
			if (!symbol_table) {
				ZVAL_NULL(&params[4]);
			} else {
				ZVAL_ARR(&params[4], gear_array_dup(symbol_table));
			}

			ZVAL_COPY_VALUE(&orig_user_error_handler, &EG(user_error_handler));
			ZVAL_UNDEF(&EG(user_error_handler));

			/* User error handler may include() additinal HYSS files.
			 * If an error was generated during comilation HYSS will compile
			 * such scripts recursivly, but some CG() variables may be
			 * inconsistent. */

			in_compilation = CG(in_compilation);
			if (in_compilation) {
				saved_class_entry = CG(active_class_entry);
				CG(active_class_entry) = NULL;
				SAVE_STACK(loop_var_stack);
				SAVE_STACK(delayed_oplines_stack);
				CG(in_compilation) = 0;
			}

			orig_fake_scope = EG(fake_scope);
			EG(fake_scope) = NULL;

			if (call_user_function(CG(function_table), NULL, &orig_user_error_handler, &retval, 5, params) == SUCCESS) {
				if (Z_TYPE(retval) != IS_UNDEF) {
					if (Z_TYPE(retval) == IS_FALSE) {
						gear_error_cb(type, error_filename, error_lineno, format, args);
					}
					zval_ptr_dtor(&retval);
				}
			} else if (!EG(exception)) {
				/* The user error handler failed, use built-in error handler */
				gear_error_cb(type, error_filename, error_lineno, format, args);
			}

			EG(fake_scope) = orig_fake_scope;

			if (in_compilation) {
				CG(active_class_entry) = saved_class_entry;
				RESTORE_STACK(loop_var_stack);
				RESTORE_STACK(delayed_oplines_stack);
				CG(in_compilation) = 1;
			}

			zval_ptr_dtor(&params[4]);
			zval_ptr_dtor(&params[2]);
			zval_ptr_dtor(&params[1]);

			if (Z_TYPE(EG(user_error_handler)) == IS_UNDEF) {
				ZVAL_COPY_VALUE(&EG(user_error_handler), &orig_user_error_handler);
			} else {
				zval_ptr_dtor(&orig_user_error_handler);
			}
			break;
	}

#if !defined(HAVE_NORETURN) || defined(HAVE_NORETURN_ALIAS)
	va_end(args);
#endif

	if (type == E_PARSE) {
		/* eval() errors do not affect exit_status */
		if (!(EG(current_execute_data) &&
			EG(current_execute_data)->func &&
			GEAR_USER_CODE(EG(current_execute_data)->func->type) &&
			EG(current_execute_data)->opline->opcode == GEAR_INCLUDE_OR_EVAL &&
			EG(current_execute_data)->opline->extended_value == GEAR_EVAL)) {
			EG(exit_status) = 255;
		}
	}
}
/* }}} */

#ifdef HAVE_NORETURN
# ifdef HAVE_NORETURN_ALIAS
GEAR_COLD void gear_error_noreturn(int type, const char *format, ...) __attribute__ ((alias("gear_error"),noreturn));
# else
GEAR_API GEAR_COLD void gear_error(int type, const char *format, ...) /* {{{ */
{
	va_list va;

	va_start(va, format);
	gear_error_va_list(type, format, va);
	va_end(va);
}

GEAR_API GEAR_COLD GEAR_NORETURN void gear_error_noreturn(int type, const char *format, ...)
{
	va_list va;

	va_start(va, format);
	gear_error_va_list(type, format, va);
	va_end(va);
}
/* }}} */
# endif
#endif

GEAR_API GEAR_COLD void gear_throw_error(gear_class_entry *exception_ce, const char *format, ...) /* {{{ */
{
	va_list va;
	char *message = NULL;

	if (exception_ce) {
		if (!instanceof_function(exception_ce, gear_ce_error)) {
			gear_error(E_NOTICE, "Error exceptions must be derived from Error");
			exception_ce = gear_ce_error;
		}
	} else {
		exception_ce = gear_ce_error;
	}

	va_start(va, format);
	gear_vspprintf(&message, 0, format, va);

	//TODO: we can't convert compile-time errors to exceptions yet???
	if (EG(current_execute_data) && !CG(in_compilation)) {
		gear_throw_exception(exception_ce, message, 0);
	} else {
		gear_error(E_ERROR, "%s", message);
	}

	efree(message);
	va_end(va);
}
/* }}} */

GEAR_API GEAR_COLD void gear_type_error(const char *format, ...) /* {{{ */
{
	va_list va;
	char *message = NULL;

	va_start(va, format);
	gear_vspprintf(&message, 0, format, va);
	gear_throw_exception(gear_ce_type_error, message, 0);
	efree(message);
	va_end(va);
} /* }}} */

GEAR_API GEAR_COLD void gear_internal_type_error(gear_bool throw_exception, const char *format, ...) /* {{{ */
{
	va_list va;
	char *message = NULL;

	va_start(va, format);
	gear_vspprintf(&message, 0, format, va);
	if (throw_exception) {
		gear_throw_exception(gear_ce_type_error, message, 0);
	} else {
		gear_error(E_WARNING, "%s", message);
	}
	efree(message);

	va_end(va);
} /* }}} */

GEAR_API GEAR_COLD void gear_internal_argument_count_error(gear_bool throw_exception, const char *format, ...) /* {{{ */
{
	va_list va;
	char *message = NULL;

	va_start(va, format);
	gear_vspprintf(&message, 0, format, va);
	if (throw_exception) {
		gear_throw_exception(gear_ce_argument_count_error, message, 0);
	} else {
		gear_error(E_WARNING, "%s", message);
	}
	efree(message);

	va_end(va);
} /* }}} */

GEAR_API GEAR_COLD void gear_output_debug_string(gear_bool trigger_break, const char *format, ...) /* {{{ */
{
#if GEAR_DEBUG
	va_list args;

	va_start(args, format);
#	ifdef GEAR_WIN32
	{
		char output_buf[1024];

		vsnprintf(output_buf, 1024, format, args);
		OutputDebugString(output_buf);
		OutputDebugString("\n");
		if (trigger_break && IsDebuggerPresent()) {
			DebugBreak();
		}
	}
#	else
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
#	endif
	va_end(args);
#endif
}
/* }}} */

GEAR_API void gear_try_exception_handler() /* {{{ */
{
	if (EG(exception)) {
		if (Z_TYPE(EG(user_exception_handler)) != IS_UNDEF) {
			zval orig_user_exception_handler;
			zval params[1], retval2;
			gear_object *old_exception;
			old_exception = EG(exception);
			EG(exception) = NULL;
			ZVAL_OBJ(&params[0], old_exception);
			ZVAL_COPY_VALUE(&orig_user_exception_handler, &EG(user_exception_handler));

			if (call_user_function(CG(function_table), NULL, &orig_user_exception_handler, &retval2, 1, params) == SUCCESS) {
				zval_ptr_dtor(&retval2);
				if (EG(exception)) {
					OBJ_RELEASE(EG(exception));
					EG(exception) = NULL;
				}
				OBJ_RELEASE(old_exception);
			} else {
				EG(exception) = old_exception;
			}
		}
	}
} /* }}} */

GEAR_API int gear_execute_scripts(int type, zval *retval, int file_count, ...) /* {{{ */
{
	va_list files;
	int i;
	gear_file_handle *file_handle;
	gear_op_array *op_array;

	va_start(files, file_count);
	for (i = 0; i < file_count; i++) {
		file_handle = va_arg(files, gear_file_handle *);
		if (!file_handle) {
			continue;
		}

		op_array = gear_compile_file(file_handle, type);
		if (file_handle->opened_path) {
			gear_hash_add_empty_element(&EG(included_files), file_handle->opened_path);
		}
		gear_destroy_file_handle(file_handle);
		if (op_array) {
			gear_execute(op_array, retval);
			gear_exception_restore();
			gear_try_exception_handler();
			if (EG(exception)) {
				gear_exception_error(EG(exception), E_ERROR);
			}
			destroy_op_array(op_array);
			efree_size(op_array, sizeof(gear_op_array));
		} else if (type==GEAR_REQUIRE) {
			va_end(files);
			return FAILURE;
		}
	}
	va_end(files);

	return SUCCESS;
}
/* }}} */

#define COMPILED_STRING_DESCRIPTION_FORMAT "%s(%d) : %s"

GEAR_API char *gear_make_compiled_string_description(const char *name) /* {{{ */
{
	const char *cur_filename;
	int cur_lineno;
	char *compiled_string_description;

	if (gear_is_compiling()) {
		cur_filename = ZSTR_VAL(gear_get_compiled_filename());
		cur_lineno = gear_get_compiled_lineno();
	} else if (gear_is_executing()) {
		cur_filename = gear_get_executed_filename();
		cur_lineno = gear_get_executed_lineno();
	} else {
		cur_filename = "Unknown";
		cur_lineno = 0;
	}

	gear_spprintf(&compiled_string_description, 0, COMPILED_STRING_DESCRIPTION_FORMAT, cur_filename, cur_lineno, name);
	return compiled_string_description;
}
/* }}} */

void free_estring(char **str_p) /* {{{ */
{
	efree(*str_p);
}
/* }}} */

