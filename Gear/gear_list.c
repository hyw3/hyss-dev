/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* resource lists */

#include "gear.h"
#include "gear_list.h"
#include "gear_API.h"
#include "gear_globals.h"

GEAR_API int le_index_ptr;

/* true global */
static HashTable list_destructors;

GEAR_API zval* GEAR_FASTCALL gear_list_insert(void *ptr, int type)
{
	int index;
	zval zv;

	index = gear_hash_next_free_element(&EG(regular_list));
	if (index == 0) {
		index = 1;
	}
	ZVAL_NEW_RES(&zv, index, ptr, type);
	return gear_hash_index_add_new(&EG(regular_list), index, &zv);
}

GEAR_API int GEAR_FASTCALL gear_list_delete(gear_resource *res)
{
	if (GC_DELREF(res) <= 0) {
		return gear_hash_index_del(&EG(regular_list), res->handle);
	} else {
		return SUCCESS;
	}
}

GEAR_API int GEAR_FASTCALL gear_list_free(gear_resource *res)
{
	if (GC_REFCOUNT(res) <= 0) {
		return gear_hash_index_del(&EG(regular_list), res->handle);
	} else {
		return SUCCESS;
	}
}

static void gear_resource_dtor(gear_resource *res)
{
	gear_rsrc_list_dtors_entry *ld;
	gear_resource r = *res;

	res->type = -1;
	res->ptr = NULL;

	ld = gear_hash_index_find_ptr(&list_destructors, r.type);
	if (ld) {
		if (ld->list_dtor_ex) {
			ld->list_dtor_ex(&r);
		}
	} else {
		gear_error(E_WARNING, "Unknown list entry type (%d)", r.type);
	}
}


GEAR_API int GEAR_FASTCALL gear_list_close(gear_resource *res)
{
	if (GC_REFCOUNT(res) <= 0) {
		return gear_list_free(res);
	} else if (res->type >= 0) {
		gear_resource_dtor(res);
	}
	return SUCCESS;
}

GEAR_API gear_resource* gear_register_resource(void *rsrc_pointer, int rsrc_type)
{
	zval *zv;

	zv = gear_list_insert(rsrc_pointer, rsrc_type);

	return Z_RES_P(zv);
}

GEAR_API void *gear_fetch_resource2(gear_resource *res, const char *resource_type_name, int resource_type1, int resource_type2)
{
	if (res) {
		if (resource_type1 == res->type) {
			return res->ptr;
		}

		if (resource_type2 == res->type) {
			return res->ptr;
		}
	}

	if (resource_type_name) {
		const char *space;
		const char *class_name = get_active_class_name(&space);
		gear_error(E_WARNING, "%s%s%s(): supplied resource is not a valid %s resource", class_name, space, get_active_function_name(), resource_type_name);
	}

	return NULL;
}

GEAR_API void *gear_fetch_resource(gear_resource *res, const char *resource_type_name, int resource_type)
{
	if (resource_type == res->type) {
		return res->ptr;
	}

	if (resource_type_name) {
		const char *space;
		const char *class_name = get_active_class_name(&space);
		gear_error(E_WARNING, "%s%s%s(): supplied resource is not a valid %s resource", class_name, space, get_active_function_name(), resource_type_name);
	}

	return NULL;
}

GEAR_API void *gear_fetch_resource_ex(zval *res, const char *resource_type_name, int resource_type)
{
	const char *space, *class_name;
	if (res == NULL) {
		if (resource_type_name) {
			class_name = get_active_class_name(&space);
			gear_error(E_WARNING, "%s%s%s(): no %s resource supplied", class_name, space, get_active_function_name(), resource_type_name);
		}
		return NULL;
	}
	if (Z_TYPE_P(res) != IS_RESOURCE) {
		if (resource_type_name) {
			class_name = get_active_class_name(&space);
			gear_error(E_WARNING, "%s%s%s(): supplied argument is not a valid %s resource", class_name, space, get_active_function_name(), resource_type_name);
		}
		return NULL;
	}

	return gear_fetch_resource(Z_RES_P(res), resource_type_name, resource_type);
}

GEAR_API void *gear_fetch_resource2_ex(zval *res, const char *resource_type_name, int resource_type1, int resource_type2)
{
	const char *space, *class_name;
	if (res == NULL) {
		if (resource_type_name) {
			class_name = get_active_class_name(&space);
			gear_error(E_WARNING, "%s%s%s(): no %s resource supplied", class_name, space, get_active_function_name(), resource_type_name);
		}
		return NULL;
	}
	if (Z_TYPE_P(res) != IS_RESOURCE) {
		if (resource_type_name) {
			class_name = get_active_class_name(&space);
			gear_error(E_WARNING, "%s%s%s(): supplied argument is not a valid %s resource", class_name, space, get_active_function_name(), resource_type_name);
		}
		return NULL;
	}

	return gear_fetch_resource2(Z_RES_P(res), resource_type_name, resource_type1, resource_type2);
}

void list_entry_destructor(zval *zv)
{
	gear_resource *res = Z_RES_P(zv);

	ZVAL_UNDEF(zv);
	if (res->type >= 0) {
		gear_resource_dtor(res);
	}
	efree_size(res, sizeof(gear_resource));
}

void plist_entry_destructor(zval *zv)
{
	gear_resource *res = Z_RES_P(zv);

	if (res->type >= 0) {
		gear_rsrc_list_dtors_entry *ld;

		ld = gear_hash_index_find_ptr(&list_destructors, res->type);
		if (ld) {
			if (ld->plist_dtor_ex) {
				ld->plist_dtor_ex(res);
			}
		} else {
			gear_error(E_WARNING,"Unknown list entry type (%d)", res->type);
		}
	}
	free(res);
}

int gear_init_rsrc_list(void)
{
	gear_hash_init(&EG(regular_list), 8, NULL, list_entry_destructor, 0);
	return SUCCESS;
}


int gear_init_rsrc_plist(void)
{
	gear_hash_init_ex(&EG(persistent_list), 8, NULL, plist_entry_destructor, 1, 0);
	return SUCCESS;
}


static int gear_close_rsrc(zval *zv)
{
	gear_resource *res = Z_PTR_P(zv);

	if (res->type >= 0) {
		gear_resource_dtor(res);
	}
	return GEAR_HASH_APPLY_KEEP;
}


void gear_close_rsrc_list(HashTable *ht)
{
	gear_hash_reverse_apply(ht, gear_close_rsrc);
}


void gear_destroy_rsrc_list(HashTable *ht)
{
	gear_hash_graceful_reverse_destroy(ht);
}

static int clean_capi_resource(zval *zv, void *arg)
{
	int resource_id = *(int *)arg;
	if (Z_RES_TYPE_P(zv) == resource_id) {
		return 1;
	} else {
		return 0;
	}
}


static int gear_clean_capi_rsrc_dtors_cb(zval *zv, void *arg)
{
	gear_rsrc_list_dtors_entry *ld = (gear_rsrc_list_dtors_entry *)Z_PTR_P(zv);
	int capi_number = *(int *)arg;
	if (ld->capi_number == capi_number) {
		gear_hash_apply_with_argument(&EG(persistent_list), clean_capi_resource, (void *) &(ld->resource_id));
		return 1;
	} else {
		return 0;
	}
}


void gear_clean_capi_rsrc_dtors(int capi_number)
{
	gear_hash_apply_with_argument(&list_destructors, gear_clean_capi_rsrc_dtors_cb, (void *) &capi_number);
}


GEAR_API int gear_register_list_destructors_ex(rsrc_dtor_func_t ld, rsrc_dtor_func_t pld, const char *type_name, int capi_number)
{
	gear_rsrc_list_dtors_entry *lde;
	zval zv;

	lde = malloc(sizeof(gear_rsrc_list_dtors_entry));
	lde->list_dtor_ex = ld;
	lde->plist_dtor_ex = pld;
	lde->capi_number = capi_number;
	lde->resource_id = list_destructors.nNextFreeElement;
	lde->type_name = type_name;
	ZVAL_PTR(&zv, lde);

	if (gear_hash_next_index_insert(&list_destructors, &zv) == NULL) {
		return FAILURE;
	}
	return list_destructors.nNextFreeElement-1;
}

GEAR_API int gear_fetch_list_dtor_id(const char *type_name)
{
	gear_rsrc_list_dtors_entry *lde;

	GEAR_HASH_FOREACH_PTR(&list_destructors, lde) {
		if (lde->type_name && (strcmp(type_name, lde->type_name) == 0)) {
			return lde->resource_id;
		}
	} GEAR_HASH_FOREACH_END();

	return 0;
}

static void list_destructors_dtor(zval *zv)
{
	free(Z_PTR_P(zv));
}

int gear_init_rsrc_list_dtors(void)
{
	gear_hash_init(&list_destructors, 64, NULL, list_destructors_dtor, 1);
	list_destructors.nNextFreeElement=1;	/* we don't want resource type 0 */
	return SUCCESS;
}


void gear_destroy_rsrc_list_dtors(void)
{
	gear_hash_destroy(&list_destructors);
}


const char *gear_rsrc_list_get_rsrc_type(gear_resource *res)
{
	gear_rsrc_list_dtors_entry *lde;

	lde = gear_hash_index_find_ptr(&list_destructors, res->type);
	if (lde) {
		return lde->type_name;
	} else {
		return NULL;
	}
}

GEAR_API gear_resource* gear_register_persistent_resource_ex(gear_string *key, void *rsrc_pointer, int rsrc_type)
{
	zval *zv;
	zval tmp;

	ZVAL_NEW_PERSISTENT_RES(&tmp, -1, rsrc_pointer, rsrc_type);
	GC_MAKE_PERSISTENT_LOCAL(Z_COUNTED(tmp));
	GC_MAKE_PERSISTENT_LOCAL(key);

	zv = gear_hash_update(&EG(persistent_list), key, &tmp);

	return Z_RES_P(zv);
}

GEAR_API gear_resource* gear_register_persistent_resource(const char *key, size_t key_len, void *rsrc_pointer, int rsrc_type)
{
	gear_string *str = gear_string_init(key, key_len, 1);
	gear_resource *ret  = gear_register_persistent_resource_ex(str, rsrc_pointer, rsrc_type);

	gear_string_release_ex(str, 1);
	return ret;
}

