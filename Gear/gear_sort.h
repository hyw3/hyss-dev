/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_SORT_H
#define GEAR_SORT_H

BEGIN_EXTERN_C()
GEAR_API void gear_qsort(void *base, size_t nmemb, size_t siz, compare_func_t cmp, swap_func_t swp);
GEAR_API void gear_sort(void *base, size_t nmemb, size_t siz, compare_func_t cmp, swap_func_t swp);
GEAR_API void gear_insert_sort(void *base, size_t nmemb, size_t siz, compare_func_t cmp, swap_func_t swp);
END_EXTERN_C()

#endif       /* GEAR_SORT_H */

