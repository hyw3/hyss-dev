/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_CONSTANTS_H
#define GEAR_CONSTANTS_H

#include "gear_globals.h"

#define CONST_CS			(1<<0)				/* Case Sensitive */
#define CONST_PERSISTENT		(1<<1)				/* Persistent */
#define CONST_CT_SUBST			(1<<2)				/* Allow compile-time substitution */
#define CONST_NO_FILE_CACHE		(1<<3)				/* Can't be saved in file cache */

#define	HYSS_USER_CONSTANT   0x7fffff /* a constant defined in user space */

/* Flag for gear_get_constant_ex(). Must not class with GEAR_FETCH_CLASS_* flags. */
#define GEAR_GET_CONSTANT_NO_DEPRECATION_CHECK 0x1000

typedef struct _gear_constant {
	zval value;
	gear_string *name;
} gear_constant;

#define GEAR_CONSTANT_FLAGS(c) \
	(Z_CONSTANT_FLAGS((c)->value) & 0xff)

#define GEAR_CONSTANT_CAPI_NUMBER(c) \
	(Z_CONSTANT_FLAGS((c)->value) >> 8)

#define GEAR_CONSTANT_SET_FLAGS(c, _flags, _capi_number) do { \
		Z_CONSTANT_FLAGS((c)->value) = \
			((_flags) & 0xff) | ((_capi_number) << 8); \
	} while (0)

#define REGISTER_NULL_CONSTANT(name, flags)  gear_register_null_constant((name), sizeof(name)-1, (flags), capi_number)
#define REGISTER_BOOL_CONSTANT(name, bval, flags)  gear_register_bool_constant((name), sizeof(name)-1, (bval), (flags), capi_number)
#define REGISTER_LONG_CONSTANT(name, lval, flags)  gear_register_long_constant((name), sizeof(name)-1, (lval), (flags), capi_number)
#define REGISTER_DOUBLE_CONSTANT(name, dval, flags)  gear_register_double_constant((name), sizeof(name)-1, (dval), (flags), capi_number)
#define REGISTER_STRING_CONSTANT(name, str, flags)  gear_register_string_constant((name), sizeof(name)-1, (str), (flags), capi_number)
#define REGISTER_STRINGL_CONSTANT(name, str, len, flags)  gear_register_stringl_constant((name), sizeof(name)-1, (str), (len), (flags), capi_number)

#define REGISTER_NS_NULL_CONSTANT(ns, name, flags)  gear_register_null_constant(GEAR_NS_NAME(ns, name), sizeof(GEAR_NS_NAME(ns, name))-1, (flags), capi_number)
#define REGISTER_NS_BOOL_CONSTANT(ns, name, bval, flags)  gear_register_bool_constant(GEAR_NS_NAME(ns, name), sizeof(GEAR_NS_NAME(ns, name))-1, (bval), (flags), capi_number)
#define REGISTER_NS_LONG_CONSTANT(ns, name, lval, flags)  gear_register_long_constant(GEAR_NS_NAME(ns, name), sizeof(GEAR_NS_NAME(ns, name))-1, (lval), (flags), capi_number)
#define REGISTER_NS_DOUBLE_CONSTANT(ns, name, dval, flags)  gear_register_double_constant(GEAR_NS_NAME(ns, name), sizeof(GEAR_NS_NAME(ns, name))-1, (dval), (flags), capi_number)
#define REGISTER_NS_STRING_CONSTANT(ns, name, str, flags)  gear_register_string_constant(GEAR_NS_NAME(ns, name), sizeof(GEAR_NS_NAME(ns, name))-1, (str), (flags), capi_number)
#define REGISTER_NS_STRINGL_CONSTANT(ns, name, str, len, flags)  gear_register_stringl_constant(GEAR_NS_NAME(ns, name), sizeof(GEAR_NS_NAME(ns, name))-1, (str), (len), (flags), capi_number)

#define REGISTER_MAIN_NULL_CONSTANT(name, flags)  gear_register_null_constant((name), sizeof(name)-1, (flags), 0)
#define REGISTER_MAIN_BOOL_CONSTANT(name, bval, flags)  gear_register_bool_constant((name), sizeof(name)-1, (bval), (flags), 0)
#define REGISTER_MAIN_LONG_CONSTANT(name, lval, flags)  gear_register_long_constant((name), sizeof(name)-1, (lval), (flags), 0)
#define REGISTER_MAIN_DOUBLE_CONSTANT(name, dval, flags)  gear_register_double_constant((name), sizeof(name)-1, (dval), (flags), 0)
#define REGISTER_MAIN_STRING_CONSTANT(name, str, flags)  gear_register_string_constant((name), sizeof(name)-1, (str), (flags), 0)
#define REGISTER_MAIN_STRINGL_CONSTANT(name, str, len, flags)  gear_register_stringl_constant((name), sizeof(name)-1, (str), (len), (flags), 0)

BEGIN_EXTERN_C()
void clean_capi_constants(int capi_number);
void free_gear_constant(zval *zv);
int gear_startup_constants(void);
int gear_shutdown_constants(void);
void gear_register_standard_constants(void);
GEAR_API int gear_verify_const_access(gear_class_constant *c, gear_class_entry *ce);
GEAR_API zval *gear_get_constant(gear_string *name);
GEAR_API zval *gear_get_constant_str(const char *name, size_t name_len);
GEAR_API zval *gear_get_constant_ex(gear_string *name, gear_class_entry *scope, uint32_t flags);
GEAR_API void gear_register_bool_constant(const char *name, size_t name_len, gear_bool bval, int flags, int capi_number);
GEAR_API void gear_register_null_constant(const char *name, size_t name_len, int flags, int capi_number);
GEAR_API void gear_register_long_constant(const char *name, size_t name_len, gear_long lval, int flags, int capi_number);
GEAR_API void gear_register_double_constant(const char *name, size_t name_len, double dval, int flags, int capi_number);
GEAR_API void gear_register_string_constant(const char *name, size_t name_len, char *strval, int flags, int capi_number);
GEAR_API void gear_register_stringl_constant(const char *name, size_t name_len, char *strval, size_t strlen, int flags, int capi_number);
GEAR_API int gear_register_constant(gear_constant *c);
#ifdef ZTS
void gear_copy_constants(HashTable *target, HashTable *sourc);
#endif
END_EXTERN_C()

#define GEAR_CONSTANT_DTOR free_gear_constant

#endif

