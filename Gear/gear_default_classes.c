/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"
#include "gear_builtin_functions.h"
#include "gear_interfaces.h"
#include "gear_exceptions.h"
#include "gear_closures.h"
#include "gear_generators.h"


GEAR_API void gear_register_default_classes(void)
{
	gear_register_interfaces();
	gear_register_default_exception();
	gear_register_iterator_wrapper();
	gear_register_closure_ce();
	gear_register_generator_ce();
}

