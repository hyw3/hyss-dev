/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_constants.h"
#include "gear_exceptions.h"
#include "gear_execute.h"
#include "gear_variables.h"
#include "gear_operators.h"
#include "gear_globals.h"
#include "gear_API.h"

/* Protection from recursive self-referencing class constants */
#define IS_CONSTANT_VISITED_MARK    0x80

#define IS_CONSTANT_VISITED(zv)     (Z_ACCESS_FLAGS_P(zv) & IS_CONSTANT_VISITED_MARK)
#define MARK_CONSTANT_VISITED(zv)   Z_ACCESS_FLAGS_P(zv) |= IS_CONSTANT_VISITED_MARK
#define RESET_CONSTANT_VISITED(zv)  Z_ACCESS_FLAGS_P(zv) &= ~IS_CONSTANT_VISITED_MARK

void free_gear_constant(zval *zv)
{
	gear_constant *c = Z_PTR_P(zv);

	if (!(GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT)) {
		zval_ptr_dtor_nogc(&c->value);
		if (c->name) {
			gear_string_release_ex(c->name, 0);
		}
		efree(c);
	} else {
		zval_internal_ptr_dtor(&c->value);
		if (c->name) {
			gear_string_release_ex(c->name, 1);
		}
		free(c);
	}
}


#ifdef ZTS
static void copy_gear_constant(zval *zv)
{
	gear_constant *c = Z_PTR_P(zv);

	GEAR_ASSERT(GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT);
	Z_PTR_P(zv) = pemalloc(sizeof(gear_constant), 1);
	memcpy(Z_PTR_P(zv), c, sizeof(gear_constant));

	c = Z_PTR_P(zv);
	c->name = gear_string_copy(c->name);
	if (Z_TYPE(c->value) == IS_STRING) {
		Z_STR(c->value) = gear_string_dup(Z_STR(c->value), 1);
	}
}


void gear_copy_constants(HashTable *target, HashTable *source)
{
	gear_hash_copy(target, source, copy_gear_constant);
}
#endif


static int clean_capi_constant(zval *el, void *arg)
{
	gear_constant *c = (gear_constant *)Z_PTR_P(el);
	int capi_number = *(int *)arg;

	if (GEAR_CONSTANT_CAPI_NUMBER(c) == capi_number) {
		return 1;
	} else {
		return 0;
	}
}


void clean_capi_constants(int capi_number)
{
	gear_hash_apply_with_argument(EG(gear_constants), clean_capi_constant, (void *) &capi_number);
}


int gear_startup_constants(void)
{
	EG(gear_constants) = (HashTable *) malloc(sizeof(HashTable));

	gear_hash_init(EG(gear_constants), 128, NULL, GEAR_CONSTANT_DTOR, 1);
	return SUCCESS;
}



void gear_register_standard_constants(void)
{
	REGISTER_MAIN_LONG_CONSTANT("E_ERROR", E_ERROR, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_RECOVERABLE_ERROR", E_RECOVERABLE_ERROR, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_WARNING", E_WARNING, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_PARSE", E_PARSE, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_NOTICE", E_NOTICE, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_STRICT", E_STRICT, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_DEPRECATED", E_DEPRECATED, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_CORE_ERROR", E_CORE_ERROR, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_CORE_WARNING", E_CORE_WARNING, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_COMPILE_ERROR", E_COMPILE_ERROR, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_COMPILE_WARNING", E_COMPILE_WARNING, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_USER_ERROR", E_USER_ERROR, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_USER_WARNING", E_USER_WARNING, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_USER_NOTICE", E_USER_NOTICE, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("E_USER_DEPRECATED", E_USER_DEPRECATED, CONST_PERSISTENT | CONST_CS);

	REGISTER_MAIN_LONG_CONSTANT("E_ALL", E_ALL, CONST_PERSISTENT | CONST_CS);

	REGISTER_MAIN_LONG_CONSTANT("DEBUG_BACKTRACE_PROVIDE_OBJECT", DEBUG_BACKTRACE_PROVIDE_OBJECT, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("DEBUG_BACKTRACE_IGNORE_ARGS", DEBUG_BACKTRACE_IGNORE_ARGS, CONST_PERSISTENT | CONST_CS);
	/* true/false constants */
	{
		REGISTER_MAIN_BOOL_CONSTANT("TRUE", 1, CONST_PERSISTENT | CONST_CT_SUBST);
		REGISTER_MAIN_BOOL_CONSTANT("FALSE", 0, CONST_PERSISTENT | CONST_CT_SUBST);
		REGISTER_MAIN_BOOL_CONSTANT("GEAR_THREAD_SAFE", ZTS_V, CONST_PERSISTENT | CONST_CS);
		REGISTER_MAIN_BOOL_CONSTANT("GEAR_DEBUG_BUILD", GEAR_DEBUG, CONST_PERSISTENT | CONST_CS);
	}
	REGISTER_MAIN_NULL_CONSTANT("NULL", CONST_PERSISTENT | CONST_CT_SUBST);
}


int gear_shutdown_constants(void)
{
	gear_hash_destroy(EG(gear_constants));
	free(EG(gear_constants));
	return SUCCESS;
}

GEAR_API void gear_register_null_constant(const char *name, size_t name_len, int flags, int capi_number)
{
	gear_constant c;

	ZVAL_NULL(&c.value);
	GEAR_CONSTANT_SET_FLAGS(&c, flags, capi_number);
	c.name = gear_string_init_interned(name, name_len, flags & CONST_PERSISTENT);
	gear_register_constant(&c);
}

GEAR_API void gear_register_bool_constant(const char *name, size_t name_len, gear_bool bval, int flags, int capi_number)
{
	gear_constant c;

	ZVAL_BOOL(&c.value, bval);
	GEAR_CONSTANT_SET_FLAGS(&c, flags, capi_number);
	c.name = gear_string_init_interned(name, name_len, flags & CONST_PERSISTENT);
	gear_register_constant(&c);
}

GEAR_API void gear_register_long_constant(const char *name, size_t name_len, gear_long lval, int flags, int capi_number)
{
	gear_constant c;

	ZVAL_LONG(&c.value, lval);
	GEAR_CONSTANT_SET_FLAGS(&c, flags, capi_number);
	c.name = gear_string_init_interned(name, name_len, flags & CONST_PERSISTENT);
	gear_register_constant(&c);
}


GEAR_API void gear_register_double_constant(const char *name, size_t name_len, double dval, int flags, int capi_number)
{
	gear_constant c;

	ZVAL_DOUBLE(&c.value, dval);
	GEAR_CONSTANT_SET_FLAGS(&c, flags, capi_number);
	c.name = gear_string_init_interned(name, name_len, flags & CONST_PERSISTENT);
	gear_register_constant(&c);
}


GEAR_API void gear_register_stringl_constant(const char *name, size_t name_len, char *strval, size_t strlen, int flags, int capi_number)
{
	gear_constant c;

	ZVAL_STR(&c.value, gear_string_init_interned(strval, strlen, flags & CONST_PERSISTENT));
	GEAR_CONSTANT_SET_FLAGS(&c, flags, capi_number);
	c.name = gear_string_init_interned(name, name_len, flags & CONST_PERSISTENT);
	gear_register_constant(&c);
}


GEAR_API void gear_register_string_constant(const char *name, size_t name_len, char *strval, int flags, int capi_number)
{
	gear_register_stringl_constant(name, name_len, strval, strlen(strval), flags, capi_number);
}

static gear_constant *gear_get_special_constant(const char *name, size_t name_len)
{
	gear_constant *c;
	static const char haltoff[] = "__COMPILER_HALT_OFFSET__";

	if (!EG(current_execute_data)) {
		return NULL;
	} else if (name_len == sizeof("__COMPILER_HALT_OFFSET__")-1 &&
	          !memcmp(name, "__COMPILER_HALT_OFFSET__", sizeof("__COMPILER_HALT_OFFSET__")-1)) {
		const char *cfilename;
		gear_string *haltname;
		size_t clen;

		cfilename = gear_get_executed_filename();
		clen = strlen(cfilename);
		/* check for __COMPILER_HALT_OFFSET__ */
		haltname = gear_mangle_property_name(haltoff,
			sizeof("__COMPILER_HALT_OFFSET__") - 1, cfilename, clen, 0);
		c = gear_hash_find_ptr(EG(gear_constants), haltname);
		gear_string_efree(haltname);
		return c;
	} else {
		return NULL;
	}
}

GEAR_API int gear_verify_const_access(gear_class_constant *c, gear_class_entry *scope) /* {{{ */
{
	if (Z_ACCESS_FLAGS(c->value) & GEAR_ACC_PUBLIC) {
		return 1;
	} else if (Z_ACCESS_FLAGS(c->value) & GEAR_ACC_PRIVATE) {
		return (c->ce == scope);
	} else {
		GEAR_ASSERT(Z_ACCESS_FLAGS(c->value) & GEAR_ACC_PROTECTED);
		return gear_check_protected(c->ce, scope);
	}
}
/* }}} */

static inline gear_constant *gear_get_constant_str_impl(const char *name, size_t name_len)
{
	gear_constant *c;
	ALLOCA_FLAG(use_heap)

	if ((c = gear_hash_str_find_ptr(EG(gear_constants), name, name_len)) == NULL) {
		char *lcname = do_alloca(name_len + 1, use_heap);
		gear_str_tolower_copy(lcname, name, name_len);
		if ((c = gear_hash_str_find_ptr(EG(gear_constants), lcname, name_len)) != NULL) {
			if (GEAR_CONSTANT_FLAGS(c) & CONST_CS) {
				c = NULL;
			}
		} else {
			c = gear_get_special_constant(name, name_len);
		}
		free_alloca(lcname, use_heap);
	}

	return c;
}

GEAR_API zval *gear_get_constant_str(const char *name, size_t name_len)
{
	gear_constant *c = gear_get_constant_str_impl(name, name_len);
	return c ? &c->value : NULL;
}

static inline gear_constant *gear_get_constant_impl(gear_string *name)
{
    zval *zv;
	gear_constant *c;
	ALLOCA_FLAG(use_heap)

	zv = gear_hash_find(EG(gear_constants), name);
	if (zv == NULL) {
		char *lcname = do_alloca(ZSTR_LEN(name) + 1, use_heap);
		gear_str_tolower_copy(lcname, ZSTR_VAL(name), ZSTR_LEN(name));
		zv = gear_hash_str_find(EG(gear_constants), lcname, ZSTR_LEN(name));
		if (zv != NULL) {
			c = Z_PTR_P(zv);
			if (GEAR_CONSTANT_FLAGS(c) & CONST_CS) {
				c = NULL;
			}
		} else {
			c = gear_get_special_constant(ZSTR_VAL(name), ZSTR_LEN(name));
		}
		free_alloca(lcname, use_heap);
		return c;
	} else {
		return (gear_constant *) Z_PTR_P(zv);
	}
}

GEAR_API zval *gear_get_constant(gear_string *name)
{
	gear_constant *c = gear_get_constant_impl(name);
	return c ? &c->value : NULL;
}

static gear_bool is_access_deprecated(const gear_constant *c, const char *access_name) {
	const char *ns_sep = gear_memrchr(ZSTR_VAL(c->name), '\\', ZSTR_LEN(c->name));
	if (ns_sep) {
		/* Namespaces are always case-insensitive. Only compare shortname. */
		size_t shortname_offset = ns_sep - ZSTR_VAL(c->name) + 1;
		size_t shortname_len = ZSTR_LEN(c->name) - shortname_offset;
		return memcmp(
			access_name + shortname_offset,
			ZSTR_VAL(c->name) + shortname_offset,
			shortname_len
		) != 0;
	} else {
		/* No namespace, compare whole name */
		return memcmp(access_name, ZSTR_VAL(c->name), ZSTR_LEN(c->name)) != 0;
	}
}

GEAR_API zval *gear_get_constant_ex(gear_string *cname, gear_class_entry *scope, uint32_t flags)
{
	gear_constant *c;
	const char *colon;
	gear_class_entry *ce = NULL;
	const char *name = ZSTR_VAL(cname);
	size_t name_len = ZSTR_LEN(cname);

	/* Skip leading \\ */
	if (name[0] == '\\') {
		name += 1;
		name_len -= 1;
		cname = NULL;
	}

	if ((colon = gear_memrchr(name, ':', name_len)) &&
	    colon > name && (*(colon - 1) == ':')) {
		int class_name_len = colon - name - 1;
		size_t const_name_len = name_len - class_name_len - 2;
		gear_string *constant_name = gear_string_init(colon + 1, const_name_len, 0);
		gear_string *class_name = gear_string_init(name, class_name_len, 0);
		gear_class_constant *c = NULL;
		zval *ret_constant = NULL;

		if (gear_string_equals_literal_ci(class_name, "self")) {
			if (UNEXPECTED(!scope)) {
				gear_throw_error(NULL, "Cannot access self:: when no class scope is active");
				goto failure;
			}
			ce = scope;
		} else if (gear_string_equals_literal_ci(class_name, "parent")) {
			if (UNEXPECTED(!scope)) {
				gear_throw_error(NULL, "Cannot access parent:: when no class scope is active");
				goto failure;
			} else if (UNEXPECTED(!scope->parent)) {
				gear_throw_error(NULL, "Cannot access parent:: when current class scope has no parent");
				goto failure;
			} else {
				ce = scope->parent;
			}
		} else if (gear_string_equals_literal_ci(class_name, "static")) {
			ce = gear_get_called_scope(EG(current_execute_data));
			if (UNEXPECTED(!ce)) {
				gear_throw_error(NULL, "Cannot access static:: when no class scope is active");
				goto failure;
			}
		} else {
			ce = gear_fetch_class(class_name, flags);
		}
		if (ce) {
			c = gear_hash_find_ptr(&ce->constants_table, constant_name);
			if (c == NULL) {
				if ((flags & GEAR_FETCH_CLASS_SILENT) == 0) {
					gear_throw_error(NULL, "Undefined class constant '%s::%s'", ZSTR_VAL(class_name), ZSTR_VAL(constant_name));
					goto failure;
				}
				ret_constant = NULL;
			} else {
				if (!gear_verify_const_access(c, scope)) {
					if ((flags & GEAR_FETCH_CLASS_SILENT) == 0) {
						gear_throw_error(NULL, "Cannot access %s const %s::%s", gear_visibility_string(Z_ACCESS_FLAGS(c->value)), ZSTR_VAL(class_name), ZSTR_VAL(constant_name));
					}
					goto failure;
				}
				ret_constant = &c->value;
			}
		}

		if (ret_constant && Z_TYPE_P(ret_constant) == IS_CONSTANT_AST) {
			int ret;

			if (IS_CONSTANT_VISITED(ret_constant)) {
				gear_throw_error(NULL, "Cannot declare self-referencing constant '%s::%s'", ZSTR_VAL(class_name), ZSTR_VAL(constant_name));
				ret_constant = NULL;
				goto failure;
			}

			MARK_CONSTANT_VISITED(ret_constant);
			ret = zval_update_constant_ex(ret_constant, c->ce);
			RESET_CONSTANT_VISITED(ret_constant);

			if (UNEXPECTED(ret != SUCCESS)) {
				ret_constant = NULL;
				goto failure;
			}
		}
failure:
		gear_string_release_ex(class_name, 0);
		gear_string_efree(constant_name);
		return ret_constant;
	}

	/* non-class constant */
	if ((colon = gear_memrchr(name, '\\', name_len)) != NULL) {
		/* compound constant name */
		int prefix_len = colon - name;
		size_t const_name_len = name_len - prefix_len - 1;
		const char *constant_name = colon + 1;
		char *lcname;
		size_t lcname_len;
		ALLOCA_FLAG(use_heap)

		lcname_len = prefix_len + 1 + const_name_len;
		lcname = do_alloca(lcname_len + 1, use_heap);
		gear_str_tolower_copy(lcname, name, prefix_len);
		/* Check for namespace constant */

		lcname[prefix_len] = '\\';
		memcpy(lcname + prefix_len + 1, constant_name, const_name_len + 1);

		if ((c = gear_hash_str_find_ptr(EG(gear_constants), lcname, lcname_len)) == NULL) {
			/* try lowercase */
			gear_str_tolower(lcname + prefix_len + 1, const_name_len);
			if ((c = gear_hash_str_find_ptr(EG(gear_constants), lcname, lcname_len)) != NULL) {
				if ((GEAR_CONSTANT_FLAGS(c) & CONST_CS) != 0) {
					c = NULL;
				}
			}
		}
		free_alloca(lcname, use_heap);

		if (!c) {
			if (!(flags & IS_CONSTANT_UNQUALIFIED)) {
				return NULL;
			}

			/* name requires runtime resolution, need to check non-namespaced name */
			c = gear_get_constant_str_impl(constant_name, const_name_len);
			name = constant_name;
		}
	} else {
		if (cname) {
			c = gear_get_constant_impl(cname);
		} else {
			c = gear_get_constant_str_impl(name, name_len);
		}
	}

	if (!c) {
		return NULL;
	}

	if (!(flags & GEAR_GET_CONSTANT_NO_DEPRECATION_CHECK)) {
		if (!(GEAR_CONSTANT_FLAGS(c) & (CONST_CS|CONST_CT_SUBST)) && is_access_deprecated(c, name)) {
			gear_error(E_DEPRECATED,
				"Case-insensitive constants are deprecated. "
				"The correct casing for this constant is \"%s\"",
				ZSTR_VAL(c->name));
		}
	}

	return &c->value;
}

static void* gear_hash_add_constant(HashTable *ht, gear_string *key, gear_constant *c)
{
	void *ret;
	gear_constant *copy = pemalloc(sizeof(gear_constant), GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT);

	memcpy(copy, c, sizeof(gear_constant));
	ret = gear_hash_add_ptr(ht, key, copy);
	if (!ret) {
		pefree(copy, GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT);
	}
	return ret;
}

GEAR_API int gear_register_constant(gear_constant *c)
{
	gear_string *lowercase_name = NULL;
	gear_string *name;
	int ret = SUCCESS;

#if 0
	printf("Registering constant for cAPI %d\n", c->capi_number);
#endif

	if (!(GEAR_CONSTANT_FLAGS(c) & CONST_CS)) {
		lowercase_name = gear_string_tolower_ex(c->name, GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT);
		lowercase_name = gear_new_interned_string(lowercase_name);
		name = lowercase_name;
	} else {
		char *slash = strrchr(ZSTR_VAL(c->name), '\\');
		if (slash) {
			lowercase_name = gear_string_init(ZSTR_VAL(c->name), ZSTR_LEN(c->name), GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT);
			gear_str_tolower(ZSTR_VAL(lowercase_name), slash - ZSTR_VAL(c->name));
			lowercase_name = gear_new_interned_string(lowercase_name);
			name = lowercase_name;
		} else {
			name = c->name;
		}
	}

	/* Check if the user is trying to define the internal pseudo constant name __COMPILER_HALT_OFFSET__ */
	if (gear_string_equals_literal(name, "__COMPILER_HALT_OFFSET__")
		|| gear_hash_add_constant(EG(gear_constants), name, c) == NULL) {

		/* The internal __COMPILER_HALT_OFFSET__ is prefixed by NULL byte */
		if (ZSTR_VAL(c->name)[0] == '\0' && ZSTR_LEN(c->name) > sizeof("\0__COMPILER_HALT_OFFSET__")-1
			&& memcmp(ZSTR_VAL(name), "\0__COMPILER_HALT_OFFSET__", sizeof("\0__COMPILER_HALT_OFFSET__")) == 0) {
		}
		gear_error(E_NOTICE,"Constant %s already defined", ZSTR_VAL(name));
		gear_string_release(c->name);
		if (!(GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT)) {
			zval_ptr_dtor_nogc(&c->value);
		}
		ret = FAILURE;
	}
	if (lowercase_name) {
		gear_string_release(lowercase_name);
	}
	return ret;
}

