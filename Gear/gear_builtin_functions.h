/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_BUILTIN_FUNCTIONS_H
#define GEAR_BUILTIN_FUNCTIONS_H

int gear_startup_builtin_functions(void);

BEGIN_EXTERN_C()
GEAR_API void gear_fetch_debug_backtrace(zval *return_value, int skip_last, int options, int limit);
END_EXTERN_C()

#endif /* GEAR_BUILTIN_FUNCTIONS_H */

