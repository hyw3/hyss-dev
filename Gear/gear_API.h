/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_API_H
#define GEAR_API_H

#include "gear_capis.h"
#include "gear_list.h"
#include "gear_operators.h"
#include "gear_variables.h"
#include "gear_execute.h"


BEGIN_EXTERN_C()

typedef struct _gear_function_entry {
	const char *fname;
	zif_handler handler;
	const struct _gear_internal_arg_info *arg_info;
	uint32_t num_args;
	uint32_t flags;
} gear_function_entry;

typedef struct _gear_fcall_info {
	size_t size;
	zval function_name;
	zval *retval;
	zval *params;
	gear_object *object;
	gear_bool no_separation;
	uint32_t param_count;
} gear_fcall_info;

typedef struct _gear_fcall_info_cache {
	gear_function *function_handler;
	gear_class_entry *calling_scope;
	gear_class_entry *called_scope;
	gear_object *object;
} gear_fcall_info_cache;

#define GEAR_NS_NAME(ns, name)			ns "\\" name

#define GEAR_FN(name) zif_##name
#define GEAR_MN(name) zim_##name
#define GEAR_NAMED_FUNCTION(name)		void GEAR_FASTCALL name(INTERNAL_FUNCTION_PARAMETERS)
#define GEAR_FUNCTION(name)				GEAR_NAMED_FUNCTION(GEAR_FN(name))
#define GEAR_METHOD(classname, name)	GEAR_NAMED_FUNCTION(GEAR_MN(classname##_##name))

#define GEAR_FENTRY(gear_name, name, arg_info, flags)	{ #gear_name, name, arg_info, (uint32_t) (sizeof(arg_info)/sizeof(struct _gear_internal_arg_info)-1), flags },

#define GEAR_RAW_FENTRY(gear_name, name, arg_info, flags)   { gear_name, name, arg_info, (uint32_t) (sizeof(arg_info)/sizeof(struct _gear_internal_arg_info)-1), flags },
#define GEAR_RAW_NAMED_FE(gear_name, name, arg_info) GEAR_RAW_FENTRY(#gear_name, name, arg_info, 0)

#define GEAR_NAMED_FE(gear_name, name, arg_info)	GEAR_FENTRY(gear_name, name, arg_info, 0)
#define GEAR_FE(name, arg_info)						GEAR_FENTRY(name, GEAR_FN(name), arg_info, 0)
#define GEAR_DEP_FE(name, arg_info)                 GEAR_FENTRY(name, GEAR_FN(name), arg_info, GEAR_ACC_DEPRECATED)
#define GEAR_FALIAS(name, alias, arg_info)			GEAR_FENTRY(name, GEAR_FN(alias), arg_info, 0)
#define GEAR_DEP_FALIAS(name, alias, arg_info)		GEAR_FENTRY(name, GEAR_FN(alias), arg_info, GEAR_ACC_DEPRECATED)
#define GEAR_NAMED_ME(gear_name, name, arg_info, flags)	GEAR_FENTRY(gear_name, name, arg_info, flags)
#define GEAR_ME(classname, name, arg_info, flags)	GEAR_FENTRY(name, GEAR_MN(classname##_##name), arg_info, flags)
#define GEAR_ABSTRACT_ME(classname, name, arg_info)	GEAR_FENTRY(name, NULL, arg_info, GEAR_ACC_PUBLIC|GEAR_ACC_ABSTRACT)
#define GEAR_MALIAS(classname, name, alias, arg_info, flags) \
                                                    GEAR_FENTRY(name, GEAR_MN(classname##_##alias), arg_info, flags)
#define GEAR_ME_MAPPING(name, func_name, arg_types, flags) GEAR_NAMED_ME(name, GEAR_FN(func_name), arg_types, flags)

#define GEAR_NS_FENTRY(ns, gear_name, name, arg_info, flags)		GEAR_RAW_FENTRY(GEAR_NS_NAME(ns, #gear_name), name, arg_info, flags)

#define GEAR_NS_RAW_FENTRY(ns, gear_name, name, arg_info, flags)	GEAR_RAW_FENTRY(GEAR_NS_NAME(ns, gear_name), name, arg_info, flags)
#define GEAR_NS_RAW_NAMED_FE(ns, gear_name, name, arg_info)			GEAR_NS_RAW_FENTRY(ns, #gear_name, name, arg_info, 0)

#define GEAR_NS_NAMED_FE(ns, gear_name, name, arg_info)	GEAR_NS_FENTRY(ns, gear_name, name, arg_info, 0)
#define GEAR_NS_FE(ns, name, arg_info)					GEAR_NS_FENTRY(ns, name, GEAR_FN(name), arg_info, 0)
#define GEAR_NS_DEP_FE(ns, name, arg_info)				GEAR_NS_FENTRY(ns, name, GEAR_FN(name), arg_info, GEAR_ACC_DEPRECATED)
#define GEAR_NS_FALIAS(ns, name, alias, arg_info)		GEAR_NS_FENTRY(ns, name, GEAR_FN(alias), arg_info, 0)
#define GEAR_NS_DEP_FALIAS(ns, name, alias, arg_info)	GEAR_NS_FENTRY(ns, name, GEAR_FN(alias), arg_info, GEAR_ACC_DEPRECATED)

#define GEAR_FE_END            { NULL, NULL, NULL, 0, 0 }

#define GEAR_ARG_INFO(pass_by_ref, name)                             { #name, 0, pass_by_ref, 0},
#define GEAR_ARG_PASS_INFO(pass_by_ref)                              { NULL,  0, pass_by_ref, 0},
#define GEAR_ARG_OBJ_INFO(pass_by_ref, name, classname, allow_null)  { #name, GEAR_TYPE_ENCODE_CLASS_CONST(#classname, allow_null), pass_by_ref, 0 },
#define GEAR_ARG_ARRAY_INFO(pass_by_ref, name, allow_null)           { #name, GEAR_TYPE_ENCODE(IS_ARRAY, allow_null), pass_by_ref, 0 },
#define GEAR_ARG_CALLABLE_INFO(pass_by_ref, name, allow_null)        { #name, GEAR_TYPE_ENCODE(IS_CALLABLE, allow_null), pass_by_ref, 0 },
#define GEAR_ARG_TYPE_INFO(pass_by_ref, name, type_hint, allow_null) { #name, GEAR_TYPE_ENCODE(type_hint, allow_null), pass_by_ref, 0 },
#define GEAR_ARG_VARIADIC_INFO(pass_by_ref, name)                             { #name, 0, pass_by_ref, 1 },
#define GEAR_ARG_VARIADIC_TYPE_INFO(pass_by_ref, name, type_hint, allow_null) { #name, GEAR_TYPE_ENCODE(type_hint, allow_null), pass_by_ref, 1 },
#define GEAR_ARG_VARIADIC_OBJ_INFO(pass_by_ref, name, classname, allow_null)  { #name, GEAR_TYPE_ENCODE_CLASS_CONST(#classname, allow_null), pass_by_ref, 1 },

#define GEAR_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(name, return_reference, required_num_args, class_name, allow_null) \
	static const gear_internal_arg_info name[] = { \
		{ (const char*)(gear_uintptr_t)(required_num_args), GEAR_TYPE_ENCODE_CLASS_CONST(#class_name, allow_null), return_reference, 0 },

#define GEAR_BEGIN_ARG_WITH_RETURN_OBJ_INFO(name, class_name, allow_null) \
	GEAR_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(name, 0, -1, class_name, allow_null)

#define GEAR_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(name, return_reference, required_num_args, type, allow_null) \
	static const gear_internal_arg_info name[] = { \
		{ (const char*)(gear_uintptr_t)(required_num_args), GEAR_TYPE_ENCODE(type, allow_null), return_reference, 0 },
#define GEAR_BEGIN_ARG_WITH_RETURN_TYPE_INFO(name, type, allow_null) \
	GEAR_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(name, 0, -1, type, allow_null)

#define GEAR_BEGIN_ARG_INFO_EX(name, _unused, return_reference, required_num_args)	\
	static const gear_internal_arg_info name[] = { \
		{ (const char*)(gear_uintptr_t)(required_num_args), 0, return_reference, 0 },
#define GEAR_BEGIN_ARG_INFO(name, _unused)	\
	GEAR_BEGIN_ARG_INFO_EX(name, 0, GEAR_RETURN_VALUE, -1)
#define GEAR_END_ARG_INFO()		};

/* Name macros */
#define GEAR_CAPI_STARTUP_N(cAPI)       zm_startup_##cAPI
#define GEAR_CAPI_SHUTDOWN_N(cAPI)		zm_shutdown_##cAPI
#define GEAR_CAPI_ACTIVATE_N(cAPI)		zm_activate_##cAPI
#define GEAR_CAPI_DEACTIVATE_N(cAPI)	zm_deactivate_##cAPI
#define GEAR_CAPI_POST_GEAR_DEACTIVATE_N(cAPI)	zm_post_gear_deactivate_##cAPI
#define GEAR_CAPI_INFO_N(cAPI)			zm_info_##cAPI
#define GEAR_CAPI_GLOBALS_CTOR_N(cAPI)  zm_globals_ctor_##cAPI
#define GEAR_CAPI_GLOBALS_DTOR_N(cAPI)  zm_globals_dtor_##cAPI

/* Declaration macros */
#define GEAR_CAPI_STARTUP_D(cAPI)		int GEAR_CAPI_STARTUP_N(cAPI)(INIT_FUNC_ARGS)
#define GEAR_CAPI_SHUTDOWN_D(cAPI)		int GEAR_CAPI_SHUTDOWN_N(cAPI)(SHUTDOWN_FUNC_ARGS)
#define GEAR_CAPI_ACTIVATE_D(cAPI)		int GEAR_CAPI_ACTIVATE_N(cAPI)(INIT_FUNC_ARGS)
#define GEAR_CAPI_DEACTIVATE_D(cAPI)	int GEAR_CAPI_DEACTIVATE_N(cAPI)(SHUTDOWN_FUNC_ARGS)
#define GEAR_CAPI_POST_GEAR_DEACTIVATE_D(cAPI)	int GEAR_CAPI_POST_GEAR_DEACTIVATE_N(cAPI)(void)
#define GEAR_CAPI_INFO_D(cAPI)			void GEAR_CAPI_INFO_N(cAPI)(GEAR_CAPI_INFO_FUNC_ARGS)
#define GEAR_CAPI_GLOBALS_CTOR_D(cAPI)  void GEAR_CAPI_GLOBALS_CTOR_N(cAPI)(gear_##cAPI##_globals *cAPI##_globals)
#define GEAR_CAPI_GLOBALS_DTOR_D(cAPI)  void GEAR_CAPI_GLOBALS_DTOR_N(cAPI)(gear_##cAPI##_globals *cAPI##_globals)

#define GEAR_GET_CAPI(name) \
    BEGIN_EXTERN_C()\
	GEAR_DLEXPORT gear_capi_entry *get_capi(void) { return &name##_capi_entry; }\
    END_EXTERN_C()

#define GEAR_BEGIN_CAPI_GLOBALS(capi_name)		\
	typedef struct _gear_##capi_name##_globals {
#define GEAR_END_CAPI_GLOBALS(capi_name)		\
	} gear_##capi_name##_globals;

#ifdef ZTS

#define GEAR_DECLARE_CAPI_GLOBALS(capi_name)							\
	ts_rsrc_id capi_name##_globals_id;
#define GEAR_EXTERN_CAPI_GLOBALS(capi_name)								\
	extern ts_rsrc_id capi_name##_globals_id;
#define GEAR_INIT_CAPI_GLOBALS(capi_name, globals_ctor, globals_dtor)	\
	ts_allocate_id(&capi_name##_globals_id, sizeof(gear_##capi_name##_globals), (ts_allocate_ctor) globals_ctor, (ts_allocate_dtor) globals_dtor);
#define GEAR_CAPI_GLOBALS_ACCESSOR(capi_name, v) GEAR_PBCG(capi_name##_globals_id, gear_##capi_name##_globals *, v)
#if GEAR_ENABLE_STATIC_PBCLS_CACHE
#define GEAR_CAPI_GLOBALS_BULK(capi_name) PBCG_BULK_STATIC(capi_name##_globals_id, gear_##capi_name##_globals *)
#else
#define GEAR_CAPI_GLOBALS_BULK(capi_name) PBCG_BULK(capi_name##_globals_id, gear_##capi_name##_globals *)
#endif

#else

#define GEAR_DECLARE_CAPI_GLOBALS(capi_name)							\
	gear_##capi_name##_globals capi_name##_globals;
#define GEAR_EXTERN_CAPI_GLOBALS(capi_name)								\
	extern gear_##capi_name##_globals capi_name##_globals;
#define GEAR_INIT_CAPI_GLOBALS(capi_name, globals_ctor, globals_dtor)	\
	globals_ctor(&capi_name##_globals);
#define GEAR_CAPI_GLOBALS_ACCESSOR(capi_name, v) (capi_name##_globals.v)
#define GEAR_CAPI_GLOBALS_BULK(capi_name) (&capi_name##_globals)

#endif

#define INIT_CLASS_ENTRY(class_container, class_name, functions) \
	INIT_CLASS_ENTRY_EX(class_container, class_name, sizeof(class_name)-1, functions)

#define INIT_CLASS_ENTRY_EX(class_container, class_name, class_name_len, functions) \
	{															\
		memset(&class_container, 0, sizeof(gear_class_entry)); \
		class_container.name = gear_string_init_interned(class_name, class_name_len, 1); \
		class_container.info.internal.builtin_functions = functions;	\
	}

#define INIT_CLASS_ENTRY_INIT_METHODS(class_container, functions) \
	{															\
		class_container.constructor = NULL;						\
		class_container.destructor = NULL;						\
		class_container.clone = NULL;							\
		class_container.serialize = NULL;						\
		class_container.unserialize = NULL;						\
		class_container.create_object = NULL;					\
		class_container.get_static_method = NULL;				\
		class_container.__call = NULL;							\
		class_container.__callstatic = NULL;					\
		class_container.__tostring = NULL;						\
		class_container.__get = NULL;							\
		class_container.__set = NULL;							\
		class_container.__unset = NULL;							\
		class_container.__isset = NULL;							\
		class_container.__debugInfo = NULL;						\
		class_container.serialize_func = NULL;					\
		class_container.unserialize_func = NULL;				\
		class_container.parent = NULL;							\
		class_container.num_interfaces = 0;						\
		class_container.traits = NULL;							\
		class_container.num_traits = 0;							\
		class_container.trait_aliases = NULL;					\
		class_container.trait_precedences = NULL;				\
		class_container.interfaces = NULL;						\
		class_container.get_iterator = NULL;					\
		class_container.iterator_funcs_ptr = NULL;				\
		class_container.info.internal.cAPI = NULL;			\
		class_container.info.internal.builtin_functions = functions;	\
	}


#define INIT_NS_CLASS_ENTRY(class_container, ns, class_name, functions) \
	INIT_CLASS_ENTRY(class_container, GEAR_NS_NAME(ns, class_name), functions)

#ifdef ZTS
#	define CE_STATIC_MEMBERS(ce) (((ce)->type==GEAR_USER_CLASS)?(ce)->static_members_table:CG(static_members_table)[(gear_intptr_t)(ce)->static_members_table])
#else
#	define CE_STATIC_MEMBERS(ce) ((ce)->static_members_table)
#endif

#define GEAR_FCI_INITIALIZED(fci) ((fci).size != 0)

GEAR_API int gear_next_free_capi(void);

BEGIN_EXTERN_C()
GEAR_API int _gear_get_parameters_array_ex(int param_count, zval *argument_array);

/* internal function to efficiently copy parameters when executing __call() */
GEAR_API int gear_copy_parameters_array(int param_count, zval *argument_array);

#define gear_get_parameters_array(ht, param_count, argument_array) \
	_gear_get_parameters_array_ex(param_count, argument_array)
#define gear_get_parameters_array_ex(param_count, argument_array) \
	_gear_get_parameters_array_ex(param_count, argument_array)
#define gear_parse_parameters_none() \
	(EXPECTED(GEAR_NUM_ARGS() == 0) ? SUCCESS : gear_wrong_parameters_none_error())
#define gear_parse_parameters_none_throw() \
	(EXPECTED(GEAR_NUM_ARGS() == 0) ? SUCCESS : gear_wrong_parameters_none_exception())

/* Parameter parsing API -- andrei */

#define GEAR_PARSE_PARAMS_QUIET (1<<1)
#define GEAR_PARSE_PARAMS_THROW (1<<2)
GEAR_API int gear_parse_parameters(int num_args, const char *type_spec, ...);
GEAR_API int gear_parse_parameters_ex(int flags, int num_args, const char *type_spec, ...);
GEAR_API int gear_parse_parameters_throw(int num_args, const char *type_spec, ...);
GEAR_API char *gear_zval_type_name(const zval *arg);
GEAR_API gear_string *gear_zval_get_type(const zval *arg);

GEAR_API int gear_parse_method_parameters(int num_args, zval *this_ptr, const char *type_spec, ...);
GEAR_API int gear_parse_method_parameters_ex(int flags, int num_args, zval *this_ptr, const char *type_spec, ...);

GEAR_API int gear_parse_parameter(int flags, int arg_num, zval *arg, const char *spec, ...);

/* End of parameter parsing API -- andrei */

GEAR_API int gear_register_functions(gear_class_entry *scope, const gear_function_entry *functions, HashTable *function_table, int type);
GEAR_API void gear_unregister_functions(const gear_function_entry *functions, int count, HashTable *function_table);
GEAR_API int gear_startup_capi(gear_capi_entry *capi_entry);
GEAR_API gear_capi_entry* gear_register_internal_capi(gear_capi_entry *capi_entry);
GEAR_API gear_capi_entry* gear_register_capi_ex(gear_capi_entry *cAPI);
GEAR_API int gear_startup_capi_ex(gear_capi_entry *cAPI);
GEAR_API int gear_startup_capis(void);
GEAR_API void gear_collect_capi_handlers(void);
GEAR_API void gear_destroy_capis(void);
GEAR_API void gear_check_magic_method_implementation(const gear_class_entry *ce, const gear_function *fptr, int error_type);

GEAR_API gear_class_entry *gear_register_internal_class(gear_class_entry *class_entry);
GEAR_API gear_class_entry *gear_register_internal_class_ex(gear_class_entry *class_entry, gear_class_entry *parent_ce);
GEAR_API gear_class_entry *gear_register_internal_interface(gear_class_entry *orig_class_entry);
GEAR_API void gear_class_implements(gear_class_entry *class_entry, int num_interfaces, ...);

GEAR_API int gear_register_class_alias_ex(const char *name, size_t name_len, gear_class_entry *ce, int persistent);

#define gear_register_class_alias(name, ce) \
	gear_register_class_alias_ex(name, sizeof(name)-1, ce, 1)
#define gear_register_ns_class_alias(ns, name, ce) \
	gear_register_class_alias_ex(GEAR_NS_NAME(ns, name), sizeof(GEAR_NS_NAME(ns, name))-1, ce, 1)

GEAR_API int gear_disable_function(char *function_name, size_t function_name_length);
GEAR_API int gear_disable_class(char *class_name, size_t class_name_length);

GEAR_API GEAR_COLD void gear_wrong_param_count(void);

#define IS_CALLABLE_CHECK_SYNTAX_ONLY (1<<0)
#define IS_CALLABLE_CHECK_NO_ACCESS   (1<<1)
#define IS_CALLABLE_CHECK_IS_STATIC   (1<<2)
#define IS_CALLABLE_CHECK_SILENT      (1<<3)

#define IS_CALLABLE_STRICT  (IS_CALLABLE_CHECK_IS_STATIC)

GEAR_API gear_string *gear_get_callable_name_ex(zval *callable, gear_object *object);
GEAR_API gear_string *gear_get_callable_name(zval *callable);
GEAR_API gear_bool gear_is_callable_ex(zval *callable, gear_object *object, uint32_t check_flags, gear_string **callable_name, gear_fcall_info_cache *fcc, char **error);
GEAR_API gear_bool gear_is_callable(zval *callable, uint32_t check_flags, gear_string **callable_name);
GEAR_API gear_bool gear_make_callable(zval *callable, gear_string **callable_name);
GEAR_API const char *gear_get_capi_version(const char *capi_name);
GEAR_API int gear_get_capi_started(const char *capi_name);
GEAR_API int gear_declare_property_ex(gear_class_entry *ce, gear_string *name, zval *property, int access_type, gear_string *doc_comment);
GEAR_API int gear_declare_property(gear_class_entry *ce, const char *name, size_t name_length, zval *property, int access_type);
GEAR_API int gear_declare_property_null(gear_class_entry *ce, const char *name, size_t name_length, int access_type);
GEAR_API int gear_declare_property_bool(gear_class_entry *ce, const char *name, size_t name_length, gear_long value, int access_type);
GEAR_API int gear_declare_property_long(gear_class_entry *ce, const char *name, size_t name_length, gear_long value, int access_type);
GEAR_API int gear_declare_property_double(gear_class_entry *ce, const char *name, size_t name_length, double value, int access_type);
GEAR_API int gear_declare_property_string(gear_class_entry *ce, const char *name, size_t name_length, const char *value, int access_type);
GEAR_API int gear_declare_property_stringl(gear_class_entry *ce, const char *name, size_t name_length, const char *value, size_t value_len, int access_type);

GEAR_API int gear_declare_class_constant_ex(gear_class_entry *ce, gear_string *name, zval *value, int access_type, gear_string *doc_comment);
GEAR_API int gear_declare_class_constant(gear_class_entry *ce, const char *name, size_t name_length, zval *value);
GEAR_API int gear_declare_class_constant_null(gear_class_entry *ce, const char *name, size_t name_length);
GEAR_API int gear_declare_class_constant_long(gear_class_entry *ce, const char *name, size_t name_length, gear_long value);
GEAR_API int gear_declare_class_constant_bool(gear_class_entry *ce, const char *name, size_t name_length, gear_bool value);
GEAR_API int gear_declare_class_constant_double(gear_class_entry *ce, const char *name, size_t name_length, double value);
GEAR_API int gear_declare_class_constant_stringl(gear_class_entry *ce, const char *name, size_t name_length, const char *value, size_t value_length);
GEAR_API int gear_declare_class_constant_string(gear_class_entry *ce, const char *name, size_t name_length, const char *value);

GEAR_API int gear_update_class_constants(gear_class_entry *class_type);

GEAR_API void gear_update_property_ex(gear_class_entry *scope, zval *object, gear_string *name, zval *value);
GEAR_API void gear_update_property(gear_class_entry *scope, zval *object, const char *name, size_t name_length, zval *value);
GEAR_API void gear_update_property_null(gear_class_entry *scope, zval *object, const char *name, size_t name_length);
GEAR_API void gear_update_property_bool(gear_class_entry *scope, zval *object, const char *name, size_t name_length, gear_long value);
GEAR_API void gear_update_property_long(gear_class_entry *scope, zval *object, const char *name, size_t name_length, gear_long value);
GEAR_API void gear_update_property_double(gear_class_entry *scope, zval *object, const char *name, size_t name_length, double value);
GEAR_API void gear_update_property_str(gear_class_entry *scope, zval *object, const char *name, size_t name_length, gear_string *value);
GEAR_API void gear_update_property_string(gear_class_entry *scope, zval *object, const char *name, size_t name_length, const char *value);
GEAR_API void gear_update_property_stringl(gear_class_entry *scope, zval *object, const char *name, size_t name_length, const char *value, size_t value_length);
GEAR_API void gear_unset_property(gear_class_entry *scope, zval *object, const char *name, size_t name_length);

GEAR_API int gear_update_static_property_ex(gear_class_entry *scope, gear_string *name, zval *value);
GEAR_API int gear_update_static_property(gear_class_entry *scope, const char *name, size_t name_length, zval *value);
GEAR_API int gear_update_static_property_null(gear_class_entry *scope, const char *name, size_t name_length);
GEAR_API int gear_update_static_property_bool(gear_class_entry *scope, const char *name, size_t name_length, gear_long value);
GEAR_API int gear_update_static_property_long(gear_class_entry *scope, const char *name, size_t name_length, gear_long value);
GEAR_API int gear_update_static_property_double(gear_class_entry *scope, const char *name, size_t name_length, double value);
GEAR_API int gear_update_static_property_string(gear_class_entry *scope, const char *name, size_t name_length, const char *value);
GEAR_API int gear_update_static_property_stringl(gear_class_entry *scope, const char *name, size_t name_length, const char *value, size_t value_length);

GEAR_API zval *gear_read_property_ex(gear_class_entry *scope, zval *object, gear_string *name, gear_bool silent, zval *rv);
GEAR_API zval *gear_read_property(gear_class_entry *scope, zval *object, const char *name, size_t name_length, gear_bool silent, zval *rv);

GEAR_API zval *gear_read_static_property_ex(gear_class_entry *scope, gear_string *name, gear_bool silent);
GEAR_API zval *gear_read_static_property(gear_class_entry *scope, const char *name, size_t name_length, gear_bool silent);

GEAR_API char *gear_get_type_by_const(int type);

#define getThis()							((Z_TYPE(EX(This)) == IS_OBJECT) ? &EX(This) : NULL)
#define GEAR_IS_METHOD_CALL()				(EX(func)->common.scope != NULL)

#define WRONG_PARAM_COUNT					GEAR_WRONG_PARAM_COUNT()
#define WRONG_PARAM_COUNT_WITH_RETVAL(ret)	GEAR_WRONG_PARAM_COUNT_WITH_RETVAL(ret)
#define ARG_COUNT(dummy)					EX_NUM_ARGS()
#define GEAR_NUM_ARGS()						EX_NUM_ARGS()
#define GEAR_WRONG_PARAM_COUNT()					{ gear_wrong_param_count(); return; }
#define GEAR_WRONG_PARAM_COUNT_WITH_RETVAL(ret)		{ gear_wrong_param_count(); return ret; }

#ifndef GEAR_WIN32
#define DLEXPORT
#endif

#define array_init(arg)				ZVAL_ARR((arg), gear_new_array(0))
#define array_init_size(arg, size)	ZVAL_ARR((arg), gear_new_array(size))
GEAR_API int object_init(zval *arg);
GEAR_API int object_init_ex(zval *arg, gear_class_entry *ce);
GEAR_API int object_and_properties_init(zval *arg, gear_class_entry *ce, HashTable *properties);
GEAR_API void object_properties_init(gear_object *object, gear_class_entry *class_type);
GEAR_API void object_properties_init_ex(gear_object *object, HashTable *properties);
GEAR_API void object_properties_load(gear_object *object, HashTable *properties);

GEAR_API void gear_merge_properties(zval *obj, HashTable *properties);

GEAR_API int add_assoc_long_ex(zval *arg, const char *key, size_t key_len, gear_long n);
GEAR_API int add_assoc_null_ex(zval *arg, const char *key, size_t key_len);
GEAR_API int add_assoc_bool_ex(zval *arg, const char *key, size_t key_len, int b);
GEAR_API int add_assoc_resource_ex(zval *arg, const char *key, size_t key_len, gear_resource *r);
GEAR_API int add_assoc_double_ex(zval *arg, const char *key, size_t key_len, double d);
GEAR_API int add_assoc_str_ex(zval *arg, const char *key, size_t key_len, gear_string *str);
GEAR_API int add_assoc_string_ex(zval *arg, const char *key, size_t key_len, const char *str);
GEAR_API int add_assoc_stringl_ex(zval *arg, const char *key, size_t key_len, const char *str, size_t length);
GEAR_API int add_assoc_zval_ex(zval *arg, const char *key, size_t key_len, zval *value);

#define add_assoc_long(__arg, __key, __n) add_assoc_long_ex(__arg, __key, strlen(__key), __n)
#define add_assoc_null(__arg, __key) add_assoc_null_ex(__arg, __key, strlen(__key))
#define add_assoc_bool(__arg, __key, __b) add_assoc_bool_ex(__arg, __key, strlen(__key), __b)
#define add_assoc_resource(__arg, __key, __r) add_assoc_resource_ex(__arg, __key, strlen(__key), __r)
#define add_assoc_double(__arg, __key, __d) add_assoc_double_ex(__arg, __key, strlen(__key), __d)
#define add_assoc_str(__arg, __key, __str) add_assoc_str_ex(__arg, __key, strlen(__key), __str)
#define add_assoc_string(__arg, __key, __str) add_assoc_string_ex(__arg, __key, strlen(__key), __str)
#define add_assoc_stringl(__arg, __key, __str, __length) add_assoc_stringl_ex(__arg, __key, strlen(__key), __str, __length)
#define add_assoc_zval(__arg, __key, __value) add_assoc_zval_ex(__arg, __key, strlen(__key), __value)

/* unset() functions are only supported for legacy cAPIs and null() functions should be used */
#define add_assoc_unset(__arg, __key) add_assoc_null_ex(__arg, __key, strlen(__key))
#define add_index_unset(__arg, __key) add_index_null(__arg, __key)
#define add_next_index_unset(__arg) add_next_index_null(__arg)
#define add_property_unset(__arg, __key) add_property_null(__arg, __key)

GEAR_API int add_index_long(zval *arg, gear_ulong idx, gear_long n);
GEAR_API int add_index_null(zval *arg, gear_ulong idx);
GEAR_API int add_index_bool(zval *arg, gear_ulong idx, int b);
GEAR_API int add_index_resource(zval *arg, gear_ulong idx, gear_resource *r);
GEAR_API int add_index_double(zval *arg, gear_ulong idx, double d);
GEAR_API int add_index_str(zval *arg, gear_ulong idx, gear_string *str);
GEAR_API int add_index_string(zval *arg, gear_ulong idx, const char *str);
GEAR_API int add_index_stringl(zval *arg, gear_ulong idx, const char *str, size_t length);
GEAR_API int add_index_zval(zval *arg, gear_ulong index, zval *value);

GEAR_API int add_next_index_long(zval *arg, gear_long n);
GEAR_API int add_next_index_null(zval *arg);
GEAR_API int add_next_index_bool(zval *arg, int b);
GEAR_API int add_next_index_resource(zval *arg, gear_resource *r);
GEAR_API int add_next_index_double(zval *arg, double d);
GEAR_API int add_next_index_str(zval *arg, gear_string *str);
GEAR_API int add_next_index_string(zval *arg, const char *str);
GEAR_API int add_next_index_stringl(zval *arg, const char *str, size_t length);
GEAR_API int add_next_index_zval(zval *arg, zval *value);

GEAR_API zval *add_get_assoc_string_ex(zval *arg, const char *key, uint32_t key_len, const char *str);
GEAR_API zval *add_get_assoc_stringl_ex(zval *arg, const char *key, uint32_t key_len, const char *str, size_t length);

#define add_get_assoc_string(__arg, __key, __str) add_get_assoc_string_ex(__arg, __key, strlen(__key), __str)
#define add_get_assoc_stringl(__arg, __key, __str, __length) add_get_assoc_stringl_ex(__arg, __key, strlen(__key), __str, __length)

GEAR_API zval *add_get_index_long(zval *arg, gear_ulong idx, gear_long l);
GEAR_API zval *add_get_index_double(zval *arg, gear_ulong idx, double d);
GEAR_API zval *add_get_index_str(zval *arg, gear_ulong index, gear_string *str);
GEAR_API zval *add_get_index_string(zval *arg, gear_ulong idx, const char *str);
GEAR_API zval *add_get_index_stringl(zval *arg, gear_ulong idx, const char *str, size_t length);

GEAR_API int array_set_zval_key(HashTable *ht, zval *key, zval *value);

GEAR_API int add_property_long_ex(zval *arg, const char *key, size_t key_len, gear_long l);
GEAR_API int add_property_null_ex(zval *arg, const char *key, size_t key_len);
GEAR_API int add_property_bool_ex(zval *arg, const char *key, size_t key_len, gear_long b);
GEAR_API int add_property_resource_ex(zval *arg, const char *key, size_t key_len, gear_resource *r);
GEAR_API int add_property_double_ex(zval *arg, const char *key, size_t key_len, double d);
GEAR_API int add_property_str_ex(zval *arg, const char *key, size_t key_len, gear_string *str);
GEAR_API int add_property_string_ex(zval *arg, const char *key, size_t key_len, const char *str);
GEAR_API int add_property_stringl_ex(zval *arg, const char *key, size_t key_len,  const char *str, size_t length);
GEAR_API int add_property_zval_ex(zval *arg, const char *key, size_t key_len, zval *value);

#define add_property_long(__arg, __key, __n) add_property_long_ex(__arg, __key, strlen(__key), __n)
#define add_property_null(__arg, __key) add_property_null_ex(__arg, __key, strlen(__key))
#define add_property_bool(__arg, __key, __b) add_property_bool_ex(__arg, __key, strlen(__key), __b)
#define add_property_resource(__arg, __key, __r) add_property_resource_ex(__arg, __key, strlen(__key), __r)
#define add_property_double(__arg, __key, __d) add_property_double_ex(__arg, __key, strlen(__key), __d)
#define add_property_str(__arg, __key, __str) add_property_str_ex(__arg, __key, strlen(__key), __str)
#define add_property_string(__arg, __key, __str) add_property_string_ex(__arg, __key, strlen(__key), __str)
#define add_property_stringl(__arg, __key, __str, __length) add_property_stringl_ex(__arg, __key, strlen(__key), __str, __length)
#define add_property_zval(__arg, __key, __value) add_property_zval_ex(__arg, __key, strlen(__key), __value)


GEAR_API int _call_user_function_ex(zval *object, zval *function_name, zval *retval_ptr, uint32_t param_count, zval params[], int no_separation);

#define call_user_function(function_table, object, function_name, retval_ptr, param_count, params) \
	_call_user_function_ex(object, function_name, retval_ptr, param_count, params, 1)
#define call_user_function_ex(function_table, object, function_name, retval_ptr, param_count, params, no_separation, symbol_table) \
	_call_user_function_ex(object, function_name, retval_ptr, param_count, params, no_separation)

GEAR_API extern const gear_fcall_info empty_fcall_info;
GEAR_API extern const gear_fcall_info_cache empty_fcall_info_cache;

/** Build gear_call_info/cache from a zval*
 *
 * Caller is responsible to provide a return value (fci->retval), otherwise the we will crash.
 * In order to pass parameters the following members need to be set:
 * fci->param_count = 0;
 * fci->params = NULL;
 * The callable_name argument may be NULL.
 * Set check_flags to IS_CALLABLE_STRICT for every new usage!
 */
GEAR_API int gear_fcall_info_init(zval *callable, uint32_t check_flags, gear_fcall_info *fci, gear_fcall_info_cache *fcc, gear_string **callable_name, char **error);

/** Clear arguments connected with gear_fcall_info *fci
 * If free_mem is not zero then the params array gets free'd as well
 */
GEAR_API void gear_fcall_info_args_clear(gear_fcall_info *fci, int free_mem);

/** Save current arguments from gear_fcall_info *fci
 * params array will be set to NULL
 */
GEAR_API void gear_fcall_info_args_save(gear_fcall_info *fci, int *param_count, zval **params);

/** Free arguments connected with gear_fcall_info *fci andset back saved ones.
 */
GEAR_API void gear_fcall_info_args_restore(gear_fcall_info *fci, int param_count, zval *params);

/** Set or clear the arguments in the gear_call_info struct taking care of
 * refcount. If args is NULL and arguments are set then those are cleared.
 */
GEAR_API int gear_fcall_info_args(gear_fcall_info *fci, zval *args);
GEAR_API int gear_fcall_info_args_ex(gear_fcall_info *fci, gear_function *func, zval *args);

/** Set arguments in the gear_fcall_info struct taking care of refcount.
 * If argc is 0 the arguments which are set will be cleared, else pass
 * a variable amount of zval** arguments.
 */
GEAR_API int gear_fcall_info_argp(gear_fcall_info *fci, int argc, zval *argv);

/** Set arguments in the gear_fcall_info struct taking care of refcount.
 * If argc is 0 the arguments which are set will be cleared, else pass
 * a variable amount of zval** arguments.
 */
GEAR_API int gear_fcall_info_argv(gear_fcall_info *fci, int argc, va_list *argv);

/** Set arguments in the gear_fcall_info struct taking care of refcount.
 * If argc is 0 the arguments which are set will be cleared, else pass
 * a variable amount of zval** arguments.
 */
GEAR_API int gear_fcall_info_argn(gear_fcall_info *fci, int argc, ...);

/** Call a function using information created by gear_fcall_info_init()/args().
 * If args is given then those replace the argument info in fci is temporarily.
 */
GEAR_API int gear_fcall_info_call(gear_fcall_info *fci, gear_fcall_info_cache *fcc, zval *retval, zval *args);

GEAR_API int gear_call_function(gear_fcall_info *fci, gear_fcall_info_cache *fci_cache);

GEAR_API int gear_set_hash_symbol(zval *symbol, const char *name, int name_length, gear_bool is_ref, int num_symbol_tables, ...);

GEAR_API int gear_delete_global_variable(gear_string *name);

GEAR_API gear_array *gear_rebuild_symbol_table(void);
GEAR_API void gear_attach_symbol_table(gear_execute_data *execute_data);
GEAR_API void gear_detach_symbol_table(gear_execute_data *execute_data);
GEAR_API int gear_set_local_var(gear_string *name, zval *value, int force);
GEAR_API int gear_set_local_var_str(const char *name, size_t len, zval *value, int force);
GEAR_API int gear_forbid_dynamic_call(const char *func_name);

GEAR_API gear_string *gear_find_alias_name(gear_class_entry *ce, gear_string *name);
GEAR_API gear_string *gear_resolve_method_name(gear_class_entry *ce, gear_function *f);

GEAR_API const char *gear_get_object_type(const gear_class_entry *ce);

GEAR_API gear_bool gear_is_iterable(zval *iterable);

GEAR_API gear_bool gear_is_countable(zval *countable);

#define add_method(arg, key, method)	add_assoc_function((arg), (key), (method))

GEAR_API GEAR_FUNCTION(display_disabled_function);
GEAR_API GEAR_FUNCTION(display_disabled_class);
END_EXTERN_C()

#if GEAR_DEBUG
#define CHECK_ZVAL_STRING(str) \
	if (ZSTR_VAL(str)[ZSTR_LEN(str)] != '\0') { gear_error(E_WARNING, "String is not zero-terminated (%s)", ZSTR_VAL(str)); }
#define CHECK_ZVAL_STRING_REL(str) \
	if (ZSTR_VAL(str)[ZSTR_LEN(str)] != '\0') { gear_error(E_WARNING, "String is not zero-terminated (%s) (source: %s:%d)", ZSTR_VAL(str) GEAR_FILE_LINE_RELAY_CC); }
#else
#define CHECK_ZVAL_STRING(z)
#define CHECK_ZVAL_STRING_REL(z)
#endif

#define CHECK_ZVAL_NULL_PATH(p) (Z_STRLEN_P(p) != strlen(Z_STRVAL_P(p)))
#define CHECK_NULL_PATH(p, l) (strlen(p) != (size_t)(l))

#define ZVAL_STRINGL(z, s, l) do {				\
		ZVAL_NEW_STR(z, gear_string_init(s, l, 0));		\
	} while (0)

#define ZVAL_STRING(z, s) do {					\
		const char *_s = (s);					\
		ZVAL_STRINGL(z, _s, strlen(_s));		\
	} while (0)

#define ZVAL_EMPTY_STRING(z) do {				\
		ZVAL_INTERNED_STR(z, ZSTR_EMPTY_ALLOC());		\
	} while (0)

#define ZVAL_PSTRINGL(z, s, l) do {				\
		ZVAL_NEW_STR(z, gear_string_init(s, l, 1));		\
	} while (0)

#define ZVAL_PSTRING(z, s) do {					\
		const char *_s = (s);					\
		ZVAL_PSTRINGL(z, _s, strlen(_s));		\
	} while (0)

#define ZVAL_EMPTY_PSTRING(z) do {				\
		ZVAL_PSTRINGL(z, "", 0);				\
	} while (0)

#define ZVAL_ZVAL(z, zv, copy, dtor) do {		\
		zval *__z = (z);						\
		zval *__zv = (zv);						\
		if (EXPECTED(!Z_ISREF_P(__zv))) {		\
			if (copy && !dtor) {				\
				ZVAL_COPY(__z, __zv);			\
			} else {							\
				ZVAL_COPY_VALUE(__z, __zv);		\
			}									\
		} else {								\
			ZVAL_COPY(__z, Z_REFVAL_P(__zv));	\
			if (dtor || !copy) {				\
				zval_ptr_dtor(__zv);			\
			}									\
		}										\
	} while (0)

#define RETVAL_BOOL(b)					ZVAL_BOOL(return_value, b)
#define RETVAL_NULL() 					ZVAL_NULL(return_value)
#define RETVAL_LONG(l) 					ZVAL_LONG(return_value, l)
#define RETVAL_DOUBLE(d) 				ZVAL_DOUBLE(return_value, d)
#define RETVAL_STR(s)			 		ZVAL_STR(return_value, s)
#define RETVAL_INTERNED_STR(s)	 		ZVAL_INTERNED_STR(return_value, s)
#define RETVAL_NEW_STR(s)		 		ZVAL_NEW_STR(return_value, s)
#define RETVAL_STR_COPY(s)		 		ZVAL_STR_COPY(return_value, s)
#define RETVAL_STRING(s)		 		ZVAL_STRING(return_value, s)
#define RETVAL_STRINGL(s, l)		 	ZVAL_STRINGL(return_value, s, l)
#define RETVAL_EMPTY_STRING() 			ZVAL_EMPTY_STRING(return_value)
#define RETVAL_RES(r)			 		ZVAL_RES(return_value, r)
#define RETVAL_ARR(r)			 		ZVAL_ARR(return_value, r)
#define RETVAL_OBJ(r)			 		ZVAL_OBJ(return_value, r)
#define RETVAL_ZVAL(zv, copy, dtor)		ZVAL_ZVAL(return_value, zv, copy, dtor)
#define RETVAL_FALSE  					ZVAL_FALSE(return_value)
#define RETVAL_TRUE   					ZVAL_TRUE(return_value)

#define RETURN_BOOL(b) 					{ RETVAL_BOOL(b); return; }
#define RETURN_NULL() 					{ RETVAL_NULL(); return;}
#define RETURN_LONG(l) 					{ RETVAL_LONG(l); return; }
#define RETURN_DOUBLE(d) 				{ RETVAL_DOUBLE(d); return; }
#define RETURN_STR(s) 					{ RETVAL_STR(s); return; }
#define RETURN_INTERNED_STR(s)			{ RETVAL_INTERNED_STR(s); return; }
#define RETURN_NEW_STR(s)				{ RETVAL_NEW_STR(s); return; }
#define RETURN_STR_COPY(s)				{ RETVAL_STR_COPY(s); return; }
#define RETURN_STRING(s) 				{ RETVAL_STRING(s); return; }
#define RETURN_STRINGL(s, l) 			{ RETVAL_STRINGL(s, l); return; }
#define RETURN_EMPTY_STRING() 			{ RETVAL_EMPTY_STRING(); return; }
#define RETURN_RES(r) 					{ RETVAL_RES(r); return; }
#define RETURN_ARR(r) 					{ RETVAL_ARR(r); return; }
#define RETURN_OBJ(r) 					{ RETVAL_OBJ(r); return; }
#define RETURN_ZVAL(zv, copy, dtor)		{ RETVAL_ZVAL(zv, copy, dtor); return; }
#define RETURN_FALSE  					{ RETVAL_FALSE; return; }
#define RETURN_TRUE   					{ RETVAL_TRUE; return; }

#define HASH_OF(p) (Z_TYPE_P(p)==IS_ARRAY ? Z_ARRVAL_P(p) : ((Z_TYPE_P(p)==IS_OBJECT ? Z_OBJ_HT_P(p)->get_properties((p)) : NULL)))
#define ZVAL_IS_NULL(z) (Z_TYPE_P(z) == IS_NULL)

/* For compatibility */
#define GEAR_MINIT			GEAR_CAPI_STARTUP_N
#define GEAR_MSHUTDOWN		GEAR_CAPI_SHUTDOWN_N
#define GEAR_RINIT			GEAR_CAPI_ACTIVATE_N
#define GEAR_RSHUTDOWN		GEAR_CAPI_DEACTIVATE_N
#define GEAR_MINFO			GEAR_CAPI_INFO_N
#define GEAR_GINIT(cAPI)		((void (*)(void*))(GEAR_CAPI_GLOBALS_CTOR_N(cAPI)))
#define GEAR_GSHUTDOWN(cAPI)	((void (*)(void*))(GEAR_CAPI_GLOBALS_DTOR_N(cAPI)))

#define GEAR_MINIT_FUNCTION			GEAR_CAPI_STARTUP_D
#define GEAR_MSHUTDOWN_FUNCTION		GEAR_CAPI_SHUTDOWN_D
#define GEAR_RINIT_FUNCTION			GEAR_CAPI_ACTIVATE_D
#define GEAR_RSHUTDOWN_FUNCTION		GEAR_CAPI_DEACTIVATE_D
#define GEAR_MINFO_FUNCTION			GEAR_CAPI_INFO_D
#define GEAR_GINIT_FUNCTION			GEAR_CAPI_GLOBALS_CTOR_D
#define GEAR_GSHUTDOWN_FUNCTION		GEAR_CAPI_GLOBALS_DTOR_D

/* Fast parameter parsing API */

/* Fast ZPP is always enabled now; this define is left in for compatibility
 * with any existing conditional compilation blocks.
 */
#define FAST_ZPP 1

#define Z_EXPECTED_TYPES(_) \
	_(Z_EXPECTED_LONG,		"int") \
	_(Z_EXPECTED_BOOL,		"bool") \
	_(Z_EXPECTED_STRING,	"string") \
	_(Z_EXPECTED_ARRAY,		"array") \
	_(Z_EXPECTED_FUNC,		"valid callback") \
	_(Z_EXPECTED_RESOURCE,	"resource") \
	_(Z_EXPECTED_PATH,		"a valid path") \
	_(Z_EXPECTED_OBJECT,	"object") \
	_(Z_EXPECTED_DOUBLE,	"float")

#define Z_EXPECTED_TYPE_ENUM(id, str) id,
#define Z_EXPECTED_TYPE_STR(id, str)  str,

typedef enum _gear_expected_type {
	Z_EXPECTED_TYPES(Z_EXPECTED_TYPE_ENUM)
	Z_EXPECTED_LAST
} gear_expected_type;

GEAR_API GEAR_COLD int  GEAR_FASTCALL gear_wrong_parameters_none_error(void);
GEAR_API GEAR_COLD int  GEAR_FASTCALL gear_wrong_parameters_none_exception(void);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameters_count_error(int min_num_args, int max_num_args);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameters_count_exception(int min_num_args, int max_num_args);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameter_type_error(int num, gear_expected_type expected_type, zval *arg);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameter_type_exception(int num, gear_expected_type expected_type, zval *arg);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameter_class_error(int num, char *name, zval *arg);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameter_class_exception(int num, char *name, zval *arg);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_callback_error(int num, char *error);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_callback_deprecated(int num, char *error);
GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_callback_exception(int num, char *error);

#define ZPP_ERROR_OK             0
#define ZPP_ERROR_FAILURE        1
#define ZPP_ERROR_WRONG_CALLBACK 2
#define ZPP_ERROR_WRONG_CLASS    3
#define ZPP_ERROR_WRONG_ARG      4
#define ZPP_ERROR_WRONG_COUNT    5

#define GEAR_PARSE_PARAMETERS_START_EX(flags, min_num_args, max_num_args) do { \
		const int _flags = (flags); \
		int _min_num_args = (min_num_args); \
		int _max_num_args = (max_num_args); \
		int _num_args = EX_NUM_ARGS(); \
		int _i; \
		zval *_real_arg, *_arg = NULL; \
		gear_expected_type _expected_type = Z_EXPECTED_LONG; \
		char *_error = NULL; \
		gear_bool _dummy; \
		gear_bool _optional = 0; \
		int error_code = ZPP_ERROR_OK; \
		((void)_i); \
		((void)_real_arg); \
		((void)_arg); \
		((void)_expected_type); \
		((void)_error); \
		((void)_dummy); \
		((void)_optional); \
		\
		do { \
			if (UNEXPECTED(_num_args < _min_num_args) || \
			    (UNEXPECTED(_num_args > _max_num_args) && \
			     EXPECTED(_max_num_args >= 0))) { \
				if (!(_flags & GEAR_PARSE_PARAMS_QUIET)) { \
					if (_flags & GEAR_PARSE_PARAMS_THROW) { \
						gear_wrong_parameters_count_exception(_min_num_args, _max_num_args); \
					} else { \
						gear_wrong_parameters_count_error(_min_num_args, _max_num_args); \
					} \
				} \
				error_code = ZPP_ERROR_FAILURE; \
				break; \
			} \
			_i = 0; \
			_real_arg = GEAR_CALL_ARG(execute_data, 0);

#define GEAR_PARSE_PARAMETERS_START(min_num_args, max_num_args) \
	GEAR_PARSE_PARAMETERS_START_EX(0, min_num_args, max_num_args)

#define GEAR_PARSE_PARAMETERS_NONE() do { \
		if (UNEXPECTED(GEAR_NUM_ARGS() != 0)) { \
			gear_wrong_parameters_none_error(); \
			return; \
		} \
	} while (0)

#define GEAR_PARSE_PARAMETERS_END_EX(failure) \
		} while (0); \
		if (UNEXPECTED(error_code != ZPP_ERROR_OK)) { \
			if (!(_flags & GEAR_PARSE_PARAMS_QUIET)) { \
				if (error_code == ZPP_ERROR_WRONG_CALLBACK) { \
					if (_flags & GEAR_PARSE_PARAMS_THROW) { \
						gear_wrong_callback_exception(_i, _error); \
					} else { \
						gear_wrong_callback_error(_i, _error); \
					} \
				} else if (error_code == ZPP_ERROR_WRONG_CLASS) { \
					if (_flags & GEAR_PARSE_PARAMS_THROW) { \
						gear_wrong_parameter_class_exception(_i, _error, _arg); \
					} else { \
						gear_wrong_parameter_class_error(_i, _error, _arg); \
					} \
				} else if (error_code == ZPP_ERROR_WRONG_ARG) { \
					if (_flags & GEAR_PARSE_PARAMS_THROW) { \
						gear_wrong_parameter_type_exception(_i, _expected_type, _arg); \
					} else { \
						gear_wrong_parameter_type_error(_i, _expected_type, _arg); \
					} \
				} \
			} \
			failure; \
		} \
	} while (0)

#define GEAR_PARSE_PARAMETERS_END() \
	GEAR_PARSE_PARAMETERS_END_EX(return)

#define Z_PARAM_PROLOGUE(deref, separate) \
	++_i; \
	GEAR_ASSERT(_i <= _min_num_args || _optional==1); \
	GEAR_ASSERT(_i >  _min_num_args || _optional==0); \
	if (_optional) { \
		if (UNEXPECTED(_i >_num_args)) break; \
	} \
	_real_arg++; \
	_arg = _real_arg; \
	if (deref) { \
		ZVAL_DEREF(_arg); \
	} \
	if (separate) { \
		SEPARATE_ZVAL_NOREF(_arg); \
	}

/* old "|" */
#define Z_PARAM_OPTIONAL \
	_optional = 1;

/* old "a" */
#define Z_PARAM_ARRAY_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_array(_arg, &dest, check_null, 0))) { \
			_expected_type = Z_EXPECTED_ARRAY; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_ARRAY_EX(dest, check_null, separate) \
	Z_PARAM_ARRAY_EX2(dest, check_null, separate, separate)

#define Z_PARAM_ARRAY(dest) \
	Z_PARAM_ARRAY_EX(dest, 0, 0)

/* old "A" */
#define Z_PARAM_ARRAY_OR_OBJECT_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_array(_arg, &dest, check_null, 1))) { \
			_expected_type = Z_EXPECTED_ARRAY; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_ARRAY_OR_OBJECT_EX(dest, check_null, separate) \
	Z_PARAM_ARRAY_OR_OBJECT_EX2(dest, check_null, separate, separate)

#define Z_PARAM_ARRAY_OR_OBJECT(dest) \
	Z_PARAM_ARRAY_OR_OBJECT_EX(dest, 0, 0)

/* old "b" */
#define Z_PARAM_BOOL_EX2(dest, is_null, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_bool(_arg, &dest, &is_null, check_null))) { \
			_expected_type = Z_EXPECTED_BOOL; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_BOOL_EX(dest, is_null, check_null, separate) \
	Z_PARAM_BOOL_EX2(dest, is_null, check_null, separate, separate)

#define Z_PARAM_BOOL(dest) \
	Z_PARAM_BOOL_EX(dest, _dummy, 0, 0)

/* old "C" */
#define Z_PARAM_CLASS_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_class(_arg, &dest, _i, check_null))) { \
			error_code = ZPP_ERROR_FAILURE; \
			break; \
		}

#define Z_PARAM_CLASS_EX(dest, check_null, separate) \
	Z_PARAM_CLASS_EX2(dest, check_null, separate, separate)

#define Z_PARAM_CLASS(dest) \
	Z_PARAM_CLASS_EX(dest, 0, 0)

/* old "d" */
#define Z_PARAM_DOUBLE_EX2(dest, is_null, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_double(_arg, &dest, &is_null, check_null))) { \
			_expected_type = Z_EXPECTED_DOUBLE; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_DOUBLE_EX(dest, is_null, check_null, separate) \
	Z_PARAM_DOUBLE_EX2(dest, is_null, check_null, separate, separate)

#define Z_PARAM_DOUBLE(dest) \
	Z_PARAM_DOUBLE_EX(dest, _dummy, 0, 0)

/* old "f" */
#define Z_PARAM_FUNC_EX2(dest_fci, dest_fcc, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_func(_arg, &dest_fci, &dest_fcc, check_null, &_error))) { \
			if (!_error) { \
				_expected_type = Z_EXPECTED_FUNC; \
				error_code = ZPP_ERROR_WRONG_ARG; \
				break; \
			} else { \
				error_code = ZPP_ERROR_WRONG_CALLBACK; \
				break; \
			} \
		} else if (UNEXPECTED(_error != NULL)) { \
			gear_wrong_callback_deprecated(_i, _error); \
		}

#define Z_PARAM_FUNC_EX(dest_fci, dest_fcc, check_null, separate) \
	Z_PARAM_FUNC_EX2(dest_fci, dest_fcc, check_null, separate, separate)

#define Z_PARAM_FUNC(dest_fci, dest_fcc) \
	Z_PARAM_FUNC_EX(dest_fci, dest_fcc, 0, 0)

/* old "h" */
#define Z_PARAM_ARRAY_HT_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_array_ht(_arg, &dest, check_null, 0, separate))) { \
			_expected_type = Z_EXPECTED_ARRAY; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_ARRAY_HT_EX(dest, check_null, separate) \
	Z_PARAM_ARRAY_HT_EX2(dest, check_null, separate, separate)

#define Z_PARAM_ARRAY_HT(dest) \
	Z_PARAM_ARRAY_HT_EX(dest, 0, 0)

/* old "H" */
#define Z_PARAM_ARRAY_OR_OBJECT_HT_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_array_ht(_arg, &dest, check_null, 1, separate))) { \
			_expected_type = Z_EXPECTED_ARRAY; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_ARRAY_OR_OBJECT_HT_EX(dest, check_null, separate) \
	Z_PARAM_ARRAY_OR_OBJECT_HT_EX2(dest, check_null, separate, separate)

#define Z_PARAM_ARRAY_OR_OBJECT_HT(dest) \
	Z_PARAM_ARRAY_OR_OBJECT_HT_EX(dest, 0, 0)

/* old "l" */
#define Z_PARAM_LONG_EX2(dest, is_null, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_long(_arg, &dest, &is_null, check_null, 0))) { \
			_expected_type = Z_EXPECTED_LONG; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_LONG_EX(dest, is_null, check_null, separate) \
	Z_PARAM_LONG_EX2(dest, is_null, check_null, separate, separate)

#define Z_PARAM_LONG(dest) \
	Z_PARAM_LONG_EX(dest, _dummy, 0, 0)

/* old "L" */
#define Z_PARAM_STRICT_LONG_EX2(dest, is_null, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_long(_arg, &dest, &is_null, check_null, 1))) { \
			_expected_type = Z_EXPECTED_LONG; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_STRICT_LONG_EX(dest, is_null, check_null, separate) \
	Z_PARAM_STRICT_LONG_EX2(dest, is_null, check_null, separate, separate)

#define Z_PARAM_STRICT_LONG(dest) \
	Z_PARAM_STRICT_LONG_EX(dest, _dummy, 0, 0)

/* old "o" */
#define Z_PARAM_OBJECT_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_object(_arg, &dest, NULL, check_null))) { \
			_expected_type = Z_EXPECTED_OBJECT; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_OBJECT_EX(dest, check_null, separate) \
	Z_PARAM_OBJECT_EX2(dest, check_null, separate, separate)

#define Z_PARAM_OBJECT(dest) \
	Z_PARAM_OBJECT_EX(dest, 0, 0)

/* old "O" */
#define Z_PARAM_OBJECT_OF_CLASS_EX2(dest, _ce, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_object(_arg, &dest, _ce, check_null))) { \
			if (_ce) { \
				_error = ZSTR_VAL((_ce)->name); \
				error_code = ZPP_ERROR_WRONG_CLASS; \
				break; \
			} else { \
				_expected_type = Z_EXPECTED_OBJECT; \
				error_code = ZPP_ERROR_WRONG_ARG; \
				break; \
			} \
		}

#define Z_PARAM_OBJECT_OF_CLASS_EX(dest, _ce, check_null, separate) \
	Z_PARAM_OBJECT_OF_CLASS_EX2(dest, _ce, check_null, separate, separate)

#define Z_PARAM_OBJECT_OF_CLASS(dest, _ce) \
	Z_PARAM_OBJECT_OF_CLASS_EX(dest, _ce, 0, 0)

/* old "p" */
#define Z_PARAM_PATH_EX2(dest, dest_len, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_path(_arg, &dest, &dest_len, check_null))) { \
			_expected_type = Z_EXPECTED_PATH; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_PATH_EX(dest, dest_len, check_null, separate) \
	Z_PARAM_PATH_EX2(dest, dest_len, check_null, separate, separate)

#define Z_PARAM_PATH(dest, dest_len) \
	Z_PARAM_PATH_EX(dest, dest_len, 0, 0)

/* old "P" */
#define Z_PARAM_PATH_STR_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_path_str(_arg, &dest, check_null))) { \
			_expected_type = Z_EXPECTED_PATH; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_PATH_STR_EX(dest, check_null, separate) \
	Z_PARAM_PATH_STR_EX2(dest, check_null, separate, separate)

#define Z_PARAM_PATH_STR(dest) \
	Z_PARAM_PATH_STR_EX(dest, 0, 0)

/* old "r" */
#define Z_PARAM_RESOURCE_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_resource(_arg, &dest, check_null))) { \
			_expected_type = Z_EXPECTED_RESOURCE; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_RESOURCE_EX(dest, check_null, separate) \
	Z_PARAM_RESOURCE_EX2(dest, check_null, separate, separate)

#define Z_PARAM_RESOURCE(dest) \
	Z_PARAM_RESOURCE_EX(dest, 0, 0)

/* old "s" */
#define Z_PARAM_STRING_EX2(dest, dest_len, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_string(_arg, &dest, &dest_len, check_null))) { \
			_expected_type = Z_EXPECTED_STRING; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_STRING_EX(dest, dest_len, check_null, separate) \
	Z_PARAM_STRING_EX2(dest, dest_len, check_null, separate, separate)

#define Z_PARAM_STRING(dest, dest_len) \
	Z_PARAM_STRING_EX(dest, dest_len, 0, 0)

/* old "S" */
#define Z_PARAM_STR_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		if (UNEXPECTED(!gear_parse_arg_str(_arg, &dest, check_null))) { \
			_expected_type = Z_EXPECTED_STRING; \
			error_code = ZPP_ERROR_WRONG_ARG; \
			break; \
		}

#define Z_PARAM_STR_EX(dest, check_null, separate) \
	Z_PARAM_STR_EX2(dest, check_null, separate, separate)

#define Z_PARAM_STR(dest) \
	Z_PARAM_STR_EX(dest, 0, 0)

/* old "z" */
#define Z_PARAM_ZVAL_EX2(dest, check_null, deref, separate) \
		Z_PARAM_PROLOGUE(deref, separate); \
		gear_parse_arg_zval_deref(_arg, &dest, check_null);

#define Z_PARAM_ZVAL_EX(dest, check_null, separate) \
	Z_PARAM_ZVAL_EX2(dest, check_null, separate, separate)

#define Z_PARAM_ZVAL(dest) \
	Z_PARAM_ZVAL_EX(dest, 0, 0)

/* old "z" (with dereference) */
#define Z_PARAM_ZVAL_DEREF_EX(dest, check_null, separate) \
		Z_PARAM_PROLOGUE(1, separate); \
		gear_parse_arg_zval_deref(_arg, &dest, check_null);

#define Z_PARAM_ZVAL_DEREF(dest) \
	Z_PARAM_ZVAL_DEREF_EX(dest, 0, 0)

/* old "+" and "*" */
#define Z_PARAM_VARIADIC_EX(spec, dest, dest_num, post_varargs) do { \
		int _num_varargs = _num_args - _i - (post_varargs); \
		if (EXPECTED(_num_varargs > 0)) { \
			dest = _real_arg + 1; \
			dest_num = _num_varargs; \
			_i += _num_varargs; \
			_real_arg += _num_varargs; \
		} else { \
			dest = NULL; \
			dest_num = 0; \
		} \
	} while (0);

#define Z_PARAM_VARIADIC(spec, dest, dest_num) \
	Z_PARAM_VARIADIC_EX(spec, dest, dest_num, 0)

/* End of new parameter parsing API */

/* Inlined implementations shared by new and old parameter parsing APIs */

GEAR_API int GEAR_FASTCALL gear_parse_arg_class(zval *arg, gear_class_entry **pce, int num, int check_null);
GEAR_API int GEAR_FASTCALL gear_parse_arg_bool_slow(zval *arg, gear_bool *dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_bool_weak(zval *arg, gear_bool *dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_long_slow(zval *arg, gear_long *dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_long_weak(zval *arg, gear_long *dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_long_cap_slow(zval *arg, gear_long *dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_long_cap_weak(zval *arg, gear_long *dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_double_slow(zval *arg, double *dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_double_weak(zval *arg, double *dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_str_slow(zval *arg, gear_string **dest);
GEAR_API int GEAR_FASTCALL gear_parse_arg_str_weak(zval *arg, gear_string **dest);

static gear_always_inline int gear_parse_arg_bool(zval *arg, gear_bool *dest, gear_bool *is_null, int check_null)
{
	if (check_null) {
		*is_null = 0;
	}
	if (EXPECTED(Z_TYPE_P(arg) == IS_TRUE)) {
		*dest = 1;
	} else if (EXPECTED(Z_TYPE_P(arg) == IS_FALSE)) {
		*dest = 0;
	} else if (check_null && Z_TYPE_P(arg) == IS_NULL) {
		*is_null = 1;
		*dest = 0;
	} else {
		return gear_parse_arg_bool_slow(arg, dest);
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_long(zval *arg, gear_long *dest, gear_bool *is_null, int check_null, int cap)
{
	if (check_null) {
		*is_null = 0;
	}
	if (EXPECTED(Z_TYPE_P(arg) == IS_LONG)) {
		*dest = Z_LVAL_P(arg);
	} else if (check_null && Z_TYPE_P(arg) == IS_NULL) {
		*is_null = 1;
		*dest = 0;
	} else if (cap) {
		return gear_parse_arg_long_cap_slow(arg, dest);
	} else {
		return gear_parse_arg_long_slow(arg, dest);
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_double(zval *arg, double *dest, gear_bool *is_null, int check_null)
{
	if (check_null) {
		*is_null = 0;
	}
	if (EXPECTED(Z_TYPE_P(arg) == IS_DOUBLE)) {
		*dest = Z_DVAL_P(arg);
	} else if (check_null && Z_TYPE_P(arg) == IS_NULL) {
		*is_null = 1;
		*dest = 0.0;
	} else {
		return gear_parse_arg_double_slow(arg, dest);
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_str(zval *arg, gear_string **dest, int check_null)
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_STRING)) {
		*dest = Z_STR_P(arg);
	} else if (check_null && Z_TYPE_P(arg) == IS_NULL) {
		*dest = NULL;
	} else {
		return gear_parse_arg_str_slow(arg, dest);
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_string(zval *arg, char **dest, size_t *dest_len, int check_null)
{
	gear_string *str;

	if (!gear_parse_arg_str(arg, &str, check_null)) {
		return 0;
	}
	if (check_null && UNEXPECTED(!str)) {
		*dest = NULL;
		*dest_len = 0;
	} else {
		*dest = ZSTR_VAL(str);
		*dest_len = ZSTR_LEN(str);
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_path_str(zval *arg, gear_string **dest, int check_null)
{
	if (!gear_parse_arg_str(arg, dest, check_null) ||
	    (*dest && UNEXPECTED(CHECK_NULL_PATH(ZSTR_VAL(*dest), ZSTR_LEN(*dest))))) {
		return 0;
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_path(zval *arg, char **dest, size_t *dest_len, int check_null)
{
	gear_string *str;

	if (!gear_parse_arg_path_str(arg, &str, check_null)) {
		return 0;
	}
	if (check_null && UNEXPECTED(!str)) {
		*dest = NULL;
		*dest_len = 0;
	} else {
		*dest = ZSTR_VAL(str);
		*dest_len = ZSTR_LEN(str);
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_array(zval *arg, zval **dest, int check_null, int or_object)
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_ARRAY) ||
		(or_object && EXPECTED(Z_TYPE_P(arg) == IS_OBJECT))) {
		*dest = arg;
	} else if (check_null && EXPECTED(Z_TYPE_P(arg) == IS_NULL)) {
		*dest = NULL;
	} else {
		return 0;
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_array_ht(zval *arg, HashTable **dest, int check_null, int or_object, int separate)
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_ARRAY)) {
		*dest = Z_ARRVAL_P(arg);
	} else if (or_object && EXPECTED(Z_TYPE_P(arg) == IS_OBJECT)) {
		if (separate
		 && Z_OBJ_P(arg)->properties
		 && UNEXPECTED(GC_REFCOUNT(Z_OBJ_P(arg)->properties) > 1)) {
			if (EXPECTED(!(GC_FLAGS(Z_OBJ_P(arg)->properties) & IS_ARRAY_IMMUTABLE))) {
				GC_DELREF(Z_OBJ_P(arg)->properties);
			}
			Z_OBJ_P(arg)->properties = gear_array_dup(Z_OBJ_P(arg)->properties);
		}
		*dest = Z_OBJ_HT_P(arg)->get_properties(arg);
	} else if (check_null && EXPECTED(Z_TYPE_P(arg) == IS_NULL)) {
		*dest = NULL;
	} else {
		return 0;
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_object(zval *arg, zval **dest, gear_class_entry *ce, int check_null)
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_OBJECT) &&
	    (!ce || EXPECTED(instanceof_function(Z_OBJCE_P(arg), ce) != 0))) {
		*dest = arg;
	} else if (check_null && EXPECTED(Z_TYPE_P(arg) == IS_NULL)) {
		*dest = NULL;
	} else {
		return 0;
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_resource(zval *arg, zval **dest, int check_null)
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_RESOURCE)) {
		*dest = arg;
	} else if (check_null && EXPECTED(Z_TYPE_P(arg) == IS_NULL)) {
		*dest = NULL;
	} else {
		return 0;
	}
	return 1;
}

static gear_always_inline int gear_parse_arg_func(zval *arg, gear_fcall_info *dest_fci, gear_fcall_info_cache *dest_fcc, int check_null, char **error)
{
	if (check_null && UNEXPECTED(Z_TYPE_P(arg) == IS_NULL)) {
		dest_fci->size = 0;
		dest_fcc->function_handler = NULL;
		*error = NULL;
	} else if (UNEXPECTED(gear_fcall_info_init(arg, 0, dest_fci, dest_fcc, NULL, error) != SUCCESS)) {
		return 0;
	}
	return 1;
}

static gear_always_inline void gear_parse_arg_zval(zval *arg, zval **dest, int check_null)
{
	*dest = (check_null &&
	    (UNEXPECTED(Z_TYPE_P(arg) == IS_NULL) ||
	     (UNEXPECTED(Z_ISREF_P(arg)) &&
	      UNEXPECTED(Z_TYPE_P(Z_REFVAL_P(arg)) == IS_NULL)))) ? NULL : arg;
}

static gear_always_inline void gear_parse_arg_zval_deref(zval *arg, zval **dest, int check_null)
{
	*dest = (check_null && UNEXPECTED(Z_TYPE_P(arg) == IS_NULL)) ? NULL : arg;
}

END_EXTERN_C()

#endif /* GEAR_API_H */

