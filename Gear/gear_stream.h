/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef GEAR_STREAM_H
#define GEAR_STREAM_H

#include <sys/types.h>
#include <sys/stat.h>

/* Lightweight stream implementation for the ZE scanners.
 * These functions are private to the engine.
 * */
typedef size_t (*gear_stream_fsizer_t)(void* handle);
typedef size_t (*gear_stream_reader_t)(void* handle, char *buf, size_t len);
typedef void   (*gear_stream_closer_t)(void* handle);

#define GEAR_MMAP_AHEAD 32

typedef enum {
	GEAR_HANDLE_FILENAME,
	GEAR_HANDLE_FD,
	GEAR_HANDLE_FP,
	GEAR_HANDLE_STREAM,
	GEAR_HANDLE_MAPPED
} gear_stream_type;

typedef struct _gear_mmap {
	size_t      len;
	size_t      pos;
	void        *map;
	char        *buf;
	void                  *old_handle;
	gear_stream_closer_t   old_closer;
} gear_mmap;

typedef struct _gear_stream {
	void        *handle;
	int         isatty;
	gear_mmap   mmap;
	gear_stream_reader_t   reader;
	gear_stream_fsizer_t   fsizer;
	gear_stream_closer_t   closer;
} gear_stream;

typedef struct _gear_file_handle {
	union {
		int           fd;
		FILE          *fp;
		gear_stream   stream;
	} handle;
	const char        *filename;
	gear_string       *opened_path;
	gear_stream_type  type;
	gear_bool free_filename;
} gear_file_handle;

BEGIN_EXTERN_C()
GEAR_API int gear_stream_open(const char *filename, gear_file_handle *handle);
GEAR_API int gear_stream_fixup(gear_file_handle *file_handle, char **buf, size_t *len);
GEAR_API void gear_file_handle_dtor(gear_file_handle *fh);
GEAR_API int gear_compare_file_handles(gear_file_handle *fh1, gear_file_handle *fh2);
END_EXTERN_C()

#ifdef _WIN64
# define gear_fseek _fseeki64
# define gear_ftell _ftelli64
# define gear_lseek _lseeki64
# define gear_fstat _fstat64
# define gear_stat  _stat64
typedef struct __stat64 gear_stat_t;
#else
# define gear_fseek fseek
# define gear_ftell ftell
# define gear_lseek lseek
# define gear_fstat fstat
# define gear_stat stat
typedef struct stat gear_stat_t;
#endif

#endif

