/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_SIGNAL_H
#define GEAR_SIGNAL_H

#ifdef GEAR_SIGNALS

# ifdef HAVE_SIGNAL_H
#  include <signal.h>
# endif

#ifndef NSIG
#define NSIG 65
#endif

#ifndef GEAR_SIGNAL_QUEUE_SIZE
#define GEAR_SIGNAL_QUEUE_SIZE 64
#endif

/* Signal structs */
typedef struct _gear_signal_entry_t {
	int   flags;          /* sigaction style flags */
	void* handler;      /* signal handler or context */
} gear_signal_entry_t;

typedef struct _gear_signal_t {
	int signo;
	siginfo_t *siginfo;
	void* context;
} gear_signal_t;

typedef struct _gear_signal_queue_t {
	gear_signal_t gear_signal;
	struct _gear_signal_queue_t *next;
} gear_signal_queue_t;

/* Signal Globals */
typedef struct _gear_signal_globals_t {
	int depth;
	int blocked;            /* 1==TRUE, 0==FALSE */
	int running;            /* in signal handler execution */
	int active;             /* internal signal handling is enabled */
	gear_bool check;        /* check for replaced handlers on shutdown */
	gear_signal_entry_t handlers[NSIG];
	gear_signal_queue_t pstorage[GEAR_SIGNAL_QUEUE_SIZE], *phead, *ptail, *pavail; /* pending queue */
} gear_signal_globals_t;

# ifdef ZTS
#  define SIGG(v) GEAR_PBCG(gear_signal_globals_id, gear_signal_globals_t *, v)
BEGIN_EXTERN_C()
GEAR_API extern int gear_signal_globals_id;
END_EXTERN_C()
# else
#  define SIGG(v) (gear_signal_globals.v)
BEGIN_EXTERN_C()
GEAR_API extern gear_signal_globals_t gear_signal_globals;
END_EXTERN_C()
# endif /* not ZTS */

# ifdef ZTS
#  define GEAR_SIGNAL_BLOCK_INTERRUPTIONS() if (EXPECTED(gear_signal_globals_id)) { SIGG(depth)++; }
#  define GEAR_SIGNAL_UNBLOCK_INTERRUPTIONS() if (EXPECTED(gear_signal_globals_id) && UNEXPECTED(((SIGG(depth)--) == SIGG(blocked)))) { gear_signal_handler_unblock(); }
# else /* ZTS */
#  define GEAR_SIGNAL_BLOCK_INTERRUPTIONS()  SIGG(depth)++;
#  define GEAR_SIGNAL_UNBLOCK_INTERRUPTIONS() if (((SIGG(depth)--) == SIGG(blocked))) { gear_signal_handler_unblock(); }
# endif /* not ZTS */

GEAR_API void gear_signal_handler_unblock(void);
void gear_signal_activate(void);
void gear_signal_deactivate(void);
BEGIN_EXTERN_C()
GEAR_API void gear_signal_startup(void);
END_EXTERN_C()
void gear_signal_init(void);

GEAR_API int gear_signal(int signo, void (*handler)(int));
GEAR_API int gear_sigaction(int signo, const struct sigaction *act, struct sigaction *oldact);

#else /* GEAR_SIGNALS */

# define GEAR_SIGNAL_BLOCK_INTERRUPTIONS()
# define GEAR_SIGNAL_UNBLOCK_INTERRUPTIONS()

# define gear_signal_activate()
# define gear_signal_deactivate()
# define gear_signal_startup()
# define gear_signal_init()

# define gear_signal(signo, handler)           signal(signo, handler)
# define gear_sigaction(signo, act, oldact)    sigaction(signo, act, oldact)

#endif /* GEAR_SIGNALS */

#endif /* GEAR_SIGNAL_H */

