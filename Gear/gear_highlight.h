/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_HIGHLIGHT_H
#define GEAR_HIGHLIGHT_H

#define HL_COMMENT_COLOR     "#FF8000"    /* orange */
#define HL_DEFAULT_COLOR     "#0000BB"    /* blue */
#define HL_HTML_COLOR        "#000000"    /* black */
#define HL_STRING_COLOR      "#DD0000"    /* red */
#define HL_KEYWORD_COLOR     "#007700"    /* green */


typedef struct _gear_syntax_highlighter_ics {
	char *highlight_html;
	char *highlight_comment;
	char *highlight_default;
	char *highlight_string;
	char *highlight_keyword;
} gear_syntax_highlighter_ics;


BEGIN_EXTERN_C()
GEAR_API void gear_highlight(gear_syntax_highlighter_ics *syntax_highlighter_ics);
GEAR_API void gear_strip(void);
GEAR_API int highlight_file(char *filename, gear_syntax_highlighter_ics *syntax_highlighter_ics);
GEAR_API int highlight_string(zval *str, gear_syntax_highlighter_ics *syntax_highlighter_ics, char *str_name);
GEAR_API void gear_html_putc(char c);
GEAR_API void gear_html_puts(const char *s, size_t len);
END_EXTERN_C()

extern gear_syntax_highlighter_ics syntax_highlighter_ics;

#endif

