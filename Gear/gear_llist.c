/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_llist.h"
#include "gear_sort.h"

GEAR_API void gear_llist_init(gear_llist *l, size_t size, llist_dtor_func_t dtor, unsigned char persistent)
{
	l->head  = NULL;
	l->tail  = NULL;
	l->count = 0;
	l->size  = size;
	l->dtor  = dtor;
	l->persistent = persistent;
}

GEAR_API void gear_llist_add_element(gear_llist *l, void *element)
{
	gear_llist_element *tmp = pemalloc(sizeof(gear_llist_element)+l->size-1, l->persistent);

	tmp->prev = l->tail;
	tmp->next = NULL;
	if (l->tail) {
		l->tail->next = tmp;
	} else {
		l->head = tmp;
	}
	l->tail = tmp;
	memcpy(tmp->data, element, l->size);

	++l->count;
}


GEAR_API void gear_llist_prepend_element(gear_llist *l, void *element)
{
	gear_llist_element *tmp = pemalloc(sizeof(gear_llist_element)+l->size-1, l->persistent);

	tmp->next = l->head;
	tmp->prev = NULL;
	if (l->head) {
		l->head->prev = tmp;
	} else {
		l->tail = tmp;
	}
	l->head = tmp;
	memcpy(tmp->data, element, l->size);

	++l->count;
}


#define DEL_LLIST_ELEMENT(current, l) \
			if ((current)->prev) {\
				(current)->prev->next = (current)->next;\
			} else {\
				(l)->head = (current)->next;\
			}\
			if ((current)->next) {\
				(current)->next->prev = (current)->prev;\
			} else {\
				(l)->tail = (current)->prev;\
			}\
			if ((l)->dtor) {\
				(l)->dtor((current)->data);\
			}\
			pefree((current), (l)->persistent);\
			--l->count;


GEAR_API void gear_llist_del_element(gear_llist *l, void *element, int (*compare)(void *element1, void *element2))
{
	gear_llist_element *current=l->head;

	while (current) {
		if (compare(current->data, element)) {
			DEL_LLIST_ELEMENT(current, l);
			break;
		}
		current = current->next;
	}
}


GEAR_API void gear_llist_destroy(gear_llist *l)
{
	gear_llist_element *current=l->head, *next;

	while (current) {
		next = current->next;
		if (l->dtor) {
			l->dtor(current->data);
		}
		pefree(current, l->persistent);
		current = next;
	}

	l->count = 0;
}


GEAR_API void gear_llist_clean(gear_llist *l)
{
	gear_llist_destroy(l);
	l->head = l->tail = NULL;
}


GEAR_API void gear_llist_remove_tail(gear_llist *l)
{
	gear_llist_element *old_tail = l->tail;
	if (!old_tail) {
		return;
	}

	if (old_tail->prev) {
		old_tail->prev->next = NULL;
	} else {
		l->head = NULL;
	}

	l->tail = old_tail->prev;
	--l->count;

	if (l->dtor) {
		l->dtor(old_tail->data);
	}
	pefree(old_tail, l->persistent);
}


GEAR_API void gear_llist_copy(gear_llist *dst, gear_llist *src)
{
	gear_llist_element *ptr;

	gear_llist_init(dst, src->size, src->dtor, src->persistent);
	ptr = src->head;
	while (ptr) {
		gear_llist_add_element(dst, ptr->data);
		ptr = ptr->next;
	}
}


GEAR_API void gear_llist_apply_with_del(gear_llist *l, int (*func)(void *data))
{
	gear_llist_element *element, *next;

	element=l->head;
	while (element) {
		next = element->next;
		if (func(element->data)) {
			DEL_LLIST_ELEMENT(element, l);
		}
		element = next;
	}
}


GEAR_API void gear_llist_apply(gear_llist *l, llist_apply_func_t func)
{
	gear_llist_element *element;

	for (element=l->head; element; element=element->next) {
		func(element->data);
	}
}

static void gear_llist_swap(gear_llist_element **p, gear_llist_element **q)
{
	gear_llist_element *t;
	t = *p;
	*p = *q;
	*q = t;
}

GEAR_API void gear_llist_sort(gear_llist *l, llist_compare_func_t comp_func)
{
	size_t i;

	gear_llist_element **elements;
	gear_llist_element *element, **ptr;

	if (l->count == 0) {
		return;
	}

	elements = (gear_llist_element **) emalloc(l->count * sizeof(gear_llist_element *));

	ptr = &elements[0];

	for (element=l->head; element; element=element->next) {
		*ptr++ = element;
	}

	gear_sort(elements, l->count, sizeof(gear_llist_element *),
			(compare_func_t) comp_func, (swap_func_t) gear_llist_swap);

	l->head = elements[0];
	elements[0]->prev = NULL;

	for (i = 1; i < l->count; i++) {
		elements[i]->prev = elements[i-1];
		elements[i-1]->next = elements[i];
	}
	elements[i-1]->next = NULL;
	l->tail = elements[i-1];
	efree(elements);
}


GEAR_API void gear_llist_apply_with_argument(gear_llist *l, llist_apply_with_arg_func_t func, void *arg)
{
	gear_llist_element *element;

	for (element=l->head; element; element=element->next) {
		func(element->data, arg);
	}
}


GEAR_API void gear_llist_apply_with_arguments(gear_llist *l, llist_apply_with_args_func_t func, int num_args, ...)
{
	gear_llist_element *element;
	va_list args;

	va_start(args, num_args);
	for (element=l->head; element; element=element->next) {
		func(element->data, num_args, args);
	}
	va_end(args);
}


GEAR_API size_t gear_llist_count(gear_llist *l)
{
	return l->count;
}


GEAR_API void *gear_llist_get_first_ex(gear_llist *l, gear_llist_position *pos)
{
	gear_llist_position *current = pos ? pos : &l->traverse_ptr;

	*current = l->head;
	if (*current) {
		return (*current)->data;
	} else {
		return NULL;
	}
}


GEAR_API void *gear_llist_get_last_ex(gear_llist *l, gear_llist_position *pos)
{
	gear_llist_position *current = pos ? pos : &l->traverse_ptr;

	*current = l->tail;
	if (*current) {
		return (*current)->data;
	} else {
		return NULL;
	}
}


GEAR_API void *gear_llist_get_next_ex(gear_llist *l, gear_llist_position *pos)
{
	gear_llist_position *current = pos ? pos : &l->traverse_ptr;

	if (*current) {
		*current = (*current)->next;
		if (*current) {
			return (*current)->data;
		}
	}
	return NULL;
}


GEAR_API void *gear_llist_get_prev_ex(gear_llist *l, gear_llist_position *pos)
{
	gear_llist_position *current = pos ? pos : &l->traverse_ptr;

	if (*current) {
		*current = (*current)->prev;
		if (*current) {
			return (*current)->data;
		}
	}
	return NULL;
}

