/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear_sort.h"

#define VM_TRACE(op)     gear_vm_trace(#op, sizeof(#op)-1);
#define VM_TRACE_START() gear_vm_trace_init();
#define VM_TRACE_END()   gear_vm_trace_finish();

static HashTable vm_trace_ht;

static void gear_vm_trace(const char *op, size_t op_len)
{
	static const char *last = NULL;
	static size_t last_len = 0;
	char buf[256];
	size_t len;
	zval tmp, *zv;

	if (EXPECTED(last)) {
		len = last_len + 1 + op_len;
		memcpy(buf, last, last_len);
		buf[last_len] = ' ';
		memcpy(buf + last_len + 1, op, op_len + 1);
		zv = gear_hash_str_find(&vm_trace_ht, buf, len);
		if (EXPECTED(zv)) {
			if (EXPECTED(Z_LVAL_P(zv) < GEAR_LONG_MAX)) {
				Z_LVAL_P(zv)++;
			}
		} else {
			ZVAL_LONG(&tmp, 1);
			gear_hash_str_add_new(&vm_trace_ht, buf, len, &tmp);
		}
	}
	last = op;
	last_len = op_len;
}

static int gear_vm_trace_compare(const Bucket *p1, const Bucket *p2)
{
	if (Z_LVAL(p1->val) < Z_LVAL(p2->val)) {
		return 1;
	} else if (Z_LVAL(p1->val) > Z_LVAL(p2->val)) {
		return -1;
	} else {
		return 0;
	}
}

static void gear_vm_trace_finish(void)
{
	gear_string *key;
	zval *val;
	FILE *f;

	f = fopen("gear_vm_trace.log", "w+");
	if (f) {
		gear_hash_sort(&vm_trace_ht, (compare_func_t)gear_vm_trace_compare, 0);
		GEAR_HASH_FOREACH_STR_KEY_VAL(&vm_trace_ht, key, val) {
			fprintf(f, "%s "GEAR_LONG_FMT"\n", ZSTR_VAL(key), Z_LVAL_P(val));
		} GEAR_HASH_FOREACH_END();
		fclose(f);
	}
	gear_hash_destroy(&vm_trace_ht);
}

static void gear_vm_trace_init(void)
{
	FILE *f;

	gear_hash_init(&vm_trace_ht, 0, NULL, NULL, 1);
	f = fopen("gear_vm_trace.log", "r");
	if (f) {
		char buf[256];
		size_t len;
		zval tmp;

		while (!feof(f)) {
			if (fgets(buf, sizeof(buf)-1, f)) {
				len = strlen(buf);
				while (len > 0 && buf[len-1] <= ' ') {
					len--;
					buf[len] = 0;
				}
				while (len > 0 && buf[len-1] >= '0' && buf[len-1] <= '9') {
					len--;
				}
				if (len > 1) {
					buf[len-1] = 0;
					ZVAL_LONG(&tmp, GEAR_STRTOL(buf + len, NULL, 10));
					gear_hash_str_add(&vm_trace_ht, buf, len - 1, &tmp);
				}
			}
		}
		fclose(f);
	}
}
