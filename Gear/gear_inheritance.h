/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_INHERITANCE_H
#define GEAR_INHERITANCE_H

#include "gear.h"

BEGIN_EXTERN_C()

GEAR_API void gear_do_inherit_interfaces(gear_class_entry *ce, const gear_class_entry *iface);
GEAR_API void gear_do_implement_interface(gear_class_entry *ce, gear_class_entry *iface);

GEAR_API void gear_do_implement_trait(gear_class_entry *ce, gear_class_entry *trait);
GEAR_API void gear_do_bind_traits(gear_class_entry *ce);

GEAR_API void gear_do_inheritance(gear_class_entry *ce, gear_class_entry *parent_ce);
void gear_do_early_binding(void);

void gear_check_deprecated_constructor(const gear_class_entry *ce);

END_EXTERN_C()

#endif

