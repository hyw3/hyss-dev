/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_LONG_H
#define GEAR_LONG_H

#include "main/hyss_stdint.h"

/* This is the heart of the whole int64 enablement in zval. */
#if defined(__x86_64__) || defined(__LP64__) || defined(_LP64) || defined(_WIN64)
# define GEAR_ENABLE_ZVAL_LONG64 1
#endif

/* Integer types. */
#ifdef GEAR_ENABLE_ZVAL_LONG64
typedef int64_t gear_long;
typedef uint64_t gear_ulong;
typedef int64_t gear_off_t;
# define GEAR_LONG_MAX INT64_MAX
# define GEAR_LONG_MIN INT64_MIN
# define GEAR_ULONG_MAX UINT64_MAX
# define Z_L(i) INT64_C(i)
# define Z_UL(i) UINT64_C(i)
# define SIZEOF_GEAR_LONG 8
#else
typedef int32_t gear_long;
typedef uint32_t gear_ulong;
typedef int32_t gear_off_t;
# define GEAR_LONG_MAX INT32_MAX
# define GEAR_LONG_MIN INT32_MIN
# define GEAR_ULONG_MAX UINT32_MAX
# define Z_L(i) INT32_C(i)
# define Z_UL(i) UINT32_C(i)
# define SIZEOF_GEAR_LONG 4
#endif


/* Conversion macros. */
#define GEAR_LTOA_BUF_LEN 65

#ifdef GEAR_ENABLE_ZVAL_LONG64
# define GEAR_LONG_FMT "%" PRId64
# define GEAR_ULONG_FMT "%" PRIu64
# define GEAR_XLONG_FMT "%" PRIx64
# define GEAR_LONG_FMT_SPEC PRId64
# define GEAR_ULONG_FMT_SPEC PRIu64
# ifdef GEAR_WIN32
#  define GEAR_LTOA(i, s, len) _i64toa_s((i), (s), (len), 10)
#  define GEAR_ATOL(i, s) i = _atoi64((s))
#  define GEAR_STRTOL(s0, s1, base) _strtoi64((s0), (s1), (base))
#  define GEAR_STRTOUL(s0, s1, base) _strtoui64((s0), (s1), (base))
#  define GEAR_STRTOL_PTR _strtoi64
#  define GEAR_STRTOUL_PTR _strtoui64
#  define GEAR_ABS _abs64
# else
#  define GEAR_LTOA(i, s, len) \
	do { \
		int st = snprintf((s), (len), GEAR_LONG_FMT, (i)); \
		(s)[st] = '\0'; \
 	} while (0)
#  define GEAR_ATOL(i, s) (i) = atoll((s))
#  define GEAR_STRTOL(s0, s1, base) strtoll((s0), (s1), (base))
#  define GEAR_STRTOUL(s0, s1, base) strtoull((s0), (s1), (base))
#  define GEAR_STRTOL_PTR strtoll
#  define GEAR_STRTOUL_PTR strtoull
#  define GEAR_ABS imaxabs
# endif
#else
# define GEAR_STRTOL(s0, s1, base) strtol((s0), (s1), (base))
# define GEAR_STRTOUL(s0, s1, base) strtoul((s0), (s1), (base))
# define GEAR_LONG_FMT "%" PRId32
# define GEAR_ULONG_FMT "%" PRIu32
# define GEAR_XLONG_FMT "%" PRIx32
# define GEAR_LONG_FMT_SPEC PRId32
# define GEAR_ULONG_FMT_SPEC PRIu32
# ifdef GEAR_WIN32
#  define GEAR_LTOA(i, s, len) _ltoa_s((i), (s), (len), 10)
#  define GEAR_ATOL(i, s) i = atol((s))
# else
#  define GEAR_LTOA(i, s, len) \
	do { \
		int st = snprintf((s), (len), GEAR_LONG_FMT, (i)); \
		(s)[st] = '\0'; \
 	} while (0)
#  define GEAR_ATOL(i, s) (i) = atol((s))
# endif
# define GEAR_STRTOL_PTR strtol
# define GEAR_STRTOUL_PTR strtoul
# define GEAR_ABS abs
#endif

#if SIZEOF_GEAR_LONG == 4
# define MAX_LENGTH_OF_LONG 11
# define LONG_MIN_DIGITS "2147483648"
#elif SIZEOF_GEAR_LONG == 8
# define MAX_LENGTH_OF_LONG 20
# define LONG_MIN_DIGITS "9223372036854775808"
#else
# error "Unknown SIZEOF_GEAR_LONG"
#endif

static const char long_min_digits[] = LONG_MIN_DIGITS;

#ifdef _WIN64
# define GEAR_ADDR_FMT "0x%016I64x"
#elif SIZEOF_SIZE_T == 4
# define GEAR_ADDR_FMT "0x%08zx"
#elif SIZEOF_SIZE_T == 8
# define GEAR_ADDR_FMT "0x%016zx"
#else
# error "Unknown SIZEOF_SIZE_T"
#endif

#endif /* GEAR_LONG_H */

