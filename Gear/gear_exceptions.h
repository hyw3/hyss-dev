/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_EXCEPTIONS_H
#define GEAR_EXCEPTIONS_H

BEGIN_EXTERN_C()

extern GEAR_API gear_class_entry *gear_ce_throwable;
extern GEAR_API gear_class_entry *gear_ce_exception;
extern GEAR_API gear_class_entry *gear_ce_error_exception;
extern GEAR_API gear_class_entry *gear_ce_error;
extern GEAR_API gear_class_entry *gear_ce_compile_error;
extern GEAR_API gear_class_entry *gear_ce_parse_error;
extern GEAR_API gear_class_entry *gear_ce_type_error;
extern GEAR_API gear_class_entry *gear_ce_argument_count_error;
extern GEAR_API gear_class_entry *gear_ce_arithmetic_error;
extern GEAR_API gear_class_entry *gear_ce_division_by_zero_error;

GEAR_API void gear_exception_set_previous(gear_object *exception, gear_object *add_previous);
GEAR_API void gear_exception_save(void);
GEAR_API void gear_exception_restore(void);

GEAR_API GEAR_COLD void gear_throw_exception_internal(zval *exception);

void gear_register_default_exception(void);

GEAR_API gear_class_entry *gear_get_exception_base(zval *object);

/* Deprecated - Use gear_ce_exception directly instead */
GEAR_API gear_class_entry *gear_exception_get_default(void);

/* Deprecated - Use gear_ce_error_exception directly instead */
GEAR_API gear_class_entry *gear_get_error_exception(void);

GEAR_API void gear_register_default_classes(void);

/* exception_ce   NULL, gear_ce_exception, gear_ce_error, or a derived class
 * message        NULL or the message of the exception */
GEAR_API GEAR_COLD gear_object *gear_throw_exception(gear_class_entry *exception_ce, const char *message, gear_long code);
GEAR_API GEAR_COLD gear_object *gear_throw_exception_ex(gear_class_entry *exception_ce, gear_long code, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 3, 4);
GEAR_API GEAR_COLD void gear_throw_exception_object(zval *exception);
GEAR_API void gear_clear_exception(void);

GEAR_API gear_object *gear_throw_error_exception(gear_class_entry *exception_ce, const char *message, gear_long code, int severity);

extern GEAR_API void (*gear_throw_exception_hook)(zval *ex);

/* show an exception using gear_error(severity,...), severity should be E_ERROR */
GEAR_API GEAR_COLD void gear_exception_error(gear_object *exception, int severity);

#include "gear_globals.h"

static gear_always_inline void gear_rethrow_exception(gear_execute_data *execute_data)
{
	if (EX(opline)->opcode != GEAR_HANDLE_EXCEPTION) {
		EG(opline_before_exception) = EX(opline);
		EX(opline) = EG(exception_op);
	}
}

END_EXTERN_C()

#endif

