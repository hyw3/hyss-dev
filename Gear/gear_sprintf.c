/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "gear.h"

#ifdef HAVE_STDARG_H
# include <stdarg.h>
#endif

#if GEAR_BROKEN_SPRINTF
int gear_sprintf(char *buffer, const char *format, ...)
{
	int len;
	va_list args;

	va_start(args, format);
	len = vsprintf(buffer, format, args);
	va_end(args);

	return len;
}
#endif

