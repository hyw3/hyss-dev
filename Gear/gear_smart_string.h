/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SMART_STRING_H
#define HYSS_SMART_STRING_H

#include "gear_smart_string_public.h"

#include <stdlib.h>
#include <gear.h>

/* wrapper */

#define smart_string_appends_ex(str, src, what) \
	smart_string_appendl_ex((str), (src), strlen(src), (what))
#define smart_string_appends(str, src) \
	smart_string_appendl((str), (src), strlen(src))
#define smart_string_append_ex(str, src, what) \
	smart_string_appendl_ex((str), ((smart_string *)(src))->c, \
		((smart_string *)(src))->len, (what));
#define smart_string_sets(str, src) \
	smart_string_setl((str), (src), strlen(src));

#define smart_string_appendc(str, c) \
	smart_string_appendc_ex((str), (c), 0)
#define smart_string_free(s) \
	smart_string_free_ex((s), 0)
#define smart_string_appendl(str, src, len) \
	smart_string_appendl_ex((str), (src), (len), 0)
#define smart_string_append(str, src) \
	smart_string_append_ex((str), (src), 0)
#define smart_string_append_long(str, val) \
	smart_string_append_long_ex((str), (val), 0)
#define smart_string_append_unsigned(str, val) \
	smart_string_append_unsigned_ex((str), (val), 0)

GEAR_API void GEAR_FASTCALL _smart_string_alloc_persistent(smart_string *str, size_t len);
GEAR_API void GEAR_FASTCALL _smart_string_alloc(smart_string *str, size_t len);

static gear_always_inline size_t smart_string_alloc(smart_string *str, size_t len, gear_bool persistent) {
	if (UNEXPECTED(!str->c) || UNEXPECTED(len >= str->a - str->len)) {
		if (persistent) {
			_smart_string_alloc_persistent(str, len);
		} else {
			_smart_string_alloc(str, len);
		}
	}
	return str->len + len;
}

static gear_always_inline void smart_string_free_ex(smart_string *str, gear_bool persistent) {
	if (str->c) {
		pefree(str->c, persistent);
		str->c = NULL;
	}
	str->a = str->len = 0;
}

static gear_always_inline void smart_string_0(smart_string *str) {
	if (str->c) {
		str->c[str->len] = '\0';
	}
}

static gear_always_inline void smart_string_appendc_ex(smart_string *dest, char ch, gear_bool persistent) {
	dest->len = smart_string_alloc(dest, 1, persistent);
	dest->c[dest->len - 1] = ch;
}

static gear_always_inline void smart_string_appendl_ex(smart_string *dest, const char *str, size_t len, gear_bool persistent) {
	size_t new_len = smart_string_alloc(dest, len, persistent);
	memcpy(dest->c + dest->len, str, len);
	dest->len = new_len;

}

static gear_always_inline void smart_string_append_long_ex(smart_string *dest, gear_long num, gear_bool persistent) {
	char buf[32];
	char *result = gear_print_long_to_buf(buf + sizeof(buf) - 1, num);
	smart_string_appendl_ex(dest, result, buf + sizeof(buf) - 1 - result, persistent);
}

static gear_always_inline void smart_string_append_unsigned_ex(smart_string *dest, gear_ulong num, gear_bool persistent) {
	char buf[32];
	char *result = gear_print_ulong_to_buf(buf + sizeof(buf) - 1, num);
	smart_string_appendl_ex(dest, result, buf + sizeof(buf) - 1 - result, persistent);
}

static gear_always_inline void smart_string_setl(smart_string *dest, char *src, size_t len) {
	dest->len = len;
	dest->a = len + 1;
	dest->c = src;
}

static gear_always_inline void smart_string_reset(smart_string *str) {
	str->len = 0;
}

#endif

