/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_CPU_INFO_H
#define GEAR_CPU_INFO_H

#include "gear.h"

#define GEAR_CPU_EBX_MASK     (1<<30)
#define GEAR_CPU_EDX_MASK     (1<<31)

typedef enum _gear_cpu_feature {
	/* ECX */
	GEAR_CPU_FEATURE_SSE3			= (1<<0),
	GEAR_CPU_FEATURE_PCLMULQDQ		= (1<<1),
	GEAR_CPU_FEATURE_DTES64			= (1<<2),
	GEAR_CPU_FEATURE_MONITOR		= (1<<3),
	GEAR_CPU_FEATURE_DSCPL			= (1<<4),
	GEAR_CPU_FEATURE_VMX			= (1<<5),
	GEAR_CPU_FEATURE_SMX			= (1<<6),
	GEAR_CPU_FEATURE_EST			= (1<<7),
	GEAR_CPU_FEATURE_TM2			= (1<<8),
	GEAR_CPU_FEATURE_SSSE3			= (1<<9),
	GEAR_CPU_FEATURE_CID			= (1<<10),
	GEAR_CPU_FEATURE_SDBG			= (1<<11),
	GEAR_CPU_FEATURE_FMA			= (1<<12),
	GEAR_CPU_FEATURE_CX16			= (1<<13),
	GEAR_CPU_FEATURE_XTPR			= (1<<14),
	GEAR_CPU_FEATURE_PDCM			= (1<<15),
	/* reserved						= (1<<16),*/
	GEAR_CPU_FEATURE_PCID			= (1<<17),
	GEAR_CPU_FEATURE_DCA			= (1<<18),
	GEAR_CPU_FEATURE_SSE41			= (1<<19),
	GEAR_CPU_FEATURE_SSE42			= (1<<20),
	GEAR_CPU_FEATURE_X2APIC			= (1<<21),
	GEAR_CPU_FEATURE_MOVBE			= (1<<22),
	GEAR_CPU_FEATURE_POPCNT			= (1<<23),
	GEAR_CPU_FEATURE_TSC_DEADLINE 	= (1<<24),
	GEAR_CPU_FEATURE_AES			= (1<<25),
	GEAR_CPU_FEATURE_XSAVE			= (1<<26),
	GEAR_CPU_FEATURE_OSXSAVE		= (1<<27) ,
	GEAR_CPU_FEATURE_AVX			= (1<<28),
	GEAR_CPU_FEATURE_F16C			= (1<<29),
	/* intentionally don't support	= (1<<30) */
	/* intentionally don't support	= (1<<31) */

	/* EBX */
	GEAR_CPU_FEATURE_AVX2			= (1<<5 | GEAR_CPU_EBX_MASK),

	/* EDX */
	GEAR_CPU_FEATURE_FPU			= (1<<0 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_VME			= (1<<1 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_DE				= (1<<2 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_PSE			= (1<<3 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_TSC			= (1<<4 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_MSR			= (1<<5 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_PAE			= (1<<6 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_MCE			= (1<<7 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_CX8			= (1<<8 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_APIC			= (1<<9 | GEAR_CPU_EDX_MASK),
	/* reserved						= (1<<10 | GEAR_CPU_EDX_MASK),*/
	GEAR_CPU_FEATURE_SEP			= (1<<11 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_MTRR			= (1<<12 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_PGE			= (1<<13 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_MCA			= (1<<14 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_CMOV			= (1<<15 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_PAT			= (1<<16 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_PSE36			= (1<<17 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_PN				= (1<<18 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_CLFLUSH		= (1<<19 | GEAR_CPU_EDX_MASK),
	/* reserved						= (1<<20 | GEAR_CPU_EDX_MASK),*/
	GEAR_CPU_FEATURE_DS				= (1<<21 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_ACPI			= (1<<22 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_MMX			= (1<<23 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_FXSR			= (1<<24 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_SSE			= (1<<25 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_SSE2			= (1<<26 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_SS				= (1<<27 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_HT				= (1<<28 | GEAR_CPU_EDX_MASK),
	GEAR_CPU_FEATURE_TM				= (1<<29 | GEAR_CPU_EDX_MASK)
	/*intentionally don't support   = (1<<30 | GEAR_CPU_EDX_MASK)*/
	/*intentionally don't support   = (1<<31 | GEAR_CPU_EDX_MASK)*/
} gear_cpu_feature;

void gear_cpu_startup();
GEAR_API int gear_cpu_supports(gear_cpu_feature feature);

/* Address sanitizer is incompatible with ifunc resolvers, so exclude the
 * CPU support helpers from asan.
 * See also https://github.com/google/sanitizers/issues/342. */
#if __has_attribute(no_sanitize_address)
# define GEAR_NO_SANITIZE_ADDRESS __attribute__((no_sanitize_address))
#else
# define GEAR_NO_SANITIZE_ADDRESS
#endif

#if HYSS_HAVE_BUILTIN_CPU_SUPPORTS
/* NOTE: you should use following inline function in
 * resolver functions (ifunc), as it could be called
 * before all PLT symbols are resloved. in other words,
 * resolver functions should not depends any external
 * functions */
GEAR_NO_SANITIZE_ADDRESS
static gear_always_inline int gear_cpu_supports_sse2() {
#if HYSS_HAVE_BUILTIN_CPU_INIT
	__builtin_cpu_init();
#endif
	return __builtin_cpu_supports("sse2");
}

GEAR_NO_SANITIZE_ADDRESS
static gear_always_inline int gear_cpu_supports_sse3() {
#if HYSS_HAVE_BUILTIN_CPU_INIT
	__builtin_cpu_init();
#endif
	return __builtin_cpu_supports("sse3");
}

GEAR_NO_SANITIZE_ADDRESS
static gear_always_inline int gear_cpu_supports_ssse3() {
#if HYSS_HAVE_BUILTIN_CPU_INIT
	__builtin_cpu_init();
#endif
	return __builtin_cpu_supports("ssse3");
}

GEAR_NO_SANITIZE_ADDRESS
static gear_always_inline int gear_cpu_supports_sse41() {
#if HYSS_HAVE_BUILTIN_CPU_INIT
	__builtin_cpu_init();
#endif
	return __builtin_cpu_supports("sse4.1");
}

GEAR_NO_SANITIZE_ADDRESS
static gear_always_inline int gear_cpu_supports_sse42() {
#if HYSS_HAVE_BUILTIN_CPU_INIT
	__builtin_cpu_init();
#endif
	return __builtin_cpu_supports("sse4.2");
}

GEAR_NO_SANITIZE_ADDRESS
static gear_always_inline int gear_cpu_supports_avx() {
#if HYSS_HAVE_BUILTIN_CPU_INIT
	__builtin_cpu_init();
#endif
	return __builtin_cpu_supports("avx");
}

GEAR_NO_SANITIZE_ADDRESS
static gear_always_inline int gear_cpu_supports_avx2() {
#if HYSS_HAVE_BUILTIN_CPU_INIT
	__builtin_cpu_init();
#endif
	return __builtin_cpu_supports("avx2");
}
#else

static gear_always_inline int gear_cpu_supports_sse2() {
	return gear_cpu_supports(GEAR_CPU_FEATURE_SSE2);
}

static gear_always_inline int gear_cpu_supports_sse3() {
	return gear_cpu_supports(GEAR_CPU_FEATURE_SSE3);
}

static gear_always_inline int gear_cpu_supports_ssse3() {
	return gear_cpu_supports(GEAR_CPU_FEATURE_SSSE3);
}

static gear_always_inline int gear_cpu_supports_sse41() {
	return gear_cpu_supports(GEAR_CPU_FEATURE_SSE41);
}

static gear_always_inline int gear_cpu_supports_sse42() {
	return gear_cpu_supports(GEAR_CPU_FEATURE_SSE42);
}

static gear_always_inline int gear_cpu_supports_avx() {
	return gear_cpu_supports(GEAR_CPU_FEATURE_AVX);
}

static gear_always_inline int gear_cpu_supports_avx2() {
	return gear_cpu_supports(GEAR_CPU_FEATURE_AVX2);
}

#endif

#endif

