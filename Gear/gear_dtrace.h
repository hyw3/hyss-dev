/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef	_GEAR_DTRACE_H
#define	_GEAR_DTRACE_H

#ifndef GEAR_WIN32
# include <unistd.h>
#endif

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef HAVE_DTRACE
GEAR_API extern gear_op_array *(*gear_dtrace_compile_file)(gear_file_handle *file_handle, int type);
GEAR_API extern void (*gear_dtrace_execute)(gear_op_array *op_array);
GEAR_API extern void (*gear_dtrace_execute_internal)(gear_execute_data *execute_data, zval *return_value);

GEAR_API gear_op_array *dtrace_compile_file(gear_file_handle *file_handle, int type);
GEAR_API void dtrace_execute_ex(gear_execute_data *execute_data);
GEAR_API void dtrace_execute_internal(gear_execute_data *execute_data, zval *return_value);
#include <gear_dtrace_gen.h>

#endif /* HAVE_DTRACE */

#ifdef	__cplusplus
}
#endif

#endif	/* _GEAR_DTRACE_H */

