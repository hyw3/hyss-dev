/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_EXTENSIONS_H
#define GEAR_EXTENSIONS_H

#include "gear_compile.h"
#include "gear_build.h"

#define GEAR_EXTENSION_API_NO	320200801

typedef struct _gear_extension_version_info {
	int gear_extension_api_no;
	char *build_id;
} gear_extension_version_info;

#define GEAR_EXTENSION_BUILD_ID "API" GEAR_TOSTR(GEAR_EXTENSION_API_NO) GEAR_BUILD_TS GEAR_BUILD_DEBUG GEAR_BUILD_SYSTEM GEAR_BUILD_EXTRA

typedef struct _gear_extension gear_extension;

/* Typedef's for gear_extension function pointers */
typedef int (*startup_func_t)(gear_extension *extension);
typedef void (*shutdown_func_t)(gear_extension *extension);
typedef void (*activate_func_t)(void);
typedef void (*deactivate_func_t)(void);

typedef void (*message_handler_func_t)(int message, void *arg);

typedef void (*op_array_handler_func_t)(gear_op_array *op_array);

typedef void (*statement_handler_func_t)(gear_execute_data *frame);
typedef void (*fcall_begin_handler_func_t)(gear_execute_data *frame);
typedef void (*fcall_end_handler_func_t)(gear_execute_data *frame);

typedef void (*op_array_ctor_func_t)(gear_op_array *op_array);
typedef void (*op_array_dtor_func_t)(gear_op_array *op_array);
typedef size_t (*op_array_persist_calc_func_t)(gear_op_array *op_array);
typedef size_t (*op_array_persist_func_t)(gear_op_array *op_array, void *mem);

struct _gear_extension {
	char *name;
	char *version;
	char *author;
	char *URL;
	char *copyright;

	startup_func_t startup;
	shutdown_func_t shutdown;
	activate_func_t activate;
	deactivate_func_t deactivate;

	message_handler_func_t message_handler;

	op_array_handler_func_t op_array_handler;

	statement_handler_func_t statement_handler;
	fcall_begin_handler_func_t fcall_begin_handler;
	fcall_end_handler_func_t fcall_end_handler;

	op_array_ctor_func_t op_array_ctor;
	op_array_dtor_func_t op_array_dtor;

	int (*api_no_check)(int api_no);
	int (*build_id_check)(const char* build_id);
	op_array_persist_calc_func_t op_array_persist_calc;
	op_array_persist_func_t op_array_persist;
	void *reserved5;
	void *reserved6;
	void *reserved7;
	void *reserved8;

	DL_HANDLE handle;
	int resource_number;
};

BEGIN_EXTERN_C()
GEAR_API int gear_get_resource_handle(gear_extension *extension);
GEAR_API void gear_extension_dispatch_message(int message, void *arg);
END_EXTERN_C()

#define GEAR_EXTMSG_NEW_EXTENSION		1


#define GEAR_EXTENSION()	\
	GEAR_EXT_API gear_extension_version_info extension_version_info = { GEAR_EXTENSION_API_NO, GEAR_EXTENSION_BUILD_ID }

#define STANDARD_GEAR_EXTENSION_PROPERTIES       NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, -1
#define COMPAT_GEAR_EXTENSION_PROPERTIES         NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, -1
#define BUILD_COMPAT_GEAR_EXTENSION_PROPERTIES   NULL, NULL, NULL, NULL, NULL, NULL, NULL, -1


GEAR_API extern gear_llist gear_extensions;
GEAR_API extern uint32_t gear_extension_flags;

#define GEAR_EXTENSIONS_HAVE_OP_ARRAY_CTOR         (1<<0)
#define GEAR_EXTENSIONS_HAVE_OP_ARRAY_DTOR         (1<<1)
#define GEAR_EXTENSIONS_HAVE_OP_ARRAY_HANDLER      (1<<2)
#define GEAR_EXTENSIONS_HAVE_OP_ARRAY_PERSIST_CALC (1<<3)
#define GEAR_EXTENSIONS_HAVE_OP_ARRAY_PERSIST      (1<<4)

void gear_extension_dtor(gear_extension *extension);
GEAR_API void gear_append_version_info(const gear_extension *extension);
int gear_startup_extensions_mechanism(void);
int gear_startup_extensions(void);
void gear_shutdown_extensions(void);

BEGIN_EXTERN_C()
GEAR_API int gear_load_extension(const char *path);
GEAR_API int gear_load_extension_handle(DL_HANDLE handle, const char *path);
GEAR_API int gear_register_extension(gear_extension *new_extension, DL_HANDLE handle);
GEAR_API gear_extension *gear_get_extension(const char *extension_name);
GEAR_API size_t gear_extensions_op_array_persist_calc(gear_op_array *op_array);
GEAR_API size_t gear_extensions_op_array_persist(gear_op_array *op_array, void *mem);
END_EXTERN_C()

#endif /* GEAR_EXTENSIONS_H */

