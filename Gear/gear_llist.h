/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_LLIST_H
#define GEAR_LLIST_H

typedef struct _gear_llist_element {
	struct _gear_llist_element *next;
	struct _gear_llist_element *prev;
	char data[1]; /* Needs to always be last in the struct */
} gear_llist_element;

typedef void (*llist_dtor_func_t)(void *);
typedef int (*llist_compare_func_t)(const gear_llist_element **, const gear_llist_element **);
typedef void (*llist_apply_with_args_func_t)(void *data, int num_args, va_list args);
typedef void (*llist_apply_with_arg_func_t)(void *data, void *arg);
typedef void (*llist_apply_func_t)(void *);

typedef struct _gear_llist {
	gear_llist_element *head;
	gear_llist_element *tail;
	size_t count;
	size_t size;
	llist_dtor_func_t dtor;
	unsigned char persistent;
	gear_llist_element *traverse_ptr;
} gear_llist;

typedef gear_llist_element* gear_llist_position;

BEGIN_EXTERN_C()
GEAR_API void gear_llist_init(gear_llist *l, size_t size, llist_dtor_func_t dtor, unsigned char persistent);
GEAR_API void gear_llist_add_element(gear_llist *l, void *element);
GEAR_API void gear_llist_prepend_element(gear_llist *l, void *element);
GEAR_API void gear_llist_del_element(gear_llist *l, void *element, int (*compare)(void *element1, void *element2));
GEAR_API void gear_llist_destroy(gear_llist *l);
GEAR_API void gear_llist_clean(gear_llist *l);
GEAR_API void gear_llist_remove_tail(gear_llist *l);
GEAR_API void gear_llist_copy(gear_llist *dst, gear_llist *src);
GEAR_API void gear_llist_apply(gear_llist *l, llist_apply_func_t func);
GEAR_API void gear_llist_apply_with_del(gear_llist *l, int (*func)(void *data));
GEAR_API void gear_llist_apply_with_argument(gear_llist *l, llist_apply_with_arg_func_t func, void *arg);
GEAR_API void gear_llist_apply_with_arguments(gear_llist *l, llist_apply_with_args_func_t func, int num_args, ...);
GEAR_API size_t gear_llist_count(gear_llist *l);
GEAR_API void gear_llist_sort(gear_llist *l, llist_compare_func_t comp_func);

/* traversal */
GEAR_API void *gear_llist_get_first_ex(gear_llist *l, gear_llist_position *pos);
GEAR_API void *gear_llist_get_last_ex(gear_llist *l, gear_llist_position *pos);
GEAR_API void *gear_llist_get_next_ex(gear_llist *l, gear_llist_position *pos);
GEAR_API void *gear_llist_get_prev_ex(gear_llist *l, gear_llist_position *pos);

#define gear_llist_get_first(l) gear_llist_get_first_ex(l, NULL)
#define gear_llist_get_last(l) gear_llist_get_last_ex(l, NULL)
#define gear_llist_get_next(l) gear_llist_get_next_ex(l, NULL)
#define gear_llist_get_prev(l) gear_llist_get_prev_ex(l, NULL)

END_EXTERN_C()

#endif /* GEAR_LLIST_H */


