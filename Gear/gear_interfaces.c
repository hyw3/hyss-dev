/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"
#include "gear_interfaces.h"
#include "gear_exceptions.h"

GEAR_API gear_class_entry *gear_ce_traversable;
GEAR_API gear_class_entry *gear_ce_aggregate;
GEAR_API gear_class_entry *gear_ce_iterator;
GEAR_API gear_class_entry *gear_ce_arrayaccess;
GEAR_API gear_class_entry *gear_ce_serializable;
GEAR_API gear_class_entry *gear_ce_countable;

/* {{{ gear_call_method
 Only returns the returned zval if retval_ptr != NULL */
GEAR_API zval* gear_call_method(zval *object, gear_class_entry *obj_ce, gear_function **fn_proxy, const char *function_name, size_t function_name_len, zval *retval_ptr, int param_count, zval* arg1, zval* arg2)
{
	int result;
	gear_fcall_info fci;
	zval retval;
	zval params[2];

	if (param_count > 0) {
		ZVAL_COPY_VALUE(&params[0], arg1);
	}
	if (param_count > 1) {
		ZVAL_COPY_VALUE(&params[1], arg2);
	}

	fci.size = sizeof(fci);
	fci.object = object ? Z_OBJ_P(object) : NULL;
	fci.retval = retval_ptr ? retval_ptr : &retval;
	fci.param_count = param_count;
	fci.params = params;
	fci.no_separation = 1;

	if (!fn_proxy && !obj_ce) {
		/* no interest in caching and no information already present that is
		 * needed later inside gear_call_function. */
		ZVAL_STRINGL(&fci.function_name, function_name, function_name_len);
		result = gear_call_function(&fci, NULL);
		zval_ptr_dtor(&fci.function_name);
	} else {
		gear_fcall_info_cache fcic;
		ZVAL_UNDEF(&fci.function_name); /* Unused */

		if (!obj_ce) {
			obj_ce = object ? Z_OBJCE_P(object) : NULL;
		}
		if (!fn_proxy || !*fn_proxy) {
			if (EXPECTED(obj_ce)) {
				fcic.function_handler = gear_hash_str_find_ptr(
					&obj_ce->function_table, function_name, function_name_len);
				if (UNEXPECTED(fcic.function_handler == NULL)) {
					/* error at c-level */
					gear_error_noreturn(E_CORE_ERROR, "Couldn't find implementation for method %s::%s", ZSTR_VAL(obj_ce->name), function_name);
				}
			} else {
				fcic.function_handler = gear_fetch_function_str(function_name, function_name_len);
				if (UNEXPECTED(fcic.function_handler == NULL)) {
					/* error at c-level */
					gear_error_noreturn(E_CORE_ERROR, "Couldn't find implementation for function %s", function_name);
				}
			}
			if (fn_proxy) {
				*fn_proxy = fcic.function_handler;
			}
		} else {
			fcic.function_handler = *fn_proxy;
		}

		if (object) {
			fcic.called_scope = Z_OBJCE_P(object);
		} else {
			gear_class_entry *called_scope = gear_get_called_scope(EG(current_execute_data));

			if (obj_ce &&
			    (!called_scope ||
			     !instanceof_function(called_scope, obj_ce))) {
				fcic.called_scope = obj_ce;
			} else {
				fcic.called_scope = called_scope;
			}
		}
		fcic.object = object ? Z_OBJ_P(object) : NULL;
		result = gear_call_function(&fci, &fcic);
	}
	if (result == FAILURE) {
		/* error at c-level */
		if (!obj_ce) {
			obj_ce = object ? Z_OBJCE_P(object) : NULL;
		}
		if (!EG(exception)) {
			gear_error_noreturn(E_CORE_ERROR, "Couldn't execute method %s%s%s", obj_ce ? ZSTR_VAL(obj_ce->name) : "", obj_ce ? "::" : "", function_name);
		}
	}
	if (!retval_ptr) {
		zval_ptr_dtor(&retval);
		return NULL;
	}
	return retval_ptr;
}
/* }}} */

/* iterator interface, c-level functions used by engine */

/* {{{ gear_user_it_new_iterator */
GEAR_API void gear_user_it_new_iterator(gear_class_entry *ce, zval *object, zval *retval)
{
	gear_call_method_with_0_params(object, ce, &ce->iterator_funcs_ptr->zf_new_iterator, "getiterator", retval);
}
/* }}} */

/* {{{ gear_user_it_invalidate_current */
GEAR_API void gear_user_it_invalidate_current(gear_object_iterator *_iter)
{
	gear_user_iterator *iter = (gear_user_iterator*)_iter;

	if (!Z_ISUNDEF(iter->value)) {
		zval_ptr_dtor(&iter->value);
		ZVAL_UNDEF(&iter->value);
	}
}
/* }}} */

/* {{{ gear_user_it_dtor */
static void gear_user_it_dtor(gear_object_iterator *_iter)
{
	gear_user_iterator *iter = (gear_user_iterator*)_iter;
	zval *object = &iter->it.data;

	gear_user_it_invalidate_current(_iter);
	zval_ptr_dtor(object);
}
/* }}} */

/* {{{ gear_user_it_valid */
GEAR_API int gear_user_it_valid(gear_object_iterator *_iter)
{
	if (_iter) {
		gear_user_iterator *iter = (gear_user_iterator*)_iter;
		zval *object = &iter->it.data;
		zval more;
		int result;

		gear_call_method_with_0_params(object, iter->ce, &iter->ce->iterator_funcs_ptr->zf_valid, "valid", &more);
		result = i_gear_is_true(&more);
		zval_ptr_dtor(&more);
		return result ? SUCCESS : FAILURE;
	}
	return FAILURE;
}
/* }}} */

/* {{{ gear_user_it_get_current_data */
GEAR_API zval *gear_user_it_get_current_data(gear_object_iterator *_iter)
{
	gear_user_iterator *iter = (gear_user_iterator*)_iter;
	zval *object = &iter->it.data;

	if (Z_ISUNDEF(iter->value)) {
		gear_call_method_with_0_params(object, iter->ce, &iter->ce->iterator_funcs_ptr->zf_current, "current", &iter->value);
	}
	return &iter->value;
}
/* }}} */

/* {{{ gear_user_it_get_current_key */
GEAR_API void gear_user_it_get_current_key(gear_object_iterator *_iter, zval *key)
{
	gear_user_iterator *iter = (gear_user_iterator*)_iter;
	zval *object = &iter->it.data;
	zval retval;

	gear_call_method_with_0_params(object, iter->ce, &iter->ce->iterator_funcs_ptr->zf_key, "key", &retval);

	if (Z_TYPE(retval) != IS_UNDEF) {
		ZVAL_ZVAL(key, &retval, 1, 1);
	} else {
		if (!EG(exception)) {
			gear_error(E_WARNING, "Nothing returned from %s::key()", ZSTR_VAL(iter->ce->name));
		}

		ZVAL_LONG(key, 0);
	}
}
/* }}} */

/* {{{ gear_user_it_move_forward */
GEAR_API void gear_user_it_move_forward(gear_object_iterator *_iter)
{
	gear_user_iterator *iter = (gear_user_iterator*)_iter;
	zval *object = &iter->it.data;

	gear_user_it_invalidate_current(_iter);
	gear_call_method_with_0_params(object, iter->ce, &iter->ce->iterator_funcs_ptr->zf_next, "next", NULL);
}
/* }}} */

/* {{{ gear_user_it_rewind */
GEAR_API void gear_user_it_rewind(gear_object_iterator *_iter)
{
	gear_user_iterator *iter = (gear_user_iterator*)_iter;
	zval *object = &iter->it.data;

	gear_user_it_invalidate_current(_iter);
	gear_call_method_with_0_params(object, iter->ce, &iter->ce->iterator_funcs_ptr->zf_rewind, "rewind", NULL);
}
/* }}} */

static const gear_object_iterator_funcs gear_interface_iterator_funcs_iterator = {
	gear_user_it_dtor,
	gear_user_it_valid,
	gear_user_it_get_current_data,
	gear_user_it_get_current_key,
	gear_user_it_move_forward,
	gear_user_it_rewind,
	gear_user_it_invalidate_current
};

/* {{{ gear_user_it_get_iterator */
static gear_object_iterator *gear_user_it_get_iterator(gear_class_entry *ce, zval *object, int by_ref)
{
	gear_user_iterator *iterator;

	if (by_ref) {
		gear_throw_error(NULL, "An iterator cannot be used with foreach by reference");
		return NULL;
	}

	iterator = emalloc(sizeof(gear_user_iterator));

	gear_iterator_init((gear_object_iterator*)iterator);

	ZVAL_COPY(&iterator->it.data, object);
	iterator->it.funcs = &gear_interface_iterator_funcs_iterator;
	iterator->ce = Z_OBJCE_P(object);
	ZVAL_UNDEF(&iterator->value);
	return (gear_object_iterator*)iterator;
}
/* }}} */

/* {{{ gear_user_it_get_new_iterator */
GEAR_API gear_object_iterator *gear_user_it_get_new_iterator(gear_class_entry *ce, zval *object, int by_ref)
{
	zval iterator;
	gear_object_iterator *new_iterator;
	gear_class_entry *ce_it;

	gear_user_it_new_iterator(ce, object, &iterator);
	ce_it = (Z_TYPE(iterator) == IS_OBJECT) ? Z_OBJCE(iterator) : NULL;

	if (!ce_it || !ce_it->get_iterator || (ce_it->get_iterator == gear_user_it_get_new_iterator && Z_OBJ(iterator) == Z_OBJ_P(object))) {
		if (!EG(exception)) {
			gear_throw_exception_ex(NULL, 0, "Objects returned by %s::getIterator() must be traversable or implement interface Iterator", ce ? ZSTR_VAL(ce->name) : ZSTR_VAL(Z_OBJCE_P(object)->name));
		}
		zval_ptr_dtor(&iterator);
		return NULL;
	}

	new_iterator = ce_it->get_iterator(ce_it, &iterator, by_ref);
	zval_ptr_dtor(&iterator);
	return new_iterator;
}
/* }}} */

/* {{{ gear_implement_traversable */
static int gear_implement_traversable(gear_class_entry *interface, gear_class_entry *class_type)
{
	/* check that class_type is traversable at c-level or implements at least one of 'aggregate' and 'Iterator' */
	uint32_t i;

	if (class_type->get_iterator || (class_type->parent && class_type->parent->get_iterator)) {
		return SUCCESS;
	}
	for (i = 0; i < class_type->num_interfaces; i++) {
		if (class_type->interfaces[i] == gear_ce_aggregate || class_type->interfaces[i] == gear_ce_iterator) {
			return SUCCESS;
		}
	}
	gear_error_noreturn(E_CORE_ERROR, "Class %s must implement interface %s as part of either %s or %s",
		ZSTR_VAL(class_type->name),
		ZSTR_VAL(gear_ce_traversable->name),
		ZSTR_VAL(gear_ce_iterator->name),
		ZSTR_VAL(gear_ce_aggregate->name));
	return FAILURE;
}
/* }}} */

/* {{{ gear_implement_aggregate */
static int gear_implement_aggregate(gear_class_entry *interface, gear_class_entry *class_type)
{
	uint32_t i;
	int t = -1;

	if (class_type->get_iterator) {
		if (class_type->type == GEAR_INTERNAL_CLASS) {
			/* inheritance ensures the class has necessary userland methods */
			return SUCCESS;
		} else if (class_type->get_iterator != gear_user_it_get_new_iterator) {
			/* c-level get_iterator cannot be changed (exception being only Traversable is implmented) */
			if (class_type->num_interfaces) {
				for (i = 0; i < class_type->num_interfaces; i++) {
					if (class_type->interfaces[i] == gear_ce_iterator) {
						gear_error_noreturn(E_ERROR, "Class %s cannot implement both %s and %s at the same time",
									ZSTR_VAL(class_type->name),
									ZSTR_VAL(interface->name),
									ZSTR_VAL(gear_ce_iterator->name));
						return FAILURE;
					}
					if (class_type->interfaces[i] == gear_ce_traversable) {
						t = i;
					}
				}
			}
			if (t == -1) {
				return FAILURE;
			}
		}
	}
	class_type->get_iterator = gear_user_it_get_new_iterator;
	if (class_type->iterator_funcs_ptr != NULL) {
		class_type->iterator_funcs_ptr->zf_new_iterator = NULL;
	} else if (class_type->type == GEAR_INTERNAL_CLASS) {
		class_type->iterator_funcs_ptr = calloc(1, sizeof(gear_class_iterator_funcs));
	} else {
		class_type->iterator_funcs_ptr = gear_arena_alloc(&CG(arena), sizeof(gear_class_iterator_funcs));
		memset(class_type->iterator_funcs_ptr, 0, sizeof(gear_class_iterator_funcs));
	}
	if (class_type->type == GEAR_INTERNAL_CLASS) {
		class_type->iterator_funcs_ptr->zf_new_iterator = gear_hash_str_find_ptr(&class_type->function_table, "getiterator", sizeof("getiterator") - 1);
	}
	return SUCCESS;
}
/* }}} */

/* {{{ gear_implement_iterator */
static int gear_implement_iterator(gear_class_entry *interface, gear_class_entry *class_type)
{
	if (class_type->get_iterator && class_type->get_iterator != gear_user_it_get_iterator) {
		if (class_type->type == GEAR_INTERNAL_CLASS) {
			/* inheritance ensures the class has the necessary userland methods */
			return SUCCESS;
		} else {
			/* c-level get_iterator cannot be changed */
			if (class_type->get_iterator == gear_user_it_get_new_iterator) {
				gear_error_noreturn(E_ERROR, "Class %s cannot implement both %s and %s at the same time",
							ZSTR_VAL(class_type->name),
							ZSTR_VAL(interface->name),
							ZSTR_VAL(gear_ce_aggregate->name));
			}
			return FAILURE;
		}
	}
	class_type->get_iterator = gear_user_it_get_iterator;
	if (class_type->iterator_funcs_ptr != NULL) {
		class_type->iterator_funcs_ptr->zf_valid = NULL;
		class_type->iterator_funcs_ptr->zf_current = NULL;
		class_type->iterator_funcs_ptr->zf_key = NULL;
		class_type->iterator_funcs_ptr->zf_next = NULL;
		class_type->iterator_funcs_ptr->zf_rewind = NULL;
	} else if (class_type->type == GEAR_INTERNAL_CLASS) {
		class_type->iterator_funcs_ptr = calloc(1, sizeof(gear_class_iterator_funcs));
	} else {
		class_type->iterator_funcs_ptr = gear_arena_alloc(&CG(arena), sizeof(gear_class_iterator_funcs));
		memset(class_type->iterator_funcs_ptr, 0, sizeof(gear_class_iterator_funcs));
	}
	if (class_type->type == GEAR_INTERNAL_CLASS) {
		class_type->iterator_funcs_ptr->zf_rewind = gear_hash_str_find_ptr(&class_type->function_table, "rewind", sizeof("rewind") - 1);
		class_type->iterator_funcs_ptr->zf_valid = gear_hash_str_find_ptr(&class_type->function_table, "valid", sizeof("valid") - 1);
		class_type->iterator_funcs_ptr->zf_key = gear_hash_str_find_ptr(&class_type->function_table, "key", sizeof("key") - 1);
		class_type->iterator_funcs_ptr->zf_current = gear_hash_str_find_ptr(&class_type->function_table, "current", sizeof("current") - 1);
		class_type->iterator_funcs_ptr->zf_next = gear_hash_str_find_ptr(&class_type->function_table, "next", sizeof("next") - 1);
	}
	return SUCCESS;
}
/* }}} */

/* {{{ gear_implement_arrayaccess */
static int gear_implement_arrayaccess(gear_class_entry *interface, gear_class_entry *class_type)
{
	return SUCCESS;
}
/* }}}*/

/* {{{ gear_user_serialize */
GEAR_API int gear_user_serialize(zval *object, unsigned char **buffer, size_t *buf_len, gear_serialize_data *data)
{
	gear_class_entry * ce = Z_OBJCE_P(object);
	zval retval;
	int result;

	gear_call_method_with_0_params(object, ce, &ce->serialize_func, "serialize", &retval);


	if (Z_TYPE(retval) == IS_UNDEF || EG(exception)) {
		result = FAILURE;
	} else {
		switch(Z_TYPE(retval)) {
		case IS_NULL:
			/* we could also make this '*buf_len = 0' but this allows to skip variables */
			zval_ptr_dtor(&retval);
			return FAILURE;
		case IS_STRING:
			*buffer = (unsigned char*)estrndup(Z_STRVAL(retval), Z_STRLEN(retval));
			*buf_len = Z_STRLEN(retval);
			result = SUCCESS;
			break;
		default: /* failure */
			result = FAILURE;
			break;
		}
		zval_ptr_dtor(&retval);
	}

	if (result == FAILURE && !EG(exception)) {
		gear_throw_exception_ex(NULL, 0, "%s::serialize() must return a string or NULL", ZSTR_VAL(ce->name));
	}
	return result;
}
/* }}} */

/* {{{ gear_user_unserialize */
GEAR_API int gear_user_unserialize(zval *object, gear_class_entry *ce, const unsigned char *buf, size_t buf_len, gear_unserialize_data *data)
{
	zval zdata;

	if (UNEXPECTED(object_init_ex(object, ce) != SUCCESS)) {
		return FAILURE;
	}

	ZVAL_STRINGL(&zdata, (char*)buf, buf_len);

	gear_call_method_with_1_params(object, ce, &ce->unserialize_func, "unserialize", NULL, &zdata);

	zval_ptr_dtor(&zdata);

	if (EG(exception)) {
		return FAILURE;
	} else {
		return SUCCESS;
	}
}
/* }}} */

GEAR_API int gear_class_serialize_deny(zval *object, unsigned char **buffer, size_t *buf_len, gear_serialize_data *data) /* {{{ */
{
	gear_class_entry *ce = Z_OBJCE_P(object);
	gear_throw_exception_ex(NULL, 0, "Serialization of '%s' is not allowed", ZSTR_VAL(ce->name));
	return FAILURE;
}
/* }}} */

GEAR_API int gear_class_unserialize_deny(zval *object, gear_class_entry *ce, const unsigned char *buf, size_t buf_len, gear_unserialize_data *data) /* {{{ */
{
	gear_throw_exception_ex(NULL, 0, "Unserialization of '%s' is not allowed", ZSTR_VAL(ce->name));
	return FAILURE;
}
/* }}} */

/* {{{ gear_implement_serializable */
static int gear_implement_serializable(gear_class_entry *interface, gear_class_entry *class_type)
{
	if (class_type->parent
		&& (class_type->parent->serialize || class_type->parent->unserialize)
		&& !instanceof_function_ex(class_type->parent, gear_ce_serializable, 1)) {
		return FAILURE;
	}
	if (!class_type->serialize) {
		class_type->serialize = gear_user_serialize;
	}
	if (!class_type->unserialize) {
		class_type->unserialize = gear_user_unserialize;
	}
	return SUCCESS;
}
/* }}}*/

/* {{{ gear_implement_countable */
static int gear_implement_countable(gear_class_entry *interface, gear_class_entry *class_type)
{
	return SUCCESS;
}
/* }}}*/

/* {{{ function tables */
static const gear_function_entry gear_funcs_aggregate[] = {
	GEAR_ABSTRACT_ME(iterator, getIterator, NULL)
	GEAR_FE_END
};

static const gear_function_entry gear_funcs_iterator[] = {
	GEAR_ABSTRACT_ME(iterator, current,  NULL)
	GEAR_ABSTRACT_ME(iterator, next,     NULL)
	GEAR_ABSTRACT_ME(iterator, key,      NULL)
	GEAR_ABSTRACT_ME(iterator, valid,    NULL)
	GEAR_ABSTRACT_ME(iterator, rewind,   NULL)
	GEAR_FE_END
};

static const gear_function_entry *gear_funcs_traversable = NULL;

GEAR_BEGIN_ARG_INFO_EX(arginfo_arrayaccess_offset, 0, 0, 1)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_arrayaccess_offset_get, 0, 0, 1) /* actually this should be return by ref but atm cannot be */
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_arrayaccess_offset_value, 0, 0, 2)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

static const gear_function_entry gear_funcs_arrayaccess[] = {
	GEAR_ABSTRACT_ME(arrayaccess, offsetExists, arginfo_arrayaccess_offset)
	GEAR_ABSTRACT_ME(arrayaccess, offsetGet,    arginfo_arrayaccess_offset_get)
	GEAR_ABSTRACT_ME(arrayaccess, offsetSet,    arginfo_arrayaccess_offset_value)
	GEAR_ABSTRACT_ME(arrayaccess, offsetUnset,  arginfo_arrayaccess_offset)
	GEAR_FE_END
};

GEAR_BEGIN_ARG_INFO(arginfo_serializable_serialize, 0)
	GEAR_ARG_INFO(0, serialized)
GEAR_END_ARG_INFO()

static const gear_function_entry gear_funcs_serializable[] = {
	GEAR_ABSTRACT_ME(serializable, serialize,   NULL)
	GEAR_FENTRY(unserialize, NULL, arginfo_serializable_serialize, GEAR_ACC_PUBLIC|GEAR_ACC_ABSTRACT|GEAR_ACC_CTOR)
	GEAR_FE_END
};

GEAR_BEGIN_ARG_INFO(arginfo_countable_count, 0)
GEAR_END_ARG_INFO()

static const gear_function_entry gear_funcs_countable[] = {
	GEAR_ABSTRACT_ME(Countable, count, arginfo_countable_count)
	GEAR_FE_END
};
/* }}} */

/* {{{ gear_register_interfaces */
GEAR_API void gear_register_interfaces(void)
{
	REGISTER_MAGIC_INTERFACE(traversable, Traversable);

	REGISTER_MAGIC_INTERFACE(aggregate, IteratorAggregate);
	REGISTER_MAGIC_IMPLEMENT(aggregate, traversable);

	REGISTER_MAGIC_INTERFACE(iterator, Iterator);
	REGISTER_MAGIC_IMPLEMENT(iterator, traversable);

	REGISTER_MAGIC_INTERFACE(arrayaccess, ArrayAccess);

	REGISTER_MAGIC_INTERFACE(serializable, Serializable);

	REGISTER_MAGIC_INTERFACE(countable, Countable);
}
/* }}} */

