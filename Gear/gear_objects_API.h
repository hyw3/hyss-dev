/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_OBJECTS_API_H
#define GEAR_OBJECTS_API_H

#include "gear.h"
#include "gear_compile.h"

#define OBJ_BUCKET_INVALID			(1<<0)

#define IS_OBJ_VALID(o)				(!(((gear_uintptr_t)(o)) & OBJ_BUCKET_INVALID))

#define SET_OBJ_INVALID(o)			((gear_object*)((((gear_uintptr_t)(o)) | OBJ_BUCKET_INVALID)))

#define GET_OBJ_BUCKET_NUMBER(o)	(((gear_intptr_t)(o)) >> 1)

#define SET_OBJ_BUCKET_NUMBER(o, n)	do { \
		(o) = (gear_object*)((((gear_uintptr_t)(n)) << 1) | OBJ_BUCKET_INVALID); \
	} while (0)

#define GEAR_OBJECTS_STORE_ADD_TO_FREE_LIST(h) do { \
		SET_OBJ_BUCKET_NUMBER(EG(objects_store).object_buckets[(h)], EG(objects_store).free_list_head); \
		EG(objects_store).free_list_head = (h); \
	} while (0)

#define OBJ_RELEASE(obj) gear_object_release(obj)

typedef struct _gear_objects_store {
	gear_object **object_buckets;
	uint32_t top;
	uint32_t size;
	int free_list_head;
} gear_objects_store;

/* Global store handling functions */
BEGIN_EXTERN_C()
GEAR_API void GEAR_FASTCALL gear_objects_store_init(gear_objects_store *objects, uint32_t init_size);
GEAR_API void GEAR_FASTCALL gear_objects_store_call_destructors(gear_objects_store *objects);
GEAR_API void GEAR_FASTCALL gear_objects_store_mark_destructed(gear_objects_store *objects);
GEAR_API void GEAR_FASTCALL gear_objects_store_free_object_storage(gear_objects_store *objects, gear_bool fast_shutdown);
GEAR_API void GEAR_FASTCALL gear_objects_store_destroy(gear_objects_store *objects);

/* Store API functions */
GEAR_API void GEAR_FASTCALL gear_objects_store_put(gear_object *object);
GEAR_API void GEAR_FASTCALL gear_objects_store_del(gear_object *object);

/* Called when the ctor was terminated by an exception */
static gear_always_inline void gear_object_store_ctor_failed(gear_object *obj)
{
	GC_ADD_FLAGS(obj, IS_OBJ_DESTRUCTOR_CALLED);
}

#define GEAR_OBJECTS_STORE_HANDLERS 0, gear_object_std_dtor, gear_objects_destroy_object, gear_objects_clone_obj

END_EXTERN_C()

static gear_always_inline void gear_object_release(gear_object *obj)
{
	if (GC_DELREF(obj) == 0) {
		gear_objects_store_del(obj);
	} else if (UNEXPECTED(GC_MAY_LEAK((gear_refcounted*)obj))) {
		gc_possible_root((gear_refcounted*)obj);
	}
}

static gear_always_inline size_t gear_object_properties_size(gear_class_entry *ce)
{
	return sizeof(zval) *
		(ce->default_properties_count -
			((ce->ce_flags & GEAR_ACC_USE_GUARDS) ? 0 : 1));
}

/* Allocates object type and zeros it, but not the properties.
 * Properties MUST be initialized using object_properties_init(). */
static gear_always_inline void *gear_object_alloc(size_t obj_size, gear_class_entry *ce) {
	void *obj = emalloc(obj_size + gear_object_properties_size(ce));
	/* Subtraction of sizeof(zval) is necessary, because gear_object_properties_size() may be
	 * -sizeof(zval), if the object has no properties. */
	memset(obj, 0, obj_size - sizeof(zval));
	return obj;
}


#endif /* GEAR_OBJECTS_H */

