/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_H
#define GEAR_H

#define GEAR_VERSION "1.0.12"

#define GEAR_ENGINE_3

#include "gear_types.h"
#include "gear_errors.h"
#include "gear_alloc.h"
#include "gear_llist.h"
#include "gear_string.h"
#include "gear_hash.h"
#include "gear_ast.h"
#include "gear_gc.h"
#include "gear_variables.h"
#include "gear_iterators.h"
#include "gear_stream.h"
#include "gear_smart_str_public.h"
#include "gear_smart_string_public.h"
#include "gear_signal.h"

#define HANDLE_BLOCK_INTERRUPTIONS()		GEAR_SIGNAL_BLOCK_INTERRUPTIONS()
#define HANDLE_UNBLOCK_INTERRUPTIONS()		GEAR_SIGNAL_UNBLOCK_INTERRUPTIONS()

#define INTERNAL_FUNCTION_PARAMETERS gear_execute_data *execute_data, zval *return_value
#define INTERNAL_FUNCTION_PARAM_PASSTHRU execute_data, return_value

#define USED_RET() \
	(!EX(prev_execute_data) || \
	 !GEAR_USER_CODE(EX(prev_execute_data)->func->common.type) || \
	 (EX(prev_execute_data)->opline->result_type != IS_UNUSED))

#ifdef GEAR_ENABLE_STATIC_PBCLS_CACHE
#define GEAR_PBCG PBCG_STATIC
#define GEAR_PBCLS_CACHE_EXTERN() PBCLS_CACHE_EXTERN()
#define GEAR_PBCLS_CACHE_DEFINE() PBCLS_CACHE_DEFINE()
#define GEAR_PBCLS_CACHE_UPDATE() PBCLS_CACHE_UPDATE()
#define GEAR_PBCLS_CACHE PBCLS_CACHE
#else
#define GEAR_PBCG PBCG
#define GEAR_PBCLS_CACHE_EXTERN()
#define GEAR_PBCLS_CACHE_DEFINE()
#define GEAR_PBCLS_CACHE_UPDATE()
#define GEAR_PBCLS_CACHE
#endif

GEAR_PBCLS_CACHE_EXTERN()

#ifdef HAVE_NORETURN
# ifdef GEAR_NORETURN_ALIAS
GEAR_COLD void gear_error_noreturn(int type, const char *format, ...) GEAR_NORETURN GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);
# else
GEAR_API GEAR_COLD GEAR_NORETURN void gear_error_noreturn(int type, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);
# endif
#else
# define gear_error_noreturn gear_error
#endif

struct _gear_serialize_data;
struct _gear_unserialize_data;

typedef struct _gear_serialize_data gear_serialize_data;
typedef struct _gear_unserialize_data gear_unserialize_data;

typedef struct _gear_trait_method_reference {
	gear_string *method_name;
	gear_string *class_name;
} gear_trait_method_reference;

typedef struct _gear_trait_precedence {
	gear_trait_method_reference trait_method;
	uint32_t num_excludes;
	gear_string *exclude_class_names[1];
} gear_trait_precedence;

typedef struct _gear_trait_alias {
	gear_trait_method_reference trait_method;

	/**
	* name for method to be added
	*/
	gear_string *alias;

	/**
	* modifiers to be set on trait method
	*/
	uint32_t modifiers;
} gear_trait_alias;

struct _gear_class_entry {
	char type;
	gear_string *name;
	struct _gear_class_entry *parent;
	int refcount;
	uint32_t ce_flags;

	int default_properties_count;
	int default_static_members_count;
	zval *default_properties_table;
	zval *default_static_members_table;
	zval *static_members_table;
	HashTable function_table;
	HashTable properties_info;
	HashTable constants_table;

	union _gear_function *constructor;
	union _gear_function *destructor;
	union _gear_function *clone;
	union _gear_function *__get;
	union _gear_function *__set;
	union _gear_function *__unset;
	union _gear_function *__isset;
	union _gear_function *__call;
	union _gear_function *__callstatic;
	union _gear_function *__tostring;
	union _gear_function *__debugInfo;
	union _gear_function *serialize_func;
	union _gear_function *unserialize_func;

	/* allocated only if class implements Iterator or IteratorAggregate interface */
	gear_class_iterator_funcs *iterator_funcs_ptr;

	/* handlers */
	union {
		gear_object* (*create_object)(gear_class_entry *class_type);
		int (*interface_gets_implemented)(gear_class_entry *iface, gear_class_entry *class_type); /* a class implements this interface */
	};
	gear_object_iterator *(*get_iterator)(gear_class_entry *ce, zval *object, int by_ref);
	union _gear_function *(*get_static_method)(gear_class_entry *ce, gear_string* method);

	/* serializer callbacks */
	int (*serialize)(zval *object, unsigned char **buffer, size_t *buf_len, gear_serialize_data *data);
	int (*unserialize)(zval *object, gear_class_entry *ce, const unsigned char *buf, size_t buf_len, gear_unserialize_data *data);

	uint32_t num_interfaces;
	uint32_t num_traits;
	gear_class_entry **interfaces;

	gear_class_entry **traits;
	gear_trait_alias **trait_aliases;
	gear_trait_precedence **trait_precedences;

	union {
		struct {
			gear_string *filename;
			uint32_t line_start;
			uint32_t line_end;
			gear_string *doc_comment;
		} user;
		struct {
			const struct _gear_function_entry *builtin_functions;
			struct _gear_capi_entry *cAPI;
		} internal;
	} info;
};

typedef struct _gear_utility_functions {
	void (*error_function)(int type, const char *error_filename, const uint32_t error_lineno, const char *format, va_list args) GEAR_ATTRIBUTE_PTR_FORMAT(printf, 4, 0);
	size_t (*printf_function)(const char *format, ...) GEAR_ATTRIBUTE_PTR_FORMAT(printf, 1, 2);
	size_t (*write_function)(const char *str, size_t str_length);
	FILE *(*fopen_function)(const char *filename, gear_string **opened_path);
	void (*message_handler)(gear_long message, const void *data);
	zval *(*get_configuration_directive)(gear_string *name);
	void (*ticks_function)(int ticks);
	void (*on_timeout)(int seconds);
	int (*stream_open_function)(const char *filename, gear_file_handle *handle);
	void (*printf_to_smart_string_function)(smart_string *buf, const char *format, va_list ap);
	void (*printf_to_smart_str_function)(smart_str *buf, const char *format, va_list ap);
	char *(*getenv_function)(char *name, size_t name_len);
	gear_string *(*resolve_path_function)(const char *filename, size_t filename_len);
} gear_utility_functions;

typedef struct _gear_utility_values {
	char *import_use_extension;
	uint32_t import_use_extension_length;
	gear_bool html_errors;
} gear_utility_values;

typedef int (*gear_write_func_t)(const char *str, size_t str_length);

#define gear_bailout()		_gear_bailout(__FILE__, __LINE__)

#define gear_try												\
	{															\
		JMP_BUF *__orig_bailout = EG(bailout);					\
		JMP_BUF __bailout;										\
																\
		EG(bailout) = &__bailout;								\
		if (SETJMP(__bailout)==0) {
#define gear_catch												\
		} else {												\
			EG(bailout) = __orig_bailout;
#define gear_end_try()											\
		}														\
		EG(bailout) = __orig_bailout;							\
	}
#define gear_first_try		EG(bailout)=NULL;	gear_try

BEGIN_EXTERN_C()
int gear_startup(gear_utility_functions *utility_functions, char **extensions);
void gear_shutdown(void);
void gear_register_standard_ics_entries(void);
int gear_post_startup(void);
void gear_set_utility_values(gear_utility_values *utility_values);

GEAR_API GEAR_COLD void _gear_bailout(const char *filename, uint32_t lineno);

GEAR_API size_t gear_vspprintf(char **pbuf, size_t max_len, const char *format, va_list ap);
GEAR_API size_t gear_spprintf(char **message, size_t max_len, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 3, 4);
GEAR_API gear_string *gear_vstrpprintf(size_t max_len, const char *format, va_list ap);
GEAR_API gear_string *gear_strpprintf(size_t max_len, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);

/* Same as gear_spprintf and gear_strpprintf, without checking of format validity.
 * For use with custom printf specifiers such as %H. */
GEAR_API size_t gear_spprintf_unchecked(char **message, size_t max_len, const char *format, ...);
GEAR_API gear_string *gear_strpprintf_unchecked(size_t max_len, const char *format, ...);

GEAR_API char *get_gear_version(void);
GEAR_API int gear_make_printable_zval(zval *expr, zval *expr_copy);
GEAR_API size_t gear_print_zval(zval *expr, int indent);
GEAR_API void gear_print_zval_r(zval *expr, int indent);
GEAR_API gear_string *gear_print_zval_r_to_str(zval *expr, int indent);
GEAR_API void gear_print_flat_zval_r(zval *expr);

#define gear_print_variable(var) \
	gear_print_zval((var), 0)

GEAR_API GEAR_COLD void gear_output_debug_string(gear_bool trigger_break, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);

GEAR_API void gear_activate(void);
GEAR_API void gear_deactivate(void);
GEAR_API void gear_call_destructors(void);
GEAR_API void gear_activate_capis(void);
GEAR_API void gear_deactivate_capis(void);
GEAR_API void gear_post_deactivate_capis(void);

GEAR_API void free_estring(char **str_p);
END_EXTERN_C()

/* output support */
#define GEAR_WRITE(str, str_len)		gear_write((str), (str_len))
#define GEAR_WRITE_EX(str, str_len)		write_func((str), (str_len))
#define GEAR_PUTS(str)					gear_write((str), strlen((str)))
#define GEAR_PUTS_EX(str)				write_func((str), strlen((str)))
#define GEAR_PUTC(c)					gear_write(&(c), 1)

BEGIN_EXTERN_C()
extern GEAR_API size_t (*gear_printf)(const char *format, ...) GEAR_ATTRIBUTE_PTR_FORMAT(printf, 1, 2);
extern GEAR_API gear_write_func_t gear_write;
extern GEAR_API FILE *(*gear_fopen)(const char *filename, gear_string **opened_path);
extern GEAR_API void (*gear_ticks_function)(int ticks);
extern GEAR_API void (*gear_interrupt_function)(gear_execute_data *execute_data);
extern GEAR_API void (*gear_error_cb)(int type, const char *error_filename, const uint32_t error_lineno, const char *format, va_list args) GEAR_ATTRIBUTE_PTR_FORMAT(printf, 4, 0);
extern GEAR_API void (*gear_on_timeout)(int seconds);
extern GEAR_API int (*gear_stream_open_function)(const char *filename, gear_file_handle *handle);
extern void (*gear_printf_to_smart_string)(smart_string *buf, const char *format, va_list ap);
extern void (*gear_printf_to_smart_str)(smart_str *buf, const char *format, va_list ap);
extern GEAR_API char *(*gear_getenv)(char *name, size_t name_len);
extern GEAR_API gear_string *(*gear_resolve_path)(const char *filename, size_t filename_len);
extern GEAR_API int (*gear_post_startup_cb)(void);

GEAR_API GEAR_COLD void gear_error(int type, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);
GEAR_API GEAR_COLD void gear_throw_error(gear_class_entry *exception_ce, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);
GEAR_API GEAR_COLD void gear_type_error(const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 1, 2);
GEAR_API GEAR_COLD void gear_internal_type_error(gear_bool throw_exception, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);
GEAR_API GEAR_COLD void gear_internal_argument_count_error(gear_bool throw_exception, const char *format, ...) GEAR_ATTRIBUTE_FORMAT(printf, 2, 3);

GEAR_COLD void gearerror(const char *error);

/* The following #define is used for code duality in HYSS for Engine 1 & 2 */
#define GEAR_STANDARD_CLASS_DEF_PTR gear_standard_class_def
extern GEAR_API gear_class_entry *gear_standard_class_def;
extern GEAR_API gear_utility_values gear_uv;

/* If DTrace is available and enabled */
extern GEAR_API gear_bool gear_dtrace_enabled;
END_EXTERN_C()

#define GEAR_UV(name) (gear_uv.name)

BEGIN_EXTERN_C()
GEAR_API void gear_message_dispatcher(gear_long message, const void *data);

GEAR_API zval *gear_get_configuration_directive(gear_string *name);
END_EXTERN_C()

/* Messages for applications of Gear */
#define ZMSG_FAILED_INCLUDE_FOPEN		1L
#define ZMSG_FAILED_REQUIRE_FOPEN		2L
#define ZMSG_FAILED_HIGHLIGHT_FOPEN		3L
#define ZMSG_MEMORY_LEAK_DETECTED		4L
#define ZMSG_MEMORY_LEAK_REPEATED		5L
#define ZMSG_LOG_SCRIPT_NAME			6L
#define ZMSG_MEMORY_LEAKS_GRAND_TOTAL	7L

typedef enum {
	EH_NORMAL = 0,
	EH_THROW
} gear_error_handling_t;

typedef struct {
	gear_error_handling_t  handling;
	gear_class_entry       *exception;
	zval                   user_handler;
} gear_error_handling;

GEAR_API void gear_save_error_handling(gear_error_handling *current);
GEAR_API void gear_replace_error_handling(gear_error_handling_t error_handling, gear_class_entry *exception_class, gear_error_handling *current);
GEAR_API void gear_restore_error_handling(gear_error_handling *saved);

#define DEBUG_BACKTRACE_PROVIDE_OBJECT (1<<0)
#define DEBUG_BACKTRACE_IGNORE_ARGS    (1<<1)

#include "gear_object_handlers.h"
#include "gear_operators.h"

#endif /* GEAR_H */

