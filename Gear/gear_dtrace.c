/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"
#include "gear_dtrace.h"

#ifdef HAVE_DTRACE

GEAR_API gear_op_array *(*gear_dtrace_compile_file)(gear_file_handle *file_handle, int type);
GEAR_API void (*gear_dtrace_execute)(gear_op_array *op_array);
GEAR_API void (*gear_dtrace_execute_internal)(gear_execute_data *execute_data, zval *return_value);

/* HYSS DTrace probes {{{ */
static inline const char *dtrace_get_executed_filename(void)
{
	gear_execute_data *ex = EG(current_execute_data);

	while (ex && (!ex->func || !GEAR_USER_CODE(ex->func->type))) {
		ex = ex->prev_execute_data;
	}
	if (ex) {
		return ZSTR_VAL(ex->func->op_array.filename);
	} else {
		return gear_get_executed_filename();
	}
}

GEAR_API gear_op_array *dtrace_compile_file(gear_file_handle *file_handle, int type)
{
	gear_op_array *res;
	DTRACE_COMPILE_FILE_ENTRY(ZSTR_VAL(file_handle->opened_path), (char *)file_handle->filename);
	res = compile_file(file_handle, type);
	DTRACE_COMPILE_FILE_RETURN(ZSTR_VAL(file_handle->opened_path), (char *)file_handle->filename);

	return res;
}

/* We wrap the execute function to have fire the execute-entry/return and function-entry/return probes */
GEAR_API void dtrace_execute_ex(gear_execute_data *execute_data)
{
	int lineno;
	const char *scope, *filename, *funcname, *classname;
	scope = filename = funcname = classname = NULL;

	/* we need filename and lineno for both execute and function probes */
	if (DTRACE_EXECUTE_ENTRY_ENABLED() || DTRACE_EXECUTE_RETURN_ENABLED()
		|| DTRACE_FUNCTION_ENTRY_ENABLED() || DTRACE_FUNCTION_RETURN_ENABLED()) {
		filename = dtrace_get_executed_filename();
		lineno = gear_get_executed_lineno();
	}

	if (DTRACE_FUNCTION_ENTRY_ENABLED() || DTRACE_FUNCTION_RETURN_ENABLED()) {
		classname = get_active_class_name(&scope);
		funcname = get_active_function_name();
	}

	if (DTRACE_EXECUTE_ENTRY_ENABLED()) {
		DTRACE_EXECUTE_ENTRY((char *)filename, lineno);
	}

	if (DTRACE_FUNCTION_ENTRY_ENABLED() && funcname != NULL) {
		DTRACE_FUNCTION_ENTRY((char *)funcname, (char *)filename, lineno, (char *)classname, (char *)scope);
	}

	execute_ex(execute_data);

	if (DTRACE_FUNCTION_RETURN_ENABLED() && funcname != NULL) {
		DTRACE_FUNCTION_RETURN((char *)funcname, (char *)filename, lineno, (char *)classname, (char *)scope);
	}

	if (DTRACE_EXECUTE_RETURN_ENABLED()) {
		DTRACE_EXECUTE_RETURN((char *)filename, lineno);
	}
}

GEAR_API void dtrace_execute_internal(gear_execute_data *execute_data, zval *return_value)
{
	int lineno;
	const char *filename;
	if (DTRACE_EXECUTE_ENTRY_ENABLED() || DTRACE_EXECUTE_RETURN_ENABLED()) {
		filename = dtrace_get_executed_filename();
		lineno = gear_get_executed_lineno();
	}

	if (DTRACE_EXECUTE_ENTRY_ENABLED()) {
		DTRACE_EXECUTE_ENTRY((char *)filename, lineno);
	}

	execute_internal(execute_data, return_value);

	if (DTRACE_EXECUTE_RETURN_ENABLED()) {
		DTRACE_EXECUTE_RETURN((char *)filename, lineno);
	}
}

/* }}} */

#endif /* HAVE_DTRACE */

