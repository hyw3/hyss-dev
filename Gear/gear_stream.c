/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_compile.h"
#include "gear_stream.h"

#if HAVE_MMAP
# if HAVE_UNISTD_H
#  include <unistd.h>
#  if defined(_SC_PAGESIZE)
#    define REAL_PAGE_SIZE sysconf(_SC_PAGESIZE);
#  elif defined(_SC_PAGE_SIZE)
#    define REAL_PAGE_SIZE sysconf(_SC_PAGE_SIZE);
#  endif
# endif
# if HAVE_SYS_MMAN_H
#  include <sys/mman.h>
# endif
# ifndef REAL_PAGE_SIZE
#  ifdef PAGE_SIZE
#   define REAL_PAGE_SIZE PAGE_SIZE
#  else
#   define REAL_PAGE_SIZE 4096
#  endif
# endif
#endif

GEAR_DLIMPORT int isatty(int fd);

static size_t gear_stream_stdio_reader(void *handle, char *buf, size_t len) /* {{{ */
{
	return fread(buf, 1, len, (FILE*)handle);
} /* }}} */

static void gear_stream_stdio_closer(void *handle) /* {{{ */
{
	if (handle && (FILE*)handle != stdin) {
		fclose((FILE*)handle);
	}
} /* }}} */

static size_t gear_stream_stdio_fsizer(void *handle) /* {{{ */
{
	gear_stat_t buf;
	if (handle && gear_fstat(fileno((FILE*)handle), &buf) == 0) {
#ifdef S_ISREG
		if (!S_ISREG(buf.st_mode)) {
			return 0;
		}
#endif
		return buf.st_size;
	}
	return 0;
} /* }}} */

static void gear_stream_unmap(gear_stream *stream) { /* {{{ */
#if HAVE_MMAP
	if (stream->mmap.map) {
		munmap(stream->mmap.map, stream->mmap.len + GEAR_MMAP_AHEAD);
	} else
#endif
	if (stream->mmap.buf) {
		efree(stream->mmap.buf);
	}
	stream->mmap.len = 0;
	stream->mmap.pos = 0;
	stream->mmap.map = 0;
	stream->mmap.buf = 0;
	stream->handle   = stream->mmap.old_handle;
} /* }}} */

static void gear_stream_mmap_closer(gear_stream *stream) /* {{{ */
{
	gear_stream_unmap(stream);
	if (stream->mmap.old_closer && stream->handle) {
		stream->mmap.old_closer(stream->handle);
	}
} /* }}} */

static inline int gear_stream_is_mmap(gear_file_handle *file_handle) { /* {{{ */
	return file_handle->type == GEAR_HANDLE_MAPPED;
} /* }}} */

static size_t gear_stream_fsize(gear_file_handle *file_handle) /* {{{ */
{
	gear_stat_t buf;

	if (gear_stream_is_mmap(file_handle)) {
		return file_handle->handle.stream.mmap.len;
	}
	if (file_handle->type == GEAR_HANDLE_STREAM || file_handle->type == GEAR_HANDLE_MAPPED) {
		return file_handle->handle.stream.fsizer(file_handle->handle.stream.handle);
	}
	if (file_handle->handle.fp && gear_fstat(fileno(file_handle->handle.fp), &buf) == 0) {
#ifdef S_ISREG
		if (!S_ISREG(buf.st_mode)) {
			return 0;
		}
#endif
		return buf.st_size;
	}

	return -1;
} /* }}} */

GEAR_API int gear_stream_open(const char *filename, gear_file_handle *handle) /* {{{ */
{
	if (gear_stream_open_function) {
		return gear_stream_open_function(filename, handle);
	}
	handle->type = GEAR_HANDLE_FP;
	handle->opened_path = NULL;
	handle->handle.fp = gear_fopen(filename, &handle->opened_path);
	handle->filename = filename;
	handle->free_filename = 0;
	memset(&handle->handle.stream.mmap, 0, sizeof(gear_mmap));

	return (handle->handle.fp) ? SUCCESS : FAILURE;
} /* }}} */

static int gear_stream_getc(gear_file_handle *file_handle) /* {{{ */
{
	char buf;

	if (file_handle->handle.stream.reader(file_handle->handle.stream.handle, &buf, sizeof(buf))) {
		return (int)buf;
	}
	return EOF;
} /* }}} */

static size_t gear_stream_read(gear_file_handle *file_handle, char *buf, size_t len) /* {{{ */
{
	if (!gear_stream_is_mmap(file_handle) && file_handle->handle.stream.isatty) {
		int c = '*';
		size_t n;

		for (n = 0; n < len && (c = gear_stream_getc(file_handle)) != EOF && c != '\n'; ++n)  {
			buf[n] = (char)c;
		}
		if (c == '\n') {
			buf[n++] = (char)c;
		}

		return n;
	}
	return file_handle->handle.stream.reader(file_handle->handle.stream.handle, buf, len);
} /* }}} */

GEAR_API int gear_stream_fixup(gear_file_handle *file_handle, char **buf, size_t *len) /* {{{ */
{
	size_t size;
	gear_stream_type old_type;

	if (file_handle->type == GEAR_HANDLE_FILENAME) {
		if (gear_stream_open(file_handle->filename, file_handle) == FAILURE) {
			return FAILURE;
		}
	}

	switch (file_handle->type) {
		case GEAR_HANDLE_FD:
			file_handle->type = GEAR_HANDLE_FP;
			file_handle->handle.fp = fdopen(file_handle->handle.fd, "rb");
			/* no break; */
		case GEAR_HANDLE_FP:
			if (!file_handle->handle.fp) {
				return FAILURE;
			}
			memset(&file_handle->handle.stream.mmap, 0, sizeof(gear_mmap));
			file_handle->handle.stream.isatty     = isatty(fileno((FILE *)file_handle->handle.stream.handle)) ? 1 : 0;
			file_handle->handle.stream.reader     = (gear_stream_reader_t)gear_stream_stdio_reader;
			file_handle->handle.stream.closer     = (gear_stream_closer_t)gear_stream_stdio_closer;
			file_handle->handle.stream.fsizer     = (gear_stream_fsizer_t)gear_stream_stdio_fsizer;
			memset(&file_handle->handle.stream.mmap, 0, sizeof(file_handle->handle.stream.mmap));
			/* no break; */
		case GEAR_HANDLE_STREAM:
			/* nothing to do */
			break;

		case GEAR_HANDLE_MAPPED:
			file_handle->handle.stream.mmap.pos = 0;
			*buf = file_handle->handle.stream.mmap.buf;
			*len = file_handle->handle.stream.mmap.len;
			return SUCCESS;

		default:
			return FAILURE;
	}

	size = gear_stream_fsize(file_handle);
	if (size == (size_t)-1) {
		return FAILURE;
	}

	old_type = file_handle->type;
	file_handle->type = GEAR_HANDLE_STREAM;  /* we might still be _FP but we need fsize() work */

	if (old_type == GEAR_HANDLE_FP && !file_handle->handle.stream.isatty && size) {
#if HAVE_MMAP
		size_t page_size = REAL_PAGE_SIZE;

		if (file_handle->handle.fp &&
		    size != 0 &&
		    ((size - 1) % page_size) <= page_size - GEAR_MMAP_AHEAD) {
			/*  *buf[size] is zeroed automatically by the kernel */
			*buf = mmap(0, size + GEAR_MMAP_AHEAD, PROT_READ, MAP_PRIVATE, fileno(file_handle->handle.fp), 0);
			if (*buf != MAP_FAILED) {
				gear_long offset = ftell(file_handle->handle.fp);
				file_handle->handle.stream.mmap.map = *buf;

				if (offset != -1) {
					*buf += offset;
					size -= offset;
				}
				file_handle->handle.stream.mmap.buf = *buf;
				file_handle->handle.stream.mmap.len = size;

				goto return_mapped;
			}
		}
#endif
		file_handle->handle.stream.mmap.map = 0;
		file_handle->handle.stream.mmap.buf = *buf = safe_emalloc(1, size, GEAR_MMAP_AHEAD);
		file_handle->handle.stream.mmap.len = gear_stream_read(file_handle, *buf, size);
	} else {
		size_t read, remain = 4*1024;
		*buf = emalloc(remain);
		size = 0;

		while ((read = gear_stream_read(file_handle, *buf + size, remain)) > 0) {
			size   += read;
			remain -= read;

			if (remain == 0) {
				*buf   = safe_erealloc(*buf, size, 2, 0);
				remain = size;
			}
		}
		file_handle->handle.stream.mmap.map = 0;
		file_handle->handle.stream.mmap.len = size;
		if (size && remain < GEAR_MMAP_AHEAD) {
			*buf = safe_erealloc(*buf, size, 1, GEAR_MMAP_AHEAD);
		}
		file_handle->handle.stream.mmap.buf = *buf;
	}

	if (file_handle->handle.stream.mmap.len == 0) {
		*buf = erealloc(*buf, GEAR_MMAP_AHEAD);
		file_handle->handle.stream.mmap.buf = *buf;
	}

	if (GEAR_MMAP_AHEAD) {
		memset(file_handle->handle.stream.mmap.buf + file_handle->handle.stream.mmap.len, 0, GEAR_MMAP_AHEAD);
	}
#if HAVE_MMAP
return_mapped:
#endif
	file_handle->type = GEAR_HANDLE_MAPPED;
	file_handle->handle.stream.mmap.pos        = 0;
	file_handle->handle.stream.mmap.old_handle = file_handle->handle.stream.handle;
	file_handle->handle.stream.mmap.old_closer = file_handle->handle.stream.closer;
	file_handle->handle.stream.handle          = &file_handle->handle.stream;
	file_handle->handle.stream.closer          = (gear_stream_closer_t)gear_stream_mmap_closer;

	*buf = file_handle->handle.stream.mmap.buf;
	*len = file_handle->handle.stream.mmap.len;

	return SUCCESS;
} /* }}} */

GEAR_API void gear_file_handle_dtor(gear_file_handle *fh) /* {{{ */
{
	switch (fh->type) {
		case GEAR_HANDLE_FD:
			/* nothing to do */
			break;
		case GEAR_HANDLE_FP:
			fclose(fh->handle.fp);
			break;
		case GEAR_HANDLE_STREAM:
		case GEAR_HANDLE_MAPPED:
			if (fh->handle.stream.closer && fh->handle.stream.handle) {
				fh->handle.stream.closer(fh->handle.stream.handle);
			}
			fh->handle.stream.handle = NULL;
			break;
		case GEAR_HANDLE_FILENAME:
			/* We're only supposed to get here when destructing the used_files hash,
			 * which doesn't really contain open files, but references to their names/paths
			 */
			break;
	}
	if (fh->opened_path) {
		gear_string_release_ex(fh->opened_path, 0);
		fh->opened_path = NULL;
	}
	if (fh->free_filename && fh->filename) {
		efree((char*)fh->filename);
		fh->filename = NULL;
	}
}
/* }}} */

GEAR_API int gear_compare_file_handles(gear_file_handle *fh1, gear_file_handle *fh2) /* {{{ */
{
	if (fh1->type != fh2->type) {
		return 0;
	}
	switch (fh1->type) {
		case GEAR_HANDLE_FD:
			return fh1->handle.fd == fh2->handle.fd;
		case GEAR_HANDLE_FP:
			return fh1->handle.fp == fh2->handle.fp;
		case GEAR_HANDLE_STREAM:
			return fh1->handle.stream.handle == fh2->handle.stream.handle;
		case GEAR_HANDLE_MAPPED:
			return (fh1->handle.stream.handle == &fh1->handle.stream &&
			        fh2->handle.stream.handle == &fh2->handle.stream &&
			        fh1->handle.stream.mmap.old_handle == fh2->handle.stream.mmap.old_handle)
				|| fh1->handle.stream.handle == fh2->handle.stream.handle;
		default:
			return 0;
	}
	return 0;
} /* }}} */

