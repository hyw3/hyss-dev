/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"
#include "gear_gc.h"
#include "gear_builtin_functions.h"
#include "gear_constants.h"
#include "gear_ics.h"
#include "gear_exceptions.h"
#include "gear_extensions.h"
#include "gear_closures.h"
#include "gear_generators.h"

static GEAR_FUNCTION(gear_version);
static GEAR_FUNCTION(func_num_args);
static GEAR_FUNCTION(func_get_arg);
static GEAR_FUNCTION(func_get_args);
static GEAR_FUNCTION(strlen);
static GEAR_FUNCTION(strcmp);
static GEAR_FUNCTION(strncmp);
static GEAR_FUNCTION(strcasecmp);
static GEAR_FUNCTION(strncasecmp);
static GEAR_FUNCTION(each);
static GEAR_FUNCTION(error_reporting);
static GEAR_FUNCTION(define);
static GEAR_FUNCTION(defined);
static GEAR_FUNCTION(get_class);
static GEAR_FUNCTION(get_called_class);
static GEAR_FUNCTION(get_parent_class);
static GEAR_FUNCTION(method_exists);
static GEAR_FUNCTION(property_exists);
static GEAR_FUNCTION(class_exists);
static GEAR_FUNCTION(interface_exists);
static GEAR_FUNCTION(trait_exists);
static GEAR_FUNCTION(function_exists);
static GEAR_FUNCTION(class_alias);
static GEAR_FUNCTION(get_included_files);
static GEAR_FUNCTION(is_subclass_of);
static GEAR_FUNCTION(is_a);
static GEAR_FUNCTION(get_class_vars);
static GEAR_FUNCTION(get_object_vars);
static GEAR_FUNCTION(get_class_methods);
static GEAR_FUNCTION(trigger_error);
static GEAR_FUNCTION(set_error_handler);
static GEAR_FUNCTION(restore_error_handler);
static GEAR_FUNCTION(set_exception_handler);
static GEAR_FUNCTION(restore_exception_handler);
static GEAR_FUNCTION(get_declared_classes);
static GEAR_FUNCTION(get_declared_traits);
static GEAR_FUNCTION(get_declared_interfaces);
static GEAR_FUNCTION(get_defined_functions);
static GEAR_FUNCTION(get_defined_vars);
static GEAR_FUNCTION(create_function);
static GEAR_FUNCTION(get_resource_type);
static GEAR_FUNCTION(get_resources);
static GEAR_FUNCTION(get_loaded_extensions);
static GEAR_FUNCTION(extension_loaded);
static GEAR_FUNCTION(get_extension_funcs);
static GEAR_FUNCTION(get_defined_constants);
static GEAR_FUNCTION(debug_backtrace);
static GEAR_FUNCTION(debug_print_backtrace);
#if GEAR_DEBUG && defined(ZTS)
static GEAR_FUNCTION(gear_thread_id);
#endif
static GEAR_FUNCTION(gc_mem_caches);
static GEAR_FUNCTION(gc_collect_cycles);
static GEAR_FUNCTION(gc_enabled);
static GEAR_FUNCTION(gc_enable);
static GEAR_FUNCTION(gc_disable);
static GEAR_FUNCTION(gc_status);

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO(arginfo_gear__void, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_func_get_arg, 0, 0, 1)
	GEAR_ARG_INFO(0, arg_num)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strlen, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strcmp, 0, 0, 2)
	GEAR_ARG_INFO(0, str1)
	GEAR_ARG_INFO(0, str2)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strncmp, 0, 0, 3)
	GEAR_ARG_INFO(0, str1)
	GEAR_ARG_INFO(0, str2)
	GEAR_ARG_INFO(0, len)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_each, 0, 0, 1)
	GEAR_ARG_INFO(1, arr)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_error_reporting, 0, 0, 0)
	GEAR_ARG_INFO(0, new_error_level)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_define, 0, 0, 2)
	GEAR_ARG_INFO(0, constant_name)
	GEAR_ARG_INFO(0, value)
	GEAR_ARG_INFO(0, case_insensitive)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_defined, 0, 0, 1)
	GEAR_ARG_INFO(0, constant_name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_class, 0, 0, 0)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_is_subclass_of, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, class_name)
	GEAR_ARG_INFO(0, allow_string)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_class_vars, 0, 0, 1)
	GEAR_ARG_INFO(0, class_name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_object_vars, 0, 0, 1)
	GEAR_ARG_INFO(0, obj)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_class_methods, 0, 0, 1)
	GEAR_ARG_INFO(0, class)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_method_exists, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, method)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_property_exists, 0, 0, 2)
	GEAR_ARG_INFO(0, object_or_class)
	GEAR_ARG_INFO(0, property_name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_exists, 0, 0, 1)
	GEAR_ARG_INFO(0, classname)
	GEAR_ARG_INFO(0, autoload)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_trait_exists, 0, 0, 1)
	GEAR_ARG_INFO(0, traitname)
	GEAR_ARG_INFO(0, autoload)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_function_exists, 0, 0, 1)
	GEAR_ARG_INFO(0, function_name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_alias, 0, 0, 2)
	GEAR_ARG_INFO(0, user_class_name)
	GEAR_ARG_INFO(0, alias_name)
	GEAR_ARG_INFO(0, autoload)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_trigger_error, 0, 0, 1)
	GEAR_ARG_INFO(0, message)
	GEAR_ARG_INFO(0, error_type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_set_error_handler, 0, 0, 1)
	GEAR_ARG_INFO(0, error_handler)
	GEAR_ARG_INFO(0, error_types)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_set_exception_handler, 0, 0, 1)
	GEAR_ARG_INFO(0, exception_handler)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_defined_functions, 0, 0, 0)
	GEAR_ARG_INFO(0, exclude_disabled)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_create_function, 0, 0, 2)
	GEAR_ARG_INFO(0, args)
	GEAR_ARG_INFO(0, code)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_resource_type, 0, 0, 1)
	GEAR_ARG_INFO(0, res)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_resources, 0, 0, 0)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_loaded_extensions, 0, 0, 0)
	GEAR_ARG_INFO(0, gear_extensions)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_defined_constants, 0, 0, 0)
	GEAR_ARG_INFO(0, categorize)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_debug_backtrace, 0, 0, 0)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, limit)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_debug_print_backtrace, 0, 0, 0)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, limit)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_extension_loaded, 0, 0, 1)
	GEAR_ARG_INFO(0, extension_name)
GEAR_END_ARG_INFO()

/* }}} */

static const gear_function_entry builtin_functions[] = { /* {{{ */
	GEAR_FE(gear_version,		arginfo_gear__void)
	GEAR_FE(func_num_args,		arginfo_gear__void)
	GEAR_FE(func_get_arg,		arginfo_func_get_arg)
	GEAR_FE(func_get_args,		arginfo_gear__void)
	GEAR_FE(strlen,			arginfo_strlen)
	GEAR_FE(strcmp,			arginfo_strcmp)
	GEAR_FE(strncmp,		arginfo_strncmp)
	GEAR_FE(strcasecmp,		arginfo_strcmp)
	GEAR_FE(strncasecmp,		arginfo_strncmp)
	GEAR_FE(each,			arginfo_each)
	GEAR_FE(error_reporting,	arginfo_error_reporting)
	GEAR_FE(define,			arginfo_define)
	GEAR_FE(defined,		arginfo_defined)
	GEAR_FE(get_class,		arginfo_get_class)
	GEAR_FE(get_called_class,	arginfo_gear__void)
	GEAR_FE(get_parent_class,	arginfo_get_class)
	GEAR_FE(method_exists,		arginfo_method_exists)
	GEAR_FE(property_exists,	arginfo_property_exists)
	GEAR_FE(class_exists,		arginfo_class_exists)
	GEAR_FE(interface_exists,	arginfo_class_exists)
	GEAR_FE(trait_exists,		arginfo_trait_exists)
	GEAR_FE(function_exists,	arginfo_function_exists)
	GEAR_FE(class_alias,		arginfo_class_alias)
	GEAR_FE(get_included_files,	arginfo_gear__void)
	GEAR_FALIAS(get_required_files,	get_included_files,		arginfo_gear__void)
	GEAR_FE(is_subclass_of,		arginfo_is_subclass_of)
	GEAR_FE(is_a,			arginfo_is_subclass_of)
	GEAR_FE(get_class_vars,		arginfo_get_class_vars)
	GEAR_FE(get_object_vars,	arginfo_get_object_vars)
	GEAR_FE(get_class_methods,	arginfo_get_class_methods)
	GEAR_FE(trigger_error,		arginfo_trigger_error)
	GEAR_FALIAS(user_error,		trigger_error,		arginfo_trigger_error)
	GEAR_FE(set_error_handler,		arginfo_set_error_handler)
	GEAR_FE(restore_error_handler,		arginfo_gear__void)
	GEAR_FE(set_exception_handler,		arginfo_set_exception_handler)
	GEAR_FE(restore_exception_handler,	arginfo_gear__void)
	GEAR_FE(get_declared_classes, 		arginfo_gear__void)
	GEAR_FE(get_declared_traits, 		arginfo_gear__void)
	GEAR_FE(get_declared_interfaces, 	arginfo_gear__void)
	GEAR_FE(get_defined_functions, 		arginfo_get_defined_functions)
	GEAR_FE(get_defined_vars,		arginfo_gear__void)
	GEAR_DEP_FE(create_function,		arginfo_create_function)
	GEAR_FE(get_resource_type,		arginfo_get_resource_type)
	GEAR_FE(get_resources,			arginfo_get_resources)
	GEAR_FE(get_loaded_extensions,		arginfo_get_loaded_extensions)
	GEAR_FE(extension_loaded,		arginfo_extension_loaded)
	GEAR_FE(get_extension_funcs,		arginfo_extension_loaded)
	GEAR_FE(get_defined_constants,		arginfo_get_defined_constants)
	GEAR_FE(debug_backtrace, 		arginfo_debug_backtrace)
	GEAR_FE(debug_print_backtrace, 		arginfo_debug_print_backtrace)
#if GEAR_DEBUG && defined(ZTS)
	GEAR_FE(gear_thread_id,		NULL)
#endif
	GEAR_FE(gc_mem_caches,      arginfo_gear__void)
	GEAR_FE(gc_collect_cycles, 	arginfo_gear__void)
	GEAR_FE(gc_enabled, 		arginfo_gear__void)
	GEAR_FE(gc_enable, 		arginfo_gear__void)
	GEAR_FE(gc_disable, 		arginfo_gear__void)
	GEAR_FE(gc_status, 		arginfo_gear__void)
	GEAR_FE_END
};
/* }}} */

GEAR_MINIT_FUNCTION(core) { /* {{{ */
	gear_class_entry class_entry;

	INIT_CLASS_ENTRY(class_entry, "stdClass", NULL);
	gear_standard_class_def = gear_register_internal_class(&class_entry);

	gear_register_default_classes();

	return SUCCESS;
}
/* }}} */

gear_capi_entry gear_builtin_capi = { /* {{{ */
    STANDARD_CAPI_HEADER,
	"Core",
	builtin_functions,
	GEAR_MINIT(core),
	NULL,
	NULL,
	NULL,
	NULL,
	GEAR_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

int gear_startup_builtin_functions(void) /* {{{ */
{
	gear_builtin_capi.capi_number = 0;
	gear_builtin_capi.type = CAPI_PERSISTENT;
	return (EG(current_capi) = gear_register_capi_ex(&gear_builtin_capi)) == NULL ? FAILURE : SUCCESS;
}
/* }}} */

/* {{{ proto string gear_version(void)
   Get the version of the Gear Engine */
GEAR_FUNCTION(gear_version)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_STRINGL(GEAR_VERSION, sizeof(GEAR_VERSION)-1);
}
/* }}} */

/* {{{ proto int gc_mem_caches(void)
   Reclaims memory used by MM caches.
   Returns number of freed bytes */
GEAR_FUNCTION(gc_mem_caches)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_LONG(gear_mm_gc(gear_mm_get_heap()));
}
/* }}} */

/* {{{ proto int gc_collect_cycles(void)
   Forces collection of any existing garbage cycles.
   Returns number of freed zvals */
GEAR_FUNCTION(gc_collect_cycles)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_LONG(gc_collect_cycles());
}
/* }}} */

/* {{{ proto void gc_enabled(void)
   Returns status of the circular reference collector */
GEAR_FUNCTION(gc_enabled)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_BOOL(gc_enabled());
}
/* }}} */

/* {{{ proto void gc_enable(void)
   Activates the circular reference collector */
GEAR_FUNCTION(gc_enable)
{
	gear_string *key;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	key = gear_string_init("gear.enable_gc", sizeof("gear.enable_gc")-1, 0);
	gear_alter_ics_entry_chars(key, "1", sizeof("1")-1, GEAR_ICS_USER, GEAR_ICS_STAGE_RUNTIME);
	gear_string_release_ex(key, 0);
}
/* }}} */

/* {{{ proto void gc_disable(void)
   Deactivates the circular reference collector */
GEAR_FUNCTION(gc_disable)
{
	gear_string *key;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	key = gear_string_init("gear.enable_gc", sizeof("gear.enable_gc")-1, 0);
	gear_alter_ics_entry_chars(key, "0", sizeof("0")-1, GEAR_ICS_USER, GEAR_ICS_STAGE_RUNTIME);
	gear_string_release_ex(key, 0);
}
/* }}} */

/* {{{ proto array gc_status(void)
   Returns current GC statistics */
GEAR_FUNCTION(gc_status)
{
	gear_gc_status status;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	gear_gc_get_status(&status);

	array_init_size(return_value, 3);

	add_assoc_long_ex(return_value, "runs", sizeof("runs")-1, (long)status.runs);
	add_assoc_long_ex(return_value, "collected", sizeof("collected")-1, (long)status.collected);
	add_assoc_long_ex(return_value, "threshold", sizeof("threshold")-1, (long)status.threshold);
	add_assoc_long_ex(return_value, "roots", sizeof("roots")-1, (long)status.num_roots);
}
/* }}} */

/* {{{ proto int func_num_args(void)
   Get the number of arguments that were passed to the function */
GEAR_FUNCTION(func_num_args)
{
	gear_execute_data *ex = EX(prev_execute_data);

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (GEAR_CALL_INFO(ex) & GEAR_CALL_CODE) {
		gear_error(E_WARNING, "func_num_args():  Called from the global scope - no function context");
		RETURN_LONG(-1);
	}

	if (gear_forbid_dynamic_call("func_num_args()") == FAILURE) {
		RETURN_LONG(-1);
	}

	RETURN_LONG(GEAR_CALL_NUM_ARGS(ex));
}
/* }}} */

/* {{{ proto mixed func_get_arg(int arg_num)
   Get the $arg_num'th argument that was passed to the function */
GEAR_FUNCTION(func_get_arg)
{
	uint32_t arg_count, first_extra_arg;
	zval *arg;
	gear_long requested_offset;
	gear_execute_data *ex;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &requested_offset) == FAILURE) {
		return;
	}

	if (requested_offset < 0) {
		gear_error(E_WARNING, "func_get_arg():  The argument number should be >= 0");
		RETURN_FALSE;
	}

	ex = EX(prev_execute_data);
	if (GEAR_CALL_INFO(ex) & GEAR_CALL_CODE) {
		gear_error(E_WARNING, "func_get_arg():  Called from the global scope - no function context");
		RETURN_FALSE;
	}

	if (gear_forbid_dynamic_call("func_get_arg()") == FAILURE) {
		RETURN_FALSE;
	}

	arg_count = GEAR_CALL_NUM_ARGS(ex);

	if ((gear_ulong)requested_offset >= arg_count) {
		gear_error(E_WARNING, "func_get_arg():  Argument " GEAR_LONG_FMT " not passed to function", requested_offset);
		RETURN_FALSE;
	}

	first_extra_arg = ex->func->op_array.num_args;
	if ((gear_ulong)requested_offset >= first_extra_arg && (GEAR_CALL_NUM_ARGS(ex) > first_extra_arg)) {
		arg = GEAR_CALL_VAR_NUM(ex, ex->func->op_array.last_var + ex->func->op_array.T) + (requested_offset - first_extra_arg);
	} else {
		arg = GEAR_CALL_ARG(ex, requested_offset + 1);
	}
	if (EXPECTED(!Z_ISUNDEF_P(arg))) {
		ZVAL_COPY_DEREF(return_value, arg);
	}
}
/* }}} */

/* {{{ proto array func_get_args()
   Get an array of the arguments that were passed to the function */
GEAR_FUNCTION(func_get_args)
{
	zval *p, *q;
	uint32_t arg_count, first_extra_arg;
	uint32_t i;
	gear_execute_data *ex = EX(prev_execute_data);

	if (GEAR_CALL_INFO(ex) & GEAR_CALL_CODE) {
		gear_error(E_WARNING, "func_get_args():  Called from the global scope - no function context");
		RETURN_FALSE;
	}

	if (gear_forbid_dynamic_call("func_get_args()") == FAILURE) {
		RETURN_FALSE;
	}

	arg_count = GEAR_CALL_NUM_ARGS(ex);

	if (arg_count) {
		array_init_size(return_value, arg_count);
		first_extra_arg = ex->func->op_array.num_args;
		gear_hash_real_init_packed(Z_ARRVAL_P(return_value));
		GEAR_HASH_FILL_PACKED(Z_ARRVAL_P(return_value)) {
			i = 0;
			p = GEAR_CALL_ARG(ex, 1);
			if (arg_count > first_extra_arg) {
				while (i < first_extra_arg) {
					q = p;
					if (EXPECTED(Z_TYPE_INFO_P(q) != IS_UNDEF)) {
						ZVAL_DEREF(q);
						if (Z_OPT_REFCOUNTED_P(q)) {
							Z_ADDREF_P(q);
						}
					} else {
						q = &EG(uninitialized_zval);
					}
					GEAR_HASH_FILL_ADD(q);
					p++;
					i++;
				}
				p = GEAR_CALL_VAR_NUM(ex, ex->func->op_array.last_var + ex->func->op_array.T);
			}
			while (i < arg_count) {
				q = p;
				if (EXPECTED(Z_TYPE_INFO_P(q) != IS_UNDEF)) {
					ZVAL_DEREF(q);
					if (Z_OPT_REFCOUNTED_P(q)) {
						Z_ADDREF_P(q);
					}
				} else {
					q = &EG(uninitialized_zval);
				}
				GEAR_HASH_FILL_ADD(q);
				p++;
				i++;
			}
		} GEAR_HASH_FILL_END();
		Z_ARRVAL_P(return_value)->nNumOfElements = arg_count;
	} else {
		ZVAL_EMPTY_ARRAY(return_value);
	}
}
/* }}} */

/* {{{ proto int strlen(string str)
   Get string length
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
GEAR_FUNCTION(strlen)
{
	gear_string *s;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(s)
	GEAR_PARSE_PARAMETERS_END();

	RETVAL_LONG(ZSTR_LEN(s));
}
/* }}} */

/* {{{ proto int strcmp(string str1, string str2)
   Binary safe string comparison */
GEAR_FUNCTION(strcmp)
{
	gear_string *s1, *s2;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(s1)
		Z_PARAM_STR(s2)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_LONG(gear_binary_strcmp(ZSTR_VAL(s1), ZSTR_LEN(s1), ZSTR_VAL(s2), ZSTR_LEN(s2)));
}
/* }}} */

/* {{{ proto int strncmp(string str1, string str2, int len)
   Binary safe string comparison */
GEAR_FUNCTION(strncmp)
{
	gear_string *s1, *s2;
	gear_long len;

	GEAR_PARSE_PARAMETERS_START(3, 3)
		Z_PARAM_STR(s1)
		Z_PARAM_STR(s2)
		Z_PARAM_LONG(len)
	GEAR_PARSE_PARAMETERS_END();

	if (len < 0) {
		gear_error(E_WARNING, "Length must be greater than or equal to 0");
		RETURN_FALSE;
	}

	RETURN_LONG(gear_binary_strncmp(ZSTR_VAL(s1), ZSTR_LEN(s1), ZSTR_VAL(s2), ZSTR_LEN(s2), len));
}
/* }}} */

/* {{{ proto int strcasecmp(string str1, string str2)
   Binary safe case-insensitive string comparison */
GEAR_FUNCTION(strcasecmp)
{
	gear_string *s1, *s2;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(s1)
		Z_PARAM_STR(s2)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_LONG(gear_binary_strcasecmp(ZSTR_VAL(s1), ZSTR_LEN(s1), ZSTR_VAL(s2), ZSTR_LEN(s2)));
}
/* }}} */

/* {{{ proto int strncasecmp(string str1, string str2, int len)
   Binary safe string comparison */
GEAR_FUNCTION(strncasecmp)
{
	gear_string *s1, *s2;
	gear_long len;

	GEAR_PARSE_PARAMETERS_START(3, 3)
		Z_PARAM_STR(s1)
		Z_PARAM_STR(s2)
		Z_PARAM_LONG(len)
	GEAR_PARSE_PARAMETERS_END();

	if (len < 0) {
		gear_error(E_WARNING, "Length must be greater than or equal to 0");
		RETURN_FALSE;
	}

	RETURN_LONG(gear_binary_strncasecmp(ZSTR_VAL(s1), ZSTR_LEN(s1), ZSTR_VAL(s2), ZSTR_LEN(s2), len));
}
/* }}} */

/* {{{ proto mixed each(array &arr)
   Return the currently pointed key..value pair in the passed array, and advance the pointer to the next element, or false if there is no element at this place */
GEAR_FUNCTION(each)
{
	zval *array, *entry, tmp;
	gear_ulong num_key;
	HashTable *target_hash;
	gear_string *key;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "z/", &array) == FAILURE) {
		return;
	}

	if (!EG(each_deprecation_thrown)) {
		gear_error(E_DEPRECATED, "The each() function is deprecated. This message will be suppressed on further calls");
		EG(each_deprecation_thrown) = 1;
	}

	target_hash = HASH_OF(array);
	if (!target_hash) {
		gear_error(E_WARNING,"Variable passed to each() is not an array or object");
		return;
	}
	while (1) {
		entry = gear_hash_get_current_data(target_hash);
		if (!entry) {
			RETURN_FALSE;
		} else if (Z_TYPE_P(entry) == IS_INDIRECT) {
			entry = Z_INDIRECT_P(entry);
			if (Z_TYPE_P(entry) == IS_UNDEF) {
				gear_hash_move_forward(target_hash);
				continue;
			}
		}
		break;
	}
	array_init_size(return_value, 4);
	gear_hash_real_init_mixed(Z_ARRVAL_P(return_value));

	/* add value elements */
	ZVAL_DEREF(entry);
	if (Z_REFCOUNTED_P(entry)) {
		GC_ADDREF_EX(Z_COUNTED_P(entry), 2);
	}
	gear_hash_index_add_new(Z_ARRVAL_P(return_value), 1, entry);
	gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_VALUE), entry);

	/* add the key elements */
	if (gear_hash_get_current_key(target_hash, &key, &num_key) == HASH_KEY_IS_STRING) {
		ZVAL_STR_COPY(&tmp, key);
		Z_TRY_ADDREF(tmp);
	} else {
		ZVAL_LONG(&tmp, num_key);
	}
	gear_hash_index_add_new(Z_ARRVAL_P(return_value), 0, &tmp);
	gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_KEY), &tmp);
	gear_hash_move_forward(target_hash);
}
/* }}} */

/* {{{ proto int error_reporting([int new_error_level])
   Return the current error_reporting level, and if an argument was passed - change to the new level */
GEAR_FUNCTION(error_reporting)
{
	zval *err = NULL;
	int old_error_reporting;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_ZVAL(err)
	GEAR_PARSE_PARAMETERS_END();

	old_error_reporting = EG(error_reporting);
	if (GEAR_NUM_ARGS() != 0) {
		gear_string *new_val = zval_get_string(err);
		do {
			gear_ics_entry *p = EG(error_reporting_ics_entry);

			if (!p) {
				zval *zv = gear_hash_find_ex(EG(ics_directives), ZSTR_KNOWN(GEAR_STR_ERROR_REPORTING), 1);
				if (zv) {
					p = EG(error_reporting_ics_entry) = (gear_ics_entry*)Z_PTR_P(zv);
				} else {
					break;
				}
			}
			if (!p->modified) {
				if (!EG(modified_ics_directives)) {
					ALLOC_HASHTABLE(EG(modified_ics_directives));
					gear_hash_init(EG(modified_ics_directives), 8, NULL, NULL, 0);
				}
				if (EXPECTED(gear_hash_add_ptr(EG(modified_ics_directives), ZSTR_KNOWN(GEAR_STR_ERROR_REPORTING), p) != NULL)) {
					p->orig_value = p->value;
					p->orig_modifiable = p->modifiable;
					p->modified = 1;
				}
			} else if (p->orig_value != p->value) {
				gear_string_release_ex(p->value, 0);
			}

			p->value = new_val;
			if (Z_TYPE_P(err) == IS_LONG) {
				EG(error_reporting) = Z_LVAL_P(err);
			} else {
				EG(error_reporting) = atoi(ZSTR_VAL(p->value));
			}
		} while (0);
	}

	RETVAL_LONG(old_error_reporting);
}
/* }}} */

static int validate_constant_array(HashTable *ht) /* {{{ */
{
	int ret = 1;
	zval *val;

	GC_PROTECT_RECURSION(ht);
	GEAR_HASH_FOREACH_VAL_IND(ht, val) {
		ZVAL_DEREF(val);
		if (Z_REFCOUNTED_P(val)) {
			if (Z_TYPE_P(val) == IS_ARRAY) {
				if (Z_REFCOUNTED_P(val)) {
					if (Z_IS_RECURSIVE_P(val)) {
						gear_error(E_WARNING, "Constants cannot be recursive arrays");
						ret = 0;
						break;
					} else if (!validate_constant_array(Z_ARRVAL_P(val))) {
						ret = 0;
						break;
					}
				}
			} else if (Z_TYPE_P(val) != IS_STRING && Z_TYPE_P(val) != IS_RESOURCE) {
				gear_error(E_WARNING, "Constants may only evaluate to scalar values, arrays or resources");
				ret = 0;
				break;
			}
		}
	} GEAR_HASH_FOREACH_END();
	GC_UNPROTECT_RECURSION(ht);
	return ret;
}
/* }}} */

static void copy_constant_array(zval *dst, zval *src) /* {{{ */
{
	gear_string *key;
	gear_ulong idx;
	zval *new_val, *val;

	array_init_size(dst, gear_hash_num_elements(Z_ARRVAL_P(src)));
	GEAR_HASH_FOREACH_KEY_VAL_IND(Z_ARRVAL_P(src), idx, key, val) {
		/* constant arrays can't contain references */
		ZVAL_DEREF(val);
		if (key) {
			new_val = gear_hash_add_new(Z_ARRVAL_P(dst), key, val);
		} else {
			new_val = gear_hash_index_add_new(Z_ARRVAL_P(dst), idx, val);
		}
		if (Z_TYPE_P(val) == IS_ARRAY) {
			if (Z_REFCOUNTED_P(val)) {
				copy_constant_array(new_val, val);
			}
		} else {
			Z_TRY_ADDREF_P(val);
		}
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto bool define(string constant_name, mixed value[, bool case_insensitive])
   Define a new constant */
GEAR_FUNCTION(define)
{
	gear_string *name;
	zval *val, val_free;
	gear_bool non_cs = 0;
	int case_sensitive = CONST_CS;
	gear_constant c;

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(name)
		Z_PARAM_ZVAL(val)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(non_cs)
	GEAR_PARSE_PARAMETERS_END();

	if (non_cs) {
		case_sensitive = 0;
	}

	if (gear_memnstr(ZSTR_VAL(name), "::", sizeof("::") - 1, ZSTR_VAL(name) + ZSTR_LEN(name))) {
		gear_error(E_WARNING, "Class constants cannot be defined or redefined");
		RETURN_FALSE;
	}

	ZVAL_UNDEF(&val_free);

repeat:
	switch (Z_TYPE_P(val)) {
		case IS_LONG:
		case IS_DOUBLE:
		case IS_STRING:
		case IS_FALSE:
		case IS_TRUE:
		case IS_NULL:
		case IS_RESOURCE:
			break;
		case IS_ARRAY:
			if (Z_REFCOUNTED_P(val)) {
				if (!validate_constant_array(Z_ARRVAL_P(val))) {
					RETURN_FALSE;
				} else {
					copy_constant_array(&c.value, val);
					goto register_constant;
				}
			}
			break;
		case IS_OBJECT:
			if (Z_TYPE(val_free) == IS_UNDEF) {
				if (Z_OBJ_HT_P(val)->get) {
					zval rv;
					val = Z_OBJ_HT_P(val)->get(val, &rv);
					ZVAL_COPY_VALUE(&val_free, val);
					goto repeat;
				} else if (Z_OBJ_HT_P(val)->cast_object) {
					if (Z_OBJ_HT_P(val)->cast_object(val, &val_free, IS_STRING) == SUCCESS) {
						val = &val_free;
						break;
					}
				}
			}
			/* no break */
		default:
			gear_error(E_WARNING, "Constants may only evaluate to scalar values, arrays or resources");
			zval_ptr_dtor(&val_free);
			RETURN_FALSE;
	}

	ZVAL_COPY(&c.value, val);
	zval_ptr_dtor(&val_free);

register_constant:
	if (non_cs) {
		gear_error(E_DEPRECATED,
			"define(): Declaration of case-insensitive constants is deprecated");
	}

	/* non persistent */
	GEAR_CONSTANT_SET_FLAGS(&c, case_sensitive, HYSS_USER_CONSTANT);
	c.name = gear_string_copy(name);
	if (gear_register_constant(&c) == SUCCESS) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool defined(string constant_name)
   Check whether a constant exists
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
GEAR_FUNCTION(defined)
{
	gear_string *name;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(name)
	GEAR_PARSE_PARAMETERS_END();

	if (gear_get_constant_ex(name, gear_get_executed_scope(), GEAR_FETCH_CLASS_SILENT | GEAR_GET_CONSTANT_NO_DEPRECATION_CHECK)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto string get_class([object object])
   Retrieves the class name */
GEAR_FUNCTION(get_class)
{
	zval *obj = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|o", &obj) == FAILURE) {
		RETURN_FALSE;
	}

	if (!obj) {
		gear_class_entry *scope = gear_get_executed_scope();

		if (scope) {
			RETURN_STR_COPY(scope->name);
		} else {
			gear_error(E_WARNING, "get_class() called without object from outside a class");
			RETURN_FALSE;
		}
	}

	RETURN_STR_COPY(Z_OBJCE_P(obj)->name);
}
/* }}} */

/* {{{ proto string get_called_class()
   Retrieves the "Late Static Binding" class name */
GEAR_FUNCTION(get_called_class)
{
	gear_class_entry *called_scope;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	called_scope = gear_get_called_scope(execute_data);
	if (called_scope) {
		RETURN_STR_COPY(called_scope->name);
	} else {
		gear_class_entry *scope = gear_get_executed_scope();
		if (!scope)  {
			gear_error(E_WARNING, "get_called_class() called from outside a class");
		}
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto mixed get_parent_class([mixed object])
   Retrieves the parent class name for object or class or current scope or false if not in a scope. */
GEAR_FUNCTION(get_parent_class)
{
	zval *arg;
	gear_class_entry *ce = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|z", &arg) == FAILURE) {
		return;
	}

	if (!GEAR_NUM_ARGS()) {
		ce = gear_get_executed_scope();
		if (ce && ce->parent) {
			RETURN_STR_COPY(ce->parent->name);
		} else {
			RETURN_FALSE;
		}
	}

	if (Z_TYPE_P(arg) == IS_OBJECT) {
		ce = Z_OBJ_P(arg)->ce;
	} else if (Z_TYPE_P(arg) == IS_STRING) {
	    ce = gear_lookup_class(Z_STR_P(arg));
	}

	if (ce && ce->parent) {
		RETURN_STR_COPY(ce->parent->name);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

static void is_a_impl(INTERNAL_FUNCTION_PARAMETERS, gear_bool only_subclass) /* {{{ */
{
	zval *obj;
	gear_string *class_name;
	gear_class_entry *instance_ce;
	gear_class_entry *ce;
	gear_bool allow_string = only_subclass;
	gear_bool retval;

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_ZVAL(obj)
		Z_PARAM_STR(class_name)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(allow_string)
	GEAR_PARSE_PARAMETERS_END();
	/*
	 * allow_string - is_a default is no, is_subclass_of is yes.
	 *   if it's allowed, then the autoloader will be called if the class does not exist.
	 *   default behaviour is different, as 'is_a' used to be used to test mixed return values
	 *   and there is no easy way to deprecate this.
	 */

	if (allow_string && Z_TYPE_P(obj) == IS_STRING) {
		instance_ce = gear_lookup_class(Z_STR_P(obj));
		if (!instance_ce) {
			RETURN_FALSE;
		}
	} else if (Z_TYPE_P(obj) == IS_OBJECT) {
		instance_ce = Z_OBJCE_P(obj);
	} else {
		RETURN_FALSE;
	}

	if (!only_subclass && EXPECTED(gear_string_equals(instance_ce->name, class_name))) {
		retval = 1;
	} else {
		ce = gear_lookup_class_ex(class_name, NULL, 0);
		if (!ce) {
			retval = 0;
		} else {
			if (only_subclass && instance_ce == ce) {
				retval = 0;
			} else {
				retval = instanceof_function(instance_ce, ce);
			}
		}
	}

	RETURN_BOOL(retval);
}
/* }}} */

/* {{{ proto bool is_subclass_of(mixed object_or_string, string class_name [, bool allow_string])
   Returns true if the object has this class as one of its parents */
GEAR_FUNCTION(is_subclass_of)
{
	is_a_impl(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto bool is_a(mixed object_or_string, string class_name [, bool allow_string])
   Returns true if the first argument is an object and is this class or has this class as one of its parents, */
GEAR_FUNCTION(is_a)
{
	is_a_impl(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ add_class_vars */
static void add_class_vars(gear_class_entry *scope, gear_class_entry *ce, int statics, zval *return_value)
{
	gear_property_info *prop_info;
	zval *prop, prop_copy;
	gear_string *key;

	GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->properties_info, key, prop_info) {
		if (((prop_info->flags & GEAR_ACC_SHADOW) &&
		     prop_info->ce != scope) ||
		    ((prop_info->flags & GEAR_ACC_PROTECTED) &&
		     !gear_check_protected(prop_info->ce, scope)) ||
		    ((prop_info->flags & GEAR_ACC_PRIVATE) &&
		      ce != scope &&
			  prop_info->ce != scope)) {
			continue;
		}
		prop = NULL;
		if (statics && (prop_info->flags & GEAR_ACC_STATIC) != 0) {
			prop = &ce->default_static_members_table[prop_info->offset];
			ZVAL_DEINDIRECT(prop);
		} else if (!statics && (prop_info->flags & GEAR_ACC_STATIC) == 0) {
			prop = &ce->default_properties_table[OBJ_PROP_TO_NUM(prop_info->offset)];
		}
		if (!prop || Z_TYPE_P(prop) == IS_UNDEF) {
			continue;
		}

		/* copy: enforce read only access */
		ZVAL_DEREF(prop);
		ZVAL_COPY_OR_DUP(&prop_copy, prop);
		prop = &prop_copy;

		/* this is necessary to make it able to work with default array
		 * properties, returned to user */
		if (Z_OPT_TYPE_P(prop) == IS_CONSTANT_AST) {
			if (UNEXPECTED(zval_update_constant_ex(prop, NULL) != SUCCESS)) {
				return;
			}
		}

		gear_hash_add_new(Z_ARRVAL_P(return_value), key, prop);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto array get_class_vars(string class_name)
   Returns an array of default properties of the class. */
GEAR_FUNCTION(get_class_vars)
{
	gear_string *class_name;
	gear_class_entry *ce, *scope;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &class_name) == FAILURE) {
		return;
	}

	ce = gear_lookup_class(class_name);
	if (!ce) {
		RETURN_FALSE;
	} else {
		array_init(return_value);
		if (UNEXPECTED(!(ce->ce_flags & GEAR_ACC_CONSTANTS_UPDATED))) {
			if (UNEXPECTED(gear_update_class_constants(ce) != SUCCESS)) {
				return;
			}
		}
		scope = gear_get_executed_scope();
		add_class_vars(scope, ce, 0, return_value);
		add_class_vars(scope, ce, 1, return_value);
	}
}
/* }}} */

/* {{{ proto array get_object_vars(object obj)
   Returns an array of object properties */
GEAR_FUNCTION(get_object_vars)
{
	zval *obj;
	zval *value;
	HashTable *properties;
	gear_string *key;
	gear_object *zobj;
	gear_ulong num_key;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT(obj)
	GEAR_PARSE_PARAMETERS_END();

	if (Z_OBJ_HT_P(obj)->get_properties == NULL) {
		RETURN_FALSE;
	}

	properties = Z_OBJ_HT_P(obj)->get_properties(obj);

	if (properties == NULL) {
		RETURN_FALSE;
	}

	zobj = Z_OBJ_P(obj);

	if (!zobj->ce->default_properties_count && properties == zobj->properties && !GC_IS_RECURSIVE(properties)) {
		/* fast copy */
		if (EXPECTED(zobj->handlers == &std_object_handlers)) {
			RETURN_ARR(gear_proptable_to_symtable(properties, 0));
		}
		RETURN_ARR(gear_proptable_to_symtable(properties, 1));
	} else {
		array_init_size(return_value, gear_hash_num_elements(properties));

		GEAR_HASH_FOREACH_KEY_VAL(properties, num_key, key, value) {
			gear_bool unmangle = 0;
			if (Z_TYPE_P(value) == IS_INDIRECT) {
				value = Z_INDIRECT_P(value);
				if (UNEXPECTED(Z_ISUNDEF_P(value))) {
					continue;
				}

				GEAR_ASSERT(key);
				if (gear_check_property_access(zobj, key) == FAILURE) {
					continue;
				}
				unmangle = 1;
			}

			if (Z_ISREF_P(value) && Z_REFCOUNT_P(value) == 1) {
				value = Z_REFVAL_P(value);
			}
			Z_TRY_ADDREF_P(value);

			if (UNEXPECTED(!key)) {
				/* This case is only possible due to loopholes, e.g. ArrayObject */
				gear_hash_index_add(Z_ARRVAL_P(return_value), num_key, value);
			} else if (unmangle && ZSTR_VAL(key)[0] == 0) {
				const char *prop_name, *class_name;
				size_t prop_len;
				gear_unmangle_property_name_ex(key, &class_name, &prop_name, &prop_len);
				/* We assume here that a mangled property name is never
				 * numeric. This is probably a safe assumption, but
				 * theoretically someone might write an extension with
				 * private, numeric properties. Well, too bad.
				 */
				gear_hash_str_add_new(Z_ARRVAL_P(return_value), prop_name, prop_len, value);
			} else {
				gear_symtable_add_new(Z_ARRVAL_P(return_value), key, value);
			}
		} GEAR_HASH_FOREACH_END();
	}
}
/* }}} */

static int same_name(gear_string *key, gear_string *name) /* {{{ */
{
	gear_string *lcname;
	int ret;

	if (key == name) {
		return 1;
	}
	if (ZSTR_LEN(key) != ZSTR_LEN(name)) {
		return 0;
	}
	lcname = gear_string_tolower(name);
	ret = memcmp(ZSTR_VAL(lcname), ZSTR_VAL(key), ZSTR_LEN(key)) == 0;
	gear_string_release_ex(lcname, 0);
	return ret;
}
/* }}} */

/* {{{ proto array get_class_methods(mixed class)
   Returns an array of method names for class or class instance. */
GEAR_FUNCTION(get_class_methods)
{
	zval *klass;
	zval method_name;
	gear_class_entry *ce = NULL;
	gear_class_entry *scope;
	gear_function *mptr;
	gear_string *key;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "z", &klass) == FAILURE) {
		return;
	}

	if (Z_TYPE_P(klass) == IS_OBJECT) {
		ce = Z_OBJCE_P(klass);
	} else if (Z_TYPE_P(klass) == IS_STRING) {
	    ce = gear_lookup_class(Z_STR_P(klass));
	}

	if (!ce) {
		RETURN_NULL();
	}

	array_init(return_value);
	scope = gear_get_executed_scope();

	GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->function_table, key, mptr) {

		if ((mptr->common.fn_flags & GEAR_ACC_PUBLIC)
		 || (scope &&
		     (((mptr->common.fn_flags & GEAR_ACC_PROTECTED) &&
		       gear_check_protected(mptr->common.scope, scope))
		   || ((mptr->common.fn_flags & GEAR_ACC_PRIVATE) &&
		       scope == mptr->common.scope)))) {
			size_t len = ZSTR_LEN(mptr->common.function_name);

			/* Do not display old-style inherited constructors */
			if (!key) {
				ZVAL_STR_COPY(&method_name, mptr->common.function_name);
				gear_hash_next_index_insert_new(Z_ARRVAL_P(return_value), &method_name);
			} else if ((mptr->common.fn_flags & GEAR_ACC_CTOR) == 0 ||
			    mptr->common.scope == ce ||
			    gear_binary_strcasecmp(ZSTR_VAL(key), ZSTR_LEN(key), ZSTR_VAL(mptr->common.function_name), len) == 0) {

				if (mptr->type == GEAR_USER_FUNCTION &&
				    (!mptr->op_array.refcount || *mptr->op_array.refcount > 1) &&
			    	 !same_name(key, mptr->common.function_name)) {
					ZVAL_STR_COPY(&method_name, gear_find_alias_name(mptr->common.scope, key));
					gear_hash_next_index_insert_new(Z_ARRVAL_P(return_value), &method_name);
				} else {
					ZVAL_STR_COPY(&method_name, mptr->common.function_name);
					gear_hash_next_index_insert_new(Z_ARRVAL_P(return_value), &method_name);
				}
			}
		}
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto bool method_exists(object object, string method)
   Checks if the class method exists */
GEAR_FUNCTION(method_exists)
{
	zval *klass;
	gear_string *method_name;
	gear_string *lcname;
	gear_class_entry * ce;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_ZVAL(klass)
		Z_PARAM_STR(method_name)
	GEAR_PARSE_PARAMETERS_END();

	if (Z_TYPE_P(klass) == IS_OBJECT) {
		ce = Z_OBJCE_P(klass);
	} else if (Z_TYPE_P(klass) == IS_STRING) {
		if ((ce = gear_lookup_class(Z_STR_P(klass))) == NULL) {
			RETURN_FALSE;
		}
	} else {
		RETURN_FALSE;
	}

	lcname = gear_string_tolower(method_name);
	if (gear_hash_exists(&ce->function_table, lcname)) {
		gear_string_release_ex(lcname, 0);
		RETURN_TRUE;
	} else if (Z_TYPE_P(klass) == IS_OBJECT && Z_OBJ_HT_P(klass)->get_method != NULL) {
		gear_object *obj = Z_OBJ_P(klass);
		gear_function *func = Z_OBJ_HT_P(klass)->get_method(&obj, method_name, NULL);
		if (func != NULL) {
			if (func->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) {
				/* Returns true to the fake Closure's __invoke */
				RETVAL_BOOL(func->common.scope == gear_ce_closure
					&& gear_string_equals_literal(method_name, GEAR_INVOKE_FUNC_NAME));

				gear_string_release_ex(lcname, 0);
				gear_string_release_ex(func->common.function_name, 0);
				gear_free_trampoline(func);
				return;
			}
			gear_string_release_ex(lcname, 0);
			RETURN_TRUE;
		}
	}
	gear_string_release_ex(lcname, 0);
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto bool property_exists(mixed object_or_class, string property_name)
   Checks if the object or class has a property */
GEAR_FUNCTION(property_exists)
{
	zval *object;
	gear_string *property;
	gear_class_entry *ce;
	gear_property_info *property_info;
	zval property_z;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "zS", &object, &property) == FAILURE) {
		return;
	}

	if (property == NULL) {
		RETURN_FALSE;
	}

	if (Z_TYPE_P(object) == IS_STRING) {
		ce = gear_lookup_class(Z_STR_P(object));
		if (!ce) {
			RETURN_FALSE;
		}
	} else if (Z_TYPE_P(object) == IS_OBJECT) {
		ce = Z_OBJCE_P(object);
	} else {
		gear_error(E_WARNING, "First parameter must either be an object or the name of an existing class");
		RETURN_NULL();
	}

	if ((property_info = gear_hash_find_ptr(&ce->properties_info, property)) != NULL
		&& (property_info->flags & GEAR_ACC_SHADOW) == 0) {
		RETURN_TRUE;
	}

	ZVAL_STR(&property_z, property);

	if (Z_TYPE_P(object) ==  IS_OBJECT &&
		Z_OBJ_HANDLER_P(object, has_property) &&
		Z_OBJ_HANDLER_P(object, has_property)(object, &property_z, 2, NULL)) {
		RETURN_TRUE;
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto bool class_exists(string classname [, bool autoload])
   Checks if the class exists */
GEAR_FUNCTION(class_exists)
{
	gear_string *class_name;
	gear_string *lc_name;
	gear_class_entry *ce;
	gear_bool autoload = 1;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(class_name)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(autoload)
	GEAR_PARSE_PARAMETERS_END();

	if (!autoload) {
		if (ZSTR_VAL(class_name)[0] == '\\') {
			/* Ignore leading "\" */
			lc_name = gear_string_alloc(ZSTR_LEN(class_name) - 1, 0);
			gear_str_tolower_copy(ZSTR_VAL(lc_name), ZSTR_VAL(class_name) + 1, ZSTR_LEN(class_name) - 1);
		} else {
			lc_name = gear_string_tolower(class_name);
		}

		ce = gear_hash_find_ptr(EG(class_table), lc_name);
		gear_string_release_ex(lc_name, 0);
	} else {
		ce = gear_lookup_class(class_name);
	}

 	if (ce) {
 		RETURN_BOOL((ce->ce_flags & (GEAR_ACC_INTERFACE | GEAR_ACC_TRAIT)) == 0);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool interface_exists(string classname [, bool autoload])
   Checks if the class exists */
GEAR_FUNCTION(interface_exists)
{
	gear_string *iface_name, *lc_name;
	gear_class_entry *ce;
	gear_bool autoload = 1;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(iface_name)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(autoload)
	GEAR_PARSE_PARAMETERS_END();

	if (!autoload) {
		if (ZSTR_VAL(iface_name)[0] == '\\') {
			/* Ignore leading "\" */
			lc_name = gear_string_alloc(ZSTR_LEN(iface_name) - 1, 0);
			gear_str_tolower_copy(ZSTR_VAL(lc_name), ZSTR_VAL(iface_name) + 1, ZSTR_LEN(iface_name) - 1);
		} else {
			lc_name = gear_string_tolower(iface_name);
		}
		ce = gear_hash_find_ptr(EG(class_table), lc_name);
		gear_string_release_ex(lc_name, 0);
		RETURN_BOOL(ce && ce->ce_flags & GEAR_ACC_INTERFACE);
	}

	ce = gear_lookup_class(iface_name);
	if (ce) {
 		RETURN_BOOL((ce->ce_flags & GEAR_ACC_INTERFACE) > 0);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool trait_exists(string traitname [, bool autoload])
 Checks if the trait exists */
GEAR_FUNCTION(trait_exists)
{
	gear_string *trait_name, *lc_name;
	gear_class_entry *ce;
	gear_bool autoload = 1;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(trait_name)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(autoload)
	GEAR_PARSE_PARAMETERS_END();

	if (!autoload) {
		if (ZSTR_VAL(trait_name)[0] == '\\') {
			/* Ignore leading "\" */
			lc_name = gear_string_alloc(ZSTR_LEN(trait_name) - 1, 0);
			gear_str_tolower_copy(ZSTR_VAL(lc_name), ZSTR_VAL(trait_name) + 1, ZSTR_LEN(trait_name) - 1);
		} else {
			lc_name = gear_string_tolower(trait_name);
		}

		ce = gear_hash_find_ptr(EG(class_table), lc_name);
		gear_string_release_ex(lc_name, 0);
	} else {
		ce = gear_lookup_class(trait_name);
	}

	if (ce) {
 		RETURN_BOOL((ce->ce_flags & GEAR_ACC_TRAIT) != 0);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool function_exists(string function_name)
   Checks if the function exists */
GEAR_FUNCTION(function_exists)
{
	gear_string *name;
	gear_function *func;
	gear_string *lcname;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(name)
	GEAR_PARSE_PARAMETERS_END();

	if (ZSTR_VAL(name)[0] == '\\') {
		/* Ignore leading "\" */
		lcname = gear_string_alloc(ZSTR_LEN(name) - 1, 0);
		gear_str_tolower_copy(ZSTR_VAL(lcname), ZSTR_VAL(name) + 1, ZSTR_LEN(name) - 1);
	} else {
		lcname = gear_string_tolower(name);
	}

	func = gear_hash_find_ptr(EG(function_table), lcname);
	gear_string_release_ex(lcname, 0);

	/*
	 * A bit of a hack, but not a bad one: we see if the handler of the function
	 * is actually one that displays "function is disabled" message.
	 */
	RETURN_BOOL(func && (func->type != GEAR_INTERNAL_FUNCTION ||
		func->internal_function.handler != zif_display_disabled_function));
}
/* }}} */

/* {{{ proto bool class_alias(string user_class_name , string alias_name [, bool autoload])
   Creates an alias for user defined class */
GEAR_FUNCTION(class_alias)
{
	gear_string *class_name;
	char *alias_name;
	gear_class_entry *ce;
	size_t alias_name_len;
	gear_bool autoload = 1;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "Ss|b", &class_name, &alias_name, &alias_name_len, &autoload) == FAILURE) {
		return;
	}

	ce = gear_lookup_class_ex(class_name, NULL, autoload);

	if (ce) {
		if (ce->type == GEAR_USER_CLASS) {
			if (gear_register_class_alias_ex(alias_name, alias_name_len, ce, 0) == SUCCESS) {
				RETURN_TRUE;
			} else {
				gear_error(E_WARNING, "Cannot declare %s %s, because the name is already in use", gear_get_object_type(ce), alias_name);
				RETURN_FALSE;
			}
		} else {
			gear_error(E_WARNING, "First argument of class_alias() must be a name of user defined class");
			RETURN_FALSE;
		}
	} else {
		gear_error(E_WARNING, "Class '%s' not found", ZSTR_VAL(class_name));
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto array get_included_files(void)
   Returns an array with the file names that were include_once()'d */
GEAR_FUNCTION(get_included_files)
{
	gear_string *entry;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);
	GEAR_HASH_FOREACH_STR_KEY(&EG(included_files), entry) {
		if (entry) {
			add_next_index_str(return_value, gear_string_copy(entry));
		}
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto void trigger_error(string message [, int error_type])
   Generates a user-level error/warning/notice message */
GEAR_FUNCTION(trigger_error)
{
	gear_long error_type = E_USER_NOTICE;
	char *message;
	size_t message_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s|l", &message, &message_len, &error_type) == FAILURE) {
		return;
	}

	switch (error_type) {
		case E_USER_ERROR:
		case E_USER_WARNING:
		case E_USER_NOTICE:
		case E_USER_DEPRECATED:
			break;
		default:
			gear_error(E_WARNING, "Invalid error type specified");
			RETURN_FALSE;
			break;
	}

	gear_error((int)error_type, "%s", message);
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto string set_error_handler(callable error_handler [, int error_types])
   Sets a user-defined error handler function.  Returns the previously defined error handler, or false on error */
GEAR_FUNCTION(set_error_handler)
{
	zval *error_handler;
	gear_long error_type = E_ALL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "z|l", &error_handler, &error_type) == FAILURE) {
		return;
	}

	if (Z_TYPE_P(error_handler) != IS_NULL) { /* NULL == unset */
		if (!gear_is_callable(error_handler, 0, NULL)) {
			gear_string *error_handler_name = gear_get_callable_name(error_handler);
			gear_error(E_WARNING, "%s() expects the argument (%s) to be a valid callback",
					   get_active_function_name(), error_handler_name?ZSTR_VAL(error_handler_name):"unknown");
			gear_string_release_ex(error_handler_name, 0);
			return;
		}
	}

	if (Z_TYPE(EG(user_error_handler)) != IS_UNDEF) {
		ZVAL_COPY(return_value, &EG(user_error_handler));

		gear_stack_push(&EG(user_error_handlers_error_reporting), &EG(user_error_handler_error_reporting));
		gear_stack_push(&EG(user_error_handlers), &EG(user_error_handler));
	}

	if (Z_TYPE_P(error_handler) == IS_NULL) { /* unset user-defined handler */
		ZVAL_UNDEF(&EG(user_error_handler));
		return;
	}

	ZVAL_COPY(&EG(user_error_handler), error_handler);
	EG(user_error_handler_error_reporting) = (int)error_type;
}
/* }}} */

/* {{{ proto void restore_error_handler(void)
   Restores the previously defined error handler function */
GEAR_FUNCTION(restore_error_handler)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (Z_TYPE(EG(user_error_handler)) != IS_UNDEF) {
		zval zeh;

		ZVAL_COPY_VALUE(&zeh, &EG(user_error_handler));
		ZVAL_UNDEF(&EG(user_error_handler));
		zval_ptr_dtor(&zeh);
	}

	if (gear_stack_is_empty(&EG(user_error_handlers))) {
		ZVAL_UNDEF(&EG(user_error_handler));
	} else {
		zval *tmp;
		EG(user_error_handler_error_reporting) = gear_stack_int_top(&EG(user_error_handlers_error_reporting));
		gear_stack_del_top(&EG(user_error_handlers_error_reporting));
		tmp = gear_stack_top(&EG(user_error_handlers));
		ZVAL_COPY_VALUE(&EG(user_error_handler), tmp);
		gear_stack_del_top(&EG(user_error_handlers));
	}
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto mixed set_exception_handler(callable exception_handler)
   Sets a user-defined exception handler function. Returns the previously defined exception handler, or false on error */
GEAR_FUNCTION(set_exception_handler)
{
	zval *exception_handler;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "z", &exception_handler) == FAILURE) {
		return;
	}

	if (Z_TYPE_P(exception_handler) != IS_NULL) { /* NULL == unset */
		if (!gear_is_callable(exception_handler, 0, NULL)) {
		gear_string *exception_handler_name = gear_get_callable_name(exception_handler);
			gear_error(E_WARNING, "%s() expects the argument (%s) to be a valid callback",
					   get_active_function_name(), exception_handler_name?ZSTR_VAL(exception_handler_name):"unknown");
			gear_string_release_ex(exception_handler_name, 0);
			return;
		}
	}

	if (Z_TYPE(EG(user_exception_handler)) != IS_UNDEF) {
		ZVAL_COPY(return_value, &EG(user_exception_handler));

		gear_stack_push(&EG(user_exception_handlers), &EG(user_exception_handler));
	}

	if (Z_TYPE_P(exception_handler) == IS_NULL) { /* unset user-defined handler */
		ZVAL_UNDEF(&EG(user_exception_handler));
		return;
	}

	ZVAL_COPY(&EG(user_exception_handler), exception_handler);
}
/* }}} */

/* {{{ proto void restore_exception_handler(void)
   Restores the previously defined exception handler function */
GEAR_FUNCTION(restore_exception_handler)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (Z_TYPE(EG(user_exception_handler)) != IS_UNDEF) {
		zval_ptr_dtor(&EG(user_exception_handler));
	}
	if (gear_stack_is_empty(&EG(user_exception_handlers))) {
		ZVAL_UNDEF(&EG(user_exception_handler));
	} else {
		zval *tmp = gear_stack_top(&EG(user_exception_handlers));
		ZVAL_COPY_VALUE(&EG(user_exception_handler), tmp);
		gear_stack_del_top(&EG(user_exception_handlers));
	}
	RETURN_TRUE;
}
/* }}} */

static int copy_class_or_interface_name(zval *el, int num_args, va_list args, gear_hash_key *hash_key) /* {{{ */
{
	gear_class_entry *ce = (gear_class_entry *)Z_PTR_P(el);
	zval *array = va_arg(args, zval *);
	uint32_t mask = va_arg(args, uint32_t);
	uint32_t comply = va_arg(args, uint32_t);
	uint32_t comply_mask = (comply)? mask:0;

	if ((hash_key->key && ZSTR_VAL(hash_key->key)[0] != 0)
		&& (comply_mask == (ce->ce_flags & mask))) {
		if (ce->refcount > 1 &&
		    !same_name(hash_key->key, ce->name)) {
			add_next_index_str(array, gear_string_copy(hash_key->key));
		} else {
			add_next_index_str(array, gear_string_copy(ce->name));
		}
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

/* {{{ proto array get_declared_traits()
   Returns an array of all declared traits. */
GEAR_FUNCTION(get_declared_traits)
{
	uint32_t mask = GEAR_ACC_TRAIT;
	uint32_t comply = 1;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);
	gear_hash_apply_with_arguments(EG(class_table), copy_class_or_interface_name, 3, return_value, mask, comply);
}
/* }}} */

/* {{{ proto array get_declared_classes()
   Returns an array of all declared classes. */
GEAR_FUNCTION(get_declared_classes)
{
	uint32_t mask = GEAR_ACC_INTERFACE | GEAR_ACC_TRAIT;
	uint32_t comply = 0;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);
	gear_hash_apply_with_arguments(EG(class_table), copy_class_or_interface_name, 3, return_value, mask, comply);
}
/* }}} */

/* {{{ proto array get_declared_interfaces()
   Returns an array of all declared interfaces. */
GEAR_FUNCTION(get_declared_interfaces)
{
	uint32_t mask = GEAR_ACC_INTERFACE;
	uint32_t comply = 1;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);
	gear_hash_apply_with_arguments(EG(class_table), copy_class_or_interface_name, 3, return_value, mask, comply);
}
/* }}} */

static int copy_function_name(zval *zv, int num_args, va_list args, gear_hash_key *hash_key) /* {{{ */
{
	gear_function *func = Z_PTR_P(zv);
	zval *internal_ar = va_arg(args, zval *),
	     *user_ar     = va_arg(args, zval *);
	gear_bool *exclude_disabled = va_arg(args, gear_bool *);

	if (hash_key->key == NULL || ZSTR_VAL(hash_key->key)[0] == 0) {
		return 0;
	}

	if (func->type == GEAR_INTERNAL_FUNCTION) {
		char *disable_functions = ICS_STR("disable_functions");

		if ((*exclude_disabled == 1) && (disable_functions != NULL)) {
			if (strstr(disable_functions, func->common.function_name->val) == NULL) {
				add_next_index_str(internal_ar, gear_string_copy(hash_key->key));
			}
		} else {
			add_next_index_str(internal_ar, gear_string_copy(hash_key->key));
		}
	} else if (func->type == GEAR_USER_FUNCTION) {
		add_next_index_str(user_ar, gear_string_copy(hash_key->key));
	}

	return 0;
}
/* }}} */

/* {{{ proto array get_defined_functions(void)
   Returns an array of all defined functions */
GEAR_FUNCTION(get_defined_functions)
{
	zval internal, user;
	gear_bool exclude_disabled = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|b", &exclude_disabled) == FAILURE) {
		return;
	}

	array_init(&internal);
	array_init(&user);
	array_init(return_value);

	gear_hash_apply_with_arguments(EG(function_table), copy_function_name, 3, &internal, &user, &exclude_disabled);

	gear_hash_str_add_new(Z_ARRVAL_P(return_value), "internal", sizeof("internal")-1, &internal);
	gear_hash_str_add_new(Z_ARRVAL_P(return_value), "user", sizeof("user")-1, &user);
}
/* }}} */

/* {{{ proto array get_defined_vars(void)
   Returns an associative array of names and values of all currently defined variable names (variables in the current scope) */
GEAR_FUNCTION(get_defined_vars)
{
	gear_array *symbol_table;
	if (gear_forbid_dynamic_call("get_defined_vars()") == FAILURE) {
		return;
	}

	symbol_table = gear_rebuild_symbol_table();
	if (UNEXPECTED(symbol_table == NULL)) {
		return;
	}

	RETURN_ARR(gear_array_dup(symbol_table));
}
/* }}} */

#define LAMBDA_TEMP_FUNCNAME	"__lambda_func"
/* {{{ proto string create_function(string args, string code)
   Creates an anonymous function, and returns its name (funny, eh?) */
GEAR_FUNCTION(create_function)
{
    gear_string *function_name;
	char *eval_code, *function_args, *function_code;
	size_t eval_code_length, function_args_len, function_code_len;
	int retval;
	char *eval_name;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss", &function_args, &function_args_len, &function_code, &function_code_len) == FAILURE) {
		return;
	}

	eval_code = (char *) emalloc(sizeof("function " LAMBDA_TEMP_FUNCNAME)
			+function_args_len
			+2	/* for the args parentheses */
			+2	/* for the curly braces */
			+function_code_len);

	eval_code_length = sizeof("function " LAMBDA_TEMP_FUNCNAME "(") - 1;
	memcpy(eval_code, "function " LAMBDA_TEMP_FUNCNAME "(", eval_code_length);

	memcpy(eval_code + eval_code_length, function_args, function_args_len);
	eval_code_length += function_args_len;

	eval_code[eval_code_length++] = ')';
	eval_code[eval_code_length++] = '{';

	memcpy(eval_code + eval_code_length, function_code, function_code_len);
	eval_code_length += function_code_len;

	eval_code[eval_code_length++] = '}';
	eval_code[eval_code_length] = '\0';

	eval_name = gear_make_compiled_string_description("runtime-created function");
	retval = gear_eval_stringl(eval_code, eval_code_length, NULL, eval_name);
	efree(eval_code);
	efree(eval_name);

	if (retval==SUCCESS) {
		gear_op_array *func;
		HashTable *static_variables;

		func = gear_hash_str_find_ptr(EG(function_table), LAMBDA_TEMP_FUNCNAME, sizeof(LAMBDA_TEMP_FUNCNAME)-1);
		if (!func) {
			gear_error_noreturn(E_CORE_ERROR, "Unexpected inconsistency in create_function()");
			RETURN_FALSE;
		}
		if (func->refcount) {
			(*func->refcount)++;
		}
		static_variables = func->static_variables;
		func->static_variables = NULL;
		gear_hash_str_del(EG(function_table), LAMBDA_TEMP_FUNCNAME, sizeof(LAMBDA_TEMP_FUNCNAME)-1);
		func->static_variables = static_variables;

		function_name = gear_string_alloc(sizeof("0lambda_")+MAX_LENGTH_OF_LONG, 0);
		ZSTR_VAL(function_name)[0] = '\0';

		do {
			ZSTR_LEN(function_name) = snprintf(ZSTR_VAL(function_name) + 1, sizeof("lambda_")+MAX_LENGTH_OF_LONG, "lambda_%d", ++EG(lambda_count)) + 1;
		} while (gear_hash_add_ptr(EG(function_table), function_name, func) == NULL);
		RETURN_NEW_STR(function_name);
	} else {
		gear_hash_str_del(EG(function_table), LAMBDA_TEMP_FUNCNAME, sizeof(LAMBDA_TEMP_FUNCNAME)-1);
		RETURN_FALSE;
	}
}
/* }}} */

#if GEAR_DEBUG && defined(ZTS)
GEAR_FUNCTION(gear_thread_id)
{
	RETURN_LONG((gear_long)pbc_thread_id());
}
#endif

/* {{{ proto string get_resource_type(resource res)
   Get the resource type name for a given resource */
GEAR_FUNCTION(get_resource_type)
{
	const char *resource_type;
	zval *z_resource_type;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &z_resource_type) == FAILURE) {
		return;
	}

	resource_type = gear_rsrc_list_get_rsrc_type(Z_RES_P(z_resource_type));
	if (resource_type) {
		RETURN_STRING(resource_type);
	} else {
		RETURN_STRING("Unknown");
	}
}
/* }}} */

/* {{{ proto array get_resources([string resouce_type])
   Get an array with all active resources */
GEAR_FUNCTION(get_resources)
{
	gear_string *type = NULL;
	gear_string *key;
	gear_ulong index;
	zval *val;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|S", &type) == FAILURE) {
		return;
	}

	if (!type) {
		array_init(return_value);
		GEAR_HASH_FOREACH_KEY_VAL(&EG(regular_list), index, key, val) {
			if (!key) {
				Z_ADDREF_P(val);
				gear_hash_index_add_new(Z_ARRVAL_P(return_value), index, val);
			}
		} GEAR_HASH_FOREACH_END();
	} else if (gear_string_equals_literal(type, "Unknown")) {
		array_init(return_value);
		GEAR_HASH_FOREACH_KEY_VAL(&EG(regular_list), index, key, val) {
			if (!key && Z_RES_TYPE_P(val) <= 0) {
				Z_ADDREF_P(val);
				gear_hash_index_add_new(Z_ARRVAL_P(return_value), index, val);
			}
		} GEAR_HASH_FOREACH_END();
	} else {
		int id = gear_fetch_list_dtor_id(ZSTR_VAL(type));

		if (id <= 0) {
			gear_error(E_WARNING, "get_resources():  Unknown resource type '%s'", ZSTR_VAL(type));
			RETURN_FALSE;
		}

		array_init(return_value);
		GEAR_HASH_FOREACH_KEY_VAL(&EG(regular_list), index, key, val) {
			if (!key && Z_RES_TYPE_P(val) == id) {
				Z_ADDREF_P(val);
				gear_hash_index_add_new(Z_ARRVAL_P(return_value), index, val);
			}
		} GEAR_HASH_FOREACH_END();
	}
}
/* }}} */

static int add_extension_info(zval *item, void *arg) /* {{{ */
{
	zval *name_array = (zval *)arg;
	gear_capi_entry *cAPI = (gear_capi_entry*)Z_PTR_P(item);
	add_next_index_string(name_array, cAPI->name);
	return 0;
}
/* }}} */

static int add_gearext_info(gear_extension *ext, void *arg) /* {{{ */
{
	zval *name_array = (zval *)arg;
	add_next_index_string(name_array, ext->name);
	return 0;
}
/* }}} */

static int add_constant_info(zval *item, void *arg) /* {{{ */
{
	zval *name_array = (zval *)arg;
	gear_constant *constant = (gear_constant*)Z_PTR_P(item);
	zval const_val;

	if (!constant->name) {
		/* skip special constants */
		return 0;
	}

	ZVAL_COPY_OR_DUP(&const_val, &constant->value);
	gear_hash_add_new(Z_ARRVAL_P(name_array), constant->name, &const_val);
	return 0;
}
/* }}} */

/* {{{ proto array get_loaded_extensions([bool gear_extensions])
   Return an array containing names of loaded extensions */
GEAR_FUNCTION(get_loaded_extensions)
{
	gear_bool gearext = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|b", &gearext) == FAILURE) {
		return;
	}

	array_init(return_value);

	if (gearext) {
		gear_llist_apply_with_argument(&gear_extensions, (llist_apply_with_arg_func_t)add_gearext_info, return_value);
	} else {
		gear_hash_apply_with_argument(&capi_registry, add_extension_info, return_value);
	}
}
/* }}} */

/* {{{ proto array get_defined_constants([bool categorize])
   Return an array containing the names and values of all defined constants */
GEAR_FUNCTION(get_defined_constants)
{
	gear_bool categorize = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|b", &categorize) == FAILURE) {
		return;
	}

	array_init(return_value);

	if (categorize) {
		gear_constant *val;
		int capi_number;
		zval *cAPIs, const_val;
		char **capi_names;
		gear_capi_entry *cAPI;
		int i = 1;

		cAPIs = ecalloc(gear_hash_num_elements(&capi_registry) + 2, sizeof(zval));
		capi_names = emalloc((gear_hash_num_elements(&capi_registry) + 2) * sizeof(char *));

		capi_names[0] = "internal";
		GEAR_HASH_FOREACH_PTR(&capi_registry, cAPI) {
			capi_names[cAPI->capi_number] = (char *)cAPI->name;
			i++;
		} GEAR_HASH_FOREACH_END();
		capi_names[i] = "user";

		GEAR_HASH_FOREACH_PTR(EG(gear_constants), val) {
			if (!val->name) {
				/* skip special constants */
				continue;
			}

			if (GEAR_CONSTANT_CAPI_NUMBER(val) == HYSS_USER_CONSTANT) {
				capi_number = i;
			} else if (GEAR_CONSTANT_CAPI_NUMBER(val) > i) {
				/* should not happen */
				continue;
			} else {
				capi_number = GEAR_CONSTANT_CAPI_NUMBER(val);
			}

			if (Z_TYPE(cAPIs[capi_number]) == IS_UNDEF) {
				array_init(&cAPIs[capi_number]);
				add_assoc_zval(return_value, capi_names[capi_number], &cAPIs[capi_number]);
			}

			ZVAL_COPY_OR_DUP(&const_val, &val->value);
			gear_hash_add_new(Z_ARRVAL(cAPIs[capi_number]), val->name, &const_val);
		} GEAR_HASH_FOREACH_END();

		efree(capi_names);
		efree(cAPIs);
	} else {
		gear_hash_apply_with_argument(EG(gear_constants), add_constant_info, return_value);
	}
}
/* }}} */

static void debug_backtrace_get_args(gear_execute_data *call, zval *arg_array) /* {{{ */
{
	uint32_t num_args = GEAR_CALL_NUM_ARGS(call);

	if (num_args) {
		uint32_t i = 0;
		zval *p = GEAR_CALL_ARG(call, 1);

		array_init_size(arg_array, num_args);
		gear_hash_real_init_packed(Z_ARRVAL_P(arg_array));
		GEAR_HASH_FILL_PACKED(Z_ARRVAL_P(arg_array)) {
			if (call->func->type == GEAR_USER_FUNCTION) {
				uint32_t first_extra_arg = MIN(num_args, call->func->op_array.num_args);

				if (UNEXPECTED(GEAR_CALL_INFO(call) & GEAR_CALL_HAS_SYMBOL_TABLE)) {
					/* In case of attached symbol_table, values on stack may be invalid
					 * and we have to access them through symbol_table
					 */
					gear_string *arg_name;
					zval *arg;

					while (i < first_extra_arg) {
						arg_name = call->func->op_array.vars[i];
						arg = gear_hash_find_ex_ind(call->symbol_table, arg_name, 1);
						if (arg) {
							if (Z_OPT_REFCOUNTED_P(arg)) {
								Z_ADDREF_P(arg);
							}
							GEAR_HASH_FILL_ADD(arg);
						} else {
							GEAR_HASH_FILL_ADD(&EG(uninitialized_zval));
						}
						i++;
					}
				} else {
					while (i < first_extra_arg) {
						if (EXPECTED(Z_TYPE_INFO_P(p) != IS_UNDEF)) {
							if (Z_OPT_REFCOUNTED_P(p)) {
								Z_ADDREF_P(p);
							}
							GEAR_HASH_FILL_ADD(p);
						} else {
							GEAR_HASH_FILL_ADD(&EG(uninitialized_zval));
						}
						p++;
						i++;
					}
				}
				p = GEAR_CALL_VAR_NUM(call, call->func->op_array.last_var + call->func->op_array.T);
			}

			while (i < num_args) {
				if (EXPECTED(Z_TYPE_INFO_P(p) != IS_UNDEF)) {
					if (Z_OPT_REFCOUNTED_P(p)) {
						Z_ADDREF_P(p);
					}
					GEAR_HASH_FILL_ADD(p);
				} else {
					GEAR_HASH_FILL_ADD(&EG(uninitialized_zval));
				}
				p++;
				i++;
			}
		} GEAR_HASH_FILL_END();
		Z_ARRVAL_P(arg_array)->nNumOfElements = num_args;
	} else {
		ZVAL_EMPTY_ARRAY(arg_array);
	}
}
/* }}} */

void debug_print_backtrace_args(zval *arg_array) /* {{{ */
{
	zval *tmp;
	int i = 0;

	GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(arg_array), tmp) {
		if (i++) {
			GEAR_PUTS(", ");
		}
		gear_print_flat_zval_r(tmp);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto void debug_print_backtrace([int options[, int limit]]) */
GEAR_FUNCTION(debug_print_backtrace)
{
	gear_execute_data *call, *ptr, *skip;
	gear_object *object;
	int lineno, frameno = 0;
	gear_function *func;
	const char *function_name;
	const char *filename;
	gear_string *class_name = NULL;
	char *call_type;
	const char *include_filename = NULL;
	zval arg_array;
	int indent = 0;
	gear_long options = 0;
	gear_long limit = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|ll", &options, &limit) == FAILURE) {
		return;
	}

	ZVAL_UNDEF(&arg_array);
	ptr = EX(prev_execute_data);

	/* skip debug_backtrace() */
	call = ptr;
	ptr = ptr->prev_execute_data;

	while (ptr && (limit == 0 || frameno < limit)) {
		frameno++;
		class_name = NULL;
		call_type = NULL;
		ZVAL_UNDEF(&arg_array);

		ptr = gear_generator_check_placeholder_frame(ptr);

		skip = ptr;
		/* skip internal handler */
		if ((!skip->func || !GEAR_USER_CODE(skip->func->common.type)) &&
		    skip->prev_execute_data &&
		    skip->prev_execute_data->func &&
		    GEAR_USER_CODE(skip->prev_execute_data->func->common.type) &&
		    skip->prev_execute_data->opline->opcode != GEAR_DO_FCALL &&
		    skip->prev_execute_data->opline->opcode != GEAR_DO_ICALL &&
		    skip->prev_execute_data->opline->opcode != GEAR_DO_UCALL &&
		    skip->prev_execute_data->opline->opcode != GEAR_DO_FCALL_BY_NAME &&
		    skip->prev_execute_data->opline->opcode != GEAR_INCLUDE_OR_EVAL) {
			skip = skip->prev_execute_data;
		}

		if (skip->func && GEAR_USER_CODE(skip->func->common.type)) {
			filename = ZSTR_VAL(skip->func->op_array.filename);
			if (skip->opline->opcode == GEAR_HANDLE_EXCEPTION) {
				if (EG(opline_before_exception)) {
					lineno = EG(opline_before_exception)->lineno;
				} else {
					lineno = skip->func->op_array.line_end;
				}
			} else {
				lineno = skip->opline->lineno;
			}
		} else {
			filename = NULL;
			lineno = 0;
		}

		/* $this may be passed into regular internal functions */
		object = (Z_TYPE(call->This) == IS_OBJECT) ? Z_OBJ(call->This) : NULL;

		if (call->func) {
			gear_string *gear_function_name;

			func = call->func;
            if (func->common.scope && func->common.scope->trait_aliases) {
                gear_function_name = gear_resolve_method_name(object ? object->ce : func->common.scope, func);
            } else {
                gear_function_name = func->common.function_name;
            }
            if (gear_function_name != NULL) {
                function_name = ZSTR_VAL(gear_function_name);
            } else {
                function_name = NULL;
            }
		} else {
			func = NULL;
			function_name = NULL;
		}

		if (function_name) {
			if (object) {
				if (func->common.scope) {
					class_name = func->common.scope->name;
				} else if (object->handlers->get_class_name == gear_std_get_class_name) {
					class_name = object->ce->name;
				} else {
					class_name = object->handlers->get_class_name(object);
				}

				call_type = "->";
			} else if (func->common.scope) {
				class_name = func->common.scope->name;
				call_type = "::";
			} else {
				class_name = NULL;
				call_type = NULL;
			}
			if (func->type != GEAR_EVAL_CODE) {
				if ((options & DEBUG_BACKTRACE_IGNORE_ARGS) == 0) {
					debug_backtrace_get_args(call, &arg_array);
				}
			}
		} else {
			/* i know this is kinda ugly, but i'm trying to avoid extra cycles in the main execution loop */
			gear_bool build_filename_arg = 1;

			if (!ptr->func || !GEAR_USER_CODE(ptr->func->common.type) || ptr->opline->opcode != GEAR_INCLUDE_OR_EVAL) {
				/* can happen when calling eval from a custom sapi */
				function_name = "unknown";
				build_filename_arg = 0;
			} else
			switch (ptr->opline->extended_value) {
				case GEAR_EVAL:
					function_name = "eval";
					build_filename_arg = 0;
					break;
				case GEAR_INCLUDE:
					function_name = "include";
					break;
				case GEAR_REQUIRE:
					function_name = "require";
					break;
				case GEAR_INCLUDE_ONCE:
					function_name = "include_once";
					break;
				case GEAR_REQUIRE_ONCE:
					function_name = "require_once";
					break;
				default:
					/* this can actually happen if you use debug_backtrace() in your error_handler and
					 * you're in the top-scope */
					function_name = "unknown";
					build_filename_arg = 0;
					break;
			}

			if (build_filename_arg && include_filename) {
				array_init(&arg_array);
				add_next_index_string(&arg_array, (char*)include_filename);
			}
			call_type = NULL;
		}
		gear_printf("#%-2d ", indent);
		if (class_name) {
			GEAR_PUTS(ZSTR_VAL(class_name));
			GEAR_PUTS(call_type);
			if (object
			  && !func->common.scope
			  && object->handlers->get_class_name != gear_std_get_class_name) {
				gear_string_release_ex(class_name, 0);
			}
		}
		gear_printf("%s(", function_name);
		if (Z_TYPE(arg_array) != IS_UNDEF) {
			debug_print_backtrace_args(&arg_array);
			zval_ptr_dtor(&arg_array);
		}
		if (filename) {
			gear_printf(") called at [%s:%d]\n", filename, lineno);
		} else {
			gear_execute_data *prev_call = skip;
			gear_execute_data *prev = skip->prev_execute_data;

			while (prev) {
				if (prev_call &&
				    prev_call->func &&
					!GEAR_USER_CODE(prev_call->func->common.type)) {
					prev = NULL;
					break;
				}
				if (prev->func && GEAR_USER_CODE(prev->func->common.type)) {
					gear_printf(") called at [%s:%d]\n", ZSTR_VAL(prev->func->op_array.filename), prev->opline->lineno);
					break;
				}
				prev_call = prev;
				prev = prev->prev_execute_data;
			}
			if (!prev) {
				GEAR_PUTS(")\n");
			}
		}
		include_filename = filename;
		call = skip;
		ptr = skip->prev_execute_data;
		++indent;
	}
}

/* }}} */

GEAR_API void gear_fetch_debug_backtrace(zval *return_value, int skip_last, int options, int limit) /* {{{ */
{
	gear_execute_data *ptr, *skip, *call = NULL;
	gear_object *object;
	int lineno, frameno = 0;
	gear_function *func;
	gear_string *function_name;
	gear_string *filename;
	gear_string *include_filename = NULL;
	zval stack_frame, tmp;

	array_init(return_value);

	if (!(ptr = EG(current_execute_data))) {
		return;
	}

	if (!ptr->func || !GEAR_USER_CODE(ptr->func->common.type)) {
		call = ptr;
		ptr = ptr->prev_execute_data;
	}

	if (ptr) {
		if (skip_last) {
			/* skip debug_backtrace() */
			call = ptr;
			ptr = ptr->prev_execute_data;
		} else {
			/* skip "new Exception()" */
			if (ptr->func && GEAR_USER_CODE(ptr->func->common.type) && (ptr->opline->opcode == GEAR_NEW)) {
				call = ptr;
				ptr = ptr->prev_execute_data;
			}
		}
		if (!call) {
			call = ptr;
			ptr = ptr->prev_execute_data;
		}
	}

	while (ptr && (limit == 0 || frameno < limit)) {
		frameno++;
		array_init(&stack_frame);

		ptr = gear_generator_check_placeholder_frame(ptr);

		skip = ptr;
		/* skip internal handler */
		if ((!skip->func || !GEAR_USER_CODE(skip->func->common.type)) &&
		    skip->prev_execute_data &&
		    skip->prev_execute_data->func &&
		    GEAR_USER_CODE(skip->prev_execute_data->func->common.type) &&
		    skip->prev_execute_data->opline->opcode != GEAR_DO_FCALL &&
		    skip->prev_execute_data->opline->opcode != GEAR_DO_ICALL &&
		    skip->prev_execute_data->opline->opcode != GEAR_DO_UCALL &&
		    skip->prev_execute_data->opline->opcode != GEAR_DO_FCALL_BY_NAME &&
		    skip->prev_execute_data->opline->opcode != GEAR_INCLUDE_OR_EVAL) {
			skip = skip->prev_execute_data;
		}

		if (skip->func && GEAR_USER_CODE(skip->func->common.type)) {
			filename = skip->func->op_array.filename;
			if (skip->opline->opcode == GEAR_HANDLE_EXCEPTION) {
				if (EG(opline_before_exception)) {
					lineno = EG(opline_before_exception)->lineno;
				} else {
					lineno = skip->func->op_array.line_end;
				}
			} else {
				lineno = skip->opline->lineno;
			}
			ZVAL_STR_COPY(&tmp, filename);
			gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_FILE), &tmp);
			ZVAL_LONG(&tmp, lineno);
			gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_LINE), &tmp);

			/* try to fetch args only if an FCALL was just made - elsewise we're in the middle of a function
			 * and debug_baktrace() might have been called by the error_handler. in this case we don't
			 * want to pop anything of the argument-stack */
		} else {
			gear_execute_data *prev_call = skip;
			gear_execute_data *prev = skip->prev_execute_data;

			while (prev) {
				if (prev_call &&
				    prev_call->func &&
					!GEAR_USER_CODE(prev_call->func->common.type) &&
					!(prev_call->func->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE)) {
					break;
				}
				if (prev->func && GEAR_USER_CODE(prev->func->common.type)) {
					ZVAL_STR_COPY(&tmp, prev->func->op_array.filename);
					gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_FILE), &tmp);
					ZVAL_LONG(&tmp, prev->opline->lineno);
					gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_LINE), &tmp);
					break;
				}
				prev_call = prev;
				prev = prev->prev_execute_data;
			}
			filename = NULL;
		}

		/* $this may be passed into regular internal functions */
		object = (call && (Z_TYPE(call->This) == IS_OBJECT)) ? Z_OBJ(call->This) : NULL;

		if (call && call->func) {
			func = call->func;
			function_name = (func->common.scope &&
			                 func->common.scope->trait_aliases) ?
				gear_resolve_method_name(
					(object ? object->ce : func->common.scope), func) :
				func->common.function_name;
		} else {
			func = NULL;
			function_name = NULL;
		}

		if (function_name) {
			ZVAL_STR_COPY(&tmp, function_name);
			gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_FUNCTION), &tmp);

			if (object) {
				if (func->common.scope) {
					ZVAL_STR_COPY(&tmp, func->common.scope->name);
				} else if (object->handlers->get_class_name == gear_std_get_class_name) {
					ZVAL_STR_COPY(&tmp, object->ce->name);
				} else {
					ZVAL_STR(&tmp, object->handlers->get_class_name(object));
				}
				gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_CLASS), &tmp);
				if ((options & DEBUG_BACKTRACE_PROVIDE_OBJECT) != 0) {
					ZVAL_OBJ(&tmp, object);
					gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_OBJECT), &tmp);
					Z_ADDREF(tmp);
				}

				ZVAL_INTERNED_STR(&tmp, ZSTR_KNOWN(GEAR_STR_OBJECT_OPERATOR));
				gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_TYPE), &tmp);
			} else if (func->common.scope) {
				ZVAL_STR_COPY(&tmp, func->common.scope->name);
				gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_CLASS), &tmp);
				ZVAL_INTERNED_STR(&tmp, ZSTR_KNOWN(GEAR_STR_PAAMAYIM_NEKUDOTAYIM));
				gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_TYPE), &tmp);
			}

			if ((options & DEBUG_BACKTRACE_IGNORE_ARGS) == 0 &&
				func->type != GEAR_EVAL_CODE) {

				debug_backtrace_get_args(call, &tmp);
				gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_ARGS), &tmp);
			}
		} else {
			/* i know this is kinda ugly, but i'm trying to avoid extra cycles in the main execution loop */
			gear_bool build_filename_arg = 1;
			gear_string *pseudo_function_name;

			if (!ptr->func || !GEAR_USER_CODE(ptr->func->common.type) || ptr->opline->opcode != GEAR_INCLUDE_OR_EVAL) {
				/* can happen when calling eval from a custom sapi */
				pseudo_function_name = ZSTR_KNOWN(GEAR_STR_UNKNOWN);
				build_filename_arg = 0;
			} else
			switch (ptr->opline->extended_value) {
				case GEAR_EVAL:
					pseudo_function_name = ZSTR_KNOWN(GEAR_STR_EVAL);
					build_filename_arg = 0;
					break;
				case GEAR_INCLUDE:
					pseudo_function_name = ZSTR_KNOWN(GEAR_STR_INCLUDE);
					break;
				case GEAR_REQUIRE:
					pseudo_function_name = ZSTR_KNOWN(GEAR_STR_REQUIRE);
					break;
				case GEAR_INCLUDE_ONCE:
					pseudo_function_name = ZSTR_KNOWN(GEAR_STR_INCLUDE_ONCE);
					break;
				case GEAR_REQUIRE_ONCE:
					pseudo_function_name = ZSTR_KNOWN(GEAR_STR_REQUIRE_ONCE);
					break;
				default:
					/* this can actually happen if you use debug_backtrace() in your error_handler and
					 * you're in the top-scope */
					pseudo_function_name = ZSTR_KNOWN(GEAR_STR_UNKNOWN);
					build_filename_arg = 0;
					break;
			}

			if (build_filename_arg && include_filename) {
				zval arg_array;

				array_init(&arg_array);

				/* include_filename always points to the last filename of the last last called-function.
				   if we have called include in the frame above - this is the file we have included.
				 */

				ZVAL_STR_COPY(&tmp, include_filename);
				gear_hash_next_index_insert_new(Z_ARRVAL(arg_array), &tmp);
				gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_ARGS), &arg_array);
			}

			ZVAL_INTERNED_STR(&tmp, pseudo_function_name);
			gear_hash_add_new(Z_ARRVAL(stack_frame), ZSTR_KNOWN(GEAR_STR_FUNCTION), &tmp);
		}

		gear_hash_next_index_insert_new(Z_ARRVAL_P(return_value), &stack_frame);

		include_filename = filename;

		call = skip;
		ptr = skip->prev_execute_data;
	}
}
/* }}} */

/* {{{ proto array debug_backtrace([int options[, int limit]])
   Return backtrace as array */
GEAR_FUNCTION(debug_backtrace)
{
	gear_long options = DEBUG_BACKTRACE_PROVIDE_OBJECT;
	gear_long limit = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|ll", &options, &limit) == FAILURE) {
		return;
	}

	gear_fetch_debug_backtrace(return_value, 1, options, limit);
}
/* }}} */

/* {{{ proto bool extension_loaded(string extension_name)
   Returns true if the named extension is loaded */
GEAR_FUNCTION(extension_loaded)
{
	gear_string *extension_name;
	gear_string *lcname;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &extension_name) == FAILURE) {
		return;
	}

	lcname = gear_string_tolower(extension_name);
	if (gear_hash_exists(&capi_registry, lcname)) {
		RETVAL_TRUE;
	} else {
		RETVAL_FALSE;
	}
	gear_string_release_ex(lcname, 0);
}
/* }}} */

/* {{{ proto array get_extension_funcs(string extension_name)
   Returns an array with the names of functions belonging to the named extension */
GEAR_FUNCTION(get_extension_funcs)
{
	gear_string *extension_name;
	gear_string *lcname;
	int array;
	gear_capi_entry *cAPI;
	gear_function *zif;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &extension_name) == FAILURE) {
		return;
	}
	if (strncasecmp(ZSTR_VAL(extension_name), "gear", sizeof("gear"))) {
		lcname = gear_string_tolower(extension_name);
		cAPI = gear_hash_find_ptr(&capi_registry, lcname);
		gear_string_release_ex(lcname, 0);
	} else {
		cAPI = gear_hash_str_find_ptr(&capi_registry, "core", sizeof("core") - 1);
	}

	if (!cAPI) {
		RETURN_FALSE;
	}

	if (cAPI->functions) {
		/* avoid BC break, if functions list is empty, will return an empty array */
		array_init(return_value);
		array = 1;
	} else {
		array = 0;
	}

	GEAR_HASH_FOREACH_PTR(CG(function_table), zif) {
		if (zif->common.type == GEAR_INTERNAL_FUNCTION
			&& zif->internal_function.cAPI == cAPI) {
			if (!array) {
				array_init(return_value);
				array = 1;
			}
			add_next_index_str(return_value, gear_string_copy(zif->common.function_name));
		}
	} GEAR_HASH_FOREACH_END();

	if (!array) {
		RETURN_FALSE;
	}
}
/* }}} */

