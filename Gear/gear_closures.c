/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"
#include "gear_closures.h"
#include "gear_exceptions.h"
#include "gear_interfaces.h"
#include "gear_objects.h"
#include "gear_objects_API.h"
#include "gear_globals.h"

#define GEAR_CLOSURE_PRINT_NAME "Closure object"

#define GEAR_CLOSURE_PROPERTY_ERROR() \
	gear_throw_error(NULL, "Closure object cannot have properties")

typedef struct _gear_closure {
	gear_object       std;
	gear_function     func;
	zval              this_ptr;
	gear_class_entry *called_scope;
	zif_handler       orig_internal_handler;
} gear_closure;

/* non-static since it needs to be referenced */
GEAR_API gear_class_entry *gear_ce_closure;
static gear_object_handlers closure_handlers;

GEAR_METHOD(Closure, __invoke) /* {{{ */
{
	gear_function *func = EX(func);
	zval *arguments = GEAR_CALL_ARG(execute_data, 1);

	if (call_user_function(CG(function_table), NULL, getThis(), return_value, GEAR_NUM_ARGS(), arguments) == FAILURE) {
		RETVAL_FALSE;
	}

	/* destruct the function also, then - we have allocated it in get_method */
	gear_string_release_ex(func->internal_function.function_name, 0);
	efree(func);
#if GEAR_DEBUG
	execute_data->func = NULL;
#endif
}
/* }}} */

static gear_bool gear_valid_closure_binding(
		gear_closure *closure, zval *newthis, gear_class_entry *scope) /* {{{ */
{
	gear_function *func = &closure->func;
	gear_bool is_fake_closure = (func->common.fn_flags & GEAR_ACC_FAKE_CLOSURE) != 0;
	if (newthis) {
		if (func->common.fn_flags & GEAR_ACC_STATIC) {
			gear_error(E_WARNING, "Cannot bind an instance to a static closure");
			return 0;
		}

		if (is_fake_closure && func->common.scope &&
				!instanceof_function(Z_OBJCE_P(newthis), func->common.scope)) {
			/* Binding incompatible $this to an internal method is not supported. */
			gear_error(E_WARNING, "Cannot bind method %s::%s() to object of class %s",
					ZSTR_VAL(func->common.scope->name),
					ZSTR_VAL(func->common.function_name),
					ZSTR_VAL(Z_OBJCE_P(newthis)->name));
			return 0;
		}
	} else if (!(func->common.fn_flags & GEAR_ACC_STATIC) && func->common.scope
			&& func->type == GEAR_INTERNAL_FUNCTION) {
		gear_error(E_WARNING, "Cannot unbind $this of internal method");
		return 0;
	}

	if (scope && scope != func->common.scope && scope->type == GEAR_INTERNAL_CLASS) {
		/* rebinding to internal class is not allowed */
		gear_error(E_WARNING, "Cannot bind closure to scope of internal class %s",
				ZSTR_VAL(scope->name));
		return 0;
	}

	if (is_fake_closure && scope != func->common.scope) {
		gear_error(E_WARNING, "Cannot rebind scope of closure created by ReflectionFunctionAbstract::getClosure()");
		return 0;
	}

	return 1;
}
/* }}} */

/* {{{ proto mixed Closure::call(object to [, mixed parameter] [, mixed ...] )
   Call closure, binding to a given object with its class as the scope */
GEAR_METHOD(Closure, call)
{
	zval *zclosure, *newthis, closure_result;
	gear_closure *closure;
	gear_fcall_info fci;
	gear_fcall_info_cache fci_cache;
	gear_function my_function;
	gear_object *newobj;

	fci.param_count = 0;
	fci.params = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "o*", &newthis, &fci.params, &fci.param_count) == FAILURE) {
		return;
	}

	zclosure = getThis();
	closure = (gear_closure *) Z_OBJ_P(zclosure);

	newobj = Z_OBJ_P(newthis);

	if (!gear_valid_closure_binding(closure, newthis, Z_OBJCE_P(newthis))) {
		return;
	}

	if (closure->func.common.fn_flags & GEAR_ACC_GENERATOR) {
		zval new_closure;
		gear_create_closure(&new_closure, &closure->func, Z_OBJCE_P(newthis), closure->called_scope, newthis);
		closure = (gear_closure *) Z_OBJ(new_closure);
		fci_cache.function_handler = &closure->func;
	} else {
		memcpy(&my_function, &closure->func, closure->func.type == GEAR_USER_FUNCTION ? sizeof(gear_op_array) : sizeof(gear_internal_function));
		my_function.common.fn_flags &= ~GEAR_ACC_CLOSURE;
		/* use scope of passed object */
		my_function.common.scope = Z_OBJCE_P(newthis);
		fci_cache.function_handler = &my_function;

		/* Runtime cache relies on bound scope to be immutable, hence we need a separate rt cache in case scope changed */
		if (GEAR_USER_CODE(my_function.type) && closure->func.common.scope != Z_OBJCE_P(newthis)) {
			my_function.op_array.run_time_cache = emalloc(my_function.op_array.cache_size);
			memset(my_function.op_array.run_time_cache, 0, my_function.op_array.cache_size);
		}
	}

	fci_cache.called_scope = newobj->ce;
	fci_cache.object = fci.object = newobj;

	fci.size = sizeof(fci);
	ZVAL_COPY_VALUE(&fci.function_name, zclosure);
	fci.retval = &closure_result;
	fci.no_separation = 1;

	if (gear_call_function(&fci, &fci_cache) == SUCCESS && Z_TYPE(closure_result) != IS_UNDEF) {
		if (Z_ISREF(closure_result)) {
			gear_unwrap_reference(&closure_result);
		}
		ZVAL_COPY_VALUE(return_value, &closure_result);
	}

	if (fci_cache.function_handler->common.fn_flags & GEAR_ACC_GENERATOR) {
		/* copied upon generator creation */
		GC_DELREF(&closure->std);
	} else if (GEAR_USER_CODE(my_function.type) && closure->func.common.scope != Z_OBJCE_P(newthis)) {
		efree(my_function.op_array.run_time_cache);
	}
}
/* }}} */

/* {{{ proto Closure Closure::bind(callable old, object to [, mixed scope])
   Create a closure from another one and bind to another object and scope */
GEAR_METHOD(Closure, bind)
{
	zval *newthis, *zclosure, *scope_arg = NULL;
	gear_closure *closure;
	gear_class_entry *ce, *called_scope;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oo!|z", &zclosure, gear_ce_closure, &newthis, &scope_arg) == FAILURE) {
		return;
	}

	closure = (gear_closure *)Z_OBJ_P(zclosure);

	if (scope_arg != NULL) { /* scope argument was given */
		if (Z_TYPE_P(scope_arg) == IS_OBJECT) {
			ce = Z_OBJCE_P(scope_arg);
		} else if (Z_TYPE_P(scope_arg) == IS_NULL) {
			ce = NULL;
		} else {
			gear_string *tmp_class_name;
			gear_string *class_name = zval_get_tmp_string(scope_arg, &tmp_class_name);
			if (gear_string_equals_literal(class_name, "static")) {
				ce = closure->func.common.scope;
			} else if ((ce = gear_lookup_class_ex(class_name, NULL, 1)) == NULL) {
				gear_error(E_WARNING, "Class '%s' not found", ZSTR_VAL(class_name));
				gear_string_release_ex(class_name, 0);
				RETURN_NULL();
			}
			gear_tmp_string_release(tmp_class_name);
		}
	} else { /* scope argument not given; do not change the scope by default */
		ce = closure->func.common.scope;
	}

	if (!gear_valid_closure_binding(closure, newthis, ce)) {
		return;
	}

	if (newthis) {
		called_scope = Z_OBJCE_P(newthis);
	} else {
		called_scope = ce;
	}

	gear_create_closure(return_value, &closure->func, ce, called_scope, newthis);
}
/* }}} */

static GEAR_NAMED_FUNCTION(gear_closure_call_magic) /* {{{ */ {
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;
	zval params[2];

	memset(&fci, 0, sizeof(gear_fcall_info));
	memset(&fcc, 0, sizeof(gear_fcall_info_cache));

	fci.size = sizeof(gear_fcall_info);
	fci.retval = return_value;

	fcc.function_handler = (EX(func)->internal_function.fn_flags & GEAR_ACC_STATIC) ?
		EX(func)->internal_function.scope->__callstatic : EX(func)->internal_function.scope->__call;
	fci.params = params;
	fci.param_count = 2;
	ZVAL_STR(&fci.params[0], EX(func)->common.function_name);
	if (GEAR_NUM_ARGS()) {
		array_init_size(&fci.params[1], GEAR_NUM_ARGS());
		gear_copy_parameters_array(GEAR_NUM_ARGS(), &fci.params[1]);
	} else {
		ZVAL_EMPTY_ARRAY(&fci.params[1]);
	}

	fci.object = Z_OBJ(EX(This));
	fcc.object = Z_OBJ(EX(This));

	gear_call_function(&fci, &fcc);

	zval_ptr_dtor(&fci.params[0]);
	zval_ptr_dtor(&fci.params[1]);
}
/* }}} */

static int gear_create_closure_from_callable(zval *return_value, zval *callable, char **error) /* {{{ */ {
	gear_fcall_info_cache fcc;
	gear_function *mptr;
	zval instance;
	gear_internal_function call;

	if (!gear_is_callable_ex(callable, NULL, 0, NULL, &fcc, error)) {
		return FAILURE;
	}

	mptr = fcc.function_handler;
	if (mptr->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) {
		memset(&call, 0, sizeof(gear_internal_function));

		call.type = GEAR_INTERNAL_FUNCTION;
		call.fn_flags = mptr->common.fn_flags & GEAR_ACC_STATIC;
		call.handler = gear_closure_call_magic;
		call.function_name = mptr->common.function_name;
		call.scope = mptr->common.scope;

		gear_free_trampoline(mptr);
		mptr = (gear_function *) &call;
	}

	if (fcc.object) {
		ZVAL_OBJ(&instance, fcc.object);
		gear_create_fake_closure(return_value, mptr, mptr->common.scope, fcc.called_scope, &instance);
	} else {
		gear_create_fake_closure(return_value, mptr, mptr->common.scope, fcc.called_scope, NULL);
	}

	return SUCCESS;
}
/* }}} */

/* {{{ proto Closure Closure::fromCallable(callable callable)
   Create a closure from a callable using the current scope. */
GEAR_METHOD(Closure, fromCallable)
{
	zval *callable;
	int success;
	char *error = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "z", &callable) == FAILURE) {
		return;
	}

	if (Z_TYPE_P(callable) == IS_OBJECT && instanceof_function(Z_OBJCE_P(callable), gear_ce_closure)) {
		/* It's already a closure */
		RETURN_ZVAL(callable, 1, 0);
	}

	/* create closure as if it were called from parent scope */
	EG(current_execute_data) = EX(prev_execute_data);
	success = gear_create_closure_from_callable(return_value, callable, &error);
	EG(current_execute_data) = execute_data;

	if (success == FAILURE || error) {
		if (error) {
			gear_throw_exception_ex(gear_ce_type_error, 0, "Failed to create closure from callable: %s", error);
			efree(error);
		} else {
			gear_throw_exception_ex(gear_ce_type_error, 0, "Failed to create closure from callable");
		}
	}
}
/* }}} */

static GEAR_COLD gear_function *gear_closure_get_constructor(gear_object *object) /* {{{ */
{
	gear_throw_error(NULL, "Instantiation of 'Closure' is not allowed");
	return NULL;
}
/* }}} */

static int gear_closure_compare_objects(zval *o1, zval *o2) /* {{{ */
{
	return (Z_OBJ_P(o1) != Z_OBJ_P(o2));
}
/* }}} */

GEAR_API gear_function *gear_get_closure_invoke_method(gear_object *object) /* {{{ */
{
	gear_closure *closure = (gear_closure *)object;
	gear_function *invoke = (gear_function*)emalloc(sizeof(gear_function));
	const uint32_t keep_flags =
		GEAR_ACC_RETURN_REFERENCE | GEAR_ACC_VARIADIC | GEAR_ACC_HAS_RETURN_TYPE;

	invoke->common = closure->func.common;
	/* We return GEAR_INTERNAL_FUNCTION, but arg_info representation is the
	 * same as for GEAR_USER_FUNCTION (uses gear_string* instead of char*).
	 * This is not a problem, because GEAR_ACC_HAS_TYPE_HINTS is never set,
	 * and we won't check arguments on internal function. We also set
	 * GEAR_ACC_USER_ARG_INFO flag to prevent invalid usage by Reflection */
	invoke->type = GEAR_INTERNAL_FUNCTION;
	invoke->internal_function.fn_flags =
		GEAR_ACC_PUBLIC | GEAR_ACC_CALL_VIA_HANDLER | (closure->func.common.fn_flags & keep_flags);
	if (closure->func.type != GEAR_INTERNAL_FUNCTION || (closure->func.common.fn_flags & GEAR_ACC_USER_ARG_INFO)) {
		invoke->internal_function.fn_flags |=
			GEAR_ACC_USER_ARG_INFO;
	}
	invoke->internal_function.handler = GEAR_MN(Closure___invoke);
	invoke->internal_function.cAPI = 0;
	invoke->internal_function.scope = gear_ce_closure;
	invoke->internal_function.function_name = ZSTR_KNOWN(GEAR_STR_MAGIC_INVOKE);
	return invoke;
}
/* }}} */

GEAR_API const gear_function *gear_get_closure_method_def(zval *obj) /* {{{ */
{
	gear_closure *closure = (gear_closure *)Z_OBJ_P(obj);
	return &closure->func;
}
/* }}} */

GEAR_API zval* gear_get_closure_this_ptr(zval *obj) /* {{{ */
{
	gear_closure *closure = (gear_closure *)Z_OBJ_P(obj);
	return &closure->this_ptr;
}
/* }}} */

static gear_function *gear_closure_get_method(gear_object **object, gear_string *method, const zval *key) /* {{{ */
{
	if (gear_string_equals_literal_ci(method, GEAR_INVOKE_FUNC_NAME)) {
		return gear_get_closure_invoke_method(*object);
	}

	return gear_std_get_method(object, method, key);
}
/* }}} */

static zval *gear_closure_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv) /* {{{ */
{
	GEAR_CLOSURE_PROPERTY_ERROR();
	return &EG(uninitialized_zval);
}
/* }}} */

static void gear_closure_write_property(zval *object, zval *member, zval *value, void **cache_slot) /* {{{ */
{
	GEAR_CLOSURE_PROPERTY_ERROR();
}
/* }}} */

static zval *gear_closure_get_property_ptr_ptr(zval *object, zval *member, int type, void **cache_slot) /* {{{ */
{
	GEAR_CLOSURE_PROPERTY_ERROR();
	return NULL;
}
/* }}} */

static int gear_closure_has_property(zval *object, zval *member, int has_set_exists, void **cache_slot) /* {{{ */
{
	if (has_set_exists != GEAR_PROPERTY_EXISTS) {
		GEAR_CLOSURE_PROPERTY_ERROR();
	}
	return 0;
}
/* }}} */

static void gear_closure_unset_property(zval *object, zval *member, void **cache_slot) /* {{{ */
{
	GEAR_CLOSURE_PROPERTY_ERROR();
}
/* }}} */

static void gear_closure_free_storage(gear_object *object) /* {{{ */
{
	gear_closure *closure = (gear_closure *)object;

	gear_object_std_dtor(&closure->std);

	if (closure->func.type == GEAR_USER_FUNCTION) {
		if (closure->func.op_array.fn_flags & GEAR_ACC_NO_RT_ARENA) {
			efree(closure->func.op_array.run_time_cache);
			closure->func.op_array.run_time_cache = NULL;
		}
		destroy_op_array(&closure->func.op_array);
	}

	if (Z_TYPE(closure->this_ptr) != IS_UNDEF) {
		zval_ptr_dtor(&closure->this_ptr);
	}
}
/* }}} */

static gear_object *gear_closure_new(gear_class_entry *class_type) /* {{{ */
{
	gear_closure *closure;

	closure = emalloc(sizeof(gear_closure));
	memset(closure, 0, sizeof(gear_closure));

	gear_object_std_init(&closure->std, class_type);
	closure->std.handlers = &closure_handlers;

	return (gear_object*)closure;
}
/* }}} */

static gear_object *gear_closure_clone(zval *zobject) /* {{{ */
{
	gear_closure *closure = (gear_closure *)Z_OBJ_P(zobject);
	zval result;

	gear_create_closure(&result, &closure->func,
		closure->func.common.scope, closure->called_scope, &closure->this_ptr);
	return Z_OBJ(result);
}
/* }}} */

int gear_closure_get_closure(zval *obj, gear_class_entry **ce_ptr, gear_function **fptr_ptr, gear_object **obj_ptr) /* {{{ */
{
	gear_closure *closure = (gear_closure *)Z_OBJ_P(obj);
	*fptr_ptr = &closure->func;
	*ce_ptr = closure->called_scope;

	if (Z_TYPE(closure->this_ptr) != IS_UNDEF) {
		*obj_ptr = Z_OBJ(closure->this_ptr);
	} else {
		*obj_ptr = NULL;
	}

	return SUCCESS;
}
/* }}} */

static HashTable *gear_closure_get_debug_info(zval *object, int *is_temp) /* {{{ */
{
	gear_closure *closure = (gear_closure *)Z_OBJ_P(object);
	zval val;
	struct _gear_arg_info *arg_info = closure->func.common.arg_info;
	HashTable *debug_info;
	gear_bool zstr_args = (closure->func.type == GEAR_USER_FUNCTION) || (closure->func.common.fn_flags & GEAR_ACC_USER_ARG_INFO);

	*is_temp = 1;

	debug_info = gear_new_array(8);

	if (closure->func.type == GEAR_USER_FUNCTION && closure->func.op_array.static_variables) {
		HashTable *static_variables = closure->func.op_array.static_variables;
		ZVAL_ARR(&val, gear_array_dup(static_variables));
		gear_hash_update(debug_info, ZSTR_KNOWN(GEAR_STR_STATIC), &val);
	}

	if (Z_TYPE(closure->this_ptr) != IS_UNDEF) {
		Z_ADDREF(closure->this_ptr);
		gear_hash_update(debug_info, ZSTR_KNOWN(GEAR_STR_THIS), &closure->this_ptr);
	}

	if (arg_info &&
		(closure->func.common.num_args ||
		 (closure->func.common.fn_flags & GEAR_ACC_VARIADIC))) {
		uint32_t i, num_args, required = closure->func.common.required_num_args;

		array_init(&val);

		num_args = closure->func.common.num_args;
		if (closure->func.common.fn_flags & GEAR_ACC_VARIADIC) {
			num_args++;
		}
		for (i = 0; i < num_args; i++) {
			gear_string *name;
			zval info;
			if (arg_info->name) {
				if (zstr_args) {
					name = gear_strpprintf(0, "%s$%s",
							arg_info->pass_by_reference ? "&" : "",
							ZSTR_VAL(arg_info->name));
				} else {
					name = gear_strpprintf(0, "%s$%s",
							arg_info->pass_by_reference ? "&" : "",
							((gear_internal_arg_info*)arg_info)->name);
				}
			} else {
				name = gear_strpprintf(0, "%s$param%d",
						arg_info->pass_by_reference ? "&" : "",
						i + 1);
			}
			ZVAL_NEW_STR(&info, gear_strpprintf(0, "%s", i >= required ? "<optional>" : "<required>"));
			gear_hash_update(Z_ARRVAL(val), name, &info);
			gear_string_release_ex(name, 0);
			arg_info++;
		}
		gear_hash_str_update(debug_info, "parameter", sizeof("parameter")-1, &val);
	}

	return debug_info;
}
/* }}} */

static HashTable *gear_closure_get_gc(zval *obj, zval **table, int *n) /* {{{ */
{
	gear_closure *closure = (gear_closure *)Z_OBJ_P(obj);

	*table = Z_TYPE(closure->this_ptr) != IS_NULL ? &closure->this_ptr : NULL;
	*n = Z_TYPE(closure->this_ptr) != IS_NULL ? 1 : 0;
	return (closure->func.type == GEAR_USER_FUNCTION) ?
		closure->func.op_array.static_variables : NULL;
}
/* }}} */

/* {{{ proto Closure::__construct()
   Private constructor preventing instantiation */
GEAR_COLD GEAR_METHOD(Closure, __construct)
{
	gear_throw_error(NULL, "Instantiation of 'Closure' is not allowed");
}
/* }}} */

GEAR_BEGIN_ARG_INFO_EX(arginfo_closure_bindto, 0, 0, 1)
	GEAR_ARG_INFO(0, newthis)
	GEAR_ARG_INFO(0, newscope)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_closure_bind, 0, 0, 2)
	GEAR_ARG_INFO(0, closure)
	GEAR_ARG_INFO(0, newthis)
	GEAR_ARG_INFO(0, newscope)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_closure_call, 0, 0, 1)
	GEAR_ARG_INFO(0, newthis)
	GEAR_ARG_VARIADIC_INFO(0, parameters)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_closure_fromcallable, 0, 0, 1)
	GEAR_ARG_INFO(0, callable)
GEAR_END_ARG_INFO()

static const gear_function_entry closure_functions[] = {
	GEAR_ME(Closure, __construct, NULL, GEAR_ACC_PRIVATE)
	GEAR_ME(Closure, bind, arginfo_closure_bind, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	GEAR_MALIAS(Closure, bindTo, bind, arginfo_closure_bindto, GEAR_ACC_PUBLIC)
	GEAR_ME(Closure, call, arginfo_closure_call, GEAR_ACC_PUBLIC)
	GEAR_ME(Closure, fromCallable, arginfo_closure_fromcallable, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	GEAR_FE_END
};

void gear_register_closure_ce(void) /* {{{ */
{
	gear_class_entry ce;

	INIT_CLASS_ENTRY(ce, "Closure", closure_functions);
	gear_ce_closure = gear_register_internal_class(&ce);
	gear_ce_closure->ce_flags |= GEAR_ACC_FINAL;
	gear_ce_closure->create_object = gear_closure_new;
	gear_ce_closure->serialize = gear_class_serialize_deny;
	gear_ce_closure->unserialize = gear_class_unserialize_deny;

	memcpy(&closure_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	closure_handlers.free_obj = gear_closure_free_storage;
	closure_handlers.get_constructor = gear_closure_get_constructor;
	closure_handlers.get_method = gear_closure_get_method;
	closure_handlers.write_property = gear_closure_write_property;
	closure_handlers.read_property = gear_closure_read_property;
	closure_handlers.get_property_ptr_ptr = gear_closure_get_property_ptr_ptr;
	closure_handlers.has_property = gear_closure_has_property;
	closure_handlers.unset_property = gear_closure_unset_property;
	closure_handlers.compare_objects = gear_closure_compare_objects;
	closure_handlers.clone_obj = gear_closure_clone;
	closure_handlers.get_debug_info = gear_closure_get_debug_info;
	closure_handlers.get_closure = gear_closure_get_closure;
	closure_handlers.get_gc = gear_closure_get_gc;
}
/* }}} */

static GEAR_NAMED_FUNCTION(gear_closure_internal_handler) /* {{{ */
{
	gear_closure *closure = (gear_closure*)GEAR_CLOSURE_OBJECT(EX(func));
	closure->orig_internal_handler(INTERNAL_FUNCTION_PARAM_PASSTHRU);
	OBJ_RELEASE((gear_object*)closure);
	EX(func) = NULL;
}
/* }}} */

GEAR_API void gear_create_closure(zval *res, gear_function *func, gear_class_entry *scope, gear_class_entry *called_scope, zval *this_ptr) /* {{{ */
{
	gear_closure *closure;

	object_init_ex(res, gear_ce_closure);

	closure = (gear_closure *)Z_OBJ_P(res);

	if ((scope == NULL) && this_ptr && (Z_TYPE_P(this_ptr) != IS_UNDEF)) {
		/* use dummy scope if we're binding an object without specifying a scope */
		/* maybe it would be better to create one for this purpose */
		scope = gear_ce_closure;
	}

	if (func->type == GEAR_USER_FUNCTION) {
		memcpy(&closure->func, func, sizeof(gear_op_array));
		closure->func.common.fn_flags |= GEAR_ACC_CLOSURE;
		if (closure->func.op_array.static_variables) {
			closure->func.op_array.static_variables =
				gear_array_dup(closure->func.op_array.static_variables);
		}

		/* Runtime cache is scope-dependent, so we cannot reuse it if the scope changed */
		if (!closure->func.op_array.run_time_cache
			|| func->common.scope != scope
			|| (func->common.fn_flags & GEAR_ACC_NO_RT_ARENA)
		) {
			if (!func->op_array.run_time_cache && (func->common.fn_flags & GEAR_ACC_CLOSURE)) {
				/* If a real closure is used for the first time, we create a shared runtime cache
				 * and remember which scope it is for. */
				func->common.scope = scope;
				func->op_array.run_time_cache = gear_arena_alloc(&CG(arena), func->op_array.cache_size);
				closure->func.op_array.run_time_cache = func->op_array.run_time_cache;
			} else {
				/* Otherwise, we use a non-shared runtime cache */
				closure->func.op_array.run_time_cache = emalloc(func->op_array.cache_size);
				closure->func.op_array.fn_flags |= GEAR_ACC_NO_RT_ARENA;
			}
			memset(closure->func.op_array.run_time_cache, 0, func->op_array.cache_size);
		}
		if (closure->func.op_array.refcount) {
			(*closure->func.op_array.refcount)++;
		}
	} else {
		memcpy(&closure->func, func, sizeof(gear_internal_function));
		closure->func.common.fn_flags |= GEAR_ACC_CLOSURE;
		/* wrap internal function handler to avoid memory leak */
		if (UNEXPECTED(closure->func.internal_function.handler == gear_closure_internal_handler)) {
			/* avoid infinity recursion, by taking handler from nested closure */
			gear_closure *nested = (gear_closure*)((char*)func - XtOffsetOf(gear_closure, func));
			GEAR_ASSERT(nested->std.ce == gear_ce_closure);
			closure->orig_internal_handler = nested->orig_internal_handler;
		} else {
			closure->orig_internal_handler = closure->func.internal_function.handler;
		}
		closure->func.internal_function.handler = gear_closure_internal_handler;
		if (!func->common.scope) {
			/* if it's a free function, we won't set scope & this since they're meaningless */
			this_ptr = NULL;
			scope = NULL;
		}
	}

	ZVAL_UNDEF(&closure->this_ptr);
	/* Invariant:
	 * If the closure is unscoped or static, it has no bound object. */
	closure->func.common.scope = scope;
	closure->called_scope = called_scope;
	if (scope) {
		closure->func.common.fn_flags |= GEAR_ACC_PUBLIC;
		if (this_ptr && Z_TYPE_P(this_ptr) == IS_OBJECT && (closure->func.common.fn_flags & GEAR_ACC_STATIC) == 0) {
			ZVAL_COPY(&closure->this_ptr, this_ptr);
		}
	}
}
/* }}} */

GEAR_API void gear_create_fake_closure(zval *res, gear_function *func, gear_class_entry *scope, gear_class_entry *called_scope, zval *this_ptr) /* {{{ */
{
	gear_closure *closure;

	gear_create_closure(res, func, scope, called_scope, this_ptr);

	closure = (gear_closure *)Z_OBJ_P(res);
	closure->func.common.fn_flags |= GEAR_ACC_FAKE_CLOSURE;
}
/* }}} */

void gear_closure_bind_var(zval *closure_zv, gear_string *var_name, zval *var) /* {{{ */
{
	gear_closure *closure = (gear_closure *) Z_OBJ_P(closure_zv);
	HashTable *static_variables = closure->func.op_array.static_variables;
	gear_hash_update(static_variables, var_name, var);
}
/* }}} */

void gear_closure_bind_var_ex(zval *closure_zv, uint32_t offset, zval *val) /* {{{ */
{
	gear_closure *closure = (gear_closure *) Z_OBJ_P(closure_zv);
	HashTable *static_variables = closure->func.op_array.static_variables;
	zval *var = (zval*)((char*)static_variables->arData + offset);
	zval_ptr_dtor(var);
	ZVAL_COPY_VALUE(var, val);
}
/* }}} */

