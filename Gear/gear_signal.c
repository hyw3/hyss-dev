/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <string.h>

#include "gear.h"
#include "gear_globals.h"

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef GEAR_SIGNALS

#include "gear_signal.h"

#ifdef ZTS
GEAR_API int gear_signal_globals_id;
#else
GEAR_API gear_signal_globals_t gear_signal_globals;
#endif /* not ZTS */

#define SIGNAL_BEGIN_CRITICAL() \
	sigset_t oldmask; \
	gear_sigprocmask(SIG_BLOCK, &global_sigmask, &oldmask);
#define SIGNAL_END_CRITICAL() \
	gear_sigprocmask(SIG_SETMASK, &oldmask, NULL);

#ifdef ZTS
# define gear_sigprocmask(signo, set, oldset) pbc_sigmask((signo), (set), (oldset))
#else
# define gear_sigprocmask(signo, set, oldset) sigprocmask((signo), (set), (oldset))
#endif

static void gear_signal_handler(int signo, siginfo_t *siginfo, void *context);
static int gear_signal_register(int signo, void (*handler)(int, siginfo_t*, void*));

#ifdef __CYGWIN__
#define TIMEOUT_SIG SIGALRM
#else
#define TIMEOUT_SIG SIGPROF
#endif

static int gear_sigs[] = { TIMEOUT_SIG, SIGHUP, SIGINT, SIGQUIT, SIGTERM, SIGUSR1, SIGUSR2 };

#define SA_FLAGS_MASK ~(SA_NODEFER | SA_RESETHAND)

/* True globals, written only at process startup */
static gear_signal_entry_t global_orig_handlers[NSIG];
static sigset_t            global_sigmask;

/* {{{ gear_signal_handler_defer
 *  Blocks signals if in critical section */
void gear_signal_handler_defer(int signo, siginfo_t *siginfo, void *context)
{
	int errno_save = errno;
	gear_signal_queue_t *queue, *qtmp;
	gear_bool is_handling_safe = 1;

#ifdef ZTS
	GEAR_PBCLS_CACHE_UPDATE();
	/* A signal could hit after hypbc shutdown, in this case globals are already freed. */
	if (NULL == PBCLS_CACHE || NULL == PBCG_BULK_STATIC(gear_signal_globals_id, gear_signal_globals_t *)) {
		is_handling_safe = 0;
	}
#endif

	if (EXPECTED(is_handling_safe && SIGG(active))) {
		if (UNEXPECTED(SIGG(depth) == 0)) { /* try to handle signal */
			if (UNEXPECTED(SIGG(blocked))) {
				SIGG(blocked) = 0;
			}
			if (EXPECTED(SIGG(running) == 0)) {
				SIGG(running) = 1;
				gear_signal_handler(signo, siginfo, context);

				queue = SIGG(phead);
				SIGG(phead) = NULL;

				while (queue) {
					gear_signal_handler(queue->gear_signal.signo, queue->gear_signal.siginfo, queue->gear_signal.context);
					qtmp = queue->next;
					queue->next = SIGG(pavail);
					queue->gear_signal.signo = 0;
					SIGG(pavail) = queue;
					queue = qtmp;
				}
				SIGG(running) = 0;
			}
		} else { /* delay signal handling */
			SIGG(blocked) = 1; /* signal is blocked */

			if ((queue = SIGG(pavail))) { /* if none available it's simply forgotton */
				SIGG(pavail) = queue->next;
				queue->gear_signal.signo = signo;
				queue->gear_signal.siginfo = siginfo;
				queue->gear_signal.context = context;
				queue->next = NULL;

				if (SIGG(phead) && SIGG(ptail)) {
					SIGG(ptail)->next = queue;
				} else {
					SIGG(phead) = queue;
				}
				SIGG(ptail) = queue;
			}
#if GEAR_DEBUG
			else { /* this may not be safe to do, but could work and be useful */
				gear_output_debug_string(0, "gear_signal: not enough queue storage, lost signal (%d)", signo);
			}
#endif
		}
	} else {
		/* need to just run handler if we're inactive and getting a signal */
		gear_signal_handler(signo, siginfo, context);
	}

	errno = errno_save;
} /* }}} */

/* {{{ gear_signal_handler_unblock
 * Handle deferred signal from HANDLE_UNBLOCK_ALARMS */
GEAR_API void gear_signal_handler_unblock(void)
{
	gear_signal_queue_t *queue;
	gear_signal_t gear_signal;

	if (EXPECTED(SIGG(active))) {
		SIGNAL_BEGIN_CRITICAL(); /* procmask to protect handler_defer as if it were called by the kernel */
		queue = SIGG(phead);
		SIGG(phead) = queue->next;
		gear_signal = queue->gear_signal;
		queue->next = SIGG(pavail);
		queue->gear_signal.signo = 0;
		SIGG(pavail) = queue;

		gear_signal_handler_defer(gear_signal.signo, gear_signal.siginfo, gear_signal.context);
		SIGNAL_END_CRITICAL();
	}
}
/* }}} */

/* {{{ gear_signal_handler
 *  Call the previously registered handler for a signal
 */
static void gear_signal_handler(int signo, siginfo_t *siginfo, void *context)
{
	int errno_save = errno;
	struct sigaction sa;
	sigset_t sigset;
	gear_signal_entry_t p_sig;
#ifdef ZTS
	if (NULL == PBCLS_CACHE || NULL == PBCG_BULK_STATIC(gear_signal_globals_id, gear_signal_globals_t *)) {
		p_sig.flags = 0;
		p_sig.handler = SIG_DFL;
	} else
#endif
	p_sig = SIGG(handlers)[signo-1];

	if (p_sig.handler == SIG_DFL) { /* raise default handler */
		if (sigaction(signo, NULL, &sa) == 0) {
			sa.sa_handler = SIG_DFL;
			sigemptyset(&sa.sa_mask);

			sigemptyset(&sigset);
			sigaddset(&sigset, signo);

			if (sigaction(signo, &sa, NULL) == 0) {
				/* throw away any blocked signals */
				gear_sigprocmask(SIG_UNBLOCK, &sigset, NULL);
#ifdef ZTS
# define RAISE_ERROR "raise() failed\n"
				if (raise(signo) != 0) {
					/* On some systems raise() fails with errno 3: No such process */
					kill(getpid(), signo);
				}
#else
				kill(getpid(), signo);
#endif
			}
		}
	} else if (p_sig.handler != SIG_IGN) {
		if (p_sig.flags & SA_SIGINFO) {
			if (p_sig.flags & SA_RESETHAND) {
				SIGG(handlers)[signo-1].flags   = 0;
				SIGG(handlers)[signo-1].handler = SIG_DFL;
			}
			(*(void (*)(int, siginfo_t*, void*))p_sig.handler)(signo, siginfo, context);
		} else {
			(*(void (*)(int))p_sig.handler)(signo);
		}
	}

	errno = errno_save;
} /* }}} */

/* {{{ gear_sigaction
 *  Register a signal handler that will be deferred in critical sections */
GEAR_API int gear_sigaction(int signo, const struct sigaction *act, struct sigaction *oldact)
{
	struct sigaction sa;
	sigset_t sigset;

	if (oldact != NULL) {
		oldact->sa_flags   = SIGG(handlers)[signo-1].flags;
		oldact->sa_handler = (void *) SIGG(handlers)[signo-1].handler;
		oldact->sa_mask    = global_sigmask;
	}
	if (act != NULL) {
		SIGG(handlers)[signo-1].flags = act->sa_flags;
		if (act->sa_flags & SA_SIGINFO) {
			SIGG(handlers)[signo-1].handler = (void *) act->sa_sigaction;
		} else {
			SIGG(handlers)[signo-1].handler = (void *) act->sa_handler;
		}

		memset(&sa, 0, sizeof(sa));
		if (SIGG(handlers)[signo-1].handler == (void *) SIG_IGN) {
			sa.sa_sigaction = (void *) SIG_IGN;
		} else {
			sa.sa_flags     = SA_SIGINFO | (act->sa_flags & SA_FLAGS_MASK);
			sa.sa_sigaction = gear_signal_handler_defer;
			sa.sa_mask      = global_sigmask;
		}

		if (sigaction(signo, &sa, NULL) < 0) {
			gear_error_noreturn(E_ERROR, "Error installing signal handler for %d", signo);
		}

		/* unsure this signal is not blocked */
		sigemptyset(&sigset);
		sigaddset(&sigset, signo);
		gear_sigprocmask(SIG_UNBLOCK, &sigset, NULL);
	}

	return SUCCESS;
}
/* }}} */

/* {{{ gear_signal
 *  Register a signal handler that will be deferred in critical sections */
GEAR_API int gear_signal(int signo, void (*handler)(int))
{
	struct sigaction sa;

	memset(&sa, 0, sizeof(sa));
	sa.sa_flags   = 0;
	sa.sa_handler = handler;
	sa.sa_mask    = global_sigmask;

	return gear_sigaction(signo, &sa, NULL);
}
/* }}} */

/* {{{ gear_signal_register
 *  Set a handler for a signal we want to defer.
 *  Previously set handler must have been saved before.
 */
static int gear_signal_register(int signo, void (*handler)(int, siginfo_t*, void*))
{
	struct sigaction sa;

	if (sigaction(signo, NULL, &sa) == 0) {
		if ((sa.sa_flags & SA_SIGINFO) && sa.sa_sigaction == handler) {
			return FAILURE;
		}

		SIGG(handlers)[signo-1].flags = sa.sa_flags;
		if (sa.sa_flags & SA_SIGINFO) {
			SIGG(handlers)[signo-1].handler = (void *)sa.sa_sigaction;
		} else {
			SIGG(handlers)[signo-1].handler = (void *)sa.sa_handler;
		}

		sa.sa_flags     = SA_SIGINFO; /* we'll use a siginfo handler */
		sa.sa_sigaction = handler;
		sa.sa_mask      = global_sigmask;

		if (sigaction(signo, &sa, NULL) < 0) {
			gear_error_noreturn(E_ERROR, "Error installing signal handler for %d", signo);
		}

		return SUCCESS;
	}
	return FAILURE;
} /* }}} */

/* {{{ gear_signal_activate
 *  Install our signal handlers, per request */
void gear_signal_activate(void)
{
	size_t x;

	memcpy(&SIGG(handlers), &global_orig_handlers, sizeof(global_orig_handlers));

	for (x = 0; x < sizeof(gear_sigs) / sizeof(*gear_sigs); x++) {
		gear_signal_register(gear_sigs[x], gear_signal_handler_defer);
	}

	SIGG(active) = 1;
	SIGG(depth)  = 0;
} /* }}} */

/* {{{ gear_signal_deactivate
 * */
void gear_signal_deactivate(void)
{

	if (SIGG(check)) {
		size_t x;
		struct sigaction sa;

		if (SIGG(depth) != 0) {
			gear_error(E_CORE_WARNING, "gear_signal: shutdown with non-zero blocking depth (%d)", SIGG(depth));
		}
		/* did anyone steal our installed handler */
		for (x = 0; x < sizeof(gear_sigs) / sizeof(*gear_sigs); x++) {
			sigaction(gear_sigs[x], NULL, &sa);
			if (sa.sa_sigaction != gear_signal_handler_defer) {
				gear_error(E_CORE_WARNING, "gear_signal: handler was replaced for signal (%d) after startup", gear_sigs[x]);
			}
		}
	}

	SIGNAL_BEGIN_CRITICAL();
	SIGG(active) = 0;
	SIGG(running) = 0;
	SIGG(blocked) = 0;
	SIGG(depth) = 0;
	SIGNAL_END_CRITICAL();
}
/* }}} */

static void gear_signal_globals_ctor(gear_signal_globals_t *gear_signal_globals) /* {{{ */
{
	size_t x;

	memset(gear_signal_globals, 0, sizeof(*gear_signal_globals));

	for (x = 0; x < sizeof(gear_signal_globals->pstorage) / sizeof(*gear_signal_globals->pstorage); ++x) {
		gear_signal_queue_t *queue = &gear_signal_globals->pstorage[x];
		queue->gear_signal.signo = 0;
		queue->next = gear_signal_globals->pavail;
		gear_signal_globals->pavail = queue;
	}
}
/* }}} */

void gear_signal_init(void) /* {{{ */
{
	int signo;
	struct sigaction sa;

	/* Save previously registered signal handlers into orig_handlers */
	memset(&global_orig_handlers, 0, sizeof(global_orig_handlers));
	for (signo = 1; signo < NSIG; ++signo) {
		if (sigaction(signo, NULL, &sa) == 0) {
			global_orig_handlers[signo-1].flags = sa.sa_flags;
			if (sa.sa_flags & SA_SIGINFO) {
				global_orig_handlers[signo-1].handler = (void *) sa.sa_sigaction;
			} else {
				global_orig_handlers[signo-1].handler = (void *) sa.sa_handler;
			}
		}
	}
}
/* }}} */

/* {{{ gear_signal_startup
 * alloc gear signal globals */
GEAR_API void gear_signal_startup(void)
{

#ifdef ZTS
	ts_allocate_id(&gear_signal_globals_id, sizeof(gear_signal_globals_t), (ts_allocate_ctor) gear_signal_globals_ctor, NULL);
#else
	gear_signal_globals_ctor(&gear_signal_globals);
#endif

	/* Used to block signals during execution of signal handlers */
	sigfillset(&global_sigmask);
	sigdelset(&global_sigmask, SIGILL);
	sigdelset(&global_sigmask, SIGABRT);
	sigdelset(&global_sigmask, SIGFPE);
	sigdelset(&global_sigmask, SIGKILL);
	sigdelset(&global_sigmask, SIGSEGV);
	sigdelset(&global_sigmask, SIGCONT);
	sigdelset(&global_sigmask, SIGSTOP);
	sigdelset(&global_sigmask, SIGTSTP);
	sigdelset(&global_sigmask, SIGTTIN);
	sigdelset(&global_sigmask, SIGTTOU);
#ifdef SIGBUS
	sigdelset(&global_sigmask, SIGBUS);
#endif
#ifdef SIGSYS
	sigdelset(&global_sigmask, SIGSYS);
#endif
#ifdef SIGTRAP
	sigdelset(&global_sigmask, SIGTRAP);
#endif

	gear_signal_init();
}
/* }}} */


#endif /* GEAR_SIGNALS */

