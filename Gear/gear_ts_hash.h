/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_TS_HASH_H
#define GEAR_TS_HASH_H

#include "gear.h"

typedef struct _gear_ts_hashtable {
	HashTable hash;
	uint32_t reader;
#ifdef ZTS
	MUTEX_T mx_reader;
	MUTEX_T mx_writer;
#endif
} TsHashTable;

BEGIN_EXTERN_C()

#define TS_HASH(table) (&(table->hash))

/* startup/shutdown */
GEAR_API void _gear_ts_hash_init(TsHashTable *ht, uint32_t nSize, dtor_func_t pDestructor, gear_bool persistent);
GEAR_API void gear_ts_hash_destroy(TsHashTable *ht);
GEAR_API void gear_ts_hash_clean(TsHashTable *ht);

#define gear_ts_hash_init(ht, nSize, pHashFunction, pDestructor, persistent)	\
	_gear_ts_hash_init(ht, nSize, pDestructor, persistent)
#define gear_ts_hash_init_ex(ht, nSize, pHashFunction, pDestructor, persistent, bApplyProtection)	\
	_gear_ts_hash_init(ht, nSize, pDestructor, persistent)


/* additions/updates/changes */
GEAR_API zval *gear_ts_hash_update(TsHashTable *ht, gear_string *key, zval *pData);
GEAR_API zval *gear_ts_hash_add(TsHashTable *ht, gear_string *key, zval *pData);
GEAR_API zval *gear_ts_hash_index_update(TsHashTable *ht, gear_ulong h, zval *pData);
GEAR_API zval *gear_ts_hash_next_index_insert(TsHashTable *ht, zval *pData);
GEAR_API zval* gear_ts_hash_add_empty_element(TsHashTable *ht, gear_string *key);

GEAR_API void gear_ts_hash_graceful_destroy(TsHashTable *ht);
GEAR_API void gear_ts_hash_apply(TsHashTable *ht, apply_func_t apply_func);
GEAR_API void gear_ts_hash_apply_with_argument(TsHashTable *ht, apply_func_arg_t apply_func, void *);
GEAR_API void gear_ts_hash_apply_with_arguments(TsHashTable *ht, apply_func_args_t apply_func, int, ...);

GEAR_API void gear_ts_hash_reverse_apply(TsHashTable *ht, apply_func_t apply_func);


/* Deletes */
GEAR_API int gear_ts_hash_del(TsHashTable *ht, gear_string *key);
GEAR_API int gear_ts_hash_index_del(TsHashTable *ht, gear_ulong h);

/* Data retreival */
GEAR_API zval *gear_ts_hash_find(TsHashTable *ht, gear_string *key);
GEAR_API zval *gear_ts_hash_index_find(TsHashTable *ht, gear_ulong);

/* Misc */
GEAR_API int gear_ts_hash_exists(TsHashTable *ht, gear_string *key);
GEAR_API int gear_ts_hash_index_exists(TsHashTable *ht, gear_ulong h);

/* Copying, merging and sorting */
GEAR_API void gear_ts_hash_copy(TsHashTable *target, TsHashTable *source, copy_ctor_func_t pCopyConstructor);
GEAR_API void gear_ts_hash_copy_to_hash(HashTable *target, TsHashTable *source, copy_ctor_func_t pCopyConstructor);
GEAR_API void gear_ts_hash_merge(TsHashTable *target, TsHashTable *source, copy_ctor_func_t pCopyConstructor, int overwrite);
GEAR_API void gear_ts_hash_merge_ex(TsHashTable *target, TsHashTable *source, copy_ctor_func_t pCopyConstructor, merge_checker_func_t pMergeSource, void *pParam);
GEAR_API int gear_ts_hash_sort(TsHashTable *ht, sort_func_t sort_func, compare_func_t compare_func, int renumber);
GEAR_API int gear_ts_hash_compare(TsHashTable *ht1, TsHashTable *ht2, compare_func_t compar, gear_bool ordered);
GEAR_API zval *gear_ts_hash_minmax(TsHashTable *ht, compare_func_t compar, int flag);

GEAR_API int gear_ts_hash_num_elements(TsHashTable *ht);

GEAR_API int gear_ts_hash_rehash(TsHashTable *ht);

#if GEAR_DEBUG
/* debug */
void gear_ts_hash_display_pListTail(TsHashTable *ht);
void gear_ts_hash_display(TsHashTable *ht);
#endif

GEAR_API zval *gear_ts_hash_str_find(TsHashTable *ht, const char *key, size_t len);
GEAR_API zval *gear_ts_hash_str_update(TsHashTable *ht, const char *key, size_t len, zval *pData);
GEAR_API zval *gear_ts_hash_str_add(TsHashTable *ht, const char *key, size_t len, zval *pData);

static gear_always_inline void *gear_ts_hash_str_find_ptr(TsHashTable *ht, const char *str, size_t len)
{
	zval *zv;

	zv = gear_ts_hash_str_find(ht, str, len);
	return zv ? Z_PTR_P(zv) : NULL;
}

static gear_always_inline void *gear_ts_hash_str_update_ptr(TsHashTable *ht, const char *str, size_t len, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_ts_hash_str_update(ht, str, len, &tmp);
	return zv ? Z_PTR_P(zv) : NULL;
}

static gear_always_inline void *gear_ts_hash_str_add_ptr(TsHashTable *ht, const char *str, size_t len, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_ts_hash_str_add(ht, str, len, &tmp);
	return zv ? Z_PTR_P(zv) : NULL;
}

END_EXTERN_C()

#define GEAR_TS_INIT_SYMTABLE(ht)					\
	GEAR_TS_INIT_SYMTABLE_EX(ht, 2, 0)

#define GEAR_TS_INIT_SYMTABLE_EX(ht, n, persistent)			\
	gear_ts_hash_init(ht, n, NULL, ZVAL_PTR_DTOR, persistent)

#endif		/* GEAR_HASH_H */

