/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_STRTOD_INT_H
#define GEAR_STRTOD_INT_H

#ifdef ZTS
#include <hypbc.h>
#endif

#include <stddef.h>
#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include <math.h>

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

/* TODO check to undef this option, this might
	make more perf. destroy_freelist()
	should be adapted then. */
#define Omit_Private_Memory 1

/* HEX strings aren't supported as per
	https://hyss.hyang.org/wiki/rfc/remove_hex_support_in_numeric_strings */
#define NO_HEX_FP 1

#if defined(HAVE_INTTYPES_H)
#include <inttypes.h>
#elif defined(HAVE_STDINT_H)
#include <stdint.h>
#endif

#ifndef HAVE_INT32_T
# if SIZEOF_INT == 4
typedef int int32_t;
# elif SIZEOF_LONG == 4
typedef long int int32_t;
# endif
#endif

#ifndef HAVE_UINT32_T
# if SIZEOF_INT == 4
typedef unsigned int uint32_t;
# elif SIZEOF_LONG == 4
typedef unsigned long int uint32_t;
# endif
#endif

#ifdef USE_LOCALE
#undef USE_LOCALE
#endif

#ifndef NO_INFNAN_CHECK
#define NO_INFNAN_CHECK
#endif

#ifndef NO_ERRNO
#define NO_ERRNO
#endif

#ifdef WORDS_BIGENDIAN
#define IEEE_BIG_ENDIAN 1
#else
#define IEEE_LITTLE_ENDIAN 1
#endif

#if (defined(__APPLE__) || defined(__APPLE_CC__)) && (defined(__BIG_ENDIAN__) || defined(__LITTLE_ENDIAN__))
# if defined(__LITTLE_ENDIAN__)
#  undef WORDS_BIGENDIAN
# else
#  if defined(__BIG_ENDIAN__)
#   define WORDS_BIGENDIAN
#  endif
# endif
#endif

#if defined(__arm__) && !defined(__VFP_FP__)
/*
 *  * Although the CPU is little endian the FP has different
 *   * byte and word endianness. The byte order is still little endian
 *    * but the word order is big endian.
 *     */
#define IEEE_BIG_ENDIAN
#undef IEEE_LITTLE_ENDIAN
#endif

#ifdef __vax__
#define VAX
#undef IEEE_LITTLE_ENDIAN
#endif

#ifdef IEEE_LITTLE_ENDIAN
#define IEEE_8087 1
#endif

#ifdef IEEE_BIG_ENDIAN
#define IEEE_MC68k 1
#endif

#if defined(_MSC_VER)
#ifndef int32_t
#define int32_t __int32
#endif
#ifndef uint32_t
#define uint32_t unsigned __int32
#endif
#endif

#ifdef ZTS
#define MULTIPLE_THREADS 1

#define  ACQUIRE_DTOA_LOCK(x) \
	if (0 == x) { \
		pbc_mutex_lock(dtoa_mutex); \
	} else if (1 == x) { \
		pbc_mutex_lock(pow5mult_mutex); \
	}

#define FREE_DTOA_LOCK(x) \
	if (0 == x) { \
		pbc_mutex_unlock(dtoa_mutex); \
	} else if (1 == x) { \
		pbc_mutex_unlock(pow5mult_mutex); \
	}


#endif

#endif /* GEAR_STRTOD_INT_H */

