/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_sort.h"
#include "gear_API.h"
#include "gear_ics.h"
#include "gear_alloc.h"
#include "gear_operators.h"
#include "gear_strtod.h"

static HashTable *registered_gear_ics_directives;

#define NO_VALUE_PLAINTEXT		"no value"
#define NO_VALUE_HTML			"<i>no value</i>"

/*
 * hash_apply functions
 */
static int gear_remove_ics_entries(zval *el, void *arg) /* {{{ */
{
	gear_ics_entry *ics_entry = (gear_ics_entry *)Z_PTR_P(el);
	int capi_number = *(int *)arg;
	if (ics_entry->capi_number == capi_number) {
		return 1;
	} else {
		return 0;
	}
}
/* }}} */

static int gear_restore_ics_entry_cb(gear_ics_entry *ics_entry, int stage) /* {{{ */
{
	int result = FAILURE;

	if (ics_entry->modified) {
		if (ics_entry->on_modify) {
			gear_try {
			/* even if on_modify bails out, we have to continue on with restoring,
				since there can be allocated variables that would be freed on MM shutdown
				and would lead to memory corruption later ics entry is modified again */
				result = ics_entry->on_modify(ics_entry, ics_entry->orig_value, ics_entry->mh_arg1, ics_entry->mh_arg2, ics_entry->mh_arg3, stage);
			} gear_end_try();
		}
		if (stage == GEAR_ICS_STAGE_RUNTIME && result == FAILURE) {
			/* runtime failure is OK */
			return 1;
		}
		if (ics_entry->value != ics_entry->orig_value) {
			gear_string_release(ics_entry->value);
		}
		ics_entry->value = ics_entry->orig_value;
		ics_entry->modifiable = ics_entry->orig_modifiable;
		ics_entry->modified = 0;
		ics_entry->orig_value = NULL;
		ics_entry->orig_modifiable = 0;
	}
	return 0;
}
/* }}} */

static int gear_restore_ics_entry_wrapper(zval *el) /* {{{ */
{
	gear_ics_entry *ics_entry = (gear_ics_entry *)Z_PTR_P(el);
	gear_restore_ics_entry_cb(ics_entry, GEAR_ICS_STAGE_DEACTIVATE);
	return 1;
}
/* }}} */

static void free_ics_entry(zval *zv) /* {{{ */
{
	gear_ics_entry *entry = (gear_ics_entry*)Z_PTR_P(zv);

	gear_string_release_ex(entry->name, 1);
	if (entry->value) {
		gear_string_release(entry->value);
	}
	if (entry->orig_value) {
		gear_string_release_ex(entry->orig_value, 1);
	}
	free(entry);
}
/* }}} */

/*
 * Startup / shutdown
 */
GEAR_API int gear_ics_startup(void) /* {{{ */
{
	registered_gear_ics_directives = (HashTable *) malloc(sizeof(HashTable));

	EG(ics_directives) = registered_gear_ics_directives;
	EG(modified_ics_directives) = NULL;
	EG(error_reporting_ics_entry) = NULL;
	gear_hash_init_ex(registered_gear_ics_directives, 128, NULL, free_ics_entry, 1, 0);
	return SUCCESS;
}
/* }}} */

GEAR_API int gear_ics_shutdown(void) /* {{{ */
{
	gear_ics_dtor(EG(ics_directives));
	return SUCCESS;
}
/* }}} */

GEAR_API void gear_ics_dtor(HashTable *ics_directives) /* {{{ */
{
	gear_hash_destroy(ics_directives);
	free(ics_directives);
}
/* }}} */

GEAR_API int gear_ics_global_shutdown(void) /* {{{ */
{
	gear_hash_destroy(registered_gear_ics_directives);
	free(registered_gear_ics_directives);
	return SUCCESS;
}
/* }}} */

GEAR_API int gear_ics_deactivate(void) /* {{{ */
{
	if (EG(modified_ics_directives)) {
		gear_hash_apply(EG(modified_ics_directives), gear_restore_ics_entry_wrapper);
		gear_hash_destroy(EG(modified_ics_directives));
		FREE_HASHTABLE(EG(modified_ics_directives));
		EG(modified_ics_directives) = NULL;
	}
	return SUCCESS;
}
/* }}} */

#ifdef ZTS
static void copy_ics_entry(zval *zv) /* {{{ */
{
	gear_ics_entry *old_entry = (gear_ics_entry*)Z_PTR_P(zv);
	gear_ics_entry *new_entry = pemalloc(sizeof(gear_ics_entry), 1);

	Z_PTR_P(zv) = new_entry;
	memcpy(new_entry, old_entry, sizeof(gear_ics_entry));
	if (old_entry->name) {
		new_entry->name = gear_string_dup(old_entry->name, 1);
	}
	if (old_entry->value) {
		new_entry->value = gear_string_dup(old_entry->value, 1);
	}
	if (old_entry->orig_value) {
		new_entry->orig_value = gear_string_dup(old_entry->orig_value, 1);
	}
}
/* }}} */

GEAR_API int gear_copy_ics_directives(void) /* {{{ */
{
	EG(modified_ics_directives) = NULL;
	EG(error_reporting_ics_entry) = NULL;
	EG(ics_directives) = (HashTable *) malloc(sizeof(HashTable));
	gear_hash_init_ex(EG(ics_directives), registered_gear_ics_directives->nNumOfElements, NULL, free_ics_entry, 1, 0);
	gear_hash_copy(EG(ics_directives), registered_gear_ics_directives, copy_ics_entry);
	return SUCCESS;
}
/* }}} */
#endif

static int ics_key_compare(const void *a, const void *b) /* {{{ */
{
	const Bucket *f;
	const Bucket *s;

	f = (const Bucket *) a;
	s = (const Bucket *) b;

	if (!f->key && !s->key) { /* both numeric */
		if (f->h > s->h) {
			return -1;
		} else if (f->h < s->h) {
			return 1;
		}
		return 0;
	} else if (!f->key) { /* f is numeric, s is not */
		return -1;
	} else if (!s->key) { /* s is numeric, f is not */
		return 1;
	} else { /* both strings */
		return gear_binary_strcasecmp(ZSTR_VAL(f->key), ZSTR_LEN(f->key), ZSTR_VAL(s->key), ZSTR_LEN(s->key));
	}
}
/* }}} */

GEAR_API void gear_ics_sort_entries(void) /* {{{ */
{
	gear_hash_sort(EG(ics_directives), ics_key_compare, 0);
}
/* }}} */

/*
 * Registration / unregistration
 */
GEAR_API int gear_register_ics_entries(const gear_ics_entry_def *ics_entry, int capi_number) /* {{{ */
{
	gear_ics_entry *p;
	zval *default_value;
	HashTable *directives = registered_gear_ics_directives;

#ifdef ZTS
	/* if we are called during the request, eg: from dl(),
	 * then we should not touch the global directives table,
	 * and should update the per-(request|thread) version instead.
	 * This solves two problems: one is that ics entries for dl()'d
	 * extensions will now work, and the second is that updating the
	 * global hash here from dl() is not mutex protected and can
	 * lead to death.
	 */
	if (directives != EG(ics_directives)) {
		directives = EG(ics_directives);
	}
#endif

	while (ics_entry->name) {
		p = pemalloc(sizeof(gear_ics_entry), 1);
		p->name = gear_string_init_interned(ics_entry->name, ics_entry->name_length, 1);
		p->on_modify = ics_entry->on_modify;
		p->mh_arg1 = ics_entry->mh_arg1;
		p->mh_arg2 = ics_entry->mh_arg2;
		p->mh_arg3 = ics_entry->mh_arg3;
		p->value = NULL;
		p->orig_value = NULL;
		p->displayer = ics_entry->displayer;
		p->modifiable = ics_entry->modifiable;

		p->orig_modifiable = 0;
		p->modified = 0;
		p->capi_number = capi_number;

		if (gear_hash_add_ptr(directives, p->name, (void*)p) == NULL) {
			if (p->name) {
				gear_string_release_ex(p->name, 1);
			}
			gear_unregister_ics_entries(capi_number);
			return FAILURE;
		}
		if (((default_value = gear_get_configuration_directive(p->name)) != NULL) &&
            (!p->on_modify || p->on_modify(p, Z_STR_P(default_value), p->mh_arg1, p->mh_arg2, p->mh_arg3, GEAR_ICS_STAGE_STARTUP) == SUCCESS)) {

			p->value = gear_new_interned_string(gear_string_copy(Z_STR_P(default_value)));
		} else {
			p->value = ics_entry->value ?
				gear_string_init_interned(ics_entry->value, ics_entry->value_length, 1) : NULL;

			if (p->on_modify) {
				p->on_modify(p, p->value, p->mh_arg1, p->mh_arg2, p->mh_arg3, GEAR_ICS_STAGE_STARTUP);
			}
		}
		ics_entry++;
	}
	return SUCCESS;
}
/* }}} */

GEAR_API void gear_unregister_ics_entries(int capi_number) /* {{{ */
{
	gear_hash_apply_with_argument(registered_gear_ics_directives, gear_remove_ics_entries, (void *) &capi_number);
}
/* }}} */

#ifdef ZTS
static int gear_ics_refresh_cache(zval *el, void *arg) /* {{{ */
{
	gear_ics_entry *p = (gear_ics_entry *)Z_PTR_P(el);
	int stage = (int)(gear_intptr_t)arg;

	if (p->on_modify) {
		p->on_modify(p, p->value, p->mh_arg1, p->mh_arg2, p->mh_arg3, stage);
	}
	return 0;
}
/* }}} */

GEAR_API void gear_ics_refresh_caches(int stage) /* {{{ */
{
	gear_hash_apply_with_argument(EG(ics_directives), gear_ics_refresh_cache, (void *)(gear_intptr_t) stage);
}
/* }}} */
#endif

GEAR_API int gear_alter_ics_entry(gear_string *name, gear_string *new_value, int modify_type, int stage) /* {{{ */
{

	return gear_alter_ics_entry_ex(name, new_value, modify_type, stage, 0);
}
/* }}} */

GEAR_API int gear_alter_ics_entry_chars(gear_string *name, const char *value, size_t value_length, int modify_type, int stage) /* {{{ */
{
    int ret;
    gear_string *new_value;

	new_value = gear_string_init(value, value_length, !(stage & GEAR_ICS_STAGE_IN_REQUEST));
	ret = gear_alter_ics_entry_ex(name, new_value, modify_type, stage, 0);
	gear_string_release(new_value);
	return ret;
}
/* }}} */

GEAR_API int gear_alter_ics_entry_chars_ex(gear_string *name, const char *value, size_t value_length, int modify_type, int stage, int force_change) /* {{{ */
{
    int ret;
    gear_string *new_value;

	new_value = gear_string_init(value, value_length, !(stage & GEAR_ICS_STAGE_IN_REQUEST));
	ret = gear_alter_ics_entry_ex(name, new_value, modify_type, stage, force_change);
	gear_string_release(new_value);
	return ret;
}
/* }}} */

GEAR_API int gear_alter_ics_entry_ex(gear_string *name, gear_string *new_value, int modify_type, int stage, int force_change) /* {{{ */
{
	gear_ics_entry *ics_entry;
	gear_string *duplicate;
	gear_bool modifiable;
	gear_bool modified;

	if ((ics_entry = gear_hash_find_ptr(EG(ics_directives), name)) == NULL) {
		return FAILURE;
	}

	modifiable = ics_entry->modifiable;
	modified = ics_entry->modified;

	if (stage == GEAR_ICS_STAGE_ACTIVATE && modify_type == GEAR_ICS_SYSTEM) {
		ics_entry->modifiable = GEAR_ICS_SYSTEM;
	}

	if (!force_change) {
		if (!(ics_entry->modifiable & modify_type)) {
			return FAILURE;
		}
	}

	if (!EG(modified_ics_directives)) {
		ALLOC_HASHTABLE(EG(modified_ics_directives));
		gear_hash_init(EG(modified_ics_directives), 8, NULL, NULL, 0);
	}
	if (!modified) {
		ics_entry->orig_value = ics_entry->value;
		ics_entry->orig_modifiable = modifiable;
		ics_entry->modified = 1;
		gear_hash_add_ptr(EG(modified_ics_directives), ics_entry->name, ics_entry);
	}

	duplicate = gear_string_copy(new_value);

	if (!ics_entry->on_modify
		|| ics_entry->on_modify(ics_entry, duplicate, ics_entry->mh_arg1, ics_entry->mh_arg2, ics_entry->mh_arg3, stage) == SUCCESS) {
		if (modified && ics_entry->orig_value != ics_entry->value) { /* we already changed the value, free the changed value */
			gear_string_release(ics_entry->value);
		}
		ics_entry->value = duplicate;
	} else {
		gear_string_release(duplicate);
		return FAILURE;
	}

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_restore_ics_entry(gear_string *name, int stage) /* {{{ */
{
	gear_ics_entry *ics_entry;

	if ((ics_entry = gear_hash_find_ptr(EG(ics_directives), name)) == NULL ||
		(stage == GEAR_ICS_STAGE_RUNTIME && (ics_entry->modifiable & GEAR_ICS_USER) == 0)) {
		return FAILURE;
	}

	if (EG(modified_ics_directives)) {
		if (gear_restore_ics_entry_cb(ics_entry, stage) == 0) {
			gear_hash_del(EG(modified_ics_directives), name);
		} else {
			return FAILURE;
		}
	}

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_ics_register_displayer(char *name, uint32_t name_length, void (*displayer)(gear_ics_entry *ics_entry, int type)) /* {{{ */
{
	gear_ics_entry *ics_entry;

	ics_entry = gear_hash_str_find_ptr(registered_gear_ics_directives, name, name_length);
	if (ics_entry == NULL) {
		return FAILURE;
	}

	ics_entry->displayer = displayer;
	return SUCCESS;
}
/* }}} */

/*
 * Data retrieval
 */

GEAR_API gear_long gear_ics_long(char *name, size_t name_length, int orig) /* {{{ */
{
	gear_ics_entry *ics_entry;

	ics_entry = gear_hash_str_find_ptr(EG(ics_directives), name, name_length);
	if (ics_entry) {
		if (orig && ics_entry->modified) {
			return (ics_entry->orig_value ? GEAR_STRTOL(ZSTR_VAL(ics_entry->orig_value), NULL, 0) : 0);
		} else {
			return (ics_entry->value      ? GEAR_STRTOL(ZSTR_VAL(ics_entry->value), NULL, 0)      : 0);
		}
	}

	return 0;
}
/* }}} */

GEAR_API double gear_ics_double(char *name, size_t name_length, int orig) /* {{{ */
{
	gear_ics_entry *ics_entry;

	ics_entry = gear_hash_str_find_ptr(EG(ics_directives), name, name_length);
	if (ics_entry) {
		if (orig && ics_entry->modified) {
			return (double) (ics_entry->orig_value ? gear_strtod(ZSTR_VAL(ics_entry->orig_value), NULL) : 0.0);
		} else {
			return (double) (ics_entry->value      ? gear_strtod(ZSTR_VAL(ics_entry->value), NULL)      : 0.0);
		}
	}

	return 0.0;
}
/* }}} */

GEAR_API char *gear_ics_string_ex(char *name, size_t name_length, int orig, gear_bool *exists) /* {{{ */
{
	gear_ics_entry *ics_entry;

	ics_entry = gear_hash_str_find_ptr(EG(ics_directives), name, name_length);
	if (ics_entry) {
		if (exists) {
			*exists = 1;
		}

		if (orig && ics_entry->modified) {
			return ics_entry->orig_value ? ZSTR_VAL(ics_entry->orig_value) : NULL;
		} else {
			return ics_entry->value ? ZSTR_VAL(ics_entry->value) : NULL;
		}
	} else {
		if (exists) {
			*exists = 0;
		}
		return NULL;
	}
}
/* }}} */

GEAR_API char *gear_ics_string(char *name, size_t name_length, int orig) /* {{{ */
{
	gear_bool exists = 1;
	char *return_value;

	return_value = gear_ics_string_ex(name, name_length, orig, &exists);
	if (!exists) {
		return NULL;
	} else if (!return_value) {
		return_value = "";
	}
	return return_value;
}
/* }}} */

GEAR_API gear_string *gear_ics_get_value(gear_string *name) /* {{{ */
{
	gear_ics_entry *ics_entry;

	ics_entry = gear_hash_find_ptr(EG(ics_directives), name);
	if (ics_entry) {
		return ics_entry->value ? ics_entry->value : ZSTR_EMPTY_ALLOC();
	} else {
		return NULL;
	}
}
/* }}} */

GEAR_API gear_bool gear_ics_parse_bool(gear_string *str)
{
	if ((ZSTR_LEN(str) == 4 && strcasecmp(ZSTR_VAL(str), "true") == 0)
	  || (ZSTR_LEN(str) == 3 && strcasecmp(ZSTR_VAL(str), "yes") == 0)
	  || (ZSTR_LEN(str) == 2 && strcasecmp(ZSTR_VAL(str), "on") == 0)) {
		return 1;
	} else {
		return atoi(ZSTR_VAL(str)) != 0;
	}
}

#if TONY_20070307
static void gear_ics_displayer_cb(gear_ics_entry *ics_entry, int type) /* {{{ */
{
	if (ics_entry->displayer) {
		ics_entry->displayer(ics_entry, type);
	} else {
		char *display_string;
		uint32_t display_string_length;

		if (type == GEAR_ICS_DISPLAY_ORIG && ics_entry->modified) {
			if (ics_entry->orig_value) {
				display_string = ics_entry->orig_value;
				display_string_length = ics_entry->orig_value_length;
			} else {
				if (gear_uv.html_errors) {
					display_string = NO_VALUE_HTML;
					display_string_length = sizeof(NO_VALUE_HTML) - 1;
				} else {
					display_string = NO_VALUE_PLAINTEXT;
					display_string_length = sizeof(NO_VALUE_PLAINTEXT) - 1;
				}
			}
		} else if (ics_entry->value && ics_entry->value[0]) {
			display_string = ics_entry->value;
			display_string_length = ics_entry->value_length;
		} else {
			if (gear_uv.html_errors) {
				display_string = NO_VALUE_HTML;
				display_string_length = sizeof(NO_VALUE_HTML) - 1;
			} else {
				display_string = NO_VALUE_PLAINTEXT;
				display_string_length = sizeof(NO_VALUE_PLAINTEXT) - 1;
			}
		}
		GEAR_WRITE(display_string, display_string_length);
	}
}
/* }}} */
#endif

GEAR_ICS_DISP(gear_ics_boolean_displayer_cb) /* {{{ */
{
	int value;
	gear_string *tmp_value;

	if (type == GEAR_ICS_DISPLAY_ORIG && ics_entry->modified) {
		tmp_value = (ics_entry->orig_value ? ics_entry->orig_value : NULL );
	} else if (ics_entry->value) {
		tmp_value = ics_entry->value;
	} else {
		tmp_value = NULL;
	}

	if (tmp_value) {
		value = gear_ics_parse_bool(tmp_value);
	} else {
		value = 0;
	}

	if (value) {
		GEAR_PUTS("On");
	} else {
		GEAR_PUTS("Off");
	}
}
/* }}} */

GEAR_ICS_DISP(gear_ics_color_displayer_cb) /* {{{ */
{
	char *value;

	if (type == GEAR_ICS_DISPLAY_ORIG && ics_entry->modified) {
		value = ZSTR_VAL(ics_entry->orig_value);
	} else if (ics_entry->value) {
		value = ZSTR_VAL(ics_entry->value);
	} else {
		value = NULL;
	}
	if (value) {
		if (gear_uv.html_errors) {
			gear_printf("<font style=\"color: %s\">%s</font>", value, value);
		} else {
			GEAR_PUTS(value);
		}
	} else {
		if (gear_uv.html_errors) {
			GEAR_PUTS(NO_VALUE_HTML);
		} else {
			GEAR_PUTS(NO_VALUE_PLAINTEXT);
		}
	}
}
/* }}} */

GEAR_ICS_DISP(display_link_numbers) /* {{{ */
{
	char *value;

	if (type == GEAR_ICS_DISPLAY_ORIG && ics_entry->modified) {
		value = ZSTR_VAL(ics_entry->orig_value);
	} else if (ics_entry->value) {
		value = ZSTR_VAL(ics_entry->value);
	} else {
		value = NULL;
	}

	if (value) {
		if (atoi(value) == -1) {
			GEAR_PUTS("Unlimited");
		} else {
			gear_printf("%s", value);
		}
	}
}
/* }}} */

/* Standard message handlers */
GEAR_API GEAR_ICS_MH(OnUpdateBool) /* {{{ */
{
	gear_bool *p;
#ifndef ZTS
	char *base = (char *) mh_arg2;
#else
	char *base;

	base = (char *) ts_resource(*((int *) mh_arg2));
#endif

	p = (gear_bool *) (base+(size_t) mh_arg1);

	*p = gear_ics_parse_bool(new_value);
	return SUCCESS;
}
/* }}} */

GEAR_API GEAR_ICS_MH(OnUpdateLong) /* {{{ */
{
	gear_long *p;
#ifndef ZTS
	char *base = (char *) mh_arg2;
#else
	char *base;

	base = (char *) ts_resource(*((int *) mh_arg2));
#endif

	p = (gear_long *) (base+(size_t) mh_arg1);

	*p = gear_atol(ZSTR_VAL(new_value), ZSTR_LEN(new_value));
	return SUCCESS;
}
/* }}} */

GEAR_API GEAR_ICS_MH(OnUpdateLongGEZero) /* {{{ */
{
	gear_long *p, tmp;
#ifndef ZTS
	char *base = (char *) mh_arg2;
#else
	char *base;

	base = (char *) ts_resource(*((int *) mh_arg2));
#endif

	tmp = gear_atol(ZSTR_VAL(new_value), ZSTR_LEN(new_value));
	if (tmp < 0) {
		return FAILURE;
	}

	p = (gear_long *) (base+(size_t) mh_arg1);
	*p = tmp;

	return SUCCESS;
}
/* }}} */

GEAR_API GEAR_ICS_MH(OnUpdateReal) /* {{{ */
{
	double *p;
#ifndef ZTS
	char *base = (char *) mh_arg2;
#else
	char *base;

	base = (char *) ts_resource(*((int *) mh_arg2));
#endif

	p = (double *) (base+(size_t) mh_arg1);

	*p = gear_strtod(ZSTR_VAL(new_value), NULL);
	return SUCCESS;
}
/* }}} */

GEAR_API GEAR_ICS_MH(OnUpdateString) /* {{{ */
{
	char **p;
#ifndef ZTS
	char *base = (char *) mh_arg2;
#else
	char *base;

	base = (char *) ts_resource(*((int *) mh_arg2));
#endif

	p = (char **) (base+(size_t) mh_arg1);

	*p = new_value ? ZSTR_VAL(new_value) : NULL;
	return SUCCESS;
}
/* }}} */

GEAR_API GEAR_ICS_MH(OnUpdateStringUnempty) /* {{{ */
{
	char **p;
#ifndef ZTS
	char *base = (char *) mh_arg2;
#else
	char *base;

	base = (char *) ts_resource(*((int *) mh_arg2));
#endif

	if (new_value && !ZSTR_VAL(new_value)[0]) {
		return FAILURE;
	}

	p = (char **) (base+(size_t) mh_arg1);

	*p = new_value ? ZSTR_VAL(new_value) : NULL;
	return SUCCESS;
}
/* }}} */

