/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_SCANNER_H
#define GEAR_SCANNER_H

typedef struct _gear_lex_state {
	unsigned int yy_leng;
	unsigned char *yy_start;
	unsigned char *yy_text;
	unsigned char *yy_cursor;
	unsigned char *yy_marker;
	unsigned char *yy_limit;
	int yy_state;
	gear_stack state_stack;
	gear_ptr_stack heredoc_label_stack;

	gear_file_handle *in;
	uint32_t lineno;
	gear_string *filename;

	/* original (unfiltered) script */
	unsigned char *script_org;
	size_t script_org_size;

	/* filtered script */
	unsigned char *script_filtered;
	size_t script_filtered_size;

	/* input/output filters */
	gear_encoding_filter input_filter;
	gear_encoding_filter output_filter;
	const gear_encoding *script_encoding;

	/* hooks */
	void (*on_event)(gear_hyss_scanner_event event, int token, int line, void *context);
	void *on_event_context;

	gear_ast *ast;
	gear_arena *ast_arena;
} gear_lex_state;

typedef struct _gear_heredoc_label {
	char *label;
	int length;
	int indentation;
	gear_bool indentation_uses_spaces;
} gear_heredoc_label;

BEGIN_EXTERN_C()
GEAR_API void gear_save_lexical_state(gear_lex_state *lex_state);
GEAR_API void gear_restore_lexical_state(gear_lex_state *lex_state);
GEAR_API int gear_prepare_string_for_scanning(zval *str, char *filename);
GEAR_API void gear_multibyte_yyinput_again(gear_encoding_filter old_input_filter, const gear_encoding *old_encoding);
GEAR_API int gear_multibyte_set_filter(const gear_encoding *onetime_encoding);
GEAR_API void gear_lex_tstring(zval *zv);

END_EXTERN_C()

#endif

