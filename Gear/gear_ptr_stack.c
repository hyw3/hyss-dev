/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_ptr_stack.h"
#ifdef HAVE_STDARG_H
# include <stdarg.h>
#endif

GEAR_API void gear_ptr_stack_init_ex(gear_ptr_stack *stack, gear_bool persistent)
{
	stack->top_element = stack->elements = NULL;
	stack->top = stack->max = 0;
	stack->persistent = persistent;
}

GEAR_API void gear_ptr_stack_init(gear_ptr_stack *stack)
{
	gear_ptr_stack_init_ex(stack, 0);
}


GEAR_API void gear_ptr_stack_n_push(gear_ptr_stack *stack, int count, ...)
{
	va_list ptr;
	void *elem;

	GEAR_PTR_STACK_RESIZE_IF_NEEDED(stack, count)

	va_start(ptr, count);
	while (count>0) {
		elem = va_arg(ptr, void *);
		stack->top++;
		*(stack->top_element++) = elem;
		count--;
	}
	va_end(ptr);
}


GEAR_API void gear_ptr_stack_n_pop(gear_ptr_stack *stack, int count, ...)
{
	va_list ptr;
	void **elem;

	va_start(ptr, count);
	while (count>0) {
		elem = va_arg(ptr, void **);
		*elem = *(--stack->top_element);
		stack->top--;
		count--;
	}
	va_end(ptr);
}



GEAR_API void gear_ptr_stack_destroy(gear_ptr_stack *stack)
{
	if (stack->elements) {
		pefree(stack->elements, stack->persistent);
	}
}


GEAR_API void gear_ptr_stack_apply(gear_ptr_stack *stack, void (*func)(void *))
{
	int i = stack->top;

	while (--i >= 0) {
		func(stack->elements[i]);
	}
}

GEAR_API void gear_ptr_stack_reverse_apply(gear_ptr_stack *stack, void (*func)(void *))
{
	int i = 0;

	while (i < stack->top) {
		func(stack->elements[i++]);
	}
}


GEAR_API void gear_ptr_stack_clean(gear_ptr_stack *stack, void (*func)(void *), gear_bool free_elements)
{
	gear_ptr_stack_apply(stack, func);
	if (free_elements) {
		int i = stack->top;

		while (--i >= 0) {
			pefree(stack->elements[i], stack->persistent);
		}
	}
	stack->top = 0;
	stack->top_element = stack->elements;
}


GEAR_API int gear_ptr_stack_num_elements(gear_ptr_stack *stack)
{
	return stack->top;
}

