/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"
#include "gear_builtin_functions.h"
#include "gear_interfaces.h"
#include "gear_exceptions.h"
#include "gear_vm.h"
#include "gear_dtrace.h"
#include "gear_smart_str.h"

GEAR_API gear_class_entry *gear_ce_throwable;
GEAR_API gear_class_entry *gear_ce_exception;
GEAR_API gear_class_entry *gear_ce_error_exception;
GEAR_API gear_class_entry *gear_ce_error;
GEAR_API gear_class_entry *gear_ce_compile_error;
GEAR_API gear_class_entry *gear_ce_parse_error;
GEAR_API gear_class_entry *gear_ce_type_error;
GEAR_API gear_class_entry *gear_ce_argument_count_error;
GEAR_API gear_class_entry *gear_ce_arithmetic_error;
GEAR_API gear_class_entry *gear_ce_division_by_zero_error;

GEAR_API void (*gear_throw_exception_hook)(zval *ex);

static gear_object_handlers default_exception_handlers;

/* {{{ gear_implement_throwable */
static int gear_implement_throwable(gear_class_entry *interface, gear_class_entry *class_type)
{
	if (instanceof_function(class_type, gear_ce_exception) || instanceof_function(class_type, gear_ce_error)) {
		return SUCCESS;
	}
	gear_error_noreturn(E_ERROR, "Class %s cannot implement interface %s, extend %s or %s instead",
		ZSTR_VAL(class_type->name),
		ZSTR_VAL(interface->name),
		ZSTR_VAL(gear_ce_exception->name),
		ZSTR_VAL(gear_ce_error->name));
	return FAILURE;
}
/* }}} */

static inline gear_class_entry *i_get_exception_base(zval *object) /* {{{ */
{
	return instanceof_function(Z_OBJCE_P(object), gear_ce_exception) ? gear_ce_exception : gear_ce_error;
}
/* }}} */

GEAR_API gear_class_entry *gear_get_exception_base(zval *object) /* {{{ */
{
	return i_get_exception_base(object);
}
/* }}} */

void gear_exception_set_previous(gear_object *exception, gear_object *add_previous) /* {{{ */
{
    zval *previous, *ancestor, *ex;
	zval  pv, zv, rv;
	gear_class_entry *base_ce;

	if (exception == add_previous || !add_previous || !exception) {
		return;
	}
	ZVAL_OBJ(&pv, add_previous);
	if (!instanceof_function(Z_OBJCE(pv), gear_ce_throwable)) {
		gear_error_noreturn(E_CORE_ERROR, "Previous exception must implement Throwable");
		return;
	}
	ZVAL_OBJ(&zv, exception);
	ex = &zv;
	do {
		ancestor = gear_read_property_ex(i_get_exception_base(&pv), &pv, ZSTR_KNOWN(GEAR_STR_PREVIOUS), 1, &rv);
		while (Z_TYPE_P(ancestor) == IS_OBJECT) {
			if (Z_OBJ_P(ancestor) == Z_OBJ_P(ex)) {
				OBJ_RELEASE(add_previous);
				return;
			}
			ancestor = gear_read_property_ex(i_get_exception_base(ancestor), ancestor, ZSTR_KNOWN(GEAR_STR_PREVIOUS), 1, &rv);
		}
		base_ce = i_get_exception_base(ex);
		previous = gear_read_property_ex(base_ce, ex, ZSTR_KNOWN(GEAR_STR_PREVIOUS), 1, &rv);
		if (Z_TYPE_P(previous) == IS_NULL) {
			gear_update_property_ex(base_ce, ex, ZSTR_KNOWN(GEAR_STR_PREVIOUS), &pv);
			GC_DELREF(add_previous);
			return;
		}
		ex = previous;
	} while (Z_OBJ_P(ex) != add_previous);
}
/* }}} */

void gear_exception_save(void) /* {{{ */
{
	if (EG(prev_exception)) {
		gear_exception_set_previous(EG(exception), EG(prev_exception));
	}
	if (EG(exception)) {
		EG(prev_exception) = EG(exception);
	}
	EG(exception) = NULL;
}
/* }}} */

void gear_exception_restore(void) /* {{{ */
{
	if (EG(prev_exception)) {
		if (EG(exception)) {
			gear_exception_set_previous(EG(exception), EG(prev_exception));
		} else {
			EG(exception) = EG(prev_exception);
		}
		EG(prev_exception) = NULL;
	}
}
/* }}} */

GEAR_API GEAR_COLD void gear_throw_exception_internal(zval *exception) /* {{{ */
{
#ifdef HAVE_DTRACE
	if (DTRACE_EXCEPTION_THROWN_ENABLED()) {
		if (exception != NULL) {
			DTRACE_EXCEPTION_THROWN(ZSTR_VAL(Z_OBJ_P(exception)->ce->name));
		} else {
			DTRACE_EXCEPTION_THROWN(NULL);
		}
	}
#endif /* HAVE_DTRACE */

	if (exception != NULL) {
		gear_object *previous = EG(exception);
		gear_exception_set_previous(Z_OBJ_P(exception), EG(exception));
		EG(exception) = Z_OBJ_P(exception);
		if (previous) {
			return;
		}
	}
	if (!EG(current_execute_data)) {
		if (exception && (Z_OBJCE_P(exception) == gear_ce_parse_error || Z_OBJCE_P(exception) == gear_ce_compile_error)) {
			return;
		}
		if(EG(exception)) {
			gear_exception_error(EG(exception), E_ERROR);
		}
		gear_error_noreturn(E_CORE_ERROR, "Exception thrown without a stack frame");
	}

	if (gear_throw_exception_hook) {
		gear_throw_exception_hook(exception);
	}

	if (!EG(current_execute_data)->func ||
	    !GEAR_USER_CODE(EG(current_execute_data)->func->common.type) ||
	    EG(current_execute_data)->opline->opcode == GEAR_HANDLE_EXCEPTION) {
		/* no need to rethrow the exception */
		return;
	}
	EG(opline_before_exception) = EG(current_execute_data)->opline;
	EG(current_execute_data)->opline = EG(exception_op);
}
/* }}} */

GEAR_API void gear_clear_exception(void) /* {{{ */
{
	if (EG(prev_exception)) {

		OBJ_RELEASE(EG(prev_exception));
		EG(prev_exception) = NULL;
	}
	if (!EG(exception)) {
		return;
	}
	OBJ_RELEASE(EG(exception));
	EG(exception) = NULL;
	if (EG(current_execute_data)) {
		EG(current_execute_data)->opline = EG(opline_before_exception);
	}
#if GEAR_DEBUG
	EG(opline_before_exception) = NULL;
#endif
}
/* }}} */

static gear_object *gear_default_exception_new_ex(gear_class_entry *class_type, int skip_top_traces) /* {{{ */
{
	zval obj, tmp;
	gear_object *object;
	zval trace;
	gear_class_entry *base_ce;
	gear_string *filename;

	Z_OBJ(obj) = object = gear_objects_new(class_type);
	Z_OBJ_HT(obj) = &default_exception_handlers;

	object_properties_init(object, class_type);

	if (EG(current_execute_data)) {
		gear_fetch_debug_backtrace(&trace, skip_top_traces, 0, 0);
	} else {
		array_init(&trace);
	}
	Z_SET_REFCOUNT(trace, 0);

	base_ce = i_get_exception_base(&obj);

	if (EXPECTED((class_type != gear_ce_parse_error && class_type != gear_ce_compile_error)
			|| !(filename = gear_get_compiled_filename()))) {
		ZVAL_STRING(&tmp, gear_get_executed_filename());
		gear_update_property_ex(base_ce, &obj, ZSTR_KNOWN(GEAR_STR_FILE), &tmp);
		zval_ptr_dtor(&tmp);
		ZVAL_LONG(&tmp, gear_get_executed_lineno());
		gear_update_property_ex(base_ce, &obj, ZSTR_KNOWN(GEAR_STR_LINE), &tmp);
	} else {
		ZVAL_STR(&tmp, filename);
		gear_update_property_ex(base_ce, &obj, ZSTR_KNOWN(GEAR_STR_FILE), &tmp);
		ZVAL_LONG(&tmp, gear_get_compiled_lineno());
		gear_update_property_ex(base_ce, &obj, ZSTR_KNOWN(GEAR_STR_LINE), &tmp);
	}
	gear_update_property_ex(base_ce, &obj, ZSTR_KNOWN(GEAR_STR_TRACE), &trace);

	return object;
}
/* }}} */

static gear_object *gear_default_exception_new(gear_class_entry *class_type) /* {{{ */
{
	return gear_default_exception_new_ex(class_type, 0);
}
/* }}} */

static gear_object *gear_error_exception_new(gear_class_entry *class_type) /* {{{ */
{
	return gear_default_exception_new_ex(class_type, 2);
}
/* }}} */

/* {{{ proto Exception|Error Exception|Error::__clone()
   Clone the exception object */
GEAR_COLD GEAR_METHOD(exception, __clone)
{
	/* Should never be executable */
	gear_throw_exception(NULL, "Cannot clone object using __clone()", 0);
}
/* }}} */

/* {{{ proto Exception|Error::__construct(string message, int code [, Throwable previous])
   Exception constructor */
GEAR_METHOD(exception, __construct)
{
	gear_string *message = NULL;
	gear_long   code = 0;
	zval  tmp, *object, *previous = NULL;
	gear_class_entry *base_ce;
	int    argc = GEAR_NUM_ARGS();

	object = getThis();
	base_ce = i_get_exception_base(object);

	if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, argc, "|SlO!", &message, &code, &previous, gear_ce_throwable) == FAILURE) {
		gear_class_entry *ce;

		if (Z_TYPE(EX(This)) == IS_OBJECT) {
			ce = Z_OBJCE(EX(This));
		} else if (Z_CE(EX(This))) {
			ce = Z_CE(EX(This));
		} else {
			ce = base_ce;
		}
		gear_throw_error(NULL, "Wrong parameters for %s([string $message [, long $code [, Throwable $previous = NULL]]])", ZSTR_VAL(ce->name));
		return;
	}

	if (message) {
		ZVAL_STR(&tmp, message);
		gear_update_property_ex(base_ce, object, ZSTR_KNOWN(GEAR_STR_MESSAGE), &tmp);
	}

	if (code) {
		ZVAL_LONG(&tmp, code);
		gear_update_property_ex(base_ce, object, ZSTR_KNOWN(GEAR_STR_CODE), &tmp);
	}

	if (previous) {
		gear_update_property_ex(base_ce, object, ZSTR_KNOWN(GEAR_STR_PREVIOUS), previous);
	}
}
/* }}} */

/* {{{ proto Exception::__wakeup()
   Exception unserialize checks */
#define CHECK_EXC_TYPE(id, type) \
	pvalue = gear_read_property_ex(i_get_exception_base(object), (object), ZSTR_KNOWN(id), 1, &value); \
	if (Z_TYPE_P(pvalue) != IS_NULL && Z_TYPE_P(pvalue) != type) { \
		gear_unset_property(i_get_exception_base(object), object, ZSTR_VAL(ZSTR_KNOWN(id)), ZSTR_LEN(ZSTR_KNOWN(id))); \
	}

GEAR_METHOD(exception, __wakeup)
{
	zval value, *pvalue;
	zval *object = getThis();
	CHECK_EXC_TYPE(GEAR_STR_MESSAGE,  IS_STRING);
	CHECK_EXC_TYPE(GEAR_STR_STRING,   IS_STRING);
	CHECK_EXC_TYPE(GEAR_STR_CODE,     IS_LONG);
	CHECK_EXC_TYPE(GEAR_STR_FILE,     IS_STRING);
	CHECK_EXC_TYPE(GEAR_STR_LINE,     IS_LONG);
	CHECK_EXC_TYPE(GEAR_STR_TRACE,    IS_ARRAY);
	pvalue = gear_read_property(i_get_exception_base(object), object, "previous", sizeof("previous")-1, 1, &value);
	if (pvalue && Z_TYPE_P(pvalue) != IS_NULL && (Z_TYPE_P(pvalue) != IS_OBJECT ||
			!instanceof_function(Z_OBJCE_P(pvalue), gear_ce_throwable) ||
			pvalue == object)) {
		gear_unset_property(i_get_exception_base(object), object, "previous", sizeof("previous")-1);
	}
}
/* }}} */

/* {{{ proto ErrorException::__construct(string message, int code, int severity [, string filename [, int lineno [, Throwable previous]]])
   ErrorException constructor */
GEAR_METHOD(error_exception, __construct)
{
	gear_string *message = NULL, *filename = NULL;
	gear_long   code = 0, severity = E_ERROR, lineno;
	zval   tmp, *object, *previous = NULL;
	int    argc = GEAR_NUM_ARGS();

	if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, argc, "|SllSlO!", &message, &code, &severity, &filename, &lineno, &previous, gear_ce_throwable) == FAILURE) {
		gear_class_entry *ce;

		if (Z_TYPE(EX(This)) == IS_OBJECT) {
			ce = Z_OBJCE(EX(This));
		} else if (Z_CE(EX(This))) {
			ce = Z_CE(EX(This));
		} else {
			ce = gear_ce_error_exception;
		}
		gear_throw_error(NULL, "Wrong parameters for %s([string $message [, long $code, [ long $severity, [ string $filename, [ long $lineno  [, Throwable $previous = NULL]]]]]])", ZSTR_VAL(ce->name));
		return;
	}

	object = getThis();

	if (message) {
		ZVAL_STR_COPY(&tmp, message);
		gear_update_property_ex(gear_ce_exception, object, ZSTR_KNOWN(GEAR_STR_MESSAGE), &tmp);
		zval_ptr_dtor(&tmp);
	}

	if (code) {
		ZVAL_LONG(&tmp, code);
		gear_update_property_ex(gear_ce_exception, object, ZSTR_KNOWN(GEAR_STR_CODE), &tmp);
	}

	if (previous) {
		gear_update_property_ex(gear_ce_exception, object, ZSTR_KNOWN(GEAR_STR_PREVIOUS), previous);
	}

	ZVAL_LONG(&tmp, severity);
	gear_update_property_ex(gear_ce_exception, object, ZSTR_KNOWN(GEAR_STR_SEVERITY), &tmp);

	if (argc >= 4) {
		ZVAL_STR_COPY(&tmp, filename);
		gear_update_property_ex(gear_ce_exception, object, ZSTR_KNOWN(GEAR_STR_FILE), &tmp);
		zval_ptr_dtor(&tmp);
    	if (argc < 5) {
    	    lineno = 0; /* invalidate lineno */
    	}
		ZVAL_LONG(&tmp, lineno);
		gear_update_property_ex(gear_ce_exception, object, ZSTR_KNOWN(GEAR_STR_LINE), &tmp);
	}
}
/* }}} */

#define DEFAULT_0_PARAMS \
	if (gear_parse_parameters_none() == FAILURE) { \
		return; \
	}

#define GET_PROPERTY(object, id) \
	gear_read_property_ex(i_get_exception_base(object), (object), ZSTR_KNOWN(id), 0, &rv)
#define GET_PROPERTY_SILENT(object, id) \
	gear_read_property_ex(i_get_exception_base(object), (object), ZSTR_KNOWN(id), 1, &rv)

/* {{{ proto string Exception|Error::getFile()
   Get the file in which the exception occurred */
GEAR_METHOD(exception, getFile)
{
	zval *prop, rv;

	DEFAULT_0_PARAMS;

	prop = GET_PROPERTY(getThis(), GEAR_STR_FILE);
	ZVAL_DEREF(prop);
	ZVAL_COPY(return_value, prop);
}
/* }}} */

/* {{{ proto int Exception|Error::getLine()
   Get the line in which the exception occurred */
GEAR_METHOD(exception, getLine)
{
	zval *prop, rv;

	DEFAULT_0_PARAMS;

	prop = GET_PROPERTY(getThis(), GEAR_STR_LINE);
	ZVAL_DEREF(prop);
	ZVAL_COPY(return_value, prop);
}
/* }}} */

/* {{{ proto string Exception|Error::getMessage()
   Get the exception message */
GEAR_METHOD(exception, getMessage)
{
	zval *prop, rv;

	DEFAULT_0_PARAMS;

	prop = GET_PROPERTY(getThis(), GEAR_STR_MESSAGE);
	ZVAL_DEREF(prop);
	ZVAL_COPY(return_value, prop);
}
/* }}} */

/* {{{ proto int Exception|Error::getCode()
   Get the exception code */
GEAR_METHOD(exception, getCode)
{
	zval *prop, rv;

	DEFAULT_0_PARAMS;

	prop = GET_PROPERTY(getThis(), GEAR_STR_CODE);
	ZVAL_DEREF(prop);
	ZVAL_COPY(return_value, prop);
}
/* }}} */

/* {{{ proto array Exception|Error::getTrace()
   Get the stack trace for the location in which the exception occurred */
GEAR_METHOD(exception, getTrace)
{
	zval *prop, rv;

	DEFAULT_0_PARAMS;

	prop = GET_PROPERTY(getThis(), GEAR_STR_TRACE);
	ZVAL_DEREF(prop);
	ZVAL_COPY(return_value, prop);
}
/* }}} */

/* {{{ proto int ErrorException::getSeverity()
   Get the exception severity */
GEAR_METHOD(error_exception, getSeverity)
{
	zval *prop, rv;

	DEFAULT_0_PARAMS;

	prop = GET_PROPERTY(getThis(), GEAR_STR_SEVERITY);
	ZVAL_DEREF(prop);
	ZVAL_COPY(return_value, prop);
}
/* }}} */

#define TRACE_APPEND_KEY(key) do {                                          \
		tmp = gear_hash_find(ht, key);                                      \
		if (tmp) {                                                          \
			if (Z_TYPE_P(tmp) != IS_STRING) {                               \
				gear_error(E_WARNING, "Value for %s is no string",          \
					ZSTR_VAL(key));                                         \
				smart_str_appends(str, "[unknown]");                        \
			} else {                                                        \
				smart_str_appends(str, Z_STRVAL_P(tmp));                    \
			}                                                               \
		} \
	} while (0)

static void _build_trace_args(zval *arg, smart_str *str) /* {{{ */
{
	/* the trivial way would be to do
	 * convert_to_string_ex(arg);
	 * append it and kill the now tmp arg.
	 * but that could cause some E_NOTICE and also damn long lines.
	 */

	ZVAL_DEREF(arg);
	switch (Z_TYPE_P(arg)) {
		case IS_NULL:
			smart_str_appends(str, "NULL, ");
			break;
		case IS_STRING:
			smart_str_appendc(str, '\'');
			smart_str_append_escaped(str, Z_STRVAL_P(arg), MIN(Z_STRLEN_P(arg), 15));
			if (Z_STRLEN_P(arg) > 15) {
				smart_str_appends(str, "...', ");
			} else {
				smart_str_appends(str, "', ");
			}
			break;
		case IS_FALSE:
			smart_str_appends(str, "false, ");
			break;
		case IS_TRUE:
			smart_str_appends(str, "true, ");
			break;
		case IS_RESOURCE:
			smart_str_appends(str, "Resource id #");
			smart_str_append_long(str, Z_RES_HANDLE_P(arg));
			smart_str_appends(str, ", ");
			break;
		case IS_LONG:
			smart_str_append_long(str, Z_LVAL_P(arg));
			smart_str_appends(str, ", ");
			break;
		case IS_DOUBLE: {
			smart_str_append_printf(str, "%.*G", (int) EG(precision), Z_DVAL_P(arg));
			smart_str_appends(str, ", ");
			break;
		}
		case IS_ARRAY:
			smart_str_appends(str, "Array, ");
			break;
		case IS_OBJECT: {
			gear_string *class_name = Z_OBJ_HANDLER_P(arg, get_class_name)(Z_OBJ_P(arg));
			smart_str_appends(str, "Object(");
			smart_str_appends(str, ZSTR_VAL(class_name));
			smart_str_appends(str, "), ");
			gear_string_release_ex(class_name, 0);
			break;
		}
	}
}
/* }}} */

static void _build_trace_string(smart_str *str, HashTable *ht, uint32_t num) /* {{{ */
{
	zval *file, *tmp;

	smart_str_appendc(str, '#');
	smart_str_append_long(str, num);
	smart_str_appendc(str, ' ');

	file = gear_hash_find_ex(ht, ZSTR_KNOWN(GEAR_STR_FILE), 1);
	if (file) {
		if (Z_TYPE_P(file) != IS_STRING) {
			gear_error(E_WARNING, "Function name is no string");
			smart_str_appends(str, "[unknown function]");
		} else{
			gear_long line;
			tmp = gear_hash_find_ex(ht, ZSTR_KNOWN(GEAR_STR_LINE), 1);
			if (tmp) {
				if (Z_TYPE_P(tmp) == IS_LONG) {
					line = Z_LVAL_P(tmp);
				} else {
					gear_error(E_WARNING, "Line is no long");
					line = 0;
				}
			} else {
				line = 0;
			}
			smart_str_append(str, Z_STR_P(file));
			smart_str_appendc(str, '(');
			smart_str_append_long(str, line);
			smart_str_appends(str, "): ");
		}
	} else {
		smart_str_appends(str, "[internal function]: ");
	}
	TRACE_APPEND_KEY(ZSTR_KNOWN(GEAR_STR_CLASS));
	TRACE_APPEND_KEY(ZSTR_KNOWN(GEAR_STR_TYPE));
	TRACE_APPEND_KEY(ZSTR_KNOWN(GEAR_STR_FUNCTION));
	smart_str_appendc(str, '(');
	tmp = gear_hash_find_ex(ht, ZSTR_KNOWN(GEAR_STR_ARGS), 1);
	if (tmp) {
		if (Z_TYPE_P(tmp) == IS_ARRAY) {
			size_t last_len = ZSTR_LEN(str->s);
			zval *arg;

			GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(tmp), arg) {
				_build_trace_args(arg, str);
			} GEAR_HASH_FOREACH_END();

			if (last_len != ZSTR_LEN(str->s)) {
				ZSTR_LEN(str->s) -= 2; /* remove last ', ' */
			}
		} else {
			gear_error(E_WARNING, "args element is no array");
		}
	}
	smart_str_appends(str, ")\n");
}
/* }}} */

/* {{{ proto string Exception|Error::getTraceAsString()
   Obtain the backtrace for the exception as a string (instead of an array) */
GEAR_METHOD(exception, getTraceAsString)
{
	zval *trace, *frame, rv;
	gear_ulong index;
	zval *object;
	gear_class_entry *base_ce;
	smart_str str = {0};
	uint32_t num = 0;

	DEFAULT_0_PARAMS;

	object = getThis();
	base_ce = i_get_exception_base(object);

	trace = gear_read_property_ex(base_ce, object, ZSTR_KNOWN(GEAR_STR_TRACE), 1, &rv);
	if (Z_TYPE_P(trace) != IS_ARRAY) {
		RETURN_FALSE;
	}
	GEAR_HASH_FOREACH_NUM_KEY_VAL(Z_ARRVAL_P(trace), index, frame) {
		if (Z_TYPE_P(frame) != IS_ARRAY) {
			gear_error(E_WARNING, "Expected array for frame " GEAR_ULONG_FMT, index);
			continue;
		}

		_build_trace_string(&str, Z_ARRVAL_P(frame), num++);
	} GEAR_HASH_FOREACH_END();

	smart_str_appendc(&str, '#');
	smart_str_append_long(&str, num);
	smart_str_appends(&str, " {main}");
	smart_str_0(&str);

	RETURN_NEW_STR(str.s);
}
/* }}} */

/* {{{ proto Throwable Exception|Error::getPrevious()
   Return previous Throwable or NULL. */
GEAR_METHOD(exception, getPrevious)
{
	zval rv;

	DEFAULT_0_PARAMS;

	ZVAL_COPY(return_value, GET_PROPERTY_SILENT(getThis(), GEAR_STR_PREVIOUS));
} /* }}} */

/* {{{ proto string Exception|Error::__toString()
   Obtain the string representation of the Exception object */
GEAR_METHOD(exception, __toString)
{
	zval trace, *exception;
	gear_class_entry *base_ce;
	gear_string *str;
	gear_fcall_info fci;
	zval rv, tmp;
	gear_string *fname;

	DEFAULT_0_PARAMS;

	str = ZSTR_EMPTY_ALLOC();

	exception = getThis();
	fname = gear_string_init("gettraceasstring", sizeof("gettraceasstring")-1, 0);

	while (exception && Z_TYPE_P(exception) == IS_OBJECT && instanceof_function(Z_OBJCE_P(exception), gear_ce_throwable)) {
		gear_string *prev_str = str;
		gear_string *message = zval_get_string(GET_PROPERTY(exception, GEAR_STR_MESSAGE));
		gear_string *file = zval_get_string(GET_PROPERTY(exception, GEAR_STR_FILE));
		gear_long line = zval_get_long(GET_PROPERTY(exception, GEAR_STR_LINE));

		fci.size = sizeof(fci);
		ZVAL_STR(&fci.function_name, fname);
		fci.object = Z_OBJ_P(exception);
		fci.retval = &trace;
		fci.param_count = 0;
		fci.params = NULL;
		fci.no_separation = 1;

		gear_call_function(&fci, NULL);

		if (Z_TYPE(trace) != IS_STRING) {
			zval_ptr_dtor(&trace);
			ZVAL_UNDEF(&trace);
		}

		if ((Z_OBJCE_P(exception) == gear_ce_type_error || Z_OBJCE_P(exception) == gear_ce_argument_count_error) && strstr(ZSTR_VAL(message), ", called in ")) {
			gear_string *real_message = gear_strpprintf(0, "%s and defined", ZSTR_VAL(message));
			gear_string_release_ex(message, 0);
			message = real_message;
		}

		if (ZSTR_LEN(message) > 0) {
			str = gear_strpprintf(0, "%s: %s in %s:" GEAR_LONG_FMT
					"\nStack trace:\n%s%s%s",
					ZSTR_VAL(Z_OBJCE_P(exception)->name), ZSTR_VAL(message), ZSTR_VAL(file), line,
					(Z_TYPE(trace) == IS_STRING && Z_STRLEN(trace)) ? Z_STRVAL(trace) : "#0 {main}\n",
					ZSTR_LEN(prev_str) ? "\n\nNext " : "", ZSTR_VAL(prev_str));
		} else {
			str = gear_strpprintf(0, "%s in %s:" GEAR_LONG_FMT
					"\nStack trace:\n%s%s%s",
					ZSTR_VAL(Z_OBJCE_P(exception)->name), ZSTR_VAL(file), line,
					(Z_TYPE(trace) == IS_STRING && Z_STRLEN(trace)) ? Z_STRVAL(trace) : "#0 {main}\n",
					ZSTR_LEN(prev_str) ? "\n\nNext " : "", ZSTR_VAL(prev_str));
		}

		gear_string_release_ex(prev_str, 0);
		gear_string_release_ex(message, 0);
		gear_string_release_ex(file, 0);
		zval_ptr_dtor(&trace);

		Z_PROTECT_RECURSION_P(exception);
		exception = GET_PROPERTY(exception, GEAR_STR_PREVIOUS);
		if (exception && Z_TYPE_P(exception) == IS_OBJECT && Z_IS_RECURSIVE_P(exception)) {
			break;
		}
	}
	gear_string_release_ex(fname, 0);

	exception = getThis();
	/* Reset apply counts */
	while (exception && Z_TYPE_P(exception) == IS_OBJECT && (base_ce = i_get_exception_base(exception)) && instanceof_function(Z_OBJCE_P(exception), base_ce)) {
		if (Z_IS_RECURSIVE_P(exception)) {
			Z_UNPROTECT_RECURSION_P(exception);
		} else {
			break;
		}
		exception = GET_PROPERTY(exception, GEAR_STR_PREVIOUS);
	}

	exception = getThis();
	base_ce = i_get_exception_base(exception);

	/* We store the result in the private property string so we can access
	 * the result in uncaught exception handlers without memleaks. */
	ZVAL_STR(&tmp, str);
	gear_update_property_ex(base_ce, exception, ZSTR_KNOWN(GEAR_STR_STRING), &tmp);

	RETURN_STR(str);
}
/* }}} */

/** {{{ Throwable method definition */
static const gear_function_entry gear_funcs_throwable[] = {
	GEAR_ABSTRACT_ME(throwable, getMessage,       NULL)
	GEAR_ABSTRACT_ME(throwable, getCode,          NULL)
	GEAR_ABSTRACT_ME(throwable, getFile,          NULL)
	GEAR_ABSTRACT_ME(throwable, getLine,          NULL)
	GEAR_ABSTRACT_ME(throwable, getTrace,         NULL)
	GEAR_ABSTRACT_ME(throwable, getPrevious,      NULL)
	GEAR_ABSTRACT_ME(throwable, getTraceAsString, NULL)
	GEAR_ABSTRACT_ME(throwable, __toString,       NULL)
	GEAR_FE_END
};
/* }}} */

/* {{{ internal structs */
/* All functions that may be used in uncaught exception handlers must be final
 * and must not throw exceptions. Otherwise we would need a facility to handle
 * such exceptions in that handler.
 * Also all getXY() methods are final because thy serve as read only access to
 * their corresponding properties, no more, no less. If after all you need to
 * override somthing then it is method __toString().
 * And never try to change the state of exceptions and never implement anything
 * that gives the user anything to accomplish this.
 */
GEAR_BEGIN_ARG_INFO_EX(arginfo_exception___construct, 0, 0, 0)
	GEAR_ARG_INFO(0, message)
	GEAR_ARG_INFO(0, code)
	GEAR_ARG_INFO(0, previous)
GEAR_END_ARG_INFO()

static const gear_function_entry default_exception_functions[] = {
	GEAR_ME(exception, __clone, NULL, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(exception, __construct, arginfo_exception___construct, GEAR_ACC_PUBLIC)
	GEAR_ME(exception, __wakeup, NULL, GEAR_ACC_PUBLIC)
	GEAR_ME(exception, getMessage, NULL, GEAR_ACC_PUBLIC|GEAR_ACC_FINAL)
	GEAR_ME(exception, getCode, NULL, GEAR_ACC_PUBLIC|GEAR_ACC_FINAL)
	GEAR_ME(exception, getFile, NULL, GEAR_ACC_PUBLIC|GEAR_ACC_FINAL)
	GEAR_ME(exception, getLine, NULL, GEAR_ACC_PUBLIC|GEAR_ACC_FINAL)
	GEAR_ME(exception, getTrace, NULL, GEAR_ACC_PUBLIC|GEAR_ACC_FINAL)
	GEAR_ME(exception, getPrevious, NULL, GEAR_ACC_PUBLIC|GEAR_ACC_FINAL)
	GEAR_ME(exception, getTraceAsString, NULL, GEAR_ACC_PUBLIC|GEAR_ACC_FINAL)
	GEAR_ME(exception, __toString, NULL, 0)
	GEAR_FE_END
};

GEAR_BEGIN_ARG_INFO_EX(arginfo_error_exception___construct, 0, 0, 0)
	GEAR_ARG_INFO(0, message)
	GEAR_ARG_INFO(0, code)
	GEAR_ARG_INFO(0, severity)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, lineno)
	GEAR_ARG_INFO(0, previous)
GEAR_END_ARG_INFO()

static const gear_function_entry error_exception_functions[] = {
	GEAR_ME(error_exception, __construct, arginfo_error_exception___construct, GEAR_ACC_PUBLIC)
	GEAR_ME(error_exception, getSeverity, NULL, GEAR_ACC_PUBLIC|GEAR_ACC_FINAL)
	GEAR_FE_END
};
/* }}} */

void gear_register_default_exception(void) /* {{{ */
{
	gear_class_entry ce;

	REGISTER_MAGIC_INTERFACE(throwable, Throwable);

	memcpy(&default_exception_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	default_exception_handlers.clone_obj = NULL;

	INIT_CLASS_ENTRY(ce, "Exception", default_exception_functions);
	gear_ce_exception = gear_register_internal_class_ex(&ce, NULL);
	gear_ce_exception->create_object = gear_default_exception_new;
	gear_class_implements(gear_ce_exception, 1, gear_ce_throwable);

	gear_declare_property_string(gear_ce_exception, "message", sizeof("message")-1, "", GEAR_ACC_PROTECTED);
	gear_declare_property_string(gear_ce_exception, "string", sizeof("string")-1, "", GEAR_ACC_PRIVATE);
	gear_declare_property_long(gear_ce_exception, "code", sizeof("code")-1, 0, GEAR_ACC_PROTECTED);
	gear_declare_property_null(gear_ce_exception, "file", sizeof("file")-1, GEAR_ACC_PROTECTED);
	gear_declare_property_null(gear_ce_exception, "line", sizeof("line")-1, GEAR_ACC_PROTECTED);
	gear_declare_property_null(gear_ce_exception, "trace", sizeof("trace")-1, GEAR_ACC_PRIVATE);
	gear_declare_property_null(gear_ce_exception, "previous", sizeof("previous")-1, GEAR_ACC_PRIVATE);

	INIT_CLASS_ENTRY(ce, "ErrorException", error_exception_functions);
	gear_ce_error_exception = gear_register_internal_class_ex(&ce, gear_ce_exception);
	gear_ce_error_exception->create_object = gear_error_exception_new;
	gear_declare_property_long(gear_ce_error_exception, "severity", sizeof("severity")-1, E_ERROR, GEAR_ACC_PROTECTED);

	INIT_CLASS_ENTRY(ce, "Error", default_exception_functions);
	gear_ce_error = gear_register_internal_class_ex(&ce, NULL);
	gear_ce_error->create_object = gear_default_exception_new;
	gear_class_implements(gear_ce_error, 1, gear_ce_throwable);

	gear_declare_property_string(gear_ce_error, "message", sizeof("message")-1, "", GEAR_ACC_PROTECTED);
	gear_declare_property_string(gear_ce_error, "string", sizeof("string")-1, "", GEAR_ACC_PRIVATE);
	gear_declare_property_long(gear_ce_error, "code", sizeof("code")-1, 0, GEAR_ACC_PROTECTED);
	gear_declare_property_null(gear_ce_error, "file", sizeof("file")-1, GEAR_ACC_PROTECTED);
	gear_declare_property_null(gear_ce_error, "line", sizeof("line")-1, GEAR_ACC_PROTECTED);
	gear_declare_property_null(gear_ce_error, "trace", sizeof("trace")-1, GEAR_ACC_PRIVATE);
	gear_declare_property_null(gear_ce_error, "previous", sizeof("previous")-1, GEAR_ACC_PRIVATE);

	INIT_CLASS_ENTRY(ce, "CompileError", NULL);
	gear_ce_compile_error = gear_register_internal_class_ex(&ce, gear_ce_error);
	gear_ce_compile_error->create_object = gear_default_exception_new;

	INIT_CLASS_ENTRY(ce, "ParseError", NULL);
	gear_ce_parse_error = gear_register_internal_class_ex(&ce, gear_ce_compile_error);
	gear_ce_parse_error->create_object = gear_default_exception_new;

	INIT_CLASS_ENTRY(ce, "TypeError", NULL);
	gear_ce_type_error = gear_register_internal_class_ex(&ce, gear_ce_error);
	gear_ce_type_error->create_object = gear_default_exception_new;

	INIT_CLASS_ENTRY(ce, "ArgumentCountError", NULL);
	gear_ce_argument_count_error = gear_register_internal_class_ex(&ce, gear_ce_type_error);
	gear_ce_argument_count_error->create_object = gear_default_exception_new;

	INIT_CLASS_ENTRY(ce, "ArithmeticError", NULL);
	gear_ce_arithmetic_error = gear_register_internal_class_ex(&ce, gear_ce_error);
	gear_ce_arithmetic_error->create_object = gear_default_exception_new;

	INIT_CLASS_ENTRY(ce, "DivisionByZeroError", NULL);
	gear_ce_division_by_zero_error = gear_register_internal_class_ex(&ce, gear_ce_arithmetic_error);
	gear_ce_division_by_zero_error->create_object = gear_default_exception_new;
}
/* }}} */

/* {{{ Deprecated - Use gear_ce_exception directly instead */
GEAR_API gear_class_entry *gear_exception_get_default(void)
{
	return gear_ce_exception;
}
/* }}} */

/* {{{ Deprecated - Use gear_ce_error_exception directly instead */
GEAR_API gear_class_entry *gear_get_error_exception(void)
{
	return gear_ce_error_exception;
}
/* }}} */

GEAR_API GEAR_COLD gear_object *gear_throw_exception(gear_class_entry *exception_ce, const char *message, gear_long code) /* {{{ */
{
	zval ex, tmp;

	if (exception_ce) {
		if (!instanceof_function(exception_ce, gear_ce_throwable)) {
			gear_error(E_NOTICE, "Exceptions must implement Throwable");
			exception_ce = gear_ce_exception;
		}
	} else {
		exception_ce = gear_ce_exception;
	}
	object_init_ex(&ex, exception_ce);


	if (message) {
		ZVAL_STRING(&tmp, message);
		gear_update_property_ex(exception_ce, &ex, ZSTR_KNOWN(GEAR_STR_MESSAGE), &tmp);
		zval_ptr_dtor(&tmp);
	}
	if (code) {
		ZVAL_LONG(&tmp, code);
		gear_update_property_ex(exception_ce, &ex, ZSTR_KNOWN(GEAR_STR_CODE), &tmp);
	}

	gear_throw_exception_internal(&ex);
	return Z_OBJ(ex);
}
/* }}} */

GEAR_API GEAR_COLD gear_object *gear_throw_exception_ex(gear_class_entry *exception_ce, gear_long code, const char *format, ...) /* {{{ */
{
	va_list arg;
	char *message;
	gear_object *obj;

	va_start(arg, format);
	gear_vspprintf(&message, 0, format, arg);
	va_end(arg);
	obj = gear_throw_exception(exception_ce, message, code);
	efree(message);
	return obj;
}
/* }}} */

GEAR_API GEAR_COLD gear_object *gear_throw_error_exception(gear_class_entry *exception_ce, const char *message, gear_long code, int severity) /* {{{ */
{
	zval ex, tmp;
	gear_object *obj = gear_throw_exception(exception_ce, message, code);
	ZVAL_OBJ(&ex, obj);
	ZVAL_LONG(&tmp, severity);
	gear_update_property_ex(gear_ce_error_exception, &ex, ZSTR_KNOWN(GEAR_STR_SEVERITY), &tmp);
	return obj;
}
/* }}} */

static void gear_error_va(int type, const char *file, uint32_t lineno, const char *format, ...) /* {{{ */
{
	va_list args;

	va_start(args, format);
	gear_error_cb(type, file, lineno, format, args);
	va_end(args);
}
/* }}} */

static void gear_error_helper(int type, const char *filename, const uint32_t lineno, const char *format, ...) /* {{{ */
{
	va_list va;

	va_start(va, format);
	gear_error_cb(type, filename, lineno, format, va);
	va_end(va);
}
/* }}} */

/* This function doesn't return if it uses E_ERROR */
GEAR_API GEAR_COLD void gear_exception_error(gear_object *ex, int severity) /* {{{ */
{
	zval exception, rv;
	gear_class_entry *ce_exception;

	ZVAL_OBJ(&exception, ex);
	ce_exception = Z_OBJCE(exception);
	EG(exception) = NULL;
	if (ce_exception == gear_ce_parse_error || ce_exception == gear_ce_compile_error) {
		gear_string *message = zval_get_string(GET_PROPERTY(&exception, GEAR_STR_MESSAGE));
		gear_string *file = zval_get_string(GET_PROPERTY_SILENT(&exception, GEAR_STR_FILE));
		gear_long line = zval_get_long(GET_PROPERTY_SILENT(&exception, GEAR_STR_LINE));

		gear_error_helper(ce_exception == gear_ce_parse_error ? E_PARSE : E_COMPILE_ERROR,
			ZSTR_VAL(file), line, "%s", ZSTR_VAL(message));

		gear_string_release_ex(file, 0);
		gear_string_release_ex(message, 0);
	} else if (instanceof_function(ce_exception, gear_ce_throwable)) {
		zval tmp, rv;
		gear_string *str, *file = NULL;
		gear_long line = 0;

		gear_call_method_with_0_params(&exception, ce_exception, &ex->ce->__tostring, "__tostring", &tmp);
		if (!EG(exception)) {
			if (Z_TYPE(tmp) != IS_STRING) {
				gear_error(E_WARNING, "%s::__toString() must return a string", ZSTR_VAL(ce_exception->name));
			} else {
				gear_update_property_ex(i_get_exception_base(&exception), &exception, ZSTR_KNOWN(GEAR_STR_STRING), &tmp);
			}
		}
		zval_ptr_dtor(&tmp);

		if (EG(exception)) {
			zval zv;

			ZVAL_OBJ(&zv, EG(exception));
			/* do the best we can to inform about the inner exception */
			if (instanceof_function(ce_exception, gear_ce_exception) || instanceof_function(ce_exception, gear_ce_error)) {
				file = zval_get_string(GET_PROPERTY_SILENT(&zv, GEAR_STR_FILE));
				line = zval_get_long(GET_PROPERTY_SILENT(&zv, GEAR_STR_LINE));
			}

			gear_error_va(E_WARNING, (file && ZSTR_LEN(file) > 0) ? ZSTR_VAL(file) : NULL, line,
				"Uncaught %s in exception handling during call to %s::__tostring()",
				ZSTR_VAL(Z_OBJCE(zv)->name), ZSTR_VAL(ce_exception->name));

			if (file) {
				gear_string_release_ex(file, 0);
			}
		}

		str = zval_get_string(GET_PROPERTY_SILENT(&exception, GEAR_STR_STRING));
		file = zval_get_string(GET_PROPERTY_SILENT(&exception, GEAR_STR_FILE));
		line = zval_get_long(GET_PROPERTY_SILENT(&exception, GEAR_STR_LINE));

		gear_error_va(severity, (file && ZSTR_LEN(file) > 0) ? ZSTR_VAL(file) : NULL, line,
			"Uncaught %s\n  thrown", ZSTR_VAL(str));

		gear_string_release_ex(str, 0);
		gear_string_release_ex(file, 0);
	} else {
		gear_error(severity, "Uncaught exception '%s'", ZSTR_VAL(ce_exception->name));
	}

	OBJ_RELEASE(ex);
}
/* }}} */

GEAR_API GEAR_COLD void gear_throw_exception_object(zval *exception) /* {{{ */
{
	gear_class_entry *exception_ce;

	if (exception == NULL || Z_TYPE_P(exception) != IS_OBJECT) {
		gear_error_noreturn(E_CORE_ERROR, "Need to supply an object when throwing an exception");
	}

	exception_ce = Z_OBJCE_P(exception);

	if (!exception_ce || !instanceof_function(exception_ce, gear_ce_throwable)) {
		gear_throw_error(NULL, "Cannot throw objects that do not implement Throwable");
		zval_ptr_dtor(exception);
		return;
	}
	gear_throw_exception_internal(exception);
}
/* }}} */

