/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_execute.h"
#include "gear_API.h"
#include "gear_capis.h"
#include "gear_extensions.h"
#include "gear_constants.h"
#include "gear_interfaces.h"
#include "gear_exceptions.h"
#include "gear_closures.h"
#include "gear_inheritance.h"

#ifdef HAVE_STDARG_H
#include <stdarg.h>
#endif

/* these variables are true statics/globals, and have to be mutex'ed on every access */
GEAR_API HashTable capi_registry;

static gear_capi_entry **capi_request_startup_handlers;
static gear_capi_entry **capi_request_shutdown_handlers;
static gear_capi_entry **capi_post_deactivate_handlers;

static gear_class_entry  **class_cleanup_handlers;

GEAR_API int _gear_get_parameters_array_ex(int param_count, zval *argument_array) /* {{{ */
{
	zval *param_ptr;
	int arg_count;

	param_ptr = GEAR_CALL_ARG(EG(current_execute_data), 1);
	arg_count = GEAR_CALL_NUM_ARGS(EG(current_execute_data));

	if (param_count>arg_count) {
		return FAILURE;
	}

	while (param_count-->0) {
		ZVAL_COPY_VALUE(argument_array, param_ptr);
		argument_array++;
		param_ptr++;
	}

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_copy_parameters_array(int param_count, zval *argument_array) /* {{{ */
{
	zval *param_ptr;
	int arg_count;

	param_ptr = GEAR_CALL_ARG(EG(current_execute_data), 1);
	arg_count = GEAR_CALL_NUM_ARGS(EG(current_execute_data));

	if (param_count>arg_count) {
		return FAILURE;
	}

	while (param_count-->0) {
		Z_TRY_ADDREF_P(param_ptr);
		gear_hash_next_index_insert_new(Z_ARRVAL_P(argument_array), param_ptr);
		param_ptr++;
	}

	return SUCCESS;
}
/* }}} */

GEAR_API GEAR_COLD void gear_wrong_param_count(void) /* {{{ */
{
	const char *space;
	const char *class_name = get_active_class_name(&space);

	gear_internal_argument_count_error(GEAR_ARG_USES_STRICT_TYPES(), "Wrong parameter count for %s%s%s()", class_name, space, get_active_function_name());
}
/* }}} */

/* Argument parsing API -- andrei */
GEAR_API char *gear_get_type_by_const(int type) /* {{{ */
{
	switch(type) {
		case IS_FALSE:
		case IS_TRUE:
		case _IS_BOOL:
			return "bool";
		case IS_LONG:
			return "int";
		case IS_DOUBLE:
			return "float";
		case IS_STRING:
			return "string";
		case IS_OBJECT:
			return "object";
		case IS_RESOURCE:
			return "resource";
		case IS_NULL:
			return "null";
		case IS_CALLABLE:
			return "callable";
		case IS_ITERABLE:
			return "iterable";
		case IS_ARRAY:
			return "array";
		case IS_VOID:
			return "void";
		case _IS_NUMBER:
			return "number";
		default:
			return "unknown";
	}
}
/* }}} */

GEAR_API char *gear_zval_type_name(const zval *arg) /* {{{ */
{
	ZVAL_DEREF(arg);
	return gear_get_type_by_const(Z_TYPE_P(arg));
}
/* }}} */

GEAR_API gear_string *gear_zval_get_type(const zval *arg) /* {{{ */
{
	switch (Z_TYPE_P(arg)) {
		case IS_NULL:
			return ZSTR_KNOWN(GEAR_STR_NULL);
		case IS_FALSE:
		case IS_TRUE:
			return ZSTR_KNOWN(GEAR_STR_BOOLEAN);
		case IS_LONG:
			return ZSTR_KNOWN(GEAR_STR_INTEGER);
		case IS_DOUBLE:
			return ZSTR_KNOWN(GEAR_STR_DOUBLE);
		case IS_STRING:
			return ZSTR_KNOWN(GEAR_STR_STRING);
		case IS_ARRAY:
			return ZSTR_KNOWN(GEAR_STR_ARRAY);
		case IS_OBJECT:
			return ZSTR_KNOWN(GEAR_STR_OBJECT);
		case IS_RESOURCE:
			if (gear_rsrc_list_get_rsrc_type(Z_RES_P(arg))) {
				return ZSTR_KNOWN(GEAR_STR_RESOURCE);
			} else {
				return ZSTR_KNOWN(GEAR_STR_CLOSED_RESOURCE);
			}
		default:
			return NULL;
	}
}
/* }}} */

GEAR_API GEAR_COLD int GEAR_FASTCALL gear_wrong_parameters_none_error(void) /* {{{ */
{
	int num_args = GEAR_CALL_NUM_ARGS(EG(current_execute_data));
	gear_function *active_function = EG(current_execute_data)->func;
	const char *class_name = active_function->common.scope ? ZSTR_VAL(active_function->common.scope->name) : "";

	gear_internal_argument_count_error(
				GEAR_ARG_USES_STRICT_TYPES(),
				"%s%s%s() expects %s %d parameter%s, %d given",
				class_name, \
				class_name[0] ? "::" : "", \
				ZSTR_VAL(active_function->common.function_name),
				"exactly",
				0,
				"s",
				num_args);
	return FAILURE;
}
/* }}} */

GEAR_API GEAR_COLD int GEAR_FASTCALL gear_wrong_parameters_none_exception(void) /* {{{ */
{
	int num_args = GEAR_CALL_NUM_ARGS(EG(current_execute_data));
	gear_function *active_function = EG(current_execute_data)->func;
	const char *class_name = active_function->common.scope ? ZSTR_VAL(active_function->common.scope->name) : "";

	gear_internal_argument_count_error(
				1,
				"%s%s%s() expects %s %d parameter%s, %d given",
				class_name, \
				class_name[0] ? "::" : "", \
				ZSTR_VAL(active_function->common.function_name),
				"exactly",
				0,
				"s",
				num_args);
	return FAILURE;
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameters_count_error(int min_num_args, int max_num_args) /* {{{ */
{
	int num_args = GEAR_CALL_NUM_ARGS(EG(current_execute_data));
	gear_function *active_function = EG(current_execute_data)->func;
	const char *class_name = active_function->common.scope ? ZSTR_VAL(active_function->common.scope->name) : "";

	gear_internal_argument_count_error(
				GEAR_ARG_USES_STRICT_TYPES(),
				"%s%s%s() expects %s %d parameter%s, %d given",
				class_name, \
				class_name[0] ? "::" : "", \
				ZSTR_VAL(active_function->common.function_name),
				min_num_args == max_num_args ? "exactly" : num_args < min_num_args ? "at least" : "at most",
				num_args < min_num_args ? min_num_args : max_num_args,
				(num_args < min_num_args ? min_num_args : max_num_args) == 1 ? "" : "s",
				num_args);
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameters_count_exception(int min_num_args, int max_num_args) /* {{{ */
{
	int num_args = GEAR_CALL_NUM_ARGS(EG(current_execute_data));
	gear_function *active_function = EG(current_execute_data)->func;
	const char *class_name = active_function->common.scope ? ZSTR_VAL(active_function->common.scope->name) : "";

	gear_internal_argument_count_error(
				1,
				"%s%s%s() expects %s %d parameter%s, %d given",
				class_name, \
				class_name[0] ? "::" : "", \
				ZSTR_VAL(active_function->common.function_name),
				min_num_args == max_num_args ? "exactly" : num_args < min_num_args ? "at least" : "at most",
				num_args < min_num_args ? min_num_args : max_num_args,
				(num_args < min_num_args ? min_num_args : max_num_args) == 1 ? "" : "s",
				num_args);
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameter_type_error(int num, gear_expected_type expected_type, zval *arg) /* {{{ */
{
	const char *space;
	const char *class_name = get_active_class_name(&space);
	static const char * const expected_error[] = {
		Z_EXPECTED_TYPES(Z_EXPECTED_TYPE_STR)
		NULL
	};

	gear_internal_type_error(GEAR_ARG_USES_STRICT_TYPES(), "%s%s%s() expects parameter %d to be %s, %s given",
		class_name, space, get_active_function_name(), num, expected_error[expected_type], gear_zval_type_name(arg));
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameter_type_exception(int num, gear_expected_type expected_type, zval *arg) /* {{{ */
{
	const char *space;
	const char *class_name = get_active_class_name(&space);
	static const char * const expected_error[] = {
		Z_EXPECTED_TYPES(Z_EXPECTED_TYPE_STR)
		NULL
	};

	gear_internal_type_error(1, "%s%s%s() expects parameter %d to be %s, %s given",
		class_name, space, get_active_function_name(), num, expected_error[expected_type], gear_zval_type_name(arg));
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameter_class_error(int num, char *name, zval *arg) /* {{{ */
{
	const char *space;
	const char *class_name = get_active_class_name(&space);

	gear_internal_type_error(GEAR_ARG_USES_STRICT_TYPES(), "%s%s%s() expects parameter %d to be %s, %s given",
		class_name, space, get_active_function_name(), num, name, gear_zval_type_name(arg));
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_parameter_class_exception(int num, char *name, zval *arg) /* {{{ */
{
	const char *space;
	const char *class_name = get_active_class_name(&space);

	gear_internal_type_error(1, "%s%s%s() expects parameter %d to be %s, %s given",
		class_name, space, get_active_function_name(), num, name, gear_zval_type_name(arg));
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_callback_error(int num, char *error) /* {{{ */
{
	const char *space;
	const char *class_name = get_active_class_name(&space);

	gear_internal_type_error(GEAR_ARG_USES_STRICT_TYPES(), "%s%s%s() expects parameter %d to be a valid callback, %s",
		class_name, space, get_active_function_name(), num, error);
	efree(error);
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_callback_exception(int num, char *error) /* {{{ */
{
	const char *space;
	const char *class_name = get_active_class_name(&space);

	gear_internal_type_error(1, "%s%s%s() expects parameter %d to be a valid callback, %s",
		class_name, space, get_active_function_name(), num, error);
	efree(error);
}
/* }}} */

GEAR_API GEAR_COLD void GEAR_FASTCALL gear_wrong_callback_deprecated(int num, char *error) /* {{{ */
{
	const char *space;
	const char *class_name = get_active_class_name(&space);

	gear_error(E_DEPRECATED, "%s%s%s() expects parameter %d to be a valid callback, %s",
		class_name, space, get_active_function_name(), num, error);
	efree(error);
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_class(zval *arg, gear_class_entry **pce, int num, int check_null) /* {{{ */
{
	gear_class_entry *ce_base = *pce;

	if (check_null && Z_TYPE_P(arg) == IS_NULL) {
		*pce = NULL;
		return 1;
	}
	convert_to_string_ex(arg);
	*pce = gear_lookup_class(Z_STR_P(arg));
	if (ce_base) {
		if ((!*pce || !instanceof_function(*pce, ce_base))) {
			const char *space;
			const char *class_name = get_active_class_name(&space);

			gear_internal_type_error(GEAR_ARG_USES_STRICT_TYPES(), "%s%s%s() expects parameter %d to be a class name derived from %s, '%s' given",
				class_name, space, get_active_function_name(), num,
				ZSTR_VAL(ce_base->name), Z_STRVAL_P(arg));
			*pce = NULL;
			return 0;
		}
	}
	if (!*pce) {
		const char *space;
		const char *class_name = get_active_class_name(&space);

		gear_internal_type_error(GEAR_ARG_USES_STRICT_TYPES(), "%s%s%s() expects parameter %d to be a valid class name, '%s' given",
			class_name, space, get_active_function_name(), num,
			Z_STRVAL_P(arg));
		return 0;
	}
	return 1;
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_bool_weak(zval *arg, gear_bool *dest) /* {{{ */
{
	if (EXPECTED(Z_TYPE_P(arg) <= IS_STRING)) {
		*dest = gear_is_true(arg);
	} else {
		return 0;
	}
	return 1;
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_bool_slow(zval *arg, gear_bool *dest) /* {{{ */
{
	if (UNEXPECTED(GEAR_ARG_USES_STRICT_TYPES())) {
		return 0;
	}
	return gear_parse_arg_bool_weak(arg, dest);
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_long_weak(zval *arg, gear_long *dest) /* {{{ */
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_DOUBLE)) {
		if (UNEXPECTED(gear_isnan(Z_DVAL_P(arg)))) {
			return 0;
		}
		if (UNEXPECTED(!GEAR_DOUBLE_FITS_LONG(Z_DVAL_P(arg)))) {
			return 0;
		} else {
			*dest = gear_dval_to_lval(Z_DVAL_P(arg));
		}
	} else if (EXPECTED(Z_TYPE_P(arg) == IS_STRING)) {
		double d;
		int type;

		if (UNEXPECTED((type = is_numeric_str_function(Z_STR_P(arg), dest, &d)) != IS_LONG)) {
			if (EXPECTED(type != 0)) {
				if (UNEXPECTED(gear_isnan(d))) {
					return 0;
				}
				if (UNEXPECTED(!GEAR_DOUBLE_FITS_LONG(d))) {
					return 0;
				} else {
					*dest = gear_dval_to_lval(d);
				}
			} else {
				return 0;
			}
		}
	} else if (EXPECTED(Z_TYPE_P(arg) < IS_TRUE)) {
		*dest = 0;
	} else if (EXPECTED(Z_TYPE_P(arg) == IS_TRUE)) {
		*dest = 1;
	} else {
		return 0;
	}
	return 1;
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_long_slow(zval *arg, gear_long *dest) /* {{{ */
{
	if (UNEXPECTED(GEAR_ARG_USES_STRICT_TYPES())) {
		return 0;
	}
	return gear_parse_arg_long_weak(arg, dest);
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_long_cap_weak(zval *arg, gear_long *dest) /* {{{ */
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_DOUBLE)) {
		if (UNEXPECTED(gear_isnan(Z_DVAL_P(arg)))) {
			return 0;
		}
		*dest = gear_dval_to_lval_cap(Z_DVAL_P(arg));
	} else if (EXPECTED(Z_TYPE_P(arg) == IS_STRING)) {
		double d;
		int type;

		if (UNEXPECTED((type = is_numeric_str_function(Z_STR_P(arg), dest, &d)) != IS_LONG)) {
			if (EXPECTED(type != 0)) {
				if (UNEXPECTED(gear_isnan(d))) {
					return 0;
				}
				*dest = gear_dval_to_lval_cap(d);
			} else {
				return 0;
			}
		}
	} else if (EXPECTED(Z_TYPE_P(arg) < IS_TRUE)) {
		*dest = 0;
	} else if (EXPECTED(Z_TYPE_P(arg) == IS_TRUE)) {
		*dest = 1;
	} else {
		return 0;
	}
	return 1;
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_long_cap_slow(zval *arg, gear_long *dest) /* {{{ */
{
	if (UNEXPECTED(GEAR_ARG_USES_STRICT_TYPES())) {
		return 0;
	}
	return gear_parse_arg_long_cap_weak(arg, dest);
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_double_weak(zval *arg, double *dest) /* {{{ */
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_LONG)) {
		*dest = (double)Z_LVAL_P(arg);
	} else if (EXPECTED(Z_TYPE_P(arg) == IS_STRING)) {
		gear_long l;
		int type;

		if (UNEXPECTED((type = is_numeric_str_function(Z_STR_P(arg), &l, dest)) != IS_DOUBLE)) {
			if (EXPECTED(type != 0)) {
				*dest = (double)(l);
			} else {
				return 0;
			}
		}
	} else if (EXPECTED(Z_TYPE_P(arg) < IS_TRUE)) {
		*dest = 0.0;
	} else if (EXPECTED(Z_TYPE_P(arg) == IS_TRUE)) {
		*dest = 1.0;
	} else {
		return 0;
	}
	return 1;
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_double_slow(zval *arg, double *dest) /* {{{ */
{
	if (EXPECTED(Z_TYPE_P(arg) == IS_LONG)) {
		/* SSTH Exception: IS_LONG may be accepted instead as IS_DOUBLE */
		*dest = (double)Z_LVAL_P(arg);
	} else if (UNEXPECTED(GEAR_ARG_USES_STRICT_TYPES())) {
		return 0;
	}
	return gear_parse_arg_double_weak(arg, dest);
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_str_weak(zval *arg, gear_string **dest) /* {{{ */
{
	if (EXPECTED(Z_TYPE_P(arg) < IS_STRING)) {
		convert_to_string(arg);
		*dest = Z_STR_P(arg);
	} else if (UNEXPECTED(Z_TYPE_P(arg) == IS_OBJECT)) {
		if (Z_OBJ_HANDLER_P(arg, cast_object)) {
			zval obj;
			if (Z_OBJ_HANDLER_P(arg, cast_object)(arg, &obj, IS_STRING) == SUCCESS) {
				zval_ptr_dtor(arg);
				ZVAL_COPY_VALUE(arg, &obj);
				*dest = Z_STR_P(arg);
				return 1;
			}
		} else if (Z_OBJ_HANDLER_P(arg, get)) {
			zval rv;
			zval *z = Z_OBJ_HANDLER_P(arg, get)(arg, &rv);

			if (Z_TYPE_P(z) != IS_OBJECT) {
				zval_ptr_dtor(arg);
				if (Z_TYPE_P(z) == IS_STRING) {
					ZVAL_COPY_VALUE(arg, z);
				} else {
					ZVAL_STR(arg, zval_get_string_func(z));
					zval_ptr_dtor(z);
				}
				*dest = Z_STR_P(arg);
				return 1;
			}
			zval_ptr_dtor(z);
		}
		return 0;
	} else {
		return 0;
	}
	return 1;
}
/* }}} */

GEAR_API int GEAR_FASTCALL gear_parse_arg_str_slow(zval *arg, gear_string **dest) /* {{{ */
{
	if (UNEXPECTED(GEAR_ARG_USES_STRICT_TYPES())) {
		return 0;
	}
	return gear_parse_arg_str_weak(arg, dest);
}
/* }}} */

static const char *gear_parse_arg_impl(int arg_num, zval *arg, va_list *va, const char **spec, char **error, int *severity) /* {{{ */
{
	const char *spec_walk = *spec;
	char c = *spec_walk++;
	int check_null = 0;
	int separate = 0;
	zval *real_arg = arg;

	/* scan through modifiers */
	ZVAL_DEREF(arg);
	while (1) {
		if (*spec_walk == '/') {
			SEPARATE_ZVAL_NOREF(arg);
			real_arg = arg;
			separate = 1;
		} else if (*spec_walk == '!') {
			check_null = 1;
		} else {
			break;
		}
		spec_walk++;
	}

	switch (c) {
		case 'l':
		case 'L':
			{
				gear_long *p = va_arg(*va, gear_long *);
				gear_bool *is_null = NULL;

				if (check_null) {
					is_null = va_arg(*va, gear_bool *);
				}

				if (!gear_parse_arg_long(arg, p, is_null, check_null, c == 'L')) {
					return "int";
				}
			}
			break;

		case 'd':
			{
				double *p = va_arg(*va, double *);
				gear_bool *is_null = NULL;

				if (check_null) {
					is_null = va_arg(*va, gear_bool *);
				}

				if (!gear_parse_arg_double(arg, p, is_null, check_null)) {
					return "float";
				}
			}
			break;

		case 's':
			{
				char **p = va_arg(*va, char **);
				size_t *pl = va_arg(*va, size_t *);
				if (!gear_parse_arg_string(arg, p, pl, check_null)) {
					return "string";
				}
			}
			break;

		case 'p':
			{
				char **p = va_arg(*va, char **);
				size_t *pl = va_arg(*va, size_t *);
				if (!gear_parse_arg_path(arg, p, pl, check_null)) {
					return "a valid path";
				}
			}
			break;

		case 'P':
			{
				gear_string **str = va_arg(*va, gear_string **);
				if (!gear_parse_arg_path_str(arg, str, check_null)) {
					return "a valid path";
				}
			}
			break;

		case 'S':
			{
				gear_string **str = va_arg(*va, gear_string **);
				if (!gear_parse_arg_str(arg, str, check_null)) {
					return "string";
				}
			}
			break;

		case 'b':
			{
				gear_bool *p = va_arg(*va, gear_bool *);
				gear_bool *is_null = NULL;

				if (check_null) {
					is_null = va_arg(*va, gear_bool *);
				}

				if (!gear_parse_arg_bool(arg, p, is_null, check_null)) {
					return "bool";
				}
			}
			break;

		case 'r':
			{
				zval **p = va_arg(*va, zval **);

				if (!gear_parse_arg_resource(arg, p, check_null)) {
					return "resource";
				}
			}
			break;

		case 'A':
		case 'a':
			{
				zval **p = va_arg(*va, zval **);

				if (!gear_parse_arg_array(arg, p, check_null, c == 'A')) {
					return "array";
				}
			}
			break;

		case 'H':
		case 'h':
			{
				HashTable **p = va_arg(*va, HashTable **);

				if (!gear_parse_arg_array_ht(arg, p, check_null, c == 'H', separate)) {
					return "array";
				}
			}
			break;

		case 'o':
			{
				zval **p = va_arg(*va, zval **);

				if (!gear_parse_arg_object(arg, p, NULL, check_null)) {
					return "object";
				}
			}
			break;

		case 'O':
			{
				zval **p = va_arg(*va, zval **);
				gear_class_entry *ce = va_arg(*va, gear_class_entry *);

				if (!gear_parse_arg_object(arg, p, ce, check_null)) {
					if (ce) {
						return ZSTR_VAL(ce->name);
					} else {
						return "object";
					}
				}
			}
			break;

		case 'C':
			{
				gear_class_entry *lookup, **pce = va_arg(*va, gear_class_entry **);
				gear_class_entry *ce_base = *pce;

				if (check_null && Z_TYPE_P(arg) == IS_NULL) {
					*pce = NULL;
					break;
				}
				convert_to_string_ex(arg);
				if ((lookup = gear_lookup_class(Z_STR_P(arg))) == NULL) {
					*pce = NULL;
				} else {
					*pce = lookup;
				}
				if (ce_base) {
					if ((!*pce || !instanceof_function(*pce, ce_base))) {
						gear_spprintf(error, 0, "to be a class name derived from %s, '%s' given",
							ZSTR_VAL(ce_base->name), Z_STRVAL_P(arg));
						*pce = NULL;
						return "";
					}
				}
				if (!*pce) {
					gear_spprintf(error, 0, "to be a valid class name, '%s' given",
						Z_STRVAL_P(arg));
					return "";
				}
				break;

			}
			break;

		case 'f':
			{
				gear_fcall_info *fci = va_arg(*va, gear_fcall_info *);
				gear_fcall_info_cache *fcc = va_arg(*va, gear_fcall_info_cache *);
				char *is_callable_error = NULL;

				if (check_null && Z_TYPE_P(arg) == IS_NULL) {
					fci->size = 0;
					fcc->function_handler = 0;
					break;
				}

				if (gear_fcall_info_init(arg, 0, fci, fcc, NULL, &is_callable_error) == SUCCESS) {
					if (is_callable_error) {
						*severity = E_DEPRECATED;
						gear_spprintf(error, 0, "to be a valid callback, %s", is_callable_error);
						efree(is_callable_error);
						*spec = spec_walk;
						return "";
					}
					break;
				} else {
					if (is_callable_error) {
						*severity = E_ERROR;
						gear_spprintf(error, 0, "to be a valid callback, %s", is_callable_error);
						efree(is_callable_error);
						return "";
					} else {
						return "valid callback";
					}
				}
			}

		case 'z':
			{
				zval **p = va_arg(*va, zval **);

				gear_parse_arg_zval_deref(real_arg, p, check_null);
			}
			break;

		case 'Z':
			/* 'Z' iz not supported anymore and should be replaced with 'z' */
			GEAR_ASSERT(c != 'Z');
		default:
			return "unknown";
	}

	*spec = spec_walk;

	return NULL;
}
/* }}} */

static int gear_parse_arg(int arg_num, zval *arg, va_list *va, const char **spec, int flags) /* {{{ */
{
	const char *expected_type = NULL;
	char *error = NULL;
	int severity = 0;

	expected_type = gear_parse_arg_impl(arg_num, arg, va, spec, &error, &severity);
	if (expected_type) {
		if (!(flags & GEAR_PARSE_PARAMS_QUIET) && (*expected_type || error)) {
			const char *space;
			const char *class_name = get_active_class_name(&space);
			gear_bool throw_exception =
				GEAR_ARG_USES_STRICT_TYPES() || (flags & GEAR_PARSE_PARAMS_THROW);

			if (error) {
				gear_internal_type_error(throw_exception, "%s%s%s() expects parameter %d %s",
						class_name, space, get_active_function_name(), arg_num, error);
				efree(error);
			} else {
				gear_internal_type_error(throw_exception,
						"%s%s%s() expects parameter %d to be %s, %s given",
						class_name, space, get_active_function_name(), arg_num, expected_type,
						gear_zval_type_name(arg));
			}
		}
		if (severity != E_DEPRECATED) {
			return FAILURE;
		}
	}

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_parse_parameter(int flags, int arg_num, zval *arg, const char *spec, ...)
{
	va_list va;
	int ret;

	va_start(va, spec);
	ret = gear_parse_arg(arg_num, arg, &va, &spec, flags);
	va_end(va);

	return ret;
}

static GEAR_COLD void gear_parse_parameters_debug_error(const char *msg) {
	gear_function *active_function = EG(current_execute_data)->func;
	const char *class_name = active_function->common.scope
		? ZSTR_VAL(active_function->common.scope->name) : "";
	gear_error_noreturn(E_CORE_ERROR, "%s%s%s(): %s",
		class_name, class_name[0] ? "::" : "",
		ZSTR_VAL(active_function->common.function_name), msg);
}

static int gear_parse_va_args(int num_args, const char *type_spec, va_list *va, int flags) /* {{{ */
{
	const  char *spec_walk;
	int c, i;
	int min_num_args = -1;
	int max_num_args = 0;
	int post_varargs = 0;
	zval *arg;
	int arg_count;
	gear_bool have_varargs = 0;
	zval **varargs = NULL;
	int *n_varargs = NULL;

	for (spec_walk = type_spec; *spec_walk; spec_walk++) {
		c = *spec_walk;
		switch (c) {
			case 'l': case 'd':
			case 's': case 'b':
			case 'r': case 'a':
			case 'o': case 'O':
			case 'z': case 'Z':
			case 'C': case 'h':
			case 'f': case 'A':
			case 'H': case 'p':
			case 'S': case 'P':
			case 'L':
				max_num_args++;
				break;

			case '|':
				min_num_args = max_num_args;
				break;

			case '/':
			case '!':
				/* Pass */
				break;

			case '*':
			case '+':
				if (have_varargs) {
					gear_parse_parameters_debug_error(
						"only one varargs specifier (* or +) is permitted");
					return FAILURE;
				}
				have_varargs = 1;
				/* we expect at least one parameter in varargs */
				if (c == '+') {
					max_num_args++;
				}
				/* mark the beginning of varargs */
				post_varargs = max_num_args;
				break;

			default:
				gear_parse_parameters_debug_error("bad type specifier while parsing parameters");
				return FAILURE;
		}
	}

	if (min_num_args < 0) {
		min_num_args = max_num_args;
	}

	if (have_varargs) {
		/* calculate how many required args are at the end of the specifier list */
		post_varargs = max_num_args - post_varargs;
		max_num_args = -1;
	}

	if (num_args < min_num_args || (num_args > max_num_args && max_num_args >= 0)) {
		if (!(flags & GEAR_PARSE_PARAMS_QUIET)) {
			gear_function *active_function = EG(current_execute_data)->func;
			const char *class_name = active_function->common.scope ? ZSTR_VAL(active_function->common.scope->name) : "";
			gear_bool throw_exception = GEAR_ARG_USES_STRICT_TYPES() || (flags & GEAR_PARSE_PARAMS_THROW);
			gear_internal_argument_count_error(throw_exception, "%s%s%s() expects %s %d parameter%s, %d given",
					class_name,
					class_name[0] ? "::" : "",
					ZSTR_VAL(active_function->common.function_name),
					min_num_args == max_num_args ? "exactly" : num_args < min_num_args ? "at least" : "at most",
					num_args < min_num_args ? min_num_args : max_num_args,
					(num_args < min_num_args ? min_num_args : max_num_args) == 1 ? "" : "s",
					num_args);
		}
		return FAILURE;
	}

	arg_count = GEAR_CALL_NUM_ARGS(EG(current_execute_data));

	if (num_args > arg_count) {
		gear_parse_parameters_debug_error("could not obtain parameters for parsing");
		return FAILURE;
	}

	i = 0;
	while (num_args-- > 0) {
		if (*type_spec == '|') {
			type_spec++;
		}

		if (*type_spec == '*' || *type_spec == '+') {
			int num_varargs = num_args + 1 - post_varargs;

			/* eat up the passed in storage even if it won't be filled in with varargs */
			varargs = va_arg(*va, zval **);
			n_varargs = va_arg(*va, int *);
			type_spec++;

			if (num_varargs > 0) {
				*n_varargs = num_varargs;
				*varargs = GEAR_CALL_ARG(EG(current_execute_data), i + 1);
				/* adjust how many args we have left and restart loop */
				num_args += 1 - num_varargs;
				i += num_varargs;
				continue;
			} else {
				*varargs = NULL;
				*n_varargs = 0;
			}
		}

		arg = GEAR_CALL_ARG(EG(current_execute_data), i + 1);

		if (gear_parse_arg(i+1, arg, va, &type_spec, flags) == FAILURE) {
			/* clean up varargs array if it was used */
			if (varargs && *varargs) {
				*varargs = NULL;
			}
			return FAILURE;
		}
		i++;
	}

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_parse_parameters_ex(int flags, int num_args, const char *type_spec, ...) /* {{{ */
{
	va_list va;
	int retval;

	va_start(va, type_spec);
	retval = gear_parse_va_args(num_args, type_spec, &va, flags);
	va_end(va);

	return retval;
}
/* }}} */

GEAR_API int gear_parse_parameters(int num_args, const char *type_spec, ...) /* {{{ */
{
	va_list va;
	int retval;
	int flags = 0;

	va_start(va, type_spec);
	retval = gear_parse_va_args(num_args, type_spec, &va, flags);
	va_end(va);

	return retval;
}
/* }}} */

GEAR_API int gear_parse_parameters_throw(int num_args, const char *type_spec, ...) /* {{{ */
{
	va_list va;
	int retval;
	int flags = GEAR_PARSE_PARAMS_THROW;

	va_start(va, type_spec);
	retval = gear_parse_va_args(num_args, type_spec, &va, flags);
	va_end(va);

	return retval;
}
/* }}} */

GEAR_API int gear_parse_method_parameters(int num_args, zval *this_ptr, const char *type_spec, ...) /* {{{ */
{
	va_list va;
	int retval;
	int flags = 0;
	const char *p = type_spec;
	zval **object;
	gear_class_entry *ce;

	/* Just checking this_ptr is not enough, because fcall_common_helper does not set
	 * Z_OBJ(EG(This)) to NULL when calling an internal function with common.scope == NULL.
	 * In that case EG(This) would still be the $this from the calling code and we'd take the
	 * wrong branch here. */
	gear_bool is_method = EG(current_execute_data)->func->common.scope != NULL;

	if (!is_method || !this_ptr || Z_TYPE_P(this_ptr) != IS_OBJECT) {
		va_start(va, type_spec);
		retval = gear_parse_va_args(num_args, type_spec, &va, flags);
		va_end(va);
	} else {
		p++;

		va_start(va, type_spec);

		object = va_arg(va, zval **);
		ce = va_arg(va, gear_class_entry *);
		*object = this_ptr;

		if (ce && !instanceof_function(Z_OBJCE_P(this_ptr), ce)) {
			gear_error_noreturn(E_CORE_ERROR, "%s::%s() must be derived from %s::%s",
				ZSTR_VAL(Z_OBJCE_P(this_ptr)->name), get_active_function_name(), ZSTR_VAL(ce->name), get_active_function_name());
		}

		retval = gear_parse_va_args(num_args, p, &va, flags);
		va_end(va);
	}
	return retval;
}
/* }}} */

GEAR_API int gear_parse_method_parameters_ex(int flags, int num_args, zval *this_ptr, const char *type_spec, ...) /* {{{ */
{
	va_list va;
	int retval;
	const char *p = type_spec;
	zval **object;
	gear_class_entry *ce;

	if (!this_ptr) {
		va_start(va, type_spec);
		retval = gear_parse_va_args(num_args, type_spec, &va, flags);
		va_end(va);
	} else {
		p++;
		va_start(va, type_spec);

		object = va_arg(va, zval **);
		ce = va_arg(va, gear_class_entry *);
		*object = this_ptr;

		if (ce && !instanceof_function(Z_OBJCE_P(this_ptr), ce)) {
			if (!(flags & GEAR_PARSE_PARAMS_QUIET)) {
				gear_error_noreturn(E_CORE_ERROR, "%s::%s() must be derived from %s::%s",
					ZSTR_VAL(ce->name), get_active_function_name(), ZSTR_VAL(Z_OBJCE_P(this_ptr)->name), get_active_function_name());
			}
			va_end(va);
			return FAILURE;
		}

		retval = gear_parse_va_args(num_args, p, &va, flags);
		va_end(va);
	}
	return retval;
}
/* }}} */

/* This function should be called after the constructor has been called
 * because it may call __set from the uninitialized object otherwise. */
GEAR_API void gear_merge_properties(zval *obj, HashTable *properties) /* {{{ */
{
	const gear_object_handlers *obj_ht = Z_OBJ_HT_P(obj);
	gear_class_entry *old_scope = EG(fake_scope);
	gear_string *key;
	zval *value;

	EG(fake_scope) = Z_OBJCE_P(obj);
	GEAR_HASH_FOREACH_STR_KEY_VAL(properties, key, value) {
		if (key) {
			zval member;

			ZVAL_STR(&member, key);
			obj_ht->write_property(obj, &member, value, NULL);
		}
	} GEAR_HASH_FOREACH_END();
	EG(fake_scope) = old_scope;
}
/* }}} */

GEAR_API int gear_update_class_constants(gear_class_entry *class_type) /* {{{ */
{
	if (!(class_type->ce_flags & GEAR_ACC_CONSTANTS_UPDATED)) {
		gear_class_entry *ce;
		gear_class_constant *c;
		zval *val;
		gear_property_info *prop_info;

		if (class_type->parent) {
			if (UNEXPECTED(gear_update_class_constants(class_type->parent) != SUCCESS)) {
				return FAILURE;
			}
		}

		GEAR_HASH_FOREACH_PTR(&class_type->constants_table, c) {
			val = &c->value;
			if (Z_TYPE_P(val) == IS_CONSTANT_AST) {
				if (UNEXPECTED(zval_update_constant_ex(val, c->ce) != SUCCESS)) {
					return FAILURE;
				}
			}
		} GEAR_HASH_FOREACH_END();

		ce = class_type;
		while (ce) {
			GEAR_HASH_FOREACH_PTR(&ce->properties_info, prop_info) {
				if (prop_info->ce == ce) {
					if (prop_info->flags & GEAR_ACC_STATIC) {
						val = CE_STATIC_MEMBERS(class_type) + prop_info->offset;
					} else {
						val = (zval*)((char*)class_type->default_properties_table + prop_info->offset - OBJ_PROP_TO_OFFSET(0));
					}
					ZVAL_DEREF(val);
					if (Z_TYPE_P(val) == IS_CONSTANT_AST) {
						if (UNEXPECTED(zval_update_constant_ex(val, ce) != SUCCESS)) {
							return FAILURE;
						}
					}
				}
			} GEAR_HASH_FOREACH_END();
			ce = ce->parent;
		}

		class_type->ce_flags |= GEAR_ACC_CONSTANTS_UPDATED;
	}

	return SUCCESS;
}
/* }}} */

GEAR_API void object_properties_init(gear_object *object, gear_class_entry *class_type) /* {{{ */
{
	if (class_type->default_properties_count) {
		zval *src = class_type->default_properties_table;
		zval *dst = object->properties_table;
		zval *end = src + class_type->default_properties_count;

		if (UNEXPECTED(class_type->type == GEAR_INTERNAL_CLASS)) {
			do {
				ZVAL_COPY_OR_DUP(dst, src);
				src++;
				dst++;
			} while (src != end);
		} else {
			do {
				ZVAL_COPY(dst, src);
				src++;
				dst++;
			} while (src != end);
		}
		object->properties = NULL;
	}
}
/* }}} */

GEAR_API void object_properties_init_ex(gear_object *object, HashTable *properties) /* {{{ */
{
	object->properties = properties;
	if (object->ce->default_properties_count) {
		zval *prop;
		gear_string *key;
		gear_property_info *property_info;

		GEAR_HASH_FOREACH_STR_KEY_VAL(properties, key, prop) {
			property_info = gear_get_property_info(object->ce, key, 1);
			if (property_info != GEAR_WRONG_PROPERTY_INFO &&
			    property_info &&
			    (property_info->flags & GEAR_ACC_STATIC) == 0) {
				zval *slot = OBJ_PROP(object, property_info->offset);
				ZVAL_COPY_VALUE(slot, prop);
				ZVAL_INDIRECT(prop, slot);
			}
		} GEAR_HASH_FOREACH_END();
	}
}
/* }}} */

GEAR_API void object_properties_load(gear_object *object, HashTable *properties) /* {{{ */
{
    zval *prop, tmp;
   	gear_string *key;
   	gear_long h;
   	gear_property_info *property_info;

   	GEAR_HASH_FOREACH_KEY_VAL(properties, h, key, prop) {
		if (key) {
			if (ZSTR_VAL(key)[0] == '\0') {
				const char *class_name, *prop_name;
				size_t prop_name_len;
				if (gear_unmangle_property_name_ex(key, &class_name, &prop_name, &prop_name_len) == SUCCESS) {
					gear_string *pname = gear_string_init(prop_name, prop_name_len, 0);
					gear_class_entry *prev_scope = EG(fake_scope);
					if (class_name && class_name[0] != '*') {
						gear_string *cname = gear_string_init(class_name, strlen(class_name), 0);
						EG(fake_scope) = gear_lookup_class(cname);
						gear_string_release_ex(cname, 0);
					}
					property_info = gear_get_property_info(object->ce, pname, 1);
					gear_string_release_ex(pname, 0);
					EG(fake_scope) = prev_scope;
				} else {
					property_info = GEAR_WRONG_PROPERTY_INFO;
				}
			} else {
				property_info = gear_get_property_info(object->ce, key, 1);
			}
			if (property_info != GEAR_WRONG_PROPERTY_INFO &&
				property_info &&
				(property_info->flags & GEAR_ACC_STATIC) == 0) {
				zval *slot = OBJ_PROP(object, property_info->offset);
				zval_ptr_dtor(slot);
				ZVAL_COPY_VALUE(slot, prop);
				zval_add_ref(slot);
				if (object->properties) {
					ZVAL_INDIRECT(&tmp, slot);
					gear_hash_update(object->properties, key, &tmp);
				}
			} else {
				if (!object->properties) {
					rebuild_object_properties(object);
				}
				prop = gear_hash_update(object->properties, key, prop);
				zval_add_ref(prop);
			}
		} else {
			if (!object->properties) {
				rebuild_object_properties(object);
			}
			prop = gear_hash_index_update(object->properties, h, prop);
			zval_add_ref(prop);
		}
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* This function requires 'properties' to contain all props declared in the
 * class and all props being public. If only a subset is given or the class
 * has protected members then you need to merge the properties separately by
 * calling gear_merge_properties(). */
GEAR_API int object_and_properties_init(zval *arg, gear_class_entry *class_type, HashTable *properties) /* {{{ */
{
	if (UNEXPECTED(class_type->ce_flags & (GEAR_ACC_INTERFACE|GEAR_ACC_TRAIT|GEAR_ACC_IMPLICIT_ABSTRACT_CLASS|GEAR_ACC_EXPLICIT_ABSTRACT_CLASS))) {
		if (class_type->ce_flags & GEAR_ACC_INTERFACE) {
			gear_throw_error(NULL, "Cannot instantiate interface %s", ZSTR_VAL(class_type->name));
		} else if (class_type->ce_flags & GEAR_ACC_TRAIT) {
			gear_throw_error(NULL, "Cannot instantiate trait %s", ZSTR_VAL(class_type->name));
		} else {
			gear_throw_error(NULL, "Cannot instantiate abstract class %s", ZSTR_VAL(class_type->name));
		}
		ZVAL_NULL(arg);
		Z_OBJ_P(arg) = NULL;
		return FAILURE;
	}

	if (UNEXPECTED(!(class_type->ce_flags & GEAR_ACC_CONSTANTS_UPDATED))) {
		if (UNEXPECTED(gear_update_class_constants(class_type) != SUCCESS)) {
			ZVAL_NULL(arg);
			Z_OBJ_P(arg) = NULL;
			return FAILURE;
		}
	}

	if (class_type->create_object == NULL) {
		ZVAL_OBJ(arg, gear_objects_new(class_type));
		if (properties) {
			object_properties_init_ex(Z_OBJ_P(arg), properties);
		} else {
			object_properties_init(Z_OBJ_P(arg), class_type);
		}
	} else {
		ZVAL_OBJ(arg, class_type->create_object(class_type));
	}
	return SUCCESS;
}
/* }}} */

GEAR_API int object_init_ex(zval *arg, gear_class_entry *class_type) /* {{{ */
{
	return object_and_properties_init(arg, class_type, 0);
}
/* }}} */

GEAR_API int object_init(zval *arg) /* {{{ */
{
	ZVAL_OBJ(arg, gear_objects_new(gear_standard_class_def));
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_long_ex(zval *arg, const char *key, size_t key_len, gear_long n) /* {{{ */
{
	zval tmp;

	ZVAL_LONG(&tmp, n);
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_null_ex(zval *arg, const char *key, size_t key_len) /* {{{ */
{
	zval tmp;

	ZVAL_NULL(&tmp);
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_bool_ex(zval *arg, const char *key, size_t key_len, int b) /* {{{ */
{
	zval tmp;

	ZVAL_BOOL(&tmp, b);
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_resource_ex(zval *arg, const char *key, size_t key_len, gear_resource *r) /* {{{ */
{
	zval tmp;

	ZVAL_RES(&tmp, r);
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_double_ex(zval *arg, const char *key, size_t key_len, double d) /* {{{ */
{
	zval tmp;

	ZVAL_DOUBLE(&tmp, d);
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_str_ex(zval *arg, const char *key, size_t key_len, gear_string *str) /* {{{ */
{
	zval tmp;

	ZVAL_STR(&tmp, str);
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_string_ex(zval *arg, const char *key, size_t key_len, const char *str) /* {{{ */
{
	zval tmp;

	ZVAL_STRING(&tmp, str);
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_stringl_ex(zval *arg, const char *key, size_t key_len, const char *str, size_t length) /* {{{ */
{
	zval tmp;

	ZVAL_STRINGL(&tmp, str, length);
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_assoc_zval_ex(zval *arg, const char *key, size_t key_len, zval *value) /* {{{ */
{
	gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, value);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_long(zval *arg, gear_ulong index, gear_long n) /* {{{ */
{
	zval tmp;

	ZVAL_LONG(&tmp, n);
	gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_null(zval *arg, gear_ulong index) /* {{{ */
{
	zval tmp;

	ZVAL_NULL(&tmp);
	gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_bool(zval *arg, gear_ulong index, int b) /* {{{ */
{
	zval tmp;

	ZVAL_BOOL(&tmp, b);
	gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_resource(zval *arg, gear_ulong index, gear_resource *r) /* {{{ */
{
	zval tmp;

	ZVAL_RES(&tmp, r);
	gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_double(zval *arg, gear_ulong index, double d) /* {{{ */
{
	zval tmp;

	ZVAL_DOUBLE(&tmp, d);
	gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_str(zval *arg, gear_ulong index, gear_string *str) /* {{{ */
{
	zval tmp;

	ZVAL_STR(&tmp, str);
	gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_string(zval *arg, gear_ulong index, const char *str) /* {{{ */
{
	zval tmp;

	ZVAL_STRING(&tmp, str);
	gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_stringl(zval *arg, gear_ulong index, const char *str, size_t length) /* {{{ */
{
	zval tmp;

	ZVAL_STRINGL(&tmp, str, length);
	gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_index_zval(zval *arg, gear_ulong index, zval *value) /* {{{ */
{
	gear_hash_index_update(Z_ARRVAL_P(arg), index, value);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_next_index_long(zval *arg, gear_long n) /* {{{ */
{
	zval tmp;

	ZVAL_LONG(&tmp, n);
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), &tmp) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API int add_next_index_null(zval *arg) /* {{{ */
{
	zval tmp;

	ZVAL_NULL(&tmp);
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), &tmp) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API int add_next_index_bool(zval *arg, int b) /* {{{ */
{
	zval tmp;

	ZVAL_BOOL(&tmp, b);
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), &tmp) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API int add_next_index_resource(zval *arg, gear_resource *r) /* {{{ */
{
	zval tmp;

	ZVAL_RES(&tmp, r);
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), &tmp) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API int add_next_index_double(zval *arg, double d) /* {{{ */
{
	zval tmp;

	ZVAL_DOUBLE(&tmp, d);
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), &tmp) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API int add_next_index_str(zval *arg, gear_string *str) /* {{{ */
{
	zval tmp;

	ZVAL_STR(&tmp, str);
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), &tmp) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API int add_next_index_string(zval *arg, const char *str) /* {{{ */
{
	zval tmp;

	ZVAL_STRING(&tmp, str);
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), &tmp) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API int add_next_index_stringl(zval *arg, const char *str, size_t length) /* {{{ */
{
	zval tmp;

	ZVAL_STRINGL(&tmp, str, length);
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), &tmp) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API int add_next_index_zval(zval *arg, zval *value) /* {{{ */
{
	return gear_hash_next_index_insert(Z_ARRVAL_P(arg), value) ? SUCCESS : FAILURE;
}
/* }}} */

GEAR_API zval *add_get_assoc_string_ex(zval *arg, const char *key, uint32_t key_len, const char *str) /* {{{ */
{
	zval tmp, *ret;

	ZVAL_STRING(&tmp, str);
	ret = gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return ret;
}
/* }}} */

GEAR_API zval *add_get_assoc_stringl_ex(zval *arg, const char *key, uint32_t key_len, const char *str, size_t length) /* {{{ */
{
	zval tmp, *ret;

	ZVAL_STRINGL(&tmp, str, length);
	ret = gear_symtable_str_update(Z_ARRVAL_P(arg), key, key_len, &tmp);
	return ret;
}
/* }}} */

GEAR_API zval *add_get_index_long(zval *arg, gear_ulong index, gear_long l) /* {{{ */
{
	zval tmp;

	ZVAL_LONG(&tmp, l);
	return gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
}
/* }}} */

GEAR_API zval *add_get_index_double(zval *arg, gear_ulong index, double d) /* {{{ */
{
	zval tmp;

	ZVAL_DOUBLE(&tmp, d);
	return gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
}
/* }}} */

GEAR_API zval *add_get_index_str(zval *arg, gear_ulong index, gear_string *str) /* {{{ */
{
	zval tmp;

	ZVAL_STR(&tmp, str);
	return gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
}
/* }}} */

GEAR_API zval *add_get_index_string(zval *arg, gear_ulong index, const char *str) /* {{{ */
{
	zval tmp;

	ZVAL_STRING(&tmp, str);
	return gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
}
/* }}} */

GEAR_API zval *add_get_index_stringl(zval *arg, gear_ulong index, const char *str, size_t length) /* {{{ */
{
	zval tmp;

	ZVAL_STRINGL(&tmp, str, length);
	return gear_hash_index_update(Z_ARRVAL_P(arg), index, &tmp);
}
/* }}} */

GEAR_API int array_set_zval_key(HashTable *ht, zval *key, zval *value) /* {{{ */
{
	zval *result;

	switch (Z_TYPE_P(key)) {
		case IS_STRING:
			result = gear_symtable_update(ht, Z_STR_P(key), value);
			break;
		case IS_NULL:
			result = gear_symtable_update(ht, ZSTR_EMPTY_ALLOC(), value);
			break;
		case IS_RESOURCE:
			gear_error(E_NOTICE, "Resource ID#%d used as offset, casting to integer (%d)", Z_RES_HANDLE_P(key), Z_RES_HANDLE_P(key));
			result = gear_hash_index_update(ht, Z_RES_HANDLE_P(key), value);
			break;
		case IS_FALSE:
			result = gear_hash_index_update(ht, 0, value);
			break;
		case IS_TRUE:
			result = gear_hash_index_update(ht, 1, value);
			break;
		case IS_LONG:
			result = gear_hash_index_update(ht, Z_LVAL_P(key), value);
			break;
		case IS_DOUBLE:
			result = gear_hash_index_update(ht, gear_dval_to_lval(Z_DVAL_P(key)), value);
			break;
		default:
			gear_error(E_WARNING, "Illegal offset type");
			result = NULL;
	}

	if (result) {
		Z_TRY_ADDREF_P(result);
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */

GEAR_API int add_property_long_ex(zval *arg, const char *key, size_t key_len, gear_long n) /* {{{ */
{
	zval tmp;
	zval z_key;

	ZVAL_LONG(&tmp, n);
	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, &tmp, NULL);
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_property_bool_ex(zval *arg, const char *key, size_t key_len, gear_long b) /* {{{ */
{
	zval tmp;
	zval z_key;

	ZVAL_BOOL(&tmp, b);
	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, &tmp, NULL);
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_property_null_ex(zval *arg, const char *key, size_t key_len) /* {{{ */
{
	zval tmp;
	zval z_key;

	ZVAL_NULL(&tmp);
	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, &tmp, NULL);
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_property_resource_ex(zval *arg, const char *key, size_t key_len, gear_resource *r) /* {{{ */
{
	zval tmp;
	zval z_key;

	ZVAL_RES(&tmp, r);
	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, &tmp, NULL);
	zval_ptr_dtor(&tmp); /* write_property will add 1 to refcount */
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_property_double_ex(zval *arg, const char *key, size_t key_len, double d) /* {{{ */
{
	zval tmp;
	zval z_key;

	ZVAL_DOUBLE(&tmp, d);
	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, &tmp, NULL);
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_property_str_ex(zval *arg, const char *key, size_t key_len, gear_string *str) /* {{{ */
{
	zval tmp;
	zval z_key;

	ZVAL_STR(&tmp, str);
	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, &tmp, NULL);
	zval_ptr_dtor(&tmp); /* write_property will add 1 to refcount */
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_property_string_ex(zval *arg, const char *key, size_t key_len, const char *str) /* {{{ */
{
	zval tmp;
	zval z_key;

	ZVAL_STRING(&tmp, str);
	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, &tmp, NULL);
	zval_ptr_dtor(&tmp); /* write_property will add 1 to refcount */
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_property_stringl_ex(zval *arg, const char *key, size_t key_len, const char *str, size_t length) /* {{{ */
{
	zval tmp;
	zval z_key;

	ZVAL_STRINGL(&tmp, str, length);
	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, &tmp, NULL);
	zval_ptr_dtor(&tmp); /* write_property will add 1 to refcount */
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int add_property_zval_ex(zval *arg, const char *key, size_t key_len, zval *value) /* {{{ */
{
	zval z_key;

	ZVAL_STRINGL(&z_key, key, key_len);
	Z_OBJ_HANDLER_P(arg, write_property)(arg, &z_key, value, NULL);
	zval_ptr_dtor(&z_key);
	return SUCCESS;
}
/* }}} */

GEAR_API int gear_startup_capi_ex(gear_capi_entry *cAPI) /* {{{ */
{
	size_t name_len;
	gear_string *lcname;

	if (cAPI->capi_started) {
		return SUCCESS;
	}
	cAPI->capi_started = 1;

	/* Check cAPI dependencies */
	if (cAPI->deps) {
		const gear_capi_dep *dep = cAPI->deps;

		while (dep->name) {
			if (dep->type == CAPI_DEP_REQUIRED) {
				gear_capi_entry *req_mod;

				name_len = strlen(dep->name);
				lcname = gear_string_alloc(name_len, 0);
				gear_str_tolower_copy(ZSTR_VAL(lcname), dep->name, name_len);

				if ((req_mod = gear_hash_find_ptr(&capi_registry, lcname)) == NULL || !req_mod->capi_started) {
					gear_string_efree(lcname);
					/* TODO: Check version relationship */
					gear_error(E_CORE_WARNING, "Cannot load cAPI '%s' because required cAPI '%s' is not loaded", cAPI->name, dep->name);
					cAPI->capi_started = 0;
					return FAILURE;
				}
				gear_string_efree(lcname);
			}
			++dep;
		}
	}

	/* Initialize cAPI globals */
	if (cAPI->globals_size) {
#ifdef ZTS
		ts_allocate_id(cAPI->globals_id_ptr, cAPI->globals_size, (ts_allocate_ctor) cAPI->globals_ctor, (ts_allocate_dtor) cAPI->globals_dtor);
#else
		if (cAPI->globals_ctor) {
			cAPI->globals_ctor(cAPI->globals_ptr);
		}
#endif
	}
	if (cAPI->capi_startup_func) {
		EG(current_capi) = cAPI;
		if (cAPI->capi_startup_func(cAPI->type, cAPI->capi_number)==FAILURE) {
			gear_error_noreturn(E_CORE_ERROR,"Unable to start %s cAPI", cAPI->name);
			EG(current_capi) = NULL;
			return FAILURE;
		}
		EG(current_capi) = NULL;
	}
	return SUCCESS;
}
/* }}} */

static int gear_startup_capi_zval(zval *zv) /* {{{ */
{
	gear_capi_entry *cAPI = Z_PTR_P(zv);

	return (gear_startup_capi_ex(cAPI) == SUCCESS) ? GEAR_HASH_APPLY_KEEP : GEAR_HASH_APPLY_REMOVE;
}
/* }}} */

static void gear_sort_capis(void *base, size_t count, size_t siz, compare_func_t compare, swap_func_t swp) /* {{{ */
{
	Bucket *b1 = base;
	Bucket *b2;
	Bucket *end = b1 + count;
	Bucket tmp;
	gear_capi_entry *m, *r;

	while (b1 < end) {
try_again:
		m = (gear_capi_entry*)Z_PTR(b1->val);
		if (!m->capi_started && m->deps) {
			const gear_capi_dep *dep = m->deps;
			while (dep->name) {
				if (dep->type == CAPI_DEP_REQUIRED || dep->type == CAPI_DEP_OPTIONAL) {
					b2 = b1 + 1;
					while (b2 < end) {
						r = (gear_capi_entry*)Z_PTR(b2->val);
						if (strcasecmp(dep->name, r->name) == 0) {
							tmp = *b1;
							*b1 = *b2;
							*b2 = tmp;
							goto try_again;
						}
						b2++;
					}
				}
				dep++;
			}
		}
		b1++;
	}
}
/* }}} */

GEAR_API void gear_collect_capi_handlers(void) /* {{{ */
{
	gear_capi_entry *cAPI;
	int startup_count = 0;
	int shutdown_count = 0;
	int post_deactivate_count = 0;
	gear_class_entry *ce;
	int class_count = 0;

	/* Collect extensions with request startup/shutdown handlers */
	GEAR_HASH_FOREACH_PTR(&capi_registry, cAPI) {
		if (cAPI->request_startup_func) {
			startup_count++;
		}
		if (cAPI->request_shutdown_func) {
			shutdown_count++;
		}
		if (cAPI->post_deactivate_func) {
			post_deactivate_count++;
		}
	} GEAR_HASH_FOREACH_END();
	capi_request_startup_handlers = (gear_capi_entry**)malloc(
	    sizeof(gear_capi_entry*) *
		(startup_count + 1 +
		 shutdown_count + 1 +
		 post_deactivate_count + 1));
	capi_request_startup_handlers[startup_count] = NULL;
	capi_request_shutdown_handlers = capi_request_startup_handlers + startup_count + 1;
	capi_request_shutdown_handlers[shutdown_count] = NULL;
	capi_post_deactivate_handlers = capi_request_shutdown_handlers + shutdown_count + 1;
	capi_post_deactivate_handlers[post_deactivate_count] = NULL;
	startup_count = 0;

	GEAR_HASH_FOREACH_PTR(&capi_registry, cAPI) {
		if (cAPI->request_startup_func) {
			capi_request_startup_handlers[startup_count++] = cAPI;
		}
		if (cAPI->request_shutdown_func) {
			capi_request_shutdown_handlers[--shutdown_count] = cAPI;
		}
		if (cAPI->post_deactivate_func) {
			capi_post_deactivate_handlers[--post_deactivate_count] = cAPI;
		}
	} GEAR_HASH_FOREACH_END();

	/* Collect internal classes with static members */
	GEAR_HASH_FOREACH_PTR(CG(class_table), ce) {
		if (ce->type == GEAR_INTERNAL_CLASS &&
		    ce->default_static_members_count > 0) {
		    class_count++;
		}
	} GEAR_HASH_FOREACH_END();

	class_cleanup_handlers = (gear_class_entry**)malloc(
		sizeof(gear_class_entry*) *
		(class_count + 1));
	class_cleanup_handlers[class_count] = NULL;

	if (class_count) {
		GEAR_HASH_FOREACH_PTR(CG(class_table), ce) {
			if (ce->type == GEAR_INTERNAL_CLASS &&
			    ce->default_static_members_count > 0) {
			    class_cleanup_handlers[--class_count] = ce;
			}
		} GEAR_HASH_FOREACH_END();
	}
}
/* }}} */

GEAR_API int gear_startup_capis(void) /* {{{ */
{
	gear_hash_sort_ex(&capi_registry, gear_sort_capis, NULL, 0);
	gear_hash_apply(&capi_registry, gear_startup_capi_zval);
	return SUCCESS;
}
/* }}} */

GEAR_API void gear_destroy_capis(void) /* {{{ */
{
	free(class_cleanup_handlers);
	free(capi_request_startup_handlers);
	gear_hash_graceful_reverse_destroy(&capi_registry);
}
/* }}} */

GEAR_API gear_capi_entry* gear_register_capi_ex(gear_capi_entry *cAPI) /* {{{ */
{
	size_t name_len;
	gear_string *lcname;
	gear_capi_entry *capi_ptr;

	if (!cAPI) {
		return NULL;
	}

#if 0
	gear_printf("%s: Registering cAPI %d\n", cAPI->name, cAPI->capi_number);
#endif

	/* Check cAPI dependencies */
	if (cAPI->deps) {
		const gear_capi_dep *dep = cAPI->deps;

		while (dep->name) {
			if (dep->type == CAPI_DEP_CONFLICTS) {
				name_len = strlen(dep->name);
				lcname = gear_string_alloc(name_len, 0);
				gear_str_tolower_copy(ZSTR_VAL(lcname), dep->name, name_len);

				if (gear_hash_exists(&capi_registry, lcname) || gear_get_extension(dep->name)) {
					gear_string_efree(lcname);
					/* TODO: Check version relationship */
					gear_error(E_CORE_WARNING, "Cannot load cAPI '%s' because conflicting cAPI '%s' is already loaded", cAPI->name, dep->name);
					return NULL;
				}
				gear_string_efree(lcname);
			}
			++dep;
		}
	}

	name_len = strlen(cAPI->name);
	lcname = gear_string_alloc(name_len, cAPI->type == CAPI_PERSISTENT);
	gear_str_tolower_copy(ZSTR_VAL(lcname), cAPI->name, name_len);

	lcname = gear_new_interned_string(lcname);
	if ((capi_ptr = gear_hash_add_mem(&capi_registry, lcname, cAPI, sizeof(gear_capi_entry))) == NULL) {
		gear_error(E_CORE_WARNING, "cAPI '%s' already loaded", cAPI->name);
		gear_string_release(lcname);
		return NULL;
	}
	cAPI = capi_ptr;
	EG(current_capi) = cAPI;

	if (cAPI->functions && gear_register_functions(NULL, cAPI->functions, NULL, cAPI->type)==FAILURE) {
		gear_hash_del(&capi_registry, lcname);
		gear_string_release(lcname);
		EG(current_capi) = NULL;
		gear_error(E_CORE_WARNING,"%s: Unable to register functions, unable to load", cAPI->name);
		return NULL;
	}

	EG(current_capi) = NULL;
	gear_string_release(lcname);
	return cAPI;
}
/* }}} */

GEAR_API gear_capi_entry* gear_register_internal_capi(gear_capi_entry *cAPI) /* {{{ */
{
	cAPI->capi_number = gear_next_free_capi();
	cAPI->type = CAPI_PERSISTENT;
	return gear_register_capi_ex(cAPI);
}
/* }}} */

GEAR_API void gear_check_magic_method_implementation(const gear_class_entry *ce, const gear_function *fptr, int error_type) /* {{{ */
{
	char lcname[16];
	size_t name_len;

	if (ZSTR_VAL(fptr->common.function_name)[0] != '_'
	 || ZSTR_VAL(fptr->common.function_name)[1] != '_') {
		return;
	}

	/* we don't care if the function name is longer, in fact lowercasing only
	 * the beginning of the name speeds up the check process */
	name_len = ZSTR_LEN(fptr->common.function_name);
	gear_str_tolower_copy(lcname, ZSTR_VAL(fptr->common.function_name), MIN(name_len, sizeof(lcname)-1));
	lcname[sizeof(lcname)-1] = '\0'; /* gear_str_tolower_copy won't necessarily set the zero byte */

	if (name_len == sizeof(GEAR_DESTRUCTOR_FUNC_NAME) - 1 && !memcmp(lcname, GEAR_DESTRUCTOR_FUNC_NAME, sizeof(GEAR_DESTRUCTOR_FUNC_NAME) - 1) && fptr->common.num_args != 0) {
		gear_error(error_type, "Destructor %s::%s() cannot take arguments", ZSTR_VAL(ce->name), GEAR_DESTRUCTOR_FUNC_NAME);
	} else if (name_len == sizeof(GEAR_CLONE_FUNC_NAME) - 1 && !memcmp(lcname, GEAR_CLONE_FUNC_NAME, sizeof(GEAR_CLONE_FUNC_NAME) - 1) && fptr->common.num_args != 0) {
		gear_error(error_type, "Method %s::%s() cannot accept any arguments", ZSTR_VAL(ce->name), GEAR_CLONE_FUNC_NAME);
	} else if (name_len == sizeof(GEAR_GET_FUNC_NAME) - 1 && !memcmp(lcname, GEAR_GET_FUNC_NAME, sizeof(GEAR_GET_FUNC_NAME) - 1)) {
		if (fptr->common.num_args != 1) {
			gear_error(error_type, "Method %s::%s() must take exactly 1 argument", ZSTR_VAL(ce->name), GEAR_GET_FUNC_NAME);
		} else if (ARG_SHOULD_BE_SENT_BY_REF(fptr, 1)) {
			gear_error(error_type, "Method %s::%s() cannot take arguments by reference", ZSTR_VAL(ce->name), GEAR_GET_FUNC_NAME);
		}
	} else if (name_len == sizeof(GEAR_SET_FUNC_NAME) - 1 && !memcmp(lcname, GEAR_SET_FUNC_NAME, sizeof(GEAR_SET_FUNC_NAME) - 1)) {
		if (fptr->common.num_args != 2) {
			gear_error(error_type, "Method %s::%s() must take exactly 2 arguments", ZSTR_VAL(ce->name), GEAR_SET_FUNC_NAME);
		} else if (ARG_SHOULD_BE_SENT_BY_REF(fptr, 1) || ARG_SHOULD_BE_SENT_BY_REF(fptr, 2)) {
			gear_error(error_type, "Method %s::%s() cannot take arguments by reference", ZSTR_VAL(ce->name), GEAR_SET_FUNC_NAME);
		}
	} else if (name_len == sizeof(GEAR_UNSET_FUNC_NAME) - 1 && !memcmp(lcname, GEAR_UNSET_FUNC_NAME, sizeof(GEAR_UNSET_FUNC_NAME) - 1)) {
		if (fptr->common.num_args != 1) {
			gear_error(error_type, "Method %s::%s() must take exactly 1 argument", ZSTR_VAL(ce->name), GEAR_UNSET_FUNC_NAME);
		} else if (ARG_SHOULD_BE_SENT_BY_REF(fptr, 1)) {
			gear_error(error_type, "Method %s::%s() cannot take arguments by reference", ZSTR_VAL(ce->name), GEAR_UNSET_FUNC_NAME);
		}
	} else if (name_len == sizeof(GEAR_ISSET_FUNC_NAME) - 1 && !memcmp(lcname, GEAR_ISSET_FUNC_NAME, sizeof(GEAR_ISSET_FUNC_NAME) - 1)) {
		if (fptr->common.num_args != 1) {
			gear_error(error_type, "Method %s::%s() must take exactly 1 argument", ZSTR_VAL(ce->name), GEAR_ISSET_FUNC_NAME);
		} else if (ARG_SHOULD_BE_SENT_BY_REF(fptr, 1)) {
			gear_error(error_type, "Method %s::%s() cannot take arguments by reference", ZSTR_VAL(ce->name), GEAR_ISSET_FUNC_NAME);
		}
	} else if (name_len == sizeof(GEAR_CALL_FUNC_NAME) - 1 && !memcmp(lcname, GEAR_CALL_FUNC_NAME, sizeof(GEAR_CALL_FUNC_NAME) - 1)) {
		if (fptr->common.num_args != 2) {
			gear_error(error_type, "Method %s::%s() must take exactly 2 arguments", ZSTR_VAL(ce->name), GEAR_CALL_FUNC_NAME);
		} else if (ARG_SHOULD_BE_SENT_BY_REF(fptr, 1) || ARG_SHOULD_BE_SENT_BY_REF(fptr, 2)) {
			gear_error(error_type, "Method %s::%s() cannot take arguments by reference", ZSTR_VAL(ce->name), GEAR_CALL_FUNC_NAME);
		}
	} else if (name_len == sizeof(GEAR_CALLSTATIC_FUNC_NAME) - 1 &&
		!memcmp(lcname, GEAR_CALLSTATIC_FUNC_NAME, sizeof(GEAR_CALLSTATIC_FUNC_NAME)-1)
	) {
		if (fptr->common.num_args != 2) {
			gear_error(error_type, "Method %s::__callStatic() must take exactly 2 arguments", ZSTR_VAL(ce->name));
		} else if (ARG_SHOULD_BE_SENT_BY_REF(fptr, 1) || ARG_SHOULD_BE_SENT_BY_REF(fptr, 2)) {
			gear_error(error_type, "Method %s::__callStatic() cannot take arguments by reference", ZSTR_VAL(ce->name));
		}
 	} else if (name_len == sizeof(GEAR_TOSTRING_FUNC_NAME) - 1 &&
 		!memcmp(lcname, GEAR_TOSTRING_FUNC_NAME, sizeof(GEAR_TOSTRING_FUNC_NAME)-1) && fptr->common.num_args != 0
	) {
		gear_error(error_type, "Method %s::%s() cannot take arguments", ZSTR_VAL(ce->name), GEAR_TOSTRING_FUNC_NAME);
	} else if (name_len == sizeof(GEAR_DEBUGINFO_FUNC_NAME) - 1 &&
		!memcmp(lcname, GEAR_DEBUGINFO_FUNC_NAME, sizeof(GEAR_DEBUGINFO_FUNC_NAME)-1) && fptr->common.num_args != 0) {
		gear_error(error_type, "Method %s::%s() cannot take arguments", ZSTR_VAL(ce->name), GEAR_DEBUGINFO_FUNC_NAME);
	}
}
/* }}} */

/* registers all functions in *library_functions in the function hash */
GEAR_API int gear_register_functions(gear_class_entry *scope, const gear_function_entry *functions, HashTable *function_table, int type) /* {{{ */
{
	const gear_function_entry *ptr = functions;
	gear_function function, *reg_function;
	gear_internal_function *internal_function = (gear_internal_function *)&function;
	int count=0, unload=0;
	HashTable *target_function_table = function_table;
	int error_type;
	gear_function *ctor = NULL, *dtor = NULL, *clone = NULL, *__get = NULL, *__set = NULL, *__unset = NULL, *__isset = NULL, *__call = NULL, *__callstatic = NULL, *__tostring = NULL, *__debugInfo = NULL;
	gear_string *lowercase_name;
	size_t fname_len;
	const char *lc_class_name = NULL;
	size_t class_name_len = 0;

	if (type==CAPI_PERSISTENT) {
		error_type = E_CORE_WARNING;
	} else {
		error_type = E_WARNING;
	}

	if (!target_function_table) {
		target_function_table = CG(function_table);
	}
	internal_function->type = GEAR_INTERNAL_FUNCTION;
	internal_function->cAPI = EG(current_capi);
	memset(internal_function->reserved, 0, GEAR_MAX_RESERVED_RESOURCES * sizeof(void*));

	if (scope) {
		class_name_len = ZSTR_LEN(scope->name);
		if ((lc_class_name = gear_memrchr(ZSTR_VAL(scope->name), '\\', class_name_len))) {
			++lc_class_name;
			class_name_len -= (lc_class_name - ZSTR_VAL(scope->name));
			lc_class_name = gear_str_tolower_dup(lc_class_name, class_name_len);
		} else {
			lc_class_name = gear_str_tolower_dup(ZSTR_VAL(scope->name), class_name_len);
		}
	}

	while (ptr->fname) {
		fname_len = strlen(ptr->fname);
		internal_function->handler = ptr->handler;
		internal_function->function_name = gear_string_init_interned(ptr->fname, fname_len, 1);
		internal_function->scope = scope;
		internal_function->prototype = NULL;
		if (ptr->flags) {
			if (!(ptr->flags & GEAR_ACC_PPP_MASK)) {
				if (ptr->flags != GEAR_ACC_DEPRECATED && scope) {
					gear_error(error_type, "Invalid access level for %s%s%s() - access must be exactly one of public, protected or private", scope ? ZSTR_VAL(scope->name) : "", scope ? "::" : "", ptr->fname);
				}
				internal_function->fn_flags = GEAR_ACC_PUBLIC | ptr->flags;
			} else {
				internal_function->fn_flags = ptr->flags;
			}
		} else {
			internal_function->fn_flags = GEAR_ACC_PUBLIC;
		}
		if (ptr->arg_info) {
			gear_internal_function_info *info = (gear_internal_function_info*)ptr->arg_info;

			internal_function->arg_info = (gear_internal_arg_info*)ptr->arg_info+1;
			internal_function->num_args = ptr->num_args;
			/* Currently you cannot denote that the function can accept less arguments than num_args */
			if (info->required_num_args == (gear_uintptr_t)-1) {
				internal_function->required_num_args = ptr->num_args;
			} else {
				internal_function->required_num_args = info->required_num_args;
			}
			if (info->return_reference) {
				internal_function->fn_flags |= GEAR_ACC_RETURN_REFERENCE;
			}
			if (ptr->arg_info[ptr->num_args].is_variadic) {
				internal_function->fn_flags |= GEAR_ACC_VARIADIC;
				/* Don't count the variadic argument */
				internal_function->num_args--;
			}
			if (GEAR_TYPE_IS_SET(info->type)) {
				if (GEAR_TYPE_IS_CLASS(info->type)) {
					const char *type_name = (const char*)info->type;

					if (type_name[0] == '?') {
						type_name++;
					}
					if (!scope && (!strcasecmp(type_name, "self") || !strcasecmp(type_name, "parent"))) {
						gear_error_noreturn(E_CORE_ERROR, "Cannot declare a return type of %s outside of a class scope", type_name);
					}
				}

				internal_function->fn_flags |= GEAR_ACC_HAS_RETURN_TYPE;
			}
		} else {
			internal_function->arg_info = NULL;
			internal_function->num_args = 0;
			internal_function->required_num_args = 0;
		}
		gear_set_function_arg_flags((gear_function*)internal_function);
		if (ptr->flags & GEAR_ACC_ABSTRACT) {
			if (scope) {
				/* This is a class that must be abstract itself. Here we set the check info. */
				scope->ce_flags |= GEAR_ACC_IMPLICIT_ABSTRACT_CLASS;
				if (!(scope->ce_flags & GEAR_ACC_INTERFACE)) {
					/* Since the class is not an interface it needs to be declared as a abstract class. */
					/* Since here we are handling internal functions only we can add the keyword flag. */
					/* This time we set the flag for the keyword 'abstract'. */
					scope->ce_flags |= GEAR_ACC_EXPLICIT_ABSTRACT_CLASS;
				}
			}
			if (ptr->flags & GEAR_ACC_STATIC && (!scope || !(scope->ce_flags & GEAR_ACC_INTERFACE))) {
				gear_error(error_type, "Static function %s%s%s() cannot be abstract", scope ? ZSTR_VAL(scope->name) : "", scope ? "::" : "", ptr->fname);
			}
		} else {
			if (scope && (scope->ce_flags & GEAR_ACC_INTERFACE)) {
				efree((char*)lc_class_name);
				gear_error(error_type, "Interface %s cannot contain non abstract method %s()", ZSTR_VAL(scope->name), ptr->fname);
				return FAILURE;
			}
			if (!internal_function->handler) {
				if (scope) {
					efree((char*)lc_class_name);
				}
				gear_error(error_type, "Method %s%s%s() cannot be a NULL function", scope ? ZSTR_VAL(scope->name) : "", scope ? "::" : "", ptr->fname);
				gear_unregister_functions(functions, count, target_function_table);
				return FAILURE;
			}
		}
		lowercase_name = gear_string_tolower_ex(internal_function->function_name, type == CAPI_PERSISTENT);
		lowercase_name = gear_new_interned_string(lowercase_name);
		reg_function = malloc(sizeof(gear_internal_function));
		memcpy(reg_function, &function, sizeof(gear_internal_function));
		if (gear_hash_add_ptr(target_function_table, lowercase_name, reg_function) == NULL) {
			unload=1;
			free(reg_function);
			gear_string_release(lowercase_name);
			break;
		}

		/* If types of arguments have to be checked */
		if (reg_function->common.arg_info && reg_function->common.num_args) {
			uint32_t i;
			for (i = 0; i < reg_function->common.num_args; i++) {
				if (GEAR_TYPE_IS_SET(reg_function->common.arg_info[i].type)) {
				    reg_function->common.fn_flags |= GEAR_ACC_HAS_TYPE_HINTS;
					break;
				}
			}
		}

		if (reg_function->common.arg_info &&
		    (reg_function->common.fn_flags & (GEAR_ACC_HAS_RETURN_TYPE|GEAR_ACC_HAS_TYPE_HINTS))) {
			/* convert "const char*" class type names into "gear_string*" */
			uint32_t i;
			uint32_t num_args = reg_function->common.num_args + 1;
			gear_arg_info *arg_info = reg_function->common.arg_info - 1;
			gear_arg_info *new_arg_info;

			if (reg_function->common.fn_flags & GEAR_ACC_VARIADIC) {
				num_args++;
			}
			new_arg_info = malloc(sizeof(gear_arg_info) * num_args);
			memcpy(new_arg_info, arg_info, sizeof(gear_arg_info) * num_args);
			reg_function->common.arg_info = new_arg_info + 1;
			for (i = 0; i < num_args; i++) {
				if (GEAR_TYPE_IS_CLASS(new_arg_info[i].type)) {
					const char *class_name = (const char*)new_arg_info[i].type;
					gear_bool allow_null = 0;
					gear_string *str;

					if (class_name[0] == '?') {
						class_name++;
						allow_null = 1;
					}
					str = gear_string_init_interned(class_name, strlen(class_name), 1);
					new_arg_info[i].type = GEAR_TYPE_ENCODE_CLASS(str, allow_null);
				}
			}
		}

		if (scope) {
			/* Look for ctor, dtor, clone
			 * If it's an old-style constructor, store it only if we don't have
			 * a constructor already.
			 */
			if ((fname_len == class_name_len) && !ctor && !memcmp(ZSTR_VAL(lowercase_name), lc_class_name, class_name_len+1)) {
				ctor = reg_function;
			} else if (ZSTR_VAL(lowercase_name)[0] != '_' || ZSTR_VAL(lowercase_name)[1] != '_') {
				reg_function = NULL;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_CONSTRUCTOR_FUNC_NAME)) {
				ctor = reg_function;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_DESTRUCTOR_FUNC_NAME)) {
				dtor = reg_function;
				if (internal_function->num_args) {
					gear_error(error_type, "Destructor %s::%s() cannot take arguments", ZSTR_VAL(scope->name), ptr->fname);
				}
			} else if (gear_string_equals_literal(lowercase_name, GEAR_CLONE_FUNC_NAME)) {
				clone = reg_function;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_CALL_FUNC_NAME)) {
				__call = reg_function;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_CALLSTATIC_FUNC_NAME)) {
				__callstatic = reg_function;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_TOSTRING_FUNC_NAME)) {
				__tostring = reg_function;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_GET_FUNC_NAME)) {
				__get = reg_function;
				scope->ce_flags |= GEAR_ACC_USE_GUARDS;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_SET_FUNC_NAME)) {
				__set = reg_function;
				scope->ce_flags |= GEAR_ACC_USE_GUARDS;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_UNSET_FUNC_NAME)) {
				__unset = reg_function;
				scope->ce_flags |= GEAR_ACC_USE_GUARDS;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_ISSET_FUNC_NAME)) {
				__isset = reg_function;
				scope->ce_flags |= GEAR_ACC_USE_GUARDS;
			} else if (gear_string_equals_literal(lowercase_name, GEAR_DEBUGINFO_FUNC_NAME)) {
				__debugInfo = reg_function;
			} else {
				reg_function = NULL;
			}
			if (reg_function) {
				gear_check_magic_method_implementation(scope, reg_function, error_type);
			}
		}
		ptr++;
		count++;
		gear_string_release(lowercase_name);
	}
	if (unload) { /* before unloading, display all remaining bad function in the cAPI */
		if (scope) {
			efree((char*)lc_class_name);
		}
		while (ptr->fname) {
			fname_len = strlen(ptr->fname);
			lowercase_name = gear_string_alloc(fname_len, 0);
			gear_str_tolower_copy(ZSTR_VAL(lowercase_name), ptr->fname, fname_len);
			if (gear_hash_exists(target_function_table, lowercase_name)) {
				gear_error(error_type, "Function registration failed - duplicate name - %s%s%s", scope ? ZSTR_VAL(scope->name) : "", scope ? "::" : "", ptr->fname);
			}
			gear_string_efree(lowercase_name);
			ptr++;
		}
		gear_unregister_functions(functions, count, target_function_table);
		return FAILURE;
	}
	if (scope) {
		scope->constructor = ctor;
		scope->destructor = dtor;
		scope->clone = clone;
		scope->__call = __call;
		scope->__callstatic = __callstatic;
		scope->__tostring = __tostring;
		scope->__get = __get;
		scope->__set = __set;
		scope->__unset = __unset;
		scope->__isset = __isset;
		scope->__debugInfo = __debugInfo;
		if (ctor) {
			ctor->common.fn_flags |= GEAR_ACC_CTOR;
			if (ctor->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Constructor %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(ctor->common.function_name));
			}
			ctor->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (dtor) {
			dtor->common.fn_flags |= GEAR_ACC_DTOR;
			if (dtor->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Destructor %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(dtor->common.function_name));
			}
			dtor->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (clone) {
			if (clone->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "%s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(clone->common.function_name));
			}
			clone->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (__call) {
			if (__call->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Method %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(__call->common.function_name));
			}
			__call->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (__callstatic) {
			if (!(__callstatic->common.fn_flags & GEAR_ACC_STATIC)) {
				gear_error(error_type, "Method %s::%s() must be static", ZSTR_VAL(scope->name), ZSTR_VAL(__callstatic->common.function_name));
			}
			__callstatic->common.fn_flags |= GEAR_ACC_STATIC;
		}
		if (__tostring) {
			if (__tostring->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Method %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(__tostring->common.function_name));
			}
			__tostring->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (__get) {
			if (__get->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Method %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(__get->common.function_name));
			}
			__get->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (__set) {
			if (__set->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Method %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(__set->common.function_name));
			}
			__set->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (__unset) {
			if (__unset->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Method %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(__unset->common.function_name));
			}
			__unset->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (__isset) {
			if (__isset->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Method %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(__isset->common.function_name));
			}
			__isset->common.fn_flags &= ~GEAR_ACC_ALLOW_STATIC;
		}
		if (__debugInfo) {
			if (__debugInfo->common.fn_flags & GEAR_ACC_STATIC) {
				gear_error(error_type, "Method %s::%s() cannot be static", ZSTR_VAL(scope->name), ZSTR_VAL(__debugInfo->common.function_name));
			}
		}

		if (ctor && ctor->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE && ctor->common.fn_flags & GEAR_ACC_CTOR) {
			gear_error_noreturn(E_CORE_ERROR, "Constructor %s::%s() cannot declare a return type", ZSTR_VAL(scope->name), ZSTR_VAL(ctor->common.function_name));
		}

		if (dtor && dtor->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE && dtor->common.fn_flags & GEAR_ACC_DTOR) {
			gear_error_noreturn(E_CORE_ERROR, "Destructor %s::%s() cannot declare a return type", ZSTR_VAL(scope->name), ZSTR_VAL(dtor->common.function_name));
		}

		if (clone && (clone->common.fn_flags & GEAR_ACC_HAS_RETURN_TYPE)) {
			gear_error_noreturn(E_CORE_ERROR, "%s::%s() cannot declare a return type", ZSTR_VAL(scope->name), ZSTR_VAL(clone->common.function_name));
		}
		efree((char*)lc_class_name);
	}
	return SUCCESS;
}
/* }}} */

/* count=-1 means erase all functions, otherwise,
 * erase the first count functions
 */
GEAR_API void gear_unregister_functions(const gear_function_entry *functions, int count, HashTable *function_table) /* {{{ */
{
	const gear_function_entry *ptr = functions;
	int i=0;
	HashTable *target_function_table = function_table;
	gear_string *lowercase_name;
	size_t fname_len;

	if (!target_function_table) {
		target_function_table = CG(function_table);
	}
	while (ptr->fname) {
		if (count!=-1 && i>=count) {
			break;
		}
		fname_len = strlen(ptr->fname);
		lowercase_name = gear_string_alloc(fname_len, 0);
		gear_str_tolower_copy(ZSTR_VAL(lowercase_name), ptr->fname, fname_len);
		gear_hash_del(target_function_table, lowercase_name);
		gear_string_efree(lowercase_name);
		ptr++;
		i++;
	}
}
/* }}} */

GEAR_API int gear_startup_capi(gear_capi_entry *cAPI) /* {{{ */
{
	if ((cAPI = gear_register_internal_capi(cAPI)) != NULL && gear_startup_capi_ex(cAPI) == SUCCESS) {
		return SUCCESS;
	}
	return FAILURE;
}
/* }}} */

GEAR_API int gear_get_capi_started(const char *capi_name) /* {{{ */
{
	gear_capi_entry *cAPI;

	cAPI = gear_hash_str_find_ptr(&capi_registry, capi_name, strlen(capi_name));
	return (cAPI && cAPI->capi_started) ? SUCCESS : FAILURE;
}
/* }}} */

static int clean_capi_class(zval *el, void *arg) /* {{{ */
{
	gear_class_entry *ce = (gear_class_entry *)Z_PTR_P(el);
	int capi_number = *(int *)arg;
	if (ce->type == GEAR_INTERNAL_CLASS && ce->info.internal.cAPI->capi_number == capi_number) {
		return GEAR_HASH_APPLY_REMOVE;
	} else {
		return GEAR_HASH_APPLY_KEEP;
	}
}
/* }}} */

static void clean_capi_classes(int capi_number) /* {{{ */
{
	gear_hash_apply_with_argument(EG(class_table), clean_capi_class, (void *) &capi_number);
}
/* }}} */

void capi_destructor(gear_capi_entry *cAPI) /* {{{ */
{

	if (cAPI->type == CAPI_TEMPORARY) {
		gear_clean_capi_rsrc_dtors(cAPI->capi_number);
		clean_capi_constants(cAPI->capi_number);
		clean_capi_classes(cAPI->capi_number);
	}

	if (cAPI->capi_started && cAPI->capi_shutdown_func) {
#if 0
		gear_printf("%s: cAPI shutdown\n", cAPI->name);
#endif
		cAPI->capi_shutdown_func(cAPI->type, cAPI->capi_number);
	}

	/* Deinitilaise cAPI globals */
	if (cAPI->globals_size) {
#ifdef ZTS
		if (*cAPI->globals_id_ptr) {
			ts_free_id(*cAPI->globals_id_ptr);
		}
#else
		if (cAPI->globals_dtor) {
			cAPI->globals_dtor(cAPI->globals_ptr);
		}
#endif
	}

	cAPI->capi_started=0;
	if (cAPI->type == CAPI_TEMPORARY && cAPI->functions) {
		gear_unregister_functions(cAPI->functions, -1, NULL);
	}

#if HAVE_LIBDL
	if (cAPI->handle && !getenv("GEAR_DONT_UNLOAD_CAPIS")) {
		DL_UNLOAD(cAPI->handle);
	}
#endif
}
/* }}} */

GEAR_API void gear_activate_capis(void) /* {{{ */
{
	gear_capi_entry **p = capi_request_startup_handlers;

	while (*p) {
		gear_capi_entry *cAPI = *p;

		if (cAPI->request_startup_func(cAPI->type, cAPI->capi_number)==FAILURE) {
			gear_error(E_WARNING, "request_startup() for %s cAPI failed", cAPI->name);
			exit(1);
		}
		p++;
	}
}
/* }}} */

/* call request shutdown for all cAPIs */
static int capi_registry_cleanup(zval *zv) /* {{{ */
{
	gear_capi_entry *cAPI = Z_PTR_P(zv);

	if (cAPI->request_shutdown_func) {
#if 0
		gear_printf("%s: Request shutdown\n", cAPI->name);
#endif
		cAPI->request_shutdown_func(cAPI->type, cAPI->capi_number);
	}
	return 0;
}
/* }}} */

GEAR_API void gear_deactivate_capis(void) /* {{{ */
{
	EG(current_execute_data) = NULL; /* we're no longer executing anything */

	gear_try {
		if (EG(full_tables_cleanup)) {
			gear_hash_reverse_apply(&capi_registry, capi_registry_cleanup);
		} else {
			gear_capi_entry **p = capi_request_shutdown_handlers;

			while (*p) {
				gear_capi_entry *cAPI = *p;

				cAPI->request_shutdown_func(cAPI->type, cAPI->capi_number);
				p++;
			}
		}
	} gear_end_try();
}
/* }}} */

GEAR_API void gear_cleanup_internal_classes(void) /* {{{ */
{
	gear_class_entry **p = class_cleanup_handlers;

	while (*p) {
		gear_cleanup_internal_class_data(*p);
		p++;
	}
}
/* }}} */

int capi_registry_unload_temp(const gear_capi_entry *cAPI) /* {{{ */
{
	return (cAPI->type == CAPI_TEMPORARY) ? GEAR_HASH_APPLY_REMOVE : GEAR_HASH_APPLY_STOP;
}
/* }}} */

static int capi_registry_unload_temp_wrapper(zval *el) /* {{{ */
{
	gear_capi_entry *cAPI = (gear_capi_entry *)Z_PTR_P(el);
	return capi_registry_unload_temp((const gear_capi_entry *)cAPI);
}
/* }}} */

static int exec_done_cb(zval *el) /* {{{ */
{
	gear_capi_entry *cAPI = (gear_capi_entry *)Z_PTR_P(el);
	if (cAPI->post_deactivate_func) {
		cAPI->post_deactivate_func();
	}
	return 0;
}
/* }}} */

GEAR_API void gear_post_deactivate_capis(void) /* {{{ */
{
	if (EG(full_tables_cleanup)) {
		gear_hash_apply(&capi_registry, exec_done_cb);
		gear_hash_reverse_apply(&capi_registry, capi_registry_unload_temp_wrapper);
	} else {
		gear_capi_entry **p = capi_post_deactivate_handlers;

		while (*p) {
			gear_capi_entry *cAPI = *p;

			cAPI->post_deactivate_func();
			p++;
		}
	}
}
/* }}} */

/* return the next free cAPI number */
GEAR_API int gear_next_free_capi(void) /* {{{ */
{
	return gear_hash_num_elements(&capi_registry) + 1;
}
/* }}} */

static gear_class_entry *do_register_internal_class(gear_class_entry *orig_class_entry, uint32_t ce_flags) /* {{{ */
{
	gear_class_entry *class_entry = malloc(sizeof(gear_class_entry));
	gear_string *lowercase_name;
	*class_entry = *orig_class_entry;

	class_entry->type = GEAR_INTERNAL_CLASS;
	gear_initialize_class_data(class_entry, 0);
	class_entry->ce_flags = ce_flags | GEAR_ACC_CONSTANTS_UPDATED;
	class_entry->info.internal.cAPI = EG(current_capi);

	if (class_entry->info.internal.builtin_functions) {
		gear_register_functions(class_entry, class_entry->info.internal.builtin_functions, &class_entry->function_table, EG(current_capi)->type);
	}

	lowercase_name = gear_string_tolower_ex(orig_class_entry->name, EG(current_capi)->type == CAPI_PERSISTENT);
	lowercase_name = gear_new_interned_string(lowercase_name);
	gear_hash_update_ptr(CG(class_table), lowercase_name, class_entry);
	gear_string_release_ex(lowercase_name, 1);
	return class_entry;
}
/* }}} */

/* If parent_ce is not NULL then it inherits from parent_ce
 * If parent_ce is NULL and parent_name isn't then it looks for the parent and inherits from it
 * If both parent_ce and parent_name are NULL it does a regular class registration
 * If parent_name is specified but not found NULL is returned
 */
GEAR_API gear_class_entry *gear_register_internal_class_ex(gear_class_entry *class_entry, gear_class_entry *parent_ce) /* {{{ */
{
	gear_class_entry *register_class;

	register_class = gear_register_internal_class(class_entry);

	if (parent_ce) {
		gear_do_inheritance(register_class, parent_ce);
	}
	return register_class;
}
/* }}} */

GEAR_API void gear_class_implements(gear_class_entry *class_entry, int num_interfaces, ...) /* {{{ */
{
	gear_class_entry *interface_entry;
	va_list interface_list;
	va_start(interface_list, num_interfaces);

	while (num_interfaces--) {
		interface_entry = va_arg(interface_list, gear_class_entry *);
		gear_do_implement_interface(class_entry, interface_entry);
	}

	va_end(interface_list);
}
/* }}} */

/* A class that contains at least one abstract method automatically becomes an abstract class.
 */
GEAR_API gear_class_entry *gear_register_internal_class(gear_class_entry *orig_class_entry) /* {{{ */
{
	return do_register_internal_class(orig_class_entry, 0);
}
/* }}} */

GEAR_API gear_class_entry *gear_register_internal_interface(gear_class_entry *orig_class_entry) /* {{{ */
{
	return do_register_internal_class(orig_class_entry, GEAR_ACC_INTERFACE);
}
/* }}} */

GEAR_API int gear_register_class_alias_ex(const char *name, size_t name_len, gear_class_entry *ce, int persistent) /* {{{ */
{
	gear_string *lcname;

	/* TODO: Move this out of here in 7.4. */
	if (persistent && EG(current_capi) && EG(current_capi)->type == CAPI_TEMPORARY) {
		persistent = 0;
	}

	if (name[0] == '\\') {
		lcname = gear_string_alloc(name_len-1, persistent);
		gear_str_tolower_copy(ZSTR_VAL(lcname), name+1, name_len-1);
	} else {
		lcname = gear_string_alloc(name_len, persistent);
		gear_str_tolower_copy(ZSTR_VAL(lcname), name, name_len);
	}

	gear_assert_valid_class_name(lcname);

	lcname = gear_new_interned_string(lcname);
	ce = gear_hash_add_ptr(CG(class_table), lcname, ce);
	gear_string_release_ex(lcname, 0);
	if (ce) {
		ce->refcount++;
		return SUCCESS;
	}
	return FAILURE;
}
/* }}} */

GEAR_API int gear_set_hash_symbol(zval *symbol, const char *name, int name_length, gear_bool is_ref, int num_symbol_tables, ...) /* {{{ */
{
	HashTable *symbol_table;
	va_list symbol_table_list;

	if (num_symbol_tables <= 0) return FAILURE;

	if (is_ref) {
		ZVAL_MAKE_REF(symbol);
	}

	va_start(symbol_table_list, num_symbol_tables);
	while (num_symbol_tables-- > 0) {
		symbol_table = va_arg(symbol_table_list, HashTable *);
		gear_hash_str_update(symbol_table, name, name_length, symbol);
		Z_TRY_ADDREF_P(symbol);
	}
	va_end(symbol_table_list);
	return SUCCESS;
}
/* }}} */

/* Disabled functions support */

/* {{{ proto void display_disabled_function(void)
Dummy function which displays an error when a disabled function is called. */
GEAR_API GEAR_FUNCTION(display_disabled_function)
{
	gear_error(E_WARNING, "%s() has been disabled for security reasons", get_active_function_name());
}
/* }}} */

GEAR_API int gear_disable_function(char *function_name, size_t function_name_length) /* {{{ */
{
	gear_internal_function *func;
	if ((func = gear_hash_str_find_ptr(CG(function_table), function_name, function_name_length))) {
	    func->fn_flags &= ~(GEAR_ACC_VARIADIC | GEAR_ACC_HAS_TYPE_HINTS);
		func->num_args = 0;
		func->arg_info = NULL;
		func->handler = GEAR_FN(display_disabled_function);
		return SUCCESS;
	}
	return FAILURE;
}
/* }}} */

#ifdef GEAR_WIN32
#pragma optimize("", off)
#endif
static gear_object *display_disabled_class(gear_class_entry *class_type) /* {{{ */
{
	gear_object *intern;

	intern = gear_objects_new(class_type);

	/* Initialize default properties */
	if (EXPECTED(class_type->default_properties_count != 0)) {
		zval *p = intern->properties_table;
		zval *end = p + class_type->default_properties_count;
		do {
			ZVAL_UNDEF(p);
			p++;
		} while (p != end);
	}

	gear_error(E_WARNING, "%s() has been disabled for security reasons", ZSTR_VAL(class_type->name));
	return intern;
}
#ifdef GEAR_WIN32
#pragma optimize("", on)
#endif
/* }}} */

static const gear_function_entry disabled_class_new[] = {
	GEAR_FE_END
};

GEAR_API int gear_disable_class(char *class_name, size_t class_name_length) /* {{{ */
{
	gear_class_entry *disabled_class;
	gear_string *key;

	key = gear_string_alloc(class_name_length, 0);
	gear_str_tolower_copy(ZSTR_VAL(key), class_name, class_name_length);
	disabled_class = gear_hash_find_ptr(CG(class_table), key);
	gear_string_release_ex(key, 0);
	if (!disabled_class) {
		return FAILURE;
	}
	INIT_CLASS_ENTRY_INIT_METHODS((*disabled_class), disabled_class_new);
	disabled_class->create_object = display_disabled_class;
	gear_hash_clean(&disabled_class->function_table);
	return SUCCESS;
}
/* }}} */

static int gear_is_callable_check_class(gear_string *name, gear_class_entry *scope, gear_fcall_info_cache *fcc, int *strict_class, char **error) /* {{{ */
{
	int ret = 0;
	gear_class_entry *ce;
	size_t name_len = ZSTR_LEN(name);
	gear_string *lcname;
	ALLOCA_FLAG(use_heap);

	ZSTR_ALLOCA_ALLOC(lcname, name_len, use_heap);
	gear_str_tolower_copy(ZSTR_VAL(lcname), ZSTR_VAL(name), name_len);

	*strict_class = 0;
	if (gear_string_equals_literal(lcname, "self")) {
		if (!scope) {
			if (error) *error = estrdup("cannot access self:: when no class scope is active");
		} else {
			fcc->called_scope = gear_get_called_scope(EG(current_execute_data));
			fcc->calling_scope = scope;
			if (!fcc->object) {
				fcc->object = gear_get_this_object(EG(current_execute_data));
			}
			ret = 1;
		}
	} else if (gear_string_equals_literal(lcname, "parent")) {
		if (!scope) {
			if (error) *error = estrdup("cannot access parent:: when no class scope is active");
		} else if (!scope->parent) {
			if (error) *error = estrdup("cannot access parent:: when current class scope has no parent");
		} else {
			fcc->called_scope = gear_get_called_scope(EG(current_execute_data));
			fcc->calling_scope = scope->parent;
			if (!fcc->object) {
				fcc->object = gear_get_this_object(EG(current_execute_data));
			}
			*strict_class = 1;
			ret = 1;
		}
	} else if (gear_string_equals_literal(lcname, "static")) {
		gear_class_entry *called_scope = gear_get_called_scope(EG(current_execute_data));

		if (!called_scope) {
			if (error) *error = estrdup("cannot access static:: when no class scope is active");
		} else {
			fcc->called_scope = called_scope;
			fcc->calling_scope = called_scope;
			if (!fcc->object) {
				fcc->object = gear_get_this_object(EG(current_execute_data));
			}
			*strict_class = 1;
			ret = 1;
		}
	} else if ((ce = gear_lookup_class_ex(name, NULL, 1)) != NULL) {
		gear_class_entry *scope;
		gear_execute_data *ex = EG(current_execute_data);

		while (ex && (!ex->func || !GEAR_USER_CODE(ex->func->type))) {
			ex = ex->prev_execute_data;
		}
		scope = ex ? ex->func->common.scope : NULL;
		fcc->calling_scope = ce;
		if (scope && !fcc->object) {
			gear_object *object = gear_get_this_object(EG(current_execute_data));

			if (object &&
			    instanceof_function(object->ce, scope) &&
			    instanceof_function(scope, ce)) {
				fcc->object = object;
				fcc->called_scope = object->ce;
			} else {
				fcc->called_scope = ce;
			}
		} else {
			fcc->called_scope = fcc->object ? fcc->object->ce : ce;
		}
		*strict_class = 1;
		ret = 1;
	} else {
		if (error) gear_spprintf(error, 0, "class '%.*s' not found", (int)name_len, ZSTR_VAL(name));
	}
	ZSTR_ALLOCA_FREE(lcname, use_heap);
	return ret;
}
/* }}} */

static gear_always_inline int gear_is_callable_check_func(int check_flags, zval *callable, gear_fcall_info_cache *fcc, int strict_class, char **error) /* {{{ */
{
	gear_class_entry *ce_org = fcc->calling_scope;
	int retval = 0;
	gear_string *mname, *cname;
	gear_string *lmname;
	const char *colon;
	size_t clen;
	HashTable *ftable;
	int call_via_handler = 0;
	gear_class_entry *scope;
	zval *zv;
	ALLOCA_FLAG(use_heap)

	fcc->calling_scope = NULL;

	if (!ce_org) {
		gear_function *func;
		gear_string *lmname;

		/* Check if function with given name exists.
		 * This may be a compound name that includes namespace name */
		if (UNEXPECTED(Z_STRVAL_P(callable)[0] == '\\')) {
			/* Skip leading \ */
			ZSTR_ALLOCA_ALLOC(lmname, Z_STRLEN_P(callable) - 1, use_heap);
			gear_str_tolower_copy(ZSTR_VAL(lmname), Z_STRVAL_P(callable) + 1, Z_STRLEN_P(callable) - 1);
			func = gear_fetch_function(lmname);
			ZSTR_ALLOCA_FREE(lmname, use_heap);
		} else {
			lmname = Z_STR_P(callable);
			func = gear_fetch_function(lmname);
			if (!func) {
				ZSTR_ALLOCA_ALLOC(lmname, Z_STRLEN_P(callable), use_heap);
				gear_str_tolower_copy(ZSTR_VAL(lmname), Z_STRVAL_P(callable), Z_STRLEN_P(callable));
				func = gear_fetch_function(lmname);
				ZSTR_ALLOCA_FREE(lmname, use_heap);
			}
		}
		if (EXPECTED(func != NULL)) {
			fcc->function_handler = func;
			return 1;
		}
	}

	/* Split name into class/namespace and method/function names */
	if ((colon = gear_memrchr(Z_STRVAL_P(callable), ':', Z_STRLEN_P(callable))) != NULL &&
		colon > Z_STRVAL_P(callable) &&
		*(colon-1) == ':'
	) {
		size_t mlen;

		colon--;
		clen = colon - Z_STRVAL_P(callable);
		mlen = Z_STRLEN_P(callable) - clen - 2;

		if (colon == Z_STRVAL_P(callable)) {
			if (error) *error = estrdup("invalid function name");
			return 0;
		}

		/* This is a compound name.
		 * Try to fetch class and then find static method. */
		if (ce_org) {
			scope = ce_org;
		} else {
			scope = gear_get_executed_scope();
		}

		cname = gear_string_init(Z_STRVAL_P(callable), clen, 0);
		if (!gear_is_callable_check_class(cname, scope, fcc, &strict_class, error)) {
			gear_string_release_ex(cname, 0);
			return 0;
		}
		gear_string_release_ex(cname, 0);

		ftable = &fcc->calling_scope->function_table;
		if (ce_org && !instanceof_function(ce_org, fcc->calling_scope)) {
			if (error) gear_spprintf(error, 0, "class '%s' is not a subclass of '%s'", ZSTR_VAL(ce_org->name), ZSTR_VAL(fcc->calling_scope->name));
			return 0;
		}
		mname = gear_string_init(Z_STRVAL_P(callable) + clen + 2, mlen, 0);
	} else if (ce_org) {
		/* Try to fetch find static method of given class. */
		mname = Z_STR_P(callable);
		gear_string_addref(mname);
		ftable = &ce_org->function_table;
		fcc->calling_scope = ce_org;
	} else {
		/* We already checked for plain function before. */
		if (error && !(check_flags & IS_CALLABLE_CHECK_SILENT)) {
			gear_spprintf(error, 0, "function '%s' not found or invalid function name", Z_STRVAL_P(callable));
		}
		return 0;
	}

	lmname = gear_string_tolower(mname);
	if (strict_class &&
	    fcc->calling_scope &&
		gear_string_equals_literal(lmname, GEAR_CONSTRUCTOR_FUNC_NAME)) {
		fcc->function_handler = fcc->calling_scope->constructor;
		if (fcc->function_handler) {
			retval = 1;
		}
	} else if ((zv = gear_hash_find(ftable, lmname)) != NULL) {
		fcc->function_handler = Z_PTR_P(zv);
		retval = 1;
		if ((fcc->function_handler->op_array.fn_flags & GEAR_ACC_CHANGED) &&
		    !strict_class) {
			scope = gear_get_executed_scope();
			if (scope &&
			    instanceof_function(fcc->function_handler->common.scope, scope)) {

				zv = gear_hash_find(&scope->function_table, lmname);
				if (zv != NULL) {
					gear_function *priv_fbc = Z_PTR_P(zv);

					if (priv_fbc->common.fn_flags & GEAR_ACC_PRIVATE
					 && priv_fbc->common.scope == scope) {
						fcc->function_handler = priv_fbc;
					}
				}
			}
		}
		if ((check_flags & IS_CALLABLE_CHECK_NO_ACCESS) == 0 &&
		    (fcc->calling_scope &&
		     ((fcc->object && fcc->calling_scope->__call) ||
		      (!fcc->object && fcc->calling_scope->__callstatic)))) {
			if (fcc->function_handler->op_array.fn_flags & GEAR_ACC_PRIVATE) {
				scope = gear_get_executed_scope();
				if (!gear_check_private(fcc->function_handler, fcc->object ? fcc->object->ce : scope, lmname)) {
					retval = 0;
					fcc->function_handler = NULL;
					goto get_function_via_handler;
				}
			} else if (fcc->function_handler->common.fn_flags & GEAR_ACC_PROTECTED) {
				scope = gear_get_executed_scope();
				if (!gear_check_protected(fcc->function_handler->common.scope, scope)) {
					retval = 0;
					fcc->function_handler = NULL;
					goto get_function_via_handler;
				}
			}
		}
	} else {
get_function_via_handler:
		if (fcc->object && fcc->calling_scope == ce_org) {
			if (strict_class && ce_org->__call) {
				fcc->function_handler = gear_get_call_trampoline_func(ce_org, mname, 0);
				call_via_handler = 1;
				retval = 1;
			} else if (fcc->object->handlers->get_method) {
				fcc->function_handler = fcc->object->handlers->get_method(&fcc->object, mname, NULL);
				if (fcc->function_handler) {
					if (strict_class &&
					    (!fcc->function_handler->common.scope ||
					     !instanceof_function(ce_org, fcc->function_handler->common.scope))) {
						if (fcc->function_handler->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) {
							if (fcc->function_handler->type != GEAR_OVERLOADED_FUNCTION &&
								fcc->function_handler->common.function_name) {
								gear_string_release_ex(fcc->function_handler->common.function_name, 0);
							}
							gear_free_trampoline(fcc->function_handler);
						}
					} else {
						retval = 1;
						call_via_handler = (fcc->function_handler->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) != 0;
					}
				}
			}
		} else if (fcc->calling_scope) {
			if (fcc->calling_scope->get_static_method) {
				fcc->function_handler = fcc->calling_scope->get_static_method(fcc->calling_scope, mname);
			} else {
				fcc->function_handler = gear_std_get_static_method(fcc->calling_scope, mname, NULL);
			}
			if (fcc->function_handler) {
				retval = 1;
				call_via_handler = (fcc->function_handler->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) != 0;
				if (call_via_handler && !fcc->object) {
					gear_object *object = gear_get_this_object(EG(current_execute_data));
					if (object &&
					    instanceof_function(object->ce, fcc->calling_scope)) {
						fcc->object = object;
					}
				}
			}
		}
	}

	if (retval) {
		if (fcc->calling_scope && !call_via_handler) {
			if (fcc->function_handler->common.fn_flags & GEAR_ACC_ABSTRACT) {
				if (error) {
					gear_spprintf(error, 0, "cannot call abstract method %s::%s()", ZSTR_VAL(fcc->calling_scope->name), ZSTR_VAL(fcc->function_handler->common.function_name));
					retval = 0;
				} else {
					gear_throw_error(NULL, "Cannot call abstract method %s::%s()", ZSTR_VAL(fcc->calling_scope->name), ZSTR_VAL(fcc->function_handler->common.function_name));
					retval = 0;
				}
			} else if (!fcc->object && !(fcc->function_handler->common.fn_flags & GEAR_ACC_STATIC)) {
				int severity;
				char *verb;
				if (fcc->function_handler->common.fn_flags & GEAR_ACC_ALLOW_STATIC) {
					severity = E_DEPRECATED;
					verb = "should not";
				} else {
					/* An internal function assumes $this is present and won't check that. So HYSS would crash by allowing the call. */
					severity = E_ERROR;
					verb = "cannot";
				}
				if ((check_flags & IS_CALLABLE_CHECK_IS_STATIC) != 0) {
					retval = 0;
				}
				if (error) {
					gear_spprintf(error, 0, "non-static method %s::%s() %s be called statically", ZSTR_VAL(fcc->calling_scope->name), ZSTR_VAL(fcc->function_handler->common.function_name), verb);
					if (severity != E_DEPRECATED) {
						retval = 0;
					}
				} else if (retval) {
					if (severity == E_ERROR) {
						gear_throw_error(NULL, "Non-static method %s::%s() %s be called statically", ZSTR_VAL(fcc->calling_scope->name), ZSTR_VAL(fcc->function_handler->common.function_name), verb);
					} else {
						gear_error(severity, "Non-static method %s::%s() %s be called statically", ZSTR_VAL(fcc->calling_scope->name), ZSTR_VAL(fcc->function_handler->common.function_name), verb);
					}
				}
			}
			if (retval && (check_flags & IS_CALLABLE_CHECK_NO_ACCESS) == 0) {
				if (fcc->function_handler->op_array.fn_flags & GEAR_ACC_PRIVATE) {
					scope = gear_get_executed_scope();
					if (!gear_check_private(fcc->function_handler, fcc->object ? fcc->object->ce : scope, lmname)) {
						if (error) {
							if (*error) {
								efree(*error);
							}
							gear_spprintf(error, 0, "cannot access private method %s::%s()", ZSTR_VAL(fcc->calling_scope->name), ZSTR_VAL(fcc->function_handler->common.function_name));
						}
						retval = 0;
					}
				} else if ((fcc->function_handler->common.fn_flags & GEAR_ACC_PROTECTED)) {
					scope = gear_get_executed_scope();
					if (!gear_check_protected(fcc->function_handler->common.scope, scope)) {
						if (error) {
							if (*error) {
								efree(*error);
							}
							gear_spprintf(error, 0, "cannot access protected method %s::%s()", ZSTR_VAL(fcc->calling_scope->name), ZSTR_VAL(fcc->function_handler->common.function_name));
						}
						retval = 0;
					}
				}
			}
		}
	} else if (error && !(check_flags & IS_CALLABLE_CHECK_SILENT)) {
		if (fcc->calling_scope) {
			if (error) gear_spprintf(error, 0, "class '%s' does not have a method '%s'", ZSTR_VAL(fcc->calling_scope->name), ZSTR_VAL(mname));
		} else {
			if (error) gear_spprintf(error, 0, "function '%s' does not exist", ZSTR_VAL(mname));
		}
	}
	gear_string_release_ex(lmname, 0);
	gear_string_release_ex(mname, 0);

	if (fcc->object) {
		fcc->called_scope = fcc->object->ce;
	}
	return retval;
}
/* }}} */

static gear_string *gear_create_method_string(gear_string *class_name, gear_string *method_name) {
	gear_string *callable_name = gear_string_alloc(
		ZSTR_LEN(class_name) + ZSTR_LEN(method_name) + sizeof("::") - 1, 0);
	char *ptr = ZSTR_VAL(callable_name);
	memcpy(ptr, ZSTR_VAL(class_name), ZSTR_LEN(class_name));
	ptr += ZSTR_LEN(class_name);
	memcpy(ptr, "::", sizeof("::") - 1);
	ptr += sizeof("::") - 1;
	memcpy(ptr, ZSTR_VAL(method_name), ZSTR_LEN(method_name) + 1);
	return callable_name;
}

GEAR_API gear_string *gear_get_callable_name_ex(zval *callable, gear_object *object) /* {{{ */
{
try_again:
	switch (Z_TYPE_P(callable)) {
		case IS_STRING:
			if (object) {
				return gear_create_method_string(object->ce->name, Z_STR_P(callable));
			}
			return gear_string_copy(Z_STR_P(callable));

		case IS_ARRAY:
		{
			zval *method = NULL;
			zval *obj = NULL;

			if (gear_hash_num_elements(Z_ARRVAL_P(callable)) == 2) {
				obj = gear_hash_index_find_deref(Z_ARRVAL_P(callable), 0);
				method = gear_hash_index_find_deref(Z_ARRVAL_P(callable), 1);
			}

			if (obj == NULL || method == NULL || Z_TYPE_P(method) != IS_STRING) {
				return gear_string_init("Array", sizeof("Array")-1, 0);
			}

			if (Z_TYPE_P(obj) == IS_STRING) {
				return gear_create_method_string(Z_STR_P(obj), Z_STR_P(method));
			} else if (Z_TYPE_P(obj) == IS_OBJECT) {
				return gear_create_method_string(Z_OBJCE_P(obj)->name, Z_STR_P(method));
			} else {
				return gear_string_init("Array", sizeof("Array")-1, 0);
			}
		}
		case IS_OBJECT:
		{
			gear_class_entry *calling_scope;
			gear_function *fptr;
			gear_object *object;
			if (Z_OBJ_HANDLER_P(callable, get_closure)
					&& Z_OBJ_HANDLER_P(callable, get_closure)(callable, &calling_scope, &fptr, &object) == SUCCESS) {
				gear_class_entry *ce = Z_OBJCE_P(callable);
				gear_string *callable_name = gear_string_alloc(
					ZSTR_LEN(ce->name) + sizeof("::__invoke") - 1, 0);
				memcpy(ZSTR_VAL(callable_name), ZSTR_VAL(ce->name), ZSTR_LEN(ce->name));
				memcpy(ZSTR_VAL(callable_name) + ZSTR_LEN(ce->name), "::__invoke", sizeof("::__invoke"));
				return callable_name;
			}
			return zval_get_string(callable);
		}
		case IS_REFERENCE:
			callable = Z_REFVAL_P(callable);
			goto try_again;
		default:
			return zval_get_string_func(callable);
	}
}
/* }}} */

GEAR_API gear_string *gear_get_callable_name(zval *callable) /* {{{ */
{
	return gear_get_callable_name_ex(callable, NULL);
}
/* }}} */

static gear_always_inline gear_bool gear_is_callable_impl(zval *callable, gear_object *object, uint32_t check_flags, gear_fcall_info_cache *fcc, char **error) /* {{{ */
{
	gear_bool ret;
	gear_fcall_info_cache fcc_local;
	int strict_class = 0;

	if (fcc == NULL) {
		fcc = &fcc_local;
	}
	if (error) {
		*error = NULL;
	}

	fcc->calling_scope = NULL;
	fcc->called_scope = NULL;
	fcc->function_handler = NULL;
	fcc->object = NULL;

again:
	switch (Z_TYPE_P(callable)) {
		case IS_STRING:
			if (object) {
				fcc->object = object;
				fcc->calling_scope = object->ce;
			}

			if (check_flags & IS_CALLABLE_CHECK_SYNTAX_ONLY) {
				fcc->called_scope = fcc->calling_scope;
				return 1;
			}

check_func:
			ret = gear_is_callable_check_func(check_flags, callable, fcc, strict_class, error);
			if (fcc == &fcc_local &&
			    fcc->function_handler &&
				((fcc->function_handler->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) ||
			     fcc->function_handler->type == GEAR_OVERLOADED_FUNCTION_TEMPORARY ||
			     fcc->function_handler->type == GEAR_OVERLOADED_FUNCTION)) {
				if (fcc->function_handler->type != GEAR_OVERLOADED_FUNCTION &&
					fcc->function_handler->common.function_name) {
					gear_string_release_ex(fcc->function_handler->common.function_name, 0);
				}
				gear_free_trampoline(fcc->function_handler);
			}
			return ret;

		case IS_ARRAY:
			{
				zval *method = NULL;
				zval *obj = NULL;

				if (gear_hash_num_elements(Z_ARRVAL_P(callable)) == 2) {
					obj = gear_hash_index_find(Z_ARRVAL_P(callable), 0);
					method = gear_hash_index_find(Z_ARRVAL_P(callable), 1);
				}

				do {
					if (obj == NULL || method == NULL) {
						break;
					}

					ZVAL_DEREF(method);
					if (Z_TYPE_P(method) != IS_STRING) {
						break;
					}

					ZVAL_DEREF(obj);
					if (Z_TYPE_P(obj) == IS_STRING) {
						if (check_flags & IS_CALLABLE_CHECK_SYNTAX_ONLY) {
							return 1;
						}

						if (!gear_is_callable_check_class(Z_STR_P(obj), gear_get_executed_scope(), fcc, &strict_class, error)) {
							return 0;
						}

					} else if (Z_TYPE_P(obj) == IS_OBJECT) {

						fcc->calling_scope = Z_OBJCE_P(obj); /* TBFixed: what if it's overloaded? */

						fcc->object = Z_OBJ_P(obj);

						if (check_flags & IS_CALLABLE_CHECK_SYNTAX_ONLY) {
							fcc->called_scope = fcc->calling_scope;
							return 1;
						}
					} else {
						break;
					}

					callable = method;
					goto check_func;

				} while (0);
				if (gear_hash_num_elements(Z_ARRVAL_P(callable)) == 2) {
					if (!obj || (!Z_ISREF_P(obj)?
								(Z_TYPE_P(obj) != IS_STRING && Z_TYPE_P(obj) != IS_OBJECT) :
								(Z_TYPE_P(Z_REFVAL_P(obj)) != IS_STRING && Z_TYPE_P(Z_REFVAL_P(obj)) != IS_OBJECT))) {
						if (error) *error = estrdup("first array member is not a valid class name or object");
					} else {
						if (error) *error = estrdup("second array member is not a valid method");
					}
				} else {
					if (error) *error = estrdup("array must have exactly two members");
				}
			}
			return 0;
		case IS_OBJECT:
			if (Z_OBJ_HANDLER_P(callable, get_closure) && Z_OBJ_HANDLER_P(callable, get_closure)(callable, &fcc->calling_scope, &fcc->function_handler, &fcc->object) == SUCCESS) {
				fcc->called_scope = fcc->calling_scope;
				return 1;
			}
			if (error) *error = estrdup("no array or string given");
			return 0;
		case IS_REFERENCE:
			callable = Z_REFVAL_P(callable);
			goto again;
		default:
			if (error) *error = estrdup("no array or string given");
			return 0;
	}
}
/* }}} */

GEAR_API gear_bool gear_is_callable_ex(zval *callable, gear_object *object, uint32_t check_flags, gear_string **callable_name, gear_fcall_info_cache *fcc, char **error) /* {{{ */
{
	gear_bool ret = gear_is_callable_impl(callable, object, check_flags, fcc, error);
	if (callable_name) {
		*callable_name = gear_get_callable_name_ex(callable, object);
	}
	return ret;
}

GEAR_API gear_bool gear_is_callable(zval *callable, uint32_t check_flags, gear_string **callable_name) /* {{{ */
{
	return gear_is_callable_ex(callable, NULL, check_flags, callable_name, NULL, NULL);
}
/* }}} */

GEAR_API gear_bool gear_make_callable(zval *callable, gear_string **callable_name) /* {{{ */
{
	gear_fcall_info_cache fcc;

	if (gear_is_callable_ex(callable, NULL, IS_CALLABLE_STRICT, callable_name, &fcc, NULL)) {
		if (Z_TYPE_P(callable) == IS_STRING && fcc.calling_scope) {
			zval_ptr_dtor_str(callable);
			array_init(callable);
			add_next_index_str(callable, gear_string_copy(fcc.calling_scope->name));
			add_next_index_str(callable, gear_string_copy(fcc.function_handler->common.function_name));
		}
		if (fcc.function_handler &&
			((fcc.function_handler->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) ||
		     fcc.function_handler->type == GEAR_OVERLOADED_FUNCTION_TEMPORARY ||
		     fcc.function_handler->type == GEAR_OVERLOADED_FUNCTION)) {
			if (fcc.function_handler->type != GEAR_OVERLOADED_FUNCTION) {
				gear_string_release_ex(fcc.function_handler->common.function_name, 0);
			}
			gear_free_trampoline(fcc.function_handler);
		}
		return 1;
	}
	return 0;
}
/* }}} */

GEAR_API int gear_fcall_info_init(zval *callable, uint32_t check_flags, gear_fcall_info *fci, gear_fcall_info_cache *fcc, gear_string **callable_name, char **error) /* {{{ */
{
	if (!gear_is_callable_ex(callable, NULL, check_flags, callable_name, fcc, error)) {
		return FAILURE;
	}

	fci->size = sizeof(*fci);
	fci->object = fcc->object;
	ZVAL_COPY_VALUE(&fci->function_name, callable);
	fci->retval = NULL;
	fci->param_count = 0;
	fci->params = NULL;
	fci->no_separation = 1;

	return SUCCESS;
}
/* }}} */

GEAR_API void gear_fcall_info_args_clear(gear_fcall_info *fci, int free_mem) /* {{{ */
{
	if (fci->params) {
		zval *p = fci->params;
		zval *end = p + fci->param_count;

		while (p != end) {
			i_zval_ptr_dtor(p GEAR_FILE_LINE_CC);
			p++;
		}
		if (free_mem) {
			efree(fci->params);
			fci->params = NULL;
		}
	}
	fci->param_count = 0;
}
/* }}} */

GEAR_API void gear_fcall_info_args_save(gear_fcall_info *fci, int *param_count, zval **params) /* {{{ */
{
	*param_count = fci->param_count;
	*params = fci->params;
	fci->param_count = 0;
	fci->params = NULL;
}
/* }}} */

GEAR_API void gear_fcall_info_args_restore(gear_fcall_info *fci, int param_count, zval *params) /* {{{ */
{
	gear_fcall_info_args_clear(fci, 1);
	fci->param_count = param_count;
	fci->params = params;
}
/* }}} */

GEAR_API int gear_fcall_info_args_ex(gear_fcall_info *fci, gear_function *func, zval *args) /* {{{ */
{
	zval *arg, *params;
	uint32_t n = 1;

	gear_fcall_info_args_clear(fci, !args);

	if (!args) {
		return SUCCESS;
	}

	if (Z_TYPE_P(args) != IS_ARRAY) {
		return FAILURE;
	}

	fci->param_count = gear_hash_num_elements(Z_ARRVAL_P(args));
	fci->params = params = (zval *) erealloc(fci->params, fci->param_count * sizeof(zval));

	GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(args), arg) {
		if (func && !Z_ISREF_P(arg) && ARG_SHOULD_BE_SENT_BY_REF(func, n)) {
			ZVAL_NEW_REF(params, arg);
			Z_TRY_ADDREF_P(arg);
		} else {
			ZVAL_COPY(params, arg);
		}
		params++;
		n++;
	} GEAR_HASH_FOREACH_END();

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_fcall_info_args(gear_fcall_info *fci, zval *args) /* {{{ */
{
	return gear_fcall_info_args_ex(fci, NULL, args);
}
/* }}} */

GEAR_API int gear_fcall_info_argp(gear_fcall_info *fci, int argc, zval *argv) /* {{{ */
{
	int i;

	if (argc < 0) {
		return FAILURE;
	}

	gear_fcall_info_args_clear(fci, !argc);

	if (argc) {
		fci->param_count = argc;
		fci->params = (zval *) erealloc(fci->params, fci->param_count * sizeof(zval));

		for (i = 0; i < argc; ++i) {
			ZVAL_COPY(&fci->params[i], &argv[i]);
		}
	}

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_fcall_info_argv(gear_fcall_info *fci, int argc, va_list *argv) /* {{{ */
{
	int i;
	zval *arg;

	if (argc < 0) {
		return FAILURE;
	}

	gear_fcall_info_args_clear(fci, !argc);

	if (argc) {
		fci->param_count = argc;
		fci->params = (zval *) erealloc(fci->params, fci->param_count * sizeof(zval));

		for (i = 0; i < argc; ++i) {
			arg = va_arg(*argv, zval *);
			ZVAL_COPY(&fci->params[i], arg);
		}
	}

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_fcall_info_argn(gear_fcall_info *fci, int argc, ...) /* {{{ */
{
	int ret;
	va_list argv;

	va_start(argv, argc);
	ret = gear_fcall_info_argv(fci, argc, &argv);
	va_end(argv);

	return ret;
}
/* }}} */

GEAR_API int gear_fcall_info_call(gear_fcall_info *fci, gear_fcall_info_cache *fcc, zval *retval_ptr, zval *args) /* {{{ */
{
	zval retval, *org_params = NULL;
	int result, org_count = 0;

	fci->retval = retval_ptr ? retval_ptr : &retval;
	if (args) {
		gear_fcall_info_args_save(fci, &org_count, &org_params);
		gear_fcall_info_args(fci, args);
	}
	result = gear_call_function(fci, fcc);

	if (!retval_ptr && Z_TYPE(retval) != IS_UNDEF) {
		zval_ptr_dtor(&retval);
	}
	if (args) {
		gear_fcall_info_args_restore(fci, org_count, org_params);
	}
	return result;
}
/* }}} */

GEAR_API const char *gear_get_capi_version(const char *capi_name) /* {{{ */
{
	gear_string *lname;
	size_t name_len = strlen(capi_name);
	gear_capi_entry *cAPI;

	lname = gear_string_alloc(name_len, 0);
	gear_str_tolower_copy(ZSTR_VAL(lname), capi_name, name_len);
	cAPI = gear_hash_find_ptr(&capi_registry, lname);
	gear_string_efree(lname);
	return cAPI ? cAPI->version : NULL;
}
/* }}} */

static inline gear_string *zval_make_interned_string(zval *zv) /* {{{ */
{
	GEAR_ASSERT(Z_TYPE_P(zv) == IS_STRING);
	Z_STR_P(zv) = gear_new_interned_string(Z_STR_P(zv));
	if (ZSTR_IS_INTERNED(Z_STR_P(zv))) {
		Z_TYPE_FLAGS_P(zv) = 0;
	}
	return Z_STR_P(zv);
}

static gear_always_inline gear_bool is_persistent_class(gear_class_entry *ce) {
	return (ce->type & GEAR_INTERNAL_CLASS)
		&& ce->info.internal.cAPI->type == CAPI_PERSISTENT;
}

GEAR_API int gear_declare_property_ex(gear_class_entry *ce, gear_string *name, zval *property, int access_type, gear_string *doc_comment) /* {{{ */
{
	gear_property_info *property_info, *property_info_ptr;

	if (ce->type == GEAR_INTERNAL_CLASS) {
		property_info = pemalloc(sizeof(gear_property_info), 1);
	} else {
		property_info = gear_arena_alloc(&CG(arena), sizeof(gear_property_info));
		if (Z_TYPE_P(property) == IS_CONSTANT_AST) {
			ce->ce_flags &= ~GEAR_ACC_CONSTANTS_UPDATED;
		}
	}

	if (Z_TYPE_P(property) == IS_STRING && !ZSTR_IS_INTERNED(Z_STR_P(property))) {
		zval_make_interned_string(property);
	}

	if (!(access_type & GEAR_ACC_PPP_MASK)) {
		access_type |= GEAR_ACC_PUBLIC;
	}
	if (access_type & GEAR_ACC_STATIC) {
		if ((property_info_ptr = gear_hash_find_ptr(&ce->properties_info, name)) != NULL &&
		    (property_info_ptr->flags & GEAR_ACC_STATIC) != 0) {
			property_info->offset = property_info_ptr->offset;
			zval_ptr_dtor(&ce->default_static_members_table[property_info->offset]);
			gear_hash_del(&ce->properties_info, name);
		} else {
			property_info->offset = ce->default_static_members_count++;
			ce->default_static_members_table = perealloc(ce->default_static_members_table, sizeof(zval) * ce->default_static_members_count, ce->type == GEAR_INTERNAL_CLASS);
		}
		ZVAL_COPY_VALUE(&ce->default_static_members_table[property_info->offset], property);
		if (ce->type == GEAR_USER_CLASS) {
			ce->static_members_table = ce->default_static_members_table;
		}
	} else {
		if ((property_info_ptr = gear_hash_find_ptr(&ce->properties_info, name)) != NULL &&
		    (property_info_ptr->flags & GEAR_ACC_STATIC) == 0) {
			property_info->offset = property_info_ptr->offset;
			zval_ptr_dtor(&ce->default_properties_table[OBJ_PROP_TO_NUM(property_info->offset)]);
			gear_hash_del(&ce->properties_info, name);
		} else {
			property_info->offset = OBJ_PROP_TO_OFFSET(ce->default_properties_count);
			ce->default_properties_count++;
			ce->default_properties_table = perealloc(ce->default_properties_table, sizeof(zval) * ce->default_properties_count, ce->type == GEAR_INTERNAL_CLASS);
		}
		ZVAL_COPY_VALUE(&ce->default_properties_table[OBJ_PROP_TO_NUM(property_info->offset)], property);
	}
	if (ce->type & GEAR_INTERNAL_CLASS) {
		switch(Z_TYPE_P(property)) {
			case IS_ARRAY:
			case IS_OBJECT:
			case IS_RESOURCE:
				gear_error_noreturn(E_CORE_ERROR, "Internal zval's can't be arrays, objects or resources");
				break;
			default:
				break;
		}

		/* Must be interned to avoid ZTS data races */
		name = gear_new_interned_string(gear_string_copy(name));
	}

	if (access_type & GEAR_ACC_PUBLIC) {
		property_info->name = gear_string_copy(name);
	} else if (access_type & GEAR_ACC_PRIVATE) {
		property_info->name = gear_mangle_property_name(ZSTR_VAL(ce->name), ZSTR_LEN(ce->name), ZSTR_VAL(name), ZSTR_LEN(name), is_persistent_class(ce));
	} else {
		GEAR_ASSERT(access_type & GEAR_ACC_PROTECTED);
		property_info->name = gear_mangle_property_name("*", 1, ZSTR_VAL(name), ZSTR_LEN(name), is_persistent_class(ce));
	}

	property_info->name = gear_new_interned_string(property_info->name);
	property_info->flags = access_type;
	property_info->doc_comment = doc_comment;
	property_info->ce = ce;
	gear_hash_update_ptr(&ce->properties_info, name, property_info);

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_declare_property(gear_class_entry *ce, const char *name, size_t name_length, zval *property, int access_type) /* {{{ */
{
	gear_string *key = gear_string_init(name, name_length, is_persistent_class(ce));
	int ret = gear_declare_property_ex(ce, key, property, access_type, NULL);
	gear_string_release(key);
	return ret;
}
/* }}} */

GEAR_API int gear_declare_property_null(gear_class_entry *ce, const char *name, size_t name_length, int access_type) /* {{{ */
{
	zval property;

	ZVAL_NULL(&property);
	return gear_declare_property(ce, name, name_length, &property, access_type);
}
/* }}} */

GEAR_API int gear_declare_property_bool(gear_class_entry *ce, const char *name, size_t name_length, gear_long value, int access_type) /* {{{ */
{
	zval property;

	ZVAL_BOOL(&property, value);
	return gear_declare_property(ce, name, name_length, &property, access_type);
}
/* }}} */

GEAR_API int gear_declare_property_long(gear_class_entry *ce, const char *name, size_t name_length, gear_long value, int access_type) /* {{{ */
{
	zval property;

	ZVAL_LONG(&property, value);
	return gear_declare_property(ce, name, name_length, &property, access_type);
}
/* }}} */

GEAR_API int gear_declare_property_double(gear_class_entry *ce, const char *name, size_t name_length, double value, int access_type) /* {{{ */
{
	zval property;

	ZVAL_DOUBLE(&property, value);
	return gear_declare_property(ce, name, name_length, &property, access_type);
}
/* }}} */

GEAR_API int gear_declare_property_string(gear_class_entry *ce, const char *name, size_t name_length, const char *value, int access_type) /* {{{ */
{
	zval property;

	ZVAL_NEW_STR(&property, gear_string_init(value, strlen(value), ce->type & GEAR_INTERNAL_CLASS));
	return gear_declare_property(ce, name, name_length, &property, access_type);
}
/* }}} */

GEAR_API int gear_declare_property_stringl(gear_class_entry *ce, const char *name, size_t name_length, const char *value, size_t value_len, int access_type) /* {{{ */
{
	zval property;

	ZVAL_NEW_STR(&property, gear_string_init(value, value_len, ce->type & GEAR_INTERNAL_CLASS));
	return gear_declare_property(ce, name, name_length, &property, access_type);
}
/* }}} */

GEAR_API int gear_declare_class_constant_ex(gear_class_entry *ce, gear_string *name, zval *value, int access_type, gear_string *doc_comment) /* {{{ */
{
	gear_class_constant *c;

	if (ce->ce_flags & GEAR_ACC_INTERFACE) {
		if (access_type != GEAR_ACC_PUBLIC) {
			gear_error_noreturn(E_COMPILE_ERROR, "Access type for interface constant %s::%s must be public", ZSTR_VAL(ce->name), ZSTR_VAL(name));
		}
	}

	if (gear_string_equals_literal_ci(name, "class")) {
		gear_error_noreturn(ce->type == GEAR_INTERNAL_CLASS ? E_CORE_ERROR : E_COMPILE_ERROR,
				"A class constant must not be called 'class'; it is reserved for class name fetching");
	}

	if (Z_TYPE_P(value) == IS_STRING && !ZSTR_IS_INTERNED(Z_STR_P(value))) {
		zval_make_interned_string(value);
	}

	if (ce->type == GEAR_INTERNAL_CLASS) {
		c = pemalloc(sizeof(gear_class_constant), 1);
	} else {
		c = gear_arena_alloc(&CG(arena), sizeof(gear_class_constant));
	}
	ZVAL_COPY_VALUE(&c->value, value);
	Z_ACCESS_FLAGS(c->value) = access_type;
	c->doc_comment = doc_comment;
	c->ce = ce;
	if (Z_TYPE_P(value) == IS_CONSTANT_AST) {
		ce->ce_flags &= ~GEAR_ACC_CONSTANTS_UPDATED;
	}

	if (!gear_hash_add_ptr(&ce->constants_table, name, c)) {
		gear_error_noreturn(ce->type == GEAR_INTERNAL_CLASS ? E_CORE_ERROR : E_COMPILE_ERROR,
			"Cannot redefine class constant %s::%s", ZSTR_VAL(ce->name), ZSTR_VAL(name));
	}

	return SUCCESS;
}
/* }}} */

GEAR_API int gear_declare_class_constant(gear_class_entry *ce, const char *name, size_t name_length, zval *value) /* {{{ */
{
	int ret;

	gear_string *key;

	if (ce->type == GEAR_INTERNAL_CLASS) {
		key = gear_string_init_interned(name, name_length, 1);
	} else {
		key = gear_string_init(name, name_length, 0);
	}
	ret = gear_declare_class_constant_ex(ce, key, value, GEAR_ACC_PUBLIC, NULL);
	gear_string_release(key);
	return ret;
}
/* }}} */

GEAR_API int gear_declare_class_constant_null(gear_class_entry *ce, const char *name, size_t name_length) /* {{{ */
{
	zval constant;

	ZVAL_NULL(&constant);
	return gear_declare_class_constant(ce, name, name_length, &constant);
}
/* }}} */

GEAR_API int gear_declare_class_constant_long(gear_class_entry *ce, const char *name, size_t name_length, gear_long value) /* {{{ */
{
	zval constant;

	ZVAL_LONG(&constant, value);
	return gear_declare_class_constant(ce, name, name_length, &constant);
}
/* }}} */

GEAR_API int gear_declare_class_constant_bool(gear_class_entry *ce, const char *name, size_t name_length, gear_bool value) /* {{{ */
{
	zval constant;

	ZVAL_BOOL(&constant, value);
	return gear_declare_class_constant(ce, name, name_length, &constant);
}
/* }}} */

GEAR_API int gear_declare_class_constant_double(gear_class_entry *ce, const char *name, size_t name_length, double value) /* {{{ */
{
	zval constant;

	ZVAL_DOUBLE(&constant, value);
	return gear_declare_class_constant(ce, name, name_length, &constant);
}
/* }}} */

GEAR_API int gear_declare_class_constant_stringl(gear_class_entry *ce, const char *name, size_t name_length, const char *value, size_t value_length) /* {{{ */
{
	zval constant;

	ZVAL_NEW_STR(&constant, gear_string_init(value, value_length, ce->type & GEAR_INTERNAL_CLASS));
	return gear_declare_class_constant(ce, name, name_length, &constant);
}
/* }}} */

GEAR_API int gear_declare_class_constant_string(gear_class_entry *ce, const char *name, size_t name_length, const char *value) /* {{{ */
{
	return gear_declare_class_constant_stringl(ce, name, name_length, value, strlen(value));
}
/* }}} */

GEAR_API void gear_update_property_ex(gear_class_entry *scope, zval *object, gear_string *name, zval *value) /* {{{ */
{
	zval property;
	gear_class_entry *old_scope = EG(fake_scope);

	EG(fake_scope) = scope;

	if (!Z_OBJ_HT_P(object)->write_property) {
		gear_error_noreturn(E_CORE_ERROR, "Property %s of class %s cannot be updated", ZSTR_VAL(name), ZSTR_VAL(Z_OBJCE_P(object)->name));
	}
	ZVAL_STR(&property, name);
	Z_OBJ_HT_P(object)->write_property(object, &property, value, NULL);

	EG(fake_scope) = old_scope;
}
/* }}} */

GEAR_API void gear_update_property(gear_class_entry *scope, zval *object, const char *name, size_t name_length, zval *value) /* {{{ */
{
	zval property;
	gear_class_entry *old_scope = EG(fake_scope);

	EG(fake_scope) = scope;

	if (!Z_OBJ_HT_P(object)->write_property) {
		gear_error_noreturn(E_CORE_ERROR, "Property %s of class %s cannot be updated", name, ZSTR_VAL(Z_OBJCE_P(object)->name));
	}
	ZVAL_STRINGL(&property, name, name_length);
	Z_OBJ_HT_P(object)->write_property(object, &property, value, NULL);
	zval_ptr_dtor(&property);

	EG(fake_scope) = old_scope;
}
/* }}} */

GEAR_API void gear_update_property_null(gear_class_entry *scope, zval *object, const char *name, size_t name_length) /* {{{ */
{
	zval tmp;

	ZVAL_NULL(&tmp);
	gear_update_property(scope, object, name, name_length, &tmp);
}
/* }}} */

GEAR_API void gear_unset_property(gear_class_entry *scope, zval *object, const char *name, size_t name_length) /* {{{ */
{
	zval property;
	gear_class_entry *old_scope = EG(fake_scope);

	EG(fake_scope) = scope;

	if (!Z_OBJ_HT_P(object)->unset_property) {
		gear_error_noreturn(E_CORE_ERROR, "Property %s of class %s cannot be unset", name, ZSTR_VAL(Z_OBJCE_P(object)->name));
	}
	ZVAL_STRINGL(&property, name, name_length);
	Z_OBJ_HT_P(object)->unset_property(object, &property, 0);
	zval_ptr_dtor(&property);

	EG(fake_scope) = old_scope;
}
/* }}} */

GEAR_API void gear_update_property_bool(gear_class_entry *scope, zval *object, const char *name, size_t name_length, gear_long value) /* {{{ */
{
	zval tmp;

	ZVAL_BOOL(&tmp, value);
	gear_update_property(scope, object, name, name_length, &tmp);
}
/* }}} */

GEAR_API void gear_update_property_long(gear_class_entry *scope, zval *object, const char *name, size_t name_length, gear_long value) /* {{{ */
{
	zval tmp;

	ZVAL_LONG(&tmp, value);
	gear_update_property(scope, object, name, name_length, &tmp);
}
/* }}} */

GEAR_API void gear_update_property_double(gear_class_entry *scope, zval *object, const char *name, size_t name_length, double value) /* {{{ */
{
	zval tmp;

	ZVAL_DOUBLE(&tmp, value);
	gear_update_property(scope, object, name, name_length, &tmp);
}
/* }}} */

GEAR_API void gear_update_property_str(gear_class_entry *scope, zval *object, const char *name, size_t name_length, gear_string *value) /* {{{ */
{
	zval tmp;

	ZVAL_STR(&tmp, value);
	gear_update_property(scope, object, name, name_length, &tmp);
}
/* }}} */

GEAR_API void gear_update_property_string(gear_class_entry *scope, zval *object, const char *name, size_t name_length, const char *value) /* {{{ */
{
	zval tmp;

	ZVAL_STRING(&tmp, value);
	Z_SET_REFCOUNT(tmp, 0);
	gear_update_property(scope, object, name, name_length, &tmp);
}
/* }}} */

GEAR_API void gear_update_property_stringl(gear_class_entry *scope, zval *object, const char *name, size_t name_length, const char *value, size_t value_len) /* {{{ */
{
	zval tmp;

	ZVAL_STRINGL(&tmp, value, value_len);
	Z_SET_REFCOUNT(tmp, 0);
	gear_update_property(scope, object, name, name_length, &tmp);
}
/* }}} */

GEAR_API int gear_update_static_property_ex(gear_class_entry *scope, gear_string *name, zval *value) /* {{{ */
{
	zval *property;
	gear_class_entry *old_scope = EG(fake_scope);

	EG(fake_scope) = scope;
	property = gear_std_get_static_property(scope, name, 0);
	EG(fake_scope) = old_scope;

	if (!property) {
		return FAILURE;
	}

	if (property != value) {
		zval garbage;
		ZVAL_DEREF(property);
		ZVAL_DEREF(value);
		ZVAL_COPY_VALUE(&garbage, property);
		ZVAL_COPY(property, value);
		zval_ptr_dtor(&garbage);
	}
	return SUCCESS;
}
/* }}} */

GEAR_API int gear_update_static_property(gear_class_entry *scope, const char *name, size_t name_length, zval *value) /* {{{ */
{
	gear_string *key = gear_string_init(name, name_length, 0);
	int retval = gear_update_static_property_ex(scope, key, value);
	gear_string_efree(key);
	return retval;
}
/* }}} */

GEAR_API int gear_update_static_property_null(gear_class_entry *scope, const char *name, size_t name_length) /* {{{ */
{
	zval tmp;

	ZVAL_NULL(&tmp);
	return gear_update_static_property(scope, name, name_length, &tmp);
}
/* }}} */

GEAR_API int gear_update_static_property_bool(gear_class_entry *scope, const char *name, size_t name_length, gear_long value) /* {{{ */
{
	zval tmp;

	ZVAL_BOOL(&tmp, value);
	return gear_update_static_property(scope, name, name_length, &tmp);
}
/* }}} */

GEAR_API int gear_update_static_property_long(gear_class_entry *scope, const char *name, size_t name_length, gear_long value) /* {{{ */
{
	zval tmp;

	ZVAL_LONG(&tmp, value);
	return gear_update_static_property(scope, name, name_length, &tmp);
}
/* }}} */

GEAR_API int gear_update_static_property_double(gear_class_entry *scope, const char *name, size_t name_length, double value) /* {{{ */
{
	zval tmp;

	ZVAL_DOUBLE(&tmp, value);
	return gear_update_static_property(scope, name, name_length, &tmp);
}
/* }}} */

GEAR_API int gear_update_static_property_string(gear_class_entry *scope, const char *name, size_t name_length, const char *value) /* {{{ */
{
	zval tmp;

	ZVAL_STRING(&tmp, value);
	Z_SET_REFCOUNT(tmp, 0);
	return gear_update_static_property(scope, name, name_length, &tmp);
}
/* }}} */

GEAR_API int gear_update_static_property_stringl(gear_class_entry *scope, const char *name, size_t name_length, const char *value, size_t value_len) /* {{{ */
{
	zval tmp;

	ZVAL_STRINGL(&tmp, value, value_len);
	Z_SET_REFCOUNT(tmp, 0);
	return gear_update_static_property(scope, name, name_length, &tmp);
}
/* }}} */

GEAR_API zval *gear_read_property_ex(gear_class_entry *scope, zval *object, gear_string *name, gear_bool silent, zval *rv) /* {{{ */
{
	zval property, *value;
	gear_class_entry *old_scope = EG(fake_scope);

	EG(fake_scope) = scope;

	if (!Z_OBJ_HT_P(object)->read_property) {
		gear_error_noreturn(E_CORE_ERROR, "Property %s of class %s cannot be read", ZSTR_VAL(name), ZSTR_VAL(Z_OBJCE_P(object)->name));
	}

	ZVAL_STR(&property, name);
	value = Z_OBJ_HT_P(object)->read_property(object, &property, silent?BP_VAR_IS:BP_VAR_R, NULL, rv);

	EG(fake_scope) = old_scope;
	return value;
}
/* }}} */

GEAR_API zval *gear_read_property(gear_class_entry *scope, zval *object, const char *name, size_t name_length, gear_bool silent, zval *rv) /* {{{ */
{
	zval *value;
	gear_string *str;

	str = gear_string_init(name, name_length, 0);
	value = gear_read_property_ex(scope, object, str, silent, rv);
	gear_string_release_ex(str, 0);
	return value;
}
/* }}} */

GEAR_API zval *gear_read_static_property_ex(gear_class_entry *scope, gear_string *name, gear_bool silent) /* {{{ */
{
	zval *property;
	gear_class_entry *old_scope = EG(fake_scope);

	EG(fake_scope) = scope;
	property = gear_std_get_static_property(scope, name, silent);
	EG(fake_scope) = old_scope;

	return property;
}
/* }}} */

GEAR_API zval *gear_read_static_property(gear_class_entry *scope, const char *name, size_t name_length, gear_bool silent) /* {{{ */
{
	gear_string *key = gear_string_init(name, name_length, 0);
	zval *property = gear_read_static_property_ex(scope, key, silent);
	gear_string_efree(key);
	return property;
}
/* }}} */

GEAR_API void gear_save_error_handling(gear_error_handling *current) /* {{{ */
{
	current->handling = EG(error_handling);
	current->exception = EG(exception_class);
	ZVAL_COPY(&current->user_handler, &EG(user_error_handler));
}
/* }}} */

GEAR_API void gear_replace_error_handling(gear_error_handling_t error_handling, gear_class_entry *exception_class, gear_error_handling *current) /* {{{ */
{
	if (current) {
		gear_save_error_handling(current);
		if (error_handling != EH_NORMAL && Z_TYPE(EG(user_error_handler)) != IS_UNDEF) {
			zval_ptr_dtor(&EG(user_error_handler));
			ZVAL_UNDEF(&EG(user_error_handler));
		}
	}
	EG(error_handling) = error_handling;
	EG(exception_class) = error_handling == EH_THROW ? exception_class : NULL;
}
/* }}} */

static int same_zval(zval *zv1, zval *zv2)  /* {{{ */
{
	if (Z_TYPE_P(zv1) != Z_TYPE_P(zv2)) {
		return 0;
	}
	switch (Z_TYPE_P(zv1)) {
		case IS_UNDEF:
		case IS_NULL:
		case IS_FALSE:
		case IS_TRUE:
			return 1;
		case IS_LONG:
			return Z_LVAL_P(zv1) == Z_LVAL_P(zv2);
		case IS_DOUBLE:
			return Z_LVAL_P(zv1) == Z_LVAL_P(zv2);
		case IS_STRING:
		case IS_ARRAY:
		case IS_OBJECT:
		case IS_RESOURCE:
			return Z_COUNTED_P(zv1) == Z_COUNTED_P(zv2);
		default:
			return 0;
	}
}
/* }}} */

GEAR_API void gear_restore_error_handling(gear_error_handling *saved) /* {{{ */
{
	EG(error_handling) = saved->handling;
	EG(exception_class) = saved->handling == EH_THROW ? saved->exception : NULL;
	if (Z_TYPE(saved->user_handler) != IS_UNDEF
		&& !same_zval(&saved->user_handler, &EG(user_error_handler))) {
		zval_ptr_dtor(&EG(user_error_handler));
		ZVAL_COPY_VALUE(&EG(user_error_handler), &saved->user_handler);
	} else if (Z_TYPE(saved->user_handler)) {
		zval_ptr_dtor(&saved->user_handler);
	}
	ZVAL_UNDEF(&saved->user_handler);
}
/* }}} */

GEAR_API gear_string* gear_find_alias_name(gear_class_entry *ce, gear_string *name) /* {{{ */
{
	gear_trait_alias *alias, **alias_ptr;

	if ((alias_ptr = ce->trait_aliases)) {
		alias = *alias_ptr;
		while (alias) {
			if (alias->alias && gear_string_equals_ci(alias->alias, name)) {
				return alias->alias;
			}
			alias_ptr++;
			alias = *alias_ptr;
		}
	}

	return name;
}
/* }}} */

GEAR_API gear_string *gear_resolve_method_name(gear_class_entry *ce, gear_function *f) /* {{{ */
{
	gear_function *func;
	HashTable *function_table;
	gear_string *name;

	if (f->common.type != GEAR_USER_FUNCTION ||
	    (f->op_array.refcount && *(f->op_array.refcount) < 2) ||
	    !f->common.scope ||
	    !f->common.scope->trait_aliases) {
		return f->common.function_name;
	}

	function_table = &ce->function_table;
	GEAR_HASH_FOREACH_STR_KEY_PTR(function_table, name, func) {
		if (func == f) {
			if (!name) {
				return f->common.function_name;
			}
			if (ZSTR_LEN(name) == ZSTR_LEN(f->common.function_name) &&
			    !strncasecmp(ZSTR_VAL(name), ZSTR_VAL(f->common.function_name), ZSTR_LEN(f->common.function_name))) {
				return f->common.function_name;
			}
			return gear_find_alias_name(f->common.scope, name);
		}
	} GEAR_HASH_FOREACH_END();
	return f->common.function_name;
}
/* }}} */

GEAR_API const char *gear_get_object_type(const gear_class_entry *ce) /* {{{ */
{
	if(ce->ce_flags & GEAR_ACC_TRAIT) {
		return "trait";
	} else if (ce->ce_flags & GEAR_ACC_INTERFACE) {
		return "interface";
	} else {
		return "class";
	}
}
/* }}} */

GEAR_API gear_bool gear_is_iterable(zval *iterable) /* {{{ */
{
	switch (Z_TYPE_P(iterable)) {
		case IS_ARRAY:
			return 1;
		case IS_OBJECT:
			return instanceof_function(Z_OBJCE_P(iterable), gear_ce_traversable);
		default:
			return 0;
	}
}
/* }}} */

GEAR_API gear_bool gear_is_countable(zval *countable) /* {{{ */
{
	switch (Z_TYPE_P(countable)) {
		case IS_ARRAY:
			return 1;
		case IS_OBJECT:
			if (Z_OBJ_HT_P(countable)->count_elements) {
				return 1;
			}

			return instanceof_function(Z_OBJCE_P(countable), gear_ce_countable);
		default:
			return 0;
	}
}
/* }}} */

