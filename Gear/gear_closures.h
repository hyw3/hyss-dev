/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_CLOSURES_H
#define GEAR_CLOSURES_H

BEGIN_EXTERN_C()

/* This macro depends on gear_closure structure layout */
#define GEAR_CLOSURE_OBJECT(op_array) \
	((gear_object*)((char*)(op_array) - sizeof(gear_object)))

void gear_register_closure_ce(void);
void gear_closure_bind_var(zval *closure_zv, gear_string *var_name, zval *var);
void gear_closure_bind_var_ex(zval *closure_zv, uint32_t offset, zval *val);

extern GEAR_API gear_class_entry *gear_ce_closure;

GEAR_API void gear_create_closure(zval *res, gear_function *op_array, gear_class_entry *scope, gear_class_entry *called_scope, zval *this_ptr);
GEAR_API void gear_create_fake_closure(zval *res, gear_function *op_array, gear_class_entry *scope, gear_class_entry *called_scope, zval *this_ptr);
GEAR_API gear_function *gear_get_closure_invoke_method(gear_object *obj);
GEAR_API const gear_function *gear_get_closure_method_def(zval *obj);
GEAR_API zval* gear_get_closure_this_ptr(zval *obj);

END_EXTERN_C()

#endif

