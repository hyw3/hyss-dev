/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_AST_H
#define GEAR_AST_H

#include "gear.h"

#ifndef GEAR_AST_SPEC
# define GEAR_AST_SPEC 1
#endif

#define GEAR_AST_SPECIAL_SHIFT      6
#define GEAR_AST_IS_LIST_SHIFT      7
#define GEAR_AST_NUM_CHILDREN_SHIFT 8

enum _gear_ast_kind {
	/* special nodes */
	GEAR_AST_ZVAL = 1 << GEAR_AST_SPECIAL_SHIFT,
	GEAR_AST_CONSTANT,
	GEAR_AST_ZNODE,

	/* declaration nodes */
	GEAR_AST_FUNC_DECL,
	GEAR_AST_CLOSURE,
	GEAR_AST_METHOD,
	GEAR_AST_CLASS,

	/* list nodes */
	GEAR_AST_ARG_LIST = 1 << GEAR_AST_IS_LIST_SHIFT,
	GEAR_AST_ARRAY,
	GEAR_AST_ENCAPS_LIST,
	GEAR_AST_EXPR_LIST,
	GEAR_AST_STMT_LIST,
	GEAR_AST_IF,
	GEAR_AST_SWITCH_LIST,
	GEAR_AST_CATCH_LIST,
	GEAR_AST_PARAM_LIST,
	GEAR_AST_CLOSURE_USES,
	GEAR_AST_PROP_DECL,
	GEAR_AST_CONST_DECL,
	GEAR_AST_CLASS_CONST_DECL,
	GEAR_AST_NAME_LIST,
	GEAR_AST_TRAIT_ADAPTATIONS,
	GEAR_AST_USE,

	/* 0 child nodes */
	GEAR_AST_MAGIC_CONST = 0 << GEAR_AST_NUM_CHILDREN_SHIFT,
	GEAR_AST_TYPE,
	GEAR_AST_CONSTANT_CLASS,

	/* 1 child node */
	GEAR_AST_VAR = 1 << GEAR_AST_NUM_CHILDREN_SHIFT,
	GEAR_AST_CONST,
	GEAR_AST_UNPACK,
	GEAR_AST_UNARY_PLUS,
	GEAR_AST_UNARY_MINUS,
	GEAR_AST_CAST,
	GEAR_AST_EMPTY,
	GEAR_AST_ISSET,
	GEAR_AST_SILENCE,
	GEAR_AST_SHELL_EXEC,
	GEAR_AST_CLONE,
	GEAR_AST_EXIT,
	GEAR_AST_PRINT,
	GEAR_AST_INCLUDE_OR_EVAL,
	GEAR_AST_UNARY_OP,
	GEAR_AST_PRE_INC,
	GEAR_AST_PRE_DEC,
	GEAR_AST_POST_INC,
	GEAR_AST_POST_DEC,
	GEAR_AST_YIELD_FROM,

	GEAR_AST_GLOBAL,
	GEAR_AST_UNSET,
	GEAR_AST_RETURN,
	GEAR_AST_LABEL,
	GEAR_AST_REF,
	GEAR_AST_HALT_COMPILER,
	GEAR_AST_ECHO,
	GEAR_AST_THROW,
	GEAR_AST_GOTO,
	GEAR_AST_BREAK,
	GEAR_AST_CONTINUE,

	/* 2 child nodes */
	GEAR_AST_DIM = 2 << GEAR_AST_NUM_CHILDREN_SHIFT,
	GEAR_AST_PROP,
	GEAR_AST_STATIC_PROP,
	GEAR_AST_CALL,
	GEAR_AST_CLASS_CONST,
	GEAR_AST_ASSIGN,
	GEAR_AST_ASSIGN_REF,
	GEAR_AST_ASSIGN_OP,
	GEAR_AST_BINARY_OP,
	GEAR_AST_GREATER,
	GEAR_AST_GREATER_EQUAL,
	GEAR_AST_AND,
	GEAR_AST_OR,
	GEAR_AST_ARRAY_ELEM,
	GEAR_AST_NEW,
	GEAR_AST_INSTANCEOF,
	GEAR_AST_YIELD,
	GEAR_AST_COALESCE,

	GEAR_AST_STATIC,
	GEAR_AST_WHILE,
	GEAR_AST_DO_WHILE,
	GEAR_AST_IF_ELEM,
	GEAR_AST_SWITCH,
	GEAR_AST_SWITCH_CASE,
	GEAR_AST_DECLARE,
	GEAR_AST_USE_TRAIT,
	GEAR_AST_TRAIT_PRECEDENCE,
	GEAR_AST_METHOD_REFERENCE,
	GEAR_AST_NAMESPACE,
	GEAR_AST_USE_ELEM,
	GEAR_AST_TRAIT_ALIAS,
	GEAR_AST_GROUP_USE,

	/* 3 child nodes */
	GEAR_AST_METHOD_CALL = 3 << GEAR_AST_NUM_CHILDREN_SHIFT,
	GEAR_AST_STATIC_CALL,
	GEAR_AST_CONDITIONAL,

	GEAR_AST_TRY,
	GEAR_AST_CATCH,
	GEAR_AST_PARAM,
	GEAR_AST_PROP_ELEM,
	GEAR_AST_CONST_ELEM,

	/* 4 child nodes */
	GEAR_AST_FOR = 4 << GEAR_AST_NUM_CHILDREN_SHIFT,
	GEAR_AST_FOREACH,
};

typedef uint16_t gear_ast_kind;
typedef uint16_t gear_ast_attr;

struct _gear_ast {
	gear_ast_kind kind; /* Type of the node (GEAR_AST_* enum constant) */
	gear_ast_attr attr; /* Additional attribute, use depending on node type */
	uint32_t lineno;    /* Line number */
	gear_ast *child[1]; /* Array of children (using struct hack) */
};

/* Same as gear_ast, but with children count, which is updated dynamically */
typedef struct _gear_ast_list {
	gear_ast_kind kind;
	gear_ast_attr attr;
	uint32_t lineno;
	uint32_t children;
	gear_ast *child[1];
} gear_ast_list;

/* Lineno is stored in val.u2.lineno */
typedef struct _gear_ast_zval {
	gear_ast_kind kind;
	gear_ast_attr attr;
	zval val;
} gear_ast_zval;

/* Separate structure for function and class declaration, as they need extra information. */
typedef struct _gear_ast_decl {
	gear_ast_kind kind;
	gear_ast_attr attr; /* Unused - for structure compatibility */
	uint32_t start_lineno;
	uint32_t end_lineno;
	uint32_t flags;
	unsigned char *lex_pos;
	gear_string *doc_comment;
	gear_string *name;
	gear_ast *child[4];
} gear_ast_decl;

typedef void (*gear_ast_process_t)(gear_ast *ast);
extern GEAR_API gear_ast_process_t gear_ast_process;

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval_with_lineno(zval *zv, uint32_t lineno);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval_ex(zval *zv, gear_ast_attr attr);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval(zval *zv);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval_from_str(gear_string *str);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_zval_from_long(gear_long lval);

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_constant(gear_string *name, gear_ast_attr attr);

#if GEAR_AST_SPEC
# define GEAR_AST_SPEC_CALL(name, ...) \
	GEAR_EXPAND_VA(GEAR_AST_SPEC_CALL_(name, __VA_ARGS__, _4, _3, _2, _1, _0)(__VA_ARGS__))
# define GEAR_AST_SPEC_CALL_(name, _, _4, _3, _2, _1, suffix, ...) \
	name ## suffix
# define GEAR_AST_SPEC_CALL_EX(name, ...) \
	GEAR_EXPAND_VA(GEAR_AST_SPEC_CALL_EX_(name, __VA_ARGS__, _4, _3, _2, _1, _0)(__VA_ARGS__))
# define GEAR_AST_SPEC_CALL_EX_(name, _, _5, _4, _3, _2, _1, suffix, ...) \
	name ## suffix

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_0(gear_ast_kind kind);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_1(gear_ast_kind kind, gear_ast *child);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_2(gear_ast_kind kind, gear_ast *child1, gear_ast *child2);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_3(gear_ast_kind kind, gear_ast *child1, gear_ast *child2, gear_ast *child3);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_4(gear_ast_kind kind, gear_ast *child1, gear_ast *child2, gear_ast *child3, gear_ast *child4);

static gear_always_inline gear_ast * gear_ast_create_ex_0(gear_ast_kind kind, gear_ast_attr attr) {
	gear_ast *ast = gear_ast_create_0(kind);
	ast->attr = attr;
	return ast;
}
static gear_always_inline gear_ast * gear_ast_create_ex_1(gear_ast_kind kind, gear_ast_attr attr, gear_ast *child) {
	gear_ast *ast = gear_ast_create_1(kind, child);
	ast->attr = attr;
	return ast;
}
static gear_always_inline gear_ast * gear_ast_create_ex_2(gear_ast_kind kind, gear_ast_attr attr, gear_ast *child1, gear_ast *child2) {
	gear_ast *ast = gear_ast_create_2(kind, child1, child2);
	ast->attr = attr;
	return ast;
}
static gear_always_inline gear_ast * gear_ast_create_ex_3(gear_ast_kind kind, gear_ast_attr attr, gear_ast *child1, gear_ast *child2, gear_ast *child3) {
	gear_ast *ast = gear_ast_create_3(kind, child1, child2, child3);
	ast->attr = attr;
	return ast;
}
static gear_always_inline gear_ast * gear_ast_create_ex_4(gear_ast_kind kind, gear_ast_attr attr, gear_ast *child1, gear_ast *child2, gear_ast *child3, gear_ast *child4) {
	gear_ast *ast = gear_ast_create_4(kind, child1, child2, child3, child4);
	ast->attr = attr;
	return ast;
}

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_list_0(gear_ast_kind kind);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_list_1(gear_ast_kind kind, gear_ast *child);
GEAR_API gear_ast * GEAR_FASTCALL gear_ast_create_list_2(gear_ast_kind kind, gear_ast *child1, gear_ast *child2);

# define gear_ast_create(...) \
	GEAR_AST_SPEC_CALL(gear_ast_create, __VA_ARGS__)
# define gear_ast_create_ex(...) \
	GEAR_AST_SPEC_CALL_EX(gear_ast_create_ex, __VA_ARGS__)
# define gear_ast_create_list(init_children, ...) \
	GEAR_AST_SPEC_CALL(gear_ast_create_list, __VA_ARGS__)

#else
GEAR_API gear_ast *gear_ast_create(gear_ast_kind kind, ...);
GEAR_API gear_ast *gear_ast_create_ex(gear_ast_kind kind, gear_ast_attr attr, ...);
GEAR_API gear_ast *gear_ast_create_list(uint32_t init_children, gear_ast_kind kind, ...);
#endif

GEAR_API gear_ast * GEAR_FASTCALL gear_ast_list_add(gear_ast *list, gear_ast *op);

GEAR_API gear_ast *gear_ast_create_decl(
	gear_ast_kind kind, uint32_t flags, uint32_t start_lineno, gear_string *doc_comment,
	gear_string *name, gear_ast *child0, gear_ast *child1, gear_ast *child2, gear_ast *child3
);

GEAR_API int GEAR_FASTCALL gear_ast_evaluate(zval *result, gear_ast *ast, gear_class_entry *scope);
GEAR_API gear_string *gear_ast_export(const char *prefix, gear_ast *ast, const char *suffix);

GEAR_API gear_ast_ref * GEAR_FASTCALL gear_ast_copy(gear_ast *ast);
GEAR_API void GEAR_FASTCALL gear_ast_destroy(gear_ast *ast);
GEAR_API void GEAR_FASTCALL gear_ast_ref_destroy(gear_ast_ref *ast);

typedef void (*gear_ast_apply_func)(gear_ast **ast_ptr);
GEAR_API void gear_ast_apply(gear_ast *ast, gear_ast_apply_func fn);

static gear_always_inline gear_bool gear_ast_is_list(gear_ast *ast) {
	return (ast->kind >> GEAR_AST_IS_LIST_SHIFT) & 1;
}
static gear_always_inline gear_ast_list *gear_ast_get_list(gear_ast *ast) {
	GEAR_ASSERT(gear_ast_is_list(ast));
	return (gear_ast_list *) ast;
}

static gear_always_inline zval *gear_ast_get_zval(gear_ast *ast) {
	GEAR_ASSERT(ast->kind == GEAR_AST_ZVAL);
	return &((gear_ast_zval *) ast)->val;
}
static gear_always_inline gear_string *gear_ast_get_str(gear_ast *ast) {
	zval *zv = gear_ast_get_zval(ast);
	GEAR_ASSERT(Z_TYPE_P(zv) == IS_STRING);
	return Z_STR_P(zv);
}

static gear_always_inline gear_string *gear_ast_get_constant_name(gear_ast *ast) {
	GEAR_ASSERT(ast->kind == GEAR_AST_CONSTANT);
	GEAR_ASSERT(Z_TYPE(((gear_ast_zval *) ast)->val) == IS_STRING);
	return Z_STR(((gear_ast_zval *) ast)->val);
}

static gear_always_inline uint32_t gear_ast_get_num_children(gear_ast *ast) {
	GEAR_ASSERT(!gear_ast_is_list(ast));
	return ast->kind >> GEAR_AST_NUM_CHILDREN_SHIFT;
}
static gear_always_inline uint32_t gear_ast_get_lineno(gear_ast *ast) {
	if (ast->kind == GEAR_AST_ZVAL) {
		zval *zv = gear_ast_get_zval(ast);
		return Z_LINENO_P(zv);
	} else {
		return ast->lineno;
	}
}

static gear_always_inline gear_ast *gear_ast_create_binary_op(uint32_t opcode, gear_ast *op0, gear_ast *op1) {
	return gear_ast_create_ex(GEAR_AST_BINARY_OP, opcode, op0, op1);
}
static gear_always_inline gear_ast *gear_ast_create_assign_op(uint32_t opcode, gear_ast *op0, gear_ast *op1) {
	return gear_ast_create_ex(GEAR_AST_ASSIGN_OP, opcode, op0, op1);
}
static gear_always_inline gear_ast *gear_ast_create_cast(uint32_t type, gear_ast *op0) {
	return gear_ast_create_ex(GEAR_AST_CAST, type, op0);
}
static gear_always_inline gear_ast *gear_ast_list_rtrim(gear_ast *ast) {
	gear_ast_list *list = gear_ast_get_list(ast);
	if (list->children && list->child[list->children - 1] == NULL) {
		list->children--;
	}
	return ast;
}
#endif

