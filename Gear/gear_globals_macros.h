/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_GLOBALS_MACROS_H
#define GEAR_GLOBALS_MACROS_H

typedef struct _gear_compiler_globals gear_compiler_globals;
typedef struct _gear_executor_globals gear_executor_globals;
typedef struct _gear_hyss_scanner_globals gear_hyss_scanner_globals;
typedef struct _gear_ics_scanner_globals gear_ics_scanner_globals;

BEGIN_EXTERN_C()

/* Compiler */
#ifdef ZTS
# define CG(v) GEAR_PBCG(compiler_globals_id, gear_compiler_globals *, v)
#else
# define CG(v) (compiler_globals.v)
extern GEAR_API struct _gear_compiler_globals compiler_globals;
#endif
GEAR_API int gearparse(void);


/* Executor */
#ifdef ZTS
# define EG(v) GEAR_PBCG(executor_globals_id, gear_executor_globals *, v)
#else
# define EG(v) (executor_globals.v)
extern GEAR_API gear_executor_globals executor_globals;
#endif

/* Language Scanner */
#ifdef ZTS
# define LANG_SCNG(v) GEAR_PBCG(language_scanner_globals_id, gear_hyss_scanner_globals *, v)
extern GEAR_API ts_rsrc_id language_scanner_globals_id;
#else
# define LANG_SCNG(v) (language_scanner_globals.v)
extern GEAR_API gear_hyss_scanner_globals language_scanner_globals;
#endif


/* ICS Scanner */
#ifdef ZTS
# define ICS_SCNG(v) GEAR_PBCG(ics_scanner_globals_id, gear_ics_scanner_globals *, v)
extern GEAR_API ts_rsrc_id ics_scanner_globals_id;
#else
# define ICS_SCNG(v) (ics_scanner_globals.v)
extern GEAR_API gear_ics_scanner_globals ics_scanner_globals;
#endif

END_EXTERN_C()

#endif /* GEAR_GLOBALS_MACROS_H */

