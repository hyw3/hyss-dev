/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_API.h"
#include "gear_interfaces.h"
#include "gear_exceptions.h"
#include "gear_generators.h"
#include "gear_closures.h"

GEAR_API gear_class_entry *gear_ce_generator;
GEAR_API gear_class_entry *gear_ce_ClosedGeneratorException;
static gear_object_handlers gear_generator_handlers;

static gear_object *gear_generator_create(gear_class_entry *class_type);

GEAR_API void gear_generator_restore_call_stack(gear_generator *generator) /* {{{ */
{
	gear_execute_data *call, *new_call, *prev_call = NULL;

	call = generator->frozen_call_stack;
	do {
		new_call = gear_vm_stack_push_call_frame(
			(GEAR_CALL_INFO(call) & ~GEAR_CALL_ALLOCATED),
			call->func,
			GEAR_CALL_NUM_ARGS(call),
			(Z_TYPE(call->This) == IS_UNDEF) ?
				(gear_class_entry*)Z_OBJ(call->This) : NULL,
			(Z_TYPE(call->This) != IS_UNDEF) ?
				Z_OBJ(call->This) : NULL);
		memcpy(((zval*)new_call) + GEAR_CALL_FRAME_SLOT, ((zval*)call) + GEAR_CALL_FRAME_SLOT, GEAR_CALL_NUM_ARGS(call) * sizeof(zval));
		new_call->prev_execute_data = prev_call;
		prev_call = new_call;

		call = call->prev_execute_data;
	} while (call);
	generator->execute_data->call = prev_call;
	efree(generator->frozen_call_stack);
	generator->frozen_call_stack = NULL;
}
/* }}} */

GEAR_API gear_execute_data* gear_generator_freeze_call_stack(gear_execute_data *execute_data) /* {{{ */
{
	size_t used_stack;
	gear_execute_data *call, *new_call, *prev_call = NULL;
	zval *stack;

	/* calculate required stack size */
	used_stack = 0;
	call = EX(call);
	do {
		used_stack += GEAR_CALL_FRAME_SLOT + GEAR_CALL_NUM_ARGS(call);
		call = call->prev_execute_data;
	} while (call);

	stack = emalloc(used_stack * sizeof(zval));

	/* save stack, linking frames in reverse order */
	call = EX(call);
	do {
		size_t frame_size = GEAR_CALL_FRAME_SLOT + GEAR_CALL_NUM_ARGS(call);

		new_call = (gear_execute_data*)(stack + used_stack - frame_size);
		memcpy(new_call, call, frame_size * sizeof(zval));
		used_stack -= frame_size;
		new_call->prev_execute_data = prev_call;
		prev_call = new_call;

		new_call = call->prev_execute_data;
		gear_vm_stack_free_call_frame(call);
		call = new_call;
	} while (call);

	execute_data->call = NULL;
	GEAR_ASSERT(prev_call == (gear_execute_data*)stack);

	return prev_call;
}
/* }}} */

static void gear_generator_cleanup_unfinished_execution(
		gear_generator *generator, uint32_t catch_op_num) /* {{{ */
{
	gear_execute_data *execute_data = generator->execute_data;

	if (execute_data->opline != execute_data->func->op_array.opcodes) {
		/* -1 required because we want the last run opcode, not the next to-be-run one. */
		uint32_t op_num = execute_data->opline - execute_data->func->op_array.opcodes - 1;

		if (UNEXPECTED(generator->frozen_call_stack)) {
			gear_generator_restore_call_stack(generator);
		}
		gear_cleanup_unfinished_execution(execute_data, op_num, catch_op_num);
	}
}
/* }}} */

GEAR_API void gear_generator_close(gear_generator *generator, gear_bool finished_execution) /* {{{ */
{
	if (EXPECTED(generator->execute_data)) {
		gear_execute_data *execute_data = generator->execute_data;

		if (EX_CALL_INFO() & GEAR_CALL_HAS_SYMBOL_TABLE) {
			gear_clean_and_cache_symbol_table(execute_data->symbol_table);
		}
		/* always free the CV's, in the symtable are only not-free'd IS_INDIRECT's */
		gear_free_compiled_variables(execute_data);

		if (EX_CALL_INFO() & GEAR_CALL_RELEASE_THIS) {
			OBJ_RELEASE(Z_OBJ(execute_data->This));
		}

		/* A fatal error / die occurred during the generator execution.
		 * Trying to clean up the stack may not be safe in this case. */
		if (UNEXPECTED(CG(unclean_shutdown))) {
			generator->execute_data = NULL;
			return;
		}

		gear_vm_stack_free_extra_args(generator->execute_data);

		/* Some cleanups are only necessary if the generator was closed
		 * before it could finish execution (reach a return statement). */
		if (UNEXPECTED(!finished_execution)) {
			gear_generator_cleanup_unfinished_execution(generator, 0);
		}

		/* Free closure object */
		if (EX_CALL_INFO() & GEAR_CALL_CLOSURE) {
			OBJ_RELEASE(GEAR_CLOSURE_OBJECT(EX(func)));
		}

		/* Free GC buffer. GC for closed generators doesn't need an allocated buffer */
		if (generator->gc_buffer) {
			efree(generator->gc_buffer);
			generator->gc_buffer = NULL;
		}

		efree(generator->execute_data);
		generator->execute_data = NULL;
	}
}
/* }}} */

static gear_generator *gear_generator_get_child(gear_generator_node *node, gear_generator *leaf);

static void gear_generator_dtor_storage(gear_object *object) /* {{{ */
{
	gear_generator *generator = (gear_generator*) object;
	gear_execute_data *ex = generator->execute_data;
	uint32_t op_num, finally_op_num, finally_op_end;
	int i;

	/* leave yield from mode to properly allow finally execution */
	if (UNEXPECTED(Z_TYPE(generator->values) != IS_UNDEF)) {
		zval_ptr_dtor(&generator->values);
		ZVAL_UNDEF(&generator->values);
	}

	if (EXPECTED(generator->node.children == 0)) {
		gear_generator *root = generator->node.ptr.root, *next;
		while (UNEXPECTED(root != generator)) {
			next = gear_generator_get_child(&root->node, generator);
			generator->node.ptr.root = next;
			next->node.parent = NULL;
			OBJ_RELEASE(&root->std);
			root = next;
		}
	}

	if (EXPECTED(!ex) || EXPECTED(!(ex->func->op_array.fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK))
			|| CG(unclean_shutdown)) {
		return;
	}

	/* -1 required because we want the last run opcode, not the
	 * next to-be-run one. */
	op_num = ex->opline - ex->func->op_array.opcodes - 1;

	/* Find next finally block */
	finally_op_num = 0;
	finally_op_end = 0;
	for (i = 0; i < ex->func->op_array.last_try_catch; i++) {
		gear_try_catch_element *try_catch = &ex->func->op_array.try_catch_array[i];

		if (op_num < try_catch->try_op) {
			break;
		}

		if (op_num < try_catch->finally_op) {
			finally_op_num = try_catch->finally_op;
			finally_op_end = try_catch->finally_end;
		}
	}

	/* If a finally block was found we jump directly to it and
	 * resume the generator. */
	if (finally_op_num) {
		zval *fast_call;

		gear_generator_cleanup_unfinished_execution(generator, finally_op_num);

		fast_call = GEAR_CALL_VAR(ex, ex->func->op_array.opcodes[finally_op_end].op1.var);
		Z_OBJ_P(fast_call) = EG(exception);
		EG(exception) = NULL;
		Z_OPLINE_NUM_P(fast_call) = (uint32_t)-1;

		ex->opline = &ex->func->op_array.opcodes[finally_op_num];
		generator->flags |= GEAR_GENERATOR_FORCED_CLOSE;
		gear_generator_resume(generator);
	}
}
/* }}} */

static void gear_generator_free_storage(gear_object *object) /* {{{ */
{
	gear_generator *generator = (gear_generator*) object;

	gear_generator_close(generator, 0);

	/* we can't immediately free them in gear_generator_close() else yield from won't be able to fetch it */
	zval_ptr_dtor(&generator->value);
	zval_ptr_dtor(&generator->key);

	if (EXPECTED(!Z_ISUNDEF(generator->retval))) {
		zval_ptr_dtor(&generator->retval);
	}

	if (UNEXPECTED(generator->node.children > 1)) {
		gear_hash_destroy(generator->node.child.ht);
		efree(generator->node.child.ht);
	}

	gear_object_std_dtor(&generator->std);

	if (generator->iterator) {
		gear_iterator_dtor(generator->iterator);
	}
}
/* }}} */

static uint32_t calc_gc_buffer_size(gear_generator *generator) /* {{{ */
{
	uint32_t size = 4; /* value, key, retval, values */
	if (generator->execute_data) {
		gear_execute_data *execute_data = generator->execute_data;
		gear_op_array *op_array = &EX(func)->op_array;

		/* Compiled variables */
		if (!(EX_CALL_INFO() & GEAR_CALL_HAS_SYMBOL_TABLE)) {
			size += op_array->last_var;
		}
		/* Extra args */
		if (EX_CALL_INFO() & GEAR_CALL_FREE_EXTRA_ARGS) {
			size += EX_NUM_ARGS() - op_array->num_args;
		}
		size += Z_TYPE(execute_data->This) == IS_OBJECT; /* $this */
		size += (EX_CALL_INFO() & GEAR_CALL_CLOSURE) != 0; /* Closure object */

		/* Live vars */
		if (execute_data->opline != op_array->opcodes) {
			/* -1 required because we want the last run opcode, not the next to-be-run one. */
			uint32_t i, op_num = execute_data->opline - op_array->opcodes - 1;
			for (i = 0; i < op_array->last_live_range; i++) {
				const gear_live_range *range = &op_array->live_range[i];
				if (range->start > op_num) {
					/* Further ranges will not be relevant... */
					break;
				} else if (op_num < range->end) {
					/* LIVE_ROPE and LIVE_SILENCE not relevant for GC */
					uint32_t kind = range->var & GEAR_LIVE_MASK;
					if (kind == GEAR_LIVE_TMPVAR || kind == GEAR_LIVE_LOOP) {
						size++;
					}
				}
			}
		}

		/* Yield from root references */
		if (generator->node.children == 0) {
			gear_generator *root = generator->node.ptr.root;
			while (root != generator) {
				root = gear_generator_get_child(&root->node, generator);
				size++;
			}
		}
	}
	return size;
}
/* }}} */

static HashTable *gear_generator_get_gc(zval *object, zval **table, int *n) /* {{{ */
{
	gear_generator *generator = (gear_generator*) Z_OBJ_P(object);
	gear_execute_data *execute_data = generator->execute_data;
	gear_op_array *op_array;
	zval *gc_buffer;
	uint32_t gc_buffer_size;

	if (!execute_data) {
		/* If the generator has been closed, it can only hold on to three values: The value, key
		 * and retval. These three zvals are stored sequentially starting at &generator->value. */
		*table = &generator->value;
		*n = 3;
		return NULL;
	}

	op_array = &EX(func)->op_array;
	gc_buffer_size = calc_gc_buffer_size(generator);
	if (generator->gc_buffer_size < gc_buffer_size) {
		generator->gc_buffer = safe_erealloc(generator->gc_buffer, sizeof(zval), gc_buffer_size, 0);
		generator->gc_buffer_size = gc_buffer_size;
	}

	*n = gc_buffer_size;
	*table = gc_buffer = generator->gc_buffer;

	ZVAL_COPY_VALUE(gc_buffer++, &generator->value);
	ZVAL_COPY_VALUE(gc_buffer++, &generator->key);
	ZVAL_COPY_VALUE(gc_buffer++, &generator->retval);
	ZVAL_COPY_VALUE(gc_buffer++, &generator->values);

	if (!(EX_CALL_INFO() & GEAR_CALL_HAS_SYMBOL_TABLE)) {
		uint32_t i, num_cvs = EX(func)->op_array.last_var;
		for (i = 0; i < num_cvs; i++) {
			ZVAL_COPY_VALUE(gc_buffer++, EX_VAR_NUM(i));
		}
	}

	if (EX_CALL_INFO() & GEAR_CALL_FREE_EXTRA_ARGS) {
		zval *zv = EX_VAR_NUM(op_array->last_var + op_array->T);
		zval *end = zv + (EX_NUM_ARGS() - op_array->num_args);
		while (zv != end) {
			ZVAL_COPY_VALUE(gc_buffer++, zv++);
		}
	}

	if (Z_TYPE(execute_data->This) == IS_OBJECT) {
		ZVAL_OBJ(gc_buffer++, Z_OBJ(execute_data->This));
	}
	if (EX_CALL_INFO() & GEAR_CALL_CLOSURE) {
		ZVAL_OBJ(gc_buffer++, GEAR_CLOSURE_OBJECT(EX(func)));
	}

	if (execute_data->opline != op_array->opcodes) {
		uint32_t i, op_num = execute_data->opline - op_array->opcodes - 1;
		for (i = 0; i < op_array->last_live_range; i++) {
			const gear_live_range *range = &op_array->live_range[i];
			if (range->start > op_num) {
				break;
			} else if (op_num < range->end) {
				uint32_t kind = range->var & GEAR_LIVE_MASK;
				uint32_t var_num = range->var & ~GEAR_LIVE_MASK;
				zval *var = EX_VAR(var_num);
				if (kind == GEAR_LIVE_TMPVAR || kind == GEAR_LIVE_LOOP) {
					ZVAL_COPY_VALUE(gc_buffer++, var);
				}
			}
		}
	}

	if (generator->node.children == 0) {
		gear_generator *root = generator->node.ptr.root;
		while (root != generator) {
			ZVAL_OBJ(gc_buffer++, &root->std);
			root = gear_generator_get_child(&root->node, generator);
		}
	}

	if (EX_CALL_INFO() & GEAR_CALL_HAS_SYMBOL_TABLE) {
		return execute_data->symbol_table;
	} else {
		return NULL;
	}
}
/* }}} */

static gear_object *gear_generator_create(gear_class_entry *class_type) /* {{{ */
{
	gear_generator *generator;

	generator = emalloc(sizeof(gear_generator));
	memset(generator, 0, sizeof(gear_generator));

	/* The key will be incremented on first use, so it'll start at 0 */
	generator->largest_used_integer_key = -1;

	ZVAL_UNDEF(&generator->retval);
	ZVAL_UNDEF(&generator->values);

	/* By default we have a tree of only one node */
	generator->node.parent = NULL;
	generator->node.children = 0;
	generator->node.ptr.root = generator;

	gear_object_std_init(&generator->std, class_type);
	generator->std.handlers = &gear_generator_handlers;

	return (gear_object*)generator;
}
/* }}} */

static GEAR_COLD gear_function *gear_generator_get_constructor(gear_object *object) /* {{{ */
{
	gear_throw_error(NULL, "The \"Generator\" class is reserved for internal use and cannot be manually instantiated");

	return NULL;
}
/* }}} */

GEAR_API gear_execute_data *gear_generator_check_placeholder_frame(gear_execute_data *ptr)
{
	if (!ptr->func && Z_TYPE(ptr->This) == IS_OBJECT) {
		if (Z_OBJCE(ptr->This) == gear_ce_generator) {
			gear_generator *generator = (gear_generator *) Z_OBJ(ptr->This);
			gear_generator *root = (generator->node.children < 1 ? generator : generator->node.ptr.leaf)->node.ptr.root;
			gear_execute_data *prev = ptr->prev_execute_data;
			if (generator->node.parent != root) {
				do {
					generator->execute_data->prev_execute_data = prev;
					prev = generator->execute_data;
					generator = generator->node.parent;
				} while (generator->node.parent != root);
			}
			generator->execute_data->prev_execute_data = prev;
			ptr = generator->execute_data;
		}
	}
	return ptr;
}

static void gear_generator_throw_exception(gear_generator *generator, zval *exception)
{
	gear_execute_data *original_execute_data = EG(current_execute_data);

	/* if we don't stop an array/iterator yield from, the exception will only reach the generator after the values were all iterated over */
	if (UNEXPECTED(Z_TYPE(generator->values) != IS_UNDEF)) {
		zval_ptr_dtor(&generator->values);
		ZVAL_UNDEF(&generator->values);
	}

	/* Throw the exception in the context of the generator. Decrementing the opline
	 * to pretend the exception happened during the YIELD opcode. */
	EG(current_execute_data) = generator->execute_data;
	generator->execute_data->opline--;
	if (exception) {
		gear_throw_exception_object(exception);
	} else {
		gear_rethrow_exception(EG(current_execute_data));
	}
	generator->execute_data->opline++;
	EG(current_execute_data) = original_execute_data;
}

static gear_generator *gear_generator_get_child(gear_generator_node *node, gear_generator *leaf)
{
	if (node->children == 0) {
		return NULL;
	} else if (node->children == 1) {
		return node->child.single.child;
	} else {
		return gear_hash_index_find_ptr(node->child.ht, (gear_ulong) leaf);
	}
}

static gear_generator_node *gear_generator_search_multi_children_node(gear_generator_node *node)
{
	while (node->children == 1) {
		node = &node->child.single.child->node;
	}
	return node->children > 1 ? node : NULL;
}

static void gear_generator_add_single_child(gear_generator_node *node, gear_generator *child, gear_generator *leaf)
{
	if (node->children == 0) {
		node->child.single.leaf = leaf;
		node->child.single.child = child;
	} else {
		if (node->children == 1) {
			HashTable *ht = emalloc(sizeof(HashTable));
			gear_hash_init(ht, 0, NULL, NULL, 0);
			gear_hash_index_add_ptr(ht,
				(gear_ulong) node->child.single.leaf, node->child.single.child);
			node->child.ht = ht;
		}

		gear_hash_index_add_ptr(node->child.ht, (gear_ulong) leaf, child);
	}

	node->children++;
}

static void gear_generator_merge_child_nodes(gear_generator_node *dest, gear_generator_node *src, gear_generator *child)
{
	gear_ulong leaf;
	GEAR_ASSERT(src->children > 1);
	GEAR_HASH_FOREACH_NUM_KEY(src->child.ht, leaf) {
		gear_generator_add_single_child(dest, child, (gear_generator *) leaf);
	} GEAR_HASH_FOREACH_END();
}

/* Pay attention so that the root of each subtree of the Generators tree is referenced
 * once per leaf */
static void gear_generator_add_child(gear_generator *generator, gear_generator *child)
{
	gear_generator *leaf = child->node.children ? child->node.ptr.leaf : child;
	gear_generator_node *multi_children_node;
	gear_bool was_leaf = generator->node.children == 0;

	if (was_leaf) {
		gear_generator *next = generator->node.parent;
		leaf->node.ptr.root = generator->node.ptr.root;
		GC_ADDREF(&generator->std); /* we need to increment the generator refcount here as it became integrated into the tree (no leaf), but we must not increment the refcount of the *whole* path in tree */
		generator->node.ptr.leaf = leaf;

		while (next) {
			if (next->node.children > 1) {
				gear_generator *child = gear_hash_index_find_ptr(next->node.child.ht, (gear_ulong) generator);
				gear_hash_index_del(next->node.child.ht, (gear_ulong) generator);
				gear_hash_index_add_ptr(next->node.child.ht, (gear_ulong) leaf, child);
			}

			next->node.ptr.leaf = leaf;
			next = next->node.parent;
		}
	} else if (generator->node.children == 1) {
		multi_children_node = gear_generator_search_multi_children_node(&generator->node);
		if (multi_children_node) {
			generator->node.children = 0;
			gear_generator_merge_child_nodes(&generator->node, multi_children_node, generator->node.child.single.child);
		}
	}

	if (!was_leaf) {
		multi_children_node = gear_generator_search_multi_children_node(&child->node);
	} else {
		multi_children_node = (gear_generator_node *) 0x1;
	}

	{
		gear_generator *parent = generator->node.parent, *cur = generator;

		if (multi_children_node > (gear_generator_node *) 0x1) {
			gear_generator_merge_child_nodes(&generator->node, multi_children_node, child);
		} else {
			gear_generator_add_single_child(&generator->node, child, leaf);
		}
		while (parent) {
			if (parent->node.children > 1) {
				if (multi_children_node == (gear_generator_node *) 0x1) {
					multi_children_node = gear_generator_search_multi_children_node(&child->node);
				}
				if (multi_children_node) {
					gear_generator_merge_child_nodes(&parent->node, multi_children_node, cur);
				} else {
					gear_generator_add_single_child(&parent->node, cur, leaf);
				}
			}
			cur = parent;
			parent = parent->node.parent;
		}
	}
}

void gear_generator_yield_from(gear_generator *generator, gear_generator *from)
{
	gear_generator_add_child(from, generator);

	generator->node.parent = from;
	gear_generator_get_current(generator);
	GC_DELREF(&from->std);
}

GEAR_API gear_generator *gear_generator_update_current(gear_generator *generator, gear_generator *leaf)
{
	gear_generator *old_root, *root = leaf->node.ptr.root;

	/* generator at the root had stopped */
	if (root != generator) {
		old_root = root;
		root = gear_generator_get_child(&root->node, leaf);
	} else {
		old_root = NULL;
	}

	while (!root->execute_data && root != generator) {
		OBJ_RELEASE(&old_root->std);
		old_root = root;

		root = gear_generator_get_child(&root->node, leaf);
	}

	if (root->node.parent) {
		if (root->node.parent->execute_data == NULL) {
			if (EXPECTED(EG(exception) == NULL)) {
				gear_op *yield_from = (gear_op *) root->execute_data->opline - 1;

				if (yield_from->opcode == GEAR_YIELD_FROM) {
					if (Z_ISUNDEF(root->node.parent->retval)) {
						/* Throw the exception in the context of the generator */
						gear_execute_data *original_execute_data = EG(current_execute_data);
						EG(current_execute_data) = root->execute_data;

						if (root == generator) {
							root->execute_data->prev_execute_data = original_execute_data;
						} else {
							root->execute_data->prev_execute_data = &generator->execute_fake;
							generator->execute_fake.prev_execute_data = original_execute_data;
						}

						root->execute_data->opline--; /* GEAR_YIELD(_FROM) already advance, so decrement opline to throw from correct place */
						gear_throw_exception(gear_ce_ClosedGeneratorException, "Generator yielded from aborted, no return value available", 0);

						EG(current_execute_data) = original_execute_data;

						if (!((old_root ? old_root : generator)->flags & GEAR_GENERATOR_CURRENTLY_RUNNING)) {
							leaf->node.ptr.root = root;
							root->node.parent = NULL;
							if (old_root) {
								OBJ_RELEASE(&old_root->std);
							}
							gear_generator_resume(leaf);
							return leaf->node.ptr.root; /* this may be updated during gear_generator_resume! */
						}
					} else {
						zval_ptr_dtor(&root->value);
						ZVAL_COPY(&root->value, &root->node.parent->value);
						ZVAL_COPY(GEAR_CALL_VAR(root->execute_data, yield_from->result.var), &root->node.parent->retval);
					}
				}
			}

			root->node.parent = NULL;
		} else {
			do {
				root = root->node.parent;
				GC_ADDREF(&root->std);
			} while (root->node.parent);
		}
	}

	leaf->node.ptr.root = root;
	if (old_root) {
		OBJ_RELEASE(&old_root->std);
	}

	return root;
}

static int gear_generator_get_next_delegated_value(gear_generator *generator) /* {{{ */
{
	zval *value;
	if (Z_TYPE(generator->values) == IS_ARRAY) {
		HashTable *ht = Z_ARR(generator->values);
		HashPosition pos = Z_FE_POS(generator->values);

		Bucket *p;
		do {
			if (UNEXPECTED(pos >= ht->nNumUsed)) {
				/* Reached end of array */
				goto failure;
			}

			p = &ht->arData[pos];
			value = &p->val;
			if (Z_TYPE_P(value) == IS_INDIRECT) {
				value = Z_INDIRECT_P(value);
			}
			pos++;
		} while (Z_ISUNDEF_P(value));

		zval_ptr_dtor(&generator->value);
		ZVAL_COPY(&generator->value, value);

		zval_ptr_dtor(&generator->key);
		if (p->key) {
			ZVAL_STR_COPY(&generator->key, p->key);
		} else {
			ZVAL_LONG(&generator->key, p->h);
		}

		Z_FE_POS(generator->values) = pos;
	} else {
		gear_object_iterator *iter = (gear_object_iterator *) Z_OBJ(generator->values);

		if (iter->index++ > 0) {
			iter->funcs->move_forward(iter);
			if (UNEXPECTED(EG(exception) != NULL)) {
				goto exception;
			}
		}

		if (iter->funcs->valid(iter) == FAILURE) {
			/* reached end of iteration */
			goto failure;
		}

		value = iter->funcs->get_current_data(iter);
		if (UNEXPECTED(EG(exception) != NULL)) {
			goto exception;
		} else if (UNEXPECTED(!value)) {
			goto failure;
		}

		zval_ptr_dtor(&generator->value);
		ZVAL_COPY(&generator->value, value);

		zval_ptr_dtor(&generator->key);
		if (iter->funcs->get_current_key) {
			iter->funcs->get_current_key(iter, &generator->key);
			if (UNEXPECTED(EG(exception) != NULL)) {
				ZVAL_UNDEF(&generator->key);
				goto exception;
			}
		} else {
			ZVAL_LONG(&generator->key, iter->index);
		}
	}
	return SUCCESS;

exception:
	gear_rethrow_exception(generator->execute_data);

failure:
	zval_ptr_dtor(&generator->values);
	ZVAL_UNDEF(&generator->values);
	return FAILURE;
}
/* }}} */

GEAR_API void gear_generator_resume(gear_generator *orig_generator) /* {{{ */
{
	gear_generator *generator = gear_generator_get_current(orig_generator);

	/* The generator is already closed, thus can't resume */
	if (UNEXPECTED(!generator->execute_data)) {
		return;
	}

try_again:
	if (generator->flags & GEAR_GENERATOR_CURRENTLY_RUNNING) {
		gear_throw_error(NULL, "Cannot resume an already running generator");
		return;
	}

	if (UNEXPECTED((orig_generator->flags & GEAR_GENERATOR_DO_INIT) != 0 && !Z_ISUNDEF(generator->value))) {
		/* We must not advance Generator if we yield from a Generator being currently run */
		return;
	}

	if (UNEXPECTED(!Z_ISUNDEF(generator->values))) {
		if (EXPECTED(gear_generator_get_next_delegated_value(generator) == SUCCESS)) {
			return;
		}
		/* If there are no more deletegated values, resume the generator
		 * after the "yield from" expression. */
	}

	/* Drop the AT_FIRST_YIELD flag */
	orig_generator->flags &= ~GEAR_GENERATOR_AT_FIRST_YIELD;

	{
		/* Backup executor globals */
		gear_execute_data *original_execute_data = EG(current_execute_data);

		/* Set executor globals */
		EG(current_execute_data) = generator->execute_data;

		/* We want the backtrace to look as if the generator function was
		 * called from whatever method we are current running (e.g. next()).
		 * So we have to link generator call frame with caller call frame. */
		if (generator == orig_generator) {
			generator->execute_data->prev_execute_data = original_execute_data;
		} else {
			/* We need some execute_data placeholder in stacktrace to be replaced
			 * by the real stack trace when needed */
			generator->execute_data->prev_execute_data = &orig_generator->execute_fake;
			orig_generator->execute_fake.prev_execute_data = original_execute_data;
		}

		if (UNEXPECTED(generator->frozen_call_stack)) {
			/* Restore frozen call-stack */
			gear_generator_restore_call_stack(generator);
		}

		/* Resume execution */
		generator->flags |= GEAR_GENERATOR_CURRENTLY_RUNNING;
		gear_execute_ex(generator->execute_data);
		generator->flags &= ~GEAR_GENERATOR_CURRENTLY_RUNNING;

		generator->frozen_call_stack = NULL;
		if (EXPECTED(generator->execute_data) &&
		    UNEXPECTED(generator->execute_data->call)) {
			/* Frize call-stack */
			generator->frozen_call_stack = gear_generator_freeze_call_stack(generator->execute_data);
		}

		/* Restore executor globals */
		EG(current_execute_data) = original_execute_data;

		/* If an exception was thrown in the generator we have to internally
		 * rethrow it in the parent scope.
		 * In case we did yield from, the Exception must be rethrown into
		 * its calling frame (see above in if (check_yield_from). */
		if (UNEXPECTED(EG(exception) != NULL)) {
			if (generator == orig_generator) {
				gear_generator_close(generator, 0);
				if (EG(current_execute_data) &&
				    EG(current_execute_data)->func &&
				    GEAR_USER_CODE(EG(current_execute_data)->func->common.type)) {
					gear_rethrow_exception(EG(current_execute_data));
				}
			} else {
				generator = gear_generator_get_current(orig_generator);
				gear_generator_throw_exception(generator, NULL);
				goto try_again;
			}
		}

		/* yield from was used, try another resume. */
		if (UNEXPECTED((generator != orig_generator && !Z_ISUNDEF(generator->retval)) || (generator->execute_data && (generator->execute_data->opline - 1)->opcode == GEAR_YIELD_FROM))) {
			generator = gear_generator_get_current(orig_generator);
			goto try_again;
		}
	}
}
/* }}} */

static inline void gear_generator_ensure_initialized(gear_generator *generator) /* {{{ */
{
	if (UNEXPECTED(Z_TYPE(generator->value) == IS_UNDEF) && EXPECTED(generator->execute_data) && EXPECTED(generator->node.parent == NULL)) {
		generator->flags |= GEAR_GENERATOR_DO_INIT;
		gear_generator_resume(generator);
		generator->flags &= ~GEAR_GENERATOR_DO_INIT;
		generator->flags |= GEAR_GENERATOR_AT_FIRST_YIELD;
	}
}
/* }}} */

static inline void gear_generator_rewind(gear_generator *generator) /* {{{ */
{
	gear_generator_ensure_initialized(generator);

	if (!(generator->flags & GEAR_GENERATOR_AT_FIRST_YIELD)) {
		gear_throw_exception(NULL, "Cannot rewind a generator that was already run", 0);
	}
}
/* }}} */

/* {{{ proto void Generator::rewind()
 * Rewind the generator */
GEAR_METHOD(Generator, rewind)
{
	gear_generator *generator;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	generator = (gear_generator *) Z_OBJ_P(getThis());

	gear_generator_rewind(generator);
}
/* }}} */

/* {{{ proto bool Generator::valid()
 * Check whether the generator is valid */
GEAR_METHOD(Generator, valid)
{
	gear_generator *generator;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	generator = (gear_generator *) Z_OBJ_P(getThis());

	gear_generator_ensure_initialized(generator);

	gear_generator_get_current(generator);

	RETURN_BOOL(EXPECTED(generator->execute_data != NULL));
}
/* }}} */

/* {{{ proto mixed Generator::current()
 * Get the current value */
GEAR_METHOD(Generator, current)
{
	gear_generator *generator, *root;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	generator = (gear_generator *) Z_OBJ_P(getThis());

	gear_generator_ensure_initialized(generator);

	root = gear_generator_get_current(generator);
	if (EXPECTED(generator->execute_data != NULL && Z_TYPE(root->value) != IS_UNDEF)) {
		zval *value = &root->value;

		ZVAL_COPY_DEREF(return_value, value);
	}
}
/* }}} */

/* {{{ proto mixed Generator::key()
 * Get the current key */
GEAR_METHOD(Generator, key)
{
	gear_generator *generator, *root;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	generator = (gear_generator *) Z_OBJ_P(getThis());

	gear_generator_ensure_initialized(generator);

	root = gear_generator_get_current(generator);
	if (EXPECTED(generator->execute_data != NULL && Z_TYPE(root->key) != IS_UNDEF)) {
		zval *key = &root->key;

		ZVAL_COPY_DEREF(return_value, key);
	}
}
/* }}} */

/* {{{ proto void Generator::next()
 * Advances the generator */
GEAR_METHOD(Generator, next)
{
	gear_generator *generator;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	generator = (gear_generator *) Z_OBJ_P(getThis());

	gear_generator_ensure_initialized(generator);

	gear_generator_resume(generator);
}
/* }}} */

/* {{{ proto mixed Generator::send(mixed value)
 * Sends a value to the generator */
GEAR_METHOD(Generator, send)
{
	zval *value;
	gear_generator *generator, *root;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(value)
	GEAR_PARSE_PARAMETERS_END();

	generator = (gear_generator *) Z_OBJ_P(getThis());

	gear_generator_ensure_initialized(generator);

	/* The generator is already closed, thus can't send anything */
	if (UNEXPECTED(!generator->execute_data)) {
		return;
	}

	root = gear_generator_get_current(generator);
	/* Put sent value in the target VAR slot, if it is used */
	if (root->send_target) {
		ZVAL_COPY(root->send_target, value);
	}

	gear_generator_resume(generator);

	root = gear_generator_get_current(generator);
	if (EXPECTED(generator->execute_data)) {
		zval *value = &root->value;

		ZVAL_COPY_DEREF(return_value, value);
	}
}
/* }}} */

/* {{{ proto mixed Generator::throw(Exception exception)
 * Throws an exception into the generator */
GEAR_METHOD(Generator, throw)
{
	zval *exception;
	gear_generator *generator;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(exception)
	GEAR_PARSE_PARAMETERS_END();

	Z_TRY_ADDREF_P(exception);

	generator = (gear_generator *) Z_OBJ_P(getThis());

	gear_generator_ensure_initialized(generator);

	if (generator->execute_data) {
		gear_generator *root = gear_generator_get_current(generator);

		gear_generator_throw_exception(root, exception);

		gear_generator_resume(generator);

		root = gear_generator_get_current(generator);
		if (generator->execute_data) {
			zval *value = &root->value;

			ZVAL_COPY_DEREF(return_value, value);
		}
	} else {
		/* If the generator is already closed throw the exception in the
		 * current context */
		gear_throw_exception_object(exception);
	}
}
/* }}} */

/* {{{ proto mixed Generator::getReturn()
 * Retrieves the return value of the generator */
GEAR_METHOD(Generator, getReturn)
{
	gear_generator *generator;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	generator = (gear_generator *) Z_OBJ_P(getThis());

	gear_generator_ensure_initialized(generator);
	if (UNEXPECTED(EG(exception))) {
		return;
	}

	if (Z_ISUNDEF(generator->retval)) {
		/* Generator hasn't returned yet -> error! */
		gear_throw_exception(NULL,
			"Cannot get return value of a generator that hasn't returned", 0);
		return;
	}

	ZVAL_COPY(return_value, &generator->retval);
}
/* }}} */

/* {{{ proto Generator::__wakeup()
 * Throws an Exception as generators can't be serialized */
GEAR_METHOD(Generator, __wakeup)
{
	/* Just specifying the gear_class_unserialize_deny handler is not enough,
	 * because it is only invoked for C unserialization. For O the error has
	 * to be thrown in __wakeup. */

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	gear_throw_exception(NULL, "Unserialization of 'Generator' is not allowed", 0);
}
/* }}} */

/* get_iterator implementation */

static void gear_generator_iterator_dtor(gear_object_iterator *iterator) /* {{{ */
{
	gear_generator *generator = (gear_generator*)Z_OBJ(iterator->data);
	generator->iterator = NULL;
	zval_ptr_dtor(&iterator->data);
}
/* }}} */

static int gear_generator_iterator_valid(gear_object_iterator *iterator) /* {{{ */
{
	gear_generator *generator = (gear_generator*)Z_OBJ(iterator->data);

	gear_generator_ensure_initialized(generator);

	gear_generator_get_current(generator);

	return generator->execute_data ? SUCCESS : FAILURE;
}
/* }}} */

static zval *gear_generator_iterator_get_data(gear_object_iterator *iterator) /* {{{ */
{
	gear_generator *generator = (gear_generator*)Z_OBJ(iterator->data), *root;

	gear_generator_ensure_initialized(generator);

	root = gear_generator_get_current(generator);

	return &root->value;
}
/* }}} */

static void gear_generator_iterator_get_key(gear_object_iterator *iterator, zval *key) /* {{{ */
{
	gear_generator *generator = (gear_generator*)Z_OBJ(iterator->data), *root;

	gear_generator_ensure_initialized(generator);

	root = gear_generator_get_current(generator);

	if (EXPECTED(Z_TYPE(root->key) != IS_UNDEF)) {
		zval *zv = &root->key;

		ZVAL_COPY_DEREF(key, zv);
	} else {
		ZVAL_NULL(key);
	}
}
/* }}} */

static void gear_generator_iterator_move_forward(gear_object_iterator *iterator) /* {{{ */
{
	gear_generator *generator = (gear_generator*)Z_OBJ(iterator->data);

	gear_generator_ensure_initialized(generator);

	gear_generator_resume(generator);
}
/* }}} */

static void gear_generator_iterator_rewind(gear_object_iterator *iterator) /* {{{ */
{
	gear_generator *generator = (gear_generator*)Z_OBJ(iterator->data);

	gear_generator_rewind(generator);
}
/* }}} */

static const gear_object_iterator_funcs gear_generator_iterator_functions = {
	gear_generator_iterator_dtor,
	gear_generator_iterator_valid,
	gear_generator_iterator_get_data,
	gear_generator_iterator_get_key,
	gear_generator_iterator_move_forward,
	gear_generator_iterator_rewind,
	NULL
};

gear_object_iterator *gear_generator_get_iterator(gear_class_entry *ce, zval *object, int by_ref) /* {{{ */
{
	gear_object_iterator *iterator;
	gear_generator *generator = (gear_generator*)Z_OBJ_P(object);

	if (!generator->execute_data) {
		gear_throw_exception(NULL, "Cannot traverse an already closed generator", 0);
		return NULL;
	}

	if (UNEXPECTED(by_ref) && !(generator->execute_data->func->op_array.fn_flags & GEAR_ACC_RETURN_REFERENCE)) {
		gear_throw_exception(NULL, "You can only iterate a generator by-reference if it declared that it yields by-reference", 0);
		return NULL;
	}

	iterator = generator->iterator = emalloc(sizeof(gear_object_iterator));

	gear_iterator_init(iterator);

	iterator->funcs = &gear_generator_iterator_functions;
	ZVAL_COPY(&iterator->data, object);

	return iterator;
}
/* }}} */

GEAR_BEGIN_ARG_INFO(arginfo_generator_void, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_generator_send, 0, 0, 1)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_generator_throw, 0, 0, 1)
	GEAR_ARG_INFO(0, exception)
GEAR_END_ARG_INFO()

static const gear_function_entry generator_functions[] = {
	GEAR_ME(Generator, rewind,   arginfo_generator_void, GEAR_ACC_PUBLIC)
	GEAR_ME(Generator, valid,    arginfo_generator_void, GEAR_ACC_PUBLIC)
	GEAR_ME(Generator, current,  arginfo_generator_void, GEAR_ACC_PUBLIC)
	GEAR_ME(Generator, key,      arginfo_generator_void, GEAR_ACC_PUBLIC)
	GEAR_ME(Generator, next,     arginfo_generator_void, GEAR_ACC_PUBLIC)
	GEAR_ME(Generator, send,     arginfo_generator_send, GEAR_ACC_PUBLIC)
	GEAR_ME(Generator, throw,    arginfo_generator_throw, GEAR_ACC_PUBLIC)
	GEAR_ME(Generator, getReturn,arginfo_generator_void, GEAR_ACC_PUBLIC)
	GEAR_ME(Generator, __wakeup, arginfo_generator_void, GEAR_ACC_PUBLIC)
	GEAR_FE_END
};

void gear_register_generator_ce(void) /* {{{ */
{
	gear_class_entry ce;

	INIT_CLASS_ENTRY(ce, "Generator", generator_functions);
	gear_ce_generator = gear_register_internal_class(&ce);
	gear_ce_generator->ce_flags |= GEAR_ACC_FINAL;
	gear_ce_generator->create_object = gear_generator_create;
	gear_ce_generator->serialize = gear_class_serialize_deny;
	gear_ce_generator->unserialize = gear_class_unserialize_deny;

	/* get_iterator has to be assigned *after* implementing the inferface */
	gear_class_implements(gear_ce_generator, 1, gear_ce_iterator);
	gear_ce_generator->get_iterator = gear_generator_get_iterator;

	memcpy(&gear_generator_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	gear_generator_handlers.free_obj = gear_generator_free_storage;
	gear_generator_handlers.dtor_obj = gear_generator_dtor_storage;
	gear_generator_handlers.get_gc = gear_generator_get_gc;
	gear_generator_handlers.clone_obj = NULL;
	gear_generator_handlers.get_constructor = gear_generator_get_constructor;

	INIT_CLASS_ENTRY(ce, "ClosedGeneratorException", NULL);
	gear_ce_ClosedGeneratorException = gear_register_internal_class_ex(&ce, gear_ce_exception);
}
/* }}} */

