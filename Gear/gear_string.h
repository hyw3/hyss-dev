/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_STRING_H
#define GEAR_STRING_H

#include "gear.h"

BEGIN_EXTERN_C()

typedef void (*gear_string_copy_storage_func_t)(void);
typedef gear_string *(GEAR_FASTCALL *gear_new_interned_string_func_t)(gear_string *str);
typedef gear_string *(GEAR_FASTCALL *gear_string_init_interned_func_t)(const char *str, size_t size, int permanent);

GEAR_API extern gear_new_interned_string_func_t gear_new_interned_string;
GEAR_API extern gear_string_init_interned_func_t gear_string_init_interned;

GEAR_API gear_ulong GEAR_FASTCALL gear_string_hash_func(gear_string *str);
GEAR_API gear_ulong GEAR_FASTCALL gear_hash_func(const char *str, size_t len);
GEAR_API gear_string* GEAR_FASTCALL gear_interned_string_find_permanent(gear_string *str);

GEAR_API void gear_interned_strings_init(void);
GEAR_API void gear_interned_strings_dtor(void);
GEAR_API void gear_interned_strings_activate(void);
GEAR_API void gear_interned_strings_deactivate(void);
GEAR_API void gear_interned_strings_set_request_storage_handlers(gear_new_interned_string_func_t handler, gear_string_init_interned_func_t init_handler);
GEAR_API void gear_interned_strings_set_permanent_storage_copy_handlers(gear_string_copy_storage_func_t copy_handler, gear_string_copy_storage_func_t restore_handler);
GEAR_API void gear_interned_strings_switch_storage(gear_bool request);

GEAR_API extern gear_string  *gear_empty_string;
GEAR_API extern gear_string  *gear_one_char_string[256];
GEAR_API extern gear_string **gear_known_strings;

END_EXTERN_C()

/* Shortcuts */

#define ZSTR_VAL(zstr)  (zstr)->val
#define ZSTR_LEN(zstr)  (zstr)->len
#define ZSTR_H(zstr)    (zstr)->h
#define ZSTR_HASH(zstr) gear_string_hash_val(zstr)

/* Compatibility macros */

#define IS_INTERNED(s)	ZSTR_IS_INTERNED(s)
#define STR_EMPTY_ALLOC()	ZSTR_EMPTY_ALLOC()
#define _STR_HEADER_SIZE _ZSTR_HEADER_SIZE
#define STR_ALLOCA_ALLOC(str, _len, use_heap) ZSTR_ALLOCA_ALLOC(str, _len, use_heap)
#define STR_ALLOCA_INIT(str, s, len, use_heap) ZSTR_ALLOCA_INIT(str, s, len, use_heap)
#define STR_ALLOCA_FREE(str, use_heap) ZSTR_ALLOCA_FREE(str, use_heap)

/*---*/

#define ZSTR_IS_INTERNED(s)					(GC_FLAGS(s) & IS_STR_INTERNED)

#define ZSTR_EMPTY_ALLOC() gear_empty_string
#define ZSTR_CHAR(c) gear_one_char_string[c]
#define ZSTR_KNOWN(idx) gear_known_strings[idx]

#define _ZSTR_HEADER_SIZE XtOffsetOf(gear_string, val)

#define _ZSTR_STRUCT_SIZE(len) (_ZSTR_HEADER_SIZE + len + 1)

#define ZSTR_ALLOCA_ALLOC(str, _len, use_heap) do { \
	(str) = (gear_string *)do_alloca(GEAR_MM_ALIGNED_SIZE_EX(_ZSTR_STRUCT_SIZE(_len), 8), (use_heap)); \
	GC_SET_REFCOUNT(str, 1); \
	GC_TYPE_INFO(str) = IS_STRING; \
	gear_string_forget_hash_val(str); \
	ZSTR_LEN(str) = _len; \
} while (0)

#define ZSTR_ALLOCA_INIT(str, s, len, use_heap) do { \
	ZSTR_ALLOCA_ALLOC(str, len, use_heap); \
	memcpy(ZSTR_VAL(str), (s), (len)); \
	ZSTR_VAL(str)[(len)] = '\0'; \
} while (0)

#define ZSTR_ALLOCA_FREE(str, use_heap) free_alloca(str, use_heap)

/*---*/

static gear_always_inline gear_ulong gear_string_hash_val(gear_string *s)
{
	return ZSTR_H(s) ? ZSTR_H(s) : gear_string_hash_func(s);
}

static gear_always_inline void gear_string_forget_hash_val(gear_string *s)
{
	ZSTR_H(s) = 0;
}

static gear_always_inline uint32_t gear_string_refcount(const gear_string *s)
{
	if (!ZSTR_IS_INTERNED(s)) {
		return GC_REFCOUNT(s);
	}
	return 1;
}

static gear_always_inline uint32_t gear_string_addref(gear_string *s)
{
	if (!ZSTR_IS_INTERNED(s)) {
		return GC_ADDREF(s);
	}
	return 1;
}

static gear_always_inline uint32_t gear_string_delref(gear_string *s)
{
	if (!ZSTR_IS_INTERNED(s)) {
		return GC_DELREF(s);
	}
	return 1;
}

static gear_always_inline gear_string *gear_string_alloc(size_t len, int persistent)
{
	gear_string *ret = (gear_string *)pemalloc(GEAR_MM_ALIGNED_SIZE(_ZSTR_STRUCT_SIZE(len)), persistent);

	GC_SET_REFCOUNT(ret, 1);
	GC_TYPE_INFO(ret) = IS_STRING | ((persistent ? IS_STR_PERSISTENT : 0) << GC_FLAGS_SHIFT);
	gear_string_forget_hash_val(ret);
	ZSTR_LEN(ret) = len;
	return ret;
}

static gear_always_inline gear_string *gear_string_safe_alloc(size_t n, size_t m, size_t l, int persistent)
{
	gear_string *ret = (gear_string *)safe_pemalloc(n, m, GEAR_MM_ALIGNED_SIZE(_ZSTR_STRUCT_SIZE(l)), persistent);

	GC_SET_REFCOUNT(ret, 1);
	GC_TYPE_INFO(ret) = IS_STRING | ((persistent ? IS_STR_PERSISTENT : 0) << GC_FLAGS_SHIFT);
	gear_string_forget_hash_val(ret);
	ZSTR_LEN(ret) = (n * m) + l;
	return ret;
}

static gear_always_inline gear_string *gear_string_init(const char *str, size_t len, int persistent)
{
	gear_string *ret = gear_string_alloc(len, persistent);

	memcpy(ZSTR_VAL(ret), str, len);
	ZSTR_VAL(ret)[len] = '\0';
	return ret;
}

static gear_always_inline gear_string *gear_string_copy(gear_string *s)
{
	if (!ZSTR_IS_INTERNED(s)) {
		GC_ADDREF(s);
	}
	return s;
}

static gear_always_inline gear_string *gear_string_dup(gear_string *s, int persistent)
{
	if (ZSTR_IS_INTERNED(s)) {
		return s;
	} else {
		return gear_string_init(ZSTR_VAL(s), ZSTR_LEN(s), persistent);
	}
}

static gear_always_inline gear_string *gear_string_realloc(gear_string *s, size_t len, int persistent)
{
	gear_string *ret;

	if (!ZSTR_IS_INTERNED(s)) {
		if (EXPECTED(GC_REFCOUNT(s) == 1)) {
			ret = (gear_string *)perealloc(s, GEAR_MM_ALIGNED_SIZE(_ZSTR_STRUCT_SIZE(len)), persistent);
			ZSTR_LEN(ret) = len;
			gear_string_forget_hash_val(ret);
			return ret;
		} else {
			GC_DELREF(s);
		}
	}
	ret = gear_string_alloc(len, persistent);
	memcpy(ZSTR_VAL(ret), ZSTR_VAL(s), MIN(len, ZSTR_LEN(s)) + 1);
	return ret;
}

static gear_always_inline gear_string *gear_string_extend(gear_string *s, size_t len, int persistent)
{
	gear_string *ret;

	GEAR_ASSERT(len >= ZSTR_LEN(s));
	if (!ZSTR_IS_INTERNED(s)) {
		if (EXPECTED(GC_REFCOUNT(s) == 1)) {
			ret = (gear_string *)perealloc(s, GEAR_MM_ALIGNED_SIZE(_ZSTR_STRUCT_SIZE(len)), persistent);
			ZSTR_LEN(ret) = len;
			gear_string_forget_hash_val(ret);
			return ret;
		} else {
			GC_DELREF(s);
		}
	}
	ret = gear_string_alloc(len, persistent);
	memcpy(ZSTR_VAL(ret), ZSTR_VAL(s), ZSTR_LEN(s) + 1);
	return ret;
}

static gear_always_inline gear_string *gear_string_truncate(gear_string *s, size_t len, int persistent)
{
	gear_string *ret;

	GEAR_ASSERT(len <= ZSTR_LEN(s));
	if (!ZSTR_IS_INTERNED(s)) {
		if (EXPECTED(GC_REFCOUNT(s) == 1)) {
			ret = (gear_string *)perealloc(s, GEAR_MM_ALIGNED_SIZE(_ZSTR_STRUCT_SIZE(len)), persistent);
			ZSTR_LEN(ret) = len;
			gear_string_forget_hash_val(ret);
			return ret;
		} else {
			GC_DELREF(s);
		}
	}
	ret = gear_string_alloc(len, persistent);
	memcpy(ZSTR_VAL(ret), ZSTR_VAL(s), len + 1);
	return ret;
}

static gear_always_inline gear_string *gear_string_safe_realloc(gear_string *s, size_t n, size_t m, size_t l, int persistent)
{
	gear_string *ret;

	if (!ZSTR_IS_INTERNED(s)) {
		if (GC_REFCOUNT(s) == 1) {
			ret = (gear_string *)safe_perealloc(s, n, m, GEAR_MM_ALIGNED_SIZE(_ZSTR_STRUCT_SIZE(l)), persistent);
			ZSTR_LEN(ret) = (n * m) + l;
			gear_string_forget_hash_val(ret);
			return ret;
		} else {
			GC_DELREF(s);
		}
	}
	ret = gear_string_safe_alloc(n, m, l, persistent);
	memcpy(ZSTR_VAL(ret), ZSTR_VAL(s), MIN((n * m) + l, ZSTR_LEN(s)) + 1);
	return ret;
}

static gear_always_inline void gear_string_free(gear_string *s)
{
	if (!ZSTR_IS_INTERNED(s)) {
		GEAR_ASSERT(GC_REFCOUNT(s) <= 1);
		pefree(s, GC_FLAGS(s) & IS_STR_PERSISTENT);
	}
}

static gear_always_inline void gear_string_efree(gear_string *s)
{
	GEAR_ASSERT(!ZSTR_IS_INTERNED(s));
	GEAR_ASSERT(GC_REFCOUNT(s) <= 1);
	GEAR_ASSERT(!(GC_FLAGS(s) & IS_STR_PERSISTENT));
	efree(s);
}

static gear_always_inline void gear_string_release(gear_string *s)
{
	if (!ZSTR_IS_INTERNED(s)) {
		if (GC_DELREF(s) == 0) {
			pefree(s, GC_FLAGS(s) & IS_STR_PERSISTENT);
		}
	}
}

static gear_always_inline void gear_string_release_ex(gear_string *s, int persistent)
{
	if (!ZSTR_IS_INTERNED(s)) {
		if (GC_DELREF(s) == 0) {
			if (persistent) {
				GEAR_ASSERT(GC_FLAGS(s) & IS_STR_PERSISTENT);
				free(s);
			} else {
				GEAR_ASSERT(!(GC_FLAGS(s) & IS_STR_PERSISTENT));
				efree(s);
			}
		}
	}
}

#if defined(__GNUC__) && (defined(__i386__) || (defined(__x86_64__) && !defined(__ILP32__)))
BEGIN_EXTERN_C()
GEAR_API gear_bool GEAR_FASTCALL gear_string_equal_val(gear_string *s1, gear_string *s2);
END_EXTERN_C()
#else
static gear_always_inline gear_bool gear_string_equal_val(gear_string *s1, gear_string *s2)
{
	return !memcmp(ZSTR_VAL(s1), ZSTR_VAL(s2), ZSTR_LEN(s1));
}
#endif

static gear_always_inline gear_bool gear_string_equal_content(gear_string *s1, gear_string *s2)
{
	return ZSTR_LEN(s1) == ZSTR_LEN(s2) && gear_string_equal_val(s1, s2);
}

static gear_always_inline gear_bool gear_string_equals(gear_string *s1, gear_string *s2)
{
	return s1 == s2 || gear_string_equal_content(s1, s2);
}

#define gear_string_equals_ci(s1, s2) \
	(ZSTR_LEN(s1) == ZSTR_LEN(s2) && !gear_binary_strcasecmp(ZSTR_VAL(s1), ZSTR_LEN(s1), ZSTR_VAL(s2), ZSTR_LEN(s2)))

#define gear_string_equals_literal_ci(str, c) \
	(ZSTR_LEN(str) == sizeof(c) - 1 && !gear_binary_strcasecmp(ZSTR_VAL(str), ZSTR_LEN(str), (c), sizeof(c) - 1))

#define gear_string_equals_literal(str, literal) \
	(ZSTR_LEN(str) == sizeof(literal)-1 && !memcmp(ZSTR_VAL(str), literal, sizeof(literal) - 1))

/*
 * DJBX33A (Daniel J. Bernstein, Times 33 with Addition)
 *
 * This is Daniel J. Bernstein's popular `times 33' hash function as
 * posted by him years ago on comp.lang.c. It basically uses a function
 * like ``hash(i) = hash(i-1) * 33 + str[i]''. This is one of the best
 * known hash functions for strings. Because it is both computed very
 * fast and distributes very well.
 *
 * The magic of number 33, i.e. why it works better than many other
 * constants, prime or not, has never been adequately explained by
 * anyone. So I try an explanation: if one experimentally tests all
 * multipliers between 1 and 256 (as RSE did now) one detects that even
 * numbers are not useable at all. The remaining 128 odd numbers
 * (except for the number 1) work more or less all equally well. They
 * all distribute in an acceptable way and this way fill a hash table
 * with an average percent of approx. 86%.
 *
 * If one compares the Chi^2 values of the variants, the number 33 not
 * even has the best value. But the number 33 and a few other equally
 * good numbers like 17, 31, 63, 127 and 129 have nevertheless a great
 * advantage to the remaining numbers in the large set of possible
 * multipliers: their multiply operation can be replaced by a faster
 * operation based on just one shift plus either a single addition
 * or subtraction operation. And because a hash function has to both
 * distribute good _and_ has to be very fast to compute, those few
 * numbers should be preferred and seems to be the reason why Daniel J.
 * Bernstein also preferred it.
 *
 *
 *                  -- Ralf S. Engelschall <rse@engelschall.com>
 */

static gear_always_inline gear_ulong gear_inline_hash_func(const char *str, size_t len)
{
	gear_ulong hash = Z_UL(5381);

	/* variant with the hash unrolled eight times */
	for (; len >= 8; len -= 8) {
		hash = ((hash << 5) + hash) + *str++;
		hash = ((hash << 5) + hash) + *str++;
		hash = ((hash << 5) + hash) + *str++;
		hash = ((hash << 5) + hash) + *str++;
		hash = ((hash << 5) + hash) + *str++;
		hash = ((hash << 5) + hash) + *str++;
		hash = ((hash << 5) + hash) + *str++;
		hash = ((hash << 5) + hash) + *str++;
	}
	switch (len) {
		case 7: hash = ((hash << 5) + hash) + *str++; /* fallthrough... */
		case 6: hash = ((hash << 5) + hash) + *str++; /* fallthrough... */
		case 5: hash = ((hash << 5) + hash) + *str++; /* fallthrough... */
		case 4: hash = ((hash << 5) + hash) + *str++; /* fallthrough... */
		case 3: hash = ((hash << 5) + hash) + *str++; /* fallthrough... */
		case 2: hash = ((hash << 5) + hash) + *str++; /* fallthrough... */
		case 1: hash = ((hash << 5) + hash) + *str++; break;
		case 0: break;
EMPTY_SWITCH_DEFAULT_CASE()
	}

	/* Hash value can't be zero, so we always set the high bit */
#if SIZEOF_GEAR_LONG == 8
	return hash | Z_UL(0x8000000000000000);
#elif SIZEOF_GEAR_LONG == 4
	return hash | Z_UL(0x80000000);
#else
# error "Unknown SIZEOF_GEAR_LONG"
#endif
}

#define GEAR_KNOWN_STRINGS(_) \
	_(GEAR_STR_FILE,                   "file") \
	_(GEAR_STR_LINE,                   "line") \
	_(GEAR_STR_FUNCTION,               "function") \
	_(GEAR_STR_CLASS,                  "class") \
	_(GEAR_STR_OBJECT,                 "object") \
	_(GEAR_STR_TYPE,                   "type") \
	_(GEAR_STR_OBJECT_OPERATOR,        "->") \
	_(GEAR_STR_PAAMAYIM_NEKUDOTAYIM,   "::") \
	_(GEAR_STR_ARGS,                   "args") \
	_(GEAR_STR_UNKNOWN,                "unknown") \
	_(GEAR_STR_EVAL,                   "eval") \
	_(GEAR_STR_INCLUDE,                "include") \
	_(GEAR_STR_REQUIRE,                "require") \
	_(GEAR_STR_INCLUDE_ONCE,           "include_once") \
	_(GEAR_STR_REQUIRE_ONCE,           "require_once") \
	_(GEAR_STR_SCALAR,                 "scalar") \
	_(GEAR_STR_ERROR_REPORTING,        "error_reporting") \
	_(GEAR_STR_STATIC,                 "static") \
	_(GEAR_STR_THIS,                   "this") \
	_(GEAR_STR_VALUE,                  "value") \
	_(GEAR_STR_KEY,                    "key") \
	_(GEAR_STR_MAGIC_AUTOLOAD,         "__autoload") \
	_(GEAR_STR_MAGIC_INVOKE,           "__invoke") \
	_(GEAR_STR_PREVIOUS,               "previous") \
	_(GEAR_STR_CODE,                   "code") \
	_(GEAR_STR_MESSAGE,                "message") \
	_(GEAR_STR_SEVERITY,               "severity") \
	_(GEAR_STR_STRING,                 "string") \
	_(GEAR_STR_TRACE,                  "trace") \
	_(GEAR_STR_SCHEME,                 "scheme") \
	_(GEAR_STR_HOST,                   "host") \
	_(GEAR_STR_PORT,                   "port") \
	_(GEAR_STR_USER,                   "user") \
	_(GEAR_STR_PASS,                   "pass") \
	_(GEAR_STR_PATH,                   "path") \
	_(GEAR_STR_QUERY,                  "query") \
	_(GEAR_STR_FRAGMENT,               "fragment") \
	_(GEAR_STR_NULL,                   "NULL") \
	_(GEAR_STR_BOOLEAN,                "boolean") \
	_(GEAR_STR_INTEGER,                "integer") \
	_(GEAR_STR_DOUBLE,                 "double") \
	_(GEAR_STR_ARRAY,                  "array") \
	_(GEAR_STR_RESOURCE,               "resource") \
	_(GEAR_STR_CLOSED_RESOURCE,        "resource (closed)") \
	_(GEAR_STR_NAME,                   "name") \
	_(GEAR_STR_ARGV,                   "argv") \
	_(GEAR_STR_ARGC,                   "argc") \


typedef enum _gear_known_string_id {
#define _GEAR_STR_ID(id, str) id,
GEAR_KNOWN_STRINGS(_GEAR_STR_ID)
#undef _GEAR_STR_ID
	GEAR_STR_LAST_KNOWN
} gear_known_string_id;

#endif /* GEAR_STRING_H */

