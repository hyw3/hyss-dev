/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_PORTABILITY_H
#define GEAR_PORTABILITY_H

#ifdef __cplusplus
#define BEGIN_EXTERN_C() extern "C" {
#define END_EXTERN_C() }
#else
#define BEGIN_EXTERN_C()
#define END_EXTERN_C()
#endif

/*
 * general definitions
 */

#ifdef GEAR_WIN32
# include "gear_config.w32.h"
# define GEAR_PATHS_SEPARATOR		';'
#elif defined(__riscos__)
# include <gear_config.h>
# define GEAR_PATHS_SEPARATOR		';'
#else
# include <gear_config.h>
# define GEAR_PATHS_SEPARATOR		':'
#endif

#include "../hypbc/hypbc.h"

#include <stdio.h>
#include <assert.h>
#include <math.h>

#ifdef HAVE_UNIX_H
# include <unix.h>
#endif

#ifdef HAVE_STDARG_H
# include <stdarg.h>
#endif

#ifdef HAVE_DLFCN_H
# include <dlfcn.h>
#endif

#ifdef HAVE_LIMITS_H
# include <limits.h>
#endif

#if HAVE_ALLOCA_H && !defined(_ALLOCA_H)
# include <alloca.h>
#endif

#if defined(GEAR_WIN32) && !defined(__clang__)
#include <intrin.h>
#endif

#include "gear_range_check.h"

/* GCC x.y.z supplies __GNUC__ = x and __GNUC_MINOR__ = y */
#ifdef __GNUC__
# define GEAR_GCC_VERSION (__GNUC__ * 1000 + __GNUC_MINOR__)
#else
# define GEAR_GCC_VERSION 0
#endif

/* Compatibility with non-clang compilers */
#ifndef __has_attribute
# define __has_attribute(x) 0
#endif
#ifndef __has_builtin
# define __has_builtin(x) 0
#endif

#if defined(GEAR_WIN32) && !defined(__clang__)
# define GEAR_ASSUME(c)	__assume(c)
#elif ((defined(__GNUC__) && GEAR_GCC_VERSION >= 4005) || __has_builtin(__builtin_unreachable)) && HYSS_HAVE_BUILTIN_EXPECT
# define GEAR_ASSUME(c)	do { \
		if (__builtin_expect(!(c), 0)) __builtin_unreachable(); \
	} while (0)
#else
# define GEAR_ASSUME(c)
#endif

#if GEAR_DEBUG
# define GEAR_ASSERT(c)	assert(c)
#else
# define GEAR_ASSERT(c) GEAR_ASSUME(c)
#endif

/* Only use this macro if you know for sure that all of the switches values
   are covered by its case statements */
#if GEAR_DEBUG
# define EMPTY_SWITCH_DEFAULT_CASE() default: GEAR_ASSERT(0); break;
#else
# define EMPTY_SWITCH_DEFAULT_CASE() default: GEAR_ASSUME(0); break;
#endif

#if defined(__GNUC__) && __GNUC__ >= 4
# define GEAR_IGNORE_VALUE(x) (({ __typeof__ (x) __x = (x); (void) __x; }))
#else
# define GEAR_IGNORE_VALUE(x) ((void) (x))
#endif

#define gear_quiet_write(...) GEAR_IGNORE_VALUE(write(__VA_ARGS__))

/* all HAVE_XXX test have to be after the include of gear_config above */

#if defined(HAVE_LIBDL) && !defined(GEAR_WIN32)

# if defined(__has_feature)
#  if __has_feature(address_sanitizer)
#   define __SANITIZE_ADDRESS__
#  endif
# endif

# ifndef RTLD_LAZY
#  define RTLD_LAZY 1    /* Solaris 1, FreeBSD's (2.1.7.1 and older) */
# endif

# ifndef RTLD_GLOBAL
#  define RTLD_GLOBAL 0
# endif

# if defined(RTLD_GROUP) && defined(RTLD_WORLD) && defined(RTLD_PARENT)
#  define DL_LOAD(libname)			dlopen(libname, RTLD_LAZY | RTLD_GLOBAL | RTLD_GROUP | RTLD_WORLD | RTLD_PARENT)
# elif defined(RTLD_DEEPBIND) && !defined(__SANITIZE_ADDRESS__)
#  define DL_LOAD(libname)			dlopen(libname, RTLD_LAZY | RTLD_GLOBAL | RTLD_DEEPBIND)
# else
#  define DL_LOAD(libname)			dlopen(libname, RTLD_LAZY | RTLD_GLOBAL)
# endif
# define DL_UNLOAD					dlclose
# if defined(DLSYM_NEEDS_UNDERSCORE)
#  define DL_FETCH_SYMBOL(h,s)		dlsym((h), "_" s)
# else
#  define DL_FETCH_SYMBOL			dlsym
# endif
# define DL_ERROR					dlerror
# define DL_HANDLE					void *
# define GEAR_EXTENSIONS_SUPPORT	1
#elif defined(GEAR_WIN32)
# define DL_LOAD(libname)			LoadLibrary(libname)
# define DL_FETCH_SYMBOL			GetProcAddress
# define DL_UNLOAD					FreeLibrary
# define DL_HANDLE					HCAPI
# define GEAR_EXTENSIONS_SUPPORT	1
#else
# define DL_HANDLE					void *
# define GEAR_EXTENSIONS_SUPPORT	0
#endif

/* AIX requires this to be the first thing in the file.  */
#ifndef __GNUC__
# ifndef HAVE_ALLOCA_H
#  ifdef _AIX
#   pragma alloca
#  else
#   ifndef alloca /* predefined by HP cc +Olibcalls */
char *alloca();
#   endif
#  endif
# endif
#endif

#if GEAR_GCC_VERSION >= 2096 || __has_attribute(__malloc__)
# define GEAR_ATTRIBUTE_MALLOC __attribute__ ((__malloc__))
#else
# define GEAR_ATTRIBUTE_MALLOC
#endif

#if GEAR_GCC_VERSION >= 4003 || __has_attribute(alloc_size)
# define GEAR_ATTRIBUTE_ALLOC_SIZE(X) __attribute__ ((alloc_size(X)))
# define GEAR_ATTRIBUTE_ALLOC_SIZE2(X,Y) __attribute__ ((alloc_size(X,Y)))
#else
# define GEAR_ATTRIBUTE_ALLOC_SIZE(X)
# define GEAR_ATTRIBUTE_ALLOC_SIZE2(X,Y)
#endif

#if GEAR_GCC_VERSION >= 2007 || __has_attribute(format)
# define GEAR_ATTRIBUTE_FORMAT(type, idx, first) __attribute__ ((format(type, idx, first)))
#else
# define GEAR_ATTRIBUTE_FORMAT(type, idx, first)
#endif

#if (GEAR_GCC_VERSION >= 3001 && !defined(__INTEL_COMPILER)) || __has_attribute(format)
# define GEAR_ATTRIBUTE_PTR_FORMAT(type, idx, first) __attribute__ ((format(type, idx, first)))
#else
# define GEAR_ATTRIBUTE_PTR_FORMAT(type, idx, first)
#endif

#if GEAR_GCC_VERSION >= 3001 || __has_attribute(deprecated)
# define GEAR_ATTRIBUTE_DEPRECATED  __attribute__((deprecated))
#elif defined(GEAR_WIN32)
# define GEAR_ATTRIBUTE_DEPRECATED  __declspec(deprecated)
#else
# define GEAR_ATTRIBUTE_DEPRECATED
#endif

#if defined(__GNUC__) && GEAR_GCC_VERSION >= 4003
# define GEAR_ATTRIBUTE_UNUSED __attribute__((unused))
# define GEAR_COLD __attribute__((cold))
# define GEAR_HOT __attribute__((hot))
# ifdef __OPTIMIZE__
#  define GEAR_OPT_SIZE  __attribute__((optimize("Os")))
#  define GEAR_OPT_SPEED __attribute__((optimize("Ofast")))
# else
#  define GEAR_OPT_SIZE
#  define GEAR_OPT_SPEED
# endif
#else
# define GEAR_ATTRIBUTE_UNUSED
# define GEAR_COLD
# define GEAR_HOT
# define GEAR_OPT_SIZE
# define GEAR_OPT_SPEED
#endif

#if defined(__GNUC__) && GEAR_GCC_VERSION >= 5000
# define GEAR_ATTRIBUTE_UNUSED_LABEL __attribute__((cold, unused));
# define GEAR_ATTRIBUTE_COLD_LABEL __attribute__((cold));
# define GEAR_ATTRIBUTE_HOT_LABEL __attribute__((hot));
#else
# define GEAR_ATTRIBUTE_UNUSED_LABEL
# define GEAR_ATTRIBUTE_COLD_LABEL
# define GEAR_ATTRIBUTE_HOT_LABEL
#endif

#if defined(__GNUC__) && GEAR_GCC_VERSION >= 3004 && defined(__i386__)
# define GEAR_FASTCALL __attribute__((fastcall))
#elif defined(_MSC_VER) && defined(_M_IX86) && _MSC_VER == 1700
# define GEAR_FASTCALL __fastcall
#elif defined(_MSC_VER) && _MSC_VER >= 1800 && !defined(__clang__)
# define GEAR_FASTCALL __vectorcall
#else
# define GEAR_FASTCALL
#endif

#ifndef restrict
# if defined(__GNUC__) && GEAR_GCC_VERSION >= 3004
# else
#  define __restrict__
# endif
# define restrict __restrict__
#endif

#if (defined(__GNUC__) && __GNUC__ >= 3 && !defined(__INTEL_COMPILER) && !defined(DARWIN) && !defined(__hpux) && !defined(_AIX) && !defined(__osf__)) || __has_attribute(noreturn)
# define HAVE_NORETURN
# define GEAR_NORETURN __attribute__((noreturn))
#elif defined(GEAR_WIN32)
# define HAVE_NORETURN
# define GEAR_NORETURN __declspec(noreturn)
#else
# define GEAR_NORETURN
#endif

#if (defined(__GNUC__) && __GNUC__ >= 3 && !defined(__INTEL_COMPILER) && !defined(DARWIN) && !defined(__hpux) && !defined(_AIX) && !defined(__osf__))
# define HAVE_NORETURN_ALIAS
# define HAVE_ATTRIBUTE_WEAK
#endif

#if GEAR_GCC_VERSION >= 3001 || __has_builtin(__builtin_constant_p)
# define HAVE_BUILTIN_CONSTANT_P
#endif

#ifdef HAVE_BUILTIN_CONSTANT_P
# define GEAR_CONST_COND(_condition, _default) \
	(__builtin_constant_p(_condition) ? (_condition) : (_default))
#else
# define GEAR_CONST_COND(_condition, _default) \
	(_default)
#endif

#if GEAR_DEBUG
# define gear_always_inline inline
# define gear_never_inline
#else
# if defined(__GNUC__)
#  if __GNUC__ >= 3
#   define gear_always_inline inline __attribute__((always_inline))
#   define gear_never_inline __attribute__((noinline))
#  else
#   define gear_always_inline inline
#   define gear_never_inline
#  endif
# elif defined(_MSC_VER)
#  define gear_always_inline __forceinline
#  define gear_never_inline __declspec(noinline)
# else
#  if __has_attribute(always_inline)
#   define gear_always_inline inline __attribute__((always_inline))
#  else
#   define gear_always_inline inline
#  endif
#  if __has_attribute(noinline)
#   define gear_never_inline __attribute__((noinline))
#  else
#   define gear_never_inline
#  endif
# endif
#endif /* GEAR_DEBUG */

#if HYSS_HAVE_BUILTIN_EXPECT
# define EXPECTED(condition)   __builtin_expect(!!(condition), 1)
# define UNEXPECTED(condition) __builtin_expect(!!(condition), 0)
#else
# define EXPECTED(condition)   (condition)
# define UNEXPECTED(condition) (condition)
#endif

#ifndef XtOffsetOf
# if defined(CRAY) || (defined(__ARMCC_VERSION) && !defined(LINUX))
# ifdef __STDC__
# define XtOffset(p_type, field) _Offsetof(p_type, field)
# else
# ifdef CRAY2
# define XtOffset(p_type, field) \
    (sizeof(int)*((unsigned int)&(((p_type)NULL)->field)))

# else /* !CRAY2 */

# define XtOffset(p_type, field) ((unsigned int)&(((p_type)NULL)->field))

# endif /* !CRAY2 */
# endif /* __STDC__ */
# else /* ! (CRAY || __arm) */

# define XtOffset(p_type, field) \
    ((gear_long) (((char *) (&(((p_type)NULL)->field))) - ((char *) NULL)))

# endif /* !CRAY */

# ifdef offsetof
# define XtOffsetOf(s_type, field) offsetof(s_type, field)
# else
# define XtOffsetOf(s_type, field) XtOffset(s_type*, field)
# endif

#endif

#if (HAVE_ALLOCA || (defined (__GNUC__) && __GNUC__ >= 2)) && !(defined(ZTS)) && !(defined(ZTS) && defined(HPUX)) && !defined(DARWIN)
# define GEAR_ALLOCA_MAX_SIZE (32 * 1024)
# define ALLOCA_FLAG(name) \
	gear_bool name;
# define SET_ALLOCA_FLAG(name) \
	name = 1
# define do_alloca_ex(size, limit, use_heap) \
	((use_heap = (UNEXPECTED((size) > (limit)))) ? emalloc(size) : alloca(size))
# define do_alloca(size, use_heap) \
	do_alloca_ex(size, GEAR_ALLOCA_MAX_SIZE, use_heap)
# define free_alloca(p, use_heap) \
	do { if (UNEXPECTED(use_heap)) efree(p); } while (0)
#else
# define ALLOCA_FLAG(name)
# define SET_ALLOCA_FLAG(name)
# define do_alloca(p, use_heap)		emalloc(p)
# define free_alloca(p, use_heap)	efree(p)
#endif

#ifdef HAVE_SIGSETJMP
# define SETJMP(a) sigsetjmp(a, 0)
# define LONGJMP(a,b) siglongjmp(a, b)
# define JMP_BUF sigjmp_buf
#else
# define SETJMP(a) setjmp(a)
# define LONGJMP(a,b) longjmp(a, b)
# define JMP_BUF jmp_buf
#endif

#if GEAR_DEBUG
# define GEAR_FILE_LINE_D				const char *__gear_filename, const uint32_t __gear_lineno
# define GEAR_FILE_LINE_DC				, GEAR_FILE_LINE_D
# define GEAR_FILE_LINE_ORIG_D			const char *__gear_orig_filename, const uint32_t __gear_orig_lineno
# define GEAR_FILE_LINE_ORIG_DC			, GEAR_FILE_LINE_ORIG_D
# define GEAR_FILE_LINE_RELAY_C			__gear_filename, __gear_lineno
# define GEAR_FILE_LINE_RELAY_CC		, GEAR_FILE_LINE_RELAY_C
# define GEAR_FILE_LINE_C				__FILE__, __LINE__
# define GEAR_FILE_LINE_CC				, GEAR_FILE_LINE_C
# define GEAR_FILE_LINE_EMPTY_C			NULL, 0
# define GEAR_FILE_LINE_EMPTY_CC		, GEAR_FILE_LINE_EMPTY_C
# define GEAR_FILE_LINE_ORIG_RELAY_C	__gear_orig_filename, __gear_orig_lineno
# define GEAR_FILE_LINE_ORIG_RELAY_CC	, GEAR_FILE_LINE_ORIG_RELAY_C
#else
# define GEAR_FILE_LINE_D				void
# define GEAR_FILE_LINE_DC
# define GEAR_FILE_LINE_ORIG_D			void
# define GEAR_FILE_LINE_ORIG_DC
# define GEAR_FILE_LINE_RELAY_C
# define GEAR_FILE_LINE_RELAY_CC
# define GEAR_FILE_LINE_C
# define GEAR_FILE_LINE_CC
# define GEAR_FILE_LINE_EMPTY_C
# define GEAR_FILE_LINE_EMPTY_CC
# define GEAR_FILE_LINE_ORIG_RELAY_C
# define GEAR_FILE_LINE_ORIG_RELAY_CC
#endif	/* GEAR_DEBUG */

#if GEAR_DEBUG
# define Z_DBG(expr)		(expr)
#else
# define Z_DBG(expr)
#endif

#ifdef ZTS
# define ZTS_V 1
#else
# define ZTS_V 0
#endif

#ifndef LONG_MAX
# define LONG_MAX 2147483647L
#endif

#ifndef LONG_MIN
# define LONG_MIN (- LONG_MAX - 1)
#endif

#define MAX_LENGTH_OF_DOUBLE 32

#undef MIN
#undef MAX
#define MAX(a, b)  (((a)>(b))?(a):(b))
#define MIN(a, b)  (((a)<(b))?(a):(b))

#define GEAR_BIT_TEST(bits, bit) \
	(((bits)[(bit) / (sizeof((bits)[0])*8)] >> ((bit) & (sizeof((bits)[0])*8-1))) & 1)

/* We always define a function, even if there's a macro or expression we could
 * alias, so that using it in contexts where we can't make function calls
 * won't fail to compile on some machines and not others.
 */
static gear_always_inline double _gear_get_inf(void) /* {{{ */
{
#ifdef INFINITY
	return INFINITY;
#elif HAVE_HUGE_VAL_INF
	return HUGE_VAL;
#elif defined(__i386__) || defined(_X86_) || defined(ALPHA) || defined(_ALPHA) || defined(__alpha)
# define _gear_DOUBLE_INFINITY_HIGH       0x7ff00000
	double val = 0.0;
	((uint32_t*)&val)[1] = _gear_DOUBLE_INFINITY_HIGH;
	((uint32_t*)&val)[0] = 0;
	return val;
#elif HAVE_ATOF_ACCEPTS_INF
	return atof("INF");
#else
	return 1.0/0.0;
#endif
} /* }}} */
#define GEAR_INFINITY (_gear_get_inf())

static gear_always_inline double _gear_get_nan(void) /* {{{ */
{
#ifdef NAN
	return NAN;
#elif HAVE_HUGE_VAL_NAN
	return HUGE_VAL + -HUGE_VAL;
#elif defined(__i386__) || defined(_X86_) || defined(ALPHA) || defined(_ALPHA) || defined(__alpha)
# define _gear_DOUBLE_QUIET_NAN_HIGH      0xfff80000
	double val = 0.0;
	((uint32_t*)&val)[1] = _gear_DOUBLE_QUIET_NAN_HIGH;
	((uint32_t*)&val)[0] = 0;
	return val;
#elif HAVE_ATOF_ACCEPTS_NAN
	return atof("NAN");
#else
	return 0.0/0.0;
#endif
} /* }}} */
#define GEAR_NAN (_gear_get_nan())

#define GEAR_STRL(str)		(str), (sizeof(str)-1)
#define GEAR_STRS(str)		(str), (sizeof(str))
#define GEAR_NORMALIZE_BOOL(n)			\
	((n) ? (((n)<0) ? -1 : 1) : 0)
#define GEAR_TRUTH(x)		((x) ? 1 : 0)
#define GEAR_LOG_XOR(a, b)		(GEAR_TRUTH(a) ^ GEAR_TRUTH(b))

#define GEAR_MAX_RESERVED_RESOURCES	6

/* excpt.h on Digital Unix 4.0 defines function_table */
#undef function_table

#ifdef GEAR_WIN32
#define GEAR_SECURE_ZERO(var, size) RtlSecureZeroMemory((var), (size))
#else
#define GEAR_SECURE_ZERO(var, size) explicit_bzero((var), (size))
#endif

/* This check should only be used on network socket, not file descriptors */
#ifdef GEAR_WIN32
#define GEAR_VALID_SOCKET(sock) (INVALID_SOCKET != (sock))
#else
#define GEAR_VALID_SOCKET(sock) ((sock) >= 0)
#endif

/* va_copy() is __va_copy() in old gcc versions.
 * According to the autoconf manual, using
 * memcpy(&dst, &src, sizeof(va_list))
 * gives maximum portability. */
#ifndef va_copy
# ifdef __va_copy
#  define va_copy(dest, src) __va_copy((dest), (src))
# else
#  define va_copy(dest, src) memcpy(&(dest), &(src), sizeof(va_list))
# endif
#endif

/* Intrinsics macros start. */

#if defined(HAVE_FUNC_ATTRIBUTE_IFUNC) && defined(HAVE_FUNC_ATTRIBUTE_TARGET)
# define GEAR_INTRIN_HAVE_IFUNC_TARGET 1
#endif

#if (defined(__i386__) || defined(__x86_64__))
# if HYSS_HAVE_SSSE3_INSTRUCTIONS && defined(HAVE_TMMINTRIN_H)
# define HYSS_HAVE_SSSE3
# endif

# if HYSS_HAVE_SSE4_2_INSTRUCTIONS && defined(HAVE_NMMINTRIN_H)
# define HYSS_HAVE_SSE4_2
# endif

/*
 * AVX2 support was added in gcc 4.7, but AVX2 intrinsics don't work in
 * __attribute__((target("avx2"))) functions until gcc 4.9.
 */
# if HYSS_HAVE_AVX2_INSTRUCTIONS && defined(HAVE_IMMINTRIN_H) && \
  (defined(__llvm__) || defined(__clang__) || (defined(__GNUC__) && GEAR_GCC_VERSION >= 4009))
# define HYSS_HAVE_AVX2
# endif
#endif

#ifdef __SSSE3__
/* Instructions compiled directly. */
# define GEAR_INTRIN_SSSE3_NATIVE 1
#elif (defined(HAVE_FUNC_ATTRIBUTE_TARGET) && defined(HYSS_HAVE_SSSE3)) || defined(GEAR_WIN32)
/* Function resolved by ifunc or MINIT. */
# define GEAR_INTRIN_SSSE3_RESOLVER 1
#endif

#if GEAR_INTRIN_SSSE3_RESOLVER && GEAR_INTRIN_HAVE_IFUNC_TARGET
# define GEAR_INTRIN_SSSE3_FUNC_PROTO 1
#elif GEAR_INTRIN_SSSE3_RESOLVER
# define GEAR_INTRIN_SSSE3_FUNC_PTR 1
#endif

#if GEAR_INTRIN_SSSE3_RESOLVER
# if defined(HAVE_FUNC_ATTRIBUTE_TARGET)
#  define GEAR_INTRIN_SSSE3_FUNC_DECL(func) GEAR_API func __attribute__((target("ssse3")))
# else
#  define GEAR_INTRIN_SSSE3_FUNC_DECL(func) func
# endif
#else
# define GEAR_INTRIN_SSSE3_FUNC_DECL(func)
#endif

#if defined(HAVE_SSE4_2_DEF) || (defined(GEAR_WIN32) && defined(__SSE4_2__))
/* Instructions compiled directly. */
# define GEAR_INTRIN_SSE4_2_NATIVE 1
#elif (defined(HAVE_FUNC_ATTRIBUTE_TARGET) && defined(HYSS_HAVE_SSE4_2)) || defined(GEAR_WIN32)
/* Function resolved by ifunc or MINIT. */
# define GEAR_INTRIN_SSE4_2_RESOLVER 1
#endif

#if GEAR_INTRIN_SSE4_2_RESOLVER && GEAR_INTRIN_HAVE_IFUNC_TARGET
# define GEAR_INTRIN_SSE4_2_FUNC_PROTO 1
#elif GEAR_INTRIN_SSE4_2_RESOLVER
# define GEAR_INTRIN_SSE4_2_FUNC_PTR 1
#endif

#if GEAR_INTRIN_SSE4_2_RESOLVER
# if defined(HAVE_FUNC_ATTRIBUTE_TARGET)
#  define GEAR_INTRIN_SSE4_2_FUNC_DECL(func) GEAR_API func __attribute__((target("sse4.2")))
# else
#  define GEAR_INTRIN_SSE4_2_FUNC_DECL(func) func
# endif
#else
# define GEAR_INTRIN_SSE4_2_FUNC_DECL(func)
#endif

#ifdef __AVX2__
# define GEAR_INTRIN_AVX2_NATIVE 1
#elif (defined(HAVE_FUNC_ATTRIBUTE_TARGET) && defined(HYSS_HAVE_AVX2)) || defined(GEAR_WIN32)
# define GEAR_INTRIN_AVX2_RESOLVER 1
#endif

#if GEAR_INTRIN_AVX2_RESOLVER && GEAR_INTRIN_HAVE_IFUNC_TARGET
# define GEAR_INTRIN_AVX2_FUNC_PROTO 1
#elif GEAR_INTRIN_AVX2_RESOLVER
# define GEAR_INTRIN_AVX2_FUNC_PTR 1
#endif

#if GEAR_INTRIN_AVX2_RESOLVER
# if defined(HAVE_FUNC_ATTRIBUTE_TARGET)
#  define GEAR_INTRIN_AVX2_FUNC_DECL(func) GEAR_API func __attribute__((target("avx2")))
# else
#  define GEAR_INTRIN_AVX2_FUNC_DECL(func) func
# endif
#else
# define GEAR_INTRIN_AVX2_FUNC_DECL(func)
#endif

/* Intrinsics macros end. */

#ifdef GEAR_WIN32
# define GEAR_SET_ALIGNED(alignment, decl) __declspec(align(alignment)) decl
#elif HAVE_ATTRIBUTE_ALIGNED
# define GEAR_SET_ALIGNED(alignment, decl) decl __attribute__ ((__aligned__ (alignment)))
#else
# define GEAR_SET_ALIGNED(alignment, decl) decl
#endif

#define GEAR_SLIDE_TO_ALIGNED(alignment, ptr) (((gear_uintptr_t)(ptr) + ((alignment)-1)) & ~((alignment)-1))
#define GEAR_SLIDE_TO_ALIGNED16(ptr) GEAR_SLIDE_TO_ALIGNED(Z_UL(16), ptr)

#ifdef GEAR_WIN32
# define _GEAR_EXPAND_VA(a) a
# define GEAR_EXPAND_VA(code) _GEAR_EXPAND_VA(code)
#else
# define GEAR_EXPAND_VA(code) code
#endif

#endif /* GEAR_PORTABILITY_H */

