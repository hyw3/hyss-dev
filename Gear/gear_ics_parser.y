%{
/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define DEBUG_CFG_PARSER 0

#include "gear.h"
#include "gear_API.h"
#include "gear_ics.h"
#include "gear_constants.h"
#include "gear_ics_scanner.h"
#include "gear_extensions.h"

#ifdef GEAR_WIN32
#include "win32/syslog.h"
#endif

#define YYERROR_VERBOSE
#define YYSTYPE zval

int ics_parse(void);

#define GEAR_ICS_PARSER_CB	(CG(ics_parser_param))->ics_parser_cb
#define GEAR_ICS_PARSER_ARG	(CG(ics_parser_param))->arg

#ifdef _MSC_VER
#define YYMALLOC malloc
#define YYFREE free
#endif

#define GEAR_SYSTEM_ICS CG(ics_parser_unbuffered_errors)

static int get_int_val(zval *op) {
	switch (Z_TYPE_P(op)) {
		case IS_LONG:
			return Z_LVAL_P(op);
		case IS_DOUBLE:
			return (int)Z_DVAL_P(op);
		case IS_STRING:
		{
			int val = atoi(Z_STRVAL_P(op));
			gear_string_free(Z_STR_P(op));
			return val;
		}
		EMPTY_SWITCH_DEFAULT_CASE()
	}
}

/* {{{ gear_ics_do_op()
*/
static void gear_ics_do_op(char type, zval *result, zval *op1, zval *op2)
{
	int i_result;
	int i_op1, i_op2;
	int str_len;
	char str_result[MAX_LENGTH_OF_LONG+1];

	i_op1 = get_int_val(op1);
	i_op2 = op2 ? get_int_val(op2) : 0;

	switch (type) {
		case '|':
			i_result = i_op1 | i_op2;
			break;
		case '&':
			i_result = i_op1 & i_op2;
			break;
		case '^':
			i_result = i_op1 ^ i_op2;
			break;
		case '~':
			i_result = ~i_op1;
			break;
		case '!':
			i_result = !i_op1;
			break;
		default:
			i_result = 0;
			break;
	}

	str_len = gear_sprintf(str_result, "%d", i_result);
	ZVAL_NEW_STR(result, gear_string_init(str_result, str_len, GEAR_SYSTEM_ICS));
}
/* }}} */

/* {{{ gear_ics_init_string()
*/
static void gear_ics_init_string(zval *result)
{
	if (GEAR_SYSTEM_ICS) {
		ZVAL_EMPTY_PSTRING(result);
	} else {
		ZVAL_EMPTY_STRING(result);
	}
}
/* }}} */

/* {{{ gear_ics_add_string()
*/
static void gear_ics_add_string(zval *result, zval *op1, zval *op2)
{
	int length, op1_len;

	if (Z_TYPE_P(op1) != IS_STRING) {
		/* GEAR_ASSERT(!Z_REFCOUNTED_P(op1)); */
		if (GEAR_SYSTEM_ICS) {
			gear_string *tmp_str;
			gear_string *str = zval_get_tmp_string(op1, &tmp_str);
			ZVAL_PSTRINGL(op1, ZSTR_VAL(str), ZSTR_LEN(str));
			gear_tmp_string_release(tmp_str);
		} else {
			ZVAL_STR(op1, zval_get_string_func(op1));
		}
	}
	op1_len = (int)Z_STRLEN_P(op1);

	if (Z_TYPE_P(op2) != IS_STRING) {
		convert_to_string(op2);
	}
	length = op1_len + (int)Z_STRLEN_P(op2);

	ZVAL_NEW_STR(result, gear_string_extend(Z_STR_P(op1), length, GEAR_SYSTEM_ICS));
	memcpy(Z_STRVAL_P(result) + op1_len, Z_STRVAL_P(op2), Z_STRLEN_P(op2) + 1);
}
/* }}} */

/* {{{ gear_ics_get_constant()
*/
static void gear_ics_get_constant(zval *result, zval *name)
{
	zval *c, tmp;

	/* If name contains ':' it is not a constant. Bug #26893. */
	if (!memchr(Z_STRVAL_P(name), ':', Z_STRLEN_P(name))
		   	&& (c = gear_get_constant(Z_STR_P(name))) != 0) {
		if (Z_TYPE_P(c) != IS_STRING) {
			ZVAL_COPY_OR_DUP(&tmp, c);
			if (Z_OPT_CONSTANT(tmp)) {
				zval_update_constant_ex(&tmp, NULL);
			}
			convert_to_string(&tmp);
			c = &tmp;
		}
		ZVAL_NEW_STR(result, gear_string_init(Z_STRVAL_P(c), Z_STRLEN_P(c), GEAR_SYSTEM_ICS));
		if (c == &tmp) {
			gear_string_release(Z_STR(tmp));
		}
		gear_string_free(Z_STR_P(name));
	} else {
		*result = *name;
	}
}
/* }}} */

/* {{{ gear_ics_get_var()
*/
static void gear_ics_get_var(zval *result, zval *name)
{
	zval *curval;
	char *envvar;

	/* Fetch configuration option value */
	if ((curval = gear_get_configuration_directive(Z_STR_P(name))) != NULL) {
		ZVAL_NEW_STR(result, gear_string_init(Z_STRVAL_P(curval), Z_STRLEN_P(curval), GEAR_SYSTEM_ICS));
	/* ..or if not found, try ENV */
	} else if ((envvar = gear_getenv(Z_STRVAL_P(name), Z_STRLEN_P(name))) != NULL ||
			   (envvar = getenv(Z_STRVAL_P(name))) != NULL) {
		ZVAL_NEW_STR(result, gear_string_init(envvar, strlen(envvar), GEAR_SYSTEM_ICS));
	} else {
		gear_ics_init_string(result);
	}
}
/* }}} */

/* {{{ ics_error()
*/
static GEAR_COLD void ics_error(const char *msg)
{
	char *error_buf;
	int error_buf_len;
	char *currently_parsed_filename;

	currently_parsed_filename = gear_ics_scanner_get_filename();
	if (currently_parsed_filename) {
		error_buf_len = 128 + (int)strlen(msg) + (int)strlen(currently_parsed_filename); /* should be more than enough */
		error_buf = (char *) emalloc(error_buf_len);

		sprintf(error_buf, "%s in %s on line %d\n", msg, currently_parsed_filename, gear_ics_scanner_get_lineno());
	} else {
		error_buf = estrdup("Invalid configuration directive\n");
	}

	if (CG(ics_parser_unbuffered_errors)) {
#ifdef GEAR_WIN32
		syslog(LOG_ALERT, "HYSS: %s (%s)", error_buf, GetCommandLine());
#endif
		fprintf(stderr, "HYSS:  %s", error_buf);
	} else {
		gear_error(E_WARNING, "%s", error_buf);
	}
	efree(error_buf);
}
/* }}} */

/* {{{ gear_parse_ics_file()
*/
GEAR_API int gear_parse_ics_file(gear_file_handle *fh, gear_bool unbuffered_errors, int scanner_mode, gear_ics_parser_cb_t ics_parser_cb, void *arg)
{
	int retval;
	gear_ics_parser_param ics_parser_param;

	ics_parser_param.ics_parser_cb = ics_parser_cb;
	ics_parser_param.arg = arg;
	CG(ics_parser_param) = &ics_parser_param;

	if (gear_ics_open_file_for_scanning(fh, scanner_mode) == FAILURE) {
		return FAILURE;
	}

	CG(ics_parser_unbuffered_errors) = unbuffered_errors;
	retval = ics_parse();
	gear_file_handle_dtor(fh);

	shutdown_ics_scanner();

	if (retval == 0) {
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */

/* {{{ gear_parse_ics_string()
*/
GEAR_API int gear_parse_ics_string(char *str, gear_bool unbuffered_errors, int scanner_mode, gear_ics_parser_cb_t ics_parser_cb, void *arg)
{
	int retval;
	gear_ics_parser_param ics_parser_param;

	ics_parser_param.ics_parser_cb = ics_parser_cb;
	ics_parser_param.arg = arg;
	CG(ics_parser_param) = &ics_parser_param;

	if (gear_ics_prepare_string_for_scanning(str, scanner_mode) == FAILURE) {
		return FAILURE;
	}

	CG(ics_parser_unbuffered_errors) = unbuffered_errors;
	retval = ics_parse();

	shutdown_ics_scanner();

	if (retval == 0) {
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */

/* {{{ zval_ics_dtor()
*/
static void zval_ics_dtor(zval *zv)
{
	if (Z_TYPE_P(zv) == IS_STRING) {
		gear_string_release(Z_STR_P(zv));
	}
}
/* }}} */

%}

%expect 0
%pure-parser

%token TC_SECTION
%token TC_RAW
%token TC_CONSTANT
%token TC_NUMBER
%token TC_STRING
%token TC_WHITESPACE
%token TC_LABEL
%token TC_OFFSET
%token TC_DOLLAR_CURLY
%token TC_VARNAME
%token TC_QUOTED_STRING
%token BOOL_TRUE
%token BOOL_FALSE
%token NULL_NULL
%token END_OF_LINE
%token '=' ':' ',' '.' '"' '\'' '^' '+' '-' '/' '*' '%' '$' '~' '<' '>' '?' '@' '{' '}'
%left '|' '&' '^'
%right '~' '!'

%destructor { zval_ics_dtor(&$$); } TC_RAW TC_CONSTANT TC_NUMBER TC_STRING TC_WHITESPACE TC_LABEL TC_OFFSET TC_VARNAME BOOL_TRUE BOOL_FALSE NULL_NULL

%%

statement_list:
		statement_list statement
	|	/* empty */
;

statement:
		TC_SECTION section_string_or_value ']' {
#if DEBUG_CFG_PARSER
			printf("SECTION: [%s]\n", Z_STRVAL($2));
#endif
			GEAR_ICS_PARSER_CB(&$2, NULL, NULL, GEAR_ICS_PARSER_SECTION, GEAR_ICS_PARSER_ARG);
			gear_string_release(Z_STR($2));
		}
	|	TC_LABEL '=' string_or_value {
#if DEBUG_CFG_PARSER
			printf("NORMAL: '%s' = '%s'\n", Z_STRVAL($1), Z_STRVAL($3));
#endif
			GEAR_ICS_PARSER_CB(&$1, &$3, NULL, GEAR_ICS_PARSER_ENTRY, GEAR_ICS_PARSER_ARG);
			gear_string_release(Z_STR($1));
			zval_ics_dtor(&$3);
		}
	|	TC_OFFSET option_offset ']' '=' string_or_value {
#if DEBUG_CFG_PARSER
			printf("OFFSET: '%s'[%s] = '%s'\n", Z_STRVAL($1), Z_STRVAL($2), Z_STRVAL($5));
#endif
			GEAR_ICS_PARSER_CB(&$1, &$5, &$2, GEAR_ICS_PARSER_POP_ENTRY, GEAR_ICS_PARSER_ARG);
			gear_string_release(Z_STR($1));
			zval_ics_dtor(&$2);
			zval_ics_dtor(&$5);
		}
	|	TC_LABEL	{ GEAR_ICS_PARSER_CB(&$1, NULL, NULL, GEAR_ICS_PARSER_ENTRY, GEAR_ICS_PARSER_ARG); gear_string_release(Z_STR($1)); }
	|	END_OF_LINE
;

section_string_or_value:
		var_string_list_section			{ $$ = $1; }
	|	/* empty */						{ gear_ics_init_string(&$$); }
;

string_or_value:
		expr							{ $$ = $1; }
	|	BOOL_TRUE						{ $$ = $1; }
	|	BOOL_FALSE						{ $$ = $1; }
	|	NULL_NULL						{ $$ = $1; }
	|	END_OF_LINE						{ gear_ics_init_string(&$$); }
;

option_offset:
		var_string_list					{ $$ = $1; }
	|	/* empty */						{ gear_ics_init_string(&$$); }
;

encapsed_list:
		encapsed_list cfg_var_ref		{ gear_ics_add_string(&$$, &$1, &$2); gear_string_free(Z_STR($2)); }
	|	encapsed_list TC_QUOTED_STRING	{ gear_ics_add_string(&$$, &$1, &$2); gear_string_free(Z_STR($2)); }
	|	/* empty */						{ gear_ics_init_string(&$$); }
;

var_string_list_section:
		cfg_var_ref						{ $$ = $1; }
	|	constant_literal				{ $$ = $1; }
	|	'"' encapsed_list '"'			{ $$ = $2; }
	|	var_string_list_section cfg_var_ref 	{ gear_ics_add_string(&$$, &$1, &$2); gear_string_free(Z_STR($2)); }
	|	var_string_list_section constant_literal	{ gear_ics_add_string(&$$, &$1, &$2); gear_string_free(Z_STR($2)); }
	|	var_string_list_section '"' encapsed_list '"'  { gear_ics_add_string(&$$, &$1, &$3); gear_string_free(Z_STR($3)); }
;

var_string_list:
		cfg_var_ref						{ $$ = $1; }
	|	constant_string					{ $$ = $1; }
	|	'"' encapsed_list '"'			{ $$ = $2; }
	|	var_string_list cfg_var_ref 	{ gear_ics_add_string(&$$, &$1, &$2); gear_string_free(Z_STR($2)); }
	|	var_string_list constant_string	{ gear_ics_add_string(&$$, &$1, &$2); gear_string_free(Z_STR($2)); }
	|	var_string_list '"' encapsed_list '"'  { gear_ics_add_string(&$$, &$1, &$3); gear_string_free(Z_STR($3)); }
;

expr:
		var_string_list					{ $$ = $1; }
	|	expr '|' expr					{ gear_ics_do_op('|', &$$, &$1, &$3); }
	|	expr '&' expr					{ gear_ics_do_op('&', &$$, &$1, &$3); }
	|	expr '^' expr					{ gear_ics_do_op('^', &$$, &$1, &$3); }
	|	'~' expr						{ gear_ics_do_op('~', &$$, &$2, NULL); }
	|	'!'	expr						{ gear_ics_do_op('!', &$$, &$2, NULL); }
	|	'(' expr ')'					{ $$ = $2; }
;

cfg_var_ref:
		TC_DOLLAR_CURLY TC_VARNAME '}'	{ gear_ics_get_var(&$$, &$2); gear_string_free(Z_STR($2)); }
;

constant_literal:
		TC_CONSTANT						{ $$ = $1; }
	|	TC_RAW							{ $$ = $1; /*printf("TC_RAW: '%s'\n", Z_STRVAL($1));*/ }
	|	TC_NUMBER						{ $$ = $1; /*printf("TC_NUMBER: '%s'\n", Z_STRVAL($1));*/ }
	|	TC_STRING						{ $$ = $1; /*printf("TC_STRING: '%s'\n", Z_STRVAL($1));*/ }
	|	TC_WHITESPACE					{ $$ = $1; /*printf("TC_WHITESPACE: '%s'\n", Z_STRVAL($1));*/ }
;

constant_string:
		TC_CONSTANT						{ gear_ics_get_constant(&$$, &$1); }
	|	TC_RAW							{ $$ = $1; /*printf("TC_RAW: '%s'\n", Z_STRVAL($1));*/ }
	|	TC_NUMBER						{ $$ = $1; /*printf("TC_NUMBER: '%s'\n", Z_STRVAL($1));*/ }
	|	TC_STRING						{ $$ = $1; /*printf("TC_STRING: '%s'\n", Z_STRVAL($1));*/ }
	|	TC_WHITESPACE					{ $$ = $1; /*printf("TC_WHITESPACE: '%s'\n", Z_STRVAL($1));*/ }
;

