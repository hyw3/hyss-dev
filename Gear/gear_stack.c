/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_stack.h"

#define GEAR_STACK_ELEMENT(stack, n) ((void *)((char *) (stack)->elements + (stack)->size * (n)))

GEAR_API int gear_stack_init(gear_stack *stack, int size)
{
	stack->size = size;
	stack->top = 0;
	stack->max = 0;
	stack->elements = NULL;
	return SUCCESS;
}

GEAR_API int gear_stack_push(gear_stack *stack, const void *element)
{
	/* We need to allocate more memory */
	if (stack->top >= stack->max) {
		stack->max += STACK_BLOCK_SIZE;
		stack->elements = safe_erealloc(stack->elements, stack->size, stack->max, 0);
	}
	memcpy(GEAR_STACK_ELEMENT(stack, stack->top), element, stack->size);
	return stack->top++;
}


GEAR_API void *gear_stack_top(const gear_stack *stack)
{
	if (stack->top > 0) {
		return GEAR_STACK_ELEMENT(stack, stack->top - 1);
	} else {
		return NULL;
	}
}


GEAR_API int gear_stack_del_top(gear_stack *stack)
{
	--stack->top;
	return SUCCESS;
}


GEAR_API int gear_stack_int_top(const gear_stack *stack)
{
	int *e = gear_stack_top(stack);
	if (e) {
		return *e;
	} else {
		return FAILURE;
	}
}


GEAR_API int gear_stack_is_empty(const gear_stack *stack)
{
	return stack->top == 0;
}


GEAR_API int gear_stack_destroy(gear_stack *stack)
{
	if (stack->elements) {
		efree(stack->elements);
		stack->elements = NULL;
	}

	return SUCCESS;
}


GEAR_API void *gear_stack_base(const gear_stack *stack)
{
	return stack->elements;
}


GEAR_API int gear_stack_count(const gear_stack *stack)
{
	return stack->top;
}


GEAR_API void gear_stack_apply(gear_stack *stack, int type, int (*apply_function)(void *element))
{
	int i;

	switch (type) {
		case GEAR_STACK_APPLY_TOPDOWN:
			for (i=stack->top-1; i>=0; i--) {
				if (apply_function(GEAR_STACK_ELEMENT(stack, i))) {
					break;
				}
			}
			break;
		case GEAR_STACK_APPLY_BOTTOMUP:
			for (i=0; i<stack->top; i++) {
				if (apply_function(GEAR_STACK_ELEMENT(stack, i))) {
					break;
				}
			}
			break;
	}
}


GEAR_API void gear_stack_apply_with_argument(gear_stack *stack, int type, int (*apply_function)(void *element, void *arg), void *arg)
{
	int i;

	switch (type) {
		case GEAR_STACK_APPLY_TOPDOWN:
			for (i=stack->top-1; i>=0; i--) {
				if (apply_function(GEAR_STACK_ELEMENT(stack, i), arg)) {
					break;
				}
			}
			break;
		case GEAR_STACK_APPLY_BOTTOMUP:
			for (i=0; i<stack->top; i++) {
				if (apply_function(GEAR_STACK_ELEMENT(stack, i), arg)) {
					break;
				}
			}
			break;
	}
}

GEAR_API void gear_stack_clean(gear_stack *stack, void (*func)(void *), gear_bool free_elements)
{
	int i;

	if (func) {
		for (i = 0; i < stack->top; i++) {
			func(GEAR_STACK_ELEMENT(stack, i));
		}
	}
	if (free_elements) {
		if (stack->elements) {
			efree(stack->elements);
			stack->elements = NULL;
		}
		stack->top = stack->max = 0;
	}
}

