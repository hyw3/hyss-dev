/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GEAR_ICS_SCANNER_H
#define _GEAR_ICS_SCANNER_H

/* Scanner modes */
#define GEAR_ICS_SCANNER_NORMAL 0 /* Normal mode. [DEFAULT] */
#define GEAR_ICS_SCANNER_RAW    1 /* Raw mode. Option values are not parsed */
#define GEAR_ICS_SCANNER_TYPED  2 /* Typed mode. */

BEGIN_EXTERN_C()
GEAR_COLD int gear_ics_scanner_get_lineno(void);
GEAR_COLD char *gear_ics_scanner_get_filename(void);
int gear_ics_open_file_for_scanning(gear_file_handle *fh, int scanner_mode);
int gear_ics_prepare_string_for_scanning(char *str, int scanner_mode);
int ics_lex(zval *ics_lval);
void shutdown_ics_scanner(void);
END_EXTERN_C()

#endif /* _GEAR_ICS_SCANNER_H */

