/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_CONFIG_W32_H
#define GEAR_CONFIG_W32_H

#include <../main/config.w32.h>

#define _CRTDBG_MAP_ALLOC

#include <malloc.h>
#include <stdlib.h>
#include <crtdbg.h>

#include <string.h>

#ifndef GEAR_INCLUDE_FULL_WINDOWS_HEADERS
#define WIN32_LEAN_AND_MEAN
#endif
#include <winsock2.h>
#include <windows.h>

#include <float.h>

typedef unsigned long ulong;
typedef unsigned int uint;

#define HAVE_STDIOSTR_H 1
#define HAVE_CLASS_ISTDIOSTREAM
#define istdiostream stdiostream

#if _MSC_VER < 1900
#define snprintf _snprintf
#endif
#define strcasecmp(s1, s2) _stricmp(s1, s2)
#define strncasecmp(s1, s2, n) _strnicmp(s1, s2, n)
#define gear_isinf(a)	((_fpclass(a) == _FPCLASS_PINF) || (_fpclass(a) == _FPCLASS_NINF))
#define gear_finite(x)	_finite(x)
#define gear_isnan(x)	_isnan(x)

#define gear_sprintf sprintf

#ifndef __cplusplus
/* This will cause the compilation process to be MUCH longer, but will generate
 * a much quicker HYSS binary
 */
#ifdef GEAR_WIN32_FORCE_INLINE
# undef inline
# define inline __forceinline
#endif
#endif

#ifdef LIBGEAR_EXPORTS
#	define GEAR_API __declspec(dllexport)
#else
#	define GEAR_API __declspec(dllimport)
#endif

#define GEAR_DLEXPORT		__declspec(dllexport)
#define GEAR_DLIMPORT		__declspec(dllimport)

#endif /* GEAR_CONFIG_W32_H */

