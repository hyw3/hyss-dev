/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CAPIS_H
#define CAPIS_H

#include "gear.h"
#include "gear_compile.h"
#include "gear_build.h"

#define INIT_FUNC_ARGS		int type, int capi_number
#define INIT_FUNC_ARGS_PASSTHRU	type, capi_number
#define SHUTDOWN_FUNC_ARGS	int type, int capi_number
#define SHUTDOWN_FUNC_ARGS_PASSTHRU type, capi_number
#define GEAR_CAPI_INFO_FUNC_ARGS gear_capi_entry *gear_capi
#define GEAR_CAPI_INFO_FUNC_ARGS_PASSTHRU gear_capi

#define GEAR_CAPI_API_NO 20200801
#ifdef ZTS
#define USING_ZTS 1
#else
#define USING_ZTS 0
#endif

#define STANDARD_CAPI_HEADER_EX sizeof(gear_capi_entry), GEAR_CAPI_API_NO, GEAR_DEBUG, USING_ZTS
#define STANDARD_CAPI_HEADER \
	STANDARD_CAPI_HEADER_EX, NULL, NULL
#define ZE2_STANDARD_CAPI_HEADER \
	STANDARD_CAPI_HEADER_EX, ics_entries, NULL

#define GEAR_CAPI_BUILD_ID "API" GEAR_TOSTR(GEAR_CAPI_API_NO) GEAR_BUILD_TS GEAR_BUILD_DEBUG GEAR_BUILD_SYSTEM GEAR_BUILD_EXTRA

#define STANDARD_CAPI_PROPERTIES_EX 0, 0, NULL, 0, GEAR_CAPI_BUILD_ID

#define NO_CAPI_GLOBALS 0, NULL, NULL, NULL

#ifdef ZTS
# define GEAR_CAPI_GLOBALS(capi_name) sizeof(gear_##capi_name##_globals), &capi_name##_globals_id
#else
# define GEAR_CAPI_GLOBALS(capi_name) sizeof(gear_##capi_name##_globals), &capi_name##_globals
#endif

#define STANDARD_CAPI_PROPERTIES \
	NO_CAPI_GLOBALS, NULL, STANDARD_CAPI_PROPERTIES_EX

#define NO_VERSION_YET NULL

#define CAPI_PERSISTENT 1
#define CAPI_TEMPORARY 2

struct _gear_ics_entry;
typedef struct _gear_capi_entry gear_capi_entry;
typedef struct _gear_capi_dep gear_capi_dep;

struct _gear_capi_entry {
	unsigned short size;
	unsigned int gear_api;
	unsigned char gear_debug;
	unsigned char zts;
	const struct _gear_ics_entry *ics_entry;
	const struct _gear_capi_dep *deps;
	const char *name;
	const struct _gear_function_entry *functions;
	int (*capi_startup_func)(INIT_FUNC_ARGS);
	int (*capi_shutdown_func)(SHUTDOWN_FUNC_ARGS);
	int (*request_startup_func)(INIT_FUNC_ARGS);
	int (*request_shutdown_func)(SHUTDOWN_FUNC_ARGS);
	void (*info_func)(GEAR_CAPI_INFO_FUNC_ARGS);
	const char *version;
	size_t globals_size;
#ifdef ZTS
	ts_rsrc_id* globals_id_ptr;
#else
	void* globals_ptr;
#endif
	void (*globals_ctor)(void *global);
	void (*globals_dtor)(void *global);
	int (*post_deactivate_func)(void);
	int capi_started;
	unsigned char type;
	void *handle;
	int capi_number;
	const char *build_id;
};

#define CAPI_DEP_REQUIRED		1
#define CAPI_DEP_CONFLICTS	2
#define CAPI_DEP_OPTIONAL		3

#define GEAR_CAPI_REQUIRED_EX(name, rel, ver)	{ name, rel, ver, CAPI_DEP_REQUIRED  },
#define GEAR_CAPI_CONFLICTS_EX(name, rel, ver)	{ name, rel, ver, CAPI_DEP_CONFLICTS },
#define GEAR_CAPI_OPTIONAL_EX(name, rel, ver)	{ name, rel, ver, CAPI_DEP_OPTIONAL  },

#define GEAR_CAPI_REQUIRED(name)		GEAR_CAPI_REQUIRED_EX(name, NULL, NULL)
#define GEAR_CAPI_CONFLICTS(name)	GEAR_CAPI_CONFLICTS_EX(name, NULL, NULL)
#define GEAR_CAPI_OPTIONAL(name)		GEAR_CAPI_OPTIONAL_EX(name, NULL, NULL)

#define GEAR_CAPI_END { NULL, NULL, NULL, 0 }

struct _gear_capi_dep {
	const char *name;		/* cAPI name */
	const char *rel;		/* version relationship: NULL (exists), lt|le|eq|ge|gt (to given version) */
	const char *version;	/* version */
	unsigned char type;		/* dependency type */
};

BEGIN_EXTERN_C()
extern GEAR_API HashTable capi_registry;

void capi_destructor(gear_capi_entry *cAPI);
int capi_registry_request_startup(gear_capi_entry *cAPI);
int capi_registry_unload_temp(const gear_capi_entry *cAPI);
END_EXTERN_C()

#endif

