/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_HASH_H
#define GEAR_HASH_H

#include "gear.h"

#define HASH_KEY_IS_STRING 1
#define HASH_KEY_IS_LONG 2
#define HASH_KEY_NON_EXISTENT 3

#define HASH_UPDATE 			(1<<0)
#define HASH_ADD			(1<<1)
#define HASH_UPDATE_INDIRECT		(1<<2)
#define HASH_ADD_NEW			(1<<3)
#define HASH_ADD_NEXT			(1<<4)

#define HASH_FLAG_CONSISTENCY      ((1<<0) | (1<<1))
#define HASH_FLAG_PACKED           (1<<2)
#define HASH_FLAG_INITIALIZED      (1<<3)
#define HASH_FLAG_STATIC_KEYS      (1<<4) /* long and interned strings */
#define HASH_FLAG_HAS_EMPTY_IND    (1<<5)
#define HASH_FLAG_ALLOW_COW_VIOLATION (1<<6)

/* Only the low byte are real flags */
#define HASH_FLAG_MASK 0xff

#define HT_FLAGS(ht) (ht)->u.flags

#define HT_IS_PACKED(ht) \
	((HT_FLAGS(ht) & HASH_FLAG_PACKED) != 0)

#define HT_IS_WITHOUT_HOLES(ht) \
	((ht)->nNumUsed == (ht)->nNumOfElements)

#define HT_HAS_STATIC_KEYS_ONLY(ht) \
	((HT_FLAGS(ht) & (HASH_FLAG_PACKED|HASH_FLAG_STATIC_KEYS)) != 0)

#if GEAR_DEBUG
# define HT_ALLOW_COW_VIOLATION(ht) HT_FLAGS(ht) |= HASH_FLAG_ALLOW_COW_VIOLATION
#else
# define HT_ALLOW_COW_VIOLATION(ht)
#endif

#define HT_ITERATORS_COUNT(ht) (ht)->u.v.nIteratorsCount
#define HT_ITERATORS_OVERFLOW(ht) (HT_ITERATORS_COUNT(ht) == 0xff)
#define HT_HAS_ITERATORS(ht) (HT_ITERATORS_COUNT(ht) != 0)

#define HT_SET_ITERATORS_COUNT(ht, iters) \
	do { HT_ITERATORS_COUNT(ht) = (iters); } while (0)
#define HT_INC_ITERATORS_COUNT(ht) \
	HT_SET_ITERATORS_COUNT(ht, HT_ITERATORS_COUNT(ht) + 1)
#define HT_DEC_ITERATORS_COUNT(ht) \
	HT_SET_ITERATORS_COUNT(ht, HT_ITERATORS_COUNT(ht) - 1)

extern GEAR_API const HashTable gear_empty_array;

#define ZVAL_EMPTY_ARRAY(z) do {						\
		zval *__z = (z);								\
		Z_ARR_P(__z) = (gear_array*)&gear_empty_array;	\
		Z_TYPE_INFO_P(__z) = IS_ARRAY; \
	} while (0)


typedef struct _gear_hash_key {
	gear_ulong h;
	gear_string *key;
} gear_hash_key;

typedef gear_bool (*merge_checker_func_t)(HashTable *target_ht, zval *source_data, gear_hash_key *hash_key, void *pParam);

BEGIN_EXTERN_C()

/* startup/shutdown */
GEAR_API void GEAR_FASTCALL _gear_hash_init(HashTable *ht, uint32_t nSize, dtor_func_t pDestructor, gear_bool persistent);
GEAR_API void GEAR_FASTCALL gear_hash_destroy(HashTable *ht);
GEAR_API void GEAR_FASTCALL gear_hash_clean(HashTable *ht);

#define gear_hash_init(ht, nSize, pHashFunction, pDestructor, persistent) \
	_gear_hash_init((ht), (nSize), (pDestructor), (persistent))
#define gear_hash_init_ex(ht, nSize, pHashFunction, pDestructor, persistent, bApplyProtection) \
	_gear_hash_init((ht), (nSize), (pDestructor), (persistent))

GEAR_API void GEAR_FASTCALL gear_hash_real_init(HashTable *ht, gear_bool packed);
GEAR_API void GEAR_FASTCALL gear_hash_real_init_packed(HashTable *ht);
GEAR_API void GEAR_FASTCALL gear_hash_real_init_mixed(HashTable *ht);
GEAR_API void GEAR_FASTCALL gear_hash_packed_to_hash(HashTable *ht);
GEAR_API void GEAR_FASTCALL gear_hash_to_packed(HashTable *ht);
GEAR_API void GEAR_FASTCALL gear_hash_extend(HashTable *ht, uint32_t nSize, gear_bool packed);
GEAR_API void GEAR_FASTCALL gear_hash_discard(HashTable *ht, uint32_t nNumUsed);

/* additions/updates/changes */
GEAR_API zval* GEAR_FASTCALL gear_hash_add_or_update(HashTable *ht, gear_string *key, zval *pData, uint32_t flag);
GEAR_API zval* GEAR_FASTCALL gear_hash_update(HashTable *ht, gear_string *key,zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_update_ind(HashTable *ht, gear_string *key,zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_add(HashTable *ht, gear_string *key,zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_add_new(HashTable *ht, gear_string *key,zval *pData);

GEAR_API zval* GEAR_FASTCALL gear_hash_str_add_or_update(HashTable *ht, const char *key, size_t len, zval *pData, uint32_t flag);
GEAR_API zval* GEAR_FASTCALL gear_hash_str_update(HashTable *ht, const char *key, size_t len, zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_str_update_ind(HashTable *ht, const char *key, size_t len, zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_str_add(HashTable *ht, const char *key, size_t len, zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_str_add_new(HashTable *ht, const char *key, size_t len, zval *pData);

GEAR_API zval* GEAR_FASTCALL gear_hash_index_add_or_update(HashTable *ht, gear_ulong h, zval *pData, uint32_t flag);
GEAR_API zval* GEAR_FASTCALL gear_hash_index_add(HashTable *ht, gear_ulong h, zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_index_add_new(HashTable *ht, gear_ulong h, zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_index_update(HashTable *ht, gear_ulong h, zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_next_index_insert(HashTable *ht, zval *pData);
GEAR_API zval* GEAR_FASTCALL gear_hash_next_index_insert_new(HashTable *ht, zval *pData);

GEAR_API zval* GEAR_FASTCALL gear_hash_index_add_empty_element(HashTable *ht, gear_ulong h);
GEAR_API zval* GEAR_FASTCALL gear_hash_add_empty_element(HashTable *ht, gear_string *key);
GEAR_API zval* GEAR_FASTCALL gear_hash_str_add_empty_element(HashTable *ht, const char *key, size_t len);

#define GEAR_HASH_APPLY_KEEP				0
#define GEAR_HASH_APPLY_REMOVE				1<<0
#define GEAR_HASH_APPLY_STOP				1<<1

typedef int (*apply_func_t)(zval *pDest);
typedef int (*apply_func_arg_t)(zval *pDest, void *argument);
typedef int (*apply_func_args_t)(zval *pDest, int num_args, va_list args, gear_hash_key *hash_key);

GEAR_API void GEAR_FASTCALL gear_hash_graceful_destroy(HashTable *ht);
GEAR_API void GEAR_FASTCALL gear_hash_graceful_reverse_destroy(HashTable *ht);
GEAR_API void GEAR_FASTCALL gear_hash_apply(HashTable *ht, apply_func_t apply_func);
GEAR_API void GEAR_FASTCALL gear_hash_apply_with_argument(HashTable *ht, apply_func_arg_t apply_func, void *);
GEAR_API void gear_hash_apply_with_arguments(HashTable *ht, apply_func_args_t apply_func, int, ...);

/* This function should be used with special care (in other words,
 * it should usually not be used).  When used with the GEAR_HASH_APPLY_STOP
 * return value, it assumes things about the order of the elements in the hash.
 * Also, it does not provide the same kind of reentrancy protection that
 * the standard apply functions do.
 */
GEAR_API void GEAR_FASTCALL gear_hash_reverse_apply(HashTable *ht, apply_func_t apply_func);


/* Deletes */
GEAR_API int GEAR_FASTCALL gear_hash_del(HashTable *ht, gear_string *key);
GEAR_API int GEAR_FASTCALL gear_hash_del_ind(HashTable *ht, gear_string *key);
GEAR_API int GEAR_FASTCALL gear_hash_str_del(HashTable *ht, const char *key, size_t len);
GEAR_API int GEAR_FASTCALL gear_hash_str_del_ind(HashTable *ht, const char *key, size_t len);
GEAR_API int GEAR_FASTCALL gear_hash_index_del(HashTable *ht, gear_ulong h);
GEAR_API void GEAR_FASTCALL gear_hash_del_bucket(HashTable *ht, Bucket *p);

/* Data retreival */
GEAR_API zval* GEAR_FASTCALL gear_hash_find(const HashTable *ht, gear_string *key);
GEAR_API zval* GEAR_FASTCALL gear_hash_str_find(const HashTable *ht, const char *key, size_t len);
GEAR_API zval* GEAR_FASTCALL gear_hash_index_find(const HashTable *ht, gear_ulong h);
GEAR_API zval* GEAR_FASTCALL _gear_hash_index_find(const HashTable *ht, gear_ulong h);

/* The same as gear_hash_find(), but hash value of the key must be already calculated */
GEAR_API zval* GEAR_FASTCALL _gear_hash_find_known_hash(const HashTable *ht, gear_string *key);

static gear_always_inline zval *gear_hash_find_ex(const HashTable *ht, gear_string *key, gear_bool known_hash)
{
	if (known_hash) {
		return _gear_hash_find_known_hash(ht, key);
	} else {
		return gear_hash_find(ht, key);
	}
}

#define GEAR_HASH_INDEX_FIND(_ht, _h, _ret, _not_found) do { \
		if (EXPECTED(HT_FLAGS(_ht) & HASH_FLAG_PACKED)) { \
			if (EXPECTED((gear_ulong)(_h) < (gear_ulong)(_ht)->nNumUsed)) { \
				_ret = &_ht->arData[_h].val; \
				if (UNEXPECTED(Z_TYPE_P(_ret) == IS_UNDEF)) { \
					goto _not_found; \
				} \
			} else { \
				goto _not_found; \
			} \
		} else { \
			_ret = _gear_hash_index_find(_ht, _h); \
			if (UNEXPECTED(_ret == NULL)) { \
				goto _not_found; \
			} \
		} \
	} while (0)


/* Misc */
GEAR_API gear_bool GEAR_FASTCALL gear_hash_exists(const HashTable *ht, gear_string *key);
GEAR_API gear_bool GEAR_FASTCALL gear_hash_str_exists(const HashTable *ht, const char *str, size_t len);
GEAR_API gear_bool GEAR_FASTCALL gear_hash_index_exists(const HashTable *ht, gear_ulong h);

/* traversing */
GEAR_API HashPosition GEAR_FASTCALL gear_hash_get_current_pos(const HashTable *ht);

#define gear_hash_has_more_elements_ex(ht, pos) \
	(gear_hash_get_current_key_type_ex(ht, pos) == HASH_KEY_NON_EXISTENT ? FAILURE : SUCCESS)
GEAR_API int   GEAR_FASTCALL gear_hash_move_forward_ex(HashTable *ht, HashPosition *pos);
GEAR_API int   GEAR_FASTCALL gear_hash_move_backwards_ex(HashTable *ht, HashPosition *pos);
GEAR_API int   GEAR_FASTCALL gear_hash_get_current_key_ex(const HashTable *ht, gear_string **str_index, gear_ulong *num_index, HashPosition *pos);
GEAR_API void  GEAR_FASTCALL gear_hash_get_current_key_zval_ex(const HashTable *ht, zval *key, HashPosition *pos);
GEAR_API int   GEAR_FASTCALL gear_hash_get_current_key_type_ex(HashTable *ht, HashPosition *pos);
GEAR_API zval* GEAR_FASTCALL gear_hash_get_current_data_ex(HashTable *ht, HashPosition *pos);
GEAR_API void  GEAR_FASTCALL gear_hash_internal_pointer_reset_ex(HashTable *ht, HashPosition *pos);
GEAR_API void  GEAR_FASTCALL gear_hash_internal_pointer_end_ex(HashTable *ht, HashPosition *pos);

#define gear_hash_has_more_elements(ht) \
	gear_hash_has_more_elements_ex(ht, &(ht)->nInternalPointer)
#define gear_hash_move_forward(ht) \
	gear_hash_move_forward_ex(ht, &(ht)->nInternalPointer)
#define gear_hash_move_backwards(ht) \
	gear_hash_move_backwards_ex(ht, &(ht)->nInternalPointer)
#define gear_hash_get_current_key(ht, str_index, num_index) \
	gear_hash_get_current_key_ex(ht, str_index, num_index, &(ht)->nInternalPointer)
#define gear_hash_get_current_key_zval(ht, key) \
	gear_hash_get_current_key_zval_ex(ht, key, &(ht)->nInternalPointer)
#define gear_hash_get_current_key_type(ht) \
	gear_hash_get_current_key_type_ex(ht, &(ht)->nInternalPointer)
#define gear_hash_get_current_data(ht) \
	gear_hash_get_current_data_ex(ht, &(ht)->nInternalPointer)
#define gear_hash_internal_pointer_reset(ht) \
	gear_hash_internal_pointer_reset_ex(ht, &(ht)->nInternalPointer)
#define gear_hash_internal_pointer_end(ht) \
	gear_hash_internal_pointer_end_ex(ht, &(ht)->nInternalPointer)

/* Copying, merging and sorting */
GEAR_API void  GEAR_FASTCALL gear_hash_copy(HashTable *target, HashTable *source, copy_ctor_func_t pCopyConstructor);
GEAR_API void  GEAR_FASTCALL gear_hash_merge(HashTable *target, HashTable *source, copy_ctor_func_t pCopyConstructor, gear_bool overwrite);
GEAR_API void  GEAR_FASTCALL gear_hash_merge_ex(HashTable *target, HashTable *source, copy_ctor_func_t pCopyConstructor, merge_checker_func_t pMergeSource, void *pParam);
GEAR_API void  gear_hash_bucket_swap(Bucket *p, Bucket *q);
GEAR_API void  gear_hash_bucket_renum_swap(Bucket *p, Bucket *q);
GEAR_API void  gear_hash_bucket_packed_swap(Bucket *p, Bucket *q);
GEAR_API int   gear_hash_compare(HashTable *ht1, HashTable *ht2, compare_func_t compar, gear_bool ordered);
GEAR_API int   GEAR_FASTCALL gear_hash_sort_ex(HashTable *ht, sort_func_t sort_func, compare_func_t compare_func, gear_bool renumber);
GEAR_API zval* GEAR_FASTCALL gear_hash_minmax(const HashTable *ht, compare_func_t compar, uint32_t flag);

#define gear_hash_sort(ht, compare_func, renumber) \
	gear_hash_sort_ex(ht, gear_sort, compare_func, renumber)

#define gear_hash_num_elements(ht) \
	(ht)->nNumOfElements

#define gear_hash_next_free_element(ht) \
	(ht)->nNextFreeElement

GEAR_API int GEAR_FASTCALL gear_hash_rehash(HashTable *ht);

#if !GEAR_DEBUG && defined(HAVE_BUILTIN_CONSTANT_P)
# define gear_new_array(size) \
	(__builtin_constant_p(size) ? \
		((((uint32_t)(size)) <= HT_MIN_SIZE) ? \
			_gear_new_array_0() \
		: \
			_gear_new_array((size)) \
		) \
	: \
		_gear_new_array((size)) \
	)
#else
# define gear_new_array(size) \
	_gear_new_array(size)
#endif

GEAR_API HashTable* GEAR_FASTCALL _gear_new_array_0(void);
GEAR_API HashTable* GEAR_FASTCALL _gear_new_array(uint32_t size);
GEAR_API uint32_t gear_array_count(HashTable *ht);
GEAR_API HashTable* GEAR_FASTCALL gear_array_dup(HashTable *source);
GEAR_API void GEAR_FASTCALL gear_array_destroy(HashTable *ht);
GEAR_API void GEAR_FASTCALL gear_symtable_clean(HashTable *ht);
GEAR_API HashTable* GEAR_FASTCALL gear_symtable_to_proptable(HashTable *ht);
GEAR_API HashTable* GEAR_FASTCALL gear_proptable_to_symtable(HashTable *ht, gear_bool always_duplicate);

GEAR_API int GEAR_FASTCALL _gear_handle_numeric_str_ex(const char *key, size_t length, gear_ulong *idx);

GEAR_API uint32_t     GEAR_FASTCALL gear_hash_iterator_add(HashTable *ht, HashPosition pos);
GEAR_API HashPosition GEAR_FASTCALL gear_hash_iterator_pos(uint32_t idx, HashTable *ht);
GEAR_API HashPosition GEAR_FASTCALL gear_hash_iterator_pos_ex(uint32_t idx, zval *array);
GEAR_API void         GEAR_FASTCALL gear_hash_iterator_del(uint32_t idx);
GEAR_API HashPosition GEAR_FASTCALL gear_hash_iterators_lower_pos(HashTable *ht, HashPosition start);
GEAR_API void         GEAR_FASTCALL _gear_hash_iterators_update(HashTable *ht, HashPosition from, HashPosition to);
GEAR_API void         GEAR_FASTCALL gear_hash_iterators_advance(HashTable *ht, HashPosition step);

static gear_always_inline void gear_hash_iterators_update(HashTable *ht, HashPosition from, HashPosition to)
{
	if (UNEXPECTED(HT_HAS_ITERATORS(ht))) {
		_gear_hash_iterators_update(ht, from, to);
	}
}


END_EXTERN_C()

#define GEAR_INIT_SYMTABLE(ht)								\
	GEAR_INIT_SYMTABLE_EX(ht, 8, 0)

#define GEAR_INIT_SYMTABLE_EX(ht, n, persistent)			\
	gear_hash_init(ht, n, NULL, ZVAL_PTR_DTOR, persistent)

static gear_always_inline int _gear_handle_numeric_str(const char *key, size_t length, gear_ulong *idx)
{
	const char *tmp = key;

	if (EXPECTED(*tmp > '9')) {
		return 0;
	} else if (*tmp < '0') {
		if (*tmp != '-') {
			return 0;
		}
		tmp++;
		if (*tmp > '9' || *tmp < '0') {
			return 0;
		}
	}
	return _gear_handle_numeric_str_ex(key, length, idx);
}

#define GEAR_HANDLE_NUMERIC_STR(key, length, idx) \
	_gear_handle_numeric_str(key, length, &idx)

#define GEAR_HANDLE_NUMERIC(key, idx) \
	GEAR_HANDLE_NUMERIC_STR(ZSTR_VAL(key), ZSTR_LEN(key), idx)


static gear_always_inline zval *gear_hash_find_ind(const HashTable *ht, gear_string *key)
{
	zval *zv;

	zv = gear_hash_find(ht, key);
	return (zv && Z_TYPE_P(zv) == IS_INDIRECT) ?
		((Z_TYPE_P(Z_INDIRECT_P(zv)) != IS_UNDEF) ? Z_INDIRECT_P(zv) : NULL) : zv;
}


static gear_always_inline zval *gear_hash_find_ex_ind(const HashTable *ht, gear_string *key, gear_bool known_hash)
{
	zval *zv;

	zv = gear_hash_find_ex(ht, key, known_hash);
	return (zv && Z_TYPE_P(zv) == IS_INDIRECT) ?
		((Z_TYPE_P(Z_INDIRECT_P(zv)) != IS_UNDEF) ? Z_INDIRECT_P(zv) : NULL) : zv;
}


static gear_always_inline int gear_hash_exists_ind(const HashTable *ht, gear_string *key)
{
	zval *zv;

	zv = gear_hash_find(ht, key);
	return zv && (Z_TYPE_P(zv) != IS_INDIRECT ||
			Z_TYPE_P(Z_INDIRECT_P(zv)) != IS_UNDEF);
}


static gear_always_inline zval *gear_hash_str_find_ind(const HashTable *ht, const char *str, size_t len)
{
	zval *zv;

	zv = gear_hash_str_find(ht, str, len);
	return (zv && Z_TYPE_P(zv) == IS_INDIRECT) ?
		((Z_TYPE_P(Z_INDIRECT_P(zv)) != IS_UNDEF) ? Z_INDIRECT_P(zv) : NULL) : zv;
}


static gear_always_inline int gear_hash_str_exists_ind(const HashTable *ht, const char *str, size_t len)
{
	zval *zv;

	zv = gear_hash_str_find(ht, str, len);
	return zv && (Z_TYPE_P(zv) != IS_INDIRECT ||
			Z_TYPE_P(Z_INDIRECT_P(zv)) != IS_UNDEF);
}

static gear_always_inline zval *gear_symtable_add_new(HashTable *ht, gear_string *key, zval *pData)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_add_new(ht, idx, pData);
	} else {
		return gear_hash_add_new(ht, key, pData);
	}
}

static gear_always_inline zval *gear_symtable_update(HashTable *ht, gear_string *key, zval *pData)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_update(ht, idx, pData);
	} else {
		return gear_hash_update(ht, key, pData);
	}
}


static gear_always_inline zval *gear_symtable_update_ind(HashTable *ht, gear_string *key, zval *pData)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_update(ht, idx, pData);
	} else {
		return gear_hash_update_ind(ht, key, pData);
	}
}


static gear_always_inline int gear_symtable_del(HashTable *ht, gear_string *key)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_del(ht, idx);
	} else {
		return gear_hash_del(ht, key);
	}
}


static gear_always_inline int gear_symtable_del_ind(HashTable *ht, gear_string *key)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_del(ht, idx);
	} else {
		return gear_hash_del_ind(ht, key);
	}
}


static gear_always_inline zval *gear_symtable_find(const HashTable *ht, gear_string *key)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_find(ht, idx);
	} else {
		return gear_hash_find(ht, key);
	}
}


static gear_always_inline zval *gear_symtable_find_ind(const HashTable *ht, gear_string *key)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_find(ht, idx);
	} else {
		return gear_hash_find_ind(ht, key);
	}
}


static gear_always_inline int gear_symtable_exists(HashTable *ht, gear_string *key)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_exists(ht, idx);
	} else {
		return gear_hash_exists(ht, key);
	}
}


static gear_always_inline int gear_symtable_exists_ind(HashTable *ht, gear_string *key)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC(key, idx)) {
		return gear_hash_index_exists(ht, idx);
	} else {
		return gear_hash_exists_ind(ht, key);
	}
}


static gear_always_inline zval *gear_symtable_str_update(HashTable *ht, const char *str, size_t len, zval *pData)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC_STR(str, len, idx)) {
		return gear_hash_index_update(ht, idx, pData);
	} else {
		return gear_hash_str_update(ht, str, len, pData);
	}
}


static gear_always_inline zval *gear_symtable_str_update_ind(HashTable *ht, const char *str, size_t len, zval *pData)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC_STR(str, len, idx)) {
		return gear_hash_index_update(ht, idx, pData);
	} else {
		return gear_hash_str_update_ind(ht, str, len, pData);
	}
}


static gear_always_inline int gear_symtable_str_del(HashTable *ht, const char *str, size_t len)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC_STR(str, len, idx)) {
		return gear_hash_index_del(ht, idx);
	} else {
		return gear_hash_str_del(ht, str, len);
	}
}


static gear_always_inline int gear_symtable_str_del_ind(HashTable *ht, const char *str, size_t len)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC_STR(str, len, idx)) {
		return gear_hash_index_del(ht, idx);
	} else {
		return gear_hash_str_del_ind(ht, str, len);
	}
}


static gear_always_inline zval *gear_symtable_str_find(HashTable *ht, const char *str, size_t len)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC_STR(str, len, idx)) {
		return gear_hash_index_find(ht, idx);
	} else {
		return gear_hash_str_find(ht, str, len);
	}
}


static gear_always_inline int gear_symtable_str_exists(HashTable *ht, const char *str, size_t len)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC_STR(str, len, idx)) {
		return gear_hash_index_exists(ht, idx);
	} else {
		return gear_hash_str_exists(ht, str, len);
	}
}

static gear_always_inline void *gear_hash_add_ptr(HashTable *ht, gear_string *key, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_add(ht, key, &tmp);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline void *gear_hash_add_new_ptr(HashTable *ht, gear_string *key, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_add_new(ht, key, &tmp);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline void *gear_hash_str_add_ptr(HashTable *ht, const char *str, size_t len, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_str_add(ht, str, len, &tmp);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline void *gear_hash_str_add_new_ptr(HashTable *ht, const char *str, size_t len, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_str_add_new(ht, str, len, &tmp);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline void *gear_hash_update_ptr(HashTable *ht, gear_string *key, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_update(ht, key, &tmp);
	GEAR_ASSUME(Z_PTR_P(zv));
	return Z_PTR_P(zv);
}

static gear_always_inline void *gear_hash_str_update_ptr(HashTable *ht, const char *str, size_t len, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_str_update(ht, str, len, &tmp);
	GEAR_ASSUME(Z_PTR_P(zv));
	return Z_PTR_P(zv);
}

static gear_always_inline void *gear_hash_add_mem(HashTable *ht, gear_string *key, void *pData, size_t size)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, NULL);
	if ((zv = gear_hash_add(ht, key, &tmp))) {
		Z_PTR_P(zv) = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
		memcpy(Z_PTR_P(zv), pData, size);
		return Z_PTR_P(zv);
	}
	return NULL;
}

static gear_always_inline void *gear_hash_add_new_mem(HashTable *ht, gear_string *key, void *pData, size_t size)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, NULL);
	if ((zv = gear_hash_add_new(ht, key, &tmp))) {
		Z_PTR_P(zv) = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
		memcpy(Z_PTR_P(zv), pData, size);
		return Z_PTR_P(zv);
	}
	return NULL;
}

static gear_always_inline void *gear_hash_str_add_mem(HashTable *ht, const char *str, size_t len, void *pData, size_t size)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, NULL);
	if ((zv = gear_hash_str_add(ht, str, len, &tmp))) {
		Z_PTR_P(zv) = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
		memcpy(Z_PTR_P(zv), pData, size);
		return Z_PTR_P(zv);
	}
	return NULL;
}

static gear_always_inline void *gear_hash_str_add_new_mem(HashTable *ht, const char *str, size_t len, void *pData, size_t size)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, NULL);
	if ((zv = gear_hash_str_add_new(ht, str, len, &tmp))) {
		Z_PTR_P(zv) = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
		memcpy(Z_PTR_P(zv), pData, size);
		return Z_PTR_P(zv);
	}
	return NULL;
}

static gear_always_inline void *gear_hash_update_mem(HashTable *ht, gear_string *key, void *pData, size_t size)
{
	void *p;

	p = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
	memcpy(p, pData, size);
	return gear_hash_update_ptr(ht, key, p);
}

static gear_always_inline void *gear_hash_str_update_mem(HashTable *ht, const char *str, size_t len, void *pData, size_t size)
{
	void *p;

	p = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
	memcpy(p, pData, size);
	return gear_hash_str_update_ptr(ht, str, len, p);
}

static gear_always_inline void *gear_hash_index_add_ptr(HashTable *ht, gear_ulong h, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_index_add(ht, h, &tmp);
	return zv ? Z_PTR_P(zv) : NULL;
}

static gear_always_inline void *gear_hash_index_add_new_ptr(HashTable *ht, gear_ulong h, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_index_add_new(ht, h, &tmp);
	return zv ? Z_PTR_P(zv) : NULL;
}

static gear_always_inline void *gear_hash_index_update_ptr(HashTable *ht, gear_ulong h, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_index_update(ht, h, &tmp);
	GEAR_ASSUME(Z_PTR_P(zv));
	return Z_PTR_P(zv);
}

static gear_always_inline void *gear_hash_index_add_mem(HashTable *ht, gear_ulong h, void *pData, size_t size)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, NULL);
	if ((zv = gear_hash_index_add(ht, h, &tmp))) {
		Z_PTR_P(zv) = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
		memcpy(Z_PTR_P(zv), pData, size);
		return Z_PTR_P(zv);
	}
	return NULL;
}

static gear_always_inline void *gear_hash_next_index_insert_ptr(HashTable *ht, void *pData)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, pData);
	zv = gear_hash_next_index_insert(ht, &tmp);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline void *gear_hash_index_update_mem(HashTable *ht, gear_ulong h, void *pData, size_t size)
{
	void *p;

	p = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
	memcpy(p, pData, size);
	return gear_hash_index_update_ptr(ht, h, p);
}

static gear_always_inline void *gear_hash_next_index_insert_mem(HashTable *ht, void *pData, size_t size)
{
	zval tmp, *zv;

	ZVAL_PTR(&tmp, NULL);
	if ((zv = gear_hash_next_index_insert(ht, &tmp))) {
		Z_PTR_P(zv) = pemalloc(size, GC_FLAGS(ht) & IS_ARRAY_PERSISTENT);
		memcpy(Z_PTR_P(zv), pData, size);
		return Z_PTR_P(zv);
	}
	return NULL;
}

static gear_always_inline void *gear_hash_find_ptr(const HashTable *ht, gear_string *key)
{
	zval *zv;

	zv = gear_hash_find(ht, key);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline void *gear_hash_find_ex_ptr(const HashTable *ht, gear_string *key, gear_bool known_hash)
{
	zval *zv;

	zv = gear_hash_find_ex(ht, key, known_hash);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline void *gear_hash_str_find_ptr(const HashTable *ht, const char *str, size_t len)
{
	zval *zv;

	zv = gear_hash_str_find(ht, str, len);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline void *gear_hash_index_find_ptr(const HashTable *ht, gear_ulong h)
{
	zval *zv;

	zv = gear_hash_index_find(ht, h);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

static gear_always_inline zval *gear_hash_index_find_deref(HashTable *ht, gear_ulong h)
{
	zval *zv = gear_hash_index_find(ht, h);
	if (zv) {
		ZVAL_DEREF(zv);
	}
	return zv;
}

static gear_always_inline zval *gear_hash_find_deref(HashTable *ht, gear_string *str)
{
	zval *zv = gear_hash_find(ht, str);
	if (zv) {
		ZVAL_DEREF(zv);
	}
	return zv;
}

static gear_always_inline zval *gear_hash_str_find_deref(HashTable *ht, const char *str, size_t len)
{
	zval *zv = gear_hash_str_find(ht, str, len);
	if (zv) {
		ZVAL_DEREF(zv);
	}
	return zv;
}

static gear_always_inline void *gear_symtable_str_find_ptr(HashTable *ht, const char *str, size_t len)
{
	gear_ulong idx;

	if (GEAR_HANDLE_NUMERIC_STR(str, len, idx)) {
		return gear_hash_index_find_ptr(ht, idx);
	} else {
		return gear_hash_str_find_ptr(ht, str, len);
	}
}

static gear_always_inline void *gear_hash_get_current_data_ptr_ex(HashTable *ht, HashPosition *pos)
{
	zval *zv;

	zv = gear_hash_get_current_data_ex(ht, pos);
	if (zv) {
		GEAR_ASSUME(Z_PTR_P(zv));
		return Z_PTR_P(zv);
	} else {
		return NULL;
	}
}

#define gear_hash_get_current_data_ptr(ht) \
	gear_hash_get_current_data_ptr_ex(ht, &(ht)->nInternalPointer)

#define GEAR_HASH_FOREACH(_ht, indirect) do { \
		HashTable *__ht = (_ht); \
		Bucket *_p = __ht->arData; \
		Bucket *_end = _p + __ht->nNumUsed; \
		for (; _p != _end; _p++) { \
			zval *_z = &_p->val; \
			if (indirect && Z_TYPE_P(_z) == IS_INDIRECT) { \
				_z = Z_INDIRECT_P(_z); \
			} \
			if (UNEXPECTED(Z_TYPE_P(_z) == IS_UNDEF)) continue;

#define GEAR_HASH_REVERSE_FOREACH(_ht, indirect) do { \
		HashTable *__ht = (_ht); \
		uint32_t _idx = __ht->nNumUsed; \
		Bucket *_p = __ht->arData + _idx; \
		zval *_z; \
		for (_idx = __ht->nNumUsed; _idx > 0; _idx--) { \
			_p--; \
			_z = &_p->val; \
			if (indirect && Z_TYPE_P(_z) == IS_INDIRECT) { \
				_z = Z_INDIRECT_P(_z); \
			} \
			if (UNEXPECTED(Z_TYPE_P(_z) == IS_UNDEF)) continue;

#define GEAR_HASH_FOREACH_END() \
		} \
	} while (0)

#define GEAR_HASH_FOREACH_END_DEL() \
			__ht->nNumOfElements--; \
			do { \
				uint32_t j = HT_IDX_TO_HASH(_idx - 1); \
				uint32_t nIndex = _p->h | __ht->nTableMask; \
				uint32_t i = HT_HASH(__ht, nIndex); \
				if (UNEXPECTED(j != i)) { \
					Bucket *prev = HT_HASH_TO_BUCKET(__ht, i); \
					while (Z_NEXT(prev->val) != j) { \
						i = Z_NEXT(prev->val); \
						prev = HT_HASH_TO_BUCKET(__ht, i); \
					} \
					Z_NEXT(prev->val) = Z_NEXT(_p->val); \
				} else { \
					HT_HASH(__ht, nIndex) = Z_NEXT(_p->val); \
				} \
			} while (0); \
		} \
		__ht->nNumUsed = _idx; \
	} while (0)

#define GEAR_HASH_FOREACH_BUCKET(ht, _bucket) \
	GEAR_HASH_FOREACH(ht, 0); \
	_bucket = _p;

#define GEAR_HASH_FOREACH_VAL(ht, _val) \
	GEAR_HASH_FOREACH(ht, 0); \
	_val = _z;

#define GEAR_HASH_FOREACH_VAL_IND(ht, _val) \
	GEAR_HASH_FOREACH(ht, 1); \
	_val = _z;

#define GEAR_HASH_FOREACH_PTR(ht, _ptr) \
	GEAR_HASH_FOREACH(ht, 0); \
	_ptr = Z_PTR_P(_z);

#define GEAR_HASH_FOREACH_NUM_KEY(ht, _h) \
	GEAR_HASH_FOREACH(ht, 0); \
	_h = _p->h;

#define GEAR_HASH_FOREACH_STR_KEY(ht, _key) \
	GEAR_HASH_FOREACH(ht, 0); \
	_key = _p->key;

#define GEAR_HASH_FOREACH_KEY(ht, _h, _key) \
	GEAR_HASH_FOREACH(ht, 0); \
	_h = _p->h; \
	_key = _p->key;

#define GEAR_HASH_FOREACH_NUM_KEY_VAL(ht, _h, _val) \
	GEAR_HASH_FOREACH(ht, 0); \
	_h = _p->h; \
	_val = _z;

#define GEAR_HASH_FOREACH_STR_KEY_VAL(ht, _key, _val) \
	GEAR_HASH_FOREACH(ht, 0); \
	_key = _p->key; \
	_val = _z;

#define GEAR_HASH_FOREACH_KEY_VAL(ht, _h, _key, _val) \
	GEAR_HASH_FOREACH(ht, 0); \
	_h = _p->h; \
	_key = _p->key; \
	_val = _z;

#define GEAR_HASH_FOREACH_STR_KEY_VAL_IND(ht, _key, _val) \
	GEAR_HASH_FOREACH(ht, 1); \
	_key = _p->key; \
	_val = _z;

#define GEAR_HASH_FOREACH_KEY_VAL_IND(ht, _h, _key, _val) \
	GEAR_HASH_FOREACH(ht, 1); \
	_h = _p->h; \
	_key = _p->key; \
	_val = _z;

#define GEAR_HASH_FOREACH_NUM_KEY_PTR(ht, _h, _ptr) \
	GEAR_HASH_FOREACH(ht, 0); \
	_h = _p->h; \
	_ptr = Z_PTR_P(_z);

#define GEAR_HASH_FOREACH_STR_KEY_PTR(ht, _key, _ptr) \
	GEAR_HASH_FOREACH(ht, 0); \
	_key = _p->key; \
	_ptr = Z_PTR_P(_z);

#define GEAR_HASH_FOREACH_KEY_PTR(ht, _h, _key, _ptr) \
	GEAR_HASH_FOREACH(ht, 0); \
	_h = _p->h; \
	_key = _p->key; \
	_ptr = Z_PTR_P(_z);

#define GEAR_HASH_REVERSE_FOREACH_BUCKET(ht, _bucket) \
	GEAR_HASH_REVERSE_FOREACH(ht, 0); \
	_bucket = _p;

#define GEAR_HASH_REVERSE_FOREACH_VAL(ht, _val) \
	GEAR_HASH_REVERSE_FOREACH(ht, 0); \
	_val = _z;

#define GEAR_HASH_REVERSE_FOREACH_PTR(ht, _ptr) \
	GEAR_HASH_REVERSE_FOREACH(ht, 0); \
	_ptr = Z_PTR_P(_z);

#define GEAR_HASH_REVERSE_FOREACH_VAL_IND(ht, _val) \
	GEAR_HASH_REVERSE_FOREACH(ht, 1); \
	_val = _z;

#define GEAR_HASH_REVERSE_FOREACH_STR_KEY_VAL(ht, _key, _val) \
	GEAR_HASH_REVERSE_FOREACH(ht, 0); \
	_key = _p->key; \
	_val = _z;

#define GEAR_HASH_REVERSE_FOREACH_KEY_VAL(ht, _h, _key, _val) \
	GEAR_HASH_REVERSE_FOREACH(ht, 0); \
	_h = _p->h; \
	_key = _p->key; \
	_val = _z;

#define GEAR_HASH_REVERSE_FOREACH_KEY_VAL_IND(ht, _h, _key, _val) \
	GEAR_HASH_REVERSE_FOREACH(ht, 1); \
	_h = _p->h; \
	_key = _p->key; \
	_val = _z;

/* The following macros are useful to insert a sequence of new elements
 * of packed array. They may be use insted of series of
 * gear_hash_next_index_insert_new()
 * (HashTable must have enough free buckets).
 */
#define GEAR_HASH_FILL_PACKED(ht) do { \
		HashTable *__fill_ht = (ht); \
		Bucket *__fill_bkt = __fill_ht->arData + __fill_ht->nNumUsed; \
		uint32_t __fill_idx = __fill_ht->nNumUsed; \
		GEAR_ASSERT(HT_FLAGS(__fill_ht) & HASH_FLAG_PACKED);

#define GEAR_HASH_FILL_ADD(_val) do { \
		ZVAL_COPY_VALUE(&__fill_bkt->val, _val); \
		__fill_bkt->h = (__fill_idx); \
		__fill_bkt->key = NULL; \
		__fill_bkt++; \
		__fill_idx++; \
	} while (0)

#define GEAR_HASH_FILL_END() \
		__fill_ht->nNumUsed = __fill_idx; \
		__fill_ht->nNumOfElements = __fill_idx; \
		__fill_ht->nNextFreeElement = __fill_idx; \
		__fill_ht->nInternalPointer = 0; \
	} while (0)

static gear_always_inline zval *_gear_hash_append_ex(HashTable *ht, gear_string *key, zval *zv, int interned)
{
	uint32_t idx = ht->nNumUsed++;
	uint32_t nIndex;
	Bucket *p = ht->arData + idx;

	ZVAL_COPY_VALUE(&p->val, zv);
	if (!interned && !ZSTR_IS_INTERNED(key)) {
		HT_FLAGS(ht) &= ~HASH_FLAG_STATIC_KEYS;
		gear_string_addref(key);
		gear_string_hash_val(key);
	}
	p->key = key;
	p->h = ZSTR_H(key);
	nIndex = (uint32_t)p->h | ht->nTableMask;
	Z_NEXT(p->val) = HT_HASH(ht, nIndex);
	HT_HASH(ht, nIndex) = HT_IDX_TO_HASH(idx);
	ht->nNumOfElements++;
	return &p->val;
}

static gear_always_inline zval *_gear_hash_append(HashTable *ht, gear_string *key, zval *zv)
{
	return _gear_hash_append_ex(ht, key, zv, 0);
}

static gear_always_inline zval *_gear_hash_append_ptr_ex(HashTable *ht, gear_string *key, void *ptr, int interned)
{
	uint32_t idx = ht->nNumUsed++;
	uint32_t nIndex;
	Bucket *p = ht->arData + idx;

	ZVAL_PTR(&p->val, ptr);
	if (!interned && !ZSTR_IS_INTERNED(key)) {
		HT_FLAGS(ht) &= ~HASH_FLAG_STATIC_KEYS;
		gear_string_addref(key);
		gear_string_hash_val(key);
	}
	p->key = key;
	p->h = ZSTR_H(key);
	nIndex = (uint32_t)p->h | ht->nTableMask;
	Z_NEXT(p->val) = HT_HASH(ht, nIndex);
	HT_HASH(ht, nIndex) = HT_IDX_TO_HASH(idx);
	ht->nNumOfElements++;
	return &p->val;
}

static gear_always_inline zval *_gear_hash_append_ptr(HashTable *ht, gear_string *key, void *ptr)
{
	return _gear_hash_append_ptr_ex(ht, key, ptr, 0);
}

static gear_always_inline void _gear_hash_append_ind(HashTable *ht, gear_string *key, zval *ptr)
{
	uint32_t idx = ht->nNumUsed++;
	uint32_t nIndex;
	Bucket *p = ht->arData + idx;

	ZVAL_INDIRECT(&p->val, ptr);
	if (!ZSTR_IS_INTERNED(key)) {
		HT_FLAGS(ht) &= ~HASH_FLAG_STATIC_KEYS;
		gear_string_addref(key);
		gear_string_hash_val(key);
	}
	p->key = key;
	p->h = ZSTR_H(key);
	nIndex = (uint32_t)p->h | ht->nTableMask;
	Z_NEXT(p->val) = HT_HASH(ht, nIndex);
	HT_HASH(ht, nIndex) = HT_IDX_TO_HASH(idx);
	ht->nNumOfElements++;
}

#endif		/* GEAR_HASH_H */
