/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_ALLOC_H
#define GEAR_ALLOC_H

#include <stdio.h>

#include "../hypbc/hypbc.h"
#include "gear.h"

#ifndef GEAR_MM_ALIGNMENT
# define GEAR_MM_ALIGNMENT Z_L(8)
# define GEAR_MM_ALIGNMENT_LOG2 Z_L(3)
#elif GEAR_MM_ALIGNMENT < 4
# undef GEAR_MM_ALIGNMENT
# undef GEAR_MM_ALIGNMENT_LOG2
# define GEAR_MM_ALIGNMENT Z_L(4)
# define GEAR_MM_ALIGNMENT_LOG2 Z_L(2)
#endif

#define GEAR_MM_ALIGNMENT_MASK ~(GEAR_MM_ALIGNMENT - Z_L(1))

#define GEAR_MM_ALIGNED_SIZE(size)	(((size) + GEAR_MM_ALIGNMENT - Z_L(1)) & GEAR_MM_ALIGNMENT_MASK)

#define GEAR_MM_ALIGNED_SIZE_EX(size, alignment) \
	(((size) + ((alignment) - Z_L(1))) & ~((alignment) - Z_L(1)))

typedef struct _gear_leak_info {
	void *addr;
	size_t size;
	const char *filename;
	const char *orig_filename;
	uint32_t lineno;
	uint32_t orig_lineno;
} gear_leak_info;

#if GEAR_DEBUG
typedef struct _gear_mm_debug_info {
	size_t             size;
	const char        *filename;
	const char        *orig_filename;
	uint32_t               lineno;
	uint32_t               orig_lineno;
} gear_mm_debug_info;

# define GEAR_MM_OVERHEAD GEAR_MM_ALIGNED_SIZE(sizeof(gear_mm_debug_info))
#else
# define GEAR_MM_OVERHEAD 0
#endif

BEGIN_EXTERN_C()

GEAR_API char*  GEAR_FASTCALL gear_strndup(const char *s, size_t length) GEAR_ATTRIBUTE_MALLOC;

GEAR_API void*  GEAR_FASTCALL _emalloc(size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) GEAR_ATTRIBUTE_MALLOC GEAR_ATTRIBUTE_ALLOC_SIZE(1);
GEAR_API void*  GEAR_FASTCALL _safe_emalloc(size_t nmemb, size_t size, size_t offset GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) GEAR_ATTRIBUTE_MALLOC;
GEAR_API void*  GEAR_FASTCALL _safe_malloc(size_t nmemb, size_t size, size_t offset) GEAR_ATTRIBUTE_MALLOC;
GEAR_API void   GEAR_FASTCALL _efree(void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
GEAR_API void*  GEAR_FASTCALL _ecalloc(size_t nmemb, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) GEAR_ATTRIBUTE_MALLOC GEAR_ATTRIBUTE_ALLOC_SIZE2(1,2);
GEAR_API void*  GEAR_FASTCALL _erealloc(void *ptr, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) GEAR_ATTRIBUTE_ALLOC_SIZE(2);
GEAR_API void*  GEAR_FASTCALL _erealloc2(void *ptr, size_t size, size_t copy_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) GEAR_ATTRIBUTE_ALLOC_SIZE(2);
GEAR_API void*  GEAR_FASTCALL _safe_erealloc(void *ptr, size_t nmemb, size_t size, size_t offset GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
GEAR_API void*  GEAR_FASTCALL _safe_realloc(void *ptr, size_t nmemb, size_t size, size_t offset);
GEAR_API char*  GEAR_FASTCALL _estrdup(const char *s GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) GEAR_ATTRIBUTE_MALLOC;
GEAR_API char*  GEAR_FASTCALL _estrndup(const char *s, size_t length GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) GEAR_ATTRIBUTE_MALLOC;
GEAR_API size_t GEAR_FASTCALL _gear_mem_block_size(void *ptr GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);

#include "gear_alloc_sizes.h"

/* _emalloc() & _efree() specialization */
#if !GEAR_DEBUG && defined(HAVE_BUILTIN_CONSTANT_P)

# define _GEAR_BIN_ALLOCATOR_DEF(_num, _size, _elements, _pages, x, y) \
	GEAR_API void* GEAR_FASTCALL _emalloc_  ## _size(void) GEAR_ATTRIBUTE_MALLOC;

GEAR_MM_BINS_INFO(_GEAR_BIN_ALLOCATOR_DEF, x, y)

GEAR_API void* GEAR_FASTCALL _emalloc_large(size_t size) GEAR_ATTRIBUTE_MALLOC GEAR_ATTRIBUTE_ALLOC_SIZE(1);
GEAR_API void* GEAR_FASTCALL _emalloc_huge(size_t size) GEAR_ATTRIBUTE_MALLOC GEAR_ATTRIBUTE_ALLOC_SIZE(1);

# define _GEAR_BIN_ALLOCATOR_SELECTOR_START(_num, _size, _elements, _pages, size, y) \
	((size <= _size) ? _emalloc_ ## _size() :
# define _GEAR_BIN_ALLOCATOR_SELECTOR_END(_num, _size, _elements, _pages, size, y) \
	)

# define GEAR_ALLOCATOR(size) \
	GEAR_MM_BINS_INFO(_GEAR_BIN_ALLOCATOR_SELECTOR_START, size, y) \
	((size <= GEAR_MM_MAX_LARGE_SIZE) ? _emalloc_large(size) : _emalloc_huge(size)) \
	GEAR_MM_BINS_INFO(_GEAR_BIN_ALLOCATOR_SELECTOR_END, size, y)

# define _emalloc(size) \
	(__builtin_constant_p(size) ? \
		GEAR_ALLOCATOR(size) \
	: \
		_emalloc(size) \
	)

# define _GEAR_BIN_DEALLOCATOR_DEF(_num, _size, _elements, _pages, x, y) \
	GEAR_API void GEAR_FASTCALL _efree_ ## _size(void *);

GEAR_MM_BINS_INFO(_GEAR_BIN_DEALLOCATOR_DEF, x, y)

GEAR_API void GEAR_FASTCALL _efree_large(void *, size_t size);
GEAR_API void GEAR_FASTCALL _efree_huge(void *, size_t size);

# define _GEAR_BIN_DEALLOCATOR_SELECTOR_START(_num, _size, _elements, _pages, ptr, size) \
	if (size <= _size) { _efree_ ## _size(ptr); } else

# define GEAR_DEALLOCATOR(ptr, size) \
	GEAR_MM_BINS_INFO(_GEAR_BIN_DEALLOCATOR_SELECTOR_START, ptr, size) \
	if (size <= GEAR_MM_MAX_LARGE_SIZE) { _efree_large(ptr, size); } \
	else { _efree_huge(ptr, size); }

# define efree_size(ptr, size) do { \
		if (__builtin_constant_p(size)) { \
			GEAR_DEALLOCATOR(ptr, size) \
		} else { \
			_efree(ptr); \
		} \
	} while (0)
# define efree_size_rel(ptr, size) \
	efree_size(ptr, size)

#else

# define efree_size(ptr, size) \
	efree(ptr)
# define efree_size_rel(ptr, size) \
	efree_rel(ptr)

#define _emalloc_large _emalloc
#define _emalloc_huge  _emalloc
#define _efree_large   _efree
#define _efree_huge    _efree

#endif

/* Standard wrapper macros */
#define emalloc(size)						_emalloc((size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define emalloc_large(size)					_emalloc_large((size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define emalloc_huge(size)					_emalloc_huge((size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define safe_emalloc(nmemb, size, offset)	_safe_emalloc((nmemb), (size), (offset) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define efree(ptr)							_efree((ptr) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define efree_large(ptr)					_efree_large((ptr) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define efree_huge(ptr)						_efree_huge((ptr) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define ecalloc(nmemb, size)				_ecalloc((nmemb), (size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define erealloc(ptr, size)					_erealloc((ptr), (size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define erealloc2(ptr, size, copy_size)		_erealloc2((ptr), (size), (copy_size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define safe_erealloc(ptr, nmemb, size, offset)	_safe_erealloc((ptr), (nmemb), (size), (offset) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define erealloc_recoverable(ptr, size)		_erealloc((ptr), (size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define erealloc2_recoverable(ptr, size, copy_size) _erealloc2((ptr), (size), (copy_size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define estrdup(s)							_estrdup((s) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define estrndup(s, length)					_estrndup((s), (length) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define gear_mem_block_size(ptr)			_gear_mem_block_size((ptr) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)

/* Relay wrapper macros */
#define emalloc_rel(size)						_emalloc((size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define safe_emalloc_rel(nmemb, size, offset)	_safe_emalloc((nmemb), (size), (offset) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define efree_rel(ptr)							_efree((ptr) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define ecalloc_rel(nmemb, size)				_ecalloc((nmemb), (size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define erealloc_rel(ptr, size)					_erealloc((ptr), (size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define erealloc2_rel(ptr, size, copy_size)		_erealloc2((ptr), (size), (copy_size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define erealloc_recoverable_rel(ptr, size)		_erealloc((ptr), (size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define erealloc2_recoverable_rel(ptr, size, copy_size) _erealloc2((ptr), (size), (copy_size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define safe_erealloc_rel(ptr, nmemb, size, offset)	_safe_erealloc((ptr), (nmemb), (size), (offset) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define estrdup_rel(s)							_estrdup((s) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define estrndup_rel(s, length)					_estrndup((s), (length) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define gear_mem_block_size_rel(ptr)			_gear_mem_block_size((ptr) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)

GEAR_API void * __gear_malloc(size_t len) GEAR_ATTRIBUTE_MALLOC GEAR_ATTRIBUTE_ALLOC_SIZE(1);
GEAR_API void * __gear_calloc(size_t nmemb, size_t len) GEAR_ATTRIBUTE_MALLOC GEAR_ATTRIBUTE_ALLOC_SIZE2(1,2);
GEAR_API void * __gear_realloc(void *p, size_t len) GEAR_ATTRIBUTE_ALLOC_SIZE(2);

/* Selective persistent/non persistent allocation macros */
#define pemalloc(size, persistent) ((persistent)?__gear_malloc(size):emalloc(size))
#define safe_pemalloc(nmemb, size, offset, persistent)	((persistent)?_safe_malloc(nmemb, size, offset):safe_emalloc(nmemb, size, offset))
#define pefree(ptr, persistent)  ((persistent)?free(ptr):efree(ptr))
#define pefree_size(ptr, size, persistent)  do { \
		if (persistent) { \
			free(ptr); \
		} else { \
			efree_size(ptr, size);\
		} \
	} while (0)

#define pecalloc(nmemb, size, persistent) ((persistent)?__gear_calloc((nmemb), (size)):ecalloc((nmemb), (size)))
#define perealloc(ptr, size, persistent) ((persistent)?__gear_realloc((ptr), (size)):erealloc((ptr), (size)))
#define perealloc2(ptr, size, copy_size, persistent) ((persistent)?__gear_realloc((ptr), (size)):erealloc2((ptr), (size), (copy_size)))
#define safe_perealloc(ptr, nmemb, size, offset, persistent)	((persistent)?_safe_realloc((ptr), (nmemb), (size), (offset)):safe_erealloc((ptr), (nmemb), (size), (offset)))
#define perealloc_recoverable(ptr, size, persistent) ((persistent)?realloc((ptr), (size)):erealloc_recoverable((ptr), (size)))
#define perealloc2_recoverable(ptr, size, persistent) ((persistent)?realloc((ptr), (size)):erealloc2_recoverable((ptr), (size), (copy_size)))
#define pestrdup(s, persistent) ((persistent)?strdup(s):estrdup(s))
#define pestrndup(s, length, persistent) ((persistent)?gear_strndup((s),(length)):estrndup((s),(length)))

#define pemalloc_rel(size, persistent) ((persistent)?__gear_malloc(size):emalloc_rel(size))
#define pefree_rel(ptr, persistent)	((persistent)?free(ptr):efree_rel(ptr))
#define pecalloc_rel(nmemb, size, persistent) ((persistent)?__gear_calloc((nmemb), (size)):ecalloc_rel((nmemb), (size)))
#define perealloc_rel(ptr, size, persistent) ((persistent)?__gear_realloc((ptr), (size)):erealloc_rel((ptr), (size)))
#define perealloc2_rel(ptr, size, copy_size, persistent) ((persistent)?__gear_realloc((ptr), (size)):erealloc2_rel((ptr), (size), (copy_size)))
#define perealloc_recoverable_rel(ptr, size, persistent) ((persistent)?realloc((ptr), (size)):erealloc_recoverable_rel((ptr), (size)))
#define perealloc2_recoverable_rel(ptr, size, copy_size, persistent) ((persistent)?realloc((ptr), (size)):erealloc2_recoverable_rel((ptr), (size), (copy_size)))
#define pestrdup_rel(s, persistent) ((persistent)?strdup(s):estrdup_rel(s))

GEAR_API int gear_set_memory_limit(size_t memory_limit);

GEAR_API void start_memory_manager(void);
GEAR_API void shutdown_memory_manager(int silent, int full_shutdown);
GEAR_API int is_gear_mm(void);

GEAR_API size_t gear_memory_usage(int real_usage);
GEAR_API size_t gear_memory_peak_usage(int real_usage);

/* fast cache for HashTables */
#define ALLOC_HASHTABLE(ht)	\
	(ht) = (HashTable *) emalloc(sizeof(HashTable))

#define FREE_HASHTABLE(ht)	\
	efree_size(ht, sizeof(HashTable))

#define ALLOC_HASHTABLE_REL(ht)	\
	(ht) = (HashTable *) emalloc_rel(sizeof(HashTable))

#define FREE_HASHTABLE_REL(ht)	\
	efree_size_rel(ht, sizeof(HashTable))

/* Heap functions */
typedef struct _gear_mm_heap gear_mm_heap;

GEAR_API gear_mm_heap *gear_mm_startup(void);
GEAR_API void gear_mm_shutdown(gear_mm_heap *heap, int full_shutdown, int silent);
GEAR_API void*  GEAR_FASTCALL _gear_mm_alloc(gear_mm_heap *heap, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) GEAR_ATTRIBUTE_MALLOC;
GEAR_API void   GEAR_FASTCALL _gear_mm_free(gear_mm_heap *heap, void *p GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
GEAR_API void*  GEAR_FASTCALL _gear_mm_realloc(gear_mm_heap *heap, void *p, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
GEAR_API void*  GEAR_FASTCALL _gear_mm_realloc2(gear_mm_heap *heap, void *p, size_t size, size_t copy_size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);
GEAR_API size_t GEAR_FASTCALL _gear_mm_block_size(gear_mm_heap *heap, void *p GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC);

#define gear_mm_alloc(heap, size)			_gear_mm_alloc((heap), (size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define gear_mm_free(heap, p)				_gear_mm_free((heap), (p) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define gear_mm_realloc(heap, p, size)		_gear_mm_realloc((heap), (p), (size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define gear_mm_realloc2(heap, p, size, copy_size) _gear_mm_realloc2((heap), (p), (size), (copy_size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)
#define gear_mm_block_size(heap, p)			_gear_mm_block_size((heap), (p) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)

#define gear_mm_alloc_rel(heap, size)		_gear_mm_alloc((heap), (size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define gear_mm_free_rel(heap, p)			_gear_mm_free((heap), (p) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define gear_mm_realloc_rel(heap, p, size)	_gear_mm_realloc((heap), (p), (size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define gear_mm_realloc2_rel(heap, p, size, copy_size) _gear_mm_realloc2((heap), (p), (size), (copy_size) GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_CC)
#define gear_mm_block_size_rel(heap, p)		_gear_mm_block_size((heap), (p) GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC)

GEAR_API gear_mm_heap *gear_mm_set_heap(gear_mm_heap *new_heap);
GEAR_API gear_mm_heap *gear_mm_get_heap(void);

GEAR_API size_t gear_mm_gc(gear_mm_heap *heap);

#define GEAR_MM_CUSTOM_HEAP_NONE  0
#define GEAR_MM_CUSTOM_HEAP_STD   1
#define GEAR_MM_CUSTOM_HEAP_DEBUG 2

GEAR_API int gear_mm_is_custom_heap(gear_mm_heap *new_heap);
GEAR_API void gear_mm_set_custom_handlers(gear_mm_heap *heap,
                                          void* (*_malloc)(size_t),
                                          void  (*_free)(void*),
                                          void* (*_realloc)(void*, size_t));
GEAR_API void gear_mm_get_custom_handlers(gear_mm_heap *heap,
                                          void* (**_malloc)(size_t),
                                          void  (**_free)(void*),
                                          void* (**_realloc)(void*, size_t));

#if GEAR_DEBUG
GEAR_API void gear_mm_set_custom_debug_handlers(gear_mm_heap *heap,
                                          void* (*_malloc)(size_t GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC),
                                          void  (*_free)(void* GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC),
                                          void* (*_realloc)(void*, size_t GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC));
#endif

typedef struct _gear_mm_storage gear_mm_storage;

typedef	void* (*gear_mm_chunk_alloc_t)(gear_mm_storage *storage, size_t size, size_t alignment);
typedef void  (*gear_mm_chunk_free_t)(gear_mm_storage *storage, void *chunk, size_t size);
typedef int   (*gear_mm_chunk_truncate_t)(gear_mm_storage *storage, void *chunk, size_t old_size, size_t new_size);
typedef int   (*gear_mm_chunk_extend_t)(gear_mm_storage *storage, void *chunk, size_t old_size, size_t new_size);

typedef struct _gear_mm_handlers {
	gear_mm_chunk_alloc_t       chunk_alloc;
	gear_mm_chunk_free_t        chunk_free;
	gear_mm_chunk_truncate_t    chunk_truncate;
	gear_mm_chunk_extend_t      chunk_extend;
} gear_mm_handlers;

struct _gear_mm_storage {
	const gear_mm_handlers handlers;
	void *data;
};

GEAR_API gear_mm_storage *gear_mm_get_storage(gear_mm_heap *heap);
GEAR_API gear_mm_heap *gear_mm_startup_ex(const gear_mm_handlers *handlers, void *data, size_t data_size);

/*

// The following example shows how to use gear_mm_heap API with custom storage

static gear_mm_heap *apc_heap = NULL;
static HashTable    *apc_ht = NULL;

typedef struct _apc_data {
	void     *mem;
	uint32_t  free_pages;
} apc_data;

static void *apc_chunk_alloc(gear_mm_storage *storage, size_t size, size_t alignment)
{
	apc_data *data = (apc_data*)(storage->data);
	size_t real_size = ((size + (GEAR_MM_CHUNK_SIZE-1)) & ~(GEAR_MM_CHUNK_SIZE-1));
	uint32_t count = real_size / GEAR_MM_CHUNK_SIZE;
	uint32_t first, last, i;

	GEAR_ASSERT(alignment == GEAR_MM_CHUNK_SIZE);

	for (first = 0; first < 32; first++) {
		if (!(data->free_pages & (1 << first))) {
			last = first;
			do {
				if (last - first == count - 1) {
					for (i = first; i <= last; i++) {
						data->free_pages |= (1 << i);
					}
					return (void *)(((char*)(data->mem)) + GEAR_MM_CHUNK_SIZE * (1 << first));
				}
				last++;
			} while (last < 32 && !(data->free_pages & (1 << last)));
			first = last;
		}
	}
	return NULL;
}

static void apc_chunk_free(gear_mm_storage *storage, void *chunk, size_t size)
{
	apc_data *data = (apc_data*)(storage->data);
	uint32_t i;

	GEAR_ASSERT(((uintptr_t)chunk & (GEAR_MM_CHUNK_SIZE - 1)) == 0);

	i = ((uintptr_t)chunk - (uintptr_t)(data->mem)) / GEAR_MM_CHUNK_SIZE;
	while (1) {
		data->free_pages &= ~(1 << i);
		if (size <= GEAR_MM_CHUNK_SIZE) {
			break;
		}
		size -= GEAR_MM_CHUNK_SIZE;
	}
}

static void apc_init_heap(void)
{
	gear_mm_handlers apc_handlers = {
		apc_chunk_alloc,
		apc_chunk_free,
		NULL,
		NULL,
	};
	apc_data tmp_data;
	gear_mm_heap *old_heap;

	// Preallocate properly aligned SHM chunks (64MB)
	tmp_data.mem = shm_memalign(GEAR_MM_CHUNK_SIZE, GEAR_MM_CHUNK_SIZE * 32);

	// Initialize temporary storage data
	tmp_data.free_pages = 0;

	// Create heap
	apc_heap = gear_mm_startup_ex(&apc_handlers, &tmp_data, sizeof(tmp_data));

	// Allocate some data in the heap
	old_heap = gear_mm_set_heap(apc_heap);
	ALLOC_HASHTABLE(apc_ht);
	gear_hash_init(apc_ht, 64, NULL, ZVAL_PTR_DTOR, 0);
	gear_mm_set_heap(old_heap);
}

*/

END_EXTERN_C()

#endif

