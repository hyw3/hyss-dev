/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This is a header file for the strtod implementation by David M. Gay which
 * can be found in gear_strtod.c */
#ifndef GEAR_STRTOD_H
#define GEAR_STRTOD_H
#include <gear.h>

BEGIN_EXTERN_C()
GEAR_API void gear_freedtoa(char *s);
GEAR_API char * gear_dtoa(double _d, int mode, int ndigits, int *decpt, int *sign, char **rve);
GEAR_API double gear_strtod(const char *s00, const char **se);
GEAR_API double gear_hex_strtod(const char *str, const char **endptr);
GEAR_API double gear_oct_strtod(const char *str, const char **endptr);
GEAR_API double gear_bin_strtod(const char *str, const char **endptr);
GEAR_API int gear_startup_strtod(void);
GEAR_API int gear_shutdown_strtod(void);
END_EXTERN_C()

#endif

