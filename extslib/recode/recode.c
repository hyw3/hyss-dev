/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes & prototypes */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_streams.h"

#if HAVE_LIBRECODE

/* For recode 3.5 */
#if HAVE_BROKEN_RECODE
extern char *program_name;
char *program_name = "hyss";
#endif

#ifdef HAVE_STDBOOL_H
# include <stdbool.h>
#else
  typedef enum {false = 0, true = 1} bool;
#endif

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <recode.h>

#include "hyss_recode.h"
#include "extslib/standard/info.h"
#include "extslib/standard/file.h"
#include "extslib/standard/hyss_string.h"

/* }}} */

GEAR_BEGIN_CAPI_GLOBALS(recode)
    RECODE_OUTER  outer;
GEAR_END_CAPI_GLOBALS(recode)

#ifdef ZTS
# define ReSG(v) PBCG(recode_globals_id, gear_recode_globals *, v)
#else
# define ReSG(v) (recode_globals.v)
#endif

GEAR_DECLARE_CAPI_GLOBALS(recode)
static HYSS_GINIT_FUNCTION(recode);

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_recode_string, 0, 0, 2)
	GEAR_ARG_INFO(0, request)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_recode_file, 0, 0, 3)
	GEAR_ARG_INFO(0, request)
	GEAR_ARG_INFO(0, input)
	GEAR_ARG_INFO(0, output)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ cAPI stuff */
static const gear_function_entry hyss_recode_functions[] = {
	HYSS_FE(recode_string, 	arginfo_recode_string)
	HYSS_FE(recode_file, 	arginfo_recode_file)
	HYSS_FALIAS(recode, recode_string, arginfo_recode_string)
	HYSS_FE_END
}; /* }}} */

gear_capi_entry recode_capi_entry = {
	STANDARD_CAPI_HEADER,
	"recode",
 	hyss_recode_functions,
	HYSS_MINIT(recode),
	HYSS_MSHUTDOWN(recode),
	NULL,
	NULL,
	HYSS_MINFO(recode),
	HYSS_RECODE_VERSION,
	HYSS_CAPI_GLOBALS(recode),
	HYSS_GINIT(recode),
	NULL,
	NULL,
	STANDARD_CAPI_PROPERTIES_EX
};

#ifdef COMPILE_DL_RECODE
GEAR_GET_CAPI(recode)
#endif

static HYSS_GINIT_FUNCTION(recode)
{
	recode_globals->outer = NULL;
}

HYSS_MINIT_FUNCTION(recode)
{
	ReSG(outer) = recode_new_outer(false);
	if (ReSG(outer) == NULL) {
		return FAILURE;
	}

	return SUCCESS;
}

HYSS_MSHUTDOWN_FUNCTION(recode)
{
	if (ReSG(outer)) {
		recode_delete_outer(ReSG(outer));
	}
	return SUCCESS;
}

HYSS_MINFO_FUNCTION(recode)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "Recode Support", "enabled");
	hyss_info_print_table_end();
}

/* {{{ proto string recode_string(string request, string str)
   Recode string str according to request string */
HYSS_FUNCTION(recode_string)
{
	RECODE_REQUEST request = NULL;
	char *r = NULL;
	size_t r_len = 0, r_alen = 0;
	size_t req_len, str_len;
	char *req, *str;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss", &req, &req_len, &str, &str_len) == FAILURE) {
		return;
	}

	request = recode_new_request(ReSG(outer));

	if (request == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Cannot allocate request structure");
		RETURN_FALSE;
	}

	if (!recode_scan_request(request, req)) {
		hyss_error_docref(NULL, E_WARNING, "Illegal recode request '%s'", req);
		goto error_exit;
	}

	recode_buffer_to_buffer(request, str, str_len, &r, &r_len, &r_alen);
	if (!r) {
		hyss_error_docref(NULL, E_WARNING, "Recoding failed.");
error_exit:
		RETVAL_FALSE;
	} else {
		RETVAL_STRINGL(r, r_len);
		free(r);
	}

	recode_delete_request(request);

	return;
}
/* }}} */

/* {{{ proto bool recode_file(string request, resource input, resource output)
   Recode file input into file output according to request */
HYSS_FUNCTION(recode_file)
{
	RECODE_REQUEST request = NULL;
	char *req;
	size_t req_len;
	zval *input, *output;
	hyss_stream *instream, *outstream;
	FILE  *in_fp,  *out_fp;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "srr", &req, &req_len, &input, &output) == FAILURE) {
	 	return;
	}

	hyss_stream_from_zval(instream, input);
	hyss_stream_from_zval(outstream, output);

	if (FAILURE == hyss_stream_cast(instream, HYSS_STREAM_AS_STDIO, (void**)&in_fp, REPORT_ERRORS))	{
		RETURN_FALSE;
	}

	if (FAILURE == hyss_stream_cast(outstream, HYSS_STREAM_AS_STDIO, (void**)&out_fp, REPORT_ERRORS))	{
		RETURN_FALSE;
	}

	request = recode_new_request(ReSG(outer));
	if (request == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Cannot allocate request structure");
		RETURN_FALSE;
	}

	if (!recode_scan_request(request, req)) {
		hyss_error_docref(NULL, E_WARNING, "Illegal recode request '%s'", req);
		goto error_exit;
	}

	if (!recode_file_to_file(request, in_fp, out_fp)) {
		hyss_error_docref(NULL, E_WARNING, "Recoding failed.");
		goto error_exit;
	}

	recode_delete_request(request);
	RETURN_TRUE;

error_exit:
	recode_delete_request(request);
	RETURN_FALSE;
}
/* }}} */

#endif

