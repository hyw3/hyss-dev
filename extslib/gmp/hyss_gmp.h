/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_GMP_H
#define HYSS_GMP_H

#include <gmp.h>

extern gear_capi_entry gmp_capi_entry;
#define hyssext_gmp_ptr &gmp_capi_entry

#include "hyss_version.h"
#define HYSS_GMP_VERSION HYSS_VERSION

GEAR_CAPI_STARTUP_D(gmp);
GEAR_CAPI_DEACTIVATE_D(gmp);
GEAR_CAPI_INFO_D(gmp);

GEAR_FUNCTION(gmp_init);
GEAR_FUNCTION(gmp_import);
GEAR_FUNCTION(gmp_export);
GEAR_FUNCTION(gmp_intval);
GEAR_FUNCTION(gmp_strval);
GEAR_FUNCTION(gmp_add);
GEAR_FUNCTION(gmp_sub);
GEAR_FUNCTION(gmp_mul);
GEAR_FUNCTION(gmp_div_qr);
GEAR_FUNCTION(gmp_div_q);
GEAR_FUNCTION(gmp_div_r);
GEAR_FUNCTION(gmp_mod);
GEAR_FUNCTION(gmp_divexact);
GEAR_FUNCTION(gmp_neg);
GEAR_FUNCTION(gmp_abs);
GEAR_FUNCTION(gmp_fact);
GEAR_FUNCTION(gmp_sqrt);
GEAR_FUNCTION(gmp_sqrtrem);
GEAR_FUNCTION(gmp_root);
GEAR_FUNCTION(gmp_rootrem);
GEAR_FUNCTION(gmp_pow);
GEAR_FUNCTION(gmp_powm);
GEAR_FUNCTION(gmp_perfect_square);
GEAR_FUNCTION(gmp_perfect_power);
GEAR_FUNCTION(gmp_prob_prime);
GEAR_FUNCTION(gmp_gcd);
GEAR_FUNCTION(gmp_gcdext);
GEAR_FUNCTION(gmp_invert);
GEAR_FUNCTION(gmp_jacobi);
GEAR_FUNCTION(gmp_legendre);
GEAR_FUNCTION(gmp_kronecker);
GEAR_FUNCTION(gmp_cmp);
GEAR_FUNCTION(gmp_sign);
GEAR_FUNCTION(gmp_and);
GEAR_FUNCTION(gmp_or);
GEAR_FUNCTION(gmp_com);
GEAR_FUNCTION(gmp_xor);
GEAR_FUNCTION(gmp_random);
GEAR_FUNCTION(gmp_random_seed);
GEAR_FUNCTION(gmp_random_bits);
GEAR_FUNCTION(gmp_random_range);
GEAR_FUNCTION(gmp_setbit);
GEAR_FUNCTION(gmp_clrbit);
GEAR_FUNCTION(gmp_scan0);
GEAR_FUNCTION(gmp_scan1);
GEAR_FUNCTION(gmp_testbit);
GEAR_FUNCTION(gmp_popcount);
GEAR_FUNCTION(gmp_hamdist);
GEAR_FUNCTION(gmp_nextprime);
GEAR_FUNCTION(gmp_binomial);
GEAR_FUNCTION(gmp_lcm);

GEAR_BEGIN_CAPI_GLOBALS(gmp)
	gear_bool rand_initialized;
	gmp_randstate_t rand_state;
GEAR_END_CAPI_GLOBALS(gmp)

#define GMPG(v) GEAR_CAPI_GLOBALS_ACCESSOR(gmp, v)

#if defined(ZTS) && defined(COMPILE_DL_GMP)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#endif	/* HYSS_GMP_H */

