HYSS_ARG_WITH(gmp, for GNU MP support,
[  --with-gmp[=DIR]          Include GNU MP support])

if test "$HYSS_GMP" != "no"; then

  MACHINE_INCLUDES=$($CC -dumpmachine)

  for i in $HYSS_GMP /usr/local /usr; do
    test -f $i/include/gmp.h && GMP_DIR=$i && break
    test -f $i/include/$MACHINE_INCLUDES/gmp.h && GMP_DIR=$i && break
  done

  if test -z "$GMP_DIR"; then
    AC_MSG_ERROR(Unable to locate gmp.h)
  fi

  HYSS_CHECK_LIBRARY(gmp, __gmpz_rootrem,
  [],[
    AC_MSG_ERROR([GNU MP Library version 4.2 or greater required.])
  ],[
    -L$GMP_DIR/$HYSS_LIBDIR
  ])

  HYSS_ADD_LIBRARY_WITH_PATH(gmp, $GMP_DIR/$HYSS_LIBDIR, GMP_SHARED_LIBADD)
  HYSS_ADD_INCLUDE($GMP_DIR/include)
  HYSS_INSTALL_HEADERS([extslib/gmp/hyss_gmp_int.h])

  HYSS_NEW_EXTENSION(gmp, gmp.c, $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
  HYSS_SUBST(GMP_SHARED_LIBADD)
  AC_DEFINE(HAVE_GMP, 1, [ ])
fi
