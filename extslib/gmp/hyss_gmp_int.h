/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef incl_HYSS_GMP_INT_H
#define incl_HYSS_GMP_INT_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include <gmp.h>

#ifdef HYSS_WIN32
# define HYSS_GMP_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
# define HYSS_GMP_API __attribute__ ((visibility("default")))
#else
# define HYSS_GMP_API
#endif

typedef struct _gmp_object {
	mpz_t num;
	gear_object std;
} gmp_object;

static inline gmp_object *hyss_gmp_object_from_gear_object(gear_object *zobj) {
	return (gmp_object *)( ((char *)zobj) - XtOffsetOf(gmp_object, std) );
}

HYSS_GMP_API gear_class_entry *hyss_gmp_class_entry();

/* GMP and MPIR use different datatypes on different platforms */
#ifdef HYSS_WIN32
typedef gear_long gmp_long;
typedef gear_ulong gmp_ulong;
#else
typedef long gmp_long;
typedef unsigned long gmp_ulong;
#endif

#endif
