/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_FILEINFO_H
#define HYSS_FILEINFO_H

extern gear_capi_entry fileinfo_capi_entry;
#define hyssext_fileinfo_ptr &fileinfo_capi_entry

#define HYSS_FILEINFO_VERSION HYSS_VERSION

#ifdef HYSS_WIN32
#define HYSS_FILEINFO_API __declspec(dllexport)
#else
#define HYSS_FILEINFO_API
#endif

#ifdef ZTS
#include "hypbc.h"
#endif

HYSS_MINFO_FUNCTION(fileinfo);

HYSS_FUNCTION(finfo_open);
HYSS_FUNCTION(finfo_close);
HYSS_FUNCTION(finfo_set_flags);
HYSS_FUNCTION(finfo_file);
HYSS_FUNCTION(finfo_buffer);
HYSS_FUNCTION(mime_content_type);

#ifdef ZTS
#define FILEINFO_G(v) PBCG(fileinfo_globals_id, gear_fileinfo_globals *, v)
#else
#define FILEINFO_G(v) (fileinfo_globals.v)
#endif

#endif	/* HYSS_FILEINFO_H */

