/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "hyss.h"

#include <magic.h>
/*
 * HOWMANY specifies the maximum offset libmagic will look at
 * this is currently hardcoded in the libmagic source but not exported
 */
#ifndef HOWMANY
#define HOWMANY 65536
#endif

#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "extslib/standard/file.h" /* needed for context stuff */
#include "hyss_fileinfo.h"
#include "fopen_wrappers.h" /* needed for is_url */
#include "Gear/gear_exceptions.h"

/* {{{ macros and type definitions */
typedef struct _hyss_fileinfo {
	gear_long options;
	struct magic_set *magic;
} hyss_fileinfo;

static gear_object_handlers finfo_object_handlers;
gear_class_entry *finfo_class_entry;

typedef struct _finfo_object {
	hyss_fileinfo *ptr;
	gear_object zo;
} finfo_object;

#define FILEINFO_DECLARE_INIT_OBJECT(object) \
	zval *object = GEAR_IS_METHOD_CALL() ? getThis() : NULL;

static inline finfo_object *hyss_finfo_fetch_object(gear_object *obj) {
	return (finfo_object *)((char*)(obj) - XtOffsetOf(finfo_object, zo));
}

#define Z_FINFO_P(zv) hyss_finfo_fetch_object(Z_OBJ_P((zv)))

#define FILEINFO_REGISTER_OBJECT(_object, _ptr) \
{ \
	finfo_object *obj; \
    obj = Z_FINFO_P(_object); \
    obj->ptr = _ptr; \
}

#define FILEINFO_FROM_OBJECT(finfo, object) \
{ \
	finfo_object *obj = Z_FINFO_P(object); \
	finfo = obj->ptr; \
	if (!finfo) { \
        	hyss_error_docref(NULL, E_WARNING, "The invalid fileinfo object."); \
		RETURN_FALSE; \
	} \
}

/* {{{ finfo_objects_free
 */
static void finfo_objects_free(gear_object *object)
{
	finfo_object *intern = hyss_finfo_fetch_object(object);

	if (intern->ptr) {
		magic_close(intern->ptr->magic);
		efree(intern->ptr);
	}

	gear_object_std_dtor(&intern->zo);
}
/* }}} */

/* {{{ finfo_objects_new
 */
HYSS_FILEINFO_API gear_object *finfo_objects_new(gear_class_entry *class_type)
{
	finfo_object *intern;

	intern = gear_object_alloc(sizeof(finfo_object), class_type);

	gear_object_std_init(&intern->zo, class_type);
	object_properties_init(&intern->zo, class_type);
	intern->zo.handlers = &finfo_object_handlers;

	return &intern->zo;
}
/* }}} */

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_finfo_open, 0, 0, 0)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, arg)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_finfo_close, 0, 0, 1)
	GEAR_ARG_INFO(0, finfo)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_finfo_set_flags, 0, 0, 2)
	GEAR_ARG_INFO(0, finfo)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_finfo_method_set_flags, 0, 0, 1)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_finfo_file, 0, 0, 2)
	GEAR_ARG_INFO(0, finfo)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_finfo_method_file, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_finfo_buffer, 0, 0, 2)
	GEAR_ARG_INFO(0, finfo)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_finfo_method_buffer, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mime_content_type, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ finfo_class_functions
 */
static const gear_function_entry finfo_class_functions[] = {
	GEAR_ME_MAPPING(finfo,          finfo_open,     arginfo_finfo_open, GEAR_ACC_PUBLIC)
	GEAR_ME_MAPPING(set_flags,      finfo_set_flags,arginfo_finfo_method_set_flags, GEAR_ACC_PUBLIC)
	GEAR_ME_MAPPING(file,           finfo_file,     arginfo_finfo_method_file, GEAR_ACC_PUBLIC)
	GEAR_ME_MAPPING(buffer,         finfo_buffer,   arginfo_finfo_method_buffer, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};
/* }}} */

#define FINFO_SET_OPTION(magic, options) \
	if (magic_setflags(magic, options) == -1) { \
		hyss_error_docref(NULL, E_WARNING, "Failed to set option '" GEAR_LONG_FMT "' %d:%s", \
				options, magic_errno(magic), magic_error(magic)); \
		RETURN_FALSE; \
	}

/* True global resources - no need for thread safety here */
static int le_fileinfo;
/* }}} */

void finfo_resource_destructor(gear_resource *rsrc) /* {{{ */
{
	if (rsrc->ptr) {
		hyss_fileinfo *finfo = (hyss_fileinfo *) rsrc->ptr;
		magic_close(finfo->magic);
		efree(rsrc->ptr);
		rsrc->ptr = NULL;
	}
}
/* }}} */


/* {{{ fileinfo_functions[]
 */
static const gear_function_entry fileinfo_functions[] = {
	HYSS_FE(finfo_open,		arginfo_finfo_open)
	HYSS_FE(finfo_close,		arginfo_finfo_close)
	HYSS_FE(finfo_set_flags,	arginfo_finfo_set_flags)
	HYSS_FE(finfo_file,		arginfo_finfo_file)
	HYSS_FE(finfo_buffer,	arginfo_finfo_buffer)
	HYSS_FE(mime_content_type, arginfo_mime_content_type)
	HYSS_FE_END
};
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(finfo)
{
	gear_class_entry _finfo_class_entry;
	INIT_CLASS_ENTRY(_finfo_class_entry, "finfo", finfo_class_functions);
	_finfo_class_entry.create_object = finfo_objects_new;
	finfo_class_entry = gear_register_internal_class(&_finfo_class_entry);

	/* copy the standard object handlers to you handler table */
	memcpy(&finfo_object_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	finfo_object_handlers.offset = XtOffsetOf(finfo_object, zo);
	finfo_object_handlers.free_obj = finfo_objects_free;

	le_fileinfo = gear_register_list_destructors_ex(finfo_resource_destructor, NULL, "file_info", capi_number);

	REGISTER_LONG_CONSTANT("FILEINFO_NONE",			MAGIC_NONE, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("FILEINFO_SYMLINK",		MAGIC_SYMLINK, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("FILEINFO_MIME",			MAGIC_MIME, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("FILEINFO_MIME_TYPE",	MAGIC_MIME_TYPE, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("FILEINFO_MIME_ENCODING",MAGIC_MIME_ENCODING, CONST_CS|CONST_PERSISTENT);
/*	REGISTER_LONG_CONSTANT("FILEINFO_COMPRESS",		MAGIC_COMPRESS, CONST_CS|CONST_PERSISTENT); disabled, as it does fork now */
	REGISTER_LONG_CONSTANT("FILEINFO_DEVICES",		MAGIC_DEVICES, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("FILEINFO_CONTINUE",		MAGIC_CONTINUE, CONST_CS|CONST_PERSISTENT);
#ifdef MAGIC_PRESERVE_ATIME
	REGISTER_LONG_CONSTANT("FILEINFO_PRESERVE_ATIME",	MAGIC_PRESERVE_ATIME, CONST_CS|CONST_PERSISTENT);
#endif
#ifdef MAGIC_RAW
	REGISTER_LONG_CONSTANT("FILEINFO_RAW",			MAGIC_RAW, CONST_CS|CONST_PERSISTENT);
#endif
#if 0
	/* seems not usable yet. */
	REGISTER_LONG_CONSTANT("FILEINFO_APPLE",		MAGIC_APPLE, CONST_CS|CONST_PERSISTENT);
#endif
	REGISTER_LONG_CONSTANT("FILEINFO_EXTENSION",	MAGIC_EXTENSION, CONST_CS|CONST_PERSISTENT);

	return SUCCESS;
}
/* }}} */

/* {{{ fileinfo_capi_entry
 */
gear_capi_entry fileinfo_capi_entry = {
	STANDARD_CAPI_HEADER,
	"fileinfo",
	fileinfo_functions,
	HYSS_MINIT(finfo),
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(fileinfo),
	HYSS_FILEINFO_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_FILEINFO
GEAR_GET_CAPI(fileinfo)
#endif

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(fileinfo)
{
	char magic_ver[5];

	(void)snprintf(magic_ver, 4, "%d", magic_version());
	magic_ver[4] = '\0';

	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "fileinfo support", "enabled");
	hyss_info_print_table_row(2, "libmagic", magic_ver);
	hyss_info_print_table_end();
}
/* }}} */

/* {{{ proto resource finfo_open([int options [, string arg]])
   Create a new fileinfo resource. */
HYSS_FUNCTION(finfo_open)
{
	gear_long options = MAGIC_NONE;
	char *file = NULL;
	size_t file_len = 0;
	hyss_fileinfo *finfo;
	FILEINFO_DECLARE_INIT_OBJECT(object)
	char resolved_path[MAXPATHLEN];
	gear_error_handling zeh;
	int flags = object ? GEAR_PARSE_PARAMS_THROW : 0;

	if (gear_parse_parameters_ex(flags, GEAR_NUM_ARGS(), "|lp", &options, &file, &file_len) == FAILURE) {
		RETURN_FALSE;
	}

	if (object) {
		finfo_object *finfo_obj = Z_FINFO_P(object);

		gear_replace_error_handling(EH_THROW, NULL, &zeh);

		if (finfo_obj->ptr) {
			magic_close(finfo_obj->ptr->magic);
			efree(finfo_obj->ptr);
			finfo_obj->ptr = NULL;
		}
	}

	if (file_len == 0) {
		file = NULL;
	} else if (file && *file) { /* user specified file, perform open_basedir checks */

		if (hyss_check_open_basedir(file)) {
			if (object) {
				gear_restore_error_handling(&zeh);
				if (!EG(exception)) {
					gear_throw_exception(NULL, "Constructor failed", 0);
				}
			}
			RETURN_FALSE;
		}
		if (!expand_filepath_with_mode(file, resolved_path, NULL, 0, CWD_EXPAND)) {
			if (object) {
				gear_restore_error_handling(&zeh);
				if (!EG(exception)) {
					gear_throw_exception(NULL, "Constructor failed", 0);
				}
			}
			RETURN_FALSE;
		}
		file = resolved_path;
	}

	finfo = emalloc(sizeof(hyss_fileinfo));

	finfo->options = options;
	finfo->magic = magic_open(options);

	if (finfo->magic == NULL) {
		efree(finfo);
		hyss_error_docref(NULL, E_WARNING, "Invalid mode '" GEAR_LONG_FMT "'.", options);
		if (object) {
			gear_restore_error_handling(&zeh);
			if (!EG(exception)) {
				gear_throw_exception(NULL, "Constructor failed", 0);
			}
		}
		RETURN_FALSE;
	}

	if (magic_load(finfo->magic, file) == -1) {
		hyss_error_docref(NULL, E_WARNING, "Failed to load magic database at '%s'.", file);
		magic_close(finfo->magic);
		efree(finfo);
		if (object) {
			gear_restore_error_handling(&zeh);
			if (!EG(exception)) {
				gear_throw_exception(NULL, "Constructor failed", 0);
			}
		}
		RETURN_FALSE;
	}

	if (object) {
		gear_restore_error_handling(&zeh);
		FILEINFO_REGISTER_OBJECT(object, finfo);
	} else {
		RETURN_RES(gear_register_resource(finfo, le_fileinfo));
	}
}
/* }}} */

/* {{{ proto resource finfo_close(resource finfo)
   Close fileinfo resource. */
HYSS_FUNCTION(finfo_close)
{
	hyss_fileinfo *finfo;
	zval *zfinfo;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &zfinfo) == FAILURE) {
		RETURN_FALSE;
	}

	if ((finfo = (hyss_fileinfo *)gear_fetch_resource(Z_RES_P(zfinfo), "file_info", le_fileinfo)) == NULL) {
		RETURN_FALSE;
	}

	gear_list_close(Z_RES_P(zfinfo));

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool finfo_set_flags(resource finfo, int options)
   Set libmagic configuration options. */
HYSS_FUNCTION(finfo_set_flags)
{
	gear_long options;
	hyss_fileinfo *finfo;
	zval *zfinfo;
	FILEINFO_DECLARE_INIT_OBJECT(object)

	if (object) {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &options) == FAILURE) {
			RETURN_FALSE;
		}
		FILEINFO_FROM_OBJECT(finfo, object);
	} else {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "rl", &zfinfo, &options) == FAILURE) {
			RETURN_FALSE;
		}
		if ((finfo = (hyss_fileinfo *)gear_fetch_resource(Z_RES_P(zfinfo), "file_info", le_fileinfo)) == NULL) {
			RETURN_FALSE;
		}
	}

	FINFO_SET_OPTION(finfo->magic, options)
	finfo->options = options;

	RETURN_TRUE;
}
/* }}} */

#define FILEINFO_MODE_BUFFER 0
#define FILEINFO_MODE_STREAM 1
#define FILEINFO_MODE_FILE 2

static void _hyss_finfo_get_type(INTERNAL_FUNCTION_PARAMETERS, int mode, int mimetype_emu) /* {{{ */
{
	gear_long options = 0;
	char *ret_val = NULL, *buffer = NULL;
	size_t buffer_len;
	hyss_fileinfo *finfo = NULL;
	zval *zfinfo, *zcontext = NULL;
	zval *what;
	char mime_directory[] = "directory";

	struct magic_set *magic = NULL;
	FILEINFO_DECLARE_INIT_OBJECT(object)

	if (mimetype_emu) {

		/* mime_content_type(..) emulation */
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "z", &what) == FAILURE) {
			return;
		}

		switch (Z_TYPE_P(what)) {
			case IS_STRING:
				buffer = Z_STRVAL_P(what);
				buffer_len = Z_STRLEN_P(what);
				mode = FILEINFO_MODE_FILE;
				break;

			case IS_RESOURCE:
				mode = FILEINFO_MODE_STREAM;
				break;

			default:
				hyss_error_docref(NULL, E_WARNING, "Can only process string or stream arguments");
				RETURN_FALSE;
		}

		magic = magic_open(MAGIC_MIME_TYPE);
		if (magic_load(magic, NULL) == -1) {
			hyss_error_docref(NULL, E_WARNING, "Failed to load magic database.");
			goto common;
		}
	} else if (object) {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "s|lr", &buffer, &buffer_len, &options, &zcontext) == FAILURE) {
			RETURN_FALSE;
		}
		FILEINFO_FROM_OBJECT(finfo, object);
		magic = finfo->magic;
	} else {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs|lr", &zfinfo, &buffer, &buffer_len, &options, &zcontext) == FAILURE) {
			RETURN_FALSE;
		}
		if ((finfo = (hyss_fileinfo *)gear_fetch_resource(Z_RES_P(zfinfo), "file_info", le_fileinfo)) == NULL) {
			RETURN_FALSE;
		}
		magic = finfo->magic;
	}

	/* Set options for the current file/buffer. */
	if (options) {
		FINFO_SET_OPTION(magic, options)
	}

	switch (mode) {
		case FILEINFO_MODE_BUFFER:
		{
			ret_val = (char *) magic_buffer(magic, buffer, buffer_len);
			break;
		}

		case FILEINFO_MODE_STREAM:
		{
				hyss_stream *stream;
				gear_off_t streampos;

				hyss_stream_from_zval_no_verify(stream, what);
				if (!stream) {
					goto common;
				}

				streampos = hyss_stream_tell(stream); /* remember stream position for restoration */
				hyss_stream_seek(stream, 0, SEEK_SET);

				ret_val = (char *) magic_stream(magic, stream);

				hyss_stream_seek(stream, streampos, SEEK_SET);
				break;
		}

		case FILEINFO_MODE_FILE:
		{
			/* determine if the file is a local file or remote URL */
			const char *tmp2;
			hyss_stream_wrapper *wrap;
			hyss_stream_statbuf ssb;

			if (buffer == NULL || !*buffer) {
				hyss_error_docref(NULL, E_WARNING, "Empty filename or path");
				RETVAL_FALSE;
				goto clean;
			}
			if (CHECK_NULL_PATH(buffer, buffer_len)) {
				hyss_error_docref(NULL, E_WARNING, "Invalid path");
				RETVAL_FALSE;
				goto clean;
			}

			wrap = hyss_stream_locate_url_wrapper(buffer, &tmp2, 0);

			if (wrap) {
				hyss_stream *stream;
				hyss_stream_context *context = hyss_stream_context_from_zval(zcontext, 0);

#ifdef HYSS_WIN32
				if (hyss_stream_stat_path_ex(buffer, 0, &ssb, context) == SUCCESS) {
					if (ssb.sb.st_mode & S_IFDIR) {
						ret_val = mime_directory;
						goto common;
					}
				}
#endif

				stream = hyss_stream_open_wrapper_ex(buffer, "rb", REPORT_ERRORS, NULL, context);

				if (!stream) {
					RETVAL_FALSE;
					goto clean;
				}

				if (hyss_stream_stat(stream, &ssb) == SUCCESS) {
					if (ssb.sb.st_mode & S_IFDIR) {
						ret_val = mime_directory;
					} else {
						ret_val = (char *)magic_stream(magic, stream);
					}
				}

				hyss_stream_close(stream);
			}
			break;
		}

		default:
			hyss_error_docref(NULL, E_WARNING, "Can only process string or stream arguments");
	}

common:
	if (ret_val) {
		RETVAL_STRING(ret_val);
	} else {
		hyss_error_docref(NULL, E_WARNING, "Failed identify data %d:%s", magic_errno(magic), magic_error(magic));
		RETVAL_FALSE;
	}

clean:
	if (mimetype_emu) {
		magic_close(magic);
	}

	/* Restore options */
	if (options) {
		FINFO_SET_OPTION(magic, finfo->options)
	}
	return;
}
/* }}} */

/* {{{ proto string finfo_file(resource finfo, char *file_name [, int options [, resource context]])
   Return information about a file. */
HYSS_FUNCTION(finfo_file)
{
	_hyss_finfo_get_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, FILEINFO_MODE_FILE, 0);
}
/* }}} */

/* {{{ proto string finfo_buffer(resource finfo, char *string [, int options [, resource context]])
   Return infromation about a string buffer. */
HYSS_FUNCTION(finfo_buffer)
{
	_hyss_finfo_get_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, FILEINFO_MODE_BUFFER, 0);
}
/* }}} */

/* {{{ proto string mime_content_type(string filename|resource stream)
   Return content-type for file */
HYSS_FUNCTION(mime_content_type)
{
	_hyss_finfo_get_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, -1, 1);
}
/* }}} */

