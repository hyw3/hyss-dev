/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_BZ2_H
#define HYSS_BZ2_H

#if HAVE_BZ2

extern gear_capi_entry bz2_capi_entry;
#define hyssext_bz2_ptr &bz2_capi_entry

/* Bzip2 includes */
#include <bzlib.h>

#else
#define hyssext_bz2_ptr NULL
#endif

#ifdef HYSS_WIN32
#	ifdef HYSS_BZ2_EXPORTS
#		define HYSS_BZ2_API __declspec(dllexport)
#	elif defined(COMPILE_DL_BZ2)
#		define HYSS_BZ2_API __declspec(dllimport)
#	else
#		define HYSS_BZ2_API /* nothing special */
#	endif
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define HYSS_BZ2_API __attribute__ ((visibility("default")))
#else
#	define HYSS_BZ2_API
#endif

#include "hyss_version.h"
#define HYSS_BZ2_VERSION HYSS_VERSION

HYSS_BZ2_API hyss_stream *_hyss_stream_bz2open(hyss_stream_wrapper *wrapper, const char *path, const char *mode, int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC);
HYSS_BZ2_API hyss_stream *_hyss_stream_bz2open_from_BZFILE(BZFILE *bz, const char *mode, hyss_stream *innerstream STREAMS_DC);

#define hyss_stream_bz2open_from_BZFILE(bz, mode, innerstream)	_hyss_stream_bz2open_from_BZFILE((bz), (mode), (innerstream) STREAMS_CC)
#define hyss_stream_bz2open(wrapper, path, mode, options, opened_path)	_hyss_stream_bz2open((wrapper), (path), (mode), (options), (opened_path), NULL STREAMS_CC)

extern const hyss_stream_filter_factory hyss_bz2_filter_factory;
extern const hyss_stream_ops hyss_stream_bz2io_ops;
#define HYSS_STREAM_IS_BZIP2	&hyss_stream_bz2io_ops

/* 400kb */
#define HYSS_BZ2_FILTER_DEFAULT_BLOCKSIZE        4

/* BZ2 Internal Default */
#define HYSS_BZ2_FILTER_DEFAULT_WORKFACTOR       0

#endif

