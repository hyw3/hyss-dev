/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_bz2.h"

#if HAVE_BZ2

/* HYSS Includes */
#include "extslib/standard/file.h"
#include "extslib/standard/info.h"
#include "extslib/standard/hyss_string.h"
#include "main/hyss_network.h"

/* for fileno() */
#include <stdio.h>

/* Internal error constants */
#define HYSS_BZ_ERRNO   0
#define HYSS_BZ_ERRSTR  1
#define HYSS_BZ_ERRBOTH 2

static HYSS_MINIT_FUNCTION(bz2);
static HYSS_MSHUTDOWN_FUNCTION(bz2);
static HYSS_MINFO_FUNCTION(bz2);
static HYSS_FUNCTION(bzopen);
static HYSS_FUNCTION(bzread);
static HYSS_FUNCTION(bzerrno);
static HYSS_FUNCTION(bzerrstr);
static HYSS_FUNCTION(bzerror);
static HYSS_FUNCTION(bzcompress);
static HYSS_FUNCTION(bzdecompress);

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_bzread, 0, 0, 1)
	GEAR_ARG_INFO(0, bz)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_bzopen, 0)
	GEAR_ARG_INFO(0, file)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_bzerrno, 0)
	GEAR_ARG_INFO(0, bz)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_bzerrstr, 0)
	GEAR_ARG_INFO(0, bz)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_bzerror, 0)
	GEAR_ARG_INFO(0, bz)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bzcompress, 0, 0, 1)
	GEAR_ARG_INFO(0, source)
	GEAR_ARG_INFO(0, blocksize)
	GEAR_ARG_INFO(0, workfactor)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bzdecompress, 0, 0, 1)
	GEAR_ARG_INFO(0, source)
	GEAR_ARG_INFO(0, small)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bzwrite, 0, 0, 2)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_bzflush, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()
/* }}} */

static const gear_function_entry bz2_functions[] = {
	HYSS_FE(bzopen,       arginfo_bzopen)
	HYSS_FE(bzread,       arginfo_bzread)
	HYSS_FALIAS(bzwrite,   fwrite,		arginfo_bzwrite)
	HYSS_FALIAS(bzflush,   fflush,		arginfo_bzflush)
	HYSS_FALIAS(bzclose,   fclose,		arginfo_bzflush)
	HYSS_FE(bzerrno,      arginfo_bzerrno)
	HYSS_FE(bzerrstr,     arginfo_bzerrstr)
	HYSS_FE(bzerror,      arginfo_bzerror)
	HYSS_FE(bzcompress,   arginfo_bzcompress)
	HYSS_FE(bzdecompress, arginfo_bzdecompress)
	HYSS_FE_END
};

gear_capi_entry bz2_capi_entry = {
	STANDARD_CAPI_HEADER,
	"bz2",
	bz2_functions,
	HYSS_MINIT(bz2),
	HYSS_MSHUTDOWN(bz2),
	NULL,
	NULL,
	HYSS_MINFO(bz2),
	HYSS_BZ2_VERSION,
	STANDARD_CAPI_PROPERTIES
};

#ifdef COMPILE_DL_BZ2
GEAR_GET_CAPI(bz2)
#endif

struct hyss_bz2_stream_data_t {
	BZFILE *bz_file;
	hyss_stream *stream;
};

/* {{{ BZip2 stream implementation */

static size_t hyss_bz2iop_read(hyss_stream *stream, char *buf, size_t count)
{
	struct hyss_bz2_stream_data_t *self = (struct hyss_bz2_stream_data_t *)stream->abstract;
	size_t ret = 0;

	do {
		int just_read;
		size_t remain = count - ret;
		int to_read = (int)(remain <= INT_MAX ? remain : INT_MAX);

		just_read = BZ2_bzread(self->bz_file, buf, to_read);

		if (just_read < 1) {
			stream->eof = 1;
			if (just_read < 0) {
				return -1;
			}
			break;
		}

		ret += just_read;
	} while (ret < count);

	return ret;
}

static size_t hyss_bz2iop_write(hyss_stream *stream, const char *buf, size_t count)
{
	size_t wrote = 0;
	struct hyss_bz2_stream_data_t *self = (struct hyss_bz2_stream_data_t *)stream->abstract;


	do {
		int just_wrote;
		size_t remain = count - wrote;
		int to_write = (int)(remain <= INT_MAX ? remain : INT_MAX);

		just_wrote = BZ2_bzwrite(self->bz_file, (char*)buf, to_write);

		if (just_wrote < 1) {
			break;
		}

		wrote += just_wrote;

	} while (wrote < count);

	return wrote;
}

static int hyss_bz2iop_close(hyss_stream *stream, int close_handle)
{
	struct hyss_bz2_stream_data_t *self = (struct hyss_bz2_stream_data_t *)stream->abstract;
	int ret = EOF;

	if (close_handle) {
		BZ2_bzclose(self->bz_file);
	}

	if (self->stream) {
		hyss_stream_free(self->stream, HYSS_STREAM_FREE_CLOSE | (close_handle == 0 ? HYSS_STREAM_FREE_PRESERVE_HANDLE : 0));
	}

	efree(self);

	return ret;
}

static int hyss_bz2iop_flush(hyss_stream *stream)
{
	struct hyss_bz2_stream_data_t *self = (struct hyss_bz2_stream_data_t *)stream->abstract;
	return BZ2_bzflush(self->bz_file);
}
/* }}} */

const hyss_stream_ops hyss_stream_bz2io_ops = {
	hyss_bz2iop_write, hyss_bz2iop_read,
	hyss_bz2iop_close, hyss_bz2iop_flush,
	"BZip2",
	NULL, /* seek */
	NULL, /* cast */
	NULL, /* stat */
	NULL  /* set_option */
};

/* {{{ Bzip2 stream openers */
HYSS_BZ2_API hyss_stream *_hyss_stream_bz2open_from_BZFILE(BZFILE *bz,
	const char *mode, hyss_stream *innerstream STREAMS_DC)
{
	struct hyss_bz2_stream_data_t *self;

	self = emalloc(sizeof(*self));

	self->stream = innerstream;
	if (innerstream) {
		GC_ADDREF(innerstream->res);
	}
	self->bz_file = bz;

	return hyss_stream_alloc_rel(&hyss_stream_bz2io_ops, self, 0, mode);
}

HYSS_BZ2_API hyss_stream *_hyss_stream_bz2open(hyss_stream_wrapper *wrapper,
	const char *path,
	const char *mode,
	int options,
	gear_string **opened_path,
	hyss_stream_context *context STREAMS_DC)
{
	hyss_stream *retstream = NULL, *stream = NULL;
	char *path_copy = NULL;
	BZFILE *bz_file = NULL;

	if (strncasecmp("compress.bzip2://", path, 17) == 0) {
		path += 17;
	}
	if (mode[0] == '\0' || (mode[0] != 'w' && mode[0] != 'r' && mode[1] != '\0')) {
		return NULL;
	}

#ifdef VIRTUAL_DIR
	virtual_filepath_ex(path, &path_copy, NULL);
#else
	path_copy = (char *)path;
#endif

	if (hyss_check_open_basedir(path_copy)) {
#ifdef VIRTUAL_DIR
		efree(path_copy);
#endif
		return NULL;
	}

	/* try and open it directly first */
	bz_file = BZ2_bzopen(path_copy, mode);

	if (opened_path && bz_file) {
		*opened_path = gear_string_init(path_copy, strlen(path_copy), 0);
	}

#ifdef VIRTUAL_DIR
	efree(path_copy);
#endif

	if (bz_file == NULL) {
		/* that didn't work, so try and get something from the network/wrapper */
		stream = hyss_stream_open_wrapper(path, mode, options | STREAM_WILL_CAST, opened_path);

		if (stream) {
			hyss_socket_t fd;
			if (SUCCESS == hyss_stream_cast(stream, HYSS_STREAM_AS_FD, (void **) &fd, REPORT_ERRORS)) {
				bz_file = BZ2_bzdopen((int)fd, mode);
			}
		}

		/* remove the file created by hyss_stream_open_wrapper(), it is not needed since BZ2 functions
		 * failed.
		 */
		if (opened_path && !bz_file && mode[0] == 'w') {
			VCWD_UNLINK(ZSTR_VAL(*opened_path));
		}
	}

	if (bz_file) {
		retstream = _hyss_stream_bz2open_from_BZFILE(bz_file, mode, stream STREAMS_REL_CC);
		if (retstream) {
			return retstream;
		}

		BZ2_bzclose(bz_file);
	}

	if (stream) {
		hyss_stream_close(stream);
	}

	return NULL;
}

/* }}} */

static const hyss_stream_wrapper_ops bzip2_stream_wops = {
	_hyss_stream_bz2open,
	NULL, /* close */
	NULL, /* fstat */
	NULL, /* stat */
	NULL, /* opendir */
	"BZip2",
	NULL, /* unlink */
	NULL, /* rename */
	NULL, /* mkdir */
	NULL, /* rmdir */
	NULL
};

static const hyss_stream_wrapper hyss_stream_bzip2_wrapper = {
	&bzip2_stream_wops,
	NULL,
	0 /* is_url */
};

static void hyss_bz2_error(INTERNAL_FUNCTION_PARAMETERS, int);

static HYSS_MINIT_FUNCTION(bz2)
{
	hyss_register_url_stream_wrapper("compress.bzip2", &hyss_stream_bzip2_wrapper);
	hyss_stream_filter_register_factory("bzip2.*", &hyss_bz2_filter_factory);
	return SUCCESS;
}

static HYSS_MSHUTDOWN_FUNCTION(bz2)
{
	hyss_unregister_url_stream_wrapper("compress.bzip2");
	hyss_stream_filter_unregister_factory("bzip2.*");

	return SUCCESS;
}

static HYSS_MINFO_FUNCTION(bz2)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "BZip2 Support", "Enabled");
	hyss_info_print_table_row(2, "Stream Wrapper support", "compress.bzip2://");
	hyss_info_print_table_row(2, "Stream Filter support", "bzip2.decompress, bzip2.compress");
	hyss_info_print_table_row(2, "BZip2 Version", (char *) BZ2_bzlibVersion());
	hyss_info_print_table_end();
}

/* {{{ proto string bzread(resource bz[, int length])
   Reads up to length bytes from a BZip2 stream, or 1024 bytes if length is not specified */
static HYSS_FUNCTION(bzread)
{
	zval *bz;
	gear_long len = 1024;
	hyss_stream *stream;
	gear_string *data;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "r|l", &bz, &len)) {
		RETURN_FALSE;
	}

	hyss_stream_from_zval(stream, bz);

	if ((len + 1) < 1) {
		hyss_error_docref(NULL, E_WARNING, "length may not be negative");
		RETURN_FALSE;
	}
	data = gear_string_alloc(len, 0);
	ZSTR_LEN(data) = hyss_stream_read(stream, ZSTR_VAL(data), ZSTR_LEN(data));
	ZSTR_VAL(data)[ZSTR_LEN(data)] = '\0';

	RETURN_NEW_STR(data);
}
/* }}} */

/* {{{ proto resource bzopen(string|int file|fp, string mode)
   Opens a new BZip2 stream */
static HYSS_FUNCTION(bzopen)
{
	zval     *file;   /* The file to open */
	char     *mode;   /* The mode to open the stream with */
	size_t      mode_len;

	BZFILE   *bz;     /* The compressed file stream */
	hyss_stream *stream = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "zs", &file, &mode, &mode_len) == FAILURE) {
		return;
	}

	if (mode_len != 1 || (mode[0] != 'r' && mode[0] != 'w')) {
		hyss_error_docref(NULL, E_WARNING, "'%s' is not a valid mode for bzopen(). Only 'w' and 'r' are supported.", mode);
		RETURN_FALSE;
	}

	/* If it's not a resource its a string containing the filename to open */
	if (Z_TYPE_P(file) == IS_STRING) {
		if (Z_STRLEN_P(file) == 0) {
			hyss_error_docref(NULL, E_WARNING, "filename cannot be empty");
			RETURN_FALSE;
		}

		if (CHECK_ZVAL_NULL_PATH(file)) {
			RETURN_FALSE;
		}

		stream = hyss_stream_bz2open(NULL, Z_STRVAL_P(file), mode, REPORT_ERRORS, NULL);
	} else if (Z_TYPE_P(file) == IS_RESOURCE) {
		/* If it is a resource, than its a stream resource */
		hyss_socket_t fd;
		size_t stream_mode_len;

		hyss_stream_from_zval(stream, file);
		stream_mode_len = strlen(stream->mode);

		if (stream_mode_len != 1 && !(stream_mode_len == 2 && memchr(stream->mode, 'b', 2))) {
			hyss_error_docref(NULL, E_WARNING, "cannot use stream opened in mode '%s'", stream->mode);
			RETURN_FALSE;
		} else if (stream_mode_len == 1 && stream->mode[0] != 'r' && stream->mode[0] != 'w' && stream->mode[0] != 'a' && stream->mode[0] != 'x') {
			hyss_error_docref(NULL, E_WARNING, "cannot use stream opened in mode '%s'", stream->mode);
			RETURN_FALSE;
		}

		switch(mode[0]) {
			case 'r':
				/* only "r" and "rb" are supported */
				if (stream->mode[0] != mode[0] && !(stream_mode_len == 2 && stream->mode[1] != mode[0])) {
					hyss_error_docref(NULL, E_WARNING, "cannot read from a stream opened in write only mode");
					RETURN_FALSE;
				}
				break;
			case 'w':
				/* support only "w"(b), "a"(b), "x"(b) */
				if (stream->mode[0] != mode[0] && !(stream_mode_len == 2 && stream->mode[1] != mode[0])
					&& stream->mode[0] != 'a' && !(stream_mode_len == 2 && stream->mode[1] != 'a')
					&& stream->mode[0] != 'x' && !(stream_mode_len == 2 && stream->mode[1] != 'x')) {
					hyss_error_docref(NULL, E_WARNING, "cannot write to a stream opened in read only mode");
					RETURN_FALSE;
				}
				break;
			default:
				/* not reachable */
				break;
		}

		if (FAILURE == hyss_stream_cast(stream, HYSS_STREAM_AS_FD, (void *) &fd, REPORT_ERRORS)) {
			RETURN_FALSE;
		}

		bz = BZ2_bzdopen((int)fd, mode);

		stream = hyss_stream_bz2open_from_BZFILE(bz, mode, stream);
	} else {
		hyss_error_docref(NULL, E_WARNING, "first parameter has to be string or file-resource");
		RETURN_FALSE;
	}

	if (stream) {
		hyss_stream_to_zval(stream, return_value);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto int bzerrno(resource bz)
   Returns the error number */
static HYSS_FUNCTION(bzerrno)
{
	hyss_bz2_error(INTERNAL_FUNCTION_PARAM_PASSTHRU, HYSS_BZ_ERRNO);
}
/* }}} */

/* {{{ proto string bzerrstr(resource bz)
   Returns the error string */
static HYSS_FUNCTION(bzerrstr)
{
	hyss_bz2_error(INTERNAL_FUNCTION_PARAM_PASSTHRU, HYSS_BZ_ERRSTR);
}
/* }}} */

/* {{{ proto array bzerror(resource bz)
   Returns the error number and error string in an associative array */
static HYSS_FUNCTION(bzerror)
{
	hyss_bz2_error(INTERNAL_FUNCTION_PARAM_PASSTHRU, HYSS_BZ_ERRBOTH);
}
/* }}} */

/* {{{ proto string bzcompress(string source [, int blocksize100k [, int workfactor]])
   Compresses a string into BZip2 encoded data */
static HYSS_FUNCTION(bzcompress)
{
	char             *source;          /* Source data to compress */
	gear_long              zblock_size = 0; /* Optional block size to use */
	gear_long              zwork_factor = 0;/* Optional work factor to use */
	gear_string      *dest = NULL;     /* Destination to place the compressed data into */
	int               error,           /* Error Container */
					  block_size  = 4, /* Block size for compression algorithm */
					  work_factor = 0, /* Work factor for compression algorithm */
					  argc = GEAR_NUM_ARGS(); /* Argument count */
	size_t               source_len;      /* Length of the source data */
	unsigned int      dest_len;        /* Length of the destination buffer */

	if (gear_parse_parameters(argc, "s|ll", &source, &source_len, &zblock_size, &zwork_factor) == FAILURE) {
		return;
	}

	/* Assign them to easy to use variables, dest_len is initially the length of the data
	   + .01 x length of data + 600 which is the largest size the results of the compression
	   could possibly be, at least that's what the libbz2 docs say (thanks to jeremy@nirvani.net
	   for pointing this out).  */
	dest_len = (unsigned int) (source_len + (0.01 * source_len) + 600);

	/* Allocate the destination buffer */
	dest = gear_string_alloc(dest_len, 0);

	/* Handle the optional arguments */
	if (argc > 1) {
		block_size = zblock_size;
	}

	if (argc > 2) {
		work_factor = zwork_factor;
	}

	error = BZ2_bzBuffToBuffCompress(ZSTR_VAL(dest), &dest_len, source, source_len, block_size, 0, work_factor);
	if (error != BZ_OK) {
		gear_string_efree(dest);
		RETURN_LONG(error);
	} else {
		/* Copy the buffer, we have perhaps allocate a lot more than we need,
		   so we erealloc() the buffer to the proper size */
		ZSTR_LEN(dest) = dest_len;
		ZSTR_VAL(dest)[ZSTR_LEN(dest)] = '\0';
		RETURN_NEW_STR(dest);
	}
}
/* }}} */

/* {{{ proto string bzdecompress(string source [, int small])
   Decompresses BZip2 compressed data */
static HYSS_FUNCTION(bzdecompress)
{
	char *source;
	gear_string *dest;
	size_t source_len;
	int error;
	gear_long small = 0;
#if defined(HYSS_WIN32)
	unsigned __int64 size = 0;
#else
	unsigned long long size = 0;
#endif
	bz_stream bzs;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "s|l", &source, &source_len, &small)) {
		RETURN_FALSE;
	}

	bzs.bzalloc = NULL;
	bzs.bzfree = NULL;

	if (BZ2_bzDecompressInit(&bzs, 0, (int)small) != BZ_OK) {
		RETURN_FALSE;
	}

	bzs.next_in = source;
	bzs.avail_in = source_len;

	/* in most cases bz2 offers at least 2:1 compression, so we use that as our base */
	dest = gear_string_safe_alloc(source_len, 2, 1, 0);
	bzs.avail_out = source_len * 2;
	bzs.next_out = ZSTR_VAL(dest);

	while ((error = BZ2_bzDecompress(&bzs)) == BZ_OK && bzs.avail_in > 0) {
		/* compression is better then 2:1, need to allocate more memory */
		bzs.avail_out = source_len;
		size = (bzs.total_out_hi32 * (unsigned int) -1) + bzs.total_out_lo32;
#if !GEAR_ENABLE_ZVAL_LONG64
		if (size > SIZE_MAX) {
			/* no reason to continue if we're going to drop it anyway */
			break;
		}
#endif
		dest = gear_string_safe_realloc(dest, 1, bzs.avail_out+1, (size_t) size, 0);
		bzs.next_out = ZSTR_VAL(dest) + size;
	}

	if (error == BZ_STREAM_END || error == BZ_OK) {
		size = (bzs.total_out_hi32 * (unsigned int) -1) + bzs.total_out_lo32;
#if !GEAR_ENABLE_ZVAL_LONG64
		if (UNEXPECTED(size > SIZE_MAX)) {
			hyss_error_docref(NULL, E_WARNING, "Decompressed size too big, max is %zd", SIZE_MAX);
			gear_string_efree(dest);
			RETVAL_LONG(BZ_MEM_ERROR);
		} else
#endif
		{
			dest = gear_string_safe_realloc(dest, 1, (size_t)size, 1, 0);
			ZSTR_LEN(dest) = (size_t)size;
			ZSTR_VAL(dest)[(size_t)size] = '\0';
			RETVAL_STR(dest);
		}
	} else { /* real error */
		gear_string_efree(dest);
		RETVAL_LONG(error);
	}

	BZ2_bzDecompressEnd(&bzs);
}
/* }}} */

/* {{{ hyss_bz2_error()
   The central error handling interface, does the work for bzerrno, bzerrstr and bzerror */
static void hyss_bz2_error(INTERNAL_FUNCTION_PARAMETERS, int opt)
{
	zval         *bzp;     /* BZip2 Resource Pointer */
	hyss_stream   *stream;
	const char   *errstr;  /* Error string */
	int           errnum;  /* Error number */
	struct hyss_bz2_stream_data_t *self;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &bzp) == FAILURE) {
		return;
	}

	hyss_stream_from_zval(stream, bzp);

	if (!hyss_stream_is(stream, HYSS_STREAM_IS_BZIP2)) {
		RETURN_FALSE;
	}

	self = (struct hyss_bz2_stream_data_t *) stream->abstract;

	/* Fetch the error information */
	errstr = BZ2_bzerror(self->bz_file, &errnum);

	/* Determine what to return */
	switch (opt) {
		case HYSS_BZ_ERRNO:
			RETURN_LONG(errnum);
			break;
		case HYSS_BZ_ERRSTR:
			RETURN_STRING((char*)errstr);
			break;
		case HYSS_BZ_ERRBOTH:
			array_init(return_value);

			add_assoc_long  (return_value, "errno",  errnum);
			add_assoc_string(return_value, "errstr", (char*)errstr);
			break;
	}
}
/* }}} */

#endif

