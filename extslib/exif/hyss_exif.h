/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_EXIF

#include "hyss_version.h"
#define HYSS_EXIF_VERSION HYSS_VERSION

extern gear_capi_entry exif_capi_entry;
#define hyssext_exif_ptr &exif_capi_entry

HYSS_FUNCTION(exif_read_data);
HYSS_FUNCTION(exif_tagname);
HYSS_FUNCTION(exif_thumbnail);
HYSS_FUNCTION(exif_imagetype);
#endif
