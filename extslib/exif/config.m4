dnl config.m4 for extension exif

HYSS_ARG_ENABLE(exif, whether to enable EXIF (metadata from images) support,
[  --enable-exif           Enable EXIF (metadata from images) support])

if test "$HYSS_EXIF" != "no"; then
  AC_DEFINE(HAVE_EXIF, 1, [Whether you want EXIF (metadata from images) support])
  HYSS_NEW_EXTENSION(exif, exif.c, $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
fi
