/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_session.h"

#define PS_SANITY_CHECK						\
	if (PS(session_status) != hyss_session_active) { \
		hyss_error_docref(NULL, E_WARNING, "Session is not active"); \
		RETURN_FALSE; \
	} \
	if (PS(default_mod) == NULL) {				\
		hyss_error_docref(NULL, E_CORE_ERROR, "Cannot call default session handler"); \
		RETURN_FALSE;						\
	}

#define PS_SANITY_CHECK_IS_OPEN				\
	PS_SANITY_CHECK; \
	if (!PS(capi_user_is_open)) {			\
		hyss_error_docref(NULL, E_WARNING, "Parent session handler is not open");	\
		RETURN_FALSE;						\
	}

/* {{{ proto bool SessionHandler::open(string save_path, string session_name)
   Wraps the old open handler */
HYSS_METHOD(SessionHandler, open)
{
	char *save_path = NULL, *session_name = NULL;
	size_t save_path_len, session_name_len;
	int ret;

	PS_SANITY_CHECK;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss", &save_path, &save_path_len, &session_name, &session_name_len) == FAILURE) {
		return;
	}

	PS(capi_user_is_open) = 1;

	gear_try {
		ret = PS(default_mod)->s_open(&PS(capi_data), save_path, session_name);
	} gear_catch {
		PS(session_status) = hyss_session_none;
		gear_bailout();
	} gear_end_try();

	RETVAL_BOOL(SUCCESS == ret);
}
/* }}} */

/* {{{ proto bool SessionHandler::close()
   Wraps the old close handler */
HYSS_METHOD(SessionHandler, close)
{
	int ret;

	PS_SANITY_CHECK_IS_OPEN;

	// don't return on failure, since not closing the default handler
	// could result in memory leaks or other nasties
	gear_parse_parameters_none();

	PS(capi_user_is_open) = 0;

	gear_try {
		ret = PS(default_mod)->s_close(&PS(capi_data));
	} gear_catch {
		PS(session_status) = hyss_session_none;
		gear_bailout();
	} gear_end_try();

	RETVAL_BOOL(SUCCESS == ret);
}
/* }}} */

/* {{{ proto bool SessionHandler::read(string id)
   Wraps the old read handler */
HYSS_METHOD(SessionHandler, read)
{
	gear_string *val;
	gear_string *key;

	PS_SANITY_CHECK_IS_OPEN;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &key) == FAILURE) {
		return;
	}

	if (PS(default_mod)->s_read(&PS(capi_data), key, &val, PS(gc_maxlifetime)) == FAILURE) {
		RETURN_FALSE;
	}

	RETURN_STR(val);
}
/* }}} */

/* {{{ proto bool SessionHandler::write(string id, string data)
   Wraps the old write handler */
HYSS_METHOD(SessionHandler, write)
{
	gear_string *key, *val;

	PS_SANITY_CHECK_IS_OPEN;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "SS", &key, &val) == FAILURE) {
		return;
	}

	RETURN_BOOL(SUCCESS == PS(default_mod)->s_write(&PS(capi_data), key, val, PS(gc_maxlifetime)));
}
/* }}} */

/* {{{ proto bool SessionHandler::destroy(string id)
   Wraps the old destroy handler */
HYSS_METHOD(SessionHandler, destroy)
{
	gear_string *key;

	PS_SANITY_CHECK_IS_OPEN;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &key) == FAILURE) {
		return;
	}

	RETURN_BOOL(SUCCESS == PS(default_mod)->s_destroy(&PS(capi_data), key));
}
/* }}} */

/* {{{ proto bool SessionHandler::gc(int maxlifetime)
   Wraps the old gc handler */
HYSS_METHOD(SessionHandler, gc)
{
	gear_long maxlifetime;
	gear_long nrdels = -1;

	PS_SANITY_CHECK_IS_OPEN;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &maxlifetime) == FAILURE) {
		return;
	}

	if (PS(default_mod)->s_gc(&PS(capi_data), maxlifetime, &nrdels) == FAILURE) {
		RETURN_FALSE;
	}
	RETURN_LONG(nrdels);
}
/* }}} */

/* {{{ proto char SessionHandler::create_sid()
   Wraps the old create_sid handler */
HYSS_METHOD(SessionHandler, create_sid)
{
	gear_string *id;

	PS_SANITY_CHECK;

	if (gear_parse_parameters_none() == FAILURE) {
	    return;
	}

	id = PS(default_mod)->s_create_sid(&PS(capi_data));

	RETURN_STR(id);
}
/* }}} */

/* {{{ proto char SessionUpdateTimestampHandler::validateId(string id)
   Simply return TRUE */
HYSS_METHOD(SessionHandler, validateId)
{
	gear_string *key;

	PS_SANITY_CHECK_IS_OPEN;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &key) == FAILURE) {
		return;
	}

	/* Legacy save handler may not support validate_sid API. Return TRUE. */
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool SessionUpdateTimestampHandler::updateTimestamp(string id, string data)
   Simply call update_timestamp */
HYSS_METHOD(SessionHandler, updateTimestamp)
{
	gear_string *key, *val;

	PS_SANITY_CHECK_IS_OPEN;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "SS", &key, &val) == FAILURE) {
		return;
	}

	/* Legacy save handler may not support update_timestamp API. Just write. */
	RETVAL_BOOL(SUCCESS == PS(default_mod)->s_write(&PS(capi_data), key, val, PS(gc_maxlifetime)));
}
/* }}} */
