/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SESSION_H
#define HYSS_SESSION_H

#include "extslib/standard/hyss_var.h"

#if defined(HAVE_HASH_EXT) && !defined(COMPILE_DL_HASH)
# include "extslib/hash/hyss_hash.h"
#endif

#define HYSS_SESSION_API 20161017

#include "hyss_version.h"
#define HYSS_SESSION_VERSION HYSS_VERSION

/* save handler macros */
#define PS_NUM_APIS      9
#define PS_OPEN_ARGS     void **capi_data, const char *save_path, const char *session_name
#define PS_CLOSE_ARGS    void **capi_data
#define PS_READ_ARGS     void **capi_data, gear_string *key, gear_string **val, gear_long maxlifetime
#define PS_WRITE_ARGS    void **capi_data, gear_string *key, gear_string *val, gear_long maxlifetime
#define PS_DESTROY_ARGS  void **capi_data, gear_string *key
#define PS_GC_ARGS       void **capi_data, gear_long maxlifetime, gear_long *nrdels
#define PS_CREATE_SID_ARGS void **capi_data
#define PS_VALIDATE_SID_ARGS void **capi_data, gear_string *key
#define PS_UPDATE_TIMESTAMP_ARGS void **capi_data, gear_string *key, gear_string *val, gear_long maxlifetime

typedef struct ps_capi_struct {
	const char *s_name;
	int (*s_open)(PS_OPEN_ARGS);
	int (*s_close)(PS_CLOSE_ARGS);
	int (*s_read)(PS_READ_ARGS);
	int (*s_write)(PS_WRITE_ARGS);
	int (*s_destroy)(PS_DESTROY_ARGS);
	gear_long (*s_gc)(PS_GC_ARGS);
	gear_string *(*s_create_sid)(PS_CREATE_SID_ARGS);
	int (*s_validate_sid)(PS_VALIDATE_SID_ARGS);
	int (*s_update_timestamp)(PS_UPDATE_TIMESTAMP_ARGS);
} ps_capi;

#define PS_GET_CAPI_DATA() *capi_data
#define PS_SET_CAPI_DATA(a) *capi_data = (a)

#define PS_OPEN_FUNC(x) 	int ps_open_##x(PS_OPEN_ARGS)
#define PS_CLOSE_FUNC(x) 	int ps_close_##x(PS_CLOSE_ARGS)
#define PS_READ_FUNC(x) 	int ps_read_##x(PS_READ_ARGS)
#define PS_WRITE_FUNC(x) 	int ps_write_##x(PS_WRITE_ARGS)
#define PS_DESTROY_FUNC(x) 	int ps_delete_##x(PS_DESTROY_ARGS)
#define PS_GC_FUNC(x) 		gear_long ps_gc_##x(PS_GC_ARGS)
#define PS_CREATE_SID_FUNC(x)	gear_string *ps_create_sid_##x(PS_CREATE_SID_ARGS)
#define PS_VALIDATE_SID_FUNC(x)	int ps_validate_sid_##x(PS_VALIDATE_SID_ARGS)
#define PS_UPDATE_TIMESTAMP_FUNC(x) 	int ps_update_timestamp_##x(PS_UPDATE_TIMESTAMP_ARGS)

/* Legacy save handler cAPI definitions */
#define PS_FUNCS(x) \
	PS_OPEN_FUNC(x); \
	PS_CLOSE_FUNC(x); \
	PS_READ_FUNC(x); \
	PS_WRITE_FUNC(x); \
	PS_DESTROY_FUNC(x); \
	PS_GC_FUNC(x);	\
	PS_CREATE_SID_FUNC(x)

#define PS_MOD(x) \
	#x, ps_open_##x, ps_close_##x, ps_read_##x, ps_write_##x, \
	 ps_delete_##x, ps_gc_##x, hyss_session_create_id, \
	 hyss_session_validate_sid, hyss_session_update_timestamp

/* Legacy SID creation enabled save handler cAPI definitions */
#define PS_FUNCS_SID(x) \
	PS_OPEN_FUNC(x); \
	PS_CLOSE_FUNC(x); \
	PS_READ_FUNC(x); \
	PS_WRITE_FUNC(x); \
	PS_DESTROY_FUNC(x); \
	PS_GC_FUNC(x); \
	PS_CREATE_SID_FUNC(x); \
	PS_VALIDATE_SID_FUNC(x); \
	PS_UPDATE_TIMESTAMP_FUNC(x);

#define PS_CAPI_SID(x) \
	#x, ps_open_##x, ps_close_##x, ps_read_##x, ps_write_##x, \
	 ps_delete_##x, ps_gc_##x, ps_create_sid_##x, \
	 hyss_session_validate_sid, hyss_session_update_timestamp

/* Update timestamp enabled save handler cAPI definitions
   New save handlers should use this API */
#define PS_FUNCS_UPDATE_TIMESTAMP(x) \
	PS_OPEN_FUNC(x); \
	PS_CLOSE_FUNC(x); \
	PS_READ_FUNC(x); \
	PS_WRITE_FUNC(x); \
	PS_DESTROY_FUNC(x); \
	PS_GC_FUNC(x); \
	PS_CREATE_SID_FUNC(x); \
	PS_VALIDATE_SID_FUNC(x); \
	PS_UPDATE_TIMESTAMP_FUNC(x);

#define PS_CAPI_UPDATE_TIMESTAMP(x) \
	#x, ps_open_##x, ps_close_##x, ps_read_##x, ps_write_##x, \
	 ps_delete_##x, ps_gc_##x, ps_create_sid_##x, \
	 ps_validate_sid_##x, ps_update_timestamp_##x


typedef enum {
	hyss_session_disabled,
	hyss_session_none,
	hyss_session_active
} hyss_session_status;

typedef struct _hyss_session_rfc1867_progress {
	size_t    sname_len;
	zval      sid;
	smart_str key;

	gear_long      update_step;
	gear_long      next_update;
	double    next_update_time;
	gear_bool cancel_upload;
	gear_bool apply_trans_sid;
	size_t    content_length;

	zval      data;                 /* the array exported to session data */
	zval	 *post_bytes_processed; /* data["bytes_processed"] */
	zval      files;                /* data["files"] array */
	zval      current_file;         /* array of currently uploading file */
	zval	 *current_file_bytes_processed;
} hyss_session_rfc1867_progress;

typedef struct _hyss_ps_globals {
	char *save_path;
	char *session_name;
	gear_string *id;
	char *extern_referer_chk;
	char *cache_limiter;
	gear_long cookie_lifetime;
	char *cookie_path;
	char *cookie_domain;
	gear_bool  cookie_secure;
	gear_bool  cookie_httponly;
	char *cookie_samesite;
	const ps_capi *mod;
	const ps_capi *default_mod;
	void *capi_data;
	hyss_session_status session_status;
	gear_long gc_probability;
	gear_long gc_divisor;
	gear_long gc_maxlifetime;
	int capi_number;
	gear_long cache_expire;
	union {
		zval names[PS_NUM_APIS];
		struct {
			zval ps_open;
			zval ps_close;
			zval ps_read;
			zval ps_write;
			zval ps_destroy;
			zval ps_gc;
			zval ps_create_sid;
			zval ps_validate_sid;
			zval ps_update_timestamp;
		} name;
	} capi_user_names;
	int capi_user_implemented;
	int capi_user_is_open;
	const struct ps_serializer_struct *serializer;
	zval http_session_vars;
	gear_bool auto_start;
	gear_bool use_cookies;
	gear_bool use_only_cookies;
	gear_bool use_trans_sid; /* contains the ICS value of whether to use trans-sid */

	gear_long sid_length;
	gear_long sid_bits_per_character;
	int send_cookie;
	int define_sid;

	hyss_session_rfc1867_progress *rfc1867_progress;
	gear_bool rfc1867_enabled; /* session.upload_progress.enabled */
	gear_bool rfc1867_cleanup; /* session.upload_progress.cleanup */
	char *rfc1867_prefix;  /* session.upload_progress.prefix */
	char *rfc1867_name;    /* session.upload_progress.name */
	gear_long rfc1867_freq;         /* session.upload_progress.freq */
	double rfc1867_min_freq;   /* session.upload_progress.min_freq */

	gear_bool use_strict_mode; /* whether or not HYSS accepts unknown session ids */
	gear_bool lazy_write; /* omit session write when it is possible */
	gear_bool in_save_handler; /* state if session is in save handler or not */
	gear_bool set_handler;     /* state if session cAPI i setting handler or not */
	gear_string *session_vars; /* serialized original session data */
} hyss_ps_globals;

typedef hyss_ps_globals gear_ps_globals;

extern gear_capi_entry session_capi_entry;
#define hyssext_session_ptr &session_capi_entry

#ifdef ZTS
#define PS(v) GEAR_PBCG(ps_globals_id, hyss_ps_globals *, v)
#ifdef COMPILE_DL_SESSION
GEAR_PBCLS_CACHE_EXTERN()
#endif
#else
#define PS(v) (ps_globals.v)
#endif

#define PS_SERIALIZER_ENCODE_ARGS void
#define PS_SERIALIZER_DECODE_ARGS const char *val, size_t vallen

typedef struct ps_serializer_struct {
	const char *name;
	gear_string *(*encode)(PS_SERIALIZER_ENCODE_ARGS);
	int (*decode)(PS_SERIALIZER_DECODE_ARGS);
} ps_serializer;

#define PS_SERIALIZER_ENCODE_NAME(x) ps_srlzr_encode_##x
#define PS_SERIALIZER_DECODE_NAME(x) ps_srlzr_decode_##x

#define PS_SERIALIZER_ENCODE_FUNC(x) \
	gear_string *PS_SERIALIZER_ENCODE_NAME(x)(PS_SERIALIZER_ENCODE_ARGS)
#define PS_SERIALIZER_DECODE_FUNC(x) \
	int PS_SERIALIZER_DECODE_NAME(x)(PS_SERIALIZER_DECODE_ARGS)

#define PS_SERIALIZER_FUNCS(x) \
	PS_SERIALIZER_ENCODE_FUNC(x); \
	PS_SERIALIZER_DECODE_FUNC(x)

#define PS_SERIALIZER_ENTRY(x) \
	{ #x, PS_SERIALIZER_ENCODE_NAME(x), PS_SERIALIZER_DECODE_NAME(x) }

/* default create id function */
HYSSAPI gear_string *hyss_session_create_id(PS_CREATE_SID_ARGS);
/* Dummy PS cAPI functions */
HYSSAPI int hyss_session_validate_sid(PS_VALIDATE_SID_ARGS);
HYSSAPI int hyss_session_update_timestamp(PS_UPDATE_TIMESTAMP_ARGS);

HYSSAPI void session_adapt_url(const char *, size_t, char **, size_t *);

HYSSAPI int hyss_session_destroy(void);
HYSSAPI void hyss_add_session_var(gear_string *name);
HYSSAPI zval *hyss_set_session_var(gear_string *name, zval *state_val, hyss_unserialize_data_t *var_hash);
HYSSAPI zval *hyss_get_session_var(gear_string *name);

HYSSAPI int hyss_session_register_capi(const ps_capi *);

HYSSAPI int hyss_session_register_serializer(const char *name,
	        gear_string *(*encode)(PS_SERIALIZER_ENCODE_ARGS),
	        int (*decode)(PS_SERIALIZER_DECODE_ARGS));

HYSSAPI void hyss_session_set_id(char *id);
HYSSAPI int hyss_session_start(void);
HYSSAPI int hyss_session_flush(int write);

HYSSAPI const ps_capi *_hyss_find_ps_capi(char *name);
HYSSAPI const ps_serializer *_hyss_find_ps_serializer(char *name);

HYSSAPI int hyss_session_valid_key(const char *key);
HYSSAPI int hyss_session_reset_id(void);

#define PS_ADD_VARL(name) do {										\
	hyss_add_session_var(name);							\
} while (0)

#define PS_ADD_VAR(name) PS_ADD_VARL(name)

#define PS_DEL_VARL(name) do {										\
	if (!Z_ISNULL(PS(http_session_vars))) {							\
		gear_hash_del(Z_ARRVAL(PS(http_session_vars)), name);		\
	}																\
} while (0)


#define PS_ENCODE_VARS 												\
	gear_string *key;												\
	gear_ulong num_key;													\
	zval *struc;

#define PS_ENCODE_LOOP(code) do {									\
	HashTable *_ht = Z_ARRVAL_P(Z_REFVAL(PS(http_session_vars)));	\
	GEAR_HASH_FOREACH_KEY(_ht, num_key, key) {						\
		if (key == NULL) {											\
			hyss_error_docref(NULL, E_NOTICE,						\
					"Skipping numeric key " GEAR_LONG_FMT, num_key);\
			continue;												\
		}															\
		if ((struc = hyss_get_session_var(key))) {					\
			code;		 											\
		} 															\
	} GEAR_HASH_FOREACH_END();										\
} while(0)

HYSSAPI GEAR_EXTERN_CAPI_GLOBALS(ps)

void hyss_session_auto_start(void *data);

#define PS_CLASS_NAME "SessionHandler"
extern HYSSAPI gear_class_entry *hyss_session_class_entry;

#define PS_IFACE_NAME "SessionHandlerInterface"
extern HYSSAPI gear_class_entry *hyss_session_iface_entry;

#define PS_SID_IFACE_NAME "SessionIdInterface"
extern HYSSAPI gear_class_entry *hyss_session_id_iface_entry;

#define PS_UPDATE_TIMESTAMP_IFACE_NAME "SessionUpdateTimestampHandlerInterface"
extern HYSSAPI gear_class_entry *hyss_session_update_timestamp_iface_entry;

extern HYSS_METHOD(SessionHandler, open);
extern HYSS_METHOD(SessionHandler, close);
extern HYSS_METHOD(SessionHandler, read);
extern HYSS_METHOD(SessionHandler, write);
extern HYSS_METHOD(SessionHandler, destroy);
extern HYSS_METHOD(SessionHandler, gc);
extern HYSS_METHOD(SessionHandler, create_sid);
extern HYSS_METHOD(SessionHandler, validateId);
extern HYSS_METHOD(SessionHandler, updateTimestamp);

#endif
