dnl config.m4 for extension session

HYSS_ARG_ENABLE(session, whether to enable HYSS sessions,
[  --disable-session       Disable session support], yes)

HYSS_ARG_WITH(mm,for mm support,
[  --with-mm[=DIR]           SESSION: Include mm support for session storage], no, no)

if test "$HYSS_SESSION" != "no"; then
  HYSS_PWRITE_TEST
  HYSS_PREAD_TEST
  HYSS_NEW_EXTENSION(session, capi_user_class.c session.c capi_files.c capi_mm.c capi_user.c, $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
  HYSS_ADD_EXTENSION_DEP(session, hash, true)
  HYSS_ADD_EXTENSION_DEP(session, spl)
  HYSS_SUBST(SESSION_SHARED_LIBADD)
  HYSS_INSTALL_HEADERS(extslib/session, [hyss_session.h capi_files.h capi_user.h])
  AC_DEFINE(HAVE_HYSS_SESSION,1,[ ])
fi

if test "$HYSS_MM" != "no"; then
  for i in $HYSS_MM /usr/local /usr; do
    test -f "$i/include/mm.h" && MM_DIR=$i && break
  done

  if test -z "$MM_DIR" ; then
    AC_MSG_ERROR(cannot find mm library)
  fi

  if test "$enable_maintainer_zts" = "yes"; then
    dnl The mm library is not thread-safe, and capi_mm.c refuses to compile.
    AC_MSG_ERROR(--with-mm cannot be combined with --enable-maintainer-zts)
  fi

  HYSS_ADD_LIBRARY_WITH_PATH(mm, $MM_DIR/$HYSS_LIBDIR, SESSION_SHARED_LIBADD)
  HYSS_ADD_INCLUDE($MM_DIR/include)
  HYSS_INSTALL_HEADERS([extslib/session/capi_mm.h])
  AC_DEFINE(HAVE_LIBMM, 1, [Whether you have libmm])
fi
