/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_session.h"
#include "capi_user.h"

const ps_capi ps_capi_user = {
	PS_CAPI_UPDATE_TIMESTAMP(user)
};


static void ps_call_handler(zval *func, int argc, zval *argv, zval *retval)
{
	int i;
	if (PS(in_save_handler)) {
		PS(in_save_handler) = 0;
		ZVAL_UNDEF(retval);
		hyss_error_docref(NULL, E_WARNING, "Cannot call session save handler in a recursive manner");
		return;
	}
	PS(in_save_handler) = 1;
	if (call_user_function(EG(function_table), NULL, func, retval, argc, argv) == FAILURE) {
		zval_ptr_dtor(retval);
		ZVAL_UNDEF(retval);
	} else if (Z_ISUNDEF_P(retval)) {
		ZVAL_NULL(retval);
	}
	PS(in_save_handler) = 0;
	for (i = 0; i < argc; i++) {
		zval_ptr_dtor(&argv[i]);
	}
}

#define STDVARS								\
	zval retval;							\
	int ret = FAILURE

#define PSF(a) PS(capi_user_names).name.ps_##a

#define FINISH \
	if (Z_TYPE(retval) != IS_UNDEF) { \
		if (Z_TYPE(retval) == IS_TRUE) { \
			ret = SUCCESS; \
		} else if (Z_TYPE(retval) == IS_FALSE) { \
			ret = FAILURE; \
        }  else if ((Z_TYPE(retval) == IS_LONG) && (Z_LVAL(retval) == -1)) { \
			/* BC for clever users - Deprecate me */ \
			ret = FAILURE; \
		} else if ((Z_TYPE(retval) == IS_LONG) && (Z_LVAL(retval) == 0)) { \
			/* BC for clever users - Deprecate me */ \
			ret = SUCCESS; \
		} else { \
			if (!EG(exception)) { \
				hyss_error_docref(NULL, E_WARNING, \
				                 "Session callback expects true/false return value"); \
			} \
			ret = FAILURE; \
			zval_ptr_dtor(&retval); \
		} \
	} \
	return ret

PS_OPEN_FUNC(user)
{
	zval args[2];
	STDVARS;

	if (Z_ISUNDEF(PSF(open))) {
		hyss_error_docref(NULL, E_WARNING,
			"user session functions not defined");

		return FAILURE;
	}

	ZVAL_STRING(&args[0], (char*)save_path);
	ZVAL_STRING(&args[1], (char*)session_name);

	gear_try {
		ps_call_handler(&PSF(open), 2, args, &retval);
	} gear_catch {
		PS(session_status) = hyss_session_none;
		if (!Z_ISUNDEF(retval)) {
			zval_ptr_dtor(&retval);
		}
		gear_bailout();
	} gear_end_try();

	PS(capi_user_implemented) = 1;

	FINISH;
}

PS_CLOSE_FUNC(user)
{
	gear_bool bailout = 0;
	STDVARS;

	if (!PS(capi_user_implemented)) {
		/* already closed */
		return SUCCESS;
	}

	gear_try {
		ps_call_handler(&PSF(close), 0, NULL, &retval);
	} gear_catch {
		bailout = 1;
	} gear_end_try();

	PS(capi_user_implemented) = 0;

	if (bailout) {
		if (!Z_ISUNDEF(retval)) {
			zval_ptr_dtor(&retval);
		}
		gear_bailout();
	}

	FINISH;
}

PS_READ_FUNC(user)
{
	zval args[1];
	STDVARS;

	ZVAL_STR_COPY(&args[0], key);

	ps_call_handler(&PSF(read), 1, args, &retval);

	if (!Z_ISUNDEF(retval)) {
		if (Z_TYPE(retval) == IS_STRING) {
			*val = gear_string_copy(Z_STR(retval));
			ret = SUCCESS;
		}
		zval_ptr_dtor(&retval);
	}

	return ret;
}

PS_WRITE_FUNC(user)
{
	zval args[2];
	STDVARS;

	ZVAL_STR_COPY(&args[0], key);
	ZVAL_STR_COPY(&args[1], val);

	ps_call_handler(&PSF(write), 2, args, &retval);

	FINISH;
}

PS_DESTROY_FUNC(user)
{
	zval args[1];
	STDVARS;

	ZVAL_STR_COPY(&args[0], key);

	ps_call_handler(&PSF(destroy), 1, args, &retval);

	FINISH;
}

PS_GC_FUNC(user)
{
	zval args[1];
	zval retval;

	ZVAL_LONG(&args[0], maxlifetime);

	ps_call_handler(&PSF(gc), 1, args, &retval);

	if (Z_TYPE(retval) == IS_LONG) {
		convert_to_long(&retval);
		return Z_LVAL(retval);
	}
	/* This is for older API compatibility */
	if (Z_TYPE(retval) == IS_TRUE) {
		return 1;
	}
	/* Anything else is some kind of error */
	return -1; // Error
}

PS_CREATE_SID_FUNC(user)
{
	/* maintain backwards compatibility */
	if (!Z_ISUNDEF(PSF(create_sid))) {
		gear_string *id = NULL;
		zval retval;

		ps_call_handler(&PSF(create_sid), 0, NULL, &retval);

		if (!Z_ISUNDEF(retval)) {
			if (Z_TYPE(retval) == IS_STRING) {
				id = gear_string_copy(Z_STR(retval));
			}
			zval_ptr_dtor(&retval);
		} else {
			gear_throw_error(NULL, "No session id returned by function");
			return NULL;
		}

		if (!id) {
			gear_throw_error(NULL, "Session id must be a string");
			return NULL;
		}

		return id;
	}

	/* function as defined by PS_MOD */
	return hyss_session_create_id(capi_data);
}

PS_VALIDATE_SID_FUNC(user)
{
	/* maintain backwards compatibility */
	if (!Z_ISUNDEF(PSF(validate_sid))) {
		zval args[1];
		STDVARS;

		ZVAL_STR_COPY(&args[0], key);

		ps_call_handler(&PSF(validate_sid), 1, args, &retval);

		FINISH;
	}

	/* dummy function defined by PS_MOD */
	return hyss_session_validate_sid(capi_data, key);
}

PS_UPDATE_TIMESTAMP_FUNC(user)
{
	zval args[2];
	STDVARS;

	ZVAL_STR_COPY(&args[0], key);
	ZVAL_STR_COPY(&args[1], val);

	/* maintain backwards compatibility */
	if (!Z_ISUNDEF(PSF(update_timestamp))) {
		ps_call_handler(&PSF(update_timestamp), 2, args, &retval);
	} else {
		ps_call_handler(&PSF(write), 2, args, &retval);
	}

	FINISH;
}

