/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_INTERBASE_H
#define HYSS_INTERBASE_H

extern gear_capi_entry ibase_capi_entry;
#define hyssext_interbase_ptr &ibase_capi_entry

#include "hyss_version.h"
#define HYSS_INTERBASE_VERSION HYSS_VERSION

HYSS_MINIT_FUNCTION(ibase);
HYSS_RINIT_FUNCTION(ibase);
HYSS_MSHUTDOWN_FUNCTION(ibase);
HYSS_RSHUTDOWN_FUNCTION(ibase);
HYSS_MINFO_FUNCTION(ibase);

HYSS_FUNCTION(ibase_connect);
HYSS_FUNCTION(ibase_pconnect);
HYSS_FUNCTION(ibase_close);
HYSS_FUNCTION(ibase_drop_db);
HYSS_FUNCTION(ibase_query);
HYSS_FUNCTION(ibase_fetch_row);
HYSS_FUNCTION(ibase_fetch_assoc);
HYSS_FUNCTION(ibase_fetch_object);
HYSS_FUNCTION(ibase_free_result);
HYSS_FUNCTION(ibase_name_result);
HYSS_FUNCTION(ibase_prepare);
HYSS_FUNCTION(ibase_execute);
HYSS_FUNCTION(ibase_free_query);

HYSS_FUNCTION(ibase_timefmt);

HYSS_FUNCTION(ibase_gen_id);
HYSS_FUNCTION(ibase_num_fields);
HYSS_FUNCTION(ibase_num_params);
#if abies_0
HYSS_FUNCTION(ibase_num_rows);
#endif
HYSS_FUNCTION(ibase_affected_rows);
HYSS_FUNCTION(ibase_field_info);
HYSS_FUNCTION(ibase_param_info);

HYSS_FUNCTION(ibase_trans);
HYSS_FUNCTION(ibase_commit);
HYSS_FUNCTION(ibase_rollback);
HYSS_FUNCTION(ibase_commit_ret);
HYSS_FUNCTION(ibase_rollback_ret);

HYSS_FUNCTION(ibase_blob_create);
HYSS_FUNCTION(ibase_blob_add);
HYSS_FUNCTION(ibase_blob_cancel);
HYSS_FUNCTION(ibase_blob_open);
HYSS_FUNCTION(ibase_blob_get);
HYSS_FUNCTION(ibase_blob_close);
HYSS_FUNCTION(ibase_blob_echo);
HYSS_FUNCTION(ibase_blob_info);
HYSS_FUNCTION(ibase_blob_import);

HYSS_FUNCTION(ibase_add_user);
HYSS_FUNCTION(ibase_modify_user);
HYSS_FUNCTION(ibase_delete_user);

HYSS_FUNCTION(ibase_service_attach);
HYSS_FUNCTION(ibase_service_detach);
HYSS_FUNCTION(ibase_backup);
HYSS_FUNCTION(ibase_restore);
HYSS_FUNCTION(ibase_maintain_db);
HYSS_FUNCTION(ibase_db_info);
HYSS_FUNCTION(ibase_server_info);

HYSS_FUNCTION(ibase_errmsg);
HYSS_FUNCTION(ibase_errcode);

HYSS_FUNCTION(ibase_wait_event);
HYSS_FUNCTION(ibase_set_event_handler);
HYSS_FUNCTION(ibase_free_event_handler);

#else

#define hyssext_interbase_ptr NULL

#endif /* HYSS_INTERBASE_H */

