/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_IBASE_INCLUDES_H
#define HYSS_IBASE_INCLUDES_H

#include <ibase.h>

#ifndef SQLDA_CURRENT_VERSION
#define SQLDA_CURRENT_VERSION SQLDA_VERSION1
#endif

#ifndef METADATALENGTH
#define METADATALENGTH 68
#endif

#define RESET_ERRMSG do { IBG(errmsg)[0] = '\0'; IBG(sql_code) = 0; } while (0)

#define IB_STATUS (IBG(status))

#ifdef IBASE_DEBUG
#define IBDEBUG(a) hyss_printf("::: %s (%d)\n", a, __LINE__);
#endif

#ifndef IBDEBUG
#define IBDEBUG(a)
#endif

extern int le_link, le_plink, le_trans;

#define LE_LINK "Firebird/InterBase link"
#define LE_PLINK "Firebird/InterBase persistent link"
#define LE_TRANS "Firebird/InterBase transaction"

#define IBASE_MSGSIZE 512
#define MAX_ERRMSG (IBASE_MSGSIZE*2)

#define IB_DEF_DATE_FMT "%Y-%m-%d"
#define IB_DEF_TIME_FMT "%H:%M:%S"

/* this value should never be > USHRT_MAX */
#define IBASE_BLOB_SEG 4096

GEAR_BEGIN_CAPI_GLOBALS(ibase)
	ISC_STATUS status[20];
	gear_resource *default_link;
	gear_long num_links, num_persistent;
	char errmsg[MAX_ERRMSG];
	gear_long sql_code;
GEAR_END_CAPI_GLOBALS(ibase)

GEAR_EXTERN_CAPI_GLOBALS(ibase)

typedef struct {
	isc_db_handle handle;
	struct tr_list *tr_list;
	unsigned short dialect;
	struct event *event_head;
} ibase_db_link;

typedef struct {
	isc_tr_handle handle;
	unsigned short link_cnt;
	unsigned long affected_rows;
	ibase_db_link *db_link[1]; /* last member */
} ibase_trans;

typedef struct tr_list {
	ibase_trans *trans;
	struct tr_list *next;
} ibase_tr_list;

typedef struct {
	isc_blob_handle bl_handle;
	unsigned short type;
	ISC_QUAD bl_qd;
} ibase_blob;

typedef struct event {
	ibase_db_link *link;
	gear_resource* link_res;
	ISC_LONG event_id;
	unsigned short event_count;
	char **events;
	char *event_buffer, *result_buffer;
	zval callback;
	void *thread_ctx;
	struct event *event_next;
	enum event_state { NEW, ACTIVE, DEAD } state;
} ibase_event;

enum hyss_interbase_option {
	HYSS_IBASE_DEFAULT 			= 0,
	HYSS_IBASE_CREATE            = 0,
	/* fetch flags */
	HYSS_IBASE_FETCH_BLOBS		= 1,
	HYSS_IBASE_FETCH_ARRAYS      = 2,
	HYSS_IBASE_UNIXTIME 			= 4,
	/* transaction access mode */
	HYSS_IBASE_WRITE 			= 1,
	HYSS_IBASE_READ 				= 2,
	/* transaction isolation level */
	HYSS_IBASE_CONCURRENCY 		= 4,
	HYSS_IBASE_COMMITTED 		= 8,
	  HYSS_IBASE_REC_NO_VERSION 	= 32,
	  HYSS_IBASE_REC_VERSION 	= 64,
	HYSS_IBASE_CONSISTENCY 		= 16,
	/* transaction lock resolution */
	HYSS_IBASE_WAIT 				= 128,
	HYSS_IBASE_NOWAIT 			= 256
};

#define IBG(v) GEAR_CAPI_GLOBALS_ACCESSOR(ibase, v)

#if defined(ZTS) && defined(COMPILE_DL_INTERBASE)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#define BLOB_ID_LEN		18
#define BLOB_ID_MASK	"0x%" LL_MASK "x"

#define BLOB_INPUT		1
#define BLOB_OUTPUT		2

#ifdef HYSS_WIN32
#define LL_MASK "I64"
#define LL_LIT(lit) lit ## I64
typedef void (__stdcall *info_func_t)(char*);
#else
#define LL_MASK "ll"
#define LL_LIT(lit) lit ## ll
typedef void (*info_func_t)(char*);
#endif

void _hyss_ibase_error(void);
void _hyss_ibase_capi_error(char *, ...)
	HYSS_ATTRIBUTE_FORMAT(printf,1,2);

/* determine if a resource is a link or transaction handle */
#define HYSS_IBASE_LINK_TRANS(zv, lh, th)													\
		do {                                                                                \
			if (!zv) {                                                                      \
				lh = (ibase_db_link *)gear_fetch_resource2(                                 \
						IBG(default_link), "InterBase link", le_link, le_plink);            \
			} else {                                                                        \
				_hyss_ibase_get_link_trans(INTERNAL_FUNCTION_PARAM_PASSTHRU, zv, &lh, &th);  \
			}                                                                               \
			if (SUCCESS != _hyss_ibase_def_trans(lh, &th)) { RETURN_FALSE; }                 \
		} while (0)

int _hyss_ibase_def_trans(ibase_db_link *ib_link, ibase_trans **trans);
void _hyss_ibase_get_link_trans(INTERNAL_FUNCTION_PARAMETERS, zval *link_id,
	ibase_db_link **ib_link, ibase_trans **trans);

/* provided by ibase_query.c */
void hyss_ibase_query_minit(INIT_FUNC_ARGS);

/* provided by ibase_blobs.c */
void hyss_ibase_blobs_minit(INIT_FUNC_ARGS);
int _hyss_ibase_string_to_quad(char const *id, ISC_QUAD *qd);
gear_string *_hyss_ibase_quad_to_string(ISC_QUAD const qd);
int _hyss_ibase_blob_get(zval *return_value, ibase_blob *ib_blob, gear_ulong max_len);
int _hyss_ibase_blob_add(zval *string_arg, ibase_blob *ib_blob);

/* provided by ibase_events.c */
void hyss_ibase_events_minit(INIT_FUNC_ARGS);
void _hyss_ibase_free_event(ibase_event *event);

/* provided by ibase_service.c */
void hyss_ibase_service_minit(INIT_FUNC_ARGS);

#ifndef max
#define max(a,b) ((a)>(b)?(a):(b))
#endif

#endif /* HYSS_IBASE_INCLUDES_H */

