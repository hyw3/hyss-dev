/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_LDAP_H
#define HYSS_LDAP_H

#ifndef HAVE_ORALDAP
#include <lber.h>
#endif

#include <ldap.h>

extern gear_capi_entry ldap_capi_entry;
#define ldap_capi_ptr &ldap_capi_entry

#include "hyss_version.h"
#define HYSS_LDAP_VERSION HYSS_VERSION

/* LDAP functions */
HYSS_MINIT_FUNCTION(ldap);
HYSS_MSHUTDOWN_FUNCTION(ldap);
HYSS_MINFO_FUNCTION(ldap);

GEAR_BEGIN_CAPI_GLOBALS(ldap)
	gear_long num_links;
	gear_long max_links;
GEAR_END_CAPI_GLOBALS(ldap)

#ifdef ZTS
# define LDAPG(v) PBCG(ldap_globals_id, gear_ldap_globals *, v)
#else
# define LDAPG(v) (ldap_globals.v)
#endif

#define hyssext_ldap_ptr ldap_capi_ptr

/* Constants for ldap_modify_batch */
#define LDAP_MODIFY_BATCH_ADD        0x01
#define LDAP_MODIFY_BATCH_REMOVE     0x02
#define LDAP_MODIFY_BATCH_REMOVE_ALL 0x12
#define LDAP_MODIFY_BATCH_REPLACE    0x03

#define LDAP_MODIFY_BATCH_ATTRIB     "attrib"
#define LDAP_MODIFY_BATCH_MODTYPE    "modtype"
#define LDAP_MODIFY_BATCH_VALUES     "values"

#endif /* HYSS_LDAP_H */
