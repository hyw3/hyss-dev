/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_TIDY_H
#define HYSS_TIDY_H

extern gear_capi_entry tidy_capi_entry;
#define hyssext_tidy_ptr &tidy_capi_entry

#include "hyss_version.h"
#define HYSS_TIDY_VERSION HYSS_VERSION

#define TIDY_METHOD_MAP(name, func_name, arg_types) \
	GEAR_NAMED_FE(name, GEAR_FN(func_name), arg_types)
#define TIDY_NODE_METHOD(name)    HYSS_FUNCTION(tnm_ ##name)
#define TIDY_NODE_ME(name, param) TIDY_METHOD_MAP(name, tnm_ ##name, param)
#define TIDY_NODE_PRIVATE_ME(name, param) GEAR_NAMED_ME(name, GEAR_FN(tnm_ ##name), param, GEAR_ACC_PRIVATE)
#define TIDY_DOC_METHOD(name)     HYSS_FUNCTION(tdm_ ##name)
#define TIDY_DOC_ME(name, param)  TIDY_METHOD_MAP(name, tdm_ ##name, param)
#define TIDY_ATTR_METHOD(name)    HYSS_FUNCTION(tam_ ##name)
#define TIDY_ATTR_ME(name, param) TIDY_METHOD_MAP(name, tam_ ##name, param)

GEAR_BEGIN_CAPI_GLOBALS(tidy)
	char *default_config;
	gear_bool clean_output;
GEAR_END_CAPI_GLOBALS(tidy)

#define TG(v) GEAR_CAPI_GLOBALS_ACCESSOR(tidy, v)

#if defined(ZTS) && defined(COMPILE_DL_TIDY)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#endif

