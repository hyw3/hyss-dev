/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_test.h"

static gear_class_entry *gear_test_interface;
static gear_class_entry *gear_test_class;
static gear_class_entry *gear_test_child_class;
static gear_class_entry *gear_test_trait;
static gear_object_handlers gear_test_class_handlers;

GEAR_BEGIN_ARG_WITH_RETURN_TYPE_INFO(arginfo_gear_test_array_return, IS_ARRAY, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_WITH_RETURN_TYPE_INFO(arginfo_gear_test_nullable_array_return, IS_ARRAY, 1)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_WITH_RETURN_TYPE_INFO(arginfo_gear_test_void_return, IS_VOID, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_gear_terminate_string, 0, 0, 1)
	GEAR_ARG_INFO(1, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_gear_leak_variable, 0, 0, 1)
	GEAR_ARG_INFO(0, variable)
GEAR_END_ARG_INFO()

GEAR_FUNCTION(gear_test_func)
{
	/* dummy */
}

GEAR_FUNCTION(gear_test_array_return)
{
	zval *arg1, *arg2;

	gear_parse_parameters(GEAR_NUM_ARGS(), "|zz", &arg1, &arg2);
}

GEAR_FUNCTION(gear_test_nullable_array_return)
{
	zval *arg1, *arg2;

	gear_parse_parameters(GEAR_NUM_ARGS(), "|zz", &arg1, &arg2);
}

GEAR_FUNCTION(gear_test_void_return)
{
	/* dummy */
}

/* Create a string without terminating null byte. Must be termined with
 * gear_terminate_string() before destruction, otherwise a warning is issued
 * in debug builds. */
GEAR_FUNCTION(gear_create_unterminated_string)
{
	gear_string *str, *res;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &str) == FAILURE) {
		return;
	}

	res = gear_string_alloc(ZSTR_LEN(str), 0);
	memcpy(ZSTR_VAL(res), ZSTR_VAL(str), ZSTR_LEN(str));
	/* No trailing null byte */

	RETURN_STR(res);
}

/* Enforce terminate null byte on string. This avoids a warning in debug builds. */
GEAR_FUNCTION(gear_terminate_string)
{
	gear_string *str;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &str) == FAILURE) {
		return;
	}

	ZSTR_VAL(str)[ZSTR_LEN(str)] = '\0';
}

/* {{{ proto void gear_leak_bytes([int num_bytes])
   Cause an intentional memory leak, for testing/debugging purposes */
GEAR_FUNCTION(gear_leak_bytes)
{
	gear_long leakbytes = 3;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|l", &leakbytes) == FAILURE) {
		return;
	}

	emalloc(leakbytes);
}
/* }}} */

/* {{{ proto void gear_leak_variable(mixed variable)
   Leak a refcounted variable */
GEAR_FUNCTION(gear_leak_variable)
{
	zval *zv;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "z", &zv) == FAILURE) {
		return;
	}

	if (!Z_REFCOUNTED_P(zv)) {
		gear_error(E_WARNING, "Cannot leak variable that is not refcounted");
		return;
	}

	Z_ADDREF_P(zv);
}
/* }}} */

static gear_object *gear_test_class_new(gear_class_entry *class_type) /* {{{ */ {
	gear_object *obj = gear_objects_new(class_type);
	obj->handlers = &gear_test_class_handlers;
	return obj;
}
/* }}} */

static gear_function *gear_test_class_method_get(gear_object **object, gear_string *name, const zval *key) /* {{{ */ {
	gear_internal_function *fptr = emalloc(sizeof(gear_internal_function));
	fptr->type = GEAR_OVERLOADED_FUNCTION_TEMPORARY;
	fptr->num_args = 1;
	fptr->arg_info = NULL;
	fptr->scope = (*object)->ce;
	fptr->fn_flags = GEAR_ACC_CALL_VIA_HANDLER;
	fptr->function_name = gear_string_copy(name);
	fptr->handler = GEAR_FN(gear_test_func);
	gear_set_function_arg_flags((gear_function*)fptr);

	return (gear_function*)fptr;
}
/* }}} */

static gear_function *gear_test_class_static_method_get(gear_class_entry *ce, gear_string *name) /* {{{ */ {
	gear_internal_function *fptr = emalloc(sizeof(gear_internal_function));
	fptr->type = GEAR_OVERLOADED_FUNCTION;
	fptr->num_args = 1;
	fptr->arg_info = NULL;
	fptr->scope = ce;
	fptr->fn_flags = GEAR_ACC_CALL_VIA_HANDLER|GEAR_ACC_STATIC;
	fptr->function_name = name;
	fptr->handler = GEAR_FN(gear_test_func);
	gear_set_function_arg_flags((gear_function*)fptr);

	return (gear_function*)fptr;
}
/* }}} */

static int gear_test_class_call_method(gear_string *method, gear_object *object, INTERNAL_FUNCTION_PARAMETERS) /* {{{ */ {
	RETVAL_STR(gear_string_copy(method));
	return 0;
}
/* }}} */

static GEAR_METHOD(_GearTestTrait, testMethod) /* {{{ */ {
	RETURN_TRUE;
}
/* }}} */

static const gear_function_entry gear_test_trait_methods[] = {
    GEAR_ME(_GearTestTrait, testMethod, NULL, GEAR_ACC_PUBLIC)
    GEAR_FE_END
};

HYSS_MINIT_FUNCTION(gear_test)
{
	gear_class_entry class_entry;

	INIT_CLASS_ENTRY(class_entry, "_GearTestInterface", NULL);
	gear_test_interface = gear_register_internal_interface(&class_entry);
	gear_declare_class_constant_long(gear_test_interface, GEAR_STRL("DUMMY"), 0);
	INIT_CLASS_ENTRY(class_entry, "_GearTestClass", NULL);
	gear_test_class = gear_register_internal_class_ex(&class_entry, NULL);
	gear_class_implements(gear_test_class, 1, gear_test_interface);
	gear_test_class->create_object = gear_test_class_new;
	gear_test_class->get_static_method = gear_test_class_static_method_get;

	gear_declare_property_null(gear_test_class, "_StaticProp", sizeof("_StaticProp") - 1, GEAR_ACC_STATIC);

	INIT_CLASS_ENTRY(class_entry, "_GearTestChildClass", NULL);
	gear_test_child_class = gear_register_internal_class_ex(&class_entry, gear_test_class);

	memcpy(&gear_test_class_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	gear_test_class_handlers.get_method = gear_test_class_method_get;
	gear_test_class_handlers.call_method = gear_test_class_call_method;

	INIT_CLASS_ENTRY(class_entry, "_GearTestTrait", gear_test_trait_methods);
	gear_test_trait = gear_register_internal_class(&class_entry);
	gear_test_trait->ce_flags |= GEAR_ACC_TRAIT;
	gear_declare_property_null(gear_test_trait, "testProp", sizeof("testProp")-1, GEAR_ACC_PUBLIC);

	gear_register_class_alias("_GearTestClassAlias", gear_test_class);
	return SUCCESS;
}

HYSS_MSHUTDOWN_FUNCTION(gear_test)
{
	return SUCCESS;
}

HYSS_RINIT_FUNCTION(gear_test)
{
#if defined(COMPILE_DL_GEAR_TEST) && defined(ZTS)
	GEAR_PBCLS_CACHE_UPDATE();
#endif
	return SUCCESS;
}

HYSS_RSHUTDOWN_FUNCTION(gear_test)
{
	return SUCCESS;
}

HYSS_MINFO_FUNCTION(gear_test)
{
	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "gear-test extension", "enabled");
	hyss_info_print_table_end();
}

static const gear_function_entry gear_test_functions[] = {
	GEAR_FE(gear_test_array_return, arginfo_gear_test_array_return)
	GEAR_FE(gear_test_nullable_array_return, arginfo_gear_test_nullable_array_return)
	GEAR_FE(gear_test_void_return, arginfo_gear_test_void_return)
	GEAR_FE(gear_create_unterminated_string, NULL)
	GEAR_FE(gear_terminate_string, arginfo_gear_terminate_string)
	GEAR_FE(gear_leak_bytes, NULL)
	GEAR_FE(gear_leak_variable, arginfo_gear_leak_variable)
	GEAR_FE_END
};

gear_capi_entry gear_test_capi_entry = {
	STANDARD_CAPI_HEADER,
	"gear-test",
	gear_test_functions,
	HYSS_MINIT(gear_test),
	HYSS_MSHUTDOWN(gear_test),
	HYSS_RINIT(gear_test),
	HYSS_RSHUTDOWN(gear_test),
	HYSS_MINFO(gear_test),
	HYSS_GEAR_TEST_VERSION,
	STANDARD_CAPI_PROPERTIES
};

#ifdef COMPILE_DL_GEAR_TEST
#ifdef ZTS
GEAR_PBCLS_CACHE_DEFINE()
#endif
GEAR_GET_CAPI(gear_test)
#endif
