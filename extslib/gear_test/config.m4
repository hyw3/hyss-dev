HYSS_ARG_ENABLE(gear-test, whether to enable gear-test extension,
[  --enable-gear-test      Enable gear-test extension])

if test "$HYSS_GEAR_TEST" != "no"; then
  HYSS_NEW_EXTENSION(gear_test, test.c, $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
fi
