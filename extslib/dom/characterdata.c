/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"


/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_characterdata_substring_data, 0, 0, 2)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, count)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_characterdata_append_data, 0, 0, 1)
	GEAR_ARG_INFO(0, arg)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_characterdata_insert_data, 0, 0, 2)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, arg)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_characterdata_delete_data, 0, 0, 2)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, count)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_characterdata_replace_data, 0, 0, 3)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, count)
	GEAR_ARG_INFO(0, arg)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class DOMCharacterData extends DOMNode
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-FF21A306
* Since:
*/

const gear_function_entry hyss_dom_characterdata_class_functions[] = {
	HYSS_FALIAS(substringData, dom_characterdata_substring_data, arginfo_dom_characterdata_substring_data)
	HYSS_FALIAS(appendData, dom_characterdata_append_data, arginfo_dom_characterdata_append_data)
	HYSS_FALIAS(insertData, dom_characterdata_insert_data, arginfo_dom_characterdata_insert_data)
	HYSS_FALIAS(deleteData, dom_characterdata_delete_data, arginfo_dom_characterdata_delete_data)
	HYSS_FALIAS(replaceData, dom_characterdata_replace_data, arginfo_dom_characterdata_replace_data)
	HYSS_FE_END
};

/* {{{ data	string
readonly=no
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-72AB8359
Since:
*/
int dom_characterdata_data_read(dom_object *obj, zval *retval)
{
	xmlNodePtr nodep = dom_object_get_node(obj);
	xmlChar *content;

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	if ((content = xmlNodeGetContent(nodep)) != NULL) {
		ZVAL_STRING(retval, (char *) content);
		xmlFree(content);
	} else {
		ZVAL_EMPTY_STRING(retval);
	}

	return SUCCESS;
}

int dom_characterdata_data_write(dom_object *obj, zval *newval)
{
	xmlNode *nodep = dom_object_get_node(obj);
	gear_string *str;

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	str = zval_get_string(newval);

	xmlNodeSetContentLen(nodep, (xmlChar *) ZSTR_VAL(str), ZSTR_LEN(str) + 1);

	gear_string_release_ex(str, 0);
	return SUCCESS;
}

/* }}} */

/* {{{ length	long
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-7D61178C
Since:
*/
int dom_characterdata_length_read(dom_object *obj, zval *retval)
{
	xmlNodePtr nodep = dom_object_get_node(obj);
	xmlChar *content;
	long length = 0;

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	content = xmlNodeGetContent(nodep);

	if (content) {
		length = xmlUTF8Strlen(content);
		xmlFree(content);
	}

	ZVAL_LONG(retval, length);

	return SUCCESS;
}

/* }}} */

/* {{{ proto string dom_characterdata_substring_data(int offset, int count);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-6531BCCF
Since:
*/
HYSS_FUNCTION(dom_characterdata_substring_data)
{
	zval       *id;
	xmlChar    *cur;
	xmlChar    *substring;
	xmlNodePtr  node;
	gear_long        offset, count;
	int         length;
	dom_object	*intern;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oll", &id, dom_characterdata_class_entry, &offset, &count) == FAILURE) {
		return;
	}

	DOM_GET_OBJ(node, id, xmlNodePtr, intern);

	cur = xmlNodeGetContent(node);
	if (cur == NULL) {
		RETURN_FALSE;
	}

	length = xmlUTF8Strlen(cur);

	if (offset < 0 || count < 0 || GEAR_LONG_INT_OVFL(offset) || GEAR_LONG_INT_OVFL(count) || offset > length) {
		xmlFree(cur);
		hyss_dom_throw_error(INDEX_SIZE_ERR, dom_get_strict_error(intern->document));
		RETURN_FALSE;
	}

	if ((offset + count) > length) {
		count = length - offset;
	}

	substring = xmlUTF8Strsub(cur, (int)offset, (int)count);
	xmlFree(cur);

	if (substring) {
		RETVAL_STRING((char *) substring);
		xmlFree(substring);
	} else {
		RETVAL_EMPTY_STRING();
	}
}
/* }}} end dom_characterdata_substring_data */

/* {{{ proto void dom_characterdata_append_data(string arg);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-32791A2F
Since:
*/
HYSS_FUNCTION(dom_characterdata_append_data)
{
	zval *id;
	xmlNode *nodep;
	dom_object *intern;
	char *arg;
	size_t arg_len;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Os", &id, dom_characterdata_class_entry, &arg, &arg_len) == FAILURE) {
		return;
	}

	DOM_GET_OBJ(nodep, id, xmlNodePtr, intern);
#if LIBXML_VERSION < 20627
/* Implement logic from libxml xmlTextConcat to add support for comments and PI */
    if ((nodep->content == (xmlChar *) &(nodep->properties)) ||
        ((nodep->doc != NULL) && (nodep->doc->dict != NULL) &&
		xmlDictOwns(nodep->doc->dict, nodep->content))) {
	nodep->content = xmlStrncatNew(nodep->content, arg, arg_len);
    } else {
        nodep->content = xmlStrncat(nodep->content, arg, arg_len);
    }
    nodep->properties = NULL;
#else
	xmlTextConcat(nodep, (xmlChar *) arg, arg_len);
#endif
	RETURN_TRUE;
}
/* }}} end dom_characterdata_append_data */

/* {{{ proto void dom_characterdata_insert_data(int offset, string arg);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-3EDB695F
Since:
*/
HYSS_FUNCTION(dom_characterdata_insert_data)
{
	zval *id;
	xmlChar		*cur, *first, *second;
	xmlNodePtr  node;
	char		*arg;
	gear_long        offset;
	int         length;
	size_t arg_len;
	dom_object	*intern;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Ols", &id, dom_characterdata_class_entry, &offset, &arg, &arg_len) == FAILURE) {
		return;
	}

	DOM_GET_OBJ(node, id, xmlNodePtr, intern);

	cur = xmlNodeGetContent(node);
	if (cur == NULL) {
		RETURN_FALSE;
	}

	length = xmlUTF8Strlen(cur);

	if (offset < 0 || GEAR_LONG_INT_OVFL(offset) || offset > length) {
		xmlFree(cur);
		hyss_dom_throw_error(INDEX_SIZE_ERR, dom_get_strict_error(intern->document));
		RETURN_FALSE;
	}

	first = xmlUTF8Strndup(cur, (int)offset);
	second = xmlUTF8Strsub(cur, (int)offset, length - (int)offset);
	xmlFree(cur);

	xmlNodeSetContent(node, first);
	xmlNodeAddContent(node, (xmlChar *) arg);
	xmlNodeAddContent(node, second);

	xmlFree(first);
	xmlFree(second);

	RETURN_TRUE;
}
/* }}} end dom_characterdata_insert_data */

/* {{{ proto void dom_characterdata_delete_data(int offset, int count);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-7C603781
Since:
*/
HYSS_FUNCTION(dom_characterdata_delete_data)
{
	zval *id;
	xmlChar    *cur, *substring, *second;
	xmlNodePtr  node;
	gear_long        offset, count;
	int         length;
	dom_object	*intern;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oll", &id, dom_characterdata_class_entry, &offset, &count) == FAILURE) {
		return;
	}

	DOM_GET_OBJ(node, id, xmlNodePtr, intern);

	cur = xmlNodeGetContent(node);
	if (cur == NULL) {
		RETURN_FALSE;
	}

	length = xmlUTF8Strlen(cur);

	if (offset < 0 || count < 0 || GEAR_LONG_INT_OVFL(offset) || GEAR_LONG_INT_OVFL(count) || offset > length) {
		xmlFree(cur);
		hyss_dom_throw_error(INDEX_SIZE_ERR, dom_get_strict_error(intern->document));
		RETURN_FALSE;
	}

	if (offset > 0) {
		substring = xmlUTF8Strsub(cur, 0, (int)offset);
	} else {
		substring = NULL;
	}

	if ((offset + count) > length) {
		count = length - offset;
	}

	second = xmlUTF8Strsub(cur, (int)offset + (int)count, length - (int)offset);
	substring = xmlStrcat(substring, second);

	xmlNodeSetContent(node, substring);

	xmlFree(cur);
	xmlFree(second);
	xmlFree(substring);

	RETURN_TRUE;
}
/* }}} end dom_characterdata_delete_data */

/* {{{ proto void dom_characterdata_replace_data(int offset, int count, string arg);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-E5CBA7FB
Since:
*/
HYSS_FUNCTION(dom_characterdata_replace_data)
{
	zval *id;
	xmlChar		*cur, *substring, *second = NULL;
	xmlNodePtr  node;
	char		*arg;
	gear_long        offset, count;
	int         length;
	size_t arg_len;
	dom_object	*intern;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Olls", &id, dom_characterdata_class_entry, &offset, &count, &arg, &arg_len) == FAILURE) {
		return;
	}

	DOM_GET_OBJ(node, id, xmlNodePtr, intern);

	cur = xmlNodeGetContent(node);
	if (cur == NULL) {
		RETURN_FALSE;
	}

	length = xmlUTF8Strlen(cur);

	if (offset < 0 || count < 0 || GEAR_LONG_INT_OVFL(offset) || GEAR_LONG_INT_OVFL(count) || offset > length) {
		xmlFree(cur);
		hyss_dom_throw_error(INDEX_SIZE_ERR, dom_get_strict_error(intern->document));
		RETURN_FALSE;
	}

	if (offset > 0) {
		substring = xmlUTF8Strsub(cur, 0, (int)offset);
	} else {
		substring = NULL;
	}

	if ((offset + count) > length) {
		count = length - offset;
	}

	if (offset < length) {
		second = xmlUTF8Strsub(cur, (int)offset + count, length - (int)offset);
	}

	substring = xmlStrcat(substring, (xmlChar *) arg);
	substring = xmlStrcat(substring, second);

	xmlNodeSetContent(node, substring);

	xmlFree(cur);
	if (second) {
		xmlFree(second);
	}
	xmlFree(substring);

	RETURN_TRUE;
}
/* }}} end dom_characterdata_replace_data */

#endif

