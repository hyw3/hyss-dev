/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_XML_COMMON_H
#define HYSS_XML_COMMON_H

#include "extslib/libxml/hyss_libxml.h"

typedef libxml_doc_props *dom_doc_propsptr;

typedef struct _dom_object {
	void *ptr;
	hyss_libxml_ref_obj *document;
	HashTable *prop_handler;
	gear_object std;
} dom_object;

static inline dom_object *hyss_dom_obj_from_obj(gear_object *obj) {
	return (dom_object*)((char*)(obj) - XtOffsetOf(dom_object, std));
}

#define Z_DOMOBJ_P(zv)  hyss_dom_obj_from_obj(Z_OBJ_P((zv)))

#ifdef HYSS_WIN32
#	ifdef DOM_EXPORTS
#		define HYSS_DOM_EXPORT __declspec(dllexport)
#	elif !defined(DOM_LOCAL_DEFINES) /* Allow to counteract LNK4049 warning. */
#		define HYSS_DOM_EXPORT __declspec(dllimport)
#   else
#		define HYSS_DOM_EXPORT
#	endif /* DOM_EXPORTS */
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define HYSS_DOM_EXPORT __attribute__ ((visibility("default")))
#elif defined(HYSSAPI)
#   define HYSS_DOM_EXPORT HYSSAPI
#else
#   define HYSS_DOM_EXPORT
#endif

HYSS_DOM_EXPORT extern gear_class_entry *dom_node_class_entry;
HYSS_DOM_EXPORT dom_object *hyss_dom_object_get_data(xmlNodePtr obj);
HYSS_DOM_EXPORT gear_bool hyss_dom_create_object(xmlNodePtr obj, zval* return_value, dom_object *domobj);
HYSS_DOM_EXPORT xmlNodePtr dom_object_get_node(dom_object *obj);

#define DOM_XMLNS_NAMESPACE \
    (const xmlChar *) "http://www.w3.org/2000/xmlns/"

#define NODE_GET_OBJ(__ptr, __id, __prtype, __intern) { \
	__intern = Z_LIBXML_NODE_P(__id); \
	if (__intern->node == NULL || !(__ptr = (__prtype)__intern->node->node)) { \
  		hyss_error_docref(NULL, E_WARNING, "Couldn't fetch %s", \
			ZSTR_VAL(__intern->std.ce->name));\
  		RETURN_NULL();\
  	} \
}

#define DOC_GET_OBJ(__ptr, __id, __prtype, __intern) { \
	__intern = Z_LIBXML_NODE_P(__id); \
	if (__intern->document != NULL) { \
		if (!(__ptr = (__prtype)__intern->document->ptr)) { \
  			hyss_error_docref(NULL, E_WARNING, "Couldn't fetch %s", __intern->std.ce->name);\
  			RETURN_NULL();\
  		} \
	} \
}

#define DOM_RET_OBJ(obj, ret, domobject) \
	*ret = hyss_dom_create_object(obj, return_value, domobject)

#define DOM_GET_THIS(zval) \
	if (NULL == (zval = getThis())) { \
		hyss_error_docref(NULL, E_WARNING, "Underlying object missing"); \
		RETURN_FALSE; \
	}

#define DOM_GET_THIS_OBJ(__ptr, __id, __prtype, __intern) \
	DOM_GET_THIS(__id); \
	DOM_GET_OBJ(__ptr, __id, __prtype, __intern);

#endif

