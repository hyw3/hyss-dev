/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"
#include "dom_ce.h"

typedef struct _nodeIterator nodeIterator;
struct _nodeIterator {
	int cur;
	int index;
	xmlNode *node;
};

typedef struct _notationIterator notationIterator;
struct _notationIterator {
	int cur;
	int index;
	xmlNotation *notation;
};

#if LIBXML_VERSION >= 20908
static void itemHashScanner (void *payload, void *data, const xmlChar *name) /* {{{ */
#else
static void itemHashScanner (void *payload, void *data, xmlChar *name)
#endif
{
	nodeIterator *priv = (nodeIterator *)data;

	if(priv->cur < priv->index) {
		priv->cur++;
	} else {
		if(priv->node == NULL) {
			priv->node = (xmlNode *)payload;
		}
	}
}
/* }}} */

xmlNodePtr create_notation(const xmlChar *name, const xmlChar *ExternalID, const xmlChar *SystemID) /* {{{ */
{
	xmlEntityPtr ret;

	ret = (xmlEntityPtr) xmlMalloc(sizeof(xmlEntity));
    memset(ret, 0, sizeof(xmlEntity));
    ret->type = XML_NOTATION_NODE;
    ret->name = xmlStrdup(name);
	ret->ExternalID = xmlStrdup(ExternalID);
	ret->SystemID = xmlStrdup(SystemID);
	ret->length = 0;
	ret->content = NULL;
	ret->URI = NULL;
	ret->orig = NULL;
	ret->children = NULL;
	ret->parent = NULL;
	ret->doc = NULL;
	ret->_private = NULL;
	ret->last = NULL;
	ret->prev = NULL;
	return((xmlNodePtr) ret);
}
/* }}} */

xmlNode *hyss_dom_libxml_hash_iter(xmlHashTable *ht, int index) /* {{{ */
{
	xmlNode *nodep = NULL;
	nodeIterator *iter;
	int htsize;

	if ((htsize = xmlHashSize(ht)) > 0 && index < htsize) {
		iter = emalloc(sizeof(nodeIterator));
		iter->cur = 0;
		iter->index = index;
		iter->node = NULL;
		xmlHashScan(ht, itemHashScanner, iter);
		nodep = iter->node;
		efree(iter);
		return nodep;
	} else {
		return NULL;
	}
}
/* }}} */

xmlNode *hyss_dom_libxml_notation_iter(xmlHashTable *ht, int index) /* {{{ */
{
	notationIterator *iter;
	xmlNotation *notep = NULL;
	int htsize;

	if ((htsize = xmlHashSize(ht)) > 0 && index < htsize) {
		iter = emalloc(sizeof(notationIterator));
		iter->cur = 0;
		iter->index = index;
		iter->notation = NULL;
		xmlHashScan(ht, itemHashScanner, iter);
		notep = iter->notation;
		efree(iter);
		return create_notation(notep->name, notep->PublicID, notep->SystemID);
	} else {
		return NULL;
	}
}
/* }}} */

static void hyss_dom_iterator_dtor(gear_object_iterator *iter) /* {{{ */
{
	hyss_dom_iterator *iterator = (hyss_dom_iterator *)iter;

	zval_ptr_dtor(&iterator->intern.data);
	zval_ptr_dtor(&iterator->curobj);
}
/* }}} */

static int hyss_dom_iterator_valid(gear_object_iterator *iter) /* {{{ */
{

	hyss_dom_iterator *iterator = (hyss_dom_iterator *)iter;

	if (Z_TYPE(iterator->curobj) != IS_UNDEF) {
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */

zval *hyss_dom_iterator_current_data(gear_object_iterator *iter) /* {{{ */
{
	hyss_dom_iterator *iterator = (hyss_dom_iterator *)iter;

	return &iterator->curobj;
}
/* }}} */

static void hyss_dom_iterator_current_key(gear_object_iterator *iter, zval *key) /* {{{ */
{
	hyss_dom_iterator *iterator = (hyss_dom_iterator *)iter;
	zval *object = &iterator->intern.data;

	if (instanceof_function(Z_OBJCE_P(object), dom_nodelist_class_entry)) {
		ZVAL_LONG(key, iter->index);
	} else {
		dom_object *intern = Z_DOMOBJ_P(&iterator->curobj);

		if (intern != NULL && intern->ptr != NULL) {
			xmlNodePtr curnode = (xmlNodePtr)((hyss_libxml_node_ptr *)intern->ptr)->node;
			ZVAL_STRINGL(key, (char *) curnode->name, xmlStrlen(curnode->name));
		} else {
			ZVAL_NULL(key);
		}
	}
}
/* }}} */

static void hyss_dom_iterator_move_forward(gear_object_iterator *iter) /* {{{ */
{
	zval *object;
	xmlNodePtr curnode = NULL, basenode;
	dom_object *intern;
	dom_object *nnmap;
	dom_nnodemap_object *objmap;
	int previndex=0;
	HashTable *nodeht;
	zval *entry;
	gear_bool do_curobj_undef = 1;

	hyss_dom_iterator *iterator = (hyss_dom_iterator *)iter;

	object = &iterator->intern.data;
	nnmap = Z_DOMOBJ_P(object);
	objmap = (dom_nnodemap_object *)nnmap->ptr;

	intern = Z_DOMOBJ_P(&iterator->curobj);

	if (intern != NULL && intern->ptr != NULL) {
		if (objmap->nodetype != XML_ENTITY_NODE &&
			objmap->nodetype != XML_NOTATION_NODE) {
			if (objmap->nodetype == DOM_NODESET) {
				nodeht = HASH_OF(&objmap->baseobj_zv);
				gear_hash_move_forward_ex(nodeht, &iterator->pos);
				if ((entry = gear_hash_get_current_data_ex(nodeht, &iterator->pos))) {
					zval_ptr_dtor(&iterator->curobj);
					ZVAL_UNDEF(&iterator->curobj);
					ZVAL_COPY(&iterator->curobj, entry);
					do_curobj_undef = 0;
				}
			} else {
				curnode = (xmlNodePtr)((hyss_libxml_node_ptr *)intern->ptr)->node;
				if (objmap->nodetype == XML_ATTRIBUTE_NODE ||
					objmap->nodetype == XML_ELEMENT_NODE) {
					curnode = curnode->next;
				} else {
					/* Nav the tree evey time as this is LIVE */
					basenode = dom_object_get_node(objmap->baseobj);
					if (basenode && (basenode->type == XML_DOCUMENT_NODE ||
						basenode->type == XML_HTML_DOCUMENT_NODE)) {
						basenode = xmlDocGetRootElement((xmlDoc *) basenode);
					} else if (basenode) {
						basenode = basenode->children;
					} else {
						goto err;
					}
					curnode = dom_get_elements_by_tag_name_ns_raw(
						basenode, (char *) objmap->ns, (char *) objmap->local, &previndex, iter->index);
				}
			}
		} else {
			if (objmap->nodetype == XML_ENTITY_NODE) {
				curnode = hyss_dom_libxml_hash_iter(objmap->ht, iter->index);
			} else {
				curnode = hyss_dom_libxml_notation_iter(objmap->ht, iter->index);
			}
		}
	}
err:
	if (do_curobj_undef) {
		zval_ptr_dtor(&iterator->curobj);
		ZVAL_UNDEF(&iterator->curobj);
	}
	if (curnode) {
		hyss_dom_create_object(curnode, &iterator->curobj, objmap->baseobj);
	}
}
/* }}} */

static const gear_object_iterator_funcs hyss_dom_iterator_funcs = {
	hyss_dom_iterator_dtor,
	hyss_dom_iterator_valid,
	hyss_dom_iterator_current_data,
	hyss_dom_iterator_current_key,
	hyss_dom_iterator_move_forward,
	NULL,
	NULL
};

gear_object_iterator *hyss_dom_get_iterator(gear_class_entry *ce, zval *object, int by_ref) /* {{{ */
{
	dom_object *intern;
	dom_nnodemap_object *objmap;
	xmlNodePtr nodep, curnode=NULL;
	int curindex = 0;
	HashTable *nodeht;
	zval *entry;
	hyss_dom_iterator *iterator;

	if (by_ref) {
		gear_throw_error(NULL, "An iterator cannot be used with foreach by reference");
		return NULL;
	}
	iterator = emalloc(sizeof(hyss_dom_iterator));
	gear_iterator_init(&iterator->intern);

	ZVAL_COPY(&iterator->intern.data, object);
	iterator->intern.funcs = &hyss_dom_iterator_funcs;

	ZVAL_UNDEF(&iterator->curobj);

	intern = Z_DOMOBJ_P(object);
	objmap = (dom_nnodemap_object *)intern->ptr;
	if (objmap != NULL) {
		if (objmap->nodetype != XML_ENTITY_NODE &&
			objmap->nodetype != XML_NOTATION_NODE) {
			if (objmap->nodetype == DOM_NODESET) {
				nodeht = HASH_OF(&objmap->baseobj_zv);
				gear_hash_internal_pointer_reset_ex(nodeht, &iterator->pos);
				if ((entry = gear_hash_get_current_data_ex(nodeht, &iterator->pos))) {
					ZVAL_COPY(&iterator->curobj, entry);
				}
			} else {
				nodep = (xmlNode *)dom_object_get_node(objmap->baseobj);
				if (!nodep) {
					goto err;
				}
				if (objmap->nodetype == XML_ATTRIBUTE_NODE || objmap->nodetype == XML_ELEMENT_NODE) {
					if (objmap->nodetype == XML_ATTRIBUTE_NODE) {
						curnode = (xmlNodePtr) nodep->properties;
					} else {
						curnode = (xmlNodePtr) nodep->children;
					}
				} else {
					if (nodep->type == XML_DOCUMENT_NODE || nodep->type == XML_HTML_DOCUMENT_NODE) {
						nodep = xmlDocGetRootElement((xmlDoc *) nodep);
					} else {
						nodep = nodep->children;
					}
					curnode = dom_get_elements_by_tag_name_ns_raw(
						nodep, (char *) objmap->ns, (char *) objmap->local, &curindex, 0);
				}
			}
		} else {
			if (objmap->nodetype == XML_ENTITY_NODE) {
				curnode = hyss_dom_libxml_hash_iter(objmap->ht, 0);
			} else {
				curnode = hyss_dom_libxml_notation_iter(objmap->ht, 0);
			}
		}
	}
err:
	if (curnode) {
		hyss_dom_create_object(curnode, &iterator->curobj, objmap->baseobj);
	}

	return &iterator->intern;
}
/* }}} */

#endif

