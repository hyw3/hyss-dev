/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_DOM_H
#define HYSS_DOM_H

extern gear_capi_entry dom_capi_entry;
#define hyssext_dom_ptr &dom_capi_entry

#ifdef ZTS
#include "hypbc.h"
#endif

#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/tree.h>
#include <libxml/uri.h>
#include <libxml/xmlerror.h>
#include <libxml/xinclude.h>
#include <libxml/hash.h>
#include <libxml/c14n.h>
#if defined(LIBXML_HTML_ENABLED)
#include <libxml/HTMLparser.h>
#include <libxml/HTMLtree.h>
#endif
#if defined(LIBXML_XPATH_ENABLED)
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#endif
#if defined(LIBXML_XPTR_ENABLED)
#include <libxml/xpointer.h>
#endif
#ifdef HYSS_WIN32
#ifndef DOM_EXPORTS
#define DOM_EXPORTS
#endif
#endif

#include "xml_common.h"
#include "extslib/libxml/hyss_libxml.h"
#include "gear_exceptions.h"
#include "dom_ce.h"
/* DOM API_VERSION, please bump it up, if you change anything in the API
    therefore it's easier for the script-programmers to check, what's working how
   Can be checked with hyssversion("dom");
*/
#define DOM_API_VERSION "20031129"
/* Define a custom type for iterating using an unused nodetype */
#define DOM_NODESET XML_XINCLUDE_START

typedef struct _dom_xpath_object {
	int registerHySSFunctions;
	HashTable *registered_hyssfunctions;
	HashTable *node_list;
	dom_object dom;
} dom_xpath_object;

static inline dom_xpath_object *hyss_xpath_obj_from_obj(gear_object *obj) {
	return (dom_xpath_object*)((char*)(obj)
		- XtOffsetOf(dom_xpath_object, dom) - XtOffsetOf(dom_object, std));
}

#define Z_XPATHOBJ_P(zv)  hyss_xpath_obj_from_obj(Z_OBJ_P((zv)))

typedef struct _dom_nnodemap_object {
	dom_object *baseobj;
	zval baseobj_zv;
	int nodetype;
	xmlHashTable *ht;
	xmlChar *local;
	xmlChar *ns;
} dom_nnodemap_object;

typedef struct {
	gear_object_iterator intern;
	zval curobj;
	HashPosition pos;
} hyss_dom_iterator;

#include "dom_fe.h"

dom_object *dom_object_get_data(xmlNodePtr obj);
dom_doc_propsptr dom_get_doc_props(hyss_libxml_ref_obj *document);
gear_object *dom_objects_new(gear_class_entry *class_type);
gear_object *dom_nnodemap_objects_new(gear_class_entry *class_type);
#if defined(LIBXML_XPATH_ENABLED)
gear_object *dom_xpath_objects_new(gear_class_entry *class_type);
#endif
int dom_get_strict_error(hyss_libxml_ref_obj *document);
void hyss_dom_throw_error(int error_code, int strict_error);
void hyss_dom_throw_error_with_message(int error_code, char *error_message, int strict_error);
void node_list_unlink(xmlNodePtr node);
int dom_check_qname(char *qname, char **localname, char **prefix, int uri_len, int name_len);
xmlNsPtr dom_get_ns(xmlNodePtr node, char *uri, int *errorcode, char *prefix);
void dom_set_old_ns(xmlDoc *doc, xmlNs *ns);
xmlNsPtr dom_get_nsdecl(xmlNode *node, xmlChar *localName);
void dom_normalize (xmlNodePtr nodep);
xmlNode *dom_get_elements_by_tag_name_ns_raw(xmlNodePtr nodep, char *ns, char *local, int *cur, int index);
void hyss_dom_create_implementation(zval *retval);
int dom_hierarchy(xmlNodePtr parent, xmlNodePtr child);
int dom_has_feature(char *feature, char *version);
int dom_node_is_read_only(xmlNodePtr node);
int dom_node_children_valid(xmlNodePtr node);
void hyss_dom_create_interator(zval *return_value, int ce_type);
void dom_namednode_iter(dom_object *basenode, int ntype, dom_object *intern, xmlHashTablePtr ht, xmlChar *local, xmlChar *ns);
xmlNodePtr create_notation(const xmlChar *name, const xmlChar *ExternalID, const xmlChar *SystemID);
xmlNode *hyss_dom_libxml_hash_iter(xmlHashTable *ht, int index);
xmlNode *hyss_dom_libxml_notation_iter(xmlHashTable *ht, int index);
gear_object_iterator *hyss_dom_get_iterator(gear_class_entry *ce, zval *object, int by_ref);
void dom_set_doc_classmap(hyss_libxml_ref_obj *document, gear_class_entry *basece, gear_class_entry *ce);
zval *dom_nodelist_read_dimension(zval *object, zval *offset, int type, zval *rv);
int dom_nodelist_has_dimension(zval *object, zval *member, int check_empty);

#define REGISTER_DOM_CLASS(ce, name, parent_ce, funcs, entry) \
INIT_CLASS_ENTRY(ce, name, funcs); \
ce.create_object = dom_objects_new; \
entry = gear_register_internal_class_ex(&ce, parent_ce);

#define DOM_GET_OBJ(__ptr, __id, __prtype, __intern) { \
	__intern = Z_DOMOBJ_P(__id); \
	if (__intern->ptr == NULL || !(__ptr = (__prtype)((hyss_libxml_node_ptr *)__intern->ptr)->node)) { \
  		hyss_error_docref(NULL, E_WARNING, "Couldn't fetch %s", ZSTR_VAL(__intern->std.ce->name));\
  		RETURN_NULL();\
  	} \
}

#define DOM_NO_ARGS() \
	if (gear_parse_parameters_none() == FAILURE) { \
		return; \
	}

#define DOM_NOT_IMPLEMENTED() \
	hyss_error_docref(NULL, E_WARNING, "Not yet implemented"); \
	return;

#define DOM_NODELIST 0
#define DOM_NAMEDNODEMAP 1

HYSS_MINIT_FUNCTION(dom);
HYSS_MSHUTDOWN_FUNCTION(dom);
HYSS_MINFO_FUNCTION(dom);

#endif /* HYSS_DOM_H */

