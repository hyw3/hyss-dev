/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_entityreference_construct, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class DOMEntityReference extends DOMNode
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-11C98490
* Since:
*/

const gear_function_entry hyss_dom_entityreference_class_functions[] = {
	HYSS_ME(domentityreference, __construct, arginfo_dom_entityreference_construct, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};

/* {{{ proto DOMEntityReference::__construct(string name) */
HYSS_METHOD(domentityreference, __construct)
{
	zval *id = getThis();
	xmlNode *node;
	xmlNodePtr oldnode = NULL;
	dom_object *intern;
	char *name;
	size_t name_len, name_valid;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "s", &name, &name_len) == FAILURE) {
		return;
	}

	name_valid = xmlValidateName((xmlChar *) name, 0);
	if (name_valid != 0) {
		hyss_dom_throw_error(INVALID_CHARACTER_ERR, 1);
		RETURN_FALSE;
	}

	node = xmlNewReference(NULL, (xmlChar *) name);

	if (!node) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 1);
		RETURN_FALSE;
	}

	intern = Z_DOMOBJ_P(id);
	if (intern != NULL) {
		oldnode = dom_object_get_node(intern);
		if (oldnode != NULL) {
			hyss_libxml_node_free_resource(oldnode );
		}
		hyss_libxml_increment_node_ptr((hyss_libxml_node_object *)intern, node, (void *)intern);
	}
}
/* }}} end DOMEntityReference::__construct */

#endif

