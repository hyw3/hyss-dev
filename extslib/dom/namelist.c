/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_namelist_get_name, 0, 0, 1)
	GEAR_ARG_INFO(0, index)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_namelist_get_namespace_uri, 0, 0, 1)
	GEAR_ARG_INFO(0, index)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class DOMNameList
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#NameList
* Since: DOM Level 3
*/

const gear_function_entry hyss_dom_namelist_class_functions[] = {
	HYSS_FALIAS(getName, dom_namelist_get_name, arginfo_dom_namelist_get_name)
	HYSS_FALIAS(getNamespaceURI, dom_namelist_get_namespace_uri, arginfo_dom_namelist_get_namespace_uri)
	HYSS_FE_END
};

/* {{{ length	int
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#NameList-length
Since:
*/
int dom_namelist_length_read(dom_object *obj, zval *retval)
{
	ZVAL_STRING(retval, "TEST");
	return SUCCESS;
}

/* }}} */

/* {{{ proto string dom_namelist_get_name(int index);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#NameList-getName
Since:
*/
HYSS_FUNCTION(dom_namelist_get_name)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_namelist_get_name */

/* {{{ proto string dom_namelist_get_namespace_uri(int index);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#NameList-getNamespaceURI
Since:
*/
HYSS_FUNCTION(dom_namelist_get_namespace_uri)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_namelist_get_namespace_uri */

#endif

