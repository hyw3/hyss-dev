/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_implementation_get_feature, 0, 0, 2)
	GEAR_ARG_INFO(0, feature)
	GEAR_ARG_INFO(0, version)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_implementation_has_feature, 0, 0, 0)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_implementation_create_documenttype, 0, 0, 3)
	GEAR_ARG_INFO(0, qualifiedName)
	GEAR_ARG_INFO(0, publicId)
	GEAR_ARG_INFO(0, systemId)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_implementation_create_document, 0, 0, 3)
	GEAR_ARG_INFO(0, namespaceURI)
	GEAR_ARG_INFO(0, qualifiedName)
	GEAR_ARG_OBJ_INFO(0, docType, DOMDocumentType, 0)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class DOMImplementation
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-102161490
* Since:
*/

const gear_function_entry hyss_dom_domimplementation_class_functions[] = {
	HYSS_ME(domimplementation, getFeature, arginfo_dom_implementation_get_feature, GEAR_ACC_PUBLIC|GEAR_ACC_ALLOW_STATIC)
	HYSS_ME(domimplementation, hasFeature, arginfo_dom_implementation_has_feature, GEAR_ACC_PUBLIC|GEAR_ACC_ALLOW_STATIC)
	HYSS_ME(domimplementation, createDocumentType, arginfo_dom_implementation_create_documenttype, GEAR_ACC_PUBLIC|GEAR_ACC_ALLOW_STATIC)
	HYSS_ME(domimplementation, createDocument, arginfo_dom_implementation_create_document, GEAR_ACC_PUBLIC|GEAR_ACC_ALLOW_STATIC)
	HYSS_FE_END
};

/* {{{ proto bool dom_domimplementation_has_feature(string feature, string version);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-5CED94D7
Since:
*/
HYSS_METHOD(domimplementation, hasFeature)
{
	size_t feature_len, version_len;
	char *feature, *version;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss", &feature, &feature_len, &version, &version_len) == FAILURE) {
		return;
	}

	if (dom_has_feature(feature, version)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} end dom_domimplementation_has_feature */

/* {{{ proto DOMDocumentType dom_domimplementation_create_document_type(string qualifiedName, string publicId, string systemId);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Level-2-Core-DOM-createDocType
Since: DOM Level 2
*/
HYSS_METHOD(domimplementation, createDocumentType)
{
	xmlDtd *doctype;
	int ret;
	size_t name_len = 0, publicid_len = 0, systemid_len = 0;
	char *name = NULL, *publicid = NULL, *systemid = NULL;
	xmlChar *pch1 = NULL, *pch2 = NULL, *localname = NULL;
	xmlURIPtr uri;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|sss", &name, &name_len, &publicid, &publicid_len, &systemid, &systemid_len) == FAILURE) {
		return;
	}

	if (name_len == 0) {
		hyss_error_docref(NULL, E_WARNING, "qualifiedName is required");
		RETURN_FALSE;
	}

	if (publicid_len > 0) {
		pch1 = (xmlChar *) publicid;
	}
	if (systemid_len > 0) {
		pch2 = (xmlChar *) systemid;
	}

	uri = xmlParseURI(name);
	if (uri != NULL && uri->opaque != NULL) {
		localname = xmlStrdup((xmlChar *) uri->opaque);
		if (xmlStrchr(localname, (xmlChar) ':') != NULL) {
			hyss_dom_throw_error(NAMESPACE_ERR, 1);
			xmlFreeURI(uri);
			xmlFree(localname);
			RETURN_FALSE;
		}
	} else {
		localname = xmlStrdup((xmlChar *) name);
	}

	/* TODO: Test that localname has no invalid chars
	hyss_dom_throw_error(INVALID_CHARACTER_ERR,);
	*/

	if (uri) {
		xmlFreeURI(uri);
	}

	doctype = xmlCreateIntSubset(NULL, localname, pch1, pch2);
	xmlFree(localname);

	if (doctype == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Unable to create DocumentType");
		RETURN_FALSE;
	}

	DOM_RET_OBJ((xmlNodePtr) doctype, &ret, NULL);
}
/* }}} end dom_domimplementation_create_document_type */

/* {{{ proto DOMDocument dom_domimplementation_create_document(string namespaceURI, string qualifiedName, DOMDocumentType doctype);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Level-2-Core-DOM-createDocument
Since: DOM Level 2
*/
HYSS_METHOD(domimplementation, createDocument)
{
	zval *node = NULL;
	xmlDoc *docp;
	xmlNode *nodep;
	xmlDtdPtr doctype = NULL;
	xmlNsPtr nsptr = NULL;
	int ret, errorcode = 0;
	size_t uri_len = 0, name_len = 0;
	char *uri = NULL, *name = NULL;
	char *prefix = NULL, *localname = NULL;
	dom_object *doctobj;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|ssO", &uri, &uri_len, &name, &name_len, &node, dom_documenttype_class_entry) == FAILURE) {
		return;
	}

	if (node != NULL) {
		DOM_GET_OBJ(doctype, node, xmlDtdPtr, doctobj);
		if (doctype->type == XML_DOCUMENT_TYPE_NODE) {
			hyss_error_docref(NULL, E_WARNING, "Invalid DocumentType object");
			RETURN_FALSE;
		}
		if (doctype->doc != NULL) {
			hyss_dom_throw_error(WRONG_DOCUMENT_ERR, 1);
			RETURN_FALSE;
		}
	} else {
		doctobj = NULL;
	}

	if (name_len > 0) {
		errorcode = dom_check_qname(name, &localname, &prefix, 1, name_len);
		if (errorcode == 0 && uri_len > 0
			&& ((nsptr = xmlNewNs(NULL, (xmlChar *) uri, (xmlChar *) prefix)) == NULL)
		) {
			errorcode = NAMESPACE_ERR;
		}
	}

	if (prefix != NULL) {
		xmlFree(prefix);
	}

	if (errorcode != 0) {
		if (localname != NULL) {
			xmlFree(localname);
		}
		hyss_dom_throw_error(errorcode, 1);
		RETURN_FALSE;
	}

	/* currently letting libxml2 set the version string */
	docp = xmlNewDoc(NULL);
	if (!docp) {
		if (localname != NULL) {
			xmlFree(localname);
		}
		RETURN_FALSE;
	}

	if (doctype != NULL) {
		docp->intSubset = doctype;
		doctype->parent = docp;
		doctype->doc = docp;
		docp->children = (xmlNodePtr) doctype;
		docp->last = (xmlNodePtr) doctype;
	}

	if (localname != NULL) {
		nodep = xmlNewDocNode(docp, nsptr, (xmlChar *) localname, NULL);
		if (!nodep) {
			if (doctype != NULL) {
				docp->intSubset = NULL;
				doctype->parent = NULL;
				doctype->doc = NULL;
				docp->children = NULL;
				docp->last = NULL;
			}
			xmlFreeDoc(docp);
			xmlFree(localname);
			/* Need some type of error here */
			hyss_error_docref(NULL, E_WARNING, "Unexpected Error");
			RETURN_FALSE;
		}

		nodep->nsDef = nsptr;

		xmlDocSetRootElement(docp, nodep);
		xmlFree(localname);
	}

	DOM_RET_OBJ((xmlNodePtr) docp, &ret, NULL);

	if (doctobj != NULL) {
		doctobj->document = ((dom_object *)((hyss_libxml_node_ptr *)docp->_private)->_private)->document;
		hyss_libxml_increment_doc_ref((hyss_libxml_node_object *)doctobj, docp);
	}
}
/* }}} end dom_domimplementation_create_document */

/* {{{ proto DOMNode dom_domimplementation_get_feature(string feature, string version);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMImplementation3-getFeature
Since: DOM Level 3
*/
HYSS_METHOD(domimplementation, getFeature)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_domimplementation_get_feature */

#endif

