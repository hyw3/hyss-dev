/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DOM_FE_H
#define DOM_FE_H

extern const gear_function_entry hyss_dom_domexception_class_functions[];
extern const gear_function_entry hyss_dom_domstringlist_class_functions[];
extern const gear_function_entry hyss_dom_namelist_class_functions[];
extern const gear_function_entry hyss_dom_domimplementationlist_class_functions[];
extern const gear_function_entry hyss_dom_domimplementationsource_class_functions[];
extern const gear_function_entry hyss_dom_domimplementation_class_functions[];
extern const gear_function_entry hyss_dom_documentfragment_class_functions[];
extern const gear_function_entry hyss_dom_document_class_functions[];
extern const gear_function_entry hyss_dom_node_class_functions[];
extern const gear_function_entry hyss_dom_nodelist_class_functions[];
extern const gear_function_entry hyss_dom_namednodemap_class_functions[];
extern const gear_function_entry hyss_dom_characterdata_class_functions[];
extern const gear_function_entry hyss_dom_attr_class_functions[];
extern const gear_function_entry hyss_dom_element_class_functions[];
extern const gear_function_entry hyss_dom_text_class_functions[];
extern const gear_function_entry hyss_dom_comment_class_functions[];
extern const gear_function_entry hyss_dom_typeinfo_class_functions[];
extern const gear_function_entry hyss_dom_userdatahandler_class_functions[];
extern const gear_function_entry hyss_dom_domerror_class_functions[];
extern const gear_function_entry hyss_dom_domerrorhandler_class_functions[];
extern const gear_function_entry hyss_dom_domlocator_class_functions[];
extern const gear_function_entry hyss_dom_domconfiguration_class_functions[];
extern const gear_function_entry hyss_dom_cdatasection_class_functions[];
extern const gear_function_entry hyss_dom_documenttype_class_functions[];
extern const gear_function_entry hyss_dom_notation_class_functions[];
extern const gear_function_entry hyss_dom_entity_class_functions[];
extern const gear_function_entry hyss_dom_entityreference_class_functions[];
extern const gear_function_entry hyss_dom_processinginstruction_class_functions[];
extern const gear_function_entry hyss_dom_string_extend_class_functions[];
extern const gear_function_entry hyss_dom_xpath_class_functions[];

/* domexception errors */
typedef enum {
/* HYSS_ERR is non-spec code for HYSS errors: */
	HYSS_ERR                        = 0,
	INDEX_SIZE_ERR                 = 1,
	DOMSTRING_SIZE_ERR             = 2,
	HIERARCHY_REQUEST_ERR          = 3,
	WRONG_DOCUMENT_ERR             = 4,
	INVALID_CHARACTER_ERR          = 5,
	NO_DATA_ALLOWED_ERR            = 6,
	NO_MODIFICATION_ALLOWED_ERR    = 7,
	NOT_FOUND_ERR                  = 8,
	NOT_SUPPORTED_ERR              = 9,
	INUSE_ATTRIBUTE_ERR            = 10,
/* Introduced in DOM Level 2: */
	INVALID_STATE_ERR              = 11,
/* Introduced in DOM Level 2: */
	SYNTAX_ERR                     = 12,
/* Introduced in DOM Level 2: */
	INVALID_MODIFICATION_ERR       = 13,
/* Introduced in DOM Level 2: */
	NAMESPACE_ERR                  = 14,
/* Introduced in DOM Level 2: */
	INVALID_ACCESS_ERR             = 15,
/* Introduced in DOM Level 3: */
	VALIDATION_ERR                 = 16
} dom_exception_code;

/* domstringlist methods */
HYSS_FUNCTION(dom_domstringlist_item);

/* domnamelist methods */
HYSS_FUNCTION(dom_namelist_get_name);
HYSS_FUNCTION(dom_namelist_get_namespace_uri);

/* domimplementationlist methods */
HYSS_FUNCTION(dom_domimplementationlist_item);

/* domimplementationsource methods */
HYSS_FUNCTION(dom_domimplementationsource_get_domimplementation);
HYSS_FUNCTION(dom_domimplementationsource_get_domimplementations);

/* domimplementation methods */
HYSS_METHOD(domimplementation, hasFeature);
HYSS_METHOD(domimplementation, createDocumentType);
HYSS_METHOD(domimplementation, createDocument);
HYSS_METHOD(domimplementation, getFeature);

/* domdocumentfragment methods */
HYSS_METHOD(domdocumentfragment, __construct);
HYSS_METHOD(domdocumentfragment, appendXML);

/* domdocument methods */
HYSS_FUNCTION(dom_document_create_element);
HYSS_FUNCTION(dom_document_create_document_fragment);
HYSS_FUNCTION(dom_document_create_text_node);
HYSS_FUNCTION(dom_document_create_comment);
HYSS_FUNCTION(dom_document_create_cdatasection);
HYSS_FUNCTION(dom_document_create_processing_instruction);
HYSS_FUNCTION(dom_document_create_attribute);
HYSS_FUNCTION(dom_document_create_entity_reference);
HYSS_FUNCTION(dom_document_get_elements_by_tag_name);
HYSS_FUNCTION(dom_document_import_node);
HYSS_FUNCTION(dom_document_create_element_ns);
HYSS_FUNCTION(dom_document_create_attribute_ns);
HYSS_FUNCTION(dom_document_get_elements_by_tag_name_ns);
HYSS_FUNCTION(dom_document_get_element_by_id);
HYSS_FUNCTION(dom_document_adopt_node);
HYSS_FUNCTION(dom_document_normalize_document);
HYSS_FUNCTION(dom_document_rename_node);
HYSS_METHOD(domdocument, __construct);
	/* convienience methods */
HYSS_METHOD(domdocument, load);
HYSS_FUNCTION(dom_document_save);
HYSS_METHOD(domdocument, loadXML);
HYSS_FUNCTION(dom_document_savexml);
HYSS_FUNCTION(dom_document_validate);
HYSS_FUNCTION(dom_document_xinclude);
HYSS_METHOD(domdocument, registerNodeClass);

#if defined(LIBXML_HTML_ENABLED)
HYSS_METHOD(domdocument, loadHTML);
HYSS_METHOD(domdocument, loadHTMLFile);
HYSS_FUNCTION(dom_document_save_html);
HYSS_FUNCTION(dom_document_save_html_file);
#endif  /* defined(LIBXML_HTML_ENABLED) */

#if defined(LIBXML_SCHEMAS_ENABLED)
HYSS_FUNCTION(dom_document_schema_validate_file);
HYSS_FUNCTION(dom_document_schema_validate_xml);
HYSS_FUNCTION(dom_document_relaxNG_validate_file);
HYSS_FUNCTION(dom_document_relaxNG_validate_xml);
#endif

/* domnode methods */
HYSS_FUNCTION(dom_node_insert_before);
HYSS_FUNCTION(dom_node_replace_child);
HYSS_FUNCTION(dom_node_remove_child);
HYSS_FUNCTION(dom_node_append_child);
HYSS_FUNCTION(dom_node_has_child_nodes);
HYSS_FUNCTION(dom_node_clone_node);
HYSS_FUNCTION(dom_node_normalize);
HYSS_FUNCTION(dom_node_is_supported);
HYSS_FUNCTION(dom_node_has_attributes);
HYSS_FUNCTION(dom_node_compare_document_position);
HYSS_FUNCTION(dom_node_is_same_node);
HYSS_FUNCTION(dom_node_lookup_prefix);
HYSS_FUNCTION(dom_node_is_default_namespace);
HYSS_FUNCTION(dom_node_lookup_namespace_uri);
HYSS_FUNCTION(dom_node_is_equal_node);
HYSS_FUNCTION(dom_node_get_feature);
HYSS_FUNCTION(dom_node_set_user_data);
HYSS_FUNCTION(dom_node_get_user_data);
HYSS_METHOD(domnode, C14N);
HYSS_METHOD(domnode, C14NFile);
HYSS_METHOD(domnode, getNodePath);
HYSS_METHOD(domnode, getLineNo);

/* domnodelist methods */
HYSS_FUNCTION(dom_nodelist_item);
HYSS_FUNCTION(dom_nodelist_count);

/* domnamednodemap methods */
HYSS_FUNCTION(dom_namednodemap_get_named_item);
HYSS_FUNCTION(dom_namednodemap_set_named_item);
HYSS_FUNCTION(dom_namednodemap_remove_named_item);
HYSS_FUNCTION(dom_namednodemap_item);
HYSS_FUNCTION(dom_namednodemap_get_named_item_ns);
HYSS_FUNCTION(dom_namednodemap_set_named_item_ns);
HYSS_FUNCTION(dom_namednodemap_remove_named_item_ns);
HYSS_FUNCTION(dom_namednodemap_count);

/* domcharacterdata methods */
HYSS_FUNCTION(dom_characterdata_substring_data);
HYSS_FUNCTION(dom_characterdata_append_data);
HYSS_FUNCTION(dom_characterdata_insert_data);
HYSS_FUNCTION(dom_characterdata_delete_data);
HYSS_FUNCTION(dom_characterdata_replace_data);

/* domattr methods */
HYSS_FUNCTION(dom_attr_is_id);
HYSS_METHOD(domattr, __construct);

/* domelement methods */
HYSS_FUNCTION(dom_element_get_attribute);
HYSS_FUNCTION(dom_element_set_attribute);
HYSS_FUNCTION(dom_element_remove_attribute);
HYSS_FUNCTION(dom_element_get_attribute_node);
HYSS_FUNCTION(dom_element_set_attribute_node);
HYSS_FUNCTION(dom_element_remove_attribute_node);
HYSS_FUNCTION(dom_element_get_elements_by_tag_name);
HYSS_FUNCTION(dom_element_get_attribute_ns);
HYSS_FUNCTION(dom_element_set_attribute_ns);
HYSS_FUNCTION(dom_element_remove_attribute_ns);
HYSS_FUNCTION(dom_element_get_attribute_node_ns);
HYSS_FUNCTION(dom_element_set_attribute_node_ns);
HYSS_FUNCTION(dom_element_get_elements_by_tag_name_ns);
HYSS_FUNCTION(dom_element_has_attribute);
HYSS_FUNCTION(dom_element_has_attribute_ns);
HYSS_FUNCTION(dom_element_set_id_attribute);
HYSS_FUNCTION(dom_element_set_id_attribute_ns);
HYSS_FUNCTION(dom_element_set_id_attribute_node);
HYSS_METHOD(domelement, __construct);

/* domtext methods */
HYSS_FUNCTION(dom_text_split_text);
HYSS_FUNCTION(dom_text_is_whitespace_in_element_content);
HYSS_FUNCTION(dom_text_replace_whole_text);
HYSS_METHOD(domtext, __construct);

/* domcomment methods */
HYSS_METHOD(domcomment, __construct);

/* domtypeinfo methods */

/* domuserdatahandler methods */
HYSS_FUNCTION(dom_userdatahandler_handle);

/* domdomerror methods */

/* domerrorhandler methods */
HYSS_FUNCTION(dom_domerrorhandler_handle_error);

/* domlocator methods */

/* domconfiguration methods */
HYSS_FUNCTION(dom_domconfiguration_set_parameter);
HYSS_FUNCTION(dom_domconfiguration_get_parameter);
HYSS_FUNCTION(dom_domconfiguration_can_set_parameter);

/* domcdatasection methods */
HYSS_METHOD(domcdatasection, __construct);

/* domdocumenttype methods */

/* domnotation methods */

/* domentity methods */

/* domentityreference methods */
HYSS_METHOD(domentityreference, __construct);

/* domprocessinginstruction methods */
HYSS_METHOD(domprocessinginstruction, __construct);

/* string_extend methods */
HYSS_FUNCTION(dom_string_extend_find_offset16);
HYSS_FUNCTION(dom_string_extend_find_offset32);

#if defined(LIBXML_XPATH_ENABLED)
/* xpath methods */
HYSS_METHOD(domxpath, __construct);
HYSS_FUNCTION(dom_xpath_register_ns);
HYSS_FUNCTION(dom_xpath_query);
HYSS_FUNCTION(dom_xpath_evaluate);
HYSS_FUNCTION(dom_xpath_register_hyss_functions);
#endif

#endif /* DOM_FE_H */

