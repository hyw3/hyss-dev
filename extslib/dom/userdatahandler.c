/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"


/*
* class domuserdatahandler
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#UserDataHandler
* Since: DOM Level 3
*/

const gear_function_entry hyss_dom_userdatahandler_class_functions[] = {
	HYSS_FALIAS(handle, dom_userdatahandler_handle, NULL)
	HYSS_FE_END
};

/* {{{ attribute protos, not implemented yet */

/* {{{ proto dom_void dom_userdatahandler_handle(short operation, string key, domobject data, node src, node dst);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-handleUserDataEvent
Since:
*/
HYSS_FUNCTION(dom_userdatahandler_handle)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_userdatahandler_handle */

/* }}} */

#endif

