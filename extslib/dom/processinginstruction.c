/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"


/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_processinginstruction_construct, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class DOMProcessingInstruction extends DOMNode
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-1004215813
* Since:
*/

const gear_function_entry hyss_dom_processinginstruction_class_functions[] = {
	HYSS_ME(domprocessinginstruction, __construct, arginfo_dom_processinginstruction_construct, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};

/* {{{ proto DOMProcessingInstruction::__construct(string name, [string value]); */
HYSS_METHOD(domprocessinginstruction, __construct)
{
	zval *id = getThis();
	xmlNodePtr nodep = NULL, oldnode = NULL;
	dom_object *intern;
	char *name, *value = NULL;
	size_t name_len, value_len;
	int name_valid;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "s|s", &name, &name_len, &value, &value_len) == FAILURE) {
		return;
	}

	name_valid = xmlValidateName((xmlChar *) name, 0);
	if (name_valid != 0) {
		hyss_dom_throw_error(INVALID_CHARACTER_ERR, 1);
		RETURN_FALSE;
	}

	nodep = xmlNewPI((xmlChar *) name, (xmlChar *) value);

	if (!nodep) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 1);
		RETURN_FALSE;
	}

	intern = Z_DOMOBJ_P(id);
	oldnode = dom_object_get_node(intern);
	if (oldnode != NULL) {
		hyss_libxml_node_free_resource(oldnode );
	}
	hyss_libxml_increment_node_ptr((hyss_libxml_node_object *)intern, nodep, (void *)intern);
}
/* }}} end DOMProcessingInstruction::__construct */

/* {{{ target	string
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-1478689192
Since:
*/
int dom_processinginstruction_target_read(dom_object *obj, zval *retval)
{
	xmlNodePtr nodep = dom_object_get_node(obj);

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	ZVAL_STRING(retval, (char *) (nodep->name));

	return SUCCESS;
}

/* }}} */

/* {{{ data	string
readonly=no
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-837822393
Since:
*/
int dom_processinginstruction_data_read(dom_object *obj, zval *retval)
{
	xmlNodePtr nodep;
	xmlChar *content;

	nodep = dom_object_get_node(obj);

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	if ((content = xmlNodeGetContent(nodep)) != NULL) {
		ZVAL_STRING(retval, (char *) content);
		xmlFree(content);
	} else {
		ZVAL_EMPTY_STRING(retval);
	}

	return SUCCESS;
}

int dom_processinginstruction_data_write(dom_object *obj, zval *newval)
{
	xmlNode *nodep = dom_object_get_node(obj);
	gear_string *str;

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	str = zval_get_string(newval);

	xmlNodeSetContentLen(nodep, (xmlChar *) ZSTR_VAL(str), ZSTR_LEN(str) + 1);

	gear_string_release_ex(str, 0);
	return SUCCESS;
}

/* }}} */

#endif

