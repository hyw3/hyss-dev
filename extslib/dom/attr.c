/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if HAVE_LIBXML && HAVE_DOM

#include "hyss_dom.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_attr_is_id, 0, 0, 0)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_attr_construct, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class DOMAttr extends DOMNode
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-637646024
* Since:
*/

const gear_function_entry hyss_dom_attr_class_functions[] = {
	HYSS_FALIAS(isId, dom_attr_is_id, arginfo_dom_attr_is_id)
	HYSS_ME(domattr, __construct, arginfo_dom_attr_construct, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};

/* {{{ proto DOMAttr::__construct(string name, [string value]) */
HYSS_METHOD(domattr, __construct)
{
	zval *id = getThis();
	xmlAttrPtr nodep = NULL;
	xmlNodePtr oldnode = NULL;
	dom_object *intern;
	char *name, *value = NULL;
	size_t name_len, value_len, name_valid;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "s|s", &name, &name_len, &value, &value_len) == FAILURE) {
		return;
	}

	intern = Z_DOMOBJ_P(id);

	name_valid = xmlValidateName((xmlChar *) name, 0);
	if (name_valid != 0) {
		hyss_dom_throw_error(INVALID_CHARACTER_ERR, 1);
		RETURN_FALSE;
	}

	nodep = xmlNewProp(NULL, (xmlChar *) name, (xmlChar *) value);

	if (!nodep) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 1);
		RETURN_FALSE;
	}

	oldnode = dom_object_get_node(intern);
	if (oldnode != NULL) {
		hyss_libxml_node_free_resource(oldnode );
	}
	hyss_libxml_increment_node_ptr((hyss_libxml_node_object *)intern, (xmlNodePtr)nodep, (void *)intern);
}

/* }}} end DOMAttr::__construct */

/* {{{ name	string
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-1112119403
Since:
*/
int dom_attr_name_read(dom_object *obj, zval *retval)
{
	xmlAttrPtr attrp;

	attrp = (xmlAttrPtr) dom_object_get_node(obj);

	if (attrp == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	ZVAL_STRING(retval, (char *) attrp->name);

	return SUCCESS;
}

/* }}} */

/* {{{ specified	boolean
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-862529273
Since:
*/
int dom_attr_specified_read(dom_object *obj, zval *retval)
{
	/* TODO */
	ZVAL_TRUE(retval);
	return SUCCESS;
}

/* }}} */

/* {{{ value	string
readonly=no
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-221662474
Since:
*/
int dom_attr_value_read(dom_object *obj, zval *retval)
{
	xmlAttrPtr attrp = (xmlAttrPtr) dom_object_get_node(obj);
	xmlChar *content;

	if (attrp == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	if ((content = xmlNodeGetContent((xmlNodePtr) attrp)) != NULL) {
		ZVAL_STRING(retval, (char *) content);
		xmlFree(content);
	} else {
		ZVAL_EMPTY_STRING(retval);
	}

	return SUCCESS;

}

int dom_attr_value_write(dom_object *obj, zval *newval)
{
	gear_string *str;
	xmlAttrPtr attrp = (xmlAttrPtr) dom_object_get_node(obj);

	if (attrp == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	if (attrp->children) {
		node_list_unlink(attrp->children);
	}

	str = zval_get_string(newval);

	xmlNodeSetContentLen((xmlNodePtr) attrp, (xmlChar *) ZSTR_VAL(str), ZSTR_LEN(str) + 1);

	gear_string_release_ex(str, 0);
	return SUCCESS;
}

/* }}} */

/* {{{ ownerElement	DOMElement
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Attr-ownerElement
Since: DOM Level 2
*/
int dom_attr_owner_element_read(dom_object *obj, zval *retval)
{
	xmlNodePtr nodep, nodeparent;

	nodep = dom_object_get_node(obj);

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	nodeparent = nodep->parent;
	if (!nodeparent) {
		ZVAL_NULL(retval);
		return SUCCESS;
	}

	hyss_dom_create_object(nodeparent, retval, obj);
	return SUCCESS;

}

/* }}} */

/* {{{ schemaTypeInfo	DOMTypeInfo
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Attr-schemaTypeInfo
Since: DOM Level 3
*/
int dom_attr_schema_type_info_read(dom_object *obj, zval *retval)
{
	/* TODO */
	ZVAL_NULL(retval);
	return SUCCESS;
}

/* }}} */

/* {{{ proto bool dom_attr_is_id()
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Attr-isId
Since: DOM Level 3
*/
HYSS_FUNCTION(dom_attr_is_id)
{
	zval *id;
	dom_object *intern;
	xmlAttrPtr attrp;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O", &id, dom_attr_class_entry) == FAILURE) {
		return;
	}

	DOM_GET_OBJ(attrp, id, xmlAttrPtr, intern);

	if (attrp->atype == XML_ATTRIBUTE_ID) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} end dom_attr_is_id */

#endif

