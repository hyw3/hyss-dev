/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"


/*
* class domlocator
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Interfaces-DOMLocator
* Since: DOM Level 3
*/

const gear_function_entry hyss_dom_domlocator_class_functions[] = {
	HYSS_FE_END
};

/* {{{ attribute protos, not implemented yet */

/* {{{ line_number	long
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMLocator-line-number
Since:
*/
int dom_domlocator_line_number_read(dom_object *obj, zval *retval)
{
	ZVAL_STRING(retval, "TEST");
	return SUCCESS;
}

/* }}} */

/* {{{ column_number	long
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMLocator-column-number
Since:
*/
int dom_domlocator_column_number_read(dom_object *obj, zval *retval)
{
	ZVAL_STRING(retval, "TEST");
	return SUCCESS;
}

/* }}} */

/* {{{ offset	long
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMLocator-offset
Since:
*/
int dom_domlocator_offset_read(dom_object *obj, zval *retval)
{
	ZVAL_STRING(retval, "TEST");
	return SUCCESS;
}

/* }}} */

/* {{{ related_node	node
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMLocator-node
Since:
*/
int dom_domlocator_related_node_read(dom_object *obj, zval *retval)
{
	ZVAL_STRING(retval, "TEST");
	return SUCCESS;
}

/* }}} */

/* {{{ uri	string
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMLocator-uri
Since:
*/
int dom_domlocator_uri_read(dom_object *obj, zval *retval)
{
	ZVAL_STRING(retval, "TEST");
	return SUCCESS;
}

/* }}} */

/* }}} */

#endif

