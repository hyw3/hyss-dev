/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_configuration_set_parameter, 0, 0, 2)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_configuration_get_parameter, 0, 0, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_configuration_can_set_parameter, 0, 0, 0)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class domdomconfiguration
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMConfiguration
* Since: DOM Level 3
*/

const gear_function_entry hyss_dom_domconfiguration_class_functions[] = {
	HYSS_FALIAS(setParameter, dom_domconfiguration_set_parameter, arginfo_dom_configuration_set_parameter)
	HYSS_FALIAS(getParameter, dom_domconfiguration_get_parameter, arginfo_dom_configuration_get_parameter)
	HYSS_FALIAS(canSetParameter, dom_domconfiguration_can_set_parameter, arginfo_dom_configuration_can_set_parameter)
	HYSS_FE_END
};

/* {{{ attribute protos, not implemented yet */

/* {{{ proto dom_void dom_domconfiguration_set_parameter(string name, domuserdata value);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMConfiguration-property
Since:
*/
HYSS_FUNCTION(dom_domconfiguration_set_parameter)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_domconfiguration_set_parameter */

/* {{{ proto domdomuserdata dom_domconfiguration_get_parameter(string name);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMConfiguration-getParameter
Since:
*/
HYSS_FUNCTION(dom_domconfiguration_get_parameter)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_domconfiguration_get_parameter */

/* {{{ proto bool dom_domconfiguration_can_set_parameter(string name, domuserdata value);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#DOMConfiguration-canSetParameter
Since:
*/
HYSS_FUNCTION(dom_domconfiguration_can_set_parameter)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_domconfiguration_can_set_parameter */

/* }}} */

#endif

