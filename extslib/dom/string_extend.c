/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_string_extend_find_offset16, 0, 0, 1)
	GEAR_ARG_INFO(0, offset32)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_string_extend_find_offset32, 0, 0, 1)
	GEAR_ARG_INFO(0, offset16)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class domstringextend
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#i18n-methods-StringExtend
* Since:
*/

const gear_function_entry hyss_dom_string_extend_class_functions[] = {
	HYSS_FALIAS(findOffset16, dom_string_extend_find_offset16, arginfo_dom_string_extend_find_offset16)
	HYSS_FALIAS(findOffset32, dom_string_extend_find_offset32, arginfo_dom_string_extend_find_offset32)
	HYSS_FE_END
};

/* {{{ attribute protos, not implemented yet */

/* {{{ proto int dom_string_extend_find_offset16(int offset32);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#i18n-methods-StringExtend-findOffset16
Since:
*/
HYSS_FUNCTION(dom_string_extend_find_offset16)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_string_extend_find_offset16 */

/* {{{ proto int dom_string_extend_find_offset32(int offset16);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#i18n-methods-StringExtend-findOffset32
Since:
*/
HYSS_FUNCTION(dom_string_extend_find_offset32)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_string_extend_find_offset32 */

/* }}} */

#endif

