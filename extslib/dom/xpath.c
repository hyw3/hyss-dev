/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"

#define HYSS_DOM_XPATH_QUERY 0
#define HYSS_DOM_XPATH_EVALUATE 1

/*
* class DOMXPath
*/

#if defined(LIBXML_XPATH_ENABLED)

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_xpath_construct, 0, 0, 1)
	GEAR_ARG_OBJ_INFO(0, doc, DOMDocument, 0)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_xpath_register_ns, 0, 0, 2)
	GEAR_ARG_INFO(0, prefix)
	GEAR_ARG_INFO(0, uri)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_xpath_query, 0, 0, 1)
	GEAR_ARG_INFO(0, expr)
	GEAR_ARG_OBJ_INFO(0, context, DOMNode, 1)
	GEAR_ARG_INFO(0, registerNodeNS)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_xpath_evaluate, 0, 0, 1)
	GEAR_ARG_INFO(0, expr)
	GEAR_ARG_OBJ_INFO(0, context, DOMNode, 1)
	GEAR_ARG_INFO(0, registerNodeNS)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_xpath_register_hyss_functions, 0, 0, 0)
GEAR_END_ARG_INFO();
/* }}} */

const gear_function_entry hyss_dom_xpath_class_functions[] = {
	HYSS_ME(domxpath, __construct, arginfo_dom_xpath_construct, GEAR_ACC_PUBLIC)
	HYSS_FALIAS(registerNamespace, dom_xpath_register_ns, arginfo_dom_xpath_register_ns)
	HYSS_FALIAS(query, dom_xpath_query, arginfo_dom_xpath_query)
	HYSS_FALIAS(evaluate, dom_xpath_evaluate, arginfo_dom_xpath_evaluate)
	HYSS_FALIAS(registerHySSFunctions, dom_xpath_register_hyss_functions, arginfo_dom_xpath_register_hyss_functions)
	HYSS_FE_END
};


static void dom_xpath_ext_function_hyss(xmlXPathParserContextPtr ctxt, int nargs, int type) /* {{{ */
{
	zval retval;
	int result, i;
	int error = 0;
	gear_fcall_info fci;
	xmlXPathObjectPtr obj;
	char *str;
	gear_string *callable = NULL;
	dom_xpath_object *intern;


	if (! gear_is_executing()) {
		xmlGenericError(xmlGenericErrorContext,
		"xmlExtFunctionTest: Function called from outside of HYSS\n");
		error = 1;
	} else {
		intern = (dom_xpath_object *) ctxt->context->userData;
		if (intern == NULL) {
			xmlGenericError(xmlGenericErrorContext,
			"xmlExtFunctionTest: failed to get the internal object\n");
			error = 1;
		}
		else if (intern->registerHySSFunctions == 0) {
			xmlGenericError(xmlGenericErrorContext,
			"xmlExtFunctionTest: HYSS Object did not register HYSS functions\n");
			error = 1;
		}
	}

	if (error == 1) {
		for (i = nargs - 1; i >= 0; i--) {
			obj = valuePop(ctxt);
			xmlXPathFreeObject(obj);
		}
		return;
	}

	fci.param_count = nargs - 1;
	if (fci.param_count > 0) {
		fci.params = safe_emalloc(fci.param_count, sizeof(zval), 0);
	}
	/* Reverse order to pop values off ctxt stack */
	for (i = nargs - 2; i >= 0; i--) {
		obj = valuePop(ctxt);
		switch (obj->type) {
			case XPATH_STRING:
				ZVAL_STRING(&fci.params[i],  (char *)obj->stringval);
				break;
			case XPATH_BOOLEAN:
				ZVAL_BOOL(&fci.params[i],  obj->boolval);
				break;
			case XPATH_NUMBER:
				ZVAL_DOUBLE(&fci.params[i], obj->floatval);
				break;
			case XPATH_NODESET:
				if (type == 1) {
					str = (char *)xmlXPathCastToString(obj);
					ZVAL_STRING(&fci.params[i], str);
					xmlFree(str);
				} else if (type == 2) {
					int j;
					if (obj->nodesetval && obj->nodesetval->nodeNr > 0) {
						array_init(&fci.params[i]);
						for (j = 0; j < obj->nodesetval->nodeNr; j++) {
							xmlNodePtr node = obj->nodesetval->nodeTab[j];
							zval child;
							/* not sure, if we need this... it's copied from xpath.c */
							if (node->type == XML_NAMESPACE_DECL) {
								xmlNsPtr curns;
								xmlNodePtr nsparent;

								nsparent = node->_private;
								curns = xmlNewNs(NULL, node->name, NULL);
								if (node->children) {
									curns->prefix = xmlStrdup((xmlChar *) node->children);
								}
								if (node->children) {
									node = xmlNewDocNode(node->doc, NULL, (xmlChar *) node->children, node->name);
								} else {
									node = xmlNewDocNode(node->doc, NULL, (xmlChar *) "xmlns", node->name);
								}
								node->type = XML_NAMESPACE_DECL;
								node->parent = nsparent;
								node->ns = curns;
							}
							hyss_dom_create_object(node, &child, &intern->dom);
							add_next_index_zval(&fci.params[i], &child);
						}
					} else {
						ZVAL_EMPTY_ARRAY(&fci.params[i]);
					}
				}
				break;
			default:
			ZVAL_STRING(&fci.params[i], (char *)xmlXPathCastToString(obj));
		}
		xmlXPathFreeObject(obj);
	}

	fci.size = sizeof(fci);

	obj = valuePop(ctxt);
	if (obj->stringval == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Handler name must be a string");
		xmlXPathFreeObject(obj);
		if (fci.param_count > 0) {
			for (i = 0; i < nargs - 1; i++) {
				zval_ptr_dtor(&fci.params[i]);
			}
			efree(fci.params);
		}
		return;
	}
	ZVAL_STRING(&fci.function_name, (char *) obj->stringval);
	xmlXPathFreeObject(obj);

	fci.object = NULL;
	fci.retval = &retval;
	fci.no_separation = 0;

	if (!gear_make_callable(&fci.function_name, &callable)) {
		hyss_error_docref(NULL, E_WARNING, "Unable to call handler %s()", ZSTR_VAL(callable));
	} else if (intern->registerHySSFunctions == 2 && gear_hash_exists(intern->registered_hyssfunctions, callable) == 0) {
		hyss_error_docref(NULL, E_WARNING, "Not allowed to call handler '%s()'.", ZSTR_VAL(callable));
		/* Push an empty string, so that we at least have an xslt result... */
		valuePush(ctxt, xmlXPathNewString((xmlChar *)""));
	} else {
		result = gear_call_function(&fci, NULL);
		if (result == SUCCESS && Z_TYPE(retval) != IS_UNDEF) {
			if (Z_TYPE(retval) == IS_OBJECT && instanceof_function(Z_OBJCE(retval), dom_node_class_entry)) {
				xmlNode *nodep;
				dom_object *obj;
				if (intern->node_list == NULL) {
					intern->node_list = gear_new_array(0);
				}
				Z_ADDREF(retval);
				gear_hash_next_index_insert(intern->node_list, &retval);
				obj = Z_DOMOBJ_P(&retval);
				nodep = dom_object_get_node(obj);
				valuePush(ctxt, xmlXPathNewNodeSet(nodep));
			} else if (Z_TYPE(retval) == IS_FALSE || Z_TYPE(retval) == IS_TRUE) {
				valuePush(ctxt, xmlXPathNewBoolean(Z_TYPE(retval) == IS_TRUE));
			} else if (Z_TYPE(retval) == IS_OBJECT) {
				hyss_error_docref(NULL, E_WARNING, "A HYSS Object cannot be converted to a XPath-string");
				valuePush(ctxt, xmlXPathNewString((xmlChar *)""));
			} else {
				gear_string *str = zval_get_string(&retval);
				valuePush(ctxt, xmlXPathNewString((xmlChar *) ZSTR_VAL(str)));
				gear_string_release_ex(str, 0);
			}
			zval_ptr_dtor(&retval);
		}
	}
	gear_string_release_ex(callable, 0);
	zval_ptr_dtor_str(&fci.function_name);
	if (fci.param_count > 0) {
		for (i = 0; i < nargs - 1; i++) {
			zval_ptr_dtor(&fci.params[i]);
		}
		efree(fci.params);
	}
}
/* }}} */

static void dom_xpath_ext_function_string_hyss(xmlXPathParserContextPtr ctxt, int nargs) /* {{{ */
{
	dom_xpath_ext_function_hyss(ctxt, nargs, 1);
}
/* }}} */

static void dom_xpath_ext_function_object_hyss(xmlXPathParserContextPtr ctxt, int nargs) /* {{{ */
{
	dom_xpath_ext_function_hyss(ctxt, nargs, 2);
}
/* }}} */

/* {{{ proto DOMXPath::__construct(DOMDocument doc) U */
HYSS_METHOD(domxpath, __construct)
{
	zval *id = getThis(), *doc;
	xmlDocPtr docp = NULL;
	dom_object *docobj;
	dom_xpath_object *intern;
	xmlXPathContextPtr ctx, oldctx;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "O", &doc, dom_document_class_entry) == FAILURE) {
		return;
	}

	DOM_GET_OBJ(docp, doc, xmlDocPtr, docobj);

	ctx = xmlXPathNewContext(docp);
	if (ctx == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 1);
		RETURN_FALSE;
	}

	intern = Z_XPATHOBJ_P(id);
	if (intern != NULL) {
		oldctx = (xmlXPathContextPtr)intern->dom.ptr;
		if (oldctx != NULL) {
			hyss_libxml_decrement_doc_ref((hyss_libxml_node_object *) &intern->dom);
			xmlXPathFreeContext(oldctx);
		}

		xmlXPathRegisterFuncNS (ctx, (const xmlChar *) "functionString",
					   (const xmlChar *) "http://hyss.hyang.org/xpath",
					   dom_xpath_ext_function_string_hyss);
		xmlXPathRegisterFuncNS (ctx, (const xmlChar *) "function",
					   (const xmlChar *) "http://hyss.hyang.org/xpath",
					   dom_xpath_ext_function_object_hyss);

		intern->dom.ptr = ctx;
		ctx->userData = (void *)intern;
		intern->dom.document = docobj->document;
		hyss_libxml_increment_doc_ref((hyss_libxml_node_object *) &intern->dom, docp);
	}
}
/* }}} end DOMXPath::__construct */

/* {{{ document DOMDocument*/
int dom_xpath_document_read(dom_object *obj, zval *retval)
{
	xmlDoc *docp = NULL;
	xmlXPathContextPtr ctx = (xmlXPathContextPtr) obj->ptr;

	if (ctx) {
		docp = (xmlDocPtr) ctx->doc;
	}

	hyss_dom_create_object((xmlNodePtr) docp, retval, obj);
	return SUCCESS;
}
/* }}} */

/* {{{ proto bool dom_xpath_register_ns(string prefix, string uri) */
HYSS_FUNCTION(dom_xpath_register_ns)
{
	zval *id;
	xmlXPathContextPtr ctxp;
	size_t prefix_len, ns_uri_len;
	dom_xpath_object *intern;
	unsigned char *prefix, *ns_uri;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oss", &id, dom_xpath_class_entry, &prefix, &prefix_len, &ns_uri, &ns_uri_len) == FAILURE) {
		return;
	}

	intern = Z_XPATHOBJ_P(id);

	ctxp = (xmlXPathContextPtr) intern->dom.ptr;
	if (ctxp == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Invalid XPath Context");
		RETURN_FALSE;
	}

	if (xmlXPathRegisterNs(ctxp, prefix, ns_uri) != 0) {
		RETURN_FALSE
	}
	RETURN_TRUE;
}
/* }}} */

static void dom_xpath_iter(zval *baseobj, dom_object *intern) /* {{{ */
{
	dom_nnodemap_object *mapptr = (dom_nnodemap_object *) intern->ptr;

	ZVAL_COPY_VALUE(&mapptr->baseobj_zv, baseobj);
	mapptr->nodetype = DOM_NODESET;
}
/* }}} */

static void hyss_xpath_eval(INTERNAL_FUNCTION_PARAMETERS, int type) /* {{{ */
{
	zval *id, retval, *context = NULL;
	xmlXPathContextPtr ctxp;
	xmlNodePtr nodep = NULL;
	xmlXPathObjectPtr xpathobjp;
	size_t expr_len, nsnbr = 0, xpath_type;
	dom_xpath_object *intern;
	dom_object *nodeobj;
	char *expr;
	xmlDoc *docp = NULL;
	xmlNsPtr *ns = NULL;
	gear_bool register_node_ns = 1;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Os|O!b", &id, dom_xpath_class_entry, &expr, &expr_len, &context, dom_node_class_entry, &register_node_ns) == FAILURE) {
		return;
	}

	intern = Z_XPATHOBJ_P(id);

	ctxp = (xmlXPathContextPtr) intern->dom.ptr;
	if (ctxp == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Invalid XPath Context");
		RETURN_FALSE;
	}

	docp = (xmlDocPtr) ctxp->doc;
	if (docp == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Invalid XPath Document Pointer");
		RETURN_FALSE;
	}

	if (context != NULL) {
		DOM_GET_OBJ(nodep, context, xmlNodePtr, nodeobj);
	}

	if (!nodep) {
		nodep = xmlDocGetRootElement(docp);
	}

	if (nodep && docp != nodep->doc) {
		hyss_error_docref(NULL, E_WARNING, "Node From Wrong Document");
		RETURN_FALSE;
	}

	ctxp->node = nodep;

	if (register_node_ns) {
		/* Register namespaces in the node */
		ns = xmlGetNsList(docp, nodep);

		if (ns != NULL) {
			while (ns[nsnbr] != NULL)
			nsnbr++;
		}
	}


    ctxp->namespaces = ns;
    ctxp->nsNr = nsnbr;

	xpathobjp = xmlXPathEvalExpression((xmlChar *) expr, ctxp);
	ctxp->node = NULL;

	if (ns != NULL) {
		xmlFree(ns);
		ctxp->namespaces = NULL;
		ctxp->nsNr = 0;
	}

	if (! xpathobjp) {
		RETURN_FALSE;
	}

	if (type == HYSS_DOM_XPATH_QUERY) {
		xpath_type = XPATH_NODESET;
	} else {
		xpath_type = xpathobjp->type;
	}

	switch (xpath_type) {

		case  XPATH_NODESET:
		{
			int i;
			xmlNodeSetPtr nodesetp;

			if (xpathobjp->type == XPATH_NODESET && NULL != (nodesetp = xpathobjp->nodesetval) && nodesetp->nodeNr) {

				array_init(&retval);
				for (i = 0; i < nodesetp->nodeNr; i++) {
					xmlNodePtr node = nodesetp->nodeTab[i];
					zval child;

					if (node->type == XML_NAMESPACE_DECL) {
						xmlNsPtr curns;
						xmlNodePtr nsparent;

						nsparent = node->_private;
						curns = xmlNewNs(NULL, node->name, NULL);
						if (node->children) {
							curns->prefix = xmlStrdup((xmlChar *) node->children);
						}
						if (node->children) {
							node = xmlNewDocNode(docp, NULL, (xmlChar *) node->children, node->name);
						} else {
							node = xmlNewDocNode(docp, NULL, (xmlChar *) "xmlns", node->name);
						}
						node->type = XML_NAMESPACE_DECL;
						node->parent = nsparent;
						node->ns = curns;
					}
					hyss_dom_create_object(node, &child, &intern->dom);
					add_next_index_zval(&retval, &child);
				}
			} else {
				ZVAL_EMPTY_ARRAY(&retval);
			}
			hyss_dom_create_interator(return_value, DOM_NODELIST);
			nodeobj = Z_DOMOBJ_P(return_value);
			dom_xpath_iter(&retval, nodeobj);
			break;
		}

		case XPATH_BOOLEAN:
			RETVAL_BOOL(xpathobjp->boolval);
			break;

		case XPATH_NUMBER:
			RETVAL_DOUBLE(xpathobjp->floatval);
			break;

		case XPATH_STRING:
			RETVAL_STRING((char *) xpathobjp->stringval);
			break;

		default:
			RETVAL_NULL();
			break;
	}

	xmlXPathFreeObject(xpathobjp);
}
/* }}} */

/* {{{ proto DOMNodeList dom_xpath_query(string expr [,DOMNode context [, bool registerNodeNS]]) */
HYSS_FUNCTION(dom_xpath_query)
{
	hyss_xpath_eval(INTERNAL_FUNCTION_PARAM_PASSTHRU, HYSS_DOM_XPATH_QUERY);
}
/* }}} end dom_xpath_query */

/* {{{ proto mixed dom_xpath_evaluate(string expr [,DOMNode context [, bool registerNodeNS]]) */
HYSS_FUNCTION(dom_xpath_evaluate)
{
	hyss_xpath_eval(INTERNAL_FUNCTION_PARAM_PASSTHRU, HYSS_DOM_XPATH_EVALUATE);
}
/* }}} end dom_xpath_evaluate */

/* {{{ proto void dom_xpath_register_hyss_functions() */
HYSS_FUNCTION(dom_xpath_register_hyss_functions)
{
	zval *id;
	dom_xpath_object *intern;
	zval *array_value, *entry, new_string;
	gear_string *name;

	DOM_GET_THIS(id);

	if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, GEAR_NUM_ARGS(), "a",  &array_value) == SUCCESS) {
		intern = Z_XPATHOBJ_P(id);
		GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(array_value), entry) {
			gear_string *str = zval_get_string(entry);
			ZVAL_LONG(&new_string,1);
			gear_hash_update(intern->registered_hyssfunctions, str, &new_string);
			gear_string_release_ex(str, 0);
		} GEAR_HASH_FOREACH_END();
		intern->registerHySSFunctions = 2;
		RETURN_TRUE;

	} else if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, GEAR_NUM_ARGS(), "S",  &name) == SUCCESS) {
		intern = Z_XPATHOBJ_P(id);

		ZVAL_LONG(&new_string, 1);
		gear_hash_update(intern->registered_hyssfunctions, name, &new_string);
		intern->registerHySSFunctions = 2;
	} else {
		intern = Z_XPATHOBJ_P(id);
		intern->registerHySSFunctions = 1;
	}

}
/* }}} end dom_xpath_register_hyss_functions */

#endif /* LIBXML_XPATH_ENABLED */

#endif

