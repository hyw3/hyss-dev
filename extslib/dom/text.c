/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"
#include "dom_ce.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_text_split_text, 0, 0, 1)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_text_is_whitespace_in_element_content, 0, 0, 0)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_text_replace_whole_text, 0, 0, 1)
	GEAR_ARG_INFO(0, content)
GEAR_END_ARG_INFO();

GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_text_construct, 0, 0, 0)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class DOMText extends DOMCharacterData
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-1312295772
* Since:
*/

const gear_function_entry hyss_dom_text_class_functions[] = {
	HYSS_FALIAS(splitText, dom_text_split_text, arginfo_dom_text_split_text)
	HYSS_FALIAS(isWhitespaceInElementContent, dom_text_is_whitespace_in_element_content, arginfo_dom_text_is_whitespace_in_element_content)
	HYSS_FALIAS(isElementContentWhitespace, dom_text_is_whitespace_in_element_content, arginfo_dom_text_is_whitespace_in_element_content)
	HYSS_FALIAS(replaceWholeText, dom_text_replace_whole_text, arginfo_dom_text_replace_whole_text)
	HYSS_ME(domtext, __construct, arginfo_dom_text_construct, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};

/* {{{ proto DOMText::__construct([string value]); */
HYSS_METHOD(domtext, __construct)
{

	zval *id = getThis();
	xmlNodePtr nodep = NULL, oldnode = NULL;
	dom_object *intern;
	char *value = NULL;
	size_t value_len;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "|s", &value, &value_len) == FAILURE) {
		return;
	}

	nodep = xmlNewText((xmlChar *) value);

	if (!nodep) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 1);
		RETURN_FALSE;
	}

	intern = Z_DOMOBJ_P(id);
	if (intern != NULL) {
		oldnode = dom_object_get_node(intern);
		if (oldnode != NULL) {
			hyss_libxml_node_free_resource(oldnode );
		}
		hyss_libxml_increment_node_ptr((hyss_libxml_node_object *)intern, nodep, (void *)intern);
	}
}
/* }}} end DOMText::__construct */

/* {{{ wholeText	string
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-Text3-wholeText
Since: DOM Level 3
*/
int dom_text_whole_text_read(dom_object *obj, zval *retval)
{
	xmlNodePtr node;
	xmlChar *wholetext = NULL;

	node = dom_object_get_node(obj);

	if (node == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	/* Find starting text node */
	while (node->prev && ((node->prev->type == XML_TEXT_NODE) || (node->prev->type == XML_CDATA_SECTION_NODE))) {
		node = node->prev;
	}

	/* concatenate all adjacent text and cdata nodes */
	while (node && ((node->type == XML_TEXT_NODE) || (node->type == XML_CDATA_SECTION_NODE))) {
		wholetext = xmlStrcat(wholetext, node->content);
		node = node->next;
	}

	if (wholetext != NULL) {
		ZVAL_STRING(retval, (char *) wholetext);
		xmlFree(wholetext);
	} else {
		ZVAL_EMPTY_STRING(retval);
	}

	return SUCCESS;
}

/* }}} */

/* {{{ proto DOMText dom_text_split_text(int offset)
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-38853C1D
Since:
*/
HYSS_FUNCTION(dom_text_split_text)
{
	zval       *id;
	xmlChar    *cur;
	xmlChar    *first;
	xmlChar    *second;
	xmlNodePtr  node;
	xmlNodePtr  nnode;
	gear_long        offset;
	int         length;
	dom_object	*intern;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Ol", &id, dom_text_class_entry, &offset) == FAILURE) {
		return;
	}
	DOM_GET_OBJ(node, id, xmlNodePtr, intern);

	if (node->type != XML_TEXT_NODE && node->type != XML_CDATA_SECTION_NODE) {
		RETURN_FALSE;
	}

	cur = xmlNodeGetContent(node);
	if (cur == NULL) {
		RETURN_FALSE;
	}
	length = xmlUTF8Strlen(cur);

	if (GEAR_LONG_INT_OVFL(offset) || (int)offset > length || offset < 0) {
		xmlFree(cur);
		RETURN_FALSE;
	}

	first = xmlUTF8Strndup(cur, (int)offset);
	second = xmlUTF8Strsub(cur, (int)offset, (int)(length - offset));

	xmlFree(cur);

	xmlNodeSetContent(node, first);
	nnode = xmlNewDocText(node->doc, second);

	xmlFree(first);
	xmlFree(second);

	if (nnode == NULL) {
		RETURN_FALSE;
	}

	if (node->parent != NULL) {
		nnode->type = XML_ELEMENT_NODE;
		xmlAddNextSibling(node, nnode);
		nnode->type = XML_TEXT_NODE;
	}

	hyss_dom_create_object(nnode, return_value, intern);
}
/* }}} end dom_text_split_text */

/* {{{ proto bool dom_text_is_whitespace_in_element_content()
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-Text3-isWhitespaceInElementContent
Since: DOM Level 3
*/
HYSS_FUNCTION(dom_text_is_whitespace_in_element_content)
{
	zval       *id;
	xmlNodePtr  node;
	dom_object	*intern;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O", &id, dom_text_class_entry) == FAILURE) {
		return;
	}
	DOM_GET_OBJ(node, id, xmlNodePtr, intern);

	if (xmlIsBlankNode(node)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} end dom_text_is_whitespace_in_element_content */

/* {{{ proto DOMText dom_text_replace_whole_text(string content)
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-Text3-replaceWholeText
Since: DOM Level 3
*/
HYSS_FUNCTION(dom_text_replace_whole_text)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_text_replace_whole_text */

#endif

