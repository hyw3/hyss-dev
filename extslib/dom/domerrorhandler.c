/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_domerrorhandler_handle_error, 0, 0, 1)
	GEAR_ARG_OBJ_INFO(0, error, DOMDomError, 0)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class domerrorhandler
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ERROR-Interfaces-DOMErrorHandler
* Since: DOM Level 3
*/

const gear_function_entry hyss_dom_domerrorhandler_class_functions[] = {
	HYSS_FALIAS(handleError, dom_domerrorhandler_handle_error, arginfo_dom_domerrorhandler_handle_error)
	HYSS_FE_END
};

/* {{{ attribute protos, not implemented yet */

/* {{{ proto dom_boolean dom_domerrorhandler_handle_error(domerror error);
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-ERRORS-DOMErrorHandler-handleError
Since:
*/
HYSS_FUNCTION(dom_domerrorhandler_handle_error)
{
 DOM_NOT_IMPLEMENTED();
}
/* }}} end dom_domerrorhandler_handle_error */

/* }}} */

#endif

