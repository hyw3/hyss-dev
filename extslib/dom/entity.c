/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"


/*
* class DOMEntity extends DOMNode
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-527DCFF2
* Since:
*/

const gear_function_entry hyss_dom_entity_class_functions[] = {
	HYSS_FE_END
};

/* {{{ publicId	string
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-D7303025
Since:
*/
int dom_entity_public_id_read(dom_object *obj, zval *retval)
{
	xmlEntity *nodep = (xmlEntity *) dom_object_get_node(obj);

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	if (nodep->etype != XML_EXTERNAL_GENERAL_UNPARSED_ENTITY) {
		ZVAL_NULL(retval);
	} else {
		ZVAL_STRING(retval, (char *) (nodep->ExternalID));
	}

	return SUCCESS;
}

/* }}} */

/* {{{ systemId	string
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-D7C29F3E
Since:
*/
int dom_entity_system_id_read(dom_object *obj, zval *retval)
{
	xmlEntity *nodep = (xmlEntity *) dom_object_get_node(obj);

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	if (nodep->etype != XML_EXTERNAL_GENERAL_UNPARSED_ENTITY) {
		ZVAL_NULL(retval);
	} else {
		ZVAL_STRING(retval, (char *) (nodep->SystemID));
	}

	return SUCCESS;
}

/* }}} */

/* {{{ notationName	string
readonly=yes
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-6ABAEB38
Since:
*/
int dom_entity_notation_name_read(dom_object *obj, zval *retval)
{
	xmlEntity *nodep = (xmlEntity *) dom_object_get_node(obj);
	char *content;

	if (nodep == NULL) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 0);
		return FAILURE;
	}

	if (nodep->etype != XML_EXTERNAL_GENERAL_UNPARSED_ENTITY) {
		ZVAL_NULL(retval);
	} else {
		content = (char *) xmlNodeGetContent((xmlNodePtr) nodep);
		ZVAL_STRING(retval, content);
		xmlFree(content);
	}

	return SUCCESS;
}

/* }}} */

/* {{{ actualEncoding	string
readonly=no
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Entity3-actualEncoding
Since: DOM Level 3
*/
int dom_entity_actual_encoding_read(dom_object *obj, zval *retval)
{
	ZVAL_NULL(retval);
	return SUCCESS;
}

int dom_entity_actual_encoding_write(dom_object *obj, zval *newval)
{
	return SUCCESS;
}

/* }}} */

/* {{{ encoding	string
readonly=no
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Entity3-encoding
Since: DOM Level 3
*/
int dom_entity_encoding_read(dom_object *obj, zval *retval)
{
	ZVAL_NULL(retval);
	return SUCCESS;
}

int dom_entity_encoding_write(dom_object *obj, zval *newval)
{
	return SUCCESS;
}

/* }}} */

/* {{{ version	string
readonly=no
URL: http://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#Entity3-version
Since: DOM Level 3
*/
int dom_entity_version_read(dom_object *obj, zval *retval)
{
	ZVAL_NULL(retval);
	return SUCCESS;
}

int dom_entity_version_write(dom_object *obj, zval *newval)
{
	return SUCCESS;
}

/* }}} */

#endif

