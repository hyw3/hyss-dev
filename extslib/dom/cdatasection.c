/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"


/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dom_cdatasection_construct, 0, 0, 1)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO();
/* }}} */

/*
* class DOMCdataSection extends DOMText
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#ID-667469212
* Since:
*/

const gear_function_entry hyss_dom_cdatasection_class_functions[] = {
	HYSS_ME(domcdatasection, __construct, arginfo_dom_cdatasection_construct, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};

/* {{{ proto DOMCdataSection::__construct(string value); */
HYSS_METHOD(domcdatasection, __construct)
{

	zval *id = getThis();
	xmlNodePtr nodep = NULL, oldnode = NULL;
	dom_object *intern;
	char *value = NULL;
	size_t value_len;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "s", &value, &value_len) == FAILURE) {
		return;
	}

	nodep = xmlNewCDataBlock(NULL, (xmlChar *) value, value_len);

	if (!nodep) {
		hyss_dom_throw_error(INVALID_STATE_ERR, 1);
		RETURN_FALSE;
	}

	intern = Z_DOMOBJ_P(id);
	oldnode = dom_object_get_node(intern);
	if (oldnode != NULL) {
		hyss_libxml_node_free_resource(oldnode );
	}
	hyss_libxml_increment_node_ptr((hyss_libxml_node_object *)intern, nodep, (void *)intern);
}
/* }}} end DOMCdataSection::__construct */

#endif

