/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#if HAVE_LIBXML && HAVE_DOM
#include "hyss_dom.h"

/*
* class DOMException
*
* URL: https://www.w3.org/TR/2003/WD-DOM-Level-3-Core-20030226/DOM3-Core.html#core-ID-17189187
* Since:
*/

extern gear_class_entry *dom_domexception_class_entry;

const gear_function_entry hyss_dom_domexception_class_functions[] = {
	HYSS_FE_END
};

void hyss_dom_throw_error_with_message(int error_code, char *error_message, int strict_error) /* {{{ */
{
	if (strict_error == 1) {
		gear_throw_exception(dom_domexception_class_entry, error_message, error_code);
	} else {
		hyss_libxml_issue_error(E_WARNING, error_message);
	}
}
/* }}} */

/* {{{ hyss_dom_throw_error */
void hyss_dom_throw_error(int error_code, int strict_error)
{
	char *error_message;

	switch (error_code)
	{
		case INDEX_SIZE_ERR:
			error_message = "Index Size Error";
			break;
		case DOMSTRING_SIZE_ERR:
			error_message = "DOM String Size Error";
			break;
		case HIERARCHY_REQUEST_ERR:
			error_message = "Hierarchy Request Error";
			break;
		case WRONG_DOCUMENT_ERR:
			error_message = "Wrong Document Error";
			break;
		case INVALID_CHARACTER_ERR:
			error_message = "Invalid Character Error";
			break;
		case NO_DATA_ALLOWED_ERR:
			error_message = "No Data Allowed Error";
			break;
		case NO_MODIFICATION_ALLOWED_ERR:
			error_message = "No Modification Allowed Error";
			break;
		case NOT_FOUND_ERR:
			error_message = "Not Found Error";
			break;
		case NOT_SUPPORTED_ERR:
			error_message = "Not Supported Error";
			break;
		case INUSE_ATTRIBUTE_ERR:
			error_message = "Inuse Attribute Error";
			break;
		case INVALID_STATE_ERR:
			error_message = "Invalid State Error";
			break;
		case SYNTAX_ERR:
			error_message = "Syntax Error";
			break;
		case INVALID_MODIFICATION_ERR:
			error_message = "Invalid Modification Error";
			break;
		case NAMESPACE_ERR:
			error_message = "Namespace Error";
			break;
		case INVALID_ACCESS_ERR:
			error_message = "Invalid Access Error";
			break;
		case VALIDATION_ERR:
			error_message = "Validation Error";
			break;
		default:
			error_message = "Unhandled Error";
	}

	hyss_dom_throw_error_with_message(error_code, error_message, strict_error);
}
/* }}} end hyss_dom_throw_error */

#endif /* HAVE_LIBXML && HAVE_DOM */

