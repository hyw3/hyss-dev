/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DOM_CE_H
#define DOM_CE_H

extern HYSS_DOM_EXPORT gear_class_entry *dom_node_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domexception_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domstringlist_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_namelist_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domimplementationlist_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domimplementationsource_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domimplementation_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_documentfragment_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_document_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_nodelist_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_namednodemap_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_characterdata_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_attr_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_element_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_text_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_comment_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_typeinfo_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_userdatahandler_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domerror_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domerrorhandler_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domlocator_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_domconfiguration_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_cdatasection_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_documenttype_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_notation_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_entity_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_entityreference_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_processinginstruction_class_entry;
extern HYSS_DOM_EXPORT gear_class_entry *dom_string_extend_class_entry;
#if defined(LIBXML_XPATH_ENABLED)
extern HYSS_DOM_EXPORT gear_class_entry *dom_xpath_class_entry;
#endif
extern HYSS_DOM_EXPORT gear_class_entry *dom_namespace_node_class_entry;

#endif /* DOM_CE_H */

