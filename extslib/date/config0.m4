dnl config.m4 for date extension

dnl Check for headers needed by timelib
AC_CHECK_HEADERS([ \
strings.h \
io.h
])

dnl Check for strtoll, atoll
AC_CHECK_FUNCS(strtoll atoll)

HYSS_DATE_CFLAGS="-I@ext_builddir@/lib -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1 -DHAVE_TIMELIB_CONFIG_H=1"
timelib_sources="lib/astro.c lib/dow.c lib/parse_date.c lib/parse_tz.c
                 lib/timelib.c lib/tm2unixtime.c lib/unixtime2tm.c lib/parse_iso_intervals.c lib/interval.c"

HYSS_NEW_EXTENSION(date, hyss_date.c $timelib_sources, no,, $HYSS_DATE_CFLAGS)

HYSS_ADD_BUILD_DIR([$ext_builddir/lib], 1)
HYSS_ADD_INCLUDE([$ext_builddir/lib])
HYSS_ADD_INCLUDE([$ext_srcdir/lib])

HYSS_INSTALL_HEADERS([extslib/date], [hyss_date.h lib/timelib.h lib/timelib_config.h])
AC_DEFINE([HAVE_TIMELIB_CONFIG_H], [1], [Have timelib_config.h])

cat > $ext_builddir/lib/timelib_config.h <<EOF
#ifdef HYSS_WIN32
# include "config.w32.h"
#else
# include <hyss_config.h>
#endif
#include <hyss_stdint.h>
#define TIMELIB_OMIT_STDINT 1

#include "gear.h"

#define timelib_malloc  emalloc
#define timelib_realloc erealloc
#define timelib_calloc  ecalloc
#define timelib_strdup  estrdup
#define timelib_free    efree
EOF
