/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <signal.h>

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_mysqli_structs.h"
#include "gear_exceptions.h"
#include "mysqli_fe.h"

#define MAP_PROPERTY_MYG_BOOL_READ(name, value) \
static zval *name(mysqli_object *obj, zval *retval) \
{ \
	ZVAL_BOOL(retval, MyG(value)); \
	return retval; \
} \

#define MAP_PROPERTY_MYG_BOOL_WRITE(name, value) \
static int name(mysqli_object *obj, zval *value) \
{ \
	MyG(value) = Z_LVAL_P(value) > 0; \
	return SUCCESS; \
} \

#define MAP_PROPERTY_MYG_LONG_READ(name, value) \
static zval *name(mysqli_object *obj, zval *retval) \
{ \
	ZVAL_LONG(retval, MyG(value)); \
	return retval; \
} \

#define MAP_PROPERTY_MYG_LONG_WRITE(name, value) \
static int name(mysqli_object *obj, zval *value) \
{ \
	MyG(value) = Z_LVAL_P(value); \
	return SUCCESS; \
} \

#define MAP_PROPERTY_MYG_STRING_READ(name, value) \
static zval *name(mysqli_object *obj, zval *retval) \
{ \
	ZVAL_STRING(retval, MyG(value)); \
	return retval; \
} \

#define MAP_PROPERTY_MYG_STRING_WRITE(name, value) \
static int name(mysqli_object *obj, zval *value) \
{ \
	MyG(value) = Z_STRVAL_P(value); \
	return SUCCESS; \
} \

/* {{{ property driver_report_write */
static int driver_report_write(mysqli_object *obj, zval *value)
{
	MyG(report_mode) = Z_LVAL_P(value);
	/*FIXME*/
	/* gear_replace_error_handling(MyG(report_mode) & MYSQLI_REPORT_STRICT ? EH_THROW : EH_NORMAL, NULL, NULL); */
	return SUCCESS;
}
/* }}} */

/* {{{ property driver_embedded_read */
static zval *driver_embedded_read(mysqli_object *obj, zval *retval)
{
#ifdef HAVE_EMBEDDED_MYSQLI
	ZVAL_TRUE(retval);
#else
	ZVAL_FALSE(retval);
#endif
	return retval;
}
/* }}} */

/* {{{ property driver_client_version_read */
static zval *driver_client_version_read(mysqli_object *obj, zval *retval)
{
	ZVAL_LONG(retval, MYSQL_VERSION_ID);
	return retval;
}
/* }}} */

/* {{{ property driver_client_info_read */
static zval *driver_client_info_read(mysqli_object *obj, zval *retval)
{
	ZVAL_STRING(retval, (char *)mysql_get_client_info());
	return retval;
}
/* }}} */

/* {{{ property driver_driver_version_read */
static zval *driver_driver_version_read(mysqli_object *obj, zval *retval)
{
	ZVAL_LONG(retval, MYSQLI_VERSION_ID);
	return retval;
}
/* }}} */

MAP_PROPERTY_MYG_BOOL_READ(driver_reconnect_read, reconnect)
MAP_PROPERTY_MYG_BOOL_WRITE(driver_reconnect_write, reconnect)
MAP_PROPERTY_MYG_LONG_READ(driver_report_read, report_mode)

GEAR_FUNCTION(mysqli_driver_construct)
{
#if G0
	MYSQLI_RESOURCE 	*mysqli_resource;

	mysqli_resource = (MYSQLI_RESOURCE *)ecalloc (1, sizeof(MYSQLI_RESOURCE));
	mysqli_resource->ptr = 1;
	mysqli_resource->status = (GEAR_NUM_ARGS() == 1) ? MYSQLI_STATUS_INITIALIZED : MYSQLI_STATUS_VALID;
	(Z_MYSQLI_P(getThis()))->ptr = mysqli_resource;
#endif
}

const mysqli_property_entry mysqli_driver_property_entries[] = {
	{"client_info", sizeof("client_info") - 1, driver_client_info_read, NULL},
	{"client_version", sizeof("client_version") - 1, driver_client_version_read, NULL},
	{"driver_version", sizeof("driver_version") - 1, driver_driver_version_read, NULL},
	{"embedded", sizeof("embedded") - 1, driver_embedded_read, NULL},
	{"reconnect", sizeof("reconnect") - 1, driver_reconnect_read, driver_reconnect_write},
	{"report_mode", sizeof("report_mode") - 1, driver_report_read, driver_report_write},
	{NULL, 0, NULL, NULL}
};

/* {{{ mysqli_driver_methods[]
 */
const gear_function_entry mysqli_driver_methods[] = {
#if defined(HAVE_EMBEDDED_MYSQLI)
	HYSS_FALIAS(embedded_server_start, mysqli_embedded_server_start, NULL)
	HYSS_FALIAS(embedded_server_end, mysqli_embedded_server_end, NULL)
#endif
	HYSS_FE_END
};
/* }}} */

