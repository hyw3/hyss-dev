/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MYSQLI_FE_H
#define MYSQLI_FE_H

HYSS_FUNCTION(mysqli);
HYSS_FUNCTION(mysqli_affected_rows);
HYSS_FUNCTION(mysqli_autocommit);
HYSS_FUNCTION(mysqli_begin_transaction);
HYSS_FUNCTION(mysqli_change_user);
HYSS_FUNCTION(mysqli_character_set_name);
HYSS_FUNCTION(mysqli_set_charset);
HYSS_FUNCTION(mysqli_close);
HYSS_FUNCTION(mysqli_commit);
HYSS_FUNCTION(mysqli_connect);
HYSS_FUNCTION(mysqli_connect_errno);
HYSS_FUNCTION(mysqli_connect_error);
HYSS_FUNCTION(mysqli_data_seek);
HYSS_FUNCTION(mysqli_debug);
HYSS_FUNCTION(mysqli_dump_debug_info);
HYSS_FUNCTION(mysqli_errno);
HYSS_FUNCTION(mysqli_error);
HYSS_FUNCTION(mysqli_error_list);
HYSS_FUNCTION(mysqli_fetch_all);
HYSS_FUNCTION(mysqli_fetch_array);
HYSS_FUNCTION(mysqli_fetch_assoc);
HYSS_FUNCTION(mysqli_fetch_object);
HYSS_FUNCTION(mysqli_fetch_field);
HYSS_FUNCTION(mysqli_fetch_fields);
HYSS_FUNCTION(mysqli_fetch_field_direct);
HYSS_FUNCTION(mysqli_fetch_lengths);
HYSS_FUNCTION(mysqli_fetch_row);
HYSS_FUNCTION(mysqli_field_count);
HYSS_FUNCTION(mysqli_field_seek);
HYSS_FUNCTION(mysqli_field_tell);
HYSS_FUNCTION(mysqli_free_result);
HYSS_FUNCTION(mysqli_get_client_stats);
HYSS_FUNCTION(mysqli_get_connection_stats);
HYSS_FUNCTION(mysqli_get_charset);
HYSS_FUNCTION(mysqli_get_client_info);
HYSS_FUNCTION(mysqli_get_client_version);
HYSS_FUNCTION(mysqli_get_host_info);
HYSS_FUNCTION(mysqli_get_links_stats);
HYSS_FUNCTION(mysqli_get_proto_info);
HYSS_FUNCTION(mysqli_get_server_info);
HYSS_FUNCTION(mysqli_get_server_version);
HYSS_FUNCTION(mysqli_get_warnings);
HYSS_FUNCTION(mysqli_info);
HYSS_FUNCTION(mysqli_insert_id);
HYSS_FUNCTION(mysqli_init);
HYSS_FUNCTION(mysqli_init_method);
HYSS_FUNCTION(mysqli_kill);
HYSS_FUNCTION(mysqli_link_construct);
HYSS_FUNCTION(mysqli_set_local_infile_default);
HYSS_FUNCTION(mysqli_set_local_infile_handler);
HYSS_FUNCTION(mysqli_more_results);
HYSS_FUNCTION(mysqli_multi_query);
HYSS_FUNCTION(mysqli_next_result);
HYSS_FUNCTION(mysqli_num_fields);
HYSS_FUNCTION(mysqli_num_rows);
HYSS_FUNCTION(mysqli_options);
HYSS_FUNCTION(mysqli_ping);
HYSS_FUNCTION(mysqli_poll);
HYSS_FUNCTION(mysqli_prepare);
HYSS_FUNCTION(mysqli_query);
HYSS_FUNCTION(mysqli_stmt_result_metadata);
HYSS_FUNCTION(mysqli_report);
HYSS_FUNCTION(mysqli_read_query_result);
HYSS_FUNCTION(mysqli_real_connect);
HYSS_FUNCTION(mysqli_real_query);
HYSS_FUNCTION(mysqli_real_escape_string);
HYSS_FUNCTION(mysqli_reap_async_query);
HYSS_FUNCTION(mysqli_rollback);
HYSS_FUNCTION(mysqli_row_seek);
HYSS_FUNCTION(mysqli_select_db);
HYSS_FUNCTION(mysqli_stmt_attr_get);
HYSS_FUNCTION(mysqli_stmt_attr_set);
HYSS_FUNCTION(mysqli_stmt_bind_param);
HYSS_FUNCTION(mysqli_stmt_bind_result);
HYSS_FUNCTION(mysqli_stmt_execute);
HYSS_FUNCTION(mysqli_stmt_field_count);
HYSS_FUNCTION(mysqli_stmt_init);
HYSS_FUNCTION(mysqli_stmt_prepare);
HYSS_FUNCTION(mysqli_stmt_fetch);
HYSS_FUNCTION(mysqli_stmt_param_count);
HYSS_FUNCTION(mysqli_stmt_send_long_data);
HYSS_FUNCTION(mysqli_embedded_server_end);
HYSS_FUNCTION(mysqli_embedded_server_start);
HYSS_FUNCTION(mysqli_sqlstate);
HYSS_FUNCTION(mysqli_ssl_set);
HYSS_FUNCTION(mysqli_stat);
HYSS_FUNCTION(mysqli_refresh);
HYSS_FUNCTION(mysqli_savepoint);
HYSS_FUNCTION(mysqli_release_savepoint);
HYSS_FUNCTION(mysqli_stmt_affected_rows);
HYSS_FUNCTION(mysqli_stmt_close);
HYSS_FUNCTION(mysqli_stmt_data_seek);
HYSS_FUNCTION(mysqli_stmt_errno);
HYSS_FUNCTION(mysqli_stmt_error);
HYSS_FUNCTION(mysqli_stmt_error_list);
HYSS_FUNCTION(mysqli_stmt_free_result);
HYSS_FUNCTION(mysqli_stmt_get_result);
HYSS_FUNCTION(mysqli_stmt_get_warnings);
HYSS_FUNCTION(mysqli_stmt_reset);
HYSS_FUNCTION(mysqli_stmt_insert_id);
HYSS_FUNCTION(mysqli_stmt_more_results);
HYSS_FUNCTION(mysqli_stmt_next_result);
HYSS_FUNCTION(mysqli_stmt_num_rows);
HYSS_FUNCTION(mysqli_stmt_sqlstate);
HYSS_FUNCTION(mysqli_stmt_store_result);
HYSS_FUNCTION(mysqli_store_result);
HYSS_FUNCTION(mysqli_thread_id);
HYSS_FUNCTION(mysqli_thread_safe);
HYSS_FUNCTION(mysqli_use_result);
HYSS_FUNCTION(mysqli_warning_count);

HYSS_FUNCTION(mysqli_stmt_construct);
HYSS_FUNCTION(mysqli_result_construct);
HYSS_FUNCTION(mysqli_driver_construct);
HYSS_METHOD(mysqli_warning,__construct);

#endif /* MYSQLI_FE_H */
