/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <signal.h>

#include "hyss.h"
#include "hyss_ics.h"
#include "hyss_mysqli_structs.h"
#include "mysqli_priv.h"
#include "gear_interfaces.h"


extern const gear_object_iterator_funcs hyss_mysqli_result_iterator_funcs;

typedef struct {
	gear_object_iterator  intern;
	mysqli_object *result;
	zval current_row;
	my_longlong row_num;
} hyss_mysqli_result_iterator;


/* {{{ */
gear_object_iterator *hyss_mysqli_result_get_iterator(gear_class_entry *ce, zval *object, int by_ref)
{
	hyss_mysqli_result_iterator *iterator;

	if (by_ref) {
		gear_error(E_ERROR, "An iterator cannot be used with foreach by reference");
	}
	iterator = ecalloc(1, sizeof(hyss_mysqli_result_iterator));
	gear_iterator_init(&iterator->intern);

	ZVAL_COPY(&iterator->intern.data, object);
	iterator->intern.funcs = &hyss_mysqli_result_iterator_funcs;
	iterator->result = Z_MYSQLI_P(object);
	iterator->row_num = -1;

	return &iterator->intern;
}
/* }}} */

/* {{{ */
static void hyss_mysqli_result_iterator_dtor(gear_object_iterator *iter)
{
	hyss_mysqli_result_iterator *iterator = (hyss_mysqli_result_iterator*)iter;

	/* cleanup handled in sxe_object_dtor as we don't always have an iterator wrapper */
	zval_ptr_dtor(&iterator->intern.data);
	zval_ptr_dtor(&iterator->current_row);
}
/* }}} */

/* {{{ */
static int hyss_mysqli_result_iterator_valid(gear_object_iterator *iter)
{
	hyss_mysqli_result_iterator *iterator = (hyss_mysqli_result_iterator*) iter;

	return Z_TYPE(iterator->current_row) == IS_ARRAY ? SUCCESS : FAILURE;
}
/* }}} */

/* {{{ */
static zval *hyss_mysqli_result_iterator_current_data(gear_object_iterator *iter)
{
	hyss_mysqli_result_iterator *iterator = (hyss_mysqli_result_iterator*) iter;

	return &iterator->current_row;
}
/* }}} */

/* {{{ */
static void hyss_mysqli_result_iterator_move_forward(gear_object_iterator *iter)
{

	hyss_mysqli_result_iterator *iterator = (hyss_mysqli_result_iterator*) iter;
	mysqli_object *intern = iterator->result;
	MYSQL_RES	*result;

	MYSQLI_FETCH_RESOURCE_BY_OBJ(result, MYSQL_RES *, intern, "mysqli_result", MYSQLI_STATUS_VALID);

	zval_ptr_dtor(&iterator->current_row);
	hyss_mysqli_fetch_into_hash_aux(&iterator->current_row, result, MYSQLI_ASSOC);
	if (Z_TYPE(iterator->current_row) == IS_ARRAY) {
		iterator->row_num++;
	}
}
/* }}} */

/* {{{ */
static void hyss_mysqli_result_iterator_rewind(gear_object_iterator *iter)
{
	hyss_mysqli_result_iterator *iterator = (hyss_mysqli_result_iterator*) iter;
	mysqli_object *intern = iterator->result;
	MYSQL_RES	*result;

	MYSQLI_FETCH_RESOURCE_BY_OBJ(result, MYSQL_RES *, intern, "mysqli_result", MYSQLI_STATUS_VALID);

	if (mysqli_result_is_unbuffered(result)) {
#if MYSQLI_USE_MYSQLND
		if (result->unbuf->eof_reached) {
#else
		if (result->eof) {
#endif
			hyss_error_docref(NULL, E_WARNING, "Data fetched with MYSQLI_USE_RESULT can be iterated only once");
			return;
		}
	} else {
		mysql_data_seek(result, 0);
	}
	iterator->row_num = -1;
	hyss_mysqli_result_iterator_move_forward(iter);
}
/* }}} */

/* {{{ hyss_mysqli_result_iterator_current_key */
static void hyss_mysqli_result_iterator_current_key(gear_object_iterator *iter, zval *key)
{
	hyss_mysqli_result_iterator *iterator = (hyss_mysqli_result_iterator*) iter;

	ZVAL_LONG(key, iterator->row_num);
}
/* }}} */

/* {{{ hyss_mysqli_result_iterator_funcs */
const gear_object_iterator_funcs hyss_mysqli_result_iterator_funcs = {
	hyss_mysqli_result_iterator_dtor,
	hyss_mysqli_result_iterator_valid,
	hyss_mysqli_result_iterator_current_data,
	hyss_mysqli_result_iterator_current_key,
	hyss_mysqli_result_iterator_move_forward,
	hyss_mysqli_result_iterator_rewind,
	NULL
};
/* }}} */

