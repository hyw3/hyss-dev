/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <signal.h>

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_mysqli_structs.h"

/* {{{ proto bool mysqli_embedded_server_start(bool start, array arguments, array groups)
   initialize and start embedded server */
HYSS_FUNCTION(mysqli_embedded_server_start)
{
#ifdef HAVE_EMBEDDED_MYSQLI
	gear_long start;
	zval *args;
	zval *grps;

	int	argc = 0;
	char **arguments;
	char **groups;
	HashPosition pos;
	int index, rc;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "laa", &start, &args, &grps) == FAILURE) {
		return;
	}

	if (!start) {
		mysql_server_init(-1,NULL, NULL);
		RETURN_TRUE;
	}
	/* get arguments */
	if ((argc = gear_hash_num_elements(Z_ARRVAL_P(args)))) {
		arguments = safe_emalloc(sizeof(char *), argc + 1, 0);
		arguments[0] = NULL;

		gear_hash_internal_pointer_reset_ex(Z_ARRVAL_P(args), &pos);

		for (index = 0;; gear_hash_move_forward_ex(Z_ARRVAL_P(args), &pos))	{
			zval **item;

			if (gear_hash_get_current_data_ex(Z_ARRVAL_P(args), (void **) &item, &pos) == FAILURE) {
				break;
			}

			convert_to_string_ex(item);

			arguments[++index] = Z_STRVAL_PP(item);
		}
		argc++;
	}

	/* get groups */
	if ((gear_hash_num_elements(Z_ARRVAL_P(grps)))) {
		groups = safe_emalloc(sizeof(char *), gear_hash_num_elements(Z_ARRVAL_P(grps)) + 1, 0);
		groups[0] = NULL;

		gear_hash_internal_pointer_reset_ex(Z_ARRVAL_P(grps), &pos);

		for (index = 0;; gear_hash_move_forward_ex(Z_ARRVAL_P(grps), &pos))	{
			zval ** item;

			if (gear_hash_get_current_data_ex(Z_ARRVAL_P(grps), (void **) &item, &pos) == FAILURE) {
				break;
			}

			convert_to_string_ex(item);

			groups[++index] = Z_STRVAL_PP(item);
		}
		groups[index] = NULL;
	} else {
		groups = safe_emalloc(sizeof(char *), 1, 0);
		groups[0] = NULL;
	}

	rc = mysql_server_init(argc, arguments, groups);

	if (argc) {
		efree(arguments);
	}
	efree(groups);

	if (rc) {
		RETURN_FALSE;
	}
	RETURN_TRUE;
#endif
}
/* }}} */

/* {{{ proto void mysqli_embedded_server_end(void)
*/
HYSS_FUNCTION(mysqli_embedded_server_end)
{
#ifdef HAVE_MYSQLI_EMBEDDED
	mysql_server_end();
#endif
}
/* }}} */

