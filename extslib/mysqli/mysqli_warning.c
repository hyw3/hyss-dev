/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <signal.h>

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_mysqli_structs.h"
#include "mysqli_priv.h"

/* Define these in the HYSS tree to make merging easy process */
#define ZSTR_DUPLICATE (1<<0)
#define ZSTR_AUTOFREE  (1<<1)

#define ZVAL_UTF8_STRING(z, s, flags)          ZVAL_STRING((z), (char*)(s))
#define ZVAL_UTF8_STRINGL(z, s, l, flags)      ZVAL_STRINGL((z), (char*)(s), (l))

/* {{{ void hyss_clear_warnings() */
void hyss_clear_warnings(MYSQLI_WARNING *w)
{
	MYSQLI_WARNING *n;

	while (w) {
		n = w;
		zval_ptr_dtor_str(&(w->reason));
		zval_ptr_dtor_str(&(w->sqlstate));
		w = w->next;
		efree(n);
	}
}
/* }}} */

#ifndef MYSQLI_USE_MYSQLND
/* {{{ MYSQLI_WARNING *hyss_new_warning */
static
MYSQLI_WARNING *hyss_new_warning(const char *reason, int errorno)
{
	MYSQLI_WARNING *w;

	w = (MYSQLI_WARNING *)ecalloc(1, sizeof(MYSQLI_WARNING));

	ZVAL_UTF8_STRING(&(w->reason), reason, ZSTR_DUPLICATE);

	ZVAL_UTF8_STRINGL(&(w->sqlstate), "HY000", sizeof("HY000") - 1,  ZSTR_DUPLICATE);

	w->errorno = errorno;

	return w;
}
/* }}} */

/* {{{ MYSQLI_WARNING *hyss_get_warnings(MYSQL *mysql) */
MYSQLI_WARNING *hyss_get_warnings(MYSQL *mysql)
{
	MYSQLI_WARNING *w, *first = NULL, *prev = NULL;
	MYSQL_RES		*result;
	MYSQL_ROW 		row;

	if (mysql_real_query(mysql, "SHOW WARNINGS", 13)) {
		return NULL;
	}

	result = mysql_store_result(mysql);

	while ((row = mysql_fetch_row(result))) {
		w = hyss_new_warning(row[2], atoi(row[1]));
		if (!first) {
			first = w;
		}
		if (prev) {
			prev->next = w;
		}
		prev = w;
	}
	mysql_free_result(result);
	return first;
}
/* }}} */
#else
/* {{{ MYSQLI_WARNING *hyss_new_warning */
static
MYSQLI_WARNING *hyss_new_warning(zval * reason, int errorno)
{
	MYSQLI_WARNING *w;

	w = (MYSQLI_WARNING *)ecalloc(1, sizeof(MYSQLI_WARNING));

	ZVAL_COPY(&w->reason, reason);
	convert_to_string(&w->reason);

	ZVAL_UTF8_STRINGL(&(w->sqlstate), "HY000", sizeof("HY000") - 1,  ZSTR_DUPLICATE);

	w->errorno = errorno;

	return w;
}
/* }}} */

/* {{{ MYSQLI_WARNING *hyss_get_warnings(MYSQL *mysql) */
MYSQLI_WARNING * hyss_get_warnings(MYSQLND_CONN_DATA * mysql)
{
	MYSQLI_WARNING	*w, *first = NULL, *prev = NULL;
	MYSQL_RES		*result;
	zval			row;

	if (mysql->m->query(mysql, "SHOW WARNINGS", 13)) {
		return NULL;
	}

	result = mysql->m->use_result(mysql, 0);

	for (;;) {
		zval *entry;
		int errno;

		mysqlnd_fetch_into(result, MYSQLND_FETCH_NUM, &row, MYSQLND_MYSQLI);
		if (Z_TYPE(row) != IS_ARRAY) {
			zval_ptr_dtor(&row);
			break;
		}
		gear_hash_internal_pointer_reset(Z_ARRVAL(row));
		/* 0. we don't care about the first */
		gear_hash_move_forward(Z_ARRVAL(row));

		/* 1. Here comes the error no */
		entry = gear_hash_get_current_data(Z_ARRVAL(row));
		errno = zval_get_long(entry);
		gear_hash_move_forward(Z_ARRVAL(row));

		/* 2. Here comes the reason */
		entry = gear_hash_get_current_data(Z_ARRVAL(row));

		w = hyss_new_warning(entry, errno);
		/*
		  Don't destroy entry, because the row destroy will decrease
		  the refcounter. Decreased twice then mysqlnd_free_result()
		  will crash, because it will try to access already freed memory.
		*/
		if (!first) {
			first = w;
		}
		if (prev) {
			prev->next = (void *)w;
		}
		prev = w;

		zval_ptr_dtor(&row);
	}

	mysql_free_result(result);
	return first;
}
/* }}} */
#endif

/* {{{ bool mysqli_warning::next() */
HYSS_METHOD(mysqli_warning, next)
{
	MYSQLI_WARNING 	*w;
	zval  			*mysqli_warning;
	mysqli_object *obj = Z_MYSQLI_P(getThis());

	if (obj->ptr) {
		if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O",
										 &mysqli_warning, mysqli_warning_class_entry) == FAILURE) {
			return;
		}

		MYSQLI_FETCH_RESOURCE(w, MYSQLI_WARNING *, mysqli_warning, "mysqli_warning", MYSQLI_STATUS_VALID);

		if (w && w->next) {
			w = w->next;
	        ((MYSQLI_RESOURCE *)(obj->ptr))->ptr = w;
			RETURN_TRUE;
		}
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ property mysqli_warning_message */
static
zval *mysqli_warning_message(mysqli_object *obj, zval *retval)
{
	MYSQLI_WARNING *w;

	if (!obj->ptr || !((MYSQLI_RESOURCE *)(obj->ptr))->ptr) {
		return NULL;
	}

	w = (MYSQLI_WARNING *)((MYSQLI_RESOURCE *)(obj->ptr))->ptr;
	ZVAL_COPY(retval, &w->reason);
	return retval;
}
/* }}} */

/* {{{ property mysqli_warning_sqlstate */
static
zval *mysqli_warning_sqlstate(mysqli_object *obj, zval *retval)
{
	MYSQLI_WARNING *w;

	if (!obj->ptr || !((MYSQLI_RESOURCE *)(obj->ptr))->ptr) {
		return NULL;
	}

	w = (MYSQLI_WARNING *)((MYSQLI_RESOURCE *)(obj->ptr))->ptr;
	ZVAL_COPY(retval, &w->sqlstate);
	return retval;
}
/* }}} */

/* {{{ property mysqli_warning_error */
static
zval *mysqli_warning_errno(mysqli_object *obj, zval *retval)
{
	MYSQLI_WARNING *w;

	if (!obj->ptr || !((MYSQLI_RESOURCE *)(obj->ptr))->ptr) {
		return NULL;
	}
	w = (MYSQLI_WARNING *)((MYSQLI_RESOURCE *)(obj->ptr))->ptr;
	ZVAL_LONG(retval, w->errorno);
	return retval;
}
/* }}} */

/* {{{ mysqli_warning_construct(object obj) */
HYSS_METHOD(mysqli_warning, __construct)
{
	zval			*z;
	mysqli_object	*obj;
#ifndef MYSQLI_USE_MYSQLND
	MYSQL			*hdl;
#endif
	MYSQLI_WARNING  *w;
	MYSQLI_RESOURCE *mysqli_resource;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "o", &z) == FAILURE) {
		return;
	}
	obj = Z_MYSQLI_P(z);

	if (obj->zo.ce == mysqli_link_class_entry) {
		MY_MYSQL *mysql;
		MYSQLI_FETCH_RESOURCE_CONN(mysql, z, MYSQLI_STATUS_VALID);
		if (mysql_warning_count(mysql->mysql)) {
#ifndef MYSQLI_USE_MYSQLND
			w = hyss_get_warnings(mysql->mysql);
#else
			w = hyss_get_warnings(mysql->mysql->data);
#endif
		} else {
			hyss_error_docref(NULL, E_WARNING, "No warnings found");
			RETURN_FALSE;
		}
	} else if (obj->zo.ce == mysqli_stmt_class_entry) {
		MY_STMT *stmt;
		MYSQLI_FETCH_RESOURCE_STMT(stmt, z, MYSQLI_STATUS_VALID);
#ifndef MYSQLI_USE_MYSQLND
		hdl = mysqli_stmt_get_connection(stmt->stmt);
		if (mysql_warning_count(hdl)) {
			w = hyss_get_warnings(hdl);
#else
		if (mysqlnd_stmt_warning_count(stmt->stmt)) {
			w = hyss_get_warnings(mysqli_stmt_get_connection(stmt->stmt));
#endif
		} else {
			hyss_error_docref(NULL, E_WARNING, "No warnings found");
			RETURN_FALSE;
		}
	} else {
		hyss_error_docref(NULL, E_WARNING, "invalid class argument");
		RETURN_FALSE;
	}

	mysqli_resource = (MYSQLI_RESOURCE *)ecalloc (1, sizeof(MYSQLI_RESOURCE));
	mysqli_resource->ptr = mysqli_resource->info = (void *)w;
	mysqli_resource->status = MYSQLI_STATUS_VALID;

	if (!getThis() || !instanceof_function(Z_OBJCE_P(getThis()), mysqli_warning_class_entry)) {
		MYSQLI_RETURN_RESOURCE(mysqli_resource, mysqli_warning_class_entry);
	} else {
		(Z_MYSQLI_P(getThis()))->ptr = mysqli_resource;
	}

}
/* }}} */

/* {{{ mysqli_warning_methods */
const gear_function_entry mysqli_warning_methods[] = {
	HYSS_ME(mysqli_warning, __construct,		NULL, GEAR_ACC_PROTECTED)
	HYSS_ME(mysqli_warning, next, 			NULL, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};
/* }}} */

/* {{{ mysqli_warning_property_entries */
const mysqli_property_entry mysqli_warning_property_entries[] = {
	{"message", sizeof("message") - 1, mysqli_warning_message, NULL},
	{"sqlstate", sizeof("sqlstate") - 1, mysqli_warning_sqlstate, NULL},
	{"errno", sizeof("errno") - 1, mysqli_warning_errno, NULL},
	{NULL, 0, NULL, NULL}
};
/* }}} */

