/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MYSQLI_PRIV_H
#define MYSQLI_PRIV_H

#ifdef HYSS_MYSQL_UNIX_SOCK_ADDR
#ifdef MYSQL_UNIX_ADDR
#undef MYSQL_UNIX_ADDR
#endif
#define MYSQL_UNIX_ADDR HYSS_MYSQL_UNIX_SOCK_ADDR
#endif

/* character set support */
#if defined(MYSQLND_VERSION_ID) || MYSQL_VERSION_ID > 50009
#define HAVE_MYSQLI_GET_CHARSET
#endif

#if defined(MYSQLND_VERSION_ID) || MYSQL_VERSION_ID > 50005
#define HAVE_MYSQLI_SET_CHARSET
#endif


extern const gear_function_entry mysqli_functions[];
extern const gear_function_entry mysqli_link_methods[];
extern const gear_function_entry mysqli_stmt_methods[];
extern const gear_function_entry mysqli_result_methods[];
extern const gear_function_entry mysqli_driver_methods[];
extern const gear_function_entry mysqli_warning_methods[];
extern const gear_function_entry mysqli_exception_methods[];

extern const mysqli_property_entry mysqli_link_property_entries[];
extern const mysqli_property_entry mysqli_result_property_entries[];
extern const mysqli_property_entry mysqli_stmt_property_entries[];
extern const mysqli_property_entry mysqli_driver_property_entries[];
extern const mysqli_property_entry mysqli_warning_property_entries[];

extern const gear_property_info mysqli_link_property_info_entries[];
extern const gear_property_info mysqli_result_property_info_entries[];
extern const gear_property_info mysqli_stmt_property_info_entries[];
extern const gear_property_info mysqli_driver_property_info_entries[];
extern const gear_property_info mysqli_warning_property_info_entries[];

extern int hyss_le_pmysqli(void);
extern void hyss_mysqli_dtor_p_elements(void *data);

extern void hyss_mysqli_close(MY_MYSQL * mysql, int close_type, int resource_status);

extern void hyss_mysqli_fetch_into_hash(INTERNAL_FUNCTION_PARAMETERS, int override_flag, int into_object);
extern void hyss_clear_stmt_bind(MY_STMT *stmt);
extern void hyss_clear_mysql(MY_MYSQL *);
#ifdef MYSQLI_USE_MYSQLND
extern MYSQLI_WARNING *hyss_get_warnings(MYSQLND_CONN_DATA * mysql);
#else
extern MYSQLI_WARNING *hyss_get_warnings(MYSQL * mysql);
#endif

extern void hyss_clear_warnings(MYSQLI_WARNING *w);
extern void hyss_free_stmt_bind_buffer(BIND_BUFFER bbuf, int type);
extern void hyss_mysqli_report_error(const char *sqlstate, int errorno, const char *error);
extern void hyss_mysqli_report_index(const char *query, unsigned int status);
extern void hyss_mysqli_throw_sql_exception(char *sqlstate, int errorno, char *format, ...);

#define HYSS_MYSQLI_EXPORT(__type) HYSS_MYSQLI_API __type

HYSS_MYSQLI_EXPORT(gear_object *) mysqli_objects_new(gear_class_entry *);

#define MYSQLI_DISABLE_MQ if (mysql->multi_query) { \
	mysql_set_server_option(mysql->mysql, MYSQL_OPTION_MULTI_STATEMENTS_OFF); \
	mysql->multi_query = 0; \
}

#define MYSQLI_ENABLE_MQ if (!mysql->multi_query) { \
	mysql_set_server_option(mysql->mysql, MYSQL_OPTION_MULTI_STATEMENTS_ON); \
	mysql->multi_query = 1; \
}

#define MYSQLI_RETURN_LONG_INT(__val) \
{ \
	if ((__val) < GEAR_LONG_MAX) {		\
		RETURN_LONG((gear_long) (__val));		\
	} else {				\
		/* always used with my_ulonglong -> %llu */ \
		RETURN_STR(strpprintf(0, MYSQLI_LLU_SPEC, (__val)));	\
	} \
}

#define MYSQLI_STORE_RESULT 0
#define MYSQLI_USE_RESULT 	1
#ifdef MYSQLI_USE_MYSQLND
#define MYSQLI_ASYNC	 	8
#define MYSQLI_STORE_RESULT_COPY_DATA 16
#else
/* libmysql */
#define MYSQLI_ASYNC	 	0
#define MYSQLI_STORE_RESULT_COPY_DATA	0
#endif

/* for mysqli_fetch_assoc */
#define MYSQLI_ASSOC	1
#define MYSQLI_NUM		2
#define MYSQLI_BOTH		3

/* fetch types */
#define FETCH_SIMPLE		1
#define FETCH_RESULT		2

/*** REPORT MODES ***/
#define MYSQLI_REPORT_OFF       0
#define MYSQLI_REPORT_ERROR	1
#define MYSQLI_REPORT_STRICT	2
#define MYSQLI_REPORT_INDEX	4
#define MYSQLI_REPORT_CLOSE	8
#define MYSQLI_REPORT_ALL       255

#define MYSQLI_REPORT_MYSQL_ERROR(mysql) \
if ((MyG(report_mode) & MYSQLI_REPORT_ERROR) && mysql_errno(mysql)) { \
	hyss_mysqli_report_error(mysql_sqlstate(mysql), mysql_errno(mysql), mysql_error(mysql)); \
}

#define MYSQLI_REPORT_STMT_ERROR(stmt) \
if ((MyG(report_mode) & MYSQLI_REPORT_ERROR) && mysql_stmt_errno(stmt)) { \
	hyss_mysqli_report_error(mysql_stmt_sqlstate(stmt), mysql_stmt_errno(stmt), mysql_stmt_error(stmt)); \
}

void mysqli_common_connect(INTERNAL_FUNCTION_PARAMETERS, gear_bool is_real_connect, gear_bool in_ctor);

void hyss_mysqli_init(INTERNAL_FUNCTION_PARAMETERS, gear_bool is_method);

#endif /* MYSQLI_PRIV_H */
