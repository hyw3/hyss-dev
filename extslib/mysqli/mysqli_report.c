/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_mysqli_structs.h"

extern void hyss_mysqli_throw_sql_exception(char *sqlstate, int errorno, char *format, ...);

/* {{{ proto bool mysqli_report(int flags)
   sets report level */
HYSS_FUNCTION(mysqli_report)
{
	gear_long flags;


	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &flags) == FAILURE) {
		return;
	}

	MyG(report_mode) = flags;

	RETURN_TRUE;
}
/* }}} */

/* {{{ void hyss_mysqli_report_error(char *sqlstate, int errorno, char *error) */
void hyss_mysqli_report_error(const char *sqlstate, int errorno, const char *error)
{
	hyss_mysqli_throw_sql_exception((char *)sqlstate, errorno, "%s", error);
}
/* }}} */

/* {{{ void hyss_mysqli_report_index() */
void hyss_mysqli_report_index(const char *query, unsigned int status) {
	char index[15];

	if (status & SERVER_QUERY_NO_GOOD_INDEX_USED) {
		strcpy(index, "Bad index");
	} else if (status & SERVER_QUERY_NO_INDEX_USED) {
		strcpy(index, "No index");
	} else {
		return;
	}
	hyss_mysqli_throw_sql_exception("00000", 0, "%s used in query/prepared statement %s", index, query);
}
/* }}} */

