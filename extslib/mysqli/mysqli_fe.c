/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <signal.h>

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_mysqli_structs.h"
#include "mysqli_fe.h"
#include "mysqli_priv.h"

#ifdef MYSQLI_USE_FULL_TYPED_ARGINFO_0
#define MYSQLI_GEAR_ARG_OBJ_INFO_LINK() GEAR_ARG_OBJ_INFO(0, link, mysqli, 0)
#define MYSQLI_GEAR_ARG_OBJ_INFO_RESULT() GEAR_ARG_OBJ_INFO(0, result, mysqli_result, 0)
#define MYSQLI_GEAR_ARG_OBJ_INFO_STMT() GEAR_ARG_OBJ_INFO(0, stmt, mysqli_stmt, 0)
#else
#define MYSQLI_GEAR_ARG_OBJ_INFO_LINK() GEAR_ARG_INFO(0, link)
#define MYSQLI_GEAR_ARG_OBJ_INFO_RESULT() GEAR_ARG_INFO(0, result)
#define MYSQLI_GEAR_ARG_OBJ_INFO_STMT() GEAR_ARG_INFO(0, stmt)
#endif

GEAR_BEGIN_ARG_INFO(arginfo_mysqli_stmt_bind_result, 0)
	MYSQLI_GEAR_ARG_OBJ_INFO_STMT()
	GEAR_ARG_VARIADIC_INFO(1, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_mysqli_stmt_bind_param, 0)
	MYSQLI_GEAR_ARG_OBJ_INFO_STMT()
	GEAR_ARG_INFO(0, types)
	GEAR_ARG_VARIADIC_INFO(1, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_class_mysqli_stmt_bind_result, 0)
	GEAR_ARG_VARIADIC_INFO(1, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_class_mysqli_stmt_bind_param, 0)
	GEAR_ARG_INFO(0, types)
	GEAR_ARG_VARIADIC_INFO(1, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_poll, 0, 0, 4)
	GEAR_ARG_ARRAY_INFO(1, read, 1)
	GEAR_ARG_ARRAY_INFO(1, write, 1)
	GEAR_ARG_ARRAY_INFO(1, error, 1)
	GEAR_ARG_INFO(0, sec)
	GEAR_ARG_INFO(0, usec)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_no_params, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_only_link, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_autocommit, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_autocommit, 0, 0, 1)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_begin_transaction, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_begin_transaction, 0, 0, 0)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_savepoint, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_savepoint, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_release_savepoint, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_release_savepoint, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_commit, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_commit, 0, 0, 0)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_rollback, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_rollback, 0, 0, 0)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_store_result, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_store_result, 0, 0, 0)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_change_user, 0, 0, 4)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, user)
	GEAR_ARG_INFO(0, password)
	GEAR_ARG_INFO(0, database)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_change_user, 0, 0, 3)
	GEAR_ARG_INFO(0, user)
	GEAR_ARG_INFO(0, password)
	GEAR_ARG_INFO(0, database)
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_connect, 0, 0, 0)
	GEAR_ARG_INFO(0, host)
	GEAR_ARG_INFO(0, user)
	GEAR_ARG_INFO(0, password)
	GEAR_ARG_INFO(0, database)
	GEAR_ARG_INFO(0, port)
	GEAR_ARG_INFO(0, socket)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_real_connect, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, host)
	GEAR_ARG_INFO(0, user)
	GEAR_ARG_INFO(0, password)
	GEAR_ARG_INFO(0, database)
	GEAR_ARG_INFO(0, port)
	GEAR_ARG_INFO(0, socket)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_real_connect, 0, 0, 0)
	GEAR_ARG_INFO(0, host)
	GEAR_ARG_INFO(0, user)
	GEAR_ARG_INFO(0, password)
	GEAR_ARG_INFO(0, database)
	GEAR_ARG_INFO(0, port)
	GEAR_ARG_INFO(0, socket)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_only_result, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_RESULT()
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_only_statement, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_STMT()
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_data_seek, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_RESULT()
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_stmt_data_seek, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_STMT()
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_data_seek, 0, 0, 1)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_debug, 0, 0, 1)
	GEAR_ARG_INFO(0, debug_options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_result_and_fieldnr, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_RESULT()
	GEAR_ARG_INFO(0, field_nr)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_result_and_fieldnr, 0, 0, 1)
	GEAR_ARG_INFO(0, field_nr)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_fetch_array, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_RESULT()
	GEAR_ARG_INFO(0, result_type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_fetch_array, 0, 0, 0)
	GEAR_ARG_INFO(0, result_type)
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_fetch_object, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_RESULT()
	GEAR_ARG_INFO(0, class_name)
	GEAR_ARG_ARRAY_INFO(0, params, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_fetch_object, 0, 0, 0)
	GEAR_ARG_INFO(0, class_name)
	GEAR_ARG_ARRAY_INFO(0, params, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_kill, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, connection_id)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_kill, 0, 0, 1)
	GEAR_ARG_INFO(0, connection_id)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_query, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, query)
	GEAR_ARG_INFO(0, resultmode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_multi_query, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, query)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_real_query, 0, 0, 1)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, query)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_prepare, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, query)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_stmt_prepare, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_STMT()
	GEAR_ARG_INFO(0, query)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_query, 0, 0, 1)
	GEAR_ARG_INFO(0, query)
	GEAR_ARG_INFO(0, resultmode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_prepare, 0, 0, 1)
	GEAR_ARG_INFO(0, query)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_multi_query, 0, 0, 1)
	GEAR_ARG_INFO(0, query)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_real_query, 0, 0, 1)
	GEAR_ARG_INFO(0, query)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_options, 0, 0, 3)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, option)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_options, 0, 0, 2)
	GEAR_ARG_INFO(0, option)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_report, 0, 0, 1)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_real_escape_string, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, string_to_escape)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_real_escape_string, 0, 0, 1)
	GEAR_ARG_INFO(0, string_to_escape)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_select_db, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, database)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_select_db, 0, 0, 1)
	GEAR_ARG_INFO(0, database)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_set_charset, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, charset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_set_charset, 0, 0, 1)
	GEAR_ARG_INFO(0, charset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_stmt_attr_get, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_STMT()
	GEAR_ARG_INFO(0, attribute)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_stmt_attr_get, 0, 0, 1)
	GEAR_ARG_INFO(0, attribute)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_stmt_attr_set, 0, 0, 3)
	MYSQLI_GEAR_ARG_OBJ_INFO_STMT()
	GEAR_ARG_INFO(0, attribute)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_stmt_attr_set, 0, 0, 2)
	GEAR_ARG_INFO(0, attribute)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_stmt_send_long_data, 0, 0, 3)
	MYSQLI_GEAR_ARG_OBJ_INFO_STMT()
	GEAR_ARG_INFO(0, param_nr)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_stmt_send_long_data, 0, 0, 2)
	GEAR_ARG_INFO(0, param_nr)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_ssl_set, 0, 0, 6)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, key)
	GEAR_ARG_INFO(0, cert)
	GEAR_ARG_INFO(0, certificate_authority)
	GEAR_ARG_INFO(0, certificate_authority_path)
	GEAR_ARG_INFO(0, cipher)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_ssl_set, 0, 0, 5)
	GEAR_ARG_INFO(0, key)
	GEAR_ARG_INFO(0, cert)
	GEAR_ARG_INFO(0, certificate_authority)
	GEAR_ARG_INFO(0, certificate_authority_path)
	GEAR_ARG_INFO(0, cipher)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_refresh, 0, 0, 2)
	MYSQLI_GEAR_ARG_OBJ_INFO_LINK()
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_class_mysqli_refresh, 0, 0, 1)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mysqli_no_options, 0, 0, 0)
GEAR_END_ARG_INFO()


/* {{{ mysqli_functions[]
 *
 * Every user visible function must have an entry in mysqli_functions[].
 */
const gear_function_entry mysqli_functions[] = {
	HYSS_FE(mysqli_affected_rows,						arginfo_mysqli_only_link)
	HYSS_FE(mysqli_autocommit,							arginfo_mysqli_autocommit)
	HYSS_FE(mysqli_begin_transaction,					arginfo_mysqli_begin_transaction)
	HYSS_FE(mysqli_change_user,							arginfo_mysqli_change_user)
	HYSS_FE(mysqli_character_set_name,					arginfo_mysqli_only_link)
	HYSS_FE(mysqli_close,								arginfo_mysqli_only_link)
	HYSS_FE(mysqli_commit,								arginfo_mysqli_commit)
	HYSS_FE(mysqli_connect, 								arginfo_mysqli_connect)
	HYSS_FE(mysqli_connect_errno,						arginfo_mysqli_no_params)
	HYSS_FE(mysqli_connect_error,						arginfo_mysqli_no_params)
	HYSS_FE(mysqli_data_seek,							arginfo_mysqli_data_seek)
	HYSS_FE(mysqli_dump_debug_info,						arginfo_mysqli_only_link)
	HYSS_FE(mysqli_debug,								arginfo_mysqli_debug)
#if defined(HAVE_EMBEDDED_MYSQLI)
	HYSS_FE(mysqli_embedded_server_end,					NULL)
	HYSS_FE(mysqli_embedded_server_start,				NULL)
#endif
	HYSS_FE(mysqli_errno,								arginfo_mysqli_only_link)
	HYSS_FE(mysqli_error,								arginfo_mysqli_only_link)
	HYSS_FE(mysqli_error_list,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_stmt_execute,							arginfo_mysqli_only_statement)
	HYSS_FALIAS(mysqli_execute, mysqli_stmt_execute,		arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_fetch_field,							arginfo_mysqli_only_result)
	HYSS_FE(mysqli_fetch_fields,							arginfo_mysqli_only_result)
	HYSS_FE(mysqli_fetch_field_direct,					arginfo_mysqli_result_and_fieldnr)
	HYSS_FE(mysqli_fetch_lengths,						arginfo_mysqli_only_result)
#ifdef MYSQLI_USE_MYSQLND
	HYSS_FE(mysqli_fetch_all,							arginfo_mysqli_fetch_array)
#endif
	HYSS_FE(mysqli_fetch_array,							arginfo_mysqli_fetch_array)
	HYSS_FE(mysqli_fetch_assoc,							arginfo_mysqli_only_result)
	HYSS_FE(mysqli_fetch_object,							arginfo_mysqli_fetch_object)
	HYSS_FE(mysqli_fetch_row,							arginfo_mysqli_only_result)
	HYSS_FE(mysqli_field_count,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_field_seek,							arginfo_mysqli_result_and_fieldnr)
	HYSS_FE(mysqli_field_tell,							arginfo_mysqli_only_result)
	HYSS_FE(mysqli_free_result,							arginfo_mysqli_only_result)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FE(mysqli_get_connection_stats,					arginfo_mysqli_only_link)
	HYSS_FE(mysqli_get_client_stats,						arginfo_mysqli_no_params)
#endif
#ifdef HAVE_MYSQLI_GET_CHARSET
	HYSS_FE(mysqli_get_charset,							arginfo_mysqli_only_link)
#endif
	HYSS_FE(mysqli_get_client_info,						arginfo_mysqli_no_options)
	HYSS_FE(mysqli_get_client_version,					arginfo_mysqli_only_link)
	HYSS_FE(mysqli_get_links_stats,						arginfo_mysqli_no_options)
	HYSS_FE(mysqli_get_host_info,						arginfo_mysqli_only_link)
	HYSS_FE(mysqli_get_proto_info,						arginfo_mysqli_only_link)
	HYSS_FE(mysqli_get_server_info,						arginfo_mysqli_only_link)
	HYSS_FE(mysqli_get_server_version,					arginfo_mysqli_only_link)
	HYSS_FE(mysqli_get_warnings,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_init, 								arginfo_mysqli_no_params)
	HYSS_FE(mysqli_info,									arginfo_mysqli_only_link)
	HYSS_FE(mysqli_insert_id,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_kill,									arginfo_mysqli_kill)
	HYSS_FE(mysqli_more_results,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_multi_query, 							arginfo_mysqli_multi_query)
	HYSS_FE(mysqli_next_result,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_num_fields,							arginfo_mysqli_only_result)
	HYSS_FE(mysqli_num_rows,								arginfo_mysqli_only_result)
	HYSS_FE(mysqli_options, 								arginfo_mysqli_options)
	HYSS_FE(mysqli_ping,									arginfo_mysqli_only_link)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FE(mysqli_poll,									arginfo_mysqli_poll)
#endif
	HYSS_FE(mysqli_prepare,								arginfo_mysqli_prepare)
	HYSS_FE(mysqli_report,								arginfo_mysqli_report)
	HYSS_FE(mysqli_query,								arginfo_mysqli_query)
	HYSS_FE(mysqli_real_connect,							arginfo_mysqli_real_connect)
	HYSS_FE(mysqli_real_escape_string,					arginfo_mysqli_real_escape_string)
	HYSS_FE(mysqli_real_query,							arginfo_mysqli_real_query)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FE(mysqli_reap_async_query,						arginfo_mysqli_only_link)
#endif
	HYSS_FE(mysqli_release_savepoint,					arginfo_mysqli_release_savepoint)
	HYSS_FE(mysqli_rollback,								arginfo_mysqli_rollback)
	HYSS_FE(mysqli_savepoint,							arginfo_mysqli_savepoint)
	HYSS_FE(mysqli_select_db,							arginfo_mysqli_select_db)
#ifdef HAVE_MYSQLI_SET_CHARSET
	HYSS_FE(mysqli_set_charset,							arginfo_mysqli_set_charset)
#endif
	HYSS_FE(mysqli_stmt_affected_rows,					arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_attr_get,						arginfo_mysqli_stmt_attr_get)
	HYSS_FE(mysqli_stmt_attr_set,						arginfo_mysqli_stmt_attr_set)
	HYSS_FE(mysqli_stmt_bind_param,						arginfo_mysqli_stmt_bind_param)
	HYSS_FE(mysqli_stmt_bind_result,						arginfo_mysqli_stmt_bind_result)
	HYSS_FE(mysqli_stmt_close,							arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_data_seek,						arginfo_mysqli_stmt_data_seek)
	HYSS_FE(mysqli_stmt_errno,							arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_error,							arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_error_list,						arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_fetch,							arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_field_count,						arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_free_result,						arginfo_mysqli_only_statement)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FE(mysqli_stmt_get_result,						arginfo_mysqli_only_statement)
#endif
	HYSS_FE(mysqli_stmt_get_warnings,					arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_init,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_stmt_insert_id,						arginfo_mysqli_only_statement)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FE(mysqli_stmt_more_results,					arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_next_result,						arginfo_mysqli_only_statement)
#endif
	HYSS_FE(mysqli_stmt_num_rows,						arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_param_count,						arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_prepare,							arginfo_mysqli_stmt_prepare)
	HYSS_FE(mysqli_stmt_reset,							arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_result_metadata,					arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_send_long_data,					arginfo_mysqli_stmt_send_long_data)
	HYSS_FE(mysqli_stmt_store_result,					arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_stmt_sqlstate,   						arginfo_mysqli_only_statement)
	HYSS_FE(mysqli_sqlstate,   							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_ssl_set,								arginfo_mysqli_ssl_set)
	HYSS_FE(mysqli_stat,									arginfo_mysqli_only_link)
	HYSS_FE(mysqli_store_result,							arginfo_mysqli_store_result)
	HYSS_FE(mysqli_thread_id,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_thread_safe,							arginfo_mysqli_no_params)
	HYSS_FE(mysqli_use_result,							arginfo_mysqli_only_link)
	HYSS_FE(mysqli_warning_count,						arginfo_mysqli_only_link)

	HYSS_FE(mysqli_refresh,								arginfo_mysqli_refresh)

	/* Aliases */
	HYSS_FALIAS(mysqli_escape_string,	mysqli_real_escape_string,	arginfo_mysqli_query)
	HYSS_FALIAS(mysqli_set_opt,			mysqli_options,				NULL)

	HYSS_FE_END
};
/* }}} */

/* {{{ mysqli_link_methods[]
 *
 * Every user visible function must have an entry in mysqli_functions[].
 */
const gear_function_entry mysqli_link_methods[] = {
	HYSS_FALIAS(autocommit, mysqli_autocommit, arginfo_class_mysqli_autocommit)
	HYSS_FALIAS(begin_transaction, mysqli_begin_transaction, arginfo_class_mysqli_begin_transaction)
	HYSS_FALIAS(change_user,mysqli_change_user, arginfo_class_mysqli_change_user)
	HYSS_FALIAS(character_set_name, mysqli_character_set_name, arginfo_mysqli_no_params)
	HYSS_FALIAS(close, mysqli_close, arginfo_mysqli_no_params)
	HYSS_FALIAS(commit, mysqli_commit, arginfo_class_mysqli_commit)
	HYSS_FALIAS(connect, mysqli_connect, arginfo_mysqli_connect)
	HYSS_FALIAS(dump_debug_info, mysqli_dump_debug_info, arginfo_mysqli_no_params)
	HYSS_FALIAS(debug, mysqli_debug, arginfo_mysqli_debug)
#ifdef HAVE_MYSQLI_GET_CHARSET
	HYSS_FALIAS(get_charset, mysqli_get_charset, arginfo_mysqli_no_params)
#endif
	HYSS_FALIAS(get_client_info, mysqli_get_client_info, arginfo_mysqli_no_params)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FALIAS(get_connection_stats, mysqli_get_connection_stats, arginfo_mysqli_no_params)
#endif
	HYSS_FALIAS(get_server_info, mysqli_get_server_info, arginfo_mysqli_no_params)
	HYSS_FALIAS(get_warnings, mysqli_get_warnings, arginfo_mysqli_no_params)
	HYSS_FALIAS(init,mysqli_init_method, arginfo_mysqli_no_params)
	HYSS_FALIAS(kill,mysqli_kill, arginfo_class_mysqli_kill)
	HYSS_FALIAS(multi_query, mysqli_multi_query, arginfo_class_mysqli_multi_query)
	HYSS_FALIAS(__construct, mysqli_link_construct, arginfo_mysqli_connect)
	HYSS_FALIAS(more_results, mysqli_more_results, arginfo_mysqli_no_params)
	HYSS_FALIAS(next_result, mysqli_next_result, arginfo_mysqli_no_params)
	HYSS_FALIAS(options, mysqli_options, arginfo_class_mysqli_options)
	HYSS_FALIAS(ping, mysqli_ping, arginfo_mysqli_no_params)
#if defined(MYSQLI_USE_MYSQLND)
	GEAR_FENTRY(poll, GEAR_FN(mysqli_poll), arginfo_mysqli_poll, GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
#endif
	HYSS_FALIAS(prepare, mysqli_prepare, arginfo_class_mysqli_prepare)
	HYSS_FALIAS(query, mysqli_query, arginfo_class_mysqli_query)
	HYSS_FALIAS(real_connect, mysqli_real_connect, arginfo_class_mysqli_real_connect)
	HYSS_FALIAS(real_escape_string, mysqli_real_escape_string, arginfo_class_mysqli_real_escape_string)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FALIAS(reap_async_query, mysqli_reap_async_query, arginfo_mysqli_no_params)
#endif
	HYSS_FALIAS(escape_string, mysqli_real_escape_string, arginfo_class_mysqli_real_escape_string)
	HYSS_FALIAS(real_query, mysqli_real_query, arginfo_class_mysqli_real_query)
	HYSS_FALIAS(release_savepoint, mysqli_release_savepoint, arginfo_class_mysqli_release_savepoint)
	HYSS_FALIAS(rollback, mysqli_rollback, arginfo_class_mysqli_rollback)
	HYSS_FALIAS(savepoint, mysqli_savepoint, arginfo_class_mysqli_savepoint)
	HYSS_FALIAS(select_db,mysqli_select_db, arginfo_class_mysqli_select_db)
#ifdef HAVE_MYSQLI_SET_CHARSET
	HYSS_FALIAS(set_charset, mysqli_set_charset, arginfo_class_mysqli_set_charset)
#endif
	HYSS_FALIAS(set_opt, mysqli_options, arginfo_class_mysqli_options)
	HYSS_FALIAS(ssl_set, mysqli_ssl_set, arginfo_class_mysqli_ssl_set)
	HYSS_FALIAS(stat, mysqli_stat, arginfo_mysqli_no_params)
	HYSS_FALIAS(stmt_init, mysqli_stmt_init, arginfo_mysqli_no_params)
	HYSS_FALIAS(store_result, mysqli_store_result, arginfo_class_store_result)
	HYSS_FALIAS(thread_safe, mysqli_thread_safe, arginfo_mysqli_no_params)
	HYSS_FALIAS(use_result, mysqli_use_result, arginfo_mysqli_no_params)
	HYSS_FALIAS(refresh,mysqli_refresh, arginfo_class_mysqli_refresh)
	HYSS_FE_END
};
/* }}} */

/* {{{ mysqli_result_methods[]
 *
 * Every user visible function must have an entry in mysqli_result_functions[].
 */
const gear_function_entry mysqli_result_methods[] = {
	HYSS_FALIAS(__construct, mysqli_result_construct, NULL)
	HYSS_FALIAS(close, mysqli_free_result, arginfo_mysqli_no_params)
	HYSS_FALIAS(free, mysqli_free_result, arginfo_mysqli_no_params)
	HYSS_FALIAS(data_seek, mysqli_data_seek, arginfo_class_mysqli_data_seek)
	HYSS_FALIAS(fetch_field, mysqli_fetch_field, arginfo_mysqli_no_params)
	HYSS_FALIAS(fetch_fields, mysqli_fetch_fields, arginfo_mysqli_no_params)
	HYSS_FALIAS(fetch_field_direct, mysqli_fetch_field_direct, arginfo_class_mysqli_result_and_fieldnr)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FALIAS(fetch_all, mysqli_fetch_all, arginfo_class_mysqli_fetch_array)
#endif
	HYSS_FALIAS(fetch_array, mysqli_fetch_array, arginfo_class_mysqli_fetch_array)
	HYSS_FALIAS(fetch_assoc, mysqli_fetch_assoc, arginfo_mysqli_no_params)
	HYSS_FALIAS(fetch_object,mysqli_fetch_object, arginfo_class_mysqli_fetch_object)
	HYSS_FALIAS(fetch_row, mysqli_fetch_row, arginfo_mysqli_no_params)
	HYSS_FALIAS(field_seek, mysqli_field_seek, arginfo_class_mysqli_result_and_fieldnr)
	HYSS_FALIAS(free_result, mysqli_free_result, arginfo_mysqli_no_params)
	HYSS_FE_END
};
/* }}} */

/* {{{ mysqli_stmt_methods[]
 *
 * Every user visible function must have an entry in mysqli_stmt_functions[].
 */
const gear_function_entry mysqli_stmt_methods[] = {
	HYSS_FALIAS(__construct, mysqli_stmt_construct, NULL)
	HYSS_FALIAS(attr_get, mysqli_stmt_attr_get, arginfo_class_mysqli_stmt_attr_get)
	HYSS_FALIAS(attr_set,mysqli_stmt_attr_set, arginfo_class_mysqli_stmt_attr_set)
	HYSS_FALIAS(bind_param,mysqli_stmt_bind_param, arginfo_class_mysqli_stmt_bind_param)
	HYSS_FALIAS(bind_result,mysqli_stmt_bind_result, arginfo_class_mysqli_stmt_bind_result)
	HYSS_FALIAS(close, mysqli_stmt_close, arginfo_mysqli_no_params)
	HYSS_FALIAS(data_seek, mysqli_stmt_data_seek, arginfo_class_mysqli_data_seek)
	HYSS_FALIAS(execute, mysqli_stmt_execute, arginfo_mysqli_no_params)
	HYSS_FALIAS(fetch, mysqli_stmt_fetch, arginfo_mysqli_no_params)
	HYSS_FALIAS(get_warnings, mysqli_stmt_get_warnings,	arginfo_mysqli_no_params)
	HYSS_FALIAS(result_metadata, mysqli_stmt_result_metadata, arginfo_mysqli_no_params)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FALIAS(more_results, mysqli_stmt_more_results, arginfo_mysqli_no_params)
	HYSS_FALIAS(next_result, mysqli_stmt_next_result, arginfo_mysqli_no_params)
#endif
	HYSS_FALIAS(num_rows, mysqli_stmt_num_rows, arginfo_mysqli_no_params)
	HYSS_FALIAS(send_long_data, mysqli_stmt_send_long_data, arginfo_class_mysqli_stmt_send_long_data)
	HYSS_FALIAS(free_result, mysqli_stmt_free_result, arginfo_mysqli_no_params)
	HYSS_FALIAS(reset, mysqli_stmt_reset, arginfo_mysqli_no_params)
	HYSS_FALIAS(prepare, mysqli_stmt_prepare, arginfo_class_mysqli_prepare)
	HYSS_FALIAS(store_result, mysqli_stmt_store_result, arginfo_mysqli_no_params)
#if defined(MYSQLI_USE_MYSQLND)
	HYSS_FALIAS(get_result, mysqli_stmt_get_result, arginfo_mysqli_no_params)
#endif
	HYSS_FE_END
};
/* }}} */

