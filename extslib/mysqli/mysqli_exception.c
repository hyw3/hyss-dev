/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <signal.h>

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_mysqli_structs.h"
#include "mysqli_priv.h"
#include "gear_exceptions.h"

/* {{{ mysqli_exception_methods[]
 */
const gear_function_entry mysqli_exception_methods[] = {
	HYSS_FE_END
};
/* }}} */

void hyss_mysqli_throw_sql_exception(char *sqlstate, int errorno, char *format, ...)
{
	zval	sql_ex;
	va_list arg;
	char 	*message;

	va_start(arg, format);
	vspprintf(&message, 0, format, arg);
	va_end(arg);

	if (!(MyG(report_mode) & MYSQLI_REPORT_STRICT)) {
	 	hyss_error_docref(NULL, E_WARNING, "(%s/%d): %s", sqlstate, errorno, message);
		efree(message);
		return;
	}

	object_init_ex(&sql_ex, mysqli_exception_class_entry);

	if (message) {
		gear_update_property_string(mysqli_exception_class_entry, &sql_ex, "message", sizeof("message") - 1,
									message);
	}

	if (sqlstate) {
		gear_update_property_string(mysqli_exception_class_entry, &sql_ex, "sqlstate", sizeof("sqlstate") - 1,
									sqlstate);
	} else {
		gear_update_property_string(mysqli_exception_class_entry, &sql_ex, "sqlstate", sizeof("sqlstate") - 1,
									"00000");
	}

	efree(message);
	gear_update_property_long(mysqli_exception_class_entry, &sql_ex, "code", sizeof("code") - 1, errorno);

	gear_throw_exception_object(&sql_ex);
}

