/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MYSQLI_MYSQLND_H
#define MYSQLI_MYSQLND_H

#include "extslib/mysqlnd/mysqlnd_libmysql_compat.h"
#include "extslib/mysqlnd/mysqlnd_portability.h"

/* Here comes non-libmysql API to have less ifdefs in mysqli*/
#define MYSQLI_CLOSE_EXPLICIT                  MYSQLND_CLOSE_EXPLICIT
#define MYSQLI_CLOSE_IMPLICIT                  MYSQLND_CLOSE_IMPLICIT
#define MYSQLI_CLOSE_DISCONNECTED              MYSQLND_CLOSE_DISCONNECTED

#define mysqli_result_is_unbuffered(r)	((r)->unbuf)
#define mysqli_result_is_unbuffered_and_not_everything_is_fetched(r)	((r)->unbuf && !(r)->unbuf->eof_reached)
#define mysqli_server_status(c)			mysqlnd_get_server_status((c))
#define mysqli_stmt_get_id(s)			((s)->data->stmt_id)
#define mysqli_stmt_warning_count(s)	mysqlnd_stmt_warning_count((s))
#define mysqli_stmt_server_status(s)	mysqlnd_stmt_server_status((s))
#define mysqli_stmt_get_connection(s)	(s)->data->conn
#define mysqli_close(c, how)			mysqlnd_close((c), (how))
#define mysqli_stmt_close(c, implicit)	mysqlnd_stmt_close((c), (implicit))
#define mysqli_free_result(r, implicit)	mysqlnd_free_result((r), (implicit))
#define mysqli_async_query(c, q, l)		mysqlnd_async_query((c), (q), (l))
#define mysqli_change_user_silent(c, u, p, d, p_len)   mysqlnd_change_user_ex((c), (u), (p), (d), TRUE, (size_t)(p_len))

#define HAVE_STMT_NEXT_RESULT

#endif
