/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_MYSQLI_STRUCTS_H
#define HYSS_MYSQLI_STRUCTS_H

/* A little hack to prevent build break, when mysql is used together with
 * c-client, which also defines LIST.
 */
#ifdef LIST
#undef LIST
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifdef MYSQLI_USE_MYSQLND
#include "extslib/mysqlnd/mysqlnd.h"
#include "mysqli_mysqlnd.h"
#else

/*
  The libmysql headers (a PITA) also define it and there will be an warning.
  Undef it and later we might need to define it again.
*/
#ifdef HAVE_MBRLEN
#undef HAVE_MBRLEN
#define WE_HAD_MBRLEN
#endif
#ifdef HAVE_MBSTATE_T
#undef HAVE_MBSTATE_T
#define WE_HAD_MBSTATE_T
#endif

#if defined(ulong) && !defined(HAVE_ULONG)
#define HAVE_ULONG
#endif

#include <my_global.h>

#if !defined(HAVE_MBRLEN) && defined(WE_HAD_MBRLEN)
#define HAVE_MBRLEN 1
#endif

#if !defined(HAVE_MBSTATE_T) && defined(WE_HAD_MBSTATE_T)
#define HAVE_MBSTATE_T 1
#endif

/*
  We need more than mysql.h because we need CHARSET_INFO in one place.
  This order has been borrowed from the ODBC driver. Nothing can be removed
  from the list of headers :(
*/

#include <my_sys.h>
#include <mysql.h>
#include <errmsg.h>
#include <my_list.h>
#include <m_string.h>
#include <mysqld_error.h>
#include <my_list.h>
#include <m_ctype.h>
#include "mysqli_libmysql.h"
#endif /* MYSQLI_USE_MYSQLND */


#define MYSQLI_VERSION_ID		101009

enum mysqli_status {
	MYSQLI_STATUS_UNKNOWN=0,
	MYSQLI_STATUS_CLEARED,
	MYSQLI_STATUS_INITIALIZED,
	MYSQLI_STATUS_VALID
};

typedef struct {
	char		*val;
	gear_ulong		buflen;
	gear_ulong		output_len;
	gear_ulong		type;
} VAR_BUFFER;

typedef struct {
	unsigned int	var_cnt;
	VAR_BUFFER		*buf;
	zval			*vars;
	char			*is_null;
} BIND_BUFFER;

typedef struct {
	MYSQL_STMT	*stmt;
	BIND_BUFFER	param;
	BIND_BUFFER	result;
	char		*query;
#ifndef MYSQLI_USE_MYSQLND
	/* used to manage refcount with libmysql (already implement in mysqlnd) */
	zval		link_handle;
#endif
} MY_STMT;

typedef struct {
	MYSQL			*mysql;
	gear_string		*hash_key;
	zval			li_read;
	hyss_stream		*li_stream;
	unsigned int 	multi_query;
	gear_bool		persistent;
#if defined(MYSQLI_USE_MYSQLND)
	int				async_result_fetch_type;
#endif
} MY_MYSQL;

typedef struct {
	void				*ptr;		/* resource: (mysql, result, stmt)   */
	void				*info;		/* additional buffer				 */
	enum mysqli_status	status;		/* object status */
} MYSQLI_RESOURCE;

typedef struct _mysqli_object {
	void 				*ptr;
	HashTable 			*prop_handler;
	gear_object 		zo;
} mysqli_object; /* extends gear_object */

static inline mysqli_object *hyss_mysqli_fetch_object(gear_object *obj) {
	return (mysqli_object *)((char*)(obj) - XtOffsetOf(mysqli_object, zo));
}

#define Z_MYSQLI_P(zv) hyss_mysqli_fetch_object(Z_OBJ_P((zv)))

typedef struct st_mysqli_warning MYSQLI_WARNING;

struct st_mysqli_warning {
	zval	reason;
	zval	sqlstate;
	int		errorno;
   	MYSQLI_WARNING	*next;
};

typedef struct _mysqli_property_entry {
	const char *pname;
	size_t pname_length;
	zval *(*r_func)(mysqli_object *obj, zval *retval);
	int (*w_func)(mysqli_object *obj, zval *value);
} mysqli_property_entry;

typedef struct {
	gear_ptr_stack free_links;
} mysqli_plist_entry;

#ifdef HYSS_WIN32
#define HYSS_MYSQLI_API __declspec(dllexport)
#define MYSQLI_LLU_SPEC "%I64u"
#define MYSQLI_LL_SPEC "%I64d"
#ifndef L64
#define L64(x) x##i64
#endif
typedef __int64 my_longlong;
#else
# if defined(__GNUC__) && __GNUC__ >= 4
#  define HYSS_MYSQLI_API __attribute__ ((visibility("default")))
# else
#  define HYSS_MYSQLI_API
# endif
/* we need this for PRIu64 and PRId64 */
#include <inttypes.h>
#define MYSQLI_LLU_SPEC "%" PRIu64
#define MYSQLI_LL_SPEC "%" PRId64
#ifndef L64
#define L64(x) x##LL
#endif
typedef int64_t my_longlong;
#endif

#ifdef ZTS
#include "hypbc.h"
#endif

extern gear_class_entry *mysqli_link_class_entry;
extern gear_class_entry *mysqli_stmt_class_entry;
extern gear_class_entry *mysqli_result_class_entry;
extern gear_class_entry *mysqli_driver_class_entry;
extern gear_class_entry *mysqli_warning_class_entry;
extern gear_class_entry *mysqli_exception_class_entry;
extern int hyss_le_pmysqli(void);
extern void hyss_mysqli_dtor_p_elements(void *data);

extern void hyss_mysqli_close(MY_MYSQL * mysql, int close_type, int resource_status);

extern const gear_object_iterator_funcs hyss_mysqli_result_iterator_funcs;
extern gear_object_iterator *hyss_mysqli_result_get_iterator(gear_class_entry *ce, zval *object, int by_ref);

extern void hyss_mysqli_fetch_into_hash_aux(zval *return_value, MYSQL_RES * result, gear_long fetchtype);

#define MYSQLI_DISABLE_MQ if (mysql->multi_query) { \
	mysql_set_server_option(mysql->mysql, MYSQL_OPTION_MULTI_STATEMENTS_OFF); \
	mysql->multi_query = 0; \
}

#define MYSQLI_ENABLE_MQ if (!mysql->multi_query) { \
	mysql_set_server_option(mysql->mysql, MYSQL_OPTION_MULTI_STATEMENTS_ON); \
	mysql->multi_query = 1; \
}

#define REGISTER_MYSQLI_CLASS_ENTRY(name, mysqli_entry, class_functions) { \
	gear_class_entry ce; \
	INIT_CLASS_ENTRY(ce, name,class_functions); \
	ce.create_object = mysqli_objects_new; \
	mysqli_entry = gear_register_internal_class(&ce); \
} \

#define MYSQLI_REGISTER_RESOURCE_EX(__ptr, __zval)  \
	(Z_MYSQLI_P(__zval))->ptr = __ptr;

#define MYSQLI_RETURN_RESOURCE(__ptr, __ce) \
	RETVAL_OBJ(mysqli_objects_new(__ce)); \
	MYSQLI_REGISTER_RESOURCE_EX(__ptr, return_value)

#define MYSQLI_REGISTER_RESOURCE(__ptr, __ce) \
{\
	zval *object = getThis(); \
	if (!object || !instanceof_function(Z_OBJCE_P(object), mysqli_link_class_entry)) { \
		object = return_value; \
		ZVAL_OBJ(object, mysqli_objects_new(__ce)); \
	} \
	MYSQLI_REGISTER_RESOURCE_EX(__ptr, object)\
}

#define MYSQLI_FETCH_RESOURCE(__ptr, __type, __id, __name, __check) \
{ \
	MYSQLI_RESOURCE *my_res; \
	mysqli_object *intern = Z_MYSQLI_P(__id); \
	if (!(my_res = (MYSQLI_RESOURCE *)intern->ptr)) {\
  		hyss_error_docref(NULL, E_WARNING, "Couldn't fetch %s", ZSTR_VAL(intern->zo.ce->name));\
		RETURN_FALSE;\
  	}\
	__ptr = (__type)my_res->ptr; \
	if (__check && my_res->status < __check) { \
		hyss_error_docref(NULL, E_WARNING, "invalid object or resource %s\n", ZSTR_VAL(intern->zo.ce->name)); \
		RETURN_FALSE;\
	}\
}

#define MYSQLI_FETCH_RESOURCE_BY_OBJ(__ptr, __type, __obj, __name, __check) \
{ \
	MYSQLI_RESOURCE *my_res; \
	if (!(my_res = (MYSQLI_RESOURCE *)(__obj->ptr))) {\
  		hyss_error_docref(NULL, E_WARNING, "Couldn't fetch %s", ZSTR_VAL(intern->zo.ce->name));\
  		return;\
  	}\
	__ptr = (__type)my_res->ptr; \
	if (__check && my_res->status < __check) { \
		hyss_error_docref(NULL, E_WARNING, "invalid object or resource %s\n", ZSTR_VAL(intern->zo.ce->name)); \
		return;\
	}\
}

#define MYSQLI_FETCH_RESOURCE_CONN(__ptr, __id, __check) \
{ \
	MYSQLI_FETCH_RESOURCE((__ptr), MY_MYSQL *, (__id), "mysqli_link", (__check)); \
	if (!(__ptr)->mysql) { \
		mysqli_object *intern = Z_MYSQLI_P(__id); \
		hyss_error_docref(NULL, E_WARNING, "invalid object or resource %s\n", ZSTR_VAL(intern->zo.ce->name)); \
		RETURN_NULL(); \
	} \
}

#define MYSQLI_FETCH_RESOURCE_STMT(__ptr, __id, __check) \
{ \
	MYSQLI_FETCH_RESOURCE((__ptr), MY_STMT *, (__id), "mysqli_stmt", (__check)); \
	if (!(__ptr)->stmt) { \
		mysqli_object *intern = Z_MYSQLI_P(__id); \
		hyss_error_docref(NULL, E_WARNING, "invalid object or resource %s\n", ZSTR_VAL(intern->zo.ce->name)); \
		RETURN_NULL();\
	} \
}

#define MYSQLI_SET_STATUS(__id, __value) \
{ \
	mysqli_object *intern = Z_MYSQLI_P(__id); \
	((MYSQLI_RESOURCE *)intern->ptr)->status = __value; \
} \

#define MYSQLI_CLEAR_RESOURCE(__id) \
{ \
	mysqli_object *intern = Z_MYSQLI_P(__id); \
	efree(intern->ptr); \
	intern->ptr = NULL; \
}


GEAR_BEGIN_CAPI_GLOBALS(mysqli)
	gear_long			default_link;
	gear_long			num_links;
	gear_long			max_links;
	gear_long 			num_active_persistent;
	gear_long 			num_inactive_persistent;
	gear_long			max_persistent;
	gear_long			allow_persistent;
	gear_ulong	default_port;
	char			*default_host;
	char			*default_user;
	char			*default_socket;
	char			*default_pw;
	gear_long			reconnect;
	gear_long			allow_local_infile;
	gear_long			strict;
	gear_long			error_no;
	char			*error_msg;
	gear_long			report_mode;
	HashTable		*report_ht;
	gear_ulong	multi_query;
	gear_ulong	embedded;
	gear_bool 		rollback_on_cached_plink;
GEAR_END_CAPI_GLOBALS(mysqli)

#define MyG(v) GEAR_CAPI_GLOBALS_ACCESSOR(mysqli, v)

#if defined(ZTS) && defined(COMPILE_DL_MYSQLI)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#define my_estrdup(x) (x) ? estrdup(x) : NULL
#define my_efree(x) if (x) efree(x)

GEAR_EXTERN_CAPI_GLOBALS(mysqli)

#endif	/* HYSS_MYSQLI_STRUCTS.H */

