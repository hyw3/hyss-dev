/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPL_EXCEPTIONS_H
#define SPL_EXCEPTIONS_H

#include "hyss.h"
#include "hyss_spl.h"

extern HYSSAPI gear_class_entry *spl_ce_LogicException;
extern HYSSAPI gear_class_entry *spl_ce_BadFunctionCallException;
extern HYSSAPI gear_class_entry *spl_ce_BadMethodCallException;
extern HYSSAPI gear_class_entry *spl_ce_DomainException;
extern HYSSAPI gear_class_entry *spl_ce_InvalidArgumentException;
extern HYSSAPI gear_class_entry *spl_ce_LengthException;
extern HYSSAPI gear_class_entry *spl_ce_OutOfRangeException;

extern HYSSAPI gear_class_entry *spl_ce_RuntimeException;
extern HYSSAPI gear_class_entry *spl_ce_OutOfBoundsException;
extern HYSSAPI gear_class_entry *spl_ce_OverflowException;
extern HYSSAPI gear_class_entry *spl_ce_RangeException;
extern HYSSAPI gear_class_entry *spl_ce_UnderflowException;
extern HYSSAPI gear_class_entry *spl_ce_UnexpectedValueException;

HYSS_MINIT_FUNCTION(spl_exceptions);

#endif /* SPL_EXCEPTIONS_H */

