/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
	#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_spl.h"

/* {{{ spl_register_interface */
void spl_register_interface(gear_class_entry ** ppce, char * class_name, const gear_function_entry * functions)
{
	gear_class_entry ce;

	INIT_CLASS_ENTRY_EX(ce, class_name, strlen(class_name), functions);
	*ppce = gear_register_internal_interface(&ce);
}
/* }}} */

/* {{{ spl_register_std_class */
HYSSAPI void spl_register_std_class(gear_class_entry ** ppce, char * class_name, void * obj_ctor, const gear_function_entry * function_list)
{
	gear_class_entry ce;

	INIT_CLASS_ENTRY_EX(ce, class_name, strlen(class_name), function_list);
	*ppce = gear_register_internal_class(&ce);

	/* entries changed by initialize */
	if (obj_ctor) {
		(*ppce)->create_object = obj_ctor;
	}
}
/* }}} */

/* {{{ spl_register_sub_class */
HYSSAPI void spl_register_sub_class(gear_class_entry ** ppce, gear_class_entry * parent_ce, char * class_name, void *obj_ctor, const gear_function_entry * function_list)
{
	gear_class_entry ce;

	INIT_CLASS_ENTRY_EX(ce, class_name, strlen(class_name), function_list);
	*ppce = gear_register_internal_class_ex(&ce, parent_ce);

	/* entries changed by initialize */
	if (obj_ctor) {
		(*ppce)->create_object = obj_ctor;
	} else {
		(*ppce)->create_object = parent_ce->create_object;
	}
}
/* }}} */

/* {{{ spl_register_property */
void spl_register_property( gear_class_entry * class_entry, char *prop_name, int prop_name_len, int prop_flags)
{
	gear_declare_property_null(class_entry, prop_name, prop_name_len, prop_flags);
}
/* }}} */

/* {{{ spl_add_class_name */
void spl_add_class_name(zval *list, gear_class_entry *pce, int allow, int ce_flags)
{
	if (!allow || (allow > 0 && pce->ce_flags & ce_flags) || (allow < 0 && !(pce->ce_flags & ce_flags))) {
		zval *tmp;

		if ((tmp = gear_hash_find(Z_ARRVAL_P(list), pce->name)) == NULL) {
			zval t;
			ZVAL_STR_COPY(&t, pce->name);
			gear_hash_add(Z_ARRVAL_P(list), pce->name, &t);
		}
	}
}
/* }}} */

/* {{{ spl_add_interfaces */
void spl_add_interfaces(zval *list, gear_class_entry * pce, int allow, int ce_flags)
{
	uint32_t num_interfaces;

	for (num_interfaces = 0; num_interfaces < pce->num_interfaces; num_interfaces++) {
		spl_add_class_name(list, pce->interfaces[num_interfaces], allow, ce_flags);
	}
}
/* }}} */

/* {{{ spl_add_traits */
void spl_add_traits(zval *list, gear_class_entry * pce, int allow, int ce_flags)
{
	uint32_t num_traits;

	for (num_traits = 0; num_traits < pce->num_traits; num_traits++) {
		spl_add_class_name(list, pce->traits[num_traits], allow, ce_flags);
	}
}
/* }}} */


/* {{{ spl_add_classes */
int spl_add_classes(gear_class_entry *pce, zval *list, int sub, int allow, int ce_flags)
{
	if (!pce) {
		return 0;
	}
	spl_add_class_name(list, pce, allow, ce_flags);
	if (sub) {
		spl_add_interfaces(list, pce, allow, ce_flags);
		while (pce->parent) {
			pce = pce->parent;
			spl_add_classes(pce, list, sub, allow, ce_flags);
		}
	}
	return 0;
}
/* }}} */

gear_string * spl_gen_private_prop_name(gear_class_entry *ce, char *prop_name, int prop_len) /* {{{ */
{
	return gear_mangle_property_name(ZSTR_VAL(ce->name), ZSTR_LEN(ce->name), prop_name, prop_len, 0);
}
/* }}} */

