/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPL_DIRECTORY_H
#define SPL_DIRECTORY_H

#include "hyss.h"
#include "hyss_spl.h"

extern HYSSAPI gear_class_entry *spl_ce_SplFileInfo;
extern HYSSAPI gear_class_entry *spl_ce_DirectoryIterator;
extern HYSSAPI gear_class_entry *spl_ce_FilesystemIterator;
extern HYSSAPI gear_class_entry *spl_ce_RecursiveDirectoryIterator;
extern HYSSAPI gear_class_entry *spl_ce_GlobIterator;
extern HYSSAPI gear_class_entry *spl_ce_SplFileObject;
extern HYSSAPI gear_class_entry *spl_ce_SplTempFileObject;

HYSS_MINIT_FUNCTION(spl_directory);

typedef enum {
	SPL_FS_INFO, /* must be 0 */
	SPL_FS_DIR,
	SPL_FS_FILE
} SPL_FS_OBJ_TYPE;

typedef struct _spl_filesystem_object  spl_filesystem_object;

typedef void (*spl_foreign_dtor_t)(spl_filesystem_object *object);
typedef void (*spl_foreign_clone_t)(spl_filesystem_object *src, spl_filesystem_object *dst);

HYSSAPI char* spl_filesystem_object_get_path(spl_filesystem_object *intern, size_t *len);

typedef struct _spl_other_handler {
	spl_foreign_dtor_t     dtor;
	spl_foreign_clone_t    clone;
} spl_other_handler;

/* define an overloaded iterator structure */
typedef struct {
	gear_object_iterator  intern;
	zval                  current;
	void                 *object;
} spl_filesystem_iterator;

struct _spl_filesystem_object {
	void               *oth;
	const spl_other_handler  *oth_handler;
	char               *_path;
	size_t             _path_len;
	char               *orig_path;
	char               *file_name;
	size_t             file_name_len;
	SPL_FS_OBJ_TYPE    type;
	gear_long               flags;
	gear_class_entry   *file_class;
	gear_class_entry   *info_class;
	union {
		struct {
			hyss_stream         *dirp;
			hyss_stream_dirent  entry;
			char               *sub_path;
			size_t             sub_path_len;
			int                index;
			int                is_recursive;
			gear_function      *func_rewind;
			gear_function      *func_next;
			gear_function      *func_valid;
		} dir;
		struct {
			hyss_stream         *stream;
			hyss_stream_context *context;
			zval               *zcontext;
			char               *open_mode;
			size_t             open_mode_len;
			zval               current_zval;
			char               *current_line;
			size_t             current_line_len;
			size_t             max_line_len;
			gear_long               current_line_num;
			zval               zresource;
			gear_function      *func_getCurr;
			char               delimiter;
			char               enclosure;
			char               escape;
		} file;
	} u;
	gear_object        std;
};

static inline spl_filesystem_object *spl_filesystem_from_obj(gear_object *obj) /* {{{ */ {
	return (spl_filesystem_object*)((char*)(obj) - XtOffsetOf(spl_filesystem_object, std));
}
/* }}} */

#define Z_SPLFILESYSTEM_P(zv)  spl_filesystem_from_obj(Z_OBJ_P((zv)))

static inline spl_filesystem_iterator* spl_filesystem_object_to_iterator(spl_filesystem_object *obj)
{
	spl_filesystem_iterator    *it;

	it = ecalloc(1, sizeof(spl_filesystem_iterator));
	it->object = (void *)obj;
	gear_iterator_init(&it->intern);
	return it;
}

static inline spl_filesystem_object* spl_filesystem_iterator_to_object(spl_filesystem_iterator *it)
{
	return (spl_filesystem_object*)it->object;
}

#define SPL_FILE_OBJECT_DROP_NEW_LINE      0x00000001 /* drop new lines */
#define SPL_FILE_OBJECT_READ_AHEAD         0x00000002 /* read on rewind/next */
#define SPL_FILE_OBJECT_SKIP_EMPTY         0x00000004 /* skip empty lines */
#define SPL_FILE_OBJECT_READ_CSV           0x00000008 /* read via fgetcsv */
#define SPL_FILE_OBJECT_MASK               0x0000000F /* read via fgetcsv */

#define SPL_FILE_DIR_CURRENT_AS_FILEINFO   0x00000000 /* make RecursiveDirectoryTree::current() return SplFileInfo */
#define SPL_FILE_DIR_CURRENT_AS_SELF       0x00000010 /* make RecursiveDirectoryTree::current() return getSelf() */
#define SPL_FILE_DIR_CURRENT_AS_PATHNAME   0x00000020 /* make RecursiveDirectoryTree::current() return getPathname() */
#define SPL_FILE_DIR_CURRENT_MODE_MASK     0x000000F0 /* mask RecursiveDirectoryTree::current() */
#define SPL_FILE_DIR_CURRENT(intern,mode)  ((intern->flags&SPL_FILE_DIR_CURRENT_MODE_MASK)==mode)

#define SPL_FILE_DIR_KEY_AS_PATHNAME       0x00000000 /* make RecursiveDirectoryTree::key() return getPathname() */
#define SPL_FILE_DIR_KEY_AS_FILENAME       0x00000100 /* make RecursiveDirectoryTree::key() return getFilename() */
#define SPL_FILE_DIR_FOLLOW_SYMLINKS       0x00000200 /* make RecursiveDirectoryTree::hasChildren() follow symlinks */
#define SPL_FILE_DIR_KEY_MODE_MASK         0x00000F00 /* mask RecursiveDirectoryTree::key() */
#define SPL_FILE_DIR_KEY(intern,mode)      ((intern->flags&SPL_FILE_DIR_KEY_MODE_MASK)==mode)

#define SPL_FILE_DIR_SKIPDOTS              0x00001000 /* Tells whether it should skip dots or not */
#define SPL_FILE_DIR_UNIXPATHS             0x00002000 /* Whether to unixify path separators */
#define SPL_FILE_DIR_OTHERS_MASK           0x00003000 /* mask used for get/setFlags */

#endif /* SPL_DIRECTORY_H */

