/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_FUNCTIONS_H
#define HYSS_FUNCTIONS_H

#include "hyss.h"

typedef gear_object* (*create_object_func_t)(gear_class_entry *class_type);

#define REGISTER_SPL_STD_CLASS(class_name, obj_ctor) \
	spl_register_std_class(&spl_ce_ ## class_name, # class_name, obj_ctor, NULL);

#define REGISTER_SPL_STD_CLASS_EX(class_name, obj_ctor, funcs) \
	spl_register_std_class(&spl_ce_ ## class_name, # class_name, obj_ctor, funcs);

#define REGISTER_SPL_SUB_CLASS_EX(class_name, parent_class_name, obj_ctor, funcs) \
	spl_register_sub_class(&spl_ce_ ## class_name, spl_ce_ ## parent_class_name, # class_name, obj_ctor, funcs);

#define REGISTER_SPL_INTERFACE(class_name) \
	spl_register_interface(&spl_ce_ ## class_name, # class_name, spl_funcs_ ## class_name);

#define REGISTER_SPL_IMPLEMENTS(class_name, interface_name) \
	gear_class_implements(spl_ce_ ## class_name, 1, spl_ce_ ## interface_name);

#define REGISTER_SPL_ITERATOR(class_name) \
	gear_class_implements(spl_ce_ ## class_name, 1, gear_ce_iterator);

#define REGISTER_SPL_PROPERTY(class_name, prop_name, prop_flags) \
	spl_register_property(spl_ce_ ## class_name, prop_name, sizeof(prop_name)-1, prop_flags);

#define REGISTER_SPL_CLASS_CONST_LONG(class_name, const_name, value) \
	gear_declare_class_constant_long(spl_ce_ ## class_name, const_name, sizeof(const_name)-1, (gear_long)value);

void spl_register_std_class(gear_class_entry ** ppce, char * class_name, create_object_func_t ctor, const gear_function_entry * function_list);
void spl_register_sub_class(gear_class_entry ** ppce, gear_class_entry * parent_ce, char * class_name, create_object_func_t ctor, const gear_function_entry * function_list);
void spl_register_interface(gear_class_entry ** ppce, char * class_name, const gear_function_entry *functions);

void spl_register_property( gear_class_entry * class_entry, char *prop_name, int prop_name_len, int prop_flags);

/* sub: whether to allow subclasses/interfaces
   allow = 0: allow all classes and interfaces
   allow > 0: allow all that match and mask ce_flags
   allow < 0: disallow all that match and mask ce_flags
 */
void spl_add_class_name(zval * list, gear_class_entry * pce, int allow, int ce_flags);
void spl_add_interfaces(zval * list, gear_class_entry * pce, int allow, int ce_flags);
void spl_add_traits(zval * list, gear_class_entry * pce, int allow, int ce_flags);
int spl_add_classes(gear_class_entry *pce, zval *list, int sub, int allow, int ce_flags);

/* caller must efree(return) */
gear_string *spl_gen_private_prop_name(gear_class_entry *ce, char *prop_name, int prop_len);

#define SPL_ME(class_name, function_name, arg_info, flags) \
	HYSS_ME( spl_ ## class_name, function_name, arg_info, flags)

#define SPL_ABSTRACT_ME(class_name, function_name, arg_info) \
	GEAR_ABSTRACT_ME( spl_ ## class_name, function_name, arg_info)

#define SPL_METHOD(class_name, function_name) \
	HYSS_METHOD(spl_ ## class_name, function_name)

#define SPL_MA(class_name, function_name, alias_class, alias_function, arg_info, flags) \
	HYSS_MALIAS(spl_ ## alias_class, function_name, alias_function, arg_info, flags)
#endif /* HYSS_FUNCTIONS_H */

