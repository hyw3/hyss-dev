/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPL_ENGINE_H
#define SPL_ENGINE_H

#include "hyss.h"
#include "hyss_spl.h"
#include "gear_interfaces.h"

HYSSAPI void spl_instantiate(gear_class_entry *pce, zval *object);

HYSSAPI gear_long spl_offset_convert_to_long(zval *offset);

/* {{{ spl_instantiate_arg_ex1 */
static inline int spl_instantiate_arg_ex1(gear_class_entry *pce, zval *retval, zval *arg1)
{
	gear_function *func = pce->constructor;
	spl_instantiate(pce, retval);

	gear_call_method(retval, pce, &func, ZSTR_VAL(func->common.function_name), ZSTR_LEN(func->common.function_name), NULL, 1, arg1, NULL);
	return 0;
}
/* }}} */

/* {{{ spl_instantiate_arg_ex2 */
static inline int spl_instantiate_arg_ex2(gear_class_entry *pce, zval *retval, zval *arg1, zval *arg2)
{
	gear_function *func = pce->constructor;
	spl_instantiate(pce, retval);

	gear_call_method(retval, pce, &func, ZSTR_VAL(func->common.function_name), ZSTR_LEN(func->common.function_name), NULL, 2, arg1, arg2);
	return 0;
}
/* }}} */

/* {{{ spl_instantiate_arg_n */
static inline void spl_instantiate_arg_n(gear_class_entry *pce, zval *retval, int argc, zval *argv)
{
	gear_function *func = pce->constructor;
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;
	zval dummy;

	spl_instantiate(pce, retval);

	fci.size = sizeof(gear_fcall_info);
	ZVAL_STR(&fci.function_name, func->common.function_name);
	fci.object = Z_OBJ_P(retval);
	fci.retval = &dummy;
	fci.param_count = argc;
	fci.params = argv;
	fci.no_separation = 1;

	fcc.function_handler = func;
	fcc.called_scope = pce;
	fcc.object = Z_OBJ_P(retval);

	gear_call_function(&fci, &fcc);
}
/* }}} */

#endif /* SPL_ENGINE_H */

