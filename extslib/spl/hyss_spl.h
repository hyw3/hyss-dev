/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SPL_H
#define HYSS_SPL_H

#include "hyss.h"
#include <stdarg.h>

#define HYSS_SPL_VERSION HYSS_VERSION

extern gear_capi_entry spl_capi_entry;
#define hyssext_spl_ptr &spl_capi_entry

#ifdef HYSS_WIN32
#	ifdef SPL_EXPORTS
#		define SPL_API __declspec(dllexport)
#	elif defined(COMPILE_DL_SPL)
#		define SPL_API __declspec(dllimport)
#	else
#		define SPL_API /* nothing */
#	endif
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define SPL_API __attribute__ ((visibility("default")))
#else
#	define SPL_API
#endif

#if defined(HYSS_WIN32) && !defined(COMPILE_DL_SPL)
#undef hyssext_spl
#define hyssext_spl NULL
#endif

HYSS_MINIT_FUNCTION(spl);
HYSS_MSHUTDOWN_FUNCTION(spl);
HYSS_RINIT_FUNCTION(spl);
HYSS_RSHUTDOWN_FUNCTION(spl);
HYSS_MINFO_FUNCTION(spl);


GEAR_BEGIN_CAPI_GLOBALS(spl)
	gear_string *autoload_extensions;
	HashTable   *autoload_functions;
	intptr_t     hash_mask_handle;
	intptr_t     hash_mask_handlers;
	int          hash_mask_init;
	int          autoload_running;
GEAR_END_CAPI_GLOBALS(spl)

GEAR_EXTERN_CAPI_GLOBALS(spl)
#define SPL_G(v) GEAR_CAPI_GLOBALS_ACCESSOR(spl, v)

HYSS_FUNCTION(spl_classes);
HYSS_FUNCTION(class_parents);
HYSS_FUNCTION(class_implements);
HYSS_FUNCTION(class_uses);

HYSSAPI gear_string *hyss_spl_object_hash(zval *obj);

#endif /* HYSS_SPL_H */

