/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "gear_interfaces.h"
#include "gear_exceptions.h"

#include "hyss_spl.h"
#include "spl_functions.h"
#include "spl_engine.h"
#include "spl_exceptions.h"

HYSSAPI gear_class_entry *spl_ce_LogicException;
HYSSAPI gear_class_entry *spl_ce_BadFunctionCallException;
HYSSAPI gear_class_entry *spl_ce_BadMethodCallException;
HYSSAPI gear_class_entry *spl_ce_DomainException;
HYSSAPI gear_class_entry *spl_ce_InvalidArgumentException;
HYSSAPI gear_class_entry *spl_ce_LengthException;
HYSSAPI gear_class_entry *spl_ce_OutOfRangeException;
HYSSAPI gear_class_entry *spl_ce_RuntimeException;
HYSSAPI gear_class_entry *spl_ce_OutOfBoundsException;
HYSSAPI gear_class_entry *spl_ce_OverflowException;
HYSSAPI gear_class_entry *spl_ce_RangeException;
HYSSAPI gear_class_entry *spl_ce_UnderflowException;
HYSSAPI gear_class_entry *spl_ce_UnexpectedValueException;

#define spl_ce_Exception gear_ce_exception

/* {{{ HYSS_MINIT_FUNCTION(spl_exceptions) */
HYSS_MINIT_FUNCTION(spl_exceptions)
{
    REGISTER_SPL_SUB_CLASS_EX(LogicException,           Exception,        NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(BadFunctionCallException, LogicException,   NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(BadMethodCallException,   BadFunctionCallException,   NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(DomainException,          LogicException,   NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(InvalidArgumentException, LogicException,   NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(LengthException,          LogicException,   NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(OutOfRangeException,      LogicException,   NULL, NULL);

    REGISTER_SPL_SUB_CLASS_EX(RuntimeException,         Exception,        NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(OutOfBoundsException,     RuntimeException, NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(OverflowException,        RuntimeException, NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(RangeException,           RuntimeException, NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(UnderflowException,       RuntimeException, NULL, NULL);
    REGISTER_SPL_SUB_CLASS_EX(UnexpectedValueException, RuntimeException, NULL, NULL);

	return SUCCESS;
}
/* }}} */

