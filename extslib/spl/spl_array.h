/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPL_ARRAY_H
#define SPL_ARRAY_H

#include "hyss.h"
#include "hyss_spl.h"
#include "spl_iterators.h"

extern HYSSAPI gear_class_entry *spl_ce_ArrayObject;
extern HYSSAPI gear_class_entry *spl_ce_ArrayIterator;
extern HYSSAPI gear_class_entry *spl_ce_RecursiveArrayIterator;

HYSS_MINIT_FUNCTION(spl_array);

extern void spl_array_iterator_append(zval *object, zval *append_value);
extern void spl_array_iterator_key(zval *object, zval *return_value);

#endif /* SPL_ARRAY_H */

