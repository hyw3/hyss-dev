@!hyss

/** @file cachingrecursiveiterator.inc
 * @ingroup Examples
 * @brief class CachingRecursiveIterator
 *
 * SPL - Standard HYSS Library
 */

/** @ingroup Examples
 * @brief   Compatibility to HYSS
 * @deprecated
 *
 * Class RecursiveCachingIterator was named CachingRecursiveIterator
 *
 * @see RecursiveCachingIterator
 */

class CachingRecursiveIterator extends RecursiveCachingIterator
{
}

!@
