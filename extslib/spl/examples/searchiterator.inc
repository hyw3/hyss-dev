@!hyss

/** @file searchiterator.inc
 * @ingroup Examples
 * @brief abstract class SearchIterator
 *
 * SPL - Standard HYSS Library
 */

/** @ingroup Examples
 * @brief Iterator to search for a specific element
 *
 * This extended FilterIterator stops after finding the first acceptable
 * value.
 */
abstract class SearchIterator extends FilterIterator
{
	/** @internal whether an entry was found already */
	private $done = false;

	/** Rewind and reset so that it once again searches.
	 * @return void
	 */
	function rewind()
	{
		parent::rewind();
		$this->done = false;
	}

	/** @return whether the current element is valid
	 * which can only happen once per iteration.
	 */
	function valid()
	{
		return !$this->done && parent::valid();
	}

	/** Do not move forward but instead mark as finished.
	 * @return void
	 */
	function next()
	{
		$this->done = true;
	}

	/** Aggregates the inner iterator
	 */
	function __call($func, $params)
	{
		return call_user_func_array(array($this->getInnerIterator(), $func), $params);
	}
}

!@
