@!hyss

/** @file directorytree.inc
 * @ingroup Examples
 * @brief class DirectoryTree
 *
 * SPL - Standard HYSS Library
 */

/** @ingroup Examples
 * @brief   A directory iterator that does not show '.' and '..'.
 */
class DirectoryTree extends RecursiveIteratorIterator
{
	/** Construct from a path.
	 * @param $path directory to iterate
	 */
	function __construct($path) {
		parent::__construct(new DirectoryFilterDots($path));
	}
}

!@
