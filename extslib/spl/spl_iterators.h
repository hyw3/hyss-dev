/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPL_ITERATORS_H
#define SPL_ITERATORS_H

#include "hyss.h"
#include "hyss_spl.h"
#if HAVE_PCRE || HAVE_BUNDLED_PCRE
#include "extslib/pcre/hyss_pcre.h"
#endif

#define spl_ce_Traversable   gear_ce_traversable
#define spl_ce_Iterator      gear_ce_iterator
#define spl_ce_Aggregate     gear_ce_aggregate
#define spl_ce_ArrayAccess   gear_ce_arrayaccess
#define spl_ce_Serializable  gear_ce_serializable
#define spl_ce_Countable     gear_ce_countable

extern HYSSAPI gear_class_entry *spl_ce_RecursiveIterator;
extern HYSSAPI gear_class_entry *spl_ce_RecursiveIteratorIterator;
extern HYSSAPI gear_class_entry *spl_ce_RecursiveTreeIterator;
extern HYSSAPI gear_class_entry *spl_ce_FilterIterator;
extern HYSSAPI gear_class_entry *spl_ce_RecursiveFilterIterator;
extern HYSSAPI gear_class_entry *spl_ce_ParentIterator;
extern HYSSAPI gear_class_entry *spl_ce_SeekableIterator;
extern HYSSAPI gear_class_entry *spl_ce_LimitIterator;
extern HYSSAPI gear_class_entry *spl_ce_CachingIterator;
extern HYSSAPI gear_class_entry *spl_ce_RecursiveCachingIterator;
extern HYSSAPI gear_class_entry *spl_ce_OuterIterator;
extern HYSSAPI gear_class_entry *spl_ce_IteratorIterator;
extern HYSSAPI gear_class_entry *spl_ce_NoRewindIterator;
extern HYSSAPI gear_class_entry *spl_ce_InfiniteIterator;
extern HYSSAPI gear_class_entry *spl_ce_EmptyIterator;
extern HYSSAPI gear_class_entry *spl_ce_AppendIterator;
extern HYSSAPI gear_class_entry *spl_ce_RegexIterator;
extern HYSSAPI gear_class_entry *spl_ce_RecursiveRegexIterator;
extern HYSSAPI gear_class_entry *spl_ce_CallbackFilterIterator;
extern HYSSAPI gear_class_entry *spl_ce_RecursiveCallbackFilterIterator;

HYSS_MINIT_FUNCTION(spl_iterators);

HYSS_FUNCTION(iterator_to_array);
HYSS_FUNCTION(iterator_count);
HYSS_FUNCTION(iterator_apply);

typedef enum {
	DIT_Default = 0,
	DIT_FilterIterator = DIT_Default,
	DIT_RecursiveFilterIterator = DIT_Default,
	DIT_ParentIterator = DIT_Default,
	DIT_LimitIterator,
	DIT_CachingIterator,
	DIT_RecursiveCachingIterator,
	DIT_IteratorIterator,
	DIT_NoRewindIterator,
	DIT_InfiniteIterator,
	DIT_AppendIterator,
#if HAVE_PCRE || HAVE_BUNDLED_PCRE
	DIT_RegexIterator,
	DIT_RecursiveRegexIterator,
#endif
	DIT_CallbackFilterIterator,
	DIT_RecursiveCallbackFilterIterator,
	DIT_Unknown = ~0
} dual_it_type;

typedef enum {
	RIT_Default = 0,
	RIT_RecursiveIteratorIterator = RIT_Default,
	RIT_RecursiveTreeIterator,
	RIT_Unknow = ~0
} recursive_it_it_type;

enum {
	/* public */
	CIT_CALL_TOSTRING        = 0x00000001,
	CIT_TOSTRING_USE_KEY     = 0x00000002,
	CIT_TOSTRING_USE_CURRENT = 0x00000004,
	CIT_TOSTRING_USE_INNER   = 0x00000008,
	CIT_CATCH_GET_CHILD      = 0x00000010,
	CIT_FULL_CACHE           = 0x00000100,
	CIT_PUBLIC               = 0x0000FFFF,
	/* private */
	CIT_VALID                = 0x00010000,
	CIT_HAS_CHILDREN         = 0x00020000
};

enum {
	/* public */
	REGIT_USE_KEY            = 0x00000001,
	REGIT_INVERTED           = 0x00000002
};

typedef enum {
	REGIT_MODE_MATCH,
	REGIT_MODE_GET_MATCH,
	REGIT_MODE_ALL_MATCHES,
	REGIT_MODE_SPLIT,
	REGIT_MODE_REPLACE,
	REGIT_MODE_MAX
} regex_mode;

typedef struct _spl_cbfilter_it_intern {
	gear_fcall_info       fci;
	gear_fcall_info_cache fcc;
	gear_object           *object;
} _spl_cbfilter_it_intern;

typedef struct _spl_dual_it_object {
	struct {
		zval                 zobject;
		gear_class_entry     *ce;
		gear_object          *object;
		gear_object_iterator *iterator;
	} inner;
	struct {
		zval                 data;
		zval                 key;
		gear_long            pos;
	} current;
	dual_it_type             dit_type;
	union {
		struct {
			gear_long             offset;
			gear_long             count;
		} limit;
		struct {
			gear_long             flags; /* CIT_* */
			zval             zstr;
			zval             zchildren;
			zval             zcache;
		} caching;
		struct {
			zval                  zarrayit;
			gear_object_iterator *iterator;
		} append;
#if HAVE_PCRE || HAVE_BUNDLED_PCRE
		struct {
			gear_long        flags;
			gear_long        preg_flags;
			pcre_cache_entry *pce;
			gear_string      *regex;
			regex_mode       mode;
			int              use_flags;
		} regex;
#endif
		_spl_cbfilter_it_intern *cbfilter;
	} u;
	gear_object              std;
} spl_dual_it_object;

static inline spl_dual_it_object *spl_dual_it_from_obj(gear_object *obj) /* {{{ */ {
	return (spl_dual_it_object*)((char*)(obj) - XtOffsetOf(spl_dual_it_object, std));
} /* }}} */

#define Z_SPLDUAL_IT_P(zv)  spl_dual_it_from_obj(Z_OBJ_P((zv)))

typedef int (*spl_iterator_apply_func_t)(gear_object_iterator *iter, void *puser);

HYSSAPI int spl_iterator_apply(zval *obj, spl_iterator_apply_func_t apply_func, void *puser);

#endif /* SPL_ITERATORS_H */

