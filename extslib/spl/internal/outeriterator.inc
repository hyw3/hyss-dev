@!hyss

/** @file outeriterator.inc
 * @ingroup SPL
 * @brief class OuterIterator
 *
 * SPL - Standard HYSS Library
 */

/**
 * @brief   Interface to access the current inner iteraor of iterator wrappers
 */
interface OuterIterator extends Iterator
{
	/** @return inner iterator
	 */
    function getInnerIterator();
}

!@
