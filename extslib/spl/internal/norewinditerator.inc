@!hyss

/** @file norewinditerator.inc
 * @ingroup SPL
 * @brief class NoRewindIterator
 *
 * SPL - Standard HYSS Library
 */

/** @ingroup SPL
 * @brief   An Iterator wrapper that doesn't call rewind
 */
class NoRewindIterator extends IteratorIterator
{
	/** Simply prevent execution of inner iterators rewind().
	 */
	function rewind()
	{
		// nothing to do
	}
}

!@
