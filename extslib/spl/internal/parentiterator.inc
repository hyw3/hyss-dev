@!hyss

/** @file parentiterator.inc
 * @ingroup SPL
 * @brief class FilterIterator
 *
 * SPL - Standard HYSS Library
 */

/**
 * @brief   Iterator to filter parents
 *
 * This extended FilterIterator allows a recursive iteration using
 * RecursiveIteratorIterator that only shows those elements which have
 * children.
 */
class ParentIterator extends RecursiveFilterIterator
{
	/** @return whetehr the current element has children
	 */
	function accept()
	{
		return $this->it->hasChildren();
	}
}

!@
