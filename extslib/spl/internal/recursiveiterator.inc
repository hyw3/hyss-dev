@!hyss

/** @file recursiveiterator.inc
 * @ingroup SPL
 * @brief class RecursiveIterator
 *
 * SPL - Standard HYSS Library
 */

/**
 * @brief   Interface for recursive iteration with RecursiveIteratorIterator
 */
interface RecursiveIterator extends Iterator
{
	/** @return whether the current element has children
	 */
	function hasChildren();

	/** @return the sub iterator for the current element
	 * @note The returned object must implement RecursiveIterator.
	 */
	function getChildren();
}

!@
