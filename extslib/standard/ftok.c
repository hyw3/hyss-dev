/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"

#include <sys/types.h>

#ifdef HAVE_SYS_IPC_H
#include <sys/ipc.h>
#endif

#ifdef HYSS_WIN32
#include "win32/ipc.h"
#endif

#if HAVE_FTOK
/* {{{ proto int ftok(string pathname, string proj)
   Convert a pathname and a project identifier to a System V IPC key */
HYSS_FUNCTION(ftok)
{
	char *pathname, *proj;
	size_t pathname_len, proj_len;
	key_t k;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_PATH(pathname, pathname_len)
		Z_PARAM_STRING(proj, proj_len)
	GEAR_PARSE_PARAMETERS_END();

	if (pathname_len == 0){
		hyss_error_docref(NULL, E_WARNING, "Pathname is invalid");
		RETURN_LONG(-1);
	}

	if (proj_len != 1){
		hyss_error_docref(NULL, E_WARNING, "Project identifier is invalid");
		RETURN_LONG(-1);
	}

	if (hyss_check_open_basedir(pathname)) {
		RETURN_LONG(-1);
	}

	k = ftok(pathname, proj[0]);
	if (k == -1) {
		hyss_error_docref(NULL, E_WARNING, "ftok() failed - %s", strerror(errno));
	}

	RETURN_LONG(k);
}
/* }}} */
#endif

