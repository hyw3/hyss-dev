/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_RANDOM_H
#define HYSS_RANDOM_H

HYSS_FUNCTION(random_bytes);
HYSS_FUNCTION(random_int);

HYSS_MINIT_FUNCTION(random);
HYSS_MSHUTDOWN_FUNCTION(random);

typedef struct {
	int fd;
} hyss_random_globals;

#define hyss_random_bytes_throw(b, s) hyss_random_bytes((b), (s), 1)
#define hyss_random_bytes_silent(b, s) hyss_random_bytes((b), (s), 0)

#define hyss_random_int_throw(min, max, result) \
	hyss_random_int((min), (max), (result), 1)
#define hyss_random_int_silent(min, max, result) \
	hyss_random_int((min), (max), (result), 0)

HYSSAPI int hyss_random_bytes(void *bytes, size_t size, gear_bool should_throw);
HYSSAPI int hyss_random_int(gear_long min, gear_long max, gear_long *result, gear_bool should_throw);

#ifdef ZTS
# define RANDOM_G(v) GEAR_PBCG(random_globals_id, hyss_random_globals *, v)
extern HYSSAPI int random_globals_id;
#else
# define RANDOM_G(v) random_globals.v
extern HYSSAPI hyss_random_globals random_globals;
#endif

#endif

