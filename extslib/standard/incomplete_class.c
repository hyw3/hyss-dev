/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "basic_functions.h"
#include "hyss_incomplete_class.h"

#define INCOMPLETE_CLASS_MSG \
		"The script tried to execute a method or "  \
		"access a property of an incomplete object. " \
		"Please ensure that the class definition \"%s\" of the object " \
		"you are trying to operate on was loaded _before_ " \
		"unserialize() gets called or provide an autoloader " \
		"to load the class definition"

static gear_object_handlers hyss_incomplete_object_handlers;

/* {{{ incomplete_class_message
 */
static void incomplete_class_message(zval *object, int error_type)
{
	gear_string *class_name;

	class_name = hyss_lookup_class_name(object);

	if (class_name) {
		hyss_error_docref(NULL, error_type, INCOMPLETE_CLASS_MSG, ZSTR_VAL(class_name));
		gear_string_release_ex(class_name, 0);
	} else {
		hyss_error_docref(NULL, error_type, INCOMPLETE_CLASS_MSG, "unknown");
	}
}
/* }}} */

static zval *incomplete_class_get_property(zval *object, zval *member, int type, void **cache_slot, zval *rv) /* {{{ */
{
	incomplete_class_message(object, E_NOTICE);

	if (type == BP_VAR_W || type == BP_VAR_RW) {
		ZVAL_ERROR(rv);
		return rv;
	} else {
		return &EG(uninitialized_zval);
	}
}
/* }}} */

static void incomplete_class_write_property(zval *object, zval *member, zval *value, void **cache_slot) /* {{{ */
{
	incomplete_class_message(object, E_NOTICE);
}
/* }}} */

static zval *incomplete_class_get_property_ptr_ptr(zval *object, zval *member, int type, void **cache_slot) /* {{{ */
{
	incomplete_class_message(object, E_NOTICE);
	return &EG(error_zval);
}
/* }}} */

static void incomplete_class_unset_property(zval *object, zval *member, void **cache_slot) /* {{{ */
{
	incomplete_class_message(object, E_NOTICE);
}
/* }}} */

static int incomplete_class_has_property(zval *object, zval *member, int check_empty, void **cache_slot) /* {{{ */
{
	incomplete_class_message(object, E_NOTICE);
	return 0;
}
/* }}} */

static union _gear_function *incomplete_class_get_method(gear_object **object, gear_string *method, const zval *key) /* {{{ */
{
	zval zobject;

	ZVAL_OBJ(&zobject, *object);
	incomplete_class_message(&zobject, E_ERROR);
	return NULL;
}
/* }}} */

/* {{{ hyss_create_incomplete_class
 */
static gear_object *hyss_create_incomplete_object(gear_class_entry *class_type)
{
	gear_object *object;

	object = gear_objects_new( class_type);
	object->handlers = &hyss_incomplete_object_handlers;

	object_properties_init(object, class_type);

	return object;
}

HYSSAPI gear_class_entry *hyss_create_incomplete_class(void)
{
	gear_class_entry incomplete_class;

	INIT_CLASS_ENTRY(incomplete_class, INCOMPLETE_CLASS, NULL);
	incomplete_class.create_object = hyss_create_incomplete_object;

	memcpy(&hyss_incomplete_object_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	hyss_incomplete_object_handlers.read_property = incomplete_class_get_property;
	hyss_incomplete_object_handlers.has_property = incomplete_class_has_property;
	hyss_incomplete_object_handlers.unset_property = incomplete_class_unset_property;
	hyss_incomplete_object_handlers.write_property = incomplete_class_write_property;
	hyss_incomplete_object_handlers.get_property_ptr_ptr = incomplete_class_get_property_ptr_ptr;
    hyss_incomplete_object_handlers.get_method = incomplete_class_get_method;

	return gear_register_internal_class(&incomplete_class);
}
/* }}} */

/* {{{ hyss_lookup_class_name
 */
HYSSAPI gear_string *hyss_lookup_class_name(zval *object)
{
	zval *val;
	HashTable *object_properties;

	object_properties = Z_OBJPROP_P(object);

	if ((val = gear_hash_str_find(object_properties, MAGIC_MEMBER, sizeof(MAGIC_MEMBER)-1)) != NULL && Z_TYPE_P(val) == IS_STRING) {
		return gear_string_copy(Z_STR_P(val));
	}

	return NULL;
}
/* }}} */

/* {{{ hyss_store_class_name
 */
HYSSAPI void hyss_store_class_name(zval *object, const char *name, size_t len)
{
	zval val;


	ZVAL_STRINGL(&val, name, len);
	gear_hash_str_update(Z_OBJPROP_P(object), MAGIC_MEMBER, sizeof(MAGIC_MEMBER)-1, &val);
}
/* }}} */

