/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes */
#include "hyss.h"
#include "hyss_assert.h"
#include "hyss_ics.h"
#include "gear_exceptions.h"
/* }}} */

GEAR_BEGIN_CAPI_GLOBALS(assert)
	zval callback;
	char *cb;
	gear_bool active;
	gear_bool bail;
	gear_bool warning;
	gear_bool quiet_eval;
	gear_bool exception;
GEAR_END_CAPI_GLOBALS(assert)

GEAR_DECLARE_CAPI_GLOBALS(assert)

static gear_class_entry *assertion_error_ce;

#define ASSERTG(v) GEAR_CAPI_GLOBALS_ACCESSOR(assert, v)

#define SAFE_STRING(s) ((s)?(s):"")

enum {
	ASSERT_ACTIVE=1,
	ASSERT_CALLBACK,
	ASSERT_BAIL,
	ASSERT_WARNING,
	ASSERT_QUIET_EVAL,
	ASSERT_EXCEPTION
};

static HYSS_ICS_MH(OnChangeCallback) /* {{{ */
{
	if (EG(current_execute_data)) {
		if (Z_TYPE(ASSERTG(callback)) != IS_UNDEF) {
			zval_ptr_dtor(&ASSERTG(callback));
			ZVAL_UNDEF(&ASSERTG(callback));
		}
		if (new_value && (Z_TYPE(ASSERTG(callback)) != IS_UNDEF || ZSTR_LEN(new_value))) {
			ZVAL_STR_COPY(&ASSERTG(callback), new_value);
		}
	} else {
		if (ASSERTG(cb)) {
			pefree(ASSERTG(cb), 1);
		}
		if (new_value && ZSTR_LEN(new_value)) {
			ASSERTG(cb) = pemalloc(ZSTR_LEN(new_value) + 1, 1);
			memcpy(ASSERTG(cb), ZSTR_VAL(new_value), ZSTR_LEN(new_value));
			ASSERTG(cb)[ZSTR_LEN(new_value)] = '\0';
		} else {
			ASSERTG(cb) = NULL;
		}
	}
	return SUCCESS;
}
/* }}} */

HYSS_ICS_BEGIN()
	 STD_HYSS_ICS_ENTRY("assert.active",		"1",	HYSS_ICS_ALL,	OnUpdateBool,		active,	 			gear_assert_globals,		assert_globals)
	 STD_HYSS_ICS_ENTRY("assert.bail",		"0",	HYSS_ICS_ALL,	OnUpdateBool,		bail,	 			gear_assert_globals,		assert_globals)
	 STD_HYSS_ICS_ENTRY("assert.warning",	"1",	HYSS_ICS_ALL,	OnUpdateBool,		warning, 			gear_assert_globals,		assert_globals)
	 HYSS_ICS_ENTRY("assert.callback",		NULL,	HYSS_ICS_ALL,	OnChangeCallback)
	 STD_HYSS_ICS_ENTRY("assert.quiet_eval", "0",	HYSS_ICS_ALL,	OnUpdateBool,		quiet_eval,		 	gear_assert_globals,		assert_globals)
	 STD_HYSS_ICS_ENTRY("assert.exception",	"0",	HYSS_ICS_ALL,	OnUpdateBool,		exception, 			gear_assert_globals,		assert_globals)
HYSS_ICS_END()

static void hyss_assert_init_globals(gear_assert_globals *assert_globals_p) /* {{{ */
{
	ZVAL_UNDEF(&assert_globals_p->callback);
	assert_globals_p->cb = NULL;
}
/* }}} */

HYSS_MINIT_FUNCTION(assert) /* {{{ */
{
	gear_class_entry ce;

	GEAR_INIT_CAPI_GLOBALS(assert, hyss_assert_init_globals, NULL);

	REGISTER_ICS_ENTRIES();

	REGISTER_LONG_CONSTANT("ASSERT_ACTIVE", ASSERT_ACTIVE, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ASSERT_CALLBACK", ASSERT_CALLBACK, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ASSERT_BAIL", ASSERT_BAIL, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ASSERT_WARNING", ASSERT_WARNING, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ASSERT_QUIET_EVAL", ASSERT_QUIET_EVAL, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ASSERT_EXCEPTION", ASSERT_EXCEPTION, CONST_CS|CONST_PERSISTENT);

	INIT_CLASS_ENTRY(ce, "AssertionError", NULL);
	assertion_error_ce = gear_register_internal_class_ex(&ce, gear_ce_error);

	return SUCCESS;
}
/* }}} */

HYSS_MSHUTDOWN_FUNCTION(assert) /* {{{ */
{
	if (ASSERTG(cb)) {
		pefree(ASSERTG(cb), 1);
		ASSERTG(cb) = NULL;
	}
	return SUCCESS;
}
/* }}} */

HYSS_RSHUTDOWN_FUNCTION(assert) /* {{{ */
{
	if (Z_TYPE(ASSERTG(callback)) != IS_UNDEF) {
		zval_ptr_dtor(&ASSERTG(callback));
		ZVAL_UNDEF(&ASSERTG(callback));
	}

	return SUCCESS;
}
/* }}} */

HYSS_MINFO_FUNCTION(assert) /* {{{ */
{
	DISPLAY_ICS_ENTRIES();
}
/* }}} */

/* {{{ proto int assert(string|bool assertion[, mixed description])
   Checks if assertion is false */
HYSS_FUNCTION(assert)
{
	zval *assertion;
	zval *description = NULL;
	int val;
	char *myeval = NULL;
	char *compiled_string_description;

	if (! ASSERTG(active)) {
		RETURN_TRUE;
	}

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_ZVAL(assertion)
		Z_PARAM_OPTIONAL
		Z_PARAM_ZVAL(description)
	GEAR_PARSE_PARAMETERS_END();

	if (Z_TYPE_P(assertion) == IS_STRING) {
		zval retval;
		int old_error_reporting = 0; /* shut up gcc! */

		if (gear_forbid_dynamic_call("assert() with string argument") == FAILURE) {
			RETURN_FALSE;
		}

		hyss_error_docref(NULL, E_DEPRECATED, "Calling assert() with a string argument is deprecated");

		myeval = Z_STRVAL_P(assertion);

		if (ASSERTG(quiet_eval)) {
			old_error_reporting = EG(error_reporting);
			EG(error_reporting) = 0;
		}

		compiled_string_description = gear_make_compiled_string_description("assert code");
		if (gear_eval_stringl(myeval, Z_STRLEN_P(assertion), &retval, compiled_string_description) == FAILURE) {
			efree(compiled_string_description);
			if (!description) {
				gear_throw_error(NULL, "Failure evaluating code: %s%s", HYSS_EOL, myeval);
			} else {
				gear_string *str = zval_get_string(description);
				gear_throw_error(NULL, "Failure evaluating code: %s%s:\"%s\"", HYSS_EOL, ZSTR_VAL(str), myeval);
				gear_string_release_ex(str, 0);
			}
			if (ASSERTG(bail)) {
				gear_bailout();
			}
			RETURN_FALSE;
		}
		efree(compiled_string_description);

		if (ASSERTG(quiet_eval)) {
			EG(error_reporting) = old_error_reporting;
		}

		convert_to_boolean(&retval);
		val = Z_TYPE(retval) == IS_TRUE;
	} else {
		val = gear_is_true(assertion);
	}

	if (val) {
		RETURN_TRUE;
	}

	if (Z_TYPE(ASSERTG(callback)) == IS_UNDEF && ASSERTG(cb)) {
		ZVAL_STRING(&ASSERTG(callback), ASSERTG(cb));
	}

	if (Z_TYPE(ASSERTG(callback)) != IS_UNDEF) {
		zval args[4];
		zval retval;
		uint32_t lineno = gear_get_executed_lineno();
		const char *filename = gear_get_executed_filename();

		ZVAL_STRING(&args[0], SAFE_STRING(filename));
		ZVAL_LONG (&args[1], lineno);
		ZVAL_STRING(&args[2], SAFE_STRING(myeval));

		ZVAL_FALSE(&retval);

		/* XXX do we want to check for error here? */
		if (!description) {
			call_user_function(CG(function_table), NULL, &ASSERTG(callback), &retval, 3, args);
			zval_ptr_dtor(&(args[2]));
			zval_ptr_dtor(&(args[0]));
		} else {
			ZVAL_STR(&args[3], zval_get_string(description));
			call_user_function(CG(function_table), NULL, &ASSERTG(callback), &retval, 4, args);
			zval_ptr_dtor(&(args[3]));
			zval_ptr_dtor(&(args[2]));
			zval_ptr_dtor(&(args[0]));
		}

		zval_ptr_dtor(&retval);
	}

	if (ASSERTG(exception)) {
		if (!description) {
			gear_throw_exception(assertion_error_ce, NULL, E_ERROR);
		} else if (Z_TYPE_P(description) == IS_OBJECT &&
			instanceof_function(Z_OBJCE_P(description), gear_ce_throwable)) {
			Z_ADDREF_P(description);
			gear_throw_exception_object(description);
		} else {
			gear_string *str = zval_get_string(description);
			gear_throw_exception(assertion_error_ce, ZSTR_VAL(str), E_ERROR);
			gear_string_release_ex(str, 0);
		}
	} else if (ASSERTG(warning)) {
		if (!description) {
			if (myeval) {
				hyss_error_docref(NULL, E_WARNING, "Assertion \"%s\" failed", myeval);
			} else {
				hyss_error_docref(NULL, E_WARNING, "Assertion failed");
			}
		} else {
			gear_string *str = zval_get_string(description);
			if (myeval) {
				hyss_error_docref(NULL, E_WARNING, "%s: \"%s\" failed", ZSTR_VAL(str), myeval);
			} else {
				hyss_error_docref(NULL, E_WARNING, "%s failed", ZSTR_VAL(str));
			}
			gear_string_release_ex(str, 0);
		}
	}

	if (ASSERTG(bail)) {
		gear_bailout();
	}

	RETURN_FALSE;
}
/* }}} */

/* {{{ proto mixed assert_options(int what [, mixed value])
   Set/get the various assert flags */
HYSS_FUNCTION(assert_options)
{
	zval *value = NULL;
	gear_long what;
	gear_bool oldint;
	int ac = GEAR_NUM_ARGS();
	gear_string *key;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_LONG(what)
		Z_PARAM_OPTIONAL
		Z_PARAM_ZVAL(value)
	GEAR_PARSE_PARAMETERS_END();

	switch (what) {
	case ASSERT_ACTIVE:
		oldint = ASSERTG(active);
		if (ac == 2) {
			gear_string *value_str = zval_get_string(value);
			key = gear_string_init("assert.active", sizeof("assert.active")-1, 0);
			gear_alter_ics_entry_ex(key, value_str, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME, 0);
			gear_string_release_ex(key, 0);
			gear_string_release_ex(value_str, 0);
		}
		RETURN_LONG(oldint);
		break;

	case ASSERT_BAIL:
		oldint = ASSERTG(bail);
		if (ac == 2) {
			gear_string *value_str = zval_get_string(value);
			key = gear_string_init("assert.bail", sizeof("assert.bail")-1, 0);
			gear_alter_ics_entry_ex(key, value_str, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME, 0);
			gear_string_release_ex(key, 0);
			gear_string_release_ex(value_str, 0);
		}
		RETURN_LONG(oldint);
		break;

	case ASSERT_QUIET_EVAL:
		oldint = ASSERTG(quiet_eval);
		if (ac == 2) {
			gear_string *value_str = zval_get_string(value);
			key = gear_string_init("assert.quiet_eval", sizeof("assert.quiet_eval")-1, 0);
			gear_alter_ics_entry_ex(key, value_str, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME, 0);
			gear_string_release_ex(key, 0);
			gear_string_release_ex(value_str, 0);
		}
		RETURN_LONG(oldint);
		break;

	case ASSERT_WARNING:
		oldint = ASSERTG(warning);
		if (ac == 2) {
			gear_string *value_str = zval_get_string(value);
			key = gear_string_init("assert.warning", sizeof("assert.warning")-1, 0);
			gear_alter_ics_entry_ex(key, value_str, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME, 0);
			gear_string_release_ex(key, 0);
			gear_string_release_ex(value_str, 0);
		}
		RETURN_LONG(oldint);
		break;

	case ASSERT_CALLBACK:
		if (Z_TYPE(ASSERTG(callback)) != IS_UNDEF) {
			ZVAL_COPY(return_value, &ASSERTG(callback));
		} else if (ASSERTG(cb)) {
			RETVAL_STRING(ASSERTG(cb));
		} else {
			RETVAL_NULL();
		}
		if (ac == 2) {
			zval_ptr_dtor(&ASSERTG(callback));
			ZVAL_COPY(&ASSERTG(callback), value);
		}
		return;

	case ASSERT_EXCEPTION:
		oldint = ASSERTG(exception);
		if (ac == 2) {
			gear_string *key = gear_string_init("assert.exception", sizeof("assert.exception")-1, 0);
			gear_string *val = zval_get_string(value);
			gear_alter_ics_entry_ex(key, val, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME, 0);
			gear_string_release_ex(val, 0);
			gear_string_release_ex(key, 0);
		}
		RETURN_LONG(oldint);
		break;

	default:
		hyss_error_docref(NULL, E_WARNING, "Unknown value " GEAR_LONG_FMT, what);
		break;
	}

	RETURN_FALSE;
}
/* }}} */

