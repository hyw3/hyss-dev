/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SHA1_H
#define SHA1_H

#include "extslib/standard/basic_functions.h"

/* SHA1 context. */
typedef struct {
	uint32_t state[5];		/* state (ABCD) */
	uint32_t count[2];		/* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];	/* input buffer */
} HYSS_SHA1_CTX;

HYSSAPI void HYSS_SHA1Init(HYSS_SHA1_CTX *);
HYSSAPI void HYSS_SHA1Update(HYSS_SHA1_CTX *, const unsigned char *, size_t);
HYSSAPI void HYSS_SHA1Final(unsigned char[20], HYSS_SHA1_CTX *);
HYSSAPI void make_sha1_digest(char *sha1str, unsigned char *digest);

HYSS_FUNCTION(sha1);
HYSS_FUNCTION(sha1_file);

#endif
