/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_globals.h"
#include "extslib/standard/basic_functions.h"
#include "extslib/standard/file.h"

#define HYSS_STREAM_BRIGADE_RES_NAME	"userfilter.bucket brigade"
#define HYSS_STREAM_BUCKET_RES_NAME "userfilter.bucket"
#define HYSS_STREAM_FILTER_RES_NAME "userfilter.filter"

struct hyss_user_filter_data {
	gear_class_entry *ce;
	/* variable length; this *must* be last in the structure */
	gear_string *classname;
};

/* to provide context for calling into the next filter from user-space */
static int le_userfilters;
static int le_bucket_brigade;
static int le_bucket;

/* define the base filter class */

HYSS_FUNCTION(user_filter_nop)
{
}
GEAR_BEGIN_ARG_INFO(arginfo_hyss_user_filter_filter, 0)
	GEAR_ARG_INFO(0, in)
	GEAR_ARG_INFO(0, out)
	GEAR_ARG_INFO(1, consumed)
	GEAR_ARG_INFO(0, closing)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hyss_user_filter_onCreate, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hyss_user_filter_onClose, 0)
GEAR_END_ARG_INFO()

static const gear_function_entry user_filter_class_funcs[] = {
	HYSS_NAMED_FE(filter,	HYSS_FN(user_filter_nop),		arginfo_hyss_user_filter_filter)
	HYSS_NAMED_FE(onCreate,	HYSS_FN(user_filter_nop),		arginfo_hyss_user_filter_onCreate)
	HYSS_NAMED_FE(onClose,	HYSS_FN(user_filter_nop),		arginfo_hyss_user_filter_onClose)
	HYSS_FE_END
};

static gear_class_entry user_filter_class_entry;

static GEAR_RSRC_DTOR_FUNC(hyss_bucket_dtor)
{
	hyss_stream_bucket *bucket = (hyss_stream_bucket *)res->ptr;
	if (bucket) {
		hyss_stream_bucket_delref(bucket);
		bucket = NULL;
	}
}

HYSS_MINIT_FUNCTION(user_filters)
{
	gear_class_entry *hyss_user_filter;
	/* init the filter class ancestor */
	INIT_CLASS_ENTRY(user_filter_class_entry, "hyss_user_filter", user_filter_class_funcs);
	if ((hyss_user_filter = gear_register_internal_class(&user_filter_class_entry)) == NULL) {
		return FAILURE;
	}
	gear_declare_property_string(hyss_user_filter, "filtername", sizeof("filtername")-1, "", GEAR_ACC_PUBLIC);
	gear_declare_property_string(hyss_user_filter, "params", sizeof("params")-1, "", GEAR_ACC_PUBLIC);

	/* init the filter resource; it has no dtor, as streams will always clean it up
	 * at the correct time */
	le_userfilters = gear_register_list_destructors_ex(NULL, NULL, HYSS_STREAM_FILTER_RES_NAME, 0);

	if (le_userfilters == FAILURE) {
		return FAILURE;
	}

	/* Filters will dispose of their brigades */
	le_bucket_brigade = gear_register_list_destructors_ex(NULL, NULL, HYSS_STREAM_BRIGADE_RES_NAME, capi_number);
	/* Brigades will dispose of their buckets */
	le_bucket = gear_register_list_destructors_ex(hyss_bucket_dtor, NULL, HYSS_STREAM_BUCKET_RES_NAME, capi_number);

	if (le_bucket_brigade == FAILURE) {
		return FAILURE;
	}

	REGISTER_LONG_CONSTANT("PSFS_PASS_ON",			PSFS_PASS_ON,			CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("PSFS_FEED_ME",			PSFS_FEED_ME,			CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("PSFS_ERR_FATAL",		PSFS_ERR_FATAL,			CONST_CS | CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("PSFS_FLAG_NORMAL",		PSFS_FLAG_NORMAL,		CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("PSFS_FLAG_FLUSH_INC",	PSFS_FLAG_FLUSH_INC,	CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("PSFS_FLAG_FLUSH_CLOSE",	PSFS_FLAG_FLUSH_CLOSE,	CONST_CS | CONST_PERSISTENT);

	return SUCCESS;
}

HYSS_RSHUTDOWN_FUNCTION(user_filters)
{
	if (BG(user_filter_map)) {
		gear_hash_destroy(BG(user_filter_map));
		efree(BG(user_filter_map));
		BG(user_filter_map) = NULL;
	}

	return SUCCESS;
}

static void userfilter_dtor(hyss_stream_filter *thisfilter)
{
	zval *obj = &thisfilter->abstract;
	zval func_name;
	zval retval;

	if (obj == NULL) {
		/* If there's no object associated then there's nothing to dispose of */
		return;
	}

	ZVAL_STRINGL(&func_name, "onclose", sizeof("onclose")-1);

	call_user_function(NULL,
			obj,
			&func_name,
			&retval,
			0, NULL);

	zval_ptr_dtor(&retval);
	zval_ptr_dtor(&func_name);

	/* kill the object */
	zval_ptr_dtor(obj);
}

hyss_stream_filter_status_t userfilter_filter(
			hyss_stream *stream,
			hyss_stream_filter *thisfilter,
			hyss_stream_bucket_brigade *buckets_in,
			hyss_stream_bucket_brigade *buckets_out,
			size_t *bytes_consumed,
			int flags
			)
{
	int ret = PSFS_ERR_FATAL;
	zval *obj = &thisfilter->abstract;
	zval func_name;
	zval retval;
	zval args[4];
	zval zpropname;
	int call_result;

	/* the userfilter object probably doesn't exist anymore */
	if (CG(unclean_shutdown)) {
		return ret;
	}

	if (!gear_hash_str_exists_ind(Z_OBJPROP_P(obj), "stream", sizeof("stream")-1)) {
		zval tmp;

		/* Give the userfilter class a hook back to the stream */
		hyss_stream_to_zval(stream, &tmp);
		Z_ADDREF(tmp);
		add_property_zval(obj, "stream", &tmp);
		/* add_property_zval increments the refcount which is unwanted here */
		zval_ptr_dtor(&tmp);
	}

	ZVAL_STRINGL(&func_name, "filter", sizeof("filter")-1);

	/* Setup calling arguments */
	ZVAL_RES(&args[0], gear_register_resource(buckets_in, le_bucket_brigade));
	ZVAL_RES(&args[1], gear_register_resource(buckets_out, le_bucket_brigade));

	if (bytes_consumed) {
		ZVAL_LONG(&args[2], *bytes_consumed);
	} else {
		ZVAL_NULL(&args[2]);
	}

	ZVAL_BOOL(&args[3], flags & PSFS_FLAG_FLUSH_CLOSE);

	call_result = call_user_function_ex(NULL,
			obj,
			&func_name,
			&retval,
			4, args,
			0, NULL);

	zval_ptr_dtor(&func_name);

	if (call_result == SUCCESS && Z_TYPE(retval) != IS_UNDEF) {
		convert_to_long(&retval);
		ret = (int)Z_LVAL(retval);
	} else if (call_result == FAILURE) {
		hyss_error_docref(NULL, E_WARNING, "failed to call filter function");
	}

	if (bytes_consumed) {
		*bytes_consumed = zval_get_long(&args[2]);
	}

	if (buckets_in->head) {
		hyss_stream_bucket *bucket = buckets_in->head;

		hyss_error_docref(NULL, E_WARNING, "Unprocessed filter buckets remaining on input brigade");
		while ((bucket = buckets_in->head)) {
			/* Remove unconsumed buckets from the brigade */
			hyss_stream_bucket_unlink(bucket);
			hyss_stream_bucket_delref(bucket);
		}
	}
	if (ret != PSFS_PASS_ON) {
		hyss_stream_bucket *bucket = buckets_out->head;
		while (bucket != NULL) {
			hyss_stream_bucket_unlink(bucket);
			hyss_stream_bucket_delref(bucket);
			bucket = buckets_out->head;
		}
	}

	/* filter resources are cleaned up by the stream destructor,
	 * keeping a reference to the stream resource here would prevent it
	 * from being destroyed properly */
	ZVAL_STRINGL(&zpropname, "stream", sizeof("stream")-1);
	Z_OBJ_HANDLER_P(obj, unset_property)(obj, &zpropname, NULL);
	zval_ptr_dtor(&zpropname);

	zval_ptr_dtor(&args[3]);
	zval_ptr_dtor(&args[2]);
	zval_ptr_dtor(&args[1]);
	zval_ptr_dtor(&args[0]);

	return ret;
}

static const hyss_stream_filter_ops userfilter_ops = {
	userfilter_filter,
	userfilter_dtor,
	"user-filter"
};

static hyss_stream_filter *user_filter_factory_create(const char *filtername,
		zval *filterparams, uint8_t persistent)
{
	struct hyss_user_filter_data *fdat = NULL;
	hyss_stream_filter *filter;
	zval obj, zfilter;
	zval func_name;
	zval retval;
	size_t len;

	/* some sanity checks */
	if (persistent) {
		hyss_error_docref(NULL, E_WARNING,
				"cannot use a user-space filter with a persistent stream");
		return NULL;
	}

	len = strlen(filtername);

	/* determine the classname/class entry */
	if (NULL == (fdat = gear_hash_str_find_ptr(BG(user_filter_map), (char*)filtername, len))) {
		char *period;

		/* Userspace Filters using ambiguous wildcards could cause problems.
           i.e.: myfilter.foo.bar will always call into myfilter.foo.*
                 never seeing myfilter.*
           TODO: Allow failed userfilter creations to continue
                 scanning through the list */
		if ((period = strrchr(filtername, '.'))) {
			char *wildcard = safe_emalloc(len, 1, 3);

			/* Search for wildcard matches instead */
			memcpy(wildcard, filtername, len + 1); /* copy \0 */
			period = wildcard + (period - filtername);
			while (period) {
				*period = '\0';
				strncat(wildcard, ".*", 2);
				if (NULL != (fdat = gear_hash_str_find_ptr(BG(user_filter_map), wildcard, strlen(wildcard)))) {
					period = NULL;
				} else {
					*period = '\0';
					period = strrchr(wildcard, '.');
				}
			}
			efree(wildcard);
		}
		if (fdat == NULL) {
			hyss_error_docref(NULL, E_WARNING,
					"Err, filter \"%s\" is not in the user-filter map, but somehow the user-filter-factory was invoked for it!?", filtername);
			return NULL;
		}
	}

	/* bind the classname to the actual class */
	if (fdat->ce == NULL) {
		if (NULL == (fdat->ce = gear_lookup_class(fdat->classname))) {
			hyss_error_docref(NULL, E_WARNING,
					"user-filter \"%s\" requires class \"%s\", but that class is not defined",
					filtername, ZSTR_VAL(fdat->classname));
			return NULL;
		}
	}

	filter = hyss_stream_filter_alloc(&userfilter_ops, NULL, 0);
	if (filter == NULL) {
		return NULL;
	}

	/* create the object */
	object_init_ex(&obj, fdat->ce);

	/* filtername */
	add_property_string(&obj, "filtername", (char*)filtername);

	/* and the parameters, if any */
	if (filterparams) {
		add_property_zval(&obj, "params", filterparams);
	} else {
		add_property_null(&obj, "params");
	}

	/* invoke the constructor */
	ZVAL_STRINGL(&func_name, "oncreate", sizeof("oncreate")-1);

	call_user_function(NULL,
			&obj,
			&func_name,
			&retval,
			0, NULL);

	if (Z_TYPE(retval) != IS_UNDEF) {
		if (Z_TYPE(retval) == IS_FALSE) {
			/* User reported filter creation error "return false;" */
			zval_ptr_dtor(&retval);

			/* Kill the filter (safely) */
			ZVAL_UNDEF(&filter->abstract);
			hyss_stream_filter_free(filter);

			/* Kill the object */
			zval_ptr_dtor(&obj);

			/* Report failure to filter_alloc */
			return NULL;
		}
		zval_ptr_dtor(&retval);
	}
	zval_ptr_dtor(&func_name);

	/* set the filter property, this will be used during cleanup */
	ZVAL_RES(&zfilter, gear_register_resource(filter, le_userfilters));
	ZVAL_COPY_VALUE(&filter->abstract, &obj);
	add_property_zval(&obj, "filter", &zfilter);
	/* add_property_zval increments the refcount which is unwanted here */
	zval_ptr_dtor(&zfilter);

	return filter;
}

static const hyss_stream_filter_factory user_filter_factory = {
	user_filter_factory_create
};

static void filter_item_dtor(zval *zv)
{
	struct hyss_user_filter_data *fdat = Z_PTR_P(zv);
	gear_string_release_ex(fdat->classname, 0);
	efree(fdat);
}

/* {{{ proto object stream_bucket_make_writeable(resource brigade)
   Return a bucket object from the brigade for operating on */
HYSS_FUNCTION(stream_bucket_make_writeable)
{
	zval *zbrigade, zbucket;
	hyss_stream_bucket_brigade *brigade;
	hyss_stream_bucket *bucket;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_RESOURCE(zbrigade)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if ((brigade = (hyss_stream_bucket_brigade*)gear_fetch_resource(
					Z_RES_P(zbrigade), HYSS_STREAM_BRIGADE_RES_NAME, le_bucket_brigade)) == NULL) {
		RETURN_FALSE;
	}

	ZVAL_NULL(return_value);

	if (brigade->head && (bucket = hyss_stream_bucket_make_writeable(brigade->head))) {
		ZVAL_RES(&zbucket, gear_register_resource(bucket, le_bucket));
		object_init(return_value);
		add_property_zval(return_value, "bucket", &zbucket);
		/* add_property_zval increments the refcount which is unwanted here */
		zval_ptr_dtor(&zbucket);
		add_property_stringl(return_value, "data", bucket->buf, bucket->buflen);
		add_property_long(return_value, "datalen", bucket->buflen);
	}
}
/* }}} */

/* {{{ hyss_stream_bucket_attach */
static void hyss_stream_bucket_attach(int append, INTERNAL_FUNCTION_PARAMETERS)
{
	zval *zbrigade, *zobject;
	zval *pzbucket, *pzdata;
	hyss_stream_bucket_brigade *brigade;
	hyss_stream_bucket *bucket;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_RESOURCE(zbrigade)
		Z_PARAM_OBJECT(zobject)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (NULL == (pzbucket = gear_hash_str_find(Z_OBJPROP_P(zobject), "bucket", sizeof("bucket")-1))) {
		hyss_error_docref(NULL, E_WARNING, "Object has no bucket property");
		RETURN_FALSE;
	}

	if ((brigade = (hyss_stream_bucket_brigade*)gear_fetch_resource(
					Z_RES_P(zbrigade), HYSS_STREAM_BRIGADE_RES_NAME, le_bucket_brigade)) == NULL) {
		RETURN_FALSE;
	}

	if ((bucket = (hyss_stream_bucket *)gear_fetch_resource_ex(pzbucket, HYSS_STREAM_BUCKET_RES_NAME, le_bucket)) == NULL) {
		RETURN_FALSE;
	}

	if (NULL != (pzdata = gear_hash_str_find(Z_OBJPROP_P(zobject), "data", sizeof("data")-1)) && Z_TYPE_P(pzdata) == IS_STRING) {
		if (!bucket->own_buf) {
			bucket = hyss_stream_bucket_make_writeable(bucket);
		}
		if (bucket->buflen != Z_STRLEN_P(pzdata)) {
			bucket->buf = perealloc(bucket->buf, Z_STRLEN_P(pzdata), bucket->is_persistent);
			bucket->buflen = Z_STRLEN_P(pzdata);
		}
		memcpy(bucket->buf, Z_STRVAL_P(pzdata), bucket->buflen);
	}

	if (append) {
		hyss_stream_bucket_append(brigade, bucket);
	} else {
		hyss_stream_bucket_prepend(brigade, bucket);
	}
	/* This is a hack necessary to accommodate situations where bucket is appended to the stream
 	 * multiple times. See bug35916.hysst for reference.
	 */
	if (bucket->refcount == 1) {
		bucket->refcount++;
	}
}
/* }}} */

/* {{{ proto void stream_bucket_prepend(resource brigade, object bucket)
   Prepend bucket to brigade */
HYSS_FUNCTION(stream_bucket_prepend)
{
	hyss_stream_bucket_attach(0, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto void stream_bucket_append(resource brigade, object bucket)
   Append bucket to brigade */
HYSS_FUNCTION(stream_bucket_append)
{
	hyss_stream_bucket_attach(1, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto resource stream_bucket_new(resource stream, string buffer)
   Create a new bucket for use on the current stream */
HYSS_FUNCTION(stream_bucket_new)
{
	zval *zstream, zbucket;
	hyss_stream *stream;
	char *buffer;
	char *pbuffer;
	size_t buffer_len;
	hyss_stream_bucket *bucket;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_ZVAL(zstream)
		Z_PARAM_STRING(buffer, buffer_len)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	hyss_stream_from_zval(stream, zstream);

	pbuffer = pemalloc(buffer_len, hyss_stream_is_persistent(stream));

	memcpy(pbuffer, buffer, buffer_len);

	bucket = hyss_stream_bucket_new(stream, pbuffer, buffer_len, 1, hyss_stream_is_persistent(stream));

	if (bucket == NULL) {
		RETURN_FALSE;
	}

	ZVAL_RES(&zbucket, gear_register_resource(bucket, le_bucket));
	object_init(return_value);
	add_property_zval(return_value, "bucket", &zbucket);
	/* add_property_zval increments the refcount which is unwanted here */
	zval_ptr_dtor(&zbucket);
	add_property_stringl(return_value, "data", bucket->buf, bucket->buflen);
	add_property_long(return_value, "datalen", bucket->buflen);
}
/* }}} */

/* {{{ proto array stream_get_filters(void)
   Returns a list of registered filters */
HYSS_FUNCTION(stream_get_filters)
{
	gear_string *filter_name;
	HashTable *filters_hash;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);

	filters_hash = hyss_get_stream_filters_hash();

	if (filters_hash) {
		GEAR_HASH_FOREACH_STR_KEY(filters_hash, filter_name) {
			if (filter_name) {
				add_next_index_str(return_value, gear_string_copy(filter_name));
			}
		} GEAR_HASH_FOREACH_END();
	}
	/* It's okay to return an empty array if no filters are registered */
}
/* }}} */

/* {{{ proto bool stream_filter_register(string filtername, string classname)
   Registers a custom filter handler class */
HYSS_FUNCTION(stream_filter_register)
{
	gear_string *filtername, *classname;
	struct hyss_user_filter_data *fdat;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(filtername)
		Z_PARAM_STR(classname)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	RETVAL_FALSE;

	if (!ZSTR_LEN(filtername)) {
		hyss_error_docref(NULL, E_WARNING, "Filter name cannot be empty");
		return;
	}

	if (!ZSTR_LEN(classname)) {
		hyss_error_docref(NULL, E_WARNING, "Class name cannot be empty");
		return;
	}

	if (!BG(user_filter_map)) {
		BG(user_filter_map) = (HashTable*) emalloc(sizeof(HashTable));
		gear_hash_init(BG(user_filter_map), 8, NULL, (dtor_func_t) filter_item_dtor, 0);
	}

	fdat = ecalloc(1, sizeof(struct hyss_user_filter_data));
	fdat->classname = gear_string_copy(classname);

	if (gear_hash_add_ptr(BG(user_filter_map), filtername, fdat) != NULL &&
			hyss_stream_filter_register_factory_volatile(filtername, &user_filter_factory) == SUCCESS) {
		RETVAL_TRUE;
	} else {
		gear_string_release_ex(classname, 0);
		efree(fdat);
	}
}
/* }}} */


