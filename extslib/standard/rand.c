/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_rand.h"
#include "hyss_mt_rand.h"

/* {{{ hyss_srand
 */
HYSSAPI void hyss_srand(gear_long seed)
{
	hyss_mt_srand(seed);
}
/* }}} */

/* {{{ hyss_rand
 */
HYSSAPI gear_long hyss_rand(void)
{
	return hyss_mt_rand();
}
/* }}} */

/* {{{ proto int mt_rand([int min, int max])
   Returns a random number from Mersenne Twister */
HYSS_FUNCTION(rand)
{
	gear_long min;
	gear_long max;
	int argc = GEAR_NUM_ARGS();

	if (argc == 0) {
		RETURN_LONG(hyss_mt_rand() >> 1);
	}

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_LONG(min)
		Z_PARAM_LONG(max)
	GEAR_PARSE_PARAMETERS_END();

	if (max < min) {
		RETURN_LONG(hyss_mt_rand_common(max, min));
	}

	RETURN_LONG(hyss_mt_rand_common(min, max));
}
/* }}} */

