/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_DNS_H
#define HYSS_DNS_H

#if defined(HAVE_DNS_SEARCH)
#define hyss_dns_search(res, dname, class, type, answer, anslen) \
    	((int)dns_search(res, dname, class, type, answer, anslen, (struct sockaddr *)&from, &fromsize))
#define hyss_dns_free_handle(res) \
		dns_free(res)

#elif defined(HAVE_RES_NSEARCH)
#define hyss_dns_search(res, dname, class, type, answer, anslen) \
			res_nsearch(res, dname, class, type, answer, anslen);
#if HAVE_RES_NDESTROY
#define hyss_dns_free_handle(res) \
			res_ndestroy(res); \
			hyss_dns_free_res(res)
#else
#define hyss_dns_free_handle(res) \
			res_nclose(res); \
			hyss_dns_free_res(res)
#endif

#elif defined(HAVE_RES_SEARCH)
#define hyss_dns_search(res, dname, class, type, answer, anslen) \
			res_search(dname, class, type, answer, anslen)
#define hyss_dns_free_handle(res) /* noop */

#endif

#if defined(HAVE_DNS_SEARCH) || defined(HAVE_RES_NSEARCH) || defined(HAVE_RES_SEARCH)
#define HAVE_DNS_SEARCH_FUNC 1
#endif

#if HAVE_DNS_SEARCH_FUNC && HAVE_DN_EXPAND && HAVE_DN_SKIPNAME
#define HAVE_FULL_DNS_FUNCS 1
#endif

HYSS_FUNCTION(gethostbyaddr);
HYSS_FUNCTION(gethostbyname);
HYSS_FUNCTION(gethostbynamel);

#ifdef HAVE_GETHOSTNAME
HYSS_FUNCTION(gethostname);
#endif

#if defined(HYSS_WIN32) || HAVE_DNS_SEARCH_FUNC
HYSS_FUNCTION(dns_check_record);

# if defined(HYSS_WIN32) || HAVE_FULL_DNS_FUNCS
HYSS_FUNCTION(dns_get_mx);
HYSS_FUNCTION(dns_get_record);
HYSS_MINIT_FUNCTION(dns);
# endif

#endif /* defined(HYSS_WIN32) || HAVE_DNS_SEARCH_FUNC */

#ifndef INT16SZ
#define INT16SZ		2
#endif

#ifndef INT32SZ
#define INT32SZ		4
#endif

#endif /* HYSS_DNS_H */
