/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_filestat.h"
#include "hyss_globals.h"

#ifdef HAVE_SYMLINK

#include <stdlib.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/stat.h>
#include <string.h>
#if HAVE_PWD_H
#ifdef HYSS_WIN32
#include "win32/pwd.h"
#else
#include <pwd.h>
#endif
#endif
#if HAVE_GRP_H
#ifdef HYSS_WIN32
#include "win32/grp.h"
#else
#include <grp.h>
#endif
#endif
#include <errno.h>
#include <ctype.h>

#include "hyss_link.h"
#include "hyss_string.h"

/* {{{ proto string readlink(string filename)
   Return the target of a symbolic link */
HYSS_FUNCTION(readlink)
{
	char *link;
	size_t link_len;
	char buff[MAXPATHLEN];
	ssize_t ret;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_PATH(link, link_len)
	GEAR_PARSE_PARAMETERS_END();

	if (hyss_check_open_basedir(link)) {
		RETURN_FALSE;
	}

	ret = hyss_sys_readlink(link, buff, MAXPATHLEN-1);

	if (ret == -1) {
		hyss_error_docref(NULL, E_WARNING, "%s", strerror(errno));
		RETURN_FALSE;
	}
	/* Append NULL to the end of the string */
	buff[ret] = '\0';

	RETURN_STRINGL(buff, ret);
}
/* }}} */

/* {{{ proto int linkinfo(string filename)
   Returns the st_dev field of the UNIX C stat structure describing the link */
HYSS_FUNCTION(linkinfo)
{
	char *link;
	char *dirname;
	size_t link_len;
	gear_stat_t sb;
	int ret;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_PATH(link, link_len)
	GEAR_PARSE_PARAMETERS_END();

	dirname = estrndup(link, link_len);
	hyss_dirname(dirname, link_len);

	if (hyss_check_open_basedir(dirname)) {
		efree(dirname);
		RETURN_FALSE;
	}

	ret = VCWD_LSTAT(link, &sb);
	if (ret == -1) {
		hyss_error_docref(NULL, E_WARNING, "%s", strerror(errno));
		efree(dirname);
		RETURN_LONG(-1L);
	}

	efree(dirname);
	RETURN_LONG((gear_long) sb.st_dev);
}
/* }}} */

/* {{{ proto int symlink(string target, string link)
   Create a symbolic link */
HYSS_FUNCTION(symlink)
{
	char *topath, *frompath;
	size_t topath_len, frompath_len;
	int ret;
	char source_p[MAXPATHLEN];
	char dest_p[MAXPATHLEN];
	char dirname[MAXPATHLEN];
	size_t len;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_PATH(topath, topath_len)
		Z_PARAM_PATH(frompath, frompath_len)
	GEAR_PARSE_PARAMETERS_END();

	if (!expand_filepath(frompath, source_p)) {
		hyss_error_docref(NULL, E_WARNING, "No such file or directory");
		RETURN_FALSE;
	}

	memcpy(dirname, source_p, sizeof(source_p));
	len = hyss_dirname(dirname, strlen(dirname));

	if (!expand_filepath_ex(topath, dest_p, dirname, len)) {
		hyss_error_docref(NULL, E_WARNING, "No such file or directory");
		RETURN_FALSE;
	}

	if (hyss_stream_locate_url_wrapper(source_p, NULL, STREAM_LOCATE_WRAPPERS_ONLY) ||
		hyss_stream_locate_url_wrapper(dest_p, NULL, STREAM_LOCATE_WRAPPERS_ONLY) )
	{
		hyss_error_docref(NULL, E_WARNING, "Unable to symlink to a URL");
		RETURN_FALSE;
	}

	if (hyss_check_open_basedir(dest_p)) {
		RETURN_FALSE;
	}

	if (hyss_check_open_basedir(source_p)) {
		RETURN_FALSE;
	}

	/* For the source, an expanded path must be used (in ZTS an other thread could have changed the CWD).
	 * For the target the exact string given by the user must be used, relative or not, existing or not.
	 * The target is relative to the link itself, not to the CWD. */
	ret = symlink(topath, source_p);

	if (ret == -1) {
		hyss_error_docref(NULL, E_WARNING, "%s", strerror(errno));
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int link(string target, string link)
   Create a hard link */
HYSS_FUNCTION(link)
{
	char *topath, *frompath;
	size_t topath_len, frompath_len;
	int ret;
	char source_p[MAXPATHLEN];
	char dest_p[MAXPATHLEN];

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_PATH(topath, topath_len)
		Z_PARAM_PATH(frompath, frompath_len)
	GEAR_PARSE_PARAMETERS_END();

	if (!expand_filepath(frompath, source_p) || !expand_filepath(topath, dest_p)) {
		hyss_error_docref(NULL, E_WARNING, "No such file or directory");
		RETURN_FALSE;
	}

	if (hyss_stream_locate_url_wrapper(source_p, NULL, STREAM_LOCATE_WRAPPERS_ONLY) ||
		hyss_stream_locate_url_wrapper(dest_p, NULL, STREAM_LOCATE_WRAPPERS_ONLY) )
	{
		hyss_error_docref(NULL, E_WARNING, "Unable to link to a URL");
		RETURN_FALSE;
	}

	if (hyss_check_open_basedir(dest_p)) {
		RETURN_FALSE;
	}

	if (hyss_check_open_basedir(source_p)) {
		RETURN_FALSE;
	}

#ifndef ZTS
	ret = link(topath, frompath);
#else
	ret = link(dest_p, source_p);
#endif
	if (ret == -1) {
		hyss_error_docref(NULL, E_WARNING, "%s", strerror(errno));
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

#endif

