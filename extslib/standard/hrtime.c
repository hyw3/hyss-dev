/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hrtime.h"

/* {{{ */
/* This file reuses code parts from the cross-platform timer library
	Public Domain - 2011 Mattias Jansson / Rampant Pixels */

#if HYSS_HRTIME_PLATFORM_POSIX

# include <unistd.h>
# include <time.h>
# include <string.h>

#elif HYSS_HRTIME_PLATFORM_WINDOWS

# define WIN32_LEAN_AND_MEAN

static double _timer_scale = .0;

#elif HYSS_HRTIME_PLATFORM_APPLE

# include <mach/mach_time.h>
# include <string.h>
static mach_timebase_info_data_t _timerlib_info;

#elif HYSS_HRTIME_PLATFORM_HPUX

# include <sys/time.h>

#elif HYSS_HRTIME_PLATFORM_AIX

# include <sys/time.h>
# include <sys/systemcfg.h>

#endif

#define NANO_IN_SEC 1000000000
/* }}} */

static int _timer_init()
{/*{{{*/
#if HYSS_HRTIME_PLATFORM_WINDOWS

	LARGE_INTEGER tf = {0};
	if (!QueryPerformanceFrequency(&tf) || 0 == tf.QuadPart) {
		return -1;
	}
	_timer_scale = (double)NANO_IN_SEC / (hyss_hrtime_t)tf.QuadPart;

#elif HYSS_HRTIME_PLATFORM_APPLE

	if (mach_timebase_info(&_timerlib_info)) {
		return -1;
	}

#elif HYSS_HRTIME_PLATFORM_POSIX

#if !_POSIX_MONOTONIC_CLOCK
#ifdef _SC_MONOTONIC_CLOCK
	if (0 >= sysconf(_SC_MONOTONIC_CLOCK)) {
		return -1;
	}
#endif
#endif

#elif HYSS_HRTIME_PLATFORM_HPUX

	/* pass */

#elif HYSS_HRTIME_PLATFORM_AIX

	/* pass */

#else
	/* Timer unavailable. */
	return -1;
#endif

	return 0;
}/*}}}*/

/* {{{ */
HYSS_MINIT_FUNCTION(hrtime)
{
	if (0 > _timer_init()) {
		hyss_error_docref(NULL, E_WARNING, "Failed to initialize high-resolution timer");
		return FAILURE;
	}

	return SUCCESS;
}
/* }}} */

static gear_always_inline hyss_hrtime_t _timer_current(void)
{/*{{{*/
#if HYSS_HRTIME_PLATFORM_WINDOWS
	LARGE_INTEGER lt = {0};
	QueryPerformanceCounter(&lt);
	return (hyss_hrtime_t)((hyss_hrtime_t)lt.QuadPart * _timer_scale);
#elif HYSS_HRTIME_PLATFORM_APPLE
	return (hyss_hrtime_t)mach_absolute_time() * _timerlib_info.numer / _timerlib_info.denom;
#elif HYSS_HRTIME_PLATFORM_POSIX
	struct timespec ts = { .tv_sec = 0, .tv_nsec = 0 };
	if (0 == clock_gettime(CLOCK_MONOTONIC, &ts)) {
		return ((hyss_hrtime_t) ts.tv_sec * (hyss_hrtime_t)NANO_IN_SEC) + ts.tv_nsec;
	}
	return 0;
#elif HYSS_HRTIME_PLATFORM_HPUX
	return (hyss_hrtime_t) gethrtime();
#elif  HYSS_HRTIME_PLATFORM_AIX
	timebasestruct_t t;
	read_wall_time(&t, TIMEBASE_SZ);
	time_base_to_time(&t, TIMEBASE_SZ);
	return (hyss_hrtime_t) t.tb_high * (hyss_hrtime_t)NANO_IN_SEC + t.tb_low;
#else
	return 0;
#endif
}/*}}}*/

#if GEAR_ENABLE_ZVAL_LONG64
#define HYSS_RETURN_HRTIME(t) RETURN_LONG((gear_long)t)
#else
#ifdef _WIN32
# define HRTIME_U64A(i, s, len) _ui64toa_s(i, s, len, 10)
#else
# define HRTIME_U64A(i, s, len) \
	do { \
		int st = snprintf(s, len, "%llu", i); \
		s[st] = '\0'; \
	} while (0)
#endif
#define HYSS_RETURN_HRTIME(t) do { \
	char _a[GEAR_LTOA_BUF_LEN]; \
	double _d; \
	HRTIME_U64A(t, _a, GEAR_LTOA_BUF_LEN); \
	_d = gear_strtod(_a, NULL); \
	RETURN_DOUBLE(_d); \
	} while (0)
#endif

/* {{{ proto mixed hrtime([bool get_as_number = false])
	Returns an array of integers in form [seconds, nanoseconds] counted
	from an arbitrary point in time. If an optional boolean argument is
	passed, returns an integer on 64-bit platforms or float on 32-bit
	containing the current high-resolution time in nanoseconds. The
	delivered timestamp is monotonic and can not be adjusted. */
HYSS_FUNCTION(hrtime)
{
#if HRTIME_AVAILABLE
	gear_bool get_as_num = 0;
	hyss_hrtime_t t = _timer_current();

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(get_as_num)
	GEAR_PARSE_PARAMETERS_END();

	if (UNEXPECTED(get_as_num)) {
		HYSS_RETURN_HRTIME(t);
	} else {
		array_init_size(return_value, 2);
		gear_hash_real_init_packed(Z_ARRVAL_P(return_value));
		add_next_index_long(return_value, (gear_long)(t / (hyss_hrtime_t)NANO_IN_SEC));
		add_next_index_long(return_value, (gear_long)(t % (hyss_hrtime_t)NANO_IN_SEC));
	}
#else
	RETURN_FALSE
#endif
}
/* }}} */

HYSSAPI hyss_hrtime_t hyss_hrtime_current(void)
{/*{{{*/
	return _timer_current();
}/*}}}*/

