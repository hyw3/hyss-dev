/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DL_H
#define DL_H

HYSSAPI int hyss_load_extension(char *filename, int type, int start_now);
HYSSAPI void hyss_dl(char *file, int type, zval *return_value, int start_now);
HYSSAPI void *hyss_load_shlib(char *path, char **errp);

/* dynamic loading functions */
HYSSAPI HYSS_FUNCTION(dl);

HYSS_MINFO_FUNCTION(dl);

#endif /* DL_H */
