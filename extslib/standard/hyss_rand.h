/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_RAND_H
#define	HYSS_RAND_H

#include "hyss_lcg.h"
#include "hyss_mt_rand.h"

/* System Rand functions */
#ifndef RAND_MAX
#define RAND_MAX HYSS_MT_RAND_MAX
#endif

#define HYSS_RAND_MAX HYSS_MT_RAND_MAX

/*
 * A bit of tricky math here.  We want to avoid using a modulus because
 * that simply tosses the high-order bits and might skew the distribution
 * of random values over the range.  Instead we map the range directly.
 *
 * We need to map the range from 0...M evenly to the range a...b
 * Let n = the random number and n' = the mapped random number
 *
 * Then we have: n' = a + n(b-a)/M
 *
 * We have a problem here in that only n==M will get mapped to b which
 # means the chances of getting b is much much less than getting any of
 # the other values in the range.  We can fix this by increasing our range
 # artificially and using:
 #
 #               n' = a + n(b-a+1)/M
 *
 # Now we only have a problem if n==M which would cause us to produce a
 # number of b+1 which would be bad.  So we bump M up by one to make sure
 # this will never happen, and the final algorithm looks like this:
 #
 #               n' = a + n(b-a+1)/(M+1)
 *
 * -RL
 */
#define RAND_RANGE_BADSCALING(__n, __min, __max, __tmax) \
	(__n) = (__min) + (gear_long) ((double) ( (double) (__max) - (__min) + 1.0) * ((__n) / ((__tmax) + 1.0)))

#ifdef HYSS_WIN32
#define GENERATE_SEED() (((gear_long) (time(0) * GetCurrentProcessId())) ^ ((gear_long) (1000000.0 * hyss_combined_lcg())))
#else
#define GENERATE_SEED() (((gear_long) (time(0) * getpid())) ^ ((gear_long) (1000000.0 * hyss_combined_lcg())))
#endif

HYSSAPI void hyss_srand(gear_long seed);
HYSSAPI gear_long hyss_rand(void);

#endif	/* HYSS_RAND_H */
