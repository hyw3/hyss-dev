/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HYSS_WIN32
#include "win32/time.h"
#include "win32/getrusage.h"
#else
#include <sys/time.h>
#endif
#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "microtime.h"
#include "extslib/date/hyss_date.h"

#define NUL  '\0'
#define MICRO_IN_SEC 1000000.00
#define SEC_IN_MIN 60

#ifdef HAVE_GETTIMEOFDAY
static void _hyss_gettimeofday(INTERNAL_FUNCTION_PARAMETERS, int mode)
{
	gear_bool get_as_float = 0;
	struct timeval tp = {0};

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(get_as_float)
	GEAR_PARSE_PARAMETERS_END();

	if (gettimeofday(&tp, NULL)) {
		RETURN_FALSE;
	}

	if (get_as_float) {
		RETURN_DOUBLE((double)(tp.tv_sec + tp.tv_usec / MICRO_IN_SEC));
	}

	if (mode) {
		timelib_time_offset *offset;

		offset = timelib_get_time_zone_info(tp.tv_sec, get_timezone_info());

		array_init(return_value);
		add_assoc_long(return_value, "sec", tp.tv_sec);
		add_assoc_long(return_value, "usec", tp.tv_usec);

		add_assoc_long(return_value, "minuteswest", -offset->offset / SEC_IN_MIN);
		add_assoc_long(return_value, "dsttime", offset->is_dst);

		timelib_time_offset_dtor(offset);
	} else {
		RETURN_NEW_STR(gear_strpprintf(0, "%.8F %ld", tp.tv_usec / MICRO_IN_SEC, (long)tp.tv_sec));
	}
}

/* {{{ proto mixed microtime([bool get_as_float])
   Returns either a string or a float containing the current time in seconds and microseconds */
HYSS_FUNCTION(microtime)
{
	_hyss_gettimeofday(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto array gettimeofday([bool get_as_float])
   Returns the current time as array */
HYSS_FUNCTION(gettimeofday)
{
	_hyss_gettimeofday(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
#endif
/* }}} */

#ifdef HAVE_GETRUSAGE
/* {{{ proto array getrusage([int who])
   Returns an array of usage statistics */
HYSS_FUNCTION(getrusage)
{
	struct rusage usg;
	gear_long pwho = 0;
	int who = RUSAGE_SELF;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(pwho)
	GEAR_PARSE_PARAMETERS_END();

	if (pwho == 1) {
		who = RUSAGE_CHILDREN;
	}

	memset(&usg, 0, sizeof(struct rusage));

	if (getrusage(who, &usg) == -1) {
		RETURN_FALSE;
	}

	array_init(return_value);

#define HYSS_RUSAGE_PARA(a) \
		add_assoc_long(return_value, #a, usg.a)

#ifdef HYSS_WIN32 /* Windows only implements a limited amount of fields from the rusage struct */
	HYSS_RUSAGE_PARA(ru_majflt);
	HYSS_RUSAGE_PARA(ru_maxrss);
#elif !defined(_OSD_POSIX)
	HYSS_RUSAGE_PARA(ru_oublock);
	HYSS_RUSAGE_PARA(ru_inblock);
	HYSS_RUSAGE_PARA(ru_msgsnd);
	HYSS_RUSAGE_PARA(ru_msgrcv);
	HYSS_RUSAGE_PARA(ru_maxrss);
	HYSS_RUSAGE_PARA(ru_ixrss);
	HYSS_RUSAGE_PARA(ru_idrss);
	HYSS_RUSAGE_PARA(ru_minflt);
	HYSS_RUSAGE_PARA(ru_majflt);
	HYSS_RUSAGE_PARA(ru_nsignals);
	HYSS_RUSAGE_PARA(ru_nvcsw);
	HYSS_RUSAGE_PARA(ru_nivcsw);
	HYSS_RUSAGE_PARA(ru_nswap);
#endif /*_OSD_POSIX*/
	HYSS_RUSAGE_PARA(ru_utime.tv_usec);
	HYSS_RUSAGE_PARA(ru_utime.tv_sec);
	HYSS_RUSAGE_PARA(ru_stime.tv_usec);
	HYSS_RUSAGE_PARA(ru_stime.tv_sec);

#undef HYSS_RUSAGE_PARA
}
#endif /* HAVE_GETRUSAGE */

/* }}} */

