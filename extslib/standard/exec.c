/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "hyss.h"
#include <ctype.h>
#include "hyss_string.h"
#include "extslib/standard/head.h"
#include "extslib/standard/file.h"
#include "basic_functions.h"
#include "exec.h"
#include "hyss_globals.h"
#include "SAPI.h"

#if HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif
#if HAVE_SIGNAL_H
#include <signal.h>
#endif

#if HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#if HAVE_FCNTL_H
#include <fcntl.h>
#endif

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#if HAVE_LIMITS_H
#include <limits.h>
#endif

#ifdef HYSS_WIN32
# include "win32/nice.h"
#endif

static size_t cmd_max_len;

/* {{{ HYSS_MINIT_FUNCTION(exec) */
HYSS_MINIT_FUNCTION(exec)
{
#ifdef _SC_ARG_MAX
	cmd_max_len = sysconf(_SC_ARG_MAX);
	if ((size_t)-1 == cmd_max_len) {
#ifdef _POSIX_ARG_MAX
		cmd_max_len = _POSIX_ARG_MAX;
#else
		cmd_max_len = 4096;
#endif
	}
#elif defined(ARG_MAX)
	cmd_max_len = ARG_MAX;
#elif defined(HYSS_WIN32)
	/* Executed commands will run through cmd.exe. As long as it's the case,
		it's just the constant limit.*/
	cmd_max_len = 8192;
#else
	/* This is just an arbitrary value for the fallback case. */
	cmd_max_len = 4096;
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ hyss_exec
 * If type==0, only last line of output is returned (exec)
 * If type==1, all lines will be printed and last lined returned (system)
 * If type==2, all lines will be saved to given array (exec with &$array)
 * If type==3, output will be printed binary, no lines will be saved or returned (passthru)
 *
 */
HYSSAPI int hyss_exec(int type, char *cmd, zval *array, zval *return_value)
{
	FILE *fp;
	char *buf;
	size_t l = 0;
	int pclose_return;
	char *b, *d=NULL;
	hyss_stream *stream;
	size_t buflen, bufl = 0;
#if HYSS_SIGCHILD
	void (*sig_handler)() = NULL;
#endif

#if HYSS_SIGCHILD
	sig_handler = signal (SIGCHLD, SIG_DFL);
#endif

#ifdef HYSS_WIN32
	fp = VCWD_POPEN(cmd, "rb");
#else
	fp = VCWD_POPEN(cmd, "r");
#endif
	if (!fp) {
		hyss_error_docref(NULL, E_WARNING, "Unable to fork [%s]", cmd);
		goto err;
	}

	stream = hyss_stream_fopen_from_pipe(fp, "rb");

	buf = (char *) emalloc(EXEC_INPUT_BUF);
	buflen = EXEC_INPUT_BUF;

	if (type != 3) {
		b = buf;

		while (hyss_stream_get_line(stream, b, EXEC_INPUT_BUF, &bufl)) {
			/* no new line found, let's read some more */
			if (b[bufl - 1] != '\n' && !hyss_stream_eof(stream)) {
				if (buflen < (bufl + (b - buf) + EXEC_INPUT_BUF)) {
					bufl += b - buf;
					buflen = bufl + EXEC_INPUT_BUF;
					buf = erealloc(buf, buflen);
					b = buf + bufl;
				} else {
					b += bufl;
				}
				continue;
			} else if (b != buf) {
				bufl += b - buf;
			}

			if (type == 1) {
				HYSSWRITE(buf, bufl);
				if (hyss_output_get_level() < 1) {
					sapi_flush();
				}
			} else if (type == 2) {
				/* strip trailing whitespaces */
				l = bufl;
				while (l-- > 0 && isspace(((unsigned char *)buf)[l]));
				if (l != (bufl - 1)) {
					bufl = l + 1;
					buf[bufl] = '\0';
				}
				add_next_index_stringl(array, buf, bufl);
			}
			b = buf;
		}
		if (bufl) {
			/* strip trailing whitespaces if we have not done so already */
			if ((type == 2 && buf != b) || type != 2) {
				l = bufl;
				while (l-- > 0 && isspace(((unsigned char *)buf)[l]));
				if (l != (bufl - 1)) {
					bufl = l + 1;
					buf[bufl] = '\0';
				}
				if (type == 2) {
					add_next_index_stringl(array, buf, bufl);
				}
			}

			/* Return last line from the shell command */
			RETVAL_STRINGL(buf, bufl);
		} else { /* should return NULL, but for BC we return "" */
			RETVAL_EMPTY_STRING();
		}
	} else {
		while((bufl = hyss_stream_read(stream, buf, EXEC_INPUT_BUF)) > 0) {
			HYSSWRITE(buf, bufl);
		}
	}

	pclose_return = hyss_stream_close(stream);
	efree(buf);

done:
#if HYSS_SIGCHILD
	if (sig_handler) {
		signal(SIGCHLD, sig_handler);
	}
#endif
	if (d) {
		efree(d);
	}
	return pclose_return;
err:
	pclose_return = -1;
	goto done;
}
/* }}} */

static void hyss_exec_ex(INTERNAL_FUNCTION_PARAMETERS, int mode) /* {{{ */
{
	char *cmd;
	size_t cmd_len;
	zval *ret_code=NULL, *ret_array=NULL;
	int ret;

	GEAR_PARSE_PARAMETERS_START(1, (mode ? 2 : 3))
		Z_PARAM_STRING(cmd, cmd_len)
		Z_PARAM_OPTIONAL
		if (!mode) {
			Z_PARAM_ZVAL_DEREF(ret_array)
		}
		Z_PARAM_ZVAL_DEREF(ret_code)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (!cmd_len) {
		hyss_error_docref(NULL, E_WARNING, "Cannot execute a blank command");
		RETURN_FALSE;
	}
	if (strlen(cmd) != cmd_len) {
		hyss_error_docref(NULL, E_WARNING, "NULL byte detected. Possible attack");
		RETURN_FALSE;
	}

	if (!ret_array) {
		ret = hyss_exec(mode, cmd, NULL, return_value);
	} else {
		if (Z_TYPE_P(ret_array) != IS_ARRAY) {
			zval_ptr_dtor(ret_array);
			array_init(ret_array);
		} else if (Z_REFCOUNT_P(ret_array) > 1) {
			zval_ptr_dtor(ret_array);
			ZVAL_ARR(ret_array, gear_array_dup(Z_ARR_P(ret_array)));
		}
		ret = hyss_exec(2, cmd, ret_array, return_value);
	}
	if (ret_code) {
		zval_ptr_dtor(ret_code);
		ZVAL_LONG(ret_code, ret);
	}
}
/* }}} */

/* {{{ proto string exec(string command [, array &output [, int &return_value]])
   Execute an external program */
HYSS_FUNCTION(exec)
{
	hyss_exec_ex(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto int system(string command [, int &return_value])
   Execute an external program and display output */
HYSS_FUNCTION(system)
{
	hyss_exec_ex(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto void passthru(string command [, int &return_value])
   Execute an external program and display raw output */
HYSS_FUNCTION(passthru)
{
	hyss_exec_ex(INTERNAL_FUNCTION_PARAM_PASSTHRU, 3);
}
/* }}} */

/* {{{ hyss_escape_shell_cmd
   Escape all chars that could possibly be used to
   break out of a shell command

   This function emalloc's a string and returns the pointer.
   Remember to efree it when done with it.

   *NOT* safe for binary strings
*/
HYSSAPI gear_string *hyss_escape_shell_cmd(char *str)
{
	register size_t x, y;
	size_t l = strlen(str);
	uint64_t estimate = (2 * (uint64_t)l) + 1;
	gear_string *cmd;
#ifndef HYSS_WIN32
	char *p = NULL;
#endif

	/* max command line length - two single quotes - \0 byte length */
	if (l > cmd_max_len - 2 - 1) {
		hyss_error_docref(NULL, E_ERROR, "Command exceeds the allowed length of %zu bytes", cmd_max_len);
		return ZSTR_EMPTY_ALLOC();
	}

	cmd = gear_string_safe_alloc(2, l, 0, 0);

	for (x = 0, y = 0; x < l; x++) {
		int mb_len = hyss_mblen(str + x, (l - x));

		/* skip non-valid multibyte characters */
		if (mb_len < 0) {
			continue;
		} else if (mb_len > 1) {
			memcpy(ZSTR_VAL(cmd) + y, str + x, mb_len);
			y += mb_len;
			x += mb_len - 1;
			continue;
		}

		switch (str[x]) {
#ifndef HYSS_WIN32
			case '"':
			case '\'':
				if (!p && (p = memchr(str + x + 1, str[x], l - x - 1))) {
					/* noop */
				} else if (p && *p == str[x]) {
					p = NULL;
				} else {
					ZSTR_VAL(cmd)[y++] = '\\';
				}
				ZSTR_VAL(cmd)[y++] = str[x];
				break;
#else
			/* % is Windows specific for environmental variables, ^%PATH% will
				output PATH while ^%PATH^% will not. escapeshellcmd->val will escape all % and !.
			*/
			case '%':
			case '!':
			case '"':
			case '\'':
#endif
			case '#': /* This is character-set independent */
			case '&':
			case ';':
			case '`':
			case '|':
			case '*':
			case '?':
			case '~':
			case '<':
			case '>':
			case '^':
			case '(':
			case ')':
			case '[':
			case ']':
			case '{':
			case '}':
			case '$':
			case '\\':
			case '\x0A': /* excluding these two */
			case '\xFF':
#ifdef HYSS_WIN32
				ZSTR_VAL(cmd)[y++] = '^';
#else
				ZSTR_VAL(cmd)[y++] = '\\';
#endif
				/* fall-through */
			default:
				ZSTR_VAL(cmd)[y++] = str[x];

		}
	}
	ZSTR_VAL(cmd)[y] = '\0';

	if (y > cmd_max_len + 1) {
		hyss_error_docref(NULL, E_ERROR, "Escaped command exceeds the allowed length of %zu bytes", cmd_max_len);
		gear_string_release_ex(cmd, 0);
		return ZSTR_EMPTY_ALLOC();
	}

	if ((estimate - y) > 4096) {
		/* realloc if the estimate was way overill
		 * Arbitrary cutoff point of 4096 */
		cmd = gear_string_truncate(cmd, y, 0);
	}

	ZSTR_LEN(cmd) = y;

	return cmd;
}
/* }}} */

/* {{{ hyss_escape_shell_arg
 */
HYSSAPI gear_string *hyss_escape_shell_arg(char *str)
{
	size_t x, y = 0;
	size_t l = strlen(str);
	gear_string *cmd;
	uint64_t estimate = (4 * (uint64_t)l) + 3;

	/* max command line length - two single quotes - \0 byte length */
	if (l > cmd_max_len - 2 - 1) {
		hyss_error_docref(NULL, E_ERROR, "Argument exceeds the allowed length of %zu bytes", cmd_max_len);
		return ZSTR_EMPTY_ALLOC();
	}

	cmd = gear_string_safe_alloc(4, l, 2, 0); /* worst case */

#ifdef HYSS_WIN32
	ZSTR_VAL(cmd)[y++] = '"';
#else
	ZSTR_VAL(cmd)[y++] = '\'';
#endif

	for (x = 0; x < l; x++) {
		int mb_len = hyss_mblen(str + x, (l - x));

		/* skip non-valid multibyte characters */
		if (mb_len < 0) {
			continue;
		} else if (mb_len > 1) {
			memcpy(ZSTR_VAL(cmd) + y, str + x, mb_len);
			y += mb_len;
			x += mb_len - 1;
			continue;
		}

		switch (str[x]) {
#ifdef HYSS_WIN32
		case '"':
		case '%':
		case '!':
			ZSTR_VAL(cmd)[y++] = ' ';
			break;
#else
		case '\'':
			ZSTR_VAL(cmd)[y++] = '\'';
			ZSTR_VAL(cmd)[y++] = '\\';
			ZSTR_VAL(cmd)[y++] = '\'';
#endif
			/* fall-through */
		default:
			ZSTR_VAL(cmd)[y++] = str[x];
		}
	}
#ifdef HYSS_WIN32
	if (y > 0 && '\\' == ZSTR_VAL(cmd)[y - 1]) {
		int k = 0, n = y - 1;
		for (; n >= 0 && '\\' == ZSTR_VAL(cmd)[n]; n--, k++);
		if (k % 2) {
			ZSTR_VAL(cmd)[y++] = '\\';
		}
	}

	ZSTR_VAL(cmd)[y++] = '"';
#else
	ZSTR_VAL(cmd)[y++] = '\'';
#endif
	ZSTR_VAL(cmd)[y] = '\0';

	if (y > cmd_max_len + 1) {
		hyss_error_docref(NULL, E_ERROR, "Escaped argument exceeds the allowed length of %zu bytes", cmd_max_len);
		gear_string_release_ex(cmd, 0);
		return ZSTR_EMPTY_ALLOC();
	}

	if ((estimate - y) > 4096) {
		/* realloc if the estimate was way overill
		 * Arbitrary cutoff point of 4096 */
		cmd = gear_string_truncate(cmd, y, 0);
	}
	ZSTR_LEN(cmd) = y;
	return cmd;
}
/* }}} */

/* {{{ proto string escapeshellcmd(string command)
   Escape shell metacharacters */
HYSS_FUNCTION(escapeshellcmd)
{
	char *command;
	size_t command_len;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(command, command_len)
	GEAR_PARSE_PARAMETERS_END();

	if (command_len) {
		if (command_len != strlen(command)) {
			hyss_error_docref(NULL, E_ERROR, "Input string contains NULL bytes");
			return;
		}
		RETVAL_STR(hyss_escape_shell_cmd(command));
	} else {
		RETVAL_EMPTY_STRING();
	}
}
/* }}} */

/* {{{ proto string escapeshellarg(string arg)
   Quote and escape an argument for use in a shell command */
HYSS_FUNCTION(escapeshellarg)
{
	char *argument;
	size_t argument_len;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(argument, argument_len)
	GEAR_PARSE_PARAMETERS_END();

	if (argument) {
		if (argument_len != strlen(argument)) {
			hyss_error_docref(NULL, E_ERROR, "Input string contains NULL bytes");
			return;
		}
		RETVAL_STR(hyss_escape_shell_arg(argument));
	}
}
/* }}} */

/* {{{ proto string shell_exec(string cmd)
   Execute command via shell and return complete output as string */
HYSS_FUNCTION(shell_exec)
{
	FILE *in;
	char *command;
	size_t command_len;
	gear_string *ret;
	hyss_stream *stream;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(command, command_len)
	GEAR_PARSE_PARAMETERS_END();

#ifdef HYSS_WIN32
	if ((in=VCWD_POPEN(command, "rt"))==NULL) {
#else
	if ((in=VCWD_POPEN(command, "r"))==NULL) {
#endif
		hyss_error_docref(NULL, E_WARNING, "Unable to execute '%s'", command);
		RETURN_FALSE;
	}

	stream = hyss_stream_fopen_from_pipe(in, "rb");
	ret = hyss_stream_copy_to_mem(stream, HYSS_STREAM_COPY_ALL, 0);
	hyss_stream_close(stream);

	if (ret && ZSTR_LEN(ret) > 0) {
		RETVAL_STR(ret);
	}
}
/* }}} */

#ifdef HAVE_NICE
/* {{{ proto bool proc_nice(int priority)
   Change the priority of the current process */
HYSS_FUNCTION(proc_nice)
{
	gear_long pri;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(pri)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	errno = 0;
	hyss_ignore_value(nice(pri));
	if (errno) {
#ifdef HYSS_WIN32
		hyss_error_docref(NULL, E_WARNING, "%s", hyss_win_err());
#else
		hyss_error_docref(NULL, E_WARNING, "Only a super user may attempt to increase the priority of a process");
#endif
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */
#endif

