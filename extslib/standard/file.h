/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILE_H
#define FILE_H

#include "hyss_network.h"

HYSS_MINIT_FUNCTION(file);
HYSS_MSHUTDOWN_FUNCTION(file);

HYSS_FUNCTION(tempnam);
HYSS_NAMED_FUNCTION(hyss_if_tmpfile);
HYSS_NAMED_FUNCTION(hyss_if_fopen);
HYSSAPI HYSS_FUNCTION(fclose);
HYSS_FUNCTION(popen);
HYSS_FUNCTION(pclose);
HYSSAPI HYSS_FUNCTION(feof);
HYSSAPI HYSS_FUNCTION(fread);
HYSSAPI HYSS_FUNCTION(fgetc);
HYSSAPI HYSS_FUNCTION(fgets);
HYSS_FUNCTION(fscanf);
HYSSAPI HYSS_FUNCTION(fgetss);
HYSS_FUNCTION(fgetcsv);
HYSS_FUNCTION(fputcsv);
HYSSAPI HYSS_FUNCTION(fwrite);
HYSSAPI HYSS_FUNCTION(fflush);
HYSSAPI HYSS_FUNCTION(rewind);
HYSSAPI HYSS_FUNCTION(ftell);
HYSSAPI HYSS_FUNCTION(fseek);
HYSS_FUNCTION(mkdir);
HYSS_FUNCTION(rmdir);
HYSSAPI HYSS_FUNCTION(fpassthru);
HYSS_FUNCTION(readfile);
HYSS_FUNCTION(umask);
HYSS_FUNCTION(rename);
HYSS_FUNCTION(unlink);
HYSS_FUNCTION(copy);
HYSS_FUNCTION(file);
HYSS_FUNCTION(file_get_contents);
HYSS_FUNCTION(file_put_contents);
HYSS_FUNCTION(get_meta_tags);
HYSS_FUNCTION(flock);
HYSS_FUNCTION(fd_set);
HYSS_FUNCTION(fd_isset);
#if HAVE_REALPATH || defined(ZTS)
HYSS_FUNCTION(realpath);
#endif
#ifdef HAVE_FNMATCH
HYSS_FUNCTION(fnmatch);
#endif
HYSS_NAMED_FUNCTION(hyss_if_ftruncate);
HYSS_NAMED_FUNCTION(hyss_if_fstat);
HYSS_FUNCTION(sys_get_temp_dir);

HYSS_MINIT_FUNCTION(user_streams);

HYSSAPI int hyss_le_stream_context(void);
HYSSAPI int hyss_set_sock_blocking(hyss_socket_t socketd, int block);
HYSSAPI int hyss_copy_file(const char *src, const char *dest);
HYSSAPI int hyss_copy_file_ex(const char *src, const char *dest, int src_chk);
HYSSAPI int hyss_copy_file_ctx(const char *src, const char *dest, int src_chk, hyss_stream_context *ctx);
HYSSAPI int hyss_mkdir_ex(const char *dir, gear_long mode, int options);
HYSSAPI int hyss_mkdir(const char *dir, gear_long mode);
HYSSAPI void hyss_fgetcsv(hyss_stream *stream, char delimiter, char enclosure, char escape_char, size_t buf_len, char *buf, zval *return_value);
HYSSAPI size_t hyss_fputcsv(hyss_stream *stream, zval *fields, char delimiter, char enclosure, char escape_char);

#define META_DEF_BUFSIZE 8192

#define HYSS_FILE_USE_INCLUDE_PATH 1
#define HYSS_FILE_IGNORE_NEW_LINES 2
#define HYSS_FILE_SKIP_EMPTY_LINES 4
#define HYSS_FILE_APPEND 8
#define HYSS_FILE_NO_DEFAULT_CONTEXT 16

typedef enum _hyss_meta_tags_token {
	TOK_EOF = 0,
	TOK_OPENTAG,
	TOK_CLOSETAG,
	TOK_SLASH,
	TOK_EQUAL,
	TOK_SPACE,
	TOK_ID,
	TOK_STRING,
	TOK_OTHER
} hyss_meta_tags_token;

typedef struct _hyss_meta_tags_data {
	hyss_stream *stream;
	int ulc;
	int lc;
	char *input_buffer;
	char *token_data;
	int token_len;
	int in_meta;
} hyss_meta_tags_data;

hyss_meta_tags_token hyss_next_meta_token(hyss_meta_tags_data *);

typedef struct {
	int pclose_ret;
	size_t def_chunk_size;
	gear_long auto_detect_line_endings;
	gear_long default_socket_timeout;
	char *user_agent; /* for the http wrapper */
	char *from_address; /* for the ftp and http wrappers */
	const char *user_stream_current_filename; /* for simple recursion protection */
	hyss_stream_context *default_context;
	HashTable *stream_wrappers;			/* per-request copy of url_stream_wrappers_hash */
	HashTable *stream_filters;			/* per-request copy of stream_filters_hash */
	HashTable *wrapper_errors;			/* key: wrapper address; value: linked list of char* */
	int pclose_wait;
#if defined(HAVE_GETHOSTBYNAME_R)
	struct hostent tmp_host_info;
	char *tmp_host_buf;
	size_t tmp_host_buf_len;
#endif
} hyss_file_globals;

#ifdef ZTS
#define FG(v) GEAR_PBCG(file_globals_id, hyss_file_globals *, v)
extern HYSSAPI int file_globals_id;
#else
#define FG(v) (file_globals.v)
extern HYSSAPI hyss_file_globals file_globals;
#endif


#endif /* FILE_H */
