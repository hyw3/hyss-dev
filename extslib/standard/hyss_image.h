/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_IMAGE_H
#define HYSS_IMAGE_H

HYSS_FUNCTION(getimagesize);
HYSS_FUNCTION(getimagesizefromstring);

HYSS_FUNCTION(image_type_to_mime_type);
HYSS_FUNCTION(image_type_to_extension);

/* {{{ enum image_filetype
   This enum is used to have extslib/standard/image.c and extslib/exif/exif.c use
   the same constants for file types.
*/
typedef enum
{ IMAGE_FILETYPE_UNKNOWN=0,
  IMAGE_FILETYPE_GIF=1,
  IMAGE_FILETYPE_JPEG,
  IMAGE_FILETYPE_PNG,
  IMAGE_FILETYPE_SWF,
  IMAGE_FILETYPE_PSD,
  IMAGE_FILETYPE_BMP,
  IMAGE_FILETYPE_TIFF_II, /* intel */
  IMAGE_FILETYPE_TIFF_MM, /* motorola */
  IMAGE_FILETYPE_JPC,
  IMAGE_FILETYPE_JP2,
  IMAGE_FILETYPE_JPX,
  IMAGE_FILETYPE_JB2,
  IMAGE_FILETYPE_SWC,
  IMAGE_FILETYPE_IFF,
  IMAGE_FILETYPE_WBMP,
  /* IMAGE_FILETYPE_JPEG2000 is a userland alias for IMAGE_FILETYPE_JPC */
  IMAGE_FILETYPE_XBM,
  IMAGE_FILETYPE_ICO,
  IMAGE_FILETYPE_WEBP,
/* WHEN EXTENDING: PLEASE ALSO REGISTER IN image.c:HYSS_MINIT_FUNCTION(imagetypes) */
  IMAGE_FILETYPE_COUNT
} image_filetype;
/* }}} */

HYSS_MINIT_FUNCTION(imagetypes);

HYSSAPI int hyss_getimagetype(hyss_stream *stream, char *filetype);

HYSSAPI char * hyss_image_type_to_mime_type(int image_type);

#endif /* HYSS_IMAGE_H */
