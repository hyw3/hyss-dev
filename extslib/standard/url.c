/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>

#include "hyss.h"

#include "url.h"
#include "file.h"
#ifdef _OSD_POSIX
#ifndef CLHY
#error On this EBCDIC platform, HYSS is only supported as an cLHy cAPI.
#else /*CLHY*/
#ifndef CHARSET_EBCDIC
#define CHARSET_EBCDIC /* this machine uses EBCDIC, not ASCII! */
#endif
#include "ebcdic.h"
#endif /*CLHY*/
#endif /*_OSD_POSIX*/

/* {{{ free_url
 */
HYSSAPI void hyss_url_free(hyss_url *theurl)
{
	if (theurl->scheme)
		gear_string_release_ex(theurl->scheme, 0);
	if (theurl->user)
		gear_string_release_ex(theurl->user, 0);
	if (theurl->pass)
		gear_string_release_ex(theurl->pass, 0);
	if (theurl->host)
		gear_string_release_ex(theurl->host, 0);
	if (theurl->path)
		gear_string_release_ex(theurl->path, 0);
	if (theurl->query)
		gear_string_release_ex(theurl->query, 0);
	if (theurl->fragment)
		gear_string_release_ex(theurl->fragment, 0);
	efree(theurl);
}
/* }}} */

/* {{{ hyss_replace_controlchars
 */
HYSSAPI char *hyss_replace_controlchars_ex(char *str, size_t len)
{
	unsigned char *s = (unsigned char *)str;
	unsigned char *e = (unsigned char *)str + len;

	if (!str) {
		return (NULL);
	}

	while (s < e) {

		if (iscntrl(*s)) {
			*s='_';
		}
		s++;
	}

	return (str);
}
/* }}} */

HYSSAPI char *hyss_replace_controlchars(char *str)
{
	return hyss_replace_controlchars_ex(str, strlen(str));
}

HYSSAPI hyss_url *hyss_url_parse(char const *str)
{
	return hyss_url_parse_ex(str, strlen(str));
}

/* {{{ hyss_url_parse
 */
HYSSAPI hyss_url *hyss_url_parse_ex(char const *str, size_t length)
{
	char port_buf[6];
	hyss_url *ret = ecalloc(1, sizeof(hyss_url));
	char const *s, *e, *p, *pp, *ue;

	s = str;
	ue = s + length;

	/* parse scheme */
	if ((e = memchr(s, ':', length)) && e != s) {
		/* validate scheme */
		p = s;
		while (p < e) {
			/* scheme = 1*[ lowalpha | digit | "+" | "-" | "." ] */
			if (!isalpha(*p) && !isdigit(*p) && *p != '+' && *p != '.' && *p != '-') {
				if (e + 1 < ue && e < s + strcspn(s, "?#")) {
					goto parse_port;
				} else if (s + 1 < ue && *s == '/' && *(s + 1) == '/') { /* relative-scheme URL */
					s += 2;
					e = 0;
					goto parse_host;
				} else {
					goto just_path;
				}
			}
			p++;
		}

		if (e + 1 == ue) { /* only scheme is available */
			ret->scheme = gear_string_init(s, (e - s), 0);
			hyss_replace_controlchars_ex(ZSTR_VAL(ret->scheme), ZSTR_LEN(ret->scheme));
			return ret;
		}

		/*
		 * certain schemas like mailto: and zlib: may not have any / after them
		 * this check ensures we support those.
		 */
		if (*(e+1) != '/') {
			/* check if the data we get is a port this allows us to
			 * correctly parse things like a.com:80
			 */
			p = e + 1;
			while (p < ue && isdigit(*p)) {
				p++;
			}

			if ((p == ue || *p == '/') && (p - e) < 7) {
				goto parse_port;
			}

			ret->scheme = gear_string_init(s, (e-s), 0);
			hyss_replace_controlchars_ex(ZSTR_VAL(ret->scheme), ZSTR_LEN(ret->scheme));

			s = e + 1;
			goto just_path;
		} else {
			ret->scheme = gear_string_init(s, (e-s), 0);
			hyss_replace_controlchars_ex(ZSTR_VAL(ret->scheme), ZSTR_LEN(ret->scheme));

			if (e + 2 < ue && *(e + 2) == '/') {
				s = e + 3;
				if (gear_string_equals_literal_ci(ret->scheme, "file")) {
					if (e + 3 < ue && *(e + 3) == '/') {
						/* support windows drive letters as in:
						   file:///c:/somedir/file.txt
						*/
						if (e + 5 < ue && *(e + 5) == ':') {
							s = e + 4;
						}
						goto just_path;
					}
				}
			} else {
				s = e + 1;
				goto just_path;
			}
		}
	} else if (e) { /* no scheme; starts with colon: look for port */
		parse_port:
		p = e + 1;
		pp = p;

		while (pp < ue && pp - p < 6 && isdigit(*pp)) {
			pp++;
		}

		if (pp - p > 0 && pp - p < 6 && (pp == ue || *pp == '/')) {
			gear_long port;
			memcpy(port_buf, p, (pp - p));
			port_buf[pp - p] = '\0';
			port = GEAR_STRTOL(port_buf, NULL, 10);
			if (port > 0 && port <= 65535) {
				ret->port = (unsigned short) port;
				if (s + 1 < ue && *s == '/' && *(s + 1) == '/') { /* relative-scheme URL */
				    s += 2;
				}
			} else {
				hyss_url_free(ret);
				return NULL;
			}
		} else if (p == pp && pp == ue) {
			hyss_url_free(ret);
			return NULL;
		} else if (s + 1 < ue && *s == '/' && *(s + 1) == '/') { /* relative-scheme URL */
			s += 2;
		} else {
			goto just_path;
		}
	} else if (s + 1 < ue && *s == '/' && *(s + 1) == '/') { /* relative-scheme URL */
		s += 2;
	} else {
		goto just_path;
	}

	parse_host:
	/* Binary-safe strcspn(s, "/?#") */
	e = ue;
	if ((p = memchr(s, '/', e - s))) {
		e = p;
	}
	if ((p = memchr(s, '?', e - s))) {
		e = p;
	}
	if ((p = memchr(s, '#', e - s))) {
		e = p;
	}

	/* check for login and password */
	if ((p = gear_memrchr(s, '@', (e-s)))) {
		if ((pp = memchr(s, ':', (p-s)))) {
			ret->user = gear_string_init(s, (pp-s), 0);
			hyss_replace_controlchars_ex(ZSTR_VAL(ret->user), ZSTR_LEN(ret->user));

			pp++;
			ret->pass = gear_string_init(pp, (p-pp), 0);
			hyss_replace_controlchars_ex(ZSTR_VAL(ret->pass), ZSTR_LEN(ret->pass));
		} else {
			ret->user = gear_string_init(s, (p-s), 0);
			hyss_replace_controlchars_ex(ZSTR_VAL(ret->user), ZSTR_LEN(ret->user));
		}

		s = p + 1;
	}

	/* check for port */
	if (s < ue && *s == '[' && *(e-1) == ']') {
		/* Short circuit portscan,
		   we're dealing with an
		   IPv6 embedded address */
		p = NULL;
	} else {
		p = gear_memrchr(s, ':', (e-s));
	}

	if (p) {
		if (!ret->port) {
			p++;
			if (e-p > 5) { /* port cannot be longer then 5 characters */
				hyss_url_free(ret);
				return NULL;
			} else if (e - p > 0) {
				gear_long port;
				memcpy(port_buf, p, (e - p));
				port_buf[e - p] = '\0';
				port = GEAR_STRTOL(port_buf, NULL, 10);
				if (port > 0 && port <= 65535) {
					ret->port = (unsigned short)port;
				} else {
					hyss_url_free(ret);
					return NULL;
				}
			}
			p--;
		}
	} else {
		p = e;
	}

	/* check if we have a valid host, if we don't reject the string as url */
	if ((p-s) < 1) {
		hyss_url_free(ret);
		return NULL;
	}

	ret->host = gear_string_init(s, (p-s), 0);
	hyss_replace_controlchars_ex(ZSTR_VAL(ret->host), ZSTR_LEN(ret->host));

	if (e == ue) {
		return ret;
	}

	s = e;

	just_path:

	e = ue;
	p = memchr(s, '#', (e - s));
	if (p) {
		p++;
		if (p < e) {
			ret->fragment = gear_string_init(p, (e - p), 0);
			hyss_replace_controlchars_ex(ZSTR_VAL(ret->fragment), ZSTR_LEN(ret->fragment));
		}
		e = p-1;
	}

	p = memchr(s, '?', (e - s));
	if (p) {
		p++;
		if (p < e) {
			ret->query = gear_string_init(p, (e - p), 0);
			hyss_replace_controlchars_ex(ZSTR_VAL(ret->query), ZSTR_LEN(ret->query));
		}
		e = p-1;
	}

	if (s < e || s == ue) {
		ret->path = gear_string_init(s, (e - s), 0);
		hyss_replace_controlchars_ex(ZSTR_VAL(ret->path), ZSTR_LEN(ret->path));
	}

	return ret;
}
/* }}} */

/* {{{ proto mixed parse_url(string url, [int url_component])
   Parse a URL and return its components */
HYSS_FUNCTION(parse_url)
{
	char *str;
	size_t str_len;
	hyss_url *resource;
	gear_long key = -1;
	zval tmp;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STRING(str, str_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(key)
	GEAR_PARSE_PARAMETERS_END();

	resource = hyss_url_parse_ex(str, str_len);
	if (resource == NULL) {
		/* @todo Find a method to determine why hyss_url_parse_ex() failed */
		RETURN_FALSE;
	}

	if (key > -1) {
		switch (key) {
			case HYSS_URL_SCHEME:
				if (resource->scheme != NULL) RETVAL_STR_COPY(resource->scheme);
				break;
			case HYSS_URL_HOST:
				if (resource->host != NULL) RETVAL_STR_COPY(resource->host);
				break;
			case HYSS_URL_PORT:
				if (resource->port != 0) RETVAL_LONG(resource->port);
				break;
			case HYSS_URL_USER:
				if (resource->user != NULL) RETVAL_STR_COPY(resource->user);
				break;
			case HYSS_URL_PASS:
				if (resource->pass != NULL) RETVAL_STR_COPY(resource->pass);
				break;
			case HYSS_URL_PATH:
				if (resource->path != NULL) RETVAL_STR_COPY(resource->path);
				break;
			case HYSS_URL_QUERY:
				if (resource->query != NULL) RETVAL_STR_COPY(resource->query);
				break;
			case HYSS_URL_FRAGMENT:
				if (resource->fragment != NULL) RETVAL_STR_COPY(resource->fragment);
				break;
			default:
				hyss_error_docref(NULL, E_WARNING, "Invalid URL component identifier " GEAR_LONG_FMT, key);
				RETVAL_FALSE;
		}
		goto done;
	}

	/* allocate an array for return */
	array_init(return_value);

    /* add the various elements to the array */
	if (resource->scheme != NULL) {
		ZVAL_STR_COPY(&tmp, resource->scheme);
		gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_SCHEME), &tmp);
	}
	if (resource->host != NULL) {
		ZVAL_STR_COPY(&tmp, resource->host);
		gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_HOST), &tmp);
	}
	if (resource->port != 0) {
		ZVAL_LONG(&tmp, resource->port);
		gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_PORT), &tmp);
	}
	if (resource->user != NULL) {
		ZVAL_STR_COPY(&tmp, resource->user);
		gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_USER), &tmp);
	}
	if (resource->pass != NULL) {
		ZVAL_STR_COPY(&tmp, resource->pass);
		gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_PASS), &tmp);
	}
	if (resource->path != NULL) {
		ZVAL_STR_COPY(&tmp, resource->path);
		gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_PATH), &tmp);
	}
	if (resource->query != NULL) {
		ZVAL_STR_COPY(&tmp, resource->query);
		gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_QUERY), &tmp);
	}
	if (resource->fragment != NULL) {
		ZVAL_STR_COPY(&tmp, resource->fragment);
		gear_hash_add_new(Z_ARRVAL_P(return_value), ZSTR_KNOWN(GEAR_STR_FRAGMENT), &tmp);
	}
done:
	hyss_url_free(resource);
}
/* }}} */

/* {{{ hyss_htoi
 */
static int hyss_htoi(char *s)
{
	int value;
	int c;

	c = ((unsigned char *)s)[0];
	if (isupper(c))
		c = tolower(c);
	value = (c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10) * 16;

	c = ((unsigned char *)s)[1];
	if (isupper(c))
		c = tolower(c);
	value += c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10;

	return (value);
}
/* }}} */

/* rfc1738:

   ...The characters ";",
   "/", "?", ":", "@", "=" and "&" are the characters which may be
   reserved for special meaning within a scheme...

   ...Thus, only alphanumerics, the special characters "$-_.+!*'(),", and
   reserved characters used for their reserved purposes may be used
   unencoded within a URL...

   For added safety, we only leave -_. unencoded.
 */

static unsigned char hexchars[] = "0123456789ABCDEF";

/* {{{ hyss_url_encode
 */
HYSSAPI gear_string *hyss_url_encode(char const *s, size_t len)
{
	register unsigned char c;
	unsigned char *to;
	unsigned char const *from, *end;
	gear_string *start;

	from = (unsigned char *)s;
	end = (unsigned char *)s + len;
	start = gear_string_safe_alloc(3, len, 0, 0);
	to = (unsigned char*)ZSTR_VAL(start);

	while (from < end) {
		c = *from++;

		if (c == ' ') {
			*to++ = '+';
#ifndef CHARSET_EBCDIC
		} else if ((c < '0' && c != '-' && c != '.') ||
				   (c < 'A' && c > '9') ||
				   (c > 'Z' && c < 'a' && c != '_') ||
				   (c > 'z')) {
			to[0] = '%';
			to[1] = hexchars[c >> 4];
			to[2] = hexchars[c & 15];
			to += 3;
#else /*CHARSET_EBCDIC*/
		} else if (!isalnum(c) && strchr("_-.", c) == NULL) {
			/* Allow only alphanumeric chars and '_', '-', '.'; escape the rest */
			to[0] = '%';
			to[1] = hexchars[os_toascii[c] >> 4];
			to[2] = hexchars[os_toascii[c] & 15];
			to += 3;
#endif /*CHARSET_EBCDIC*/
		} else {
			*to++ = c;
		}
	}
	*to = '\0';

	start = gear_string_truncate(start, to - (unsigned char*)ZSTR_VAL(start), 0);

	return start;
}
/* }}} */

/* {{{ proto string urlencode(string str)
   URL-encodes string */
HYSS_FUNCTION(urlencode)
{
	gear_string *in_str;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(in_str)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_STR(hyss_url_encode(ZSTR_VAL(in_str), ZSTR_LEN(in_str)));
}
/* }}} */

/* {{{ proto string urldecode(string str)
   Decodes URL-encoded string */
HYSS_FUNCTION(urldecode)
{
	gear_string *in_str, *out_str;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(in_str)
	GEAR_PARSE_PARAMETERS_END();

	out_str = gear_string_init(ZSTR_VAL(in_str), ZSTR_LEN(in_str), 0);
	ZSTR_LEN(out_str) = hyss_url_decode(ZSTR_VAL(out_str), ZSTR_LEN(out_str));

    RETURN_NEW_STR(out_str);
}
/* }}} */

/* {{{ hyss_url_decode
 */
HYSSAPI size_t hyss_url_decode(char *str, size_t len)
{
	char *dest = str;
	char *data = str;

	while (len--) {
		if (*data == '+') {
			*dest = ' ';
		}
		else if (*data == '%' && len >= 2 && isxdigit((int) *(data + 1))
				 && isxdigit((int) *(data + 2))) {
#ifndef CHARSET_EBCDIC
			*dest = (char) hyss_htoi(data + 1);
#else
			*dest = os_toebcdic[(char) hyss_htoi(data + 1)];
#endif
			data += 2;
			len -= 2;
		} else {
			*dest = *data;
		}
		data++;
		dest++;
	}
	*dest = '\0';
	return dest - str;
}
/* }}} */

/* {{{ hyss_raw_url_encode
 */
HYSSAPI gear_string *hyss_raw_url_encode(char const *s, size_t len)
{
	register size_t x, y;
	gear_string *str;
	char *ret;

	str = gear_string_safe_alloc(3, len, 0, 0);
	ret = ZSTR_VAL(str);
	for (x = 0, y = 0; len--; x++, y++) {
		char c = s[x];

		ret[y] = c;
#ifndef CHARSET_EBCDIC
		if ((c < '0' && c != '-' &&  c != '.') ||
			(c < 'A' && c > '9') ||
			(c > 'Z' && c < 'a' && c != '_') ||
			(c > 'z' && c != '~')) {
			ret[y++] = '%';
			ret[y++] = hexchars[(unsigned char) c >> 4];
			ret[y] = hexchars[(unsigned char) c & 15];
#else /*CHARSET_EBCDIC*/
		if (!isalnum(c) && strchr("_-.~", c) != NULL) {
			ret[y++] = '%';
			ret[y++] = hexchars[os_toascii[(unsigned char) c] >> 4];
			ret[y] = hexchars[os_toascii[(unsigned char) c] & 15];
#endif /*CHARSET_EBCDIC*/
		}
	}
	ret[y] = '\0';
	str = gear_string_truncate(str, y, 0);

	return str;
}
/* }}} */

/* {{{ proto string rawurlencode(string str)
   URL-encodes string */
HYSS_FUNCTION(rawurlencode)
{
	gear_string *in_str;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(in_str)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_STR(hyss_raw_url_encode(ZSTR_VAL(in_str), ZSTR_LEN(in_str)));
}
/* }}} */

/* {{{ proto string rawurldecode(string str)
   Decodes URL-encodes string */
HYSS_FUNCTION(rawurldecode)
{
	gear_string *in_str, *out_str;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(in_str)
	GEAR_PARSE_PARAMETERS_END();

	out_str = gear_string_init(ZSTR_VAL(in_str), ZSTR_LEN(in_str), 0);
	ZSTR_LEN(out_str) = hyss_raw_url_decode(ZSTR_VAL(out_str), ZSTR_LEN(out_str));

    RETURN_NEW_STR(out_str);
}
/* }}} */

/* {{{ hyss_raw_url_decode
 */
HYSSAPI size_t hyss_raw_url_decode(char *str, size_t len)
{
	char *dest = str;
	char *data = str;

	while (len--) {
		if (*data == '%' && len >= 2 && isxdigit((int) *(data + 1))
			&& isxdigit((int) *(data + 2))) {
#ifndef CHARSET_EBCDIC
			*dest = (char) hyss_htoi(data + 1);
#else
			*dest = os_toebcdic[(char) hyss_htoi(data + 1)];
#endif
			data += 2;
			len -= 2;
		} else {
			*dest = *data;
		}
		data++;
		dest++;
	}
	*dest = '\0';
	return dest - str;
}
/* }}} */

/* {{{ proto array get_headers(string url[, int format[, resource context]])
   fetches all the headers sent by the server in response to a HTTP request */
HYSS_FUNCTION(get_headers)
{
	char *url;
	size_t url_len;
	hyss_stream *stream;
	zval *prev_val, *hdr = NULL, *h;
	HashTable *hashT;
	gear_long format = 0;
	zval *zcontext = NULL;
	hyss_stream_context *context;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_STRING(url, url_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(format)
		Z_PARAM_RESOURCE_EX(zcontext, 1, 0)
	GEAR_PARSE_PARAMETERS_END();

	context = hyss_stream_context_from_zval(zcontext, 0);

	if (!(stream = hyss_stream_open_wrapper_ex(url, "r", REPORT_ERRORS | STREAM_USE_URL | STREAM_ONLY_GET_HEADERS, NULL, context))) {
		RETURN_FALSE;
	}

	if (Z_TYPE(stream->wrapperdata) != IS_ARRAY) {
		hyss_stream_close(stream);
		RETURN_FALSE;
	}

	array_init(return_value);

	/* check for curl-wrappers that provide headers via a special "headers" element */
	if ((h = gear_hash_str_find(HASH_OF(&stream->wrapperdata), "headers", sizeof("headers")-1)) != NULL && Z_TYPE_P(h) == IS_ARRAY) {
		/* curl-wrappers don't load data until the 1st read */
		if (!Z_ARRVAL_P(h)->nNumOfElements) {
			hyss_stream_getc(stream);
		}
		h = gear_hash_str_find(HASH_OF(&stream->wrapperdata), "headers", sizeof("headers")-1);
		hashT = Z_ARRVAL_P(h);
	} else {
		hashT = HASH_OF(&stream->wrapperdata);
	}

	GEAR_HASH_FOREACH_VAL(hashT, hdr) {
		if (Z_TYPE_P(hdr) != IS_STRING) {
			continue;
		}
		if (!format) {
no_name_header:
			add_next_index_str(return_value, gear_string_copy(Z_STR_P(hdr)));
		} else {
			char c;
			char *s, *p;

			if ((p = strchr(Z_STRVAL_P(hdr), ':'))) {
				c = *p;
				*p = '\0';
				s = p + 1;
				while (isspace((int)*(unsigned char *)s)) {
					s++;
				}

				if ((prev_val = gear_hash_str_find(Z_ARRVAL_P(return_value), Z_STRVAL_P(hdr), (p - Z_STRVAL_P(hdr)))) == NULL) {
					add_assoc_stringl_ex(return_value, Z_STRVAL_P(hdr), (p - Z_STRVAL_P(hdr)), s, (Z_STRLEN_P(hdr) - (s - Z_STRVAL_P(hdr))));
				} else { /* some headers may occur more than once, therefor we need to remake the string into an array */
					convert_to_array(prev_val);
					add_next_index_stringl(prev_val, s, (Z_STRLEN_P(hdr) - (s - Z_STRVAL_P(hdr))));
				}

				*p = c;
			} else {
				goto no_name_header;
			}
		}
	} GEAR_HASH_FOREACH_END();

	hyss_stream_close(stream);
}
/* }}} */

