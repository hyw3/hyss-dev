/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PASSWORD_H
#define HYSS_PASSWORD_H

HYSS_FUNCTION(password_hash);
HYSS_FUNCTION(password_verify);
HYSS_FUNCTION(password_needs_rehash);
HYSS_FUNCTION(password_get_info);

HYSS_MINIT_FUNCTION(password);

#define HYSS_PASSWORD_DEFAULT    HYSS_PASSWORD_BCRYPT
#define HYSS_PASSWORD_BCRYPT_COST 10

#if HAVE_ARGON2LIB
#define HYSS_PASSWORD_ARGON2_MEMORY_COST 1<<10
#define HYSS_PASSWORD_ARGON2_TIME_COST 2
#define HYSS_PASSWORD_ARGON2_THREADS 2
#endif

typedef enum {
    HYSS_PASSWORD_UNKNOWN,
    HYSS_PASSWORD_BCRYPT,
#if HAVE_ARGON2LIB
    HYSS_PASSWORD_ARGON2I,
    HYSS_PASSWORD_ARGON2ID,
#endif
} hyss_password_algo;

#endif


