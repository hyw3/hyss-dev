/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_MT_RAND_H
#define HYSS_MT_RAND_H

#include "hyss_lcg.h"
#include "hyss_rand.h"

#define HYSS_MT_RAND_MAX ((gear_long) (0x7FFFFFFF)) /* (1<<31) - 1 */

#define MT_RAND_MT19937 0
#define MT_RAND_HYSS 1

HYSSAPI void hyss_mt_srand(uint32_t seed);
HYSSAPI uint32_t hyss_mt_rand(void);
HYSSAPI gear_long hyss_mt_rand_range(gear_long min, gear_long max);
HYSSAPI gear_long hyss_mt_rand_common(gear_long min, gear_long max);

HYSS_MINIT_FUNCTION(mt_rand);

#endif	/* HYSS_MT_RAND_H */
