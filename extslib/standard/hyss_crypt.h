/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_CRYPT_H
#define HYSS_CRYPT_H

HYSSAPI gear_string *hyss_crypt(const char *password, const int pass_len, const char *salt, int salt_len, gear_bool quiet);
HYSS_FUNCTION(crypt);
HYSS_MINIT_FUNCTION(crypt);
HYSS_MSHUTDOWN_FUNCTION(crypt);
HYSS_RINIT_FUNCTION(crypt);

#endif

