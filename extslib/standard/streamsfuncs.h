/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Flags for stream_socket_client */
#define HYSS_STREAM_CLIENT_PERSISTENT		1
#define HYSS_STREAM_CLIENT_ASYNC_CONNECT	2
#define HYSS_STREAM_CLIENT_CONNECT		4

HYSS_FUNCTION(stream_socket_client);
HYSS_FUNCTION(stream_socket_server);
HYSS_FUNCTION(stream_socket_accept);
HYSS_FUNCTION(stream_socket_get_name);
HYSS_FUNCTION(stream_socket_recvfrom);
HYSS_FUNCTION(stream_socket_sendto);

HYSS_FUNCTION(stream_copy_to_stream);
HYSS_FUNCTION(stream_get_contents);

HYSS_FUNCTION(stream_set_blocking);
HYSS_FUNCTION(stream_select);
HYSS_FUNCTION(stream_set_timeout);
HYSS_FUNCTION(stream_set_read_buffer);
HYSS_FUNCTION(stream_set_write_buffer);
HYSS_FUNCTION(stream_set_chunk_size);
HYSS_FUNCTION(stream_get_transports);
HYSS_FUNCTION(stream_get_wrappers);
HYSS_FUNCTION(stream_get_line);
HYSS_FUNCTION(stream_get_meta_data);
HYSS_FUNCTION(stream_wrapper_register);
HYSS_FUNCTION(stream_wrapper_unregister);
HYSS_FUNCTION(stream_wrapper_restore);
HYSS_FUNCTION(stream_context_create);
HYSS_FUNCTION(stream_context_set_params);
HYSS_FUNCTION(stream_context_get_params);
HYSS_FUNCTION(stream_context_set_option);
HYSS_FUNCTION(stream_context_get_options);
HYSS_FUNCTION(stream_context_get_default);
HYSS_FUNCTION(stream_context_set_default);
HYSS_FUNCTION(stream_filter_prepend);
HYSS_FUNCTION(stream_filter_append);
HYSS_FUNCTION(stream_filter_remove);
HYSS_FUNCTION(stream_socket_enable_crypto);
HYSS_FUNCTION(stream_socket_shutdown);
HYSS_FUNCTION(stream_resolve_include_path);
HYSS_FUNCTION(stream_is_local);
HYSS_FUNCTION(stream_supports_lock);
HYSS_FUNCTION(stream_isatty);
#ifdef HYSS_WIN32
HYSS_FUNCTION(sapi_windows_vt100_support);
#endif

#if HAVE_SOCKETPAIR
HYSS_FUNCTION(stream_socket_pair);
#endif

