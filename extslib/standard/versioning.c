/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <sys/types.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "hyss.h"
#include "hyss_versioning.h"

#define sign(n) ((n)<0?-1:((n)>0?1:0))

/* {{{ hyss_canonicalize_version() */

HYSSAPI char *
hyss_canonicalize_version(const char *version)
{
    size_t len = strlen(version);
    char *buf = safe_emalloc(len, 2, 1), *q, lp, lq;
    const char *p;

    if (len == 0) {
        *buf = '\0';
        return buf;
    }

    p = version;
    q = buf;
    *q++ = lp = *p++;

    while (*p) {
/*  s/[-_+]/./g;
 *  s/([^\d\.])([^\D\.])/$1.$2/g;
 *  s/([^\D\.])([^\d\.])/$1.$2/g;
 */
#define isdig(x) (isdigit(x)&&(x)!='.')
#define isndig(x) (!isdigit(x)&&(x)!='.')
#define isspecialver(x) ((x)=='-'||(x)=='_'||(x)=='+')

		lq = *(q - 1);
		if (isspecialver(*p)) {
			if (lq != '.') {
				*q++ = '.';
			}
		} else if ((isndig(lp) && isdig(*p)) || (isdig(lp) && isndig(*p))) {
			if (lq != '.') {
				*q++ = '.';
			}
			*q++ = *p;
		} else if (!isalnum(*p)) {
			if (lq != '.') {
				*q++ = '.';
			}
		} else {
			*q++ = *p;
		}
		lp = *p++;
    }
    *q++ = '\0';
    return buf;
}

/* }}} */
/* {{{ compare_special_version_forms() */

typedef struct {
	const char *name;
	int order;
} special_forms_t;

static int
compare_special_version_forms(char *form1, char *form2)
{
	int found1 = -1, found2 = -1;
	special_forms_t special_forms[11] = {
		{"dev", 0},
		{"alpha", 1},
		{"a", 1},
		{"beta", 2},
		{"b", 2},
		{"RC", 3},
		{"rc", 3},
		{"#", 4},
		{"pl", 5},
		{"p", 5},
		{NULL, 0},
	};
	special_forms_t *pp;

	for (pp = special_forms; pp && pp->name; pp++) {
		if (strncmp(form1, pp->name, strlen(pp->name)) == 0) {
			found1 = pp->order;
			break;
		}
	}
	for (pp = special_forms; pp && pp->name; pp++) {
		if (strncmp(form2, pp->name, strlen(pp->name)) == 0) {
			found2 = pp->order;
			break;
		}
	}
	return sign(found1 - found2);
}

/* }}} */
/* {{{ hyss_version_compare() */

HYSSAPI int
hyss_version_compare(const char *orig_ver1, const char *orig_ver2)
{
	char *ver1;
	char *ver2;
	char *p1, *p2, *n1, *n2;
	long l1, l2;
	int compare = 0;

	if (!*orig_ver1 || !*orig_ver2) {
		if (!*orig_ver1 && !*orig_ver2) {
			return 0;
		} else {
			return *orig_ver1 ? 1 : -1;
		}
	}
	if (orig_ver1[0] == '#') {
		ver1 = estrdup(orig_ver1);
	} else {
		ver1 = hyss_canonicalize_version(orig_ver1);
	}
	if (orig_ver2[0] == '#') {
		ver2 = estrdup(orig_ver2);
	} else {
		ver2 = hyss_canonicalize_version(orig_ver2);
	}
	p1 = n1 = ver1;
	p2 = n2 = ver2;
	while (*p1 && *p2 && n1 && n2) {
		if ((n1 = strchr(p1, '.')) != NULL) {
			*n1 = '\0';
		}
		if ((n2 = strchr(p2, '.')) != NULL) {
			*n2 = '\0';
		}
		if (isdigit(*p1) && isdigit(*p2)) {
			/* compare element numerically */
			l1 = strtol(p1, NULL, 10);
			l2 = strtol(p2, NULL, 10);
			compare = sign(l1 - l2);
		} else if (!isdigit(*p1) && !isdigit(*p2)) {
			/* compare element names */
			compare = compare_special_version_forms(p1, p2);
		} else {
			/* mix of names and numbers */
			if (isdigit(*p1)) {
				compare = compare_special_version_forms("#N#", p2);
			} else {
				compare = compare_special_version_forms(p1, "#N#");
			}
		}
		if (compare != 0) {
			break;
		}
		if (n1 != NULL) {
			p1 = n1 + 1;
		}
		if (n2 != NULL) {
			p2 = n2 + 1;
		}
	}
	if (compare == 0) {
		if (n1 != NULL) {
			if (isdigit(*p1)) {
				compare = 1;
			} else {
				compare = hyss_version_compare(p1, "#N#");
			}
		} else if (n2 != NULL) {
			if (isdigit(*p2)) {
				compare = -1;
			} else {
				compare = hyss_version_compare("#N#", p2);
			}
		}
	}
	efree(ver1);
	efree(ver2);
	return compare;
}

/* }}} */
/* {{{ proto int version_compare(string ver1, string ver2 [, string oper])
  Compares two "HYSS-standardized" version number strings */

HYSS_FUNCTION(version_compare)
{
	char *v1, *v2, *op = NULL;
	size_t v1_len, v2_len, op_len = 0;
	int compare;

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STRING(v1, v1_len)
		Z_PARAM_STRING(v2, v2_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(op, op_len)
	GEAR_PARSE_PARAMETERS_END();

	compare = hyss_version_compare(v1, v2);
	if (!op) {
		RETURN_LONG(compare);
	}
	if (!strncmp(op, "<", op_len) || !strncmp(op, "lt", op_len)) {
		RETURN_BOOL(compare == -1);
	}
	if (!strncmp(op, "<=", op_len) || !strncmp(op, "le", op_len)) {
		RETURN_BOOL(compare != 1);
	}
	if (!strncmp(op, ">", op_len) || !strncmp(op, "gt", op_len)) {
		RETURN_BOOL(compare == 1);
	}
	if (!strncmp(op, ">=", op_len) || !strncmp(op, "ge", op_len)) {
		RETURN_BOOL(compare != -1);
	}
	if (!strncmp(op, "==", op_len) || !strncmp(op, "=", op_len) || !strncmp(op, "eq", op_len)) {
		RETURN_BOOL(compare == 0);
	}
	if (!strncmp(op, "!=", op_len) || !strncmp(op, "<>", op_len) || !strncmp(op, "ne", op_len)) {
		RETURN_BOOL(compare != 0);
	}
	RETURN_NULL();
}

/* }}} */

