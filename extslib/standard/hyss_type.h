/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_TYPE_H
#define HYSS_TYPE_H

HYSS_FUNCTION(intval);
HYSS_FUNCTION(floatval);
HYSS_FUNCTION(strval);
HYSS_FUNCTION(boolval);
HYSS_FUNCTION(gettype);
HYSS_FUNCTION(settype);
HYSS_FUNCTION(is_null);
HYSS_FUNCTION(is_resource);
HYSS_FUNCTION(is_bool);
HYSS_FUNCTION(is_int);
HYSS_FUNCTION(is_float);
HYSS_FUNCTION(is_numeric);
HYSS_FUNCTION(is_string);
HYSS_FUNCTION(is_array);
HYSS_FUNCTION(is_object);
HYSS_FUNCTION(is_scalar);
HYSS_FUNCTION(is_callable);
HYSS_FUNCTION(is_iterable);
HYSS_FUNCTION(is_countable);

#endif
