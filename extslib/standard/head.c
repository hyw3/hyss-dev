/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "hyss.h"
#include "extslib/standard/hyss_standard.h"
#include "extslib/date/hyss_date.h"
#include "SAPI.h"
#include "hyss_main.h"
#include "head.h"
#ifdef TM_IN_SYS_TIME
#include <sys/time.h>
#else
#include <time.h>
#endif

#include "hyss_globals.h"
#include "gear_smart_str.h"


/* Implementation of the language Header() function */
/* {{{ proto void header(string header [, bool replace, [int http_response_code]])
   Sends a raw HTTP header */
HYSS_FUNCTION(header)
{
	gear_bool rep = 1;
	sapi_header_line ctr = {0};
	size_t len;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_STRING(ctr.line, len)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(rep)
		Z_PARAM_LONG(ctr.response_code)
	GEAR_PARSE_PARAMETERS_END();

	ctr.line_len = (uint32_t)len;
	sapi_header_op(rep ? SAPI_HEADER_REPLACE:SAPI_HEADER_ADD, &ctr);
}
/* }}} */

/* {{{ proto void header_remove([string name])
   Removes an HTTP header previously set using header() */
HYSS_FUNCTION(header_remove)
{
	sapi_header_line ctr = {0};
	size_t len = 0;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(ctr.line, len)
	GEAR_PARSE_PARAMETERS_END();

	ctr.line_len = (uint32_t)len;
	sapi_header_op(GEAR_NUM_ARGS() == 0 ? SAPI_HEADER_DELETE_ALL : SAPI_HEADER_DELETE, &ctr);
}
/* }}} */

HYSSAPI int hyss_header(void)
{
	if (sapi_send_headers()==FAILURE || SG(request_info).headers_only) {
		return 0; /* don't allow output */
	} else {
		return 1; /* allow output */
	}
}

HYSSAPI int hyss_setcookie(gear_string *name, gear_string *value, time_t expires, gear_string *path, gear_string *domain, int secure, int httponly, gear_string *samesite, int url_encode)
{
	gear_string *dt;
	sapi_header_line ctr = {0};
	int result;
	smart_str buf = {0};

	if (!ZSTR_LEN(name)) {
		gear_error( E_WARNING, "Cookie names must not be empty" );
		return FAILURE;
	} else if (strpbrk(ZSTR_VAL(name), "=,; \t\r\n\013\014") != NULL) {   /* man isspace for \013 and \014 */
		gear_error(E_WARNING, "Cookie names cannot contain any of the following '=,; \\t\\r\\n\\013\\014'" );
		return FAILURE;
	}

	if (!url_encode && value &&
			strpbrk(ZSTR_VAL(value), ",; \t\r\n\013\014") != NULL) { /* man isspace for \013 and \014 */
		gear_error(E_WARNING, "Cookie values cannot contain any of the following ',; \\t\\r\\n\\013\\014'" );
		return FAILURE;
	}

	if (path && strpbrk(ZSTR_VAL(path), ",; \t\r\n\013\014") != NULL) { /* man isspace for \013 and \014 */
		gear_error(E_WARNING, "Cookie paths cannot contain any of the following ',; \\t\\r\\n\\013\\014'" );
		return FAILURE;
	}

	if (domain && strpbrk(ZSTR_VAL(domain), ",; \t\r\n\013\014") != NULL) { /* man isspace for \013 and \014 */
		gear_error(E_WARNING, "Cookie domains cannot contain any of the following ',; \\t\\r\\n\\013\\014'" );
		return FAILURE;
	}

	if (value == NULL || ZSTR_LEN(value) == 0) {
		/*
		 * MSIE doesn't delete a cookie when you set it to a null value
		 * so in order to force cookies to be deleted, even on MSIE, we
		 * pick an expiry date in the past
		 */
		dt = hyss_format_date("D, d-M-Y H:i:s T", sizeof("D, d-M-Y H:i:s T")-1, 1, 0);
		smart_str_appends(&buf, "Set-Cookie: ");
		smart_str_append(&buf, name);
		smart_str_appends(&buf, "=deleted; expires=");
		smart_str_append(&buf, dt);
		smart_str_appends(&buf, "; Max-Age=0");
		gear_string_free(dt);
	} else {
		smart_str_appends(&buf, "Set-Cookie: ");
		smart_str_append(&buf, name);
		smart_str_appendc(&buf, '=');
		if (url_encode) {
			gear_string *encoded_value = hyss_url_encode(ZSTR_VAL(value), ZSTR_LEN(value));
			smart_str_append(&buf, encoded_value);
			gear_string_release_ex(encoded_value, 0);
		} else {
			smart_str_append(&buf, value);
		}
		if (expires > 0) {
			const char *p;
			double diff;

			smart_str_appends(&buf, COOKIE_EXPIRES);
			dt = hyss_format_date("D, d-M-Y H:i:s T", sizeof("D, d-M-Y H:i:s T")-1, expires, 0);
			/* check to make sure that the year does not exceed 4 digits in length */
			p = gear_memrchr(ZSTR_VAL(dt), '-', ZSTR_LEN(dt));
			if (!p || *(p + 5) != ' ') {
				gear_string_free(dt);
				smart_str_free(&buf);
				gear_error(E_WARNING, "Expiry date cannot have a year greater than 9999");
				return FAILURE;
			}

			smart_str_append(&buf, dt);
			gear_string_free(dt);

			diff = difftime(expires, time(NULL));
			if (diff < 0) {
				diff = 0;
			}

			smart_str_appends(&buf, COOKIE_MAX_AGE);
			smart_str_append_long(&buf, (gear_long) diff);
		}
	}

	if (path && ZSTR_LEN(path)) {
		smart_str_appends(&buf, COOKIE_PATH);
		smart_str_append(&buf, path);
	}
	if (domain && ZSTR_LEN(domain)) {
		smart_str_appends(&buf, COOKIE_DOMAIN);
		smart_str_append(&buf, domain);
	}
	if (secure) {
		smart_str_appends(&buf, COOKIE_SECURE);
	}
	if (httponly) {
		smart_str_appends(&buf, COOKIE_HTTPONLY);
	}
	if (samesite && ZSTR_LEN(samesite)) {
		smart_str_appends(&buf, COOKIE_SAMESITE);
		smart_str_append(&buf, samesite);
	}

	ctr.line = ZSTR_VAL(buf.s);
	ctr.line_len = (uint32_t) ZSTR_LEN(buf.s);

	result = sapi_header_op(SAPI_HEADER_ADD, &ctr);
	gear_string_release(buf.s);
	return result;
}

static void hyss_head_parse_cookie_options_array(zval *options, gear_long *expires, gear_string **path, gear_string **domain, gear_bool *secure, gear_bool *httponly, gear_string **samesite) {
	int found = 0;
	gear_string *key;
	zval *value;

	GEAR_HASH_FOREACH_STR_KEY_VAL(Z_ARRVAL_P(options), key, value) {
		if (key) {
			if (gear_string_equals_literal_ci(key, "expires")) {
				*expires = zval_get_long(value);
				found++;
			} else if (gear_string_equals_literal_ci(key, "path")) {
				*path = zval_get_string(value);
				found++;
			} else if (gear_string_equals_literal_ci(key, "domain")) {
				*domain = zval_get_string(value);
				found++;
			} else if (gear_string_equals_literal_ci(key, "secure")) {
				*secure = zval_is_true(value);
				found++;
			} else if (gear_string_equals_literal_ci(key, "httponly")) {
				*httponly = zval_is_true(value);
				found++;
			} else if (gear_string_equals_literal_ci(key, "samesite")) {
				*samesite = zval_get_string(value);
				found++;
			} else {
				hyss_error_docref(NULL, E_WARNING, "Unrecognized key '%s' found in the options array", ZSTR_VAL(key));
			}
		} else {
			hyss_error_docref(NULL, E_WARNING, "Numeric key found in the options array");
		}
	} GEAR_HASH_FOREACH_END();

	/* Array is not empty but no valid keys were found */
	if (found == 0 && gear_hash_num_elements(Z_ARRVAL_P(options)) > 0) {
		hyss_error_docref(NULL, E_WARNING, "No valid options were found in the given array");
	}
}

/* {{{ proto bool setcookie(string name [, string value [, int expires [, string path [, string domain [, bool secure[, bool httponly]]]]]])
                  setcookie(string name [, string value [, array options]])
   Send a cookie */
HYSS_FUNCTION(setcookie)
{
	zval *expires_or_options = NULL;
	gear_string *name, *value = NULL, *path = NULL, *domain = NULL, *samesite = NULL;
	gear_long expires = 0;
	gear_bool secure = 0, httponly = 0;

	GEAR_PARSE_PARAMETERS_START(1, 7)
		Z_PARAM_STR(name)
		Z_PARAM_OPTIONAL
		Z_PARAM_STR(value)
		Z_PARAM_ZVAL(expires_or_options)
		Z_PARAM_STR(path)
		Z_PARAM_STR(domain)
		Z_PARAM_BOOL(secure)
		Z_PARAM_BOOL(httponly)
	GEAR_PARSE_PARAMETERS_END();

	if (expires_or_options) {
		if (Z_TYPE_P(expires_or_options) == IS_ARRAY) {
			if (UNEXPECTED(GEAR_NUM_ARGS() > 3)) {
				hyss_error_docref(NULL, E_WARNING, "Cannot pass arguments after the options array");
				RETURN_FALSE;
			}
			hyss_head_parse_cookie_options_array(expires_or_options, &expires, &path, &domain, &secure, &httponly, &samesite);
		} else {
			expires = zval_get_long(expires_or_options);
		}
	}

	if (hyss_setcookie(name, value, expires, path, domain, secure, httponly, samesite, 1) == SUCCESS) {
		RETVAL_TRUE;
	} else {
		RETVAL_FALSE;
	}

	if (expires_or_options && Z_TYPE_P(expires_or_options) == IS_ARRAY) {
		if (path) {
			gear_string_release(path);
		}
		if (domain) {
			gear_string_release(domain);
		}
		if (samesite) {
			gear_string_release(samesite);
		}
	}
}
/* }}} */

/* {{{ proto bool setrawcookie(string name [, string value [, int expires [, string path [, string domain [, bool secure[, bool httponly]]]]]])
                  setrawcookie(string name [, string value [, array options]])
   Send a cookie with no url encoding of the value */
HYSS_FUNCTION(setrawcookie)
{
	zval *expires_or_options = NULL;
	gear_string *name, *value = NULL, *path = NULL, *domain = NULL, *samesite = NULL;
	gear_long expires = 0;
	gear_bool secure = 0, httponly = 0;

	GEAR_PARSE_PARAMETERS_START(1, 7)
		Z_PARAM_STR(name)
		Z_PARAM_OPTIONAL
		Z_PARAM_STR(value)
		Z_PARAM_ZVAL(expires_or_options)
		Z_PARAM_STR(path)
		Z_PARAM_STR(domain)
		Z_PARAM_BOOL(secure)
		Z_PARAM_BOOL(httponly)
	GEAR_PARSE_PARAMETERS_END();

	if (expires_or_options) {
		if (Z_TYPE_P(expires_or_options) == IS_ARRAY) {
			if (UNEXPECTED(GEAR_NUM_ARGS() > 3)) {
				hyss_error_docref(NULL, E_WARNING, "Cannot pass arguments after the options array");
				RETURN_FALSE;
			}
			hyss_head_parse_cookie_options_array(expires_or_options, &expires, &path, &domain, &secure, &httponly, &samesite);
		} else {
			expires = zval_get_long(expires_or_options);
		}
	}

	if (hyss_setcookie(name, value, expires, path, domain, secure, httponly, samesite, 0) == SUCCESS) {
		RETVAL_TRUE;
	} else {
		RETVAL_FALSE;
	}

	if (expires_or_options && Z_TYPE_P(expires_or_options) == IS_ARRAY) {
		if (path) {
			gear_string_release(path);
		}
		if (domain) {
			gear_string_release(domain);
		}
		if (samesite) {
			gear_string_release(samesite);
		}
	}
}
/* }}} */


/* {{{ proto bool headers_sent([string &$file [, int &$line]])
   Returns true if headers have already been sent, false otherwise */
HYSS_FUNCTION(headers_sent)
{
	zval *arg1 = NULL, *arg2 = NULL;
	const char *file="";
	int line=0;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_ZVAL_DEREF(arg1)
		Z_PARAM_ZVAL_DEREF(arg2)
	GEAR_PARSE_PARAMETERS_END();

	if (SG(headers_sent)) {
		line = hyss_output_get_start_lineno();
		file = hyss_output_get_start_filename();
	}

	switch(GEAR_NUM_ARGS()) {
	case 2:
		zval_ptr_dtor(arg2);
		ZVAL_LONG(arg2, line);
	case 1:
		zval_ptr_dtor(arg1);
		if (file) {
			ZVAL_STRING(arg1, file);
		} else {
			ZVAL_EMPTY_STRING(arg1);
		}
		break;
	}

	if (SG(headers_sent)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ hyss_head_apply_header_list_to_hash
   Turn an llist of sapi_header_struct headers into a numerically indexed zval hash */
static void hyss_head_apply_header_list_to_hash(void *data, void *arg)
{
	sapi_header_struct *sapi_header = (sapi_header_struct *)data;

	if (arg && sapi_header) {
		add_next_index_string((zval *)arg, (char *)(sapi_header->header));
	}
}

/* {{{ proto array headers_list(void)
   Return list of headers to be sent / already sent */
HYSS_FUNCTION(headers_list)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);
	gear_llist_apply_with_argument(&SG(sapi_headers).headers, hyss_head_apply_header_list_to_hash, return_value);
}
/* }}} */

/* {{{ proto int http_response_code([int response_code])
   Sets a response code, or returns the current HTTP response code */
HYSS_FUNCTION(http_response_code)
{
	gear_long response_code = 0;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(response_code)
	GEAR_PARSE_PARAMETERS_END();

	if (response_code)
	{
		gear_long old_response_code;

		old_response_code = SG(sapi_headers).http_response_code;
		SG(sapi_headers).http_response_code = (int)response_code;

		if (old_response_code) {
			RETURN_LONG(old_response_code);
		}

		RETURN_TRUE;
	}

	if (!SG(sapi_headers).http_response_code) {
		RETURN_FALSE;
	}

	RETURN_LONG(SG(sapi_headers).http_response_code);
}
/* }}} */

