/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MD5_H
#define MD5_H

HYSSAPI void make_digest(char *md5str, const unsigned char *digest);
HYSSAPI void make_digest_ex(char *md5str, const unsigned char *digest, int len);

HYSS_NAMED_FUNCTION(hyss_if_md5);
HYSS_NAMED_FUNCTION(hyss_if_md5_file);

#include "extslib/standard/basic_functions.h"

/*
 * This is an OpenSSL-compatible implementation of the RSA Data Security,
 * Inc. MD5 Message-Digest Algorithm (RFC 1321).
 *
 * Written by Solar Designer <solar at openwall.com> in 2001, and placed
 * in the public domain.  There's absolutely no warranty.
 *
 * See md5.c for more information.
 */

/* MD5 context. */
typedef struct {
	uint32_t lo, hi;
	uint32_t a, b, c, d;
	unsigned char buffer[64];
	uint32_t block[16];
} HYSS_MD5_CTX;

HYSSAPI void HYSS_MD5Init(HYSS_MD5_CTX *ctx);
HYSSAPI void HYSS_MD5Update(HYSS_MD5_CTX *ctx, const void *data, size_t size);
HYSSAPI void HYSS_MD5Final(unsigned char *result, HYSS_MD5_CTX *ctx);

#endif
