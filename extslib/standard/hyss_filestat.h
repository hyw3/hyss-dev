/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_FILESTAT_H
#define HYSS_FILESTAT_H

HYSS_RINIT_FUNCTION(filestat);
HYSS_RSHUTDOWN_FUNCTION(filestat);

HYSS_FUNCTION(realpath_cache_size);
HYSS_FUNCTION(realpath_cache_get);
HYSS_FUNCTION(clearstatcache);
HYSS_FUNCTION(fileatime);
HYSS_FUNCTION(filectime);
HYSS_FUNCTION(filegroup);
HYSS_FUNCTION(fileinode);
HYSS_FUNCTION(filemtime);
HYSS_FUNCTION(fileowner);
HYSS_FUNCTION(fileperms);
HYSS_FUNCTION(filesize);
HYSS_FUNCTION(filetype);
HYSS_FUNCTION(is_writable);
HYSS_FUNCTION(is_readable);
HYSS_FUNCTION(is_executable);
HYSS_FUNCTION(is_file);
HYSS_FUNCTION(is_dir);
HYSS_FUNCTION(is_link);
HYSS_FUNCTION(file_exists);
HYSS_NAMED_FUNCTION(hyss_if_stat);
HYSS_NAMED_FUNCTION(hyss_if_lstat);
HYSS_FUNCTION(disk_total_space);
HYSS_FUNCTION(disk_free_space);
HYSS_FUNCTION(chown);
HYSS_FUNCTION(chgrp);
#if HAVE_LCHOWN
HYSS_FUNCTION(lchown);
#endif
#if HAVE_LCHOWN
HYSS_FUNCTION(lchgrp);
#endif
HYSS_FUNCTION(chmod);
#if HAVE_UTIME
HYSS_FUNCTION(touch);
#endif
HYSS_FUNCTION(clearstatcache);

#ifdef HYSS_WIN32
#define S_IRUSR S_IREAD
#define S_IWUSR S_IWRITE
#define S_IXUSR S_IEXEC
#define S_IRGRP S_IREAD
#define S_IWGRP S_IWRITE
#define S_IXGRP S_IEXEC
#define S_IROTH S_IREAD
#define S_IWOTH S_IWRITE
#define S_IXOTH S_IEXEC

#undef getgid
#define getgroups(a, b) 0
#define getgid() 1
#define getuid() 1
#endif

/* Compatibility. */
typedef size_t hyss_stat_len;

HYSSAPI void hyss_clear_stat_cache(gear_bool clear_realpath_cache, const char *filename, size_t filename_len);
HYSSAPI void hyss_stat(const char *filename, size_t filename_length, int type, zval *return_value);

/* Switches for various filestat functions: */
#define FS_PERMS    0
#define FS_INODE    1
#define FS_SIZE     2
#define FS_OWNER    3
#define FS_GROUP    4
#define FS_ATIME    5
#define FS_MTIME    6
#define FS_CTIME    7
#define FS_TYPE     8
#define FS_IS_W     9
#define FS_IS_R    10
#define FS_IS_X    11
#define FS_IS_FILE 12
#define FS_IS_DIR  13
#define FS_IS_LINK 14
#define FS_EXISTS  15
#define FS_LSTAT   16
#define FS_STAT    17

#endif /* HYSS_FILESTAT_H */
