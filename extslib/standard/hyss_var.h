/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_VAR_H
#define HYSS_VAR_H

#include "extslib/standard/basic_functions.h"
#include "gear_smart_str_public.h"

HYSS_FUNCTION(var_dump);
HYSS_FUNCTION(var_export);
HYSS_FUNCTION(debug_zval_dump);
HYSS_FUNCTION(serialize);
HYSS_FUNCTION(unserialize);
HYSS_FUNCTION(memory_get_usage);
HYSS_FUNCTION(memory_get_peak_usage);

HYSSAPI void hyss_var_dump(zval *struc, int level);
HYSSAPI void hyss_var_export(zval *struc, int level);
HYSSAPI void hyss_var_export_ex(zval *struc, int level, smart_str *buf);

HYSSAPI void hyss_debug_zval_dump(zval *struc, int level);

typedef struct hyss_serialize_data *hyss_serialize_data_t;
typedef struct hyss_unserialize_data *hyss_unserialize_data_t;

HYSSAPI void hyss_var_serialize(smart_str *buf, zval *struc, hyss_serialize_data_t *data);
HYSSAPI int hyss_var_unserialize(zval *rval, const unsigned char **p, const unsigned char *max, hyss_unserialize_data_t *var_hash);
HYSSAPI int hyss_var_unserialize_ref(zval *rval, const unsigned char **p, const unsigned char *max, hyss_unserialize_data_t *var_hash);
HYSSAPI int hyss_var_unserialize_intern(zval *rval, const unsigned char **p, const unsigned char *max, hyss_unserialize_data_t *var_hash);

HYSSAPI hyss_serialize_data_t hyss_var_serialize_init(void);
HYSSAPI void hyss_var_serialize_destroy(hyss_serialize_data_t d);
HYSSAPI hyss_unserialize_data_t hyss_var_unserialize_init(void);
HYSSAPI void hyss_var_unserialize_destroy(hyss_unserialize_data_t d);
HYSSAPI HashTable *hyss_var_unserialize_get_allowed_classes(hyss_unserialize_data_t d);
HYSSAPI void hyss_var_unserialize_set_allowed_classes(hyss_unserialize_data_t d, HashTable *classes);

#define HYSS_VAR_SERIALIZE_INIT(d) \
	(d) = hyss_var_serialize_init()

#define HYSS_VAR_SERIALIZE_DESTROY(d) \
	hyss_var_serialize_destroy(d)

#define HYSS_VAR_UNSERIALIZE_INIT(d) \
	(d) = hyss_var_unserialize_init()

#define HYSS_VAR_UNSERIALIZE_DESTROY(d) \
	hyss_var_unserialize_destroy(d)

HYSSAPI void var_replace(hyss_unserialize_data_t *var_hash, zval *ozval, zval *nzval);
HYSSAPI void var_push_dtor(hyss_unserialize_data_t *var_hash, zval *val);
HYSSAPI zval *var_tmp_var(hyss_unserialize_data_t *var_hashx);
HYSSAPI void var_destroy(hyss_unserialize_data_t *var_hash);

#endif /* HYSS_VAR_H */
