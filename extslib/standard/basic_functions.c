/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_streams.h"
#include "hyss_main.h"
#include "hyss_globals.h"
#include "hyss_variables.h"
#include "hyss_ics.h"
#include "hyss_standard.h"
#include "hyss_math.h"
#include "hyss_http.h"
#include "hyss_incomplete_class.h"
#include "hyss_getopt.h"
#include "extslib/standard/info.h"
#include "extslib/session/hyss_session.h"
#include "gear_operators.h"
#include "extslib/standard/hyss_dns.h"
#include "extslib/standard/hyss_uuencode.h"
#include "extslib/standard/hyss_mt_rand.h"

#ifdef HYSS_WIN32
#include "win32/hyss_win32_globals.h"
#include "win32/time.h"
#include "win32/ioutil.h"
#endif

typedef struct yy_buffer_state *YY_BUFFER_STATE;

#include "gear.h"
#include "gear_ics_scanner.h"
#include "gear_language_scanner.h"
#include <gear_language_parser.h>

#include "gear_portability.h"

#include <stdarg.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdio.h>

#ifndef HYSS_WIN32
#include <sys/types.h>
#include <sys/stat.h>
#endif

#ifndef HYSS_WIN32
# include <netdb.h>
#else
#include "win32/inet.h"
#endif

#if HAVE_ARPA_INET_H
# include <arpa/inet.h>
#endif

#if HAVE_UNISTD_H
# include <unistd.h>
#endif

#if HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#if HAVE_LOCALE_H
# include <locale.h>
#endif

#if HAVE_SYS_MMAN_H
# include <sys/mman.h>
#endif

#if HAVE_SYS_LOADAVG_H
# include <sys/loadavg.h>
#endif

#ifdef HYSS_WIN32
# include "win32/unistd.h"
#endif

#ifndef INADDR_NONE
#define INADDR_NONE ((gear_ulong) -1)
#endif

#include "gear_globals.h"
#include "hyss_globals.h"
#include "SAPI.h"
#include "hyss_ticks.h"

#ifdef ZTS
HYSSAPI int basic_globals_id;
#else
HYSSAPI hyss_basic_globals basic_globals;
#endif

#include "hyss_fopen_wrappers.h"
#include "streamsfuncs.h"

static gear_class_entry *incomplete_class_entry = NULL;

typedef struct _user_tick_function_entry {
	zval *arguments;
	int arg_count;
	int calling;
} user_tick_function_entry;

/* some prototypes for local functions */
static void user_shutdown_function_dtor(zval *zv);
static void user_tick_function_dtor(user_tick_function_entry *tick_function_entry);

static HashTable basic_subcAPIs;

#undef sprintf

/* {{{ arginfo */
/* {{{ main/main.c */
GEAR_BEGIN_ARG_INFO(arginfo_set_time_limit, 0)
	GEAR_ARG_INFO(0, seconds)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ main/sapi.c */
GEAR_BEGIN_ARG_INFO(arginfo_header_register_callback, 0)
	GEAR_ARG_INFO(0, callback)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ main/output.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_ob_start, 0, 0, 0)
	GEAR_ARG_INFO(0, user_function)
	GEAR_ARG_INFO(0, chunk_size)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_flush, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_clean, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_end_flush, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_end_clean, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_get_flush, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_get_clean, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_get_contents, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_get_level, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_get_length, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ob_list_handlers, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ob_get_status, 0, 0, 0)
	GEAR_ARG_INFO(0, full_status)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ob_implicit_flush, 0, 0, 0)
	GEAR_ARG_INFO(0, flag)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_output_reset_rewrite_vars, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_output_add_rewrite_var, 0)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ main/streams/userspace.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_wrapper_register, 0, 0, 2)
	GEAR_ARG_INFO(0, protocol)
	GEAR_ARG_INFO(0, classname)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_wrapper_unregister, 0)
	GEAR_ARG_INFO(0, protocol)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_wrapper_restore, 0)
	GEAR_ARG_INFO(0, protocol)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ array.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_krsort, 0, 0, 1)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, sort_flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ksort, 0, 0, 1)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, sort_flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_count, 0, 0, 1)
	GEAR_ARG_INFO(0, var)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_natsort, 0)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_natcasesort, 0)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_asort, 0, 0, 1)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, sort_flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_arsort, 0, 0, 1)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, sort_flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_sort, 0, 0, 1)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, sort_flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_rsort, 0, 0, 1)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, sort_flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_usort, 0)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, cmp_function)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_uasort, 0)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, cmp_function)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_uksort, 0)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, cmp_function)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_end, 0)
	GEAR_ARG_INFO(1, arg)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_prev, 0)
	GEAR_ARG_INFO(1, arg)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_next, 0)
	GEAR_ARG_INFO(1, arg)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reset, 0)
	GEAR_ARG_INFO(1, arg)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_current, 0)
	GEAR_ARG_INFO(0, arg)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_key, 0)
	GEAR_ARG_INFO(0, arg)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_min, 0, 0, 1)
	GEAR_ARG_VARIADIC_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_max, 0, 0, 1)
	GEAR_ARG_VARIADIC_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_walk, 0, 0, 2)
	GEAR_ARG_INFO(1, input) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, funcname)
	GEAR_ARG_INFO(0, userdata)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_walk_recursive, 0, 0, 2)
	GEAR_ARG_INFO(1, input) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, funcname)
	GEAR_ARG_INFO(0, userdata)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_in_array, 0, 0, 2)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, haystack) /* ARRAY_INFO(0, haystack, 0) */
	GEAR_ARG_INFO(0, strict)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_search, 0, 0, 2)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, haystack) /* ARRAY_INFO(0, haystack, 0) */
	GEAR_ARG_INFO(0, strict)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_extract, 0, 0, 1)
	GEAR_ARG_INFO(GEAR_SEND_PREFER_REF, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, extract_type)
	GEAR_ARG_INFO(0, prefix)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_compact, 0, 0, 1)
	GEAR_ARG_VARIADIC_INFO(0, var_names)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_fill, 0)
	GEAR_ARG_INFO(0, start_key)
	GEAR_ARG_INFO(0, num)
	GEAR_ARG_INFO(0, val)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_fill_keys, 0)
	GEAR_ARG_INFO(0, keys) /* ARRAY_INFO(0, keys, 0) */
	GEAR_ARG_INFO(0, val)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_range, 0, 0, 2)
	GEAR_ARG_INFO(0, low)
	GEAR_ARG_INFO(0, high)
	GEAR_ARG_INFO(0, step)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_shuffle, 0)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_push, 0, 0, 1)
	GEAR_ARG_INFO(1, stack) /* ARRAY_INFO(1, stack, 0) */
	GEAR_ARG_VARIADIC_INFO(0, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_pop, 0)
	GEAR_ARG_INFO(1, stack) /* ARRAY_INFO(1, stack, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_shift, 0)
	GEAR_ARG_INFO(1, stack) /* ARRAY_INFO(1, stack, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_unshift, 0, 0, 1)
	GEAR_ARG_INFO(1, stack) /* ARRAY_INFO(1, stack, 0) */
	GEAR_ARG_VARIADIC_INFO(0, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_splice, 0, 0, 2)
	GEAR_ARG_INFO(1, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, length)
	GEAR_ARG_INFO(0, replacement) /* ARRAY_INFO(0, arg, 1) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_slice, 0, 0, 2)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(1, arg, 0) */
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, length)
	GEAR_ARG_INFO(0, preserve_keys)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_merge, 0, 0, 1)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_merge_recursive, 0, 0, 1)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_replace, 0, 0, 1)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_replace_recursive, 0, 0, 1)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_keys, 0, 0, 1)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, search_value)
	GEAR_ARG_INFO(0, strict)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_key_first, 0)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_key_last, 0)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO(arginfo_array_values, 0)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_count_values, 0)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_column, 0, 0, 2)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, column_key)
	GEAR_ARG_INFO(0, index_key)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_reverse, 0, 0, 1)
	GEAR_ARG_INFO(0, input) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, preserve_keys)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_pad, 0)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, pad_size)
	GEAR_ARG_INFO(0, pad_value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_flip, 0)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_change_key_case, 0, 0, 1)
	GEAR_ARG_INFO(0, input) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, case)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_unique, 0, 0, 1)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_intersect_key, 0, 0, 2)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_intersect_ukey, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_key_compare_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_intersect, 0, 0, 2)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_uintersect, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_data_compare_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_intersect_assoc, 0, 0, 2)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_uintersect_assoc, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_data_compare_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_intersect_uassoc, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_key_compare_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_uintersect_uassoc, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_data_compare_func)
	GEAR_ARG_INFO(0, callback_key_compare_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_diff_key, 0, 0, 2)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_diff_ukey, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_key_comp_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_diff, 0, 0, 2)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_udiff, 0)
	GEAR_ARG_INFO(0, arr1)
	GEAR_ARG_INFO(0, arr2)
	GEAR_ARG_INFO(0, callback_data_comp_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_diff_assoc, 0, 0, 2)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_diff_uassoc, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_data_comp_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_udiff_assoc, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_key_comp_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_udiff_uassoc, 0)
	GEAR_ARG_INFO(0, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(0, arr2) /* ARRAY_INFO(0, arg2, 0) */
	GEAR_ARG_INFO(0, callback_data_comp_func)
	GEAR_ARG_INFO(0, callback_key_comp_func)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_multisort, 0, 0, 1)
	GEAR_ARG_INFO(GEAR_SEND_PREFER_REF, arr1) /* ARRAY_INFO(0, arg1, 0) */
	GEAR_ARG_INFO(GEAR_SEND_PREFER_REF, sort_order)
	GEAR_ARG_INFO(GEAR_SEND_PREFER_REF, sort_flags)
	GEAR_ARG_VARIADIC_INFO(GEAR_SEND_PREFER_REF, arr2)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_rand, 0, 0, 1)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, num_req)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_sum, 0)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_product, 0)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_reduce, 0, 0, 2)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, callback)
	GEAR_ARG_INFO(0, initial)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_filter, 0, 0, 1)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, callback)
    GEAR_ARG_INFO(0, use_keys)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_map, 0, 0, 2)
	GEAR_ARG_INFO(0, callback)
	GEAR_ARG_VARIADIC_INFO(0, arrays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_key_exists, 0)
	GEAR_ARG_INFO(0, key)
	GEAR_ARG_INFO(0, search)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_array_chunk, 0, 0, 2)
	GEAR_ARG_INFO(0, arg) /* ARRAY_INFO(0, arg, 0) */
	GEAR_ARG_INFO(0, size)
	GEAR_ARG_INFO(0, preserve_keys)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_array_combine, 0)
	GEAR_ARG_INFO(0, keys)   /* ARRAY_INFO(0, keys, 0) */
	GEAR_ARG_INFO(0, values) /* ARRAY_INFO(0, values, 0) */
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ basic_functions.c */
GEAR_BEGIN_ARG_INFO(arginfo_get_magic_quotes_gpc, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_get_magic_quotes_runtime, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_constant, 0)
	GEAR_ARG_INFO(0, const_name)
GEAR_END_ARG_INFO()

#ifdef HAVE_INET_NTOP
GEAR_BEGIN_ARG_INFO(arginfo_inet_ntop, 0)
	GEAR_ARG_INFO(0, in_addr)
GEAR_END_ARG_INFO()
#endif

#ifdef HAVE_INET_PTON
GEAR_BEGIN_ARG_INFO(arginfo_inet_pton, 0)
	GEAR_ARG_INFO(0, ip_address)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO(arginfo_ip2long, 0)
	GEAR_ARG_INFO(0, ip_address)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_long2ip, 0)
	GEAR_ARG_INFO(0, proper_address)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_getenv, 0, 0, 0)
	GEAR_ARG_INFO(0, varname)
	GEAR_ARG_INFO(0, local_only)
GEAR_END_ARG_INFO()

#ifdef HAVE_PUTENV
GEAR_BEGIN_ARG_INFO(arginfo_putenv, 0)
	GEAR_ARG_INFO(0, setting)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_getopt, 0, 0, 1)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, opts) /* ARRAY_INFO(0, opts, 1) */
	GEAR_ARG_INFO(1, optind)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_flush, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_sleep, 0)
	GEAR_ARG_INFO(0, seconds)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_usleep, 0)
	GEAR_ARG_INFO(0, micro_seconds)
GEAR_END_ARG_INFO()

#if HAVE_NANOSLEEP
GEAR_BEGIN_ARG_INFO(arginfo_time_nanosleep, 0)
	GEAR_ARG_INFO(0, seconds)
	GEAR_ARG_INFO(0, nanoseconds)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_time_sleep_until, 0)
	GEAR_ARG_INFO(0, timestamp)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO(arginfo_get_current_user, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_get_cfg_var, 0)
	GEAR_ARG_INFO(0, option_name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_error_log, 0, 0, 1)
	GEAR_ARG_INFO(0, message)
	GEAR_ARG_INFO(0, message_type)
	GEAR_ARG_INFO(0, destination)
	GEAR_ARG_INFO(0, extra_headers)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_error_get_last, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_error_clear_last, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_call_user_func, 0, 0, 1)
	GEAR_ARG_INFO(0, function_name)
	GEAR_ARG_VARIADIC_INFO(0, parameters)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_call_user_func_array, 0, 0, 2)
	GEAR_ARG_INFO(0, function_name)
	GEAR_ARG_INFO(0, parameters) /* ARRAY_INFO(0, parameters, 1) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_forward_static_call, 0, 0, 1)
	GEAR_ARG_INFO(0, function_name)
	GEAR_ARG_VARIADIC_INFO(0, parameters)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_forward_static_call_array, 0, 0, 2)
	GEAR_ARG_INFO(0, function_name)
	GEAR_ARG_INFO(0, parameters) /* ARRAY_INFO(0, parameters, 1) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_register_shutdown_function, 0, 0, 1)
	GEAR_ARG_INFO(0, function_name)
	GEAR_ARG_VARIADIC_INFO(0, parameters)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_highlight_file, 0, 0, 1)
	GEAR_ARG_INFO(0, file_name)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hyss_strip_whitespace, 0)
	GEAR_ARG_INFO(0, file_name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_highlight_string, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ics_get, 0)
	GEAR_ARG_INFO(0, varname)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ics_get_all, 0, 0, 0)
	GEAR_ARG_INFO(0, extension)
	GEAR_ARG_INFO(0, details)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ics_set, 0)
	GEAR_ARG_INFO(0, varname)
	GEAR_ARG_INFO(0, newvalue)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ics_restore, 0)
	GEAR_ARG_INFO(0, varname)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_set_include_path, 0)
	GEAR_ARG_INFO(0, new_include_path)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_get_include_path, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_restore_include_path, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_print_r, 0, 0, 1)
	GEAR_ARG_INFO(0, var)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_connection_aborted, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_connection_status, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ignore_user_abort, 0, 0, 0)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

#if HAVE_GETSERVBYNAME
GEAR_BEGIN_ARG_INFO(arginfo_getservbyname, 0)
	GEAR_ARG_INFO(0, service)
	GEAR_ARG_INFO(0, protocol)
GEAR_END_ARG_INFO()
#endif

#if HAVE_GETSERVBYPORT
GEAR_BEGIN_ARG_INFO(arginfo_getservbyport, 0)
	GEAR_ARG_INFO(0, port)
	GEAR_ARG_INFO(0, protocol)
GEAR_END_ARG_INFO()
#endif

#if HAVE_GETPROTOBYNAME
GEAR_BEGIN_ARG_INFO(arginfo_getprotobyname, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()
#endif

#if HAVE_GETPROTOBYNUMBER
GEAR_BEGIN_ARG_INFO(arginfo_getprotobynumber, 0)
	GEAR_ARG_INFO(0, proto)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_register_tick_function, 0, 0, 1)
	GEAR_ARG_INFO(0, function_name)
	GEAR_ARG_VARIADIC_INFO(0, parameters)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_unregister_tick_function, 0)
	GEAR_ARG_INFO(0, function_name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_uploaded_file, 0)
	GEAR_ARG_INFO(0, path)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_move_uploaded_file, 0)
	GEAR_ARG_INFO(0, path)
	GEAR_ARG_INFO(0, new_path)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_parse_ics_file, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, process_sections)
	GEAR_ARG_INFO(0, scanner_mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_parse_ics_string, 0, 0, 1)
    GEAR_ARG_INFO(0, ics_string)
    GEAR_ARG_INFO(0, process_sections)
    GEAR_ARG_INFO(0, scanner_mode)
GEAR_END_ARG_INFO()

#if GEAR_DEBUG
GEAR_BEGIN_ARG_INFO(arginfo_config_get_hash, 0)
GEAR_END_ARG_INFO()
#endif

#ifdef HAVE_GETLOADAVG
GEAR_BEGIN_ARG_INFO(arginfo_sys_getloadavg, 0)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ assert.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_assert, 0, 0, 1)
	GEAR_ARG_INFO(0, assertion)
	GEAR_ARG_INFO(0, description)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_assert_options, 0, 0, 1)
	GEAR_ARG_INFO(0, what)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ base64.c */
GEAR_BEGIN_ARG_INFO(arginfo_base64_encode, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_base64_decode, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, strict)
GEAR_END_ARG_INFO()

/* }}} */
/* {{{ browscap.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_get_browser, 0, 0, 0)
	GEAR_ARG_INFO(0, browser_name)
	GEAR_ARG_INFO(0, return_array)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ crc32.c */
GEAR_BEGIN_ARG_INFO(arginfo_crc32, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

/* }}} */
/* {{{ crypt.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_crypt, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, salt)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ cyr_convert.c */
GEAR_BEGIN_ARG_INFO(arginfo_convert_cyr_string, 0)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, from)
	GEAR_ARG_INFO(0, to)
GEAR_END_ARG_INFO()

/* }}} */
/* {{{ datetime.c */
#if HAVE_STRPTIME
GEAR_BEGIN_ARG_INFO(arginfo_strptime, 0)
	GEAR_ARG_INFO(0, timestamp)
	GEAR_ARG_INFO(0, format)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ dir.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_opendir, 0, 0, 1)
	GEAR_ARG_INFO(0, path)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_dir, 0, 0, 1)
	GEAR_ARG_INFO(0, directory)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_closedir, 0, 0, 0)
	GEAR_ARG_INFO(0, dir_handle)
GEAR_END_ARG_INFO()

#if defined(HAVE_CHROOT) && !defined(ZTS) && ENABLE_CHROOT_FUNC
GEAR_BEGIN_ARG_INFO(arginfo_chroot, 0)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO(arginfo_chdir, 0)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_getcwd, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_rewinddir, 0, 0, 0)
	GEAR_ARG_INFO(0, dir_handle)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_readdir, 0, 0, 0)
	GEAR_ARG_INFO(0, dir_handle)
GEAR_END_ARG_INFO()

#ifdef HAVE_GLOB
GEAR_BEGIN_ARG_INFO_EX(arginfo_glob, 0, 0, 1)
	GEAR_ARG_INFO(0, pattern)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_scandir, 0, 0, 1)
	GEAR_ARG_INFO(0, dir)
	GEAR_ARG_INFO(0, sorting_order)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ dns.c */
GEAR_BEGIN_ARG_INFO(arginfo_gethostbyaddr, 0)
	GEAR_ARG_INFO(0, ip_address)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_gethostbyname, 0)
	GEAR_ARG_INFO(0, hostname)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_gethostbynamel, 0)
	GEAR_ARG_INFO(0, hostname)
GEAR_END_ARG_INFO()

#ifdef HAVE_GETHOSTNAME
GEAR_BEGIN_ARG_INFO(arginfo_gethostname, 0)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO(arginfo_net_get_interfaces, 0)
GEAR_END_ARG_INFO()

#if defined(HYSS_WIN32) || HAVE_DNS_SEARCH_FUNC
GEAR_BEGIN_ARG_INFO_EX(arginfo_dns_check_record, 0, 0, 1)
	GEAR_ARG_INFO(0, host)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()

# if defined(HYSS_WIN32) || HAVE_FULL_DNS_FUNCS
GEAR_BEGIN_ARG_INFO_EX(arginfo_dns_get_record, 0, 0, 1)
	GEAR_ARG_INFO(0, hostname)
	GEAR_ARG_INFO(0, type)
	GEAR_ARG_ARRAY_INFO(1, authns, 1)
	GEAR_ARG_ARRAY_INFO(1, addtl, 1)
	GEAR_ARG_INFO(0, raw)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_dns_get_mx, 0, 0, 2)
	GEAR_ARG_INFO(0, hostname)
	GEAR_ARG_INFO(1, mxhosts) /* ARRAY_INFO(1, mxhosts, 1) */
	GEAR_ARG_INFO(1, weight) /* ARRAY_INFO(1, weight, 1) */
GEAR_END_ARG_INFO()
# endif

#endif /* defined(HYSS_WIN32) || HAVE_DNS_SEARCH_FUNC */
/* }}} */

/* {{{ exec.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_exec, 0, 0, 1)
	GEAR_ARG_INFO(0, command)
	GEAR_ARG_INFO(1, output) /* ARRAY_INFO(1, output, 1) */
	GEAR_ARG_INFO(1, return_value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_system, 0, 0, 1)
	GEAR_ARG_INFO(0, command)
	GEAR_ARG_INFO(1, return_value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_passthru, 0, 0, 1)
	GEAR_ARG_INFO(0, command)
	GEAR_ARG_INFO(1, return_value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_escapeshellcmd, 0)
	GEAR_ARG_INFO(0, command)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_escapeshellarg, 0)
	GEAR_ARG_INFO(0, arg)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_shell_exec, 0)
	GEAR_ARG_INFO(0, cmd)
GEAR_END_ARG_INFO()

#ifdef HAVE_NICE
GEAR_BEGIN_ARG_INFO(arginfo_proc_nice, 0)
	GEAR_ARG_INFO(0, priority)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ file.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_flock, 0, 0, 2)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, operation)
	GEAR_ARG_INFO(1, wouldblock)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_meta_tags, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, use_include_path)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_file_get_contents, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, context)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, maxlen)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_file_put_contents, 0, 0, 2)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, data)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_file, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_tempnam, 0)
	GEAR_ARG_INFO(0, dir)
	GEAR_ARG_INFO(0, prefix)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_tmpfile, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fopen, 0, 0, 2)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, use_include_path)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fclose, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_popen, 0)
	GEAR_ARG_INFO(0, command)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_pclose, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_feof, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fgets, 0, 0, 1)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fgetc, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fgetss, 0, 0, 1)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, length)
	GEAR_ARG_INFO(0, allowable_tags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fscanf, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_VARIADIC_INFO(1, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fwrite, 0, 0, 2)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fflush, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_rewind, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftell, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fseek, 0, 0, 2)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, whence)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mkdir, 0, 0, 1)
	GEAR_ARG_INFO(0, pathname)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, recursive)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_rmdir, 0, 0, 1)
	GEAR_ARG_INFO(0, dirname)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_readfile, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_umask, 0, 0, 0)
	GEAR_ARG_INFO(0, mask)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fpassthru, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_rename, 0, 0, 2)
	GEAR_ARG_INFO(0, old_name)
	GEAR_ARG_INFO(0, new_name)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_unlink, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftruncate, 0)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, size)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fstat, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()
GEAR_BEGIN_ARG_INFO_EX(arginfo_copy, 0, 0, 2)
	GEAR_ARG_INFO(0, source_file)
	GEAR_ARG_INFO(0, destination_file)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fread, 0)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fputcsv, 0, 0, 2)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, fields) /* ARRAY_INFO(0, fields, 1) */
	GEAR_ARG_INFO(0, delimiter)
	GEAR_ARG_INFO(0, enclosure)
	GEAR_ARG_INFO(0, escape_char)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fgetcsv, 0, 0, 1)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, length)
	GEAR_ARG_INFO(0, delimiter)
	GEAR_ARG_INFO(0, enclosure)
	GEAR_ARG_INFO(0, escape)
GEAR_END_ARG_INFO()

#if HAVE_REALPATH || defined(ZTS)
GEAR_BEGIN_ARG_INFO(arginfo_realpath, 0)
	GEAR_ARG_INFO(0, path)
GEAR_END_ARG_INFO()
#endif

#ifdef HAVE_FNMATCH
GEAR_BEGIN_ARG_INFO_EX(arginfo_fnmatch, 0, 0, 2)
	GEAR_ARG_INFO(0, pattern)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO(arginfo_sys_get_temp_dir, 0)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ filestat.c */
GEAR_BEGIN_ARG_INFO(arginfo_disk_total_space, 0)
	GEAR_ARG_INFO(0, path)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_disk_free_space, 0)
	GEAR_ARG_INFO(0, path)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_chgrp, 0)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, group)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_chown, 0)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, user)
GEAR_END_ARG_INFO()

#if HAVE_LCHOWN
GEAR_BEGIN_ARG_INFO(arginfo_lchgrp, 0)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, group)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_lchown, 0)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, user)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO(arginfo_chmod, 0)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

#if HAVE_UTIME
GEAR_BEGIN_ARG_INFO_EX(arginfo_touch, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, time)
	GEAR_ARG_INFO(0, atime)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_clearstatcache, 0, 0, 0)
	GEAR_ARG_INFO(0, clear_realpath_cache)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_realpath_cache_size, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_realpath_cache_get, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fileperms, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fileinode, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_filesize, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fileowner, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_filegroup, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fileatime, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_filemtime, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_filectime, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_filetype, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_writable, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_readable, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_executable, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_file, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_dir, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_link, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_file_exists, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_lstat, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stat, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ formatted_print.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_sprintf, 0, 0, 1)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_VARIADIC_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_vsprintf, 0)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, args) /* ARRAY_INFO(0, args, 1) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_printf, 0, 0, 1)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_VARIADIC_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_vprintf, 0)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, args) /* ARRAY_INFO(0, args, 1) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_fprintf, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_VARIADIC_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_vfprintf, 0)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, args) /* ARRAY_INFO(0, args, 1) */
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ fsock.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_fsockopen, 0, 0, 1)
	GEAR_ARG_INFO(0, hostname)
	GEAR_ARG_INFO(0, port)
	GEAR_ARG_INFO(1, errno)
	GEAR_ARG_INFO(1, errstr)
	GEAR_ARG_INFO(0, timeout)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pfsockopen, 0, 0, 1)
	GEAR_ARG_INFO(0, hostname)
	GEAR_ARG_INFO(0, port)
	GEAR_ARG_INFO(1, errno)
	GEAR_ARG_INFO(1, errstr)
	GEAR_ARG_INFO(0, timeout)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ ftok.c */
#if HAVE_FTOK
GEAR_BEGIN_ARG_INFO(arginfo_ftok, 0)
	GEAR_ARG_INFO(0, pathname)
	GEAR_ARG_INFO(0, proj)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ head.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_header, 0, 0, 1)
	GEAR_ARG_INFO(0, header)
	GEAR_ARG_INFO(0, replace)
	GEAR_ARG_INFO(0, http_response_code)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_header_remove, 0, 0, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_setcookie, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
	GEAR_ARG_INFO(0, expires_or_options)
	GEAR_ARG_INFO(0, path)
	GEAR_ARG_INFO(0, domain)
	GEAR_ARG_INFO(0, secure)
	GEAR_ARG_INFO(0, httponly)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_setrawcookie, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
	GEAR_ARG_INFO(0, expires_or_options)
	GEAR_ARG_INFO(0, path)
	GEAR_ARG_INFO(0, domain)
	GEAR_ARG_INFO(0, secure)
	GEAR_ARG_INFO(0, httponly)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_headers_sent, 0, 0, 0)
	GEAR_ARG_INFO(1, file)
	GEAR_ARG_INFO(1, line)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_headers_list, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_http_response_code, 0, 0, 0)
	GEAR_ARG_INFO(0, response_code)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ hrtime.c */
GEAR_BEGIN_ARG_INFO(arginfo_hrtime, 0)
	GEAR_ARG_INFO(0, get_as_number)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ html.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_htmlspecialchars, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, quote_style)
	GEAR_ARG_INFO(0, encoding)
	GEAR_ARG_INFO(0, double_encode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_htmlspecialchars_decode, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, quote_style)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_html_entity_decode, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, quote_style)
	GEAR_ARG_INFO(0, encoding)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_htmlentities, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, quote_style)
	GEAR_ARG_INFO(0, encoding)
	GEAR_ARG_INFO(0, double_encode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_html_translation_table, 0, 0, 0)
	GEAR_ARG_INFO(0, table)
	GEAR_ARG_INFO(0, quote_style)
	GEAR_ARG_INFO(0, encoding)
GEAR_END_ARG_INFO()

/* }}} */
/* {{{ http.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_http_build_query, 0, 0, 1)
	GEAR_ARG_INFO(0, formdata)
	GEAR_ARG_INFO(0, prefix)
	GEAR_ARG_INFO(0, arg_separator)
	GEAR_ARG_INFO(0, enc_type)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ image.c */
GEAR_BEGIN_ARG_INFO(arginfo_image_type_to_mime_type, 0)
	GEAR_ARG_INFO(0, imagetype)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_image_type_to_extension, 0, 0, 1)
	GEAR_ARG_INFO(0, imagetype)
	GEAR_ARG_INFO(0, include_dot)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_getimagesize, 0, 0, 1)
	GEAR_ARG_INFO(0, imagefile)
	GEAR_ARG_INFO(1, info) /* ARRAY_INFO(1, info, 1) */
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ info.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_hyssinfo, 0, 0, 0)
	GEAR_ARG_INFO(0, what)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hyssversion, 0, 0, 0)
	GEAR_ARG_INFO(0, extension)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hysscredits, 0, 0, 0)
	GEAR_ARG_INFO(0, flag)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hyss_sapi_name, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hyss_uname, 0, 0, 0)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hyss_ics_scanned_files, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hyss_ics_loaded_file, 0)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ iptc.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_iptcembed, 0, 0, 2)
	GEAR_ARG_INFO(0, iptcdata)
	GEAR_ARG_INFO(0, jpeg_file_name)
	GEAR_ARG_INFO(0, spool)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_iptcparse, 0)
	GEAR_ARG_INFO(0, iptcdata)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ lcg.c */
GEAR_BEGIN_ARG_INFO(arginfo_lcg_value, 0)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ levenshtein.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_levenshtein, 0, 0, 2)
	GEAR_ARG_INFO(0, str1)
	GEAR_ARG_INFO(0, str2)
	GEAR_ARG_INFO(0, cost_ins)
	GEAR_ARG_INFO(0, cost_rep)
	GEAR_ARG_INFO(0, cost_del)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ link.c */
#if defined(HAVE_SYMLINK) || defined(HYSS_WIN32)
GEAR_BEGIN_ARG_INFO(arginfo_readlink, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_linkinfo, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_symlink, 0)
	GEAR_ARG_INFO(0, target)
	GEAR_ARG_INFO(0, link)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_link, 0)
	GEAR_ARG_INFO(0, target)
	GEAR_ARG_INFO(0, link)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ mail.c */
GEAR_BEGIN_ARG_INFO(arginfo_ezmlm_hash, 0)
	GEAR_ARG_INFO(0, addr)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mail, 0, 0, 3)
	GEAR_ARG_INFO(0, to)
	GEAR_ARG_INFO(0, subject)
	GEAR_ARG_INFO(0, message)
	GEAR_ARG_INFO(0, additional_headers)
	GEAR_ARG_INFO(0, additional_parameters)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ math.c */
GEAR_BEGIN_ARG_INFO(arginfo_abs, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ceil, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_floor, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_round, 0, 0, 1)
	GEAR_ARG_INFO(0, number)
	GEAR_ARG_INFO(0, precision)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_sin, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_cos, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_tan, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_asin, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_acos, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_atan, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_atan2, 0)
	GEAR_ARG_INFO(0, y)
	GEAR_ARG_INFO(0, x)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_sinh, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_cosh, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_tanh, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_asinh, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_acosh, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_atanh, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_pi, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_finite, 0)
	GEAR_ARG_INFO(0, val)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_infinite, 0)
	GEAR_ARG_INFO(0, val)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_nan, 0)
	GEAR_ARG_INFO(0, val)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_pow, 0)
	GEAR_ARG_INFO(0, base)
	GEAR_ARG_INFO(0, exponent)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_exp, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_expm1, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_log1p, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_log, 0, 0, 1)
	GEAR_ARG_INFO(0, number)
	GEAR_ARG_INFO(0, base)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_log10, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_sqrt, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hypot, 0)
	GEAR_ARG_INFO(0, num1)
	GEAR_ARG_INFO(0, num2)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_deg2rad, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_rad2deg, 0)
	GEAR_ARG_INFO(0, number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_bindec, 0)
	GEAR_ARG_INFO(0, binary_number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hexdec, 0)
	GEAR_ARG_INFO(0, hexadecimal_number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_octdec, 0)
	GEAR_ARG_INFO(0, octal_number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_decbin, 0)
	GEAR_ARG_INFO(0, decimal_number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_decoct, 0)
	GEAR_ARG_INFO(0, decimal_number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_dechex, 0)
	GEAR_ARG_INFO(0, decimal_number)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_base_convert, 0)
	GEAR_ARG_INFO(0, number)
	GEAR_ARG_INFO(0, frombase)
	GEAR_ARG_INFO(0, tobase)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_number_format, 0, 0, 1)
	GEAR_ARG_INFO(0, number)
	GEAR_ARG_INFO(0, num_decimal_places)
	GEAR_ARG_INFO(0, dec_separator)
	GEAR_ARG_INFO(0, thousands_separator)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_fmod, 0)
	GEAR_ARG_INFO(0, x)
	GEAR_ARG_INFO(0, y)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_intdiv, 0)
	GEAR_ARG_INFO(0, dividend)
	GEAR_ARG_INFO(0, divisor)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ md5.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_md5, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_md5_file, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ metaphone.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_metaphone, 0, 0, 1)
	GEAR_ARG_INFO(0, text)
	GEAR_ARG_INFO(0, phones)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ microtime.c */
#ifdef HAVE_GETTIMEOFDAY
GEAR_BEGIN_ARG_INFO_EX(arginfo_microtime, 0, 0, 0)
	GEAR_ARG_INFO(0, get_as_float)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_gettimeofday, 0, 0, 0)
	GEAR_ARG_INFO(0, get_as_float)
GEAR_END_ARG_INFO()
#endif

#ifdef HAVE_GETRUSAGE
GEAR_BEGIN_ARG_INFO_EX(arginfo_getrusage, 0, 0, 0)
	GEAR_ARG_INFO(0, who)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ pack.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_pack, 0, 0, 2)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_VARIADIC_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_unpack, 0, 0, 2)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, input)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ pageinfo.c */
GEAR_BEGIN_ARG_INFO(arginfo_getmyuid, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_getmygid, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_getmypid, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_getmyinode, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_getlastmod, 0)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ password.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_password_hash, 0, 0, 2)
	GEAR_ARG_INFO(0, password)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()
GEAR_BEGIN_ARG_INFO_EX(arginfo_password_get_info, 0, 0, 1)
	GEAR_ARG_INFO(0, hash)
GEAR_END_ARG_INFO()
GEAR_BEGIN_ARG_INFO_EX(arginfo_password_needs_rehash, 0, 0, 2)
	GEAR_ARG_INFO(0, hash)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()
GEAR_BEGIN_ARG_INFO_EX(arginfo_password_verify, 0, 0, 2)
	GEAR_ARG_INFO(0, password)
	GEAR_ARG_INFO(0, hash)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ proc_open.c */
#ifdef HYSS_CAN_SUPPORT_PROC_OPEN
GEAR_BEGIN_ARG_INFO_EX(arginfo_proc_terminate, 0, 0, 1)
	GEAR_ARG_INFO(0, process)
	GEAR_ARG_INFO(0, signal)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_proc_close, 0)
	GEAR_ARG_INFO(0, process)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_proc_get_status, 0)
	GEAR_ARG_INFO(0, process)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_proc_open, 0, 0, 3)
	GEAR_ARG_INFO(0, command)
	GEAR_ARG_INFO(0, descriptorspec) /* ARRAY_INFO(0, descriptorspec, 1) */
	GEAR_ARG_INFO(1, pipes) /* ARRAY_INFO(1, pipes, 1) */
	GEAR_ARG_INFO(0, cwd)
	GEAR_ARG_INFO(0, env) /* ARRAY_INFO(0, env, 1) */
	GEAR_ARG_INFO(0, other_options) /* ARRAY_INFO(0, other_options, 1) */
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ quot_print.c */
GEAR_BEGIN_ARG_INFO(arginfo_quoted_printable_decode, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ quot_print.c */
GEAR_BEGIN_ARG_INFO(arginfo_quoted_printable_encode, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ mt_rand.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_mt_srand, 0, 0, 0)
	GEAR_ARG_INFO(0, seed)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mt_rand, 0, 0, 0)
	GEAR_ARG_INFO(0, min)
	GEAR_ARG_INFO(0, max)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_mt_getrandmax, 0)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ random.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_random_bytes, 0, 0, 1)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_random_int, 0, 0, 2)
	GEAR_ARG_INFO(0, min)
	GEAR_ARG_INFO(0, max)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ sha1.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_sha1, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_sha1_file, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ soundex.c */
GEAR_BEGIN_ARG_INFO(arginfo_soundex, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ streamsfuncs.c */
#if HAVE_SOCKETPAIR
GEAR_BEGIN_ARG_INFO(arginfo_stream_socket_pair, 0)
	GEAR_ARG_INFO(0, domain)
	GEAR_ARG_INFO(0, type)
	GEAR_ARG_INFO(0, protocol)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_socket_client, 0, 0, 1)
	GEAR_ARG_INFO(0, remoteaddress)
	GEAR_ARG_INFO(1, errcode)
	GEAR_ARG_INFO(1, errstring)
	GEAR_ARG_INFO(0, timeout)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_socket_server, 0, 0, 1)
	GEAR_ARG_INFO(0, localaddress)
	GEAR_ARG_INFO(1, errcode)
	GEAR_ARG_INFO(1, errstring)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_socket_accept, 0, 0, 1)
	GEAR_ARG_INFO(0, serverstream)
	GEAR_ARG_INFO(0, timeout)
	GEAR_ARG_INFO(1, peername)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_socket_get_name, 0)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, want_peer)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_socket_sendto, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, data)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, target_addr)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_socket_recvfrom, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, amount)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(1, remote_addr)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_get_contents, 0, 0, 1)
	GEAR_ARG_INFO(0, source)
	GEAR_ARG_INFO(0, maxlen)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_copy_to_stream, 0, 0, 2)
	GEAR_ARG_INFO(0, source)
	GEAR_ARG_INFO(0, dest)
	GEAR_ARG_INFO(0, maxlen)
	GEAR_ARG_INFO(0, pos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_get_meta_data, 0)
	GEAR_ARG_INFO(0, fp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_get_transports, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_get_wrappers, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_resolve_include_path, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_is_local, 0)
	GEAR_ARG_INFO(0, stream)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_supports_lock, 0, 0, 1)
    GEAR_ARG_INFO(0, stream)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_isatty, 0, 0, 1)
	GEAR_ARG_INFO(0, stream)
GEAR_END_ARG_INFO()

#ifdef HYSS_WIN32
GEAR_BEGIN_ARG_INFO_EX(arginfo_sapi_windows_vt100_support, 0, 0, 1)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, enable)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_select, 0, 0, 4)
	GEAR_ARG_INFO(1, read_streams) /* ARRAY_INFO(1, read_streams, 1) */
	GEAR_ARG_INFO(1, write_streams) /* ARRAY_INFO(1, write_streams, 1) */
	GEAR_ARG_INFO(1, except_streams) /* ARRAY_INFO(1, except_streams, 1) */
	GEAR_ARG_INFO(0, tv_sec)
	GEAR_ARG_INFO(0, tv_usec)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_context_get_options, 0)
	GEAR_ARG_INFO(0, stream_or_context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_context_set_option, 0)
	GEAR_ARG_INFO(0, stream_or_context)
	GEAR_ARG_INFO(0, wrappername)
	GEAR_ARG_INFO(0, optionname)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_context_set_params, 0)
	GEAR_ARG_INFO(0, stream_or_context)
	GEAR_ARG_INFO(0, options) /* ARRAY_INFO(0, options, 1) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_context_get_params, 0, GEAR_RETURN_VALUE, 1)
	GEAR_ARG_INFO(0, stream_or_context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_context_get_default, 0, 0, 0)
	GEAR_ARG_INFO(0, options) /* ARRAY_INFO(0, options, 1) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_context_set_default, 0)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_context_create, 0, 0, 0)
	GEAR_ARG_INFO(0, options) /* ARRAY_INFO(0, options, 1) */
	GEAR_ARG_INFO(0, params) /* ARRAY_INFO(0, params, 1) */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_filter_prepend, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, filtername)
	GEAR_ARG_INFO(0, read_write)
	GEAR_ARG_INFO(0, filterparams)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_filter_append, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, filtername)
	GEAR_ARG_INFO(0, read_write)
	GEAR_ARG_INFO(0, filterparams)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_filter_remove, 0)
	GEAR_ARG_INFO(0, stream_filter)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_get_line, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, maxlen)
	GEAR_ARG_INFO(0, ending)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_set_blocking, 0)
	GEAR_ARG_INFO(0, socket)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

#if HAVE_SYS_TIME_H || defined(HYSS_WIN32)
GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_set_timeout, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, seconds)
	GEAR_ARG_INFO(0, microseconds)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO(arginfo_stream_set_read_buffer, 0)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, buffer)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_set_write_buffer, 0)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, buffer)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_set_chunk_size, 0)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, chunk_size)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stream_socket_enable_crypto, 0, 0, 2)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, enable)
	GEAR_ARG_INFO(0, cryptokind)
	GEAR_ARG_INFO(0, sessionstream)
GEAR_END_ARG_INFO()

#ifdef HAVE_SHUTDOWN
GEAR_BEGIN_ARG_INFO(arginfo_stream_socket_shutdown, 0)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, how)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ string.c */
GEAR_BEGIN_ARG_INFO(arginfo_bin2hex, 0)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hex2bin, 0)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strspn, 0, 0, 2)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, mask)
	GEAR_ARG_INFO(0, start)
	GEAR_ARG_INFO(0, len)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strcspn, 0, 0, 2)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, mask)
	GEAR_ARG_INFO(0, start)
	GEAR_ARG_INFO(0, len)
GEAR_END_ARG_INFO()

#if HAVE_NL_LANGINFO
GEAR_BEGIN_ARG_INFO(arginfo_nl_langinfo, 0)
	GEAR_ARG_INFO(0, item)
GEAR_END_ARG_INFO()
#endif

#ifdef HAVE_STRCOLL
GEAR_BEGIN_ARG_INFO(arginfo_strcoll, 0)
	GEAR_ARG_INFO(0, str1)
	GEAR_ARG_INFO(0, str2)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_trim, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, character_mask)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_rtrim, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, character_mask)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ltrim, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, character_mask)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_wordwrap, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, width)
	GEAR_ARG_INFO(0, break)
	GEAR_ARG_INFO(0, cut)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_explode, 0, 0, 2)
	GEAR_ARG_INFO(0, separator)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, limit)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_implode, 0)
	GEAR_ARG_INFO(0, glue)
	GEAR_ARG_INFO(0, pieces)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strtok, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, token)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_strtoupper, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_strtolower, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_basename, 0, 0, 1)
	GEAR_ARG_INFO(0, path)
	GEAR_ARG_INFO(0, suffix)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_dirname, 0, 0, 1)
	GEAR_ARG_INFO(0, path)
	GEAR_ARG_INFO(0, levels)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pathinfo, 0, 0, 1)
	GEAR_ARG_INFO(0, path)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stristr, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, part)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strstr, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, part)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strpos, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_stripos, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strrpos, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strripos, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_strrchr, 0)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_chunk_split, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, chunklen)
	GEAR_ARG_INFO(0, ending)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_substr, 0, 0, 2)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, start)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_substr_replace, 0, 0, 3)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, replace)
	GEAR_ARG_INFO(0, start)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_quotemeta, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ord, 0)
	GEAR_ARG_INFO(0, character)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_chr, 0)
	GEAR_ARG_INFO(0, codepoint)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ucfirst, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_lcfirst, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ucwords, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, delimiters)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strtr, 0, 0, 2)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, from)
	GEAR_ARG_INFO(0, to)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_strrev, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_similar_text, 0, 0, 2)
	GEAR_ARG_INFO(0, str1)
	GEAR_ARG_INFO(0, str2)
	GEAR_ARG_INFO(1, percent)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_addcslashes, 0)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, charlist)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_addslashes, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stripcslashes, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stripslashes, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_str_replace, 0, 0, 3)
	GEAR_ARG_INFO(0, search)
	GEAR_ARG_INFO(0, replace)
	GEAR_ARG_INFO(0, subject)
	GEAR_ARG_INFO(1, replace_count)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_str_ireplace, 0, 0, 3)
	GEAR_ARG_INFO(0, search)
	GEAR_ARG_INFO(0, replace)
	GEAR_ARG_INFO(0, subject)
	GEAR_ARG_INFO(1, replace_count)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hebrev, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, max_chars_per_line)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hebrevc, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, max_chars_per_line)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_nl2br, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, is_xhtml)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strip_tags, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, allowable_tags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_setlocale, 0, 0, 2)
	GEAR_ARG_INFO(0, category)
	GEAR_ARG_VARIADIC_INFO(0, locales)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_parse_str, 0, 0, 1)
	GEAR_ARG_INFO(0, encoded_string)
	GEAR_ARG_INFO(1, result)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_str_getcsv, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, delimiter)
	GEAR_ARG_INFO(0, enclosure)
	GEAR_ARG_INFO(0, escape)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_str_repeat, 0)
	GEAR_ARG_INFO(0, input)
	GEAR_ARG_INFO(0, mult)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_count_chars, 0, 0, 1)
	GEAR_ARG_INFO(0, input)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_strnatcmp, 0)
	GEAR_ARG_INFO(0, s1)
	GEAR_ARG_INFO(0, s2)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_localeconv, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_strnatcasecmp, 0)
	GEAR_ARG_INFO(0, s1)
	GEAR_ARG_INFO(0, s2)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_substr_count, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_str_pad, 0, 0, 2)
	GEAR_ARG_INFO(0, input)
	GEAR_ARG_INFO(0, pad_length)
	GEAR_ARG_INFO(0, pad_string)
	GEAR_ARG_INFO(0, pad_type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_sscanf, 0, 0, 2)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_VARIADIC_INFO(1, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_str_rot13, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_str_shuffle, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_str_word_count, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, charlist)
GEAR_END_ARG_INFO()

#ifdef HAVE_STRFMON
GEAR_BEGIN_ARG_INFO(arginfo_money_format, 0)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_str_split, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, split_length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strpbrk, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, char_list)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_substr_compare, 0, 0, 3)
	GEAR_ARG_INFO(0, main_str)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, offset)
	GEAR_ARG_INFO(0, length)
	GEAR_ARG_INFO(0, case_sensitivity)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_utf8_encode, 0, 0, 1)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_utf8_decode, 0, 0, 1)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ syslog.c */
#ifdef HAVE_SYSLOG_H
GEAR_BEGIN_ARG_INFO(arginfo_openlog, 0)
	GEAR_ARG_INFO(0, ident)
	GEAR_ARG_INFO(0, option)
	GEAR_ARG_INFO(0, facility)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_closelog, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_syslog, 0)
	GEAR_ARG_INFO(0, priority)
	GEAR_ARG_INFO(0, message)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ type.c */
GEAR_BEGIN_ARG_INFO(arginfo_gettype, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_settype, 0)
	GEAR_ARG_INFO(1, var)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_intval, 0, 0, 1)
	GEAR_ARG_INFO(0, var)
	GEAR_ARG_INFO(0, base)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_floatval, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_strval, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_boolval, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_null, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_resource, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_bool, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_int, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_float, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_string, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_array, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_object, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_numeric, 0)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_scalar, 0)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_is_callable, 0, 0, 1)
	GEAR_ARG_INFO(0, var)
	GEAR_ARG_INFO(0, syntax_only)
	GEAR_ARG_INFO(1, callable_name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_is_iterable, 0, 0, 1)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_is_countable, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ uniqid.c */
#ifdef HAVE_GETTIMEOFDAY
GEAR_BEGIN_ARG_INFO_EX(arginfo_uniqid, 0, 0, 0)
	GEAR_ARG_INFO(0, prefix)
	GEAR_ARG_INFO(0, more_entropy)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* {{{ url.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_parse_url, 0, 0, 1)
	GEAR_ARG_INFO(0, url)
	GEAR_ARG_INFO(0, component)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_urlencode, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_urldecode, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_rawurlencode, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_rawurldecode, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_get_headers, 0, 0, 1)
	GEAR_ARG_INFO(0, url)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ user_filters.c */
GEAR_BEGIN_ARG_INFO(arginfo_stream_bucket_make_writeable, 0)
	GEAR_ARG_INFO(0, brigade)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_bucket_prepend, 0)
	GEAR_ARG_INFO(0, brigade)
	GEAR_ARG_INFO(0, bucket)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_bucket_append, 0)
	GEAR_ARG_INFO(0, brigade)
	GEAR_ARG_INFO(0, bucket)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_bucket_new, 0)
	GEAR_ARG_INFO(0, stream)
	GEAR_ARG_INFO(0, buffer)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_get_filters, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_stream_filter_register, 0)
	GEAR_ARG_INFO(0, filtername)
	GEAR_ARG_INFO(0, classname)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ uuencode.c */
GEAR_BEGIN_ARG_INFO(arginfo_convert_uuencode, 0)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_convert_uudecode, 0)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ var.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_var_dump, 0, 0, 1)
	GEAR_ARG_VARIADIC_INFO(0, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_debug_zval_dump, 0, 0, 1)
	GEAR_ARG_VARIADIC_INFO(0, vars)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_var_export, 0, 0, 1)
	GEAR_ARG_INFO(0, var)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_serialize, 0)
	GEAR_ARG_INFO(0, var)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_unserialize, 0, 0, 1)
	GEAR_ARG_INFO(0, variable_representation)
	GEAR_ARG_INFO(0, allowed_classes)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_memory_get_usage, 0, 0, 0)
	GEAR_ARG_INFO(0, real_usage)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_memory_get_peak_usage, 0, 0, 0)
	GEAR_ARG_INFO(0, real_usage)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ versioning.c */
GEAR_BEGIN_ARG_INFO_EX(arginfo_version_compare, 0, 0, 2)
	GEAR_ARG_INFO(0, ver1)
	GEAR_ARG_INFO(0, ver2)
	GEAR_ARG_INFO(0, oper)
GEAR_END_ARG_INFO()
/* }}} */
/* {{{ win32/codepage.c */
#ifdef HYSS_WIN32
GEAR_BEGIN_ARG_INFO_EX(arginfo_sapi_windows_cp_set, 0, 0, 1)
	GEAR_ARG_TYPE_INFO(0, code_page, IS_LONG, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_sapi_windows_cp_get, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_sapi_windows_cp_is_utf8, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_sapi_windows_cp_conv, 0, 0, 3)
	GEAR_ARG_INFO(0, in_codepage)
	GEAR_ARG_INFO(0, out_codepage)
	GEAR_ARG_TYPE_INFO(0, subject, IS_STRING, 0)
GEAR_END_ARG_INFO()
#endif
/* }}} */
/* }}} */

static const gear_function_entry basic_functions[] = { /* {{{ */
	HYSS_FE(constant,														arginfo_constant)
	HYSS_FE(bin2hex,															arginfo_bin2hex)
	HYSS_FE(hex2bin,															arginfo_hex2bin)
	HYSS_FE(sleep,															arginfo_sleep)
	HYSS_FE(usleep,															arginfo_usleep)
#if HAVE_NANOSLEEP
	HYSS_FE(time_nanosleep,													arginfo_time_nanosleep)
	HYSS_FE(time_sleep_until,												arginfo_time_sleep_until)
#endif

#if HAVE_STRPTIME
	HYSS_FE(strptime,														arginfo_strptime)
#endif

	HYSS_FE(flush,															arginfo_flush)
	HYSS_FE(wordwrap,														arginfo_wordwrap)
	HYSS_FE(htmlspecialchars,												arginfo_htmlspecialchars)
	HYSS_FE(htmlentities,													arginfo_htmlentities)
	HYSS_FE(html_entity_decode,												arginfo_html_entity_decode)
	HYSS_FE(htmlspecialchars_decode,											arginfo_htmlspecialchars_decode)
	HYSS_FE(get_html_translation_table,										arginfo_get_html_translation_table)
	HYSS_FE(sha1,															arginfo_sha1)
	HYSS_FE(sha1_file,														arginfo_sha1_file)
	HYSS_NAMED_FE(md5,hyss_if_md5,											arginfo_md5)
	HYSS_NAMED_FE(md5_file,hyss_if_md5_file,									arginfo_md5_file)
	HYSS_NAMED_FE(crc32,hyss_if_crc32,										arginfo_crc32)

	HYSS_FE(iptcparse,														arginfo_iptcparse)
	HYSS_FE(iptcembed,														arginfo_iptcembed)
	HYSS_FE(getimagesize,													arginfo_getimagesize)
	HYSS_FE(getimagesizefromstring,											arginfo_getimagesize)
	HYSS_FE(image_type_to_mime_type,											arginfo_image_type_to_mime_type)
	HYSS_FE(image_type_to_extension,											arginfo_image_type_to_extension)

	HYSS_FE(hyssinfo,															arginfo_hyssinfo)
	HYSS_FE(hyssversion,														arginfo_hyssversion)
	HYSS_FE(hysscredits,														arginfo_hysscredits)
	HYSS_FE(hyss_sapi_name,													arginfo_hyss_sapi_name)
	HYSS_FE(hyss_uname,														arginfo_hyss_uname)
	HYSS_FE(hyss_ics_scanned_files,											arginfo_hyss_ics_scanned_files)
	HYSS_FE(hyss_ics_loaded_file,												arginfo_hyss_ics_loaded_file)

	HYSS_FE(strnatcmp,														arginfo_strnatcmp)
	HYSS_FE(strnatcasecmp,													arginfo_strnatcasecmp)
	HYSS_FE(substr_count,													arginfo_substr_count)
	HYSS_FE(strspn,															arginfo_strspn)
	HYSS_FE(strcspn,															arginfo_strcspn)
	HYSS_FE(strtok,															arginfo_strtok)
	HYSS_FE(strtoupper,														arginfo_strtoupper)
	HYSS_FE(strtolower,														arginfo_strtolower)
	HYSS_FE(strpos,															arginfo_strpos)
	HYSS_FE(stripos,															arginfo_stripos)
	HYSS_FE(strrpos,															arginfo_strrpos)
	HYSS_FE(strripos,														arginfo_strripos)
	HYSS_FE(strrev,															arginfo_strrev)
	HYSS_FE(hebrev,															arginfo_hebrev)
	HYSS_FE(hebrevc,															arginfo_hebrevc)
	HYSS_FE(nl2br,															arginfo_nl2br)
	HYSS_FE(basename,														arginfo_basename)
	HYSS_FE(dirname,															arginfo_dirname)
	HYSS_FE(pathinfo,														arginfo_pathinfo)
	HYSS_FE(stripslashes,													arginfo_stripslashes)
	HYSS_FE(stripcslashes,													arginfo_stripcslashes)
	HYSS_FE(strstr,															arginfo_strstr)
	HYSS_FE(stristr,															arginfo_stristr)
	HYSS_FE(strrchr,															arginfo_strrchr)
	HYSS_FE(str_shuffle,														arginfo_str_shuffle)
	HYSS_FE(str_word_count,													arginfo_str_word_count)
	HYSS_FE(str_split,														arginfo_str_split)
	HYSS_FE(strpbrk,															arginfo_strpbrk)
	HYSS_FE(substr_compare,													arginfo_substr_compare)
	HYSS_FE(utf8_encode, 													arginfo_utf8_encode)
	HYSS_FE(utf8_decode, 													arginfo_utf8_decode)

#ifdef HAVE_STRCOLL
	HYSS_FE(strcoll,															arginfo_strcoll)
#endif

#ifdef HAVE_STRFMON
	HYSS_FE(money_format,													arginfo_money_format)
#endif

	HYSS_FE(substr,															arginfo_substr)
	HYSS_FE(substr_replace,													arginfo_substr_replace)
	HYSS_FE(quotemeta,														arginfo_quotemeta)
	HYSS_FE(ucfirst,															arginfo_ucfirst)
	HYSS_FE(lcfirst,															arginfo_lcfirst)
	HYSS_FE(ucwords,															arginfo_ucwords)
	HYSS_FE(strtr,															arginfo_strtr)
	HYSS_FE(addslashes,														arginfo_addslashes)
	HYSS_FE(addcslashes,														arginfo_addcslashes)
	HYSS_FE(rtrim,															arginfo_rtrim)
	HYSS_FE(str_replace,														arginfo_str_replace)
	HYSS_FE(str_ireplace,													arginfo_str_ireplace)
	HYSS_FE(str_repeat,														arginfo_str_repeat)
	HYSS_FE(count_chars,														arginfo_count_chars)
	HYSS_FE(chunk_split,														arginfo_chunk_split)
	HYSS_FE(trim,															arginfo_trim)
	HYSS_FE(ltrim,															arginfo_ltrim)
	HYSS_FE(strip_tags,														arginfo_strip_tags)
	HYSS_FE(similar_text,													arginfo_similar_text)
	HYSS_FE(explode,															arginfo_explode)
	HYSS_FE(implode,															arginfo_implode)
	HYSS_FALIAS(join,				implode,								arginfo_implode)
	HYSS_FE(setlocale,														arginfo_setlocale)
	HYSS_FE(localeconv,														arginfo_localeconv)

#if HAVE_NL_LANGINFO
	HYSS_FE(nl_langinfo,														arginfo_nl_langinfo)
#endif

	HYSS_FE(soundex,															arginfo_soundex)
	HYSS_FE(levenshtein,														arginfo_levenshtein)
	HYSS_FE(chr,																arginfo_chr)
	HYSS_FE(ord,																arginfo_ord)
	HYSS_FE(parse_str,														arginfo_parse_str)
	HYSS_FE(str_getcsv,														arginfo_str_getcsv)
	HYSS_FE(str_pad,															arginfo_str_pad)
	HYSS_FALIAS(chop,				rtrim,									arginfo_rtrim)
	HYSS_FALIAS(strchr,				strstr,									arginfo_strstr)
	HYSS_NAMED_FE(sprintf,			HYSS_FN(user_sprintf),					arginfo_sprintf)
	HYSS_NAMED_FE(printf,			HYSS_FN(user_printf),					arginfo_printf)
	HYSS_FE(vprintf,															arginfo_vprintf)
	HYSS_FE(vsprintf,														arginfo_vsprintf)
	HYSS_FE(fprintf,															arginfo_fprintf)
	HYSS_FE(vfprintf,														arginfo_vfprintf)
	HYSS_FE(sscanf,															arginfo_sscanf)
	HYSS_FE(fscanf,															arginfo_fscanf)
	HYSS_FE(parse_url,														arginfo_parse_url)
	HYSS_FE(urlencode,														arginfo_urlencode)
	HYSS_FE(urldecode,														arginfo_urldecode)
	HYSS_FE(rawurlencode,													arginfo_rawurlencode)
	HYSS_FE(rawurldecode,													arginfo_rawurldecode)
	HYSS_FE(http_build_query,												arginfo_http_build_query)

#if defined(HAVE_SYMLINK) || defined(HYSS_WIN32)
	HYSS_FE(readlink,														arginfo_readlink)
	HYSS_FE(linkinfo,														arginfo_linkinfo)
	HYSS_FE(symlink,															arginfo_symlink)
	HYSS_FE(link,															arginfo_link)
#endif

	HYSS_FE(unlink,															arginfo_unlink)
	HYSS_FE(exec,															arginfo_exec)
	HYSS_FE(system,															arginfo_system)
	HYSS_FE(escapeshellcmd,													arginfo_escapeshellcmd)
	HYSS_FE(escapeshellarg,													arginfo_escapeshellarg)
	HYSS_FE(passthru,														arginfo_passthru)
	HYSS_FE(shell_exec,														arginfo_shell_exec)
#ifdef HYSS_CAN_SUPPORT_PROC_OPEN
	HYSS_FE(proc_open,														arginfo_proc_open)
	HYSS_FE(proc_close,														arginfo_proc_close)
	HYSS_FE(proc_terminate,													arginfo_proc_terminate)
	HYSS_FE(proc_get_status,													arginfo_proc_get_status)
#endif

#ifdef HAVE_NICE
	HYSS_FE(proc_nice,														arginfo_proc_nice)
#endif

	HYSS_FE(rand,															arginfo_mt_rand)
	HYSS_FALIAS(srand, mt_srand,												arginfo_mt_srand)
	HYSS_FALIAS(getrandmax, mt_getrandmax,									arginfo_mt_getrandmax)
	HYSS_FE(mt_rand,															arginfo_mt_rand)
	HYSS_FE(mt_srand,														arginfo_mt_srand)
	HYSS_FE(mt_getrandmax,													arginfo_mt_getrandmax)

	HYSS_FE(random_bytes,													arginfo_random_bytes)
	HYSS_FE(random_int,													arginfo_random_int)

#if HAVE_GETSERVBYNAME
	HYSS_FE(getservbyname,													arginfo_getservbyname)
#endif

#if HAVE_GETSERVBYPORT
	HYSS_FE(getservbyport,													arginfo_getservbyport)
#endif

#if HAVE_GETPROTOBYNAME
	HYSS_FE(getprotobyname,													arginfo_getprotobyname)
#endif

#if HAVE_GETPROTOBYNUMBER
	HYSS_FE(getprotobynumber,												arginfo_getprotobynumber)
#endif

	HYSS_FE(getmyuid,														arginfo_getmyuid)
	HYSS_FE(getmygid,														arginfo_getmygid)
	HYSS_FE(getmypid,														arginfo_getmypid)
	HYSS_FE(getmyinode,														arginfo_getmyinode)
	HYSS_FE(getlastmod,														arginfo_getlastmod)

	HYSS_FE(base64_decode,													arginfo_base64_decode)
	HYSS_FE(base64_encode,													arginfo_base64_encode)

	HYSS_FE(password_hash,													arginfo_password_hash)
	HYSS_FE(password_get_info,												arginfo_password_get_info)
	HYSS_FE(password_needs_rehash,											arginfo_password_needs_rehash)
	HYSS_FE(password_verify,													arginfo_password_verify)
	HYSS_FE(convert_uuencode,												arginfo_convert_uuencode)
	HYSS_FE(convert_uudecode,												arginfo_convert_uudecode)

	HYSS_FE(abs,																arginfo_abs)
	HYSS_FE(ceil,															arginfo_ceil)
	HYSS_FE(floor,															arginfo_floor)
	HYSS_FE(round,															arginfo_round)
	HYSS_FE(sin,																arginfo_sin)
	HYSS_FE(cos,																arginfo_cos)
	HYSS_FE(tan,																arginfo_tan)
	HYSS_FE(asin,															arginfo_asin)
	HYSS_FE(acos,															arginfo_acos)
	HYSS_FE(atan,															arginfo_atan)
	HYSS_FE(atanh,															arginfo_atanh)
	HYSS_FE(atan2,															arginfo_atan2)
	HYSS_FE(sinh,															arginfo_sinh)
	HYSS_FE(cosh,															arginfo_cosh)
	HYSS_FE(tanh,															arginfo_tanh)
	HYSS_FE(asinh,															arginfo_asinh)
	HYSS_FE(acosh,															arginfo_acosh)
	HYSS_FE(expm1,															arginfo_expm1)
	HYSS_FE(log1p,															arginfo_log1p)
	HYSS_FE(pi,																arginfo_pi)
	HYSS_FE(is_finite,														arginfo_is_finite)
	HYSS_FE(is_nan,															arginfo_is_nan)
	HYSS_FE(is_infinite,														arginfo_is_infinite)
	HYSS_FE(pow,																arginfo_pow)
	HYSS_FE(exp,																arginfo_exp)
	HYSS_FE(log,																arginfo_log)
	HYSS_FE(log10,															arginfo_log10)
	HYSS_FE(sqrt,															arginfo_sqrt)
	HYSS_FE(hypot,															arginfo_hypot)
	HYSS_FE(deg2rad,															arginfo_deg2rad)
	HYSS_FE(rad2deg,															arginfo_rad2deg)
	HYSS_FE(bindec,															arginfo_bindec)
	HYSS_FE(hexdec,															arginfo_hexdec)
	HYSS_FE(octdec,															arginfo_octdec)
	HYSS_FE(decbin,															arginfo_decbin)
	HYSS_FE(decoct,															arginfo_decoct)
	HYSS_FE(dechex,															arginfo_dechex)
	HYSS_FE(base_convert,													arginfo_base_convert)
	HYSS_FE(number_format,													arginfo_number_format)
	HYSS_FE(fmod,															arginfo_fmod)
	HYSS_FE(intdiv,															arginfo_intdiv)
#ifdef HAVE_INET_NTOP
	HYSS_RAW_NAMED_FE(inet_ntop,		zif_inet_ntop,								arginfo_inet_ntop)
#endif
#ifdef HAVE_INET_PTON
	HYSS_RAW_NAMED_FE(inet_pton,		hyss_inet_pton,								arginfo_inet_pton)
#endif
	HYSS_FE(ip2long,															arginfo_ip2long)
	HYSS_FE(long2ip,															arginfo_long2ip)

	HYSS_FE(getenv,															arginfo_getenv)
#ifdef HAVE_PUTENV
	HYSS_FE(putenv,															arginfo_putenv)
#endif

	HYSS_FE(getopt,															arginfo_getopt)

#ifdef HAVE_GETLOADAVG
	HYSS_FE(sys_getloadavg,													arginfo_sys_getloadavg)
#endif
#ifdef HAVE_GETTIMEOFDAY
	HYSS_FE(microtime,														arginfo_microtime)
	HYSS_FE(gettimeofday,													arginfo_gettimeofday)
#endif

#ifdef HAVE_GETRUSAGE
	HYSS_FE(getrusage,														arginfo_getrusage)
#endif

	HYSS_FE(hrtime,															arginfo_hrtime)

#ifdef HAVE_GETTIMEOFDAY
	HYSS_FE(uniqid,															arginfo_uniqid)
#endif

	HYSS_FE(quoted_printable_decode,											arginfo_quoted_printable_decode)
	HYSS_FE(quoted_printable_encode,											arginfo_quoted_printable_encode)
	HYSS_FE(convert_cyr_string,												arginfo_convert_cyr_string)
	HYSS_FE(get_current_user,												arginfo_get_current_user)
	HYSS_FE(set_time_limit,													arginfo_set_time_limit)
	HYSS_FE(header_register_callback,										arginfo_header_register_callback)
	HYSS_FE(get_cfg_var,														arginfo_get_cfg_var)

	HYSS_FE(get_magic_quotes_gpc,										arginfo_get_magic_quotes_gpc)
	HYSS_FE(get_magic_quotes_runtime,									arginfo_get_magic_quotes_runtime)

	HYSS_FE(error_log,														arginfo_error_log)
	HYSS_FE(error_get_last,													arginfo_error_get_last)
	HYSS_FE(error_clear_last,													arginfo_error_clear_last)
	HYSS_FE(call_user_func,													arginfo_call_user_func)
	HYSS_FE(call_user_func_array,											arginfo_call_user_func_array)
	HYSS_FE(forward_static_call,											arginfo_forward_static_call)
	HYSS_FE(forward_static_call_array,										arginfo_forward_static_call_array)
	HYSS_FE(serialize,														arginfo_serialize)
	HYSS_FE(unserialize,														arginfo_unserialize)

	HYSS_FE(var_dump,														arginfo_var_dump)
	HYSS_FE(var_export,														arginfo_var_export)
	HYSS_FE(debug_zval_dump,													arginfo_debug_zval_dump)
	HYSS_FE(print_r,															arginfo_print_r)
	HYSS_FE(memory_get_usage,												arginfo_memory_get_usage)
	HYSS_FE(memory_get_peak_usage,											arginfo_memory_get_peak_usage)

	HYSS_FE(register_shutdown_function,										arginfo_register_shutdown_function)
	HYSS_FE(register_tick_function,											arginfo_register_tick_function)
	HYSS_FE(unregister_tick_function,										arginfo_unregister_tick_function)

	HYSS_FE(highlight_file,													arginfo_highlight_file)
	HYSS_FALIAS(show_source,			highlight_file,							arginfo_highlight_file)
	HYSS_FE(highlight_string,												arginfo_highlight_string)
	HYSS_FE(hyss_strip_whitespace,											arginfo_hyss_strip_whitespace)

	HYSS_FE(ics_get,															arginfo_ics_get)
	HYSS_FE(ics_get_all,														arginfo_ics_get_all)
	HYSS_FE(ics_set,															arginfo_ics_set)
	HYSS_FALIAS(ics_alter,			ics_set,								arginfo_ics_set)
	HYSS_FE(ics_restore,														arginfo_ics_restore)
	HYSS_FE(get_include_path,												arginfo_get_include_path)
	HYSS_FE(set_include_path,												arginfo_set_include_path)
	HYSS_FE(restore_include_path,											arginfo_restore_include_path)

	HYSS_FE(setcookie,														arginfo_setcookie)
	HYSS_FE(setrawcookie,													arginfo_setrawcookie)
	HYSS_FE(header,															arginfo_header)
	HYSS_FE(header_remove,													arginfo_header_remove)
	HYSS_FE(headers_sent,													arginfo_headers_sent)
	HYSS_FE(headers_list,													arginfo_headers_list)
	HYSS_FE(http_response_code,												arginfo_http_response_code)

	HYSS_FE(connection_aborted,												arginfo_connection_aborted)
	HYSS_FE(connection_status,												arginfo_connection_status)
	HYSS_FE(ignore_user_abort,												arginfo_ignore_user_abort)
	HYSS_FE(parse_ics_file,													arginfo_parse_ics_file)
	HYSS_FE(parse_ics_string,												arginfo_parse_ics_string)
#if GEAR_DEBUG
	HYSS_FE(config_get_hash,													arginfo_config_get_hash)
#endif
	HYSS_FE(is_uploaded_file,												arginfo_is_uploaded_file)
	HYSS_FE(move_uploaded_file,												arginfo_move_uploaded_file)

	/* functions from dns.c */
	HYSS_FE(gethostbyaddr,													arginfo_gethostbyaddr)
	HYSS_FE(gethostbyname,													arginfo_gethostbyname)
	HYSS_FE(gethostbynamel,													arginfo_gethostbynamel)

#ifdef HAVE_GETHOSTNAME
	HYSS_FE(gethostname,													arginfo_gethostname)
#endif

#if defined(HYSS_WIN32) || HAVE_GETIFADDRS
	HYSS_FE(net_get_interfaces,												arginfo_net_get_interfaces)
#endif

#if defined(HYSS_WIN32) || HAVE_DNS_SEARCH_FUNC

	HYSS_FE(dns_check_record,												arginfo_dns_check_record)
	HYSS_FALIAS(checkdnsrr,			dns_check_record,						arginfo_dns_check_record)

# if defined(HYSS_WIN32) || HAVE_FULL_DNS_FUNCS
	HYSS_FE(dns_get_mx,														arginfo_dns_get_mx)
	HYSS_FALIAS(getmxrr,				dns_get_mx,					arginfo_dns_get_mx)
	HYSS_FE(dns_get_record,													arginfo_dns_get_record)
# endif
#endif

	/* functions from type.c */
	HYSS_FE(intval,															arginfo_intval)
	HYSS_FE(floatval,														arginfo_floatval)
	HYSS_FALIAS(doubleval,			floatval,								arginfo_floatval)
	HYSS_FE(strval,															arginfo_strval)
	HYSS_FE(boolval,															arginfo_boolval)
	HYSS_FE(gettype,															arginfo_gettype)
	HYSS_FE(settype,															arginfo_settype)
	HYSS_FE(is_null,															arginfo_is_null)
	HYSS_FE(is_resource,														arginfo_is_resource)
	HYSS_FE(is_bool,															arginfo_is_bool)
	HYSS_FE(is_int,															arginfo_is_int)
	HYSS_FE(is_float,														arginfo_is_float)
	HYSS_FALIAS(is_integer,			is_int,									arginfo_is_int)
	HYSS_FALIAS(is_long,				is_int,									arginfo_is_int)
	HYSS_FALIAS(is_double,			is_float,								arginfo_is_float)
	HYSS_FALIAS(is_real,				is_float,								arginfo_is_float)
	HYSS_FE(is_numeric,														arginfo_is_numeric)
	HYSS_FE(is_string,														arginfo_is_string)
	HYSS_FE(is_array,														arginfo_is_array)
	HYSS_FE(is_object,														arginfo_is_object)
	HYSS_FE(is_scalar,														arginfo_is_scalar)
	HYSS_FE(is_callable,														arginfo_is_callable)
	HYSS_FE(is_iterable,														arginfo_is_iterable)
	HYSS_FE(is_countable,													arginfo_is_countable)

	/* functions from file.c */
	HYSS_FE(pclose,															arginfo_pclose)
	HYSS_FE(popen,															arginfo_popen)
	HYSS_FE(readfile,														arginfo_readfile)
	HYSS_FE(rewind,															arginfo_rewind)
	HYSS_FE(rmdir,															arginfo_rmdir)
	HYSS_FE(umask,															arginfo_umask)
	HYSS_FE(fclose,															arginfo_fclose)
	HYSS_FE(feof,															arginfo_feof)
	HYSS_FE(fgetc,															arginfo_fgetc)
	HYSS_FE(fgets,															arginfo_fgets)
	HYSS_DEP_FE(fgetss,														arginfo_fgetss)
	HYSS_FE(fread,															arginfo_fread)
	HYSS_NAMED_FE(fopen,				hyss_if_fopen,							arginfo_fopen)
	HYSS_FE(fpassthru,														arginfo_fpassthru)
	HYSS_NAMED_FE(ftruncate,			hyss_if_ftruncate,						arginfo_ftruncate)
	HYSS_NAMED_FE(fstat,				hyss_if_fstat,							arginfo_fstat)
	HYSS_FE(fseek,															arginfo_fseek)
	HYSS_FE(ftell,															arginfo_ftell)
	HYSS_FE(fflush,															arginfo_fflush)
	HYSS_FE(fwrite,															arginfo_fwrite)
	HYSS_FALIAS(fputs,				fwrite,									arginfo_fwrite)
	HYSS_FE(mkdir,															arginfo_mkdir)
	HYSS_FE(rename,															arginfo_rename)
	HYSS_FE(copy,															arginfo_copy)
	HYSS_FE(tempnam,															arginfo_tempnam)
	HYSS_NAMED_FE(tmpfile,			hyss_if_tmpfile,							arginfo_tmpfile)
	HYSS_FE(file,															arginfo_file)
	HYSS_FE(file_get_contents,												arginfo_file_get_contents)
	HYSS_FE(file_put_contents,												arginfo_file_put_contents)
	HYSS_FE(stream_select,													arginfo_stream_select)
	HYSS_FE(stream_context_create,											arginfo_stream_context_create)
	HYSS_FE(stream_context_set_params,										arginfo_stream_context_set_params)
	HYSS_FE(stream_context_get_params,										arginfo_stream_context_get_params)
	HYSS_FE(stream_context_set_option,										arginfo_stream_context_set_option)
	HYSS_FE(stream_context_get_options,										arginfo_stream_context_get_options)
	HYSS_FE(stream_context_get_default,										arginfo_stream_context_get_default)
	HYSS_FE(stream_context_set_default,										arginfo_stream_context_set_default)
	HYSS_FE(stream_filter_prepend,											arginfo_stream_filter_prepend)
	HYSS_FE(stream_filter_append,											arginfo_stream_filter_append)
	HYSS_FE(stream_filter_remove,											arginfo_stream_filter_remove)
	HYSS_FE(stream_socket_client,											arginfo_stream_socket_client)
	HYSS_FE(stream_socket_server,											arginfo_stream_socket_server)
	HYSS_FE(stream_socket_accept,											arginfo_stream_socket_accept)
	HYSS_FE(stream_socket_get_name,											arginfo_stream_socket_get_name)
	HYSS_FE(stream_socket_recvfrom,											arginfo_stream_socket_recvfrom)
	HYSS_FE(stream_socket_sendto,											arginfo_stream_socket_sendto)
	HYSS_FE(stream_socket_enable_crypto,										arginfo_stream_socket_enable_crypto)
#ifdef HAVE_SHUTDOWN
	HYSS_FE(stream_socket_shutdown,											arginfo_stream_socket_shutdown)
#endif
#if HAVE_SOCKETPAIR
	HYSS_FE(stream_socket_pair,												arginfo_stream_socket_pair)
#endif
	HYSS_FE(stream_copy_to_stream,											arginfo_stream_copy_to_stream)
	HYSS_FE(stream_get_contents,												arginfo_stream_get_contents)
	HYSS_FE(stream_supports_lock,											arginfo_stream_supports_lock)
	HYSS_FE(stream_isatty,													arginfo_stream_isatty)
#ifdef HYSS_WIN32
	HYSS_FE(sapi_windows_vt100_support,										arginfo_sapi_windows_vt100_support)
#endif
	HYSS_FE(fgetcsv,															arginfo_fgetcsv)
	HYSS_FE(fputcsv,															arginfo_fputcsv)
	HYSS_FE(flock,															arginfo_flock)
	HYSS_FE(get_meta_tags,													arginfo_get_meta_tags)
	HYSS_FE(stream_set_read_buffer,											arginfo_stream_set_read_buffer)
	HYSS_FE(stream_set_write_buffer,											arginfo_stream_set_write_buffer)
	HYSS_FALIAS(set_file_buffer, stream_set_write_buffer,					arginfo_stream_set_write_buffer)
	HYSS_FE(stream_set_chunk_size,											arginfo_stream_set_chunk_size)

	HYSS_FE(stream_set_blocking,												arginfo_stream_set_blocking)
	HYSS_FALIAS(socket_set_blocking, stream_set_blocking,					arginfo_stream_set_blocking)

	HYSS_FE(stream_get_meta_data,											arginfo_stream_get_meta_data)
	HYSS_FE(stream_get_line,													arginfo_stream_get_line)
	HYSS_FE(stream_wrapper_register,											arginfo_stream_wrapper_register)
	HYSS_FALIAS(stream_register_wrapper, stream_wrapper_register,			arginfo_stream_wrapper_register)
	HYSS_FE(stream_wrapper_unregister,										arginfo_stream_wrapper_unregister)
	HYSS_FE(stream_wrapper_restore,											arginfo_stream_wrapper_restore)
	HYSS_FE(stream_get_wrappers,												arginfo_stream_get_wrappers)
	HYSS_FE(stream_get_transports,											arginfo_stream_get_transports)
	HYSS_FE(stream_resolve_include_path,										arginfo_stream_resolve_include_path)
	HYSS_FE(stream_is_local,												arginfo_stream_is_local)
	HYSS_FE(get_headers,														arginfo_get_headers)

#if HAVE_SYS_TIME_H || defined(HYSS_WIN32)
	HYSS_FE(stream_set_timeout,												arginfo_stream_set_timeout)
	HYSS_FALIAS(socket_set_timeout, stream_set_timeout,						arginfo_stream_set_timeout)
#endif

	HYSS_FALIAS(socket_get_status, stream_get_meta_data,						arginfo_stream_get_meta_data)

#if HAVE_REALPATH || defined(ZTS)
	HYSS_FE(realpath,														arginfo_realpath)
#endif

#ifdef HAVE_FNMATCH
	HYSS_FE(fnmatch,															arginfo_fnmatch)
#endif

	/* functions from fsock.c */
	HYSS_FE(fsockopen,														arginfo_fsockopen)
	HYSS_FE(pfsockopen,														arginfo_pfsockopen)

	/* functions from pack.c */
	HYSS_FE(pack,															arginfo_pack)
	HYSS_FE(unpack,															arginfo_unpack)

	/* functions from browscap.c */
	HYSS_FE(get_browser,														arginfo_get_browser)

	/* functions from crypt.c */
	HYSS_FE(crypt,															arginfo_crypt)

	/* functions from dir.c */
	HYSS_FE(opendir,															arginfo_opendir)
	HYSS_FE(closedir,														arginfo_closedir)
	HYSS_FE(chdir,															arginfo_chdir)

#if defined(HAVE_CHROOT) && !defined(ZTS) && ENABLE_CHROOT_FUNC
	HYSS_FE(chroot,															arginfo_chroot)
#endif

	HYSS_FE(getcwd,															arginfo_getcwd)
	HYSS_FE(rewinddir,														arginfo_rewinddir)
	HYSS_NAMED_FE(readdir,			hyss_if_readdir,							arginfo_readdir)
	HYSS_FALIAS(dir,					getdir,									arginfo_dir)
	HYSS_FE(scandir,															arginfo_scandir)
#ifdef HAVE_GLOB
	HYSS_FE(glob,															arginfo_glob)
#endif
	/* functions from filestat.c */
	HYSS_FE(fileatime,														arginfo_fileatime)
	HYSS_FE(filectime,														arginfo_filectime)
	HYSS_FE(filegroup,														arginfo_filegroup)
	HYSS_FE(fileinode,														arginfo_fileinode)
	HYSS_FE(filemtime,														arginfo_filemtime)
	HYSS_FE(fileowner,														arginfo_fileowner)
	HYSS_FE(fileperms,														arginfo_fileperms)
	HYSS_FE(filesize,														arginfo_filesize)
	HYSS_FE(filetype,														arginfo_filetype)
	HYSS_FE(file_exists,														arginfo_file_exists)
	HYSS_FE(is_writable,														arginfo_is_writable)
	HYSS_FALIAS(is_writeable,		is_writable,							arginfo_is_writable)
	HYSS_FE(is_readable,														arginfo_is_readable)
	HYSS_FE(is_executable,													arginfo_is_executable)
	HYSS_FE(is_file,															arginfo_is_file)
	HYSS_FE(is_dir,															arginfo_is_dir)
	HYSS_FE(is_link,															arginfo_is_link)
	HYSS_NAMED_FE(stat,				hyss_if_stat,							arginfo_stat)
	HYSS_NAMED_FE(lstat,				hyss_if_lstat,							arginfo_lstat)
	HYSS_FE(chown,															arginfo_chown)
	HYSS_FE(chgrp,															arginfo_chgrp)
#if HAVE_LCHOWN
	HYSS_FE(lchown,															arginfo_lchown)
#endif
#if HAVE_LCHOWN
	HYSS_FE(lchgrp,															arginfo_lchgrp)
#endif
	HYSS_FE(chmod,															arginfo_chmod)
#if HAVE_UTIME
	HYSS_FE(touch,															arginfo_touch)
#endif
	HYSS_FE(clearstatcache,													arginfo_clearstatcache)
	HYSS_FE(disk_total_space,												arginfo_disk_total_space)
	HYSS_FE(disk_free_space,													arginfo_disk_free_space)
	HYSS_FALIAS(diskfreespace,		disk_free_space,						arginfo_disk_free_space)
	HYSS_FE(realpath_cache_size,												arginfo_realpath_cache_size)
	HYSS_FE(realpath_cache_get,												arginfo_realpath_cache_get)

	/* functions from mail.c */
	HYSS_FE(mail,															arginfo_mail)
	HYSS_FE(ezmlm_hash,														arginfo_ezmlm_hash)

	/* functions from syslog.c */
#ifdef HAVE_SYSLOG_H
	HYSS_FE(openlog,															arginfo_openlog)
	HYSS_FE(syslog,															arginfo_syslog)
	HYSS_FE(closelog,														arginfo_closelog)
#endif

	/* functions from lcg.c */
	HYSS_FE(lcg_value,														arginfo_lcg_value)

	/* functions from metaphone.c */
	HYSS_FE(metaphone,														arginfo_metaphone)

	/* functions from output.c */
	HYSS_FE(ob_start,														arginfo_ob_start)
	HYSS_FE(ob_flush,														arginfo_ob_flush)
	HYSS_FE(ob_clean,														arginfo_ob_clean)
	HYSS_FE(ob_end_flush,													arginfo_ob_end_flush)
	HYSS_FE(ob_end_clean,													arginfo_ob_end_clean)
	HYSS_FE(ob_get_flush,													arginfo_ob_get_flush)
	HYSS_FE(ob_get_clean,													arginfo_ob_get_clean)
	HYSS_FE(ob_get_length,													arginfo_ob_get_length)
	HYSS_FE(ob_get_level,													arginfo_ob_get_level)
	HYSS_FE(ob_get_status,													arginfo_ob_get_status)
	HYSS_FE(ob_get_contents,													arginfo_ob_get_contents)
	HYSS_FE(ob_implicit_flush,												arginfo_ob_implicit_flush)
	HYSS_FE(ob_list_handlers,												arginfo_ob_list_handlers)

	/* functions from array.c */
	HYSS_FE(ksort,															arginfo_ksort)
	HYSS_FE(krsort,															arginfo_krsort)
	HYSS_FE(natsort,															arginfo_natsort)
	HYSS_FE(natcasesort,														arginfo_natcasesort)
	HYSS_FE(asort,															arginfo_asort)
	HYSS_FE(arsort,															arginfo_arsort)
	HYSS_FE(sort,															arginfo_sort)
	HYSS_FE(rsort,															arginfo_rsort)
	HYSS_FE(usort,															arginfo_usort)
	HYSS_FE(uasort,															arginfo_uasort)
	HYSS_FE(uksort,															arginfo_uksort)
	HYSS_FE(shuffle,															arginfo_shuffle)
	HYSS_FE(array_walk,														arginfo_array_walk)
	HYSS_FE(array_walk_recursive,											arginfo_array_walk_recursive)
	HYSS_FE(count,															arginfo_count)
	HYSS_FE(end,																arginfo_end)
	HYSS_FE(prev,															arginfo_prev)
	HYSS_FE(next,															arginfo_next)
	HYSS_FE(reset,															arginfo_reset)
	HYSS_FE(current,															arginfo_current)
	HYSS_FE(key,																arginfo_key)
	HYSS_FE(min,																arginfo_min)
	HYSS_FE(max,																arginfo_max)
	HYSS_FE(in_array,														arginfo_in_array)
	HYSS_FE(array_search,													arginfo_array_search)
	HYSS_FE(extract,															arginfo_extract)
	HYSS_FE(compact,															arginfo_compact)
	HYSS_FE(array_fill,														arginfo_array_fill)
	HYSS_FE(array_fill_keys,													arginfo_array_fill_keys)
	HYSS_FE(range,															arginfo_range)
	HYSS_FE(array_multisort,													arginfo_array_multisort)
	HYSS_FE(array_push,														arginfo_array_push)
	HYSS_FE(array_pop,														arginfo_array_pop)
	HYSS_FE(array_shift,														arginfo_array_shift)
	HYSS_FE(array_unshift,													arginfo_array_unshift)
	HYSS_FE(array_splice,													arginfo_array_splice)
	HYSS_FE(array_slice,														arginfo_array_slice)
	HYSS_FE(array_merge,														arginfo_array_merge)
	HYSS_FE(array_merge_recursive,											arginfo_array_merge_recursive)
	HYSS_FE(array_replace,													arginfo_array_replace)
	HYSS_FE(array_replace_recursive,											arginfo_array_replace_recursive)
	HYSS_FE(array_keys,														arginfo_array_keys)
	HYSS_FE(array_key_first,													arginfo_array_key_first)
	HYSS_FE(array_key_last,													arginfo_array_key_last)
	HYSS_FE(array_values,													arginfo_array_values)
	HYSS_FE(array_count_values,												arginfo_array_count_values)
	HYSS_FE(array_column,													arginfo_array_column)
	HYSS_FE(array_reverse,													arginfo_array_reverse)
	HYSS_FE(array_reduce,													arginfo_array_reduce)
	HYSS_FE(array_pad,														arginfo_array_pad)
	HYSS_FE(array_flip,														arginfo_array_flip)
	HYSS_FE(array_change_key_case,											arginfo_array_change_key_case)
	HYSS_FE(array_rand,														arginfo_array_rand)
	HYSS_FE(array_unique,													arginfo_array_unique)
	HYSS_FE(array_intersect,													arginfo_array_intersect)
	HYSS_FE(array_intersect_key,												arginfo_array_intersect_key)
	HYSS_FE(array_intersect_ukey,											arginfo_array_intersect_ukey)
	HYSS_FE(array_uintersect,												arginfo_array_uintersect)
	HYSS_FE(array_intersect_assoc,											arginfo_array_intersect_assoc)
	HYSS_FE(array_uintersect_assoc,											arginfo_array_uintersect_assoc)
	HYSS_FE(array_intersect_uassoc,											arginfo_array_intersect_uassoc)
	HYSS_FE(array_uintersect_uassoc,											arginfo_array_uintersect_uassoc)
	HYSS_FE(array_diff,														arginfo_array_diff)
	HYSS_FE(array_diff_key,													arginfo_array_diff_key)
	HYSS_FE(array_diff_ukey,													arginfo_array_diff_ukey)
	HYSS_FE(array_udiff,														arginfo_array_udiff)
	HYSS_FE(array_diff_assoc,												arginfo_array_diff_assoc)
	HYSS_FE(array_udiff_assoc,												arginfo_array_udiff_assoc)
	HYSS_FE(array_diff_uassoc,												arginfo_array_diff_uassoc)
	HYSS_FE(array_udiff_uassoc,												arginfo_array_udiff_uassoc)
	HYSS_FE(array_sum,														arginfo_array_sum)
	HYSS_FE(array_product,													arginfo_array_product)
	HYSS_FE(array_filter,													arginfo_array_filter)
	HYSS_FE(array_map,														arginfo_array_map)
	HYSS_FE(array_chunk,														arginfo_array_chunk)
	HYSS_FE(array_combine,													arginfo_array_combine)
	HYSS_FE(array_key_exists,												arginfo_array_key_exists)

	/* aliases from array.c */
	HYSS_FALIAS(pos,					current,								arginfo_current)
	HYSS_FALIAS(sizeof,				count,									arginfo_count)
	HYSS_FALIAS(key_exists,			array_key_exists,						arginfo_array_key_exists)

	/* functions from assert.c */
	HYSS_FE(assert,															arginfo_assert)
	HYSS_FE(assert_options,													arginfo_assert_options)

	/* functions from versioning.c */
	HYSS_FE(version_compare,													arginfo_version_compare)

	/* functions from ftok.c*/
#if HAVE_FTOK
	HYSS_FE(ftok,															arginfo_ftok)
#endif

	HYSS_FE(str_rot13,														arginfo_str_rot13)
	HYSS_FE(stream_get_filters,												arginfo_stream_get_filters)
	HYSS_FE(stream_filter_register,											arginfo_stream_filter_register)
	HYSS_FE(stream_bucket_make_writeable,									arginfo_stream_bucket_make_writeable)
	HYSS_FE(stream_bucket_prepend,											arginfo_stream_bucket_prepend)
	HYSS_FE(stream_bucket_append,											arginfo_stream_bucket_append)
	HYSS_FE(stream_bucket_new,												arginfo_stream_bucket_new)

	HYSS_FE(output_add_rewrite_var,											arginfo_output_add_rewrite_var)
	HYSS_FE(output_reset_rewrite_vars,										arginfo_output_reset_rewrite_vars)

	HYSS_FE(sys_get_temp_dir,												arginfo_sys_get_temp_dir)

#ifdef HYSS_WIN32
	HYSS_FE(sapi_windows_cp_set, arginfo_sapi_windows_cp_set)
	HYSS_FE(sapi_windows_cp_get, arginfo_sapi_windows_cp_get)
	HYSS_FE(sapi_windows_cp_is_utf8, arginfo_sapi_windows_cp_is_utf8)
	HYSS_FE(sapi_windows_cp_conv, arginfo_sapi_windows_cp_conv)
#endif
	HYSS_FE_END
};
/* }}} */

static const gear_capi_dep standard_deps[] = { /* {{{ */
	GEAR_CAPI_OPTIONAL("session")
	GEAR_CAPI_END
};
/* }}} */

gear_capi_entry basic_functions_capi = { /* {{{ */
	STANDARD_CAPI_HEADER_EX,
	NULL,
	standard_deps,
	"standard",					/* extension name */
	basic_functions,			/* function list */
	HYSS_MINIT(basic),			/* process startup */
	HYSS_MSHUTDOWN(basic),		/* process shutdown */
	HYSS_RINIT(basic),			/* request startup */
	HYSS_RSHUTDOWN(basic),		/* request shutdown */
	HYSS_MINFO(basic),			/* extension info */
	HYSS_STANDARD_VERSION,		/* extension version */
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#if defined(HAVE_PUTENV)
static void hyss_putenv_destructor(zval *zv) /* {{{ */
{
	putenv_entry *pe = Z_PTR_P(zv);

	if (pe->previous_value) {
# if defined(HYSS_WIN32)
		/* MSVCRT has a bug in putenv() when setting a variable that
		 * is already set; if the SetEnvironmentVariable() API call
		 * fails, the Crt will double free() a string.
		 * We try to avoid this by setting our own value first */
		SetEnvironmentVariable(pe->key, "bugbug");
# endif
		putenv(pe->previous_value);
# if defined(HYSS_WIN32)
		efree(pe->previous_value);
# endif
	} else {
# if HAVE_UNSETENV
		unsetenv(pe->key);
# elif defined(HYSS_WIN32)
		SetEnvironmentVariable(pe->key, NULL);
# ifndef ZTS
		_putenv_s(pe->key, "");
# endif
# else
		char **env;

		for (env = environ; env != NULL && *env != NULL; env++) {
			if (!strncmp(*env, pe->key, pe->key_len) && (*env)[pe->key_len] == '=') {	/* found it */
				*env = "";
				break;
			}
		}
# endif
	}
#ifdef HAVE_TZSET
	/* don't forget to reset the various libc globals that
	 * we might have changed by an earlier call to tzset(). */
	if (!strncmp(pe->key, "TZ", pe->key_len)) {
		tzset();
	}
#endif

	efree(pe->putenv_string);
	efree(pe->key);
	efree(pe);
}
/* }}} */
#endif

static void basic_globals_ctor(hyss_basic_globals *basic_globals_p) /* {{{ */
{
	BG(mt_rand_is_seeded) = 0;
	BG(mt_rand_mode) = MT_RAND_MT19937;
	BG(umask) = -1;
	BG(next) = NULL;
	BG(left) = -1;
	BG(user_tick_functions) = NULL;
	BG(user_filter_map) = NULL;
	BG(serialize_lock) = 0;

	memset(&BG(serialize), 0, sizeof(BG(serialize)));
	memset(&BG(unserialize), 0, sizeof(BG(unserialize)));

	memset(&BG(url_adapt_session_ex), 0, sizeof(BG(url_adapt_session_ex)));
	memset(&BG(url_adapt_output_ex), 0, sizeof(BG(url_adapt_output_ex)));

	BG(url_adapt_session_ex).type = 1;
	BG(url_adapt_output_ex).type  = 0;

	gear_hash_init(&BG(url_adapt_session_hosts_ht), 0, NULL, NULL, 1);
	gear_hash_init(&BG(url_adapt_output_hosts_ht), 0, NULL, NULL, 1);

#if defined(_REENTRANT) && defined(HAVE_MBRLEN) && defined(HAVE_MBSTATE_T)
	memset(&BG(mblen_state), 0, sizeof(BG(mblen_state)));
#endif

	BG(incomplete_class) = incomplete_class_entry;
	BG(page_uid) = -1;
	BG(page_gid) = -1;
}
/* }}} */

static void basic_globals_dtor(hyss_basic_globals *basic_globals_p) /* {{{ */
{
	if (basic_globals_p->url_adapt_session_ex.tags) {
		gear_hash_destroy(basic_globals_p->url_adapt_session_ex.tags);
		free(basic_globals_p->url_adapt_session_ex.tags);
	}
	if (basic_globals_p->url_adapt_output_ex.tags) {
		gear_hash_destroy(basic_globals_p->url_adapt_output_ex.tags);
		free(basic_globals_p->url_adapt_output_ex.tags);
	}

	gear_hash_destroy(&basic_globals_p->url_adapt_session_hosts_ht);
	gear_hash_destroy(&basic_globals_p->url_adapt_output_hosts_ht);
}
/* }}} */

HYSSAPI double hyss_get_nan(void) /* {{{ */
{
	return GEAR_NAN;
}
/* }}} */

HYSSAPI double hyss_get_inf(void) /* {{{ */
{
	return GEAR_INFINITY;
}
/* }}} */

#define BASIC_MINIT_SUBCAPI(cAPI) \
	if (HYSS_MINIT(cAPI)(INIT_FUNC_ARGS_PASSTHRU) == SUCCESS) {\
		BASIC_ADD_SUBCAPI(cAPI); \
	}

#define BASIC_ADD_SUBCAPI(cAPI) \
	gear_hash_str_add_empty_element(&basic_subcAPIs, #cAPI, strlen(#cAPI));

#define BASIC_RINIT_SUBCAPI(cAPI) \
	if (gear_hash_str_exists(&basic_subcAPIs, #cAPI, strlen(#cAPI))) { \
		HYSS_RINIT(cAPI)(INIT_FUNC_ARGS_PASSTHRU); \
	}

#define BASIC_MINFO_SUBCAPI(cAPI) \
	if (gear_hash_str_exists(&basic_subcAPIs, #cAPI, strlen(#cAPI))) { \
		HYSS_MINFO(cAPI)(GEAR_CAPI_INFO_FUNC_ARGS_PASSTHRU); \
	}

#define BASIC_RSHUTDOWN_SUBCAPI(cAPI) \
	if (gear_hash_str_exists(&basic_subcAPIs, #cAPI, strlen(#cAPI))) { \
		HYSS_RSHUTDOWN(cAPI)(SHUTDOWN_FUNC_ARGS_PASSTHRU); \
	}

#define BASIC_MSHUTDOWN_SUBCAPI(cAPI) \
	if (gear_hash_str_exists(&basic_subcAPIs, #cAPI, strlen(#cAPI))) { \
		HYSS_MSHUTDOWN(cAPI)(SHUTDOWN_FUNC_ARGS_PASSTHRU); \
	}

HYSS_MINIT_FUNCTION(basic) /* {{{ */
{
#ifdef ZTS
	ts_allocate_id(&basic_globals_id, sizeof(hyss_basic_globals), (ts_allocate_ctor) basic_globals_ctor, (ts_allocate_dtor) basic_globals_dtor);
#ifdef HYSS_WIN32
	ts_allocate_id(&hyss_win32_core_globals_id, sizeof(hyss_win32_core_globals), (ts_allocate_ctor)hyss_win32_core_globals_ctor, (ts_allocate_dtor)hyss_win32_core_globals_dtor );
#endif
#else
	basic_globals_ctor(&basic_globals);
#ifdef HYSS_WIN32
	hyss_win32_core_globals_ctor(&the_hyss_win32_core_globals);
#endif
#endif

	gear_hash_init(&basic_subcAPIs, 0, NULL, NULL, 1);

	BG(incomplete_class) = incomplete_class_entry = hyss_create_incomplete_class();

	REGISTER_LONG_CONSTANT("CONNECTION_ABORTED", HYSS_CONNECTION_ABORTED, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("CONNECTION_NORMAL",  HYSS_CONNECTION_NORMAL,  CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("CONNECTION_TIMEOUT", HYSS_CONNECTION_TIMEOUT, CONST_CS | CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("ICS_USER",   GEAR_ICS_USER,   CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ICS_PERDIR", GEAR_ICS_PERDIR, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ICS_SYSTEM", GEAR_ICS_SYSTEM, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ICS_ALL",    GEAR_ICS_ALL,    CONST_CS | CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("ICS_SCANNER_NORMAL", GEAR_ICS_SCANNER_NORMAL, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ICS_SCANNER_RAW",    GEAR_ICS_SCANNER_RAW,    CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ICS_SCANNER_TYPED",  GEAR_ICS_SCANNER_TYPED,  CONST_CS | CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("HYSS_URL_SCHEME", HYSS_URL_SCHEME, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_URL_HOST", HYSS_URL_HOST, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_URL_PORT", HYSS_URL_PORT, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_URL_USER", HYSS_URL_USER, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_URL_PASS", HYSS_URL_PASS, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_URL_PATH", HYSS_URL_PATH, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_URL_QUERY", HYSS_URL_QUERY, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_URL_FRAGMENT", HYSS_URL_FRAGMENT, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_QUERY_RFC1738", HYSS_QUERY_RFC1738, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_QUERY_RFC3986", HYSS_QUERY_RFC3986, CONST_CS | CONST_PERSISTENT);

#define REGISTER_MATH_CONSTANT(x)  REGISTER_DOUBLE_CONSTANT(#x, x, CONST_CS | CONST_PERSISTENT)
	REGISTER_MATH_CONSTANT(M_E);
	REGISTER_MATH_CONSTANT(M_LOG2E);
	REGISTER_MATH_CONSTANT(M_LOG10E);
	REGISTER_MATH_CONSTANT(M_LN2);
	REGISTER_MATH_CONSTANT(M_LN10);
	REGISTER_MATH_CONSTANT(M_PI);
	REGISTER_MATH_CONSTANT(M_PI_2);
	REGISTER_MATH_CONSTANT(M_PI_4);
	REGISTER_MATH_CONSTANT(M_1_PI);
	REGISTER_MATH_CONSTANT(M_2_PI);
	REGISTER_MATH_CONSTANT(M_SQRTPI);
	REGISTER_MATH_CONSTANT(M_2_SQRTPI);
	REGISTER_MATH_CONSTANT(M_LNPI);
	REGISTER_MATH_CONSTANT(M_EULER);
	REGISTER_MATH_CONSTANT(M_SQRT2);
	REGISTER_MATH_CONSTANT(M_SQRT1_2);
	REGISTER_MATH_CONSTANT(M_SQRT3);
	REGISTER_DOUBLE_CONSTANT("INF", GEAR_INFINITY, CONST_CS | CONST_PERSISTENT);
	REGISTER_DOUBLE_CONSTANT("NAN", GEAR_NAN, CONST_CS | CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("HYSS_ROUND_HALF_UP", HYSS_ROUND_HALF_UP, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_ROUND_HALF_DOWN", HYSS_ROUND_HALF_DOWN, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_ROUND_HALF_EVEN", HYSS_ROUND_HALF_EVEN, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSS_ROUND_HALF_ODD", HYSS_ROUND_HALF_ODD, CONST_CS | CONST_PERSISTENT);

#if ENABLE_TEST_CLASS
	test_class_startup();
#endif

	register_hyssinfo_constants(INIT_FUNC_ARGS_PASSTHRU);
	register_html_constants(INIT_FUNC_ARGS_PASSTHRU);
	register_string_constants(INIT_FUNC_ARGS_PASSTHRU);

	BASIC_ADD_SUBCAPI(dl)
	BASIC_ADD_SUBCAPI(mail)
	BASIC_ADD_SUBCAPI(streams)
	BASIC_MINIT_SUBCAPI(file)
	BASIC_MINIT_SUBCAPI(pack)
	BASIC_MINIT_SUBCAPI(browscap)
	BASIC_MINIT_SUBCAPI(standard_filters)
	BASIC_MINIT_SUBCAPI(user_filters)
	BASIC_MINIT_SUBCAPI(password)
	BASIC_MINIT_SUBCAPI(mt_rand)

#if defined(HAVE_LOCALECONV) && defined(ZTS)
	BASIC_MINIT_SUBCAPI(localeconv)
#endif

#if defined(HAVE_NL_LANGINFO)
	BASIC_MINIT_SUBCAPI(nl_langinfo)
#endif

#if GEAR_INTRIN_SSE4_2_FUNC_PTR
	BASIC_MINIT_SUBCAPI(string_intrin)
#endif

#if GEAR_INTRIN_AVX2_FUNC_PTR || GEAR_INTRIN_SSSE3_FUNC_PTR
	BASIC_MINIT_SUBCAPI(base64_intrin)
#endif

	BASIC_MINIT_SUBCAPI(crypt)
	BASIC_MINIT_SUBCAPI(lcg)

	BASIC_MINIT_SUBCAPI(dir)
#ifdef HAVE_SYSLOG_H
	BASIC_MINIT_SUBCAPI(syslog)
#endif
	BASIC_MINIT_SUBCAPI(array)
	BASIC_MINIT_SUBCAPI(assert)
	BASIC_MINIT_SUBCAPI(url_scanner_ex)
#ifdef HYSS_CAN_SUPPORT_PROC_OPEN
	BASIC_MINIT_SUBCAPI(proc_open)
#endif
	BASIC_MINIT_SUBCAPI(exec)

	BASIC_MINIT_SUBCAPI(user_streams)
	BASIC_MINIT_SUBCAPI(imagetypes)

	hyss_register_url_stream_wrapper("hyss", &hyss_stream_hyss_wrapper);
	hyss_register_url_stream_wrapper("file", &hyss_plain_files_wrapper);
#ifdef HAVE_GLOB
	hyss_register_url_stream_wrapper("glob", &hyss_glob_stream_wrapper);
#endif
	hyss_register_url_stream_wrapper("data", &hyss_stream_rfc2397_wrapper);
	hyss_register_url_stream_wrapper("http", &hyss_stream_http_wrapper);
	hyss_register_url_stream_wrapper("ftp", &hyss_stream_ftp_wrapper);

#if defined(HYSS_WIN32) || HAVE_DNS_SEARCH_FUNC
# if defined(HYSS_WIN32) || HAVE_FULL_DNS_FUNCS
	BASIC_MINIT_SUBCAPI(dns)
# endif
#endif

	BASIC_MINIT_SUBCAPI(random)

	BASIC_MINIT_SUBCAPI(hrtime)

	return SUCCESS;
}
/* }}} */

HYSS_MSHUTDOWN_FUNCTION(basic) /* {{{ */
{
#ifdef HAVE_SYSLOG_H
	HYSS_MSHUTDOWN(syslog)(SHUTDOWN_FUNC_ARGS_PASSTHRU);
#endif
#ifdef ZTS
	ts_free_id(basic_globals_id);
#ifdef HYSS_WIN32
	ts_free_id(hyss_win32_core_globals_id);
#endif
#else
	basic_globals_dtor(&basic_globals);
#ifdef HYSS_WIN32
	hyss_win32_core_globals_dtor(&the_hyss_win32_core_globals);
#endif
#endif

	hyss_unregister_url_stream_wrapper("hyss");
	hyss_unregister_url_stream_wrapper("http");
	hyss_unregister_url_stream_wrapper("ftp");

	BASIC_MSHUTDOWN_SUBCAPI(browscap)
	BASIC_MSHUTDOWN_SUBCAPI(array)
	BASIC_MSHUTDOWN_SUBCAPI(assert)
	BASIC_MSHUTDOWN_SUBCAPI(url_scanner_ex)
	BASIC_MSHUTDOWN_SUBCAPI(file)
	BASIC_MSHUTDOWN_SUBCAPI(standard_filters)
#if defined(HAVE_LOCALECONV) && defined(ZTS)
	BASIC_MSHUTDOWN_SUBCAPI(localeconv)
#endif
	BASIC_MSHUTDOWN_SUBCAPI(crypt)
	BASIC_MSHUTDOWN_SUBCAPI(random)

	gear_hash_destroy(&basic_subcAPIs);
	return SUCCESS;
}
/* }}} */

HYSS_RINIT_FUNCTION(basic) /* {{{ */
{
	memset(BG(strtok_table), 0, 256);

	BG(serialize_lock) = 0;
	memset(&BG(serialize), 0, sizeof(BG(serialize)));
	memset(&BG(unserialize), 0, sizeof(BG(unserialize)));

	BG(strtok_string) = NULL;
	ZVAL_UNDEF(&BG(strtok_zval));
	BG(strtok_last) = NULL;
	BG(locale_string) = NULL;
	BG(locale_changed) = 0;
	BG(array_walk_fci) = empty_fcall_info;
	BG(array_walk_fci_cache) = empty_fcall_info_cache;
	BG(user_compare_fci) = empty_fcall_info;
	BG(user_compare_fci_cache) = empty_fcall_info_cache;
	BG(page_uid) = -1;
	BG(page_gid) = -1;
	BG(page_inode) = -1;
	BG(page_mtime) = -1;
#ifdef HAVE_PUTENV
	gear_hash_init(&BG(putenv_ht), 1, NULL, hyss_putenv_destructor, 0);
#endif
	BG(user_shutdown_function_names) = NULL;

	HYSS_RINIT(filestat)(INIT_FUNC_ARGS_PASSTHRU);
#ifdef HAVE_SYSLOG_H
	BASIC_RINIT_SUBCAPI(syslog)
#endif
	BASIC_RINIT_SUBCAPI(dir)
	BASIC_RINIT_SUBCAPI(url_scanner_ex)

	/* Setup default context */
	FG(default_context) = NULL;

	/* Default to global wrappers only */
	FG(stream_wrappers) = NULL;

	/* Default to global filters only */
	FG(stream_filters) = NULL;

	return SUCCESS;
}
/* }}} */

HYSS_RSHUTDOWN_FUNCTION(basic) /* {{{ */
{
	zval_ptr_dtor(&BG(strtok_zval));
	ZVAL_UNDEF(&BG(strtok_zval));
	BG(strtok_string) = NULL;
#ifdef HAVE_PUTENV
	gear_hash_destroy(&BG(putenv_ht));
#endif

	BG(mt_rand_is_seeded) = 0;

	if (BG(umask) != -1) {
		umask(BG(umask));
	}

	/* Check if locale was changed and change it back
	 * to the value in startup environment */
	if (BG(locale_changed)) {
		setlocale(LC_ALL, "C");
		setlocale(LC_CTYPE, "");
		gear_update_current_locale();
		if (BG(locale_string)) {
			gear_string_release_ex(BG(locale_string), 0);
			BG(locale_string) = NULL;
		}
	}

	/* FG(stream_wrappers) and FG(stream_filters) are destroyed
	 * during hyss_request_shutdown() */

	HYSS_RSHUTDOWN(filestat)(SHUTDOWN_FUNC_ARGS_PASSTHRU);
#ifdef HAVE_SYSLOG_H
#ifdef HYSS_WIN32
	BASIC_RSHUTDOWN_SUBCAPI(syslog)(SHUTDOWN_FUNC_ARGS_PASSTHRU);
#endif
#endif
	BASIC_RSHUTDOWN_SUBCAPI(assert)
	BASIC_RSHUTDOWN_SUBCAPI(url_scanner_ex)
	BASIC_RSHUTDOWN_SUBCAPI(streams)
#ifdef HYSS_WIN32
	BASIC_RSHUTDOWN_SUBCAPI(win32_core_globals)
#endif

	if (BG(user_tick_functions)) {
		gear_llist_destroy(BG(user_tick_functions));
		efree(BG(user_tick_functions));
		BG(user_tick_functions) = NULL;
	}

	BASIC_RSHUTDOWN_SUBCAPI(user_filters)
	BASIC_RSHUTDOWN_SUBCAPI(browscap)

 	BG(page_uid) = -1;
 	BG(page_gid) = -1;
	return SUCCESS;
}
/* }}} */

HYSS_MINFO_FUNCTION(basic) /* {{{ */
{
	hyss_info_print_table_start();
	BASIC_MINFO_SUBCAPI(dl)
	BASIC_MINFO_SUBCAPI(mail)
	hyss_info_print_table_end();
	BASIC_MINFO_SUBCAPI(assert)
}
/* }}} */

/* {{{ proto mixed constant(string const_name)
   Given the name of a constant this function will return the constant's associated value */
HYSS_FUNCTION(constant)
{
	gear_string *const_name;
	zval *c;
	gear_class_entry *scope;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(const_name)
	GEAR_PARSE_PARAMETERS_END();

	scope = gear_get_executed_scope();
	c = gear_get_constant_ex(const_name, scope, GEAR_FETCH_CLASS_SILENT);
	if (c) {
		ZVAL_COPY_OR_DUP(return_value, c);
		if (Z_TYPE_P(return_value) == IS_CONSTANT_AST) {
			if (UNEXPECTED(zval_update_constant_ex(return_value, scope) != SUCCESS)) {
				return;
			}
		}
	} else {
		if (!EG(exception)) {
			hyss_error_docref(NULL, E_WARNING, "Couldn't find constant %s", ZSTR_VAL(const_name));
		}
		RETURN_NULL();
	}
}
/* }}} */

#ifdef HAVE_INET_NTOP
/* {{{ proto string inet_ntop(string in_addr)
   Converts a packed inet address to a human readable IP address string */
HYSS_NAMED_FUNCTION(zif_inet_ntop)
{
	char *address;
	size_t address_len;
	int af = AF_INET;
	char buffer[40];

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(address, address_len)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

#ifdef HAVE_IPV6
	if (address_len == 16) {
		af = AF_INET6;
	} else
#endif
	if (address_len != 4) {
		RETURN_FALSE;
	}

	if (!inet_ntop(af, address, buffer, sizeof(buffer))) {
		RETURN_FALSE;
	}

	RETURN_STRING(buffer);
}
/* }}} */
#endif /* HAVE_INET_NTOP */

#ifdef HAVE_INET_PTON
/* {{{ proto string inet_pton(string ip_address)
   Converts a human readable IP address to a packed binary string */
HYSS_NAMED_FUNCTION(hyss_inet_pton)
{
	int ret, af = AF_INET;
	char *address;
	size_t address_len;
	char buffer[17];

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(address, address_len)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	memset(buffer, 0, sizeof(buffer));

#ifdef HAVE_IPV6
	if (strchr(address, ':')) {
		af = AF_INET6;
	} else
#endif
	if (!strchr(address, '.')) {
		RETURN_FALSE;
	}

	ret = inet_pton(af, address, buffer);

	if (ret <= 0) {
		RETURN_FALSE;
	}

	RETURN_STRINGL(buffer, af == AF_INET ? 4 : 16);
}
/* }}} */
#endif /* HAVE_INET_PTON */

/* {{{ proto int ip2long(string ip_address)
   Converts a string containing an (IPv4) Internet Protocol dotted address into a proper address */
HYSS_FUNCTION(ip2long)
{
	char *addr;
	size_t addr_len;
#ifdef HAVE_INET_PTON
	struct in_addr ip;
#else
	gear_ulong ip;
#endif

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(addr, addr_len)
	GEAR_PARSE_PARAMETERS_END();

#ifdef HAVE_INET_PTON
	if (addr_len == 0 || inet_pton(AF_INET, addr, &ip) != 1) {
		RETURN_FALSE;
	}
	RETURN_LONG(ntohl(ip.s_addr));
#else
	if (addr_len == 0 || (ip = inet_addr(addr)) == INADDR_NONE) {
		/* The only special case when we should return -1 ourselves,
		 * because inet_addr() considers it wrong. We return 0xFFFFFFFF and
		 * not -1 or ~0 because of 32/64bit issues. */
		if (addr_len == sizeof("255.255.255.255") - 1 &&
			!memcmp(addr, "255.255.255.255", sizeof("255.255.255.255") - 1)
		) {
			RETURN_LONG(0xFFFFFFFF);
		}
		RETURN_FALSE;
	}
	RETURN_LONG(ntohl(ip));
#endif
}
/* }}} */

/* {{{ proto string long2ip(int proper_address)
   Converts an (IPv4) Internet network address into a string in Internet standard dotted format */
HYSS_FUNCTION(long2ip)
{
	gear_ulong ip;
	gear_long sip;
	struct in_addr myaddr;
#ifdef HAVE_INET_PTON
	char str[40];
#endif

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(sip)
	GEAR_PARSE_PARAMETERS_END();

	/* autoboxes on 32bit platforms, but that's expected */
	ip = (gear_ulong)sip;

	myaddr.s_addr = htonl(ip);
#ifdef HAVE_INET_PTON
	if (inet_ntop(AF_INET, &myaddr, str, sizeof(str))) {
		RETURN_STRING(str);
	} else {
		RETURN_FALSE;
	}
#else
	RETURN_STRING(inet_ntoa(myaddr));
#endif
}
/* }}} */

/********************
 * System Functions *
 ********************/

/* {{{ proto string getenv(string varname[, bool local_only]
   Get the value of an environment variable or every available environment variable
   if no varname is present  */
HYSS_FUNCTION(getenv)
{
	char *ptr, *str = NULL;
	size_t str_len;
	gear_bool local_only = 0;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(str, str_len)
		Z_PARAM_BOOL(local_only)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (!str) {
		array_init(return_value);
		hyss_import_environment_variables(return_value);
		return;
	}

	if (!local_only) {
		/* SAPI method returns an emalloc()'d string */
		ptr = sapi_getenv(str, str_len);
		if (ptr) {
			// TODO: avoid realocation ???
			RETVAL_STRING(ptr);
			efree(ptr);
			return;
		}
	}
#ifdef HYSS_WIN32
	{
		wchar_t dummybuf;
		DWORD size;
		wchar_t *keyw, *valw;

		keyw = hyss_win32_cp_conv_any_to_w(str, str_len, HYSS_WIN32_CP_IGNORE_LEN_P);
		if (!keyw) {
				RETURN_FALSE;
		}

		SetLastError(0);
		/*If the given bugger is not large enough to hold the data, the return value is
		the buffer size,  in characters, required to hold the string and its terminating
		null character. We use this return value to alloc the final buffer. */
		size = GetEnvironmentVariableW(keyw, &dummybuf, 0);
		if (GetLastError() == ERROR_ENVVAR_NOT_FOUND) {
				/* The environment variable doesn't exist. */
				free(keyw);
				RETURN_FALSE;
		}

		if (size == 0) {
				/* env exists, but it is empty */
				free(keyw);
				RETURN_EMPTY_STRING();
		}

		valw = emalloc((size + 1) * sizeof(wchar_t));
		size = GetEnvironmentVariableW(keyw, valw, size);
		if (size == 0) {
				/* has been removed between the two calls */
				free(keyw);
				efree(valw);
				RETURN_EMPTY_STRING();
		} else {
			ptr = hyss_win32_cp_w_to_any(valw);
			RETVAL_STRING(ptr);
			free(ptr);
			free(keyw);
			efree(valw);
			return;
		}
	}
#else
	/* system method returns a const */
	ptr = getenv(str);
	if (ptr) {
		RETURN_STRING(ptr);
	}
#endif
	RETURN_FALSE;
}
/* }}} */

#ifdef HAVE_PUTENV
/* {{{ proto bool putenv(string setting)
   Set the value of an environment variable */
HYSS_FUNCTION(putenv)
{
	char *setting;
	size_t setting_len;
	char *p, **env;
	putenv_entry pe;
#ifdef HYSS_WIN32
	char *value = NULL;
	int equals = 0;
	int error_code;
#endif

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(setting, setting_len)
	GEAR_PARSE_PARAMETERS_END();

    if(setting_len == 0 || setting[0] == '=') {
    	hyss_error_docref(NULL, E_WARNING, "Invalid parameter syntax");
    	RETURN_FALSE;
    }

	pe.putenv_string = estrndup(setting, setting_len);
	pe.key = estrndup(setting, setting_len);
	if ((p = strchr(pe.key, '='))) {	/* nullify the '=' if there is one */
		*p = '\0';
#ifdef HYSS_WIN32
		equals = 1;
#endif
	}

	pe.key_len = strlen(pe.key);
#ifdef HYSS_WIN32
	if (equals) {
		if (pe.key_len < setting_len - 1) {
			value = p + 1;
		} else {
			/* empty string*/
			value = p;
		}
	}
#endif

	gear_hash_str_del(&BG(putenv_ht), pe.key, pe.key_len);

	/* find previous value */
	pe.previous_value = NULL;
	for (env = environ; env != NULL && *env != NULL; env++) {
		if (!strncmp(*env, pe.key, pe.key_len) && (*env)[pe.key_len] == '=') {	/* found it */
#if defined(HYSS_WIN32)
			/* must copy previous value because MSVCRT's putenv can free the string without notice */
			pe.previous_value = estrdup(*env);
#else
			pe.previous_value = *env;
#endif
			break;
		}
	}

#if HAVE_UNSETENV
	if (!p) { /* no '=' means we want to unset it */
		unsetenv(pe.putenv_string);
	}
	if (!p || putenv(pe.putenv_string) == 0) { /* success */
#else
# ifndef HYSS_WIN32
	if (putenv(pe.putenv_string) == 0) { /* success */
# else
		wchar_t *keyw, *valw = NULL;

		keyw = hyss_win32_cp_any_to_w(pe.key);
		if (value) {
			valw = hyss_win32_cp_any_to_w(value);
		}
		/* valw may be NULL, but the failed conversion still needs to be checked. */
		if (!keyw || !valw && value) {
			efree(pe.putenv_string);
			efree(pe.key);
			free(keyw);
			free(valw);
			RETURN_FALSE;
		}

	error_code = SetEnvironmentVariableW(keyw, valw);

	if (error_code != 0
# ifndef ZTS
	/* We need both SetEnvironmentVariable and _putenv here as some
		dependency lib could use either way to read the environment.
		Obviously the CRT version will be useful more often. But
		generally, doing both brings us on the safe track at least
		in NTS build. */
	&& _wputenv_s(keyw, valw ? valw : L"") == 0
# endif
	) { /* success */
# endif
#endif
		gear_hash_str_add_mem(&BG(putenv_ht), pe.key, pe.key_len, &pe, sizeof(putenv_entry));
#ifdef HAVE_TZSET
		if (!strncmp(pe.key, "TZ", pe.key_len)) {
			tzset();
		}
#endif
#if defined(HYSS_WIN32)
		free(keyw);
		free(valw);
#endif
		RETURN_TRUE;
	} else {
		efree(pe.putenv_string);
		efree(pe.key);
#if defined(HYSS_WIN32)
		free(keyw);
		free(valw);
#endif
		RETURN_FALSE;
	}
}
/* }}} */
#endif

/* {{{ free_argv()
   Free the memory allocated to an argv array. */
static void free_argv(char **argv, int argc)
{
	int i;

	if (argv) {
		for (i = 0; i < argc; i++) {
			if (argv[i]) {
				efree(argv[i]);
			}
		}
		efree(argv);
	}
}
/* }}} */

/* {{{ free_longopts()
   Free the memory allocated to an longopt array. */
static void free_longopts(opt_struct *longopts)
{
	opt_struct *p;

	if (longopts) {
		for (p = longopts; p && p->opt_char != '-'; p++) {
			if (p->opt_name != NULL) {
				efree((char *)(p->opt_name));
			}
		}
	}
}
/* }}} */

/* {{{ parse_opts()
   Convert the typical getopt input characters to the hyss_getopt struct array */
static int parse_opts(char * opts, opt_struct ** result)
{
	opt_struct * paras = NULL;
	unsigned int i, count = 0;
	unsigned int opts_len = (unsigned int)strlen(opts);

	for (i = 0; i < opts_len; i++) {
		if ((opts[i] >= 48 && opts[i] <= 57) ||
			(opts[i] >= 65 && opts[i] <= 90) ||
			(opts[i] >= 97 && opts[i] <= 122)
		) {
			count++;
		}
	}

	paras = safe_emalloc(sizeof(opt_struct), count, 0);
	memset(paras, 0, sizeof(opt_struct) * count);
	*result = paras;
	while ( (*opts >= 48 && *opts <= 57) || /* 0 - 9 */
			(*opts >= 65 && *opts <= 90) || /* A - Z */
			(*opts >= 97 && *opts <= 122)   /* a - z */
	) {
		paras->opt_char = *opts;
		paras->need_param = (*(++opts) == ':') ? 1 : 0;
		paras->opt_name = NULL;
		if (paras->need_param == 1) {
			opts++;
			if (*opts == ':') {
				paras->need_param++;
				opts++;
			}
		}
		paras++;
	}
	return count;
}
/* }}} */

/* {{{ proto array getopt(string options [, array longopts [, int &optind]])
   Get options from the command line argument list */
HYSS_FUNCTION(getopt)
{
	char *options = NULL, **argv = NULL;
	char opt[2] = { '\0' };
	char *optname;
	int argc = 0, o;
	size_t options_len = 0, len;
	char *hyss_optarg = NULL;
	int hyss_optind = 1;
	zval val, *args = NULL, *p_longopts = NULL;
	zval *zoptind = NULL;
	size_t optname_len = 0;
	opt_struct *opts, *orig_opts;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_STRING(options, options_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY(p_longopts)
		Z_PARAM_ZVAL_DEREF(zoptind)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	/* Init zoptind to 1 */
	if (zoptind) {
		zval_ptr_dtor(zoptind);
		ZVAL_LONG(zoptind, 1);
	}

	/* Get argv from the global symbol table. We calculate argc ourselves
	 * in order to be on the safe side, even though it is also available
	 * from the symbol table. */
	if ((Z_TYPE(PG(http_globals)[TRACK_VARS_SERVER]) == IS_ARRAY || gear_is_auto_global_str(GEAR_STRL("_SERVER"))) &&
		((args = gear_hash_find_ex_ind(Z_ARRVAL_P(&PG(http_globals)[TRACK_VARS_SERVER]), ZSTR_KNOWN(GEAR_STR_ARGV), 1)) != NULL ||
		(args = gear_hash_find_ex_ind(&EG(symbol_table), ZSTR_KNOWN(GEAR_STR_ARGV), 1)) != NULL)
	) {
		int pos = 0;
		zval *entry;

 		if (Z_TYPE_P(args) != IS_ARRAY) {
 			RETURN_FALSE;
 		}
 		argc = gear_hash_num_elements(Z_ARRVAL_P(args));

		/* Attempt to allocate enough memory to hold all of the arguments
		 * and a trailing NULL */
		argv = (char **) safe_emalloc(sizeof(char *), (argc + 1), 0);

		/* Iterate over the hash to construct the argv array. */
		GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(args), entry) {
			gear_string *tmp_arg_str;
			gear_string *arg_str = zval_get_tmp_string(entry, &tmp_arg_str);

			argv[pos++] = estrdup(ZSTR_VAL(arg_str));

			gear_tmp_string_release(tmp_arg_str);
		} GEAR_HASH_FOREACH_END();

		/* The C Standard requires argv[argc] to be NULL - this might
		 * keep some getopt implementations happy. */
		argv[argc] = NULL;
	} else {
		/* Return false if we can't find argv. */
		RETURN_FALSE;
	}

	len = parse_opts(options, &opts);

	if (p_longopts) {
		int count;
		zval *entry;

		count = gear_hash_num_elements(Z_ARRVAL_P(p_longopts));

		/* the first <len> slots are filled by the one short ops
		 * we now extend our array and jump to the new added structs */
		opts = (opt_struct *) erealloc(opts, sizeof(opt_struct) * (len + count + 1));
		orig_opts = opts;
		opts += len;

		memset(opts, 0, count * sizeof(opt_struct));

		/* Iterate over the hash to construct the argv array. */
		GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(p_longopts), entry) {
			gear_string *tmp_arg_str;
			gear_string *arg_str = zval_get_tmp_string(entry, &tmp_arg_str);

			opts->need_param = 0;
			opts->opt_name = estrdup(ZSTR_VAL(arg_str));
			len = strlen(opts->opt_name);
			if ((len > 0) && (opts->opt_name[len - 1] == ':')) {
				opts->need_param++;
				opts->opt_name[len - 1] = '\0';
				if ((len > 1) && (opts->opt_name[len - 2] == ':')) {
					opts->need_param++;
					opts->opt_name[len - 2] = '\0';
				}
			}
			opts->opt_char = 0;
			opts++;

			gear_tmp_string_release(tmp_arg_str);
		} GEAR_HASH_FOREACH_END();
	} else {
		opts = (opt_struct*) erealloc(opts, sizeof(opt_struct) * (len + 1));
		orig_opts = opts;
		opts += len;
	}

	/* hyss_getopt want to identify the last param */
	opts->opt_char   = '-';
	opts->need_param = 0;
	opts->opt_name   = NULL;

	/* Initialize the return value as an array. */
	array_init(return_value);

	/* after our pointer arithmetic jump back to the first element */
	opts = orig_opts;

	while ((o = hyss_getopt(argc, argv, opts, &hyss_optarg, &hyss_optind, 0, 1)) != -1) {
		/* Skip unknown arguments. */
		if (o == '?') {
			continue;
		}

		/* Prepare the option character and the argument string. */
		if (o == 0) {
			optname = opts[hyss_optidx].opt_name;
		} else {
			if (o == 1) {
				o = '-';
			}
			opt[0] = o;
			optname = opt;
		}

		if (hyss_optarg != NULL) {
			/* keep the arg as binary, since the encoding is not known */
			ZVAL_STRING(&val, hyss_optarg);
		} else {
			ZVAL_FALSE(&val);
		}

		/* Add this option / argument pair to the result hash. */
		optname_len = strlen(optname);
		if (!(optname_len > 1 && optname[0] == '0') && is_numeric_string(optname, optname_len, NULL, NULL, 0) == IS_LONG) {
			/* numeric string */
			int optname_int = atoi(optname);
			if ((args = gear_hash_index_find(Z_ARRVAL_P(return_value), optname_int)) != NULL) {
				if (Z_TYPE_P(args) != IS_ARRAY) {
					convert_to_array_ex(args);
				}
				gear_hash_next_index_insert(Z_ARRVAL_P(args), &val);
			} else {
				gear_hash_index_update(Z_ARRVAL_P(return_value), optname_int, &val);
			}
		} else {
			/* other strings */
			if ((args = gear_hash_str_find(Z_ARRVAL_P(return_value), optname, strlen(optname))) != NULL) {
				if (Z_TYPE_P(args) != IS_ARRAY) {
					convert_to_array_ex(args);
				}
				gear_hash_next_index_insert(Z_ARRVAL_P(args), &val);
			} else {
				gear_hash_str_add(Z_ARRVAL_P(return_value), optname, strlen(optname), &val);
			}
		}

		hyss_optarg = NULL;
	}

	/* Set zoptind to hyss_optind */
	if (zoptind) {
		ZVAL_LONG(zoptind, hyss_optind);
	}

	free_longopts(orig_opts);
	efree(orig_opts);
	free_argv(argv, argc);
}
/* }}} */

/* {{{ proto void flush(void)
   Flush the output buffer */
HYSS_FUNCTION(flush)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	sapi_flush();
}
/* }}} */

/* {{{ proto void sleep(int seconds)
   Delay for a given number of seconds */
HYSS_FUNCTION(sleep)
{
	gear_long num;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(num)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (num < 0) {
		hyss_error_docref(NULL, E_WARNING, "Number of seconds must be greater than or equal to 0");
		RETURN_FALSE;
	}
#ifdef HYSS_SLEEP_NON_VOID
	RETURN_LONG(hyss_sleep((unsigned int)num));
#else
	hyss_sleep((unsigned int)num);
#endif

}
/* }}} */

/* {{{ proto void usleep(int micro_seconds)
   Delay for a given number of micro seconds */
HYSS_FUNCTION(usleep)
{
#if HAVE_USLEEP
	gear_long num;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(num)
	GEAR_PARSE_PARAMETERS_END();

	if (num < 0) {
		hyss_error_docref(NULL, E_WARNING, "Number of microseconds must be greater than or equal to 0");
		RETURN_FALSE;
	}
	usleep((unsigned int)num);
#endif
}
/* }}} */

#if HAVE_NANOSLEEP
/* {{{ proto mixed time_nanosleep(int seconds, int nanoseconds)
   Delay for a number of seconds and nano seconds */
HYSS_FUNCTION(time_nanosleep)
{
	gear_long tv_sec, tv_nsec;
	struct timespec hyss_req, hyss_rem;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_LONG(tv_sec)
		Z_PARAM_LONG(tv_nsec)
	GEAR_PARSE_PARAMETERS_END();

	if (tv_sec < 0) {
		hyss_error_docref(NULL, E_WARNING, "The seconds value must be greater than 0");
		RETURN_FALSE;
	}
	if (tv_nsec < 0) {
		hyss_error_docref(NULL, E_WARNING, "The nanoseconds value must be greater than 0");
		RETURN_FALSE;
	}

	hyss_req.tv_sec = (time_t) tv_sec;
	hyss_req.tv_nsec = (long)tv_nsec;
	if (!nanosleep(&hyss_req, &hyss_rem)) {
		RETURN_TRUE;
	} else if (errno == EINTR) {
		array_init(return_value);
		add_assoc_long_ex(return_value, "seconds", sizeof("seconds")-1, hyss_rem.tv_sec);
		add_assoc_long_ex(return_value, "nanoseconds", sizeof("nanoseconds")-1, hyss_rem.tv_nsec);
		return;
	} else if (errno == EINVAL) {
		hyss_error_docref(NULL, E_WARNING, "nanoseconds was not in the range 0 to 999 999 999 or seconds was negative");
	}

	RETURN_FALSE;
}
/* }}} */

/* {{{ proto mixed time_sleep_until(float timestamp)
   Make the script sleep until the specified time */
HYSS_FUNCTION(time_sleep_until)
{
	double d_ts, c_ts;
	struct timeval tm;
	struct timespec hyss_req, hyss_rem;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_DOUBLE(d_ts)
	GEAR_PARSE_PARAMETERS_END();

	if (gettimeofday((struct timeval *) &tm, NULL) != 0) {
		RETURN_FALSE;
	}

	c_ts = (double)(d_ts - tm.tv_sec - tm.tv_usec / 1000000.00);
	if (c_ts < 0) {
		hyss_error_docref(NULL, E_WARNING, "Sleep until to time is less than current time");
		RETURN_FALSE;
	}

	hyss_req.tv_sec = (time_t) c_ts;
	if (hyss_req.tv_sec > c_ts) { /* rounding up occurred */
		hyss_req.tv_sec--;
	}
	/* 1sec = 1000000000 nanoseconds */
	hyss_req.tv_nsec = (long) ((c_ts - hyss_req.tv_sec) * 1000000000.00);

	while (nanosleep(&hyss_req, &hyss_rem)) {
		if (errno == EINTR) {
			hyss_req.tv_sec = hyss_rem.tv_sec;
			hyss_req.tv_nsec = hyss_rem.tv_nsec;
		} else {
			RETURN_FALSE;
		}
	}

	RETURN_TRUE;
}
/* }}} */
#endif

/* {{{ proto string get_current_user(void)
   Get the name of the owner of the current HYSS script */
HYSS_FUNCTION(get_current_user)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_STRING(hyss_get_current_user());
}
/* }}} */

/* {{{ add_config_entry_cb
 */
static int add_config_entry_cb(zval *entry, int num_args, va_list args, gear_hash_key *hash_key)
{
	zval *retval = (zval *)va_arg(args, zval*);
	zval tmp;

	if (Z_TYPE_P(entry) == IS_STRING) {
		if (hash_key->key) {
			add_assoc_str_ex(retval, ZSTR_VAL(hash_key->key), ZSTR_LEN(hash_key->key), gear_string_copy(Z_STR_P(entry)));
		} else {
			add_index_str(retval, hash_key->h, gear_string_copy(Z_STR_P(entry)));
		}
	} else if (Z_TYPE_P(entry) == IS_ARRAY) {
		array_init(&tmp);
		gear_hash_apply_with_arguments(Z_ARRVAL_P(entry), add_config_entry_cb, 1, tmp);
		gear_hash_update(Z_ARRVAL_P(retval), hash_key->key, &tmp);
	}
	return 0;
}
/* }}} */

/* {{{ proto mixed get_cfg_var(string option_name)
   Get the value of a HYSS configuration option */
HYSS_FUNCTION(get_cfg_var)
{
	char *varname;
	size_t varname_len;
	zval *retval;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(varname, varname_len)
	GEAR_PARSE_PARAMETERS_END();

	retval = cfg_get_entry(varname, (uint32_t)varname_len);

	if (retval) {
		if (Z_TYPE_P(retval) == IS_ARRAY) {
			array_init(return_value);
			gear_hash_apply_with_arguments(Z_ARRVAL_P(retval), add_config_entry_cb, 1, return_value);
			return;
		} else {
			RETURN_STRING(Z_STRVAL_P(retval));
		}
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto int get_magic_quotes_runtime(void)
   Get the current active configuration setting of magic_quotes_runtime */
HYSS_FUNCTION(get_magic_quotes_runtime)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto int get_magic_quotes_gpc(void)
   Get the current active configuration setting of magic_quotes_gpc */
HYSS_FUNCTION(get_magic_quotes_gpc)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	RETURN_FALSE;
}
/* }}} */

/*
	1st arg = error message
	2nd arg = error option
	3rd arg = optional parameters (email address or tcp address)
	4th arg = used for additional headers if email

error options:
	0 = send to hyss_error_log (uses syslog or file depending on ics setting)
	1 = send via email to 3rd parameter 4th option = additional headers
	2 = send via tcp/ip to 3rd parameter (name or ip:port)
	3 = save to file in 3rd parameter
	4 = send to SAPI logger directly
*/

/* {{{ proto bool error_log(string message [, int message_type [, string destination [, string extra_headers]]])
   Send an error message somewhere */
HYSS_FUNCTION(error_log)
{
	char *message, *opt = NULL, *headers = NULL;
	size_t message_len, opt_len = 0, headers_len = 0;
	int opt_err = 0, argc = GEAR_NUM_ARGS();
	gear_long erropt = 0;

	GEAR_PARSE_PARAMETERS_START(1, 4)
		Z_PARAM_STRING(message, message_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(erropt)
		Z_PARAM_PATH(opt, opt_len)
		Z_PARAM_STRING(headers, headers_len)
	GEAR_PARSE_PARAMETERS_END();

	if (argc > 1) {
		opt_err = (int)erropt;
	}

	if (_hyss_error_log_ex(opt_err, message, message_len, opt, headers) == FAILURE) {
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* For BC (not binary-safe!) */
HYSSAPI int _hyss_error_log(int opt_err, char *message, char *opt, char *headers) /* {{{ */
{
	return _hyss_error_log_ex(opt_err, message, (opt_err == 3) ? strlen(message) : 0, opt, headers);
}
/* }}} */

HYSSAPI int _hyss_error_log_ex(int opt_err, char *message, size_t message_len, char *opt, char *headers) /* {{{ */
{
	hyss_stream *stream = NULL;
	size_t nbytes;

	switch (opt_err)
	{
		case 1:		/*send an email */
			if (!hyss_mail(opt, "HYSS error_log message", message, headers, NULL)) {
				return FAILURE;
			}
			break;

		case 2:		/*send to an address */
			hyss_error_docref(NULL, E_WARNING, "TCP/IP option not available!");
			return FAILURE;
			break;

		case 3:		/*save to a file */
			stream = hyss_stream_open_wrapper(opt, "a", IGNORE_URL_WIN | REPORT_ERRORS, NULL);
			if (!stream) {
				return FAILURE;
			}
			nbytes = hyss_stream_write(stream, message, message_len);
			hyss_stream_close(stream);
			if (nbytes != message_len) {
				return FAILURE;
			}
			break;

		case 4: /* send to SAPI */
			if (sapi_capi.log_message) {
				sapi_capi.log_message(message, -1);
			} else {
				return FAILURE;
			}
			break;

		default:
			hyss_log_err_with_severity(message, LOG_NOTICE);
			break;
	}
	return SUCCESS;
}
/* }}} */

/* {{{ proto array error_get_last()
   Get the last occurred error as associative array. Returns NULL if there hasn't been an error yet. */
HYSS_FUNCTION(error_get_last)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (PG(last_error_message)) {
		array_init(return_value);
		add_assoc_long_ex(return_value, "type", sizeof("type")-1, PG(last_error_type));
		add_assoc_string_ex(return_value, "message", sizeof("message")-1, PG(last_error_message));
		add_assoc_string_ex(return_value, "file", sizeof("file")-1, PG(last_error_file)?PG(last_error_file):"-");
		add_assoc_long_ex(return_value, "line", sizeof("line")-1, PG(last_error_lineno));
	}
}
/* }}} */

/* {{{ proto void error_clear_last(void)
   Clear the last occurred error. */
HYSS_FUNCTION(error_clear_last)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (PG(last_error_message)) {
		PG(last_error_type) = 0;
		PG(last_error_lineno) = 0;

		free(PG(last_error_message));
		PG(last_error_message) = NULL;

		if (PG(last_error_file)) {
			free(PG(last_error_file));
			PG(last_error_file) = NULL;
		}
	}
}
/* }}} */

/* {{{ proto mixed call_user_func(mixed function_name [, mixed parmeter] [, mixed ...])
   Call a user function which is the first parameter
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(call_user_func)
{
	zval retval;
	gear_fcall_info fci;
	gear_fcall_info_cache fci_cache;

	GEAR_PARSE_PARAMETERS_START(1, -1)
		Z_PARAM_FUNC(fci, fci_cache)
		Z_PARAM_VARIADIC('*', fci.params, fci.param_count)
	GEAR_PARSE_PARAMETERS_END();

	fci.retval = &retval;

	if (gear_call_function(&fci, &fci_cache) == SUCCESS && Z_TYPE(retval) != IS_UNDEF) {
		if (Z_ISREF(retval)) {
			gear_unwrap_reference(&retval);
		}
		ZVAL_COPY_VALUE(return_value, &retval);
	}
}
/* }}} */

/* {{{ proto mixed call_user_func_array(string function_name, array parameters)
   Call a user function which is the first parameter with the arguments contained in array
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(call_user_func_array)
{
	zval *params, retval;
	gear_fcall_info fci;
	gear_fcall_info_cache fci_cache;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_FUNC(fci, fci_cache)
		Z_PARAM_ARRAY(params)
	GEAR_PARSE_PARAMETERS_END();

	gear_fcall_info_args(&fci, params);
	fci.retval = &retval;

	if (gear_call_function(&fci, &fci_cache) == SUCCESS && Z_TYPE(retval) != IS_UNDEF) {
		if (Z_ISREF(retval)) {
			gear_unwrap_reference(&retval);
		}
		ZVAL_COPY_VALUE(return_value, &retval);
	}

	gear_fcall_info_args_clear(&fci, 1);
}
/* }}} */

/* {{{ proto mixed forward_static_call(mixed function_name [, mixed parmeter] [, mixed ...]) U
   Call a user function which is the first parameter */
HYSS_FUNCTION(forward_static_call)
{
	zval retval;
	gear_fcall_info fci;
	gear_fcall_info_cache fci_cache;
	gear_class_entry *called_scope;

	GEAR_PARSE_PARAMETERS_START(1, -1)
		Z_PARAM_FUNC(fci, fci_cache)
		Z_PARAM_VARIADIC('*', fci.params, fci.param_count)
	GEAR_PARSE_PARAMETERS_END();

	if (!EX(prev_execute_data)->func->common.scope) {
		gear_throw_error(NULL, "Cannot call forward_static_call() when no class scope is active");
		return;
	}

	fci.retval = &retval;

	called_scope = gear_get_called_scope(execute_data);
	if (called_scope && fci_cache.calling_scope &&
		instanceof_function(called_scope, fci_cache.calling_scope)) {
			fci_cache.called_scope = called_scope;
	}

	if (gear_call_function(&fci, &fci_cache) == SUCCESS && Z_TYPE(retval) != IS_UNDEF) {
		if (Z_ISREF(retval)) {
			gear_unwrap_reference(&retval);
		}
		ZVAL_COPY_VALUE(return_value, &retval);
	}
}
/* }}} */

/* {{{ proto mixed call_user_func_array(string function_name, array parameters) U
   Call a user function which is the first parameter with the arguments contained in array */
HYSS_FUNCTION(forward_static_call_array)
{
	zval *params, retval;
	gear_fcall_info fci;
	gear_fcall_info_cache fci_cache;
	gear_class_entry *called_scope;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_FUNC(fci, fci_cache)
		Z_PARAM_ARRAY(params)
	GEAR_PARSE_PARAMETERS_END();

	gear_fcall_info_args(&fci, params);
	fci.retval = &retval;

	called_scope = gear_get_called_scope(execute_data);
	if (called_scope && fci_cache.calling_scope &&
		instanceof_function(called_scope, fci_cache.calling_scope)) {
			fci_cache.called_scope = called_scope;
	}

	if (gear_call_function(&fci, &fci_cache) == SUCCESS && Z_TYPE(retval) != IS_UNDEF) {
		if (Z_ISREF(retval)) {
			gear_unwrap_reference(&retval);
		}
		ZVAL_COPY_VALUE(return_value, &retval);
	}

	gear_fcall_info_args_clear(&fci, 1);
}
/* }}} */

void user_shutdown_function_dtor(zval *zv) /* {{{ */
{
	int i;
	hyss_shutdown_function_entry *shutdown_function_entry = Z_PTR_P(zv);

	for (i = 0; i < shutdown_function_entry->arg_count; i++) {
		zval_ptr_dtor(&shutdown_function_entry->arguments[i]);
	}
	efree(shutdown_function_entry->arguments);
	efree(shutdown_function_entry);
}
/* }}} */

void user_tick_function_dtor(user_tick_function_entry *tick_function_entry) /* {{{ */
{
	int i;

	for (i = 0; i < tick_function_entry->arg_count; i++) {
		zval_ptr_dtor(&tick_function_entry->arguments[i]);
	}
	efree(tick_function_entry->arguments);
}
/* }}} */

static int user_shutdown_function_call(zval *zv) /* {{{ */
{
    hyss_shutdown_function_entry *shutdown_function_entry = Z_PTR_P(zv);
	zval retval;

	if (!gear_is_callable(&shutdown_function_entry->arguments[0], 0, NULL)) {
		gear_string *function_name
			= gear_get_callable_name(&shutdown_function_entry->arguments[0]);
		hyss_error(E_WARNING, "(Registered shutdown functions) Unable to call %s() - function does not exist", ZSTR_VAL(function_name));
		gear_string_release_ex(function_name, 0);
		return 0;
	}

	if (call_user_function(EG(function_table), NULL,
				&shutdown_function_entry->arguments[0],
				&retval,
				shutdown_function_entry->arg_count - 1,
				shutdown_function_entry->arguments + 1) == SUCCESS)
	{
		zval_ptr_dtor(&retval);
	}
	return 0;
}
/* }}} */

static void user_tick_function_call(user_tick_function_entry *tick_fe) /* {{{ */
{
	zval retval;
	zval *function = &tick_fe->arguments[0];

	/* Prevent reentrant calls to the same user ticks function */
	if (! tick_fe->calling) {
		tick_fe->calling = 1;

		if (call_user_function(	EG(function_table), NULL,
								function,
								&retval,
								tick_fe->arg_count - 1,
								tick_fe->arguments + 1
								) == SUCCESS) {
			zval_ptr_dtor(&retval);

		} else {
			zval *obj, *method;

			if (Z_TYPE_P(function) == IS_STRING) {
				hyss_error_docref(NULL, E_WARNING, "Unable to call %s() - function does not exist", Z_STRVAL_P(function));
			} else if (	Z_TYPE_P(function) == IS_ARRAY
						&& (obj = gear_hash_index_find(Z_ARRVAL_P(function), 0)) != NULL
						&& (method = gear_hash_index_find(Z_ARRVAL_P(function), 1)) != NULL
						&& Z_TYPE_P(obj) == IS_OBJECT
						&& Z_TYPE_P(method) == IS_STRING) {
				hyss_error_docref(NULL, E_WARNING, "Unable to call %s::%s() - function does not exist", ZSTR_VAL(Z_OBJCE_P(obj)->name), Z_STRVAL_P(method));
			} else {
				hyss_error_docref(NULL, E_WARNING, "Unable to call tick function");
			}
		}

		tick_fe->calling = 0;
	}
}
/* }}} */

static void run_user_tick_functions(int tick_count, void *arg) /* {{{ */
{
	gear_llist_apply(BG(user_tick_functions), (llist_apply_func_t) user_tick_function_call);
}
/* }}} */

static int user_tick_function_compare(user_tick_function_entry * tick_fe1, user_tick_function_entry * tick_fe2) /* {{{ */
{
	zval *func1 = &tick_fe1->arguments[0];
	zval *func2 = &tick_fe2->arguments[0];
	int ret;

	if (Z_TYPE_P(func1) == IS_STRING && Z_TYPE_P(func2) == IS_STRING) {
		ret = gear_binary_zval_strcmp(func1, func2) == 0;
	} else if (Z_TYPE_P(func1) == IS_ARRAY && Z_TYPE_P(func2) == IS_ARRAY) {
		ret = gear_compare_arrays(func1, func2) == 0;
	} else if (Z_TYPE_P(func1) == IS_OBJECT && Z_TYPE_P(func2) == IS_OBJECT) {
		ret = gear_compare_objects(func1, func2) == 0;
	} else {
		ret = 0;
	}

	if (ret && tick_fe1->calling) {
		hyss_error_docref(NULL, E_WARNING, "Unable to delete tick function executed at the moment");
		return 0;
	}
	return ret;
}
/* }}} */

HYSSAPI void hyss_call_shutdown_functions(void) /* {{{ */
{
	if (BG(user_shutdown_function_names)) {
		gear_try {
			gear_hash_apply(BG(user_shutdown_function_names), user_shutdown_function_call);
		}
		gear_end_try();
	}
}
/* }}} */

HYSSAPI void hyss_free_shutdown_functions(void) /* {{{ */
{
	if (BG(user_shutdown_function_names))
		gear_try {
			gear_hash_destroy(BG(user_shutdown_function_names));
			FREE_HASHTABLE(BG(user_shutdown_function_names));
			BG(user_shutdown_function_names) = NULL;
		} gear_catch {
			/* maybe shutdown method call exit, we just ignore it */
			FREE_HASHTABLE(BG(user_shutdown_function_names));
			BG(user_shutdown_function_names) = NULL;
		} gear_end_try();
}
/* }}} */

/* {{{ proto void register_shutdown_function(callback function) U
   Register a user-level function to be called on request termination */
HYSS_FUNCTION(register_shutdown_function)
{
	hyss_shutdown_function_entry shutdown_function_entry;
	int i;

	shutdown_function_entry.arg_count = GEAR_NUM_ARGS();

	if (shutdown_function_entry.arg_count < 1) {
		WRONG_PARAM_COUNT;
	}

	shutdown_function_entry.arguments = (zval *) safe_emalloc(sizeof(zval), shutdown_function_entry.arg_count, 0);

	if (gear_get_parameters_array(GEAR_NUM_ARGS(), shutdown_function_entry.arg_count, shutdown_function_entry.arguments) == FAILURE) {
		efree(shutdown_function_entry.arguments);
		RETURN_FALSE;
	}

	/* Prevent entering of anything but valid callback (syntax check only!) */
	if (!gear_is_callable(&shutdown_function_entry.arguments[0], 0, NULL)) {
		gear_string *callback_name
			= gear_get_callable_name(&shutdown_function_entry.arguments[0]);
		hyss_error_docref(NULL, E_WARNING, "Invalid shutdown callback '%s' passed", ZSTR_VAL(callback_name));
		efree(shutdown_function_entry.arguments);
		gear_string_release_ex(callback_name, 0);
		RETVAL_FALSE;
	} else {
		if (!BG(user_shutdown_function_names)) {
			ALLOC_HASHTABLE(BG(user_shutdown_function_names));
			gear_hash_init(BG(user_shutdown_function_names), 0, NULL, user_shutdown_function_dtor, 0);
		}

		for (i = 0; i < shutdown_function_entry.arg_count; i++) {
			Z_TRY_ADDREF(shutdown_function_entry.arguments[i]);
		}
		gear_hash_next_index_insert_mem(BG(user_shutdown_function_names), &shutdown_function_entry, sizeof(hyss_shutdown_function_entry));
	}
}
/* }}} */

HYSSAPI gear_bool register_user_shutdown_function(char *function_name, size_t function_len, hyss_shutdown_function_entry *shutdown_function_entry) /* {{{ */
{
	if (!BG(user_shutdown_function_names)) {
		ALLOC_HASHTABLE(BG(user_shutdown_function_names));
		gear_hash_init(BG(user_shutdown_function_names), 0, NULL, user_shutdown_function_dtor, 0);
	}

	gear_hash_str_update_mem(BG(user_shutdown_function_names), function_name, function_len, shutdown_function_entry, sizeof(hyss_shutdown_function_entry));
	return 1;
}
/* }}} */

HYSSAPI gear_bool remove_user_shutdown_function(char *function_name, size_t function_len) /* {{{ */
{
	if (BG(user_shutdown_function_names)) {
		return gear_hash_str_del(BG(user_shutdown_function_names), function_name, function_len) != FAILURE;
	}

	return 0;
}
/* }}} */

HYSSAPI gear_bool append_user_shutdown_function(hyss_shutdown_function_entry shutdown_function_entry) /* {{{ */
{
	if (!BG(user_shutdown_function_names)) {
		ALLOC_HASHTABLE(BG(user_shutdown_function_names));
		gear_hash_init(BG(user_shutdown_function_names), 0, NULL, user_shutdown_function_dtor, 0);
	}

	return gear_hash_next_index_insert_mem(BG(user_shutdown_function_names), &shutdown_function_entry, sizeof(hyss_shutdown_function_entry)) != NULL;
}
/* }}} */

GEAR_API void hyss_get_highlight_struct(gear_syntax_highlighter_ics *syntax_highlighter_ics) /* {{{ */
{
	syntax_highlighter_ics->highlight_comment = ICS_STR("highlight.comment");
	syntax_highlighter_ics->highlight_default = ICS_STR("highlight.default");
	syntax_highlighter_ics->highlight_html    = ICS_STR("highlight.html");
	syntax_highlighter_ics->highlight_keyword = ICS_STR("highlight.keyword");
	syntax_highlighter_ics->highlight_string  = ICS_STR("highlight.string");
}
/* }}} */

/* {{{ proto bool highlight_file(string file_name [, bool return] )
   Syntax highlight a source file */
HYSS_FUNCTION(highlight_file)
{
	char *filename;
	size_t filename_len;
	int ret;
	gear_syntax_highlighter_ics syntax_highlighter_ics;
	gear_bool i = 0;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_PATH(filename, filename_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(i)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (hyss_check_open_basedir(filename)) {
		RETURN_FALSE;
	}

	if (i) {
		hyss_output_start_default();
	}

	hyss_get_highlight_struct(&syntax_highlighter_ics);

	ret = highlight_file(filename, &syntax_highlighter_ics);

	if (ret == FAILURE) {
		if (i) {
			hyss_output_end();
		}
		RETURN_FALSE;
	}

	if (i) {
		hyss_output_get_contents(return_value);
		hyss_output_discard();
	} else {
		RETURN_TRUE;
	}
}
/* }}} */

/* {{{ proto string hyss_strip_whitespace(string file_name)
   Return source with stripped comments and whitespace */
HYSS_FUNCTION(hyss_strip_whitespace)
{
	char *filename;
	size_t filename_len;
	gear_lex_state original_lex_state;
	gear_file_handle file_handle;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_PATH(filename, filename_len)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	hyss_output_start_default();

	memset(&file_handle, 0, sizeof(file_handle));
	file_handle.type = GEAR_HANDLE_FILENAME;
	file_handle.filename = filename;
	file_handle.free_filename = 0;
	file_handle.opened_path = NULL;
	gear_save_lexical_state(&original_lex_state);
	if (open_file_for_scanning(&file_handle) == FAILURE) {
		gear_restore_lexical_state(&original_lex_state);
		hyss_output_end();
		RETURN_EMPTY_STRING();
	}

	gear_strip();

	gear_destroy_file_handle(&file_handle);
	gear_restore_lexical_state(&original_lex_state);

	hyss_output_get_contents(return_value);
	hyss_output_discard();
}
/* }}} */

/* {{{ proto bool highlight_string(string string [, bool return] )
   Syntax highlight a string or optionally return it */
HYSS_FUNCTION(highlight_string)
{
	zval *expr;
	gear_syntax_highlighter_ics syntax_highlighter_ics;
	char *hicompiled_string_description;
	gear_bool i = 0;
	int old_error_reporting = EG(error_reporting);

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_ZVAL(expr)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(i)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);
	convert_to_string_ex(expr);

	if (i) {
		hyss_output_start_default();
	}

	EG(error_reporting) = E_ERROR;

	hyss_get_highlight_struct(&syntax_highlighter_ics);

	hicompiled_string_description = gear_make_compiled_string_description("highlighted code");

	if (highlight_string(expr, &syntax_highlighter_ics, hicompiled_string_description) == FAILURE) {
		efree(hicompiled_string_description);
		EG(error_reporting) = old_error_reporting;
		if (i) {
			hyss_output_end();
		}
		RETURN_FALSE;
	}
	efree(hicompiled_string_description);

	EG(error_reporting) = old_error_reporting;

	if (i) {
		hyss_output_get_contents(return_value);
		hyss_output_discard();
	} else {
		RETURN_TRUE;
	}
}
/* }}} */

/* {{{ proto string ics_get(string varname)
   Get a configuration option */
HYSS_FUNCTION(ics_get)
{
	gear_string *varname, *val;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(varname)
	GEAR_PARSE_PARAMETERS_END();

	val = gear_ics_get_value(varname);

	if (!val) {
		RETURN_FALSE;
	}

	if (ZSTR_IS_INTERNED(val)) {
		RETVAL_INTERNED_STR(val);
	} else if (ZSTR_LEN(val) == 0) {
		RETVAL_EMPTY_STRING();
	} else if (ZSTR_LEN(val) == 1) {
		RETVAL_INTERNED_STR(ZSTR_CHAR((gear_uchar)ZSTR_VAL(val)[0]));
	} else if (!(GC_FLAGS(val) & GC_PERSISTENT)) {
		ZVAL_NEW_STR(return_value, gear_string_copy(val));
	} else {
		ZVAL_NEW_STR(return_value, gear_string_init(ZSTR_VAL(val), ZSTR_LEN(val), 0));
	}
}
/* }}} */

static int hyss_ics_get_option(zval *zv, int num_args, va_list args, gear_hash_key *hash_key) /* {{{ */
{
	gear_ics_entry *ics_entry = Z_PTR_P(zv);
	zval *ics_array = va_arg(args, zval *);
	int capi_number = va_arg(args, int);
	int details = va_arg(args, int);
	zval option;

	if (capi_number != 0 && ics_entry->capi_number != capi_number) {
		return 0;
	}

	if (hash_key->key == NULL ||
		ZSTR_VAL(hash_key->key)[0] != 0
	) {
		if (details) {
			array_init(&option);

			if (ics_entry->orig_value) {
				add_assoc_str(&option, "global_value", gear_string_copy(ics_entry->orig_value));
			} else if (ics_entry->value) {
				add_assoc_str(&option, "global_value", gear_string_copy(ics_entry->value));
			} else {
				add_assoc_null(&option, "global_value");
			}

			if (ics_entry->value) {
				add_assoc_str(&option, "local_value", gear_string_copy(ics_entry->value));
			} else {
				add_assoc_null(&option, "local_value");
			}

			add_assoc_long(&option, "access", ics_entry->modifiable);

			gear_symtable_update(Z_ARRVAL_P(ics_array), ics_entry->name, &option);
		} else {
			if (ics_entry->value) {
				zval zv;

				ZVAL_STR_COPY(&zv, ics_entry->value);
				gear_symtable_update(Z_ARRVAL_P(ics_array), ics_entry->name, &zv);
			} else {
				gear_symtable_update(Z_ARRVAL_P(ics_array), ics_entry->name, &EG(uninitialized_zval));
			}
		}
	}
	return 0;
}
/* }}} */

/* {{{ proto array ics_get_all([string extension[, bool details = true]])
   Get all configuration options */
HYSS_FUNCTION(ics_get_all)
{
	char *extname = NULL;
	size_t extname_len = 0, extnumber = 0;
	gear_capi_entry *cAPI;
	gear_bool details = 1;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING_EX(extname, extname_len, 1, 0)
		Z_PARAM_BOOL(details)
	GEAR_PARSE_PARAMETERS_END();

	gear_ics_sort_entries();

	if (extname) {
		if ((cAPI = gear_hash_str_find_ptr(&capi_registry, extname, extname_len)) == NULL) {
			hyss_error_docref(NULL, E_WARNING, "Unable to find extension '%s'", extname);
			RETURN_FALSE;
		}
		extnumber = cAPI->capi_number;
	}

	array_init(return_value);
	gear_hash_apply_with_arguments(EG(ics_directives), hyss_ics_get_option, 2, return_value, extnumber, details);
}
/* }}} */

static int hyss_ics_check_path(char *option_name, size_t option_len, char *new_option_name, size_t new_option_len) /* {{{ */
{
	if (option_len + 1 != new_option_len) {
		return 0;
	}

	return !strncmp(option_name, new_option_name, option_len);
}
/* }}} */

/* {{{ proto string ics_set(string varname, string newvalue)
   Set a configuration option, returns false on error and the old value of the configuration option on success */
HYSS_FUNCTION(ics_set)
{
	gear_string *varname;
	gear_string *new_value;
	gear_string *val;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(varname)
		Z_PARAM_STR(new_value)
	GEAR_PARSE_PARAMETERS_END();

	val = gear_ics_get_value(varname);

	/* copy to return here, because alter might free it! */
	if (val) {
		if (ZSTR_IS_INTERNED(val)) {
			RETVAL_INTERNED_STR(val);
		} else if (ZSTR_LEN(val) == 0) {
			RETVAL_EMPTY_STRING();
		} else if (ZSTR_LEN(val) == 1) {
			RETVAL_INTERNED_STR(ZSTR_CHAR((gear_uchar)ZSTR_VAL(val)[0]));
		} else if (!(GC_FLAGS(val) & GC_PERSISTENT)) {
			ZVAL_NEW_STR(return_value, gear_string_copy(val));
		} else {
			ZVAL_NEW_STR(return_value, gear_string_init(ZSTR_VAL(val), ZSTR_LEN(val), 0));
		}
	} else {
		RETVAL_FALSE;
	}

#define _CHECK_PATH(var, var_len, ics) hyss_ics_check_path(var, var_len, ics, sizeof(ics))
	/* open basedir check */
	if (PG(open_basedir)) {
		if (_CHECK_PATH(ZSTR_VAL(varname), ZSTR_LEN(varname), "error_log") ||
			_CHECK_PATH(ZSTR_VAL(varname), ZSTR_LEN(varname), "java.class.path") ||
			_CHECK_PATH(ZSTR_VAL(varname), ZSTR_LEN(varname), "java.home") ||
			_CHECK_PATH(ZSTR_VAL(varname), ZSTR_LEN(varname), "mail.log") ||
			_CHECK_PATH(ZSTR_VAL(varname), ZSTR_LEN(varname), "java.library.path") ||
			_CHECK_PATH(ZSTR_VAL(varname), ZSTR_LEN(varname), "vpopmail.directory")) {
			if (hyss_check_open_basedir(ZSTR_VAL(new_value))) {
				zval_ptr_dtor_str(return_value);
				RETURN_FALSE;
			}
		}
	}
#undef _CHECK_PATH

	if (gear_alter_ics_entry_ex(varname, new_value, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME, 0) == FAILURE) {
		zval_ptr_dtor_str(return_value);
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto void ics_restore(string varname)
   Restore the value of a configuration option specified by varname */
HYSS_FUNCTION(ics_restore)
{
	gear_string *varname;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(varname)
	GEAR_PARSE_PARAMETERS_END();

	gear_restore_ics_entry(varname, HYSS_ICS_STAGE_RUNTIME);
}
/* }}} */

/* {{{ proto string set_include_path(string new_include_path)
   Sets the include_path configuration option */
HYSS_FUNCTION(set_include_path)
{
	gear_string *new_value;
	char *old_value;
	gear_string *key;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_PATH_STR(new_value)
	GEAR_PARSE_PARAMETERS_END();

	old_value = gear_ics_string("include_path", sizeof("include_path") - 1, 0);
	/* copy to return here, because alter might free it! */
	if (old_value) {
		RETVAL_STRING(old_value);
	} else {
		RETVAL_FALSE;
	}

	key = gear_string_init("include_path", sizeof("include_path") - 1, 0);
	if (gear_alter_ics_entry_ex(key, new_value, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME, 0) == FAILURE) {
		gear_string_release_ex(key, 0);
		zval_ptr_dtor_str(return_value);
		RETURN_FALSE;
	}
	gear_string_release_ex(key, 0);
}
/* }}} */

/* {{{ proto string get_include_path()
   Get the current include_path configuration option */
HYSS_FUNCTION(get_include_path)
{
	char *str;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	str = gear_ics_string("include_path", sizeof("include_path") - 1, 0);

	if (str == NULL) {
		RETURN_FALSE;
	}

	RETURN_STRING(str);
}
/* }}} */

/* {{{ proto void restore_include_path()
   Restore the value of the include_path configuration option */
HYSS_FUNCTION(restore_include_path)
{
	gear_string *key;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	key = gear_string_init("include_path", sizeof("include_path")-1, 0);
	gear_restore_ics_entry(key, HYSS_ICS_STAGE_RUNTIME);
	gear_string_efree(key);
}
/* }}} */

/* {{{ proto mixed print_r(mixed var [, bool return])
   Prints out or returns information about the specified variable */
HYSS_FUNCTION(print_r)
{
	zval *var;
	gear_bool do_return = 0;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_ZVAL(var)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(do_return)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (do_return) {
		RETURN_STR(gear_print_zval_r_to_str(var, 0));
	} else {
		gear_print_zval_r(var, 0);
		RETURN_TRUE;
	}
}
/* }}} */

/* {{{ proto int connection_aborted(void)
   Returns true if client disconnected */
HYSS_FUNCTION(connection_aborted)
{
	RETURN_LONG(PG(connection_status) & HYSS_CONNECTION_ABORTED);
}
/* }}} */

/* {{{ proto int connection_status(void)
   Returns the connection status bitfield */
HYSS_FUNCTION(connection_status)
{
	RETURN_LONG(PG(connection_status));
}
/* }}} */

/* {{{ proto int ignore_user_abort([bool value])
   Set whether we want to ignore a user abort event or not */
HYSS_FUNCTION(ignore_user_abort)
{
	gear_bool arg = 0;
	int old_setting;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(arg)
	GEAR_PARSE_PARAMETERS_END();

	old_setting = (unsigned short)PG(ignore_user_abort);

	if (GEAR_NUM_ARGS()) {
		gear_string *key = gear_string_init("ignore_user_abort", sizeof("ignore_user_abort") - 1, 0);
		gear_alter_ics_entry_chars(key, arg ? "1" : "0", 1, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME);
		gear_string_release_ex(key, 0);
	}

	RETURN_LONG(old_setting);
}
/* }}} */

#if HAVE_GETSERVBYNAME
/* {{{ proto int getservbyname(string service, string protocol)
   Returns port associated with service. Protocol must be "tcp" or "udp" */
HYSS_FUNCTION(getservbyname)
{
	char *name, *proto;
	size_t name_len, proto_len;
	struct servent *serv;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STRING(name, name_len)
		Z_PARAM_STRING(proto, proto_len)
	GEAR_PARSE_PARAMETERS_END();


/* empty string behaves like NULL on windows implementation of
   getservbyname. Let be portable instead. */
#ifdef HYSS_WIN32
	if (proto_len == 0) {
		RETURN_FALSE;
	}
#endif

	serv = getservbyname(name, proto);

#if defined(_AIX)
	/*
        On AIX, imap is only known as imap2 in /etc/services, while on Linux imap is an alias for imap2.
        If a request for imap gives no result, we try again with imap2.
        */
	if (serv == NULL && strcmp(name,  "imap") == 0) {
		serv = getservbyname("imap2", proto);
	}
#endif
	if (serv == NULL) {
		RETURN_FALSE;
	}

	RETURN_LONG(ntohs(serv->s_port));
}
/* }}} */
#endif

#if HAVE_GETSERVBYPORT
/* {{{ proto string getservbyport(int port, string protocol)
   Returns service name associated with port. Protocol must be "tcp" or "udp" */
HYSS_FUNCTION(getservbyport)
{
	char *proto;
	size_t proto_len;
	gear_long port;
	struct servent *serv;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_LONG(port)
		Z_PARAM_STRING(proto, proto_len)
	GEAR_PARSE_PARAMETERS_END();

	serv = getservbyport(htons((unsigned short) port), proto);

	if (serv == NULL) {
		RETURN_FALSE;
	}

	RETURN_STRING(serv->s_name);
}
/* }}} */
#endif

#if HAVE_GETPROTOBYNAME
/* {{{ proto int getprotobyname(string name)
   Returns protocol number associated with name as per /etc/protocols */
HYSS_FUNCTION(getprotobyname)
{
	char *name;
	size_t name_len;
	struct protoent *ent;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(name, name_len)
	GEAR_PARSE_PARAMETERS_END();

	ent = getprotobyname(name);

	if (ent == NULL) {
		RETURN_FALSE;
	}

	RETURN_LONG(ent->p_proto);
}
/* }}} */
#endif

#if HAVE_GETPROTOBYNUMBER
/* {{{ proto string getprotobynumber(int proto)
   Returns protocol name associated with protocol number proto */
HYSS_FUNCTION(getprotobynumber)
{
	gear_long proto;
	struct protoent *ent;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(proto)
	GEAR_PARSE_PARAMETERS_END();

	ent = getprotobynumber((int)proto);

	if (ent == NULL) {
		RETURN_FALSE;
	}

	RETURN_STRING(ent->p_name);
}
/* }}} */
#endif

/* {{{ proto bool register_tick_function(string function_name [, mixed arg [, mixed ... ]])
   Registers a tick callback function */
HYSS_FUNCTION(register_tick_function)
{
	user_tick_function_entry tick_fe;
	int i;
	gear_string *function_name = NULL;

	tick_fe.calling = 0;
	tick_fe.arg_count = GEAR_NUM_ARGS();

	if (tick_fe.arg_count < 1) {
		WRONG_PARAM_COUNT;
	}

	tick_fe.arguments = (zval *) safe_emalloc(sizeof(zval), tick_fe.arg_count, 0);

	if (gear_get_parameters_array(GEAR_NUM_ARGS(), tick_fe.arg_count, tick_fe.arguments) == FAILURE) {
		efree(tick_fe.arguments);
		RETURN_FALSE;
	}

	if (!gear_is_callable(&tick_fe.arguments[0], 0, &function_name)) {
		efree(tick_fe.arguments);
		hyss_error_docref(NULL, E_WARNING, "Invalid tick callback '%s' passed", ZSTR_VAL(function_name));
		gear_string_release_ex(function_name, 0);
		RETURN_FALSE;
	} else if (function_name) {
		gear_string_release_ex(function_name, 0);
	}

	if (Z_TYPE(tick_fe.arguments[0]) != IS_ARRAY && Z_TYPE(tick_fe.arguments[0]) != IS_OBJECT) {
		convert_to_string_ex(&tick_fe.arguments[0]);
	}

	if (!BG(user_tick_functions)) {
		BG(user_tick_functions) = (gear_llist *) emalloc(sizeof(gear_llist));
		gear_llist_init(BG(user_tick_functions),
						sizeof(user_tick_function_entry),
						(llist_dtor_func_t) user_tick_function_dtor, 0);
		hyss_add_tick_function(run_user_tick_functions, NULL);
	}

	for (i = 0; i < tick_fe.arg_count; i++) {
		Z_TRY_ADDREF(tick_fe.arguments[i]);
	}

	gear_llist_add_element(BG(user_tick_functions), &tick_fe);

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto void unregister_tick_function(string function_name)
   Unregisters a tick callback function */
HYSS_FUNCTION(unregister_tick_function)
{
	zval *function;
	user_tick_function_entry tick_fe;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(function)
	GEAR_PARSE_PARAMETERS_END();

	if (!BG(user_tick_functions)) {
		return;
	}

	if (Z_TYPE_P(function) != IS_ARRAY && Z_TYPE_P(function) != IS_OBJECT) {
		convert_to_string(function);
	}

	tick_fe.arguments = (zval *) emalloc(sizeof(zval));
	ZVAL_COPY_VALUE(&tick_fe.arguments[0], function);
	tick_fe.arg_count = 1;
	gear_llist_del_element(BG(user_tick_functions), &tick_fe, (int (*)(void *, void *)) user_tick_function_compare);
	efree(tick_fe.arguments);
}
/* }}} */

/* {{{ proto bool is_uploaded_file(string path)
   Check if file was created by rfc1867 upload */
HYSS_FUNCTION(is_uploaded_file)
{
	char *path;
	size_t path_len;

	if (!SG(rfc1867_uploaded_files)) {
		RETURN_FALSE;
	}

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(path, path_len)
	GEAR_PARSE_PARAMETERS_END();

	if (gear_hash_str_exists(SG(rfc1867_uploaded_files), path, path_len)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool move_uploaded_file(string path, string new_path)
   Move a file if and only if it was created by an upload */
HYSS_FUNCTION(move_uploaded_file)
{
	char *path, *new_path;
	size_t path_len, new_path_len;
	gear_bool successful = 0;

#ifndef HYSS_WIN32
	int oldmask; int ret;
#endif

	if (!SG(rfc1867_uploaded_files)) {
		RETURN_FALSE;
	}

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STRING(path, path_len)
		Z_PARAM_PATH(new_path, new_path_len)
	GEAR_PARSE_PARAMETERS_END();

	if (!gear_hash_str_exists(SG(rfc1867_uploaded_files), path, path_len)) {
		RETURN_FALSE;
	}

	if (hyss_check_open_basedir(new_path)) {
		RETURN_FALSE;
	}

	if (VCWD_RENAME(path, new_path) == 0) {
		successful = 1;
#ifndef HYSS_WIN32
		oldmask = umask(077);
		umask(oldmask);

		ret = VCWD_CHMOD(new_path, 0666 & ~oldmask);

		if (ret == -1) {
			hyss_error_docref(NULL, E_WARNING, "%s", strerror(errno));
		}
#endif
	} else if (hyss_copy_file_ex(path, new_path, STREAM_DISABLE_OPEN_BASEDIR) == SUCCESS) {
		VCWD_UNLINK(path);
		successful = 1;
	}

	if (successful) {
		gear_hash_str_del(SG(rfc1867_uploaded_files), path, path_len);
	} else {
		hyss_error_docref(NULL, E_WARNING, "Unable to move '%s' to '%s'", path, new_path);
	}

	RETURN_BOOL(successful);
}
/* }}} */

/* {{{ hyss_simple_ics_parser_cb
 */
static void hyss_simple_ics_parser_cb(zval *arg1, zval *arg2, zval *arg3, int callback_type, zval *arr)
{
	switch (callback_type) {

		case GEAR_ICS_PARSER_ENTRY:
			if (!arg2) {
				/* bare string - nothing to do */
				break;
			}
			Z_TRY_ADDREF_P(arg2);
			gear_symtable_update(Z_ARRVAL_P(arr), Z_STR_P(arg1), arg2);
			break;

		case GEAR_ICS_PARSER_POP_ENTRY:
		{
			zval hash, *find_hash;

			if (!arg2) {
				/* bare string - nothing to do */
				break;
			}

			if (!(Z_STRLEN_P(arg1) > 1 && Z_STRVAL_P(arg1)[0] == '0') && is_numeric_string(Z_STRVAL_P(arg1), Z_STRLEN_P(arg1), NULL, NULL, 0) == IS_LONG) {
				gear_ulong key = (gear_ulong) gear_atol(Z_STRVAL_P(arg1), Z_STRLEN_P(arg1));
				if ((find_hash = gear_hash_index_find(Z_ARRVAL_P(arr), key)) == NULL) {
					array_init(&hash);
					find_hash = gear_hash_index_add_new(Z_ARRVAL_P(arr), key, &hash);
				}
			} else {
				if ((find_hash = gear_hash_find(Z_ARRVAL_P(arr), Z_STR_P(arg1))) == NULL) {
					array_init(&hash);
					find_hash = gear_hash_add_new(Z_ARRVAL_P(arr), Z_STR_P(arg1), &hash);
				}
			}

			if (Z_TYPE_P(find_hash) != IS_ARRAY) {
				zval_ptr_dtor_nogc(find_hash);
				array_init(find_hash);
			}

			if (!arg3 || (Z_TYPE_P(arg3) == IS_STRING && Z_STRLEN_P(arg3) == 0)) {
				Z_TRY_ADDREF_P(arg2);
				add_next_index_zval(find_hash, arg2);
			} else {
				array_set_zval_key(Z_ARRVAL_P(find_hash), arg3, arg2);
			}
		}
		break;

		case GEAR_ICS_PARSER_SECTION:
			break;
	}
}
/* }}} */

/* {{{ hyss_ics_parser_cb_with_sections
 */
static void hyss_ics_parser_cb_with_sections(zval *arg1, zval *arg2, zval *arg3, int callback_type, zval *arr)
{
	if (callback_type == GEAR_ICS_PARSER_SECTION) {
		array_init(&BG(active_ics_file_section));
		gear_symtable_update(Z_ARRVAL_P(arr), Z_STR_P(arg1), &BG(active_ics_file_section));
	} else if (arg2) {
		zval *active_arr;

		if (Z_TYPE(BG(active_ics_file_section)) != IS_UNDEF) {
			active_arr = &BG(active_ics_file_section);
		} else {
			active_arr = arr;
		}

		hyss_simple_ics_parser_cb(arg1, arg2, arg3, callback_type, active_arr);
	}
}
/* }}} */

/* {{{ proto array parse_ics_file(string filename [, bool process_sections [, int scanner_mode]])
   Parse configuration file */
HYSS_FUNCTION(parse_ics_file)
{
	char *filename = NULL;
	size_t filename_len = 0;
	gear_bool process_sections = 0;
	gear_long scanner_mode = GEAR_ICS_SCANNER_NORMAL;
	gear_file_handle fh;
	gear_ics_parser_cb_t ics_parser_cb;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_PATH(filename, filename_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(process_sections)
		Z_PARAM_LONG(scanner_mode)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (filename_len == 0) {
		hyss_error_docref(NULL, E_WARNING, "Filename cannot be empty!");
		RETURN_FALSE;
	}

	/* Set callback function */
	if (process_sections) {
		ZVAL_UNDEF(&BG(active_ics_file_section));
		ics_parser_cb = (gear_ics_parser_cb_t) hyss_ics_parser_cb_with_sections;
	} else {
		ics_parser_cb = (gear_ics_parser_cb_t) hyss_simple_ics_parser_cb;
	}

	/* Setup filehandle */
	memset(&fh, 0, sizeof(fh));
	fh.filename = filename;
	fh.type = GEAR_HANDLE_FILENAME;

	array_init(return_value);
	if (gear_parse_ics_file(&fh, 0, (int)scanner_mode, ics_parser_cb, return_value) == FAILURE) {
		gear_array_destroy(Z_ARR_P(return_value));
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto array parse_ics_string(string ics_string [, bool process_sections [, int scanner_mode]])
   Parse configuration string */
HYSS_FUNCTION(parse_ics_string)
{
	char *string = NULL, *str = NULL;
	size_t str_len = 0;
	gear_bool process_sections = 0;
	gear_long scanner_mode = GEAR_ICS_SCANNER_NORMAL;
	gear_ics_parser_cb_t ics_parser_cb;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_STRING(str, str_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(process_sections)
		Z_PARAM_LONG(scanner_mode)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (INT_MAX - str_len < GEAR_MMAP_AHEAD) {
		RETVAL_FALSE;
	}

	/* Set callback function */
	if (process_sections) {
		ZVAL_UNDEF(&BG(active_ics_file_section));
		ics_parser_cb = (gear_ics_parser_cb_t) hyss_ics_parser_cb_with_sections;
	} else {
		ics_parser_cb = (gear_ics_parser_cb_t) hyss_simple_ics_parser_cb;
	}

	/* Setup string */
	string = (char *) emalloc(str_len + GEAR_MMAP_AHEAD);
	memcpy(string, str, str_len);
	memset(string + str_len, 0, GEAR_MMAP_AHEAD);

	array_init(return_value);
	if (gear_parse_ics_string(string, 0, (int)scanner_mode, ics_parser_cb, return_value) == FAILURE) {
		gear_array_destroy(Z_ARR_P(return_value));
		RETVAL_FALSE;
	}
	efree(string);
}
/* }}} */

#if GEAR_DEBUG
/* This function returns an array of ALL valid ics options with values and
 *  is not the same as ics_get_all() which returns only registered ics options. Only useful for devs to debug hyss.ics scanner/parser! */
HYSS_FUNCTION(config_get_hash) /* {{{ */
{
	HashTable *hash = hyss_ics_get_configuration_hash();

	array_init(return_value);
	gear_hash_apply_with_arguments(hash, add_config_entry_cb, 1, return_value);
}
/* }}} */
#endif

#ifdef HAVE_GETLOADAVG
/* {{{ proto array sys_getloadavg()
*/
HYSS_FUNCTION(sys_getloadavg)
{
	double load[3];

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (getloadavg(load, 3) == -1) {
		RETURN_FALSE;
	} else {
		array_init(return_value);
		add_index_double(return_value, 0, load[0]);
		add_index_double(return_value, 1, load[1]);
		add_index_double(return_value, 2, load[2]);
	}
}
/* }}} */
#endif

