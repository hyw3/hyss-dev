/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXEC_H
#define EXEC_H

HYSS_FUNCTION(system);
HYSS_FUNCTION(exec);
HYSS_FUNCTION(escapeshellcmd);
HYSS_FUNCTION(escapeshellarg);
HYSS_FUNCTION(passthru);
HYSS_FUNCTION(shell_exec);
HYSS_FUNCTION(proc_open);
HYSS_FUNCTION(proc_get_status);
HYSS_FUNCTION(proc_close);
HYSS_FUNCTION(proc_terminate);
HYSS_FUNCTION(proc_nice);
HYSS_MINIT_FUNCTION(proc_open);
HYSS_MINIT_FUNCTION(exec);

HYSSAPI gear_string *hyss_escape_shell_cmd(char *);
HYSSAPI gear_string *hyss_escape_shell_arg(char *);
HYSSAPI int hyss_exec(int type, char *cmd, zval *array, zval *return_value);

#endif /* EXEC_H */
