/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_ARRAY_H
#define HYSS_ARRAY_H

HYSS_MINIT_FUNCTION(array);
HYSS_MSHUTDOWN_FUNCTION(array);

HYSS_FUNCTION(ksort);
HYSS_FUNCTION(krsort);
HYSS_FUNCTION(natsort);
HYSS_FUNCTION(natcasesort);
HYSS_FUNCTION(asort);
HYSS_FUNCTION(arsort);
HYSS_FUNCTION(sort);
HYSS_FUNCTION(rsort);
HYSS_FUNCTION(usort);
HYSS_FUNCTION(uasort);
HYSS_FUNCTION(uksort);
HYSS_FUNCTION(array_walk);
HYSS_FUNCTION(array_walk_recursive);
HYSS_FUNCTION(count);
HYSS_FUNCTION(end);
HYSS_FUNCTION(prev);
HYSS_FUNCTION(next);
HYSS_FUNCTION(reset);
HYSS_FUNCTION(current);
HYSS_FUNCTION(key);
HYSS_FUNCTION(min);
HYSS_FUNCTION(max);
HYSS_FUNCTION(in_array);
HYSS_FUNCTION(array_search);
HYSS_FUNCTION(extract);
HYSS_FUNCTION(compact);
HYSS_FUNCTION(array_fill);
HYSS_FUNCTION(array_fill_keys);
HYSS_FUNCTION(range);
HYSS_FUNCTION(shuffle);
HYSS_FUNCTION(array_multisort);
HYSS_FUNCTION(array_push);
HYSS_FUNCTION(array_pop);
HYSS_FUNCTION(array_shift);
HYSS_FUNCTION(array_unshift);
HYSS_FUNCTION(array_splice);
HYSS_FUNCTION(array_slice);
HYSS_FUNCTION(array_merge);
HYSS_FUNCTION(array_merge_recursive);
HYSS_FUNCTION(array_replace);
HYSS_FUNCTION(array_replace_recursive);
HYSS_FUNCTION(array_keys);
HYSS_FUNCTION(array_key_first);
HYSS_FUNCTION(array_key_last);
HYSS_FUNCTION(array_values);
HYSS_FUNCTION(array_count_values);
HYSS_FUNCTION(array_column);
HYSS_FUNCTION(array_reverse);
HYSS_FUNCTION(array_reduce);
HYSS_FUNCTION(array_pad);
HYSS_FUNCTION(array_flip);
HYSS_FUNCTION(array_change_key_case);
HYSS_FUNCTION(array_rand);
HYSS_FUNCTION(array_unique);
HYSS_FUNCTION(array_intersect);
HYSS_FUNCTION(array_intersect_key);
HYSS_FUNCTION(array_intersect_ukey);
HYSS_FUNCTION(array_uintersect);
HYSS_FUNCTION(array_intersect_assoc);
HYSS_FUNCTION(array_uintersect_assoc);
HYSS_FUNCTION(array_intersect_uassoc);
HYSS_FUNCTION(array_uintersect_uassoc);
HYSS_FUNCTION(array_diff);
HYSS_FUNCTION(array_diff_key);
HYSS_FUNCTION(array_diff_ukey);
HYSS_FUNCTION(array_udiff);
HYSS_FUNCTION(array_diff_assoc);
HYSS_FUNCTION(array_udiff_assoc);
HYSS_FUNCTION(array_diff_uassoc);
HYSS_FUNCTION(array_udiff_uassoc);
HYSS_FUNCTION(array_sum);
HYSS_FUNCTION(array_product);
HYSS_FUNCTION(array_filter);
HYSS_FUNCTION(array_map);
HYSS_FUNCTION(array_key_exists);
HYSS_FUNCTION(array_chunk);
HYSS_FUNCTION(array_combine);

HYSSAPI int hyss_array_merge(HashTable *dest, HashTable *src);
HYSSAPI int hyss_array_merge_recursive(HashTable *dest, HashTable *src);
HYSSAPI int hyss_array_replace_recursive(HashTable *dest, HashTable *src);
HYSSAPI int hyss_multisort_compare(const void *a, const void *b);
HYSSAPI gear_long hyss_count_recursive(HashTable *ht);

#define HYSS_SORT_REGULAR            0
#define HYSS_SORT_NUMERIC            1
#define HYSS_SORT_STRING             2
#define HYSS_SORT_DESC               3
#define HYSS_SORT_ASC                4
#define HYSS_SORT_LOCALE_STRING      5
#define HYSS_SORT_NATURAL            6
#define HYSS_SORT_FLAG_CASE          8

#define COUNT_NORMAL      0
#define COUNT_RECURSIVE   1

#define ARRAY_FILTER_USE_BOTH	1
#define ARRAY_FILTER_USE_KEY	2

GEAR_BEGIN_CAPI_GLOBALS(array)
	compare_func_t *multisort_func;
GEAR_END_CAPI_GLOBALS(array)

#define ARRAYG(v) GEAR_CAPI_GLOBALS_ACCESSOR(array, v)

#endif /* HYSS_ARRAY_H */
