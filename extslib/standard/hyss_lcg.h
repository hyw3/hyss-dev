/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_LCG_H
#define HYSS_LCG_H

#include "extslib/standard/basic_functions.h"

typedef struct {
	int32_t s1;
	int32_t s2;
	int seeded;
} hyss_lcg_globals;

HYSSAPI double hyss_combined_lcg(void);
HYSS_FUNCTION(lcg_value);

HYSS_MINIT_FUNCTION(lcg);

#ifdef ZTS
#define LCG(v) GEAR_PBCG(lcg_globals_id, hyss_lcg_globals *, v)
#else
#define LCG(v) (lcg_globals.v)
#endif

#endif
