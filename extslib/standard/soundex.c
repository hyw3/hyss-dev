/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include "hyss_string.h"

/* Simple soundex algorithm as described by Knuth in TAOCP, vol 3 */
/* {{{ proto string soundex(string str)
   Calculate the soundex key of a string */
HYSS_FUNCTION(soundex)
{
	char	*str;
	size_t	i, _small, str_len, code, last;
	char	soundex[4 + 1];

	static char soundex_table[26] =
	{0,							/* A */
	 '1',						/* B */
	 '2',						/* C */
	 '3',						/* D */
	 0,							/* E */
	 '1',						/* F */
	 '2',						/* G */
	 0,							/* H */
	 0,							/* I */
	 '2',						/* J */
	 '2',						/* K */
	 '4',						/* L */
	 '5',						/* M */
	 '5',						/* N */
	 0,							/* O */
	 '1',						/* P */
	 '2',						/* Q */
	 '6',						/* R */
	 '2',						/* S */
	 '3',						/* T */
	 0,							/* U */
	 '1',						/* V */
	 0,							/* W */
	 '2',						/* X */
	 0,							/* Y */
	 '2'};						/* Z */

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(str, str_len)
	GEAR_PARSE_PARAMETERS_END();

	if (str_len == 0) {
		RETURN_FALSE;
	}

	/* build soundex string */
	last = -1;
	for (i = 0, _small = 0; i < str_len && _small < 4; i++) {
		/* convert chars to upper case and strip non-letter chars */
		/* BUG: should also map here accented letters used in non */
		/* English words or names (also found in English text!): */
		/* esstsett, thorn, n-tilde, c-cedilla, s-caron, ... */
		code = toupper((int)(unsigned char)str[i]);
		if (code >= 'A' && code <= 'Z') {
			if (_small == 0) {
				/* remember first valid char */
				soundex[_small++] = (char)code;
				last = soundex_table[code - 'A'];
			}
			else {
				/* ignore sequences of consonants with same soundex */
				/* code in trail, and vowels unless they separate */
				/* consonant letters */
				code = soundex_table[code - 'A'];
				if (code != last) {
					if (code != 0) {
						soundex[_small++] = (char)code;
					}
					last = code;
				}
			}
		}
	}
	/* pad with '0' and terminate with 0 ;-) */
	while (_small < 4) {
		soundex[_small++] = '0';
	}
	soundex[_small] = '\0';

	RETURN_STRINGL(soundex, _small);
}
/* }}} */

