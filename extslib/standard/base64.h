/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASE64_H
#define BASE64_H

/*
 * SSSE3 and AVX2 implementation are based on https://github.com/aklomp/base64
 * which is copyrighted to:
 *
 * Copyright (c) 2005-2007, Nick Galbreath
 * Copyright (c) 2013-2017, Alfred Klomp
 * Copyright (c) 2015-2017, Wojciech Mula
 * Copyright (c) 2016-2017, Matthieu Darbois
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

HYSS_FUNCTION(base64_decode);
HYSS_FUNCTION(base64_encode);

#if (GEAR_INTRIN_AVX2_FUNC_PTR || GEAR_INTRIN_SSSE3_FUNC_PTR) && !GEAR_INTRIN_AVX2_NATIVE
HYSS_MINIT_FUNCTION(base64_intrin);
HYSSAPI extern gear_string *(*hyss_base64_encode)(const unsigned char *, size_t);
HYSSAPI extern gear_string *(*hyss_base64_decode_ex)(const unsigned char *, size_t, gear_bool);
#else
HYSSAPI extern gear_string *hyss_base64_encode(const unsigned char *, size_t);
HYSSAPI extern gear_string *hyss_base64_decode_ex(const unsigned char *, size_t, gear_bool);
#endif

static inline gear_string *hyss_base64_encode_str(const gear_string *str) {
	return hyss_base64_encode((const unsigned char*)(ZSTR_VAL(str)), ZSTR_LEN(str));
}

static inline gear_string *hyss_base64_decode(const unsigned char *str, size_t len) {
	return hyss_base64_decode_ex(str, len, 0);
}
static inline gear_string *hyss_base64_decode_str(const gear_string *str) {
	return hyss_base64_decode_ex((const unsigned char*)(ZSTR_VAL(str)), ZSTR_LEN(str), 0);
}

#endif /* BASE64_H */

