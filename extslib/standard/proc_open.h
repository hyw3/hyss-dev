/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HYSS_WIN32
typedef HANDLE hyss_file_descriptor_t;
typedef DWORD hyss_process_id_t;
#else
typedef int hyss_file_descriptor_t;
typedef pid_t hyss_process_id_t;
#endif

/* Environment block under win32 is a NUL terminated sequence of NUL terminated
 * name=value strings.
 * Under unix, it is an argv style array.
 * */
typedef struct _hyss_process_env {
	char *envp;
#ifndef HYSS_WIN32
	char **envarray;
#endif
} hyss_process_env_t;

struct hyss_process_handle {
	hyss_process_id_t	child;
#ifdef HYSS_WIN32
	HANDLE childHandle;
#endif
	int npipes;
	gear_resource **pipes;
	char *command;
	int is_persistent;
	hyss_process_env_t env;
};
