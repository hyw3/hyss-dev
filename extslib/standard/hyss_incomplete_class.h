/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_INCOMPLETE_CLASS_H
#define HYSS_INCOMPLETE_CLASS_H

#include "extslib/standard/basic_functions.h"

#define HYSS_IC_ENTRY \
	BG(incomplete_class)

#define HYSS_SET_CLASS_ATTRIBUTES(struc) \
	/* OBJECTS_FIXME: Fix for new object model */	\
	if (Z_OBJCE_P(struc) == BG(incomplete_class)) {	\
		class_name = hyss_lookup_class_name(struc); \
		if (!class_name) { \
			class_name = gear_string_init(INCOMPLETE_CLASS, sizeof(INCOMPLETE_CLASS) - 1, 0); \
		} \
		incomplete_class = 1; \
	} else { \
		class_name = gear_string_copy(Z_OBJCE_P(struc)->name); \
	}

#define HYSS_CLEANUP_CLASS_ATTRIBUTES()	\
	gear_string_release_ex(class_name, 0)

#define HYSS_CLASS_ATTRIBUTES											\
	gear_string *class_name;											\
	gear_bool incomplete_class GEAR_ATTRIBUTE_UNUSED = 0

#define INCOMPLETE_CLASS "__HYSS_Incomplete_Class"
#define MAGIC_MEMBER "__HYSS_Incomplete_Class_Name"

#ifdef __cplusplus
extern "C" {
#endif

HYSSAPI gear_class_entry *hyss_create_incomplete_class(void);
HYSSAPI gear_string *hyss_lookup_class_name(zval *object);
HYSSAPI void  hyss_store_class_name(zval *object, const char *name, size_t len);

#ifdef __cplusplus
};
#endif

#endif
