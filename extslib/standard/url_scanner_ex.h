/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef URL_SCANNER_EX_H
#define URL_SCANNER_EX_H

HYSS_MINIT_FUNCTION(url_scanner_ex);
HYSS_MSHUTDOWN_FUNCTION(url_scanner_ex);

HYSS_RINIT_FUNCTION(url_scanner_ex);
HYSS_RSHUTDOWN_FUNCTION(url_scanner_ex);

HYSSAPI char *hyss_url_scanner_adapt_single_url(const char *url, size_t urllen, const char *name, const char *value, size_t *newlen, int encode);
HYSSAPI int hyss_url_scanner_add_session_var(char *name, size_t name_len, char *value, size_t value_len, int encode);
HYSSAPI int hyss_url_scanner_reset_session_var(gear_string *name, int encode);
HYSSAPI int hyss_url_scanner_reset_session_vars(void);
HYSSAPI int hyss_url_scanner_add_var(char *name, size_t name_len, char *value, size_t value_len, int encode);
HYSSAPI int hyss_url_scanner_reset_var(gear_string *name, int encode);
HYSSAPI int hyss_url_scanner_reset_vars(void);

#include "gear_smart_str_public.h"

typedef struct {
	/* Used by the mainloop of the scanner */
	smart_str tag; /* read only */
	smart_str arg; /* read only */
	smart_str val; /* read only */
	smart_str buf;

	/* The result buffer */
	smart_str result;

	/* The data which is appended to each relative URL/FORM */
	smart_str form_app, url_app;

	int active;

	char *lookup_data;
	int state;

	int type;
	smart_str attr_val;
	int tag_type;
	int attr_type;

	/* Everything above is zeroed in RINIT */
	HashTable *tags;
} url_adapt_state_ex_t;

#endif
