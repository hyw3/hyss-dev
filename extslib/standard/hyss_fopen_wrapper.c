/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "hyss.h"
#include "hyss_globals.h"
#include "hyss_standard.h"
#include "hyss_memory_streams.h"
#include "hyss_fopen_wrappers.h"
#include "SAPI.h"

static size_t hyss_stream_output_write(hyss_stream *stream, const char *buf, size_t count) /* {{{ */
{
	HYSSWRITE(buf, count);
	return count;
}
/* }}} */

static size_t hyss_stream_output_read(hyss_stream *stream, char *buf, size_t count) /* {{{ */
{
	stream->eof = 1;
	return 0;
}
/* }}} */

static int hyss_stream_output_close(hyss_stream *stream, int close_handle) /* {{{ */
{
	return 0;
}
/* }}} */

const hyss_stream_ops hyss_stream_output_ops = {
	hyss_stream_output_write,
	hyss_stream_output_read,
	hyss_stream_output_close,
	NULL, /* flush */
	"Output",
	NULL, /* seek */
	NULL, /* cast */
	NULL, /* stat */
	NULL  /* set_option */
};

typedef struct hyss_stream_input { /* {{{ */
	hyss_stream *body;
	gear_off_t position;
} hyss_stream_input_t;
/* }}} */

static size_t hyss_stream_input_write(hyss_stream *stream, const char *buf, size_t count) /* {{{ */
{
	return -1;
}
/* }}} */

static size_t hyss_stream_input_read(hyss_stream *stream, char *buf, size_t count) /* {{{ */
{
	hyss_stream_input_t *input = stream->abstract;
	size_t read;

	if (!SG(post_read) && SG(read_post_bytes) < (int64_t)(input->position + count)) {
		/* read requested data from SAPI */
		size_t read_bytes = sapi_read_post_block(buf, count);

		if (read_bytes > 0) {
			hyss_stream_seek(input->body, 0, SEEK_END);
			hyss_stream_write(input->body, buf, read_bytes);
		}
	}

	if (!input->body->readfilters.head) {
		/* If the input stream contains filters, it's not really seekable. The
			input->position is likely to be wrong for unfiltered data. */
		hyss_stream_seek(input->body, input->position, SEEK_SET);
	}
	read = hyss_stream_read(input->body, buf, count);

	if (!read || read == (size_t) -1) {
		stream->eof = 1;
	} else {
		input->position += read;
	}

	return read;
}
/* }}} */

static int hyss_stream_input_close(hyss_stream *stream, int close_handle) /* {{{ */
{
	efree(stream->abstract);
	stream->abstract = NULL;

	return 0;
}
/* }}} */

static int hyss_stream_input_flush(hyss_stream *stream) /* {{{ */
{
	return -1;
}
/* }}} */

static int hyss_stream_input_seek(hyss_stream *stream, gear_off_t offset, int whence, gear_off_t *newoffset) /* {{{ */
{
	hyss_stream_input_t *input = stream->abstract;

	if (input->body) {
		int sought = hyss_stream_seek(input->body, offset, whence);
		*newoffset = input->position = (input->body)->position;
		return sought;
	}

	return -1;
}
/* }}} */

const hyss_stream_ops hyss_stream_input_ops = {
	hyss_stream_input_write,
	hyss_stream_input_read,
	hyss_stream_input_close,
	hyss_stream_input_flush,
	"Input",
	hyss_stream_input_seek,
	NULL, /* cast */
	NULL, /* stat */
	NULL  /* set_option */
};

static void hyss_stream_apply_filter_list(hyss_stream *stream, char *filterlist, int read_chain, int write_chain) /* {{{ */
{
	char *p, *token = NULL;
	hyss_stream_filter *temp_filter;

	p = hyss_strtok_r(filterlist, "|", &token);
	while (p) {
		hyss_url_decode(p, strlen(p));
		if (read_chain) {
			if ((temp_filter = hyss_stream_filter_create(p, NULL, hyss_stream_is_persistent(stream)))) {
				hyss_stream_filter_append(&stream->readfilters, temp_filter);
			} else {
				hyss_error_docref(NULL, E_WARNING, "Unable to create filter (%s)", p);
			}
		}
		if (write_chain) {
			if ((temp_filter = hyss_stream_filter_create(p, NULL, hyss_stream_is_persistent(stream)))) {
				hyss_stream_filter_append(&stream->writefilters, temp_filter);
			} else {
				hyss_error_docref(NULL, E_WARNING, "Unable to create filter (%s)", p);
			}
		}
		p = hyss_strtok_r(NULL, "|", &token);
	}
}
/* }}} */

hyss_stream * hyss_stream_url_wrap_hyss(hyss_stream_wrapper *wrapper, const char *path, const char *mode, int options,
									 gear_string **opened_path, hyss_stream_context *context STREAMS_DC) /* {{{ */
{
	int fd = -1;
	int mode_rw = 0;
	hyss_stream * stream = NULL;
	char *p, *token, *pathdup;
	gear_long max_memory;
	FILE *file = NULL;
#ifdef HYSS_WIN32
	int pipe_requested = 0;
#endif

	if (!strncasecmp(path, "hyss://", 6)) {
		path += 6;
	}

	if (!strncasecmp(path, "temp", 4)) {
		path += 4;
		max_memory = HYSS_STREAM_MAX_MEM;
		if (!strncasecmp(path, "/maxmemory:", 11)) {
			path += 11;
			max_memory = GEAR_STRTOL(path, NULL, 10);
			if (max_memory < 0) {
				gear_throw_error(NULL, "Max memory must be >= 0");
				return NULL;
			}
		}
		mode_rw = hyss_stream_mode_from_str(mode);
		return hyss_stream_temp_create(mode_rw, max_memory);
	}

	if (!strcasecmp(path, "memory")) {
		mode_rw = hyss_stream_mode_from_str(mode);
		return hyss_stream_memory_create(mode_rw);
	}

	if (!strcasecmp(path, "output")) {
		return hyss_stream_alloc(&hyss_stream_output_ops, NULL, 0, "wb");
	}

	if (!strcasecmp(path, "input")) {
		hyss_stream_input_t *input;

		if ((options & STREAM_OPEN_FOR_INCLUDE) && !PG(allow_url_include) ) {
			if (options & REPORT_ERRORS) {
				hyss_error_docref(NULL, E_WARNING, "URL file-access is disabled in the server configuration");
			}
			return NULL;
		}

		input = ecalloc(1, sizeof(*input));
		if ((input->body = SG(request_info).request_body)) {
			hyss_stream_rewind(input->body);
		} else {
			input->body = hyss_stream_temp_create_ex(TEMP_STREAM_DEFAULT, SAPI_POST_BLOCK_SIZE, PG(upload_tmp_dir));
			SG(request_info).request_body = input->body;
		}

		return hyss_stream_alloc(&hyss_stream_input_ops, input, 0, "rb");
	}

	if (!strcasecmp(path, "stdin")) {
		if ((options & STREAM_OPEN_FOR_INCLUDE) && !PG(allow_url_include) ) {
			if (options & REPORT_ERRORS) {
				hyss_error_docref(NULL, E_WARNING, "URL file-access is disabled in the server configuration");
			}
			return NULL;
		}
		if (!strcmp(sapi_capi.name, "cli")) {
			static int cli_in = 0;
			fd = STDIN_FILENO;
			if (cli_in) {
				fd = dup(fd);
			} else {
				cli_in = 1;
				file = stdin;
			}
		} else {
			fd = dup(STDIN_FILENO);
		}
#ifdef HYSS_WIN32
		pipe_requested = 1;
#endif
	} else if (!strcasecmp(path, "stdout")) {
		if (!strcmp(sapi_capi.name, "cli")) {
			static int cli_out = 0;
			fd = STDOUT_FILENO;
			if (cli_out++) {
				fd = dup(fd);
			} else {
				cli_out = 1;
				file = stdout;
			}
		} else {
			fd = dup(STDOUT_FILENO);
		}
#ifdef HYSS_WIN32
		pipe_requested = 1;
#endif
	} else if (!strcasecmp(path, "stderr")) {
		if (!strcmp(sapi_capi.name, "cli")) {
			static int cli_err = 0;
			fd = STDERR_FILENO;
			if (cli_err++) {
				fd = dup(fd);
			} else {
				cli_err = 1;
				file = stderr;
			}
		} else {
			fd = dup(STDERR_FILENO);
		}
#ifdef HYSS_WIN32
		pipe_requested = 1;
#endif
	} else if (!strncasecmp(path, "fd/", 3)) {
		const char *start;
		char       *end;
		gear_long  fildes_ori;
		int		   dtablesize;

		if (strcmp(sapi_capi.name, "cli")) {
			if (options & REPORT_ERRORS) {
				hyss_error_docref(NULL, E_WARNING, "Direct access to file descriptors is only available from command-line HYSS");
			}
			return NULL;
		}

		if ((options & STREAM_OPEN_FOR_INCLUDE) && !PG(allow_url_include) ) {
			if (options & REPORT_ERRORS) {
				hyss_error_docref(NULL, E_WARNING, "URL file-access is disabled in the server configuration");
			}
			return NULL;
		}

		start = &path[3];
		fildes_ori = GEAR_STRTOL(start, &end, 10);
		if (end == start || *end != '\0') {
			hyss_stream_wrapper_log_error(wrapper, options,
				"hyss://fd/ stream must be specified in the form hyss://fd/<orig fd>");
			return NULL;
		}

#if HAVE_UNISTD_H
		dtablesize = getdtablesize();
#else
		dtablesize = INT_MAX;
#endif

		if (fildes_ori < 0 || fildes_ori >= dtablesize) {
			hyss_stream_wrapper_log_error(wrapper, options,
				"The file descriptors must be non-negative numbers smaller than %d", dtablesize);
			return NULL;
		}

		fd = dup((int)fildes_ori);
		if (fd == -1) {
			hyss_stream_wrapper_log_error(wrapper, options,
				"Error duping file descriptor " GEAR_LONG_FMT "; possibly it doesn't exist: "
				"[%d]: %s", fildes_ori, errno, strerror(errno));
			return NULL;
		}
	} else if (!strncasecmp(path, "filter/", 7)) {
		/* Save time/memory when chain isn't specified */
		if (strchr(mode, 'r') || strchr(mode, '+')) {
			mode_rw |= HYSS_STREAM_FILTER_READ;
		}
		if (strchr(mode, 'w') || strchr(mode, '+') || strchr(mode, 'a')) {
			mode_rw |= HYSS_STREAM_FILTER_WRITE;
		}
		pathdup = estrndup(path + 6, strlen(path + 6));
		p = strstr(pathdup, "/resource=");
		if (!p) {
			gear_throw_error(NULL, "No URL resource specified");
			efree(pathdup);
			return NULL;
		}

		if (!(stream = hyss_stream_open_wrapper(p + 10, mode, options, opened_path))) {
			efree(pathdup);
			return NULL;
		}

		*p = '\0';

		p = hyss_strtok_r(pathdup + 1, "/", &token);
		while (p) {
			if (!strncasecmp(p, "read=", 5)) {
				hyss_stream_apply_filter_list(stream, p + 5, 1, 0);
			} else if (!strncasecmp(p, "write=", 6)) {
				hyss_stream_apply_filter_list(stream, p + 6, 0, 1);
			} else {
				hyss_stream_apply_filter_list(stream, p, mode_rw & HYSS_STREAM_FILTER_READ, mode_rw & HYSS_STREAM_FILTER_WRITE);
			}
			p = hyss_strtok_r(NULL, "/", &token);
		}
		efree(pathdup);

		return stream;
	} else {
		/* invalid hyss://thingy */
		hyss_error_docref(NULL, E_WARNING, "Invalid hyss:// URL specified");
		return NULL;
	}

	/* must be stdin, stderr or stdout */
	if (fd == -1)	{
		/* failed to dup */
		return NULL;
	}

#if defined(S_IFSOCK) && !defined(HYSS_WIN32)
	do {
		gear_stat_t st;
		memset(&st, 0, sizeof(st));
		if (gear_fstat(fd, &st) == 0 && (st.st_mode & S_IFMT) == S_IFSOCK) {
			stream = hyss_stream_sock_open_from_socket(fd, NULL);
			if (stream) {
				stream->ops = &hyss_stream_socket_ops;
				return stream;
			}
		}
	} while (0);
#endif

	if (file) {
		stream = hyss_stream_fopen_from_file(file, mode);
	} else {
		stream = hyss_stream_fopen_from_fd(fd, mode, NULL);
		if (stream == NULL) {
			close(fd);
		}
	}

#ifdef HYSS_WIN32
	if (pipe_requested && stream && context) {
		zval *blocking_pipes = hyss_stream_context_get_option(context, "pipe", "blocking");
		if (blocking_pipes) {
			hyss_stream_set_option(stream, HYSS_STREAM_OPTION_PIPE_BLOCKING, zval_get_long(blocking_pipes), NULL);
		}
	}
#endif
	return stream;
}
/* }}} */

static const hyss_stream_wrapper_ops hyss_stdio_wops = {
	hyss_stream_url_wrap_hyss,
	NULL, /* close */
	NULL, /* fstat */
	NULL, /* stat */
	NULL, /* opendir */
	"HYSS",
	NULL, /* unlink */
	NULL, /* rename */
	NULL, /* mkdir */
	NULL, /* rmdir */
	NULL
};

HYSSAPI const hyss_stream_wrapper hyss_stream_hyss_wrapper =	{
	&hyss_stdio_wops,
	NULL,
	0, /* is_url */
};


