/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_FOPEN_WRAPPERS_H
#define HYSS_FOPEN_WRAPPERS_H

hyss_stream *hyss_stream_url_wrap_http(hyss_stream_wrapper *wrapper, const char *path, const char *mode, int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC);
hyss_stream *hyss_stream_url_wrap_ftp(hyss_stream_wrapper *wrapper, const char *path, const char *mode, int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC);
extern HYSSAPI const hyss_stream_wrapper hyss_stream_http_wrapper;
extern HYSSAPI const hyss_stream_wrapper hyss_stream_ftp_wrapper;
extern HYSSAPI const hyss_stream_wrapper hyss_stream_hyss_wrapper;
extern HYSSAPI /*const*/ hyss_stream_wrapper hyss_plain_files_wrapper;

#endif
