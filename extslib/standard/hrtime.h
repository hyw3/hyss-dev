/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HRTIME_H
#define HRTIME_H

#define HYSS_HRTIME_PLATFORM_POSIX   0
#define HYSS_HRTIME_PLATFORM_WINDOWS 0
#define HYSS_HRTIME_PLATFORM_APPLE   0
#define HYSS_HRTIME_PLATFORM_HPUX    0
#define HYSS_HRTIME_PLATFORM_AIX     0

#if defined(_POSIX_TIMERS) && ((_POSIX_TIMERS > 0) || defined(__OpenBSD__)) && defined(_POSIX_MONOTONIC_CLOCK) && defined(CLOCK_MONOTONIC)
# undef  HYSS_HRTIME_PLATFORM_POSIX
# define HYSS_HRTIME_PLATFORM_POSIX 1
#elif defined(_WIN32) || defined(_WIN64)
# undef  HYSS_HRTIME_PLATFORM_WINDOWS
# define HYSS_HRTIME_PLATFORM_WINDOWS 1
#elif defined(__APPLE__)
# undef  HYSS_HRTIME_PLATFORM_APPLE
# define HYSS_HRTIME_PLATFORM_APPLE 1
#elif (defined(__hpux) || defined(hpux)) || ((defined(__sun__) || defined(__sun) || defined(sun)) && (defined(__SVR4) || defined(__svr4__)))
# undef  HYSS_HRTIME_PLATFORM_HPUX
# define HYSS_HRTIME_PLATFORM_HPUX 1
#elif defined(_AIX)
# undef  HYSS_HRTIME_PLATFORM_AIX
# define HYSS_HRTIME_PLATFORM_AIX 1
#endif

#define HRTIME_AVAILABLE (HYSS_HRTIME_PLATFORM_POSIX || HYSS_HRTIME_PLATFORM_WINDOWS || HYSS_HRTIME_PLATFORM_APPLE || HYSS_HRTIME_PLATFORM_HPUX || HYSS_HRTIME_PLATFORM_AIX)

typedef uint64_t hyss_hrtime_t;

HYSSAPI hyss_hrtime_t hyss_hrtime_current(void);

HYSS_MINIT_FUNCTION(hrtime);

HYSS_FUNCTION(hrtime);

#endif /* HRTIME_H */
