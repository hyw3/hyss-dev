/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_lcg.h"

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HYSS_WIN32
#include "win32/time.h"
#else
#include <sys/time.h>
#endif

#ifdef ZTS
int lcg_globals_id;
#else
static hyss_lcg_globals lcg_globals;
#endif

#ifdef HYSS_WIN32
#include <process.h>
#endif

/*
 * combinedLCG() returns a pseudo random number in the range of (0, 1).
 * The function combines two CGs with periods of
 * 2^31 - 85 and 2^31 - 249. The period of this function
 * is equal to the product of both primes.
 */

#define MODMULT(a, b, c, m, s) q = s/a;s=b*(s-a*q)-c*q;if(s<0)s+=m

static void lcg_seed(void);

HYSSAPI double hyss_combined_lcg(void) /* {{{ */
{
	int32_t q;
	int32_t z;

	if (!LCG(seeded)) {
		lcg_seed();
	}

	MODMULT(53668, 40014, 12211, 2147483563L, LCG(s1));
	MODMULT(52774, 40692, 3791, 2147483399L, LCG(s2));

	z = LCG(s1) - LCG(s2);
	if (z < 1) {
		z += 2147483562;
	}

	return z * 4.656613e-10;
}
/* }}} */

static void lcg_seed(void) /* {{{ */
{
	struct timeval tv;

	if (gettimeofday(&tv, NULL) == 0) {
		LCG(s1) = tv.tv_sec ^ (tv.tv_usec<<11);
	} else {
		LCG(s1) = 1;
	}
#ifdef ZTS
	LCG(s2) = (gear_long) pbc_thread_id();
#else
	LCG(s2) = (gear_long) getpid();
#endif

	/* Add entropy to s2 by calling gettimeofday() again */
	if (gettimeofday(&tv, NULL) == 0) {
		LCG(s2) ^= (tv.tv_usec<<11);
	}

	LCG(seeded) = 1;
}
/* }}} */

static void lcg_init_globals(hyss_lcg_globals *lcg_globals_p) /* {{{ */
{
	LCG(seeded) = 0;
}
/* }}} */

HYSS_MINIT_FUNCTION(lcg) /* {{{ */
{
#ifdef ZTS
	ts_allocate_id(&lcg_globals_id, sizeof(hyss_lcg_globals), (ts_allocate_ctor) lcg_init_globals, NULL);
#else
	lcg_init_globals(&lcg_globals);
#endif
	return SUCCESS;
}
/* }}} */

/* {{{ proto float lcg_value()
   Returns a value from the combined linear congruential generator */
HYSS_FUNCTION(lcg_value)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	RETURN_DOUBLE(hyss_combined_lcg());
}
/* }}} */

