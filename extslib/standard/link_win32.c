/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HYSS_WIN32

#include "hyss.h"
#include "hyss_filestat.h"
#include "hyss_globals.h"

#include <WinBase.h>

#include <stdlib.h>

#include <string.h>
#if HAVE_PWD_H
#include "win32/pwd.h"
#endif

#if HAVE_GRP_H
#include "win32/grp.h"
#endif

#include <errno.h>
#include <ctype.h>

#include "hyss_link.h"
#include "hyss_string.h"

/*
TODO:
- Create hyss_readlink (done), hyss_link and hyss_symlink in win32/link.c
- Expose them (HYSSAPI) so extensions developers can use them
- define link/readlink/symlink to their hyss_ equivalent and use them in extslib/standart/link.c
- this file is then useless and we have a portable link API
*/

#ifndef VOLUME_NAME_NT
#define VOLUME_NAME_NT 0x2
#endif

#ifndef VOLUME_NAME_DOS
#define VOLUME_NAME_DOS 0x0
#endif

/* {{{ proto string readlink(string filename)
   Return the target of a symbolic link */
HYSS_FUNCTION(readlink)
{
	char *link;
	ssize_t link_len;
	char target[MAXPATHLEN];

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "p", &link, &link_len) == FAILURE) {
		return;
	}

	if (OPENBASEDIR_CHECKPATH(link)) {
		RETURN_FALSE;
	}

	link_len = hyss_sys_readlink(link, target, MAXPATHLEN);
	if (link_len == -1) {
		hyss_error_docref(NULL, E_WARNING, "readlink failed to read the symbolic link (%s), error %d)", link, GetLastError());
		RETURN_FALSE;
	}
	RETURN_STRING(target);
}
/* }}} */

/* {{{ proto int linkinfo(string filename)
   Returns the st_dev field of the UNIX C stat structure describing the link */
HYSS_FUNCTION(linkinfo)
{
	char *link;
	char *dirname;
	size_t link_len;
	gear_stat_t sb;
	int ret;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "p", &link, &link_len) == FAILURE) {
		return;
	}

	dirname = estrndup(link, link_len);
	hyss_dirname(dirname, link_len);

	if (hyss_check_open_basedir(dirname)) {
		efree(dirname);
		RETURN_FALSE;
	}

	ret = VCWD_STAT(link, &sb);
	if (ret == -1) {
		hyss_error_docref(NULL, E_WARNING, "%s", strerror(errno));
		efree(dirname);
		RETURN_LONG(Z_L(-1));
	}

	efree(dirname);
	RETURN_LONG((gear_long) sb.st_dev);
}
/* }}} */

/* {{{ proto int symlink(string target, string link)
   Create a symbolic link */
HYSS_FUNCTION(symlink)
{
	char *topath, *frompath;
	size_t topath_len, frompath_len;
	BOOLEAN ret;
	char source_p[MAXPATHLEN];
	char dest_p[MAXPATHLEN];
	char dirname[MAXPATHLEN];
	size_t len;
	DWORD attr;
	wchar_t *dstw, *srcw;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "pp", &topath, &topath_len, &frompath, &frompath_len) == FAILURE) {
		return;
	}

	if (!expand_filepath(frompath, source_p)) {
		hyss_error_docref(NULL, E_WARNING, "No such file or directory");
		RETURN_FALSE;
	}

	memcpy(dirname, source_p, sizeof(source_p));
	len = hyss_dirname(dirname, strlen(dirname));

	if (!expand_filepath_ex(topath, dest_p, dirname, len)) {
		hyss_error_docref(NULL, E_WARNING, "No such file or directory");
		RETURN_FALSE;
	}

	if (hyss_stream_locate_url_wrapper(source_p, NULL, STREAM_LOCATE_WRAPPERS_ONLY) ||
		hyss_stream_locate_url_wrapper(dest_p, NULL, STREAM_LOCATE_WRAPPERS_ONLY) )
	{
		hyss_error_docref(NULL, E_WARNING, "Unable to symlink to a URL");
		RETURN_FALSE;
	}

	if (OPENBASEDIR_CHECKPATH(dest_p)) {
		RETURN_FALSE;
	}

	if (OPENBASEDIR_CHECKPATH(source_p)) {
		RETURN_FALSE;
	}

	dstw = hyss_win32_ioutil_any_to_w(topath);
	if (!dstw) {
		hyss_error_docref(NULL, E_WARNING, "UTF-16 conversion failed (error %d)", GetLastError());
		RETURN_FALSE;
	}
	if ((attr = GetFileAttributesW(dstw)) == INVALID_FILE_ATTRIBUTES) {
		free(dstw);
		hyss_error_docref(NULL, E_WARNING, "Could not fetch file information(error %d)", GetLastError());
		RETURN_FALSE;
	}

	srcw = hyss_win32_ioutil_any_to_w(source_p);
	if (!srcw) {
		free(dstw);
		hyss_error_docref(NULL, E_WARNING, "UTF-16 conversion failed (error %d)", GetLastError());
		RETURN_FALSE;
	}
	/* For the source, an expanded path must be used (in ZTS an other thread could have changed the CWD).
	 * For the target the exact string given by the user must be used, relative or not, existing or not.
	 * The target is relative to the link itself, not to the CWD. */
	ret = CreateSymbolicLinkW(srcw, dstw, (attr & FILE_ATTRIBUTE_DIRECTORY ? 1 : 0));

	if (!ret) {
		free(dstw);
		free(srcw);
		hyss_error_docref(NULL, E_WARNING, "Cannot create symlink, error code(%d)", GetLastError());
		RETURN_FALSE;
	}

	free(dstw);
	free(srcw);

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int link(string target, string link)
   Create a hard link */
HYSS_FUNCTION(link)
{
	char *topath, *frompath;
	size_t topath_len, frompath_len;
	int ret;
	char source_p[MAXPATHLEN];
	char dest_p[MAXPATHLEN];
	wchar_t *dstw, *srcw;

	/*First argument to link function is the target and hence should go to frompath
	  Second argument to link function is the link itself and hence should go to topath */
	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss", &frompath, &frompath_len, &topath, &topath_len) == FAILURE) {
		return;
	}

	if (!expand_filepath(frompath, source_p) || !expand_filepath(topath, dest_p)) {
		hyss_error_docref(NULL, E_WARNING, "No such file or directory");
		RETURN_FALSE;
	}

	if (hyss_stream_locate_url_wrapper(source_p, NULL, STREAM_LOCATE_WRAPPERS_ONLY) ||
		hyss_stream_locate_url_wrapper(dest_p, NULL, STREAM_LOCATE_WRAPPERS_ONLY) )
	{
		hyss_error_docref(NULL, E_WARNING, "Unable to link to a URL");
		RETURN_FALSE;
	}

	if (OPENBASEDIR_CHECKPATH(source_p)) {
		RETURN_FALSE;
	}

	if (OPENBASEDIR_CHECKPATH(dest_p)) {
		RETURN_FALSE;
	}

#ifndef ZTS
# define _TO_PATH topath
# define _FROM_PATH frompath
#else
# define _TO_PATH dest_p
# define _FROM_PATH source_p
#endif
	dstw = hyss_win32_ioutil_any_to_w(_TO_PATH);
	if (!dstw) {
		hyss_error_docref(NULL, E_WARNING, "UTF-16 conversion failed (error %d)", GetLastError());
		RETURN_FALSE;
	}
	srcw = hyss_win32_ioutil_any_to_w(_FROM_PATH);
	if (!srcw) {
		free(dstw);
		hyss_error_docref(NULL, E_WARNING, "UTF-16 conversion failed (error %d)", GetLastError());
		RETURN_FALSE;
	}
#undef _TO_PATH
#undef _FROM_PATH

	ret = CreateHardLinkW(dstw, srcw, NULL);

	if (ret == 0) {
		free(dstw);
		free(srcw);
		hyss_error_docref(NULL, E_WARNING, "%s", strerror(errno));
		RETURN_FALSE;
	}

	free(dstw);
	free(srcw);

	RETURN_TRUE;
}
/* }}} */

#endif

