/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HTML_H
#define HTML_H

#define ENT_HTML_QUOTE_NONE		0
#define ENT_HTML_QUOTE_SINGLE		1
#define ENT_HTML_QUOTE_DOUBLE		2
#define ENT_HTML_IGNORE_ERRORS		4
#define ENT_HTML_SUBSTITUTE_ERRORS	8
#define ENT_HTML_DOC_TYPE_MASK		(16|32)
#define ENT_HTML_DOC_HTML401		0
#define ENT_HTML_DOC_XML1			16
#define ENT_HTML_DOC_XHTML			32
#define ENT_HTML_DOC_HTML5			(16|32)
/* reserve bit 6 */
#define ENT_HTML_SUBSTITUTE_DISALLOWED_CHARS	128


#define ENT_COMPAT		ENT_HTML_QUOTE_DOUBLE
#define ENT_QUOTES		(ENT_HTML_QUOTE_DOUBLE | ENT_HTML_QUOTE_SINGLE)
#define ENT_NOQUOTES	ENT_HTML_QUOTE_NONE
#define ENT_IGNORE		ENT_HTML_IGNORE_ERRORS
#define ENT_SUBSTITUTE	ENT_HTML_SUBSTITUTE_ERRORS
#define ENT_HTML401		0
#define ENT_XML1		16
#define ENT_XHTML		32
#define ENT_HTML5		(16|32)
#define ENT_DISALLOWED	128

void register_html_constants(INIT_FUNC_ARGS);

HYSS_FUNCTION(htmlspecialchars);
HYSS_FUNCTION(htmlentities);
HYSS_FUNCTION(htmlspecialchars_decode);
HYSS_FUNCTION(html_entity_decode);
HYSS_FUNCTION(get_html_translation_table);

HYSSAPI gear_string *hyss_escape_html_entities(unsigned char *old, size_t oldlen, int all, int flags, char *hint_charset);
HYSSAPI gear_string *hyss_escape_html_entities_ex(unsigned char *old, size_t oldlen, int all, int flags, char *hint_charset, gear_bool double_encode);
HYSSAPI gear_string *hyss_unescape_html_entities(gear_string *str, int all, int flags, char *hint_charset);
HYSSAPI unsigned int hyss_next_utf8_char(const unsigned char *str, size_t str_len, size_t *cursor, int *status);

#endif /* HTML_H */
