/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Portions of this code are based on Berkeley's uuencode/uudecode
 * implementation.
 *
 * Copyright (c) 1983, 1993
 *  The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *  This product includes software developed by the University of
 *  California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <math.h>

#include "hyss.h"
#include "hyss_uuencode.h"

#define HYSS_UU_ENC(c) ((c) ? ((c) & 077) + ' ' : '`')
#define HYSS_UU_ENC_C2(c) HYSS_UU_ENC(((*(c) << 4) & 060) | ((*((c) + 1) >> 4) & 017))
#define HYSS_UU_ENC_C3(c) HYSS_UU_ENC(((*(c + 1) << 2) & 074) | ((*((c) + 2) >> 6) & 03))

#define HYSS_UU_DEC(c) (((c) - ' ') & 077)

HYSSAPI gear_string *hyss_uuencode(char *src, size_t src_len) /* {{{ */
{
	size_t len = 45;
	char *p, *s, *e, *ee;
	gear_string *dest;

	/* encoded length is ~ 38% greater than the original
       Use 1.5 for easier calculation.
    */
	dest = gear_string_safe_alloc(src_len/2, 3, 46, 0);
	p = ZSTR_VAL(dest);
	s = src;
	e = src + src_len;

	while ((s + 3) < e) {
		ee = s + len;
		if (ee > e) {
			ee = e;
			len = ee - s;
			if (len % 3) {
				ee = s + (int) (floor((double)len / 3) * 3);
			}
		}
		*p++ = HYSS_UU_ENC(len);

		while (s < ee) {
			*p++ = HYSS_UU_ENC(*s >> 2);
			*p++ = HYSS_UU_ENC_C2(s);
			*p++ = HYSS_UU_ENC_C3(s);
			*p++ = HYSS_UU_ENC(*(s + 2) & 077);

			s += 3;
		}

		if (len == 45) {
			*p++ = '\n';
		}
	}

	if (s < e) {
		if (len == 45) {
			*p++ = HYSS_UU_ENC(e - s);
			len = 0;
		}

		*p++ = HYSS_UU_ENC(*s >> 2);
		*p++ = HYSS_UU_ENC_C2(s);
		*p++ = ((e - s) > 1) ? HYSS_UU_ENC_C3(s) : HYSS_UU_ENC('\0');
		*p++ = ((e - s) > 2) ? HYSS_UU_ENC(*(s + 2) & 077) : HYSS_UU_ENC('\0');
	}

	if (len < 45) {
		*p++ = '\n';
	}

	*p++ = HYSS_UU_ENC('\0');
	*p++ = '\n';
	*p = '\0';

	dest = gear_string_truncate(dest, p - ZSTR_VAL(dest), 0);
	return dest;
}
/* }}} */

HYSSAPI gear_string *hyss_uudecode(char *src, size_t src_len) /* {{{ */
{
	size_t len, total_len=0;
	char *s, *e, *p, *ee;
	gear_string *dest;

	dest = gear_string_alloc((size_t) ceil(src_len * 0.75), 0);
	p = ZSTR_VAL(dest);
	s = src;
	e = src + src_len;

	while (s < e) {
		if ((len = HYSS_UU_DEC(*s++)) == 0) {
			break;
		}
		/* sanity check */
		if (len > src_len) {
			goto err;
		}

		total_len += len;

		ee = s + (len == 45 ? 60 : (int) floor(len * 1.33));
		/* sanity check */
		if (ee > e) {
			goto err;
		}

		while (s < ee) {
			if(s+4 > e) {
				goto err;
			}
			*p++ = HYSS_UU_DEC(*s) << 2 | HYSS_UU_DEC(*(s + 1)) >> 4;
			*p++ = HYSS_UU_DEC(*(s + 1)) << 4 | HYSS_UU_DEC(*(s + 2)) >> 2;
			*p++ = HYSS_UU_DEC(*(s + 2)) << 6 | HYSS_UU_DEC(*(s + 3));
			s += 4;
		}

		if (len < 45) {
			break;
		}

		/* skip \n */
		s++;
	}

	assert(p >= ZSTR_VAL(dest));
	if ((len = total_len) > (size_t)(p - ZSTR_VAL(dest))) {
		*p++ = HYSS_UU_DEC(*s) << 2 | HYSS_UU_DEC(*(s + 1)) >> 4;
		if (len > 1) {
			*p++ = HYSS_UU_DEC(*(s + 1)) << 4 | HYSS_UU_DEC(*(s + 2)) >> 2;
			if (len > 2) {
				*p++ = HYSS_UU_DEC(*(s + 2)) << 6 | HYSS_UU_DEC(*(s + 3));
			}
		}
	}

	ZSTR_LEN(dest) = total_len;
	ZSTR_VAL(dest)[ZSTR_LEN(dest)] = '\0';

	return dest;

err:
	gear_string_efree(dest);

	return NULL;
}
/* }}} */

/* {{{ proto string convert_uuencode(string data)
   uuencode a string */
HYSS_FUNCTION(convert_uuencode)
{
	gear_string *src;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(src)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);
	if (ZSTR_LEN(src) < 1) { RETURN_FALSE; }

	RETURN_STR(hyss_uuencode(ZSTR_VAL(src), ZSTR_LEN(src)));
}
/* }}} */

/* {{{ proto string convert_uudecode(string data)
   decode a uuencoded string */
HYSS_FUNCTION(convert_uudecode)
{
	gear_string *src;
	gear_string *dest;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(src)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);
	if (ZSTR_LEN(src) < 1) { RETURN_FALSE; }

	if ((dest = hyss_uudecode(ZSTR_VAL(src), ZSTR_LEN(src))) == NULL) {
		hyss_error_docref(NULL, E_WARNING, "The given parameter is not a valid uuencoded string");
		RETURN_FALSE;
	}

	RETURN_STR(dest);
}
/* }}} */

