/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "dl.h"
#include "hyss_globals.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"

#include "SAPI.h"

#if defined(HAVE_LIBDL)
#include <stdlib.h>
#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#else
#include <strings.h>
#endif
#ifdef HYSS_WIN32
#include "win32/param.h"
#include "win32/winutil.h"
#define GET_DL_ERROR()	hyss_win_err()
#else
#include <sys/param.h>
#define GET_DL_ERROR()	DL_ERROR()
#endif
#endif /* defined(HAVE_LIBDL) */

/* {{{ proto int dl(string extension_filename)
   Load a HYSS extension at runtime */
HYSSAPI HYSS_FUNCTION(dl)
{
	char *filename;
	size_t filename_len;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(filename, filename_len)
	GEAR_PARSE_PARAMETERS_END();

	if (!PG(enable_dl)) {
		hyss_error_docref(NULL, E_WARNING, "Dynamically loaded extensions aren't enabled");
		RETURN_FALSE;
	}

	if (filename_len >= MAXPATHLEN) {
		hyss_error_docref(NULL, E_WARNING, "File name exceeds the maximum allowed length of %d characters", MAXPATHLEN);
		RETURN_FALSE;
	}

	hyss_dl(filename, CAPI_TEMPORARY, return_value, 0);
	if (Z_TYPE_P(return_value) == IS_TRUE) {
		EG(full_tables_cleanup) = 1;
	}
}
/* }}} */

#if defined(HAVE_LIBDL)

/* {{{ hyss_load_shlib
 */
HYSSAPI void *hyss_load_shlib(char *path, char **errp)
{
	void *handle;
	char *err;

	handle = DL_LOAD(path);
	if (!handle) {
		err = GET_DL_ERROR();
#ifdef HYSS_WIN32
		if (err && (*err)) {
			size_t i = strlen(err);
			(*errp)=estrdup(err);
			LocalFree(err);
			while (i > 0 && isspace((*errp)[i-1])) { (*errp)[i-1] = '\0'; i--; }
		} else {
			(*errp) = estrdup("<No message>");
		}
#else
		(*errp) = estrdup(err);
		GET_DL_ERROR(); /* free the buffer storing the error */
#endif
	}
	return handle;
}
/* }}} */

/* {{{ hyss_load_extension
 */
HYSSAPI int hyss_load_extension(char *filename, int type, int start_now)
{
	void *handle;
	char *libpath;
	gear_capi_entry *capi_entry;
	gear_capi_entry *(*get_capi)(void);
	int error_type, slash_suffix = 0;
	char *extension_dir;
	char *err1, *err2;

	if (type == CAPI_PERSISTENT) {
		extension_dir = ICS_STR("extension_dir");
	} else {
		extension_dir = PG(extension_dir);
	}

	if (type == CAPI_TEMPORARY) {
		error_type = E_WARNING;
	} else {
		error_type = E_CORE_WARNING;
	}

	/* Check if passed filename contains directory separators */
	if (strchr(filename, '/') != NULL || strchr(filename, DEFAULT_SLASH) != NULL) {
		/* Passing cAPIs with full path is not supported for dynamically loaded extensions */
		if (type == CAPI_TEMPORARY) {
			hyss_error_docref(NULL, E_WARNING, "Temporary cAPI name should contain only filename");
			return FAILURE;
		}
		libpath = estrdup(filename);
	} else if (extension_dir && extension_dir[0]) {
		slash_suffix = IS_SLASH(extension_dir[strlen(extension_dir)-1]);
		/* Try as filename first */
		if (slash_suffix) {
			spprintf(&libpath, 0, "%s%s", extension_dir, filename); /* SAFE */
		} else {
			spprintf(&libpath, 0, "%s%c%s", extension_dir, DEFAULT_SLASH, filename); /* SAFE */
		}
	} else {
		return FAILURE; /* Not full path given or extension_dir is not set */
	}

	handle = hyss_load_shlib(libpath, &err1);
	if (!handle) {
		/* Now, consider 'filename' as extension name and build file name */
		char *orig_libpath = libpath;

		if (slash_suffix) {
			spprintf(&libpath, 0, "%s" HYSS_SHLIB_EXT_PREFIX "%s." HYSS_SHLIB_SUFFIX, extension_dir, filename); /* SAFE */
		} else {
			spprintf(&libpath, 0, "%s%c" HYSS_SHLIB_EXT_PREFIX "%s." HYSS_SHLIB_SUFFIX, extension_dir, DEFAULT_SLASH, filename); /* SAFE */
		}

		handle = hyss_load_shlib(libpath, &err2);
		if (!handle) {
			hyss_error_docref(NULL, error_type, "Unable to load dynamic library '%s' (tried: %s (%s), %s (%s))",
				filename, orig_libpath, err1, libpath, err2);
			efree(orig_libpath);
			efree(err1);
			efree(libpath);
			efree(err2);
			return FAILURE;
		}
		efree(orig_libpath);
		efree(err1);
	}

	efree(libpath);

	get_capi = (gear_capi_entry *(*)(void)) DL_FETCH_SYMBOL(handle, "get_capi");

	/* Some OS prepend _ to symbol names while their dynamic linker
	 * does not do that automatically. Thus we check manually for
	 * _get_capi. */

	if (!get_capi) {
		get_capi = (gear_capi_entry *(*)(void)) DL_FETCH_SYMBOL(handle, "_get_capi");
	}

	if (!get_capi) {
		if (DL_FETCH_SYMBOL(handle, "gear_extension_entry") || DL_FETCH_SYMBOL(handle, "_gear_extension_entry")) {
			DL_UNLOAD(handle);
			hyss_error_docref(NULL, error_type, "Invalid library (appears to be a Gear Extension, try loading using gear_extension=%s from hyss.ics)", filename);
			return FAILURE;
		}
		DL_UNLOAD(handle);
		hyss_error_docref(NULL, error_type, "Invalid library (maybe not a HYSS library) '%s'", filename);
		return FAILURE;
	}
	capi_entry = get_capi();
	if (capi_entry->gear_api != GEAR_CAPI_API_NO) {
			hyss_error_docref(NULL, error_type,
					"%s: Unable to initialize cAPI\n"
					"cAPI compiled with cAPI API=%d\n"
					"HYSS    compiled with cAPI API=%d\n"
					"These options need to match\n",
					capi_entry->name, capi_entry->gear_api, GEAR_CAPI_API_NO);
			DL_UNLOAD(handle);
			return FAILURE;
	}
	if(strcmp(capi_entry->build_id, GEAR_CAPI_BUILD_ID)) {
		hyss_error_docref(NULL, error_type,
				"%s: Unable to initialize cAPI\n"
				"cAPI compiled with build ID=%s\n"
				"HYSS    compiled with build ID=%s\n"
				"These options need to match\n",
				capi_entry->name, capi_entry->build_id, GEAR_CAPI_BUILD_ID);
		DL_UNLOAD(handle);
		return FAILURE;
	}
	capi_entry->type = type;
	capi_entry->capi_number = gear_next_free_capi();
	capi_entry->handle = handle;

	if ((capi_entry = gear_register_capi_ex(capi_entry)) == NULL) {
		DL_UNLOAD(handle);
		return FAILURE;
	}

	if ((type == CAPI_TEMPORARY || start_now) && gear_startup_capi_ex(capi_entry) == FAILURE) {
		DL_UNLOAD(handle);
		return FAILURE;
	}

	if ((type == CAPI_TEMPORARY || start_now) && capi_entry->request_startup_func) {
		if (capi_entry->request_startup_func(type, capi_entry->capi_number) == FAILURE) {
			hyss_error_docref(NULL, error_type, "Unable to initialize cAPI '%s'", capi_entry->name);
			DL_UNLOAD(handle);
			return FAILURE;
		}
	}
	return SUCCESS;
}
/* }}} */

/* {{{ hyss_dl
 */
HYSSAPI void hyss_dl(char *file, int type, zval *return_value, int start_now)
{
	/* Load extension */
	if (hyss_load_extension(file, type, start_now) == FAILURE) {
		RETVAL_FALSE;
	} else {
		RETVAL_TRUE;
	}
}
/* }}} */

HYSS_MINFO_FUNCTION(dl)
{
	hyss_info_print_table_row(2, "Dynamic Library Support", "enabled");
}

#else

HYSSAPI void hyss_dl(char *file, int type, zval *return_value, int start_now)
{
	hyss_error_docref(NULL, E_WARNING, "Cannot dynamically load %s - dynamic cAPIs are not supported", file);
	RETVAL_FALSE;
}

HYSS_MINFO_FUNCTION(dl)
{
	PUTS("Dynamic Library support not available<br />.\n");
}

#endif

