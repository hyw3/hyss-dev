/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_MATH_H
#define HYSS_MATH_H

HYSSAPI double _hyss_math_round(double, int, int);
HYSSAPI gear_string *_hyss_math_number_format(double, int, char, char);
HYSSAPI gear_string *_hyss_math_number_format_ex(double, int, char *, size_t, char *, size_t);
HYSSAPI gear_string * _hyss_math_longtobase(zval *arg, int base);
HYSSAPI gear_long _hyss_math_basetolong(zval *arg, int base);
HYSSAPI int _hyss_math_basetozval(zval *arg, int base, zval *ret);
HYSSAPI gear_string * _hyss_math_zvaltobase(zval *arg, int base);

HYSS_FUNCTION(sin);
HYSS_FUNCTION(cos);
HYSS_FUNCTION(tan);
HYSS_FUNCTION(asin);
HYSS_FUNCTION(acos);
HYSS_FUNCTION(atan);
HYSS_FUNCTION(atan2);
HYSS_FUNCTION(pi);
HYSS_FUNCTION(exp);
HYSS_FUNCTION(log);
HYSS_FUNCTION(log10);
HYSS_FUNCTION(is_finite);
HYSS_FUNCTION(is_infinite);
HYSS_FUNCTION(is_nan);
HYSS_FUNCTION(pow);
HYSS_FUNCTION(sqrt);
HYSS_FUNCTION(rand);
HYSS_FUNCTION(mt_srand);
HYSS_FUNCTION(mt_rand);
HYSS_FUNCTION(mt_getrandmax);
HYSS_FUNCTION(abs);
HYSS_FUNCTION(ceil);
HYSS_FUNCTION(floor);
HYSS_FUNCTION(round);
HYSS_FUNCTION(decbin);
HYSS_FUNCTION(dechex);
HYSS_FUNCTION(decoct);
HYSS_FUNCTION(bindec);
HYSS_FUNCTION(hexdec);
HYSS_FUNCTION(octdec);
HYSS_FUNCTION(base_convert);
HYSS_FUNCTION(number_format);
HYSS_FUNCTION(fmod);
HYSS_FUNCTION(deg2rad);
HYSS_FUNCTION(rad2deg);
HYSS_FUNCTION(intdiv);

   /*
   WARNING: these functions are expermental: they could change their names or
   disappear in the next version of HYSS!
   */
HYSS_FUNCTION(hypot);
HYSS_FUNCTION(expm1);
HYSS_FUNCTION(log1p);

HYSS_FUNCTION(sinh);
HYSS_FUNCTION(cosh);
HYSS_FUNCTION(tanh);

HYSS_FUNCTION(asinh);
HYSS_FUNCTION(acosh);
HYSS_FUNCTION(atanh);

#include <math.h>

#ifndef M_E
#define M_E            2.7182818284590452354   /* e */
#endif

#ifndef M_LOG2E
#define M_LOG2E        1.4426950408889634074   /* log_2 e */
#endif

#ifndef M_LOG10E
#define M_LOG10E       0.43429448190325182765  /* log_10 e */
#endif

#ifndef M_LN2
#define M_LN2          0.69314718055994530942  /* log_e 2 */
#endif

#ifndef M_LN10
#define M_LN10         2.30258509299404568402  /* log_e 10 */
#endif

#ifndef M_PI
#define M_PI           3.14159265358979323846  /* pi */
#endif

#ifndef M_PI_2
#define M_PI_2         1.57079632679489661923  /* pi/2 */
#endif

#ifndef M_PI_4
#define M_PI_4         0.78539816339744830962  /* pi/4 */
#endif

#ifndef M_1_PI
#define M_1_PI         0.31830988618379067154  /* 1/pi */
#endif

#ifndef M_2_PI
#define M_2_PI         0.63661977236758134308  /* 2/pi */
#endif

#ifndef M_SQRTPI
#define M_SQRTPI       1.77245385090551602729  /* sqrt(pi) */
#endif

#ifndef M_2_SQRTPI
#define M_2_SQRTPI     1.12837916709551257390  /* 2/sqrt(pi) */
#endif

#ifndef M_LNPI
#define M_LNPI         1.14472988584940017414  /* ln(pi) */
#endif

#ifndef M_EULER
#define M_EULER        0.57721566490153286061 /* Euler constant */
#endif

#ifndef M_SQRT2
#define M_SQRT2        1.41421356237309504880  /* sqrt(2) */
#endif

#ifndef M_SQRT1_2
#define M_SQRT1_2      0.70710678118654752440  /* 1/sqrt(2) */
#endif

#ifndef M_SQRT3
#define M_SQRT3	       1.73205080756887729352  /* sqrt(3) */
#endif

/* Define rounding modes (all are round-to-nearest) */
#ifndef HYSS_ROUND_HALF_UP
#define HYSS_ROUND_HALF_UP        0x01    /* Arithmetic rounding, up == away from zero */
#endif

#ifndef HYSS_ROUND_HALF_DOWN
#define HYSS_ROUND_HALF_DOWN      0x02    /* Down == towards zero */
#endif

#ifndef HYSS_ROUND_HALF_EVEN
#define HYSS_ROUND_HALF_EVEN      0x03    /* Banker's rounding */
#endif

#ifndef HYSS_ROUND_HALF_ODD
#define HYSS_ROUND_HALF_ODD       0x04
#endif

#endif /* HYSS_MATH_H */
