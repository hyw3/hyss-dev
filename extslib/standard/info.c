/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_ics.h"
#include "hyss_globals.h"
#include "extslib/standard/head.h"
#include "extslib/standard/html.h"
#include "info.h"
#include "credits.h"
#include "css.h"
#include "SAPI.h"
#include <time.h>
#include "hyss_main.h"
#include "gear_globals.h"
#include "gear_extensions.h"
#include "gear_highlight.h"
#ifdef HAVE_SYS_UTSNAME_H
#include <sys/utsname.h>
#endif
#include "url.h"
#include "hyss_string.h"

#ifdef HYSS_WIN32
# include "winver.h"
#endif

#define SECTION(name)	if (!sapi_capi.hyssinfo_as_text) { \
							hyss_info_print("<h2>" name "</h2>\n"); \
						} else { \
							hyss_info_print_table_start(); \
							hyss_info_print_table_header(1, name); \
							hyss_info_print_table_end(); \
						} \

HYSSAPI extern char *hyss_ics_opened_path;
HYSSAPI extern char *hyss_ics_scanned_path;
HYSSAPI extern char *hyss_ics_scanned_files;

static int hyss_info_print_html_esc(const char *str, size_t len) /* {{{ */
{
	size_t written;
	gear_string *new_str;

	new_str = hyss_escape_html_entities((unsigned char *) str, len, 0, ENT_QUOTES, "utf-8");
	written = hyss_output_write(ZSTR_VAL(new_str), ZSTR_LEN(new_str));
	gear_string_free(new_str);
	return written;
}
/* }}} */

static int hyss_info_printf(const char *fmt, ...) /* {{{ */
{
	char *buf;
	size_t len, written;
	va_list argv;

	va_start(argv, fmt);
	len = vspprintf(&buf, 0, fmt, argv);
	va_end(argv);

	written = hyss_output_write(buf, len);
	efree(buf);
	return written;
}
/* }}} */

static int hyss_info_print(const char *str) /* {{{ */
{
	return hyss_output_write(str, strlen(str));
}
/* }}} */

static void hyss_info_print_stream_hash(const char *name, HashTable *ht) /* {{{ */
{
	gear_string *key;

	if (ht) {
		if (gear_hash_num_elements(ht)) {
			int first = 1;

			if (!sapi_capi.hyssinfo_as_text) {
				hyss_info_printf("<tr><td class=\"e\">Registered %s</td><td class=\"v\">", name);
			} else {
				hyss_info_printf("\nRegistered %s => ", name);
			}

			GEAR_HASH_FOREACH_STR_KEY(ht, key) {
				if (key) {
					if (first) {
						first = 0;
					} else {
						hyss_info_print(", ");
					}
					if (!sapi_capi.hyssinfo_as_text) {
						hyss_info_print_html_esc(ZSTR_VAL(key), ZSTR_LEN(key));
					} else {
						hyss_info_print(ZSTR_VAL(key));
					}
				}
			} GEAR_HASH_FOREACH_END();

			if (!sapi_capi.hyssinfo_as_text) {
				hyss_info_print("</td></tr>\n");
			}
		} else {
			char reg_name[128];
			snprintf(reg_name, sizeof(reg_name), "Registered %s", name);
			hyss_info_print_table_row(2, reg_name, "none registered");
		}
	} else {
		hyss_info_print_table_row(2, name, "disabled");
	}
}
/* }}} */

HYSSAPI void hyss_info_print_capi(gear_capi_entry *gear_capi) /* {{{ */
{
	if (gear_capi->info_func || gear_capi->version) {
		if (!sapi_capi.hyssinfo_as_text) {
			gear_string *url_name = hyss_url_encode(gear_capi->name, strlen(gear_capi->name));

			hyss_strtolower(ZSTR_VAL(url_name), ZSTR_LEN(url_name));
			hyss_info_printf("<h2><a name=\"capi_%s\">%s</a></h2>\n", ZSTR_VAL(url_name), gear_capi->name);

			efree(url_name);
		} else {
			hyss_info_print_table_start();
			hyss_info_print_table_header(1, gear_capi->name);
			hyss_info_print_table_end();
		}
		if (gear_capi->info_func) {
			gear_capi->info_func(gear_capi);
		} else {
			hyss_info_print_table_start();
			hyss_info_print_table_row(2, "Version", gear_capi->version);
			hyss_info_print_table_end();
			DISPLAY_ICS_ENTRIES();
		}
	} else {
		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_printf("<tr><td class=\"v\">%s</td></tr>\n", gear_capi->name);
		} else {
			hyss_info_printf("%s\n", gear_capi->name);
		}
	}
}
/* }}} */

static int _display_capi_info_func(zval *el) /* {{{ */
{
	gear_capi_entry *cAPI = (gear_capi_entry*)Z_PTR_P(el);
	if (cAPI->info_func || cAPI->version) {
		hyss_info_print_capi(cAPI);
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

static int _display_capi_info_def(zval *el) /* {{{ */
{
	gear_capi_entry *cAPI = (gear_capi_entry*)Z_PTR_P(el);
	if (!cAPI->info_func && !cAPI->version) {
		hyss_info_print_capi(cAPI);
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

/* {{{ hyss_print_gpcse_array
 */
static void hyss_print_gpcse_array(char *name, uint32_t name_length)
{
	zval *data, *tmp;
	gear_string *string_key;
	gear_ulong num_key;
	gear_string *key;

	key = gear_string_init(name, name_length, 0);
	gear_is_auto_global(key);

	if ((data = gear_hash_find(&EG(symbol_table), key)) != NULL && (Z_TYPE_P(data) == IS_ARRAY)) {
		GEAR_HASH_FOREACH_KEY_VAL(Z_ARRVAL_P(data), num_key, string_key, tmp) {
			if (!sapi_capi.hyssinfo_as_text) {
				hyss_info_print("<tr>");
				hyss_info_print("<td class=\"e\">");
			}

			hyss_info_print("$");
			hyss_info_print(name);
			hyss_info_print("['");

			if (string_key != NULL) {
				if (!sapi_capi.hyssinfo_as_text) {
					hyss_info_print_html_esc(ZSTR_VAL(string_key), ZSTR_LEN(string_key));
				} else {
					hyss_info_print(ZSTR_VAL(string_key));
				}
			} else {
				hyss_info_printf(GEAR_ULONG_FMT, num_key);
			}
			hyss_info_print("']");
			if (!sapi_capi.hyssinfo_as_text) {
				hyss_info_print("</td><td class=\"v\">");
			} else {
				hyss_info_print(" => ");
			}
			if (Z_TYPE_P(tmp) == IS_ARRAY) {
				if (!sapi_capi.hyssinfo_as_text) {
					gear_string *str = gear_print_zval_r_to_str(tmp, 0);
					hyss_info_print("<pre>");
					hyss_info_print_html_esc(ZSTR_VAL(str), ZSTR_LEN(str));
					hyss_info_print("</pre>");
					gear_string_release_ex(str, 0);
				} else {
					gear_print_zval_r(tmp, 0);
				}
			} else {
				gear_string *tmp2;
				gear_string *str = zval_get_tmp_string(tmp, &tmp2);

				if (!sapi_capi.hyssinfo_as_text) {
					if (ZSTR_LEN(str) == 0) {
						hyss_info_print("<i>no value</i>");
					} else {
						hyss_info_print_html_esc(ZSTR_VAL(str), ZSTR_LEN(str));
					}
				} else {
					hyss_info_print(ZSTR_VAL(str));
				}

				gear_tmp_string_release(tmp2);
			}
			if (!sapi_capi.hyssinfo_as_text) {
				hyss_info_print("</td></tr>\n");
			} else {
				hyss_info_print("\n");
			}
		} GEAR_HASH_FOREACH_END();
	}
	gear_string_efree(key);
}
/* }}} */

/* {{{ hyss_info_print_style
 */
void hyss_info_print_style(void)
{
	hyss_info_printf("<style type=\"text/css\">\n");
	hyss_info_print_css();
	hyss_info_printf("</style>\n");
}
/* }}} */

/* {{{ hyss_info_html_esc
 */
HYSSAPI gear_string *hyss_info_html_esc(char *string)
{
	return hyss_escape_html_entities((unsigned char *) string, strlen(string), 0, ENT_QUOTES, NULL);
}
/* }}} */

#ifdef HYSS_WIN32
/* {{{  */

char* hyss_get_windows_name()
{
	OSVERSIONINFOEX osvi = EG(windows_version_info);
	SYSTEM_INFO si;
	DWORD dwType;
	char *major = NULL, *sub = NULL, *retval;

	ZeroMemory(&si, sizeof(SYSTEM_INFO));

	GetNativeSystemInfo(&si);

	if (VER_PLATFORM_WIN32_NT==osvi.dwPlatformId && osvi.dwMajorVersion >= 10) {
		if (osvi.dwMajorVersion == 10) {
			if( osvi.dwMinorVersion == 0 ) {
				if( osvi.wProductType == VER_NT_WORKSTATION ) {
					major = "Windows 10";
				} else {
					major = "Windows Server 2016";
				}
			}
		}
	} else if (VER_PLATFORM_WIN32_NT==osvi.dwPlatformId && osvi.dwMajorVersion >= 6) {
		if (osvi.dwMajorVersion == 6) {
			if( osvi.dwMinorVersion == 0 ) {
				if( osvi.wProductType == VER_NT_WORKSTATION ) {
					major = "Windows Vista";
				} else {
					major = "Windows Server 2008";
				}
			} else if ( osvi.dwMinorVersion == 1 ) {
				if( osvi.wProductType == VER_NT_WORKSTATION )  {
					major = "Windows 7";
				} else {
					major = "Windows Server 2008 R2";
				}
			} else if ( osvi.dwMinorVersion == 2 ) {
				/* could be Windows 8/Windows Server 2012, could be Windows 8.1/Windows Server 2012 R2 */
				/* XXX and one more X - the above comment is true if no manifest is used for two cases:
					- if the HYSS build doesn't use the correct manifest
					- if HYSS DLL loaded under some binary that doesn't use the correct manifest

					So keep the handling here as is for now, even if we know 6.2 is win8 and nothing else, and think about an improvement. */
				OSVERSIONINFOEX osvi81;
				DWORDLONG dwlConditionMask = 0;
				int op = VER_GREATER_EQUAL;

				ZeroMemory(&osvi81, sizeof(OSVERSIONINFOEX));
				osvi81.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
				osvi81.dwMajorVersion = 6;
				osvi81.dwMinorVersion = 3;
				osvi81.wServicePackMajor = 0;

				VER_SET_CONDITION(dwlConditionMask, VER_MAJORVERSION, op);
				VER_SET_CONDITION(dwlConditionMask, VER_MINORVERSION, op);
				VER_SET_CONDITION(dwlConditionMask, VER_SERVICEPACKMAJOR, op);

				if (VerifyVersionInfo(&osvi81,
					VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR,
					dwlConditionMask)) {
					osvi.dwMinorVersion = 3; /* Windows 8.1/Windows Server 2012 R2 */
					if( osvi.wProductType == VER_NT_WORKSTATION )  {
						major = "Windows 8.1";
					} else {
						major = "Windows Server 2012 R2";
					}
				} else {
					if( osvi.wProductType == VER_NT_WORKSTATION )  {
						major = "Windows 8";
					} else {
						major = "Windows Server 2012";
					}
				}
			} else if (osvi.dwMinorVersion == 3) {
				if( osvi.wProductType == VER_NT_WORKSTATION )  {
					major = "Windows 8.1";
				} else {
					major = "Windows Server 2012 R2";
				}
			} else {
				major = "Unknown Windows version";
			}

			/* No return value check, as it can only fail if the input parameters are broken (which we manually supply) */
			GetProductInfo(6, 0, 0, 0, &dwType);

			switch (dwType) {
				case PRODUCT_ULTIMATE:
					sub = "Ultimate Edition";
					break;
				case PRODUCT_HOME_BASIC:
					sub = "Home Basic Edition";
					break;
				case PRODUCT_HOME_PREMIUM:
					sub = "Home Premium Edition";
					break;
				case PRODUCT_ENTERPRISE:
					sub = "Enterprise Edition";
					break;
				case PRODUCT_HOME_BASIC_N:
					sub = "Home Basic N Edition";
					break;
				case PRODUCT_BUSINESS:
					if ((osvi.dwMajorVersion > 6) || (osvi.dwMajorVersion == 6 && osvi.dwMinorVersion > 0)) {
						sub = "Professional Edition";
					} else {
						sub = "Business Edition";
					}
					break;
				case PRODUCT_STANDARD_SERVER:
					sub = "Standard Edition";
					break;
				case PRODUCT_DATACENTER_SERVER:
					sub = "Datacenter Edition";
					break;
				case PRODUCT_SMALLBUSINESS_SERVER:
					sub = "Small Business Server";
					break;
				case PRODUCT_ENTERPRISE_SERVER:
					sub = "Enterprise Edition";
					break;
				case PRODUCT_STARTER:
					if ((osvi.dwMajorVersion > 6) || (osvi.dwMajorVersion == 6 && osvi.dwMinorVersion > 0)) {
						sub = "Starter N Edition";
					} else {
					    sub = "Starter Edition";
					}
					break;
				case PRODUCT_DATACENTER_SERVER_CORE:
					sub = "Datacenter Edition (core installation)";
					break;
				case PRODUCT_STANDARD_SERVER_CORE:
					sub = "Standard Edition (core installation)";
					break;
				case PRODUCT_ENTERPRISE_SERVER_CORE:
					sub = "Enterprise Edition (core installation)";
					break;
				case PRODUCT_ENTERPRISE_SERVER_IA64:
					sub = "Enterprise Edition for Itanium-based Systems";
					break;
				case PRODUCT_BUSINESS_N:
					if ((osvi.dwMajorVersion > 6) || (osvi.dwMajorVersion == 6 && osvi.dwMinorVersion > 0)) {
						sub = "Professional N Edition";
					} else {
						sub = "Business N Edition";
					}
					break;
				case PRODUCT_WEB_SERVER:
					sub = "Web Server Edition";
					break;
				case PRODUCT_CLUSTER_SERVER:
					sub = "HPC Edition";
					break;
				case PRODUCT_HOME_SERVER:
					sub = "Storage Server Essentials Edition";
					break;
				case PRODUCT_STORAGE_EXPRESS_SERVER:
					sub = "Storage Server Express Edition";
					break;
				case PRODUCT_STORAGE_STANDARD_SERVER:
					sub = "Storage Server Standard Edition";
					break;
				case PRODUCT_STORAGE_WORKGROUP_SERVER:
					sub = "Storage Server Workgroup Edition";
					break;
				case PRODUCT_STORAGE_ENTERPRISE_SERVER:
					sub = "Storage Server Enterprise Edition";
					break;
				case PRODUCT_SERVER_FOR_SMALLBUSINESS:
					sub = "Essential Server Solutions Edition";
					break;
				case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM:
					sub = "Small Business Server Premium Edition";
					break;
				case PRODUCT_HOME_PREMIUM_N:
					sub = "Home Premium N Edition";
					break;
				case PRODUCT_ENTERPRISE_N:
					sub = "Enterprise N Edition";
					break;
				case PRODUCT_ULTIMATE_N:
					sub = "Ultimate N Edition";
					break;
				case PRODUCT_WEB_SERVER_CORE:
					sub = "Web Server Edition (core installation)";
					break;
				case PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT:
					sub = "Essential Business Server Management Server Edition";
					break;
				case PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY:
					sub = "Essential Business Server Management Security Edition";
					break;
				case PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING:
					sub = "Essential Business Server Management Messaging Edition";
					break;
				case PRODUCT_SERVER_FOUNDATION:
					sub = "Foundation Edition";
					break;
				case PRODUCT_HOME_PREMIUM_SERVER:
					sub = "Home Server 2011 Edition";
					break;
				case PRODUCT_SERVER_FOR_SMALLBUSINESS_V:
					sub = "Essential Server Solutions Edition (without Hyper-V)";
					break;
				case PRODUCT_STANDARD_SERVER_V:
					sub = "Standard Edition (without Hyper-V)";
					break;
				case PRODUCT_DATACENTER_SERVER_V:
					sub = "Datacenter Edition (without Hyper-V)";
					break;
				case PRODUCT_ENTERPRISE_SERVER_V:
					sub = "Enterprise Edition (without Hyper-V)";
					break;
				case PRODUCT_DATACENTER_SERVER_CORE_V:
					sub = "Datacenter Edition (core installation, without Hyper-V)";
					break;
				case PRODUCT_STANDARD_SERVER_CORE_V:
					sub = "Standard Edition (core installation, without Hyper-V)";
					break;
				case PRODUCT_ENTERPRISE_SERVER_CORE_V:
					sub = "Enterprise Edition (core installation, without Hyper-V)";
					break;
				case PRODUCT_HYPERV:
					sub = "Hyper-V Server";
					break;
				case PRODUCT_STORAGE_EXPRESS_SERVER_CORE:
					sub = "Storage Server Express Edition (core installation)";
					break;
				case PRODUCT_STORAGE_STANDARD_SERVER_CORE:
					sub = "Storage Server Standard Edition (core installation)";
					break;
				case PRODUCT_STORAGE_WORKGROUP_SERVER_CORE:
					sub = "Storage Server Workgroup Edition (core installation)";
					break;
				case PRODUCT_STORAGE_ENTERPRISE_SERVER_CORE:
					sub = "Storage Server Enterprise Edition (core installation)";
					break;
				case PRODUCT_STARTER_N:
					sub = "Starter N Edition";
					break;
				case PRODUCT_PROFESSIONAL:
					sub = "Professional Edition";
					break;
				case PRODUCT_PROFESSIONAL_N:
					sub = "Professional N Edition";
					break;
				case PRODUCT_SB_SOLUTION_SERVER:
					sub = "Small Business Server 2011 Essentials Edition";
					break;
				case PRODUCT_SERVER_FOR_SB_SOLUTIONS:
					sub = "Server For SB Solutions Edition";
					break;
				case PRODUCT_STANDARD_SERVER_SOLUTIONS:
					sub = "Solutions Premium Edition";
					break;
				case PRODUCT_STANDARD_SERVER_SOLUTIONS_CORE:
					sub = "Solutions Premium Edition (core installation)";
					break;
				case PRODUCT_SB_SOLUTION_SERVER_EM:
					sub = "Server For SB Solutions EM Edition";
					break;
				case PRODUCT_SERVER_FOR_SB_SOLUTIONS_EM:
					sub = "Server For SB Solutions EM Edition";
					break;
				case PRODUCT_SOLUTION_EMBEDDEDSERVER:
					sub = "MultiPoint Server Edition";
					break;
				case PRODUCT_ESSENTIALBUSINESS_SERVER_MGMT:
					sub = "Essential Server Solution Management Edition";
					break;
				case PRODUCT_ESSENTIALBUSINESS_SERVER_ADDL:
					sub = "Essential Server Solution Additional Edition";
					break;
				case PRODUCT_ESSENTIALBUSINESS_SERVER_MGMTSVC:
					sub = "Essential Server Solution Management SVC Edition";
					break;
				case PRODUCT_ESSENTIALBUSINESS_SERVER_ADDLSVC:
					sub = "Essential Server Solution Additional SVC Edition";
					break;
				case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM_CORE:
					sub = "Small Business Server Premium Edition (core installation)";
					break;
				case PRODUCT_CLUSTER_SERVER_V:
					sub = "Hyper Core V Edition";
					break;
				case PRODUCT_STARTER_E:
					sub = "Hyper Core V Edition";
					break;
				case PRODUCT_ENTERPRISE_EVALUATION:
					sub = "Enterprise Edition (evaluation installation)";
					break;
				case PRODUCT_MULTIPOINT_STANDARD_SERVER:
					sub = "MultiPoint Server Standard Edition (full installation)";
					break;
				case PRODUCT_MULTIPOINT_PREMIUM_SERVER:
					sub = "MultiPoint Server Premium Edition (full installation)";
					break;
				case PRODUCT_STANDARD_EVALUATION_SERVER:
					sub = "Standard Edition (evaluation installation)";
					break;
				case PRODUCT_DATACENTER_EVALUATION_SERVER:
					sub = "Datacenter Edition (evaluation installation)";
					break;
				case PRODUCT_ENTERPRISE_N_EVALUATION:
					sub = "Enterprise N Edition (evaluation installation)";
					break;
				case PRODUCT_STORAGE_WORKGROUP_EVALUATION_SERVER:
					sub = "Storage Server Workgroup Edition (evaluation installation)";
					break;
				case PRODUCT_STORAGE_STANDARD_EVALUATION_SERVER:
					sub = "Storage Server Standard Edition (evaluation installation)";
					break;
				case PRODUCT_CORE_N:
					sub = "Windows 8 N Edition";
					break;
				case PRODUCT_CORE_COUNTRYSPECIFIC:
					sub = "Windows 8 China Edition";
					break;
				case PRODUCT_CORE_SINGLELANGUAGE:
					sub = "Windows 8 Single Language Edition";
					break;
				case PRODUCT_CORE:
					sub = "Windows 8 Edition";
					break;
				case PRODUCT_PROFESSIONAL_WMC:
					sub = "Professional with Media Center Edition";
					break;
			}
		}
	} else {
		return NULL;
	}

	spprintf(&retval, 0, "%s%s%s%s%s", major, sub?" ":"", sub?sub:"", osvi.szCSDVersion[0] != '\0'?" ":"", osvi.szCSDVersion);
	return retval;
}
/* }}}  */

/* {{{  */
void hyss_get_windows_cpu(char *buf, int bufsize)
{
	SYSTEM_INFO SysInfo;
	GetSystemInfo(&SysInfo);
	switch (SysInfo.wProcessorArchitecture) {
		case PROCESSOR_ARCHITECTURE_INTEL :
			snprintf(buf, bufsize, "i%d", SysInfo.dwProcessorType);
			break;
		case PROCESSOR_ARCHITECTURE_MIPS :
			snprintf(buf, bufsize, "MIPS R%d000", SysInfo.wProcessorLevel);
			break;
		case PROCESSOR_ARCHITECTURE_ALPHA :
			snprintf(buf, bufsize, "Alpha %d", SysInfo.wProcessorLevel);
			break;
		case PROCESSOR_ARCHITECTURE_PPC :
			snprintf(buf, bufsize, "PPC 6%02d", SysInfo.wProcessorLevel);
			break;
		case PROCESSOR_ARCHITECTURE_IA64 :
			snprintf(buf, bufsize,  "IA64");
			break;
#if defined(PROCESSOR_ARCHITECTURE_IA32_ON_WIN64)
		case PROCESSOR_ARCHITECTURE_IA32_ON_WIN64 :
			snprintf(buf, bufsize, "IA32");
			break;
#endif
#if defined(PROCESSOR_ARCHITECTURE_AMD64)
		case PROCESSOR_ARCHITECTURE_AMD64 :
			snprintf(buf, bufsize, "AMD64");
			break;
#endif
		case PROCESSOR_ARCHITECTURE_UNKNOWN :
		default:
			snprintf(buf, bufsize, "Unknown");
			break;
	}
}
/* }}}  */
#endif

/* {{{ hyss_get_uname
 */
HYSSAPI gear_string *hyss_get_uname(char mode)
{
	char *hyss_uname;
	char tmp_uname[256];
#ifdef HYSS_WIN32
	DWORD dwBuild=0;
	DWORD dwVersion = GetVersion();
	DWORD dwWindowsMajorVersion =  (DWORD)(LOBYTE(LOWORD(dwVersion)));
	DWORD dwWindowsMinorVersion =  (DWORD)(HIBYTE(LOWORD(dwVersion)));
	DWORD dwSize = MAX_COMPUTERNAME_LENGTH + 1;
	char ComputerName[MAX_COMPUTERNAME_LENGTH + 1];

	GetComputerName(ComputerName, &dwSize);

	if (mode == 's') {
		hyss_uname = "Windows NT";
	} else if (mode == 'r') {
		snprintf(tmp_uname, sizeof(tmp_uname), "%d.%d", dwWindowsMajorVersion, dwWindowsMinorVersion);
		hyss_uname = tmp_uname;
	} else if (mode == 'n') {
		hyss_uname = ComputerName;
	} else if (mode == 'v') {
		char *winver = hyss_get_windows_name();
		dwBuild = (DWORD)(HIWORD(dwVersion));
		if(winver == NULL) {
			snprintf(tmp_uname, sizeof(tmp_uname), "build %d", dwBuild);
		} else {
			snprintf(tmp_uname, sizeof(tmp_uname), "build %d (%s)", dwBuild, winver);
		}
		hyss_uname = tmp_uname;
		if(winver) {
			efree(winver);
		}
	} else if (mode == 'm') {
		hyss_get_windows_cpu(tmp_uname, sizeof(tmp_uname));
		hyss_uname = tmp_uname;
	} else { /* assume mode == 'a' */
		char *winver = hyss_get_windows_name();
		char wincpu[20];

		GEAR_ASSERT(winver != NULL);

		hyss_get_windows_cpu(wincpu, sizeof(wincpu));
		dwBuild = (DWORD)(HIWORD(dwVersion));

		/* Windows "version" 6.2 could be Windows 8/Windows Server 2012, but also Windows 8.1/Windows Server 2012 R2 */
		if (dwWindowsMajorVersion == 6 && dwWindowsMinorVersion == 2) {
			if (strncmp(winver, "Windows 8.1", 11) == 0 || strncmp(winver, "Windows Server 2012 R2", 22) == 0) {
				dwWindowsMinorVersion = 3;
			}
		}

		snprintf(tmp_uname, sizeof(tmp_uname), "%s %s %d.%d build %d (%s) %s",
				 "Windows NT", ComputerName,
				 dwWindowsMajorVersion, dwWindowsMinorVersion, dwBuild, winver?winver:"unknown", wincpu);
		if(winver) {
			efree(winver);
		}
		hyss_uname = tmp_uname;
	}
#else
#ifdef HAVE_SYS_UTSNAME_H
	struct utsname buf;
	if (uname((struct utsname *)&buf) == -1) {
		hyss_uname = HYSS_UNAME;
	} else {
		if (mode == 's') {
			hyss_uname = buf.sysname;
		} else if (mode == 'r') {
			hyss_uname = buf.release;
		} else if (mode == 'n') {
			hyss_uname = buf.nodename;
		} else if (mode == 'v') {
			hyss_uname = buf.version;
		} else if (mode == 'm') {
			hyss_uname = buf.machine;
		} else { /* assume mode == 'a' */
			snprintf(tmp_uname, sizeof(tmp_uname), "%s %s %s %s %s",
					 buf.sysname, buf.nodename, buf.release, buf.version,
					 buf.machine);
			hyss_uname = tmp_uname;
		}
	}
#else
	hyss_uname = HYSS_UNAME;
#endif
#endif
	return gear_string_init(hyss_uname, strlen(hyss_uname), 0);
}
/* }}} */

/* {{{ hyss_print_info_htmlhead
 */
HYSSAPI void hyss_print_info_htmlhead(void)
{
	hyss_info_print("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"DTD/xhtml1-transitional.dtd\">\n");
	hyss_info_print("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
	hyss_info_print("<head>\n");
	hyss_info_print_style();
	hyss_info_printf("<title>HYSS %s - hyssinfo()</title>", HYSS_VERSION);
	hyss_info_print("<meta name=\"ROBOTS\" content=\"NOINDEX,NOFOLLOW,NOARCHIVE\" />");
	hyss_info_print("</head>\n");
	hyss_info_print("<body><div class=\"center\">\n");
}
/* }}} */

/* {{{ capi_name_cmp */
static int capi_name_cmp(const void *a, const void *b)
{
	Bucket *f = (Bucket *) a;
	Bucket *s = (Bucket *) b;

	return strcasecmp(((gear_capi_entry *)Z_PTR(f->val))->name,
				  ((gear_capi_entry *)Z_PTR(s->val))->name);
}
/* }}} */

/* {{{ hyss_print_info
 */
HYSSAPI void hyss_print_info(int flag)
{
	char **env, *tmp1, *tmp2;
	gear_string *hyss_uname;

	if (!sapi_capi.hyssinfo_as_text) {
		hyss_print_info_htmlhead();
	} else {
		hyss_info_print("hyssinfo()\n");
	}

	if (flag & HYSS_INFO_GENERAL) {
		char *gear_version = get_gear_version();
		char temp_api[10];

		hyss_uname = hyss_get_uname('a');

		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_print_box_start(1);
		}


		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_printf("<h1 class=\"p\">HYSS Version %s</h1>\n", HYSS_VERSION);
		} else {
			hyss_info_print_table_row(2, "HYSS Version", HYSS_VERSION);
		}
		hyss_info_print_box_end();
		hyss_info_print_table_start();
		hyss_info_print_table_row(2, "System", ZSTR_VAL(hyss_uname));
		hyss_info_print_table_row(2, "Build Date", __DATE__ " " __TIME__);
#ifdef COMPILER
		hyss_info_print_table_row(2, "Compiler", COMPILER);
#endif
#ifdef ARCHITECTURE
		hyss_info_print_table_row(2, "Architecture", ARCHITECTURE);
#endif
#ifdef CONFIGURE_COMMAND
		hyss_info_print_table_row(2, "Configure Command", CONFIGURE_COMMAND );
#endif

		if (sapi_capi.pretty_name) {
			hyss_info_print_table_row(2, "Server API", sapi_capi.pretty_name );
		}

#ifdef VIRTUAL_DIR
		hyss_info_print_table_row(2, "Virtual Directory Support", "enabled" );
#else
		hyss_info_print_table_row(2, "Virtual Directory Support", "disabled" );
#endif

		hyss_info_print_table_row(2, "Configuration File (hyss.ics) Path", HYSS_CONFIG_FILE_PATH);
		hyss_info_print_table_row(2, "Loaded Configuration File", hyss_ics_opened_path ? hyss_ics_opened_path : "(none)");
		hyss_info_print_table_row(2, "Scan this dir for additional .ics files", hyss_ics_scanned_path ? hyss_ics_scanned_path : "(none)");
		hyss_info_print_table_row(2, "Additional .ics files parsed", hyss_ics_scanned_files ? hyss_ics_scanned_files : "(none)");

		snprintf(temp_api, sizeof(temp_api), "%d", HYSS_API_VERSION);
		hyss_info_print_table_row(2, "HYSS API", temp_api);

		snprintf(temp_api, sizeof(temp_api), "%d", GEAR_CAPI_API_NO);
		hyss_info_print_table_row(2, "HYSS Extension", temp_api);

		snprintf(temp_api, sizeof(temp_api), "%d", GEAR_EXTENSION_API_NO);
		hyss_info_print_table_row(2, "Gear Extension", temp_api);

		hyss_info_print_table_row(2, "Gear Extension Build", GEAR_EXTENSION_BUILD_ID);
		hyss_info_print_table_row(2, "HYSS Extension Build", GEAR_CAPI_BUILD_ID);

#if GEAR_DEBUG
		hyss_info_print_table_row(2, "Debug Build", "yes" );
#else
		hyss_info_print_table_row(2, "Debug Build", "no" );
#endif

#ifdef ZTS
		hyss_info_print_table_row(2, "Thread Safety", "enabled" );
		hyss_info_print_table_row(2, "Thread API", pbc_api_name() );
#else
		hyss_info_print_table_row(2, "Thread Safety", "disabled" );
#endif

#ifdef GEAR_SIGNALS
		hyss_info_print_table_row(2, "Gear Signal Handling", "enabled" );
#else
		hyss_info_print_table_row(2, "Gear Signal Handling", "disabled" );
#endif

		hyss_info_print_table_row(2, "Gear Memory Manager", is_gear_mm() ? "enabled" : "disabled" );

		{
			const gear_multibyte_functions *functions = gear_multibyte_get_functions();
			char *descr;
			if (functions) {
				spprintf(&descr, 0, "provided by %s", functions->provider_name);
			} else {
				descr = estrdup("disabled");
			}
            hyss_info_print_table_row(2, "Gear Multibyte Support", descr);
			efree(descr);
		}

#if HAVE_IPV6
		hyss_info_print_table_row(2, "IPv6 Support", "enabled" );
#else
		hyss_info_print_table_row(2, "IPv6 Support", "disabled" );
#endif

#if HAVE_DTRACE
		hyss_info_print_table_row(2, "DTrace Support", (gear_dtrace_enabled ? "enabled" : "available, disabled"));
#else
		hyss_info_print_table_row(2, "DTrace Support", "disabled" );
#endif

		hyss_info_print_stream_hash("HYSS Streams",  hyss_stream_get_url_stream_wrappers_hash());
		hyss_info_print_stream_hash("Stream Socket Transports", hyss_stream_xport_get_hash());
		hyss_info_print_stream_hash("Stream Filters", hyss_get_stream_filters_hash());

		hyss_info_print_table_end();

		gear_string_free(hyss_uname);
	}

	gear_ics_sort_entries();

	if (flag & HYSS_INFO_CONFIGURATION) {
		hyss_info_print_hr();
		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_print("<h1>Configuration</h1>\n");
		} else {
			SECTION("Configuration");
		}
		if (!(flag & HYSS_INFO_CAPIS)) {
			SECTION("HYSS Core");
			display_ics_entries(NULL);
		}
	}

	if (flag & HYSS_INFO_CAPIS) {
		HashTable sorted_registry;

		gear_hash_init(&sorted_registry, gear_hash_num_elements(&capi_registry), NULL, NULL, 1);
		gear_hash_copy(&sorted_registry, &capi_registry, NULL);
		gear_hash_sort(&sorted_registry, capi_name_cmp, 0);

		gear_hash_apply(&sorted_registry, _display_capi_info_func);

		SECTION("Additional cAPIs");
		hyss_info_print_table_start();
		hyss_info_print_table_header(1, "cAPI Name");
		gear_hash_apply(&sorted_registry, _display_capi_info_def);
		hyss_info_print_table_end();

		gear_hash_destroy(&sorted_registry);
	}

	if (flag & HYSS_INFO_ENVIRONMENT) {
		SECTION("Environment");
		hyss_info_print_table_start();
		hyss_info_print_table_header(2, "Variable", "Value");
		for (env=environ; env!=NULL && *env !=NULL; env++) {
			tmp1 = estrdup(*env);
			if (!(tmp2=strchr(tmp1,'='))) { /* malformed entry? */
				efree(tmp1);
				continue;
			}
			*tmp2 = 0;
			tmp2++;
			hyss_info_print_table_row(2, tmp1, tmp2);
			efree(tmp1);
		}
		hyss_info_print_table_end();
	}

	if (flag & HYSS_INFO_VARIABLES) {
		zval *data;

		SECTION("HYSS Variables");

		hyss_info_print_table_start();
		hyss_info_print_table_header(2, "Variable", "Value");
		if ((data = gear_hash_str_find(&EG(symbol_table), "HYSS_SELF", sizeof("HYSS_SELF")-1)) != NULL && Z_TYPE_P(data) == IS_STRING) {
			hyss_info_print_table_row(2, "HYSS_SELF", Z_STRVAL_P(data));
		}
		if ((data = gear_hash_str_find(&EG(symbol_table), "HYSS_AUTH_TYPE", sizeof("HYSS_AUTH_TYPE")-1)) != NULL && Z_TYPE_P(data) == IS_STRING) {
			hyss_info_print_table_row(2, "HYSS_AUTH_TYPE", Z_STRVAL_P(data));
		}
		if ((data = gear_hash_str_find(&EG(symbol_table), "HYSS_AUTH_USER", sizeof("HYSS_AUTH_USER")-1)) != NULL && Z_TYPE_P(data) == IS_STRING) {
			hyss_info_print_table_row(2, "HYSS_AUTH_USER", Z_STRVAL_P(data));
		}
		if ((data = gear_hash_str_find(&EG(symbol_table), "HYSS_AUTH_PW", sizeof("HYSS_AUTH_PW")-1)) != NULL && Z_TYPE_P(data) == IS_STRING) {
			hyss_info_print_table_row(2, "HYSS_AUTH_PW", Z_STRVAL_P(data));
		}
		hyss_print_gpcse_array(GEAR_STRL("_REQUEST"));
		hyss_print_gpcse_array(GEAR_STRL("_GET"));
		hyss_print_gpcse_array(GEAR_STRL("_POST"));
		hyss_print_gpcse_array(GEAR_STRL("_FILES"));
		hyss_print_gpcse_array(GEAR_STRL("_COOKIE"));
		hyss_print_gpcse_array(GEAR_STRL("_SERVER"));
		hyss_print_gpcse_array(GEAR_STRL("_ENV"));
		hyss_info_print_table_end();
	}


	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("</div></body></html>");
	}
}
/* }}} */

HYSSAPI void hyss_info_print_table_start(void) /* {{{ */
{
	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("<table>\n");
	} else {
		hyss_info_print("\n");
	}
}
/* }}} */

HYSSAPI void hyss_info_print_table_end(void) /* {{{ */
{
	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("</table>\n");
	}

}
/* }}} */

HYSSAPI void hyss_info_print_box_start(int flag) /* {{{ */
{
	hyss_info_print_table_start();
	if (flag) {
		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_print("<tr class=\"h\"><td>\n");
		}
	} else {
		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_print("<tr class=\"v\"><td>\n");
		} else {
			hyss_info_print("\n");
		}
	}
}
/* }}} */

HYSSAPI void hyss_info_print_box_end(void) /* {{{ */
{
	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("</td></tr>\n");
	}
	hyss_info_print_table_end();
}
/* }}} */

HYSSAPI void hyss_info_print_hr(void) /* {{{ */
{
	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("<hr />\n");
	} else {
		hyss_info_print("\n\n _______________________________________________________________________\n\n");
	}
}
/* }}} */

HYSSAPI void hyss_info_print_table_colspan_header(int num_cols, char *header) /* {{{ */
{
	int spaces;

	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_printf("<tr class=\"h\"><th colspan=\"%d\">%s</th></tr>\n", num_cols, header );
	} else {
		spaces = (int)(74 - strlen(header));
		hyss_info_printf("%*s%s%*s\n", (int)(spaces/2), " ", header, (int)(spaces/2), " ");
	}
}
/* }}} */

/* {{{ hyss_info_print_table_header
 */
HYSSAPI void hyss_info_print_table_header(int num_cols, ...)
{
	int i;
	va_list row_elements;
	char *row_element;

	va_start(row_elements, num_cols);
	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("<tr class=\"h\">");
	}
	for (i=0; i<num_cols; i++) {
		row_element = va_arg(row_elements, char *);
		if (!row_element || !*row_element) {
			row_element = " ";
		}
		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_print("<th>");
			hyss_info_print(row_element);
			hyss_info_print("</th>");
		} else {
			hyss_info_print(row_element);
			if (i < num_cols-1) {
				hyss_info_print(" => ");
			} else {
				hyss_info_print("\n");
			}
		}
	}
	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("</tr>\n");
	}

	va_end(row_elements);
}
/* }}} */

/* {{{ hyss_info_print_table_row_internal
 */
static void hyss_info_print_table_row_internal(int num_cols,
		const char *value_class, va_list row_elements)
{
	int i;
	char *row_element;

	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("<tr>");
	}
	for (i=0; i<num_cols; i++) {
		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_printf("<td class=\"%s\">",
			   (i==0 ? "e" : value_class )
			);
		}
		row_element = va_arg(row_elements, char *);
		if (!row_element || !*row_element) {
			if (!sapi_capi.hyssinfo_as_text) {
				hyss_info_print( "<i>no value</i>" );
			} else {
				hyss_info_print( " " );
			}
		} else {
			if (!sapi_capi.hyssinfo_as_text) {
				hyss_info_print_html_esc(row_element, strlen(row_element));
			} else {
				hyss_info_print(row_element);
				if (i < num_cols-1) {
					hyss_info_print(" => ");
				}
			}
		}
		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_print(" </td>");
		} else if (i == (num_cols - 1)) {
			hyss_info_print("\n");
		}
	}
	if (!sapi_capi.hyssinfo_as_text) {
		hyss_info_print("</tr>\n");
	}
}
/* }}} */

/* {{{ hyss_info_print_table_row
 */
HYSSAPI void hyss_info_print_table_row(int num_cols, ...)
{
	va_list row_elements;

	va_start(row_elements, num_cols);
	hyss_info_print_table_row_internal(num_cols, "v", row_elements);
	va_end(row_elements);
}
/* }}} */

/* {{{ hyss_info_print_table_row_ex
 */
HYSSAPI void hyss_info_print_table_row_ex(int num_cols, const char *value_class,
		...)
{
	va_list row_elements;

	va_start(row_elements, value_class);
	hyss_info_print_table_row_internal(num_cols, value_class, row_elements);
	va_end(row_elements);
}
/* }}} */

/* {{{ register_hyssinfo_constants
 */
void register_hyssinfo_constants(INIT_FUNC_ARGS)
{
	REGISTER_LONG_CONSTANT("INFO_GENERAL", HYSS_INFO_GENERAL, CONST_PERSISTENT|CONST_CS);
	REGISTER_LONG_CONSTANT("INFO_CONFIGURATION", HYSS_INFO_CONFIGURATION, CONST_PERSISTENT|CONST_CS);
	REGISTER_LONG_CONSTANT("INFO_CAPIS", HYSS_INFO_CAPIS, CONST_PERSISTENT|CONST_CS);
	REGISTER_LONG_CONSTANT("INFO_ENVIRONMENT", HYSS_INFO_ENVIRONMENT, CONST_PERSISTENT|CONST_CS);
	REGISTER_LONG_CONSTANT("INFO_VARIABLES", HYSS_INFO_VARIABLES, CONST_PERSISTENT|CONST_CS);
	REGISTER_LONG_CONSTANT("INFO_ALL", HYSS_INFO_ALL, CONST_PERSISTENT|CONST_CS);
}
/* }}} */

/* {{{ proto void hyssinfo([int what])
   Output a page of useful information about HYSS and the current request */
HYSS_FUNCTION(hyssinfo)
{
	gear_long flag = HYSS_INFO_ALL;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(flag)
	GEAR_PARSE_PARAMETERS_END();

	/* Andale!  Andale!  Yee-Hah! */
	hyss_output_start_default();
	hyss_print_info((int)flag);
	hyss_output_end();

	RETURN_TRUE;
}

/* }}} */

/* {{{ proto string hyssversion([string extension])
   Return the current HYSS version */
HYSS_FUNCTION(hyssversion)
{
	char *ext_name = NULL;
	size_t ext_name_len = 0;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(ext_name, ext_name_len)
	GEAR_PARSE_PARAMETERS_END();

	if (!ext_name) {
		RETURN_STRING(HYSS_VERSION);
	} else {
		const char *version;
		version = gear_get_capi_version(ext_name);
		if (version == NULL) {
			RETURN_FALSE;
		}
		RETURN_STRING(version);
	}
}
/* }}} */

/* {{{ proto void hysscredits([int flag])
   Prints the list of people who've contributed to the HYSS project */
HYSS_FUNCTION(hysscredits)
{
	gear_long flag = HYSS_CREDITS_ALL;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(flag)
	GEAR_PARSE_PARAMETERS_END();

	hyss_print_credits((int)flag);
	RETURN_TRUE;
}
/* }}} */


/* {{{ proto string hyss_sapi_name(void)
   Return the current SAPI cAPI name */
HYSS_FUNCTION(hyss_sapi_name)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (sapi_capi.name) {
		RETURN_STRING(sapi_capi.name);
	} else {
		RETURN_FALSE;
	}
}

/* }}} */

/* {{{ proto string hyss_uname(void)
   Return information about the system HYSS was built on */
HYSS_FUNCTION(hyss_uname)
{
	char *mode = "a";
	size_t modelen = sizeof("a")-1;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(mode, modelen)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_STR(hyss_get_uname(*mode));
}

/* }}} */

/* {{{ proto string hyss_ics_scanned_files(void)
   Return comma-separated string of .ics files parsed from the additional ics dir */
HYSS_FUNCTION(hyss_ics_scanned_files)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (hyss_ics_scanned_files) {
		RETURN_STRING(hyss_ics_scanned_files);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto string hyss_ics_loaded_file(void)
   Return the actual loaded ics filename */
HYSS_FUNCTION(hyss_ics_loaded_file)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (hyss_ics_opened_path) {
		RETURN_STRING(hyss_ics_opened_path);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

