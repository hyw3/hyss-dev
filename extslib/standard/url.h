/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef URL_H
#define URL_H

typedef struct hyss_url {
	gear_string *scheme;
	gear_string *user;
	gear_string *pass;
	gear_string *host;
	unsigned short port;
	gear_string *path;
	gear_string *query;
	gear_string *fragment;
} hyss_url;

HYSSAPI void hyss_url_free(hyss_url *theurl);
HYSSAPI hyss_url *hyss_url_parse(char const *str);
HYSSAPI hyss_url *hyss_url_parse_ex(char const *str, size_t length);
HYSSAPI size_t hyss_url_decode(char *str, size_t len); /* return value: length of decoded string */
HYSSAPI size_t hyss_raw_url_decode(char *str, size_t len); /* return value: length of decoded string */
HYSSAPI gear_string *hyss_url_encode(char const *s, size_t len);
HYSSAPI gear_string *hyss_raw_url_encode(char const *s, size_t len);
HYSSAPI char *hyss_replace_controlchars_ex(char *str, size_t len);

HYSS_FUNCTION(parse_url);
HYSS_FUNCTION(urlencode);
HYSS_FUNCTION(urldecode);
HYSS_FUNCTION(rawurlencode);
HYSS_FUNCTION(rawurldecode);
HYSS_FUNCTION(get_headers);

#define HYSS_URL_SCHEME 0
#define HYSS_URL_HOST 1
#define HYSS_URL_PORT 2
#define HYSS_URL_USER 3
#define HYSS_URL_PASS 4
#define HYSS_URL_PATH 5
#define HYSS_URL_QUERY 6
#define HYSS_URL_FRAGMENT 7

#define HYSS_QUERY_RFC1738 1
#define HYSS_QUERY_RFC3986 2

#endif /* URL_H */


