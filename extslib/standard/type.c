/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_incomplete_class.h"

/* {{{ proto string gettype(mixed var)
   Returns the type of the variable */
HYSS_FUNCTION(gettype)
{
	zval *arg;
	gear_string *type;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(arg)
	GEAR_PARSE_PARAMETERS_END();

	type = gear_zval_get_type(arg);
	if (EXPECTED(type)) {
		RETURN_INTERNED_STR(type);
	} else {
		RETURN_STRING("unknown type");
	}
}
/* }}} */

/* {{{ proto bool settype(mixed &var, string type)
   Set the type of the variable */
HYSS_FUNCTION(settype)
{
	zval *var;
	char *type;
	size_t type_len = 0;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_ZVAL_DEREF(var)
		Z_PARAM_STRING(type, type_len)
	GEAR_PARSE_PARAMETERS_END();

	if (!strcasecmp(type, "integer")) {
		convert_to_long(var);
	} else if (!strcasecmp(type, "int")) {
		convert_to_long(var);
	} else if (!strcasecmp(type, "float")) {
		convert_to_double(var);
	} else if (!strcasecmp(type, "double")) { /* deprecated */
		convert_to_double(var);
	} else if (!strcasecmp(type, "string")) {
		convert_to_string(var);
	} else if (!strcasecmp(type, "array")) {
		convert_to_array(var);
	} else if (!strcasecmp(type, "object")) {
		convert_to_object(var);
	} else if (!strcasecmp(type, "bool")) {
		convert_to_boolean(var);
	} else if (!strcasecmp(type, "boolean")) {
		convert_to_boolean(var);
	} else if (!strcasecmp(type, "null")) {
		convert_to_null(var);
	} else if (!strcasecmp(type, "resource")) {
		hyss_error_docref(NULL, E_WARNING, "Cannot convert to resource type");
		RETURN_FALSE;
	} else {
		hyss_error_docref(NULL, E_WARNING, "Invalid type");
		RETURN_FALSE;
	}
	RETVAL_TRUE;
}
/* }}} */

/* {{{ proto int intval(mixed var [, int base])
   Get the integer value of a variable using the optional base for the conversion */
HYSS_FUNCTION(intval)
{
	zval *num;
	gear_long base = 10;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_ZVAL(num)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(base)
	GEAR_PARSE_PARAMETERS_END();

	if (Z_TYPE_P(num) != IS_STRING || base == 10) {
		RETVAL_LONG(zval_get_long(num));
		return;
	}


	if (base == 0 || base == 2) {
		char *strval = Z_STRVAL_P(num);
		size_t strlen = Z_STRLEN_P(num);

		while (isspace(*strval) && strlen) {
			strval++;
			strlen--;
		}

		/* Length of 3+ covers "0b#" and "-0b" (which results in 0) */
		if (strlen > 2) {
			int offset = 0;
			if (strval[0] == '-' || strval[0] == '+') {
				offset = 1;
			}

			if (strval[offset] == '0' && (strval[offset + 1] == 'b' || strval[offset + 1] == 'B')) {
				char *tmpval;
				strlen -= 2; /* Removing "0b" */
				tmpval = emalloc(strlen + 1);

				/* Place the unary symbol at pos 0 if there was one */
				if (offset) {
					tmpval[0] = strval[0];
				}

				/* Copy the data from after "0b" to the end of the buffer */
				memcpy(tmpval + offset, strval + offset + 2, strlen - offset);
				tmpval[strlen] = 0;

				RETVAL_LONG(GEAR_STRTOL(tmpval, NULL, 2));
				efree(tmpval);
				return;
			}
		}
	}

	RETVAL_LONG(GEAR_STRTOL(Z_STRVAL_P(num), NULL, base));
}
/* }}} */

/* {{{ proto float floatval(mixed var)
   Get the float value of a variable */
HYSS_FUNCTION(floatval)
{
	zval *num;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(num)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_DOUBLE(zval_get_double(num));
}
/* }}} */

/* {{{ proto bool boolval(mixed var)
   Get the boolean value of a variable */
HYSS_FUNCTION(boolval)
{
	zval *val;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(val)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_BOOL(gear_is_true(val));
}
/* }}} */

/* {{{ proto string strval(mixed var)
   Get the string value of a variable */
HYSS_FUNCTION(strval)
{
	zval *num;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(num)
	GEAR_PARSE_PARAMETERS_END();

	RETVAL_STR(zval_get_string(num));
}
/* }}} */

static inline void hyss_is_type(INTERNAL_FUNCTION_PARAMETERS, int type)
{
	zval *arg;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(arg)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (Z_TYPE_P(arg) == type) {
		if (type == IS_RESOURCE) {
			const char *type_name = gear_rsrc_list_get_rsrc_type(Z_RES_P(arg));
			if (!type_name) {
				RETURN_FALSE;
			}
		}
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}


/* {{{ proto bool is_null(mixed var)
   Returns true if variable is null
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(is_null)
{
	hyss_is_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, IS_NULL);
}
/* }}} */

/* {{{ proto bool is_resource(mixed var)
   Returns true if variable is a resource
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(is_resource)
{
	hyss_is_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, IS_RESOURCE);
}
/* }}} */

/* {{{ proto bool is_bool(mixed var)
   Returns true if variable is a boolean
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(is_bool)
{
	zval *arg;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(arg)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	RETURN_BOOL(Z_TYPE_P(arg) == IS_FALSE || Z_TYPE_P(arg) == IS_TRUE);
}
/* }}} */

/* {{{ proto bool is_int(mixed var)
   Returns true if variable is an integer
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(is_int)
{
	hyss_is_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, IS_LONG);
}
/* }}} */

/* {{{ proto bool is_float(mixed var)
   Returns true if variable is float point
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(is_float)
{
	hyss_is_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, IS_DOUBLE);
}
/* }}} */

/* {{{ proto bool is_string(mixed var)
   Returns true if variable is a string
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(is_string)
{
	hyss_is_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, IS_STRING);
}
/* }}} */

/* {{{ proto bool is_array(mixed var)
   Returns true if variable is an array
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(is_array)
{
	hyss_is_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, IS_ARRAY);
}
/* }}} */

/* {{{ proto bool is_object(mixed var)
   Returns true if variable is an object
   Warning: This function is special-cased by gear_compile.c and so is usually bypassed */
HYSS_FUNCTION(is_object)
{
	hyss_is_type(INTERNAL_FUNCTION_PARAM_PASSTHRU, IS_OBJECT);
}
/* }}} */

/* {{{ proto bool is_numeric(mixed value)
   Returns true if value is a number or a numeric string */
HYSS_FUNCTION(is_numeric)
{
	zval *arg;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(arg)
	GEAR_PARSE_PARAMETERS_END();

	switch (Z_TYPE_P(arg)) {
		case IS_LONG:
		case IS_DOUBLE:
			RETURN_TRUE;
			break;

		case IS_STRING:
			if (is_numeric_string(Z_STRVAL_P(arg), Z_STRLEN_P(arg), NULL, NULL, 0)) {
				RETURN_TRUE;
			} else {
				RETURN_FALSE;
			}
			break;

		default:
			RETURN_FALSE;
			break;
	}
}
/* }}} */

/* {{{ proto bool is_scalar(mixed value)
   Returns true if value is a scalar */
HYSS_FUNCTION(is_scalar)
{
	zval *arg;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(arg)
	GEAR_PARSE_PARAMETERS_END();

	switch (Z_TYPE_P(arg)) {
		case IS_FALSE:
		case IS_TRUE:
		case IS_DOUBLE:
		case IS_LONG:
		case IS_STRING:
			RETURN_TRUE;
			break;

		default:
			RETURN_FALSE;
			break;
	}
}
/* }}} */

/* {{{ proto bool is_callable(mixed var [, bool syntax_only [, string &callable_name]])
   Returns true if var is callable. */
HYSS_FUNCTION(is_callable)
{
	zval *var, *callable_name = NULL;
	gear_string *name;
	char *error;
	gear_bool retval;
	gear_bool syntax_only = 0;
	int check_flags = 0;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_ZVAL(var)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(syntax_only)
		Z_PARAM_ZVAL_DEREF(callable_name)
	GEAR_PARSE_PARAMETERS_END();

	if (syntax_only) {
		check_flags |= IS_CALLABLE_CHECK_SYNTAX_ONLY;
	}
	if (GEAR_NUM_ARGS() > 2) {
		retval = gear_is_callable_ex(var, NULL, check_flags, &name, NULL, &error);
		zval_ptr_dtor(callable_name);
		ZVAL_STR(callable_name, name);
	} else {
		retval = gear_is_callable_ex(var, NULL, check_flags, NULL, NULL, &error);
	}
	if (error) {
		/* ignore errors */
		efree(error);
	}

	RETURN_BOOL(retval);
}
/* }}} */

/* {{{ proto bool is_iterable(mixed var)
   Returns true if var is iterable (array or instance of Traversable). */
HYSS_FUNCTION(is_iterable)
{
	zval *var;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(var)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_BOOL(gear_is_iterable(var));
}
/* }}} */

/* {{{ proto bool is_countable(mixed var)
   Returns true if var is countable (array or instance of Countable). */
HYSS_FUNCTION(is_countable)
{
	zval *var;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(var)
	GEAR_PARSE_PARAMETERS_END();

	RETURN_BOOL(gear_is_countable(var));
}
/* }}} */

