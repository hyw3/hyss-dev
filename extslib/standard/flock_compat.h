/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLOCK_COMPAT_H
#define FLOCK_COMPAT_H

/* hyss_flock internally uses fcntl whether or not flock is available
 * This way our hyss_flock even works on NFS files.
 * More info: /usr/src/linux/Documentation
 */
HYSSAPI int hyss_flock(int fd, int operation);

#ifndef HAVE_FLOCK
#	define LOCK_SH 1
#	define LOCK_EX 2
#	define LOCK_NB 4
#	define LOCK_UN 8
HYSSAPI int flock(int fd, int operation);
#endif

/* Userland LOCK_* constants */
#define HYSS_LOCK_SH 1
#define HYSS_LOCK_EX 2
#define HYSS_LOCK_UN 3
#define HYSS_LOCK_NB 4

#ifdef HYSS_WIN32
# ifdef EWOULDBLOCK
#  undef EWOULDBLOCK
# endif
# define EWOULDBLOCK WSAEWOULDBLOCK
# define fsync _commit
# define ftruncate(a, b) chsize(a, b)
#endif /* defined(HYSS_WIN32) */

#if !HAVE_INET_ATON
#if HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#if HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#ifndef HYSS_WIN32
extern int inet_aton(const char *, struct in_addr *);
#endif
#endif

#endif	/* FLOCK_COMPAT_H */
