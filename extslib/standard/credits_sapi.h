/*
 automaticaly created by hyss/doc/credits from
 the information found in the various hyss/extslib/.../CREDITS and
 hyss/server/.../CREDITS files
*/

CREDIT_LINE("cLHy Handler", "M. Sindu Natama, Hilman P. Alisabana");
CREDIT_LINE("CGI / FastCGI", "Yazid Amini, Tedja Arsyad");
CREDIT_LINE("CLI", "Aldi Dinata, M. Sindu Natama");
CREDIT_LINE("Embed", "Nikita Yusuf, Anna Nita");
CREDIT_LINE("FastCGI Process Manager", "Yazid Amini");
CREDIT_LINE("hyssdbg", "M. Sindu Natama, Aldi Dinata");
