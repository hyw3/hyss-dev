/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes
*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "hyss.h"
#include "hyss_string.h"
#include "hyss_var.h"
#include "gear_smart_str.h"
#include "basic_functions.h"
#include "hyss_incomplete_class.h"
/* }}} */

struct hyss_serialize_data {
	HashTable ht;
	uint32_t n;
};

#define COMMON (is_ref ? "&" : "")

static void hyss_array_element_dump(zval *zv, gear_ulong index, gear_string *key, int level) /* {{{ */
{
	if (key == NULL) { /* numeric key */
		hyss_printf("%*c[" GEAR_LONG_FMT "]=>\n", level + 1, ' ', index);
	} else { /* string key */
		hyss_printf("%*c[\"", level + 1, ' ');
		HYSSWRITE(ZSTR_VAL(key), ZSTR_LEN(key));
		hyss_printf("\"]=>\n");
	}
	hyss_var_dump(zv, level + 2);
}
/* }}} */

static void hyss_object_property_dump(zval *zv, gear_ulong index, gear_string *key, int level) /* {{{ */
{
	const char *prop_name, *class_name;

	if (key == NULL) { /* numeric key */
		hyss_printf("%*c[" GEAR_LONG_FMT "]=>\n", level + 1, ' ', index);
	} else { /* string key */
		int unmangle = gear_unmangle_property_name(key, &class_name, &prop_name);
		hyss_printf("%*c[", level + 1, ' ');

		if (class_name && unmangle == SUCCESS) {
			if (class_name[0] == '*') {
				hyss_printf("\"%s\":protected", prop_name);
			} else {
				hyss_printf("\"%s\":\"%s\":private", prop_name, class_name);
			}
		} else {
			hyss_printf("\"");
			HYSSWRITE(ZSTR_VAL(key), ZSTR_LEN(key));
			hyss_printf("\"");
		}
		GEAR_PUTS("]=>\n");
	}
	hyss_var_dump(zv, level + 2);
}
/* }}} */

HYSSAPI void hyss_var_dump(zval *struc, int level) /* {{{ */
{
	HashTable *myht;
	gear_string *class_name;
	int is_temp;
	int is_ref = 0;
	gear_ulong num;
	gear_string *key;
	zval *val;
	uint32_t count;

	if (level > 1) {
		hyss_printf("%*c", level - 1, ' ');
	}

again:
	switch (Z_TYPE_P(struc)) {
		case IS_FALSE:
			hyss_printf("%sbool(false)\n", COMMON);
			break;
		case IS_TRUE:
			hyss_printf("%sbool(true)\n", COMMON);
			break;
		case IS_NULL:
			hyss_printf("%sNULL\n", COMMON);
			break;
		case IS_LONG:
			hyss_printf("%sint(" GEAR_LONG_FMT ")\n", COMMON, Z_LVAL_P(struc));
			break;
		case IS_DOUBLE:
			hyss_printf("%sfloat(%.*G)\n", COMMON, (int) EG(precision), Z_DVAL_P(struc));
			break;
		case IS_STRING:
			hyss_printf("%sstring(%zd) \"", COMMON, Z_STRLEN_P(struc));
			HYSSWRITE(Z_STRVAL_P(struc), Z_STRLEN_P(struc));
			PUTS("\"\n");
			break;
		case IS_ARRAY:
			myht = Z_ARRVAL_P(struc);
			if (level > 1 && !(GC_FLAGS(myht) & GC_IMMUTABLE)) {
				if (GC_IS_RECURSIVE(myht)) {
					PUTS("*RECURSION*\n");
					return;
				}
				GC_PROTECT_RECURSION(myht);
			}
			count = gear_array_count(myht);
			hyss_printf("%sarray(%d) {\n", COMMON, count);

			GEAR_HASH_FOREACH_KEY_VAL_IND(myht, num, key, val) {
				hyss_array_element_dump(val, num, key, level);
			} GEAR_HASH_FOREACH_END();
			if (level > 1 && !(GC_FLAGS(myht) & GC_IMMUTABLE)) {
				GC_UNPROTECT_RECURSION(myht);
			}
			if (level > 1) {
				hyss_printf("%*c", level-1, ' ');
			}
			PUTS("}\n");
			break;
		case IS_OBJECT:
			if (Z_IS_RECURSIVE_P(struc)) {
				PUTS("*RECURSION*\n");
				return;
			}
			Z_PROTECT_RECURSION_P(struc);

			myht = Z_OBJDEBUG_P(struc, is_temp);
			class_name = Z_OBJ_HANDLER_P(struc, get_class_name)(Z_OBJ_P(struc));
			hyss_printf("%sobject(%s)#%d (%d) {\n", COMMON, ZSTR_VAL(class_name), Z_OBJ_HANDLE_P(struc), myht ? gear_array_count(myht) : 0);
			gear_string_release_ex(class_name, 0);

			if (myht) {
				gear_ulong num;
				gear_string *key;
				zval *val;

				GEAR_HASH_FOREACH_KEY_VAL_IND(myht, num, key, val) {
					hyss_object_property_dump(val, num, key, level);
				} GEAR_HASH_FOREACH_END();
				if (is_temp) {
					gear_hash_destroy(myht);
					efree(myht);
				}
			}
			if (level > 1) {
				hyss_printf("%*c", level-1, ' ');
			}
			PUTS("}\n");
			Z_UNPROTECT_RECURSION_P(struc);
			break;
		case IS_RESOURCE: {
			const char *type_name = gear_rsrc_list_get_rsrc_type(Z_RES_P(struc));
			hyss_printf("%sresource(%d) of type (%s)\n", COMMON, Z_RES_P(struc)->handle, type_name ? type_name : "Unknown");
			break;
		}
		case IS_REFERENCE:
			//??? hide references with refcount==1 (for compatibility)
			if (Z_REFCOUNT_P(struc) > 1) {
				is_ref = 1;
			}
			struc = Z_REFVAL_P(struc);
			goto again;
			break;
		default:
			hyss_printf("%sUNKNOWN:0\n", COMMON);
			break;
	}
}
/* }}} */

/* {{{ proto void var_dump(mixed var)
   Dumps a string representation of variable to output */
HYSS_FUNCTION(var_dump)
{
	zval *args;
	int argc;
	int	i;

	GEAR_PARSE_PARAMETERS_START(1, -1)
		Z_PARAM_VARIADIC('+', args, argc)
	GEAR_PARSE_PARAMETERS_END();

	for (i = 0; i < argc; i++) {
		hyss_var_dump(&args[i], 1);
	}
}
/* }}} */

static void zval_array_element_dump(zval *zv, gear_ulong index, gear_string *key, int level) /* {{{ */
{
	if (key == NULL) { /* numeric key */
		hyss_printf("%*c[" GEAR_LONG_FMT "]=>\n", level + 1, ' ', index);
	} else { /* string key */
		hyss_printf("%*c[\"", level + 1, ' ');
		HYSSWRITE(ZSTR_VAL(key), ZSTR_LEN(key));
		hyss_printf("\"]=>\n");
	}
	hyss_debug_zval_dump(zv, level + 2);
}
/* }}} */

static void zval_object_property_dump(zval *zv, gear_ulong index, gear_string *key, int level) /* {{{ */
{
	const char *prop_name, *class_name;

	if (key == NULL) { /* numeric key */
		hyss_printf("%*c[" GEAR_LONG_FMT "]=>\n", level + 1, ' ', index);
	} else { /* string key */
		gear_unmangle_property_name(key, &class_name, &prop_name);
		hyss_printf("%*c[", level + 1, ' ');

		if (class_name) {
			if (class_name[0] == '*') {
				hyss_printf("\"%s\":protected", prop_name);
			} else {
				hyss_printf("\"%s\":\"%s\":private", prop_name, class_name);
			}
		} else {
			hyss_printf("\"%s\"", prop_name);
		}
		GEAR_PUTS("]=>\n");
	}
	hyss_debug_zval_dump(zv, level + 2);
}
/* }}} */

HYSSAPI void hyss_debug_zval_dump(zval *struc, int level) /* {{{ */
{
	HashTable *myht = NULL;
	gear_string *class_name;
	int is_temp = 0;
	int is_ref = 0;
	gear_ulong index;
	gear_string *key;
	zval *val;
	uint32_t count;

	if (level > 1) {
		hyss_printf("%*c", level - 1, ' ');
	}

again:
	switch (Z_TYPE_P(struc)) {
	case IS_FALSE:
		hyss_printf("%sbool(false)\n", COMMON);
		break;
	case IS_TRUE:
		hyss_printf("%sbool(true)\n", COMMON);
		break;
	case IS_NULL:
		hyss_printf("%sNULL\n", COMMON);
		break;
	case IS_LONG:
		hyss_printf("%sint(" GEAR_LONG_FMT ")\n", COMMON, Z_LVAL_P(struc));
		break;
	case IS_DOUBLE:
		hyss_printf("%sfloat(%.*G)\n", COMMON, (int) EG(precision), Z_DVAL_P(struc));
		break;
	case IS_STRING:
		hyss_printf("%sstring(%zd) \"", COMMON, Z_STRLEN_P(struc));
		HYSSWRITE(Z_STRVAL_P(struc), Z_STRLEN_P(struc));
		hyss_printf("\" refcount(%u)\n", Z_REFCOUNTED_P(struc) ? Z_REFCOUNT_P(struc) : 1);
		break;
	case IS_ARRAY:
		myht = Z_ARRVAL_P(struc);
		if (level > 1 && !(GC_FLAGS(myht) & GC_IMMUTABLE)) {
			if (GC_IS_RECURSIVE(myht)) {
				PUTS("*RECURSION*\n");
				return;
			}
			GC_PROTECT_RECURSION(myht);
		}
		count = gear_array_count(myht);
		hyss_printf("%sarray(%d) refcount(%u){\n", COMMON, count, Z_REFCOUNTED_P(struc) ? Z_REFCOUNT_P(struc) : 1);
		GEAR_HASH_FOREACH_KEY_VAL_IND(myht, index, key, val) {
			zval_array_element_dump(val, index, key, level);
		} GEAR_HASH_FOREACH_END();
		if (level > 1 && !(GC_FLAGS(myht) & GC_IMMUTABLE)) {
			GC_UNPROTECT_RECURSION(myht);
		}
		if (is_temp) {
			gear_hash_destroy(myht);
			efree(myht);
		}
		if (level > 1) {
			hyss_printf("%*c", level - 1, ' ');
		}
		PUTS("}\n");
		break;
	case IS_OBJECT:
		myht = Z_OBJDEBUG_P(struc, is_temp);
		if (myht) {
			if (GC_IS_RECURSIVE(myht)) {
				PUTS("*RECURSION*\n");
				return;
			}
			GC_PROTECT_RECURSION(myht);
		}
		class_name = Z_OBJ_HANDLER_P(struc, get_class_name)(Z_OBJ_P(struc));
		hyss_printf("%sobject(%s)#%d (%d) refcount(%u){\n", COMMON, ZSTR_VAL(class_name), Z_OBJ_HANDLE_P(struc), myht ? gear_array_count(myht) : 0, Z_REFCOUNT_P(struc));
		gear_string_release_ex(class_name, 0);
		if (myht) {
			GEAR_HASH_FOREACH_KEY_VAL_IND(myht, index, key, val) {
				zval_object_property_dump(val, index, key, level);
			} GEAR_HASH_FOREACH_END();
			GC_UNPROTECT_RECURSION(myht);
			if (is_temp) {
				gear_hash_destroy(myht);
				efree(myht);
			}
		}
		if (level > 1) {
			hyss_printf("%*c", level - 1, ' ');
		}
		PUTS("}\n");
		break;
	case IS_RESOURCE: {
		const char *type_name = gear_rsrc_list_get_rsrc_type(Z_RES_P(struc));
		hyss_printf("%sresource(%d) of type (%s) refcount(%u)\n", COMMON, Z_RES_P(struc)->handle, type_name ? type_name : "Unknown", Z_REFCOUNT_P(struc));
		break;
	}
	case IS_REFERENCE:
		//??? hide references with refcount==1 (for compatibility)
		if (Z_REFCOUNT_P(struc) > 1) {
			is_ref = 1;
		}
		struc = Z_REFVAL_P(struc);
		goto again;
	default:
		hyss_printf("%sUNKNOWN:0\n", COMMON);
		break;
	}
}
/* }}} */

/* {{{ proto void debug_zval_dump(mixed var)
   Dumps a string representation of an internal gear value to output. */
HYSS_FUNCTION(debug_zval_dump)
{
	zval *args;
	int argc;
	int	i;

	GEAR_PARSE_PARAMETERS_START(1, -1)
		Z_PARAM_VARIADIC('+', args, argc)
	GEAR_PARSE_PARAMETERS_END();

	for (i = 0; i < argc; i++) {
		hyss_debug_zval_dump(&args[i], 1);
	}
}
/* }}} */

#define buffer_append_spaces(buf, num_spaces) \
	do { \
		char *tmp_spaces; \
		size_t tmp_spaces_len; \
		tmp_spaces_len = spprintf(&tmp_spaces, 0,"%*c", num_spaces, ' '); \
		smart_str_appendl(buf, tmp_spaces, tmp_spaces_len); \
		efree(tmp_spaces); \
	} while(0);

static void hyss_array_element_export(zval *zv, gear_ulong index, gear_string *key, int level, smart_str *buf) /* {{{ */
{
	if (key == NULL) { /* numeric key */
		buffer_append_spaces(buf, level+1);
		smart_str_append_long(buf, (gear_long) index);
		smart_str_appendl(buf, " => ", 4);

	} else { /* string key */
		gear_string *tmp_str;
		gear_string *ckey = hyss_addcslashes(key, "'\\", 2);
		tmp_str = hyss_str_to_str(ZSTR_VAL(ckey), ZSTR_LEN(ckey), "\0", 1, "' . \"\\0\" . '", 12);

		buffer_append_spaces(buf, level + 1);

		smart_str_appendc(buf, '\'');
		smart_str_append(buf, tmp_str);
		smart_str_appendl(buf, "' => ", 5);

		gear_string_free(ckey);
		gear_string_free(tmp_str);
	}
	hyss_var_export_ex(zv, level + 2, buf);

	smart_str_appendc(buf, ',');
	smart_str_appendc(buf, '\n');
}
/* }}} */

static void hyss_object_element_export(zval *zv, gear_ulong index, gear_string *key, int level, smart_str *buf) /* {{{ */
{
	buffer_append_spaces(buf, level + 2);
	if (key != NULL) {
		const char *class_name, *prop_name;
		size_t prop_name_len;
		gear_string *pname_esc;

		gear_unmangle_property_name_ex(key, &class_name, &prop_name, &prop_name_len);
		pname_esc = hyss_addcslashes_str(prop_name, prop_name_len, "'\\", 2);

		smart_str_appendc(buf, '\'');
		smart_str_append(buf, pname_esc);
		smart_str_appendc(buf, '\'');
		gear_string_release_ex(pname_esc, 0);
	} else {
		smart_str_append_long(buf, (gear_long) index);
	}
	smart_str_appendl(buf, " => ", 4);
	hyss_var_export_ex(zv, level + 2, buf);
	smart_str_appendc(buf, ',');
	smart_str_appendc(buf, '\n');
}
/* }}} */

HYSSAPI void hyss_var_export_ex(zval *struc, int level, smart_str *buf) /* {{{ */
{
	HashTable *myht;
	char tmp_str[HYSS_DOUBLE_MAX_LENGTH];
	gear_string *ztmp, *ztmp2;
	gear_ulong index;
	gear_string *key;
	zval *val;

again:
	switch (Z_TYPE_P(struc)) {
		case IS_FALSE:
			smart_str_appendl(buf, "false", 5);
			break;
		case IS_TRUE:
			smart_str_appendl(buf, "true", 4);
			break;
		case IS_NULL:
			smart_str_appendl(buf, "NULL", 4);
			break;
		case IS_LONG:
			/* INT_MIN as a literal will be parsed as a float. Emit something like
			 * -9223372036854775807-1 to avoid this. */
			if (Z_LVAL_P(struc) == GEAR_LONG_MIN) {
				smart_str_append_long(buf, GEAR_LONG_MIN+1);
				smart_str_appends(buf, "-1");
				break;
			}
			smart_str_append_long(buf, Z_LVAL_P(struc));
			break;
		case IS_DOUBLE:
			hyss_gcvt(Z_DVAL_P(struc), (int)PG(serialize_precision), '.', 'E', tmp_str);
			smart_str_appends(buf, tmp_str);
			/* Without a decimal point, HYSS treats a number literal as an int.
			 * This check even works for scientific notation, because the
			 * mantissa always contains a decimal point.
			 * We need to check for finiteness, because INF, -INF and NAN
			 * must not have a decimal point added.
			 */
			if (gear_finite(Z_DVAL_P(struc)) && NULL == strchr(tmp_str, '.')) {
				smart_str_appendl(buf, ".0", 2);
			}
			break;
		case IS_STRING:
			ztmp = hyss_addcslashes(Z_STR_P(struc), "'\\", 2);
			ztmp2 = hyss_str_to_str(ZSTR_VAL(ztmp), ZSTR_LEN(ztmp), "\0", 1, "' . \"\\0\" . '", 12);

			smart_str_appendc(buf, '\'');
			smart_str_append(buf, ztmp2);
			smart_str_appendc(buf, '\'');

			gear_string_free(ztmp);
			gear_string_free(ztmp2);
			break;
		case IS_ARRAY:
			myht = Z_ARRVAL_P(struc);
			if (!(GC_FLAGS(myht) & GC_IMMUTABLE)) {
				if (GC_IS_RECURSIVE(myht)) {
					smart_str_appendl(buf, "NULL", 4);
					gear_error(E_WARNING, "var_export does not handle circular references");
					return;
				}
				GC_PROTECT_RECURSION(myht);
			}
			if (level > 1) {
				smart_str_appendc(buf, '\n');
				buffer_append_spaces(buf, level - 1);
			}
			smart_str_appendl(buf, "array (\n", 8);
			GEAR_HASH_FOREACH_KEY_VAL_IND(myht, index, key, val) {
				hyss_array_element_export(val, index, key, level, buf);
			} GEAR_HASH_FOREACH_END();
			if (!(GC_FLAGS(myht) & GC_IMMUTABLE)) {
				GC_UNPROTECT_RECURSION(myht);
			}
			if (level > 1) {
				buffer_append_spaces(buf, level - 1);
			}
			smart_str_appendc(buf, ')');

			break;

		case IS_OBJECT:
			myht = Z_OBJPROP_P(struc);
			if (myht) {
				if (GC_IS_RECURSIVE(myht)) {
					smart_str_appendl(buf, "NULL", 4);
					gear_error(E_WARNING, "var_export does not handle circular references");
					return;
				} else {
					GC_PROTECT_RECURSION(myht);
				}
			}
			if (level > 1) {
				smart_str_appendc(buf, '\n');
				buffer_append_spaces(buf, level - 1);
			}

			/* stdClass has no __set_state method, but can be casted to */
			if (Z_OBJCE_P(struc) == gear_standard_class_def) {
				smart_str_appendl(buf, "(object) array(\n", 16);
			} else {
				smart_str_append(buf, Z_OBJCE_P(struc)->name);
				smart_str_appendl(buf, "::__set_state(array(\n", 21);
			}

			if (myht) {
				GEAR_HASH_FOREACH_KEY_VAL_IND(myht, index, key, val) {
					hyss_object_element_export(val, index, key, level, buf);
				} GEAR_HASH_FOREACH_END();
				GC_UNPROTECT_RECURSION(myht);
			}
			if (level > 1) {
				buffer_append_spaces(buf, level - 1);
			}
			if (Z_OBJCE_P(struc) == gear_standard_class_def) {
				smart_str_appendc(buf, ')');
			} else {
				smart_str_appendl(buf, "))", 2);
			}

			break;
		case IS_REFERENCE:
			struc = Z_REFVAL_P(struc);
			goto again;
			break;
		default:
			smart_str_appendl(buf, "NULL", 4);
			break;
	}
}
/* }}} */

/* FOR BC reasons, this will always perform and then print */
HYSSAPI void hyss_var_export(zval *struc, int level) /* {{{ */
{
	smart_str buf = {0};
	hyss_var_export_ex(struc, level, &buf);
	smart_str_0(&buf);
	HYSSWRITE(ZSTR_VAL(buf.s), ZSTR_LEN(buf.s));
	smart_str_free(&buf);
}
/* }}} */

/* {{{ proto mixed var_export(mixed var [, bool return])
   Outputs or returns a string representation of a variable */
HYSS_FUNCTION(var_export)
{
	zval *var;
	gear_bool return_output = 0;
	smart_str buf = {0};

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_ZVAL(var)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(return_output)
	GEAR_PARSE_PARAMETERS_END();

	hyss_var_export_ex(var, 1, &buf);
	smart_str_0 (&buf);

	if (return_output) {
		RETURN_NEW_STR(buf.s);
	} else {
		HYSSWRITE(ZSTR_VAL(buf.s), ZSTR_LEN(buf.s));
		smart_str_free(&buf);
	}
}
/* }}} */

static void hyss_var_serialize_intern(smart_str *buf, zval *struc, hyss_serialize_data_t var_hash);

static inline gear_long hyss_add_var_hash(hyss_serialize_data_t data, zval *var) /* {{{ */
{
	zval *zv;
	gear_ulong key;
	gear_bool is_ref = Z_ISREF_P(var);

	data->n += 1;

	if (!is_ref && Z_TYPE_P(var) != IS_OBJECT) {
		return 0;
	}

	/* References to objects are treated as if the reference didn't exist */
	if (is_ref && Z_TYPE_P(Z_REFVAL_P(var)) == IS_OBJECT) {
		var = Z_REFVAL_P(var);
	}

	/* Index for the variable is stored using the numeric value of the pointer to
	 * the gear_refcounted struct */
	key = (gear_ulong) (gear_uintptr_t) Z_COUNTED_P(var);
	zv = gear_hash_index_find(&data->ht, key);

	if (zv) {
		/* References are only counted once, undo the data->n increment above */
		if (is_ref) {
			data->n -= 1;
		}

		return Z_LVAL_P(zv);
	} else {
		zval zv_n;
		ZVAL_LONG(&zv_n, data->n);
		gear_hash_index_add_new(&data->ht, key, &zv_n);

		/* Additionally to the index, we also store the variable, to ensure that it is
		 * not destroyed during serialization and its pointer reused. The variable is
		 * stored at the numeric value of the pointer + 1, which cannot be the location
		 * of another gear_refcounted structure. */
		gear_hash_index_add_new(&data->ht, key + 1, var);
		Z_ADDREF_P(var);

		return 0;
	}
}
/* }}} */

static inline void hyss_var_serialize_long(smart_str *buf, gear_long val) /* {{{ */
{
	smart_str_appendl(buf, "i:", 2);
	smart_str_append_long(buf, val);
	smart_str_appendc(buf, ';');
}
/* }}} */

static inline void hyss_var_serialize_string(smart_str *buf, char *str, size_t len) /* {{{ */
{
	smart_str_appendl(buf, "s:", 2);
	smart_str_append_unsigned(buf, len);
	smart_str_appendl(buf, ":\"", 2);
	smart_str_appendl(buf, str, len);
	smart_str_appendl(buf, "\";", 2);
}
/* }}} */

static inline gear_bool hyss_var_serialize_class_name(smart_str *buf, zval *struc) /* {{{ */
{
	HYSS_CLASS_ATTRIBUTES;

	HYSS_SET_CLASS_ATTRIBUTES(struc);
	smart_str_appendl(buf, "O:", 2);
	smart_str_append_unsigned(buf, ZSTR_LEN(class_name));
	smart_str_appendl(buf, ":\"", 2);
	smart_str_append(buf, class_name);
	smart_str_appendl(buf, "\":", 2);
	HYSS_CLEANUP_CLASS_ATTRIBUTES();
	return incomplete_class;
}
/* }}} */

static int hyss_var_serialize_call_sleep(zval *retval, zval *struc) /* {{{ */
{
	zval fname;
	int res;

	ZVAL_STRINGL(&fname, "__sleep", sizeof("__sleep") - 1);
	BG(serialize_lock)++;
	res = call_user_function(CG(function_table), struc, &fname, retval, 0, 0);
	BG(serialize_lock)--;
	zval_ptr_dtor_str(&fname);

	if (res == FAILURE || Z_ISUNDEF_P(retval)) {
		zval_ptr_dtor(retval);
		return FAILURE;
	}

	if (!HASH_OF(retval)) {
		zval_ptr_dtor(retval);
		hyss_error_docref(NULL, E_NOTICE, "__sleep should return an array only containing the names of instance-variables to serialize");
		return FAILURE;
	}

	return SUCCESS;
}
/* }}} */

static void hyss_var_serialize_collect_names(HashTable *ht, HashTable *src) /* {{{ */
{
	zval *val;
	gear_string *name, *tmp_name;

	gear_hash_init(ht, gear_hash_num_elements(src), NULL, NULL, 0);
	GEAR_HASH_FOREACH_VAL(src, val) {
		if (Z_TYPE_P(val) != IS_STRING) {
			hyss_error_docref(NULL, E_NOTICE,
					"__sleep should return an array only containing the names of instance-variables to serialize.");
		}

		name = zval_get_tmp_string(val, &tmp_name);
		if (gear_hash_exists(ht, name)) {
			hyss_error_docref(NULL, E_NOTICE,
					"\"%s\" is returned from __sleep multiple times", ZSTR_VAL(name));
			gear_tmp_string_release(tmp_name);
			continue;
		}
		gear_hash_add_empty_element(ht, name);
		gear_tmp_string_release(tmp_name);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

static void hyss_var_serialize_class(smart_str *buf, zval *struc, zval *retval_ptr, hyss_serialize_data_t var_hash) /* {{{ */
{
	gear_class_entry *ce = Z_OBJCE_P(struc);
	HashTable names, *propers;
	zval nval;
	gear_string *name;

	hyss_var_serialize_class_name(buf, struc);
	hyss_var_serialize_collect_names(&names, HASH_OF(retval_ptr));

	smart_str_append_unsigned(buf, gear_hash_num_elements(&names));
	smart_str_appendl(buf, ":{", 2);

	ZVAL_NULL(&nval);
	propers = Z_OBJPROP_P(struc);

	GEAR_HASH_FOREACH_STR_KEY(&names, name) {
		gear_string *prot_name, *priv_name;

		zval *val = gear_hash_find_ex(propers, name, 1);
		if (val != NULL) {
			if (Z_TYPE_P(val) == IS_INDIRECT) {
				val = Z_INDIRECT_P(val);
				if (Z_TYPE_P(val) == IS_UNDEF) {
					goto undef_prop;
				}
			}

			hyss_var_serialize_string(buf, ZSTR_VAL(name), ZSTR_LEN(name));
			hyss_var_serialize_intern(buf, val, var_hash);
			continue;
		}

		priv_name = gear_mangle_property_name(
				ZSTR_VAL(ce->name), ZSTR_LEN(ce->name), ZSTR_VAL(name), ZSTR_LEN(name), 0);
		val = gear_hash_find(propers, priv_name);
		if (val != NULL) {
			if (Z_TYPE_P(val) == IS_INDIRECT) {
				val = Z_INDIRECT_P(val);
				if (Z_ISUNDEF_P(val)) {
					gear_string_free(priv_name);
					goto undef_prop;
				}
			}

			hyss_var_serialize_string(buf, ZSTR_VAL(priv_name), ZSTR_LEN(priv_name));
			gear_string_free(priv_name);
			hyss_var_serialize_intern(buf, val, var_hash);
			continue;
		}
		gear_string_free(priv_name);

		prot_name = gear_mangle_property_name(
				"*", 1, ZSTR_VAL(name), ZSTR_LEN(name), 0);
		val = gear_hash_find(propers, prot_name);
		if (val != NULL) {
			if (Z_TYPE_P(val) == IS_INDIRECT) {
				val = Z_INDIRECT_P(val);
				if (Z_TYPE_P(val) == IS_UNDEF) {
					gear_string_free(prot_name);
					goto undef_prop;
				}
			}

			hyss_var_serialize_string(buf, ZSTR_VAL(prot_name), ZSTR_LEN(prot_name));
			gear_string_free(prot_name);
			hyss_var_serialize_intern(buf, val, var_hash);
			continue;
		}
		gear_string_free(prot_name);

undef_prop:
		hyss_var_serialize_string(buf, ZSTR_VAL(name), ZSTR_LEN(name));
		hyss_var_serialize_intern(buf, &nval, var_hash);
		hyss_error_docref(NULL, E_NOTICE,
				"\"%s\" returned as member variable from __sleep() but does not exist", ZSTR_VAL(name));
	} GEAR_HASH_FOREACH_END();
	smart_str_appendc(buf, '}');

	gear_hash_destroy(&names);
}
/* }}} */

static void hyss_var_serialize_intern(smart_str *buf, zval *struc, hyss_serialize_data_t var_hash) /* {{{ */
{
	gear_long var_already;
	HashTable *myht;

	if (EG(exception)) {
		return;
	}

	if (var_hash && (var_already = hyss_add_var_hash(var_hash, struc))) {
		if (var_already == -1) {
			/* Reference to an object that failed to serialize, replace with null. */
			smart_str_appendl(buf, "N;", 2);
			return;
		} else if (Z_ISREF_P(struc)) {
			smart_str_appendl(buf, "R:", 2);
			smart_str_append_long(buf, var_already);
			smart_str_appendc(buf, ';');
			return;
		} else if (Z_TYPE_P(struc) == IS_OBJECT) {
			smart_str_appendl(buf, "r:", 2);
			smart_str_append_long(buf, var_already);
			smart_str_appendc(buf, ';');
			return;
		}
	}

again:
	switch (Z_TYPE_P(struc)) {
		case IS_FALSE:
			smart_str_appendl(buf, "b:0;", 4);
			return;

		case IS_TRUE:
			smart_str_appendl(buf, "b:1;", 4);
			return;

		case IS_NULL:
			smart_str_appendl(buf, "N;", 2);
			return;

		case IS_LONG:
			hyss_var_serialize_long(buf, Z_LVAL_P(struc));
			return;

		case IS_DOUBLE: {
			char tmp_str[HYSS_DOUBLE_MAX_LENGTH];
			smart_str_appendl(buf, "d:", 2);
			hyss_gcvt(Z_DVAL_P(struc), (int)PG(serialize_precision), '.', 'E', tmp_str);
			smart_str_appends(buf, tmp_str);
			smart_str_appendc(buf, ';');
			return;
		}

		case IS_STRING:
			hyss_var_serialize_string(buf, Z_STRVAL_P(struc), Z_STRLEN_P(struc));
			return;

		case IS_OBJECT: {
				gear_class_entry *ce = Z_OBJCE_P(struc);

				if (ce->serialize != NULL) {
					/* has custom handler */
					unsigned char *serialized_data = NULL;
					size_t serialized_length;

					if (ce->serialize(struc, &serialized_data, &serialized_length, (gear_serialize_data *)var_hash) == SUCCESS) {
						smart_str_appendl(buf, "C:", 2);
						smart_str_append_unsigned(buf, ZSTR_LEN(Z_OBJCE_P(struc)->name));
						smart_str_appendl(buf, ":\"", 2);
						smart_str_append(buf, Z_OBJCE_P(struc)->name);
						smart_str_appendl(buf, "\":", 2);

						smart_str_append_unsigned(buf, serialized_length);
						smart_str_appendl(buf, ":{", 2);
						smart_str_appendl(buf, (char *) serialized_data, serialized_length);
						smart_str_appendc(buf, '}');
					} else {
						/* Mark this value in the var_hash, to avoid creating references to it. */
						zval *var_idx = gear_hash_index_find(&var_hash->ht,
							(gear_ulong) (gear_uintptr_t) Z_COUNTED_P(struc));
						ZVAL_LONG(var_idx, -1);
						smart_str_appendl(buf, "N;", 2);
					}
					if (serialized_data) {
						efree(serialized_data);
					}
					return;
				}

				if (ce != HYSS_IC_ENTRY && gear_hash_str_exists(&ce->function_table, "__sleep", sizeof("__sleep")-1)) {
					zval retval, tmp;
					ZVAL_COPY(&tmp, struc);

					if (hyss_var_serialize_call_sleep(&retval, &tmp) == FAILURE) {
						if (!EG(exception)) {
							/* we should still add element even if it's not OK,
							 * since we already wrote the length of the array before */
							smart_str_appendl(buf, "N;", 2);
						}
						zval_ptr_dtor(&tmp);
						return;
					}

					hyss_var_serialize_class(buf, &tmp, &retval, var_hash);
					zval_ptr_dtor(&retval);
					zval_ptr_dtor(&tmp);
					return;
				}

				/* fall-through */
			}
		case IS_ARRAY: {
			uint32_t i;
			gear_bool incomplete_class = 0;
			if (Z_TYPE_P(struc) == IS_ARRAY) {
				smart_str_appendl(buf, "a:", 2);
				myht = Z_ARRVAL_P(struc);
				i = gear_array_count(myht);
			} else {
				incomplete_class = hyss_var_serialize_class_name(buf, struc);
				myht = Z_OBJPROP_P(struc);
				/* count after serializing name, since hyss_var_serialize_class_name
				 * changes the count if the variable is incomplete class */
				i = gear_array_count(myht);
				if (i > 0 && incomplete_class) {
					--i;
				}
			}
			smart_str_append_unsigned(buf, i);
			smart_str_appendl(buf, ":{", 2);
			if (i > 0) {
				gear_string *key;
				zval *data;
				gear_ulong index;

				GEAR_HASH_FOREACH_KEY_VAL_IND(myht, index, key, data) {

					if (incomplete_class && strcmp(ZSTR_VAL(key), MAGIC_MEMBER) == 0) {
						continue;
					}

					if (!key) {
						hyss_var_serialize_long(buf, index);
					} else {
						hyss_var_serialize_string(buf, ZSTR_VAL(key), ZSTR_LEN(key));
					}

					if (Z_ISREF_P(data) && Z_REFCOUNT_P(data) == 1) {
						data = Z_REFVAL_P(data);
					}

					/* we should still add element even if it's not OK,
					 * since we already wrote the length of the array before */
					if (Z_TYPE_P(data) == IS_ARRAY) {
						if (UNEXPECTED(Z_IS_RECURSIVE_P(data))
							|| UNEXPECTED(Z_TYPE_P(struc) == IS_ARRAY && Z_ARR_P(data) == Z_ARR_P(struc))) {
							hyss_add_var_hash(var_hash, struc);
							smart_str_appendl(buf, "N;", 2);
						} else {
							if (Z_REFCOUNTED_P(data)) {
								Z_PROTECT_RECURSION_P(data);
							}
							hyss_var_serialize_intern(buf, data, var_hash);
							if (Z_REFCOUNTED_P(data)) {
								Z_UNPROTECT_RECURSION_P(data);
							}
						}
					} else {
						hyss_var_serialize_intern(buf, data, var_hash);
					}
				} GEAR_HASH_FOREACH_END();
			}
			smart_str_appendc(buf, '}');
			return;
		}
		case IS_REFERENCE:
			struc = Z_REFVAL_P(struc);
			goto again;
		default:
			smart_str_appendl(buf, "i:0;", 4);
			return;
	}
}
/* }}} */

HYSSAPI void hyss_var_serialize(smart_str *buf, zval *struc, hyss_serialize_data_t *data) /* {{{ */
{
	hyss_var_serialize_intern(buf, struc, *data);
	smart_str_0(buf);
}
/* }}} */

HYSSAPI hyss_serialize_data_t hyss_var_serialize_init() {
	struct hyss_serialize_data *d;
	/* fprintf(stderr, "SERIALIZE_INIT      == lock: %u, level: %u\n", BG(serialize_lock), BG(serialize).level); */
	if (BG(serialize_lock) || !BG(serialize).level) {
		d = emalloc(sizeof(struct hyss_serialize_data));
		gear_hash_init(&d->ht, 16, NULL, ZVAL_PTR_DTOR, 0);
		d->n = 0;
		if (!BG(serialize_lock)) {
			BG(serialize).data = d;
			BG(serialize).level = 1;
		}
	} else {
		d = BG(serialize).data;
		++BG(serialize).level;
	}
	return d;
}

HYSSAPI void hyss_var_serialize_destroy(hyss_serialize_data_t d) {
	/* fprintf(stderr, "SERIALIZE_DESTROY   == lock: %u, level: %u\n", BG(serialize_lock), BG(serialize).level); */
	if (BG(serialize_lock) || BG(serialize).level == 1) {
		gear_hash_destroy(&d->ht);
		efree(d);
	}
	if (!BG(serialize_lock) && !--BG(serialize).level) {
		BG(serialize).data = NULL;
	}
}

/* {{{ proto string serialize(mixed variable)
   Returns a string representation of variable (which can later be unserialized) */
HYSS_FUNCTION(serialize)
{
	zval *struc;
	hyss_serialize_data_t var_hash;
	smart_str buf = {0};

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(struc)
	GEAR_PARSE_PARAMETERS_END();

	HYSS_VAR_SERIALIZE_INIT(var_hash);
	hyss_var_serialize(&buf, struc, &var_hash);
	HYSS_VAR_SERIALIZE_DESTROY(var_hash);

	if (EG(exception)) {
		smart_str_free(&buf);
		RETURN_FALSE;
	}

	if (buf.s) {
		RETURN_NEW_STR(buf.s);
	} else {
		RETURN_NULL();
	}
}
/* }}} */

/* {{{ proto mixed unserialize(string variable_representation[, array allowed_classes])
   Takes a string representation of variable and recreates it */
HYSS_FUNCTION(unserialize)
{
	char *buf = NULL;
	size_t buf_len;
	const unsigned char *p;
	hyss_unserialize_data_t var_hash;
	zval *options = NULL, *classes = NULL;
	zval *retval;
	HashTable *class_hash = NULL, *prev_class_hash;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STRING(buf, buf_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY(options)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (buf_len == 0) {
		RETURN_FALSE;
	}

	p = (const unsigned char*) buf;
	HYSS_VAR_UNSERIALIZE_INIT(var_hash);

	prev_class_hash = hyss_var_unserialize_get_allowed_classes(var_hash);
	if (options != NULL) {
		classes = gear_hash_str_find(Z_ARRVAL_P(options), "allowed_classes", sizeof("allowed_classes")-1);
		if (classes && Z_TYPE_P(classes) != IS_ARRAY && Z_TYPE_P(classes) != IS_TRUE && Z_TYPE_P(classes) != IS_FALSE) {
			hyss_error_docref(NULL, E_WARNING, "allowed_classes option should be array or boolean");
			HYSS_VAR_UNSERIALIZE_DESTROY(var_hash);
			RETURN_FALSE;
		}

		if(classes && (Z_TYPE_P(classes) == IS_ARRAY || !gear_is_true(classes))) {
			ALLOC_HASHTABLE(class_hash);
			gear_hash_init(class_hash, (Z_TYPE_P(classes) == IS_ARRAY)?gear_hash_num_elements(Z_ARRVAL_P(classes)):0, NULL, NULL, 0);
		}
		if(class_hash && Z_TYPE_P(classes) == IS_ARRAY) {
			zval *entry;
			gear_string *lcname;

			GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(classes), entry) {
				convert_to_string_ex(entry);
				lcname = gear_string_tolower(Z_STR_P(entry));
				gear_hash_add_empty_element(class_hash, lcname);
		        gear_string_release_ex(lcname, 0);
			} GEAR_HASH_FOREACH_END();
		}
		hyss_var_unserialize_set_allowed_classes(var_hash, class_hash);
	}

	retval = var_tmp_var(&var_hash);
	if (!hyss_var_unserialize(retval, &p, p + buf_len, &var_hash)) {
		if (!EG(exception)) {
			hyss_error_docref(NULL, E_NOTICE, "Error at offset " GEAR_LONG_FMT " of %zd bytes",
				(gear_long)((char*)p - buf), buf_len);
		}
		RETVAL_FALSE;
	} else {
		ZVAL_COPY(return_value, retval);
	}

	if (class_hash) {
		gear_hash_destroy(class_hash);
		FREE_HASHTABLE(class_hash);
	}

	/* Reset to previous allowed_classes in case this is a nested call */
	hyss_var_unserialize_set_allowed_classes(var_hash, prev_class_hash);
	HYSS_VAR_UNSERIALIZE_DESTROY(var_hash);

	/* Per calling convention we must not return a reference here, so unwrap. We're doing this at
	 * the very end, because __wakeup() calls performed during UNSERIALIZE_DESTROY might affect
	 * the value we unwrap here. This is compatible with behavior in HYSS <=7.0. */
	if (Z_ISREF_P(return_value)) {
		gear_unwrap_reference(return_value);
	}
}
/* }}} */

/* {{{ proto int memory_get_usage([bool real_usage])
   Returns the allocated by HYSS memory */
HYSS_FUNCTION(memory_get_usage) {
	gear_bool real_usage = 0;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(real_usage)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	RETURN_LONG(gear_memory_usage(real_usage));
}
/* }}} */

/* {{{ proto int memory_get_peak_usage([bool real_usage])
   Returns the peak allocated by HYSS memory */
HYSS_FUNCTION(memory_get_peak_usage) {
	gear_bool real_usage = 0;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(real_usage)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	RETURN_LONG(gear_memory_peak_usage(real_usage));
}
/* }}} */

