$(srcdir)/var_unserializer.c: $(srcdir)/var_unserializer.re
	@(cd $(top_srcdir); $(RE2C) $(RE2C_FLAGS) --no-generation-date -b -o extslib/standard/var_unserializer.c extslib/standard/var_unserializer.re)

$(srcdir)/url_scanner_ex.c: $(srcdir)/url_scanner_ex.re
	@(cd $(top_srcdir); $(RE2C) $(RE2C_FLAGS) --no-generation-date -b -o extslib/standard/url_scanner_ex.c	extslib/standard/url_scanner_ex.re)

$(builddir)/info.lo: $(builddir)/../../main/build-defs.h

$(builddir)/basic_functions.lo: $(top_srcdir)/Gear/gear_language_parser.h
