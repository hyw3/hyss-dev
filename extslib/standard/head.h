/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HEAD_H
#define HEAD_H

#define COOKIE_EXPIRES    "; expires="
#define COOKIE_MAX_AGE    "; Max-Age="
#define COOKIE_DOMAIN     "; domain="
#define COOKIE_PATH       "; path="
#define COOKIE_SECURE     "; secure"
#define COOKIE_HTTPONLY   "; HttpOnly"
#define COOKIE_SAMESITE   "; SameSite="

extern HYSS_RINIT_FUNCTION(head);
HYSS_FUNCTION(header);
HYSS_FUNCTION(header_remove);
HYSS_FUNCTION(setcookie);
HYSS_FUNCTION(setrawcookie);
HYSS_FUNCTION(headers_sent);
HYSS_FUNCTION(headers_list);
HYSS_FUNCTION(http_response_code);

HYSSAPI int hyss_header(void);
HYSSAPI int hyss_setcookie(gear_string *name, gear_string *value, time_t expires, gear_string *path, gear_string *domain, int secure, int httponly, gear_string *samesite, int url_encode);

#endif
