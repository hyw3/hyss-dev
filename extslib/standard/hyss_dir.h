/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_DIR_H
#define HYSS_DIR_H

/* directory functions */
HYSS_MINIT_FUNCTION(dir);
HYSS_RINIT_FUNCTION(dir);
HYSS_FUNCTION(opendir);
HYSS_FUNCTION(closedir);
HYSS_FUNCTION(chdir);
#if defined(HAVE_CHROOT) && !defined(ZTS) && ENABLE_CHROOT_FUNC
HYSS_FUNCTION(chroot);
#endif
HYSS_FUNCTION(getcwd);
HYSS_FUNCTION(rewinddir);
HYSS_NAMED_FUNCTION(hyss_if_readdir);
HYSS_FUNCTION(getdir);
HYSS_FUNCTION(glob);
HYSS_FUNCTION(scandir);

#define HYSS_SCANDIR_SORT_ASCENDING 0
#define HYSS_SCANDIR_SORT_DESCENDING 1
#define HYSS_SCANDIR_SORT_NONE 2

#endif /* HYSS_DIR_H */
