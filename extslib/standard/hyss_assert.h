/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_ASSERT_H
#define HYSS_ASSERT_H

HYSS_MINIT_FUNCTION(assert);
HYSS_MSHUTDOWN_FUNCTION(assert);
HYSS_RINIT_FUNCTION(assert);
HYSS_RSHUTDOWN_FUNCTION(assert);
HYSS_MINFO_FUNCTION(assert);
HYSS_FUNCTION(assert);
HYSS_FUNCTION(assert_options);

#endif /* HYSS_ASSERT_H */
