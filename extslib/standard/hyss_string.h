/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_STRING_H
#define HYSS_STRING_H

HYSS_FUNCTION(strspn);
HYSS_FUNCTION(strcspn);
HYSS_FUNCTION(str_replace);
HYSS_FUNCTION(str_ireplace);
HYSS_FUNCTION(rtrim);
HYSS_FUNCTION(trim);
HYSS_FUNCTION(ltrim);
HYSS_FUNCTION(soundex);
HYSS_FUNCTION(levenshtein);

HYSS_FUNCTION(count_chars);
HYSS_FUNCTION(wordwrap);
HYSS_FUNCTION(explode);
HYSS_FUNCTION(implode);
HYSS_FUNCTION(strtok);
HYSS_FUNCTION(strtoupper);
HYSS_FUNCTION(strtolower);
HYSS_FUNCTION(basename);
HYSS_FUNCTION(dirname);
HYSS_FUNCTION(pathinfo);
HYSS_FUNCTION(strstr);
HYSS_FUNCTION(strpos);
HYSS_FUNCTION(stripos);
HYSS_FUNCTION(strrpos);
HYSS_FUNCTION(strripos);
HYSS_FUNCTION(strrchr);
HYSS_FUNCTION(substr);
HYSS_FUNCTION(quotemeta);
HYSS_FUNCTION(ucfirst);
HYSS_FUNCTION(lcfirst);
HYSS_FUNCTION(ucwords);
HYSS_FUNCTION(strtr);
HYSS_FUNCTION(strrev);
HYSS_FUNCTION(hebrev);
HYSS_FUNCTION(hebrevc);
HYSS_FUNCTION(user_sprintf);
HYSS_FUNCTION(user_printf);
HYSS_FUNCTION(vprintf);
HYSS_FUNCTION(vsprintf);
HYSS_FUNCTION(addcslashes);
HYSS_FUNCTION(addslashes);
HYSS_FUNCTION(stripcslashes);
HYSS_FUNCTION(stripslashes);
HYSS_FUNCTION(chr);
HYSS_FUNCTION(ord);
HYSS_FUNCTION(nl2br);
HYSS_FUNCTION(setlocale);
HYSS_FUNCTION(localeconv);
HYSS_FUNCTION(nl_langinfo);
HYSS_FUNCTION(stristr);
HYSS_FUNCTION(chunk_split);
HYSS_FUNCTION(parse_str);
HYSS_FUNCTION(str_getcsv);
HYSS_FUNCTION(bin2hex);
HYSS_FUNCTION(hex2bin);
HYSS_FUNCTION(similar_text);
HYSS_FUNCTION(strip_tags);
HYSS_FUNCTION(str_repeat);
HYSS_FUNCTION(substr_replace);
HYSS_FUNCTION(strnatcmp);
HYSS_FUNCTION(strnatcasecmp);
HYSS_FUNCTION(substr_count);
HYSS_FUNCTION(str_pad);
HYSS_FUNCTION(sscanf);
HYSS_FUNCTION(str_shuffle);
HYSS_FUNCTION(str_word_count);
HYSS_FUNCTION(str_split);
HYSS_FUNCTION(strpbrk);
HYSS_FUNCTION(substr_compare);
HYSS_FUNCTION(utf8_encode);
HYSS_FUNCTION(utf8_decode);
#ifdef HAVE_STRCOLL
HYSS_FUNCTION(strcoll);
#endif
#if HAVE_STRFMON
HYSS_FUNCTION(money_format);
#endif

#if defined(HAVE_LOCALECONV) && defined(ZTS)
HYSS_MINIT_FUNCTION(localeconv);
HYSS_MSHUTDOWN_FUNCTION(localeconv);
#endif
#if HAVE_NL_LANGINFO
HYSS_MINIT_FUNCTION(nl_langinfo);
#endif
#if GEAR_INTRIN_SSE4_2_FUNC_PTR
HYSS_MINIT_FUNCTION(string_intrin);
#endif

#define strnatcmp(a, b) \
	strnatcmp_ex(a, strlen(a), b, strlen(b), 0)
#define strnatcasecmp(a, b) \
	strnatcmp_ex(a, strlen(a), b, strlen(b), 1)
HYSSAPI int strnatcmp_ex(char const *a, size_t a_len, char const *b, size_t b_len, int fold_case);

#ifdef HAVE_LOCALECONV
HYSSAPI struct lconv *localeconv_r(struct lconv *out);
#endif

HYSSAPI char *hyss_strtoupper(char *s, size_t len);
HYSSAPI char *hyss_strtolower(char *s, size_t len);
HYSSAPI gear_string *hyss_string_toupper(gear_string *s);
HYSSAPI gear_string *hyss_string_tolower(gear_string *s);
HYSSAPI char *hyss_strtr(char *str, size_t len, const char *str_from, const char *str_to, size_t trlen);
#if GEAR_INTRIN_SSE4_2_FUNC_PTR
HYSSAPI extern gear_string *(*hyss_addslashes)(gear_string *str);
HYSSAPI extern void (*hyss_stripslashes)(gear_string *str);
#else
HYSSAPI gear_string *hyss_addslashes(gear_string *str);
HYSSAPI void hyss_stripslashes(gear_string *str);
#endif
HYSSAPI gear_string *hyss_addcslashes_str(const char *str, size_t len, char *what, size_t what_len);
HYSSAPI gear_string *hyss_addcslashes(gear_string *str, char *what, size_t what_len);
HYSSAPI void hyss_stripcslashes(gear_string *str);
HYSSAPI gear_string *hyss_basename(const char *s, size_t len, char *suffix, size_t sufflen);
HYSSAPI size_t hyss_dirname(char *str, size_t len);
HYSSAPI char *hyss_stristr(char *s, char *t, size_t s_len, size_t t_len);
HYSSAPI gear_string *hyss_str_to_str(const char *haystack, size_t length, const char *needle,
		size_t needle_len, const char *str, size_t str_len);
HYSSAPI gear_string *hyss_trim(gear_string *str, char *what, size_t what_len, int mode);
HYSSAPI size_t hyss_strip_tags(char *rbuf, size_t len, uint8_t *state, const char *allow, size_t allow_len);
HYSSAPI size_t hyss_strip_tags_ex(char *rbuf, size_t len, uint8_t *stateptr, const char *allow, size_t allow_len, gear_bool allow_tag_spaces);
HYSSAPI void hyss_implode(const gear_string *delim, zval *arr, zval *return_value);
HYSSAPI void hyss_explode(const gear_string *delim, gear_string *str, zval *return_value, gear_long limit);

HYSSAPI size_t hyss_strspn(char *s1, char *s2, char *s1_end, char *s2_end);
HYSSAPI size_t hyss_strcspn(char *s1, char *s2, char *s1_end, char *s2_end);

HYSSAPI int string_natural_compare_function_ex(zval *result, zval *op1, zval *op2, gear_bool case_insensitive);
HYSSAPI int string_natural_compare_function(zval *result, zval *op1, zval *op2);
HYSSAPI int string_natural_case_compare_function(zval *result, zval *op1, zval *op2);

#ifndef HAVE_STRERROR
HYSSAPI char *hyss_strerror(int errnum);
#define strerror hyss_strerror
#endif

#ifndef HAVE_MBLEN
# define hyss_mblen(ptr, len) 1
# define hyss_mb_reset()
#elif defined(_REENTRANT) && defined(HAVE_MBRLEN) && defined(HAVE_MBSTATE_T)
# ifdef HYSS_WIN32
# include <wchar.h>
# endif
# define hyss_mblen(ptr, len) ((int) mbrlen(ptr, len, &BG(mblen_state)))
# define hyss_mb_reset() memset(&BG(mblen_state), 0, sizeof(BG(mblen_state)))
#else
# define hyss_mblen(ptr, len) mblen(ptr, len)
# define hyss_mb_reset() hyss_ignore_value(mblen(NULL, 0))
#endif

void register_string_constants(INIT_FUNC_ARGS);

#endif /* HYSS_STRING_H */
