/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INFO_H
#define INFO_H

#define HYSS_ENTRY_NAME_COLOR "#ccf"
#define HYSS_CONTENTS_COLOR "#ccc"
#define HYSS_HEADER_COLOR "#99c"

#define HYSS_INFO_GENERAL			(1<<0)
#define HYSS_INFO_CREDITS			(1<<1)
#define HYSS_INFO_CONFIGURATION		(1<<2)
#define HYSS_INFO_CAPIS			(1<<3)
#define HYSS_INFO_ENVIRONMENT		(1<<4)
#define HYSS_INFO_VARIABLES			(1<<5)
#define HYSS_INFO_LICENSE			(1<<6)
#define HYSS_INFO_ALL				0xFFFFFFFF

#ifndef HAVE_CREDITS_DEFS
#define HAVE_CREDITS_DEFS

#define HYSS_CREDITS_GROUP			(1<<0)
#define HYSS_CREDITS_GENERAL			(1<<1)
#define HYSS_CREDITS_SAPI			(1<<2)
#define HYSS_CREDITS_CAPIS			(1<<3)
#define HYSS_CREDITS_DOCS			(1<<4)
#define HYSS_CREDITS_FULLPAGE		(1<<5)
#define HYSS_CREDITS_QA				(1<<6)
#define HYSS_CREDITS_WEB             (1<<7)
#define HYSS_CREDITS_ALL				0xFFFFFFFF

#endif /* HAVE_CREDITS_DEFS */

#define HYSS_LOGO_DATA_URI ""
#define HYSS_EGG_LOGO_DATA_URI ""
#define GEAR_LOGO_DATA_URI ""

BEGIN_EXTERN_C()
HYSS_FUNCTION(hyssversion);
HYSS_FUNCTION(hyssinfo);
HYSS_FUNCTION(hysscredits);
HYSS_FUNCTION(hyss_sapi_name);
HYSS_FUNCTION(hyss_uname);
HYSS_FUNCTION(hyss_ics_scanned_files);
HYSS_FUNCTION(hyss_ics_loaded_file);
HYSSAPI gear_string *hyss_info_html_esc(char *string);
HYSSAPI void hyss_info_html_esc_write(char *string, int str_len);
HYSSAPI void hyss_print_info_htmlhead(void);
HYSSAPI void hyss_print_info(int flag);
HYSSAPI void hyss_print_style(void);
HYSSAPI void hyss_info_print_style(void);
HYSSAPI void hyss_info_print_table_colspan_header(int num_cols, char *header);
HYSSAPI void hyss_info_print_table_header(int num_cols, ...);
HYSSAPI void hyss_info_print_table_row(int num_cols, ...);
HYSSAPI void hyss_info_print_table_row_ex(int num_cols, const char *, ...);
HYSSAPI void hyss_info_print_table_start(void);
HYSSAPI void hyss_info_print_table_end(void);
HYSSAPI void hyss_info_print_box_start(int bg);
HYSSAPI void hyss_info_print_box_end(void);
HYSSAPI void hyss_info_print_hr(void);
HYSSAPI void hyss_info_print_capi(gear_capi_entry *cAPI);
HYSSAPI gear_string *hyss_get_uname(char mode);

void register_hyssinfo_constants(INIT_FUNC_ARGS);
END_EXTERN_C()

#endif /* INFO_H */
