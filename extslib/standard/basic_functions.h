/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASIC_FUNCTIONS_H
#define BASIC_FUNCTIONS_H

#include <sys/stat.h>

#ifdef HAVE_WCHAR_H
#include <wchar.h>
#endif

#include "hyss_filestat.h"

#include "gear_highlight.h"

#include "url_scanner_ex.h"

#if defined(_WIN32) && !defined(__clang__)
#include <intrin.h>
#endif

extern gear_capi_entry basic_functions_capi;
#define basic_functions_capi_ptr &basic_functions_capi

HYSS_MINIT_FUNCTION(basic);
HYSS_MSHUTDOWN_FUNCTION(basic);
HYSS_RINIT_FUNCTION(basic);
HYSS_RSHUTDOWN_FUNCTION(basic);
HYSS_MINFO_FUNCTION(basic);

HYSS_FUNCTION(constant);
HYSS_FUNCTION(sleep);
HYSS_FUNCTION(usleep);
#if HAVE_NANOSLEEP
HYSS_FUNCTION(time_nanosleep);
HYSS_FUNCTION(time_sleep_until);
#endif
HYSS_FUNCTION(flush);
#ifdef HAVE_INET_NTOP
HYSS_NAMED_FUNCTION(zif_inet_ntop);
#endif
#ifdef HAVE_INET_PTON
HYSS_NAMED_FUNCTION(hyss_inet_pton);
#endif
HYSS_FUNCTION(ip2long);
HYSS_FUNCTION(long2ip);

/* system functions */
HYSS_FUNCTION(getenv);
HYSS_FUNCTION(putenv);

HYSS_FUNCTION(getopt);

HYSS_FUNCTION(get_current_user);
HYSS_FUNCTION(set_time_limit);

HYSS_FUNCTION(header_register_callback);

HYSS_FUNCTION(get_cfg_var);
HYSS_FUNCTION(get_magic_quotes_runtime);
HYSS_FUNCTION(get_magic_quotes_gpc);

HYSS_FUNCTION(error_log);
HYSS_FUNCTION(error_get_last);
HYSS_FUNCTION(error_clear_last);

HYSS_FUNCTION(call_user_func);
HYSS_FUNCTION(call_user_func_array);
HYSS_FUNCTION(forward_static_call);
HYSS_FUNCTION(forward_static_call_array);

HYSS_FUNCTION(register_shutdown_function);
HYSS_FUNCTION(highlight_file);
HYSS_FUNCTION(highlight_string);
HYSS_FUNCTION(hyss_strip_whitespace);
GEAR_API void hyss_get_highlight_struct(gear_syntax_highlighter_ics *syntax_highlighter_ics);

HYSS_FUNCTION(ics_get);
HYSS_FUNCTION(ics_get_all);
HYSS_FUNCTION(ics_set);
HYSS_FUNCTION(ics_restore);
HYSS_FUNCTION(get_include_path);
HYSS_FUNCTION(set_include_path);
HYSS_FUNCTION(restore_include_path);

HYSS_FUNCTION(print_r);
HYSS_FUNCTION(fprintf);
HYSS_FUNCTION(vfprintf);

HYSS_FUNCTION(connection_aborted);
HYSS_FUNCTION(connection_status);
HYSS_FUNCTION(ignore_user_abort);

HYSS_FUNCTION(getservbyname);
HYSS_FUNCTION(getservbyport);
HYSS_FUNCTION(getprotobyname);
HYSS_FUNCTION(getprotobynumber);

HYSS_NAMED_FUNCTION(hyss_if_crc32);

HYSS_FUNCTION(register_tick_function);
HYSS_FUNCTION(unregister_tick_function);
#ifdef HAVE_GETLOADAVG
HYSS_FUNCTION(sys_getloadavg);
#endif

HYSS_FUNCTION(is_uploaded_file);
HYSS_FUNCTION(move_uploaded_file);

HYSS_FUNCTION(net_get_interfaces);

/* From the ICS parser */
HYSS_FUNCTION(parse_ics_file);
HYSS_FUNCTION(parse_ics_string);
#if GEAR_DEBUG
HYSS_FUNCTION(config_get_hash);
#endif

#if defined(HYSS_WIN32)
HYSS_FUNCTION(sapi_windows_cp_set);
HYSS_FUNCTION(sapi_windows_cp_get);
HYSS_FUNCTION(sapi_windows_cp_is_utf8);
HYSS_FUNCTION(sapi_windows_cp_conv);
#endif

HYSS_FUNCTION(str_rot13);
HYSS_FUNCTION(stream_get_filters);
HYSS_FUNCTION(stream_filter_register);
HYSS_FUNCTION(stream_bucket_make_writeable);
HYSS_FUNCTION(stream_bucket_prepend);
HYSS_FUNCTION(stream_bucket_append);
HYSS_FUNCTION(stream_bucket_new);
HYSS_MINIT_FUNCTION(user_filters);
HYSS_RSHUTDOWN_FUNCTION(user_filters);
HYSS_RSHUTDOWN_FUNCTION(browscap);

/* Left for BC (not binary safe!) */
HYSSAPI int _hyss_error_log(int opt_err, char *message, char *opt, char *headers);
HYSSAPI int _hyss_error_log_ex(int opt_err, char *message, size_t message_len, char *opt, char *headers);
HYSSAPI int hyss_prefix_varname(zval *result, const zval *prefix, const char *var_name, size_t var_name_len, gear_bool add_underscore);

#define MT_N (624)

/* Deprecated type aliases -- use the standard types instead */
typedef uint32_t hyss_uint32;
typedef int32_t hyss_int32;

typedef struct _hyss_basic_globals {
	HashTable *user_shutdown_function_names;
	HashTable putenv_ht;
	zval  strtok_zval;
	char *strtok_string;
	gear_string *locale_string; /* current LC_CTYPE locale (or NULL for 'C') */
	gear_bool locale_changed;   /* locale was changed and has to be restored */
	char *strtok_last;
	char strtok_table[256];
	gear_ulong strtok_len;
	char str_ebuf[40];
	gear_fcall_info array_walk_fci;
	gear_fcall_info_cache array_walk_fci_cache;
	gear_fcall_info user_compare_fci;
	gear_fcall_info_cache user_compare_fci_cache;
	gear_llist *user_tick_functions;

	zval active_ics_file_section;

	/* pageinfo.c */
	gear_long page_uid;
	gear_long page_gid;
	gear_long page_inode;
	time_t page_mtime;

	/* filestat.c && main/streams/streams.c */
	char *CurrentStatFile, *CurrentLStatFile;
	hyss_stream_statbuf ssb, lssb;

	/* mt_rand.c */
	uint32_t state[MT_N+1];  /* state vector + 1 extra to not violate ANSI C */
	uint32_t *next;       /* next random value is computed from here */
	int      left;        /* can *next++ this many times before reloading */

	gear_bool mt_rand_is_seeded; /* Whether mt_rand() has been seeded */
	gear_long mt_rand_mode;

	/* syslog.c */
	char *syslog_device;

	/* var.c */
	gear_class_entry *incomplete_class;
	unsigned serialize_lock; /* whether to use the locally supplied var_hash instead (__sleep/__wakeup) */
	struct {
		struct hyss_serialize_data *data;
		unsigned level;
	} serialize;
	struct {
		struct hyss_unserialize_data *data;
		unsigned level;
	} unserialize;

	/* url_scanner_ex.re */
	url_adapt_state_ex_t url_adapt_session_ex;
	HashTable url_adapt_session_hosts_ht;
	url_adapt_state_ex_t url_adapt_output_ex;
	HashTable url_adapt_output_hosts_ht;

#ifdef HAVE_MMAP
	void *mmap_file;
	size_t mmap_len;
#endif

	HashTable *user_filter_map;

	/* file.c */
#if defined(_REENTRANT) && defined(HAVE_MBRLEN) && defined(HAVE_MBSTATE_T)
	mbstate_t mblen_state;
#endif

	int umask;
} hyss_basic_globals;

#ifdef ZTS
#define BG(v) GEAR_PBCG(basic_globals_id, hyss_basic_globals *, v)
HYSSAPI extern int basic_globals_id;
#else
#define BG(v) (basic_globals.v)
HYSSAPI extern hyss_basic_globals basic_globals;
#endif

#if HAVE_PUTENV
typedef struct {
	char *putenv_string;
	char *previous_value;
	char *key;
	size_t key_len;
} putenv_entry;
#endif

HYSSAPI double hyss_get_nan(void);
HYSSAPI double hyss_get_inf(void);

typedef struct _hyss_shutdown_function_entry {
	zval *arguments;
	int arg_count;
} hyss_shutdown_function_entry;

HYSSAPI extern gear_bool register_user_shutdown_function(char *function_name, size_t function_len, hyss_shutdown_function_entry *shutdown_function_entry);
HYSSAPI extern gear_bool remove_user_shutdown_function(char *function_name, size_t function_len);
HYSSAPI extern gear_bool append_user_shutdown_function(hyss_shutdown_function_entry shutdown_function_entry);

HYSSAPI void hyss_call_shutdown_functions(void);
HYSSAPI void hyss_free_shutdown_functions(void);


#endif /* BASIC_FUNCTIONS_H */
