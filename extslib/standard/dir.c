/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes/startup/misc */

#include "hyss.h"
#include "fopen_wrappers.h"
#include "file.h"
#include "hyss_dir.h"
#include "hyss_string.h"
#include "hyss_scandir.h"
#include "basic_functions.h"

#ifdef HAVE_DIRENT_H
#include <dirent.h>
#endif

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <errno.h>

#ifdef HYSS_WIN32
#include "win32/readdir.h"
#endif


#ifdef HAVE_GLOB
#ifndef HYSS_WIN32
#include <glob.h>
#else
#include "win32/glob.h"
#endif
#endif

typedef struct {
	gear_resource *default_dir;
} hyss_dir_globals;

#ifdef ZTS
#define DIRG(v) GEAR_PBCG(dir_globals_id, hyss_dir_globals *, v)
int dir_globals_id;
#else
#define DIRG(v) (dir_globals.v)
hyss_dir_globals dir_globals;
#endif

static gear_class_entry *dir_class_entry_ptr;

#define FETCH_DIRP() \
	GEAR_PARSE_PARAMETERS_START(0, 1) \
		Z_PARAM_OPTIONAL \
		Z_PARAM_RESOURCE(id) \
	GEAR_PARSE_PARAMETERS_END(); \
	if (GEAR_NUM_ARGS() == 0) { \
		myself = getThis(); \
		if (myself) { \
			if ((tmp = gear_hash_str_find(Z_OBJPROP_P(myself), "handle", sizeof("handle")-1)) == NULL) { \
				hyss_error_docref(NULL, E_WARNING, "Unable to find my handle property"); \
				RETURN_FALSE; \
			} \
			if ((dirp = (hyss_stream *)gear_fetch_resource_ex(tmp, "Directory", hyss_file_le_stream())) == NULL) { \
				RETURN_FALSE; \
			} \
		} else { \
			if (!DIRG(default_dir) || \
				(dirp = (hyss_stream *)gear_fetch_resource(DIRG(default_dir), "Directory", hyss_file_le_stream())) == NULL) { \
				RETURN_FALSE; \
			} \
		} \
	} else { \
		if ((dirp = (hyss_stream *)gear_fetch_resource(Z_RES_P(id), "Directory", hyss_file_le_stream())) == NULL) { \
			RETURN_FALSE; \
		} \
	}

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_dir, 0, 0, 0)
	GEAR_ARG_INFO(0, dir_handle)
GEAR_END_ARG_INFO()
/* }}} */

static const gear_function_entry hyss_dir_class_functions[] = {
	HYSS_FALIAS(close,	closedir,		arginfo_dir)
	HYSS_FALIAS(rewind,	rewinddir,		arginfo_dir)
	HYSS_NAMED_FE(read,  hyss_if_readdir, arginfo_dir)
	HYSS_FE_END
};


static void hyss_set_default_dir(gear_resource *res)
{
	if (DIRG(default_dir)) {
		gear_list_delete(DIRG(default_dir));
	}

	if (res) {
		GC_ADDREF(res);
	}

	DIRG(default_dir) = res;
}

HYSS_RINIT_FUNCTION(dir)
{
	DIRG(default_dir) = NULL;
	return SUCCESS;
}

HYSS_MINIT_FUNCTION(dir)
{
	static char dirsep_str[2], pathsep_str[2];
	gear_class_entry dir_class_entry;

	INIT_CLASS_ENTRY(dir_class_entry, "Directory", hyss_dir_class_functions);
	dir_class_entry_ptr = gear_register_internal_class(&dir_class_entry);

#ifdef ZTS
	ts_allocate_id(&dir_globals_id, sizeof(hyss_dir_globals), NULL, NULL);
#endif

	dirsep_str[0] = DEFAULT_SLASH;
	dirsep_str[1] = '\0';
	REGISTER_STRING_CONSTANT("DIRECTORY_SEPARATOR", dirsep_str, CONST_CS|CONST_PERSISTENT);

	pathsep_str[0] = GEAR_PATHS_SEPARATOR;
	pathsep_str[1] = '\0';
	REGISTER_STRING_CONSTANT("PATH_SEPARATOR", pathsep_str, CONST_CS|CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("SCANDIR_SORT_ASCENDING",  HYSS_SCANDIR_SORT_ASCENDING,  CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("SCANDIR_SORT_DESCENDING", HYSS_SCANDIR_SORT_DESCENDING, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("SCANDIR_SORT_NONE",       HYSS_SCANDIR_SORT_NONE,       CONST_CS | CONST_PERSISTENT);

#ifdef HAVE_GLOB

#ifdef GLOB_BRACE
	REGISTER_LONG_CONSTANT("GLOB_BRACE", GLOB_BRACE, CONST_CS | CONST_PERSISTENT);
#else
# define GLOB_BRACE 0
#endif

#ifdef GLOB_MARK
	REGISTER_LONG_CONSTANT("GLOB_MARK", GLOB_MARK, CONST_CS | CONST_PERSISTENT);
#else
# define GLOB_MARK 0
#endif

#ifdef GLOB_NOSORT
	REGISTER_LONG_CONSTANT("GLOB_NOSORT", GLOB_NOSORT, CONST_CS | CONST_PERSISTENT);
#else
# define GLOB_NOSORT 0
#endif

#ifdef GLOB_NOCHECK
	REGISTER_LONG_CONSTANT("GLOB_NOCHECK", GLOB_NOCHECK, CONST_CS | CONST_PERSISTENT);
#else
# define GLOB_NOCHECK 0
#endif

#ifdef GLOB_NOESCAPE
	REGISTER_LONG_CONSTANT("GLOB_NOESCAPE", GLOB_NOESCAPE, CONST_CS | CONST_PERSISTENT);
#else
# define GLOB_NOESCAPE 0
#endif

#ifdef GLOB_ERR
	REGISTER_LONG_CONSTANT("GLOB_ERR", GLOB_ERR, CONST_CS | CONST_PERSISTENT);
#else
# define GLOB_ERR 0
#endif

#ifndef GLOB_ONLYDIR
# define GLOB_ONLYDIR (1<<30)
# define GLOB_EMULATE_ONLYDIR
# define GLOB_FLAGMASK (~GLOB_ONLYDIR)
#else
# define GLOB_FLAGMASK (~0)
#endif

/* This is used for checking validity of passed flags (passing invalid flags causes segfault in glob()!! */
#define GLOB_AVAILABLE_FLAGS (0 | GLOB_BRACE | GLOB_MARK | GLOB_NOSORT | GLOB_NOCHECK | GLOB_NOESCAPE | GLOB_ERR | GLOB_ONLYDIR)

	REGISTER_LONG_CONSTANT("GLOB_ONLYDIR", GLOB_ONLYDIR, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("GLOB_AVAILABLE_FLAGS", GLOB_AVAILABLE_FLAGS, CONST_CS | CONST_PERSISTENT);

#endif /* HAVE_GLOB */

	return SUCCESS;
}
/* }}} */

/* {{{ internal functions */
static void _hyss_do_opendir(INTERNAL_FUNCTION_PARAMETERS, int createobject)
{
	char *dirname;
	size_t dir_len;
	zval *zcontext = NULL;
	hyss_stream_context *context = NULL;
	hyss_stream *dirp;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_PATH(dirname, dir_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_RESOURCE(zcontext)
	GEAR_PARSE_PARAMETERS_END();

	context = hyss_stream_context_from_zval(zcontext, 0);

	dirp = hyss_stream_opendir(dirname, REPORT_ERRORS, context);

	if (dirp == NULL) {
		RETURN_FALSE;
	}

	dirp->flags |= HYSS_STREAM_FLAG_NO_FCLOSE;

	hyss_set_default_dir(dirp->res);

	if (createobject) {
		object_init_ex(return_value, dir_class_entry_ptr);
		add_property_stringl(return_value, "path", dirname, dir_len);
		add_property_resource(return_value, "handle", dirp->res);
		hyss_stream_auto_cleanup(dirp); /* so we don't get warnings under debug */
	} else {
		hyss_stream_to_zval(dirp, return_value);
	}
}
/* }}} */

/* {{{ proto mixed opendir(string path[, resource context])
   Open a directory and return a dir_handle */
HYSS_FUNCTION(opendir)
{
	_hyss_do_opendir(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto object dir(string directory[, resource context])
   Directory class with properties, handle and class and methods read, rewind and close */
HYSS_FUNCTION(getdir)
{
	_hyss_do_opendir(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto void closedir([resource dir_handle])
   Close directory connection identified by the dir_handle */
HYSS_FUNCTION(closedir)
{
	zval *id = NULL, *tmp, *myself;
	hyss_stream *dirp;
	gear_resource *res;

	FETCH_DIRP();

	if (!(dirp->flags & HYSS_STREAM_FLAG_IS_DIR)) {
		hyss_error_docref(NULL, E_WARNING, "%d is not a valid Directory resource", dirp->res->handle);
		RETURN_FALSE;
	}

	res = dirp->res;
	gear_list_close(dirp->res);

	if (res == DIRG(default_dir)) {
		hyss_set_default_dir(NULL);
	}
}
/* }}} */

#if defined(HAVE_CHROOT) && !defined(ZTS) && ENABLE_CHROOT_FUNC
/* {{{ proto bool chroot(string directory)
   Change root directory */
HYSS_FUNCTION(chroot)
{
	char *str;
	int ret;
	size_t str_len;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_PATH(str, str_len)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	ret = chroot(str);
	if (ret != 0) {
		hyss_error_docref(NULL, E_WARNING, "%s (errno %d)", strerror(errno), errno);
		RETURN_FALSE;
	}

	hyss_clear_stat_cache(1, NULL, 0);

	ret = chdir("/");

	if (ret != 0) {
		hyss_error_docref(NULL, E_WARNING, "%s (errno %d)", strerror(errno), errno);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */
#endif

/* {{{ proto bool chdir(string directory)
   Change the current directory */
HYSS_FUNCTION(chdir)
{
	char *str;
	int ret;
	size_t str_len;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_PATH(str, str_len)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (hyss_check_open_basedir(str)) {
		RETURN_FALSE;
	}
	ret = VCWD_CHDIR(str);

	if (ret != 0) {
		hyss_error_docref(NULL, E_WARNING, "%s (errno %d)", strerror(errno), errno);
		RETURN_FALSE;
	}

	if (BG(CurrentStatFile) && !IS_ABSOLUTE_PATH(BG(CurrentStatFile), strlen(BG(CurrentStatFile)))) {
		efree(BG(CurrentStatFile));
		BG(CurrentStatFile) = NULL;
	}
	if (BG(CurrentLStatFile) && !IS_ABSOLUTE_PATH(BG(CurrentLStatFile), strlen(BG(CurrentLStatFile)))) {
		efree(BG(CurrentLStatFile));
		BG(CurrentLStatFile) = NULL;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto mixed getcwd(void)
   Gets the current directory */
HYSS_FUNCTION(getcwd)
{
	char path[MAXPATHLEN];
	char *ret=NULL;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

#if HAVE_GETCWD
	ret = VCWD_GETCWD(path, MAXPATHLEN);
#elif HAVE_GETWD
	ret = VCWD_GETWD(path);
#endif

	if (ret) {
		RETURN_STRING(path);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto void rewinddir([resource dir_handle])
   Rewind dir_handle back to the start */
HYSS_FUNCTION(rewinddir)
{
	zval *id = NULL, *tmp, *myself;
	hyss_stream *dirp;

	FETCH_DIRP();

	if (!(dirp->flags & HYSS_STREAM_FLAG_IS_DIR)) {
		hyss_error_docref(NULL, E_WARNING, "%d is not a valid Directory resource", dirp->res->handle);
		RETURN_FALSE;
	}

	hyss_stream_rewinddir(dirp);
}
/* }}} */

/* {{{ proto string readdir([resource dir_handle])
   Read directory entry from dir_handle */
HYSS_NAMED_FUNCTION(hyss_if_readdir)
{
	zval *id = NULL, *tmp, *myself;
	hyss_stream *dirp;
	hyss_stream_dirent entry;

	FETCH_DIRP();

	if (!(dirp->flags & HYSS_STREAM_FLAG_IS_DIR)) {
		hyss_error_docref(NULL, E_WARNING, "%d is not a valid Directory resource", dirp->res->handle);
		RETURN_FALSE;
	}

	if (hyss_stream_readdir(dirp, &entry)) {
		RETURN_STRINGL(entry.d_name, strlen(entry.d_name));
	}
	RETURN_FALSE;
}
/* }}} */

#ifdef HAVE_GLOB
/* {{{ proto array glob(string pattern [, int flags])
   Find pathnames matching a pattern */
HYSS_FUNCTION(glob)
{
	size_t cwd_skip = 0;
#ifdef ZTS
	char cwd[MAXPATHLEN];
	char work_pattern[MAXPATHLEN];
	char *result;
#endif
	char *pattern = NULL;
	size_t pattern_len;
	gear_long flags = 0;
	glob_t globbuf;
	size_t n;
	int ret;
	gear_bool basedir_limit = 0;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_PATH(pattern, pattern_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(flags)
	GEAR_PARSE_PARAMETERS_END();

	if (pattern_len >= MAXPATHLEN) {
		hyss_error_docref(NULL, E_WARNING, "Pattern exceeds the maximum allowed length of %d characters", MAXPATHLEN);
		RETURN_FALSE;
	}

	if ((GLOB_AVAILABLE_FLAGS & flags) != flags) {
		hyss_error_docref(NULL, E_WARNING, "At least one of the passed flags is invalid or not supported on this platform");
		RETURN_FALSE;
	}

#ifdef ZTS
	if (!IS_ABSOLUTE_PATH(pattern, pattern_len)) {
		result = VCWD_GETCWD(cwd, MAXPATHLEN);
		if (!result) {
			cwd[0] = '\0';
		}
#ifdef HYSS_WIN32
		if (IS_SLASH(*pattern)) {
			cwd[2] = '\0';
		}
#endif
		cwd_skip = strlen(cwd)+1;

		snprintf(work_pattern, MAXPATHLEN, "%s%c%s", cwd, DEFAULT_SLASH, pattern);
		pattern = work_pattern;
	}
#endif


	memset(&globbuf, 0, sizeof(glob_t));
	globbuf.gl_offs = 0;
	if (0 != (ret = glob(pattern, flags & GLOB_FLAGMASK, NULL, &globbuf))) {
#ifdef GLOB_NOMATCH
		if (GLOB_NOMATCH == ret) {
			/* Some glob implementation simply return no data if no matches
			   were found, others return the GLOB_NOMATCH error code.
			   We don't want to treat GLOB_NOMATCH as an error condition
			   so that HYSS glob() behaves the same on both types of
			   implementations and so that 'foreach (glob() as ...'
			   can be used for simple glob() calls without further error
			   checking.
			*/
			goto no_results;
		}
#endif
		RETURN_FALSE;
	}

	/* now catch the FreeBSD style of "no matches" */
	if (!globbuf.gl_pathc || !globbuf.gl_pathv) {
#ifdef GLOB_NOMATCH
no_results:
#endif
#ifndef HYSS_WIN32
		/* Paths containing '*', '?' and some other chars are
		illegal on Windows but legit on other platforms. For
		this reason the direct basedir check against the glob
		query is senseless on windows. For instance while *.txt
		is a pretty valid filename on EXT3, it's invalid on NTFS. */
		if (PG(open_basedir) && *PG(open_basedir)) {
			if (hyss_check_open_basedir_ex(pattern, 0)) {
				RETURN_FALSE;
			}
		}
#endif
		array_init(return_value);
		return;
	}

	array_init(return_value);
	for (n = 0; n < (size_t)globbuf.gl_pathc; n++) {
		if (PG(open_basedir) && *PG(open_basedir)) {
			if (hyss_check_open_basedir_ex(globbuf.gl_pathv[n], 0)) {
				basedir_limit = 1;
				continue;
			}
		}
		/* we need to do this every time since GLOB_ONLYDIR does not guarantee that
		 * all directories will be filtered. GNU libc documentation states the
		 * following:
		 * If the information about the type of the file is easily available
		 * non-directories will be rejected but no extra work will be done to
		 * determine the information for each file. I.e., the caller must still be
		 * able to filter directories out.
		 */
		if (flags & GLOB_ONLYDIR) {
			gear_stat_t s;

			if (0 != VCWD_STAT(globbuf.gl_pathv[n], &s)) {
				continue;
			}

			if (S_IFDIR != (s.st_mode & S_IFMT)) {
				continue;
			}
		}
		add_next_index_string(return_value, globbuf.gl_pathv[n]+cwd_skip);
	}

	globfree(&globbuf);

	if (basedir_limit && !gear_hash_num_elements(Z_ARRVAL_P(return_value))) {
		gear_array_destroy(Z_ARR_P(return_value));
		RETURN_FALSE;
	}
}
/* }}} */
#endif

/* {{{ proto array scandir(string dir [, int sorting_order [, resource context]])
   List files & directories inside the specified path */
HYSS_FUNCTION(scandir)
{
	char *dirn;
	size_t dirn_len;
	gear_long flags = 0;
	gear_string **namelist;
	int n, i;
	zval *zcontext = NULL;
	hyss_stream_context *context = NULL;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_PATH(dirn, dirn_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(flags)
		Z_PARAM_RESOURCE(zcontext)
	GEAR_PARSE_PARAMETERS_END();

	if (dirn_len < 1) {
		hyss_error_docref(NULL, E_WARNING, "Directory name cannot be empty");
		RETURN_FALSE;
	}

	if (zcontext) {
		context = hyss_stream_context_from_zval(zcontext, 0);
	}

	if (flags == HYSS_SCANDIR_SORT_ASCENDING) {
		n = hyss_stream_scandir(dirn, &namelist, context, (void *) hyss_stream_dirent_alphasort);
	} else if (flags == HYSS_SCANDIR_SORT_NONE) {
		n = hyss_stream_scandir(dirn, &namelist, context, NULL);
	} else {
		n = hyss_stream_scandir(dirn, &namelist, context, (void *) hyss_stream_dirent_alphasortr);
	}
	if (n < 0) {
		hyss_error_docref(NULL, E_WARNING, "(errno %d): %s", errno, strerror(errno));
		RETURN_FALSE;
	}

	array_init(return_value);

	for (i = 0; i < n; i++) {
		add_next_index_str(return_value, namelist[i]);
	}

	if (n) {
		efree(namelist);
	}
}
/* }}} */

