/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "basic_functions.h"
#include "hyss_math.h"
#include "hyss_string.h"
#include "base64.h"
#include "hyss_dir.h"
#include "hyss_dns.h"
#include "hyss_mail.h"
#include "md5.h"
#include "sha1.h"
#include "hrtime.h"
#include "html.h"
#include "exec.h"
#include "file.h"
#include "hyss_ext_syslog.h"
#include "hyss_filestat.h"
#include "hyss_browscap.h"
#include "pack.h"
#include "datetime.h"
#include "microtime.h"
#include "url.h"
#include "pageinfo.h"
#include "cyr_convert.h"
#include "hyss_link.h"
#include "fsock.h"
#include "hyss_image.h"
#include "hyss_iptc.h"
#include "info.h"
#include "uniqid.h"
#include "hyss_var.h"
#include "quot_print.h"
#include "dl.h"
#include "hyss_crypt.h"
#include "head.h"
#include "hyss_lcg.h"
#include "hyss_metaphone.h"
#include "hyss_output.h"
#include "hyss_array.h"
#include "hyss_assert.h"
#include "hyss_versioning.h"
#include "hyss_ftok.h"
#include "hyss_type.h"
#include "hyss_password.h"
#include "hyss_random.h"

#include "hyss_version.h"
#define HYSS_STANDARD_VERSION HYSS_VERSION

#define hyssext_standard_ptr basic_functions_capi_ptr
HYSS_MINIT_FUNCTION(standard_filters);
HYSS_MSHUTDOWN_FUNCTION(standard_filters);

