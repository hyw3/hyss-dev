/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QUOT_PRINT_H
#define QUOT_PRINT_H

HYSSAPI gear_string *hyss_quot_print_decode(const unsigned char *str, size_t length, int replace_us_by_ws);
HYSSAPI gear_string *hyss_quot_print_encode(const unsigned char *str, size_t length);

HYSS_FUNCTION(quoted_printable_decode);
HYSS_FUNCTION(quoted_printable_encode);

#endif /* QUOT_PRINT_H */
