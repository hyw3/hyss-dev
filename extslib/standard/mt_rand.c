/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_rand.h"
#include "hyss_mt_rand.h"

/* MT RAND FUNCTIONS */

/*
	The following hyss_mt_...() functions are based on a C++ class MTRand by
	Richard J. Wagner. For more information see the web page at
	http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/VERSIONS/C-LANG/MersenneTwister.h

	Mersenne Twister random number generator -- a C++ class MTRand
	Based on code by Makoto Matsumoto, Takuji Nishimura, and Shawn Cokus
	Richard J. Wagner  v1.0  15 May 2003  rjwagner@writeme.com

	The Mersenne Twister is an algorithm for generating random numbers.  It
	was designed with consideration of the flaws in various other generators.
	The period, 2^19937-1, and the order of equidistribution, 623 dimensions,
	are far greater.  The generator is also fast; it avoids multiplication and
	division, and it benefits from caches and pipelines.  For more information
	see the inventors' web page at http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html

	Reference
	M. Matsumoto and T. Nishimura, "Mersenne Twister: A 623-Dimensionally
	Equidistributed Uniform Pseudo-Random Number Generator", ACM Transactions on
	Modeling and Computer Simulation, Vol. 8, No. 1, January 1998, pp 3-30.

	Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
	Copyright (C) 2000 - 2003, Richard J. Wagner
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions
	are met:

	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.

	3. The names of its contributors may not be used to endorse or promote
	   products derived from this software without specific prior written
	   permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
	LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
	A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
	EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
	PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define N             MT_N                 /* length of state vector */
#define M             (397)                /* a period parameter */
#define hiBit(u)      ((u) & 0x80000000U)  /* mask all but highest   bit of u */
#define loBit(u)      ((u) & 0x00000001U)  /* mask all but lowest    bit of u */
#define loBits(u)     ((u) & 0x7FFFFFFFU)  /* mask     the highest   bit of u */
#define mixBits(u, v) (hiBit(u)|loBits(v)) /* move hi bit of u to hi bit of v */

#define twist(m,u,v)  (m ^ (mixBits(u,v)>>1) ^ ((uint32_t)(-(int32_t)(loBit(v))) & 0x9908b0dfU))
#define twist_hyss(m,u,v)  (m ^ (mixBits(u,v)>>1) ^ ((uint32_t)(-(int32_t)(loBit(u))) & 0x9908b0dfU))

/* {{{ hyss_mt_initialize
 */
static inline void hyss_mt_initialize(uint32_t seed, uint32_t *state)
{
	/* Initialize generator state with seed
	   See Knuth TAOCP Vol 2, 3rd Ed, p.106 for multiplier.
	   In previous versions, most significant bits (MSBs) of the seed affect
	   only MSBs of the state array.  Modified 9 Jan 2002 by Makoto Matsumoto. */

	register uint32_t *s = state;
	register uint32_t *r = state;
	register int i = 1;

	*s++ = seed & 0xffffffffU;
	for( ; i < N; ++i ) {
		*s++ = ( 1812433253U * ( *r ^ (*r >> 30) ) + i ) & 0xffffffffU;
		r++;
	}
}
/* }}} */

/* {{{ hyss_mt_reload
 */
static inline void hyss_mt_reload(void)
{
	/* Generate N new values in state
	   Made clearer and faster by Matthew Bellew (matthew.bellew@home.com) */

	register uint32_t *state = BG(state);
	register uint32_t *p = state;
	register int i;

	if (BG(mt_rand_mode) == MT_RAND_MT19937) {
		for (i = N - M; i--; ++p)
			*p = twist(p[M], p[0], p[1]);
		for (i = M; --i; ++p)
			*p = twist(p[M-N], p[0], p[1]);
		*p = twist(p[M-N], p[0], state[0]);
	}
	else {
		for (i = N - M; i--; ++p)
			*p = twist_hyss(p[M], p[0], p[1]);
		for (i = M; --i; ++p)
			*p = twist_hyss(p[M-N], p[0], p[1]);
		*p = twist_hyss(p[M-N], p[0], state[0]);
	}
	BG(left) = N;
	BG(next) = state;
}
/* }}} */

/* {{{ hyss_mt_srand
 */
HYSSAPI void hyss_mt_srand(uint32_t seed)
{
	/* Seed the generator with a simple uint32 */
	hyss_mt_initialize(seed, BG(state));
	hyss_mt_reload();

	/* Seed only once */
	BG(mt_rand_is_seeded) = 1;
}
/* }}} */

/* {{{ hyss_mt_rand
 */
HYSSAPI uint32_t hyss_mt_rand(void)
{
	/* Pull a 32-bit integer from the generator state
	   Every other access function simply transforms the numbers extracted here */

	register uint32_t s1;

	if (UNEXPECTED(!BG(mt_rand_is_seeded))) {
		hyss_mt_srand(GENERATE_SEED());
	}

	if (BG(left) == 0) {
		hyss_mt_reload();
	}
	--BG(left);

	s1 = *BG(next)++;
	s1 ^= (s1 >> 11);
	s1 ^= (s1 <<  7) & 0x9d2c5680U;
	s1 ^= (s1 << 15) & 0xefc60000U;
	return ( s1 ^ (s1 >> 18) );
}
/* }}} */

/* {{{ proto void mt_srand([int seed])
   Seeds Mersenne Twister random number generator */
HYSS_FUNCTION(mt_srand)
{
	gear_long seed = 0;
	gear_long mode = MT_RAND_MT19937;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(seed)
		Z_PARAM_LONG(mode)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 0)
		seed = GENERATE_SEED();

	switch (mode) {
		case MT_RAND_HYSS:
			BG(mt_rand_mode) = MT_RAND_HYSS;
			break;
		default:
			BG(mt_rand_mode) = MT_RAND_MT19937;
	}

	hyss_mt_srand(seed);
}
/* }}} */

static uint32_t rand_range32(uint32_t umax) {
	uint32_t result, limit;

	result = hyss_mt_rand();

	/* Special case where no modulus is required */
	if (UNEXPECTED(umax == UINT32_MAX)) {
		return result;
	}

	/* Increment the max so the range is inclusive of max */
	umax++;

	/* Powers of two are not biased */
	if ((umax & (umax - 1)) == 0) {
		return result & (umax - 1);
	}

	/* Ceiling under which UINT32_MAX % max == 0 */
	limit = UINT32_MAX - (UINT32_MAX % umax) - 1;

	/* Discard numbers over the limit to avoid modulo bias */
	while (UNEXPECTED(result > limit)) {
		result = hyss_mt_rand();
	}

	return result % umax;
}

#if GEAR_ULONG_MAX > UINT32_MAX
static uint64_t rand_range64(uint64_t umax) {
	uint64_t result, limit;

	result = hyss_mt_rand();
	result = (result << 32) | hyss_mt_rand();

	/* Special case where no modulus is required */
	if (UNEXPECTED(umax == UINT64_MAX)) {
		return result;
	}

	/* Increment the max so the range is inclusive of max */
	umax++;

	/* Powers of two are not biased */
	if ((umax & (umax - 1)) == 0) {
		return result & (umax - 1);
	}

	/* Ceiling under which UINT64_MAX % max == 0 */
	limit = UINT64_MAX - (UINT64_MAX % umax) - 1;

	/* Discard numbers over the limit to avoid modulo bias */
	while (UNEXPECTED(result > limit)) {
		result = hyss_mt_rand();
		result = (result << 32) | hyss_mt_rand();
	}

	return result % umax;
}
#endif

/* {{{ hyss_mt_rand_range
 */
HYSSAPI gear_long hyss_mt_rand_range(gear_long min, gear_long max)
{
	gear_ulong umax = max - min;

#if GEAR_ULONG_MAX > UINT32_MAX
	if (umax > UINT32_MAX) {
		return (gear_long) (rand_range64(umax) + min);
	}
#endif

	return (gear_long) (rand_range32(umax) + min);
}
/* }}} */

/* {{{ hyss_mt_rand_common
 * rand() allows min > max, mt_rand does not */
HYSSAPI gear_long hyss_mt_rand_common(gear_long min, gear_long max)
{
	int64_t n;

	if (BG(mt_rand_mode) == MT_RAND_MT19937) {
		return hyss_mt_rand_range(min, max);
	}

	/* Legacy mode deliberately not inside hyss_mt_rand_range()
	 * to prevent other functions being affected */
	n = (int64_t)hyss_mt_rand() >> 1;
	RAND_RANGE_BADSCALING(n, min, max, HYSS_MT_RAND_MAX);

	return n;
}
/* }}} */

/* {{{ proto int mt_rand([int min, int max])
   Returns a random number from Mersenne Twister */
HYSS_FUNCTION(mt_rand)
{
	gear_long min;
	gear_long max;
	int argc = GEAR_NUM_ARGS();

	if (argc == 0) {
		// genrand_int31 in mt19937ar.c performs a right shift
		RETURN_LONG(hyss_mt_rand() >> 1);
	}

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_LONG(min)
		Z_PARAM_LONG(max)
	GEAR_PARSE_PARAMETERS_END();

	if (UNEXPECTED(max < min)) {
		hyss_error_docref(NULL, E_WARNING, "max(" GEAR_LONG_FMT ") is smaller than min(" GEAR_LONG_FMT ")", max, min);
		RETURN_FALSE;
	}

	RETURN_LONG(hyss_mt_rand_common(min, max));
}
/* }}} */

/* {{{ proto int mt_getrandmax(void)
   Returns the maximum value a random number from Mersenne Twister can have */
HYSS_FUNCTION(mt_getrandmax)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	/*
	 * Melo: it could be 2^^32 but we only use 2^^31 to maintain
	 * compatibility with the previous hyss_rand
	 */
  	RETURN_LONG(HYSS_MT_RAND_MAX); /* 2^^31 */
}
/* }}} */

HYSS_MINIT_FUNCTION(mt_rand)
{
	REGISTER_LONG_CONSTANT("MT_RAND_MT19937", MT_RAND_MT19937, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("MT_RAND_HYSS",     MT_RAND_HYSS, CONST_CS | CONST_PERSISTENT);

	return SUCCESS;
}

