/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "pageinfo.h"
#include "SAPI.h"

#include <stdio.h>
#include <stdlib.h>
#if HAVE_PWD_H
#ifdef HYSS_WIN32
#include "win32/pwd.h"
#else
#include <pwd.h>
#endif
#endif
#if HAVE_GRP_H
# ifdef HYSS_WIN32
#  include "win32/grp.h"
# else
#  include <grp.h>
# endif
#endif
#ifdef HYSS_WIN32
#undef getgid
#define getgroups(a, b) 0
#define getgid() 1
#define getuid() 1
#endif
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/stat.h>
#include <sys/types.h>
#ifdef HYSS_WIN32
#include <process.h>
#endif

#include "extslib/standard/basic_functions.h"

/* {{{ hyss_statpage
 */
HYSSAPI void hyss_statpage(void)
{
	gear_stat_t *pstat;

	pstat = sapi_get_stat();

	if (BG(page_uid)==-1 || BG(page_gid)==-1) {
		if(pstat) {
			BG(page_uid)   = pstat->st_uid;
			BG(page_gid)   = pstat->st_gid;
			BG(page_inode) = pstat->st_ino;
			BG(page_mtime) = pstat->st_mtime;
		} else { /* handler for situations where there is no source file, ex. hyss -r */
			BG(page_uid) = getuid();
			BG(page_gid) = getgid();
		}
	}
}
/* }}} */

/* {{{ hyss_getuid
 */
gear_long hyss_getuid(void)
{
	hyss_statpage();
	return (BG(page_uid));
}
/* }}} */

gear_long hyss_getgid(void)
{
	hyss_statpage();
	return (BG(page_gid));
}

/* {{{ proto int getmyuid(void)
   Get HYSS script owner's UID */
HYSS_FUNCTION(getmyuid)
{
	gear_long uid;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	uid = hyss_getuid();
	if (uid < 0) {
		RETURN_FALSE;
	} else {
		RETURN_LONG(uid);
	}
}
/* }}} */

/* {{{ proto int getmygid(void)
   Get HYSS script owner's GID */
HYSS_FUNCTION(getmygid)
{
	gear_long gid;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	gid = hyss_getgid();
	if (gid < 0) {
		RETURN_FALSE;
	} else {
		RETURN_LONG(gid);
	}
}
/* }}} */

/* {{{ proto int getmypid(void)
   Get current process ID */
HYSS_FUNCTION(getmypid)
{
	gear_long pid;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	pid = getpid();
	if (pid < 0) {
		RETURN_FALSE;
	} else {
		RETURN_LONG(pid);
	}
}
/* }}} */

/* {{{ proto int getmyinode(void)
   Get the inode of the current script being parsed */
HYSS_FUNCTION(getmyinode)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	hyss_statpage();
	if (BG(page_inode) < 0) {
		RETURN_FALSE;
	} else {
		RETURN_LONG(BG(page_inode));
	}
}
/* }}} */

HYSSAPI time_t hyss_getlastmod(void)
{
	hyss_statpage();
	return BG(page_mtime);
}

/* {{{ proto int getlastmod(void)
   Get time of last page modification */
HYSS_FUNCTION(getlastmod)
{
	gear_long lm;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	lm = hyss_getlastmod();
	if (lm < 0) {
		RETURN_FALSE;
	} else {
		RETURN_LONG(lm);
	}
}
/* }}} */

