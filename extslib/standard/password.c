/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "hyss.h"

#include "fcntl.h"
#include "hyss_password.h"
#include "hyss_rand.h"
#include "hyss_crypt.h"
#include "base64.h"
#include "gear_interfaces.h"
#include "info.h"
#include "hyss_random.h"
#if HAVE_ARGON2LIB
#include "argon2.h"
#endif

#ifdef HYSS_WIN32
#include "win32/winutil.h"
#endif

HYSS_MINIT_FUNCTION(password) /* {{{ */
{
	REGISTER_LONG_CONSTANT("PASSWORD_DEFAULT", HYSS_PASSWORD_DEFAULT, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("PASSWORD_BCRYPT", HYSS_PASSWORD_BCRYPT, CONST_CS | CONST_PERSISTENT);
#if HAVE_ARGON2LIB
	REGISTER_LONG_CONSTANT("PASSWORD_ARGON2I", HYSS_PASSWORD_ARGON2I, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("PASSWORD_ARGON2ID", HYSS_PASSWORD_ARGON2ID, CONST_CS | CONST_PERSISTENT);
#endif

	REGISTER_LONG_CONSTANT("PASSWORD_BCRYPT_DEFAULT_COST", HYSS_PASSWORD_BCRYPT_COST, CONST_CS | CONST_PERSISTENT);
#if HAVE_ARGON2LIB
	REGISTER_LONG_CONSTANT("PASSWORD_ARGON2_DEFAULT_MEMORY_COST", HYSS_PASSWORD_ARGON2_MEMORY_COST, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("PASSWORD_ARGON2_DEFAULT_TIME_COST", HYSS_PASSWORD_ARGON2_TIME_COST, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("PASSWORD_ARGON2_DEFAULT_THREADS", HYSS_PASSWORD_ARGON2_THREADS, CONST_CS | CONST_PERSISTENT);
#endif

	return SUCCESS;
}
/* }}} */

static gear_string* hyss_password_get_algo_name(const hyss_password_algo algo)
{
	switch (algo) {
		case HYSS_PASSWORD_BCRYPT:
			return gear_string_init("bcrypt", sizeof("bcrypt") - 1, 0);
#if HAVE_ARGON2LIB
		case HYSS_PASSWORD_ARGON2I:
			return gear_string_init("argon2i", sizeof("argon2i") - 1, 0);
		case HYSS_PASSWORD_ARGON2ID:
			return gear_string_init("argon2id", sizeof("argon2id") - 1, 0);
#endif
		case HYSS_PASSWORD_UNKNOWN:
		default:
			return gear_string_init("unknown", sizeof("unknown") - 1, 0);
	}
}

static hyss_password_algo hyss_password_determine_algo(const gear_string *hash)
{
	const char *h = ZSTR_VAL(hash);
	const size_t len = ZSTR_LEN(hash);
	if (len == 60 && h[0] == '$' && h[1] == '2' && h[2] == 'y') {
		return HYSS_PASSWORD_BCRYPT;
	}
#if HAVE_ARGON2LIB
	if (len >= sizeof("$argon2id$")-1 && !memcmp(h, "$argon2id$", sizeof("$argon2id$")-1)) {
    	return HYSS_PASSWORD_ARGON2ID;
	}

	if (len >= sizeof("$argon2i$")-1 && !memcmp(h, "$argon2i$", sizeof("$argon2i$")-1)) {
    	return HYSS_PASSWORD_ARGON2I;
	}
#endif

	return HYSS_PASSWORD_UNKNOWN;
}

static int hyss_password_salt_is_alphabet(const char *str, const size_t len) /* {{{ */
{
	size_t i = 0;

	for (i = 0; i < len; i++) {
		if (!((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z') || (str[i] >= '0' && str[i] <= '9') || str[i] == '.' || str[i] == '/')) {
			return FAILURE;
		}
	}
	return SUCCESS;
}
/* }}} */

static int hyss_password_salt_to64(const char *str, const size_t str_len, const size_t out_len, char *ret) /* {{{ */
{
	size_t pos = 0;
	gear_string *buffer;
	if ((int) str_len < 0) {
		return FAILURE;
	}
	buffer = hyss_base64_encode((unsigned char*) str, str_len);
	if (ZSTR_LEN(buffer) < out_len) {
		/* Too short of an encoded string generated */
		gear_string_release_ex(buffer, 0);
		return FAILURE;
	}
	for (pos = 0; pos < out_len; pos++) {
		if (ZSTR_VAL(buffer)[pos] == '+') {
			ret[pos] = '.';
		} else if (ZSTR_VAL(buffer)[pos] == '=') {
			gear_string_free(buffer);
			return FAILURE;
		} else {
			ret[pos] = ZSTR_VAL(buffer)[pos];
		}
	}
	gear_string_free(buffer);
	return SUCCESS;
}
/* }}} */

static gear_string* hyss_password_make_salt(size_t length) /* {{{ */
{
	gear_string *ret, *buffer;

	if (length > (INT_MAX / 3)) {
		hyss_error_docref(NULL, E_WARNING, "Length is too large to safely generate");
		return NULL;
	}

	buffer = gear_string_alloc(length * 3 / 4 + 1, 0);
	if (FAILURE == hyss_random_bytes_silent(ZSTR_VAL(buffer), ZSTR_LEN(buffer))) {
		hyss_error_docref(NULL, E_WARNING, "Unable to generate salt");
		gear_string_release_ex(buffer, 0);
		return NULL;
	}

	ret = gear_string_alloc(length, 0);
	if (hyss_password_salt_to64(ZSTR_VAL(buffer), ZSTR_LEN(buffer), length, ZSTR_VAL(ret)) == FAILURE) {
		hyss_error_docref(NULL, E_WARNING, "Generated salt too short");
		gear_string_release_ex(buffer, 0);
		gear_string_release_ex(ret, 0);
		return NULL;
	}
	gear_string_release_ex(buffer, 0);
	ZSTR_VAL(ret)[length] = 0;
	return ret;
}
/* }}} */

#if HAVE_ARGON2LIB
static void extract_argon2_parameters(const hyss_password_algo algo, const gear_string *hash,
									  gear_long *v, gear_long *memory_cost,
									  gear_long *time_cost, gear_long *threads) /* {{{ */
{
	if (algo == HYSS_PASSWORD_ARGON2ID) {
		sscanf(ZSTR_VAL(hash), "$%*[argon2id]$v=" GEAR_LONG_FMT "$m=" GEAR_LONG_FMT ",t=" GEAR_LONG_FMT ",p=" GEAR_LONG_FMT, v, memory_cost, time_cost, threads);
	} else if (algo == HYSS_PASSWORD_ARGON2I) {
		sscanf(ZSTR_VAL(hash), "$%*[argon2i]$v=" GEAR_LONG_FMT "$m=" GEAR_LONG_FMT ",t=" GEAR_LONG_FMT ",p=" GEAR_LONG_FMT, v, memory_cost, time_cost, threads);
	}

	return;
}
#endif

/* {{{ proto array password_get_info(string $hash)
Retrieves information about a given hash */
HYSS_FUNCTION(password_get_info)
{
	hyss_password_algo algo;
	gear_string *hash, *algo_name;
	zval options;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(hash)
	GEAR_PARSE_PARAMETERS_END();

	array_init(&options);

	algo = hyss_password_determine_algo(hash);
	algo_name = hyss_password_get_algo_name(algo);

	switch (algo) {
		case HYSS_PASSWORD_BCRYPT:
			{
				gear_long cost = HYSS_PASSWORD_BCRYPT_COST;
				sscanf(ZSTR_VAL(hash), "$2y$" GEAR_LONG_FMT "$", &cost);
				add_assoc_long(&options, "cost", cost);
			}
			break;
#if HAVE_ARGON2LIB
		case HYSS_PASSWORD_ARGON2I:
		case HYSS_PASSWORD_ARGON2ID:
			{
				gear_long v = 0;
				gear_long memory_cost = HYSS_PASSWORD_ARGON2_MEMORY_COST;
				gear_long time_cost = HYSS_PASSWORD_ARGON2_TIME_COST;
				gear_long threads = HYSS_PASSWORD_ARGON2_THREADS;

				extract_argon2_parameters(algo, hash, &v, &memory_cost, &time_cost, &threads);

				add_assoc_long(&options, "memory_cost", memory_cost);
				add_assoc_long(&options, "time_cost", time_cost);
				add_assoc_long(&options, "threads", threads);
			}
			break;
#endif
		case HYSS_PASSWORD_UNKNOWN:
		default:
			break;
	}

	array_init(return_value);

	add_assoc_long(return_value, "algo", algo);
	add_assoc_str(return_value, "algoName", algo_name);
	add_assoc_zval(return_value, "options", &options);
}
/** }}} */

/* {{{ proto bool password_needs_rehash(string $hash, int $algo[, array $options])
Determines if a given hash requires re-hashing based upon parameters */
HYSS_FUNCTION(password_needs_rehash)
{
	gear_long new_algo = 0;
	hyss_password_algo algo;
	gear_string *hash;
	HashTable *options = 0;
	zval *option_buffer;

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(hash)
		Z_PARAM_LONG(new_algo)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY_OR_OBJECT_HT(options)
	GEAR_PARSE_PARAMETERS_END();

	algo = hyss_password_determine_algo(hash);

	if ((gear_long)algo != new_algo) {
		RETURN_TRUE;
	}

	switch (algo) {
		case HYSS_PASSWORD_BCRYPT:
			{
				gear_long new_cost = HYSS_PASSWORD_BCRYPT_COST, cost = 0;

				if (options && (option_buffer = gear_hash_str_find(options, "cost", sizeof("cost")-1)) != NULL) {
					new_cost = zval_get_long(option_buffer);
				}

				sscanf(ZSTR_VAL(hash), "$2y$" GEAR_LONG_FMT "$", &cost);
				if (cost != new_cost) {
					RETURN_TRUE;
				}
			}
			break;
#if HAVE_ARGON2LIB
		case HYSS_PASSWORD_ARGON2I:
		case HYSS_PASSWORD_ARGON2ID:
			{
				gear_long v = 0;
				gear_long new_memory_cost = HYSS_PASSWORD_ARGON2_MEMORY_COST, memory_cost = 0;
				gear_long new_time_cost = HYSS_PASSWORD_ARGON2_TIME_COST, time_cost = 0;
				gear_long new_threads = HYSS_PASSWORD_ARGON2_THREADS, threads = 0;

				if (options && (option_buffer = gear_hash_str_find(options, "memory_cost", sizeof("memory_cost")-1)) != NULL) {
					new_memory_cost = zval_get_long(option_buffer);
				}

				if (options && (option_buffer = gear_hash_str_find(options, "time_cost", sizeof("time_cost")-1)) != NULL) {
					new_time_cost = zval_get_long(option_buffer);
				}

				if (options && (option_buffer = gear_hash_str_find(options, "threads", sizeof("threads")-1)) != NULL) {
					new_threads = zval_get_long(option_buffer);
				}

				extract_argon2_parameters(algo, hash, &v, &memory_cost, &time_cost, &threads);

				if (new_time_cost != time_cost || new_memory_cost != memory_cost || new_threads != threads) {
					RETURN_TRUE;
				}
			}
			break;
#endif
		case HYSS_PASSWORD_UNKNOWN:
		default:
			break;
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto bool password_verify(string password, string hash)
Verify a hash created using crypt() or password_hash() */
HYSS_FUNCTION(password_verify)
{
	gear_string *password, *hash;
	hyss_password_algo algo;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(password)
		Z_PARAM_STR(hash)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	algo = hyss_password_determine_algo(hash);

	switch(algo) {
#if HAVE_ARGON2LIB
		case HYSS_PASSWORD_ARGON2I:
		case HYSS_PASSWORD_ARGON2ID:
			{
				argon2_type type;
				if (algo == HYSS_PASSWORD_ARGON2ID) {
					type = Argon2_id;
				} else if (algo == HYSS_PASSWORD_ARGON2I) {
					type = Argon2_i;
				}
				RETURN_BOOL(ARGON2_OK == argon2_verify(ZSTR_VAL(hash), ZSTR_VAL(password), ZSTR_LEN(password), type));
			}
			break;
#endif
		case HYSS_PASSWORD_BCRYPT:
		case HYSS_PASSWORD_UNKNOWN:
		default:
			{
				size_t i;
				int status = 0;
				gear_string *ret = hyss_crypt(ZSTR_VAL(password), (int)ZSTR_LEN(password), ZSTR_VAL(hash), (int)ZSTR_LEN(hash), 1);

				if (!ret) {
					RETURN_FALSE;
				}

				if (ZSTR_LEN(ret) != ZSTR_LEN(hash) || ZSTR_LEN(hash) < 13) {
					gear_string_free(ret);
					RETURN_FALSE;
				}

				/* We're using this method instead of == in order to provide
				* resistance towards timing attacks. This is a constant time
				* equality check that will always check every byte of both
				* values. */
				for (i = 0; i < ZSTR_LEN(hash); i++) {
					status |= (ZSTR_VAL(ret)[i] ^ ZSTR_VAL(hash)[i]);
				}

				gear_string_free(ret);

				RETURN_BOOL(status == 0);
			}
	}

	RETURN_FALSE;
}
/* }}} */

static gear_string* hyss_password_get_salt(zval *return_value, size_t required_salt_len, HashTable *options) {
	gear_string *buffer;
	zval *option_buffer;

	if (!options || !(option_buffer = gear_hash_str_find(options, "salt", sizeof("salt") - 1))) {
		buffer = hyss_password_make_salt(required_salt_len);
		if (!buffer) {
			RETVAL_FALSE;
		}
		return buffer;
	}

	hyss_error_docref(NULL, E_DEPRECATED, "Use of the 'salt' option to password_hash is deprecated");

	switch (Z_TYPE_P(option_buffer)) {
		case IS_STRING:
			buffer = gear_string_copy(Z_STR_P(option_buffer));
			break;
		case IS_LONG:
		case IS_DOUBLE:
		case IS_OBJECT:
			buffer = zval_get_string(option_buffer);
			break;
		case IS_FALSE:
		case IS_TRUE:
		case IS_NULL:
		case IS_RESOURCE:
		case IS_ARRAY:
		default:
			hyss_error_docref(NULL, E_WARNING, "Non-string salt parameter supplied");
			return NULL;
	}

	/* XXX all the crypt related APIs work with int for string length.
		That should be revised for size_t and then we maybe don't require
		the > INT_MAX check. */
	if (GEAR_SIZE_T_INT_OVFL(ZSTR_LEN(buffer))) {
		hyss_error_docref(NULL, E_WARNING, "Supplied salt is too long");
		gear_string_release_ex(buffer, 0);
		return NULL;
	}

	if (ZSTR_LEN(buffer) < required_salt_len) {
		hyss_error_docref(NULL, E_WARNING, "Provided salt is too short: %zd expecting %zd", ZSTR_LEN(buffer), required_salt_len);
		gear_string_release_ex(buffer, 0);
		return NULL;
	}

	if (hyss_password_salt_is_alphabet(ZSTR_VAL(buffer), ZSTR_LEN(buffer)) == FAILURE) {
		gear_string *salt = gear_string_alloc(required_salt_len, 0);
		if (hyss_password_salt_to64(ZSTR_VAL(buffer), ZSTR_LEN(buffer), required_salt_len, ZSTR_VAL(salt)) == FAILURE) {
			hyss_error_docref(NULL, E_WARNING, "Provided salt is too short: %zd", ZSTR_LEN(buffer));
			gear_string_release_ex(salt, 0);
			gear_string_release_ex(buffer, 0);
			return NULL;
		}
		gear_string_release_ex(buffer, 0);
		return salt;
	} else {
		gear_string *salt = gear_string_alloc(required_salt_len, 0);
		memcpy(ZSTR_VAL(salt), ZSTR_VAL(buffer), required_salt_len);
		gear_string_release_ex(buffer, 0);
		return salt;
	}
}

/* {{{ proto string password_hash(string password, int algo[, array options = array()])
Hash a password */
HYSS_FUNCTION(password_hash)
{
	gear_string *password;
	gear_long algo = HYSS_PASSWORD_DEFAULT;
	HashTable *options = NULL;

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(password)
		Z_PARAM_LONG(algo)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY_OR_OBJECT_HT(options)
	GEAR_PARSE_PARAMETERS_END();

	switch (algo) {
		case HYSS_PASSWORD_BCRYPT:
			{
				char hash_format[10];
				size_t hash_format_len;
				gear_string *result, *hash, *salt;
				zval *option_buffer;
				gear_long cost = HYSS_PASSWORD_BCRYPT_COST;

				if (options && (option_buffer = gear_hash_str_find(options, "cost", sizeof("cost")-1)) != NULL) {
					cost = zval_get_long(option_buffer);
				}

				if (cost < 4 || cost > 31) {
					hyss_error_docref(NULL, E_WARNING, "Invalid bcrypt cost parameter specified: " GEAR_LONG_FMT, cost);
					RETURN_NULL();
				}

				hash_format_len = snprintf(hash_format, sizeof(hash_format), "$2y$%02" GEAR_LONG_FMT_SPEC "$", cost);
				if (!(salt = hyss_password_get_salt(return_value, Z_UL(22), options))) {
					return;
				}
				ZSTR_VAL(salt)[ZSTR_LEN(salt)] = 0;

				hash = gear_string_alloc(ZSTR_LEN(salt) + hash_format_len, 0);
				sprintf(ZSTR_VAL(hash), "%s%s", hash_format, ZSTR_VAL(salt));
				ZSTR_VAL(hash)[hash_format_len + ZSTR_LEN(salt)] = 0;

				gear_string_release_ex(salt, 0);

				/* This cast is safe, since both values are defined here in code and cannot overflow */
				result = hyss_crypt(ZSTR_VAL(password), (int)ZSTR_LEN(password), ZSTR_VAL(hash), (int)ZSTR_LEN(hash), 1);
				gear_string_release_ex(hash, 0);

				if (!result) {
					RETURN_FALSE;
				}

				if (ZSTR_LEN(result) < 13) {
					gear_string_free(result);
					RETURN_FALSE;
				}

				RETURN_STR(result);
			}
			break;
#if HAVE_ARGON2LIB
		case HYSS_PASSWORD_ARGON2I:
		case HYSS_PASSWORD_ARGON2ID:
			{
				zval *option_buffer;
				gear_string *salt, *out, *encoded;
				size_t time_cost = HYSS_PASSWORD_ARGON2_TIME_COST;
				size_t memory_cost = HYSS_PASSWORD_ARGON2_MEMORY_COST;
				size_t threads = HYSS_PASSWORD_ARGON2_THREADS;
				argon2_type type;
				if (algo == HYSS_PASSWORD_ARGON2ID) {
					type = Argon2_id;
				} else if (algo == HYSS_PASSWORD_ARGON2I) {
					type = Argon2_i;
				}
				size_t encoded_len;
				int status = 0;

				if (options && (option_buffer = gear_hash_str_find(options, "memory_cost", sizeof("memory_cost")-1)) != NULL) {
					memory_cost = zval_get_long(option_buffer);
				}

				if (memory_cost > ARGON2_MAX_MEMORY || memory_cost < ARGON2_MIN_MEMORY) {
					hyss_error_docref(NULL, E_WARNING, "Memory cost is outside of allowed memory range");
					RETURN_NULL();
				}

				if (options && (option_buffer = gear_hash_str_find(options, "time_cost", sizeof("time_cost")-1)) != NULL) {
					time_cost = zval_get_long(option_buffer);
				}

				if (time_cost > ARGON2_MAX_TIME || time_cost < ARGON2_MIN_TIME) {
					hyss_error_docref(NULL, E_WARNING, "Time cost is outside of allowed time range");
					RETURN_NULL();
				}

				if (options && (option_buffer = gear_hash_str_find(options, "threads", sizeof("threads")-1)) != NULL) {
					threads = zval_get_long(option_buffer);
				}

				if (threads > ARGON2_MAX_LANES || threads == 0) {
					hyss_error_docref(NULL, E_WARNING, "Invalid number of threads");
					RETURN_NULL();
				}

				if (!(salt = hyss_password_get_salt(return_value, Z_UL(16), options))) {
					return;
				}

				out = gear_string_alloc(32, 0);
				encoded_len = argon2_encodedlen(
					time_cost,
					memory_cost,
					threads,
					(uint32_t)ZSTR_LEN(salt),
					ZSTR_LEN(out),
					type
				);

				encoded = gear_string_alloc(encoded_len - 1, 0);
				status = argon2_hash(
					time_cost,
					memory_cost,
					threads,
					ZSTR_VAL(password),
					ZSTR_LEN(password),
					ZSTR_VAL(salt),
					ZSTR_LEN(salt),
					ZSTR_VAL(out),
					ZSTR_LEN(out),
					ZSTR_VAL(encoded),
					encoded_len,
					type,
					ARGON2_VERSION_NUMBER
				);

				gear_string_release_ex(out, 0);
				gear_string_release_ex(salt, 0);

				if (status != ARGON2_OK) {
					gear_string_efree(encoded);
					hyss_error_docref(NULL, E_WARNING, "%s", argon2_error_message(status));
					RETURN_FALSE;
				}

				ZSTR_VAL(encoded)[ZSTR_LEN(encoded)] = 0;
				RETURN_NEW_STR(encoded);
			}
			break;
#endif
		case HYSS_PASSWORD_UNKNOWN:
		default:
			hyss_error_docref(NULL, E_WARNING, "Unknown password hashing algorithm: " GEAR_LONG_FMT, algo);
			RETURN_NULL();
	}
}
/* }}} */

