/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_MAIL_H
#define HYSS_MAIL_H

HYSS_FUNCTION(mail);
HYSS_FUNCTION(ezmlm_hash);

HYSS_MINFO_FUNCTION(mail);

HYSSAPI gear_string *hyss_mail_build_headers(zval *headers);
HYSSAPI extern int hyss_mail(char *to, char *subject, char *message, char *headers, char *extra_cmd);

#define HYSS_MAIL_BUILD_HEADER_CHECK(target, s, key, val) \
do { \
	if (Z_TYPE_P(val) == IS_STRING) { \
		hyss_mail_build_headers_elem(&s, key, val); \
	} else if (Z_TYPE_P(val) == IS_ARRAY) { \
		if (!strncasecmp(target, ZSTR_VAL(key), ZSTR_LEN(key))) { \
			hyss_error_docref(NULL, E_WARNING, "'%s' header must be at most one header. Array is passed for '%s'", target, target); \
			continue; \
		} \
		hyss_mail_build_headers_elems(&s, key, val); \
	} else { \
		hyss_error_docref(NULL, E_WARNING, "Extra header element '%s' cannot be other than string or array.", ZSTR_VAL(key)); \
	} \
} while(0)


#define HYSS_MAIL_BUILD_HEADER_DEFAULT(s, key, val) \
do { \
	if (Z_TYPE_P(val) == IS_STRING) { \
		hyss_mail_build_headers_elem(&s, key, val); \
	} else if (Z_TYPE_P(val) == IS_ARRAY) { \
		hyss_mail_build_headers_elems(&s, key, val); \
	} else { \
		hyss_error_docref(NULL, E_WARNING, "Extra header element '%s' cannot be other than string or array.", ZSTR_VAL(key)); \
	} \
} while(0)


#endif /* HYSS_MAIL_H */
