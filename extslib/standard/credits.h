/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CREDITS_H
#define CREDITS_H

#ifndef HAVE_CREDITS_DEFS
#define HAVE_CREDITS_DEFS

#define HYSS_CREDITS_GROUP			(1<<0)
#define HYSS_CREDITS_GENERAL			(1<<1)
#define HYSS_CREDITS_SAPI			(1<<2)
#define HYSS_CREDITS_CAPIS			(1<<3)
#define HYSS_CREDITS_DOCS			(1<<4)
#define HYSS_CREDITS_FULLPAGE			(1<<5)
#define HYSS_CREDITS_QA				(1<<6)
#define HYSS_CREDITS_WEB			(1<<7)
#define HYSS_CREDITS_ALL			0xFFFFFFFF

#endif /* HAVE_CREDITS_DEFS */

HYSSAPI void hyss_print_credits(int flag);

#endif
