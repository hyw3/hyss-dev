/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "info.h"
#include "SAPI.h"

#define CREDIT_LINE(cAPI, authors) hyss_info_print_table_row(2, cAPI, authors)

HYSSAPI void hyss_print_credits(int flag) /* {{{ */
{
	if (!sapi_capi.hyssinfo_as_text && flag & HYSS_CREDITS_FULLPAGE) {
		hyss_print_info_htmlhead();
	}

	if (!sapi_capi.hyssinfo_as_text) {
		PUTS("<h1>HYSS Credits</h1>\n");
	} else {
		PUTS("HYSS Credits\n");
	}

	if (flag & HYSS_CREDITS_GROUP) {
		hyss_info_print_table_start();
		hyss_info_print_table_header(1, "The Hyang Language Foundation");
		hyss_info_print_table_row(1, "Hilman P. Alisabana, M. Sindu Natama, Heru Yonardi, Rida Farida, Dian Shaphira, Inneu Susanti, Dara Irawan, Nadya, Lena C.S., Aldi Dinata, Elvina Larasati, Ade Sambudi, Anna Nita, Nikita Yusuf, Tedja Arsyad, Yazid Amini");
		hyss_info_print_table_end();
	}

	if (flag & HYSS_CREDITS_GENERAL) {
		hyss_info_print_table_start();
		if (!sapi_capi.hyssinfo_as_text) {
			hyss_info_print_table_header(1, "Language Design &amp; Concept");
		} else {
			hyss_info_print_table_header(1, "Language Design & Concept");
		}
		hyss_info_print_table_row(1, "Hilman P. Alisabana, Aldi Dinata, M. Sindu Natama");
		hyss_info_print_table_end();
		hyss_info_print_table_start();
		hyss_info_print_table_colspan_header(2, "HYSS Authors");
		hyss_info_print_table_header(2, "Contribution", "Authors");
		CREDIT_LINE("Gear Scripting Language Engine", "M. Sindu Natama, Aldi Dinata, Hilman P. Alisabana");
		CREDIT_LINE("Extension cAPI API", "Aldi Dinata, M. Sindu Natama, Hilman P. Alisabana");
		CREDIT_LINE("UNIX Build and Modularization", "Heru Yonardi");
		CREDIT_LINE("Windows Support", "Ade Sambudi, Lena C.S.");
		CREDIT_LINE("Server API (SAPI) Abstraction Layer", "Nikita Yusuf, Nadya");
		CREDIT_LINE("Streams Abstraction Layer", "Yazid Amini");
		CREDIT_LINE("HYSS Data Objects Layer", "M. Sindu Natama, Dian Shaphira, Tedja Arsyad");
		CREDIT_LINE("Output Handler", "Inneu Susanti");
		CREDIT_LINE("Consistent 64 bit support", "Heru Yonardi, Lena C.S.");
		hyss_info_print_table_end();
	}

	if (flag & HYSS_CREDITS_SAPI) {
		hyss_info_print_table_start();
		hyss_info_print_table_colspan_header(2, "SAPI cAPIs");
		hyss_info_print_table_header(2, "Contribution", "Authors");
#include "credits_sapi.h"
		hyss_info_print_table_end();
	}

	if (flag & HYSS_CREDITS_CAPIS) {
		hyss_info_print_table_start();
		hyss_info_print_table_colspan_header(2, "cAPI Authors");
		hyss_info_print_table_header(2, "cAPI", "Authors");
#include "credits_ext.h"
		hyss_info_print_table_end();
	}

	if (flag & HYSS_CREDITS_DOCS) {
		hyss_info_print_table_start();
		hyss_info_print_table_colspan_header(2, "HYSS Documentation");
		CREDIT_LINE("Authors", "M. Sindu Natama, Aldi Dinata, Hilman P. Alisabana");
		CREDIT_LINE("Editor", "Hilman P. Alisabana");
		CREDIT_LINE("User Note Maintainers", "Inneu Susanti");
		CREDIT_LINE("Other Contributors", "Dian Shaphira");
		hyss_info_print_table_end();
	}

	if (flag & HYSS_CREDITS_QA) {
		hyss_info_print_table_start();
		hyss_info_print_table_header(1, "HYSS Quality Assurance Team");
		hyss_info_print_table_row(1, "Hilman P. Alisabana, Dian Shaphira, M. Sindu Natama");
		hyss_info_print_table_end();
	}

	if (flag & HYSS_CREDITS_WEB) {
		hyss_info_print_table_start();
		hyss_info_print_table_colspan_header(2, "Websites and Infrastructure team");
		CREDIT_LINE("HYSS Websites Team", "Inneu Susanti, M. Sindu Natama");
		CREDIT_LINE("Event Maintainers", "Heru Yonardi, Ade Sambudi");
		CREDIT_LINE("Network Infrastructure", "M. Sindu Natama");
		CREDIT_LINE("Windows Infrastructure", "Lena C.S., Ade Sambudi");
		hyss_info_print_table_end();
	}

	if (!sapi_capi.hyssinfo_as_text && flag & HYSS_CREDITS_FULLPAGE) {
		PUTS("</div></body></html>\n");
	}
}
/* }}} */

