/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"

#include <stdlib.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <string.h>
#include <errno.h>

#include <stdio.h>
#ifdef HYSS_WIN32
#include "win32/time.h"
#else
#include <sys/time.h>
#endif

#include "hyss_lcg.h"
#include "uniqid.h"

#ifdef HAVE_GETTIMEOFDAY
GEAR_TLS struct timeval prev_tv = { 0, 0 };

/* {{{ proto string uniqid([string prefix [, bool more_entropy]])
   Generates a unique ID */
HYSS_FUNCTION(uniqid)
{
	char *prefix = "";
	gear_bool more_entropy = 0;
	gear_string *uniqid;
	int sec, usec;
	size_t prefix_len = 0;
	struct timeval tv;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(prefix, prefix_len)
		Z_PARAM_BOOL(more_entropy)
	GEAR_PARSE_PARAMETERS_END();

	/* This implementation needs current microsecond to change,
	 * hence we poll time until it does. This is much faster than
	 * calling usleep(1) which may cause the kernel to schedule
	 * another process, causing a pause of around 10ms.
	 */
	do {
		(void)gettimeofday((struct timeval *) &tv, (struct timezone *) NULL);
	} while (tv.tv_sec == prev_tv.tv_sec && tv.tv_usec == prev_tv.tv_usec);

	prev_tv.tv_sec = tv.tv_sec;
	prev_tv.tv_usec = tv.tv_usec;

	sec = (int) tv.tv_sec;
	usec = (int) (tv.tv_usec % 0x100000);

	/* The max value usec can have is 0xF423F, so we use only five hex
	 * digits for usecs.
	 */
	if (more_entropy) {
		uniqid = strpprintf(0, "%s%08x%05x%.8F", prefix, sec, usec, hyss_combined_lcg() * 10);
	} else {
		uniqid = strpprintf(0, "%s%08x%05x", prefix, sec, usec);
	}

	RETURN_STR(uniqid);
}
#endif
/* }}} */

