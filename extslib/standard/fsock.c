/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_globals.h"
#include <stdlib.h>
#include <stddef.h>
#include "hyss_network.h"
#include "file.h"

/* {{{ hyss_fsockopen() */

static void hyss_fsockopen_stream(INTERNAL_FUNCTION_PARAMETERS, int persistent)
{
	char *host;
	size_t host_len;
	gear_long port = -1;
	zval *zerrno = NULL, *zerrstr = NULL;
	double timeout = (double)FG(default_socket_timeout);
#ifndef HYSS_WIN32
	time_t conv;
#else
	long conv;
#endif
	struct timeval tv;
	char *hashkey = NULL;
	hyss_stream *stream = NULL;
	int err;
	char *hostname = NULL;
	size_t hostname_len;
	gear_string *errstr = NULL;

	RETVAL_FALSE;

	GEAR_PARSE_PARAMETERS_START(1, 5)
		Z_PARAM_STRING(host, host_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(port)
		Z_PARAM_ZVAL_DEREF(zerrno)
		Z_PARAM_ZVAL_DEREF(zerrstr)
		Z_PARAM_DOUBLE(timeout)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (persistent) {
		spprintf(&hashkey, 0, "pfsockopen__%s:" GEAR_LONG_FMT, host, port);
	}

	if (port > 0) {
		hostname_len = spprintf(&hostname, 0, "%s:" GEAR_LONG_FMT, host, port);
	} else {
		hostname_len = host_len;
		hostname = host;
	}

	/* prepare the timeout value for use */
#ifndef HYSS_WIN32
	conv = (time_t) (timeout * 1000000.0);
	tv.tv_sec = conv / 1000000;
#else
	conv = (long) (timeout * 1000000.0);
	tv.tv_sec = conv / 1000000;
#endif
	tv.tv_usec = conv % 1000000;

	if (zerrno)	{
		zval_ptr_dtor(zerrno);
		ZVAL_LONG(zerrno, 0);
	}
	if (zerrstr) {
		zval_ptr_dtor(zerrstr);
		ZVAL_EMPTY_STRING(zerrstr);
	}

	stream = hyss_stream_xport_create(hostname, hostname_len, REPORT_ERRORS,
			STREAM_XPORT_CLIENT | STREAM_XPORT_CONNECT, hashkey, &tv, NULL, &errstr, &err);

	if (port > 0) {
		efree(hostname);
	}
	if (stream == NULL) {
		hyss_error_docref(NULL, E_WARNING, "unable to connect to %s:" GEAR_LONG_FMT " (%s)", host, port, errstr == NULL ? "Unknown error" : ZSTR_VAL(errstr));
	}

	if (hashkey) {
		efree(hashkey);
	}

	if (stream == NULL)	{
		if (zerrno) {
			zval_ptr_dtor(zerrno);
			ZVAL_LONG(zerrno, err);
		}
		if (zerrstr && errstr) {
			/* no need to dup; we need to efree buf anyway */
			zval_ptr_dtor(zerrstr);
			ZVAL_STR(zerrstr, errstr);
		} else if (!zerrstr && errstr) {
			gear_string_release_ex(errstr, 0);
		}

		RETURN_FALSE;
	}

	if (errstr) {
		gear_string_release_ex(errstr, 0);
	}

	hyss_stream_to_zval(stream, return_value);
}

/* }}} */

/* {{{ proto resource fsockopen(string hostname, int port [, int errno [, string errstr [, float timeout]]])
   Open Internet or Unix domain socket connection */
HYSS_FUNCTION(fsockopen)
{
	hyss_fsockopen_stream(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto resource pfsockopen(string hostname, int port [, int errno [, string errstr [, float timeout]]])
   Open persistent Internet or Unix domain socket connection */
HYSS_FUNCTION(pfsockopen)
{
	hyss_fsockopen_stream(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

