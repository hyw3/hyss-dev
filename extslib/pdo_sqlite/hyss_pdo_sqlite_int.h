/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PDO_SQLITE_INT_H
#define HYSS_PDO_SQLITE_INT_H

#include <sqlite3.h>

typedef struct {
	const char *file;
	int line;
	unsigned int errcode;
	char *errmsg;
} pdo_sqlite_error_info;

struct pdo_sqlite_fci {
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;
};

struct pdo_sqlite_func {
	struct pdo_sqlite_func *next;

	zval func, step, fini;
	int argc;
	const char *funcname;

	/* accelerated callback references */
	struct pdo_sqlite_fci afunc, astep, afini;
};

struct pdo_sqlite_collation {
	struct pdo_sqlite_collation *next;

	const char *name;
	zval callback;
	struct pdo_sqlite_fci fc;
};

typedef struct {
	sqlite3 *db;
	pdo_sqlite_error_info einfo;
	struct pdo_sqlite_func *funcs;
	struct pdo_sqlite_collation *collations;
} pdo_sqlite_db_handle;

typedef struct {
	pdo_sqlite_db_handle 	*H;
	sqlite3_stmt *stmt;
	unsigned pre_fetched:1;
	unsigned done:1;
} pdo_sqlite_stmt;

extern const pdo_driver_t pdo_sqlite_driver;

extern int _pdo_sqlite_error(pdo_dbh_t *dbh, pdo_stmt_t *stmt, const char *file, int line);
#define pdo_sqlite_error(s) _pdo_sqlite_error(s, NULL, __FILE__, __LINE__)
#define pdo_sqlite_error_stmt(s) _pdo_sqlite_error(stmt->dbh, stmt, __FILE__, __LINE__)

extern const struct pdo_stmt_methods sqlite_stmt_methods;

enum {
	PDO_SQLITE_ATTR_OPEN_FLAGS = PDO_ATTR_DRIVER_SPECIFIC,
};

#endif
