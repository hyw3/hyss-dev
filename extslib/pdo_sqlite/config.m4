dnl config.m4 for extension pdo_sqlite
dnl vim:et:sw=2:ts=2:

HYSS_ARG_WITH(pdo-sqlite, for sqlite 3 support for PDO,
[  --without-pdo-sqlite[=DIR]
                          PDO: sqlite 3 support.  DIR is the sqlite base
                          install directory [BUNDLED]], $HYSS_PDO)

if test "$HYSS_PDO_SQLITE" != "no"; then

  if test "$HYSS_PDO" = "no" && test "$ext_shared" = "no"; then
    AC_MSG_ERROR([PDO is not enabled! Add --enable-pdo to your configure line.])
  fi

  ifdef([HYSS_CHECK_PDO_INCLUDES],
  [
    HYSS_CHECK_PDO_INCLUDES
  ],[
    AC_MSG_CHECKING([for PDO includes])
    if test -f $abs_srcdir/include/hyss/extslib/pdo/hyss_pdo_driver.h; then
      pdo_cv_inc_path=$abs_srcdir/extslib
    elif test -f $abs_srcdir/extslib/pdo/hyss_pdo_driver.h; then
      pdo_cv_inc_path=$abs_srcdir/extslib
    elif test -f $hyssincludedir/extslib/pdo/hyss_pdo_driver.h; then
      pdo_cv_inc_path=$hyssincludedir/extslib
    else
      AC_MSG_ERROR([Cannot find hyss_pdo_driver.h.])
    fi
    AC_MSG_RESULT($pdo_cv_inc_path)
  ])

  hyss_pdo_sqlite_sources_core="pdo_sqlite.c sqlite_driver.c sqlite_statement.c"

  if test "$HYSS_PDO_SQLITE" != "yes"; then
    SEARCH_PATH="$HYSS_PDO_SQLITE /usr/local /usr"     # you might want to change this
    SEARCH_FOR="/include/sqlite3.h"  # you most likely want to change this
    if test -r $HYSS_PDO_SQLITE/$SEARCH_FOR; then # path given as parameter
      PDO_SQLITE_DIR=$HYSS_PDO_SQLITE
    else # search default path list
      AC_MSG_CHECKING([for sqlite3 files in default path])
      for i in $SEARCH_PATH ; do
        if test -r $i/$SEARCH_FOR; then
          PDO_SQLITE_DIR=$i
          AC_MSG_RESULT(found in $i)
        fi
      done
    fi
    if test -z "$PDO_SQLITE_DIR"; then
      AC_MSG_RESULT([not found])
      AC_MSG_ERROR([Please reinstall the sqlite3 distribution])
    fi

    HYSS_ADD_INCLUDE($PDO_SQLITE_DIR/include)

    LIBNAME=sqlite3
    LIBSYMBOL=sqlite3_open

    HYSS_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
    [
      HYSS_ADD_LIBRARY_WITH_PATH($LIBNAME, $PDO_SQLITE_DIR/$HYSS_LIBDIR, PDO_SQLITE_SHARED_LIBADD)
      AC_DEFINE(HAVE_PDO_SQLITELIB,1,[ ])
    ],[
      AC_MSG_ERROR([wrong sqlite lib version or lib not found])
    ],[
      -L$PDO_SQLITE_DIR/$HYSS_LIBDIR -lm
    ])
    HYSS_CHECK_LIBRARY(sqlite3,sqlite3_key,[
      AC_DEFINE(HAVE_SQLITE3_KEY,1, [have commercial sqlite3 with crypto support])
    ])
    HYSS_CHECK_LIBRARY(sqlite3,sqlite3_close_v2,[
      AC_DEFINE(HAVE_SQLITE3_CLOSE_V2, 1, [have sqlite3_close_v2])
    ])

    HYSS_SUBST(PDO_SQLITE_SHARED_LIBADD)
    HYSS_NEW_EXTENSION(pdo_sqlite, $hyss_pdo_sqlite_sources_core, $ext_shared,,-I$pdo_cv_inc_path)
  else
      # use bundled libs
      if test "$enable_maintainer_zts" = "yes"; then
        threadsafe_flags="-DSQLITE_THREADSAFE=1"
      else
        threadsafe_flags="-DSQLITE_THREADSAFE=0"
      fi

      AC_DEFINE(HAVE_SQLITE3_CLOSE_V2, 1, [have sqlite3_close_v2])
      other_flags="-DSQLITE_ENABLE_FTS3=1 -DSQLITE_ENABLE_FTS4=1 -DSQLITE_ENABLE_FTS5=1 -DSQLITE_ENABLE_JSON1=1 -DSQLITE_CORE=1 -DSQLITE_ENABLE_COLUMN_METADATA=1"

	  dnl As long as intl is not shared we can have ICU support
      if test "$HYSS_INTL" = "yes" && test "$HYSS_INTL_SHARED" != "yes"; then
        other_flags="$other_flags -DSQLITE_ENABLE_ICU=1"
      fi

      if test "$HYSS_SQLITE3" != "yes"; then
        HYSS_ADD_SOURCES(HYSS_EXT_DIR(sqlite3), libsqlite/sqlite3.c)
      fi

      HYSS_NEW_EXTENSION(pdo_sqlite,
        $hyss_pdo_sqlite_sources_core,
        $ext_shared,,-DPDO_SQLITE_BUNDLED=1 $other_flags $threadsafe_flags -I$pdo_cv_inc_path)

      HYSS_SUBST(PDO_SQLITE_SHARED_LIBADD)
      HYSS_ADD_EXTENSION_DEP(pdo_sqlite, sqlite3)
      HYSS_ADD_INCLUDE($abs_srcdir/extslib/sqlite3/libsqlite)

      AC_CHECK_FUNCS(usleep nanosleep)
      AC_CHECK_HEADERS(time.h)
  fi

  dnl Solaris fix
  HYSS_CHECK_LIBRARY(rt, fdatasync, [HYSS_ADD_LIBRARY(rt,, PDO_SQLITE_SHARED_LIBADD)])

  ifdef([HYSS_ADD_EXTENSION_DEP],
  [
    HYSS_ADD_EXTENSION_DEP(pdo_sqlite, pdo)
  ])
fi
