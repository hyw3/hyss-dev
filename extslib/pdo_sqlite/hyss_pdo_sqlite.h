/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PDO_SQLITE_H
#define HYSS_PDO_SQLITE_H

extern gear_capi_entry pdo_sqlite_capi_entry;
#define hyssext_pdo_sqlite_ptr &pdo_sqlite_capi_entry

#include "hyss_version.h"
#define HYSS_PDO_SQLITE_VERSION HYSS_VERSION

#ifdef ZTS
#include "hypbc.h"
#endif

HYSS_MINIT_FUNCTION(pdo_sqlite);
HYSS_MSHUTDOWN_FUNCTION(pdo_sqlite);
HYSS_RINIT_FUNCTION(pdo_sqlite);
HYSS_RSHUTDOWN_FUNCTION(pdo_sqlite);
HYSS_MINFO_FUNCTION(pdo_sqlite);

/*
  	Declare any global variables you may need between the BEGIN
	and END macros here:

GEAR_BEGIN_CAPI_GLOBALS(pdo_sqlite)
	long  global_value;
	char *global_string;
GEAR_END_CAPI_GLOBALS(pdo_sqlite)
*/

#ifdef ZTS
#define PDO_SQLITE_G(v) PBCG(pdo_sqlite_globals_id, gear_pdo_sqlite_globals *, v)
#else
#define PDO_SQLITE_G(v) (pdo_sqlite_globals.v)
#endif

#endif	/* HYSS_PDO_SQLITE_H */
