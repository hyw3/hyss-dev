/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "pdo/hyss_pdo.h"
#include "pdo/hyss_pdo_driver.h"
#include "hyss_pdo_sqlite.h"
#include "hyss_pdo_sqlite_int.h"
#include "gear_exceptions.h"

/* {{{ pdo_sqlite_functions[] */
static const gear_function_entry pdo_sqlite_functions[] = {
	HYSS_FE_END
};
/* }}} */

/* {{{ pdo_sqlite_deps
 */
static const gear_capi_dep pdo_sqlite_deps[] = {
	GEAR_CAPI_REQUIRED("pdo")
	GEAR_CAPI_END
};
/* }}} */

/* {{{ pdo_sqlite_capi_entry
 */
gear_capi_entry pdo_sqlite_capi_entry = {
	STANDARD_CAPI_HEADER_EX, NULL,
	pdo_sqlite_deps,
	"pdo_sqlite",
	pdo_sqlite_functions,
	HYSS_MINIT(pdo_sqlite),
	HYSS_MSHUTDOWN(pdo_sqlite),
	NULL,
	NULL,
	HYSS_MINFO(pdo_sqlite),
	HYSS_PDO_SQLITE_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#if defined(COMPILE_DL_PDO_SQLITE) || defined(COMPILE_DL_PDO_SQLITE_EXTERNAL)
GEAR_GET_CAPI(pdo_sqlite)
#endif

/* {{{ HYSS_MINIT_FUNCTION */
HYSS_MINIT_FUNCTION(pdo_sqlite)
{
#ifdef SQLITE_DETERMINISTIC
	REGISTER_PDO_CLASS_CONST_LONG("SQLITE_DETERMINISTIC", (gear_long)SQLITE_DETERMINISTIC);
#endif

	REGISTER_PDO_CLASS_CONST_LONG("SQLITE_ATTR_OPEN_FLAGS", (gear_long)PDO_SQLITE_ATTR_OPEN_FLAGS);
	REGISTER_PDO_CLASS_CONST_LONG("SQLITE_OPEN_READONLY", (gear_long)SQLITE_OPEN_READONLY);
	REGISTER_PDO_CLASS_CONST_LONG("SQLITE_OPEN_READWRITE", (gear_long)SQLITE_OPEN_READWRITE);
	REGISTER_PDO_CLASS_CONST_LONG("SQLITE_OPEN_CREATE", (gear_long)SQLITE_OPEN_CREATE);

	return hyss_pdo_register_driver(&pdo_sqlite_driver);
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION */
HYSS_MSHUTDOWN_FUNCTION(pdo_sqlite)
{
	hyss_pdo_unregister_driver(&pdo_sqlite_driver);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(pdo_sqlite)
{
	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "PDO Driver for SQLite 3.x", "enabled");
	hyss_info_print_table_row(2, "SQLite Library", sqlite3_libversion());
	hyss_info_print_table_end();
}
/* }}} */

