/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_XMLREADER_H
#define HYSS_XMLREADER_H

extern gear_capi_entry xmlreader_capi_entry;
#define hyssext_xmlreader_ptr &xmlreader_capi_entry

#include "hyss_version.h"
#define HYSS_XMLREADER_VERSION HYSS_VERSION

#ifdef ZTS
#include "hypbc.h"
#endif

#include "extslib/libxml/hyss_libxml.h"
#include <libxml/xmlreader.h>

/* If xmlreader and dom both are compiled statically,
   no DLL import should be used in xmlreader for dom symbols. */
#ifdef HYSS_WIN32
# if defined(HAVE_DOM) && !defined(COMPILE_DL_DOM) && !defined(COMPILE_DL_XMLREADER)
#  define DOM_LOCAL_DEFINES 1
# endif
#endif

typedef struct _xmlreader_object {
	xmlTextReaderPtr ptr;
	/* strings must be set in input buffer as copy is required */
	xmlParserInputBufferPtr input;
	void *schema;
	HashTable *prop_handler;
	gear_object  std;
} xmlreader_object;

static inline xmlreader_object *hyss_xmlreader_fetch_object(gear_object *obj) {
	return (xmlreader_object *)((char*)(obj) - XtOffsetOf(xmlreader_object, std));
}

#define Z_XMLREADER_P(zv) hyss_xmlreader_fetch_object(Z_OBJ_P((zv)))

HYSS_MINIT_FUNCTION(xmlreader);
HYSS_MSHUTDOWN_FUNCTION(xmlreader);
HYSS_MINFO_FUNCTION(xmlreader);

#define REGISTER_XMLREADER_CLASS_CONST_LONG(const_name, value) \
	gear_declare_class_constant_long(xmlreader_class_entry, const_name, sizeof(const_name)-1, (gear_long)value);

#ifdef ZTS
#define XMLREADER_G(v) PBCG(xmlreader_globals_id, gear_xmlreader_globals *, v)
#else
#define XMLREADER_G(v) (xmlreader_globals.v)
#endif

#endif	/* HYSS_XMLREADER_H */

