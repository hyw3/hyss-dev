dnl config.m4 for extension xmlreader

HYSS_ARG_ENABLE(xmlreader, whether to enable XMLReader support,
[  --disable-xmlreader     Disable XMLReader support], yes)

if test -z "$HYSS_LIBXML_DIR"; then
  HYSS_ARG_WITH(libxml-dir, libxml2 install dir,
  [  --with-libxml-dir=DIR   XMLReader: libxml2 install prefix], no, no)
fi

if test "$HYSS_XMLREADER" != "no"; then

  if test "$HYSS_LIBXML" = "no"; then
    AC_MSG_ERROR([XMLReader extension requires LIBXML extension, add --enable-libxml])
  fi

  HYSS_SETUP_LIBXML(XMLREADER_SHARED_LIBADD, [
    AC_DEFINE(HAVE_XMLREADER,1,[ ])
    HYSS_NEW_EXTENSION(xmlreader, hyss_xmlreader.c, $ext_shared)
    HYSS_ADD_EXTENSION_DEP(xmlreader, dom, true)
    HYSS_SUBST(XMLREADER_SHARED_LIBADD)
  ], [
    AC_MSG_ERROR([libxml2 not found. Please check your libxml2 installation.])
  ])
fi
