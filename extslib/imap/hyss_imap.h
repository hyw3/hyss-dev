/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_IMAP_H
#define HYSS_IMAP_H

#if HAVE_IMAP

#if defined(HAVE_IMAP2000) || defined(HAVE_IMAP2001)

 /* For now these appear on Windows, remove this check if it appears outside */
# ifdef HYSS_WIN32
 /* Undefine these LOG defines to avoid warnings */
#  undef LOG_EMERG
#  undef LOG_CRIT
#  undef LOG_ERR
#  undef LOG_WARNING
#  undef LOG_NOTICE
#  undef LOG_DEBUG

 /* c-client also redefines its own ftruncate */
#  undef ftruncate
# endif

 /* these are used for quota support */
# include "c-client.h"	/* includes mail.h and rfc822.h */
# include "imap4r1.h"	/* location of c-client quota functions */
#else
# include "mail.h"
# include "rfc822.h"
#endif

extern gear_capi_entry imap_capi_entry;
#define imap_capi_ptr &imap_capi_entry

#include "hyss_version.h"
#define HYSS_IMAP_VERSION HYSS_VERSION

/* Data types */

#ifdef IMAP41
#define LSIZE text.size
#define LTEXT text.data
#define DTYPE int
#define CONTENT_PART nested.part
#define CONTENT_MSG_BODY nested.msg->body
#define IMAPVER "Imap 4R1"
#else
#define LSIZE size
#define LTEXT text
#define DTYPE char
#define CONTENT_PART contents.part
#define CONTENT_MSG_BODY contents.msg.body
#define IMAPVER "Imap 4"
#endif


/* Determines how mm_list() and mm_lsub() are to return their results. */
typedef enum {
	FLIST_ARRAY,
	FLIST_OBJECT
} folderlist_style_t;

typedef struct hyss_imap_le_struct {
	MAILSTREAM *imap_stream;
	long flags;
} pils;

typedef struct hyss_imap_mailbox_struct {
	SIZEDTEXT text;
	DTYPE delimiter;
	long attributes;
	struct hyss_imap_mailbox_struct *next;
} FOBJECTLIST;

typedef struct hyss_imap_error_struct {
	SIZEDTEXT text;
	long errflg;
	struct hyss_imap_error_struct *next;
} ERRORLIST;

typedef struct _hyss_imap_message_struct {
	unsigned long msgid;
	struct _hyss_imap_message_struct *next;
} MESSAGELIST;


/* Functions */

HYSS_MINIT_FUNCTION(imap);
HYSS_RINIT_FUNCTION(imap);
HYSS_RSHUTDOWN_FUNCTION(imap);
HYSS_MINFO_FUNCTION(imap);

HYSS_FUNCTION(imap_open);
HYSS_FUNCTION(imap_popen);
HYSS_FUNCTION(imap_reopen);
HYSS_FUNCTION(imap_num_msg);
HYSS_FUNCTION(imap_num_recent);
HYSS_FUNCTION(imap_headers);
HYSS_FUNCTION(imap_headerinfo);
HYSS_FUNCTION(imap_rfc822_parse_headers);
HYSS_FUNCTION(imap_body);
HYSS_FUNCTION(imap_fetchstructure);
HYSS_FUNCTION(imap_fetchbody);
HYSS_FUNCTION(imap_fetchmime);
HYSS_FUNCTION(imap_savebody);
HYSS_FUNCTION(imap_gc);
HYSS_FUNCTION(imap_expunge);
HYSS_FUNCTION(imap_delete);
HYSS_FUNCTION(imap_undelete);
HYSS_FUNCTION(imap_check);
HYSS_FUNCTION(imap_close);
HYSS_FUNCTION(imap_mail_copy);
HYSS_FUNCTION(imap_mail_move);
HYSS_FUNCTION(imap_createmailbox);
HYSS_FUNCTION(imap_renamemailbox);
HYSS_FUNCTION(imap_deletemailbox);
HYSS_FUNCTION(imap_listmailbox);
HYSS_FUNCTION(imap_scanmailbox);
HYSS_FUNCTION(imap_subscribe);
HYSS_FUNCTION(imap_unsubscribe);
HYSS_FUNCTION(imap_append);
HYSS_FUNCTION(imap_ping);
HYSS_FUNCTION(imap_base64);
HYSS_FUNCTION(imap_qprint);
HYSS_FUNCTION(imap_8bit);
HYSS_FUNCTION(imap_binary);
HYSS_FUNCTION(imap_mailboxmsginfo);
HYSS_FUNCTION(imap_rfc822_write_address);
HYSS_FUNCTION(imap_rfc822_parse_adrlist);
HYSS_FUNCTION(imap_setflag_full);
HYSS_FUNCTION(imap_clearflag_full);
HYSS_FUNCTION(imap_sort);
HYSS_FUNCTION(imap_fetchheader);
HYSS_FUNCTION(imap_fetchtext);
HYSS_FUNCTION(imap_uid);
HYSS_FUNCTION(imap_msgno);
HYSS_FUNCTION(imap_list);
HYSS_FUNCTION(imap_list_full);
HYSS_FUNCTION(imap_listscan);
HYSS_FUNCTION(imap_lsub);
HYSS_FUNCTION(imap_lsub_full);
HYSS_FUNCTION(imap_create);
HYSS_FUNCTION(imap_rename);
HYSS_FUNCTION(imap_status);
HYSS_FUNCTION(imap_bodystruct);
HYSS_FUNCTION(imap_fetch_overview);
HYSS_FUNCTION(imap_mail_compose);
HYSS_FUNCTION(imap_alerts);
HYSS_FUNCTION(imap_errors);
HYSS_FUNCTION(imap_last_error);
HYSS_FUNCTION(imap_mail);
HYSS_FUNCTION(imap_search);
HYSS_FUNCTION(imap_utf8);
HYSS_FUNCTION(imap_utf7_decode);
HYSS_FUNCTION(imap_utf7_encode);
#ifdef HAVE_IMAP_MUTF7
HYSS_FUNCTION(imap_utf8_to_mutf7);
HYSS_FUNCTION(imap_mutf7_to_utf8);
#endif
HYSS_FUNCTION(imap_mime_header_decode);
HYSS_FUNCTION(imap_thread);
HYSS_FUNCTION(imap_timeout);

#if defined(HAVE_IMAP2000) || defined(HAVE_IMAP2001)
HYSS_FUNCTION(imap_get_quota);
HYSS_FUNCTION(imap_get_quotaroot);
HYSS_FUNCTION(imap_set_quota);
HYSS_FUNCTION(imap_setacl);
HYSS_FUNCTION(imap_getacl);
#endif


GEAR_BEGIN_CAPI_GLOBALS(imap)
	char *imap_user;
	char *imap_password;

	STRINGLIST *imap_alertstack;
	ERRORLIST *imap_errorstack;

	STRINGLIST *imap_folders;
	STRINGLIST *imap_folders_tail;
	STRINGLIST *imap_sfolders;
	STRINGLIST *imap_sfolders_tail;
	MESSAGELIST *imap_messages;
	MESSAGELIST *imap_messages_tail;
	FOBJECTLIST *imap_folder_objects;
	FOBJECTLIST *imap_folder_objects_tail;
	FOBJECTLIST *imap_sfolder_objects;
	FOBJECTLIST *imap_sfolder_objects_tail;

	folderlist_style_t folderlist_style;
	long status_flags;
	unsigned long status_messages;
	unsigned long status_recent;
	unsigned long status_unseen;
	unsigned long status_uidnext;
	unsigned long status_uidvalidity;
#if defined(HAVE_IMAP2000) || defined(HAVE_IMAP2001)
	zval **quota_return;
	zval *imap_acl_list;
#endif
	/* hyss_stream for hyss_mail_gets() */
	hyss_stream *gets_stream;
	gear_bool enable_rsh;
GEAR_END_CAPI_GLOBALS(imap)

#ifdef ZTS
# define IMAPG(v) PBCG(imap_globals_id, gear_imap_globals *, v)
#else
# define IMAPG(v) (imap_globals.v)
#endif

#else

#define imap_capi_ptr NULL

#endif

#define hyssext_imap_ptr imap_capi_ptr

#endif /* HYSS_IMAP_H */
