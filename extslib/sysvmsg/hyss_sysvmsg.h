/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SYSVMSG_H
#define HYSS_SYSVMSG_H

#if HAVE_SYSVMSG

extern gear_capi_entry sysvmsg_capi_entry;
#define hyssext_sysvmsg_ptr &sysvmsg_capi_entry

#include "hyss_version.h"
#define HYSS_SYSVMSG_VERSION HYSS_VERSION

#ifndef __USE_GNU
/* we want to use mtype instead of __mtype */
#define __USE_GNU
#endif

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#ifdef ZTS
#include "hypbc.h"
#endif

HYSS_MINIT_FUNCTION(sysvmsg);
HYSS_MINFO_FUNCTION(sysvmsg);

HYSS_FUNCTION(msg_get_queue);
HYSS_FUNCTION(msg_remove_queue);
HYSS_FUNCTION(msg_stat_queue);
HYSS_FUNCTION(msg_set_queue);
HYSS_FUNCTION(msg_send);
HYSS_FUNCTION(msg_receive);
HYSS_FUNCTION(msg_queue_exists);

typedef struct {
	key_t key;
	gear_long id;
} sysvmsg_queue_t;

struct hyss_msgbuf {
	gear_long mtype;
	char mtext[1];
};

#endif /* HAVE_SYSVMSG */

#endif	/* HYSS_SYSVMSG_H */

