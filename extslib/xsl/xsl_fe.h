/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XSL_FE_H
#define XSL_FE_H

extern const gear_function_entry hyss_xsl_xsltprocessor_class_functions[];
extern gear_class_entry *xsl_xsltprocessor_class_entry;

HYSS_FUNCTION(xsl_xsltprocessor_import_stylesheet);
HYSS_FUNCTION(xsl_xsltprocessor_transform_to_doc);
HYSS_FUNCTION(xsl_xsltprocessor_transform_to_uri);
HYSS_FUNCTION(xsl_xsltprocessor_transform_to_xml);
HYSS_FUNCTION(xsl_xsltprocessor_set_parameter);
HYSS_FUNCTION(xsl_xsltprocessor_get_parameter);
HYSS_FUNCTION(xsl_xsltprocessor_remove_parameter);
HYSS_FUNCTION(xsl_xsltprocessor_has_exslt_support);
HYSS_FUNCTION(xsl_xsltprocessor_register_hyss_functions);
HYSS_FUNCTION(xsl_xsltprocessor_set_profiling);
HYSS_FUNCTION(xsl_xsltprocessor_set_security_prefs);
HYSS_FUNCTION(xsl_xsltprocessor_get_security_prefs);

#endif

