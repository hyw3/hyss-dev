/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_XSL_H
#define HYSS_XSL_H

extern gear_capi_entry xsl_capi_entry;
#define hyssext_xsl_ptr &xsl_capi_entry

#include "hyss_version.h"
#define HYSS_XSL_VERSION HYSS_VERSION

#ifdef ZTS
#include "hypbc.h"
#endif

#include <libxslt/xsltconfig.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/xsltutils.h>
#include <libxslt/transform.h>
#include <libxslt/security.h>
#if HAVE_XSL_EXSLT
#include <libexslt/exslt.h>
#include <libexslt/exsltconfig.h>
#endif

#include "../dom/xml_common.h"
#include "xsl_fe.h"

#include <libxslt/extensions.h>
#include <libxml/xpathInternals.h>

#define XSL_SECPREF_NONE 0
#define XSL_SECPREF_READ_FILE 2
#define XSL_SECPREF_WRITE_FILE 4
#define XSL_SECPREF_CREATE_DIRECTORY 8
#define XSL_SECPREF_READ_NETWORK 16
#define XSL_SECPREF_WRITE_NETWORK 32
/* Default == disable all write access ==  XSL_SECPREF_WRITE_NETWORK | XSL_SECPREF_CREATE_DIRECTORY | XSL_SECPREF_WRITE_FILE */
#define XSL_SECPREF_DEFAULT 44

typedef struct _xsl_object {
	void *ptr;
	HashTable *prop_handler;
	zval handle;
	HashTable *parameter;
	int hasKeys;
	int registerHySSFunctions;
	HashTable *registered_hyssfunctions;
	HashTable *node_list;
	hyss_libxml_node_object *doc;
	char *profiling;
	gear_long securityPrefs;
	int securityPrefsSet;
	gear_object  std;
} xsl_object;

static inline xsl_object *hyss_xsl_fetch_object(gear_object *obj) {
	return (xsl_object *)((char*)(obj) - XtOffsetOf(xsl_object, std));
}

#define Z_XSL_P(zv) hyss_xsl_fetch_object(Z_OBJ_P((zv)))

void hyss_xsl_set_object(zval *wrapper, void *obj);
void xsl_objects_free_storage(gear_object *object);
void hyss_xsl_create_object(xsltStylesheetPtr obj, zval *wrapper_in, zval *return_value );

void xsl_ext_function_string_hyss(xmlXPathParserContextPtr ctxt, int nargs);
void xsl_ext_function_object_hyss(xmlXPathParserContextPtr ctxt, int nargs);

#define REGISTER_XSL_CLASS(ce, name, parent_ce, funcs, entry) \
INIT_CLASS_ENTRY(ce, name, funcs); \
ce.create_object = xsl_objects_new; \
entry = gear_register_internal_class_ex(&ce, parent_ce);

#define XSL_DOMOBJ_NEW(zval, obj, ret) \
	zval = hyss_xsl_create_object(obj, ret, zval, return_value); \
	if (ZVAL_IS_NULL(zval)) { \
		hyss_error_docref(NULL, E_WARNING, "Cannot create required DOM object"); \
		RETURN_FALSE; \
	}


HYSS_MINIT_FUNCTION(xsl);
HYSS_MSHUTDOWN_FUNCTION(xsl);
HYSS_RINIT_FUNCTION(xsl);
HYSS_RSHUTDOWN_FUNCTION(xsl);
HYSS_MINFO_FUNCTION(xsl);


/*
  	Declare any global variables you may need between the BEGIN
	and END macros here:

GEAR_BEGIN_CAPI_GLOBALS(xsl)
	long  global_value;
	char *global_string;
GEAR_END_CAPI_GLOBALS(xsl)
*/

#ifdef ZTS
#define XSL_G(v) PBCG(xsl_globals_id, gear_xsl_globals *, v)
#else
#define XSL_G(v) (xsl_globals.v)
#endif

#endif	/* HYSS_XSL_H */

