/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_xsl.h"

gear_class_entry *xsl_xsltprocessor_class_entry;
static gear_object_handlers xsl_object_handlers;

/* {{{ xsl_functions[]
 *
 * Every user visible function must have an entry in xsl_functions[].
 */
const gear_function_entry xsl_functions[] = {
	HYSS_FE_END
};
/* }}} */

static const gear_capi_dep xsl_deps[] = {
	GEAR_CAPI_REQUIRED("libxml")
	GEAR_CAPI_END
};

/* {{{ xsl_capi_entry
 */
gear_capi_entry xsl_capi_entry = {
	STANDARD_CAPI_HEADER_EX, NULL,
	xsl_deps,
	"xsl",
	xsl_functions,
	HYSS_MINIT(xsl),
	HYSS_MSHUTDOWN(xsl),
	NULL,
	NULL,
	HYSS_MINFO(xsl),
	HYSS_XSL_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_XSL
GEAR_GET_CAPI(xsl)
#endif

/* {{{ xsl_objects_free_storage */
void xsl_objects_free_storage(gear_object *object)
{
	xsl_object *intern = hyss_xsl_fetch_object(object);

	gear_object_std_dtor(&intern->std);

	gear_hash_destroy(intern->parameter);
	FREE_HASHTABLE(intern->parameter);

	gear_hash_destroy(intern->registered_hyssfunctions);
	FREE_HASHTABLE(intern->registered_hyssfunctions);

	if (intern->node_list) {
		gear_hash_destroy(intern->node_list);
		FREE_HASHTABLE(intern->node_list);
	}

	if (intern->doc) {
		hyss_libxml_decrement_doc_ref(intern->doc);
		efree(intern->doc);
	}

	if (intern->ptr) {
		/* free wrapper */
		if (((xsltStylesheetPtr) intern->ptr)->_private != NULL) {
			((xsltStylesheetPtr) intern->ptr)->_private = NULL;
		}

		xsltFreeStylesheet((xsltStylesheetPtr) intern->ptr);
		intern->ptr = NULL;
	}
	if (intern->profiling) {
		efree(intern->profiling);
	}
}
/* }}} */

/* {{{ xsl_objects_new */
gear_object *xsl_objects_new(gear_class_entry *class_type)
{
	xsl_object *intern;

	intern = gear_object_alloc(sizeof(xsl_object), class_type);
	intern->securityPrefs = XSL_SECPREF_DEFAULT;

	gear_object_std_init(&intern->std, class_type);
	object_properties_init(&intern->std, class_type);
	intern->parameter = gear_new_array(0);
	intern->registered_hyssfunctions = gear_new_array(0);

	intern->std.handlers = &xsl_object_handlers;
	return &intern->std;
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(xsl)
{

	gear_class_entry ce;

	memcpy(&xsl_object_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	xsl_object_handlers.offset = XtOffsetOf(xsl_object, std);
	xsl_object_handlers.clone_obj = NULL;
	xsl_object_handlers.free_obj = xsl_objects_free_storage;

	REGISTER_XSL_CLASS(ce, "XSLTProcessor", NULL, hyss_xsl_xsltprocessor_class_functions, xsl_xsltprocessor_class_entry);
#if HAVE_XSL_EXSLT
	exsltRegisterAll();
#endif

	/*xsltRegisterExtcAPIFunction ((const xmlChar *) "functionString",
				   (const xmlChar *) "http://hyss.hyang.org/xsl",
				   xsl_ext_function_string_hyss);
	xsltRegisterExtcAPIFunction ((const xmlChar *) "function",
				   (const xmlChar *) "http://hyss.hyang.org/xsl",
				   xsl_ext_function_object_hyss); */
	xsltSetGenericErrorFunc(NULL, hyss_libxml_error_handler);

	REGISTER_LONG_CONSTANT("XSL_CLONE_AUTO",      0,     CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("XSL_CLONE_NEVER",    -1,     CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("XSL_CLONE_ALWAYS",    1,     CONST_CS | CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("XSL_SECPREF_NONE",             XSL_SECPREF_NONE,             CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("XSL_SECPREF_READ_FILE",        XSL_SECPREF_READ_FILE,        CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("XSL_SECPREF_WRITE_FILE",       XSL_SECPREF_WRITE_FILE,       CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("XSL_SECPREF_CREATE_DIRECTORY", XSL_SECPREF_CREATE_DIRECTORY, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("XSL_SECPREF_READ_NETWORK",     XSL_SECPREF_READ_NETWORK,     CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("XSL_SECPREF_WRITE_NETWORK",    XSL_SECPREF_WRITE_NETWORK,    CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("XSL_SECPREF_DEFAULT",          XSL_SECPREF_DEFAULT,          CONST_CS | CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("LIBXSLT_VERSION",           LIBXSLT_VERSION,            CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("LIBXSLT_DOTTED_VERSION",  LIBXSLT_DOTTED_VERSION,     CONST_CS | CONST_PERSISTENT);

#if HAVE_XSL_EXSLT
	REGISTER_LONG_CONSTANT("LIBEXSLT_VERSION",           LIBEXSLT_VERSION,            CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("LIBEXSLT_DOTTED_VERSION",  LIBEXSLT_DOTTED_VERSION,     CONST_CS | CONST_PERSISTENT);
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ xsl_object_get_data */
zval *xsl_object_get_data(void *obj)
{
	zval *dom_wrapper;
	dom_wrapper = ((xsltStylesheetPtr) obj)->_private;
	return dom_wrapper;
}
/* }}} */

/* {{{ xsl_object_set_data */
static void xsl_object_set_data(void *obj, zval *wrapper)
{
	((xsltStylesheetPtr) obj)->_private = wrapper;
}
/* }}} */

/* {{{ hyss_xsl_set_object */
void hyss_xsl_set_object(zval *wrapper, void *obj)
{
	xsl_object *object;

	object = Z_XSL_P(wrapper);
	object->ptr = obj;
	xsl_object_set_data(obj, wrapper);
}
/* }}} */

/* {{{ hyss_xsl_create_object */
void hyss_xsl_create_object(xsltStylesheetPtr obj, zval *wrapper_in, zval *return_value )
{
	zval *wrapper;
	gear_class_entry *ce;

	if (!obj) {
		wrapper = wrapper_in;
		ZVAL_NULL(wrapper);
		return;
	}

	if ((wrapper = xsl_object_get_data((void *) obj))) {
		ZVAL_COPY(wrapper, wrapper_in);
		return;
	}

	if (!wrapper_in) {
		wrapper = return_value;
	} else {
		wrapper = wrapper_in;
	}


	ce = xsl_xsltprocessor_class_entry;

	if (!wrapper_in) {
		object_init_ex(wrapper, ce);
	}
	hyss_xsl_set_object(wrapper, (void *) obj);

	return;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION(xsl)
{
	/*xsltUnregisterExtcAPIFunction ((const xmlChar *) "functionString",
				   (const xmlChar *) "http://hyss.hyang.org/xsl");
	xsltUnregisterExtcAPIFunction ((const xmlChar *) "function",
				   (const xmlChar *) "http://hyss.hyang.org/xsl");*/
	xsltSetGenericErrorFunc(NULL, NULL);
	xsltCleanupGlobals();

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(xsl)
{
	hyss_info_print_table_start();
	{
		char buffer[128];
		int major, minor, subminor;

		hyss_info_print_table_row(2, "XSL", "enabled");
		major = xsltLibxsltVersion/10000;
		minor = (xsltLibxsltVersion - major * 10000) / 100;
		subminor = (xsltLibxsltVersion - major * 10000 - minor * 100);
		snprintf(buffer, 128, "%d.%d.%d", major, minor, subminor);
		hyss_info_print_table_row(2, "libxslt Version", buffer);
		major = xsltLibxmlVersion/10000;
		minor = (xsltLibxmlVersion - major * 10000) / 100;
		subminor = (xsltLibxmlVersion - major * 10000 - minor * 100);
		snprintf(buffer, 128, "%d.%d.%d", major, minor, subminor);
		hyss_info_print_table_row(2, "libxslt compiled against libxml Version", buffer);
	}
#if HAVE_XSL_EXSLT
	hyss_info_print_table_row(2, "EXSLT", "enabled");
	hyss_info_print_table_row(2, "libexslt Version", LIBEXSLT_DOTTED_VERSION);
#endif
	hyss_info_print_table_end();
}
/* }}} */

