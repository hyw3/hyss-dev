/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HYSS_ONIG_COMPAT_H
#define _HYSS_ONIG_COMPAT_H

#define re_pattern_buffer           hyss_mb_re_pattern_buffer
#define regex_t                     hyss_mb_regex_t
#define re_registers                hyss_mb_re_registers

#endif /* _HYSS_ONIG_COMPAT_H */
