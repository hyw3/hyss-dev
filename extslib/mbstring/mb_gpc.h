/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
/* }}} */

#if HAVE_MBSTRING
/* {{{ typedefs */
typedef struct _hyss_mb_encoding_handler_info_t {
	const char *separator;
	const mbfl_encoding *to_encoding;
	const mbfl_encoding **from_encodings;
	size_t num_from_encodings;
	int data_type;
	unsigned int report_errors : 1;
	enum mbfl_no_language to_language;
	enum mbfl_no_language from_language;
} hyss_mb_encoding_handler_info_t;

/* }}}*/

/* {{{ prototypes */
SAPI_POST_HANDLER_FUNC(hyss_mb_post_handler);
MBSTRING_API SAPI_TREAT_DATA_FUNC(mbstr_treat_data);

int _hyss_mb_enable_encoding_translation(int flag);
const mbfl_encoding *_hyss_mb_encoding_handler_ex(const hyss_mb_encoding_handler_info_t *info, zval *arg, char *res);
/* }}} */
#endif /* HAVE_MBSTRING */

