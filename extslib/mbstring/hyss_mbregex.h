/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HYSS_MBREGEX_H
#define _HYSS_MBREGEX_H

#if HAVE_MBREGEX

#include "hyss.h"
#include "gear.h"

/* {{{ HYSS_MBREGEX_FUNCTION_ENTRIES */
#define HYSS_MBREGEX_FUNCTION_ENTRIES \
	HYSS_FE(mb_regex_encoding,	arginfo_mb_regex_encoding) \
	HYSS_FE(mb_regex_set_options,	arginfo_mb_regex_set_options) \
	HYSS_FE(mb_ereg,			arginfo_mb_ereg) \
	HYSS_FE(mb_eregi,			arginfo_mb_eregi) \
	HYSS_FE(mb_ereg_replace,			arginfo_mb_ereg_replace) \
	HYSS_FE(mb_eregi_replace,			arginfo_mb_eregi_replace) \
	HYSS_FE(mb_ereg_replace_callback,			arginfo_mb_ereg_replace_callback) \
	HYSS_FE(mb_split,					arginfo_mb_split) \
	HYSS_FE(mb_ereg_match,			arginfo_mb_ereg_match) \
	HYSS_FE(mb_ereg_search,			arginfo_mb_ereg_search) \
	HYSS_FE(mb_ereg_search_pos,		arginfo_mb_ereg_search_pos) \
	HYSS_FE(mb_ereg_search_regs,		arginfo_mb_ereg_search_regs) \
	HYSS_FE(mb_ereg_search_init,		arginfo_mb_ereg_search_init) \
	HYSS_FE(mb_ereg_search_getregs,	arginfo_mb_ereg_search_getregs) \
	HYSS_FE(mb_ereg_search_getpos,	arginfo_mb_ereg_search_getpos) \
	HYSS_FE(mb_ereg_search_setpos,	arginfo_mb_ereg_search_setpos) \
	HYSS_DEP_FALIAS(mbregex_encoding,	mb_regex_encoding,	arginfo_mb_regex_encoding) \
	HYSS_DEP_FALIAS(mbereg,	mb_ereg,	arginfo_mb_ereg) \
	HYSS_DEP_FALIAS(mberegi,	mb_eregi,	arginfo_mb_eregi) \
	HYSS_DEP_FALIAS(mbereg_replace,	mb_ereg_replace,	arginfo_mb_ereg_replace) \
	HYSS_DEP_FALIAS(mberegi_replace,	mb_eregi_replace,	arginfo_mb_eregi_replace) \
	HYSS_DEP_FALIAS(mbsplit,	mb_split,	arginfo_mb_split) \
	HYSS_DEP_FALIAS(mbereg_match,	mb_ereg_match,	arginfo_mb_ereg_match) \
	HYSS_DEP_FALIAS(mbereg_search,	mb_ereg_search,	arginfo_mb_ereg_search) \
	HYSS_DEP_FALIAS(mbereg_search_pos,	mb_ereg_search_pos,	arginfo_mb_ereg_search_pos) \
	HYSS_DEP_FALIAS(mbereg_search_regs,	mb_ereg_search_regs,	arginfo_mb_ereg_search_regs) \
	HYSS_DEP_FALIAS(mbereg_search_init,	mb_ereg_search_init,	arginfo_mb_ereg_search_init) \
	HYSS_DEP_FALIAS(mbereg_search_getregs,	mb_ereg_search_getregs,	arginfo_mb_ereg_search_getregs) \
	HYSS_DEP_FALIAS(mbereg_search_getpos,	mb_ereg_search_getpos,	arginfo_mb_ereg_search_getpos) \
	HYSS_DEP_FALIAS(mbereg_search_setpos,	mb_ereg_search_setpos,	arginfo_mb_ereg_search_setpos)
/* }}} */

#define HYSS_MBREGEX_MAXCACHE 50

HYSS_MINIT_FUNCTION(mb_regex);
HYSS_MSHUTDOWN_FUNCTION(mb_regex);
HYSS_RINIT_FUNCTION(mb_regex);
HYSS_RSHUTDOWN_FUNCTION(mb_regex);
HYSS_MINFO_FUNCTION(mb_regex);

typedef struct _gear_mb_regex_globals gear_mb_regex_globals;

gear_mb_regex_globals *hyss_mb_regex_globals_alloc(void);
void hyss_mb_regex_globals_free(gear_mb_regex_globals *pglobals);
int hyss_mb_regex_set_mbctype(const char *enc);
int hyss_mb_regex_set_default_mbctype(const char *encname);
const char *hyss_mb_regex_get_mbctype(void);
const char *hyss_mb_regex_get_default_mbctype(void);

HYSS_FUNCTION(mb_regex_encoding);
HYSS_FUNCTION(mb_ereg);
HYSS_FUNCTION(mb_eregi);
HYSS_FUNCTION(mb_ereg_replace);
HYSS_FUNCTION(mb_eregi_replace);
HYSS_FUNCTION(mb_ereg_replace_callback);
HYSS_FUNCTION(mb_split);
HYSS_FUNCTION(mb_ereg_match);
HYSS_FUNCTION(mb_ereg_search);
HYSS_FUNCTION(mb_ereg_search_pos);
HYSS_FUNCTION(mb_ereg_search_regs);
HYSS_FUNCTION(mb_ereg_search_init);
HYSS_FUNCTION(mb_ereg_search_getregs);
HYSS_FUNCTION(mb_ereg_search_getpos);
HYSS_FUNCTION(mb_ereg_search_setpos);
HYSS_FUNCTION(mb_regex_set_options);

#endif /* HAVE_MBREGEX */

#endif /* _HYSS_MBREGEX_H */

