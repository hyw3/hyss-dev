/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "hyss_variables.h"
#include "libmbfl/mbfl/mbfilter_pass.h"
#include "mbstring.h"
#include "extslib/standard/hyss_string.h"
#include "extslib/standard/hyss_mail.h"
#include "extslib/standard/url.h"
#include "main/hyss_output.h"
#include "extslib/standard/info.h"

#include "hyss_variables.h"
#include "hyss_globals.h"
#include "rfc1867.h"
#include "hyss_content_types.h"
#include "SAPI.h"
#include "hypbc.h"

#include "mb_gpc.h"
/* }}} */

#if HAVE_MBSTRING

GEAR_EXTERN_CAPI_GLOBALS(mbstring)

/* {{{ MBSTRING_API SAPI_TREAT_DATA_FUNC(mbstr_treat_data)
 * http input processing */
MBSTRING_API SAPI_TREAT_DATA_FUNC(mbstr_treat_data)
{
	char *res = NULL, *separator=NULL;
	const char *c_var;
	zval v_array;
	int free_buffer=0;
	const mbfl_encoding *detected;
	hyss_mb_encoding_handler_info_t info;

	if (arg != PARSE_STRING) {
		char *value = MBSTRG(internal_encoding_name);
		_hyss_mb_ics_mbstring_internal_encoding_set(value, value ? strlen(value): 0);
	}

	if (!MBSTRG(encoding_translation)) {
		hyss_default_treat_data(arg, str, destArray);
		return;
	}

	switch (arg) {
		case PARSE_POST:
		case PARSE_GET:
		case PARSE_COOKIE:
			array_init(&v_array);
			switch (arg) {
				case PARSE_POST:
					ZVAL_COPY_VALUE(&PG(http_globals)[TRACK_VARS_POST], &v_array);
					break;
				case PARSE_GET:
					ZVAL_COPY_VALUE(&PG(http_globals)[TRACK_VARS_GET], &v_array);
					break;
				case PARSE_COOKIE:
					ZVAL_COPY_VALUE(&PG(http_globals)[TRACK_VARS_COOKIE], &v_array);
					break;
			}
			break;
		default:
			ZVAL_COPY_VALUE(&v_array, destArray);
			break;
	}

	if (arg == PARSE_POST) {
		sapi_handle_post(&v_array);
		return;
	}

	if (arg == PARSE_GET) {		/* GET data */
		c_var = SG(request_info).query_string;
		if (c_var && *c_var) {
			res = (char *) estrdup(c_var);
			free_buffer = 1;
		} else {
			free_buffer = 0;
		}
	} else if (arg == PARSE_COOKIE) {		/* Cookie data */
		c_var = SG(request_info).cookie_data;
		if (c_var && *c_var) {
			res = (char *) estrdup(c_var);
			free_buffer = 1;
		} else {
			free_buffer = 0;
		}
	} else if (arg == PARSE_STRING) {		/* String data */
		res = str;
		free_buffer = 1;
	}

	if (!res) {
		return;
	}

	switch (arg) {
		case PARSE_POST:
		case PARSE_GET:
		case PARSE_STRING:
			separator = (char *) estrdup(PG(arg_separator).input);
			break;
		case PARSE_COOKIE:
			separator = ";\0";
			break;
	}

	switch (arg) {
		case PARSE_POST:
			MBSTRG(http_input_identify_post) = NULL;
			break;
		case PARSE_GET:
			MBSTRG(http_input_identify_get) = NULL;
			break;
		case PARSE_COOKIE:
			MBSTRG(http_input_identify_cookie) = NULL;
			break;
		case PARSE_STRING:
			MBSTRG(http_input_identify_string) = NULL;
			break;
	}

	info.data_type              = arg;
	info.separator              = separator;
	info.report_errors          = 0;
	info.to_encoding            = MBSTRG(internal_encoding);
	info.to_language            = MBSTRG(language);
	info.from_encodings         = MBSTRG(http_input_list);
	info.num_from_encodings     = MBSTRG(http_input_list_size);
	info.from_language          = MBSTRG(language);

	MBSTRG(illegalchars) = 0;

	detected = _hyss_mb_encoding_handler_ex(&info, &v_array, res);
	MBSTRG(http_input_identify) = detected;

	if (detected) {
		switch(arg){
		case PARSE_POST:
			MBSTRG(http_input_identify_post) = detected;
			break;
		case PARSE_GET:
			MBSTRG(http_input_identify_get) = detected;
			break;
		case PARSE_COOKIE:
			MBSTRG(http_input_identify_cookie) = detected;
			break;
		case PARSE_STRING:
			MBSTRG(http_input_identify_string) = detected;
			break;
		}
	}

	if (arg != PARSE_COOKIE) {
		efree(separator);
	}

	if (free_buffer) {
		efree(res);
	}
}
/* }}} */

/* {{{ mbfl_no_encoding _hyss_mb_encoding_handler_ex() */
const mbfl_encoding *_hyss_mb_encoding_handler_ex(const hyss_mb_encoding_handler_info_t *info, zval *arg, char *res)
{
	char *var, *val;
	const char *s1, *s2;
	char *strtok_buf = NULL, **val_list = NULL;
	zval *array_ptr = (zval *) arg;
	size_t n, num, *len_list = NULL;
	size_t val_len, new_val_len;
	mbfl_string string, resvar, resval;
	const mbfl_encoding *from_encoding = NULL;
	mbfl_encoding_detector *identd = NULL;
	mbfl_buffer_converter *convd = NULL;

	mbfl_string_init_set(&string, info->to_language, info->to_encoding);
	mbfl_string_init_set(&resvar, info->to_language, info->to_encoding);
	mbfl_string_init_set(&resval, info->to_language, info->to_encoding);

	if (!res || *res == '\0') {
		goto out;
	}

	/* count the variables(separators) contained in the "res".
	 * separator may contain multiple separator chars.
	 */
	num = 1;
	for (s1=res; *s1 != '\0'; s1++) {
		for (s2=info->separator; *s2 != '\0'; s2++) {
			if (*s1 == *s2) {
				num++;
			}
		}
	}
	num *= 2; /* need space for variable name and value */

	val_list = (char **)ecalloc(num, sizeof(char *));
	len_list = (size_t *)ecalloc(num, sizeof(size_t));

	/* split and decode the query */
	n = 0;
	strtok_buf = NULL;
	var = hyss_strtok_r(res, info->separator, &strtok_buf);
	while (var)  {
		val = strchr(var, '=');
		if (val) { /* have a value */
			len_list[n] = hyss_url_decode(var, val-var);
			val_list[n] = var;
			n++;

			*val++ = '\0';
			val_list[n] = val;
			len_list[n] = hyss_url_decode(val, strlen(val));
		} else {
			len_list[n] = hyss_url_decode(var, strlen(var));
			val_list[n] = var;
			n++;

			val_list[n] = "";
			len_list[n] = 0;
		}
		n++;
		var = hyss_strtok_r(NULL, info->separator, &strtok_buf);
	}

	if (GEAR_SIZE_T_GT_GEAR_LONG(n, (PG(max_input_vars) * 2))) {
		hyss_error_docref(NULL, E_WARNING, "Input variables exceeded " GEAR_LONG_FMT ". To increase the limit change max_input_vars in hyss.ics.", PG(max_input_vars));
		goto out;
	}

	num = n; /* make sure to process initialized vars only */

	/* initialize converter */
	if (info->num_from_encodings == 0) {
		from_encoding = &mbfl_encoding_pass;
	} else if (info->num_from_encodings == 1) {
		from_encoding = info->from_encodings[0];
	} else {
		/* auto detect */
		from_encoding = NULL;
		identd = mbfl_encoding_detector_new(info->from_encodings, info->num_from_encodings, MBSTRG(strict_detection));
		if (identd != NULL) {
			n = 0;
			while (n < num) {
				string.val = (unsigned char *)val_list[n];
				string.len = len_list[n];
				if (mbfl_encoding_detector_feed(identd, &string)) {
					break;
				}
				n++;
			}
			from_encoding = mbfl_encoding_detector_judge(identd);
			mbfl_encoding_detector_delete(identd);
		}
		if (!from_encoding) {
			if (info->report_errors) {
				hyss_error_docref(NULL, E_WARNING, "Unable to detect encoding");
			}
			from_encoding = &mbfl_encoding_pass;
		}
	}

	convd = NULL;
	if (from_encoding != &mbfl_encoding_pass) {
		convd = mbfl_buffer_converter_new(from_encoding, info->to_encoding, 0);
		if (convd != NULL) {
			mbfl_buffer_converter_illegal_mode(convd, MBSTRG(current_filter_illegal_mode));
			mbfl_buffer_converter_illegal_substchar(convd, MBSTRG(current_filter_illegal_substchar));
		} else {
			if (info->report_errors) {
				hyss_error_docref(NULL, E_WARNING, "Unable to create converter");
			}
			goto out;
		}
	}

	/* convert encoding */
	string.encoding = from_encoding;

	n = 0;
	while (n < num) {
		string.val = (unsigned char *)val_list[n];
		string.len = len_list[n];
		if (convd != NULL && mbfl_buffer_converter_feed_result(convd, &string, &resvar) != NULL) {
			var = (char *)resvar.val;
		} else {
			var = val_list[n];
		}
		n++;
		string.val = (unsigned char *)val_list[n];
		string.len = len_list[n];
		if (convd != NULL && mbfl_buffer_converter_feed_result(convd, &string, &resval) != NULL) {
			val = (char *)resval.val;
			val_len = resval.len;
		} else {
			val = val_list[n];
			val_len = len_list[n];
		}
		n++;
		/* we need val to be emalloc()ed */
		val = estrndup(val, val_len);
		if (sapi_capi.input_filter(info->data_type, var, &val, val_len, &new_val_len)) {
			/* add variable to symbol table */
			hyss_register_variable_safe(var, val, new_val_len, array_ptr);
		}
		efree(val);

		if (convd != NULL){
			mbfl_string_clear(&resvar);
			mbfl_string_clear(&resval);
		}
	}

out:
	if (convd != NULL) {
		MBSTRG(illegalchars) += mbfl_buffer_illegalchars(convd);
		mbfl_buffer_converter_delete(convd);
	}
	if (val_list != NULL) {
		efree((void *)val_list);
	}
	if (len_list != NULL) {
		efree((void *)len_list);
	}

	return from_encoding;
}
/* }}} */

/* {{{ SAPI_POST_HANDLER_FUNC(hyss_mb_post_handler) */
SAPI_POST_HANDLER_FUNC(hyss_mb_post_handler)
{
	const mbfl_encoding *detected;
	hyss_mb_encoding_handler_info_t info;
	gear_string *post_data_str = NULL;

	MBSTRG(http_input_identify_post) = NULL;

	info.data_type              = PARSE_POST;
	info.separator              = "&";
	info.report_errors          = 0;
	info.to_encoding            = MBSTRG(internal_encoding);
	info.to_language            = MBSTRG(language);
	info.from_encodings         = MBSTRG(http_input_list);
	info.num_from_encodings     = MBSTRG(http_input_list_size);
	info.from_language          = MBSTRG(language);

	hyss_stream_rewind(SG(request_info).request_body);
	post_data_str = hyss_stream_copy_to_mem(SG(request_info).request_body, HYSS_STREAM_COPY_ALL, 0);
	detected = _hyss_mb_encoding_handler_ex(&info, arg, post_data_str ? ZSTR_VAL(post_data_str) : NULL);
	if (post_data_str) {
		gear_string_release_ex(post_data_str, 0);
	}

	MBSTRG(http_input_identify) = detected;
	if (detected) {
		MBSTRG(http_input_identify_post) = detected;
	}
}
/* }}} */

#endif /* HAVE_MBSTRING */

