/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MBSTRING_H
#define _MBSTRING_H

#ifdef COMPILE_DL_MBSTRING
#undef HAVE_MBSTRING
#define HAVE_MBSTRING 1
#endif

#include "hyss_version.h"
#define HYSS_MBSTRING_VERSION HYSS_VERSION

#ifdef HYSS_WIN32
#	undef MBSTRING_API
#	ifdef MBSTRING_EXPORTS
#		define MBSTRING_API __declspec(dllexport)
#	elif defined(COMPILE_DL_MBSTRING)
#		define MBSTRING_API __declspec(dllimport)
#	else
#		define MBSTRING_API /* nothing special */
#	endif
#elif defined(__GNUC__) && __GNUC__ >= 4
#	undef MBSTRING_API
#	define MBSTRING_API __attribute__ ((visibility("default")))
#else
#	undef MBSTRING_API
#	define MBSTRING_API /* nothing special */
#endif


#if HAVE_MBSTRING

#include "libmbfl/mbfl/mbfilter.h"
#include "SAPI.h"

#define HYSS_MBSTRING_API 20021024

extern gear_capi_entry mbstring_capi_entry;
#define mbstring_capi_ptr &mbstring_capi_entry

HYSS_MINIT_FUNCTION(mbstring);
HYSS_MSHUTDOWN_FUNCTION(mbstring);
HYSS_RINIT_FUNCTION(mbstring);
HYSS_RSHUTDOWN_FUNCTION(mbstring);
HYSS_MINFO_FUNCTION(mbstring);

/* functions in hyss_unicode.c */
HYSS_FUNCTION(mb_convert_case);
HYSS_FUNCTION(mb_strtoupper);
HYSS_FUNCTION(mb_strtolower);

/* hyss function registration */
HYSS_FUNCTION(mb_language);
HYSS_FUNCTION(mb_internal_encoding);
HYSS_FUNCTION(mb_http_input);
HYSS_FUNCTION(mb_http_output);
HYSS_FUNCTION(mb_detect_order);
HYSS_FUNCTION(mb_substitute_character);
HYSS_FUNCTION(mb_preferred_mime_name);
HYSS_FUNCTION(mb_parse_str);
HYSS_FUNCTION(mb_output_handler);
HYSS_FUNCTION(mb_strlen);
HYSS_FUNCTION(mb_strpos);
HYSS_FUNCTION(mb_strrpos);
HYSS_FUNCTION(mb_stripos);
HYSS_FUNCTION(mb_strripos);
HYSS_FUNCTION(mb_strstr);
HYSS_FUNCTION(mb_strrchr);
HYSS_FUNCTION(mb_stristr);
HYSS_FUNCTION(mb_strrichr);
HYSS_FUNCTION(mb_substr_count);
HYSS_FUNCTION(mb_substr);
HYSS_FUNCTION(mb_strcut);
HYSS_FUNCTION(mb_strwidth);
HYSS_FUNCTION(mb_strimwidth);
HYSS_FUNCTION(mb_convert_encoding);
HYSS_FUNCTION(mb_detect_encoding);
HYSS_FUNCTION(mb_list_encodings);
HYSS_FUNCTION(mb_encoding_aliases);
HYSS_FUNCTION(mb_convert_kana);
HYSS_FUNCTION(mb_encode_mimeheader);
HYSS_FUNCTION(mb_decode_mimeheader);
HYSS_FUNCTION(mb_convert_variables);
HYSS_FUNCTION(mb_encode_numericentity);
HYSS_FUNCTION(mb_decode_numericentity);
HYSS_FUNCTION(mb_send_mail);
HYSS_FUNCTION(mb_get_info);
HYSS_FUNCTION(mb_check_encoding);
HYSS_FUNCTION(mb_ord);
HYSS_FUNCTION(mb_chr);
HYSS_FUNCTION(mb_scrub);


MBSTRING_API char *hyss_mb_safe_strrchr_ex(const char *s, unsigned int c,
                                    size_t nbytes, const mbfl_encoding *enc);
MBSTRING_API char *hyss_mb_safe_strrchr(const char *s, unsigned int c,
                                 size_t nbytes);

MBSTRING_API char *hyss_mb_convert_encoding_ex(
		const char *input, size_t length,
		const mbfl_encoding *to_encoding, const mbfl_encoding *from_encoding, size_t *output_len);
MBSTRING_API char * hyss_mb_convert_encoding(const char *input, size_t length,
                                      const char *_to_encoding,
                                      const char *_from_encodings,
                                      size_t *output_len);

MBSTRING_API int hyss_mb_check_encoding_list(const char *encoding_list);

MBSTRING_API size_t hyss_mb_mbchar_bytes_ex(const char *s, const mbfl_encoding *enc);
MBSTRING_API size_t hyss_mb_mbchar_bytes(const char *s);

MBSTRING_API size_t hyss_mb_stripos(int mode, const char *old_haystack, size_t old_haystack_len, const char *old_needle, size_t old_needle_len, gear_long offset, const char *from_encoding);
MBSTRING_API int hyss_mb_check_encoding(const char *input, size_t length, const char *enc);

/* internal use only */
int _hyss_mb_ics_mbstring_internal_encoding_set(const char *new_value, size_t new_value_length);

GEAR_BEGIN_CAPI_GLOBALS(mbstring)
	char *internal_encoding_name;
	const mbfl_encoding *internal_encoding;
	const mbfl_encoding *current_internal_encoding;
	const mbfl_encoding *http_output_encoding;
	const mbfl_encoding *current_http_output_encoding;
	const mbfl_encoding *http_input_identify;
	const mbfl_encoding *http_input_identify_get;
	const mbfl_encoding *http_input_identify_post;
	const mbfl_encoding *http_input_identify_cookie;
	const mbfl_encoding *http_input_identify_string;
	const mbfl_encoding **http_input_list;
	size_t http_input_list_size;
	const mbfl_encoding **detect_order_list;
	size_t detect_order_list_size;
	const mbfl_encoding **current_detect_order_list;
	size_t current_detect_order_list_size;
	enum mbfl_no_encoding *default_detect_order_list;
	size_t default_detect_order_list_size;
	int filter_illegal_mode;
	int filter_illegal_substchar;
	int current_filter_illegal_mode;
	int current_filter_illegal_substchar;
	gear_long func_overload;
	enum mbfl_no_language language;
	gear_bool encoding_translation;
	gear_bool strict_detection;
	size_t illegalchars;
	mbfl_buffer_converter *outconv;
    void *http_output_conv_mimetypes;
#if HAVE_MBREGEX
    struct _gear_mb_regex_globals *mb_regex_globals;
    gear_long regex_stack_limit;
#endif
	char *last_used_encoding_name;
	const mbfl_encoding *last_used_encoding;
GEAR_END_CAPI_GLOBALS(mbstring)

#define MB_OVERLOAD_MAIL 1
#define MB_OVERLOAD_STRING 2
#define MB_OVERLOAD_REGEX 4

struct mb_overload_def {
	int type;
	char *orig_func;
	char *ovld_func;
	char *save_func;
};

#define MBSTRG(v) GEAR_CAPI_GLOBALS_ACCESSOR(mbstring, v)

#if defined(ZTS) && defined(COMPILE_DL_MBSTRING)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#else	/* HAVE_MBSTRING */

#define mbstring_capi_ptr NULL

#endif	/* HAVE_MBSTRING */

#define hyssext_mbstring_ptr mbstring_capi_ptr

#endif		/* _MBSTRING_H */

