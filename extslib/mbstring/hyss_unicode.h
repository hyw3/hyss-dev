/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_UNICODE_H
#define HYSS_UNICODE_H

#if HAVE_MBSTRING

#define UC_MN  0 /* Mark, Non-Spacing          */
#define UC_MC  1 /* Mark, Spacing Combining    */
#define UC_ME  2 /* Mark, Enclosing            */
#define UC_ND  3 /* Number, Decimal Digit      */
#define UC_NL  4 /* Number, Letter             */
#define UC_NO  5 /* Number, Other              */
#define UC_ZS  6 /* Separator, Space           */
#define UC_ZL  7 /* Separator, Line            */
#define UC_ZP  8 /* Separator, Paragraph       */
#define UC_CC  9 /* Other, Control             */
#define UC_CF 10 /* Other, Format              */
#define UC_OS 11 /* Other, Surrogate           */
#define UC_CO 12 /* Other, Private Use         */
#define UC_CN 13 /* Other, Not Assigned        */
#define UC_LU 14 /* Letter, Uppercase          */
#define UC_LL 15 /* Letter, Lowercase          */
#define UC_LT 16 /* Letter, Titlecase          */
#define UC_LM 17 /* Letter, Modifier           */
#define UC_LO 18 /* Letter, Other              */
#define UC_PC 19 /* Punctuation, Connector     */
#define UC_PD 20 /* Punctuation, Dash          */
#define UC_PS 21 /* Punctuation, Open          */
#define UC_PE 22 /* Punctuation, Close         */
#define UC_PO 23 /* Punctuation, Other         */
#define UC_SM 24 /* Symbol, Math               */
#define UC_SC 25 /* Symbol, Currency           */
#define UC_SK 26 /* Symbol, Modifier           */
#define UC_SO 27 /* Symbol, Other              */
#define UC_L  28 /* Left-To-Right              */
#define UC_R  29 /* Right-To-Left              */
#define UC_EN 30 /* European Number            */
#define UC_ES 31 /* European Number Separator  */
#define UC_ET 32 /* European Number Terminator */
#define UC_AN 33 /* Arabic Number              */
#define UC_CS 34 /* Common Number Separator    */
#define UC_B  35 /* Block Separator            */
#define UC_S  36 /* Segment Separator          */
#define UC_WS 37 /* Whitespace                 */
#define UC_ON 38 /* Other Neutrals             */
#define UC_PI 39 /* Punctuation, Initial       */
#define UC_PF 40 /* Punctuation, Final         */
#define UC_AL 41 /* Arabic Letter              */

/* Derived properties from DerivedCoreProperties.txt */
#define UC_CASED          42
#define UC_CASE_IGNORABLE 43


MBSTRING_API int hyss_unicode_is_prop(unsigned long code, ...);
MBSTRING_API int hyss_unicode_is_prop1(unsigned long code, int prop);

MBSTRING_API char *hyss_unicode_convert_case(
		int case_mode, const char *srcstr, size_t srclen, size_t *ret_len,
		const mbfl_encoding *src_encoding, int illegal_mode, int illegal_substchar);

#define HYSS_UNICODE_CASE_UPPER        0
#define HYSS_UNICODE_CASE_LOWER        1
#define HYSS_UNICODE_CASE_TITLE        2
#define HYSS_UNICODE_CASE_FOLD         3
#define HYSS_UNICODE_CASE_UPPER_SIMPLE 4
#define HYSS_UNICODE_CASE_LOWER_SIMPLE 5
#define HYSS_UNICODE_CASE_TITLE_SIMPLE 6
#define HYSS_UNICODE_CASE_FOLD_SIMPLE  7
#define HYSS_UNICODE_CASE_MODE_MAX     7

/* Optimize the common ASCII case for lower/upper */

static inline int hyss_unicode_is_lower(unsigned long code) {
	if (code < 0x80) {
		return code >= 0x61 && code <= 0x7A;
	} else {
		return hyss_unicode_is_prop1(code, UC_LL);
	}
}

static inline int hyss_unicode_is_upper(unsigned long code) {
	if (code < 0x80) {
		return code >= 0x41 && code <= 0x5A;
	} else {
		return hyss_unicode_is_prop1(code, UC_LU);
	}
}

#define hyss_unicode_is_alpha(cc) hyss_unicode_is_prop(cc, UC_LU, UC_LL, UC_LM, UC_LO, UC_LT, -1)
#define hyss_unicode_is_digit(cc) hyss_unicode_is_prop1(cc, UC_ND)
#define hyss_unicode_is_alnum(cc) hyss_unicode_is_prop(cc, UC_LU, UC_LL, UC_LM, UC_LO, UC_LT, UC_ND, -1)
#define hyss_unicode_is_cntrl(cc) hyss_unicode_is_prop(cc, UC_CC, UC_CF, -1)
#define hyss_unicode_is_blank(cc) hyss_unicode_is_prop1(cc, UC_ZS)
#define hyss_unicode_is_punct(cc) hyss_unicode_is_prop(cc, UC_PD, UC_PS, UC_PE, UC_PO, UC_PI, UC_PF, -1)
#define hyss_unicode_is_graph(cc) hyss_unicode_is_prop(cc, UC_MN, UC_MC, UC_ME, UC_ND, UC_NL, UC_NO, \
                               UC_LU, UC_LL, UC_LT, UC_LM, UC_LO, UC_PC, UC_PD, \
                               UC_PS, UC_PE, UC_PO, UC_SM, UC_SM, UC_SC, UC_SK, \
                               UC_SO, UC_PI, UC_PF, -1)
#define hyss_unicode_is_print(cc) hyss_unicode_is_prop(cc, UC_MN, UC_MC, UC_ME, UC_ND, UC_NL, UC_NO, \
                               UC_LU, UC_LL, UC_LT, UC_LM, UC_LO, UC_PC, UC_PD, \
                               UC_PS, UC_PE, UC_PO, UC_SM, UC_SM, UC_SC, UC_SK, \
                               UC_SO, UC_ZS, UC_PI, UC_PF, -1)
#define hyss_unicode_is_title(cc) hyss_unicode_is_prop1(cc, UC_LT)

#define hyss_unicode_is_isocntrl(cc) hyss_unicode_is_prop1(cc, UC_CC)
#define hyss_unicode_is_fmtcntrl(cc) hyss_unicode_is_prop1(cc, UC_CF)

#define hyss_unicode_is_symbol(cc) hyss_unicode_is_prop(cc, UC_SM, UC_SC, UC_SO, UC_SK, -1)
#define hyss_unicode_is_number(cc) hyss_unicode_is_prop(cc, UC_ND, UC_NO, UC_NL, -1)
#define hyss_unicode_is_nonspacing(cc) hyss_unicode_is_prop1(cc, UC_MN)
#define hyss_unicode_is_openpunct(cc) hyss_unicode_is_prop1(cc, UC_PS)
#define hyss_unicode_is_closepunct(cc) hyss_unicode_is_prop1(cc, UC_PE)
#define hyss_unicode_is_initialpunct(cc) hyss_unicode_is_prop1(cc, UC_PI)
#define hyss_unicode_is_finalpunct(cc) hyss_unicode_is_prop1(cc, UC_PF)

/*
 * Directionality macros.
 */
#define hyss_unicode_is_rtl(cc) hyss_unicode_is_prop1(cc, UC_R)
#define hyss_unicode_is_ltr(cc) hyss_unicode_is_prop1(cc, UC_L)
#define hyss_unicode_is_strong(cc) hyss_unicode_is_prop(cc, UC_L, UC_R, -1)
#define hyss_unicode_is_weak(cc) hyss_unicode_is_prop(cc, UC_EN, UC_ES, UC_ET, UC_AN, UC_CS, -1)
#define hyss_unicode_is_neutral(cc) hyss_unicode_is_prop(cc, UC_B, UC_S, UC_WS, UC_ON, -1)
#define hyss_unicode_is_separator(cc) hyss_unicode_is_prop(cc, UC_B, UC_S, -1)

/*
 * Other macros inspired by John Cowan.
 */
#define hyss_unicode_is_mark(cc) hyss_unicode_is_prop(cc, UC_MN, UC_MC, UC_ME, -1)
#define hyss_unicode_is_modif(cc) hyss_unicode_is_prop1(cc, UC_LM)
#define hyss_unicode_is_letnum(cc) hyss_unicode_is_prop1(cc, UC_NL)
#define hyss_unicode_is_connect(cc) hyss_unicode_is_prop1(cc, UC_PC)
#define hyss_unicode_is_dash(cc) hyss_unicode_is_prop1(cc, UC_PD)
#define hyss_unicode_is_math(cc) hyss_unicode_is_prop1(cc, UC_SM)
#define hyss_unicode_is_currency(cc) hyss_unicode_is_prop1(cc, UC_SC)
#define hyss_unicode_is_modifsymbol(cc) hyss_unicode_is_prop1(cc, UC_SK)
#define hyss_unicode_is_nsmark(cc) hyss_unicode_is_prop1(cc, UC_MN)
#define hyss_unicode_is_spmark(cc) hyss_unicode_is_prop1(cc, UC_MC)
#define hyss_unicode_is_enclosing(cc) hyss_unicode_is_prop1(cc, UC_ME)
#define hyss_unicode_is_private(cc) hyss_unicode_is_prop1(cc, UC_CO)
#define hyss_unicode_is_surrogate(cc) hyss_unicode_is_prop1(cc, UC_OS)
#define hyss_unicode_is_lsep(cc) hyss_unicode_is_prop1(cc, UC_ZL)
#define hyss_unicode_is_psep(cc) hyss_unicode_is_prop1(cc, UC_ZP)

#define hyss_unicode_is_identstart(cc) hyss_unicode_is_prop(cc, UC_LU, UC_LL, UC_LT, UC_LO, UC_NL, -1)
#define hyss_unicode_is_identpart(cc) hyss_unicode_is_prop(cc, UC_LU, UC_LL, UC_LT, UC_LO, UC_NL, \
                                   UC_MN, UC_MC, UC_ND, UC_PC, UC_CF, -1)

/*
 * Other miscellaneous character property macros.
 */
#define hyss_unicode_is_han(cc) (((cc) >= 0x4e00 && (cc) <= 0x9fff) ||\
                     ((cc) >= 0xf900 && (cc) <= 0xfaff))
#define hyss_unicode_is_hangul(cc) ((cc) >= 0xac00 && (cc) <= 0xd7ff)

/*
 * Derived core properties.
 */

#define hyss_unicode_is_cased(cc) hyss_unicode_is_prop1(cc, UC_CASED)
#define hyss_unicode_is_case_ignorable(cc) hyss_unicode_is_prop1(cc, UC_CASE_IGNORABLE)


#endif


#endif /* HYSS_UNICODE_H */
