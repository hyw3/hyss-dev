dnl config.m4 for extension sysvsem

HYSS_ARG_ENABLE(sysvsem,whether to enable System V semaphore support,
[  --enable-sysvsem        Enable System V semaphore support])

if test "$HYSS_SYSVSEM" != "no"; then
 HYSS_NEW_EXTENSION(sysvsem, sysvsem.c, $ext_shared)
 AC_DEFINE(HAVE_SYSVSEM, 1, [ ])
 AC_CACHE_CHECK(for union semun,hyss_cv_semun,
   AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
   ]], [[union semun x;]])],[
     hyss_cv_semun=yes
   ],[
     hyss_cv_semun=no
   ])
 )
 if test "$hyss_cv_semun" = "yes"; then
   AC_DEFINE(HAVE_SEMUN, 1, [ ])
 else
   AC_DEFINE(HAVE_SEMUN, 0, [ ])
 fi
fi
