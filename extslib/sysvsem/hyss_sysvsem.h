/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SYSVSEM_H
#define HYSS_SYSVSEM_H

#if HAVE_SYSVSEM

extern gear_capi_entry sysvsem_capi_entry;
#define sysvsem_capi_ptr &sysvsem_capi_entry

#include "hyss_version.h"
#define HYSS_SYSVSEM_VERSION HYSS_VERSION

HYSS_MINIT_FUNCTION(sysvsem);
HYSS_MINFO_FUNCTION(sysvsem);
HYSS_FUNCTION(sem_get);
HYSS_FUNCTION(sem_acquire);
HYSS_FUNCTION(sem_release);
HYSS_FUNCTION(sem_remove);

typedef struct {
	int le_sem;
} sysvsem_capi;

typedef struct {
	int id;				/* For error reporting. */
	int key;			/* For error reporting. */
	int semid;			/* Returned by semget(). */
	int count;			/* Acquire count for auto-release. */
	int auto_release;		/* flag that says to auto-release. */
} sysvsem_sem;

extern sysvsem_capi hyss_sysvsem_capi;

#else

#define sysvsem_capi_ptr NULL

#endif

#define hyssext_sysvsem_ptr sysvsem_capi_ptr

#endif /* HYSS_SYSVSEM_H */
