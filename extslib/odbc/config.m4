dnl config.m4 for extension odbc

AC_DEFUN([HYSS_ODBC_CHECK_HEADER],[
if ! test -f "$ODBC_INCDIR/$1"; then
  AC_MSG_ERROR([ODBC header file '$ODBC_INCDIR/$1' not found!])
fi
])

dnl
dnl Figure out which library file to link with for the Solid support.
dnl
AC_DEFUN([HYSS_ODBC_FIND_SOLID_LIBS],[
  AC_MSG_CHECKING([Solid library file])
  ac_solid_uname_r=`uname -r 2>/dev/null`
  ac_solid_uname_s=`uname -s 2>/dev/null`
  case $ac_solid_uname_s in
    AIX) ac_solid_os=a3x;;   # a4x for AIX4/ Solid 2.3/3.0 only
    HP-UX) ac_solid_os=h9x;; # h1x for hpux11, h0x for hpux10
    IRIX) ac_solid_os=irx;;  # Solid 2.3(?)/ 3.0 only
    Linux)
      if ldd -v /bin/sh | grep GLIBC > /dev/null; then
        AC_DEFINE(SS_LINUX,1,[Needed in sqlunix.h ])
        ac_solid_os=l2x
      else
        AC_DEFINE(SS_LINUX,1,[Needed in sqlunix.h ])
        ac_solid_os=lux
      fi;;
    SunOS)
      ac_solid_os=ssx;; # should we deal with SunOS 4?
    FreeBSD)
      if test `expr $ac_solid_uname_r : '\(.\)'` -gt "2"; then
        AC_DEFINE(SS_FBX,1,[Needed in sqlunix.h for wchar defs ])
        ac_solid_os=fex
      else
        AC_DEFINE(SS_FBX,1,[Needed in sqlunix.h for wchar defs ])
        ac_solid_os=fbx
      fi;;
  esac

  if test -f $1/soc${ac_solid_os}35.a; then
    ac_solid_version=35
    ac_solid_prefix=soc
  elif test -f $1/scl${ac_solid_os}30.a; then
    ac_solid_version=30
    ac_solid_prefix=scl
  elif test -f $1/scl${ac_solid_os}23.a; then
    ac_solid_version=23
    ac_solid_prefix=scl
  fi

#
# Check for the library files, and setup the ODBC_LIBS path...
#
if test ! -f $1/lib${ac_solid_prefix}${ac_solid_os}${ac_solid_version}.so -a \
  ! -f $1/lib${ac_solid_prefix}${ac_solid_os}${ac_solid_version}.a; then
  #
  # we have an error and should bail out, as we can't find the libs!
  #
  echo ""
  echo "*********************************************************************"
  echo "* Unable to locate $1/lib${ac_solid_prefix}${ac_solid_os}${ac_solid_version}.so or $1/lib${ac_solid_prefix}${ac_solid_os}${ac_solid_version}.a"
  echo "* Please correct this by creating the following links and reconfiguring:"
  echo "* $1/lib${ac_solid_prefix}${ac_solid_os}${ac_solid_version}.a -> $1/lib${ac_solid_prefix}${ac_solid_os}${ac_solid_version}.a"
  echo "* $1/${ac_solid_prefix}${ac_solid_os}${ac_solid_version}.so -> $1/lib${ac_solid_prefix}${ac_solid_os}${ac_solid_version}.so"
  echo "*********************************************************************"
else
  ODBC_LFLAGS=-L$1
  ODBC_LIBS=-l${ac_solid_prefix}${ac_solid_os}${ac_solid_version}
fi

  AC_MSG_RESULT(`echo $ODBC_LIBS | sed -e 's!.*/!!'`)
])


dnl
dnl Figure out which library file to link with for the Empress support.
dnl

AC_DEFUN([HYSS_ODBC_FIND_EMPRESS_LIBS],[
  AC_MSG_CHECKING([Empress library file])
  ODBC_LIBS=`echo $1/libempodbccl.so | cut -d' ' -f1`
  if test ! -f $ODBC_LIBS; then
    ODBC_LIBS=`echo $1/libempodbccl.so | cut -d' ' -f1`
  fi
  AC_MSG_RESULT(`echo $ODBC_LIBS | sed -e 's!.*/!!'`)
])

AC_DEFUN([HYSS_ODBC_FIND_EMPRESS_BCS_LIBS],[
  AC_MSG_CHECKING([Empress local access library file])
  ODBCBCS_LIBS=`echo $1/libempodbcbcs.a | cut -d' ' -f1`
  if test ! -f $ODBCBCS_LIBS; then
    ODBCBCS_LIBS=`echo $1/libempodbcbcs.a | cut -d' ' -f1`
  fi
  AC_MSG_RESULT(`echo $ODBCBCS_LIBS | sed -e 's!.*/!!'`)
])

dnl
dnl configure options
dnl

HYSS_ARG_WITH(odbcver,,
[  --with-odbcver[=HEX]      Force support for the passed ODBC version. A hex number is expected, default 0x0350.
                          Use the special value of 0 to prevent an explicit ODBCVER to be defined. ], 0x0350)

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(adabas,,
[  --with-adabas[=DIR]       Include Adabas D support [/usr/local]])

  AC_MSG_CHECKING([for Adabas support])
  if test "$HYSS_ADABAS" != "no"; then
    if test "$HYSS_ADABAS" = "yes"; then
      HYSS_ADABAS=/usr/local
    fi
    HYSS_ADD_INCLUDE($HYSS_ADABAS/incl)
    HYSS_ADD_LIBPATH($HYSS_ADABAS/$HYSS_LIBDIR)
    ODBC_OBJS="$HYSS_ADABAS/$HYSS_LIBDIR/odbclib.a"
    ODBC_LIB="$abs_builddir/extslib/odbc/libodbc_adabas.a"
    $srcdir/build/shtool mkdir -f -p extslib/odbc
    rm -f "$ODBC_LIB"
    cp "$ODBC_OBJS" "$ODBC_LIB"
    HYSS_ADD_LIBRARY(sqlptc)
    HYSS_ADD_LIBRARY(sqlrte)
    HYSS_ADD_LIBRARY_WITH_PATH(odbc_adabas, $abs_builddir/extslib/odbc)
    ODBC_TYPE=adabas
    ODBC_INCDIR=$HYSS_ADABAS/incl
    HYSS_ODBC_CHECK_HEADER(sqlext.h)
    AC_DEFINE(HAVE_ADABAS,1,[ ])
    AC_MSG_RESULT([$ext_output])
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(sapdb,,
[  --with-sapdb[=DIR]        Include SAP DB support [/usr/local]])

  AC_MSG_CHECKING([for SAP DB support])
  if test "$HYSS_SAPDB" != "no"; then
    if test "$HYSS_SAPDB" = "yes"; then
      HYSS_SAPDB=/usr/local
    fi
    HYSS_ADD_INCLUDE($HYSS_SAPDB/incl)
    HYSS_ADD_LIBPATH($HYSS_SAPDB/$HYSS_LIBDIR)
    HYSS_ADD_LIBRARY(sqlod)
    ODBC_TYPE=sapdb
    AC_DEFINE(HAVE_SAPDB,1,[ ])
    AC_MSG_RESULT([$ext_output])
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(solid,,
[  --with-solid[=DIR]        Include Solid support [/usr/local/solid]])

  AC_MSG_CHECKING(for Solid support)
  if test "$HYSS_SOLID" != "no"; then
    if test "$HYSS_SOLID" = "yes"; then
      HYSS_SOLID=/usr/local/solid
    fi
    ODBC_INCDIR=$HYSS_SOLID/include
    ODBC_LIBDIR=$HYSS_SOLID/$HYSS_LIBDIR
    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_TYPE=solid
    if test -f $ODBC_LIBDIR/soc*35.a; then
      AC_DEFINE(HAVE_SOLID_35,1,[ ])
    elif test -f $ODBC_LIBDIR/scl*30.a; then
      AC_DEFINE(HAVE_SOLID_30,1,[ ])
    elif test -f $ODBC_LIBDIR/scl*23.a; then
      AC_DEFINE(HAVE_SOLID,1,[ ])
    fi
    AC_MSG_RESULT([$ext_output])
    HYSS_ODBC_FIND_SOLID_LIBS($ODBC_LIBDIR)
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(ibm-db2,,
[  --with-ibm-db2[=DIR]      Include IBM DB2 support [/home/db2inst1/sqllib]])

  AC_MSG_CHECKING(for IBM DB2 support)
  if test "$HYSS_IBM_DB2" != "no"; then
    if test "$HYSS_IBM_DB2" = "yes"; then
      ODBC_INCDIR=/home/db2inst1/sqllib/include
      ODBC_LIBDIR=/home/db2inst1/sqllib/lib
    else
      ODBC_INCDIR=$HYSS_IBM_DB2/include
      ODBC_LIBDIR=$HYSS_IBM_DB2/$HYSS_LIBDIR
    fi

    HYSS_ODBC_CHECK_HEADER(sqlcli1.h)

    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_LFLAGS=-L$ODBC_LIBDIR
    ODBC_TYPE=ibm-db2
    ODBC_LIBS=-ldb2

    HYSS_TEST_BUILD(SQLExecute, [
      AC_DEFINE(HAVE_IBMDB2,1,[ ])
      AC_MSG_RESULT([$ext_output])
    ], [
      AC_MSG_RESULT(no)
      AC_MSG_ERROR([
build test failed. Please check the config.log for details.
You need to source your DB2 environment before running HYSS configure:
# . \$IBM_DB2/db2profile
])
    ], [
      $ODBC_LFLAGS $ODBC_LIBS
    ])
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(empress,,
[  --with-empress[=DIR]      Include Empress support [\$EMPRESSPATH]
                          (Empress Version >= 8.60 required)])

  AC_MSG_CHECKING(for Empress support)
  if test "$HYSS_EMPRESS" != "no"; then
    if test "$HYSS_EMPRESS" = "yes"; then
      ODBC_INCDIR=$EMPRESSPATH/include/odbc
      ODBC_LIBDIR=$EMPRESSPATH/shlib
    else
      ODBC_INCDIR=$HYSS_EMPRESS/include/odbc
      ODBC_LIBDIR=$HYSS_EMPRESS/shlib
    fi
    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_LFLAGS=-L$ODBC_LIBDIR
    ODBC_TYPE=empress
    AC_DEFINE(HAVE_EMPRESS,1,[ ])
    AC_MSG_RESULT([$ext_output])
    HYSS_ODBC_FIND_EMPRESS_LIBS($ODBC_LIBDIR)
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(empress-bcs,,
[  --with-empress-bcs[=DIR]  Include Empress Local Access support [\$EMPRESSPATH]
                          (Empress Version >= 8.60 required)])

  AC_MSG_CHECKING(for Empress local access support)
  if test "$HYSS_EMPRESS_BCS" != "no"; then
    if test "$HYSS_EMPRESS_BCS" = "yes"; then
      ODBC_INCDIR=$EMPRESSPATH/include/odbc
      ODBC_LIBDIR=$EMPRESSPATH/shlib
    else
      ODBC_INCDIR=$HYSS_EMPRESS_BCS/include/odbc
      ODBC_LIBDIR=$HYSS_EMPRESS_BCS/shlib
    fi
    CC="empocc -bcs";export CC;
    LD="empocc -bcs";export LD;
    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_LFLAGS=-L$ODBC_LIBDIR
    LIST=`empocc -listlines -bcs -o a a.c`

    NEWLIST=
    for I in $LIST
    do
      case $I in
        $EMPRESSPATH/odbccl/lib/* | \
        $EMPRESSPATH/rdbms/lib/* | \
        $EMPRESSPATH/common/lib/*)
              NEWLIST="$NEWLIST $I"
              ;;
      esac
    done
    ODBC_LIBS="-lemphyssbcs -lms -lmscfg -lbasic -lbasic_os -lnlscstab -lnlsmsgtab -lm -ldl -lcrypt"
    ODBC_TYPE=empress-bcs
    AC_DEFINE(HAVE_EMPRESS,1,[ ])
    AC_MSG_RESULT([$ext_output])
    HYSS_ODBC_FIND_EMPRESS_BCS_LIBS($ODBC_LIBDIR)
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(custom-odbc,,
[  --with-custom-odbc[=DIR]  Include user defined ODBC support. DIR is ODBC install base
                          directory [/usr/local]. Make sure to define CUSTOM_ODBC_LIBS and
                          have some odbc.h in your include dirs. f.e. you should define
                          following for Sybase SQL Anywhere 5.5.00 on QNX, prior to
                          running this configure script:
                            CPPFLAGS=\"-DODBC_QNX -DSQLANY_BUG\"
                            LDFLAGS=-lunix
                            CUSTOM_ODBC_LIBS=\"-ldblib -lodbc\"])

  AC_MSG_CHECKING(for a custom ODBC support)
  if test "$HYSS_CUSTOM_ODBC" != "no"; then
    if test "$HYSS_CUSTOM_ODBC" = "yes"; then
      HYSS_CUSTOM_ODBC=/usr/local
    fi
    ODBC_INCDIR=$HYSS_CUSTOM_ODBC/include
    ODBC_LIBDIR=$HYSS_CUSTOM_ODBC/$HYSS_LIBDIR
    ODBC_LFLAGS=-L$ODBC_LIBDIR
    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_LIBS=$CUSTOM_ODBC_LIBS
    ODBC_TYPE=custom-odbc
    AC_DEFINE(HAVE_CODBC,1,[ ])
    AC_MSG_RESULT([$ext_output])
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(iodbc,,
[  --with-iodbc[=DIR]        Include iODBC support [/usr/local]])

  AC_MSG_CHECKING(for iODBC support)
  if test "$HYSS_IODBC" != "no"; then
    if test -z "$PKG_CONFIG"; then
      AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
    fi
    if test -x "$PKG_CONFIG" && $PKG_CONFIG --exists libiodbc ; then
      HYSS_ADD_LIBRARY_WITH_PATH(iodbc, $HYSS_IODBC/$HYSS_LIBDIR)
      ODBC_TYPE=iodbc
      ODBC_INCLUDE=`$PKG_CONFIG --cflags-only-I libiodbc`
      ODBC_LFLAGS=`$PKG_CONFIG --libs-only-L libiodbc`
      ODBC_LIBS=`$PKG_CONFIG --libs-only-l libiodbc`
      HYSS_EVAL_INCLINE($ODBC_INCLUDE)
      AC_DEFINE(HAVE_IODBC,1,[ ])
      AC_DEFINE(HAVE_ODBC2,1,[ ])
      AC_MSG_RESULT([$ext_output])
    else
      if test "$HYSS_IODBC" = "yes"; then
        HYSS_IODBC=/usr/local
      fi
      HYSS_ADD_LIBRARY_WITH_PATH(iodbc, $HYSS_IODBC/$HYSS_LIBDIR)
      HYSS_ADD_INCLUDE($HYSS_IODBC/include, 1)
      ODBC_TYPE=iodbc
      ODBC_INCLUDE=-I$HYSS_IODBC/include
      ODBC_LFLAGS=-L$HYSS_IODBC/$HYSS_LIBDIR
      ODBC_LIBS=-liodbc
      AC_DEFINE(HAVE_IODBC,1,[ ])
      AC_DEFINE(HAVE_ODBC2,1,[ ])
      AC_MSG_RESULT([$ext_output])
    fi
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(esoob,,
[  --with-esoob[=DIR]        Include Easysoft OOB support [/usr/local/easysoft/oob/client]])

  AC_MSG_CHECKING(for Easysoft ODBC-ODBC Bridge support)
  if test "$HYSS_ESOOB" != "no"; then
    if test "$HYSS_ESOOB" = "yes"; then
      HYSS_ESOOB=/usr/local/easysoft/oob/client
    fi
    ODBC_INCDIR=$HYSS_ESOOB/include
    ODBC_LIBDIR=$HYSS_ESOOB/$HYSS_LIBDIR
    ODBC_LFLAGS=-L$ODBC_LIBDIR
    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_LIBS=-lesoobclient
    ODBC_TYPE=esoob
    AC_DEFINE(HAVE_ESOOB,1,[ ])
    AC_MSG_RESULT([$ext_output])
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(unixODBC,,
[  --with-unixODBC[=DIR]     Include unixODBC support [/usr/local]])

  AC_MSG_CHECKING(for unixODBC support)
  if test "$HYSS_UNIXODBC" != "no"; then
    if test "$HYSS_UNIXODBC" = "yes"; then
      HYSS_UNIXODBC=/usr/local
    fi
    ODBC_INCDIR=$HYSS_UNIXODBC/include
    ODBC_LIBDIR=$HYSS_UNIXODBC/$HYSS_LIBDIR
    ODBC_LFLAGS=-L$ODBC_LIBDIR
    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_LIBS=-lodbc
    ODBC_TYPE=unixODBC
    HYSS_ODBC_CHECK_HEADER(sqlext.h)
    AC_DEFINE(HAVE_UNIXODBC,1,[ ])
    AC_MSG_RESULT([$ext_output])
  else
    AC_MSG_RESULT(no)
  fi
fi

if test -z "$ODBC_TYPE"; then
HYSS_ARG_WITH(dbmaker,,
[  --with-dbmaker[=DIR]      Include DBMaker support])

  AC_MSG_CHECKING(for DBMaker support)
  if test "$HYSS_DBMAKER" != "no"; then
    if test "$HYSS_DBMAKER" = "yes"; then
      # find dbmaker's home directory
      DBMAKER_HOME=`grep "^dbmaker:" /etc/passwd | $AWK -F: '{print $6}'`

      # check DBMaker version (from 5.0 to 2.0)
      DBMAKER_VERSION=5.0

      while test ! -d $DBMAKER_HOME/$DBMAKER_VERSION -a "$DBMAKER_VERSION" != "2.9"; do
        DM_VER=`echo $DBMAKER_VERSION | sed -e 's/\.//' | $AWK '{ print $1-1;}'`
        MAJOR_V=`echo $DM_VER | $AWK '{ print $1/10; }'  | $AWK -F. '{ print $1; }'`
        MINOR_V=`echo $DM_VER | $AWK '{ print $1%10; }'`
        DBMAKER_VERSION=$MAJOR_V.$MINOR_V
      done

      if test "$DBMAKER_VERSION" = "2.9"; then
        HYSS_DBMAKER=$DBMAKER_HOME
      else
        HYSS_DBMAKER=$DBMAKER_HOME/$DBMAKER_VERSION
      fi
    fi

    ODBC_INCDIR=$HYSS_DBMAKER/include
    ODBC_LIBDIR=$HYSS_DBMAKER/$HYSS_LIBDIR
    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_LFLAGS=-L$ODBC_LIBDIR
    ODBC_INCLUDE=-I$ODBC_INCDIR
    ODBC_LIBS="-ldmapic -lc"
    ODBC_TYPE=dbmaker

    AC_DEFINE(HAVE_DBMAKER,1,[Whether you want DBMaker])

    if test "$ext_shared" = "yes"; then
      AC_MSG_RESULT([yes (shared)])
      ODBC_LIBS="-ldmapic -lc -lm"
      ODBC_SHARED="odbc.la"
    else
      AC_MSG_RESULT([yes (static)])
      HYSS_ADD_LIBRARY_WITH_PATH(dmapic, $ODBC_LIBDIR)
      HYSS_ADD_INCLUDE($ODBC_INCDIR)
      ODBC_STATIC="libhyssext_odbc.la"
    fi
  else
    AC_MSG_RESULT(no)
  fi
fi

if test "no" != "$HYSS_ODBCVER"; then
  if test "$HYSS_ODBCVER" != "0"; then
    AC_DEFINE_UNQUOTED(ODBCVER, $HYSS_ODBCVER, [ The highest supported ODBC version ])
  fi
else
  AC_DEFINE(ODBCVER, 0x0300, [ The highest supported ODBC version ])
fi


dnl
dnl Extension setup
dnl
if test -n "$ODBC_TYPE"; then
  if test "$ODBC_TYPE" != "dbmaker"; then
    HYSS_EVAL_LIBLINE([$ODBC_LFLAGS $ODBC_LIBS], ODBC_SHARED_LIBADD)
    if test "$ODBC_TYPE" != "solid"; then
      AC_DEFINE(HAVE_SQLDATASOURCES,1,[ ])
    fi
  fi

  AC_DEFINE(HAVE_UODBC,1,[ ])
  HYSS_SUBST(ODBC_SHARED_LIBADD)
  HYSS_SUBST(ODBC_INCDIR)
  HYSS_SUBST(ODBC_LIBDIR)
  HYSS_SUBST_OLD(ODBC_INCLUDE)
  HYSS_SUBST_OLD(ODBC_LIBS)
  HYSS_SUBST_OLD(ODBC_LFLAGS)
  HYSS_SUBST_OLD(ODBC_TYPE)

  HYSS_NEW_EXTENSION(odbc, hyss_odbc.c, $ext_shared,, [$ODBC_INCLUDE -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1])
else
  AC_MSG_CHECKING([for any ODBC driver support])
  AC_MSG_RESULT(no)
fi
