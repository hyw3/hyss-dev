/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_ODBC_H
#define HYSS_ODBC_H

#if HAVE_UODBC

#ifdef ZTS
#include "hypbc.h"
#endif

extern gear_capi_entry odbc_capi_entry;
#define odbc_capi_ptr &odbc_capi_entry

#include "hyss_version.h"
#define HYSS_ODBC_VERSION HYSS_VERSION

#if defined(HAVE_DBMAKER) || defined(HYSS_WIN32) || defined(HAVE_IBMDB2) || defined(HAVE_UNIXODBC) || defined(HAVE_IODBC)
# define HYSS_ODBC_HAVE_FETCH_HASH 1
#endif

/* user functions */
HYSS_MINIT_FUNCTION(odbc);
HYSS_MSHUTDOWN_FUNCTION(odbc);
HYSS_RINIT_FUNCTION(odbc);
HYSS_RSHUTDOWN_FUNCTION(odbc);
HYSS_MINFO_FUNCTION(odbc);

HYSS_FUNCTION(odbc_error);
HYSS_FUNCTION(odbc_errormsg);
HYSS_FUNCTION(odbc_setoption);
HYSS_FUNCTION(odbc_autocommit);
HYSS_FUNCTION(odbc_close);
HYSS_FUNCTION(odbc_close_all);
HYSS_FUNCTION(odbc_commit);
HYSS_FUNCTION(odbc_connect);
HYSS_FUNCTION(odbc_pconnect);
HYSS_FUNCTION(odbc_cursor);
#ifdef HAVE_SQLDATASOURCES
HYSS_FUNCTION(odbc_data_source);
#endif
HYSS_FUNCTION(odbc_do);
HYSS_FUNCTION(odbc_exec);
HYSS_FUNCTION(odbc_execute);
#ifdef HYSS_ODBC_HAVE_FETCH_HASH
HYSS_FUNCTION(odbc_fetch_array);
HYSS_FUNCTION(odbc_fetch_object);
#endif
HYSS_FUNCTION(odbc_fetch_into);
HYSS_FUNCTION(odbc_fetch_row);
HYSS_FUNCTION(odbc_field_len);
HYSS_FUNCTION(odbc_field_scale);
HYSS_FUNCTION(odbc_field_name);
HYSS_FUNCTION(odbc_field_type);
HYSS_FUNCTION(odbc_field_num);
HYSS_FUNCTION(odbc_free_result);
#if !defined(HAVE_SOLID) && !defined(HAVE_SOLID_30)
HYSS_FUNCTION(odbc_next_result);
#endif
HYSS_FUNCTION(odbc_num_fields);
HYSS_FUNCTION(odbc_num_rows);
HYSS_FUNCTION(odbc_prepare);
HYSS_FUNCTION(odbc_result);
HYSS_FUNCTION(odbc_result_all);
HYSS_FUNCTION(odbc_rollback);
HYSS_FUNCTION(odbc_binmode);
HYSS_FUNCTION(odbc_longreadlen);
HYSS_FUNCTION(odbc_tables);
HYSS_FUNCTION(odbc_columns);
#if !defined(HAVE_DBMAKER) && !defined(HAVE_SOLID) && !defined(HAVE_SOLID_35)    /* not supported now */
HYSS_FUNCTION(odbc_columnprivileges);
HYSS_FUNCTION(odbc_tableprivileges);
#endif
#if !defined(HAVE_SOLID) || !defined(HAVE_SOLID_35)    /* not supported */
HYSS_FUNCTION(odbc_foreignkeys);
HYSS_FUNCTION(odbc_procedures);
HYSS_FUNCTION(odbc_procedurecolumns);
#endif
HYSS_FUNCTION(odbc_gettypeinfo);
HYSS_FUNCTION(odbc_primarykeys);
HYSS_FUNCTION(odbc_specialcolumns);
HYSS_FUNCTION(odbc_statistics);

#ifdef HYSS_WIN32
# define HYSS_ODBC_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
# define HYSS_ODBC_API __attribute__ ((visibility("default")))
#else
# define HYSS_ODBC_API
#endif

#else

#define odbc_capi_ptr NULL

#endif /* HAVE_UODBC */

#define hyssext_odbc_ptr odbc_capi_ptr

#endif /* HYSS_ODBC_H */

