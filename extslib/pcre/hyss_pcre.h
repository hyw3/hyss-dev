/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PCRE_H
#define HYSS_PCRE_H

#if HAVE_PCRE || HAVE_BUNDLED_PCRE

#if HAVE_BUNDLED_PCRE
#include "pcre2lib/pcre2.h"
#else
#include "pcre2.h"
#endif

#if HAVE_LOCALE_H
#include <locale.h>
#endif

HYSSAPI gear_string *hyss_pcre_replace(gear_string *regex, gear_string *subject_str, char *subject, size_t subject_len, gear_string *replace_str, size_t limit, size_t *replace_count);
HYSSAPI pcre2_code* pcre_get_compiled_regex(gear_string *regex, uint32_t *capture_count, uint32_t *options);
HYSSAPI pcre2_code* pcre_get_compiled_regex_ex(gear_string *regex, uint32_t *capture_count, uint32_t *preg_options, uint32_t *coptions);

extern gear_capi_entry pcre_capi_entry;
#define pcre_capi_ptr &pcre_capi_entry

#include "hyss_version.h"
#define HYSS_PCRE_VERSION HYSS_VERSION

typedef struct _pcre_cache_entry pcre_cache_entry;

HYSSAPI pcre_cache_entry* pcre_get_compiled_regex_cache(gear_string *regex);

HYSSAPI void  hyss_pcre_match_impl(  pcre_cache_entry *pce, char *subject, size_t subject_len, zval *return_value,
	zval *subpats, int global, int use_flags, gear_long flags, gear_off_t start_offset);

HYSSAPI gear_string *hyss_pcre_replace_impl(pcre_cache_entry *pce, gear_string *subject_str, char *subject, size_t subject_len, gear_string *replace_str,
	size_t limit, size_t *replace_count);

HYSSAPI void  hyss_pcre_split_impl(  pcre_cache_entry *pce, gear_string *subject_str, zval *return_value,
	gear_long limit_val, gear_long flags);

HYSSAPI void  hyss_pcre_grep_impl(   pcre_cache_entry *pce, zval *input, zval *return_value,
	gear_long flags);

HYSSAPI pcre2_match_context *hyss_pcre_mctx(void);
HYSSAPI pcre2_general_context *hyss_pcre_gctx(void);
HYSSAPI pcre2_compile_context *hyss_pcre_cctx(void);
HYSSAPI void hyss_pcre_pce_incref(pcre_cache_entry *);
HYSSAPI void hyss_pcre_pce_decref(pcre_cache_entry *);
HYSSAPI pcre2_code *hyss_pcre_pce_re(pcre_cache_entry *);
/* capture_count can be ignored, re is required. */
HYSSAPI pcre2_match_data *hyss_pcre_create_match_data(uint32_t, pcre2_code *);
HYSSAPI void hyss_pcre_free_match_data(pcre2_match_data *);

GEAR_BEGIN_CAPI_GLOBALS(pcre)
	HashTable pcre_cache;
	gear_long backtrack_limit;
	gear_long recursion_limit;
#ifdef HAVE_PCRE_JIT_SUPPORT
	gear_bool jit;
#endif
	int  error_code;
GEAR_END_CAPI_GLOBALS(pcre)

HYSSAPI GEAR_EXTERN_CAPI_GLOBALS(pcre)
#define PCRE_G(v) GEAR_CAPI_GLOBALS_ACCESSOR(pcre, v)

#else

#define pcre_capi_ptr NULL

#endif /* HAVE_PCRE || HAVE_BUNDLED_PCRE */

#define hyssext_pcre_ptr pcre_capi_ptr

#endif /* HYSS_PCRE_H */
