dnl config.m4 for extension xmlwriter

HYSS_ARG_ENABLE(xmlwriter, whether to enable XMLWriter support,
[  --disable-xmlwriter     Disable XMLWriter support], yes)

if test -z "$HYSS_LIBXML_DIR"; then
  HYSS_ARG_WITH(libxml-dir, libxml2 install dir,
  [  --with-libxml-dir=DIR   XMLWriter: libxml2 install prefix], no, no)
fi

if test "$HYSS_XMLWRITER" != "no"; then

  if test "$HYSS_LIBXML" = "no"; then
    AC_MSG_ERROR([XMLWriter extension requires LIBXML extension, add --enable-libxml])
  fi

  HYSS_SETUP_LIBXML(XMLWRITER_SHARED_LIBADD, [
    AC_DEFINE(HAVE_XMLWRITER,1,[ ])
    HYSS_NEW_EXTENSION(xmlwriter, hyss_xmlwriter.c, $ext_shared)
    HYSS_SUBST(XMLWRITER_SHARED_LIBADD)
  ], [
    AC_MSG_ERROR([libxml2 not found. Please check your libxml2 installation.])
  ])
fi
