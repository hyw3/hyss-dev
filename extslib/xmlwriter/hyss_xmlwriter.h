/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_XMLWRITER_H
#define HYSS_XMLWRITER_H

extern gear_capi_entry xmlwriter_capi_entry;
#define hyssext_xmlwriter_ptr &xmlwriter_capi_entry

#include "hyss_version.h"
#define HYSS_XMLWRITER_VERSION HYSS_VERSION

#ifdef ZTS
#include "hypbc.h"
#endif

#include <libxml/tree.h>
#include <libxml/xmlwriter.h>
#include <libxml/uri.h>

/* Resource struct, not the object :) */
typedef struct _xmlwriter_object {
	xmlTextWriterPtr ptr;
	xmlBufferPtr output;
} xmlwriter_object;


/* Extends gear object */
typedef struct _ze_xmlwriter_object {
	xmlwriter_object *xmlwriter_ptr;
	gear_object std;
} ze_xmlwriter_object;

static inline ze_xmlwriter_object *hyss_xmlwriter_fetch_object(gear_object *obj) {
	return (ze_xmlwriter_object *)((char*)(obj) - XtOffsetOf(ze_xmlwriter_object, std));
}

#define Z_XMLWRITER_P(zv) hyss_xmlwriter_fetch_object(Z_OBJ_P((zv)))

#endif	/* HYSS_XMLWRITER_H */

