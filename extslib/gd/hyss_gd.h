/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_GD_H
#define HYSS_GD_H

#if HAVE_LIBFREETYPE
# ifndef ENABLE_GD_TTF
#  define ENABLE_GD_TTF
# endif
#endif

#if defined(HAVE_LIBGD) || defined(HAVE_GD_BUNDLED)

/* open_basedir and safe_mode checks */
#define HYSS_GD_CHECK_OPEN_BASEDIR(filename, errormsg)             \
	if (!filename || hyss_check_open_basedir(filename)) {      \
		hyss_error_docref(NULL, E_WARNING, errormsg);      \
		RETURN_FALSE;                                      \
	}

#define HYSS_GDIMG_TYPE_GIF      1
#define HYSS_GDIMG_TYPE_PNG      2
#define HYSS_GDIMG_TYPE_JPG      3
#define HYSS_GDIMG_TYPE_WBM      4
#define HYSS_GDIMG_TYPE_XBM      5
#define HYSS_GDIMG_TYPE_XPM      6
#define HYSS_GDIMG_CONVERT_WBM   7
#define HYSS_GDIMG_TYPE_GD       8
#define HYSS_GDIMG_TYPE_GD2      9
#define HYSS_GDIMG_TYPE_GD2PART  10
#define HYSS_GDIMG_TYPE_WEBP     11
#define HYSS_GDIMG_TYPE_BMP      12

#define HYSS_IMG_GIF    1
#define HYSS_IMG_JPG    2
#define HYSS_IMG_JPEG   2
#define HYSS_IMG_PNG    4
#define HYSS_IMG_WBMP   8
#define HYSS_IMG_XPM   16
#define HYSS_IMG_WEBP  32
#define HYSS_IMG_BMP   64

#ifdef HYSS_WIN32
#	define HYSS_GD_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define HYSS_GD_API __attribute__ ((visibility("default")))
#else
#	define HYSS_GD_API
#endif

HYSSAPI extern const char hyss_sig_gif[3];
HYSSAPI extern const char hyss_sig_jpg[3];
HYSSAPI extern const char hyss_sig_png[8];
HYSSAPI extern const char hyss_sig_bmp[2];
HYSSAPI extern const char hyss_sig_riff[4];
HYSSAPI extern const char hyss_sig_webp[4];

extern gear_capi_entry gd_capi_entry;
#define hyssext_gd_ptr &gd_capi_entry

#include "hyss_version.h"
#define HYSS_GD_VERSION HYSS_VERSION

/* gd.c functions */
HYSS_MINFO_FUNCTION(gd);
HYSS_MINIT_FUNCTION(gd);
#if HAVE_GD_FREETYPE && HAVE_LIBFREETYPE
HYSS_RSHUTDOWN_FUNCTION(gd);
#endif

HYSS_FUNCTION(gd_info);
HYSS_FUNCTION(imagearc);
HYSS_FUNCTION(imageellipse);
HYSS_FUNCTION(imagechar);
HYSS_FUNCTION(imagecharup);
HYSS_FUNCTION(imageistruecolor);
HYSS_FUNCTION(imagecolorallocate);
HYSS_FUNCTION(imagepalettecopy);
HYSS_FUNCTION(imagecolorat);
HYSS_FUNCTION(imagecolorclosest);
HYSS_FUNCTION(imagecolorclosesthwb);
HYSS_FUNCTION(imagecolordeallocate);
HYSS_FUNCTION(imagecolorresolve);
HYSS_FUNCTION(imagecolorexact);
HYSS_FUNCTION(imagecolorset);
HYSS_FUNCTION(imagecolorstotal);
HYSS_FUNCTION(imagecolorsforindex);
HYSS_FUNCTION(imagecolortransparent);
HYSS_FUNCTION(imagecopy);
HYSS_FUNCTION(imagecopymerge);
HYSS_FUNCTION(imagecopyresized);
HYSS_FUNCTION(imagetypes);
HYSS_FUNCTION(imagecreate);
HYSS_FUNCTION(imageftbbox);
HYSS_FUNCTION(imagefttext);

HYSS_FUNCTION(imagecreatetruecolor);
HYSS_FUNCTION(imagetruecolortopalette);
HYSS_FUNCTION(imagepalettetotruecolor);
HYSS_FUNCTION(imagesetthickness);
HYSS_FUNCTION(imagefilledellipse);
HYSS_FUNCTION(imagefilledarc);
HYSS_FUNCTION(imagealphablending);
HYSS_FUNCTION(imagesavealpha);
HYSS_FUNCTION(imagecolorallocatealpha);
HYSS_FUNCTION(imagecolorresolvealpha);
HYSS_FUNCTION(imagecolorclosestalpha);
HYSS_FUNCTION(imagecolorexactalpha);
HYSS_FUNCTION(imagecopyresampled);

#ifdef HYSS_WIN32
HYSS_FUNCTION(imagegrabwindow);
HYSS_FUNCTION(imagegrabscreen);
#endif

HYSS_FUNCTION(imagerotate);

HYSS_FUNCTION(imageflip);

HYSS_FUNCTION(imageantialias);

HYSS_FUNCTION(imagecrop);
HYSS_FUNCTION(imagecropauto);
HYSS_FUNCTION(imagescale);
HYSS_FUNCTION(imageaffine);
HYSS_FUNCTION(imageaffinematrixget);
HYSS_FUNCTION(imageaffinematrixconcat);
HYSS_FUNCTION(imagesetinterpolation);

HYSS_FUNCTION(imagesetthickness);
HYSS_FUNCTION(imagecopymergegray);
HYSS_FUNCTION(imagesetbrush);
HYSS_FUNCTION(imagesettile);
HYSS_FUNCTION(imagesetstyle);

HYSS_FUNCTION(imagecreatefromstring);
HYSS_FUNCTION(imagecreatefromgif);
HYSS_FUNCTION(imagecreatefromjpeg);
HYSS_FUNCTION(imagecreatefromxbm);
HYSS_FUNCTION(imagecreatefromwebp);
HYSS_FUNCTION(imagecreatefrompng);
HYSS_FUNCTION(imagecreatefromwbmp);
HYSS_FUNCTION(imagecreatefromgd);
HYSS_FUNCTION(imagecreatefromgd2);
HYSS_FUNCTION(imagecreatefromgd2part);
#if defined(HAVE_GD_BMP)
HYSS_FUNCTION(imagecreatefrombmp);
#endif
#if defined(HAVE_GD_XPM)
HYSS_FUNCTION(imagecreatefromxpm);
#endif

HYSS_FUNCTION(imagegammacorrect);
HYSS_FUNCTION(imagedestroy);
HYSS_FUNCTION(imagefill);
HYSS_FUNCTION(imagefilledpolygon);
HYSS_FUNCTION(imagefilledrectangle);
HYSS_FUNCTION(imagefilltoborder);
HYSS_FUNCTION(imagefontwidth);
HYSS_FUNCTION(imagefontheight);

HYSS_FUNCTION(imagegif );
HYSS_FUNCTION(imagejpeg );
HYSS_FUNCTION(imagepng);
HYSS_FUNCTION(imagewebp);
HYSS_FUNCTION(imagewbmp);
HYSS_FUNCTION(imagegd);
HYSS_FUNCTION(imagegd2);
#if defined(HAVE_GD_BMP)
HYSS_FUNCTION(imagebmp);
#endif

HYSS_FUNCTION(imageinterlace);
HYSS_FUNCTION(imageline);
HYSS_FUNCTION(imageloadfont);
HYSS_FUNCTION(imagepolygon);
HYSS_FUNCTION(imageopenpolygon);
HYSS_FUNCTION(imagerectangle);
HYSS_FUNCTION(imagesetpixel);
HYSS_FUNCTION(imagestring);
HYSS_FUNCTION(imagestringup);
HYSS_FUNCTION(imagesx);
HYSS_FUNCTION(imagesy);
HYSS_FUNCTION(imagesetclip);
HYSS_FUNCTION(imagegetclip);
HYSS_FUNCTION(imagedashedline);
HYSS_FUNCTION(imagettfbbox);
HYSS_FUNCTION(imagettftext);

HYSS_FUNCTION(jpeg2wbmp);
HYSS_FUNCTION(png2wbmp);
HYSS_FUNCTION(image2wbmp);

HYSS_FUNCTION(imagecolormatch);

HYSS_FUNCTION(imagelayereffect);
HYSS_FUNCTION(imagexbm);

HYSS_FUNCTION(imagefilter);
HYSS_FUNCTION(imageconvolution);

HYSS_FUNCTION(imageresolution);

HYSS_GD_API int hyssi_get_le_gd(void);

#else

#define hyssext_gd_ptr NULL

#endif

#endif /* HYSS_GD_H */
