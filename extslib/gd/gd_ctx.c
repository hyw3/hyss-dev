/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss_gd.h"

#define CTX_PUTC(c,ctx) ctx->putC(ctx, c)

static void _hyss_image_output_putc(struct gdIOCtx *ctx, int c) /* {{{ */
{
	/* without the following downcast, the write will fail
	 * (i.e., will write a zero byte) for all
	 * big endian architectures:
	 */
	unsigned char ch = (unsigned char) c;
	hyss_write(&ch, 1);
} /* }}} */

static int _hyss_image_output_putbuf(struct gdIOCtx *ctx, const void* buf, int l) /* {{{ */
{
	return hyss_write((void *)buf, l);
} /* }}} */

static void _hyss_image_output_ctxfree(struct gdIOCtx *ctx) /* {{{ */
{
	if(ctx) {
		efree(ctx);
	}
} /* }}} */

static void _hyss_image_stream_putc(struct gdIOCtx *ctx, int c) /* {{{ */ {
	char ch = (char) c;
	hyss_stream * stream = (hyss_stream *)ctx->data;
	hyss_stream_write(stream, &ch, 1);
} /* }}} */

static int _hyss_image_stream_putbuf(struct gdIOCtx *ctx, const void* buf, int l) /* {{{ */
{
	hyss_stream * stream = (hyss_stream *)ctx->data;
	return hyss_stream_write(stream, (void *)buf, l);
} /* }}} */

static void _hyss_image_stream_ctxfree(struct gdIOCtx *ctx) /* {{{ */
{
	if(ctx->data) {
		ctx->data = NULL;
	}
	if(ctx) {
		efree(ctx);
	}
} /* }}} */

static void _hyss_image_stream_ctxfreeandclose(struct gdIOCtx *ctx) /* {{{ */
{

	if(ctx->data) {
		hyss_stream_close((hyss_stream *) ctx->data);
		ctx->data = NULL;
	}
	if(ctx) {
		efree(ctx);
	}
} /* }}} */

/* {{{ _hyss_image_output_ctx */
static void _hyss_image_output_ctx(INTERNAL_FUNCTION_PARAMETERS, int image_type, char *tn, void (*func_p)())
{
	zval *imgind;
	char *file = NULL;
	size_t file_len = 0;
	gear_long quality, basefilter;
	gear_bool compressed = 1;
	gdImagePtr im;
	int argc = GEAR_NUM_ARGS();
	int q = -1, i;
	int f = -1;
	gdIOCtx *ctx = NULL;
	zval *to_zval = NULL;
	hyss_stream *stream;
	int close_stream = 1;

	/* The third (quality) parameter for Wbmp stands for the foreground when called from image2wbmp().
	 * The third (quality) parameter for Wbmp and Xbm stands for the foreground color index when called
	 * from imagey<type>().
	 */
	switch (image_type) {
		case HYSS_GDIMG_TYPE_XBM:
			if (gear_parse_parameters(argc, "rp!|ll", &imgind, &file, &file_len, &quality, &basefilter) == FAILURE) {
				return;
			}
			break;
		case HYSS_GDIMG_TYPE_BMP:
			if (gear_parse_parameters(argc, "r|z!b", &imgind, &to_zval, &compressed) == FAILURE) {
				return;
			}
			break;
		default:
			/* HYSS_GDIMG_TYPE_GIF
			 * HYSS_GDIMG_TYPE_PNG
			 * HYSS_GDIMG_TYPE_JPG
			 * HYSS_GDIMG_TYPE_WBM
			 * HYSS_GDIMG_TYPE_WEBP
			 * */
			if (gear_parse_parameters(argc, "r|z!ll", &imgind, &to_zval, &quality, &basefilter) == FAILURE) {
				return;
			}
	}

	if ((im = (gdImagePtr)gear_fetch_resource(Z_RES_P(imgind), "Image", hyssi_get_le_gd())) == NULL) {
		RETURN_FALSE;
	}

	if (image_type != HYSS_GDIMG_TYPE_BMP && argc >= 3) {
		q = quality; /* or colorindex for foreground of BW images (defaults to black) */
		if (argc == 4) {
			f = basefilter;
		}
	}

	if (argc > 1 && to_zval != NULL) {
		if (Z_TYPE_P(to_zval) == IS_RESOURCE) {
			hyss_stream_from_zval_no_verify(stream, to_zval);
			if (stream == NULL) {
				RETURN_FALSE;
			}
			close_stream = 0;
		} else if (Z_TYPE_P(to_zval) == IS_STRING) {
			if (CHECK_ZVAL_NULL_PATH(to_zval)) {
				hyss_error_docref(NULL, E_WARNING, "Invalid 2nd parameter, filename must not contain null bytes");
				RETURN_FALSE;
			}

			stream = hyss_stream_open_wrapper(Z_STRVAL_P(to_zval), "wb", REPORT_ERRORS|IGNORE_PATH|IGNORE_URL_WIN, NULL);
			if (stream == NULL) {
				RETURN_FALSE;
			}
		} else {
			hyss_error_docref(NULL, E_WARNING, "Invalid 2nd parameter, it must a filename or a stream");
			RETURN_FALSE;
		}
	} else if (argc > 1 && file != NULL) {
		stream = hyss_stream_open_wrapper(file, "wb", REPORT_ERRORS|IGNORE_PATH|IGNORE_URL_WIN, NULL);
		if (stream == NULL) {
			RETURN_FALSE;
		}
	} else {
		ctx = ecalloc(1, sizeof(gdIOCtx));
		ctx->putC = _hyss_image_output_putc;
		ctx->putBuf = _hyss_image_output_putbuf;
		ctx->gd_free = _hyss_image_output_ctxfree;
	}

	if (!ctx)	{
		ctx = ecalloc(1, sizeof(gdIOCtx));
		ctx->putC = _hyss_image_stream_putc;
		ctx->putBuf = _hyss_image_stream_putbuf;
		if (close_stream) {
			ctx->gd_free = _hyss_image_stream_ctxfreeandclose;
		} else {
			ctx->gd_free = _hyss_image_stream_ctxfree;
		}
		ctx->data = (void *)stream;
	}

	switch(image_type) {
		case HYSS_GDIMG_CONVERT_WBM:
			if(q<0||q>255) {
				hyss_error_docref(NULL, E_WARNING, "Invalid threshold value '%d'. It must be between 0 and 255", q);
			}
		case HYSS_GDIMG_TYPE_JPG:
			(*func_p)(im, ctx, q);
			break;
		case HYSS_GDIMG_TYPE_WEBP:
			if (q == -1) {
				q = 80;
			}
			(*func_p)(im, ctx, q);
			break;
		case HYSS_GDIMG_TYPE_PNG:
			(*func_p)(im, ctx, q, f);
			break;
		case HYSS_GDIMG_TYPE_XBM:
		case HYSS_GDIMG_TYPE_WBM:
			if (argc < 3) {
				for(i=0; i < gdImageColorsTotal(im); i++) {
					if(!gdImageRed(im, i) && !gdImageGreen(im, i) && !gdImageBlue(im, i)) break;
				}
				q = i;
			}
			if (image_type == HYSS_GDIMG_TYPE_XBM) {
				(*func_p)(im, file ? file : "", q, ctx);
			} else {
				(*func_p)(im, q, ctx);
			}
			break;
		case HYSS_GDIMG_TYPE_BMP:
			(*func_p)(im, ctx, (int) compressed);
			break;
		default:
			(*func_p)(im, ctx);
			break;
	}

	ctx->gd_free(ctx);

	RETURN_TRUE;
}
/* }}} */

