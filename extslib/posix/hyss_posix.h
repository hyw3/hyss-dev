/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_POSIX_H
#define HYSS_POSIX_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#if HAVE_POSIX
#ifndef DLEXPORT
#define DLEXPORT
#endif

extern gear_capi_entry posix_capi_entry;
#define posix_capi_ptr &posix_capi_entry

#include "hyss_version.h"
#define HYSS_POSIX_VERSION HYSS_VERSION

/* POSIX.1, 3.3 */
HYSS_FUNCTION(posix_kill);

/* POSIX.1, 4.1 */
HYSS_FUNCTION(posix_getpid);
HYSS_FUNCTION(posix_getppid);

/* POSIX.1,  4.2 */
HYSS_FUNCTION(posix_getuid);
HYSS_FUNCTION(posix_getgid);
HYSS_FUNCTION(posix_geteuid);
HYSS_FUNCTION(posix_getegid);
HYSS_FUNCTION(posix_setuid);
HYSS_FUNCTION(posix_setgid);
#ifdef HAVE_SETEUID
HYSS_FUNCTION(posix_seteuid);
#endif
#ifdef HAVE_SETEGID
HYSS_FUNCTION(posix_setegid);
#endif
#ifdef HAVE_GETGROUPS
HYSS_FUNCTION(posix_getgroups);
#endif
#ifdef HAVE_GETLOGIN
HYSS_FUNCTION(posix_getlogin);
#endif

/* POSIX.1, 4.3 */
HYSS_FUNCTION(posix_getpgrp);
#ifdef HAVE_SETSID
HYSS_FUNCTION(posix_setsid);
#endif
HYSS_FUNCTION(posix_setpgid);
/* Non-Posix functions which are common */
#ifdef HAVE_GETPGID
HYSS_FUNCTION(posix_getpgid);
#endif
#ifdef HAVE_GETSID
HYSS_FUNCTION(posix_getsid);
#endif

/* POSIX.1, 4.4 */
HYSS_FUNCTION(posix_uname);
HYSS_FUNCTION(posix_times);

/* POSIX.1, 4.5 */
#ifdef HAVE_CTERMID
HYSS_FUNCTION(posix_ctermid);
#endif
HYSS_FUNCTION(posix_ttyname);
HYSS_FUNCTION(posix_isatty);

/* POSIX.1, 5.2 */
HYSS_FUNCTION(posix_getcwd);

/* POSIX.1, 5.4 */
#ifdef HAVE_MKFIFO
HYSS_FUNCTION(posix_mkfifo);
#endif
#ifdef HAVE_MKNOD
HYSS_FUNCTION(posix_mknod);
#endif

/* POSIX.1, 5.6 */
HYSS_FUNCTION(posix_access);

/* POSIX.1, 9.2 */
HYSS_FUNCTION(posix_getgrnam);
HYSS_FUNCTION(posix_getgrgid);
HYSS_FUNCTION(posix_getpwnam);
HYSS_FUNCTION(posix_getpwuid);

#ifdef HAVE_GETRLIMIT
HYSS_FUNCTION(posix_getrlimit);
#endif

#ifdef HAVE_SETRLIMIT
HYSS_FUNCTION(posix_setrlimit);
#endif

#ifdef HAVE_INITGROUPS
HYSS_FUNCTION(posix_initgroups);
#endif

HYSS_FUNCTION(posix_get_last_error);
HYSS_FUNCTION(posix_strerror);

GEAR_BEGIN_CAPI_GLOBALS(posix)
	int last_error;
GEAR_END_CAPI_GLOBALS(posix)

#ifdef ZTS
# define POSIX_G(v) PBCG(posix_globals_id, gear_posix_globals *, v)
#else
# define POSIX_G(v)	(posix_globals.v)
#endif

#else

#define posix_capi_ptr NULL

#endif

#define hyssext_posix_ptr posix_capi_ptr

#endif /* HYSS_POSIX_H */
