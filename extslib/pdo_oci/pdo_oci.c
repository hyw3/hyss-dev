/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "pdo/hyss_pdo.h"
#include "pdo/hyss_pdo_driver.h"
#include "hyss_pdo_oci.h"
#include "hyss_pdo_oci_int.h"
#ifdef ZTS
#include <hypbc/hypbc.h>
#endif

/* {{{ pdo_oci_functions[] */
static const gear_function_entry pdo_oci_functions[] = {
	HYSS_FE_END
};
/* }}} */

/* {{{ pdo_oci_capi_entry */

static const gear_capi_dep pdo_oci_deps[] = {
	GEAR_CAPI_REQUIRED("pdo")
	GEAR_CAPI_END
};

gear_capi_entry pdo_oci_capi_entry = {
	STANDARD_CAPI_HEADER_EX, NULL,
	pdo_oci_deps,
	"PDO_OCI",
	pdo_oci_functions,
	HYSS_MINIT(pdo_oci),
	HYSS_MSHUTDOWN(pdo_oci),
	HYSS_RINIT(pdo_oci),
	NULL,
	HYSS_MINFO(pdo_oci),
	HYSS_PDO_OCI_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_PDO_OCI
GEAR_GET_CAPI(pdo_oci)
#endif

const ub4 PDO_OCI_INIT_MODE =
#if 0 && defined(OCI_SHARED)
			/* shared mode is known to be bad for HYSS */
			OCI_SHARED
#else
			OCI_DEFAULT
#endif
#ifdef OCI_OBJECT
			|OCI_OBJECT
#endif
#ifdef ZTS
			|OCI_THREADED
#endif
			;

/* true global environment */
OCIEnv *pdo_oci_Env = NULL;

#ifdef ZTS
/* lock for pdo_oci_Env initialization */
static MUTEX_T pdo_oci_env_mutex;
#endif

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(pdo_oci)
{
	REGISTER_PDO_CLASS_CONST_LONG("OCI_ATTR_ACTION", (gear_long)PDO_OCI_ATTR_ACTION);
	REGISTER_PDO_CLASS_CONST_LONG("OCI_ATTR_CLIENT_INFO", (gear_long)PDO_OCI_ATTR_CLIENT_INFO);
	REGISTER_PDO_CLASS_CONST_LONG("OCI_ATTR_CLIENT_IDENTIFIER", (gear_long)PDO_OCI_ATTR_CLIENT_IDENTIFIER);
	REGISTER_PDO_CLASS_CONST_LONG("OCI_ATTR_CAPI", (gear_long)PDO_OCI_ATTR_CAPI);

	hyss_pdo_register_driver(&pdo_oci_driver);

	// Defer OCI init to HYSS_RINIT_FUNCTION because with hyss-fpm,
	// NLS_LANG is not yet available here.

#ifdef ZTS
	pdo_oci_env_mutex = pbc_mutex_alloc();
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_RINIT_FUNCTION
 */
HYSS_RINIT_FUNCTION(pdo_oci)
{
	if (!pdo_oci_Env) {
#ifdef ZTS
		pbc_mutex_lock(pdo_oci_env_mutex);
		if (!pdo_oci_Env) { // double-checked locking idiom
#endif
#if HAVE_OCIENVCREATE
		OCIEnvCreate(&pdo_oci_Env, PDO_OCI_INIT_MODE, NULL, NULL, NULL, NULL, 0, NULL);
#else
		OCIInitialize(PDO_OCI_INIT_MODE, NULL, NULL, NULL, NULL);
		OCIEnvInit(&pdo_oci_Env, OCI_DEFAULT, 0, NULL);
#endif
#ifdef ZTS
		}
		pbc_mutex_unlock(pdo_oci_env_mutex);
#endif
	}

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION(pdo_oci)
{
	hyss_pdo_unregister_driver(&pdo_oci_driver);

	if (pdo_oci_Env) {
		OCIHandleFree((dvoid*)pdo_oci_Env, OCI_HTYPE_ENV);
	}

#ifdef ZTS
	pbc_mutex_free(pdo_oci_env_mutex);
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(pdo_oci)
{
	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "PDO Driver for OCI 8 and later", "enabled");
	hyss_info_print_table_end();
}
/* }}} */

