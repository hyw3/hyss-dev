dnl config.m4 for extension soap

HYSS_ARG_ENABLE(soap, whether to enable SOAP support,
[  --enable-soap           Enable SOAP support])

if test -z "$HYSS_LIBXML_DIR"; then
  HYSS_ARG_WITH(libxml-dir, libxml2 install dir,
  [  --with-libxml-dir=DIR   SOAP: libxml2 install prefix], no, no)
fi

if test "$HYSS_SOAP" != "no"; then

  if test "$HYSS_LIBXML" = "no"; then
    AC_MSG_ERROR([SOAP extension requires LIBXML extension, add --enable-libxml])
  fi

  HYSS_SETUP_LIBXML(SOAP_SHARED_LIBADD, [
    AC_DEFINE(HAVE_SOAP,1,[ ])
    HYSS_NEW_EXTENSION(soap, soap.c hyss_encoding.c hyss_http.c hyss_packet_soap.c hyss_schema.c hyss_sdl.c hyss_xml.c, $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
    HYSS_SUBST(SOAP_SHARED_LIBADD)
  ], [
    AC_MSG_ERROR([libxml2 not found. Please check your libxml2 installation.])
  ])
fi
