/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SOAP_XML_H
#define HYSS_SOAP_XML_H

#define get_attribute(node, name) get_attribute_ex(node, name, NULL)
#define get_node(node, name) get_node_ex(node, name, NULL)
#define get_node_recursive(node, name) get_node_recursive_ex(node, name, NULL)
#define get_node_with_attribute(node, name, attr, val) get_node_with_attribute_ex(node, name, NULL, attr, val, NULL)
#define get_node_with_attribute_recursive(node, name, attr, val) get_node_with_attribute_recursive_ex(node, name, NULL, attr, val, NULL)
#define node_is_equal(node, name) node_is_equal_ex(node, name, NULL)
#define attr_is_equal(node, name) attr_is_equal_ex(node, name, NULL)

xmlDocPtr soap_xmlParseFile(const char *filename);
xmlDocPtr soap_xmlParseMemory(const void *buf, size_t size);

xmlNsPtr attr_find_ns(xmlAttrPtr node);
xmlNsPtr node_find_ns(xmlNodePtr node);
int attr_is_equal_ex(xmlAttrPtr node, char *name, char *ns);
int node_is_equal_ex(xmlNodePtr node, char *name, char *ns);
xmlAttrPtr get_attribute_ex(xmlAttrPtr node,char *name, char *ns);
xmlNodePtr get_node_ex(xmlNodePtr node,char *name, char *ns);
xmlNodePtr get_node_recursive_ex(xmlNodePtr node,char *name, char *ns);
xmlNodePtr get_node_with_attribute_ex(xmlNodePtr node, char *name, char *name_ns, char *attribute, char *value, char *attr_ns);
xmlNodePtr get_node_with_attribute_recursive_ex(xmlNodePtr node, char *name, char *name_ns, char *attribute, char *value, char *attr_ns);
int parse_namespace(const xmlChar *inval,char **value,char **namespace);

#define FOREACHATTRNODE(n,c,i)      FOREACHATTRNODEEX(n,c,NULL,i)
#define FOREACHATTRNODEEX(n,c,ns,i) \
	do { \
		if (n == NULL) { \
			break; \
		} \
		if (c) { \
			i = get_attribute_ex(n,c,ns); \
		} else { \
			i = n; \
		} \
		if (i != NULL) { \
			n = i;

#define FOREACHNODE(n,c,i)      FOREACHNODEEX(n,c,NULL,i)
#define FOREACHNODEEX(n,c,ns,i) \
	do { \
		if (n == NULL) { \
			break; \
		} \
		if (c) { \
			i = get_node_ex(n,c,NULL); \
		} else { \
			i = n; \
		} \
		if(i != NULL) { \
			n = i;

#define ENDFOREACH(n) \
		} \
	} while ((n = n->next));

#endif
