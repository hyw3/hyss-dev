/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SCHEMA_H
#define HYSS_SCHEMA_H

int load_schema(sdlCtx *ctx, xmlNodePtr schema);
void schema_pass2(sdlCtx *ctx);

void delete_model(zval *zv);
void delete_model_persistent(zval *zv);
void delete_type(zval *zv);
void delete_type_persistent(zval *zv);
void delete_extra_attribute(zval *zv);
void delete_extra_attribute_persistent(zval *zv);
void delete_attribute(zval *zv);
void delete_attribute_persistent(zval *zv);
void delete_restriction_var_int(sdlRestrictionIntPtr ptr);
void delete_restriction_var_int_persistent(sdlRestrictionIntPtr ptr);
void delete_restriction_var_char(zval *zv);
void delete_restriction_var_char_int(sdlRestrictionCharPtr ptr);
void delete_restriction_var_char_persistent(zval *zv);
void delete_restriction_var_char_persistent_int(sdlRestrictionCharPtr ptr);
#endif
