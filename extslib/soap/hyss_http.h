/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HTTP_H
#define HYSS_HTTP_H

int make_http_soap_request(zval        *this_ptr,
                           gear_string *request,
                           char        *location,
                           char        *soapaction,
                           int          soap_version,
                           zval        *response);

int proxy_authentication(zval* this_ptr, smart_str* soap_headers);
int basic_authentication(zval* this_ptr, smart_str* soap_headers);
void http_context_headers(hyss_stream_context* context,
                          gear_bool has_authorization,
                          gear_bool has_proxy_authorization,
                          gear_bool has_cookies,
                          smart_str* soap_headers);
#endif
