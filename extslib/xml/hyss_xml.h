/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_XML_H
#define HYSS_XML_H

#ifdef HAVE_XML
extern gear_capi_entry xml_capi_entry;
#define xml_capi_ptr &xml_capi_entry

#include "hyss_version.h"
#define HYSS_XML_VERSION HYSS_VERSION

#else
#define xml_capi_ptr NULL
#endif

#ifdef HAVE_XML

#include "expat_compat.h"

#ifdef XML_UNICODE
#error "UTF-16 Unicode support not implemented!"
#endif

GEAR_BEGIN_CAPI_GLOBALS(xml)
	XML_Char *default_encoding;
GEAR_END_CAPI_GLOBALS(xml)

typedef struct {
	int case_folding;
	XML_Parser parser;
	XML_Char *target_encoding;

	zval index;
	zval startElementHandler;
	zval endElementHandler;
	zval characterDataHandler;
	zval processingInstructionHandler;
	zval defaultHandler;
	zval unparsedEntityDeclHandler;
	zval notationDeclHandler;
	zval externalEntityRefHandler;
	zval unknownEncodingHandler;
	zval startNamespaceDeclHandler;
	zval endNamespaceDeclHandler;

	gear_function *startElementPtr;
	gear_function *endElementPtr;
	gear_function *characterDataPtr;
	gear_function *processingInstructionPtr;
	gear_function *defaultPtr;
	gear_function *unparsedEntityDeclPtr;
	gear_function *notationDeclPtr;
	gear_function *externalEntityRefPtr;
	gear_function *unknownEncodingPtr;
	gear_function *startNamespaceDeclPtr;
	gear_function *endNamespaceDeclPtr;

	zval object;

	zval data;
	zval info;
	int level;
	int toffset;
	int curtag;
	zval *ctag;
	char **ltags;
	int lastwasopen;
	int skipwhite;
	int isparsing;

	XML_Char *baseURI;
} xml_parser;


typedef struct {
	XML_Char *name;
	char (*decoding_function)(unsigned short);
	unsigned short (*encoding_function)(unsigned char);
} xml_encoding;


enum hyss_xml_option {
    HYSS_XML_OPTION_CASE_FOLDING = 1,
    HYSS_XML_OPTION_TARGET_ENCODING,
    HYSS_XML_OPTION_SKIP_TAGSTART,
    HYSS_XML_OPTION_SKIP_WHITE
};

/* for xml_parse_into_struct */

#define XML_MAXLEVEL 255 /* XXX this should be dynamic */

HYSS_FUNCTION(xml_parser_create);
HYSS_FUNCTION(xml_parser_create_ns);
HYSS_FUNCTION(xml_set_object);
HYSS_FUNCTION(xml_set_element_handler);
HYSS_FUNCTION(xml_set_character_data_handler);
HYSS_FUNCTION(xml_set_processing_instruction_handler);
HYSS_FUNCTION(xml_set_default_handler);
HYSS_FUNCTION(xml_set_unparsed_entity_decl_handler);
HYSS_FUNCTION(xml_set_notation_decl_handler);
HYSS_FUNCTION(xml_set_external_entity_ref_handler);
HYSS_FUNCTION(xml_set_start_namespace_decl_handler);
HYSS_FUNCTION(xml_set_end_namespace_decl_handler);
HYSS_FUNCTION(xml_parse);
HYSS_FUNCTION(xml_get_error_code);
HYSS_FUNCTION(xml_error_string);
HYSS_FUNCTION(xml_get_current_line_number);
HYSS_FUNCTION(xml_get_current_column_number);
HYSS_FUNCTION(xml_get_current_byte_index);
HYSS_FUNCTION(xml_parser_free);
HYSS_FUNCTION(xml_parser_set_option);
HYSS_FUNCTION(xml_parser_get_option);
HYSS_FUNCTION(utf8_encode);
HYSS_FUNCTION(utf8_decode);
HYSS_FUNCTION(xml_parse_into_struct);

HYSS_XML_API char *_xml_zval_strdup(zval *);
HYSS_XML_API gear_string *xml_utf8_decode(const XML_Char *, size_t, const XML_Char *);
HYSS_XML_API gear_string *xml_utf8_encode(const char *, size_t, const XML_Char *);

#endif /* HAVE_LIBEXPAT */

#define hyssext_xml_ptr xml_capi_ptr

#define XML(v) GEAR_CAPI_GLOBALS_ACCESSOR(xml, v)

#if defined(ZTS) && defined(COMPILE_DL_XML)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#endif /* HYSS_XML_H */

