dnl config.m4 for extension xml

HYSS_ARG_ENABLE(xml,whether to enable XML support,
[  --disable-xml           Disable XML support], yes)

if test -z "$HYSS_LIBXML_DIR"; then
  HYSS_ARG_WITH(libxml-dir, libxml2 install dir,
  [  --with-libxml-dir=DIR   XML: libxml2 install prefix], no, no)
fi

HYSS_ARG_WITH(libexpat-dir, libexpat install dir,
[  --with-libexpat-dir=DIR XML: libexpat install prefix (deprecated)], no, no)

if test "$HYSS_XML" != "no"; then

  dnl
  dnl Default to libxml2 if --with-libexpat-dir is not used.
  dnl
  if test "$HYSS_LIBEXPAT_DIR" = "no"; then

    if test "$HYSS_LIBXML" = "no"; then
      AC_MSG_ERROR([XML extension requires LIBXML extension, add --enable-libxml])
    fi

    HYSS_SETUP_LIBXML(XML_SHARED_LIBADD, [
      xml_extra_sources="compat.c"
      HYSS_ADD_EXTENSION_DEP(xml, libxml)
    ], [
      AC_MSG_ERROR([libxml2 not found. Use --with-libxml-dir=<DIR>])
    ])
  fi

  dnl
  dnl Check for expat only if --with-libexpat-dir is used.
  dnl
  if test "$HYSS_LIBEXPAT_DIR" != "no"; then
    for i in $HYSS_XML $HYSS_LIBEXPAT_DIR /usr /usr/local; do
      if test -f "$i/$HYSS_LIBDIR/libexpat.a" || test -f "$i/$HYSS_LIBDIR/libexpat.$SHLIB_SUFFIX_NAME"; then
        EXPAT_DIR=$i
        break
      fi
    done

    if test -z "$EXPAT_DIR"; then
      AC_MSG_ERROR([not found. Please reinstall the expat distribution.])
    fi

    HYSS_ADD_INCLUDE($EXPAT_DIR/include)
    HYSS_ADD_LIBRARY_WITH_PATH(expat, $EXPAT_DIR/$HYSS_LIBDIR, XML_SHARED_LIBADD)
    AC_DEFINE(HAVE_LIBEXPAT, 1, [ ])
  fi

  HYSS_NEW_EXTENSION(xml, xml.c $xml_extra_sources, $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
  HYSS_SUBST(XML_SHARED_LIBADD)
  HYSS_INSTALL_HEADERS([extslib/xml/])
  AC_DEFINE(HAVE_XML, 1, [ ])
fi
