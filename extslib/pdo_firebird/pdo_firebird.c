/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "pdo/hyss_pdo.h"
#include "pdo/hyss_pdo_driver.h"
#include "hyss_pdo_firebird.h"
#include "hyss_pdo_firebird_int.h"

static const gear_function_entry pdo_firebird_functions[] = { /* {{{ */
	HYSS_FE_END
};
/* }}} */

/* {{{ pdo_firebird_deps
 */
static const gear_capi_dep pdo_firebird_deps[] = {
	GEAR_CAPI_REQUIRED("pdo")
	GEAR_CAPI_END
};
/* }}} */

gear_capi_entry pdo_firebird_capi_entry = { /* {{{ */
	STANDARD_CAPI_HEADER_EX, NULL,
	pdo_firebird_deps,
	"PDO_Firebird",
	pdo_firebird_functions,
	HYSS_MINIT(pdo_firebird),
	HYSS_MSHUTDOWN(pdo_firebird),
	NULL,
	NULL,
	HYSS_MINFO(pdo_firebird),
	HYSS_PDO_FIREBIRD_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_PDO_FIREBIRD
GEAR_GET_CAPI(pdo_firebird)
#endif

HYSS_MINIT_FUNCTION(pdo_firebird) /* {{{ */
{
	REGISTER_PDO_CLASS_CONST_LONG("FB_ATTR_DATE_FORMAT", (gear_long) PDO_FB_ATTR_DATE_FORMAT);
	REGISTER_PDO_CLASS_CONST_LONG("FB_ATTR_TIME_FORMAT", (gear_long) PDO_FB_ATTR_TIME_FORMAT);
	REGISTER_PDO_CLASS_CONST_LONG("FB_ATTR_TIMESTAMP_FORMAT", (gear_long) PDO_FB_ATTR_TIMESTAMP_FORMAT);

	hyss_pdo_register_driver(&pdo_firebird_driver);

	return SUCCESS;
}
/* }}} */

HYSS_MSHUTDOWN_FUNCTION(pdo_firebird) /* {{{ */
{
	hyss_pdo_unregister_driver(&pdo_firebird_driver);

	return SUCCESS;
}
/* }}} */

HYSS_MINFO_FUNCTION(pdo_firebird) /* {{{ */
{
	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "PDO Driver for Firebird", "enabled");
	hyss_info_print_table_end();
}
/* }}} */

