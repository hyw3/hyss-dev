/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_ACCELERATOR_HASH_H
#define GEAR_ACCELERATOR_HASH_H

#include "gear.h"

/*
	gear_accel_hash - is a hash table allocated in shared memory and
	distributed across simultaneously running processes. The hash tables have
	fixed sizen selected during construction by gear_accel_hash_init(). All the
	hash entries are preallocated in the 'hash_entries' array. 'num_entries' is
	initialized by zero and grows when new data is added.
	gear_accel_hash_update() just takes the next entry from 'hash_entries'
	array and puts it into appropriate place of 'hash_table'.
	Hash collisions are resolved by separate chaining with linked lists,
	however, entries are still taken from the same 'hash_entries' array.
	'key' and 'data' passed to gear_accel_hash_update() must be already
	allocated in shared memory. Few keys may be resolved to the same data.
	using 'indirect' entries, that point to other entries ('data' is actually
	a pointer to another gear_accel_hash_entry).
	gear_accel_hash_update() requires exclusive lock, however,
	gear_accel_hash_find() does not.
*/

typedef struct _gear_accel_hash_entry gear_accel_hash_entry;

struct _gear_accel_hash_entry {
	gear_ulong             hash_value;
	const char            *key;
	gear_accel_hash_entry *next;
	void                  *data;
	uint32_t               key_length;
	gear_bool              indirect;
};

typedef struct _gear_accel_hash {
	gear_accel_hash_entry **hash_table;
	gear_accel_hash_entry  *hash_entries;
	uint32_t               num_entries;
	uint32_t               max_num_entries;
	uint32_t               num_direct_entries;
} gear_accel_hash;

void gear_accel_hash_init(gear_accel_hash *accel_hash, uint32_t hash_size);
void gear_accel_hash_clean(gear_accel_hash *accel_hash);

gear_accel_hash_entry* gear_accel_hash_update(
		gear_accel_hash        *accel_hash,
		const char             *key,
		uint32_t               key_length,
		gear_bool               indirect,
		void                   *data);

void* gear_accel_hash_find(
		gear_accel_hash        *accel_hash,
		gear_string            *key);

gear_accel_hash_entry* gear_accel_hash_find_entry(
		gear_accel_hash        *accel_hash,
		gear_string            *key);

void* gear_accel_hash_str_find(
		gear_accel_hash        *accel_hash,
		const char             *key,
		uint32_t               key_length);

gear_accel_hash_entry* gear_accel_hash_str_find_entry(
		gear_accel_hash        *accel_hash,
		const char             *key,
		uint32_t               key_length);

int gear_accel_hash_unlink(
		gear_accel_hash        *accel_hash,
		const char             *key,
		uint32_t               key_length);

static inline gear_bool gear_accel_hash_is_full(gear_accel_hash *accel_hash)
{
	if (accel_hash->num_entries == accel_hash->max_num_entries) {
		return 1;
	} else {
		return 0;
	}
}

#endif /* GEAR_ACCELERATOR_HASH_H */
