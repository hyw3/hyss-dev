/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_ACCELERATOR_BLACKLIST_H
#define GEAR_ACCELERATOR_BLACKLIST_H

typedef struct _gear_regexp_list gear_regexp_list;

typedef struct _gear_blacklist_entry {
    char *path;
    int   path_length;
	int   id;
} gear_blacklist_entry;

typedef struct _gear_blacklist {
	gear_blacklist_entry *entries;
	int                   size;
	int                   pos;
	gear_regexp_list     *regexp_list;
} gear_blacklist;

typedef int (*blacklist_apply_func_arg_t)(gear_blacklist_entry *, zval *);

extern gear_blacklist accel_blacklist;

void gear_accel_blacklist_init(gear_blacklist *blacklist);
void gear_accel_blacklist_shutdown(gear_blacklist *blacklist);

void gear_accel_blacklist_load(gear_blacklist *blacklist, char *filename);
gear_bool gear_accel_blacklist_is_blacklisted(gear_blacklist *blacklist, char *verify_path, size_t verify_path_len);
void gear_accel_blacklist_apply(gear_blacklist *blacklist, blacklist_apply_func_arg_t func, void *argument);

#endif /* GEAR_ACCELERATOR_BLACKLIST_H */
