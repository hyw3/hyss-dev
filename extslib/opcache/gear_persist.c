/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "GearAccelerator.h"
#include "gear_persist.h"
#include "gear_extensions.h"
#include "gear_shared_alloc.h"
#include "gear_vm.h"
#include "gear_constants.h"
#include "gear_operators.h"

#define gear_accel_store(p, size) \
	    (p = _gear_shared_memdup((void*)p, size, 1))
#define gear_accel_memdup(p, size) \
	    _gear_shared_memdup((void*)p, size, 0)

#ifdef HAVE_OPCACHE_FILE_CACHE
#define gear_set_str_gc_flags(str) do { \
	if (file_cache_only) { \
		GC_TYPE_INFO(str) = IS_STRING | (IS_STR_INTERNED << GC_FLAGS_SHIFT); \
	} else { \
		GC_TYPE_INFO(str) = IS_STRING | ((IS_STR_INTERNED | IS_STR_PERMANENT) << GC_FLAGS_SHIFT); \
	} \
} while (0)
#else
#define gear_set_str_gc_flags(str) do {\
	GC_TYPE_INFO(str) = IS_STRING | ((IS_STR_INTERNED | IS_STR_PERMANENT) << GC_FLAGS_SHIFT); \
} while (0)
#endif

#define gear_accel_store_string(str) do { \
		gear_string *new_str = gear_shared_alloc_get_xlat_entry(str); \
		if (new_str) { \
			gear_string_release_ex(str, 0); \
			str = new_str; \
		} else { \
	    	new_str = gear_accel_memdup((void*)str, _ZSTR_STRUCT_SIZE(ZSTR_LEN(str))); \
			gear_string_release_ex(str, 0); \
	    	str = new_str; \
	    	gear_string_hash_val(str); \
		gear_set_str_gc_flags(str); \
		} \
    } while (0)
#define gear_accel_memdup_string(str) do { \
		str = gear_accel_memdup(str, _ZSTR_STRUCT_SIZE(ZSTR_LEN(str))); \
    	gear_string_hash_val(str); \
		gear_set_str_gc_flags(str); \
	} while (0)
#define gear_accel_store_interned_string(str) do { \
		if (!IS_ACCEL_INTERNED(str)) { \
			gear_accel_store_string(str); \
		} \
	} while (0)
#define gear_accel_memdup_interned_string(str) do { \
		if (!IS_ACCEL_INTERNED(str)) { \
			gear_accel_memdup_string(str); \
		} \
	} while (0)

typedef void (*gear_persist_func_t)(zval*);

static void gear_persist_zval(zval *z);

static const uint32_t uninitialized_bucket[-HT_MIN_MASK] =
	{HT_INVALID_IDX, HT_INVALID_IDX};

static void gear_hash_persist(HashTable *ht, gear_persist_func_t pPersistElement)
{
	uint32_t idx, nIndex;
	Bucket *p;

	HT_FLAGS(ht) |= HASH_FLAG_STATIC_KEYS;
	ht->pDestructor = NULL;

	if (!(HT_FLAGS(ht) & HASH_FLAG_INITIALIZED)) {
		if (EXPECTED(!ZCG(current_persistent_script)->corrupted)) {
			HT_SET_DATA_ADDR(ht, &ZCSG(uninitialized_bucket));
		} else {
			HT_SET_DATA_ADDR(ht, &uninitialized_bucket);
		}
		return;
	}
	if (ht->nNumUsed == 0) {
		efree(HT_GET_DATA_ADDR(ht));
		ht->nTableMask = HT_MIN_MASK;
		if (EXPECTED(!ZCG(current_persistent_script)->corrupted)) {
			HT_SET_DATA_ADDR(ht, &ZCSG(uninitialized_bucket));
		} else {
			HT_SET_DATA_ADDR(ht, &uninitialized_bucket);
		}
		HT_FLAGS(ht) &= ~HASH_FLAG_INITIALIZED;
		return;
	}
	if (HT_FLAGS(ht) & HASH_FLAG_PACKED) {
		void *data = HT_GET_DATA_ADDR(ht);
		gear_accel_store(data, HT_USED_SIZE(ht));
		HT_SET_DATA_ADDR(ht, data);
	} else if (ht->nNumUsed < (uint32_t)(-(int32_t)ht->nTableMask) / 4) {
		/* compact table */
		void *old_data = HT_GET_DATA_ADDR(ht);
		Bucket *old_buckets = ht->arData;
		uint32_t hash_size;

		if (ht->nNumUsed <= HT_MIN_SIZE) {
			hash_size = HT_MIN_SIZE * 2;
		} else {
			hash_size = (uint32_t)(-(int32_t)ht->nTableMask);
			while (hash_size >> 2 > ht->nNumUsed) {
				hash_size >>= 1;
			}
		}
		ht->nTableMask = (uint32_t)(-(int32_t)hash_size);
		GEAR_ASSERT(((gear_uintptr_t)ZCG(mem) & 0x7) == 0); /* should be 8 byte aligned */
		HT_SET_DATA_ADDR(ht, ZCG(mem));
		ZCG(mem) = (void*)((char*)ZCG(mem) + GEAR_ALIGNED_SIZE((hash_size * sizeof(uint32_t)) + (ht->nNumUsed * sizeof(Bucket))));
		HT_HASH_RESET(ht);
		memcpy(ht->arData, old_buckets, ht->nNumUsed * sizeof(Bucket));
		efree(old_data);

		for (idx = 0; idx < ht->nNumUsed; idx++) {
			p = ht->arData + idx;
			if (Z_TYPE(p->val) == IS_UNDEF) continue;

			/* persist bucket and key */
			if (p->key) {
				gear_accel_store_interned_string(p->key);
			}

			/* persist the data itself */
			pPersistElement(&p->val);

			nIndex = p->h | ht->nTableMask;
			Z_NEXT(p->val) = HT_HASH(ht, nIndex);
			HT_HASH(ht, nIndex) = HT_IDX_TO_HASH(idx);
		}
		return;
	} else {
		void *data = ZCG(mem);
		void *old_data = HT_GET_DATA_ADDR(ht);

		GEAR_ASSERT(((gear_uintptr_t)ZCG(mem) & 0x7) == 0); /* should be 8 byte aligned */
		ZCG(mem) = (void*)((char*)data + GEAR_ALIGNED_SIZE(HT_USED_SIZE(ht)));
		memcpy(data, old_data, HT_USED_SIZE(ht));
		efree(old_data);
		HT_SET_DATA_ADDR(ht, data);
	}

	for (idx = 0; idx < ht->nNumUsed; idx++) {
		p = ht->arData + idx;
		if (Z_TYPE(p->val) == IS_UNDEF) continue;

		/* persist bucket and key */
		if (p->key) {
			gear_accel_store_interned_string(p->key);
		}

		/* persist the data itself */
		pPersistElement(&p->val);
	}
}

static void gear_hash_persist_immutable(HashTable *ht)
{
	uint32_t idx, nIndex;
	Bucket *p;

	HT_FLAGS(ht) |= HASH_FLAG_STATIC_KEYS;
	ht->pDestructor = NULL;

	if (!(HT_FLAGS(ht) & HASH_FLAG_INITIALIZED)) {
		if (EXPECTED(!ZCG(current_persistent_script)->corrupted)) {
			HT_SET_DATA_ADDR(ht, &ZCSG(uninitialized_bucket));
		} else {
			HT_SET_DATA_ADDR(ht, &uninitialized_bucket);
		}
		return;
	}
	if (ht->nNumUsed == 0) {
		efree(HT_GET_DATA_ADDR(ht));
		ht->nTableMask = HT_MIN_MASK;
		if (EXPECTED(!ZCG(current_persistent_script)->corrupted)) {
			HT_SET_DATA_ADDR(ht, &ZCSG(uninitialized_bucket));
		} else {
			HT_SET_DATA_ADDR(ht, &uninitialized_bucket);
		}
		HT_FLAGS(ht) &= ~HASH_FLAG_INITIALIZED;
		return;
	}
	if (HT_FLAGS(ht) & HASH_FLAG_PACKED) {
		HT_SET_DATA_ADDR(ht, gear_accel_memdup(HT_GET_DATA_ADDR(ht), HT_USED_SIZE(ht)));
	} else if (ht->nNumUsed < (uint32_t)(-(int32_t)ht->nTableMask) / 4) {
		/* compact table */
		void *old_data = HT_GET_DATA_ADDR(ht);
		Bucket *old_buckets = ht->arData;
		uint32_t hash_size;

		if (ht->nNumUsed <= HT_MIN_SIZE) {
			hash_size = HT_MIN_SIZE * 2;
		} else {
			hash_size = (uint32_t)(-(int32_t)ht->nTableMask);
			while (hash_size >> 2 > ht->nNumUsed) {
				hash_size >>= 1;
			}
		}
		ht->nTableMask = (uint32_t)(-(int32_t)hash_size);
		GEAR_ASSERT(((gear_uintptr_t)ZCG(mem) & 0x7) == 0); /* should be 8 byte aligned */
		HT_SET_DATA_ADDR(ht, ZCG(mem));
		ZCG(mem) = (void*)((char*)ZCG(mem) + (hash_size * sizeof(uint32_t)) + (ht->nNumUsed * sizeof(Bucket)));
		HT_HASH_RESET(ht);
		memcpy(ht->arData, old_buckets, ht->nNumUsed * sizeof(Bucket));
		efree(old_data);

		for (idx = 0; idx < ht->nNumUsed; idx++) {
			p = ht->arData + idx;
			if (Z_TYPE(p->val) == IS_UNDEF) continue;

			/* persist bucket and key */
			if (p->key) {
				gear_accel_memdup_interned_string(p->key);
			}

			/* persist the data itself */
			gear_persist_zval(&p->val);

			nIndex = p->h | ht->nTableMask;
			Z_NEXT(p->val) = HT_HASH(ht, nIndex);
			HT_HASH(ht, nIndex) = HT_IDX_TO_HASH(idx);
		}
		return;
	} else {
		void *data = ZCG(mem);

		GEAR_ASSERT(((gear_uintptr_t)ZCG(mem) & 0x7) == 0); /* should be 8 byte aligned */
		ZCG(mem) = (void*)((char*)data + GEAR_ALIGNED_SIZE(HT_USED_SIZE(ht)));
		memcpy(data, HT_GET_DATA_ADDR(ht), HT_USED_SIZE(ht));
		HT_SET_DATA_ADDR(ht, data);
	}
	for (idx = 0; idx < ht->nNumUsed; idx++) {
		p = ht->arData + idx;
		if (Z_TYPE(p->val) == IS_UNDEF) continue;

		/* persist bucket and key */
		if (p->key) {
			gear_accel_memdup_interned_string(p->key);
		}

		/* persist the data itself */
		gear_persist_zval(&p->val);
	}
}

static gear_ast *gear_persist_ast(gear_ast *ast)
{
	uint32_t i;
	gear_ast *node;

	if (ast->kind == GEAR_AST_ZVAL || ast->kind == GEAR_AST_CONSTANT) {
		gear_ast_zval *copy = gear_accel_memdup(ast, sizeof(gear_ast_zval));
		gear_persist_zval(&copy->val);
		node = (gear_ast *) copy;
	} else if (gear_ast_is_list(ast)) {
		gear_ast_list *list = gear_ast_get_list(ast);
		gear_ast_list *copy = gear_accel_memdup(ast,
			sizeof(gear_ast_list) - sizeof(gear_ast *) + sizeof(gear_ast *) * list->children);
		for (i = 0; i < list->children; i++) {
			if (copy->child[i]) {
				copy->child[i] = gear_persist_ast(copy->child[i]);
			}
		}
		node = (gear_ast *) copy;
	} else {
		uint32_t children = gear_ast_get_num_children(ast);
		node = gear_accel_memdup(ast, sizeof(gear_ast) - sizeof(gear_ast *) + sizeof(gear_ast *) * children);
		for (i = 0; i < children; i++) {
			if (node->child[i]) {
				node->child[i] = gear_persist_ast(node->child[i]);
			}
		}
	}

	return node;
}

static void gear_persist_zval(zval *z)
{
	void *new_ptr;

	switch (Z_TYPE_P(z)) {
		case IS_STRING:
			gear_accel_store_interned_string(Z_STR_P(z));
			Z_TYPE_FLAGS_P(z) = 0;
			break;
		case IS_ARRAY:
			new_ptr = gear_shared_alloc_get_xlat_entry(Z_ARR_P(z));
			if (new_ptr) {
				Z_ARR_P(z) = new_ptr;
				Z_TYPE_FLAGS_P(z) = 0;
			} else {
				if (!Z_REFCOUNTED_P(z)) {
					Z_ARR_P(z) = gear_accel_memdup(Z_ARR_P(z), sizeof(gear_array));
					gear_hash_persist_immutable(Z_ARRVAL_P(z));
				} else {
					GC_REMOVE_FROM_BUFFER(Z_ARR_P(z));
					gear_accel_store(Z_ARR_P(z), sizeof(gear_array));
					gear_hash_persist(Z_ARRVAL_P(z), gear_persist_zval);
					/* make immutable array */
					Z_TYPE_FLAGS_P(z) = 0;
					GC_SET_REFCOUNT(Z_COUNTED_P(z), 2);
					GC_ADD_FLAGS(Z_COUNTED_P(z), IS_ARRAY_IMMUTABLE);
				}
			}
			break;
		case IS_REFERENCE:
			new_ptr = gear_shared_alloc_get_xlat_entry(Z_REF_P(z));
			if (new_ptr) {
				Z_REF_P(z) = new_ptr;
			} else {
				gear_accel_store(Z_REF_P(z), sizeof(gear_reference));
				gear_persist_zval(Z_REFVAL_P(z));
			}
			break;
		case IS_CONSTANT_AST:
			new_ptr = gear_shared_alloc_get_xlat_entry(Z_AST_P(z));
			if (new_ptr) {
				Z_AST_P(z) = new_ptr;
				Z_TYPE_FLAGS_P(z) = 0;
			} else {
				gear_ast_ref *old_ref = Z_AST_P(z);
				Z_ARR_P(z) = gear_accel_memdup(Z_AST_P(z), sizeof(gear_ast_ref));
				gear_persist_ast(GC_AST(old_ref));
				Z_TYPE_FLAGS_P(z) = 0;
				GC_SET_REFCOUNT(Z_COUNTED_P(z), 1);
				efree(old_ref);
			}
			break;
	}
}

static void gear_persist_op_array_ex(gear_op_array *op_array, gear_persistent_script* main_persistent_script)
{
	int already_stored = 0;
	gear_op *persist_ptr;
	zval *orig_literals = NULL;

	if (op_array->refcount && --(*op_array->refcount) == 0) {
		efree(op_array->refcount);
	}
	op_array->refcount = NULL;

	if (main_persistent_script) {
		gear_execute_data *orig_execute_data = EG(current_execute_data);
		gear_execute_data fake_execute_data;
		zval *offset;

		memset(&fake_execute_data, 0, sizeof(fake_execute_data));
		fake_execute_data.func = (gear_function*)op_array;
		EG(current_execute_data) = &fake_execute_data;
		if ((offset = gear_get_constant_str("__COMPILER_HALT_OFFSET__", sizeof("__COMPILER_HALT_OFFSET__") - 1)) != NULL) {
			main_persistent_script->compiler_halt_offset = Z_LVAL_P(offset);
		}
		EG(current_execute_data) = orig_execute_data;
	}

	if (op_array->static_variables) {
		HashTable *stored = gear_shared_alloc_get_xlat_entry(op_array->static_variables);

		if (stored) {
			op_array->static_variables = stored;
		} else {
			gear_hash_persist(op_array->static_variables, gear_persist_zval);
			gear_accel_store(op_array->static_variables, sizeof(HashTable));
			/* make immutable array */
			GC_SET_REFCOUNT(op_array->static_variables, 2);
			GC_TYPE_INFO(op_array->static_variables) = IS_ARRAY | (IS_ARRAY_IMMUTABLE << GC_FLAGS_SHIFT);
		}
	}

	if (gear_shared_alloc_get_xlat_entry(op_array->opcodes)) {
		already_stored = 1;
	}

	if (op_array->literals) {
		if (already_stored) {
			orig_literals = gear_shared_alloc_get_xlat_entry(op_array->literals);
			GEAR_ASSERT(orig_literals != NULL);
			op_array->literals = orig_literals;
		} else {
			zval *p = gear_accel_memdup(op_array->literals, sizeof(zval) * op_array->last_literal);
			zval *end = p + op_array->last_literal;
			orig_literals = op_array->literals;
			op_array->literals = p;
			while (p < end) {
				gear_persist_zval(p);
				p++;
			}
#if GEAR_USE_ABS_CONST_ADDR
			efree(orig_literals);
#endif
		}
	}

	if (already_stored) {
		persist_ptr = gear_shared_alloc_get_xlat_entry(op_array->opcodes);
		GEAR_ASSERT(persist_ptr != NULL);
		op_array->opcodes = persist_ptr;
	} else {
		gear_op *new_opcodes = gear_accel_memdup(op_array->opcodes, sizeof(gear_op) * op_array->last);
		gear_op *opline = new_opcodes;
		gear_op *end = new_opcodes + op_array->last;
		int offset = 0;

		for (; opline < end ; opline++, offset++) {
#if GEAR_USE_ABS_CONST_ADDR
			if (opline->op1_type == IS_CONST) {
				opline->op1.zv = (zval*)((char*)opline->op1.zv + ((char*)op_array->literals - (char*)orig_literals));
				if (opline->opcode == GEAR_SEND_VAL
				 || opline->opcode == GEAR_SEND_VAL_EX
				 || opline->opcode == GEAR_QM_ASSIGN) {
					/* Update handlers to eliminate REFCOUNTED check */
					gear_vm_set_opcode_handler_ex(opline, 0, 0, 0);
				}
			}
			if (opline->op2_type == IS_CONST) {
				opline->op2.zv = (zval*)((char*)opline->op2.zv + ((char*)op_array->literals - (char*)orig_literals));
			}
#else
			if (opline->op1_type == IS_CONST) {
				opline->op1.constant =
					(char*)(op_array->literals +
						((zval*)((char*)(op_array->opcodes + (opline - new_opcodes)) +
						(int32_t)opline->op1.constant) - orig_literals)) -
					(char*)opline;
				if (opline->opcode == GEAR_SEND_VAL
				 || opline->opcode == GEAR_SEND_VAL_EX
				 || opline->opcode == GEAR_QM_ASSIGN) {
					gear_vm_set_opcode_handler_ex(opline, 0, 0, 0);
				}
			}
			if (opline->op2_type == IS_CONST) {
				opline->op2.constant =
					(char*)(op_array->literals +
						((zval*)((char*)(op_array->opcodes + (opline - new_opcodes)) +
						(int32_t)opline->op2.constant) - orig_literals)) -
					(char*)opline;
			}
#endif
#if GEAR_USE_ABS_JMP_ADDR
			if (op_array->fn_flags & GEAR_ACC_DONE_PASS_TWO) {
				/* fix jumps to point to new array */
				switch (opline->opcode) {
					case GEAR_JMP:
					case GEAR_FAST_CALL:
						opline->op1.jmp_addr = &new_opcodes[opline->op1.jmp_addr - op_array->opcodes];
						break;
					case GEAR_JMPZNZ:
						/* relative extended_value don't have to be changed */
						/* break omitted intentionally */
					case GEAR_JMPZ:
					case GEAR_JMPNZ:
					case GEAR_JMPZ_EX:
					case GEAR_JMPNZ_EX:
					case GEAR_JMP_SET:
					case GEAR_COALESCE:
					case GEAR_FE_RESET_R:
					case GEAR_FE_RESET_RW:
					case GEAR_ASSERT_CHECK:
						opline->op2.jmp_addr = &new_opcodes[opline->op2.jmp_addr - op_array->opcodes];
						break;
					case GEAR_CATCH:
						if (!(opline->extended_value & GEAR_LAST_CATCH)) {
							opline->op2.jmp_addr = &new_opcodes[opline->op2.jmp_addr - op_array->opcodes];
						}
						break;
					case GEAR_DECLARE_ANON_CLASS:
					case GEAR_DECLARE_ANON_INHERITED_CLASS:
					case GEAR_FE_FETCH_R:
					case GEAR_FE_FETCH_RW:
					case GEAR_SWITCH_LONG:
					case GEAR_SWITCH_STRING:
						/* relative extended_value don't have to be changed */
						break;
				}
			}
#endif
		}

		efree(op_array->opcodes);
		op_array->opcodes = new_opcodes;

		if (op_array->run_time_cache) {
			efree(op_array->run_time_cache);
			op_array->run_time_cache = NULL;
		}
	}

	if (op_array->function_name && !IS_ACCEL_INTERNED(op_array->function_name)) {
		gear_string *new_name;
		if (already_stored) {
			new_name = gear_shared_alloc_get_xlat_entry(op_array->function_name);
			GEAR_ASSERT(new_name != NULL);
			op_array->function_name = new_name;
		} else {
			gear_accel_store_interned_string(op_array->function_name);
		}
	}

	if (op_array->filename) {
		/* do not free! HYSS has centralized filename storage, compiler will free it */
		gear_accel_memdup_string(op_array->filename);
	}

	if (op_array->arg_info) {
		gear_arg_info *arg_info = op_array->arg_info;
		uint32_t num_args = op_array->num_args;

		if (op_array->fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
			arg_info--;
			num_args++;
		}
		if (already_stored) {
			arg_info = gear_shared_alloc_get_xlat_entry(arg_info);
			GEAR_ASSERT(arg_info != NULL);
		} else {
			uint32_t i;

			if (op_array->fn_flags & GEAR_ACC_VARIADIC) {
				num_args++;
			}
			gear_accel_store(arg_info, sizeof(gear_arg_info) * num_args);
			for (i = 0; i < num_args; i++) {
				if (arg_info[i].name) {
					gear_accel_store_interned_string(arg_info[i].name);
				}
				if (GEAR_TYPE_IS_CLASS(arg_info[i].type)) {
					gear_string *type_name = GEAR_TYPE_NAME(arg_info[i].type);
					gear_bool allow_null = GEAR_TYPE_ALLOW_NULL(arg_info[i].type);

					gear_accel_store_interned_string(type_name);
					arg_info[i].type = GEAR_TYPE_ENCODE_CLASS(type_name, allow_null);
				}
			}
		}
		if (op_array->fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
			arg_info++;
		}
		op_array->arg_info = arg_info;
	}

	if (op_array->live_range) {
		gear_accel_store(op_array->live_range, sizeof(gear_live_range) * op_array->last_live_range);
	}

	if (op_array->scope) {
		op_array->scope = gear_shared_alloc_get_xlat_entry(op_array->scope);
	}

	if (op_array->doc_comment) {
		if (ZCG(accel_directives).save_comments) {
			if (already_stored) {
				op_array->doc_comment = gear_shared_alloc_get_xlat_entry(op_array->doc_comment);
				GEAR_ASSERT(op_array->doc_comment != NULL);
			} else {
				gear_accel_store_interned_string(op_array->doc_comment);
			}
		} else {
			if (!already_stored) {
				gear_string_release_ex(op_array->doc_comment, 0);
			}
			op_array->doc_comment = NULL;
		}
	}

	if (op_array->try_catch_array) {
		gear_accel_store(op_array->try_catch_array, sizeof(gear_try_catch_element) * op_array->last_try_catch);
	}

	if (op_array->vars) {
		if (already_stored) {
			persist_ptr = gear_shared_alloc_get_xlat_entry(op_array->vars);
			GEAR_ASSERT(persist_ptr != NULL);
			op_array->vars = (gear_string**)persist_ptr;
		} else {
			int i;
			gear_accel_store(op_array->vars, sizeof(gear_string*) * op_array->last_var);
			for (i = 0; i < op_array->last_var; i++) {
				gear_accel_store_interned_string(op_array->vars[i]);
			}
		}
	}

	/* "prototype" may be undefined if "scope" isn't set */
	if (op_array->scope && op_array->prototype) {
		if ((persist_ptr = gear_shared_alloc_get_xlat_entry(op_array->prototype))) {
			op_array->prototype = (union _gear_function*)persist_ptr;
		}
	} else {
		op_array->prototype = NULL;
	}

	ZCG(mem) = (void*)((char*)ZCG(mem) + GEAR_ALIGNED_SIZE(gear_extensions_op_array_persist(op_array, ZCG(mem))));
}

static void gear_persist_op_array(zval *zv)
{
	gear_op_array *op_array = Z_PTR_P(zv);

	GEAR_ASSERT(op_array->type == GEAR_USER_FUNCTION);
	memcpy(ZCG(mem), Z_PTR_P(zv), sizeof(gear_op_array));
	Z_PTR_P(zv) = ZCG(mem);
	ZCG(mem) = (void*)((char*)ZCG(mem) + GEAR_ALIGNED_SIZE(sizeof(gear_op_array)));
	gear_persist_op_array_ex(Z_PTR_P(zv), NULL);
	((gear_op_array*)Z_PTR_P(zv))->fn_flags |= GEAR_ACC_IMMUTABLE;
}

static void gear_persist_class_method(zval *zv)
{
	gear_op_array *op_array = Z_PTR_P(zv);
	gear_op_array *old_op_array;

	GEAR_ASSERT(op_array->type == GEAR_USER_FUNCTION);
	old_op_array = gear_shared_alloc_get_xlat_entry(op_array);
	if (old_op_array) {
		Z_PTR_P(zv) = old_op_array;
		if (op_array->refcount && --(*op_array->refcount) == 0) {
			efree(op_array->refcount);
		}
		return;
	}
	memcpy(ZCG(arena_mem), Z_PTR_P(zv), sizeof(gear_op_array));
	gear_shared_alloc_register_xlat_entry(Z_PTR_P(zv), ZCG(arena_mem));
	Z_PTR_P(zv) = ZCG(arena_mem);
	ZCG(arena_mem) = (void*)((char*)ZCG(arena_mem) + GEAR_ALIGNED_SIZE(sizeof(gear_op_array)));
	gear_persist_op_array_ex(Z_PTR_P(zv), NULL);
}

static void gear_persist_property_info(zval *zv)
{
	gear_property_info *prop = gear_shared_alloc_get_xlat_entry(Z_PTR_P(zv));

	if (prop) {
		Z_PTR_P(zv) = prop;
		return;
	}
	memcpy(ZCG(arena_mem), Z_PTR_P(zv), sizeof(gear_property_info));
	gear_shared_alloc_register_xlat_entry(Z_PTR_P(zv), ZCG(arena_mem));
	prop = Z_PTR_P(zv) = ZCG(arena_mem);
	ZCG(arena_mem) = (void*)((char*)ZCG(arena_mem) + GEAR_ALIGNED_SIZE(sizeof(gear_property_info)));
	prop->ce = gear_shared_alloc_get_xlat_entry(prop->ce);
	gear_accel_store_interned_string(prop->name);
	if (prop->doc_comment) {
		if (ZCG(accel_directives).save_comments) {
			gear_accel_store_interned_string(prop->doc_comment);
		} else {
			if (!gear_shared_alloc_get_xlat_entry(prop->doc_comment)) {
				gear_shared_alloc_register_xlat_entry(prop->doc_comment, prop->doc_comment);
			}
			gear_string_release_ex(prop->doc_comment, 0);
			prop->doc_comment = NULL;
		}
	}
}

static void gear_persist_class_constant(zval *zv)
{
	gear_class_constant *c = gear_shared_alloc_get_xlat_entry(Z_PTR_P(zv));

	if (c) {
		Z_PTR_P(zv) = c;
		return;
	}
	memcpy(ZCG(arena_mem), Z_PTR_P(zv), sizeof(gear_class_constant));
	gear_shared_alloc_register_xlat_entry(Z_PTR_P(zv), ZCG(arena_mem));
	c = Z_PTR_P(zv) = ZCG(arena_mem);
	ZCG(arena_mem) = (void*)((char*)ZCG(arena_mem) + GEAR_ALIGNED_SIZE(sizeof(gear_class_constant)));
	gear_persist_zval(&c->value);
	c->ce = gear_shared_alloc_get_xlat_entry(c->ce);
	if (c->doc_comment) {
		if (ZCG(accel_directives).save_comments) {
			gear_string *doc_comment = gear_shared_alloc_get_xlat_entry(c->doc_comment);
			if (doc_comment) {
				c->doc_comment = doc_comment;
			} else {
				gear_accel_store_interned_string(c->doc_comment);
			}
		} else {
			gear_string *doc_comment = gear_shared_alloc_get_xlat_entry(c->doc_comment);
			if (!doc_comment) {
				gear_shared_alloc_register_xlat_entry(c->doc_comment, c->doc_comment);
				gear_string_release_ex(c->doc_comment, 0);
			}
			c->doc_comment = NULL;
		}
	}
}

static void gear_persist_class_entry(zval *zv)
{
	gear_class_entry *ce = Z_PTR_P(zv);

	if (ce->type == GEAR_USER_CLASS) {
		memcpy(ZCG(arena_mem), Z_PTR_P(zv), sizeof(gear_class_entry));
		gear_shared_alloc_register_xlat_entry(Z_PTR_P(zv), ZCG(arena_mem));
		ce = Z_PTR_P(zv) = ZCG(arena_mem);
		ZCG(arena_mem) = (void*)((char*)ZCG(arena_mem) + GEAR_ALIGNED_SIZE(sizeof(gear_class_entry)));
		gear_accel_store_interned_string(ce->name);
		gear_hash_persist(&ce->function_table, gear_persist_class_method);
		if (ce->default_properties_table) {
		    int i;

			gear_accel_store(ce->default_properties_table, sizeof(zval) * ce->default_properties_count);
			for (i = 0; i < ce->default_properties_count; i++) {
				gear_persist_zval(&ce->default_properties_table[i]);
			}
		}
		if (ce->default_static_members_table) {
			int i;
			gear_accel_store(ce->default_static_members_table, sizeof(zval) * ce->default_static_members_count);

			/* Persist only static properties in this class.
			 * Static properties from parent classes will be handled in class_copy_ctor */
			i = ce->parent ? ce->parent->default_static_members_count : 0;
			for (; i < ce->default_static_members_count; i++) {
				gear_persist_zval(&ce->default_static_members_table[i]);
			}
		}
		ce->static_members_table = NULL;

		gear_hash_persist(&ce->constants_table, gear_persist_class_constant);

		if (ce->info.user.filename) {
			/* do not free! HYSS has centralized filename storage, compiler will free it */
			gear_accel_memdup_string(ce->info.user.filename);
		}
		if (ce->info.user.doc_comment) {
			if (ZCG(accel_directives).save_comments) {
				gear_accel_store_interned_string(ce->info.user.doc_comment);
			} else {
				if (!gear_shared_alloc_get_xlat_entry(ce->info.user.doc_comment)) {
					gear_shared_alloc_register_xlat_entry(ce->info.user.doc_comment, ce->info.user.doc_comment);
					gear_string_release_ex(ce->info.user.doc_comment, 0);
				}
				ce->info.user.doc_comment = NULL;
			}
		}
		gear_hash_persist(&ce->properties_info, gear_persist_property_info);
		if (ce->num_interfaces && ce->interfaces) {
			efree(ce->interfaces);
		}
		ce->interfaces = NULL; /* will be filled in on fetch */

		if (ce->num_traits && ce->traits) {
			efree(ce->traits);
		}
		ce->traits = NULL;

		if (ce->trait_aliases) {
			int i = 0;
			while (ce->trait_aliases[i]) {
				if (ce->trait_aliases[i]->trait_method.method_name) {
					gear_accel_store_interned_string(ce->trait_aliases[i]->trait_method.method_name);
				}
				if (ce->trait_aliases[i]->trait_method.class_name) {
					gear_accel_store_interned_string(ce->trait_aliases[i]->trait_method.class_name);
				}

				if (ce->trait_aliases[i]->alias) {
					gear_accel_store_interned_string(ce->trait_aliases[i]->alias);
				}

				gear_accel_store(ce->trait_aliases[i], sizeof(gear_trait_alias));
				i++;
			}

			gear_accel_store(ce->trait_aliases, sizeof(gear_trait_alias*) * (i + 1));
		}

		if (ce->trait_precedences) {
			int i = 0;
			int j;

			while (ce->trait_precedences[i]) {
				gear_accel_store_interned_string(ce->trait_precedences[i]->trait_method.method_name);
				gear_accel_store_interned_string(ce->trait_precedences[i]->trait_method.class_name);

				for (j = 0; j < ce->trait_precedences[i]->num_excludes; j++) {
					gear_accel_store_interned_string(ce->trait_precedences[i]->exclude_class_names[j]);
				}

				gear_accel_store(ce->trait_precedences[i], sizeof(gear_trait_precedence) + (ce->trait_precedences[i]->num_excludes - 1) * sizeof(gear_string*));
				i++;
			}
			gear_accel_store(
				ce->trait_precedences, sizeof(gear_trait_precedence*) * (i + 1));
		}
	}
}

//static int gear_update_property_info_ce(zval *zv)
//{
//	gear_property_info *prop = Z_PTR_P(zv);
//
//	prop->ce = gear_shared_alloc_get_xlat_entry(prop->ce);
//	return 0;
//}

static int gear_update_parent_ce(zval *zv)
{
	gear_class_entry *ce = Z_PTR_P(zv);

	if (ce->parent) {
		ce->parent = gear_shared_alloc_get_xlat_entry(ce->parent);
	}

	/* update methods */
	if (ce->constructor) {
		ce->constructor = gear_shared_alloc_get_xlat_entry(ce->constructor);
	}
	if (ce->destructor) {
		ce->destructor = gear_shared_alloc_get_xlat_entry(ce->destructor);
	}
	if (ce->clone) {
		ce->clone = gear_shared_alloc_get_xlat_entry(ce->clone);
	}
	if (ce->__get) {
		ce->__get = gear_shared_alloc_get_xlat_entry(ce->__get);
	}
	if (ce->__set) {
		ce->__set = gear_shared_alloc_get_xlat_entry(ce->__set);
	}
	if (ce->__call) {
		ce->__call = gear_shared_alloc_get_xlat_entry(ce->__call);
	}
	if (ce->serialize_func) {
		ce->serialize_func = gear_shared_alloc_get_xlat_entry(ce->serialize_func);
	}
	if (ce->unserialize_func) {
		ce->unserialize_func = gear_shared_alloc_get_xlat_entry(ce->unserialize_func);
	}
	if (ce->__isset) {
		ce->__isset = gear_shared_alloc_get_xlat_entry(ce->__isset);
	}
	if (ce->__unset) {
		ce->__unset = gear_shared_alloc_get_xlat_entry(ce->__unset);
	}
	if (ce->__tostring) {
		ce->__tostring = gear_shared_alloc_get_xlat_entry(ce->__tostring);
	}
	if (ce->__callstatic) {
		ce->__callstatic = gear_shared_alloc_get_xlat_entry(ce->__callstatic);
	}
	if (ce->__debugInfo) {
		ce->__debugInfo = gear_shared_alloc_get_xlat_entry(ce->__debugInfo);
	}
//	gear_hash_apply(&ce->properties_info, (apply_func_t) gear_update_property_info_ce);
	return 0;
}

static void gear_accel_persist_class_table(HashTable *class_table)
{
    gear_hash_persist(class_table, gear_persist_class_entry);
	gear_hash_apply(class_table, (apply_func_t) gear_update_parent_ce);
}

gear_persistent_script *gear_accel_script_persist(gear_persistent_script *script, const char **key, unsigned int key_length, int for_shm)
{
	script->mem = ZCG(mem);

	GEAR_ASSERT(((gear_uintptr_t)ZCG(mem) & 0x7) == 0); /* should be 8 byte aligned */
	gear_shared_alloc_clear_xlat_table();

	gear_accel_store(script, sizeof(gear_persistent_script));
	if (key && *key) {
		*key = gear_accel_memdup(*key, key_length + 1);
	}

	script->corrupted = 0;
	ZCG(current_persistent_script) = script;

	if (!for_shm) {
		/* script is not going to be saved in SHM */
		script->corrupted = 1;
	}

	gear_accel_store_interned_string(script->script.filename);

#if defined(__AVX__) || defined(__SSE2__)
	/* Align to 64-byte boundary */
	ZCG(mem) = (void*)(((gear_uintptr_t)ZCG(mem) + 63L) & ~63L);
#else
	GEAR_ASSERT(((gear_uintptr_t)ZCG(mem) & 0x7) == 0); /* should be 8 byte aligned */
#endif

	script->arena_mem = ZCG(arena_mem) = ZCG(mem);
	ZCG(mem) = (void*)((char*)ZCG(mem) + script->arena_size);

	gear_accel_persist_class_table(&script->script.class_table);
	gear_hash_persist(&script->script.function_table, gear_persist_op_array);
	gear_persist_op_array_ex(&script->script.main_op_array, script);

	script->corrupted = 0;
	ZCG(current_persistent_script) = NULL;

	return script;
}

int gear_accel_script_persistable(gear_persistent_script *script)
{
	return 1;
}
