/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_OPTIMIZER_H
#define GEAR_OPTIMIZER_H

#include "gear.h"
#include "gear_compile.h"

#define GEAR_OPTIMIZER_PASS_1		(1<<0)   /* CSE, STRING construction     */
#define GEAR_OPTIMIZER_PASS_2		(1<<1)   /* Constant conversion and jumps */
#define GEAR_OPTIMIZER_PASS_3		(1<<2)   /* ++, +=, series of jumps      */
#define GEAR_OPTIMIZER_PASS_4		(1<<3)   /* INIT_FCALL_BY_NAME -> DO_FCALL */
#define GEAR_OPTIMIZER_PASS_5		(1<<4)   /* CFG based optimization       */
#define GEAR_OPTIMIZER_PASS_6		(1<<5)   /* DFA based optimization       */
#define GEAR_OPTIMIZER_PASS_7		(1<<6)   /* CALL GRAPH optimization      */
#define GEAR_OPTIMIZER_PASS_8		(1<<7)   /* SCCP (constant propagation)  */
#define GEAR_OPTIMIZER_PASS_9		(1<<8)   /* TMP VAR usage                */
#define GEAR_OPTIMIZER_PASS_10		(1<<9)   /* NOP removal                 */
#define GEAR_OPTIMIZER_PASS_11		(1<<10)  /* Merge equal constants       */
#define GEAR_OPTIMIZER_PASS_12		(1<<11)  /* Adjust used stack           */
#define GEAR_OPTIMIZER_PASS_13		(1<<12)  /* Remove unused variables     */
#define GEAR_OPTIMIZER_PASS_14		(1<<13)  /* DCE (dead code elimination) */
#define GEAR_OPTIMIZER_PASS_15		(1<<14)  /* (unsafe) Collect constants */
#define GEAR_OPTIMIZER_PASS_16		(1<<15)  /* Inline functions */

#define GEAR_OPTIMIZER_IGNORE_OVERLOADING	(1<<16)  /* (unsafe) Ignore possibility of operator overloading */

#define GEAR_OPTIMIZER_ALL_PASSES	0x7FFFFFFF

#define DEFAULT_OPTIMIZATION_LEVEL  "0x7FFEBFFF"


#define GEAR_DUMP_AFTER_PASS_1		GEAR_OPTIMIZER_PASS_1
#define GEAR_DUMP_AFTER_PASS_2		GEAR_OPTIMIZER_PASS_2
#define GEAR_DUMP_AFTER_PASS_3		GEAR_OPTIMIZER_PASS_3
#define GEAR_DUMP_AFTER_PASS_4		GEAR_OPTIMIZER_PASS_4
#define GEAR_DUMP_AFTER_PASS_5		GEAR_OPTIMIZER_PASS_5
#define GEAR_DUMP_AFTER_PASS_6		GEAR_OPTIMIZER_PASS_6
#define GEAR_DUMP_AFTER_PASS_7		GEAR_OPTIMIZER_PASS_7
#define GEAR_DUMP_AFTER_PASS_8		GEAR_OPTIMIZER_PASS_8
#define GEAR_DUMP_AFTER_PASS_9		GEAR_OPTIMIZER_PASS_9
#define GEAR_DUMP_AFTER_PASS_10		GEAR_OPTIMIZER_PASS_10
#define GEAR_DUMP_AFTER_PASS_11		GEAR_OPTIMIZER_PASS_11
#define GEAR_DUMP_AFTER_PASS_12		GEAR_OPTIMIZER_PASS_12
#define GEAR_DUMP_AFTER_PASS_13		GEAR_OPTIMIZER_PASS_13
#define GEAR_DUMP_AFTER_PASS_14		GEAR_OPTIMIZER_PASS_14

#define GEAR_DUMP_BEFORE_OPTIMIZER  (1<<16)
#define GEAR_DUMP_AFTER_OPTIMIZER   (1<<17)

#define GEAR_DUMP_BEFORE_BLOCK_PASS (1<<18)
#define GEAR_DUMP_AFTER_BLOCK_PASS  (1<<19)
#define GEAR_DUMP_BLOCK_PASS_VARS   (1<<20)

#define GEAR_DUMP_BEFORE_DFA_PASS   (1<<21)
#define GEAR_DUMP_AFTER_DFA_PASS    (1<<22)
#define GEAR_DUMP_DFA_CFG           (1<<23)
#define GEAR_DUMP_DFA_DOMINATORS    (1<<24)
#define GEAR_DUMP_DFA_LIVENESS      (1<<25)
#define GEAR_DUMP_DFA_PHI           (1<<26)
#define GEAR_DUMP_DFA_SSA           (1<<27)
#define GEAR_DUMP_DFA_SSA_VARS      (1<<28)
#define GEAR_DUMP_SCCP              (1<<29)

typedef struct _gear_script {
	gear_string   *filename;
	gear_op_array  main_op_array;
	HashTable      function_table;
	HashTable      class_table;
	uint32_t       first_early_binding_opline; /* the linked list of delayed declarations */
} gear_script;

int gear_optimize_script(gear_script *script, gear_long optimization_level, gear_long debug_level);
int gear_optimizer_startup(void);
int gear_optimizer_shutdown(void);

#endif
