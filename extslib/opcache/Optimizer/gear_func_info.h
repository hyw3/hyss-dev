/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_FUNC_INFO_H
#define GEAR_FUNC_INFO_H

#include "gear_ssa.h"

/* func flags */
#define GEAR_FUNC_INDIRECT_VAR_ACCESS      (1<<0)  /* accesses variables by name  */
#define GEAR_FUNC_HAS_CALLS                (1<<1)
#define GEAR_FUNC_VARARG                   (1<<2)  /* uses func_get_args()        */
#define GEAR_FUNC_NO_LOOPS                 (1<<3)
#define GEAR_FUNC_IRREDUCIBLE              (1<<4)
#define GEAR_FUNC_RECURSIVE                (1<<7)
#define GEAR_FUNC_RECURSIVE_DIRECTLY       (1<<8)
#define GEAR_FUNC_RECURSIVE_INDIRECTLY     (1<<9)
#define GEAR_FUNC_HAS_EXTENDED_INFO        (1<<10)

/* The following flags are valid only for return values of internal functions
 * returned by gear_get_func_info()
 */
#define FUNC_MAY_WARN                      (1<<30)

typedef struct _gear_func_info gear_func_info;
typedef struct _gear_call_info gear_call_info;

#define GEAR_FUNC_INFO(op_array) \
	((gear_func_info*)((op_array)->reserved[gear_func_info_rid]))

#define GEAR_SET_FUNC_INFO(op_array, info) do { \
		gear_func_info** pinfo = (gear_func_info**)&(op_array)->reserved[gear_func_info_rid]; \
		*pinfo = info; \
	} while (0)

BEGIN_EXTERN_C()

extern int gear_func_info_rid;

uint32_t gear_get_func_info(const gear_call_info *call_info, const gear_ssa *ssa);

int gear_func_info_startup(void);
int gear_func_info_shutdown(void);

END_EXTERN_C()

#endif /* GEAR_FUNC_INFO_H */

