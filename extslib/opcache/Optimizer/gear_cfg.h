/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_CFG_H
#define GEAR_CFG_H

/* gear_basic_bloc.flags */
#define GEAR_BB_START            (1<<0)  /* fist block             */
#define GEAR_BB_FOLLOW           (1<<1)  /* follows the next block */
#define GEAR_BB_TARGET           (1<<2)  /* jump taget             */
#define GEAR_BB_EXIT             (1<<3)  /* without successors     */
#define GEAR_BB_ENTRY            (1<<4)  /* stackless entry        */
#define GEAR_BB_TRY              (1<<5)  /* start of try block     */
#define GEAR_BB_CATCH            (1<<6)  /* start of catch block   */
#define GEAR_BB_FINALLY          (1<<7)  /* start of finally block */
#define GEAR_BB_FINALLY_END      (1<<8)  /* end of finally block   */
#define GEAR_BB_GEN_VAR          (1<<9)  /* start of live range    */
#define GEAR_BB_KILL_VAR         (1<<10) /* end of live range      */
#define GEAR_BB_UNREACHABLE_FREE (1<<11) /* unreachable loop free  */
#define GEAR_BB_RECV_ENTRY       (1<<12) /* RECV entry             */

#define GEAR_BB_LOOP_HEADER      (1<<16)
#define GEAR_BB_IRREDUCIBLE_LOOP (1<<17)

#define GEAR_BB_REACHABLE        (1<<31)

#define GEAR_BB_PROTECTED        (GEAR_BB_ENTRY|GEAR_BB_RECV_ENTRY|GEAR_BB_TRY|GEAR_BB_CATCH|GEAR_BB_FINALLY|GEAR_BB_FINALLY_END|GEAR_BB_GEN_VAR|GEAR_BB_KILL_VAR)

typedef struct _gear_basic_block {
	int              *successors;         /* successor block indices     */
	uint32_t          flags;
	uint32_t          start;              /* first opcode number         */
	uint32_t          len;                /* number of opcodes           */
	int               successors_count;   /* number of successors        */
	int               predecessors_count; /* number of predecessors      */
	int               predecessor_offset; /* offset of 1-st predecessor  */
	int               idom;               /* immediate dominator block   */
	int               loop_header;        /* closest loop header, or -1  */
	int               level;              /* steps away from the entry in the dom. tree */
	int               children;           /* list of dominated blocks    */
	int               next_child;         /* next dominated block        */
	int               successors_storage[2]; /* up to 2 successor blocks */
} gear_basic_block;

/*
+------------+---+---+---+---+---+
|            |OP1|OP2|EXT| 0 | 1 |
+------------+---+---+---+---+---+
|JMP         |ADR|   |   |OP1| - |
|JMPZ        |   |ADR|   |OP2|FOL|
|JMPNZ       |   |ADR|   |OP2|FOL|
|JMPZNZ      |   |ADR|ADR|OP2|EXT|
|JMPZ_EX     |   |ADR|   |OP2|FOL|
|JMPNZ_EX    |   |ADR|   |OP2|FOL|
|JMP_SET     |   |ADR|   |OP2|FOL|
|COALESCE    |   |ADR|   |OP2|FOL|
|ASSERT_CHK  |   |ADR|   |OP2|FOL|
|NEW         |   |ADR|   |OP2|FOL|
|DCL_ANON*   |ADR|   |   |OP1|FOL|
|FE_RESET_*  |   |ADR|   |OP2|FOL|
|FE_FETCH_*  |   |   |ADR|EXT|FOL|
|CATCH       |   |   |ADR|EXT|FOL|
|FAST_CALL   |ADR|   |   |OP1|FOL|
|FAST_RET    |   |   |   | - | - |
|RETURN*     |   |   |   | - | - |
|EXIT        |   |   |   | - | - |
|THROW       |   |   |   | - | - |
|*           |   |   |   |FOL| - |
+------------+---+---+---+---+---+
*/

typedef struct _gear_cfg {
	int               blocks_count;       /* number of basic blocks      */
	int               edges_count;        /* number of edges             */
	gear_basic_block *blocks;             /* array of basic blocks       */
	int              *predecessors;
	uint32_t         *map;
	uint32_t          flags;
} gear_cfg;

/* Build Flags */
#define GEAR_RT_CONSTANTS              (1<<31)
#define GEAR_CFG_STACKLESS             (1<<30)
#define GEAR_SSA_DEBUG_LIVENESS        (1<<29)
#define GEAR_SSA_DEBUG_PHI_PLACEMENT   (1<<28)
#define GEAR_SSA_RC_INFERENCE          (1<<27)
#define GEAR_CFG_SPLIT_AT_LIVE_RANGES  (1<<26)
#define GEAR_CFG_NO_ENTRY_PREDECESSORS (1<<25)
#define GEAR_CFG_RECV_ENTRY            (1<<24)
#define GEAR_CALL_TREE                 (1<<23)
#define GEAR_SSA_USE_CV_RESULTS        (1<<22)

#define CRT_CONSTANT_EX(op_array, opline, node, rt_constants) \
	((rt_constants) ? \
		RT_CONSTANT(opline, (node)) \
	: \
		CT_CONSTANT_EX(op_array, (node).constant) \
	)

#define CRT_CONSTANT(node) \
	CRT_CONSTANT_EX(op_array, opline, node, (build_flags & GEAR_RT_CONSTANTS))

#define RETURN_VALUE_USED(opline) \
	((opline)->result_type != IS_UNUSED)

BEGIN_EXTERN_C()

int  gear_build_cfg(gear_arena **arena, const gear_op_array *op_array, uint32_t build_flags, gear_cfg *cfg);
void gear_cfg_remark_reachable_blocks(const gear_op_array *op_array, gear_cfg *cfg);
int  gear_cfg_build_predecessors(gear_arena **arena, gear_cfg *cfg);
int  gear_cfg_compute_dominators_tree(const gear_op_array *op_array, gear_cfg *cfg);
int  gear_cfg_identify_loops(const gear_op_array *op_array, gear_cfg *cfg);

END_EXTERN_C()

#endif /* GEAR_CFG_H */

