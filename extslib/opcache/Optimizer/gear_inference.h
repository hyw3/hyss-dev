/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_INFERENCE_H
#define GEAR_INFERENCE_H

#include "gear_optimizer.h"
#include "gear_ssa.h"
#include "gear_bitset.h"

/* Bitmask for type inference (gear_ssa_var_info.type) */
#include "gear_type_info.h"

#define MAY_BE_IN_REG               (1<<25) /* value allocated in CPU register */

//TODO: remome MAY_BE_RC1, MAY_BE_RCN???
#define MAY_BE_RC1                  (1<<27) /* may be non-reference with refcount == 1 */
#define MAY_BE_RCN                  (1<<28) /* may be non-reference with refcount > 1  */

#define MAY_HAVE_DTOR \
	(MAY_BE_OBJECT|MAY_BE_RESOURCE \
	|MAY_BE_ARRAY_OF_ARRAY|MAY_BE_ARRAY_OF_OBJECT|MAY_BE_ARRAY_OF_RESOURCE)

#define DEFINE_SSA_OP_HAS_RANGE(opN) \
	static gear_always_inline gear_bool _ssa_##opN##_has_range(const gear_op_array *op_array, const gear_ssa *ssa, const gear_op *opline) \
	{ \
		if (opline->opN##_type == IS_CONST) { \
			zval *zv = CRT_CONSTANT_EX(op_array, opline, opline->opN, ssa->rt_constants); \
			return (Z_TYPE_P(zv) == IS_LONG || Z_TYPE_P(zv) == IS_TRUE || Z_TYPE_P(zv) == IS_FALSE || Z_TYPE_P(zv) == IS_NULL); \
		} else { \
			return (opline->opN##_type != IS_UNUSED && \
		        ssa->ops && \
		        ssa->var_info && \
		        ssa->ops[opline - op_array->opcodes].opN##_use >= 0 && \
			    ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].has_range); \
		} \
		return 0; \
	}

#define DEFINE_SSA_OP_MIN_RANGE(opN) \
	static gear_always_inline gear_long _ssa_##opN##_min_range(const gear_op_array *op_array, const gear_ssa *ssa, const gear_op *opline) \
	{ \
		if (opline->opN##_type == IS_CONST) { \
			zval *zv = CRT_CONSTANT_EX(op_array, opline, opline->opN, ssa->rt_constants); \
			if (Z_TYPE_P(zv) == IS_LONG) { \
				return Z_LVAL_P(zv); \
			} else if (Z_TYPE_P(zv) == IS_TRUE) { \
				return 1; \
			} else if (Z_TYPE_P(zv) == IS_FALSE) { \
				return 0; \
			} else if (Z_TYPE_P(zv) == IS_NULL) { \
				return 0; \
			} \
		} else if (opline->opN##_type != IS_UNUSED && \
		    ssa->ops && \
		    ssa->var_info && \
		    ssa->ops[opline - op_array->opcodes].opN##_use >= 0 && \
		    ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].has_range) { \
			return ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].range.min; \
		} \
		return GEAR_LONG_MIN; \
	}

#define DEFINE_SSA_OP_MAX_RANGE(opN) \
	static gear_always_inline gear_long _ssa_##opN##_max_range(const gear_op_array *op_array, const gear_ssa *ssa, const gear_op *opline) \
	{ \
		if (opline->opN##_type == IS_CONST) { \
			zval *zv = CRT_CONSTANT_EX(op_array, opline, opline->opN, ssa->rt_constants); \
			if (Z_TYPE_P(zv) == IS_LONG) { \
				return Z_LVAL_P(zv); \
			} else if (Z_TYPE_P(zv) == IS_TRUE) { \
				return 1; \
			} else if (Z_TYPE_P(zv) == IS_FALSE) { \
				return 0; \
			} else if (Z_TYPE_P(zv) == IS_NULL) { \
				return 0; \
			} \
		} else if (opline->opN##_type != IS_UNUSED && \
		    ssa->ops && \
		    ssa->var_info && \
		    ssa->ops[opline - op_array->opcodes].opN##_use >= 0 && \
		    ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].has_range) { \
			return ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].range.max; \
		} \
		return GEAR_LONG_MAX; \
	}

#define DEFINE_SSA_OP_RANGE_UNDERFLOW(opN) \
	static gear_always_inline char _ssa_##opN##_range_underflow(const gear_op_array *op_array, const gear_ssa *ssa, const gear_op *opline) \
	{ \
		if (opline->opN##_type == IS_CONST) { \
			zval *zv = CRT_CONSTANT_EX(op_array, opline, opline->opN, ssa->rt_constants); \
			if (Z_TYPE_P(zv) == IS_LONG || Z_TYPE_P(zv) == IS_TRUE || Z_TYPE_P(zv) == IS_FALSE || Z_TYPE_P(zv) == IS_NULL) { \
				return 0; \
			} \
		} else if (opline->opN##_type != IS_UNUSED && \
		    ssa->ops && \
		    ssa->var_info && \
		    ssa->ops[opline - op_array->opcodes].opN##_use >= 0 && \
		    ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].has_range) { \
			return ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].range.underflow; \
		} \
		return 1; \
	}

#define DEFINE_SSA_OP_RANGE_OVERFLOW(opN) \
	static gear_always_inline char _ssa_##opN##_range_overflow(const gear_op_array *op_array, const gear_ssa *ssa, const gear_op *opline) \
	{ \
		if (opline->opN##_type == IS_CONST) { \
			zval *zv = CRT_CONSTANT_EX(op_array, opline, opline->opN, ssa->rt_constants); \
			if (Z_TYPE_P(zv) == IS_LONG || Z_TYPE_P(zv) == IS_TRUE || Z_TYPE_P(zv) == IS_FALSE || Z_TYPE_P(zv) == IS_NULL) { \
				return 0; \
			} \
		} else if (opline->opN##_type != IS_UNUSED && \
		    ssa->ops && \
		    ssa->var_info && \
		    ssa->ops[opline - op_array->opcodes].opN##_use >= 0 && \
		    ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].has_range) { \
			return ssa->var_info[ssa->ops[opline - op_array->opcodes].opN##_use].range.overflow; \
		} \
		return 1; \
	}

DEFINE_SSA_OP_HAS_RANGE(op1)
DEFINE_SSA_OP_MIN_RANGE(op1)
DEFINE_SSA_OP_MAX_RANGE(op1)
DEFINE_SSA_OP_RANGE_UNDERFLOW(op1)
DEFINE_SSA_OP_RANGE_OVERFLOW(op1)
DEFINE_SSA_OP_HAS_RANGE(op2)
DEFINE_SSA_OP_MIN_RANGE(op2)
DEFINE_SSA_OP_MAX_RANGE(op2)
DEFINE_SSA_OP_RANGE_UNDERFLOW(op2)
DEFINE_SSA_OP_RANGE_OVERFLOW(op2)

#define OP1_HAS_RANGE()         (_ssa_op1_has_range (op_array, ssa, opline))
#define OP1_MIN_RANGE()         (_ssa_op1_min_range (op_array, ssa, opline))
#define OP1_MAX_RANGE()         (_ssa_op1_max_range (op_array, ssa, opline))
#define OP1_RANGE_UNDERFLOW()   (_ssa_op1_range_underflow (op_array, ssa, opline))
#define OP1_RANGE_OVERFLOW()    (_ssa_op1_range_overflow (op_array, ssa, opline))
#define OP2_HAS_RANGE()         (_ssa_op2_has_range (op_array, ssa, opline))
#define OP2_MIN_RANGE()         (_ssa_op2_min_range (op_array, ssa, opline))
#define OP2_MAX_RANGE()         (_ssa_op2_max_range (op_array, ssa, opline))
#define OP2_RANGE_UNDERFLOW()   (_ssa_op2_range_underflow (op_array, ssa, opline))
#define OP2_RANGE_OVERFLOW()    (_ssa_op2_range_overflow (op_array, ssa, opline))

static gear_always_inline uint32_t _const_op_type(const zval *zv) {
	if (Z_TYPE_P(zv) == IS_CONSTANT_AST) {
		return MAY_BE_RC1 | MAY_BE_RCN | MAY_BE_ANY | MAY_BE_ARRAY_KEY_ANY | MAY_BE_ARRAY_OF_ANY;
	} else if (Z_TYPE_P(zv) == IS_ARRAY) {
		HashTable *ht = Z_ARRVAL_P(zv);
		uint32_t tmp = MAY_BE_ARRAY;
		gear_string *str;
		zval *val;

		if (Z_REFCOUNTED_P(zv)) {
			tmp |= MAY_BE_RC1 | MAY_BE_RCN;
		} else {
			tmp |= MAY_BE_RCN;
		}

		GEAR_HASH_FOREACH_STR_KEY_VAL(ht, str, val) {
			if (str) {
				tmp |= MAY_BE_ARRAY_KEY_STRING;
			} else {
				tmp |= MAY_BE_ARRAY_KEY_LONG;
			}
			tmp |= 1 << (Z_TYPE_P(val) + MAY_BE_ARRAY_SHIFT);
		} GEAR_HASH_FOREACH_END();
		return tmp;
	} else {
		uint32_t tmp = (1 << Z_TYPE_P(zv));

		if (Z_REFCOUNTED_P(zv)) {
			tmp |= MAY_BE_RC1 | MAY_BE_RCN;
		} else if (Z_TYPE_P(zv) == IS_STRING) {
			tmp |= MAY_BE_RCN;
		}
		return tmp;
	}
}

static gear_always_inline uint32_t get_ssa_var_info(const gear_ssa *ssa, int ssa_var_num)
{
	if (ssa->var_info && ssa_var_num >= 0) {
		return ssa->var_info[ssa_var_num].type;
	} else {
		return MAY_BE_UNDEF | MAY_BE_RC1 | MAY_BE_RCN | MAY_BE_REF | MAY_BE_ANY | MAY_BE_ARRAY_KEY_ANY | MAY_BE_ARRAY_OF_ANY | MAY_BE_ARRAY_OF_REF | MAY_BE_ERROR;
	}
}

#define DEFINE_SSA_OP_INFO(opN) \
	static gear_always_inline uint32_t _ssa_##opN##_info(const gear_op_array *op_array, const gear_ssa *ssa, const gear_op *opline) \
	{																		\
		if (opline->opN##_type == IS_CONST) {							\
			return _const_op_type(CRT_CONSTANT_EX(op_array, opline, opline->opN, ssa->rt_constants)); \
		} else { \
			return get_ssa_var_info(ssa, ssa->ops ? ssa->ops[opline - op_array->opcodes].opN##_use : -1); \
		} \
	}

#define DEFINE_SSA_OP_DEF_INFO(opN) \
	static gear_always_inline uint32_t _ssa_##opN##_def_info(const gear_op_array *op_array, const gear_ssa *ssa, const gear_op *opline) \
	{ \
		return get_ssa_var_info(ssa, ssa->ops ? ssa->ops[opline - op_array->opcodes].opN##_def : -1); \
	}


DEFINE_SSA_OP_INFO(op1)
DEFINE_SSA_OP_INFO(op2)
DEFINE_SSA_OP_INFO(result)
DEFINE_SSA_OP_DEF_INFO(op1)
DEFINE_SSA_OP_DEF_INFO(op2)
DEFINE_SSA_OP_DEF_INFO(result)

#define OP1_INFO()              (_ssa_op1_info(op_array, ssa, opline))
#define OP2_INFO()              (_ssa_op2_info(op_array, ssa, opline))
#define OP1_DATA_INFO()         (_ssa_op1_info(op_array, ssa, (opline+1)))
#define OP2_DATA_INFO()         (_ssa_op2_info(op_array, ssa, (opline+1)))
#define RES_USE_INFO()          (_ssa_result_info(op_array, ssa, opline))
#define OP1_DEF_INFO()          (_ssa_op1_def_info(op_array, ssa, opline))
#define OP2_DEF_INFO()          (_ssa_op2_def_info(op_array, ssa, opline))
#define OP1_DATA_DEF_INFO()     (_ssa_op1_def_info(op_array, ssa, (opline+1)))
#define OP2_DATA_DEF_INFO()     (_ssa_op2_def_info(op_array, ssa, (opline+1)))
#define RES_INFO()              (_ssa_result_def_info(op_array, ssa, opline))


BEGIN_EXTERN_C()

int gear_ssa_find_false_dependencies(const gear_op_array *op_array, gear_ssa *ssa);
int gear_ssa_find_sccs(const gear_op_array *op_array, gear_ssa *ssa);
int gear_ssa_inference(gear_arena **raena, const gear_op_array *op_array, const gear_script *script, gear_ssa *ssa, gear_long optimization_level);

uint32_t gear_array_element_type(uint32_t t1, int write, int insert);

int  gear_inference_calc_range(const gear_op_array *op_array, gear_ssa *ssa, int var, int widening, int narrowing, gear_ssa_range *tmp);
void gear_inference_init_range(const gear_op_array *op_array, gear_ssa *ssa, int var, gear_bool underflow, gear_long min, gear_long max, gear_bool overflow);
int  gear_inference_narrowing_meet(gear_ssa_var_info *var_info, gear_ssa_range *r);
int  gear_inference_widening_meet(gear_ssa_var_info *var_info, gear_ssa_range *r);
void gear_inference_check_recursive_dependencies(gear_op_array *op_array);

int  gear_infer_types_ex(const gear_op_array *op_array, const gear_script *script, gear_ssa *ssa, gear_bitset worklist, gear_long optimization_level);

void gear_init_func_return_info(const gear_op_array   *op_array,
                                const gear_script     *script,
                                gear_ssa_var_info     *ret);
void gear_func_return_info(const gear_op_array   *op_array,
                           const gear_script     *script,
                           int                    recursive,
                           int                    widening,
                           gear_ssa_var_info     *ret);

int gear_may_throw(const gear_op *opline, gear_op_array *op_array, gear_ssa *ssa);

END_EXTERN_C()

#endif /* GEAR_INFERENCE_H */

