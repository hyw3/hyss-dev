/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_CALL_GRAPH_H
#define GEAR_CALL_GRAPH_H

#include "gear_ssa.h"
#include "gear_func_info.h"
#include "gear_optimizer.h"

typedef struct _gear_send_arg_info {
	gear_op                *opline;
} gear_send_arg_info;

typedef struct _gear_recv_arg_info {
	int                     ssa_var;
	gear_ssa_var_info       info;
} gear_recv_arg_info;

struct _gear_call_info {
	gear_op_array          *caller_op_array;
	gear_op                *caller_init_opline;
	gear_op                *caller_call_opline;
	gear_function          *callee_func;
	gear_call_info         *next_caller;
	gear_call_info         *next_callee;
	gear_func_info         *clone;
	int                     recursive;
	int                     num_args;
	gear_send_arg_info      arg_info[1];
};

struct _gear_func_info {
	int                     num;
	uint32_t                flags;
	gear_ssa                ssa;          /* Static Single Assignmnt Form  */
	gear_call_info         *caller_info;  /* where this function is called from */
	gear_call_info         *callee_info;  /* which functions are called from this one */
	gear_call_info        **call_map;     /* Call info associated with init/call/send opnum */
	int                     num_args;     /* (-1 - unknown) */
	gear_recv_arg_info     *arg_info;
	gear_ssa_var_info       return_info;
	gear_func_info         *clone;
	int                     clone_num;
	int                     return_value_used; /* -1 unknown, 0 no, 1 yes */
	void                   *codegen_data;
};

typedef struct _gear_call_graph {
	int                     op_arrays_count;
	gear_op_array         **op_arrays;
	gear_func_info         *func_infos;
} gear_call_graph;

BEGIN_EXTERN_C()

int gear_build_call_graph(gear_arena **arena, gear_script *script, uint32_t build_flags, gear_call_graph *call_graph);
gear_call_info **gear_build_call_map(gear_arena **arena, gear_func_info *info, gear_op_array *op_array);
int gear_analyze_calls(gear_arena **arena, gear_script *script, uint32_t build_flags, gear_op_array *op_array, gear_func_info *func_info);

END_EXTERN_C()

#endif /* GEAR_CALL_GRAPH_H */

