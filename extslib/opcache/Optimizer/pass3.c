/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* pass 3:
 * - optimize $i = $i+expr to $i+=expr
 * - optimize series of JMPs
 * - change $i++ to ++$i where possible
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_API.h"
#include "gear_constants.h"
#include "gear_execute.h"
#include "gear_vm.h"

/* we use "jmp_hitlist" to avoid infinity loops during jmp optimization */
#define CHECK_JMP(target, label) 			\
	for (i=0; i<jmp_hitlist_count; i++) {	\
		if (jmp_hitlist[i] == GEAR_OP1_JMP_ADDR(target)) {		\
			goto label;						\
		}									\
	}										\
	jmp_hitlist[jmp_hitlist_count++] = GEAR_OP1_JMP_ADDR(target);

#define CHECK_JMP2(target, label) 			\
	for (i=0; i<jmp_hitlist_count; i++) {	\
		if (jmp_hitlist[i] == GEAR_OP2_JMP_ADDR(target)) {		\
			goto label;						\
		}									\
	}										\
	jmp_hitlist[jmp_hitlist_count++] = GEAR_OP2_JMP_ADDR(target);

void gear_optimizer_pass3(gear_op_array *op_array, gear_optimizer_ctx *ctx)
{
	gear_op *opline;
	gear_op *end = op_array->opcodes + op_array->last;
	gear_op **jmp_hitlist;
	int jmp_hitlist_count;
	int i;
	uint32_t opline_num = 0;
	ALLOCA_FLAG(use_heap);

	jmp_hitlist = (gear_op**)do_alloca(sizeof(gear_op*)*op_array->last, use_heap);
	opline = op_array->opcodes;

	while (opline < end) {
		jmp_hitlist_count = 0;

		switch (opline->opcode) {
			case GEAR_ADD:
			case GEAR_SUB:
			case GEAR_MUL:
			case GEAR_DIV:
			case GEAR_MOD:
			case GEAR_POW:
			case GEAR_CONCAT:
			case GEAR_SL:
			case GEAR_SR:
			case GEAR_BW_OR:
			case GEAR_BW_AND:
			case GEAR_BW_XOR:
				{
					gear_op *next_opline = opline + 1;

					while (next_opline < end && next_opline->opcode == GEAR_NOP) {
						++next_opline;
					}

					if (next_opline >= end || next_opline->opcode != GEAR_ASSIGN) {
						break;
					}

					/* change $i=expr+$i to $i=$i+expr so that the following optimization
					 * works on it. Only do this if we are ignoring operator overloading,
					 * as operand order might be significant otherwise. */
					if ((ctx->optimization_level & GEAR_OPTIMIZER_IGNORE_OVERLOADING)
						&& (opline->op2_type & (IS_VAR | IS_CV))
						&& opline->op2.var == next_opline->op1.var &&
						(opline->opcode == GEAR_ADD ||
						 opline->opcode == GEAR_MUL ||
						 opline->opcode == GEAR_BW_OR ||
						 opline->opcode == GEAR_BW_AND ||
						 opline->opcode == GEAR_BW_XOR)) {
						gear_uchar tmp_type = opline->op1_type;
						znode_op tmp = opline->op1;

						if (opline->opcode != GEAR_ADD
								|| (opline->op1_type == IS_CONST
									&& Z_TYPE(GEAR_OP1_LITERAL(opline)) != IS_ARRAY)) {
							/* protection from array add: $a = array + $a is not commutative! */
							COPY_NODE(opline->op1, opline->op2);
							COPY_NODE(opline->op2, tmp);
						}
					}

					if ((opline->op1_type & (IS_VAR | IS_CV))
						&& opline->op1.var == next_opline->op1.var
						&& opline->op1_type == next_opline->op1_type) {
						switch (opline->opcode) {
							case GEAR_ADD:
								opline->opcode = GEAR_ASSIGN_ADD;
								break;
							case GEAR_SUB:
								opline->opcode = GEAR_ASSIGN_SUB;
								break;
							case GEAR_MUL:
								opline->opcode = GEAR_ASSIGN_MUL;
								break;
							case GEAR_DIV:
								opline->opcode = GEAR_ASSIGN_DIV;
								break;
							case GEAR_MOD:
								opline->opcode = GEAR_ASSIGN_MOD;
								break;
							case GEAR_POW:
								opline->opcode = GEAR_ASSIGN_POW;
								break;
							case GEAR_CONCAT:
								opline->opcode = GEAR_ASSIGN_CONCAT;
								break;
							case GEAR_SL:
								opline->opcode = GEAR_ASSIGN_SL;
								break;
							case GEAR_SR:
								opline->opcode = GEAR_ASSIGN_SR;
								break;
							case GEAR_BW_OR:
								opline->opcode = GEAR_ASSIGN_BW_OR;
								break;
							case GEAR_BW_AND:
								opline->opcode = GEAR_ASSIGN_BW_AND;
								break;
							case GEAR_BW_XOR:
								opline->opcode = GEAR_ASSIGN_BW_XOR;
								break;
						}
						COPY_NODE(opline->result, next_opline->result);
						MAKE_NOP(next_opline);
						opline++;
						opline_num++;
					}
				}
				break;

			case GEAR_JMP:
				if (op_array->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK) {
					break;
				}

				/* convert L: JMP L+1 to NOP */
				if (GEAR_OP1_JMP_ADDR(opline) == opline + 1) {
					MAKE_NOP(opline);
					goto done_jmp_optimization;
				}

				/* convert JMP L1 ... L1: JMP L2 to JMP L2 .. L1: JMP L2 */
				while (GEAR_OP1_JMP_ADDR(opline) < end
						&& GEAR_OP1_JMP_ADDR(opline)->opcode == GEAR_JMP) {
					gear_op *target = GEAR_OP1_JMP_ADDR(opline);
					CHECK_JMP(target, done_jmp_optimization);
					GEAR_SET_OP_JMP_ADDR(opline, opline->op1, GEAR_OP1_JMP_ADDR(target));
				}
				break;

			case GEAR_JMP_SET:
			case GEAR_COALESCE:
				if (op_array->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK) {
					break;
				}

				while (GEAR_OP2_JMP_ADDR(opline) < end) {
					gear_op *target = GEAR_OP2_JMP_ADDR(opline);
					if (target->opcode == GEAR_JMP) {
						GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP1_JMP_ADDR(target));
					} else {
						break;
					}
				}
				break;
			case GEAR_JMPZ:
			case GEAR_JMPNZ:
				if (op_array->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK) {
					break;
				}

				while (GEAR_OP2_JMP_ADDR(opline) < end) {
					gear_op *target = GEAR_OP2_JMP_ADDR(opline);

					if (target->opcode == GEAR_JMP) {
						/* plain JMP */
						/* JMPZ(X,L1), L1: JMP(L2) => JMPZ(X,L2), L1: JMP(L2) */
						CHECK_JMP(target, done_jmp_optimization);
						GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP1_JMP_ADDR(target));
					} else if (target->opcode == opline->opcode &&
					           SAME_VAR(opline->op1, target->op1)) {
						/* same opcode and same var as this opcode */
						/* JMPZ(X,L1), L1: JMPZ(X,L2) => JMPZ(X,L2), L1: JMPZ(X,L2) */
						CHECK_JMP2(target, done_jmp_optimization);
						GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP2_JMP_ADDR(target));
					} else if (target->opcode == opline->opcode + 3 &&
					           SAME_VAR(opline->op1, target->op1)) {
						/* convert JMPZ(X,L1), L1: T JMPZ_EX(X,L2) to
						   T = JMPZ_EX(X, L2) */
						GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP2_JMP_ADDR(target));
						opline->opcode += 3;
						COPY_NODE(opline->result, target->result);
						break;
					} else if (target->opcode == INV_COND(opline->opcode) &&
					           SAME_VAR(opline->op1, target->op1)) {
						/* convert JMPZ(X,L1), L1: JMPNZ(X,L2) to
						   JMPZ(X,L1+1) */
						GEAR_SET_OP_JMP_ADDR(opline, opline->op2, target + 1);
						break;
					} else if (target->opcode == INV_COND_EX(opline->opcode) &&
					           SAME_VAR(opline->op1, target->op1)) {
						/* convert JMPZ(X,L1), L1: T = JMPNZ_EX(X,L2) to
						   T = JMPZ_EX(X,L1+1) */
						GEAR_SET_OP_JMP_ADDR(opline, opline->op2, target + 1);
						opline->opcode += 3;
						COPY_NODE(opline->result, target->result);
						break;
					} else {
						break;
					}
				}
				break;

			case GEAR_JMPZ_EX:
			case GEAR_JMPNZ_EX: {
					gear_uchar T_type = opline->result_type;
					znode_op T = opline->result;

					if (op_array->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK) {
						break;
					}

					/* convert L: T = JMPZ_EX X,L+1 to T = BOOL(X) */
					/* convert L: T = JMPZ_EX T,L+1 to NOP */
					if (GEAR_OP2_JMP_ADDR(opline) == opline + 1) {
						if (opline->op1.var == opline->result.var) {
							MAKE_NOP(opline);
						} else {
							opline->opcode = GEAR_BOOL;
							SET_UNUSED(opline->op2);
						}
						goto done_jmp_optimization;
					}

					while (GEAR_OP2_JMP_ADDR(opline) < end) {
						gear_op *target = GEAR_OP2_JMP_ADDR(opline);

						if (target->opcode == opline->opcode-3 &&
							SAME_VAR(target->op1, T)) {
						   /* convert T=JMPZ_EX(X,L1), L1: JMPZ(T,L2) to
							  JMPZ_EX(X,L2) */
							CHECK_JMP2(target, continue_jmp_ex_optimization);
							GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP2_JMP_ADDR(target));
						} else if (target->opcode == opline->opcode &&
							SAME_VAR(target->op1, T) &&
							SAME_VAR(target->result, T)) {
						   /* convert T=JMPZ_EX(X,L1), L1: T=JMPZ_EX(T,L2) to
							  JMPZ_EX(X,L2) */
							CHECK_JMP2(target, continue_jmp_ex_optimization);
							GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP2_JMP_ADDR(target));
						} else if (target->opcode == GEAR_JMPZNZ &&
								  SAME_VAR(target->op1, T)) {
							/* Check for JMPZNZ with same cond variable */
							gear_op *new_target;

							CHECK_JMP2(target, continue_jmp_ex_optimization);
							if (opline->opcode == GEAR_JMPZ_EX) {
								new_target = GEAR_OP2_JMP_ADDR(target);
							} else {
								/* JMPNZ_EX */
								new_target = GEAR_OFFSET_TO_OPLINE(target, target->extended_value);
							}
							GEAR_SET_OP_JMP_ADDR(opline, opline->op2, new_target);
						} else if ((target->opcode == INV_EX_COND_EX(opline->opcode) ||
									target->opcode == INV_EX_COND(opline->opcode)) &&
									SAME_VAR(opline->op1, target->op1)) {
						   /* convert JMPZ_EX(X,L1), L1: JMPNZ_EX(X,L2) to
							  JMPZ_EX(X,L1+1) */
							GEAR_SET_OP_JMP_ADDR(opline, opline->op2, target + 1);
							break;
						} else if (target->opcode == INV_EX_COND(opline->opcode) &&
									SAME_VAR(target->op1, T)) {
						   /* convert T=JMPZ_EX(X,L1), L1: JMPNZ(T,L2) to
							  JMPZ_EX(X,L1+1) */
							GEAR_SET_OP_JMP_ADDR(opline, opline->op2, target + 1);
							break;
						} else if (target->opcode == INV_EX_COND_EX(opline->opcode) &&
									SAME_VAR(target->op1, T) &&
									SAME_VAR(target->result, T)) {
						   /* convert T=JMPZ_EX(X,L1), L1: T=JMPNZ_EX(T,L2) to
							  JMPZ_EX(X,L1+1) */
							GEAR_SET_OP_JMP_ADDR(opline, opline->op2, target + 1);
							break;
						} else if (target->opcode == GEAR_BOOL &&
						           SAME_VAR(opline->result, target->op1)) {
							/* convert Y = JMPZ_EX(X,L1), L1: Z = BOOL(Y) to
							   Z = JMPZ_EX(X,L1+1) */
							opline->result.var = target->result.var;
							GEAR_SET_OP_JMP_ADDR(opline, opline->op2, target + 1);
							break;
						} else {
							break;
						}
					} /* while */
continue_jmp_ex_optimization:
					break;
#if 0
					/* If Ti = JMPZ_EX(X, L) and Ti is not used, convert to JMPZ(X, L) */
					{
						gear_op *op;
						for(op = opline+1; op<end; op++) {
							if(op->result_type == IS_TMP_VAR &&
							   op->result.var == opline->result.var) {
								break; /* can pass to part 2 */
							}

							if(op->opcode == GEAR_JMP ||
							   op->opcode == GEAR_JMPZ ||
							   op->opcode == GEAR_JMPZ_EX ||
							   op->opcode == GEAR_JMPNZ ||
							   op->opcode == GEAR_JMPNZ_EX ||
							   op->opcode == GEAR_JMPZNZ ||
							   op->opcode == GEAR_CASE ||
							   op->opcode == GEAR_RETURN ||
							   op->opcode == GEAR_RETURN_BY_REF ||
							   op->opcode == GEAR_FAST_RET ||
							   op->opcode == GEAR_FE_FETCH_R ||
							   op->opcode == GEAR_FE_FETCH_RW ||
							   op->opcode == GEAR_EXIT) {
								break;
							}

							if(op->op1_type == IS_TMP_VAR &&
							   op->op1.var == opline->result.var) {
								goto done_jmp_optimization;
							}

							if(op->op2_type == IS_TMP_VAR &&
							   op->op2.var == opline->result.var) {
								goto done_jmp_optimization;
							}
						} /* for */

						for(op = &op_array->opcodes[opline->op2.opline_num]; op<end; op++) {

							if(op->result_type == IS_TMP_VAR &&
							   op->result.var == opline->result.var) {
								break; /* can pass to optimization */
							}

							if(op->opcode == GEAR_JMP ||
							   op->opcode == GEAR_JMPZ ||
							   op->opcode == GEAR_JMPZ_EX ||
							   op->opcode == GEAR_JMPNZ ||
							   op->opcode == GEAR_JMPNZ_EX ||
							   op->opcode == GEAR_JMPZNZ ||
							   op->opcode == GEAR_CASE ||
							   op->opcode == GEAR_RETURN ||
							   op->opcode == GEAR_RETURN_BY_REF ||
							   op->opcode == GEAR_FAST_RET ||
							   op->opcode == GEAR_FE_FETCH_R ||
							   op->opcode == GEAR_FE_FETCH_RW ||
							   op->opcode == GEAR_EXIT) {
								break;
							}

							if(op->op1_type == IS_TMP_VAR &&
							   op->op1.var == opline->result.var) {
								goto done_jmp_optimization;
							}

							if(op->op2_type == IS_TMP_VAR &&
							   op->op2.var == opline->result.var) {
								goto done_jmp_optimization;
							}
						}

						opline->opcode = opline->opcode-3; /* JMP_EX -> JMP */
						SET_UNUSED(opline->result);
						break;
					}
#endif
				}
				break;

			case GEAR_JMPZNZ:
				if (op_array->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK) {
					break;
				}

				/* JMPZNZ(X,L1,L2), L1: JMP(L3) => JMPZNZ(X,L3,L2), L1: JMP(L3) */
				while (GEAR_OP2_JMP_ADDR(opline) < end
						&& GEAR_OP2_JMP_ADDR(opline)->opcode == GEAR_JMP) {
					gear_op *target = GEAR_OP2_JMP_ADDR(opline);
					CHECK_JMP(target, continue_jmpznz_optimization);
					GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP1_JMP_ADDR(target));
				}
continue_jmpznz_optimization:
				/* JMPZNZ(X,L1,L2), L2: JMP(L3) => JMPZNZ(X,L1,L3), L2: JMP(L3) */
				while (GEAR_OFFSET_TO_OPLINE(opline, opline->extended_value) < end
						&& GEAR_OFFSET_TO_OPLINE(opline, opline->extended_value)->opcode == GEAR_JMP) {
					gear_op *target = GEAR_OFFSET_TO_OPLINE(opline, opline->extended_value);
					CHECK_JMP(target, done_jmp_optimization);
					opline->extended_value = GEAR_OPLINE_TO_OFFSET(opline, GEAR_OP1_JMP_ADDR(target));
				}
				break;

			case GEAR_POST_INC_OBJ:
			case GEAR_POST_DEC_OBJ:
			case GEAR_POST_INC:
			case GEAR_POST_DEC: {
					/* POST_INC, FREE => PRE_INC */
					gear_op *next_op = opline + 1;

					if (next_op >= end) {
						break;
					}
					if (next_op->opcode == GEAR_FREE &&
						next_op->op1.var == opline->result.var) {
						MAKE_NOP(next_op);
						opline->opcode -= 2;
						opline->result_type = IS_UNUSED;
					}
				}
				break;
		}
done_jmp_optimization:
		opline++;
		opline_num++;
	}
	free_alloca(jmp_hitlist, use_heap);
}
