/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_DUMP_H
#define GEAR_DUMP_H

#include "gear_ssa.h"
#include "gear_dfg.h"

#define GEAR_DUMP_HIDE_UNREACHABLE     (1<<0)
#define GEAR_DUMP_RC_INFERENCE         (1<<1)
#define GEAR_DUMP_CFG                  (1<<2)
#define GEAR_DUMP_SSA                  (1<<3)
#define GEAR_DUMP_RT_CONSTANTS         GEAR_RT_CONSTANTS

BEGIN_EXTERN_C()

void gear_dump_op_array(const gear_op_array *op_array, uint32_t dump_flags, const char *msg, const void *data);
void gear_dump_dominators(const gear_op_array *op_array, const gear_cfg *cfg);
void gear_dump_dfg(const gear_op_array *op_array, const gear_cfg *cfg, const gear_dfg *dfg);
void gear_dump_phi_placement(const gear_op_array *op_array, const gear_ssa *ssa);
void gear_dump_variables(const gear_op_array *op_array);
void gear_dump_ssa_variables(const gear_op_array *op_array, const gear_ssa *ssa, uint32_t dump_flags);
void gear_dump_var(const gear_op_array *op_array, gear_uchar var_type, int var_num);
void gear_dump_op_array_name(const gear_op_array *op_array);
void gear_dump_const(const zval *zv);
void gear_dump_ht(HashTable *ht);

END_EXTERN_C()

#endif /* GEAR_DUMP_H */

