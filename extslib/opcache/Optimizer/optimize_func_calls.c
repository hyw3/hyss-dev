/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_API.h"
#include "gear_constants.h"
#include "gear_execute.h"
#include "gear_vm.h"

#define GEAR_OP1_IS_CONST_STRING(opline) \
	(opline->op1_type == IS_CONST && \
	Z_TYPE(op_array->literals[(opline)->op1.constant]) == IS_STRING)
#define GEAR_OP2_IS_CONST_STRING(opline) \
	(opline->op2_type == IS_CONST && \
	Z_TYPE(op_array->literals[(opline)->op2.constant]) == IS_STRING)

typedef struct _optimizer_call_info {
	gear_function *func;
	gear_op       *opline;
	gear_bool      try_inline;
	uint32_t       func_arg_num;
} optimizer_call_info;

static void gear_delete_call_instructions(gear_op *opline)
{
	int call = 0;

	while (1) {
		switch (opline->opcode) {
			case GEAR_INIT_FCALL_BY_NAME:
			case GEAR_INIT_NS_FCALL_BY_NAME:
			case GEAR_INIT_STATIC_METHOD_CALL:
			case GEAR_INIT_METHOD_CALL:
			case GEAR_INIT_FCALL:
				if (call == 0) {
					MAKE_NOP(opline);
					return;
				}
				/* break missing intentionally */
			case GEAR_NEW:
			case GEAR_INIT_DYNAMIC_CALL:
			case GEAR_INIT_USER_CALL:
				call--;
				break;
			case GEAR_DO_FCALL:
			case GEAR_DO_ICALL:
			case GEAR_DO_UCALL:
			case GEAR_DO_FCALL_BY_NAME:
				call++;
				break;
			case GEAR_SEND_VAL:
			case GEAR_SEND_VAR:
				if (call == 0) {
					if (opline->op1_type == IS_CONST) {
						MAKE_NOP(opline);
					} else if (opline->op1_type == IS_CV) {
						opline->opcode = GEAR_CHECK_VAR;
						opline->extended_value = 0;
						opline->result.var = 0;
					} else {
						opline->opcode = GEAR_FREE;
						opline->extended_value = 0;
						opline->result.var = 0;
					}
				}
				break;
		}
		opline--;
	}
}

static void gear_try_inline_call(gear_op_array *op_array, gear_op *fcall, gear_op *opline, gear_function *func)
{
	if (func->type == GEAR_USER_FUNCTION
	 && !(func->op_array.fn_flags & (GEAR_ACC_ABSTRACT|GEAR_ACC_HAS_TYPE_HINTS))
	 && fcall->extended_value >= func->op_array.required_num_args
	 && func->op_array.opcodes[func->op_array.num_args].opcode == GEAR_RETURN) {

		gear_op *ret_opline = func->op_array.opcodes + func->op_array.num_args;

		if (ret_opline->op1_type == IS_CONST) {
			uint32_t i, num_args = func->op_array.num_args;
			num_args += (func->op_array.fn_flags & GEAR_ACC_VARIADIC) != 0;

			if (fcall->opcode == GEAR_INIT_METHOD_CALL && fcall->op1_type == IS_UNUSED) {
				/* TODO: we can't inlne methods, because $this may be used
				 *       not in object context ???
				 */
				return;
			}

			for (i = 0; i < num_args; i++) {
				/* Don't inline functions with by-reference arguments. This would require
				 * correct handling of INDIRECT arguments. */
				if (func->op_array.arg_info[i].pass_by_reference) {
					return;
				}
			}

			if (fcall->extended_value < func->op_array.num_args) {
				/* don't inline functions with named constants in default arguments */
				i = fcall->extended_value;

				do {
					if (Z_TYPE_P(RT_CONSTANT(&func->op_array.opcodes[i], func->op_array.opcodes[i].op2)) == IS_CONSTANT_AST) {
						return;
					}
					i++;
				} while (i < func->op_array.num_args);
			}

			if (RETURN_VALUE_USED(opline)) {
				zval zv;

				ZVAL_COPY(&zv, RT_CONSTANT(ret_opline, ret_opline->op1));
				opline->opcode = GEAR_QM_ASSIGN;
				opline->op1_type = IS_CONST;
				opline->op1.constant = gear_optimizer_add_literal(op_array, &zv);
				SET_UNUSED(opline->op2);
			} else {
				MAKE_NOP(opline);
			}

			gear_delete_call_instructions(opline-1);
		}
	}
}

void gear_optimize_func_calls(gear_op_array *op_array, gear_optimizer_ctx *ctx)
{
	gear_op *opline = op_array->opcodes;
	gear_op *end = opline + op_array->last;
	int call = 0;
	void *checkpoint;
	optimizer_call_info *call_stack;

	if (op_array->last < 2) {
		return;
	}

	checkpoint = gear_arena_checkpoint(ctx->arena);
	call_stack = gear_arena_calloc(&ctx->arena, op_array->last / 2, sizeof(optimizer_call_info));
	while (opline < end) {
		switch (opline->opcode) {
			case GEAR_INIT_FCALL_BY_NAME:
			case GEAR_INIT_NS_FCALL_BY_NAME:
			case GEAR_INIT_STATIC_METHOD_CALL:
			case GEAR_INIT_METHOD_CALL:
			case GEAR_INIT_FCALL:
			case GEAR_NEW:
				call_stack[call].func = gear_optimizer_get_called_func(
					ctx->script, op_array, opline, 0);
				call_stack[call].try_inline = opline->opcode != GEAR_NEW;
				/* break missing intentionally */
			case GEAR_INIT_DYNAMIC_CALL:
			case GEAR_INIT_USER_CALL:
				call_stack[call].opline = opline;
				call_stack[call].func_arg_num = (uint32_t)-1;
				call++;
				break;
			case GEAR_DO_FCALL:
			case GEAR_DO_ICALL:
			case GEAR_DO_UCALL:
			case GEAR_DO_FCALL_BY_NAME:
				call--;
				if (call_stack[call].func && call_stack[call].opline) {
					gear_op *fcall = call_stack[call].opline;

					if (fcall->opcode == GEAR_INIT_FCALL) {
						/* nothing to do */
					} else if (fcall->opcode == GEAR_INIT_FCALL_BY_NAME) {
						fcall->opcode = GEAR_INIT_FCALL;
						fcall->op1.num = gear_vm_calc_used_stack(fcall->extended_value, call_stack[call].func);
						literal_dtor(&GEAR_OP2_LITERAL(fcall));
						fcall->op2.constant = fcall->op2.constant + 1;
						opline->opcode = gear_get_call_op(fcall, call_stack[call].func);
					} else if (fcall->opcode == GEAR_INIT_NS_FCALL_BY_NAME) {
						fcall->opcode = GEAR_INIT_FCALL;
						fcall->op1.num = gear_vm_calc_used_stack(fcall->extended_value, call_stack[call].func);
						literal_dtor(&op_array->literals[fcall->op2.constant]);
						literal_dtor(&op_array->literals[fcall->op2.constant + 2]);
						fcall->op2.constant = fcall->op2.constant + 1;
						opline->opcode = gear_get_call_op(fcall, call_stack[call].func);
					} else if (fcall->opcode == GEAR_INIT_STATIC_METHOD_CALL
							|| fcall->opcode == GEAR_INIT_METHOD_CALL
							|| fcall->opcode == GEAR_NEW) {
						/* We don't have specialized opcodes for this, do nothing */
					} else {
						GEAR_ASSERT(0);
					}

					if ((GEAR_OPTIMIZER_PASS_16 & ctx->optimization_level)
					 && call_stack[call].try_inline) {
						gear_try_inline_call(op_array, fcall, opline, call_stack[call].func);
					}
				}
				call_stack[call].func = NULL;
				call_stack[call].opline = NULL;
				call_stack[call].try_inline = 0;
				call_stack[call].func_arg_num = (uint32_t)-1;
				break;
			case GEAR_FETCH_FUNC_ARG:
			case GEAR_FETCH_STATIC_PROP_FUNC_ARG:
			case GEAR_FETCH_OBJ_FUNC_ARG:
			case GEAR_FETCH_DIM_FUNC_ARG:
				if (call_stack[call - 1].func) {
					GEAR_ASSERT(call_stack[call - 1].func_arg_num != (uint32_t)-1);
					if (ARG_SHOULD_BE_SENT_BY_REF(call_stack[call - 1].func, call_stack[call - 1].func_arg_num)) {
						if (opline->opcode != GEAR_FETCH_STATIC_PROP_FUNC_ARG) {
							opline->opcode -= 9;
						} else {
							opline->opcode = GEAR_FETCH_STATIC_PROP_W;
						}
					} else {
						if (opline->opcode == GEAR_FETCH_DIM_FUNC_ARG
								&& opline->op2_type == IS_UNUSED) {
							/* FETCH_DIM_FUNC_ARG supports UNUSED op2, while FETCH_DIM_R does not.
							 * Performing the replacement would create an invalid opcode. */
							call_stack[call - 1].try_inline = 0;
							break;
						}

						if (opline->opcode != GEAR_FETCH_STATIC_PROP_FUNC_ARG) {
							opline->opcode -= 12;
						} else {
							opline->opcode = GEAR_FETCH_STATIC_PROP_R;
						}
					}
				}
				break;
			case GEAR_SEND_VAL_EX:
				if (call_stack[call - 1].func) {
					if (ARG_MUST_BE_SENT_BY_REF(call_stack[call - 1].func, opline->op2.num)) {
						/* We won't convert it into_DO_FCALL to emit error at run-time */
						call_stack[call - 1].opline = NULL;
					} else {
						opline->opcode = GEAR_SEND_VAL;
					}
				}
				break;
			case GEAR_CHECK_FUNC_ARG:
				if (call_stack[call - 1].func) {
					call_stack[call - 1].func_arg_num = opline->op2.num;
					MAKE_NOP(opline);
				}
				break;
			case GEAR_SEND_VAR_EX:
			case GEAR_SEND_FUNC_ARG:
				if (call_stack[call - 1].func) {
					call_stack[call - 1].func_arg_num = (uint32_t)-1;
					if (ARG_SHOULD_BE_SENT_BY_REF(call_stack[call - 1].func, opline->op2.num)) {
						opline->opcode = GEAR_SEND_REF;
					} else {
						opline->opcode = GEAR_SEND_VAR;
					}
				}
				break;
			case GEAR_SEND_VAR_NO_REF_EX:
				if (call_stack[call - 1].func) {
					if (ARG_MUST_BE_SENT_BY_REF(call_stack[call - 1].func, opline->op2.num)) {
						opline->opcode = GEAR_SEND_VAR_NO_REF;
					} else if (ARG_MAY_BE_SENT_BY_REF(call_stack[call - 1].func, opline->op2.num)) {
						opline->opcode = GEAR_SEND_VAL;
					} else {
						opline->opcode = GEAR_SEND_VAR;
					}
				}
				break;
			case GEAR_SEND_UNPACK:
			case GEAR_SEND_USER:
			case GEAR_SEND_ARRAY:
				call_stack[call - 1].try_inline = 0;
				break;
			default:
				break;
		}
		opline++;
	}

	gear_arena_release(&ctx->arena, checkpoint);
}
