/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_API.h"
#include "gear_constants.h"
#include "gear_execute.h"
#include "gear_vm.h"
#include "gear_bitset.h"
#include "gear_cfg.h"
#include "gear_ssa.h"
#include "gear_func_info.h"
#include "gear_call_graph.h"
#include "gear_inference.h"
#include "gear_dump.h"

#ifndef GEAR_DEBUG_DFA
# define GEAR_DEBUG_DFA GEAR_DEBUG
#endif

#if GEAR_DEBUG_DFA
# include "ssa_integrity.c"
#endif

int gear_dfa_analyze_op_array(gear_op_array *op_array, gear_optimizer_ctx *ctx, gear_ssa *ssa)
{
	uint32_t build_flags;

	if (op_array->last_try_catch) {
		/* TODO: we can't analyze functions with try/catch/finally ??? */
		return FAILURE;
	}

    /* Build SSA */
	memset(ssa, 0, sizeof(gear_ssa));

	if (gear_build_cfg(&ctx->arena, op_array, GEAR_CFG_NO_ENTRY_PREDECESSORS, &ssa->cfg) != SUCCESS) {
		return FAILURE;
	}

	if ((ssa->cfg.flags & GEAR_FUNC_INDIRECT_VAR_ACCESS)) {
		/* TODO: we can't analyze functions with indirect variable access ??? */
		return FAILURE;
	}

	if (gear_cfg_build_predecessors(&ctx->arena, &ssa->cfg) != SUCCESS) {
		return FAILURE;
	}

	if (ctx->debug_level & GEAR_DUMP_DFA_CFG) {
		gear_dump_op_array(op_array, GEAR_DUMP_CFG, "dfa cfg", &ssa->cfg);
	}

	/* Compute Dominators Tree */
	if (gear_cfg_compute_dominators_tree(op_array, &ssa->cfg) != SUCCESS) {
		return FAILURE;
	}

	/* Identify reducible and irreducible loops */
	if (gear_cfg_identify_loops(op_array, &ssa->cfg) != SUCCESS) {
		return FAILURE;
	}

	if (ctx->debug_level & GEAR_DUMP_DFA_DOMINATORS) {
		gear_dump_dominators(op_array, &ssa->cfg);
	}

	build_flags = 0;
	if (ctx->debug_level & GEAR_DUMP_DFA_LIVENESS) {
		build_flags |= GEAR_SSA_DEBUG_LIVENESS;
	}
	if (ctx->debug_level & GEAR_DUMP_DFA_PHI) {
		build_flags |= GEAR_SSA_DEBUG_PHI_PLACEMENT;
	}
	if (gear_build_ssa(&ctx->arena, ctx->script, op_array, build_flags, ssa) != SUCCESS) {
		return FAILURE;
	}

	if (ctx->debug_level & GEAR_DUMP_DFA_SSA) {
		gear_dump_op_array(op_array, GEAR_DUMP_SSA, "dfa ssa", ssa);
	}


	if (gear_ssa_compute_use_def_chains(&ctx->arena, op_array, ssa) != SUCCESS){
		return FAILURE;
	}

	if (gear_ssa_find_false_dependencies(op_array, ssa) != SUCCESS) {
		return FAILURE;
	}

	if (gear_ssa_find_sccs(op_array, ssa) != SUCCESS){
		return FAILURE;
	}

	if (gear_ssa_inference(&ctx->arena, op_array, ctx->script, ssa, ctx->optimization_level) != SUCCESS) {
		return FAILURE;
	}

	if (gear_ssa_escape_analysis(ctx->script, op_array, ssa) != SUCCESS) {
		return FAILURE;
	}

	if (ctx->debug_level & GEAR_DUMP_DFA_SSA_VARS) {
		gear_dump_ssa_variables(op_array, ssa, 0);
	}

	return SUCCESS;
}

static void gear_ssa_remove_nops(gear_op_array *op_array, gear_ssa *ssa, gear_optimizer_ctx *ctx)
{
	gear_basic_block *blocks = ssa->cfg.blocks;
	gear_basic_block *end = blocks + ssa->cfg.blocks_count;
	gear_basic_block *b;
	gear_func_info *func_info;
	int j;
	uint32_t i = 0;
	uint32_t target = 0;
	uint32_t *shiftlist;
	ALLOCA_FLAG(use_heap);

	shiftlist = (uint32_t *)do_alloca(sizeof(uint32_t) * op_array->last, use_heap);
	memset(shiftlist, 0, sizeof(uint32_t) * op_array->last);
	/* remove empty callee_info */
	func_info = GEAR_FUNC_INFO(op_array);
	if (func_info) {
		gear_call_info **call_info = &func_info->callee_info;
		while ((*call_info)) {
			if ((*call_info)->caller_init_opline->opcode == GEAR_NOP) {
				*call_info = (*call_info)->next_callee;
			} else {
				call_info = &(*call_info)->next_callee;
			}
		}
	}

	for (b = blocks; b < end; b++) {
		if (b->flags & (GEAR_BB_REACHABLE|GEAR_BB_UNREACHABLE_FREE)) {
			uint32_t end;

			if (b->len) {
				while (i < b->start) {
					shiftlist[i] = i - target;
					i++;
				}

				if (b->flags & GEAR_BB_UNREACHABLE_FREE) {
					/* Only keep the FREE for the loop var */
					GEAR_ASSERT(op_array->opcodes[b->start].opcode == GEAR_FREE
							|| op_array->opcodes[b->start].opcode == GEAR_FE_FREE);
					b->len = 1;
				}

				end = b->start + b->len;
				b->start = target;
				while (i < end) {
					shiftlist[i] = i - target;
					if (EXPECTED(op_array->opcodes[i].opcode != GEAR_NOP) ||
					   /* Keep NOP to support GEAR_VM_SMART_BRANCH. Using "target-1" instead of
					    * "i-1" here to check the last non-NOP instruction. */
					   (target > 0 &&
					    i + 1 < op_array->last &&
					    (op_array->opcodes[i+1].opcode == GEAR_JMPZ ||
					     op_array->opcodes[i+1].opcode == GEAR_JMPNZ) &&
					    gear_is_smart_branch(op_array->opcodes + target - 1))) {
						if (i != target) {
							op_array->opcodes[target] = op_array->opcodes[i];
							ssa->ops[target] = ssa->ops[i];
							ssa->cfg.map[target] = b - blocks;
						}
						target++;
					}
					i++;
				}
				if (target != end) {
					gear_op *opline;
					gear_op *new_opline;

					b->len = target - b->start;
					opline = op_array->opcodes + end - 1;
					if (opline->opcode == GEAR_NOP) {
						continue;
					}

					new_opline = op_array->opcodes + target - 1;
					gear_optimizer_migrate_jump(op_array, new_opline, opline);
				}
			} else {
				b->start = target;
			}
		} else {
			b->start = target;
			b->len = 0;
		}
	}

	if (target != op_array->last) {
		/* reset rest opcodes */
		for (i = target; i < op_array->last; i++) {
			MAKE_NOP(op_array->opcodes + i);
		}

		/* update SSA variables */
		for (j = 0; j < ssa->vars_count; j++) {
			if (ssa->vars[j].definition >= 0) {
				ssa->vars[j].definition -= shiftlist[ssa->vars[j].definition];
			}
			if (ssa->vars[j].use_chain >= 0) {
				ssa->vars[j].use_chain -= shiftlist[ssa->vars[j].use_chain];
			}
		}
		for (i = 0; i < op_array->last; i++) {
			if (ssa->ops[i].op1_use_chain >= 0) {
				ssa->ops[i].op1_use_chain -= shiftlist[ssa->ops[i].op1_use_chain];
			}
			if (ssa->ops[i].op2_use_chain >= 0) {
				ssa->ops[i].op2_use_chain -= shiftlist[ssa->ops[i].op2_use_chain];
			}
			if (ssa->ops[i].res_use_chain >= 0) {
				ssa->ops[i].res_use_chain -= shiftlist[ssa->ops[i].res_use_chain];
			}
		}

		/* update branch targets */
		for (b = blocks; b < end; b++) {
			if ((b->flags & GEAR_BB_REACHABLE) && b->len != 0) {
				gear_op *opline = op_array->opcodes + b->start + b->len - 1;
				gear_optimizer_shift_jump(op_array, opline, shiftlist);
			}
		}

		/* update brk/cont array */
		for (j = 0; j < op_array->last_live_range; j++) {
			op_array->live_range[j].start -= shiftlist[op_array->live_range[j].start];
			op_array->live_range[j].end   -= shiftlist[op_array->live_range[j].end];
		}

		/* update try/catch array */
		for (j = 0; j < op_array->last_try_catch; j++) {
			op_array->try_catch_array[j].try_op -= shiftlist[op_array->try_catch_array[j].try_op];
			op_array->try_catch_array[j].catch_op -= shiftlist[op_array->try_catch_array[j].catch_op];
			if (op_array->try_catch_array[j].finally_op) {
				op_array->try_catch_array[j].finally_op -= shiftlist[op_array->try_catch_array[j].finally_op];
				op_array->try_catch_array[j].finally_end -= shiftlist[op_array->try_catch_array[j].finally_end];
			}
		}

		/* update early binding list */
		if (op_array->fn_flags & GEAR_ACC_EARLY_BINDING) {
			uint32_t *opline_num = &ctx->script->first_early_binding_opline;

			GEAR_ASSERT(op_array == &ctx->script->main_op_array);
			do {
				*opline_num -= shiftlist[*opline_num];
				opline_num = &op_array->opcodes[*opline_num].result.opline_num;
			} while (*opline_num != (uint32_t)-1);
		}

		/* update call graph */
		if (func_info) {
			gear_call_info *call_info = func_info->callee_info;
			while (call_info) {
				call_info->caller_init_opline -=
					shiftlist[call_info->caller_init_opline - op_array->opcodes];
				call_info->caller_call_opline -=
					shiftlist[call_info->caller_call_opline - op_array->opcodes];
				call_info = call_info->next_callee;
			}
		}

		op_array->last = target;
	}
	free_alloca(shiftlist, use_heap);
}

static inline gear_bool can_elide_return_type_check(
		gear_op_array *op_array, gear_ssa *ssa, gear_ssa_op *ssa_op) {
	gear_arg_info *info = &op_array->arg_info[-1];
	gear_ssa_var_info *use_info = &ssa->var_info[ssa_op->op1_use];
	gear_ssa_var_info *def_info = &ssa->var_info[ssa_op->op1_def];

	if (use_info->type & MAY_BE_REF) {
		return 0;
	}

	/* A type is possible that is not in the allowed types */
	if ((use_info->type & (MAY_BE_ANY|MAY_BE_UNDEF)) & ~(def_info->type & MAY_BE_ANY)) {
		return 0;
	}

	if (GEAR_TYPE_CODE(info->type) == IS_CALLABLE) {
		return 0;
	}

	if (GEAR_TYPE_IS_CLASS(info->type)) {
		if (!use_info->ce || !def_info->ce || !instanceof_function(use_info->ce, def_info->ce)) {
			return 0;
		}
	}

	return 1;
}

static gear_bool opline_supports_assign_contraction(
		gear_ssa *ssa, gear_op *opline, int src_var, uint32_t cv_var) {
	if (opline->opcode == GEAR_NEW) {
		/* see Gear/tests/generators/aborted_yield_during_new.hysst */
		return 0;
	}

	if (opline->opcode == GEAR_DO_ICALL || opline->opcode == GEAR_DO_UCALL
			|| opline->opcode == GEAR_DO_FCALL || opline->opcode == GEAR_DO_FCALL_BY_NAME) {
		/* Function calls may dtor the return value after it has already been written -- allow
		 * direct assignment only for types where a double-dtor does not matter. */
		uint32_t type = ssa->var_info[src_var].type;
		uint32_t simple = MAY_BE_NULL|MAY_BE_FALSE|MAY_BE_TRUE|MAY_BE_LONG|MAY_BE_DOUBLE;
		return !((type & MAY_BE_ANY) & ~simple);
	}

	if (opline->opcode == GEAR_POST_INC || opline->opcode == GEAR_POST_DEC) {
		/* POST_INC/DEC write the result variable before performing the inc/dec. For $i = $i++
		 * eliding the temporary variable would thus yield an incorrect result. */
		return opline->op1_type != IS_CV || opline->op1.var != cv_var;
	}

	if (opline->opcode == GEAR_INIT_ARRAY) {
		/* INIT_ARRAY initializes the result array before reading key/value. */
		return (opline->op1_type != IS_CV || opline->op1.var != cv_var)
			&& (opline->op2_type != IS_CV || opline->op2.var != cv_var);
	}

	if (opline->opcode == GEAR_CAST
			&& (opline->extended_value == IS_ARRAY || opline->extended_value == IS_OBJECT)) {
		/* CAST to array/object may initialize the result to an empty array/object before
		 * reading the expression. */
		return opline->op1_type != IS_CV || opline->op1.var != cv_var;
	}

	return 1;
}

int gear_dfa_optimize_calls(gear_op_array *op_array, gear_ssa *ssa)
{
	gear_func_info *func_info = GEAR_FUNC_INFO(op_array);
	int removed_ops = 0;

	if (func_info->callee_info) {
		gear_call_info *call_info = func_info->callee_info;

		do {
			if (call_info->caller_call_opline->opcode == GEAR_DO_ICALL
			 && call_info->callee_func
			 && ZSTR_LEN(call_info->callee_func->common.function_name) == sizeof("in_array")-1
			 && memcmp(ZSTR_VAL(call_info->callee_func->common.function_name), "in_array", sizeof("in_array")-1) == 0
			 && (call_info->caller_init_opline->extended_value == 2
			  || (call_info->caller_init_opline->extended_value == 3
			   && (call_info->caller_call_opline - 1)->opcode == GEAR_SEND_VAL
			   && (call_info->caller_call_opline - 1)->op1_type == IS_CONST))) {

				gear_op *send_array;
				gear_op *send_needly;
				gear_bool strict = 0;

				if (call_info->caller_init_opline->extended_value == 2) {
					send_array = call_info->caller_call_opline - 1;
					send_needly = call_info->caller_call_opline - 2;
				} else {
					if (gear_is_true(CT_CONSTANT_EX(op_array, (call_info->caller_call_opline - 1)->op1.constant))) {
						strict = 1;
					}
					send_array = call_info->caller_call_opline - 2;
					send_needly = call_info->caller_call_opline - 3;
				}

				if (send_array->opcode == GEAR_SEND_VAL
				 && send_array->op1_type == IS_CONST
				 && Z_TYPE_P(CT_CONSTANT_EX(op_array, send_array->op1.constant)) == IS_ARRAY
				 && (send_needly->opcode == GEAR_SEND_VAL
				  || send_needly->opcode == GEAR_SEND_VAR)
			    ) {
					int ok = 1;

					HashTable *src = Z_ARRVAL_P(CT_CONSTANT_EX(op_array, send_array->op1.constant));
					HashTable *dst;
					zval *val, tmp;
					gear_ulong idx;

					ZVAL_TRUE(&tmp);
					dst = gear_new_array(gear_hash_num_elements(src));
					if (strict) {
						GEAR_HASH_FOREACH_VAL(src, val) {
							if (Z_TYPE_P(val) == IS_STRING) {
								gear_hash_add(dst, Z_STR_P(val), &tmp);
							} else if (Z_TYPE_P(val) == IS_LONG) {
								gear_hash_index_add(dst, Z_LVAL_P(val), &tmp);
							} else {
								gear_array_destroy(dst);
								ok = 0;
								break;
							}
						} GEAR_HASH_FOREACH_END();
					} else {
						GEAR_HASH_FOREACH_VAL(src, val) {
							if (Z_TYPE_P(val) != IS_STRING || GEAR_HANDLE_NUMERIC(Z_STR_P(val), idx)) {
								gear_array_destroy(dst);
								ok = 0;
								break;
							}
							gear_hash_add(dst, Z_STR_P(val), &tmp);
						} GEAR_HASH_FOREACH_END();
					}

					if (ok) {
						uint32_t op_num = send_needly - op_array->opcodes;
						gear_ssa_op *ssa_op = ssa->ops + op_num;

						if (ssa_op->op1_use >= 0) {
							/* Reconstruct SSA */
							int var_num = ssa_op->op1_use;
							gear_ssa_var *var = ssa->vars + var_num;

							GEAR_ASSERT(ssa_op->op1_def < 0);
							gear_ssa_unlink_use_chain(ssa, op_num, ssa_op->op1_use);
							ssa_op->op1_use = -1;
							ssa_op->op1_use_chain = -1;
							op_num = call_info->caller_call_opline - op_array->opcodes;
							ssa_op = ssa->ops + op_num;
							ssa_op->op1_use = var_num;
							ssa_op->op1_use_chain = var->use_chain;
							var->use_chain = op_num;
						}

						ZVAL_ARR(&tmp, dst);

						/* Update opcode */
						call_info->caller_call_opline->opcode = GEAR_IN_ARRAY;
						call_info->caller_call_opline->extended_value = strict;
						call_info->caller_call_opline->op1_type = send_needly->op1_type;
						call_info->caller_call_opline->op1.num = send_needly->op1.num;
						call_info->caller_call_opline->op2_type = IS_CONST;
						call_info->caller_call_opline->op2.constant = gear_optimizer_add_literal(op_array, &tmp);
						if (call_info->caller_init_opline->extended_value == 3) {
							MAKE_NOP(call_info->caller_call_opline - 1);
						}
						MAKE_NOP(call_info->caller_init_opline);
						MAKE_NOP(send_needly);
						MAKE_NOP(send_array);
						removed_ops++;

					}
				}
			}
			call_info = call_info->next_callee;
		} while (call_info);
	}

	return removed_ops;
}

static gear_always_inline void take_successor_0(gear_ssa *ssa, int block_num, gear_basic_block *block)
{
	if (block->successors_count == 2) {
		if (block->successors[1] != block->successors[0]) {
			gear_ssa_remove_predecessor(ssa, block_num, block->successors[1]);
		}
		block->successors_count = 1;
	}
}

static gear_always_inline void take_successor_1(gear_ssa *ssa, int block_num, gear_basic_block *block)
{
	if (block->successors_count == 2) {
		if (block->successors[1] != block->successors[0]) {
			gear_ssa_remove_predecessor(ssa, block_num, block->successors[0]);
			block->successors[0] = block->successors[1];
		}
		block->successors_count = 1;
	}
}

static void compress_block(gear_op_array *op_array, gear_basic_block *block)
{
	while (block->len > 0) {
		gear_op *opline = &op_array->opcodes[block->start + block->len - 1];

		if (opline->opcode == GEAR_NOP
				&& (block->len == 1 || !gear_is_smart_branch(opline - 1))) {
			block->len--;
		} else {
			break;
		}
	}
}

static void replace_predecessor(gear_ssa *ssa, int block_id, int old_pred, int new_pred) {
	gear_basic_block *block = &ssa->cfg.blocks[block_id];
	int *predecessors = &ssa->cfg.predecessors[block->predecessor_offset];
	gear_ssa_phi *phi;

	int i;
	int old_pred_idx = -1;
	int new_pred_idx = -1;
	for (i = 0; i < block->predecessors_count; i++) {
		if (predecessors[i] == old_pred) {
			old_pred_idx = i;
		}
		if (predecessors[i] == new_pred) {
			new_pred_idx = i;
		}
	}

	GEAR_ASSERT(old_pred_idx != -1);
	if (new_pred_idx == -1) {
		/* If the new predecessor doesn't exist yet, simply rewire the old one */
		predecessors[old_pred_idx] = new_pred;
	} else {
		/* Otherwise, rewiring the old predecessor would make the new predecessor appear
		 * twice, which violates our CFG invariants. Remove the old predecessor instead. */
		memmove(
			predecessors + old_pred_idx,
			predecessors + old_pred_idx + 1,
			sizeof(int) * (block->predecessors_count - old_pred_idx - 1)
		);

		/* Also remove the corresponding phi node entries */
		for (phi = ssa->blocks[block_id].phis; phi; phi = phi->next) {
			memmove(
				phi->sources + old_pred_idx,
				phi->sources + old_pred_idx + 1,
				sizeof(int) * (block->predecessors_count - old_pred_idx - 1)
			);
		}

		block->predecessors_count--;
	}
}

static void gear_ssa_replace_control_link(gear_op_array *op_array, gear_ssa *ssa, int from, int to, int new_to)
{
	gear_basic_block *src = &ssa->cfg.blocks[from];
	gear_basic_block *old = &ssa->cfg.blocks[to];
	gear_basic_block *dst = &ssa->cfg.blocks[new_to];
	int i;
	gear_op *opline;

	for (i = 0; i < src->successors_count; i++) {
		if (src->successors[i] == to) {
			src->successors[i] = new_to;
		}
	}

	if (src->len > 0) {
		opline = op_array->opcodes + src->start + src->len - 1;
		switch (opline->opcode) {
			case GEAR_JMP:
			case GEAR_FAST_CALL:
				GEAR_ASSERT(GEAR_OP1_JMP_ADDR(opline) == op_array->opcodes + old->start);
				GEAR_SET_OP_JMP_ADDR(opline, opline->op1, op_array->opcodes + dst->start);
				break;
			case GEAR_JMPZNZ:
				if (GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value) == old->start) {
					opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, dst->start);
				}
				/* break missing intentionally */
			case GEAR_JMPZ:
			case GEAR_JMPNZ:
			case GEAR_JMPZ_EX:
			case GEAR_JMPNZ_EX:
			case GEAR_FE_RESET_R:
			case GEAR_FE_RESET_RW:
			case GEAR_JMP_SET:
			case GEAR_COALESCE:
			case GEAR_ASSERT_CHECK:
				if (GEAR_OP2_JMP_ADDR(opline) == op_array->opcodes + old->start) {
					GEAR_SET_OP_JMP_ADDR(opline, opline->op2, op_array->opcodes + dst->start);
				}
				break;
			case GEAR_CATCH:
				if (!(opline->extended_value & GEAR_LAST_CATCH)) {
					if (GEAR_OP2_JMP_ADDR(opline) == op_array->opcodes + old->start) {
						GEAR_SET_OP_JMP_ADDR(opline, opline->op2, op_array->opcodes + dst->start);
					}
				}
				break;
			case GEAR_DECLARE_ANON_CLASS:
			case GEAR_DECLARE_ANON_INHERITED_CLASS:
			case GEAR_FE_FETCH_R:
			case GEAR_FE_FETCH_RW:
				if (GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value) == old->start) {
					opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, dst->start);
				}
				break;
			case GEAR_SWITCH_LONG:
			case GEAR_SWITCH_STRING:
				{
					HashTable *jumptable = Z_ARRVAL(GEAR_OP2_LITERAL(opline));
					zval *zv;
					GEAR_HASH_FOREACH_VAL(jumptable, zv) {
						if (GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, Z_LVAL_P(zv)) == old->start) {
							Z_LVAL_P(zv) = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, dst->start);
						}
					} GEAR_HASH_FOREACH_END();
					if (GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value) == old->start) {
						opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, dst->start);
					}
					break;
				}
		}
	}

	replace_predecessor(ssa, new_to, to, from);
}

static void gear_ssa_unlink_block(gear_op_array *op_array, gear_ssa *ssa, gear_basic_block *block, int block_num)
{
	if (block->predecessors_count == 1 && ssa->blocks[block_num].phis == NULL) {
		int *predecessors, i;

		GEAR_ASSERT(block->successors_count == 1);
		predecessors = &ssa->cfg.predecessors[block->predecessor_offset];
		for (i = 0; i < block->predecessors_count; i++) {
			gear_ssa_replace_control_link(op_array, ssa, predecessors[i], block_num, block->successors[0]);
		}
		gear_ssa_remove_block(op_array, ssa, block_num);
	}
}

static int gear_dfa_optimize_jmps(gear_op_array *op_array, gear_ssa *ssa)
{
	int removed_ops = 0;
	int block_num = 0;

	for (block_num = 1; block_num < ssa->cfg.blocks_count; block_num++) {
		gear_basic_block *block = &ssa->cfg.blocks[block_num];

		if (!(block->flags & GEAR_BB_REACHABLE)) {
			continue;
		}
		compress_block(op_array, block);
		if (block->len == 0) {
			gear_ssa_unlink_block(op_array, ssa, block, block_num);
		}
	}

	block_num = 0;
	while (block_num < ssa->cfg.blocks_count
		&& !(ssa->cfg.blocks[block_num].flags & GEAR_BB_REACHABLE)) {
		block_num++;
	}
	while (block_num < ssa->cfg.blocks_count) {
		int next_block_num = block_num + 1;
		gear_basic_block *block = &ssa->cfg.blocks[block_num];
		uint32_t op_num;
		gear_op *opline;
		gear_ssa_op *ssa_op;

		while (next_block_num < ssa->cfg.blocks_count
			&& !(ssa->cfg.blocks[next_block_num].flags & GEAR_BB_REACHABLE)) {
			next_block_num++;
		}

		if (block->len) {
			op_num = block->start + block->len - 1;
			opline = op_array->opcodes + op_num;
			ssa_op = ssa->ops + op_num;

			switch (opline->opcode) {
				case GEAR_JMP:
optimize_jmp:
					if (block->successors[0] == next_block_num) {
						MAKE_NOP(opline);
						removed_ops++;
						goto optimize_nop;
					}
					break;
				case GEAR_JMPZ:
optimize_jmpz:
					if (opline->op1_type == IS_CONST) {
						if (gear_is_true(CT_CONSTANT_EX(op_array, opline->op1.constant))) {
							MAKE_NOP(opline);
							removed_ops++;
							take_successor_1(ssa, block_num, block);
							goto optimize_nop;
						} else {
							opline->opcode = GEAR_JMP;
							COPY_NODE(opline->op1, opline->op2);
							take_successor_0(ssa, block_num, block);
							goto optimize_jmp;
						}
					} else {
						if (block->successors[0] == next_block_num) {
							take_successor_0(ssa, block_num, block);
							if (opline->op1_type == IS_CV && (OP1_INFO() & MAY_BE_UNDEF)) {
								opline->opcode = GEAR_CHECK_VAR;
								opline->op2.num = 0;
							} else if (opline->op1_type == IS_CV || !(OP1_INFO() & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))) {
								gear_ssa_remove_instr(ssa, opline, ssa_op);
								removed_ops++;
								goto optimize_nop;
							} else {
								opline->opcode = GEAR_FREE;
								opline->op2.num = 0;
							}
						}
					}
					break;
				case GEAR_JMPNZ:
optimize_jmpnz:
					if (opline->op1_type == IS_CONST) {
						if (gear_is_true(CT_CONSTANT_EX(op_array, opline->op1.constant))) {
							opline->opcode = GEAR_JMP;
							COPY_NODE(opline->op1, opline->op2);
							take_successor_0(ssa, block_num, block);
							goto optimize_jmp;
						} else {
							MAKE_NOP(opline);
							removed_ops++;
							take_successor_1(ssa, block_num, block);
							goto optimize_nop;
						}
					} else if (block->successors_count == 2) {
						if (block->successors[0] == next_block_num) {
							take_successor_0(ssa, block_num, block);
							if (opline->op1_type == IS_CV && (OP1_INFO() & MAY_BE_UNDEF)) {
								opline->opcode = GEAR_CHECK_VAR;
								opline->op2.num = 0;
							} else if (opline->op1_type == IS_CV || !(OP1_INFO() & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))) {
								gear_ssa_remove_instr(ssa, opline, ssa_op);
								removed_ops++;
								goto optimize_nop;
							} else {
								opline->opcode = GEAR_FREE;
								opline->op2.num = 0;
							}
						}
					}
					break;
				case GEAR_JMPZNZ:
					if (opline->op1_type == IS_CONST) {
						if (gear_is_true(CT_CONSTANT_EX(op_array, opline->op1.constant))) {
							gear_op *target_opline = GEAR_OFFSET_TO_OPLINE(opline, opline->extended_value);
							GEAR_SET_OP_JMP_ADDR(opline, opline->op1, target_opline);
							take_successor_1(ssa, block_num, block);
						} else {
							gear_op *target_opline = GEAR_OP2_JMP_ADDR(opline);
							GEAR_SET_OP_JMP_ADDR(opline, opline->op1, target_opline);
							take_successor_0(ssa, block_num, block);
						}
						opline->op1_type = IS_UNUSED;
						opline->extended_value = 0;
						opline->opcode = GEAR_JMP;
						goto optimize_jmp;
					} else if (block->successors_count == 2) {
						if (block->successors[0] == block->successors[1]) {
							take_successor_0(ssa, block_num, block);
							if (block->successors[0] == next_block_num) {
								if (opline->op1_type == IS_CV && (OP1_INFO() & MAY_BE_UNDEF)) {
									opline->opcode = GEAR_CHECK_VAR;
									opline->op2.num = 0;
								} else if (opline->op1_type == IS_CV || !(OP1_INFO() & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))) {
									gear_ssa_remove_instr(ssa, opline, ssa_op);
									removed_ops++;
									goto optimize_nop;
								} else {
									opline->opcode = GEAR_FREE;
									opline->op2.num = 0;
								}
							} else if ((opline->op1_type == IS_CV && !(OP1_INFO() & MAY_BE_UNDEF)) || !(OP1_INFO() & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))) {
								GEAR_ASSERT(ssa_op->op1_use >= 0);
								gear_ssa_unlink_use_chain(ssa, op_num, ssa_op->op1_use);
								ssa_op->op1_use = -1;
								ssa_op->op1_use_chain = -1;
								opline->opcode = GEAR_JMP;
								opline->op1_type = IS_UNUSED;
								opline->op1.num = opline->op2.num;
								goto optimize_jmp;
							}
						}
					}
					break;
				case GEAR_JMPZ_EX:
					if (ssa->vars[ssa_op->result_def].use_chain < 0
							&& ssa->vars[ssa_op->result_def].phi_use_chain == NULL) {
						opline->opcode = GEAR_JMPZ;
						opline->result_type = IS_UNUSED;
						gear_ssa_remove_result_def(ssa, ssa_op);
						goto optimize_jmpz;
					} else if (opline->op1_type == IS_CONST) {
						if (gear_is_true(CT_CONSTANT_EX(op_array, opline->op1.constant))) {
							opline->opcode = GEAR_QM_ASSIGN;
							take_successor_1(ssa, block_num, block);
						}
					}
					break;
				case GEAR_JMPNZ_EX:
					if (ssa->vars[ssa_op->result_def].use_chain < 0
							&& ssa->vars[ssa_op->result_def].phi_use_chain == NULL) {
						opline->opcode = GEAR_JMPNZ;
						opline->result_type = IS_UNUSED;
						gear_ssa_remove_result_def(ssa, ssa_op);
						goto optimize_jmpnz;
					} else if (opline->op1_type == IS_CONST) {
						if (!gear_is_true(CT_CONSTANT_EX(op_array, opline->op1.constant))) {
							opline->opcode = GEAR_QM_ASSIGN;
							take_successor_1(ssa, block_num, block);
						}
					}
					break;
				case GEAR_JMP_SET:
					if (ssa->vars[ssa_op->result_def].use_chain < 0
							&& ssa->vars[ssa_op->result_def].phi_use_chain == NULL) {
						opline->opcode = GEAR_JMPNZ;
						opline->result_type = IS_UNUSED;
						gear_ssa_remove_result_def(ssa, ssa_op);
						goto optimize_jmpnz;
					} else if (opline->op1_type == IS_CONST) {
						if (!gear_is_true(CT_CONSTANT_EX(op_array, opline->op1.constant))) {
							MAKE_NOP(opline);
							removed_ops++;
							take_successor_1(ssa, block_num, block);
							gear_ssa_remove_result_def(ssa, ssa_op);
							goto optimize_nop;
						}
					}
					break;
				case GEAR_COALESCE:
				{
					gear_ssa_var *var = &ssa->vars[ssa_op->result_def];
					if (opline->op1_type == IS_CONST
							&& var->use_chain < 0 && var->phi_use_chain == NULL) {
						if (Z_TYPE_P(CT_CONSTANT_EX(op_array, opline->op1.constant)) == IS_NULL) {
							gear_ssa_remove_result_def(ssa, ssa_op);
							MAKE_NOP(opline);
							removed_ops++;
							take_successor_1(ssa, block_num, block);
							goto optimize_nop;
						} else {
							if (opline->result_type & (IS_TMP_VAR|IS_VAR)) {
								gear_optimizer_remove_live_range_ex(op_array, opline->result.var, var->definition);
							}
							opline->opcode = GEAR_JMP;
							opline->result_type = IS_UNUSED;
							gear_ssa_remove_result_def(ssa, ssa_op);
							COPY_NODE(opline->op1, opline->op2);
							take_successor_0(ssa, block_num, block);
							goto optimize_jmp;
						}
					}
					break;
				}
				case GEAR_NOP:
optimize_nop:
					compress_block(op_array, block);
					if (block->len == 0) {
						if (block_num > 0) {
							gear_ssa_unlink_block(op_array, ssa, block, block_num);
							/* backtrack to previous basic block */
							do {
								block_num--;
							} while (block_num >= 0
								&& !(ssa->cfg.blocks[block_num].flags & GEAR_BB_REACHABLE));
							if (block_num >= 0) {
								continue;
							}
						}
					}
					break;
				default:
					break;
			}
		}

		block_num = next_block_num;
	}

	return removed_ops;
}

void gear_dfa_optimize_op_array(gear_op_array *op_array, gear_optimizer_ctx *ctx, gear_ssa *ssa, gear_call_info **call_map)
{
	if (ctx->debug_level & GEAR_DUMP_BEFORE_DFA_PASS) {
		gear_dump_op_array(op_array, GEAR_DUMP_SSA, "before dfa pass", ssa);
	}

	if (ssa->var_info) {
		int op_1;
		int v;
		int remove_nops = 0;
		gear_op *opline;
		zval tmp;

#if GEAR_DEBUG_DFA
		ssa_verify_integrity(op_array, ssa, "before dfa");
#endif

		if (GEAR_OPTIMIZER_PASS_8 & ctx->optimization_level) {
			if (sccp_optimize_op_array(ctx, op_array, ssa, call_map)) {
				remove_nops = 1;
			}

			if (gear_dfa_optimize_jmps(op_array, ssa)) {
				remove_nops = 1;
			}

#if GEAR_DEBUG_DFA
			ssa_verify_integrity(op_array, ssa, "after sccp");
#endif
			if (GEAR_FUNC_INFO(op_array)) {
				if (gear_dfa_optimize_calls(op_array, ssa)) {
					remove_nops = 1;
				}
			}
			if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_8) {
				gear_dump_op_array(op_array, GEAR_DUMP_SSA, "after sccp pass", ssa);
			}
#if GEAR_DEBUG_DFA
			ssa_verify_integrity(op_array, ssa, "after calls");
#endif
		}

		if (GEAR_OPTIMIZER_PASS_14 & ctx->optimization_level) {
			if (dce_optimize_op_array(op_array, ssa, 0)) {
				remove_nops = 1;
			}
			if (gear_dfa_optimize_jmps(op_array, ssa)) {
				remove_nops = 1;
			}
			if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_14) {
				gear_dump_op_array(op_array, GEAR_DUMP_SSA, "after dce pass", ssa);
			}
#if GEAR_DEBUG_DFA
			ssa_verify_integrity(op_array, ssa, "after dce");
#endif
		}

		for (v = op_array->last_var; v < ssa->vars_count; v++) {

			op_1 = ssa->vars[v].definition;

			if (op_1 < 0) {
				continue;
			}

			opline = op_array->opcodes + op_1;

			/* Convert LONG constants to DOUBLE */
			if (ssa->var_info[v].use_as_double) {
				if (opline->opcode == GEAR_ASSIGN
				 && opline->op2_type == IS_CONST
				 && ssa->ops[op_1].op1_def == v
				 && !RETURN_VALUE_USED(opline)
				) {

// op_1: ASSIGN ? -> #v [use_as_double], long(?) => ASSIGN ? -> #v, double(?)

					zval *zv = CT_CONSTANT_EX(op_array, opline->op2.constant);
					GEAR_ASSERT(Z_TYPE_INFO_P(zv) == IS_LONG);
					ZVAL_DOUBLE(&tmp, zval_get_double(zv));
					opline->op2.constant = gear_optimizer_add_literal(op_array, &tmp);

				} else if (opline->opcode == GEAR_QM_ASSIGN
				 && opline->op1_type == IS_CONST
				) {

// op_1: QM_ASSIGN #v [use_as_double], long(?) => QM_ASSIGN #v, double(?)

					zval *zv = CT_CONSTANT_EX(op_array, opline->op1.constant);
					GEAR_ASSERT(Z_TYPE_INFO_P(zv) == IS_LONG);
					ZVAL_DOUBLE(&tmp, zval_get_double(zv));
					opline->op1.constant = gear_optimizer_add_literal(op_array, &tmp);
				}

			} else {
				if (opline->opcode == GEAR_ADD
				 || opline->opcode == GEAR_SUB
				 || opline->opcode == GEAR_MUL
				 || opline->opcode == GEAR_IS_EQUAL
				 || opline->opcode == GEAR_IS_NOT_EQUAL
				 || opline->opcode == GEAR_IS_SMALLER
				 || opline->opcode == GEAR_IS_SMALLER_OR_EQUAL
				) {

					if (opline->op1_type == IS_CONST
					 && opline->op2_type != IS_CONST
					 && (OP2_INFO() & MAY_BE_ANY) == MAY_BE_DOUBLE
					 && Z_TYPE_INFO_P(CT_CONSTANT_EX(op_array, opline->op1.constant)) == IS_LONG
					) {

// op_1: #v.? = ADD long(?), #?.? [double] => #v.? = ADD double(?), #?.? [double]

						zval *zv = CT_CONSTANT_EX(op_array, opline->op1.constant);
						ZVAL_DOUBLE(&tmp, zval_get_double(zv));
						opline->op1.constant = gear_optimizer_add_literal(op_array, &tmp);

					} else if (opline->op1_type != IS_CONST
					 && opline->op2_type == IS_CONST
					 && (OP1_INFO() & MAY_BE_ANY) == MAY_BE_DOUBLE
					 && Z_TYPE_INFO_P(CT_CONSTANT_EX(op_array, opline->op2.constant)) == IS_LONG
					) {

// op_1: #v.? = ADD #?.? [double], long(?) => #v.? = ADD #?.? [double], double(?)

						zval *zv = CT_CONSTANT_EX(op_array, opline->op2.constant);
						ZVAL_DOUBLE(&tmp, zval_get_double(zv));
						opline->op2.constant = gear_optimizer_add_literal(op_array, &tmp);
					}
				} else if (opline->opcode == GEAR_CONCAT) {
					if (!(OP1_INFO() & MAY_BE_OBJECT)
					 && !(OP2_INFO() & MAY_BE_OBJECT)) {
						opline->opcode = GEAR_FAST_CONCAT;
					}
				}
			}

			if (ssa->vars[v].var >= op_array->last_var) {
				/* skip TMP and VAR */
				continue;
			}

			if (opline->opcode == GEAR_ASSIGN
			 && ssa->ops[op_1].op1_def == v
			 && !RETURN_VALUE_USED(opline)
			) {
				int orig_var = ssa->ops[op_1].op1_use;

				if (orig_var >= 0
				 && !(ssa->var_info[orig_var].type & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))
				) {

					int src_var = ssa->ops[op_1].op2_use;

					if ((opline->op2_type & (IS_TMP_VAR|IS_VAR))
					 && src_var >= 0
					 && !(ssa->var_info[src_var].type & MAY_BE_REF)
					 && ssa->vars[src_var].definition >= 0
					 && ssa->ops[ssa->vars[src_var].definition].result_def == src_var
					 && ssa->ops[ssa->vars[src_var].definition].result_use < 0
					 && ssa->vars[src_var].use_chain == op_1
					 && ssa->ops[op_1].op2_use_chain < 0
					 && !ssa->vars[src_var].phi_use_chain
					 && !ssa->vars[src_var].sym_use_chain
					 && opline_supports_assign_contraction(
						 ssa, &op_array->opcodes[ssa->vars[src_var].definition],
						 src_var, opline->op1.var)
					) {

						int op_2 = ssa->vars[src_var].definition;

// op_2: #src_var.T = OP ...                                     => #v.CV = OP ...
// op_1: ASSIGN #orig_var.CV [undef,scalar] -> #v.CV, #src_var.T    NOP

						if (gear_ssa_unlink_use_chain(ssa, op_1, orig_var)) {
							/* Reconstruct SSA */
							ssa->vars[v].definition = op_2;
							ssa->ops[op_2].result_def = v;

							ssa->vars[src_var].definition = -1;
							ssa->vars[src_var].use_chain = -1;

							ssa->ops[op_1].op1_use = -1;
							ssa->ops[op_1].op2_use = -1;
							ssa->ops[op_1].op1_def = -1;
							ssa->ops[op_1].op1_use_chain = -1;

							/* Update opcodes */
							op_array->opcodes[op_2].result_type = opline->op1_type;
							op_array->opcodes[op_2].result.var = opline->op1.var;
							MAKE_NOP(opline);
							remove_nops = 1;
						}
					} else if (opline->op2_type == IS_CONST
					 || ((opline->op2_type & (IS_TMP_VAR|IS_VAR|IS_CV))
					     && ssa->ops[op_1].op2_use >= 0
					     && ssa->ops[op_1].op2_def < 0)
					) {

// op_1: ASSIGN #orig_var.CV [undef,scalar] -> #v.CV, CONST|TMPVAR => QM_ASSIGN v.CV, CONST|TMPVAR

						if (ssa->ops[op_1].op1_use != ssa->ops[op_1].op2_use) {
							gear_ssa_unlink_use_chain(ssa, op_1, orig_var);
						} else {
							ssa->ops[op_1].op2_use_chain = ssa->ops[op_1].op1_use_chain;
						}

						/* Reconstruct SSA */
						ssa->ops[op_1].result_def = v;
						ssa->ops[op_1].op1_def = -1;
						ssa->ops[op_1].op1_use = ssa->ops[op_1].op2_use;
						ssa->ops[op_1].op1_use_chain = ssa->ops[op_1].op2_use_chain;
						ssa->ops[op_1].op2_use = -1;
						ssa->ops[op_1].op2_use_chain = -1;

						/* Update opcode */
						opline->result_type = opline->op1_type;
						opline->result.var = opline->op1.var;
						opline->op1_type = opline->op2_type;
						opline->op1.var = opline->op2.var;
						opline->op2_type = IS_UNUSED;
						opline->op2.var = 0;
						opline->opcode = GEAR_QM_ASSIGN;
					}
				}

			} else if (opline->opcode == GEAR_ASSIGN_ADD
			 && opline->extended_value == 0
			 && ssa->ops[op_1].op1_def == v
			 && opline->op2_type == IS_CONST
			 && Z_TYPE_P(CT_CONSTANT_EX(op_array, opline->op2.constant)) == IS_LONG
			 && Z_LVAL_P(CT_CONSTANT_EX(op_array, opline->op2.constant)) == 1
			 && ssa->ops[op_1].op1_use >= 0
			 && !(ssa->var_info[ssa->ops[op_1].op1_use].type & (MAY_BE_FALSE|MAY_BE_TRUE|MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))) {

// op_1: ASSIGN_ADD #?.CV [undef,null,int,foat] ->#v.CV, int(1) => PRE_INC #?.CV ->#v.CV

				opline->opcode = GEAR_PRE_INC;
				SET_UNUSED(opline->op2);

			} else if (opline->opcode == GEAR_ASSIGN_SUB
			 && opline->extended_value == 0
			 && ssa->ops[op_1].op1_def == v
			 && opline->op2_type == IS_CONST
			 && Z_TYPE_P(CT_CONSTANT_EX(op_array, opline->op2.constant)) == IS_LONG
			 && Z_LVAL_P(CT_CONSTANT_EX(op_array, opline->op2.constant)) == 1
			 && ssa->ops[op_1].op1_use >= 0
			 && !(ssa->var_info[ssa->ops[op_1].op1_use].type & (MAY_BE_FALSE|MAY_BE_TRUE|MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))) {

// op_1: ASSIGN_SUB #?.CV [undef,null,int,foat] -> #v.CV, int(1) => PRE_DEC #?.CV ->#v.CV

				opline->opcode = GEAR_PRE_DEC;
				SET_UNUSED(opline->op2);

			} else if (opline->opcode == GEAR_VERIFY_RETURN_TYPE
			 && ssa->ops[op_1].op1_def == v
			 && ssa->ops[op_1].op1_use >= 0
			 && ssa->ops[op_1].op1_use_chain == -1
			 && ssa->vars[v].use_chain >= 0
			 && can_elide_return_type_check(op_array, ssa, &ssa->ops[op_1])) {

// op_1: VERIFY_RETURN_TYPE #orig_var.CV [T] -> #v.CV [T] => NOP

				int orig_var = ssa->ops[op_1].op1_use;
				if (gear_ssa_unlink_use_chain(ssa, op_1, orig_var)) {

					int ret = ssa->vars[v].use_chain;

					ssa->ops[ret].op1_use = orig_var;
					ssa->ops[ret].op1_use_chain = ssa->vars[orig_var].use_chain;
					ssa->vars[orig_var].use_chain = ret;

					ssa->vars[v].definition = -1;
					ssa->vars[v].use_chain = -1;

					ssa->ops[op_1].op1_def = -1;
					ssa->ops[op_1].op1_use = -1;

					MAKE_NOP(opline);
					remove_nops = 1;
				}

			} else if (ssa->ops[op_1].op1_def == v
			 && !RETURN_VALUE_USED(opline)
			 && ssa->ops[op_1].op1_use >= 0
			 && !(ssa->var_info[ssa->ops[op_1].op1_use].type & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))
			 && (opline->opcode == GEAR_ASSIGN_ADD
			  || opline->opcode == GEAR_ASSIGN_SUB
			  || opline->opcode == GEAR_ASSIGN_MUL
			  || opline->opcode == GEAR_ASSIGN_DIV
			  || opline->opcode == GEAR_ASSIGN_MOD
			  || opline->opcode == GEAR_ASSIGN_SL
			  || opline->opcode == GEAR_ASSIGN_SR
			  || opline->opcode == GEAR_ASSIGN_BW_OR
			  || opline->opcode == GEAR_ASSIGN_BW_AND
			  || opline->opcode == GEAR_ASSIGN_BW_XOR)
			 && opline->extended_value == 0) {

// op_1: ASSIGN_ADD #orig_var.CV [undef,null,bool,int,double] -> #v.CV, ? => #v.CV = ADD #orig_var.CV, ?

				/* Reconstruct SSA */
				ssa->ops[op_1].result_def = ssa->ops[op_1].op1_def;
				ssa->ops[op_1].op1_def = -1;

				/* Update opcode */
				opline->opcode -= (GEAR_ASSIGN_ADD - GEAR_ADD);
				opline->result_type = opline->op1_type;
				opline->result.var = opline->op1.var;

			}
		}

#if GEAR_DEBUG_DFA
		ssa_verify_integrity(op_array, ssa, "after dfa");
#endif

		if (remove_nops) {
			gear_ssa_remove_nops(op_array, ssa, ctx);
#if GEAR_DEBUG_DFA
			ssa_verify_integrity(op_array, ssa, "after nop");
#endif
		}
	}

	if (ctx->debug_level & GEAR_DUMP_AFTER_DFA_PASS) {
		gear_dump_op_array(op_array, GEAR_DUMP_SSA, "after dfa pass", ssa);
	}
}

void gear_optimize_dfa(gear_op_array *op_array, gear_optimizer_ctx *ctx)
{
	void *checkpoint = gear_arena_checkpoint(ctx->arena);
	gear_ssa ssa;

	if (gear_dfa_analyze_op_array(op_array, ctx, &ssa) != SUCCESS) {
		gear_arena_release(&ctx->arena, checkpoint);
		return;
	}

	gear_dfa_optimize_op_array(op_array, ctx, &ssa, NULL);

	/* Destroy SSA */
	gear_arena_release(&ctx->arena, checkpoint);
}

