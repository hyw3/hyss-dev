/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_API.h"
#include "gear_constants.h"
#include "gear_execute.h"
#include "gear_vm.h"
#include "gear_cfg.h"
#include "gear_func_info.h"
#include "gear_call_graph.h"
#include "gear_inference.h"
#include "gear_dump.h"

#ifndef HAVE_DFA_PASS
# define HAVE_DFA_PASS 1
#endif

static void gear_optimizer_zval_dtor_wrapper(zval *zvalue)
{
	zval_ptr_dtor_nogc(zvalue);
}

void gear_optimizer_collect_constant(gear_optimizer_ctx *ctx, zval *name, zval* value)
{
	zval val;

	if (!ctx->constants) {
		ctx->constants = gear_arena_alloc(&ctx->arena, sizeof(HashTable));
		gear_hash_init(ctx->constants, 16, NULL, gear_optimizer_zval_dtor_wrapper, 0);
	}
	ZVAL_COPY(&val, value);
	gear_hash_add(ctx->constants, Z_STR_P(name), &val);
}

gear_uchar gear_compound_assign_to_binary_op(gear_uchar opcode)
{
	switch (opcode) {
		case GEAR_ASSIGN_ADD: return GEAR_ADD;
		case GEAR_ASSIGN_SUB: return GEAR_SUB;
		case GEAR_ASSIGN_MUL: return GEAR_MUL;
		case GEAR_ASSIGN_DIV: return GEAR_DIV;
		case GEAR_ASSIGN_MOD: return GEAR_MOD;
		case GEAR_ASSIGN_SL: return GEAR_SL;
		case GEAR_ASSIGN_SR: return GEAR_SR;
		case GEAR_ASSIGN_CONCAT: return GEAR_CONCAT;
		case GEAR_ASSIGN_BW_OR: return GEAR_BW_OR;
		case GEAR_ASSIGN_BW_AND: return GEAR_BW_AND;
		case GEAR_ASSIGN_BW_XOR: return GEAR_BW_XOR;
		case GEAR_ASSIGN_POW: return GEAR_POW;
		EMPTY_SWITCH_DEFAULT_CASE()
	}
}

int gear_optimizer_eval_binary_op(zval *result, gear_uchar opcode, zval *op1, zval *op2) /* {{{ */
{
	binary_op_type binary_op = get_binary_op(opcode);
	int er, ret;

	if (gear_binary_op_produces_numeric_string_error(opcode, op1, op2)) {
		/* produces numeric string E_NOTICE/E_WARNING */
		return FAILURE;
	}

	switch (opcode) {
		case GEAR_ADD:
			if ((Z_TYPE_P(op1) == IS_ARRAY
			  || Z_TYPE_P(op2) == IS_ARRAY)
			 && Z_TYPE_P(op1) != Z_TYPE_P(op2)) {
				/* produces "Unsupported operand types" exception */
				return FAILURE;
			}
			break;
		case GEAR_DIV:
		case GEAR_MOD:
			if (zval_get_long(op2) == 0) {
				/* division by 0 */
				return FAILURE;
			}
			/* break missing intentionally */
		case GEAR_SUB:
		case GEAR_MUL:
		case GEAR_POW:
		case GEAR_CONCAT:
		case GEAR_FAST_CONCAT:
			if (Z_TYPE_P(op1) == IS_ARRAY
			 || Z_TYPE_P(op2) == IS_ARRAY) {
				/* produces "Unsupported operand types" exception */
				return FAILURE;
			}
			break;
		case GEAR_SL:
		case GEAR_SR:
			if (zval_get_long(op2) < 0) {
				/* shift by negative number */
				return FAILURE;
			}
			break;
	}

	er = EG(error_reporting);
	EG(error_reporting) = 0;
	ret = binary_op(result, op1, op2);
	EG(error_reporting) = er;

	return ret;
}
/* }}} */

int gear_optimizer_eval_unary_op(zval *result, gear_uchar opcode, zval *op1) /* {{{ */
{
	unary_op_type unary_op = get_unary_op(opcode);

	if (unary_op) {
		if (opcode == GEAR_BW_NOT
		 && Z_TYPE_P(op1) != IS_LONG
		 && Z_TYPE_P(op1) != IS_DOUBLE
		 && Z_TYPE_P(op1) != IS_STRING) {
			/* produces "Unsupported operand types" exception */
			return FAILURE;
		}
		return unary_op(result, op1);
	} else { /* GEAR_BOOL */
		ZVAL_BOOL(result, gear_is_true(op1));
		return SUCCESS;
	}
}
/* }}} */

int gear_optimizer_eval_cast(zval *result, uint32_t type, zval *op1) /* {{{ */
{
	switch (type) {
		case IS_NULL:
			ZVAL_NULL(result);
			return SUCCESS;
		case _IS_BOOL:
			ZVAL_BOOL(result, zval_is_true(op1));
			return SUCCESS;
		case IS_LONG:
			ZVAL_LONG(result, zval_get_long(op1));
			return SUCCESS;
		case IS_DOUBLE:
			ZVAL_DOUBLE(result, zval_get_double(op1));
			return SUCCESS;
		case IS_STRING:
			/* Conversion from double to string takes into account run-time
			   'precision' setting and cannot be evaluated at compile-time */
			if (Z_TYPE_P(op1) != IS_ARRAY && Z_TYPE_P(op1) != IS_DOUBLE) {
				ZVAL_STR(result, zval_get_string(op1));
				return SUCCESS;
			}
			break;
		case IS_ARRAY:
			ZVAL_COPY(result, op1);
			convert_to_array(result);
			return SUCCESS;
	}
	return FAILURE;
}
/* }}} */

int gear_optimizer_eval_strlen(zval *result, zval *op1) /* {{{ */
{
	if (Z_TYPE_P(op1) != IS_STRING) {
		return FAILURE;
	}
	ZVAL_LONG(result, Z_STRLEN_P(op1));
	return SUCCESS;
}
/* }}} */

int gear_optimizer_get_collected_constant(HashTable *constants, zval *name, zval* value)
{
	zval *val;

	if ((val = gear_hash_find(constants, Z_STR_P(name))) != NULL) {
		ZVAL_COPY(value, val);
		return 1;
	}
	return 0;
}

int gear_optimizer_add_literal(gear_op_array *op_array, zval *zv)
{
	int i = op_array->last_literal;
	op_array->last_literal++;
	op_array->literals = (zval*)erealloc(op_array->literals, op_array->last_literal * sizeof(zval));
	ZVAL_COPY_VALUE(&op_array->literals[i], zv);
	Z_EXTRA(op_array->literals[i]) = 0;
	return i;
}

static inline int gear_optimizer_add_literal_string(gear_op_array *op_array, gear_string *str) {
	zval zv;
	ZVAL_STR(&zv, str);
	gear_string_hash_val(str);
	return gear_optimizer_add_literal(op_array, &zv);
}

int gear_optimizer_is_disabled_func(const char *name, size_t len) {
	gear_function *fbc = (gear_function *)gear_hash_str_find_ptr(EG(function_table), name, len);

	return (fbc && fbc->type == GEAR_INTERNAL_FUNCTION &&
			fbc->internal_function.handler == GEAR_FN(display_disabled_function));
}

static inline void drop_leading_backslash(zval *val) {
	if (Z_STRVAL_P(val)[0] == '\\') {
		gear_string *str = gear_string_init(Z_STRVAL_P(val) + 1, Z_STRLEN_P(val) - 1, 0);
		zval_ptr_dtor_nogc(val);
		ZVAL_STR(val, str);
	}
}

static inline uint32_t alloc_cache_slots(gear_op_array *op_array, uint32_t num) {
	uint32_t ret = op_array->cache_size;
	op_array->cache_size += num * sizeof(void *);
	return ret;
}

#define REQUIRES_STRING(val) do { \
	if (Z_TYPE_P(val) != IS_STRING) { \
		return 0; \
	} \
} while (0)

#define TO_STRING_NOWARN(val) do { \
	if (Z_TYPE_P(val) >= IS_ARRAY) { \
		return 0; \
	} \
	convert_to_string(val); \
} while (0)

int gear_optimizer_update_op1_const(gear_op_array *op_array,
                                    gear_op       *opline,
                                    zval          *val)
{
	switch (opline->opcode) {
		case GEAR_FREE:
		case GEAR_CHECK_VAR:
			MAKE_NOP(opline);
			zval_ptr_dtor_nogc(val);
			return 1;
		case GEAR_SEND_VAR_EX:
		case GEAR_SEND_FUNC_ARG:
		case GEAR_FETCH_DIM_W:
		case GEAR_FETCH_DIM_RW:
		case GEAR_FETCH_DIM_FUNC_ARG:
		case GEAR_FETCH_DIM_UNSET:
		case GEAR_FETCH_LIST_W:
		case GEAR_ASSIGN_DIM:
		case GEAR_RETURN_BY_REF:
		case GEAR_INSTANCEOF:
		case GEAR_MAKE_REF:
			return 0;
		case GEAR_CATCH:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			opline->extended_value = alloc_cache_slots(op_array, 1) | (opline->extended_value & GEAR_LAST_CATCH);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			break;
		case GEAR_DEFINED:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			opline->extended_value = alloc_cache_slots(op_array, 1);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			break;
		case GEAR_NEW:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			opline->op2.num = alloc_cache_slots(op_array, 1);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			break;
		case GEAR_INIT_STATIC_METHOD_CALL:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			if (opline->op2_type != IS_CONST) {
				opline->result.num = alloc_cache_slots(op_array, 1);
			}
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			break;
		case GEAR_FETCH_CLASS_CONSTANT:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			if (opline->op2_type != IS_CONST) {
				opline->extended_value = alloc_cache_slots(op_array, 1);
			}
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			break;
		case GEAR_FETCH_STATIC_PROP_R:
		case GEAR_FETCH_STATIC_PROP_W:
		case GEAR_FETCH_STATIC_PROP_RW:
		case GEAR_FETCH_STATIC_PROP_IS:
		case GEAR_FETCH_STATIC_PROP_UNSET:
		case GEAR_FETCH_STATIC_PROP_FUNC_ARG:
		case GEAR_UNSET_STATIC_PROP:
			TO_STRING_NOWARN(val);
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			if (opline->op2_type == IS_CONST && opline->extended_value + sizeof(void*) == op_array->cache_size) {
				op_array->cache_size += sizeof(void *);
			} else {
				opline->extended_value = alloc_cache_slots(op_array, 2);
			}
			break;
		case GEAR_ISSET_ISEMPTY_STATIC_PROP:
			TO_STRING_NOWARN(val);
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			if (opline->op2_type == IS_CONST && (opline->extended_value & ~GEAR_ISEMPTY) + sizeof(void*) == op_array->cache_size) {
				op_array->cache_size += sizeof(void *);
			} else {
				opline->extended_value = alloc_cache_slots(op_array, 2) | (opline->extended_value & GEAR_ISEMPTY);
			}
			break;
		case GEAR_SEND_VAR:
			opline->opcode = GEAR_SEND_VAL;
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			break;
		case GEAR_SEPARATE:
		case GEAR_SEND_VAR_NO_REF:
		case GEAR_SEND_VAR_NO_REF_EX:
			return 0;
		case GEAR_VERIFY_RETURN_TYPE:
			/* This would require a non-local change.
			 * gear_optimizer_replace_by_const() supports this. */
			return 0;
		case GEAR_CASE:
		case GEAR_FETCH_LIST_R:
			return 0;
		case GEAR_CONCAT:
		case GEAR_FAST_CONCAT:
		case GEAR_FETCH_R:
		case GEAR_FETCH_W:
		case GEAR_FETCH_RW:
		case GEAR_FETCH_IS:
		case GEAR_FETCH_UNSET:
		case GEAR_FETCH_FUNC_ARG:
			TO_STRING_NOWARN(val);
			if (opline->opcode == GEAR_CONCAT && opline->op2_type == IS_CONST) {
				opline->opcode = GEAR_FAST_CONCAT;
			}
			/* break missing intentionally */
		default:
			opline->op1.constant = gear_optimizer_add_literal(op_array, val);
			break;
	}

	opline->op1_type = IS_CONST;
	if (Z_TYPE(GEAR_OP1_LITERAL(opline)) == IS_STRING) {
		gear_string_hash_val(Z_STR(GEAR_OP1_LITERAL(opline)));
	}
	return 1;
}

int gear_optimizer_update_op2_const(gear_op_array *op_array,
                                    gear_op       *opline,
                                    zval          *val)
{
	zval tmp;

	switch (opline->opcode) {
		case GEAR_ASSIGN_REF:
		case GEAR_FAST_CALL:
			return 0;
		case GEAR_FETCH_CLASS:
			if ((opline + 1)->opcode == GEAR_INSTANCEOF &&
				(opline + 1)->op2.var == opline->result.var) {
				return 0;
			}
		case GEAR_ADD_INTERFACE:
		case GEAR_ADD_TRAIT:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			break;
		case GEAR_INSTANCEOF:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			opline->extended_value = alloc_cache_slots(op_array, 1);
			break;
		case GEAR_INIT_FCALL_BY_NAME:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			opline->result.num = alloc_cache_slots(op_array, 1);
			break;
		case GEAR_FETCH_STATIC_PROP_R:
		case GEAR_FETCH_STATIC_PROP_W:
		case GEAR_FETCH_STATIC_PROP_RW:
		case GEAR_FETCH_STATIC_PROP_IS:
		case GEAR_FETCH_STATIC_PROP_UNSET:
		case GEAR_FETCH_STATIC_PROP_FUNC_ARG:
		case GEAR_UNSET_STATIC_PROP:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			if (opline->op1_type != IS_CONST) {
				opline->extended_value = alloc_cache_slots(op_array, 1);
			}
			break;
		case GEAR_ISSET_ISEMPTY_STATIC_PROP:
			REQUIRES_STRING(val);
			drop_leading_backslash(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			if (opline->op1_type != IS_CONST) {
				opline->extended_value = alloc_cache_slots(op_array, 1) | (opline->extended_value & GEAR_ISEMPTY);
			}
			break;
		case GEAR_INIT_FCALL:
			REQUIRES_STRING(val);
			if (Z_REFCOUNT_P(val) == 1) {
				gear_str_tolower(Z_STRVAL_P(val), Z_STRLEN_P(val));
			} else {
				ZVAL_STR(&tmp, gear_string_tolower(Z_STR_P(val)));
				zval_ptr_dtor_nogc(val);
				val = &tmp;
			}
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			opline->result.num = alloc_cache_slots(op_array, 1);
			break;
		case GEAR_INIT_DYNAMIC_CALL:
			if (Z_TYPE_P(val) == IS_STRING) {
				if (gear_memrchr(Z_STRVAL_P(val), ':', Z_STRLEN_P(val))) {
					return 0;
				}

				if (gear_optimizer_classify_function(Z_STR_P(val), opline->extended_value)) {
					/* Dynamic call to various special functions must stay dynamic,
					 * otherwise would drop a warning */
					return 0;
				}

				opline->opcode = GEAR_INIT_FCALL_BY_NAME;
				drop_leading_backslash(val);
				opline->op2.constant = gear_optimizer_add_literal(op_array, val);
				gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
				opline->result.num = alloc_cache_slots(op_array, 1);
			} else {
				opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			}
			break;
		case GEAR_INIT_METHOD_CALL:
			REQUIRES_STRING(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			opline->result.num = alloc_cache_slots(op_array, 2);
			break;
		case GEAR_INIT_STATIC_METHOD_CALL:
			REQUIRES_STRING(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			gear_optimizer_add_literal_string(op_array, gear_string_tolower(Z_STR_P(val)));
			if (opline->op1_type != IS_CONST) {
				opline->result.num = alloc_cache_slots(op_array, 2);
			}
			break;
		case GEAR_ASSIGN_OBJ:
		case GEAR_FETCH_OBJ_R:
		case GEAR_FETCH_OBJ_W:
		case GEAR_FETCH_OBJ_RW:
		case GEAR_FETCH_OBJ_IS:
		case GEAR_FETCH_OBJ_UNSET:
		case GEAR_FETCH_OBJ_FUNC_ARG:
		case GEAR_UNSET_OBJ:
		case GEAR_PRE_INC_OBJ:
		case GEAR_PRE_DEC_OBJ:
		case GEAR_POST_INC_OBJ:
		case GEAR_POST_DEC_OBJ:
			TO_STRING_NOWARN(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			opline->extended_value = alloc_cache_slots(op_array, 2);
			break;
		case GEAR_ISSET_ISEMPTY_PROP_OBJ:
			TO_STRING_NOWARN(val);
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			opline->extended_value = alloc_cache_slots(op_array, 2) | (opline->extended_value & GEAR_ISEMPTY);
			break;
		case GEAR_ASSIGN_ADD:
		case GEAR_ASSIGN_SUB:
		case GEAR_ASSIGN_MUL:
		case GEAR_ASSIGN_DIV:
		case GEAR_ASSIGN_POW:
		case GEAR_ASSIGN_MOD:
		case GEAR_ASSIGN_SL:
		case GEAR_ASSIGN_SR:
		case GEAR_ASSIGN_CONCAT:
		case GEAR_ASSIGN_BW_OR:
		case GEAR_ASSIGN_BW_AND:
		case GEAR_ASSIGN_BW_XOR:
			if (opline->extended_value == GEAR_ASSIGN_OBJ) {
				TO_STRING_NOWARN(val);
				opline->op2.constant = gear_optimizer_add_literal(op_array, val);
				(opline+1)->extended_value = alloc_cache_slots(op_array, 2);
			} else if (opline->extended_value == GEAR_ASSIGN_DIM) {
				if (Z_TYPE_P(val) == IS_STRING) {
					gear_ulong index;

					if (GEAR_HANDLE_NUMERIC(Z_STR_P(val), index)) {
						ZVAL_LONG(&tmp, index);
						opline->op2.constant = gear_optimizer_add_literal(op_array, &tmp);
						gear_string_hash_val(Z_STR_P(val));
						gear_optimizer_add_literal(op_array, val);
						Z_EXTRA(op_array->literals[opline->op2.constant]) = GEAR_EXTRA_VALUE;
						break;
					}
				}
				opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			} else {
				opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			}
			break;
		case GEAR_ISSET_ISEMPTY_DIM_OBJ:
		case GEAR_ASSIGN_DIM:
		case GEAR_UNSET_DIM:
		case GEAR_FETCH_DIM_R:
		case GEAR_FETCH_DIM_W:
		case GEAR_FETCH_DIM_RW:
		case GEAR_FETCH_DIM_IS:
		case GEAR_FETCH_DIM_FUNC_ARG:
		case GEAR_FETCH_DIM_UNSET:
		case GEAR_FETCH_LIST_R:
		case GEAR_FETCH_LIST_W:
			if (Z_TYPE_P(val) == IS_STRING) {
				gear_ulong index;

				if (GEAR_HANDLE_NUMERIC(Z_STR_P(val), index)) {
					ZVAL_LONG(&tmp, index);
					opline->op2.constant = gear_optimizer_add_literal(op_array, &tmp);
					gear_string_hash_val(Z_STR_P(val));
					gear_optimizer_add_literal(op_array, val);
					Z_EXTRA(op_array->literals[opline->op2.constant]) = GEAR_EXTRA_VALUE;
					break;
				}
			}
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			break;
		case GEAR_ADD_ARRAY_ELEMENT:
		case GEAR_INIT_ARRAY:
			if (Z_TYPE_P(val) == IS_STRING) {
				gear_ulong index;
				if (GEAR_HANDLE_NUMERIC(Z_STR_P(val), index)) {
					zval_ptr_dtor_nogc(val);
					ZVAL_LONG(val, index);
				}
			}
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			break;
		case GEAR_ROPE_INIT:
		case GEAR_ROPE_ADD:
		case GEAR_ROPE_END:
		case GEAR_CONCAT:
		case GEAR_FAST_CONCAT:
			TO_STRING_NOWARN(val);
			if (opline->opcode == GEAR_CONCAT && opline->op1_type == IS_CONST) {
				opline->opcode = GEAR_FAST_CONCAT;
			}
			/* break missing intentionally */
		default:
			opline->op2.constant = gear_optimizer_add_literal(op_array, val);
			break;
	}

	opline->op2_type = IS_CONST;
	if (Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_STRING) {
		gear_string_hash_val(Z_STR(GEAR_OP2_LITERAL(opline)));
	}
	return 1;
}

void gear_optimizer_remove_live_range(gear_op_array *op_array, uint32_t var)
{
	if (op_array->last_live_range) {
		int i = 0;
		int j = 0;

		do {
			if ((op_array->live_range[i].var & ~GEAR_LIVE_MASK) != var) {
				if (i != j) {
					op_array->live_range[j] = op_array->live_range[i];
				}
				j++;
			}
			i++;
		} while (i < op_array->last_live_range);
		if (i != j) {
			op_array->last_live_range = j;
			if (j == 0) {
				efree(op_array->live_range);
				op_array->live_range = NULL;
			}
		}
	}
}

static uint32_t gear_determine_constructor_call(gear_op_array *op_array, uint32_t start) {
	int call = 0;
	while (++start < op_array->last) {
		switch (op_array->opcodes[start].opcode) {
			case GEAR_INIT_FCALL_BY_NAME:
			case GEAR_INIT_NS_FCALL_BY_NAME:
			case GEAR_INIT_STATIC_METHOD_CALL:
			case GEAR_INIT_METHOD_CALL:
			case GEAR_INIT_FCALL:
			case GEAR_NEW:
			case GEAR_INIT_DYNAMIC_CALL:
			case GEAR_INIT_USER_CALL:
				call++;
				break;
			case GEAR_DO_FCALL:
				if (call == 0) {
					return start;
				}
			/* break missing intentionally */
			case GEAR_DO_ICALL:
			case GEAR_DO_UCALL:
			case GEAR_DO_FCALL_BY_NAME:
				call--;
				break;
			default:
				break;
		}
	}

	GEAR_ASSERT(0);
	return -1;
}

void gear_optimizer_remove_live_range_ex(gear_op_array *op_array, uint32_t var, uint32_t start)
{
	uint32_t i = 0;

	switch (op_array->opcodes[start].opcode) {
		case GEAR_ROPE_ADD:
		case GEAR_ADD_ARRAY_ELEMENT:
			return;
		case GEAR_ROPE_INIT:
			var |= GEAR_LIVE_ROPE;
			break;
		case GEAR_BEGIN_SILENCE:
			var |= GEAR_LIVE_SILENCE;
			break;
		case GEAR_FE_RESET_R:
		case GEAR_FE_RESET_RW:
			var |= GEAR_LIVE_LOOP;
			start++;
			break;
		case GEAR_NEW:
			start = gear_determine_constructor_call(op_array, start);
			start++;
			break;
		default:
			start++;
	}

	while (i < op_array->last_live_range) {
		if (op_array->live_range[i].var == var
				&& op_array->live_range[i].start == start) {
			op_array->last_live_range--;
			if (i < op_array->last_live_range) {
				memmove(&op_array->live_range[i], &op_array->live_range[i+1], (op_array->last_live_range - i) * sizeof(gear_live_range));
			}
			break;
		}
		i++;
	}
}

int gear_optimizer_replace_by_const(gear_op_array *op_array,
                                    gear_op       *opline,
                                    gear_uchar     type,
                                    uint32_t       var,
                                    zval          *val)
{
	gear_op *end = op_array->opcodes + op_array->last;

	while (opline < end) {
		if (opline->op1_type == type &&
			opline->op1.var == var) {
			switch (opline->opcode) {
				case GEAR_FETCH_DIM_W:
				case GEAR_FETCH_DIM_RW:
				case GEAR_FETCH_DIM_FUNC_ARG:
				case GEAR_FETCH_DIM_UNSET:
				case GEAR_FETCH_LIST_W:
				case GEAR_ASSIGN_DIM:
				case GEAR_SEPARATE:
				case GEAR_RETURN_BY_REF:
					return 0;
				case GEAR_SEND_VAR:
					opline->extended_value = 0;
					opline->opcode = GEAR_SEND_VAL;
					break;
				case GEAR_SEND_VAR_EX:
				case GEAR_SEND_FUNC_ARG:
					opline->extended_value = 0;
					opline->opcode = GEAR_SEND_VAL_EX;
					break;
				case GEAR_SEND_VAR_NO_REF:
					return 0;
				case GEAR_SEND_VAR_NO_REF_EX:
					opline->opcode = GEAR_SEND_VAL;
					break;
				case GEAR_SEND_USER:
					opline->opcode = GEAR_SEND_VAL_EX;
					break;
				/* In most cases IS_TMP_VAR operand may be used only once.
				 * The operands are usually destroyed by the opcode handler.
				 * GEAR_CASE and GEAR_FETCH_LIST_R are exceptions, they keeps operand
				 * unchanged, and allows its reuse. these instructions
				 * usually terminated by GEAR_FREE that finally kills the value.
				 */
				case GEAR_FETCH_LIST_R: {
					gear_op *m = opline;

					do {
						if (m->opcode == GEAR_FETCH_LIST_R &&
							m->op1_type == type &&
							m->op1.var == var) {
							zval v;
							ZVAL_COPY(&v, val);
							if (Z_TYPE(v) == IS_STRING) {
								gear_string_hash_val(Z_STR(v));
							}
							m->op1.constant = gear_optimizer_add_literal(op_array, &v);
							m->op1_type = IS_CONST;
						}
						m++;
					} while (m->opcode != GEAR_FREE || m->op1_type != type || m->op1.var != var);

					GEAR_ASSERT(m->opcode == GEAR_FREE && m->op1_type == type && m->op1.var == var);
					MAKE_NOP(m);
					zval_ptr_dtor_nogc(val);
					gear_optimizer_remove_live_range(op_array, var);
					return 1;
				}
				case GEAR_SWITCH_LONG:
				case GEAR_SWITCH_STRING:
				case GEAR_CASE:
				case GEAR_FREE: {
					gear_op *m, *n;
					int brk = op_array->last_live_range;
					gear_bool in_switch = 0;
					while (brk--) {
						if (op_array->live_range[brk].start <= (uint32_t)(opline - op_array->opcodes) &&
						    op_array->live_range[brk].end > (uint32_t)(opline - op_array->opcodes)) {
							in_switch = 1;
							break;
						}
					}

					if (!in_switch) {
						GEAR_ASSERT(opline->opcode == GEAR_FREE);
						MAKE_NOP(opline);
						zval_ptr_dtor_nogc(val);
						return 1;
					}

					m = opline;
					n = op_array->opcodes + op_array->live_range[brk].end;
					if (n->opcode == GEAR_FREE &&
					    !(n->extended_value & GEAR_FREE_ON_RETURN)) {
						n++;
					} else {
						n = op_array->opcodes + op_array->last;
					}

					while (m < n) {
						if (m->op1_type == type &&
								m->op1.var == var) {
							if (m->opcode == GEAR_CASE
									|| m->opcode == GEAR_SWITCH_LONG
									|| m->opcode == GEAR_SWITCH_STRING) {
								zval v;

								if (m->opcode == GEAR_CASE) {
									m->opcode = GEAR_IS_EQUAL;
								}
								ZVAL_COPY(&v, val);
								if (Z_TYPE(v) == IS_STRING) {
									gear_string_hash_val(Z_STR(v));
								}
								m->op1.constant = gear_optimizer_add_literal(op_array, &v);
								m->op1_type = IS_CONST;
							} else if (m->opcode == GEAR_FREE) {
								MAKE_NOP(m);
							} else {
								GEAR_ASSERT(0);
							}
						}
						m++;
					}
					zval_ptr_dtor_nogc(val);
					gear_optimizer_remove_live_range(op_array, var);
					return 1;
				}
				case GEAR_VERIFY_RETURN_TYPE: {
					gear_arg_info *ret_info = op_array->arg_info - 1;
					if (GEAR_TYPE_IS_CLASS(ret_info->type)
						|| GEAR_TYPE_CODE(ret_info->type) == IS_CALLABLE
						|| !GEAR_SAME_FAKE_TYPE(GEAR_TYPE_CODE(ret_info->type), Z_TYPE_P(val))
						|| (op_array->fn_flags & GEAR_ACC_RETURN_REFERENCE)) {
						return 0;
					}
					MAKE_NOP(opline);

					/* gear_handle_loops_and_finally may inserts other oplines */
					do {
						++opline;
					} while (opline->opcode != GEAR_RETURN && opline->opcode != GEAR_RETURN_BY_REF);
					GEAR_ASSERT(opline->op1.var == var);

					break;
				  }
				default:
					break;
			}
			if (gear_optimizer_update_op1_const(op_array, opline, val)) {
				gear_optimizer_remove_live_range(op_array, var);
				return 1;
			}
			return 0;
		}

		if (opline->op2_type == type &&
			opline->op2.var == var) {
			if (gear_optimizer_update_op2_const(op_array, opline, val)) {
				gear_optimizer_remove_live_range(op_array, var);
				return 1;
			}
			return 0;
		}
		opline++;
	}

	return 1;
}

/* Update jump offsets after a jump was migrated to another opline */
void gear_optimizer_migrate_jump(gear_op_array *op_array, gear_op *new_opline, gear_op *opline) {
	switch (new_opline->opcode) {
		case GEAR_JMP:
		case GEAR_FAST_CALL:
			GEAR_SET_OP_JMP_ADDR(new_opline, new_opline->op1, GEAR_OP1_JMP_ADDR(opline));
			break;
		case GEAR_JMPZNZ:
			new_opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, new_opline, GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value));
			/* break missing intentionally */
		case GEAR_JMPZ:
		case GEAR_JMPNZ:
		case GEAR_JMPZ_EX:
		case GEAR_JMPNZ_EX:
		case GEAR_FE_RESET_R:
		case GEAR_FE_RESET_RW:
		case GEAR_JMP_SET:
		case GEAR_COALESCE:
		case GEAR_ASSERT_CHECK:
			GEAR_SET_OP_JMP_ADDR(new_opline, new_opline->op2, GEAR_OP2_JMP_ADDR(opline));
			break;
		case GEAR_DECLARE_ANON_CLASS:
		case GEAR_DECLARE_ANON_INHERITED_CLASS:
		case GEAR_FE_FETCH_R:
		case GEAR_FE_FETCH_RW:
			new_opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, new_opline, GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value));
			break;
		case GEAR_CATCH:
			if (!(opline->extended_value & GEAR_LAST_CATCH)) {
				GEAR_SET_OP_JMP_ADDR(new_opline, new_opline->op2, GEAR_OP2_JMP_ADDR(opline));
			}
			break;
		case GEAR_SWITCH_LONG:
		case GEAR_SWITCH_STRING:
		{
			HashTable *jumptable = Z_ARRVAL(GEAR_OP2_LITERAL(opline));
			zval *zv;
			GEAR_HASH_FOREACH_VAL(jumptable, zv) {
				Z_LVAL_P(zv) = GEAR_OPLINE_NUM_TO_OFFSET(op_array, new_opline, GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, Z_LVAL_P(zv)));
			} GEAR_HASH_FOREACH_END();
			new_opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, new_opline, GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value));
			break;
		}
	}
}

/* Shift jump offsets based on shiftlist */
void gear_optimizer_shift_jump(gear_op_array *op_array, gear_op *opline, uint32_t *shiftlist) {
	switch (opline->opcode) {
		case GEAR_JMP:
		case GEAR_FAST_CALL:
			GEAR_SET_OP_JMP_ADDR(opline, opline->op1, GEAR_OP1_JMP_ADDR(opline) - shiftlist[GEAR_OP1_JMP_ADDR(opline) - op_array->opcodes]);
			break;
		case GEAR_JMPZNZ:
			opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value) - shiftlist[GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value)]);
			/* break missing intentionally */
		case GEAR_JMPZ:
		case GEAR_JMPNZ:
		case GEAR_JMPZ_EX:
		case GEAR_JMPNZ_EX:
		case GEAR_FE_RESET_R:
		case GEAR_FE_RESET_RW:
		case GEAR_JMP_SET:
		case GEAR_COALESCE:
		case GEAR_ASSERT_CHECK:
			GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP2_JMP_ADDR(opline) - shiftlist[GEAR_OP2_JMP_ADDR(opline) - op_array->opcodes]);
			break;
		case GEAR_CATCH:
			if (!(opline->extended_value & GEAR_LAST_CATCH)) {
				GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP2_JMP_ADDR(opline) - shiftlist[GEAR_OP2_JMP_ADDR(opline) - op_array->opcodes]);
			}
			break;
		case GEAR_DECLARE_ANON_CLASS:
		case GEAR_DECLARE_ANON_INHERITED_CLASS:
		case GEAR_FE_FETCH_R:
		case GEAR_FE_FETCH_RW:
			opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value) - shiftlist[GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value)]);
			break;
		case GEAR_SWITCH_LONG:
		case GEAR_SWITCH_STRING:
		{
			HashTable *jumptable = Z_ARRVAL(GEAR_OP2_LITERAL(opline));
			zval *zv;
			GEAR_HASH_FOREACH_VAL(jumptable, zv) {
				Z_LVAL_P(zv) = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, Z_LVAL_P(zv)) - shiftlist[GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, Z_LVAL_P(zv))]);
			} GEAR_HASH_FOREACH_END();
			opline->extended_value = GEAR_OPLINE_NUM_TO_OFFSET(op_array, opline, GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value) - shiftlist[GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value)]);
			break;
		}
	}
}

static gear_class_entry *get_class_entry_from_op1(
		gear_script *script, gear_op_array *op_array, gear_op *opline, gear_bool rt_constants) {
	if (opline->op1_type == IS_CONST) {
		zval *op1 = CRT_CONSTANT_EX(op_array, opline, opline->op1, rt_constants);
		if (Z_TYPE_P(op1) == IS_STRING) {
			gear_string *class_name = Z_STR_P(op1 + 1);
			gear_class_entry *ce;
			if (script && (ce = gear_hash_find_ptr(&script->class_table, class_name))) {
				return ce;
			} else if ((ce = gear_hash_find_ptr(EG(class_table), class_name))) {
				if (ce->type == GEAR_INTERNAL_CLASS) {
					return ce;
				} else if (ce->type == GEAR_USER_CLASS &&
						   ce->info.user.filename &&
						   ce->info.user.filename == op_array->filename) {
					return ce;
				}
			}
		}
	} else if (opline->op1_type == IS_UNUSED && op_array->scope
			&& !(op_array->scope->ce_flags & GEAR_ACC_TRAIT)
			&& (opline->op1.num & GEAR_FETCH_CLASS_MASK) == GEAR_FETCH_CLASS_SELF) {
		return op_array->scope;
	}
	return NULL;
}

gear_function *gear_optimizer_get_called_func(
		gear_script *script, gear_op_array *op_array, gear_op *opline, gear_bool rt_constants)
{
#define GET_OP(op) CRT_CONSTANT_EX(op_array, opline, opline->op, rt_constants)
	switch (opline->opcode) {
		case GEAR_INIT_FCALL:
		{
			gear_string *function_name = Z_STR_P(GET_OP(op2));
			gear_function *func;
			if (script && (func = gear_hash_find_ptr(&script->function_table, function_name)) != NULL) {
				return func;
			} else if ((func = gear_hash_find_ptr(EG(function_table), function_name)) != NULL) {
				if (func->type == GEAR_INTERNAL_FUNCTION) {
					return func;
				} else if (func->type == GEAR_USER_FUNCTION &&
				           func->op_array.filename &&
				           func->op_array.filename == op_array->filename) {
					return func;
				}
			}
			break;
		}
		case GEAR_INIT_FCALL_BY_NAME:
		case GEAR_INIT_NS_FCALL_BY_NAME:
			if (opline->op2_type == IS_CONST && Z_TYPE_P(GET_OP(op2)) == IS_STRING) {
				zval *function_name = GET_OP(op2) + 1;
				gear_function *func;
				if (script && (func = gear_hash_find_ptr(&script->function_table, Z_STR_P(function_name)))) {
					return func;
				} else if ((func = gear_hash_find_ptr(EG(function_table), Z_STR_P(function_name))) != NULL) {
					if (func->type == GEAR_INTERNAL_FUNCTION) {
						return func;
					} else if (func->type == GEAR_USER_FUNCTION &&
					           func->op_array.filename &&
					           func->op_array.filename == op_array->filename) {
						return func;
					}
				}
			}
			break;
		case GEAR_INIT_STATIC_METHOD_CALL:
			if (opline->op2_type == IS_CONST && Z_TYPE_P(GET_OP(op2)) == IS_STRING) {
				gear_class_entry *ce = get_class_entry_from_op1(
					script, op_array, opline, rt_constants);
				if (ce) {
					gear_string *func_name = Z_STR_P(GET_OP(op2) + 1);
					return gear_hash_find_ptr(&ce->function_table, func_name);
				}
			}
			break;
		case GEAR_INIT_METHOD_CALL:
			if (opline->op1_type == IS_UNUSED
					&& opline->op2_type == IS_CONST && Z_TYPE_P(GET_OP(op2)) == IS_STRING
					&& op_array->scope && !(op_array->scope->ce_flags & GEAR_ACC_TRAIT)) {
				gear_string *method_name = Z_STR_P(GET_OP(op2) + 1);
				gear_function *fbc = gear_hash_find_ptr(
					&op_array->scope->function_table, method_name);
				if (fbc) {
					gear_bool is_private = (fbc->common.fn_flags & GEAR_ACC_PRIVATE) != 0;
					gear_bool is_final = (fbc->common.fn_flags & GEAR_ACC_FINAL) != 0;
					gear_bool same_scope = fbc->common.scope == op_array->scope;
					if ((is_private && same_scope)
							|| (is_final && (!is_private || same_scope))) {
						return fbc;
					}
				}
			}
			break;
		case GEAR_NEW:
		{
			gear_class_entry *ce = get_class_entry_from_op1(
				script, op_array, opline, rt_constants);
			if (ce && ce->type == GEAR_USER_CLASS) {
				return ce->constructor;
			}
			break;
		}
	}
	return NULL;
#undef GET_OP
}

uint32_t gear_optimizer_classify_function(gear_string *name, uint32_t num_args) {
	if (gear_string_equals_literal(name, "extract")) {
		return GEAR_FUNC_INDIRECT_VAR_ACCESS;
	} else if (gear_string_equals_literal(name, "compact")) {
		return GEAR_FUNC_INDIRECT_VAR_ACCESS;
	} else if (gear_string_equals_literal(name, "parse_str") && num_args <= 1) {
		return GEAR_FUNC_INDIRECT_VAR_ACCESS;
	} else if (gear_string_equals_literal(name, "mb_parse_str") && num_args <= 1) {
		return GEAR_FUNC_INDIRECT_VAR_ACCESS;
	} else if (gear_string_equals_literal(name, "get_defined_vars")) {
		return GEAR_FUNC_INDIRECT_VAR_ACCESS;
	} else if (gear_string_equals_literal(name, "assert")) {
		return GEAR_FUNC_INDIRECT_VAR_ACCESS;
	} else if (gear_string_equals_literal(name, "func_num_args")) {
		return GEAR_FUNC_VARARG;
	} else if (gear_string_equals_literal(name, "func_get_arg")) {
		return GEAR_FUNC_VARARG;
	} else if (gear_string_equals_literal(name, "func_get_args")) {
		return GEAR_FUNC_VARARG;
	} else {
		return 0;
	}
}

static void gear_optimize(gear_op_array      *op_array,
                          gear_optimizer_ctx *ctx)
{
	if (op_array->type == GEAR_EVAL_CODE) {
		return;
	}

	if (ctx->debug_level & GEAR_DUMP_BEFORE_OPTIMIZER) {
		gear_dump_op_array(op_array, 0, "before optimizer", NULL);
	}

	/* pass 1
	 * - substitute persistent constants (true, false, null, etc)
	 * - perform compile-time evaluation of constant binary and unary operations
	 * - optimize series of ADD_STRING and/or ADD_CHAR
	 * - convert CAST(IS_BOOL,x) into BOOL(x)
         * - pre-evaluate constant function calls
	 */
	if (GEAR_OPTIMIZER_PASS_1 & ctx->optimization_level) {
		gear_optimizer_pass1(op_array, ctx);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_1) {
			gear_dump_op_array(op_array, 0, "after pass 1", NULL);
		}
	}

	/* pass 2:
	 * - convert non-numeric constants to numeric constants in numeric operators
	 * - optimize constant conditional JMPs
	 */
	if (GEAR_OPTIMIZER_PASS_2 & ctx->optimization_level) {
		gear_optimizer_pass2(op_array);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_2) {
			gear_dump_op_array(op_array, 0, "after pass 2", NULL);
		}
	}

	/* pass 3:
	 * - optimize $i = $i+expr to $i+=expr
	 * - optimize series of JMPs
	 * - change $i++ to ++$i where possible
	 */
	if (GEAR_OPTIMIZER_PASS_3 & ctx->optimization_level) {
		gear_optimizer_pass3(op_array, ctx);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_3) {
			gear_dump_op_array(op_array, 0, "after pass 3", NULL);
		}
	}

	/* pass 4:
	 * - INIT_FCALL_BY_NAME -> DO_FCALL
	 */
	if (GEAR_OPTIMIZER_PASS_4 & ctx->optimization_level) {
		gear_optimize_func_calls(op_array, ctx);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_4) {
			gear_dump_op_array(op_array, 0, "after pass 4", NULL);
		}
	}

	/* pass 5:
	 * - CFG optimization
	 */
	if (GEAR_OPTIMIZER_PASS_5 & ctx->optimization_level) {
		gear_optimize_cfg(op_array, ctx);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_5) {
			gear_dump_op_array(op_array, 0, "after pass 5", NULL);
		}
	}

#if HAVE_DFA_PASS
	/* pass 6:
	 * - DFA optimization
	 */
	if ((GEAR_OPTIMIZER_PASS_6 & ctx->optimization_level) &&
	    !(GEAR_OPTIMIZER_PASS_7 & ctx->optimization_level)) {
		gear_optimize_dfa(op_array, ctx);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_6) {
			gear_dump_op_array(op_array, 0, "after pass 6", NULL);
		}
	}
#endif

	/* pass 9:
	 * - Optimize temp variables usage
	 */
	if (GEAR_OPTIMIZER_PASS_9 & ctx->optimization_level) {
		gear_optimize_temporary_variables(op_array, ctx);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_9) {
			gear_dump_op_array(op_array, 0, "after pass 9", NULL);
		}
	}

	/* pass 10:
	 * - remove NOPs
	 */
	if (((GEAR_OPTIMIZER_PASS_10|GEAR_OPTIMIZER_PASS_5) & ctx->optimization_level) == GEAR_OPTIMIZER_PASS_10) {
		gear_optimizer_nop_removal(op_array, ctx);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_10) {
			gear_dump_op_array(op_array, 0, "after pass 10", NULL);
		}
	}

	/* pass 11:
	 * - Compact literals table
	 */
	if ((GEAR_OPTIMIZER_PASS_11 & ctx->optimization_level) &&
	    (!(GEAR_OPTIMIZER_PASS_6 & ctx->optimization_level) ||
	     !(GEAR_OPTIMIZER_PASS_7 & ctx->optimization_level))) {
		gear_optimizer_compact_literals(op_array, ctx);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_11) {
			gear_dump_op_array(op_array, 0, "after pass 11", NULL);
		}
	}

	if ((GEAR_OPTIMIZER_PASS_13 & ctx->optimization_level) &&
	    (!(GEAR_OPTIMIZER_PASS_6 & ctx->optimization_level) ||
	     !(GEAR_OPTIMIZER_PASS_7 & ctx->optimization_level))) {
		gear_optimizer_compact_vars(op_array);
		if (ctx->debug_level & GEAR_DUMP_AFTER_PASS_13) {
			gear_dump_op_array(op_array, 0, "after pass 13", NULL);
		}
	}

	if (GEAR_OPTIMIZER_PASS_7 & ctx->optimization_level) {
		return;
	}

	if (ctx->debug_level & GEAR_DUMP_AFTER_OPTIMIZER) {
		gear_dump_op_array(op_array, 0, "after optimizer", NULL);
	}
}

static void gear_revert_pass_two(gear_op_array *op_array)
{
	gear_op *opline, *end;

	opline = op_array->opcodes;
	end = opline + op_array->last;
	while (opline < end) {
		if (opline->op1_type == IS_CONST) {
			GEAR_PASS_TWO_UNDO_CONSTANT(op_array, opline, opline->op1);
		}
		if (opline->op2_type == IS_CONST) {
			GEAR_PASS_TWO_UNDO_CONSTANT(op_array, opline, opline->op2);
		}
		opline++;
	}
#if !GEAR_USE_ABS_CONST_ADDR
	if (op_array->literals) {
		zval *literals = emalloc(sizeof(zval) * op_array->last_literal);
		memcpy(literals, op_array->literals, sizeof(zval) * op_array->last_literal);
		op_array->literals = literals;
	}
#endif
}

static void gear_redo_pass_two(gear_op_array *op_array)
{
	gear_op *opline, *end;
#if GEAR_USE_ABS_JMP_ADDR && !GEAR_USE_ABS_CONST_ADDR
	gear_op *old_opcodes = op_array->opcodes;
#endif

#if !GEAR_USE_ABS_CONST_ADDR
	if (op_array->last_literal) {
		op_array->opcodes = (gear_op *) erealloc(op_array->opcodes,
			GEAR_MM_ALIGNED_SIZE_EX(sizeof(gear_op) * op_array->last, 16) +
			sizeof(zval) * op_array->last_literal);
		memcpy(((char*)op_array->opcodes) + GEAR_MM_ALIGNED_SIZE_EX(sizeof(gear_op) * op_array->last, 16),
			op_array->literals, sizeof(zval) * op_array->last_literal);
		efree(op_array->literals);
		op_array->literals = (zval*)(((char*)op_array->opcodes) + GEAR_MM_ALIGNED_SIZE_EX(sizeof(gear_op) * op_array->last, 16));
	} else {
		if (op_array->literals) {
			efree(op_array->literals);
		}
		op_array->literals = NULL;
	}
#endif

	opline = op_array->opcodes;
	end = opline + op_array->last;
	while (opline < end) {
		if (opline->op1_type == IS_CONST) {
			GEAR_PASS_TWO_UPDATE_CONSTANT(op_array, opline, opline->op1);
		}
		if (opline->op2_type == IS_CONST) {
			GEAR_PASS_TWO_UPDATE_CONSTANT(op_array, opline, opline->op2);
		}
#if GEAR_USE_ABS_JMP_ADDR && !GEAR_USE_ABS_CONST_ADDR
		if (op_array->fn_flags & GEAR_ACC_DONE_PASS_TWO) {
			/* fix jumps to point to new array */
			switch (opline->opcode) {
				case GEAR_JMP:
				case GEAR_FAST_CALL:
					opline->op1.jmp_addr = &op_array->opcodes[opline->op1.jmp_addr - old_opcodes];
					break;
				case GEAR_JMPZNZ:
					/* relative extended_value don't have to be changed */
					/* break omitted intentionally */
				case GEAR_JMPZ:
				case GEAR_JMPNZ:
				case GEAR_JMPZ_EX:
				case GEAR_JMPNZ_EX:
				case GEAR_JMP_SET:
				case GEAR_COALESCE:
				case GEAR_FE_RESET_R:
				case GEAR_FE_RESET_RW:
				case GEAR_ASSERT_CHECK:
					opline->op2.jmp_addr = &op_array->opcodes[opline->op2.jmp_addr - old_opcodes];
					break;
				case GEAR_CATCH:
					if (!(opline->extended_value & GEAR_LAST_CATCH)) {
						opline->op2.jmp_addr = &op_array->opcodes[opline->op2.jmp_addr - old_opcodes];
					}
					break;
				case GEAR_DECLARE_ANON_CLASS:
				case GEAR_DECLARE_ANON_INHERITED_CLASS:
				case GEAR_FE_FETCH_R:
				case GEAR_FE_FETCH_RW:
				case GEAR_SWITCH_LONG:
				case GEAR_SWITCH_STRING:
					/* relative extended_value don't have to be changed */
					break;
			}
		}
#endif
		GEAR_VM_SET_OPCODE_HANDLER(opline);
		opline++;
	}
}

#if HAVE_DFA_PASS
static void gear_redo_pass_two_ex(gear_op_array *op_array, gear_ssa *ssa)
{
	gear_op *opline, *end;
#if GEAR_USE_ABS_JMP_ADDR && !GEAR_USE_ABS_CONST_ADDR
	gear_op *old_opcodes = op_array->opcodes;
#endif

#if !GEAR_USE_ABS_CONST_ADDR
	if (op_array->last_literal) {
		op_array->opcodes = (gear_op *) erealloc(op_array->opcodes,
			GEAR_MM_ALIGNED_SIZE_EX(sizeof(gear_op) * op_array->last, 16) +
			sizeof(zval) * op_array->last_literal);
		memcpy(((char*)op_array->opcodes) + GEAR_MM_ALIGNED_SIZE_EX(sizeof(gear_op) * op_array->last, 16),
			op_array->literals, sizeof(zval) * op_array->last_literal);
		efree(op_array->literals);
		op_array->literals = (zval*)(((char*)op_array->opcodes) + GEAR_MM_ALIGNED_SIZE_EX(sizeof(gear_op) * op_array->last, 16));
	} else {
		if (op_array->literals) {
			efree(op_array->literals);
		}
		op_array->literals = NULL;
	}
#endif

	opline = op_array->opcodes;
	end = opline + op_array->last;
	while (opline < end) {
		uint32_t op1_info = opline->op1_type == IS_UNUSED ? 0 : (OP1_INFO() & (MAY_BE_UNDEF|MAY_BE_ANY|MAY_BE_REF|MAY_BE_ARRAY_OF_ANY|MAY_BE_ARRAY_KEY_ANY));
		uint32_t op2_info = opline->op1_type == IS_UNUSED ? 0 : (OP2_INFO() & (MAY_BE_UNDEF|MAY_BE_ANY|MAY_BE_REF|MAY_BE_ARRAY_OF_ANY|MAY_BE_ARRAY_KEY_ANY));
		uint32_t res_info =
			(opline->opcode == GEAR_PRE_INC ||
			 opline->opcode == GEAR_PRE_DEC ||
			 opline->opcode == GEAR_POST_INC ||
			 opline->opcode == GEAR_POST_DEC) ?
				((ssa->ops[opline - op_array->opcodes].op1_def >= 0) ? (OP1_DEF_INFO() & (MAY_BE_UNDEF|MAY_BE_ANY|MAY_BE_REF|MAY_BE_ARRAY_OF_ANY|MAY_BE_ARRAY_KEY_ANY)) : MAY_BE_ANY) :
				(opline->result_type == IS_UNUSED ? 0 : (RES_INFO() & (MAY_BE_UNDEF|MAY_BE_ANY|MAY_BE_REF|MAY_BE_ARRAY_OF_ANY|MAY_BE_ARRAY_KEY_ANY)));

		if (opline->op1_type == IS_CONST) {
			GEAR_PASS_TWO_UPDATE_CONSTANT(op_array, opline, opline->op1);
		}
		if (opline->op2_type == IS_CONST) {
			GEAR_PASS_TWO_UPDATE_CONSTANT(op_array, opline, opline->op2);
		}

		gear_vm_set_opcode_handler_ex(opline, op1_info, op2_info, res_info);
#if GEAR_USE_ABS_JMP_ADDR && !GEAR_USE_ABS_CONST_ADDR
		if (op_array->fn_flags & GEAR_ACC_DONE_PASS_TWO) {
			/* fix jumps to point to new array */
			switch (opline->opcode) {
				case GEAR_JMP:
				case GEAR_FAST_CALL:
					opline->op1.jmp_addr = &op_array->opcodes[opline->op1.jmp_addr - old_opcodes];
					break;
				case GEAR_JMPZNZ:
					/* relative extended_value don't have to be changed */
					/* break omitted intentionally */
				case GEAR_JMPZ:
				case GEAR_JMPNZ:
				case GEAR_JMPZ_EX:
				case GEAR_JMPNZ_EX:
				case GEAR_JMP_SET:
				case GEAR_COALESCE:
				case GEAR_FE_RESET_R:
				case GEAR_FE_RESET_RW:
				case GEAR_ASSERT_CHECK:
					opline->op2.jmp_addr = &op_array->opcodes[opline->op2.jmp_addr - old_opcodes];
					break;
				case GEAR_CATCH:
					if (!(opline->extended_value & GEAR_LAST_CATCH)) {
						opline->op2.jmp_addr = &op_array->opcodes[opline->op2.jmp_addr - old_opcodes];
					}
					break;
				case GEAR_DECLARE_ANON_CLASS:
				case GEAR_DECLARE_ANON_INHERITED_CLASS:
				case GEAR_FE_FETCH_R:
				case GEAR_FE_FETCH_RW:
				case GEAR_SWITCH_LONG:
				case GEAR_SWITCH_STRING:
					/* relative extended_value don't have to be changed */
					break;
			}
		}
#endif
		opline++;
	}
}
#endif

static void gear_optimize_op_array(gear_op_array      *op_array,
                                   gear_optimizer_ctx *ctx)
{
	/* Revert pass_two() */
	gear_revert_pass_two(op_array);

	/* Do actual optimizations */
	gear_optimize(op_array, ctx);

	/* Redo pass_two() */
	gear_redo_pass_two(op_array);
}

static void gear_adjust_fcall_stack_size(gear_op_array *op_array, gear_optimizer_ctx *ctx)
{
	gear_function *func;
	gear_op *opline, *end;

	opline = op_array->opcodes;
	end = opline + op_array->last;
	while (opline < end) {
		if (opline->opcode == GEAR_INIT_FCALL) {
			func = gear_hash_find_ptr(
				&ctx->script->function_table,
				Z_STR_P(RT_CONSTANT(opline, opline->op2)));
			if (func) {
				opline->op1.num = gear_vm_calc_used_stack(opline->extended_value, func);
			}
		}
		opline++;
	}
}

#if HAVE_DFA_PASS
static void gear_adjust_fcall_stack_size_graph(gear_op_array *op_array)
{
	gear_func_info *func_info = GEAR_FUNC_INFO(op_array);

	if (func_info) {
		gear_call_info *call_info =func_info->callee_info;

		while (call_info) {
			gear_op *opline = call_info->caller_init_opline;

			if (opline && call_info->callee_func && opline->opcode == GEAR_INIT_FCALL) {
				opline->op1.num = gear_vm_calc_used_stack(opline->extended_value, call_info->callee_func);
			}
			call_info = call_info->next_callee;
		}
	}
}
#endif

int gear_optimize_script(gear_script *script, gear_long optimization_level, gear_long debug_level)
{
	gear_class_entry *ce;
	gear_op_array *op_array;
	gear_string *name;
	gear_optimizer_ctx ctx;
#if HAVE_DFA_PASS
	gear_call_graph call_graph;
#endif

	ctx.arena = gear_arena_create(64 * 1024);
	ctx.script = script;
	ctx.constants = NULL;
	ctx.optimization_level = optimization_level;
	ctx.debug_level = debug_level;

	gear_optimize_op_array(&script->main_op_array, &ctx);

	GEAR_HASH_FOREACH_PTR(&script->function_table, op_array) {
		gear_optimize_op_array(op_array, &ctx);
	} GEAR_HASH_FOREACH_END();

	GEAR_HASH_FOREACH_PTR(&script->class_table, ce) {
		GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->function_table, name, op_array) {
			if (op_array->scope == ce) {
				gear_optimize_op_array(op_array, &ctx);
			} else if (op_array->type == GEAR_USER_FUNCTION) {
				gear_op_array *orig_op_array;
				if ((orig_op_array = gear_hash_find_ptr(&op_array->scope->function_table, name)) != NULL) {
					HashTable *ht = op_array->static_variables;
					*op_array = *orig_op_array;
					op_array->static_variables = ht;
				}
			}
		} GEAR_HASH_FOREACH_END();
	} GEAR_HASH_FOREACH_END();

#if HAVE_DFA_PASS
	if ((GEAR_OPTIMIZER_PASS_6 & optimization_level) &&
	    (GEAR_OPTIMIZER_PASS_7 & optimization_level) &&
	    gear_build_call_graph(&ctx.arena, script, GEAR_RT_CONSTANTS, &call_graph) == SUCCESS) {
		/* Optimize using call-graph */
		void *checkpoint = gear_arena_checkpoint(ctx.arena);
		int i;
		gear_func_info *func_info;

		for (i = 0; i < call_graph.op_arrays_count; i++) {
			gear_revert_pass_two(call_graph.op_arrays[i]);
		}

		for (i = 0; i < call_graph.op_arrays_count; i++) {
			func_info = GEAR_FUNC_INFO(call_graph.op_arrays[i]);
			if (func_info) {
				func_info->call_map = gear_build_call_map(&ctx.arena, func_info, call_graph.op_arrays[i]);
				if (call_graph.op_arrays[i]->fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
					gear_init_func_return_info(call_graph.op_arrays[i], script, &func_info->return_info);
				}
			}
		}

		for (i = 0; i < call_graph.op_arrays_count; i++) {
			func_info = GEAR_FUNC_INFO(call_graph.op_arrays[i]);
			if (func_info) {
				if (gear_dfa_analyze_op_array(call_graph.op_arrays[i], &ctx, &func_info->ssa) == SUCCESS) {
					func_info->flags = func_info->ssa.cfg.flags;
				} else {
					GEAR_SET_FUNC_INFO(call_graph.op_arrays[i], NULL);
				}
			}
		}

		//TODO: perform inner-script inference???
		for (i = 0; i < call_graph.op_arrays_count; i++) {
			func_info = GEAR_FUNC_INFO(call_graph.op_arrays[i]);
			if (func_info) {
				gear_dfa_optimize_op_array(call_graph.op_arrays[i], &ctx, &func_info->ssa, func_info->call_map);
			}
		}

		if (debug_level & GEAR_DUMP_AFTER_PASS_7) {
			for (i = 0; i < call_graph.op_arrays_count; i++) {
				gear_dump_op_array(call_graph.op_arrays[i], 0, "after pass 7", NULL);
			}
		}

		if (GEAR_OPTIMIZER_PASS_11 & optimization_level) {
			for (i = 0; i < call_graph.op_arrays_count; i++) {
				gear_optimizer_compact_literals(call_graph.op_arrays[i], &ctx);
				if (debug_level & GEAR_DUMP_AFTER_PASS_11) {
					gear_dump_op_array(call_graph.op_arrays[i], 0, "after pass 11", NULL);
				}
			}
		}

		if (GEAR_OPTIMIZER_PASS_13 & optimization_level) {
			for (i = 0; i < call_graph.op_arrays_count; i++) {
				gear_optimizer_compact_vars(call_graph.op_arrays[i]);
				if (debug_level & GEAR_DUMP_AFTER_PASS_13) {
					gear_dump_op_array(call_graph.op_arrays[i], 0, "after pass 13", NULL);
				}
			}
		}

		if (GEAR_OPTIMIZER_PASS_12 & optimization_level) {
			for (i = 0; i < call_graph.op_arrays_count; i++) {
				gear_adjust_fcall_stack_size_graph(call_graph.op_arrays[i]);
			}
		}

		for (i = 0; i < call_graph.op_arrays_count; i++) {
			func_info = GEAR_FUNC_INFO(call_graph.op_arrays[i]);
			if (func_info && func_info->ssa.var_info) {
				gear_redo_pass_two_ex(call_graph.op_arrays[i], &func_info->ssa);
			} else {
				gear_redo_pass_two(call_graph.op_arrays[i]);
			}
		}

		for (i = 0; i < call_graph.op_arrays_count; i++) {
			GEAR_SET_FUNC_INFO(call_graph.op_arrays[i], NULL);
		}

		GEAR_HASH_FOREACH_PTR(&script->class_table, ce) {
			GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->function_table, name, op_array) {
				if (op_array->scope != ce) {
					gear_op_array *orig_op_array;
					if ((orig_op_array = gear_hash_find_ptr(&op_array->scope->function_table, name)) != NULL) {
						HashTable *ht = op_array->static_variables;
						*op_array = *orig_op_array;
						op_array->static_variables = ht;
					}
				}
			} GEAR_HASH_FOREACH_END();
		} GEAR_HASH_FOREACH_END();

		gear_arena_release(&ctx.arena, checkpoint);
	} else
#endif

	if (GEAR_OPTIMIZER_PASS_12 & optimization_level) {
		gear_adjust_fcall_stack_size(&script->main_op_array, &ctx);

		GEAR_HASH_FOREACH_PTR(&script->function_table, op_array) {
			gear_adjust_fcall_stack_size(op_array, &ctx);
		} GEAR_HASH_FOREACH_END();

		GEAR_HASH_FOREACH_PTR(&script->class_table, ce) {
			GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->function_table, name, op_array) {
				if (op_array->scope == ce) {
					gear_adjust_fcall_stack_size(op_array, &ctx);
				} else if (op_array->type == GEAR_USER_FUNCTION) {
					gear_op_array *orig_op_array;
					if ((orig_op_array = gear_hash_find_ptr(&op_array->scope->function_table, name)) != NULL) {
						HashTable *ht = op_array->static_variables;
						*op_array = *orig_op_array;
						op_array->static_variables = ht;
					}
				}
			} GEAR_HASH_FOREACH_END();
		} GEAR_HASH_FOREACH_END();
	}

	if ((debug_level & GEAR_DUMP_AFTER_OPTIMIZER) &&
	    (GEAR_OPTIMIZER_PASS_7 & optimization_level)) {
		gear_dump_op_array(&script->main_op_array, GEAR_DUMP_RT_CONSTANTS, "after optimizer", NULL);

		GEAR_HASH_FOREACH_PTR(&script->function_table, op_array) {
			gear_dump_op_array(op_array, GEAR_DUMP_RT_CONSTANTS, "after optimizer", NULL);
		} GEAR_HASH_FOREACH_END();

		GEAR_HASH_FOREACH_PTR(&script->class_table, ce) {
			GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->function_table, name, op_array) {
				if (op_array->scope == ce) {
					gear_dump_op_array(op_array, GEAR_DUMP_RT_CONSTANTS, "after optimizer", NULL);
				}
			} GEAR_HASH_FOREACH_END();
		} GEAR_HASH_FOREACH_END();
	}

	if (ctx.constants) {
		gear_hash_destroy(ctx.constants);
	}
	gear_arena_destroy(ctx.arena);

	return 1;
}

int gear_optimizer_startup(void)
{
	return gear_func_info_startup();
}

int gear_optimizer_shutdown(void)
{
	return gear_func_info_shutdown();
}

