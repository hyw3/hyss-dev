/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "gear_compile.h"
#include "gear_cfg.h"
#include "gear_ssa.h"
#include "gear_inference.h"
#include "gear_func_info.h"
#include "gear_call_graph.h"
#include "gear_dump.h"

void gear_dump_ht(HashTable *ht)
{
	gear_ulong index;
	gear_string *key;
	zval *val;
	int first = 1;

	GEAR_HASH_FOREACH_KEY_VAL(ht, index, key, val) {
		if (first) {
			first = 0;
		} else {
			fprintf(stderr, ", ");
		}
		if (key) {
			fprintf(stderr, "\"%s\"", ZSTR_VAL(key));
		} else {
			fprintf(stderr, GEAR_LONG_FMT, index);
		}
		fprintf(stderr, " =>");
		gear_dump_const(val);
	} GEAR_HASH_FOREACH_END();
}

void gear_dump_const(const zval *zv)
{
	switch (Z_TYPE_P(zv)) {
		case IS_NULL:
			fprintf(stderr, " null");
			break;
		case IS_FALSE:
			fprintf(stderr, " bool(false)");
			break;
		case IS_TRUE:
			fprintf(stderr, " bool(true)");
			break;
		case IS_LONG:
			fprintf(stderr, " int(" GEAR_LONG_FMT ")", Z_LVAL_P(zv));
			break;
		case IS_DOUBLE:
			fprintf(stderr, " float(%g)", Z_DVAL_P(zv));
			break;
		case IS_STRING:
			fprintf(stderr, " string(\"%s\")", Z_STRVAL_P(zv));
			break;
		case IS_ARRAY:
			fprintf(stderr, " array(...)");
			break;
		default:
			fprintf(stderr, " zval(type=%d)", Z_TYPE_P(zv));
			break;
	}
}

static void gear_dump_class_fetch_type(uint32_t fetch_type)
{
	switch (fetch_type & GEAR_FETCH_CLASS_MASK) {
		case GEAR_FETCH_CLASS_SELF:
			fprintf(stderr, " (self)");
			break;
		case GEAR_FETCH_CLASS_PARENT:
			fprintf(stderr, " (parent)");
			break;
		case GEAR_FETCH_CLASS_STATIC:
			fprintf(stderr, " (static)");
			break;
		case GEAR_FETCH_CLASS_AUTO:
			fprintf(stderr, " (auto)");
			break;
		case GEAR_FETCH_CLASS_INTERFACE:
			fprintf(stderr, " (interface)");
			break;
		case GEAR_FETCH_CLASS_TRAIT:
			fprintf(stderr, " (trait)");
			break;
	}
	if (fetch_type & GEAR_FETCH_CLASS_NO_AUTOLOAD) {
			fprintf(stderr, " (no-autolod)");
	}
	if (fetch_type & GEAR_FETCH_CLASS_SILENT) {
			fprintf(stderr, " (silent)");
	}
	if (fetch_type & GEAR_FETCH_CLASS_EXCEPTION) {
			fprintf(stderr, " (exception)");
	}
}

static void gear_dump_unused_op(const gear_op *opline, znode_op op, uint32_t flags) {
	if (GEAR_VM_OP_NUM == (flags & GEAR_VM_OP_MASK)) {
		fprintf(stderr, " %u", op.num);
	} else if (GEAR_VM_OP_TRY_CATCH == (flags & GEAR_VM_OP_MASK)) {
		if (op.num != (uint32_t)-1) {
			fprintf(stderr, " try-catch(%u)", op.num);
		}
	} else if (GEAR_VM_OP_THIS == (flags & GEAR_VM_OP_MASK)) {
		fprintf(stderr, " THIS");
	} else if (GEAR_VM_OP_NEXT == (flags & GEAR_VM_OP_MASK)) {
		fprintf(stderr, " NEXT");
	} else if (GEAR_VM_OP_CLASS_FETCH == (flags & GEAR_VM_OP_MASK)) {
		gear_dump_class_fetch_type(op.num);
	} else if (GEAR_VM_OP_CONSTRUCTOR == (flags & GEAR_VM_OP_MASK)) {
		fprintf(stderr, " CONSTRUCTOR");
	} else if (GEAR_VM_OP_CONST_FETCH == (flags & GEAR_VM_EXT_MASK)) {
		if (op.num & IS_CONSTANT_UNQUALIFIED) {
			fprintf(stderr, " (unqualified)");
		}
		if (op.num & IS_CONSTANT_IN_NAMESPACE) {
			fprintf(stderr, " (in-namespace)");
		}
	}
}

void gear_dump_var(const gear_op_array *op_array, gear_uchar var_type, int var_num)
{
	if (var_type == IS_CV && var_num < op_array->last_var) {
		fprintf(stderr, "CV%d($%s)", var_num, op_array->vars[var_num]->val);
	} else if (var_type == IS_VAR) {
		fprintf(stderr, "V%d", var_num);
	} else if (var_type == IS_TMP_VAR) {
		fprintf(stderr, "T%d", var_num);
	} else {
		fprintf(stderr, "X%d", var_num);
	}
}

static void gear_dump_range(const gear_ssa_range *r)
{
	if (r->underflow && r->overflow) {
		return;
	}
	fprintf(stderr, " RANGE[");
	if (r->underflow) {
		fprintf(stderr, "--..");
	} else {
		fprintf(stderr, GEAR_LONG_FMT "..", r->min);
	}
	if (r->overflow) {
		fprintf(stderr, "++]");
	} else {
		fprintf(stderr, GEAR_LONG_FMT "]", r->max);
	}
}

static void gear_dump_type_info(uint32_t info, gear_class_entry *ce, int is_instanceof, uint32_t dump_flags)
{
	int first = 1;

	fprintf(stderr, " [");
	if (info & MAY_BE_UNDEF) {
		if (first) first = 0; else fprintf(stderr, ", ");
		fprintf(stderr, "undef");
	}
	if (info & MAY_BE_REF) {
		if (first) first = 0; else fprintf(stderr, ", ");
		fprintf(stderr, "ref");
	}
	if (dump_flags & GEAR_DUMP_RC_INFERENCE) {
		if (info & MAY_BE_RC1) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "rc1");
		}
		if (info & MAY_BE_RCN) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "rcn");
		}
	}
	if (info & MAY_BE_CLASS) {
		if (first) first = 0; else fprintf(stderr, ", ");
		fprintf(stderr, "class");
		if (ce) {
			if (is_instanceof) {
				fprintf(stderr, " (instanceof %s)", ce->name->val);
			} else {
				fprintf(stderr, " (%s)", ce->name->val);
			}
		}
	} else if ((info & MAY_BE_ANY) == MAY_BE_ANY) {
		if (first) first = 0; else fprintf(stderr, ", ");
		fprintf(stderr, "any");
	} else {
		if (info & MAY_BE_NULL) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "null");
		}
		if ((info & MAY_BE_FALSE) && (info & MAY_BE_TRUE)) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "bool");
		} else if (info & MAY_BE_FALSE) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "false");
		} else if (info & MAY_BE_TRUE) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "true");
		}
		if (info & MAY_BE_LONG) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "long");
		}
		if (info & MAY_BE_DOUBLE) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "double");
		}
		if (info & MAY_BE_STRING) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "string");
		}
		if (info & MAY_BE_ARRAY) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "array");
			if ((info & MAY_BE_ARRAY_KEY_ANY) != 0 &&
			    (info & MAY_BE_ARRAY_KEY_ANY) != MAY_BE_ARRAY_KEY_ANY) {
				int afirst = 1;
				fprintf(stderr, " [");
				if (info & MAY_BE_ARRAY_KEY_LONG) {
					if (afirst) afirst = 0; else fprintf(stderr, ", ");
					fprintf(stderr, "long");
				}
				if (info & MAY_BE_ARRAY_KEY_STRING) {
					if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "string");
					}
				fprintf(stderr, "]");
			}
			if (info & (MAY_BE_ARRAY_OF_ANY|MAY_BE_ARRAY_OF_REF)) {
				int afirst = 1;
				fprintf(stderr, " of [");
				if ((info & MAY_BE_ARRAY_OF_ANY) == MAY_BE_ARRAY_OF_ANY) {
					if (afirst) afirst = 0; else fprintf(stderr, ", ");
					fprintf(stderr, "any");
				} else {
					if (info & MAY_BE_ARRAY_OF_NULL) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "null");
					}
					if (info & MAY_BE_ARRAY_OF_FALSE) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "false");
					}
					if (info & MAY_BE_ARRAY_OF_TRUE) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "true");
					}
					if (info & MAY_BE_ARRAY_OF_LONG) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "long");
					}
					if (info & MAY_BE_ARRAY_OF_DOUBLE) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "double");
					}
					if (info & MAY_BE_ARRAY_OF_STRING) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "string");
					}
					if (info & MAY_BE_ARRAY_OF_ARRAY) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "array");
					}
					if (info & MAY_BE_ARRAY_OF_OBJECT) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "object");
					}
					if (info & MAY_BE_ARRAY_OF_RESOURCE) {
						if (afirst) afirst = 0; else fprintf(stderr, ", ");
						fprintf(stderr, "resource");
					}
				}
				if (info & MAY_BE_ARRAY_OF_REF) {
					if (afirst) afirst = 0; else fprintf(stderr, ", ");
					fprintf(stderr, "ref");
				}
				fprintf(stderr, "]");
			}
		}
		if (info & MAY_BE_OBJECT) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "object");
			if (ce) {
				if (is_instanceof) {
					fprintf(stderr, " (instanceof %s)", ce->name->val);
				} else {
					fprintf(stderr, " (%s)", ce->name->val);
				}
			}
		}
		if (info & MAY_BE_RESOURCE) {
			if (first) first = 0; else fprintf(stderr, ", ");
			fprintf(stderr, "resource");
		}
	}
	if (info & MAY_BE_ERROR) {
		if (first) first = 0; else fprintf(stderr, ", ");
		fprintf(stderr, "error");
	}
//TODO: this is useful only for JIT???
	if (info & MAY_BE_IN_REG) {
		if (first) first = 0; else fprintf(stderr, ", ");
		fprintf(stderr, "reg");
	}
	fprintf(stderr, "]");
}

static void gear_dump_ssa_var_info(const gear_ssa *ssa, int ssa_var_num, uint32_t dump_flags)
{
	gear_dump_type_info(
		ssa->var_info[ssa_var_num].type,
		ssa->var_info[ssa_var_num].ce,
		ssa->var_info[ssa_var_num].ce ?
			ssa->var_info[ssa_var_num].is_instanceof : 0,
		dump_flags);
}

static void gear_dump_ssa_var(const gear_op_array *op_array, const gear_ssa *ssa, int ssa_var_num, gear_uchar var_type, int var_num, uint32_t dump_flags)
{
	if (ssa_var_num >= 0) {
		fprintf(stderr, "#%d.", ssa_var_num);
	} else {
		fprintf(stderr, "#?.");
	}
	gear_dump_var(op_array, (var_num < op_array->last_var ? IS_CV : var_type), var_num);

	if (ssa_var_num >= 0 && ssa->vars) {
		if (ssa->vars[ssa_var_num].no_val) {
			fprintf(stderr, " NOVAL");
		}
		if (ssa->vars[ssa_var_num].escape_state == ESCAPE_STATE_NO_ESCAPE) {
			fprintf(stderr, " NOESC");
		}
		if (ssa->var_info) {
			gear_dump_ssa_var_info(ssa, ssa_var_num, dump_flags);
			if (ssa->var_info[ssa_var_num].has_range) {
				gear_dump_range(&ssa->var_info[ssa_var_num].range);
			}
		}
	}
}

static void gear_dump_type_constraint(const gear_op_array *op_array, const gear_ssa *ssa, const gear_ssa_type_constraint *constraint, uint32_t dump_flags)
{
	fprintf(stderr, " TYPE");
	gear_dump_type_info(constraint->type_mask, constraint->ce, 1, dump_flags);
}

static void gear_dump_range_constraint(const gear_op_array *op_array, const gear_ssa *ssa, const gear_ssa_range_constraint *r, uint32_t dump_flags)
{
	if (r->range.underflow && r->range.overflow) {
		return;
	}
	fprintf(stderr, " RANGE");
	if (r->negative) {
		fprintf(stderr, "~");
	}
	fprintf(stderr, "[");
	if (r->range.underflow) {
		fprintf(stderr, "-- .. ");
	} else {
		if (r->min_ssa_var >= 0) {
			gear_dump_ssa_var(op_array, ssa, r->min_ssa_var, (r->min_var < op_array->last_var ? IS_CV : 0), r->min_var, dump_flags);
			if (r->range.min > 0) {
				fprintf(stderr, " + " GEAR_LONG_FMT, r->range.min);
			} else if (r->range.min < 0) {
				fprintf(stderr, " - " GEAR_LONG_FMT, -r->range.min);
			}
			fprintf(stderr, " .. ");
		} else {
			fprintf(stderr, GEAR_LONG_FMT " .. ", r->range.min);
		}
	}
	if (r->range.overflow) {
		fprintf(stderr, "++]");
	} else {
		if (r->max_ssa_var >= 0) {
			gear_dump_ssa_var(op_array, ssa, r->max_ssa_var, (r->max_var < op_array->last_var ? IS_CV : 0), r->max_var, dump_flags);
			if (r->range.max > 0) {
				fprintf(stderr, " + " GEAR_LONG_FMT, r->range.max);
			} else if (r->range.max < 0) {
				fprintf(stderr, " - " GEAR_LONG_FMT, -r->range.max);
			}
			fprintf(stderr, "]");
		} else {
			fprintf(stderr, GEAR_LONG_FMT "]", r->range.max);
		}
	}
}

static void gear_dump_op(const gear_op_array *op_array, const gear_basic_block *b, const gear_op *opline, uint32_t dump_flags, const void *data)
{
	const char *name = gear_get_opcode_name(opline->opcode);
	uint32_t flags = gear_get_opcode_flags(opline->opcode);
	uint32_t n = 0;
	int len = 0;
	const gear_ssa *ssa = NULL;

	if (dump_flags & GEAR_DUMP_SSA) {
		ssa = (const gear_ssa*)data;
	}

	if (!b) {
		len = fprintf(stderr, "L%u (%u):", (uint32_t)(opline - op_array->opcodes), opline->lineno);
	}
	fprintf(stderr, "%*c", 12-len, ' ');

	if (!ssa || !ssa->ops || ssa->ops[opline - op_array->opcodes].result_use < 0) {
		if (opline->result_type & (IS_CV|IS_VAR|IS_TMP_VAR)) {
			if (ssa && ssa->ops && ssa->ops[opline - op_array->opcodes].result_def >= 0) {
				int ssa_var_num = ssa->ops[opline - op_array->opcodes].result_def;
				gear_dump_ssa_var(op_array, ssa, ssa_var_num, opline->result_type, EX_VAR_TO_NUM(opline->result.var), dump_flags);
			} else {
				gear_dump_var(op_array, opline->result_type, EX_VAR_TO_NUM(opline->result.var));
			}
			fprintf(stderr, " = ");
		}
	}

	if (name) {
		fprintf(stderr, "%s", (name + 5));
	} else {
		fprintf(stderr, "OP_%d", (int)opline->opcode);
	}

	if (GEAR_VM_EXT_NUM == (flags & GEAR_VM_EXT_MASK)) {
		fprintf(stderr, " %u", opline->extended_value);
	} else if (GEAR_VM_EXT_DIM_OBJ == (flags & GEAR_VM_EXT_MASK)) {
		if (opline->extended_value == GEAR_ASSIGN_DIM) {
			fprintf(stderr, " (dim)");
		} else if (opline->extended_value == GEAR_ASSIGN_OBJ) {
			fprintf(stderr, " (obj)");
		}
	} else if (GEAR_VM_EXT_TYPE == (flags & GEAR_VM_EXT_MASK)) {
		switch (opline->extended_value) {
			case IS_NULL:
				fprintf(stderr, " (null)");
				break;
			case IS_FALSE:
				fprintf(stderr, " (false)");
				break;
			case IS_TRUE:
				fprintf(stderr, " (true)");
				break;
			case IS_LONG:
				fprintf(stderr, " (long)");
				break;
			case IS_DOUBLE:
				fprintf(stderr, " (double)");
				break;
			case IS_STRING:
				fprintf(stderr, " (string)");
				break;
			case IS_ARRAY:
				fprintf(stderr, " (array)");
				break;
			case IS_OBJECT:
				fprintf(stderr, " (object)");
				break;
			case IS_RESOURCE:
				fprintf(stderr, " (resource)");
				break;
			case _IS_BOOL:
				fprintf(stderr, " (bool)");
				break;
			case IS_CALLABLE:
				fprintf(stderr, " (callable)");
				break;
			case IS_VOID:
				fprintf(stderr, " (void)");
				break;
			default:
				fprintf(stderr, " (\?\?\?)");
				break;
		}
	} else if (GEAR_VM_EXT_TYPE_MASK == (flags & GEAR_VM_EXT_MASK)) {
		switch (opline->extended_value) {
			case (1<<IS_NULL):
				fprintf(stderr, " (null)");
				break;
			case (1<<IS_FALSE):
				fprintf(stderr, " (false)");
				break;
			case (1<<IS_TRUE):
				fprintf(stderr, " (true)");
				break;
			case (1<<IS_LONG):
				fprintf(stderr, " (long)");
				break;
			case (1<<IS_DOUBLE):
				fprintf(stderr, " (double)");
				break;
			case (1<<IS_STRING):
				fprintf(stderr, " (string)");
				break;
			case (1<<IS_ARRAY):
				fprintf(stderr, " (array)");
				break;
			case (1<<IS_OBJECT):
				fprintf(stderr, " (object)");
				break;
			case (1<<IS_RESOURCE):
				fprintf(stderr, " (resource)");
				break;
			case ((1<<IS_FALSE)|(1<<IS_TRUE)):
				fprintf(stderr, " (bool)");
				break;
			default:
				fprintf(stderr, " (\?\?\?)");
				break;
		}
	} else if (GEAR_VM_EXT_EVAL == (flags & GEAR_VM_EXT_MASK)) {
		switch (opline->extended_value) {
			case GEAR_EVAL:
				fprintf(stderr, " (eval)");
				break;
			case GEAR_INCLUDE:
				fprintf(stderr, " (include)");
				break;
			case GEAR_INCLUDE_ONCE:
				fprintf(stderr, " (include_once)");
				break;
			case GEAR_REQUIRE:
				fprintf(stderr, " (require)");
				break;
			case GEAR_REQUIRE_ONCE:
				fprintf(stderr, " (require_once)");
				break;
			default:
				fprintf(stderr, " (\?\?\?)");
				break;
		}
	} else if (GEAR_VM_EXT_SRC == (flags & GEAR_VM_EXT_MASK)) {
		if (opline->extended_value == GEAR_RETURNS_VALUE) {
			fprintf(stderr, " (value)");
		} else if (opline->extended_value == GEAR_RETURNS_FUNCTION) {
			fprintf(stderr, " (function)");
		}
	} else {
		if (GEAR_VM_EXT_VAR_FETCH & flags) {
			if (opline->extended_value & GEAR_FETCH_GLOBAL) {
				fprintf(stderr, " (global)");
			} else if (opline->extended_value & GEAR_FETCH_LOCAL) {
				fprintf(stderr, " (local)");
			} else if (opline->extended_value & GEAR_FETCH_GLOBAL_LOCK) {
				fprintf(stderr, " (global+lock)");
			}
		}
		if (GEAR_VM_EXT_ISSET & flags) {
			if (!(opline->extended_value & GEAR_ISEMPTY)) {
				fprintf(stderr, " (isset)");
			} else {
				fprintf(stderr, " (empty)");
			}
		}
		if (GEAR_VM_EXT_ARRAY_INIT & flags) {
			fprintf(stderr, " %u", opline->extended_value >> GEAR_ARRAY_SIZE_SHIFT);
			if (!(opline->extended_value & GEAR_ARRAY_NOT_PACKED)) {
				fprintf(stderr, " (packed)");
			}
		}
		if (GEAR_VM_EXT_REF & flags) {
			if (opline->extended_value & GEAR_ARRAY_ELEMENT_REF) {
				fprintf(stderr, " (ref)");
			}
		}
	}

	if (opline->op1_type == IS_CONST) {
		gear_dump_const(CRT_CONSTANT_EX(op_array, opline, opline->op1, (dump_flags & GEAR_DUMP_RT_CONSTANTS)));
	} else if (opline->op1_type & (IS_CV|IS_VAR|IS_TMP_VAR)) {
		if (ssa && ssa->ops) {
			int ssa_var_num = ssa->ops[opline - op_array->opcodes].op1_use;
			if (ssa_var_num >= 0) {
				fprintf(stderr, " ");
				gear_dump_ssa_var(op_array, ssa, ssa_var_num, opline->op1_type, EX_VAR_TO_NUM(opline->op1.var), dump_flags);
			} else if (ssa->ops[opline - op_array->opcodes].op1_def < 0) {
				fprintf(stderr, " ");
				gear_dump_var(op_array, opline->op1_type, EX_VAR_TO_NUM(opline->op1.var));
			}
		} else {
			fprintf(stderr, " ");
			gear_dump_var(op_array, opline->op1_type, EX_VAR_TO_NUM(opline->op1.var));
		}
		if (ssa && ssa->ops) {
			int ssa_var_num = ssa->ops[opline - op_array->opcodes].op1_def;
			if (ssa_var_num >= 0) {
				fprintf(stderr, " -> ");
				gear_dump_ssa_var(op_array, ssa, ssa_var_num, opline->op1_type, EX_VAR_TO_NUM(opline->op1.var), dump_flags);
			}
		}
	} else {
		uint32_t op1_flags = GEAR_VM_OP1_FLAGS(flags);
		if (GEAR_VM_OP_JMP_ADDR == (op1_flags & GEAR_VM_OP_MASK)) {
			if (b) {
				fprintf(stderr, " BB%d", b->successors[n++]);
			} else {
				fprintf(stderr, " L%u", (uint32_t)(OP_JMP_ADDR(opline, opline->op1) - op_array->opcodes));
			}
		} else {
			gear_dump_unused_op(opline, opline->op1, op1_flags);
		}
	}

	if (opline->op2_type == IS_CONST) {
		zval *op = CRT_CONSTANT_EX(op_array, opline, opline->op2, (dump_flags & GEAR_DUMP_RT_CONSTANTS));
		if (opline->opcode == GEAR_SWITCH_LONG || opline->opcode == GEAR_SWITCH_STRING) {
			HashTable *jumptable = Z_ARRVAL_P(op);
			gear_string *key;
			gear_ulong num_key;
			zval *zv;
			GEAR_HASH_FOREACH_KEY_VAL(jumptable, num_key, key, zv) {
				if (key) {
					fprintf(stderr, " \"%s\":", ZSTR_VAL(key));
				} else {
					fprintf(stderr, " " GEAR_LONG_FMT ":", num_key);
				}
				if (b) {
					fprintf(stderr, " BB%d,", b->successors[n++]);
				} else {
					fprintf(stderr, " L%u,", (uint32_t)GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, Z_LVAL_P(zv)));
				}
			} GEAR_HASH_FOREACH_END();
			fprintf(stderr, " default:");
		} else {
			gear_dump_const(op);
		}
	} else if (opline->op2_type & (IS_CV|IS_VAR|IS_TMP_VAR)) {
		if (ssa && ssa->ops) {
			int ssa_var_num = ssa->ops[opline - op_array->opcodes].op2_use;
			if (ssa_var_num >= 0) {
				fprintf(stderr, " ");
				gear_dump_ssa_var(op_array, ssa, ssa_var_num, opline->op2_type, EX_VAR_TO_NUM(opline->op2.var), dump_flags);
			} else if (ssa->ops[opline - op_array->opcodes].op2_def < 0) {
				fprintf(stderr, " ");
				gear_dump_var(op_array, opline->op2_type, EX_VAR_TO_NUM(opline->op2.var));
			}
		} else {
			fprintf(stderr, " ");
			gear_dump_var(op_array, opline->op2_type, EX_VAR_TO_NUM(opline->op2.var));
		}
		if (ssa && ssa->ops) {
			int ssa_var_num = ssa->ops[opline - op_array->opcodes].op2_def;
			if (ssa_var_num >= 0) {
				fprintf(stderr, " -> ");
				gear_dump_ssa_var(op_array, ssa, ssa_var_num, opline->op2_type, EX_VAR_TO_NUM(opline->op2.var), dump_flags);
			}
		}
	} else {
		uint32_t op2_flags = GEAR_VM_OP2_FLAGS(flags);
		if (GEAR_VM_OP_JMP_ADDR == (op2_flags & GEAR_VM_OP_MASK)) {
			if (opline->opcode != GEAR_CATCH || !(opline->extended_value & GEAR_LAST_CATCH)) {
				if (b) {
					fprintf(stderr, " BB%d", b->successors[n++]);
				} else {
					fprintf(stderr, " L%u", (uint32_t)(OP_JMP_ADDR(opline, opline->op2) - op_array->opcodes));
				}
			}
		} else {
			gear_dump_unused_op(opline, opline->op2, op2_flags);
		}
	}

	if (GEAR_VM_EXT_JMP_ADDR == (flags & GEAR_VM_EXT_MASK)) {
		if (b) {
			fprintf(stderr, " BB%d", b->successors[n++]);
		} else {
			fprintf(stderr, " L%u", (uint32_t)GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, opline->extended_value));
		}
	}
	if (opline->result_type == IS_CONST) {
		gear_dump_const(CRT_CONSTANT_EX(op_array, opline, opline->result, (dump_flags & GEAR_DUMP_RT_CONSTANTS)));
	} else if (ssa && ssa->ops && ssa->ops[opline - op_array->opcodes].result_use >= 0) {
		if (opline->result_type & (IS_CV|IS_VAR|IS_TMP_VAR)) {
			if (ssa && ssa->ops) {
				int ssa_var_num = ssa->ops[opline - op_array->opcodes].result_use;
				if (ssa_var_num >= 0) {
					fprintf(stderr, " ");
					gear_dump_ssa_var(op_array, ssa, ssa_var_num, opline->result_type, EX_VAR_TO_NUM(opline->result.var), dump_flags);
				}
			} else {
				fprintf(stderr, " ");
				gear_dump_var(op_array, opline->result_type, EX_VAR_TO_NUM(opline->result.var));
			}
			if (ssa && ssa->ops) {
				int ssa_var_num = ssa->ops[opline - op_array->opcodes].result_def;
				if (ssa_var_num >= 0) {
					fprintf(stderr, " -> ");
					gear_dump_ssa_var(op_array, ssa, ssa_var_num, opline->result_type, EX_VAR_TO_NUM(opline->result.var), dump_flags);
				}
			}
		}
	}
	fprintf(stderr, "\n");
}

static void gear_dump_block_info(const gear_cfg *cfg, int n, uint32_t dump_flags)
{
	gear_basic_block *b = cfg->blocks + n;

	fprintf(stderr, "BB%d:", n);
	if (b->flags & GEAR_BB_START) {
		fprintf(stderr, " start");
	}
	if (b->flags & GEAR_BB_FOLLOW) {
		fprintf(stderr, " follow");
	}
	if (b->flags & GEAR_BB_TARGET) {
		fprintf(stderr, " target");
	}
	if (b->flags & GEAR_BB_EXIT) {
		fprintf(stderr, " exit");
	}
	if (b->flags & (GEAR_BB_ENTRY|GEAR_BB_RECV_ENTRY)) {
		fprintf(stderr, " entry");
	}
	if (b->flags & GEAR_BB_TRY) {
		fprintf(stderr, " try");
	}
	if (b->flags & GEAR_BB_CATCH) {
		fprintf(stderr, " catch");
	}
	if (b->flags & GEAR_BB_FINALLY) {
		fprintf(stderr, " finally");
	}
	if (b->flags & GEAR_BB_FINALLY_END) {
		fprintf(stderr, " finally_end");
	}
	if (b->flags & GEAR_BB_GEN_VAR) {
		fprintf(stderr, " gen_var");
	}
	if (b->flags & GEAR_BB_KILL_VAR) {
		fprintf(stderr, " kill_var");
	}
	if (!(dump_flags & GEAR_DUMP_HIDE_UNREACHABLE) && !(b->flags & GEAR_BB_REACHABLE)) {
		fprintf(stderr, " unreachable");
	}
	if (b->flags & GEAR_BB_LOOP_HEADER) {
		fprintf(stderr, " loop_header");
	}
	if (b->flags & GEAR_BB_IRREDUCIBLE_LOOP) {
		fprintf(stderr, " irreducible");
	}
	if (b->len != 0) {
		fprintf(stderr, " lines=[%d-%d]", b->start, b->start + b->len - 1);
	} else {
		fprintf(stderr, " empty");
	}
	fprintf(stderr, "\n");

	if (b->predecessors_count) {
		int *p = cfg->predecessors + b->predecessor_offset;
		int *end = p + b->predecessors_count;

		fprintf(stderr, "    ; from=(BB%d", *p);
		for (p++; p < end; p++) {
			fprintf(stderr, ", BB%d", *p);
		}
		fprintf(stderr, ")\n");
	}

	if (b->successors_count > 0) {
		int s;
		fprintf(stderr, "    ; to=(BB%d", b->successors[0]);
		for (s = 1; s < b->successors_count; s++) {
			fprintf(stderr, ", BB%d", b->successors[s]);
		}
		fprintf(stderr, ")\n");
	}

	if (b->idom >= 0) {
		fprintf(stderr, "    ; idom=BB%d\n", b->idom);
	}
	if (b->level >= 0) {
		fprintf(stderr, "    ; level=%d\n", b->level);
	}
	if (b->loop_header >= 0) {
		fprintf(stderr, "    ; loop_header=%d\n", b->loop_header);
	}
	if (b->children >= 0) {
		int j = b->children;
		fprintf(stderr, "    ; children=(BB%d", j);
		j = cfg->blocks[j].next_child;
		while (j >= 0) {
			fprintf(stderr, ", BB%d", j);
			j = cfg->blocks[j].next_child;
		}
		fprintf(stderr, ")\n");
	}
}

static void gear_dump_block_header(const gear_cfg *cfg, const gear_op_array *op_array, const gear_ssa *ssa, int n, uint32_t dump_flags)
{
	gear_dump_block_info(cfg, n, dump_flags);
	if (ssa && ssa->blocks && ssa->blocks[n].phis) {
		gear_ssa_phi *p = ssa->blocks[n].phis;

		do {
			int j;

			fprintf(stderr, "        ");
			gear_dump_ssa_var(op_array, ssa, p->ssa_var, 0, p->var, dump_flags);
			if (p->pi < 0) {
				fprintf(stderr, " = Phi(");
				for (j = 0; j < cfg->blocks[n].predecessors_count; j++) {
					if (j > 0) {
						fprintf(stderr, ", ");
					}
					gear_dump_ssa_var(op_array, ssa, p->sources[j], 0, p->var, dump_flags);
				}
				fprintf(stderr, ")\n");
			} else {
				fprintf(stderr, " = Pi<BB%d>(", p->pi);
				gear_dump_ssa_var(op_array, ssa, p->sources[0], 0, p->var, dump_flags);
				fprintf(stderr, " &");
				if (p->has_range_constraint) {
					gear_dump_range_constraint(op_array, ssa, &p->constraint.range, dump_flags);
				} else {
					gear_dump_type_constraint(op_array, ssa, &p->constraint.type, dump_flags);
				}
				fprintf(stderr, ")\n");
			}
			p = p->next;
		} while (p);
	}
}

void gear_dump_op_array_name(const gear_op_array *op_array)
{
	gear_func_info *func_info = NULL;

	func_info = GEAR_FUNC_INFO(op_array);
	if (op_array->function_name) {
		if (op_array->scope && op_array->scope->name) {
			fprintf(stderr, "%s::%s", op_array->scope->name->val, op_array->function_name->val);
		} else {
			fprintf(stderr, "%s", op_array->function_name->val);
		}
	} else {
		fprintf(stderr, "%s", "$_main");
	}
	if (func_info && func_info->clone_num > 0) {
		fprintf(stderr, "_@_clone_%d", func_info->clone_num);
	}
}

void gear_dump_op_array(const gear_op_array *op_array, uint32_t dump_flags, const char *msg, const void *data)
{
	int i;
	const gear_cfg *cfg = NULL;
	const gear_ssa *ssa = NULL;
	gear_func_info *func_info = NULL;
	uint32_t func_flags = 0;

	if (dump_flags & (GEAR_DUMP_CFG|GEAR_DUMP_SSA)) {
		cfg = (const gear_cfg*)data;
		if (!cfg->blocks) {
			cfg = data = NULL;
		}
	}
	if (dump_flags & GEAR_DUMP_SSA) {
		ssa = (const gear_ssa*)data;
	}

	func_info = GEAR_FUNC_INFO(op_array);
	if (func_info) {
		func_flags = func_info->flags;
	}

	fprintf(stderr, "\n");
	gear_dump_op_array_name(op_array);
	fprintf(stderr, ": ; (lines=%d, args=%d",
		op_array->last,
		op_array->num_args);
	if (func_info && func_info->num_args >= 0) {
		fprintf(stderr, "/%d", func_info->num_args);
	}
	fprintf(stderr, ", vars=%d, tmps=%d", op_array->last_var, op_array->T);
	if (ssa) {
		fprintf(stderr, ", ssa_vars=%d", ssa->vars_count);
	}
	if (func_flags & GEAR_FUNC_INDIRECT_VAR_ACCESS) {
		fprintf(stderr, ", dynamic");
	}
	if (func_flags & GEAR_FUNC_RECURSIVE) {
		fprintf(stderr, ", recursive");
		if (func_flags & GEAR_FUNC_RECURSIVE_DIRECTLY) {
			fprintf(stderr, " directly");
		}
		if (func_flags & GEAR_FUNC_RECURSIVE_INDIRECTLY) {
			fprintf(stderr, " indirectly");
		}
	}
	if (func_flags & GEAR_FUNC_IRREDUCIBLE) {
		fprintf(stderr, ", irreducable");
	}
	if (func_flags & GEAR_FUNC_NO_LOOPS) {
		fprintf(stderr, ", no_loops");
	}
	if (func_flags & GEAR_FUNC_HAS_EXTENDED_INFO) {
		fprintf(stderr, ", extended_info");
	}
//TODO: this is useful only for JIT???
#if 0
	if (info->flags & GEAR_JIT_FUNC_NO_IN_MEM_CVS) {
		fprintf(stderr, ", no_in_mem_cvs");
	}
	if (info->flags & GEAR_JIT_FUNC_NO_USED_ARGS) {
		fprintf(stderr, ", no_used_args");
	}
	if (info->flags & GEAR_JIT_FUNC_NO_SYMTAB) {
		fprintf(stderr, ", no_symtab");
	}
	if (info->flags & GEAR_JIT_FUNC_NO_FRAME) {
		fprintf(stderr, ", no_frame");
	}
	if (info->flags & GEAR_JIT_FUNC_INLINE) {
		fprintf(stderr, ", inline");
	}
#endif
	if (func_info && func_info->return_value_used == 0) {
		fprintf(stderr, ", no_return_value");
	} else if (func_info && func_info->return_value_used == 1) {
		fprintf(stderr, ", return_value");
	}
	fprintf(stderr, ")\n");
	if (msg) {
		fprintf(stderr, "    ; (%s)\n", msg);
	}
	fprintf(stderr, "    ; %s:%u-%u\n", op_array->filename->val, op_array->line_start, op_array->line_end);

	if (func_info && func_info->num_args > 0) {
		uint32_t j;

		for (j = 0; j < MIN(op_array->num_args, func_info->num_args ); j++) {
			fprintf(stderr, "    ; arg %d ", j);
			gear_dump_type_info(func_info->arg_info[j].info.type, func_info->arg_info[j].info.ce, func_info->arg_info[j].info.is_instanceof, dump_flags);
			gear_dump_range(&func_info->arg_info[j].info.range);
			fprintf(stderr, "\n");
		}
	}

	if (func_info) {
		fprintf(stderr, "    ; return ");
		gear_dump_type_info(func_info->return_info.type, func_info->return_info.ce, func_info->return_info.is_instanceof, dump_flags);
		gear_dump_range(&func_info->return_info.range);
		fprintf(stderr, "\n");
	}

	if (ssa && ssa->var_info) {
		for (i = 0; i < op_array->last_var; i++) {
			fprintf(stderr, "    ; ");
			gear_dump_ssa_var(op_array, ssa, i, IS_CV, i, dump_flags);
			fprintf(stderr, "\n");
		}
	}

	if (cfg) {
		int n;
		gear_basic_block *b;

		for (n = 0; n < cfg->blocks_count; n++) {
			b = cfg->blocks + n;
			if (!(dump_flags & GEAR_DUMP_HIDE_UNREACHABLE) || (b->flags & GEAR_BB_REACHABLE)) {
				const gear_op *opline;
				const gear_op *end;

				gear_dump_block_header(cfg, op_array, ssa, n, dump_flags);
				opline = op_array->opcodes + b->start;
				end = opline + b->len;
				while (opline < end) {
					gear_dump_op(op_array, b, opline, dump_flags, data);
					opline++;
				}
			}
		}
		if (op_array->last_live_range) {
			fprintf(stderr, "LIVE RANGES:\n");
			for (i = 0; i < op_array->last_live_range; i++) {
				if ((cfg->flags & GEAR_CFG_SPLIT_AT_LIVE_RANGES)) {
					fprintf(stderr, "        %u: BB%u - BB%u ",
						EX_VAR_TO_NUM(op_array->live_range[i].var & ~GEAR_LIVE_MASK),
						cfg->map[op_array->live_range[i].start],
						cfg->map[op_array->live_range[i].end]);
				} else {
					fprintf(stderr, "        %u: L%u - L%u ",
						EX_VAR_TO_NUM(op_array->live_range[i].var & ~GEAR_LIVE_MASK),
						op_array->live_range[i].start,
						op_array->live_range[i].end);
				}
				switch (op_array->live_range[i].var & GEAR_LIVE_MASK) {
					case GEAR_LIVE_TMPVAR:
						fprintf(stderr, "(tmp/var)\n");
						break;
					case GEAR_LIVE_LOOP:
						fprintf(stderr, "(loop)\n");
						break;
					case GEAR_LIVE_SILENCE:
						fprintf(stderr, "(silence)\n");
						break;
					case GEAR_LIVE_ROPE:
						fprintf(stderr, "(rope)\n");
						break;
				}
			}
		}
		if (op_array->last_try_catch) {
			fprintf(stderr, "EXCEPTION TABLE:\n");
			for (i = 0; i < op_array->last_try_catch; i++) {
				fprintf(stderr, "        BB%u",
					cfg->map[op_array->try_catch_array[i].try_op]);
				if (op_array->try_catch_array[i].catch_op) {
					fprintf(stderr, ", BB%u",
						cfg->map[op_array->try_catch_array[i].catch_op]);
				} else {
					fprintf(stderr, ", -");
				}
				if (op_array->try_catch_array[i].finally_op) {
					fprintf(stderr, ", BB%u",
						cfg->map[op_array->try_catch_array[i].finally_op]);
				} else {
					fprintf(stderr, ", -");
				}
				if (op_array->try_catch_array[i].finally_end) {
					fprintf(stderr, ", BB%u\n",
						cfg->map[op_array->try_catch_array[i].finally_end]);
				} else {
					fprintf(stderr, ", -\n");
				}
			}
		}
	} else {
		const gear_op *opline = op_array->opcodes;
		const gear_op *end = opline + op_array->last;

		while (opline < end) {
			gear_dump_op(op_array, NULL, opline, dump_flags, data);
			opline++;
		}
		if (op_array->last_live_range) {
			fprintf(stderr, "LIVE RANGES:\n");
			for (i = 0; i < op_array->last_live_range; i++) {
				fprintf(stderr, "        %u: L%u - L%u ",
					EX_VAR_TO_NUM(op_array->live_range[i].var & ~GEAR_LIVE_MASK),
					op_array->live_range[i].start,
					op_array->live_range[i].end);
				switch (op_array->live_range[i].var & GEAR_LIVE_MASK) {
					case GEAR_LIVE_TMPVAR:
						fprintf(stderr, "(tmp/var)\n");
						break;
					case GEAR_LIVE_LOOP:
						fprintf(stderr, "(loop)\n");
						break;
					case GEAR_LIVE_SILENCE:
						fprintf(stderr, "(silence)\n");
						break;
					case GEAR_LIVE_ROPE:
						fprintf(stderr, "(rope)\n");
						break;
				}
			}
		}
		if (op_array->last_try_catch) {
			fprintf(stderr, "EXCEPTION TABLE:\n");
			for (i = 0; i < op_array->last_try_catch; i++) {
				fprintf(stderr, "        L%u",
					op_array->try_catch_array[i].try_op);
				if (op_array->try_catch_array[i].catch_op) {
					fprintf(stderr, ", L%u",
						op_array->try_catch_array[i].catch_op);
				} else {
					fprintf(stderr, ", -");
				}
				if (op_array->try_catch_array[i].finally_op) {
					fprintf(stderr, ", L%u",
						op_array->try_catch_array[i].finally_op);
				} else {
					fprintf(stderr, ", -");
				}
				if (op_array->try_catch_array[i].finally_end) {
					fprintf(stderr, ", L%u\n",
						op_array->try_catch_array[i].finally_end);
				} else {
					fprintf(stderr, ", -\n");
				}
			}
		}
	}
}

void gear_dump_dominators(const gear_op_array *op_array, const gear_cfg *cfg)
{
	int j;

	fprintf(stderr, "\nDOMINATORS-TREE for \"");
	gear_dump_op_array_name(op_array);
	fprintf(stderr, "\"\n");
	for (j = 0; j < cfg->blocks_count; j++) {
		gear_basic_block *b = cfg->blocks + j;
		if (b->flags & GEAR_BB_REACHABLE) {
			gear_dump_block_info(cfg, j, 0);
		}
	}
}

void gear_dump_variables(const gear_op_array *op_array)
{
	int j;

	fprintf(stderr, "\nCV Variables for \"");
	gear_dump_op_array_name(op_array);
	fprintf(stderr, "\"\n");
	for (j = 0; j < op_array->last_var; j++) {
		fprintf(stderr, "    ");
		gear_dump_var(op_array, IS_CV, j);
		fprintf(stderr, "\n");
	}
}

void gear_dump_ssa_variables(const gear_op_array *op_array, const gear_ssa *ssa, uint32_t dump_flags)
{
	int j;

	if (ssa->vars) {
		fprintf(stderr, "\nSSA Variable for \"");
		gear_dump_op_array_name(op_array);
		fprintf(stderr, "\"\n");

		for (j = 0; j < ssa->vars_count; j++) {
			fprintf(stderr, "    ");
			gear_dump_ssa_var(op_array, ssa, j, IS_CV, ssa->vars[j].var, dump_flags);
			if (ssa->vars[j].scc >= 0) {
				if (ssa->vars[j].scc_entry) {
					fprintf(stderr, " *");
				}  else {
					fprintf(stderr, "  ");
				}
				fprintf(stderr, "SCC=%d", ssa->vars[j].scc);
			}
			fprintf(stderr, "\n");
		}
	}
}

static void gear_dump_var_set(const gear_op_array *op_array, const char *name, gear_bitset set)
{
	int first = 1;
	uint32_t i;

	fprintf(stderr, "    ; %s = {", name);
	for (i = 0; i < op_array->last_var + op_array->T; i++) {
		if (gear_bitset_in(set, i)) {
			if (first) {
				first = 0;
			} else {
				fprintf(stderr, ", ");
			}
			gear_dump_var(op_array, IS_CV, i);
		}
	}
	fprintf(stderr, "}\n");
}

void gear_dump_dfg(const gear_op_array *op_array, const gear_cfg *cfg, const gear_dfg *dfg)
{
	int j;
	fprintf(stderr, "\nVariable Liveness for \"");
	gear_dump_op_array_name(op_array);
	fprintf(stderr, "\"\n");

	for (j = 0; j < cfg->blocks_count; j++) {
		fprintf(stderr, "  BB%d:\n", j);
		gear_dump_var_set(op_array, "def", DFG_BITSET(dfg->def, dfg->size, j));
		gear_dump_var_set(op_array, "use", DFG_BITSET(dfg->use, dfg->size, j));
		gear_dump_var_set(op_array, "in ", DFG_BITSET(dfg->in,  dfg->size, j));
		gear_dump_var_set(op_array, "out", DFG_BITSET(dfg->out, dfg->size, j));
	}
}

void gear_dump_phi_placement(const gear_op_array *op_array, const gear_ssa *ssa)
{
	int j;
	gear_ssa_block *ssa_blocks = ssa->blocks;
	int blocks_count = ssa->cfg.blocks_count;

	fprintf(stderr, "\nSSA Phi() Placement for \"");
	gear_dump_op_array_name(op_array);
	fprintf(stderr, "\"\n");
	for (j = 0; j < blocks_count; j++) {
		if (ssa_blocks && ssa_blocks[j].phis) {
			gear_ssa_phi *p = ssa_blocks[j].phis;
			int first = 1;

			fprintf(stderr, "  BB%d:\n", j);
			if (p->pi >= 0) {
				fprintf(stderr, "    ; pi={");
			} else {
				fprintf(stderr, "    ; phi={");
			}
			do {
				if (first) {
					first = 0;
				} else {
					fprintf(stderr, ", ");
				}
				gear_dump_var(op_array, IS_CV, p->var);
				p = p->next;
			} while (p);
			fprintf(stderr, "}\n");
		}
	}
}

