/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_SSA_H
#define GEAR_SSA_H

#include "gear_optimizer.h"
#include "gear_cfg.h"

typedef struct _gear_ssa_range {
	gear_long              min;
	gear_long              max;
	gear_bool              underflow;
	gear_bool              overflow;
} gear_ssa_range;

typedef enum _gear_ssa_negative_lat {
	NEG_NONE      = 0,
	NEG_INIT      = 1,
	NEG_INVARIANT = 2,
	NEG_USE_LT    = 3,
	NEG_USE_GT    = 4,
	NEG_UNKNOWN   = 5
} gear_ssa_negative_lat;

/* Special kind of SSA Phi function used in eSSA */
typedef struct _gear_ssa_range_constraint {
	gear_ssa_range         range;       /* simple range constraint */
	int                    min_var;
	int                    max_var;
	int                    min_ssa_var; /* ((min_var>0) ? MIN(ssa_var) : 0) + range.min */
	int                    max_ssa_var; /* ((max_var>0) ? MAX(ssa_var) : 0) + range.max */
	gear_ssa_negative_lat  negative;
} gear_ssa_range_constraint;

typedef struct _gear_ssa_type_constraint {
	uint32_t               type_mask;   /* Type mask to intersect with */
	gear_class_entry      *ce;          /* Class entry for instanceof constraints */
} gear_ssa_type_constraint;

typedef union _gear_ssa_pi_constraint {
	gear_ssa_range_constraint range;
	gear_ssa_type_constraint type;
} gear_ssa_pi_constraint;

/* SSA Phi - ssa_var = Phi(source0, source1, ...sourceN) */
typedef struct _gear_ssa_phi gear_ssa_phi;
struct _gear_ssa_phi {
	gear_ssa_phi          *next;          /* next Phi in the same BB */
	int                    pi;            /* if >= 0 this is actually a e-SSA Pi */
	gear_ssa_pi_constraint constraint;    /* e-SSA Pi constraint */
	int                    var;           /* Original CV, VAR or TMP variable index */
	int                    ssa_var;       /* SSA variable index */
	int                    block;         /* current BB index */
	int                    visited : 1;   /* flag to avoid recursive processing */
	int                    has_range_constraint : 1;
	gear_ssa_phi         **use_chains;
	gear_ssa_phi          *sym_use_chain;
	int                   *sources;       /* Array of SSA IDs that produce this var.
									         As many as this block has
									         predecessors.  */
};

typedef struct _gear_ssa_block {
	gear_ssa_phi          *phis;
} gear_ssa_block;

typedef struct _gear_ssa_op {
	int                    op1_use;
	int                    op2_use;
	int                    result_use;
	int                    op1_def;
	int                    op2_def;
	int                    result_def;
	int                    op1_use_chain;
	int                    op2_use_chain;
	int                    res_use_chain;
} gear_ssa_op;

typedef enum _gear_ssa_alias_kind {
	NO_ALIAS,
	SYMTABLE_ALIAS,
	HYSS_ERRORMSG_ALIAS,
	HTTP_RESPONSE_HEADER_ALIAS
} gear_ssa_alias_kind;

typedef enum _gear_ssa_escape_state {
	ESCAPE_STATE_UNKNOWN,
	ESCAPE_STATE_NO_ESCAPE,
	ESCAPE_STATE_FUNCTION_ESCAPE,
	ESCAPE_STATE_GLOBAL_ESCAPE
} gear_ssa_escape_state;

typedef struct _gear_ssa_var {
	int                    var;            /* original var number; op.var for CVs and following numbers for VARs and TMP_VARs */
	int                    scc;            /* strongly connected component */
	int                    definition;     /* opcode that defines this value */
	gear_ssa_phi          *definition_phi; /* phi that defines this value */
	int                    use_chain;      /* uses of this value, linked through opN_use_chain */
	gear_ssa_phi          *phi_use_chain;  /* uses of this value in Phi, linked through use_chain */
	gear_ssa_phi          *sym_use_chain;  /* uses of this value in Pi constraints */
	unsigned int           no_val : 1;     /* value doesn't mater (used as op1 in GEAR_ASSIGN) */
	unsigned int           scc_entry : 1;
	unsigned int           alias : 2;  /* value may be changed indirectly */
	unsigned int           escape_state : 2;
} gear_ssa_var;

typedef struct _gear_ssa_var_info {
	uint32_t               type; /* inferred type (see gear_inference.h) */
	gear_ssa_range         range;
	gear_class_entry      *ce;
	unsigned int           has_range : 1;
	unsigned int           is_instanceof : 1; /* 0 - class == "ce", 1 - may be child of "ce" */
	unsigned int           recursive : 1;
	unsigned int           use_as_double : 1;
} gear_ssa_var_info;

typedef struct _gear_ssa {
	gear_cfg               cfg;            /* control flow graph             */
	int                    rt_constants;   /* run-time or compile-time       */
	int                    vars_count;     /* number of SSA variables        */
	gear_ssa_block        *blocks;         /* array of SSA blocks            */
	gear_ssa_op           *ops;            /* array of SSA instructions      */
	gear_ssa_var          *vars;           /* use/def chain of SSA variables */
	int                    sccs;           /* number of SCCs                 */
	gear_ssa_var_info     *var_info;
} gear_ssa;

BEGIN_EXTERN_C()

int gear_build_ssa(gear_arena **arena, const gear_script *script, const gear_op_array *op_array, uint32_t build_flags, gear_ssa *ssa);
int gear_ssa_compute_use_def_chains(gear_arena **arena, const gear_op_array *op_array, gear_ssa *ssa);
int gear_ssa_unlink_use_chain(gear_ssa *ssa, int op, int var);

void gear_ssa_remove_predecessor(gear_ssa *ssa, int from, int to);
void gear_ssa_remove_instr(gear_ssa *ssa, gear_op *opline, gear_ssa_op *ssa_op);
void gear_ssa_remove_phi(gear_ssa *ssa, gear_ssa_phi *phi);
void gear_ssa_remove_uses_of_var(gear_ssa *ssa, int var_num);
void gear_ssa_remove_block(gear_op_array *op_array, gear_ssa *ssa, int b);
void gear_ssa_rename_var_uses(gear_ssa *ssa, int old_var, int new_var, gear_bool update_types);

static gear_always_inline void _gear_ssa_remove_def(gear_ssa_var *var)
{
	GEAR_ASSERT(var->definition >= 0);
	GEAR_ASSERT(var->use_chain < 0);
	GEAR_ASSERT(!var->phi_use_chain);
	var->definition = -1;
}

static gear_always_inline void gear_ssa_remove_result_def(gear_ssa *ssa, gear_ssa_op *ssa_op)
{
	gear_ssa_var *var = &ssa->vars[ssa_op->result_def];
	_gear_ssa_remove_def(var);
	ssa_op->result_def = -1;
}

static gear_always_inline void gear_ssa_remove_op1_def(gear_ssa *ssa, gear_ssa_op *ssa_op)
{
	gear_ssa_var *var = &ssa->vars[ssa_op->op1_def];
	_gear_ssa_remove_def(var);
	ssa_op->op1_def = -1;
}

static gear_always_inline void gear_ssa_remove_op2_def(gear_ssa *ssa, gear_ssa_op *ssa_op)
{
	gear_ssa_var *var = &ssa->vars[ssa_op->op2_def];
	_gear_ssa_remove_def(var);
	ssa_op->op2_def = -1;
}

END_EXTERN_C()

static gear_always_inline int gear_ssa_next_use(const gear_ssa_op *ssa_op, int var, int use)
{
	ssa_op += use;
	if (ssa_op->op1_use == var) {
		return ssa_op->op1_use_chain;
	} else if (ssa_op->op2_use == var) {
		return ssa_op->op2_use_chain;
	} else {
		return ssa_op->res_use_chain;
	}
}

static gear_always_inline gear_ssa_phi* gear_ssa_next_use_phi(const gear_ssa *ssa, int var, const gear_ssa_phi *p)
{
	if (p->pi >= 0) {
		return p->use_chains[0];
	} else {
		int j;
		for (j = 0; j < ssa->cfg.blocks[p->block].predecessors_count; j++) {
			if (p->sources[j] == var) {
				return p->use_chains[j];
			}
		}
	}
	return NULL;
}

static gear_always_inline gear_bool gear_ssa_is_no_val_use(const gear_op *opline, const gear_ssa_op *ssa_op, int var)
{
	if (opline->opcode == GEAR_ASSIGN || opline->opcode == GEAR_UNSET_CV) {
		return ssa_op->op1_use == var && ssa_op->op2_use != var;
	}
	if (opline->opcode == GEAR_FE_FETCH_R) {
		return ssa_op->op2_use == var && ssa_op->op1_use != var;
	}
	if (ssa_op->result_use == var && opline->opcode != GEAR_ADD_ARRAY_ELEMENT) {
		return ssa_op->op1_use != var && ssa_op->op2_use != var;
	}
	return 0;
}

static gear_always_inline void gear_ssa_rename_defs_of_instr(gear_ssa *ssa, gear_ssa_op *ssa_op) {
	/* Rename def to use if possible. Mark variable as not defined otherwise. */
	if (ssa_op->op1_def >= 0) {
		if (ssa_op->op1_use >= 0) {
			gear_ssa_rename_var_uses(ssa, ssa_op->op1_def, ssa_op->op1_use, 1);
		}
		ssa->vars[ssa_op->op1_def].definition = -1;
		ssa_op->op1_def = -1;
	}
	if (ssa_op->op2_def >= 0) {
		if (ssa_op->op2_use >= 0) {
			gear_ssa_rename_var_uses(ssa, ssa_op->op2_def, ssa_op->op2_use, 1);
		}
		ssa->vars[ssa_op->op2_def].definition = -1;
		ssa_op->op2_def = -1;
	}
	if (ssa_op->result_def >= 0) {
		if (ssa_op->result_use >= 0) {
			gear_ssa_rename_var_uses(ssa, ssa_op->result_def, ssa_op->result_use, 1);
		}
		ssa->vars[ssa_op->result_def].definition = -1;
		ssa_op->result_def = -1;
	}
}

#define NUM_PHI_SOURCES(phi) \
	((phi)->pi >= 0 ? 1 : (ssa->cfg.blocks[(phi)->block].predecessors_count))

/* FOREACH_USE and FOREACH_PHI_USE explicitly support "continue"
 * and changing the use chain of the current element */
#define FOREACH_USE(var, use) do { \
	int _var_num = (var) - ssa->vars, next; \
	for (use = (var)->use_chain; use >= 0; use = next) { \
		next = gear_ssa_next_use(ssa->ops, _var_num, use);
#define FOREACH_USE_END() \
	} \
} while (0)

#define FOREACH_PHI_USE(var, phi) do { \
	int _var_num = (var) - ssa->vars; \
	gear_ssa_phi *next_phi; \
	for (phi = (var)->phi_use_chain; phi; phi = next_phi) { \
		next_phi = gear_ssa_next_use_phi(ssa, _var_num, phi);
#define FOREACH_PHI_USE_END() \
	} \
} while (0)

#define FOREACH_PHI_SOURCE(phi, source) do { \
	gear_ssa_phi *_phi = (phi); \
	int _i, _end = NUM_PHI_SOURCES(phi); \
	for (_i = 0; _i < _end; _i++) { \
		GEAR_ASSERT(_phi->sources[_i] >= 0); \
		source = _phi->sources[_i];
#define FOREACH_PHI_SOURCE_END() \
	} \
} while (0)

#define FOREACH_PHI(phi) do { \
	int _i; \
	for (_i = 0; _i < ssa->cfg.blocks_count; _i++) { \
		phi = ssa->blocks[_i].phis; \
		for (; phi; phi = phi->next) {
#define FOREACH_PHI_END() \
		} \
	} \
} while (0)

#define FOREACH_BLOCK(block) do { \
	int _i; \
	for (_i = 0; _i < ssa->cfg.blocks_count; _i++) { \
		(block) = &ssa->cfg.blocks[_i]; \
		if (!((block)->flags & GEAR_BB_REACHABLE)) { \
			continue; \
		}
#define FOREACH_BLOCK_END() \
	} \
} while (0)

/* Does not support "break" */
#define FOREACH_INSTR_NUM(i) do { \
	gear_basic_block *_block; \
	FOREACH_BLOCK(_block) { \
		uint32_t _end = _block->start + _block->len; \
		for ((i) = _block->start; (i) < _end; (i)++) {
#define FOREACH_INSTR_NUM_END() \
		} \
	} FOREACH_BLOCK_END(); \
} while (0)

#endif /* GEAR_SSA_H */

