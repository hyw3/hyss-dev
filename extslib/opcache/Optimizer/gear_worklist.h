/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GEAR_WORKLIST_H_
#define _GEAR_WORKLIST_H_

#include "gear_arena.h"
#include "gear_bitset.h"

typedef struct _gear_worklist_stack {
	int *buf;
	int len;
	int capacity;
} gear_worklist_stack;

#define GEAR_WORKLIST_STACK_ALLOCA(s, _len, use_heap) do { \
		(s)->buf = (int*)do_alloca(sizeof(int) * _len, use_heap); \
		(s)->len = 0; \
		(s)->capacity = _len; \
	} while (0)

#define GEAR_WORKLIST_STACK_FREE_ALLOCA(s, use_heap) \
	free_alloca((s)->buf, use_heap)

static inline int gear_worklist_stack_prepare(gear_arena **arena, gear_worklist_stack *stack, int len)
{
	GEAR_ASSERT(len >= 0);

	stack->buf = (int*)gear_arena_calloc(arena, sizeof(*stack->buf), len);
	stack->len = 0;
	stack->capacity = len;

	return SUCCESS;
}

static inline void gear_worklist_stack_push(gear_worklist_stack *stack, int i)
{
	GEAR_ASSERT(stack->len < stack->capacity);
	stack->buf[stack->len++] = i;
}

static inline int gear_worklist_stack_peek(gear_worklist_stack *stack)
{
	GEAR_ASSERT(stack->len);
	return stack->buf[stack->len - 1];
}

static inline int gear_worklist_stack_pop(gear_worklist_stack *stack)
{
	GEAR_ASSERT(stack->len);
	return stack->buf[--stack->len];
}

typedef struct _gear_worklist {
	gear_bitset visited;
	gear_worklist_stack stack;
} gear_worklist;

#define GEAR_WORKLIST_ALLOCA(w, _len, use_heap) do { \
		(w)->stack.buf = (int*)do_alloca(GEAR_MM_ALIGNED_SIZE(sizeof(int) * _len) + sizeof(gear_ulong) * gear_bitset_len(_len), use_heap); \
		(w)->stack.len = 0; \
		(w)->stack.capacity = _len; \
		(w)->visited = (gear_bitset)((char*)(w)->stack.buf + GEAR_MM_ALIGNED_SIZE(sizeof(int) * _len)); \
		memset((w)->visited, 0, sizeof(gear_ulong) * gear_bitset_len(_len)); \
	} while (0)

#define GEAR_WORKLIST_FREE_ALLOCA(w, use_heap) \
	free_alloca((w)->stack.buf, use_heap)

static inline int gear_worklist_prepare(gear_arena **arena, gear_worklist *worklist, int len)
{
	GEAR_ASSERT(len >= 0);
	worklist->visited = (gear_bitset)gear_arena_calloc(arena, sizeof(gear_ulong), gear_bitset_len(len));
	return gear_worklist_stack_prepare(arena, &worklist->stack, len);
}

static inline int gear_worklist_len(gear_worklist *worklist)
{
	return worklist->stack.len;
}

static inline int gear_worklist_push(gear_worklist *worklist, int i)
{
	GEAR_ASSERT(i >= 0 && i < worklist->stack.capacity);

	if (gear_bitset_in(worklist->visited, i)) {
		return 0;
	}

	gear_bitset_incl(worklist->visited, i);
	gear_worklist_stack_push(&worklist->stack, i);
	return 1;
}

static inline int gear_worklist_peek(gear_worklist *worklist)
{
	return gear_worklist_stack_peek(&worklist->stack);
}

static inline int gear_worklist_pop(gear_worklist *worklist)
{
	/* Does not clear visited flag */
	return gear_worklist_stack_pop(&worklist->stack);
}

#endif /* _GEAR_WORKLIST_H_ */

