/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_DFG_H
#define GEAR_DFG_H

#include "gear_bitset.h"
#include "gear_cfg.h"

typedef struct _gear_dfg {
	int         vars;
	uint32_t    size;
	gear_bitset tmp;
	gear_bitset def;
	gear_bitset use;
	gear_bitset in;
	gear_bitset out;
} gear_dfg;

#define DFG_BITSET(set, set_size, block_num) \
	((set) + ((block_num) * (set_size)))

#define DFG_SET(set, set_size, block_num, var_num) \
	gear_bitset_incl(DFG_BITSET(set, set_size, block_num), (var_num))

#define DFG_ISSET(set, set_size, block_num, var_num) \
	gear_bitset_in(DFG_BITSET(set, set_size, block_num), (var_num))

BEGIN_EXTERN_C()

int gear_build_dfg(const gear_op_array *op_array, const gear_cfg *cfg, gear_dfg *dfg, uint32_t build_flags);

END_EXTERN_C()

#endif /* GEAR_DFG_H */

