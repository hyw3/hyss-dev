/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GearAccelerator.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "Optimizer/scdf.h"

/* This defines a generic framework for sparse conditional dataflow propagation. The algorithm is
 * based on "Sparse conditional constant propagation" by Wegman and Zadeck. We're using a
 * generalized implementation as described in chapter 8.3 of the SSA book.
 *
 * Every SSA variable is associated with an element on a finite-height lattice, those value can only
 * ever be lowered during the operation of the algorithm. If a value is lowered all instructions and
 * phis using that value need to be reconsidered (this is done by adding the variable to a
 * worklist). For phi functions the result is computed by applying the meet operation to the
 * operands. This continues until a fixed point is reached.
 *
 * The algorithm is control-flow sensitive: All blocks except the start block are initially assumed
 * to be unreachable. When considering a branch instruction, we determine the feasible successors
 * based on the current state of the variable lattice. If a new edge becomes feasible we either have
 * to mark the successor block executable and consider all instructions in it, or, if the target is
 * already executable, we only have to reconsider the phi functions (as we only consider phi
 * operands which are associated with a feasible edge).
 *
 * The generic framework requires the definition of three functions:
 * * visit_instr() should recompute the lattice values of all SSA variables defined by an
 *   instruction.
 * * visit_phi() should recompute the lattice value of the SSA variable defined by the phi. While
 *   doing this it should only consider operands for which scfg_is_edge_feasible() returns true.
 * * get_feasible_successors() should determine the feasible successors for a branch instruction.
 *   Note that this callback only needs to handle conditional branches (with two successors).
 */

#if 0
#define DEBUG_PRINT(...) fprintf(stderr, __VA_ARGS__)
#else
#define DEBUG_PRINT(...)
#endif

void scdf_mark_edge_feasible(scdf_ctx *scdf, int from, int to) {
	uint32_t edge = scdf_edge(&scdf->ssa->cfg, from, to);

	if (gear_bitset_in(scdf->feasible_edges, edge)) {
		/* We already handled this edge */
		return;
	}

	DEBUG_PRINT("Marking edge %d->%d feasible\n", from, to);
	gear_bitset_incl(scdf->feasible_edges, edge);

	if (!gear_bitset_in(scdf->executable_blocks, to)) {
		if (!gear_bitset_in(scdf->block_worklist, to)) {
			DEBUG_PRINT("Adding block %d to worklist\n", to);
		}
		gear_bitset_incl(scdf->block_worklist, to);
	} else {
		/* Block is already executable, only a new edge became feasible.
		 * Reevaluate phi nodes to account for changed source operands. */
		gear_ssa_block *ssa_block = &scdf->ssa->blocks[to];
		gear_ssa_phi *phi;
		for (phi = ssa_block->phis; phi; phi = phi->next) {
			gear_bitset_excl(scdf->phi_var_worklist, phi->ssa_var);
			scdf->handlers.visit_phi(scdf, phi);
		}
	}
}

void scdf_init(gear_optimizer_ctx *ctx, scdf_ctx *scdf, gear_op_array *op_array, gear_ssa *ssa) {
	scdf->op_array = op_array;
	scdf->ssa = ssa;

	scdf->instr_worklist_len = gear_bitset_len(op_array->last);
	scdf->phi_var_worklist_len = gear_bitset_len(ssa->vars_count);
	scdf->block_worklist_len = gear_bitset_len(ssa->cfg.blocks_count);

	scdf->instr_worklist = gear_arena_calloc(&ctx->arena,
		scdf->instr_worklist_len + scdf->phi_var_worklist_len + 2 * scdf->block_worklist_len + gear_bitset_len(ssa->cfg.edges_count),
		sizeof(gear_ulong));

	scdf->phi_var_worklist = scdf->instr_worklist + scdf->instr_worklist_len;
	scdf->block_worklist = scdf->phi_var_worklist + scdf->phi_var_worklist_len;
	scdf->executable_blocks = scdf->block_worklist + scdf->block_worklist_len;
	scdf->feasible_edges = scdf->executable_blocks + scdf->block_worklist_len;

	gear_bitset_incl(scdf->block_worklist, 0);
	gear_bitset_incl(scdf->executable_blocks, 0);
}

void scdf_solve(scdf_ctx *scdf, const char *name) {
	gear_ssa *ssa = scdf->ssa;
	DEBUG_PRINT("Start SCDF solve (%s)\n", name);
	while (!gear_bitset_empty(scdf->instr_worklist, scdf->instr_worklist_len)
		|| !gear_bitset_empty(scdf->phi_var_worklist, scdf->phi_var_worklist_len)
		|| !gear_bitset_empty(scdf->block_worklist, scdf->block_worklist_len)
	) {
		int i;
		while ((i = gear_bitset_pop_first(scdf->phi_var_worklist, scdf->phi_var_worklist_len)) >= 0) {
			gear_ssa_phi *phi = ssa->vars[i].definition_phi;
			GEAR_ASSERT(phi);
			if (gear_bitset_in(scdf->executable_blocks, phi->block)) {
				scdf->handlers.visit_phi(scdf, phi);
			}
		}

		while ((i = gear_bitset_pop_first(scdf->instr_worklist, scdf->instr_worklist_len)) >= 0) {
			int block_num = ssa->cfg.map[i];
			if (gear_bitset_in(scdf->executable_blocks, block_num)) {
				gear_basic_block *block = &ssa->cfg.blocks[block_num];
				gear_op *opline = &scdf->op_array->opcodes[i];
				gear_ssa_op *ssa_op = &ssa->ops[i];
				if (opline->opcode == GEAR_OP_DATA) {
					opline--;
					ssa_op--;
				}
				scdf->handlers.visit_instr(scdf, opline, ssa_op);
				if (i == block->start + block->len - 1) {
					if (block->successors_count == 1) {
						scdf_mark_edge_feasible(scdf, block_num, block->successors[0]);
					} else if (block->successors_count > 1) {
						scdf->handlers.mark_feasible_successors(scdf, block_num, block, opline, ssa_op);
					}
				}
			}
		}

		while ((i = gear_bitset_pop_first(scdf->block_worklist, scdf->block_worklist_len)) >= 0) {
			/* This block is now live. Interpret phis and instructions in it. */
			gear_basic_block *block = &ssa->cfg.blocks[i];
			gear_ssa_block *ssa_block = &ssa->blocks[i];

			DEBUG_PRINT("Pop block %d from worklist\n", i);
			gear_bitset_incl(scdf->executable_blocks, i);

			{
				gear_ssa_phi *phi;
				for (phi = ssa_block->phis; phi; phi = phi->next) {
					gear_bitset_excl(scdf->phi_var_worklist, phi->ssa_var);
					scdf->handlers.visit_phi(scdf, phi);
				}
			}

			if (block->len == 0) {
				/* Zero length blocks don't have a last instruction that would normally do this */
				scdf_mark_edge_feasible(scdf, i, block->successors[0]);
			} else {
				gear_op *opline;
				int j, end = block->start + block->len;
				for (j = block->start; j < end; j++) {
					opline = &scdf->op_array->opcodes[j];
					gear_bitset_excl(scdf->instr_worklist, j);
					if (opline->opcode != GEAR_OP_DATA) {
						scdf->handlers.visit_instr(scdf, opline, &ssa->ops[j]);
					}
				}
				if (block->successors_count == 1) {
					scdf_mark_edge_feasible(scdf, i, block->successors[0]);
				} else if (block->successors_count > 1) {
					if (opline->opcode == GEAR_OP_DATA) {
						opline--;
						j--;
					}
					scdf->handlers.mark_feasible_successors(scdf, i, block, opline, &ssa->ops[j-1]);
				}
			}
		}
	}
}

/* If a live range starts in a reachable block and ends in an unreachable block, we should
 * not eliminate the latter. While it cannot be reached, the FREE opcode of the loop var
 * is necessary for the correctness of temporary compaction. */
static gear_bool kept_alive_by_live_range(scdf_ctx *scdf, uint32_t block) {
	uint32_t i;
	const gear_op_array *op_array = scdf->op_array;
	const gear_cfg *cfg = &scdf->ssa->cfg;
	for (i = 0; i < op_array->last_live_range; i++) {
		gear_live_range *live_range = &op_array->live_range[i];
		uint32_t start_block = cfg->map[live_range->start];
		uint32_t end_block = cfg->map[live_range->end];

		if (end_block == block && start_block != block
				&& gear_bitset_in(scdf->executable_blocks, start_block)) {
			return 1;
		}
	}
	return 0;
}

/* Removes unreachable blocks. This will remove both the instructions (and phis) in the
 * blocks, as well as remove them from the successor / predecessor lists and mark them
 * unreachable. Blocks already marked unreachable are not removed. */
int scdf_remove_unreachable_blocks(scdf_ctx *scdf) {
	gear_ssa *ssa = scdf->ssa;
	int i;
	int removed_ops = 0;

	for (i = 0; i < ssa->cfg.blocks_count; i++) {
		if (!gear_bitset_in(scdf->executable_blocks, i)
				&& (ssa->cfg.blocks[i].flags & GEAR_BB_REACHABLE)
				&& !kept_alive_by_live_range(scdf, i)) {
			removed_ops += ssa->cfg.blocks[i].len;
			gear_ssa_remove_block(scdf->op_array, ssa, i);
		}
	}
	return removed_ops;
}
