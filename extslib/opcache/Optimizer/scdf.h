/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SCDF_H
#define _SCDF_H

#include "gear_bitset.h"

typedef struct _scdf_ctx {
	gear_op_array *op_array;
	gear_ssa *ssa;
	gear_bitset instr_worklist;
	/* Represent phi-instructions through the defining var */
	gear_bitset phi_var_worklist;
	gear_bitset block_worklist;
	gear_bitset executable_blocks;
	/* 1 bit per edge, see scdf_edge(cfg, from, to) */
	gear_bitset feasible_edges;
	uint32_t instr_worklist_len;
	uint32_t phi_var_worklist_len;
	uint32_t block_worklist_len;

	struct {
		void (*visit_instr)(
			struct _scdf_ctx *scdf, gear_op *opline, gear_ssa_op *ssa_op);
		void (*visit_phi)(
			struct _scdf_ctx *scdf, gear_ssa_phi *phi);
		void (*mark_feasible_successors)(
			struct _scdf_ctx *scdf, int block_num, gear_basic_block *block,
			gear_op *opline, gear_ssa_op *ssa_op);
	} handlers;
} scdf_ctx;

void scdf_init(gear_optimizer_ctx *ctx, scdf_ctx *scdf, gear_op_array *op_array, gear_ssa *ssa);
void scdf_solve(scdf_ctx *scdf, const char *name);

int scdf_remove_unreachable_blocks(scdf_ctx *scdf);

/* Add uses to worklist */
static inline void scdf_add_to_worklist(scdf_ctx *scdf, int var_num) {
	gear_ssa *ssa = scdf->ssa;
	gear_ssa_var *var = &ssa->vars[var_num];
	int use;
	gear_ssa_phi *phi;
	FOREACH_USE(var, use) {
		gear_bitset_incl(scdf->instr_worklist, use);
	} FOREACH_USE_END();
	FOREACH_PHI_USE(var, phi) {
		gear_bitset_incl(scdf->phi_var_worklist, phi->ssa_var);
	} FOREACH_PHI_USE_END();
}

/* This should usually not be necessary, however it's used for type narrowing. */
static inline void scdf_add_def_to_worklist(scdf_ctx *scdf, int var_num) {
	gear_ssa_var *var = &scdf->ssa->vars[var_num];
	if (var->definition >= 0) {
		gear_bitset_incl(scdf->instr_worklist, var->definition);
	} else if (var->definition_phi) {
		gear_bitset_incl(scdf->phi_var_worklist, var_num);
	}
}

static inline uint32_t scdf_edge(gear_cfg *cfg, int from, int to) {
	gear_basic_block *to_block = cfg->blocks + to;
	int i;

	for (i = 0; i < to_block->predecessors_count; i++) {
		uint32_t edge = to_block->predecessor_offset + i;

		if (cfg->predecessors[edge] == from) {
			return edge;
		}
	}
	GEAR_ASSERT(0);
}

static inline gear_bool scdf_is_edge_feasible(scdf_ctx *scdf, int from, int to) {
	uint32_t edge = scdf_edge(&scdf->ssa->cfg, from, to);
	return gear_bitset_in(scdf->feasible_edges, edge);
}

void scdf_mark_edge_feasible(scdf_ctx *scdf, int from, int to);

#endif
