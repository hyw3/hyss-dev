/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_API.h"
#include "gear_constants.h"
#include "gear_execute.h"
#include "gear_vm.h"
#include "gear_bitset.h"

#define GET_AVAILABLE_T()					\
	for (i = 0; i < T; i++) {				\
		if (!gear_bitset_in(taken_T, i)) {	\
			break;							\
		}									\
	}										\
	gear_bitset_incl(taken_T, i);			\
	if (i > max) {							\
		max = i;							\
	}

void gear_optimize_temporary_variables(gear_op_array *op_array, gear_optimizer_ctx *ctx)
{
	int T = op_array->T;
	int offset = op_array->last_var;
	uint32_t bitset_len;
	gear_bitset taken_T;	/* T index in use */
	gear_op **start_of_T;	/* opline where T is first used */
	gear_bitset valid_T;	/* Is the map_T valid */
	int *map_T;				/* Map's the T to its new index */
	gear_op *opline, *end;
	int currT;
	int i;
	int max = -1;
	int var_to_free = -1;
	void *checkpoint = gear_arena_checkpoint(ctx->arena);

	bitset_len = gear_bitset_len(T);
	taken_T = (gear_bitset) gear_arena_alloc(&ctx->arena, bitset_len * GEAR_BITSET_ELM_SIZE);
	start_of_T = (gear_op **) gear_arena_alloc(&ctx->arena, T * sizeof(gear_op *));
	valid_T = (gear_bitset) gear_arena_alloc(&ctx->arena, bitset_len * GEAR_BITSET_ELM_SIZE);
	map_T = (int *) gear_arena_alloc(&ctx->arena, T * sizeof(int));

    end = op_array->opcodes;
    opline = &op_array->opcodes[op_array->last - 1];

    /* Find T definition points */
    while (opline >= end) {
        if (opline->result_type & (IS_VAR | IS_TMP_VAR)) {
			start_of_T[VAR_NUM(opline->result.var) - offset] = opline;
		}
		opline--;
	}

	gear_bitset_clear(valid_T, bitset_len);
	gear_bitset_clear(taken_T, bitset_len);

    end = op_array->opcodes;
    opline = &op_array->opcodes[op_array->last - 1];

    while (opline >= end) {
		if ((opline->op1_type & (IS_VAR | IS_TMP_VAR))) {
			currT = VAR_NUM(opline->op1.var) - offset;
			if (opline->opcode == GEAR_ROPE_END) {
				int num = (((opline->extended_value + 1) * sizeof(gear_string*)) + (sizeof(zval) - 1)) / sizeof(zval);
				int var;

				var = max;
				while (var >= 0 && !gear_bitset_in(taken_T, var)) {
					var--;
				}
				max = MAX(max, var + num);
				var = var + 1;
				map_T[currT] = var;
				gear_bitset_incl(valid_T, currT);
				gear_bitset_incl(taken_T, var);
				opline->op1.var = NUM_VAR(var + offset);
				while (num > 1) {
					num--;
					gear_bitset_incl(taken_T, var + num);
				}
			} else {
				if (!gear_bitset_in(valid_T, currT)) {
					int use_new_var = 0;

					/* Code in "finally" blocks may modify temorary variables.
					 * We allocate new temporaries for values that need to
					 * relive FAST_CALLs.
					 */
					if ((op_array->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK) &&
					    (opline->opcode == GEAR_RETURN ||
					     opline->opcode == GEAR_GENERATOR_RETURN ||
					     opline->opcode == GEAR_RETURN_BY_REF ||
					     opline->opcode == GEAR_FREE ||
					     opline->opcode == GEAR_FE_FREE)) {
						gear_op *curr = opline;

						while (--curr >= end) {
							if (curr->opcode == GEAR_FAST_CALL) {
								use_new_var = 1;
								break;
							} else if (curr->opcode != GEAR_FREE &&
							           curr->opcode != GEAR_FE_FREE &&
							           curr->opcode != GEAR_VERIFY_RETURN_TYPE &&
							           curr->opcode != GEAR_DISCARD_EXCEPTION) {
								break;
							}
						}
					}
					if (use_new_var) {
						i = ++max;
						gear_bitset_incl(taken_T, i);
					} else {
						GET_AVAILABLE_T();
					}
					map_T[currT] = i;
					gear_bitset_incl(valid_T, currT);
				}
				opline->op1.var = NUM_VAR(map_T[currT] + offset);
			}
		}

		if ((opline->op2_type & (IS_VAR | IS_TMP_VAR))) {
			currT = VAR_NUM(opline->op2.var) - offset;
			if (!gear_bitset_in(valid_T, currT)) {
				GET_AVAILABLE_T();
				map_T[currT] = i;
				gear_bitset_incl(valid_T, currT);
			}
			opline->op2.var = NUM_VAR(map_T[currT] + offset);
		}

		if (opline->result_type & (IS_VAR | IS_TMP_VAR)) {
			currT = VAR_NUM(opline->result.var) - offset;
			if (gear_bitset_in(valid_T, currT)) {
				if (start_of_T[currT] == opline) {
					/* GEAR_FAST_CALL can not share temporary var with others
					 * since the fast_var could also be set by GEAR_HANDLE_EXCEPTION
					 * which could be ahead of it */
					if (opline->opcode != GEAR_FAST_CALL) {
						gear_bitset_excl(taken_T, map_T[currT]);
					}
				}
				opline->result.var = NUM_VAR(map_T[currT] + offset);
				if (opline->opcode == GEAR_ROPE_INIT) {
					if (start_of_T[currT] == opline) {
						uint32_t num = ((opline->extended_value * sizeof(gear_string*)) + (sizeof(zval) - 1)) / sizeof(zval);
						while (num > 1) {
							num--;
							gear_bitset_excl(taken_T, map_T[currT]+num);
						}
					}
				}
			} else {
				/* Code which gets here is using a wrongly built opcode such as RECV() */
				GET_AVAILABLE_T();
				map_T[currT] = i;
				gear_bitset_incl(valid_T, currT);
				opline->result.var = NUM_VAR(i + offset);
			}
		}

		if (var_to_free >= 0) {
			gear_bitset_excl(taken_T, var_to_free);
			var_to_free = -1;
		}

		opline--;
	}

	if (op_array->live_range) {
		for (i = 0; i < op_array->last_live_range; i++) {
			op_array->live_range[i].var =
				NUM_VAR(map_T[VAR_NUM(op_array->live_range[i].var & ~GEAR_LIVE_MASK) - offset] + offset) |
				(op_array->live_range[i].var & GEAR_LIVE_MASK);
		}
	}

	gear_arena_release(&ctx->arena, checkpoint);
	op_array->T = max + 1;
}
