/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* pass 2:
 * - convert non-numeric constants to numeric constants in numeric operators
 * - optimize constant conditional JMPs
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_API.h"
#include "gear_constants.h"
#include "gear_execute.h"
#include "gear_vm.h"

void gear_optimizer_pass2(gear_op_array *op_array)
{
	gear_op *opline;
	gear_op *end = op_array->opcodes + op_array->last;

	opline = op_array->opcodes;
	while (opline < end) {
		switch (opline->opcode) {
			case GEAR_ADD:
			case GEAR_SUB:
			case GEAR_MUL:
			case GEAR_DIV:
			case GEAR_POW:
				if (opline->op1_type == IS_CONST) {
					if (Z_TYPE(GEAR_OP1_LITERAL(opline)) == IS_STRING) {
						/* don't optimise if it should produce a runtime numeric string error */
						if (is_numeric_string(Z_STRVAL(GEAR_OP1_LITERAL(opline)), Z_STRLEN(GEAR_OP1_LITERAL(opline)), NULL, NULL, 0)) {
							convert_scalar_to_number(&GEAR_OP1_LITERAL(opline));
						}
					}
				}
				/* break missing *intentionally* - the assign_op's may only optimize op2 */
			case GEAR_ASSIGN_ADD:
			case GEAR_ASSIGN_SUB:
			case GEAR_ASSIGN_MUL:
			case GEAR_ASSIGN_DIV:
			case GEAR_ASSIGN_POW:
				if (opline->extended_value != 0) {
					/* object tristate op - don't attempt to optimize it! */
					break;
				}
				if (opline->op2_type == IS_CONST) {
					if (Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_STRING) {
						/* don't optimise if it should produce a runtime numeric string error */
						if (is_numeric_string(Z_STRVAL(GEAR_OP2_LITERAL(opline)), Z_STRLEN(GEAR_OP2_LITERAL(opline)), NULL, NULL, 0)) {
							convert_scalar_to_number(&GEAR_OP2_LITERAL(opline));
						}
					}
				}
				break;

			case GEAR_MOD:
			case GEAR_SL:
			case GEAR_SR:
				if (opline->op1_type == IS_CONST) {
					if (Z_TYPE(GEAR_OP1_LITERAL(opline)) != IS_LONG) {
						/* don't optimise if it should produce a runtime numeric string error */
						if (!(Z_TYPE(GEAR_OP1_LITERAL(opline)) == IS_STRING
							&& !is_numeric_string(Z_STRVAL(GEAR_OP1_LITERAL(opline)), Z_STRLEN(GEAR_OP1_LITERAL(opline)), NULL, NULL, 0))) {
							convert_to_long(&GEAR_OP1_LITERAL(opline));
						}
					}
				}
				/* break missing *intentionally - the assign_op's may only optimize op2 */
			case GEAR_ASSIGN_MOD:
			case GEAR_ASSIGN_SL:
			case GEAR_ASSIGN_SR:
				if (opline->extended_value != 0) {
					/* object tristate op - don't attempt to optimize it! */
					break;
				}
				if (opline->op2_type == IS_CONST) {
					if (Z_TYPE(GEAR_OP2_LITERAL(opline)) != IS_LONG) {
						/* don't optimise if it should produce a runtime numeric string error */
						if (!(Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_STRING
							&& !is_numeric_string(Z_STRVAL(GEAR_OP2_LITERAL(opline)), Z_STRLEN(GEAR_OP2_LITERAL(opline)), NULL, NULL, 0))) {
							convert_to_long(&GEAR_OP2_LITERAL(opline));
						}
					}
				}
				break;

			case GEAR_CONCAT:
			case GEAR_FAST_CONCAT:
				if (opline->op1_type == IS_CONST) {
					if (Z_TYPE(GEAR_OP1_LITERAL(opline)) != IS_STRING) {
						convert_to_string(&GEAR_OP1_LITERAL(opline));
					}
				}
				/* break missing *intentionally - the assign_op's may only optimize op2 */
			case GEAR_ASSIGN_CONCAT:
				if (opline->extended_value != 0) {
					/* object tristate op - don't attempt to optimize it! */
					break;
				}
				if (opline->op2_type == IS_CONST) {
					if (Z_TYPE(GEAR_OP2_LITERAL(opline)) != IS_STRING) {
						convert_to_string(&GEAR_OP2_LITERAL(opline));
					}
				}
				break;

			case GEAR_JMPZ_EX:
			case GEAR_JMPNZ_EX:
				/* convert Ti = JMPZ_EX(Ti, L) to JMPZ(Ti, L) */
#if 0
				/* Disabled unsafe pattern: in conjunction with
				 * GEAR_VM_SMART_BRANCH() this may improperly eliminate
				 * assignment to Ti.
				 */
				if (opline->op1_type == IS_TMP_VAR &&
				    opline->result_type == IS_TMP_VAR &&
				    opline->op1.var == opline->result.var) {
					opline->opcode -= 3;
					SET_UNUSED(opline->result);
				} else
#endif
				/* convert Ti = JMPZ_EX(C, L) => Ti = QM_ASSIGN(C)
				   in case we know it wouldn't jump */
				if (opline->op1_type == IS_CONST) {
					int should_jmp = gear_is_true(&GEAR_OP1_LITERAL(opline));
					if (opline->opcode == GEAR_JMPZ_EX) {
						should_jmp = !should_jmp;
					}
					if (!should_jmp) {
						opline->opcode = GEAR_QM_ASSIGN;
						SET_UNUSED(opline->op2);
					}
				}
				break;

			case GEAR_JMPZ:
			case GEAR_JMPNZ:
				if (opline->op1_type == IS_CONST) {
					int should_jmp = gear_is_true(&GEAR_OP1_LITERAL(opline));

					if (opline->opcode == GEAR_JMPZ) {
						should_jmp = !should_jmp;
					}
					literal_dtor(&GEAR_OP1_LITERAL(opline));
					opline->op1_type = IS_UNUSED;
					if (should_jmp) {
						opline->opcode = GEAR_JMP;
						COPY_NODE(opline->op1, opline->op2);
					} else {
						MAKE_NOP(opline);
					}
					break;
				}
				if ((opline + 1)->opcode == GEAR_JMP) {
					/* JMPZ(X, L1), JMP(L2) => JMPZNZ(X, L1, L2) */
					/* JMPNZ(X, L1), JMP(L2) => JMPZNZ(X, L2, L1) */
					if (GEAR_OP2_JMP_ADDR(opline) == GEAR_OP1_JMP_ADDR(opline + 1)) {
						/* JMPZ(X, L1), JMP(L1) => NOP, JMP(L1) */
						if (opline->op1_type == IS_CV) {
							opline->opcode = GEAR_CHECK_VAR;
							opline->op2.num = 0;
						} else if (opline->op1_type & (IS_TMP_VAR|IS_VAR)) {
							opline->opcode = GEAR_FREE;
							opline->op2.num = 0;
						} else {
							MAKE_NOP(opline);
						}
					} else {
						if (opline->opcode == GEAR_JMPZ) {
							opline->extended_value = GEAR_OPLINE_TO_OFFSET(opline, GEAR_OP1_JMP_ADDR(opline + 1));
						} else {
							opline->extended_value = GEAR_OPLINE_TO_OFFSET(opline, GEAR_OP2_JMP_ADDR(opline));
							GEAR_SET_OP_JMP_ADDR(opline, opline->op2, GEAR_OP1_JMP_ADDR(opline + 1));
						}
						opline->opcode = GEAR_JMPZNZ;
					}
				}
				break;

			case GEAR_JMPZNZ:
				if (opline->op1_type == IS_CONST) {
					gear_op *target_opline;

					if (gear_is_true(&GEAR_OP1_LITERAL(opline))) {
						target_opline = GEAR_OFFSET_TO_OPLINE(opline, opline->extended_value); /* JMPNZ */
					} else {
						target_opline = GEAR_OP2_JMP_ADDR(opline); /* JMPZ */
					}
					literal_dtor(&GEAR_OP1_LITERAL(opline));
					GEAR_SET_OP_JMP_ADDR(opline, opline->op1, target_opline);
					opline->op1_type = IS_UNUSED;
					opline->opcode = GEAR_JMP;
				}
				break;
		}
		opline++;
	}
}
