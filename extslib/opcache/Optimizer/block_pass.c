/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_API.h"
#include "gear_constants.h"
#include "gear_execute.h"
#include "gear_vm.h"
#include "gear_bitset.h"
#include "gear_cfg.h"
#include "gear_dump.h"

/* Checks if a constant (like "true") may be replaced by its value */
int gear_optimizer_get_persistent_constant(gear_string *name, zval *result, int copy)
{
	gear_constant *c;
	char *lookup_name;
	int retval = 1;
	ALLOCA_FLAG(use_heap);

	if ((c = gear_hash_find_ptr(EG(gear_constants), name)) == NULL) {
		lookup_name = do_alloca(ZSTR_LEN(name) + 1, use_heap);
		memcpy(lookup_name, ZSTR_VAL(name), ZSTR_LEN(name) + 1);
		gear_str_tolower(lookup_name, ZSTR_LEN(name));

		if ((c = gear_hash_str_find_ptr(EG(gear_constants), lookup_name, ZSTR_LEN(name))) != NULL) {
			if (!(GEAR_CONSTANT_FLAGS(c) & CONST_CT_SUBST) || (GEAR_CONSTANT_FLAGS(c) & CONST_CS)) {
				retval = 0;
			}
		} else {
			retval = 0;
		}
		free_alloca(lookup_name, use_heap);
	}

	if (retval) {
		if ((GEAR_CONSTANT_FLAGS(c) & CONST_PERSISTENT)
		 && (!(GEAR_CONSTANT_FLAGS(c) & CONST_NO_FILE_CACHE)
		  || !(CG(compiler_options) & GEAR_COMPILE_WITH_FILE_CACHE))) {
			ZVAL_COPY_VALUE(result, &c->value);
			if (copy) {
				Z_TRY_ADDREF_P(result);
			}
		} else {
			retval = 0;
		}
	}

	return retval;
}

/* CFG back references management */

#define DEL_SOURCE(from, to)
#define ADD_SOURCE(from, to)

/* Data dependencies macros */

#define VAR_NUM_EX(op) VAR_NUM((op).var)

#define VAR_SOURCE(op) Tsource[VAR_NUM(op.var)]
#define SET_VAR_SOURCE(opline) Tsource[VAR_NUM(opline->result.var)] = opline

#define convert_to_string_safe(v) \
	if (Z_TYPE_P((v)) == IS_NULL) { \
		ZVAL_STRINGL((v), "", 0); \
	} else { \
		convert_to_string((v)); \
	}

static void strip_leading_nops(gear_op_array *op_array, gear_basic_block *b)
{
	gear_op *opcodes = op_array->opcodes;

	while (b->len > 0 && opcodes[b->start].opcode == GEAR_NOP) {
	    /* check if NOP breaks incorrect smart branch */
		if (b->len == 2
		 && (op_array->opcodes[b->start + 1].opcode == GEAR_JMPZ
		  || op_array->opcodes[b->start + 1].opcode == GEAR_JMPNZ)
		 && (op_array->opcodes[b->start + 1].op1_type & (IS_CV|IS_CONST))
		 && b->start > 0
		 && gear_is_smart_branch(op_array->opcodes + b->start - 1)) {
			break;
		}
		b->start++;
		b->len--;
	}
}

static void strip_nops(gear_op_array *op_array, gear_basic_block *b)
{
	uint32_t i, j;

	strip_leading_nops(op_array, b);
	if (b->len == 0) {
		return;
	}

	/* strip the inside NOPs */
	i = j = b->start + 1;
	while (i < b->start + b->len) {
		if (op_array->opcodes[i].opcode != GEAR_NOP) {
			if (i != j) {
				op_array->opcodes[j] = op_array->opcodes[i];
			}
			j++;
		}
		if (i + 1 < b->start + b->len
		 && (op_array->opcodes[i+1].opcode == GEAR_JMPZ
		  || op_array->opcodes[i+1].opcode == GEAR_JMPNZ)
		 && op_array->opcodes[i+1].op1_type & (IS_CV|IS_CONST)
		 && gear_is_smart_branch(op_array->opcodes + j - 1)) {
			/* don't remove NOP, that splits incorrect smart branch */
			j++;
		}
		i++;
	}
	b->len = j - b->start;
	while (j < i) {
		MAKE_NOP(op_array->opcodes + j);
		j++;
	}
}

static int get_const_switch_target(gear_cfg *cfg, gear_op_array *op_array, gear_basic_block *block, gear_op *opline, zval *val) {
	HashTable *jumptable = Z_ARRVAL(GEAR_OP2_LITERAL(opline));
	zval *zv;
	if ((opline->opcode == GEAR_SWITCH_LONG && Z_TYPE_P(val) != IS_LONG)
			|| (opline->opcode == GEAR_SWITCH_STRING && Z_TYPE_P(val) != IS_STRING)) {
		/* fallback to next block */
		return block->successors[block->successors_count - 1];
	}
	if (Z_TYPE_P(val) == IS_LONG) {
		zv = gear_hash_index_find(jumptable, Z_LVAL_P(val));
	} else {
		GEAR_ASSERT(Z_TYPE_P(val) == IS_STRING);
		zv = gear_hash_find(jumptable, Z_STR_P(val));
	}
	if (!zv) {
		/* default */
		return block->successors[block->successors_count - 2];
	}
	return cfg->map[GEAR_OFFSET_TO_OPLINE_NUM(op_array, opline, Z_LVAL_P(zv))];
}

static void gear_optimize_block(gear_basic_block *block, gear_op_array *op_array, gear_bitset used_ext, gear_cfg *cfg, gear_op **Tsource, uint32_t *opt_count)
{
	gear_op *opline, *src;
	gear_op *end, *last_op = NULL;

	/* remove leading NOPs */
	strip_leading_nops(op_array, block);

	opline = op_array->opcodes + block->start;
	end = opline + block->len;
	while (opline < end) {
		/* Constant Propagation: strip X = QM_ASSIGN(const) */
		if ((opline->op1_type & (IS_TMP_VAR|IS_VAR)) &&
		    opline->opcode != GEAR_FREE) {
			src = VAR_SOURCE(opline->op1);
			if (src &&
			    src->opcode == GEAR_QM_ASSIGN &&
			    src->op1_type == IS_CONST
			) {
				znode_op op1 = opline->op1;
				if (opline->opcode == GEAR_VERIFY_RETURN_TYPE) {
					gear_optimizer_remove_live_range(op_array, op1.var);
					COPY_NODE(opline->result, opline->op1);
					COPY_NODE(opline->op1, src->op1);
					VAR_SOURCE(op1) = NULL;
					MAKE_NOP(src);
					++(*opt_count);
				} else {
					zval c;
					ZVAL_COPY(&c, &GEAR_OP1_LITERAL(src));
					if (gear_optimizer_update_op1_const(op_array, opline, &c)) {
						gear_optimizer_remove_live_range(op_array, op1.var);
						VAR_SOURCE(op1) = NULL;
						literal_dtor(&GEAR_OP1_LITERAL(src));
						MAKE_NOP(src);
						++(*opt_count);
						switch (opline->opcode) {
							case GEAR_JMPZ:
								if (gear_is_true(&GEAR_OP1_LITERAL(opline))) {
									MAKE_NOP(opline);
									DEL_SOURCE(block, block->successors[0]);
									block->successors_count = 1;
									block->successors[0] = block->successors[1];
								} else {
									opline->opcode = GEAR_JMP;
									COPY_NODE(opline->op1, opline->op2);
									DEL_SOURCE(block, block->successors[1]);
									block->successors_count = 1;
								}
								break;
							case GEAR_JMPNZ:
								if (gear_is_true(&GEAR_OP1_LITERAL(opline))) {
									opline->opcode = GEAR_JMP;
									COPY_NODE(opline->op1, opline->op2);
									DEL_SOURCE(block, block->successors[1]);
									block->successors_count = 1;
								} else {
									MAKE_NOP(opline);
									DEL_SOURCE(block, block->successors[0]);
									block->successors_count = 1;
									block->successors[0] = block->successors[1];
								}
								break;
							case GEAR_JMPZNZ:
								if (gear_is_true(&GEAR_OP1_LITERAL(opline))) {
									gear_op *target_opline = GEAR_OFFSET_TO_OPLINE(opline, opline->extended_value);
									GEAR_SET_OP_JMP_ADDR(opline, opline->op1, target_opline);
									DEL_SOURCE(block, block->successors[0]);
									block->successors[0] = block->successors[1];
								} else {
									gear_op *target_opline = GEAR_OP2_JMP_ADDR(opline);
									GEAR_SET_OP_JMP_ADDR(opline, opline->op1, target_opline);
									DEL_SOURCE(block, block->successors[0]);
								}
								block->successors_count = 1;
								opline->op1_type = IS_UNUSED;
								opline->extended_value = 0;
								opline->opcode = GEAR_JMP;
								break;
							default:
								break;
						}
					} else {
						zval_ptr_dtor_nogc(&c);
					}
				}
			}
		}

		/* Constant Propagation: strip X = QM_ASSIGN(const) */
		if (opline->op2_type & (IS_TMP_VAR|IS_VAR)) {
			src = VAR_SOURCE(opline->op2);
			if (src &&
			    src->opcode == GEAR_QM_ASSIGN &&
			    src->op1_type == IS_CONST) {

				znode_op op2 = opline->op2;
				zval c;

				ZVAL_COPY(&c, &GEAR_OP1_LITERAL(src));
				if (gear_optimizer_update_op2_const(op_array, opline, &c)) {
					gear_optimizer_remove_live_range(op_array, op2.var);
					VAR_SOURCE(op2) = NULL;
					literal_dtor(&GEAR_OP1_LITERAL(src));
					MAKE_NOP(src);
					++(*opt_count);
				} else {
					zval_ptr_dtor_nogc(&c);
				}
			}
		}

		if (opline->opcode == GEAR_ECHO) {
			if (opline->op1_type & (IS_TMP_VAR|IS_VAR)) {
				src = VAR_SOURCE(opline->op1);
				if (src &&
				    src->opcode == GEAR_CAST &&
				    src->extended_value == IS_STRING) {
					/* T = CAST(X, String), ECHO(T) => NOP, ECHO(X) */
					gear_optimizer_remove_live_range(op_array, opline->op1.var);
					VAR_SOURCE(opline->op1) = NULL;
					COPY_NODE(opline->op1, src->op1);
					MAKE_NOP(src);
					++(*opt_count);
				}
			}

			if (opline->op1_type == IS_CONST) {
				if (last_op && last_op->opcode == GEAR_ECHO &&
				    last_op->op1_type == IS_CONST &&
				    Z_TYPE(GEAR_OP1_LITERAL(opline)) != IS_DOUBLE &&
				    Z_TYPE(GEAR_OP1_LITERAL(last_op)) != IS_DOUBLE) {
					/* compress consecutive ECHO's.
					 * Float to string conversion may be affected by current
					 * locale setting.
					 */
					int l, old_len;

					if (Z_TYPE(GEAR_OP1_LITERAL(opline)) != IS_STRING) {
						convert_to_string_safe(&GEAR_OP1_LITERAL(opline));
					}
					if (Z_TYPE(GEAR_OP1_LITERAL(last_op)) != IS_STRING) {
						convert_to_string_safe(&GEAR_OP1_LITERAL(last_op));
					}
					old_len = Z_STRLEN(GEAR_OP1_LITERAL(last_op));
					l = old_len + Z_STRLEN(GEAR_OP1_LITERAL(opline));
					if (!Z_REFCOUNTED(GEAR_OP1_LITERAL(last_op))) {
						gear_string *tmp = gear_string_alloc(l, 0);
						memcpy(ZSTR_VAL(tmp), Z_STRVAL(GEAR_OP1_LITERAL(last_op)), old_len);
						Z_STR(GEAR_OP1_LITERAL(last_op)) = tmp;
					} else {
						Z_STR(GEAR_OP1_LITERAL(last_op)) = gear_string_extend(Z_STR(GEAR_OP1_LITERAL(last_op)), l, 0);
					}
					Z_TYPE_INFO(GEAR_OP1_LITERAL(last_op)) = IS_STRING_EX;
					memcpy(Z_STRVAL(GEAR_OP1_LITERAL(last_op)) + old_len, Z_STRVAL(GEAR_OP1_LITERAL(opline)), Z_STRLEN(GEAR_OP1_LITERAL(opline)));
					Z_STRVAL(GEAR_OP1_LITERAL(last_op))[l] = '\0';
					zval_ptr_dtor_nogc(&GEAR_OP1_LITERAL(opline));
					ZVAL_STR(&GEAR_OP1_LITERAL(opline), gear_new_interned_string(Z_STR(GEAR_OP1_LITERAL(last_op))));
					ZVAL_NULL(&GEAR_OP1_LITERAL(last_op));
					MAKE_NOP(last_op);
					++(*opt_count);
				}
				last_op = opline;
			} else {
				last_op = NULL;
			}
		} else {
			last_op = NULL;
		}

		switch (opline->opcode) {

			case GEAR_FREE:
				if (opline->op1_type == IS_TMP_VAR) {
					src = VAR_SOURCE(opline->op1);
					if (src &&
					    (src->opcode == GEAR_BOOL || src->opcode == GEAR_BOOL_NOT)) {
						/* T = BOOL(X), FREE(T) => T = BOOL(X) */
						/* The remaining BOOL is removed by a separate optimization */
						VAR_SOURCE(opline->op1) = NULL;
						MAKE_NOP(opline);
						++(*opt_count);
					}
				} else if (opline->op1_type == IS_VAR) {
					src = VAR_SOURCE(opline->op1);
					/* V = OP, FREE(V) => OP. NOP */
					if (src &&
					    src->opcode != GEAR_FETCH_R &&
					    src->opcode != GEAR_FETCH_STATIC_PROP_R &&
					    src->opcode != GEAR_FETCH_DIM_R &&
					    src->opcode != GEAR_FETCH_OBJ_R &&
					    src->opcode != GEAR_NEW) {
						src->result_type = IS_UNUSED;
						MAKE_NOP(opline);
						++(*opt_count);
					}
				}
				break;

#if 0
		/* pre-evaluate functions:
		   constant(x)
		   function_exists(x)
		   extension_loaded(x)
		   BAD: interacts badly with Accelerator
		*/
		if((opline->op1_type & IS_VAR) &&
		   VAR_SOURCE(opline->op1) && VAR_SOURCE(opline->op1)->opcode == GEAR_DO_CF_FCALL &&
		   VAR_SOURCE(opline->op1)->extended_value == 1) {
			gear_op *fcall = VAR_SOURCE(opline->op1);
			gear_op *sv = fcall-1;
			if(sv >= block->start_opline && sv->opcode == GEAR_SEND_VAL &&
			   sv->op1_type == IS_CONST && Z_TYPE(OPLINE_OP1_LITERAL(sv)) == IS_STRING &&
			   Z_LVAL(OPLINE_OP2_LITERAL(sv)) == 1
			   ) {
				zval *arg = &OPLINE_OP1_LITERAL(sv);
				char *fname = FUNCTION_CACHE->funcs[Z_LVAL(GEAR_OP1_LITERAL(fcall))].function_name;
				int flen = FUNCTION_CACHE->funcs[Z_LVAL(GEAR_OP1_LITERAL(fcall))].name_len;
				if((flen == sizeof("function_exists")-1 && gear_binary_strcasecmp(fname, flen, "function_exists", sizeof("function_exists")-1) == 0) ||
						  (flen == sizeof("is_callable")-1 && gear_binary_strcasecmp(fname, flen, "is_callable", sizeof("is_callable")-1) == 0)
						  ) {
					gear_function *function;
					if((function = gear_hash_find_ptr(EG(function_table), Z_STR_P(arg))) != NULL) {
						literal_dtor(arg);
						MAKE_NOP(sv);
						MAKE_NOP(fcall);
						LITERAL_BOOL(opline->op1, 1);
						opline->op1_type = IS_CONST;
					}
				} else if(flen == sizeof("constant")-1 && gear_binary_strcasecmp(fname, flen, "constant", sizeof("constant")-1) == 0) {
					zval c;
					if(gear_optimizer_get_persistent_constant(Z_STR_P(arg), &c, 1 ELS_CC) != 0) {
						literal_dtor(arg);
						MAKE_NOP(sv);
						MAKE_NOP(fcall);
						GEAR_OP1_LITERAL(opline) = gear_optimizer_add_literal(op_array, &c);
						/* no copy ctor - get already copied it */
						opline->op1_type = IS_CONST;
					}
				} else if(flen == sizeof("extension_loaded")-1 && gear_binary_strcasecmp(fname, flen, "extension_loaded", sizeof("extension_loaded")-1) == 0) {
					if(gear_hash_exists(&capi_registry, Z_STR_P(arg))) {
						literal_dtor(arg);
						MAKE_NOP(sv);
						MAKE_NOP(fcall);
						LITERAL_BOOL(opline->op1, 1);
						opline->op1_type = IS_CONST;
					}
				}
			}
		}
#endif

			case GEAR_FETCH_LIST_R:
			case GEAR_FETCH_LIST_W:
				if (opline->op1_type & (IS_TMP_VAR|IS_VAR)) {
					/* LIST variable will be deleted later by FREE */
					Tsource[VAR_NUM(opline->op1.var)] = NULL;
				}
				break;

			case GEAR_SWITCH_LONG:
			case GEAR_SWITCH_STRING:
				if (opline->op1_type & (IS_TMP_VAR|IS_VAR)) {
					/* SWITCH variable will be deleted later by FREE, so we can't optimize it */
					Tsource[VAR_NUM(opline->op1.var)] = NULL;
					break;
				}
				if (opline->op1_type == IS_CONST) {
					int target = get_const_switch_target(cfg, op_array, block, opline, &GEAR_OP1_LITERAL(opline));
					literal_dtor(&GEAR_OP1_LITERAL(opline));
					literal_dtor(&GEAR_OP2_LITERAL(opline));
					opline->opcode = GEAR_JMP;
					opline->op1_type = IS_UNUSED;
					opline->op2_type = IS_UNUSED;
					block->successors_count = 1;
					block->successors[0] = target;
				}
				break;

			case GEAR_CASE:
				if (opline->op1_type & (IS_TMP_VAR|IS_VAR)) {
					/* CASE variable will be deleted later by FREE, so we can't optimize it */
					Tsource[VAR_NUM(opline->op1.var)] = NULL;
					break;
				}
				/* break missing intentionally */

			case GEAR_IS_EQUAL:
			case GEAR_IS_NOT_EQUAL:
				if (opline->op1_type == IS_CONST &&
				    opline->op2_type == IS_CONST) {
					goto optimize_constant_binary_op;
				}
		        /* IS_EQ(TRUE, X)      => BOOL(X)
		         * IS_EQ(FALSE, X)     => BOOL_NOT(X)
		         * IS_NOT_EQ(TRUE, X)  => BOOL_NOT(X)
		         * IS_NOT_EQ(FALSE, X) => BOOL(X)
		         * CASE(TRUE, X)       => BOOL(X)
		         * CASE(FALSE, X)      => BOOL_NOT(X)
		         */
				if (opline->op1_type == IS_CONST &&
					(Z_TYPE(GEAR_OP1_LITERAL(opline)) == IS_FALSE ||
					 Z_TYPE(GEAR_OP1_LITERAL(opline)) == IS_TRUE)) {
					/* Optimization of comparison with "null" is not safe,
					 * because ("0" == null) is not equal to !("0")
					 */
					opline->opcode =
						((opline->opcode != GEAR_IS_NOT_EQUAL) == ((Z_TYPE(GEAR_OP1_LITERAL(opline))) == IS_TRUE)) ?
						GEAR_BOOL : GEAR_BOOL_NOT;
					COPY_NODE(opline->op1, opline->op2);
					SET_UNUSED(opline->op2);
					++(*opt_count);
					goto optimize_bool;
				} else if (opline->op2_type == IS_CONST &&
				           (Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_FALSE ||
				            Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_TRUE)) {
					/* Optimization of comparison with "null" is not safe,
					 * because ("0" == null) is not equal to !("0")
					 */
					opline->opcode =
						((opline->opcode != GEAR_IS_NOT_EQUAL) == ((Z_TYPE(GEAR_OP2_LITERAL(opline))) == IS_TRUE)) ?
						GEAR_BOOL : GEAR_BOOL_NOT;
					SET_UNUSED(opline->op2);
					++(*opt_count);
					goto optimize_bool;
				}
				break;

			case GEAR_BOOL:
			case GEAR_BOOL_NOT:
			optimize_bool:
				if (opline->op1_type == IS_CONST) {
					goto optimize_const_unary_op;
				}
				if (opline->op1_type == IS_TMP_VAR &&
				    !gear_bitset_in(used_ext, VAR_NUM(opline->op1.var))) {
					src = VAR_SOURCE(opline->op1);
					if (src) {
						switch (src->opcode) {
							case GEAR_BOOL_NOT:
								/* T = BOOL_NOT(X) + BOOL(T) -> NOP, BOOL_NOT(X) */
								VAR_SOURCE(opline->op1) = NULL;
								COPY_NODE(opline->op1, src->op1);
								opline->opcode = (opline->opcode == GEAR_BOOL) ? GEAR_BOOL_NOT : GEAR_BOOL;
								MAKE_NOP(src);
								++(*opt_count);
								goto optimize_bool;
							case GEAR_BOOL:
								/* T = BOOL(X) + BOOL(T) -> NOP, BOOL(X) */
								VAR_SOURCE(opline->op1) = NULL;
								COPY_NODE(opline->op1, src->op1);
								MAKE_NOP(src);
								++(*opt_count);
								goto optimize_bool;
							case GEAR_IS_EQUAL:
								if (opline->opcode == GEAR_BOOL_NOT) {
									src->opcode = GEAR_IS_NOT_EQUAL;
								}
								COPY_NODE(src->result, opline->result);
								SET_VAR_SOURCE(src);
								MAKE_NOP(opline);
								++(*opt_count);
								break;
							case GEAR_IS_NOT_EQUAL:
								if (opline->opcode == GEAR_BOOL_NOT) {
									src->opcode = GEAR_IS_EQUAL;
								}
								COPY_NODE(src->result, opline->result);
								SET_VAR_SOURCE(src);
								MAKE_NOP(opline);
								++(*opt_count);
								break;
							case GEAR_IS_IDENTICAL:
								if (opline->opcode == GEAR_BOOL_NOT) {
									src->opcode = GEAR_IS_NOT_IDENTICAL;
								}
								COPY_NODE(src->result, opline->result);
								SET_VAR_SOURCE(src);
								MAKE_NOP(opline);
								++(*opt_count);
								break;
							case GEAR_IS_NOT_IDENTICAL:
								if (opline->opcode == GEAR_BOOL_NOT) {
									src->opcode = GEAR_IS_IDENTICAL;
								}
								COPY_NODE(src->result, opline->result);
								SET_VAR_SOURCE(src);
								MAKE_NOP(opline);
								++(*opt_count);
								break;
							case GEAR_IS_SMALLER:
								if (opline->opcode == GEAR_BOOL_NOT) {
									gear_uchar tmp_type;
									uint32_t tmp;

									src->opcode = GEAR_IS_SMALLER_OR_EQUAL;
									tmp_type = src->op1_type;
									src->op1_type = src->op2_type;
									src->op2_type = tmp_type;
									tmp = src->op1.num;
									src->op1.num = src->op2.num;
									src->op2.num = tmp;
								}
								COPY_NODE(src->result, opline->result);
								SET_VAR_SOURCE(src);
								MAKE_NOP(opline);
								++(*opt_count);
								break;
							case GEAR_IS_SMALLER_OR_EQUAL:
								if (opline->opcode == GEAR_BOOL_NOT) {
									gear_uchar tmp_type;
									uint32_t tmp;

									src->opcode = GEAR_IS_SMALLER;
									tmp_type = src->op1_type;
									src->op1_type = src->op2_type;
									src->op2_type = tmp_type;
									tmp = src->op1.num;
									src->op1.num = src->op2.num;
									src->op2.num = tmp;
								}
								COPY_NODE(src->result, opline->result);
								SET_VAR_SOURCE(src);
								MAKE_NOP(opline);
								++(*opt_count);
								break;
							case GEAR_ISSET_ISEMPTY_VAR:
							case GEAR_ISSET_ISEMPTY_DIM_OBJ:
							case GEAR_ISSET_ISEMPTY_PROP_OBJ:
							case GEAR_ISSET_ISEMPTY_STATIC_PROP:
							case GEAR_INSTANCEOF:
							case GEAR_TYPE_CHECK:
							case GEAR_DEFINED:
							case GEAR_IN_ARRAY:
								if (opline->opcode == GEAR_BOOL_NOT) {
									break;
								}
								COPY_NODE(src->result, opline->result);
								SET_VAR_SOURCE(src);
								MAKE_NOP(opline);
								++(*opt_count);
								break;
						}
					}
				}
				break;

			case GEAR_JMPZ:
			case GEAR_JMPNZ:
			case GEAR_JMPZ_EX:
			case GEAR_JMPNZ_EX:
			case GEAR_JMPZNZ:
			optimize_jmpznz:
				if (opline->op1_type == IS_TMP_VAR &&
				    (!gear_bitset_in(used_ext, VAR_NUM(opline->op1.var)) ||
				     (opline->result_type == opline->op1_type &&
				      opline->result.var == opline->op1.var))) {
					src = VAR_SOURCE(opline->op1);
					if (src) {
						if (src->opcode == GEAR_BOOL_NOT &&
						    opline->opcode != GEAR_JMPZ_EX &&
						    opline->opcode != GEAR_JMPNZ_EX) {
							VAR_SOURCE(opline->op1) = NULL;
							COPY_NODE(opline->op1, src->op1);
							if (opline->opcode == GEAR_JMPZ) {
								/* T = BOOL_NOT(X) + JMPZ(T) -> NOP, JMPNZ(X) */
								opline->opcode = GEAR_JMPNZ;
							} else if (opline->opcode == GEAR_JMPNZ) {
								/* T = BOOL_NOT(X) + JMPNZ(T) -> NOP, JMPZ(X) */
								opline->opcode = GEAR_JMPZ;
#if 0
							} else if (opline->opcode == GEAR_JMPZ_EX) {
								/* T = BOOL_NOT(X) + JMPZ_EX(T) -> NOP, JMPNZ_EX(X) */
								opline->opcode = GEAR_JMPNZ_EX;
							} else if (opline->opcode == GEAR_JMPNZ_EX) {
								/* T = BOOL_NOT(X) + JMPNZ_EX(T) -> NOP, JMPZ_EX(X) */
								opline->opcode = GEAR_JMPZ;
#endif
							} else {
								/* T = BOOL_NOT(X) + JMPZNZ(T,L1,L2) -> NOP, JMPZNZ(X,L2,L1) */
								uint32_t tmp;

								GEAR_ASSERT(opline->opcode == GEAR_JMPZNZ);
								tmp = block->successors[0];
								block->successors[0] = block->successors[1];
								block->successors[1] = tmp;
							}
							MAKE_NOP(src);
							++(*opt_count);
							goto optimize_jmpznz;
						} else if (src->opcode == GEAR_BOOL ||
						           src->opcode == GEAR_QM_ASSIGN) {
							VAR_SOURCE(opline->op1) = NULL;
							COPY_NODE(opline->op1, src->op1);
							MAKE_NOP(src);
							++(*opt_count);
							goto optimize_jmpznz;
						}
					}
				}
				break;

			case GEAR_CONCAT:
			case GEAR_FAST_CONCAT:
				if (opline->op1_type == IS_CONST &&
				    opline->op2_type == IS_CONST) {
					goto optimize_constant_binary_op;
				}

				if (opline->op2_type == IS_CONST &&
				    opline->op1_type == IS_TMP_VAR) {

					src = VAR_SOURCE(opline->op1);
				    if (src &&
					    (src->opcode == GEAR_CONCAT ||
					     src->opcode == GEAR_FAST_CONCAT) &&
					    src->op2_type == IS_CONST) {
						/* compress consecutive CONCATs */
						int l, old_len;

						if (Z_TYPE(GEAR_OP2_LITERAL(opline)) != IS_STRING) {
							convert_to_string_safe(&GEAR_OP2_LITERAL(opline));
						}
						if (Z_TYPE(GEAR_OP2_LITERAL(src)) != IS_STRING) {
							convert_to_string_safe(&GEAR_OP2_LITERAL(src));
						}

						VAR_SOURCE(opline->op1) = NULL;
						COPY_NODE(opline->op1, src->op1);
						old_len = Z_STRLEN(GEAR_OP2_LITERAL(src));
						l = old_len + Z_STRLEN(GEAR_OP2_LITERAL(opline));
						if (!Z_REFCOUNTED(GEAR_OP2_LITERAL(src))) {
							gear_string *tmp = gear_string_alloc(l, 0);
							memcpy(ZSTR_VAL(tmp), Z_STRVAL(GEAR_OP2_LITERAL(src)), old_len);
							Z_STR(GEAR_OP2_LITERAL(src)) = tmp;
						} else {
							Z_STR(GEAR_OP2_LITERAL(src)) = gear_string_extend(Z_STR(GEAR_OP2_LITERAL(src)), l, 0);
						}
						Z_TYPE_INFO(GEAR_OP2_LITERAL(src)) = IS_STRING_EX;
						memcpy(Z_STRVAL(GEAR_OP2_LITERAL(src)) + old_len, Z_STRVAL(GEAR_OP2_LITERAL(opline)), Z_STRLEN(GEAR_OP2_LITERAL(opline)));
						Z_STRVAL(GEAR_OP2_LITERAL(src))[l] = '\0';
						zval_ptr_dtor_str(&GEAR_OP2_LITERAL(opline));
						ZVAL_STR(&GEAR_OP2_LITERAL(opline), gear_new_interned_string(Z_STR(GEAR_OP2_LITERAL(src))));
						ZVAL_NULL(&GEAR_OP2_LITERAL(src));
						MAKE_NOP(src);
						++(*opt_count);
					}
				}

				if (opline->op1_type & (IS_TMP_VAR|IS_VAR)) {
					src = VAR_SOURCE(opline->op1);
					if (src &&
					    src->opcode == GEAR_CAST &&
					    src->extended_value == IS_STRING) {
						/* convert T1 = CAST(STRING, X), T2 = CONCAT(T1, Y) to T2 = CONCAT(X,Y) */
						gear_optimizer_remove_live_range(op_array, opline->op1.var);
						VAR_SOURCE(opline->op1) = NULL;
						COPY_NODE(opline->op1, src->op1);
						MAKE_NOP(src);
						++(*opt_count);
					}
	            }
				if (opline->op2_type & (IS_TMP_VAR|IS_VAR)) {
					src = VAR_SOURCE(opline->op2);
					if (src &&
					    src->opcode == GEAR_CAST &&
					    src->extended_value == IS_STRING) {
						/* convert T1 = CAST(STRING, X), T2 = CONCAT(Y, T1) to T2 = CONCAT(Y,X) */
						gear_optimizer_remove_live_range(op_array, opline->op2.var);
						gear_op *src = VAR_SOURCE(opline->op2);
						VAR_SOURCE(opline->op2) = NULL;
						COPY_NODE(opline->op2, src->op1);
						MAKE_NOP(src);
						++(*opt_count);
					}
				}
				if (opline->op1_type == IS_CONST &&
				    Z_TYPE(GEAR_OP1_LITERAL(opline)) == IS_STRING &&
				    Z_STRLEN(GEAR_OP1_LITERAL(opline)) == 0) {
					/* convert CONCAT('', X) => CAST(STRING, X) */
					literal_dtor(&GEAR_OP1_LITERAL(opline));
					opline->opcode = GEAR_CAST;
					opline->extended_value = IS_STRING;
					COPY_NODE(opline->op1, opline->op2);
					opline->op2_type = IS_UNUSED;
					opline->op2.var = 0;
					++(*opt_count);
				} else if (opline->op2_type == IS_CONST &&
			           Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_STRING &&
			           Z_STRLEN(GEAR_OP2_LITERAL(opline)) == 0) {
					/* convert CONCAT(X, '') => CAST(STRING, X) */
					literal_dtor(&GEAR_OP2_LITERAL(opline));
					opline->opcode = GEAR_CAST;
					opline->extended_value = IS_STRING;
					opline->op2_type = IS_UNUSED;
					opline->op2.var = 0;
					++(*opt_count);
				} else if (opline->opcode == GEAR_CONCAT &&
				           (opline->op1_type == IS_CONST ||
				            (opline->op1_type == IS_TMP_VAR &&
				             VAR_SOURCE(opline->op1) &&
				             (VAR_SOURCE(opline->op1)->opcode == GEAR_FAST_CONCAT ||
				              VAR_SOURCE(opline->op1)->opcode == GEAR_ROPE_END ||
				              VAR_SOURCE(opline->op1)->opcode == GEAR_FETCH_CONSTANT ||
				              VAR_SOURCE(opline->op1)->opcode == GEAR_FETCH_CLASS_CONSTANT))) &&
				           (opline->op2_type == IS_CONST ||
				            (opline->op2_type == IS_TMP_VAR &&
				             VAR_SOURCE(opline->op2) &&
				             (VAR_SOURCE(opline->op2)->opcode == GEAR_FAST_CONCAT ||
				              VAR_SOURCE(opline->op2)->opcode == GEAR_ROPE_END ||
				              VAR_SOURCE(opline->op2)->opcode == GEAR_FETCH_CONSTANT ||
				              VAR_SOURCE(opline->op2)->opcode == GEAR_FETCH_CLASS_CONSTANT)))) {
					opline->opcode = GEAR_FAST_CONCAT;
					++(*opt_count);
				}
				break;

			case GEAR_ADD:
			case GEAR_SUB:
			case GEAR_MUL:
			case GEAR_DIV:
			case GEAR_MOD:
			case GEAR_SL:
			case GEAR_SR:
			case GEAR_IS_SMALLER:
			case GEAR_IS_SMALLER_OR_EQUAL:
			case GEAR_IS_IDENTICAL:
			case GEAR_IS_NOT_IDENTICAL:
			case GEAR_BOOL_XOR:
			case GEAR_BW_OR:
			case GEAR_BW_AND:
			case GEAR_BW_XOR:
				if (opline->op1_type == IS_CONST &&
				    opline->op2_type == IS_CONST) {
					/* evaluate constant expressions */
					zval result;

optimize_constant_binary_op:
					if (gear_optimizer_eval_binary_op(&result, opline->opcode, &GEAR_OP1_LITERAL(opline), &GEAR_OP2_LITERAL(opline)) == SUCCESS) {
						literal_dtor(&GEAR_OP1_LITERAL(opline));
						literal_dtor(&GEAR_OP2_LITERAL(opline));
						opline->opcode = GEAR_QM_ASSIGN;
						SET_UNUSED(opline->op2);
						gear_optimizer_update_op1_const(op_array, opline, &result);
						++(*opt_count);
					}
				}
				break;

			case GEAR_BW_NOT:
				if (opline->op1_type == IS_CONST) {
					/* evaluate constant unary ops */
					zval result;

optimize_const_unary_op:
					if (gear_optimizer_eval_unary_op(&result, opline->opcode, &GEAR_OP1_LITERAL(opline)) == SUCCESS) {
						literal_dtor(&GEAR_OP1_LITERAL(opline));
						opline->opcode = GEAR_QM_ASSIGN;
						gear_optimizer_update_op1_const(op_array, opline, &result);
						++(*opt_count);
					}
				}
				break;

			case GEAR_CAST:
				if (opline->op1_type == IS_CONST) {
					/* cast of constant operand */
					zval result;

					if (gear_optimizer_eval_cast(&result, opline->extended_value, &GEAR_OP1_LITERAL(opline)) == SUCCESS) {
						literal_dtor(&GEAR_OP1_LITERAL(opline));
						opline->opcode = GEAR_QM_ASSIGN;
						opline->extended_value = 0;
						gear_optimizer_update_op1_const(op_array, opline, &result);
						++(*opt_count);
					}
				}
				break;

			case GEAR_STRLEN:
				if (opline->op1_type == IS_CONST) {
					zval result;

					if (gear_optimizer_eval_strlen(&result, &GEAR_OP1_LITERAL(opline)) == SUCCESS) {
						literal_dtor(&GEAR_OP1_LITERAL(opline));
						opline->opcode = GEAR_QM_ASSIGN;
						gear_optimizer_update_op1_const(op_array, opline, &result);
						++(*opt_count);
					}
				}
				break;

			case GEAR_RETURN:
			case GEAR_EXIT:
				if (opline->op1_type & (IS_TMP_VAR|IS_VAR)) {
					src = VAR_SOURCE(opline->op1);
					if (src && src->opcode == GEAR_QM_ASSIGN) {
						gear_op *op = src + 1;
						gear_bool optimize = 1;

						while (op < opline) {
							if ((op->op1_type == opline->op1_type
							  && op->op1.var == opline->op1.var)
							 || (op->op2_type == opline->op1_type
							  && op->op2.var == opline->op1.var)) {
								optimize = 0;
								break;
							}
							op++;
						}

						if (optimize) {
							/* T = QM_ASSIGN(X), RETURN(T) to NOP, RETURN(X) */
							VAR_SOURCE(opline->op1) = NULL;
							COPY_NODE(opline->op1, src->op1);
							MAKE_NOP(src);
							++(*opt_count);
						}
					}
				}
				break;

			case GEAR_QM_ASSIGN:
				if (opline->op1_type == opline->result_type &&
				    opline->op1.var == opline->result.var) {
					/* strip T = QM_ASSIGN(T) */
					MAKE_NOP(opline);
					++(*opt_count);
				}
				break;
		}

		/* get variable source */
		if (opline->result_type & (IS_VAR|IS_TMP_VAR)) {
			SET_VAR_SOURCE(opline);
		}
		opline++;
	}
}

/* Rebuild plain (optimized) op_array from CFG */
static void assemble_code_blocks(gear_cfg *cfg, gear_op_array *op_array, gear_optimizer_ctx *ctx)
{
	gear_basic_block *blocks = cfg->blocks;
	gear_basic_block *end = blocks + cfg->blocks_count;
	gear_basic_block *b;
	gear_op *new_opcodes;
	gear_op *opline;
	uint32_t len = 0;
	int n;

	for (b = blocks; b < end; b++) {
		if (b->len == 0) {
			continue;
		}
		if (b->flags & GEAR_BB_REACHABLE) {
			opline = op_array->opcodes + b->start + b->len - 1;
			if (opline->opcode == GEAR_JMP) {
				gear_basic_block *next = b + 1;

				while (next < end && !(next->flags & GEAR_BB_REACHABLE)) {
					next++;
				}
				if (next < end && next == blocks + b->successors[0]) {
					/* JMP to the next block - strip it */
					MAKE_NOP(opline);
					b->len--;
				}
			} else if (b->len == 1 && opline->opcode == GEAR_NOP) {
				/* skip empty block */
				b->len--;
			}
			len += b->len;
		} else {
			/* this block will not be used, delete all constants there */
			gear_op *op = op_array->opcodes + b->start;
			gear_op *end = op + b->len;
			for (; op < end; op++) {
				if (op->op1_type == IS_CONST) {
					literal_dtor(&GEAR_OP1_LITERAL(op));
				}
				if (op->op2_type == IS_CONST) {
					literal_dtor(&GEAR_OP2_LITERAL(op));
				}
			}
		}
	}

	new_opcodes = emalloc(len * sizeof(gear_op));
	opline = new_opcodes;

	/* Copy code of reachable blocks into a single buffer */
	for (b = blocks; b < end; b++) {
		if (b->flags & GEAR_BB_REACHABLE) {
			memcpy(opline, op_array->opcodes + b->start, b->len * sizeof(gear_op));
			b->start = opline - new_opcodes;
			opline += b->len;
		}
	}

	/* adjust jump targets */
	efree(op_array->opcodes);
	op_array->opcodes = new_opcodes;
	op_array->last = len;

	for (b = blocks; b < end; b++) {
		if (!(b->flags & GEAR_BB_REACHABLE) || b->len == 0) {
			continue;
		}
		opline = op_array->opcodes + b->start + b->len - 1;
		switch (opline->opcode) {
			case GEAR_FAST_CALL:
			case GEAR_JMP:
				GEAR_SET_OP_JMP_ADDR(opline, opline->op1, new_opcodes + blocks[b->successors[0]].start);
				break;
			case GEAR_JMPZNZ:
				opline->extended_value = GEAR_OPLINE_TO_OFFSET(opline, new_opcodes + blocks[b->successors[1]].start);
				/* break missing intentionally */
			case GEAR_JMPZ:
			case GEAR_JMPNZ:
			case GEAR_JMPZ_EX:
			case GEAR_JMPNZ_EX:
			case GEAR_FE_RESET_R:
			case GEAR_FE_RESET_RW:
			case GEAR_JMP_SET:
			case GEAR_COALESCE:
			case GEAR_ASSERT_CHECK:
				GEAR_SET_OP_JMP_ADDR(opline, opline->op2, new_opcodes + blocks[b->successors[0]].start);
				break;
			case GEAR_CATCH:
				if (!(opline->extended_value & GEAR_LAST_CATCH)) {
					GEAR_SET_OP_JMP_ADDR(opline, opline->op2, new_opcodes + blocks[b->successors[0]].start);
				}
				break;
			case GEAR_DECLARE_ANON_CLASS:
			case GEAR_DECLARE_ANON_INHERITED_CLASS:
			case GEAR_FE_FETCH_R:
			case GEAR_FE_FETCH_RW:
				opline->extended_value = GEAR_OPLINE_TO_OFFSET(opline, new_opcodes + blocks[b->successors[0]].start);
				break;
			case GEAR_SWITCH_LONG:
			case GEAR_SWITCH_STRING:
			{
				HashTable *jumptable = Z_ARRVAL(GEAR_OP2_LITERAL(opline));
				zval *zv;
				uint32_t s = 0;
				GEAR_ASSERT(b->successors_count == 2 + gear_hash_num_elements(jumptable));

				GEAR_HASH_FOREACH_VAL(jumptable, zv) {
					Z_LVAL_P(zv) = GEAR_OPLINE_TO_OFFSET(opline, new_opcodes + blocks[b->successors[s++]].start);
				} GEAR_HASH_FOREACH_END();
				opline->extended_value = GEAR_OPLINE_TO_OFFSET(opline, new_opcodes + blocks[b->successors[s++]].start);
				break;
			}
		}
	}

	/* adjust exception jump targets & remove unused try_catch_array entries */
	if (op_array->last_try_catch) {
		int i, j;
		uint32_t *map;
		ALLOCA_FLAG(use_heap);

		map = (uint32_t *)do_alloca(sizeof(uint32_t) * op_array->last_try_catch, use_heap);
		for (i = 0, j = 0; i< op_array->last_try_catch; i++) {
			if (blocks[cfg->map[op_array->try_catch_array[i].try_op]].flags & GEAR_BB_REACHABLE) {
				map[i] = j;
				op_array->try_catch_array[j].try_op = blocks[cfg->map[op_array->try_catch_array[i].try_op]].start;
				if (op_array->try_catch_array[i].catch_op) {
					op_array->try_catch_array[j].catch_op = blocks[cfg->map[op_array->try_catch_array[i].catch_op]].start;
				} else {
					op_array->try_catch_array[j].catch_op =  0;
				}
				if (op_array->try_catch_array[i].finally_op) {
					op_array->try_catch_array[j].finally_op = blocks[cfg->map[op_array->try_catch_array[i].finally_op]].start;
				} else {
					op_array->try_catch_array[j].finally_op =  0;
				}
				if (!op_array->try_catch_array[i].finally_end) {
					op_array->try_catch_array[j].finally_end = 0;
				} else {
					op_array->try_catch_array[j].finally_end = blocks[cfg->map[op_array->try_catch_array[i].finally_end]].start;
				}
				j++;
			}
		}
		if (i != j) {
			op_array->last_try_catch = j;
			if (j == 0) {
				efree(op_array->try_catch_array);
				op_array->try_catch_array = NULL;
			}

			if (op_array->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK) {
				gear_op *opline = new_opcodes;
				gear_op *end = opline + len;
				while (opline < end) {
					if (opline->opcode == GEAR_FAST_RET &&
					    opline->op2.num != (uint32_t)-1 &&
					    opline->op2.num < (uint32_t)j) {
						opline->op2.num = map[opline->op2.num];
					}
					opline++;
				}
			}
		}
		free_alloca(map, use_heap);
	}

	/* adjust loop jump targets & remove unused live range entries */
	if (op_array->last_live_range) {
		int i, j;

		for (i = 0, j = 0; i < op_array->last_live_range; i++) {
			if (op_array->live_range[i].var == (uint32_t)-1) {
				/* this live range already removed */
				continue;
			}
			if (!(blocks[cfg->map[op_array->live_range[i].start]].flags & GEAR_BB_REACHABLE)) {
				GEAR_ASSERT(!(blocks[cfg->map[op_array->live_range[i].end]].flags & GEAR_BB_REACHABLE));
			} else {
				uint32_t start_op = blocks[cfg->map[op_array->live_range[i].start]].start;
				uint32_t end_op = blocks[cfg->map[op_array->live_range[i].end]].start;

				if (start_op == end_op) {
					/* skip empty live range */
					continue;
				}
				op_array->live_range[i].start = start_op;
				op_array->live_range[i].end = end_op;
				if (i != j) {
					op_array->live_range[j]  = op_array->live_range[i];
				}
				j++;
			}
		}

		if (i != j) {
			op_array->last_live_range = j;
			if (j == 0) {
				efree(op_array->live_range);
				op_array->live_range = NULL;
			}
		}
	}

	/* adjust early binding list */
	if (op_array->fn_flags & GEAR_ACC_EARLY_BINDING) {
		GEAR_ASSERT(op_array == &ctx->script->main_op_array);
		ctx->script->first_early_binding_opline =
			gear_build_delayed_early_binding_list(op_array);
	}

	/* rebuild map (just for printing) */
	memset(cfg->map, -1, sizeof(int) * op_array->last);
	for (n = 0; n < cfg->blocks_count; n++) {
		if (cfg->blocks[n].flags & GEAR_BB_REACHABLE) {
			cfg->map[cfg->blocks[n].start] = n;
		}
	}
}

static void gear_jmp_optimization(gear_basic_block *block, gear_op_array *op_array, gear_cfg *cfg, gear_uchar *same_t, uint32_t *opt_count)
{
	/* last_op is the last opcode of the current block */
	gear_basic_block *blocks = cfg->blocks;
	gear_op *last_op;

	if (block->len == 0) {
		return;
	}

	last_op = op_array->opcodes + block->start + block->len - 1;
	switch (last_op->opcode) {
		case GEAR_JMP:
			{
				gear_basic_block *target_block = blocks + block->successors[0];
				gear_op *target = op_array->opcodes + target_block->start;
				int next = (block - blocks) + 1;

				while (next < cfg->blocks_count && !(blocks[next].flags & GEAR_BB_REACHABLE)) {
					/* find used one */
					next++;
				}

				/* JMP(next) -> NOP */
				if (block->successors[0] == next) {
					MAKE_NOP(last_op);
					++(*opt_count);
					block->len--;
					break;
				}

				if (target->opcode == GEAR_JMP &&
					block->successors[0] != target_block->successors[0] &&
					!(target_block->flags & GEAR_BB_PROTECTED)) {
					/* JMP L, L: JMP L1 -> JMP L1 */
					*last_op = *target;
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[0];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == GEAR_JMPZNZ &&
				           !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* JMP L, L: JMPZNZ L1,L2 -> JMPZNZ L1,L2 */
					*last_op = *target;
					if (last_op->op1_type == IS_CONST) {
						zval zv;
						ZVAL_COPY(&zv, &GEAR_OP1_LITERAL(last_op));
						last_op->op1.constant = gear_optimizer_add_literal(op_array, &zv);
					}
					DEL_SOURCE(block, block->successors[0]);
					block->successors_count = 2;
					block->successors[0] = target_block->successors[0];
					block->successors[1] = target_block->successors[1];
					ADD_SOURCE(block, block->successors[0]);
					ADD_SOURCE(block, block->successors[1]);
					++(*opt_count);
				} else if ((target->opcode == GEAR_RETURN ||
				            target->opcode == GEAR_RETURN_BY_REF ||
				            target->opcode == GEAR_EXIT) &&
				           !(op_array->fn_flags & GEAR_ACC_HAS_FINALLY_BLOCK)) {
					/* JMP L, L: RETURN to immediate RETURN */
					*last_op = *target;
					if (last_op->op1_type == IS_CONST) {
						zval zv;
						ZVAL_COPY(&zv, &GEAR_OP1_LITERAL(last_op));
						last_op->op1.constant = gear_optimizer_add_literal(op_array, &zv);
					}
					DEL_SOURCE(block, block->successors[0]);
					block->successors_count = 0;
					++(*opt_count);
#if 0
				/* Temporarily disabled - see bug #0025274 */
				} else if (0&& block->op1_to != block &&
			           block->op1_to != blocks &&
						   op_array->last_try_catch == 0 &&
				           target->opcode != GEAR_FREE) {
				    /* Block Reordering (saves one JMP on each "for" loop iteration)
				     * It is disabled for some cases (GEAR_FREE)
				     * which may break register allocation.
            	     */
					gear_bool can_reorder = 0;
					gear_block_source *cs = block->op1_to->sources;

					/* the "target" block doesn't had any followed block */
					while(cs) {
						if (cs->from->follow_to == block->op1_to) {
							can_reorder = 0;
							break;
						}
						cs = cs->next;
					}
					if (can_reorder) {
						next = block->op1_to;
						/* the "target" block is not followed by current "block" */
						while (next->follow_to != NULL) {
							if (next->follow_to == block) {
								can_reorder = 0;
								break;
							}
							next = next->follow_to;
						}
						if (can_reorder) {
							gear_basic_block *prev = blocks;

							while (prev->next != block->op1_to) {
								prev = prev->next;
							}
							prev->next = next->next;
							next->next = block->next;
							block->next = block->op1_to;

							block->follow_to = block->op1_to;
							block->op1_to = NULL;
							MAKE_NOP(last_op);
							block->len--;
							if(block->len == 0) {
								/* this block is nothing but NOP now */
								delete_code_block(block, ctx);
							}
							break;
						}
					}
#endif
				}
			}
			break;

		case GEAR_JMPZ:
		case GEAR_JMPNZ:
			/* constant conditional JMPs */
			if (last_op->op1_type == IS_CONST) {
				int should_jmp = gear_is_true(&GEAR_OP1_LITERAL(last_op));

				if (last_op->opcode == GEAR_JMPZ) {
					should_jmp = !should_jmp;
				}
				literal_dtor(&GEAR_OP1_LITERAL(last_op));
				last_op->op1_type = IS_UNUSED;
				if (should_jmp) {
					/* JMPNZ(true) -> JMP */
					last_op->opcode = GEAR_JMP;
					DEL_SOURCE(block, block->successors[1]);
					block->successors_count = 1;
				} else {
					/* JMPNZ(false) -> NOP */
					MAKE_NOP(last_op);
					DEL_SOURCE(block, block->successors[0]);
					block->successors_count = 1;
					block->successors[0] = block->successors[1];
				}
				++(*opt_count);
				break;
			}

			if (block->successors[0] == block->successors[1]) {
				/* L: JMP[N]Z(X, L+1) -> NOP or FREE(X) */

				if (last_op->op1_type == IS_CV) {
					last_op->opcode = GEAR_CHECK_VAR;
					last_op->op2.num = 0;
				} else if (last_op->op1_type & (IS_VAR|IS_TMP_VAR)) {
					last_op->opcode = GEAR_FREE;
					last_op->op2.num = 0;
				} else {
					MAKE_NOP(last_op);
				}
				block->successors_count = 1;
				++(*opt_count);
				break;
			}

			if (1) {
				gear_uchar same_type = last_op->op1_type;
				uint32_t same_var = VAR_NUM_EX(last_op->op1);
				gear_op *target;
				gear_op *target_end;
				gear_basic_block *target_block = blocks + block->successors[0];

next_target:
				target = op_array->opcodes + target_block->start;
				target_end = target + target_block->len;
				while (target < target_end && target->opcode == GEAR_NOP) {
					target++;
				}

				/* next block is only NOP's */
				if (target == target_end) {
					target_block = blocks + target_block->successors[0];
					++(*opt_count);
					goto next_target;
				} else if (target->opcode == INV_COND(last_op->opcode) &&
					/* JMPZ(X, L), L: JMPNZ(X, L2) -> JMPZ(X, L+1) */
				   (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
				   same_type == target->op1_type &&
				   same_var == VAR_NUM_EX(target->op1) &&
				   !(target_block->flags & GEAR_BB_PROTECTED)
				   ) {
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[1];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == INV_COND_EX(last_op->opcode) &&
							(target->op1_type & (IS_TMP_VAR|IS_CV)) &&
				    		same_type == target->op1_type &&
				    		same_var == VAR_NUM_EX(target->op1) &&
							!(target_block->flags & GEAR_BB_PROTECTED)) {
					/* JMPZ(X, L), L: T = JMPNZ_EX(X, L2) -> T = JMPZ_EX(X, L+1) */
					last_op->opcode += 3;
					COPY_NODE(last_op->result, target->result);
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[1];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == last_op->opcode &&
						   (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
						   same_type == target->op1_type &&
						   same_var == VAR_NUM_EX(target->op1) &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* JMPZ(X, L), L: JMPZ(X, L2) -> JMPZ(X, L2) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[0];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == GEAR_JMP &&
				           !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* JMPZ(X, L), L: JMP(L2) -> JMPZ(X, L2) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[0];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == GEAR_JMPZNZ &&
				           (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
				           same_type == target->op1_type &&
				           same_var == VAR_NUM_EX(target->op1) &&
				           !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* JMPZ(X, L), L: JMPZNZ(X, L2, L3) -> JMPZ(X, L2) */
					DEL_SOURCE(block, block->successors[0]);
					if (last_op->opcode == GEAR_JMPZ) {
						block->successors[0] = target_block->successors[0];
					} else {
						block->successors[0] = target_block->successors[1];
					}
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				}
			}

			if (last_op->opcode == GEAR_JMPZ || last_op->opcode == GEAR_JMPNZ) {
				gear_op *target;
				gear_op *target_end;
				gear_basic_block *target_block;

				while (1) {
					target_block = blocks + block->successors[1];
					target = op_array->opcodes + target_block->start;
					target_end = op_array->opcodes + target_block->start + 1;
					while (target < target_end && target->opcode == GEAR_NOP) {
						target++;
					}

					/* next block is only NOP's */
					if (target == target_end && !(target_block->flags & GEAR_BB_PROTECTED)) {
						DEL_SOURCE(block, block->successors[1]);
						block->successors[1] = target_block->successors[0];
						ADD_SOURCE(block, block->successors[1]);
						++(*opt_count);
					} else {
						break;
					}
				}
				/* JMPZ(X,L1), JMP(L2) -> JMPZNZ(X,L1,L2) */
				if (target->opcode == GEAR_JMP &&
					!(target_block->flags & GEAR_BB_PROTECTED)) {
					DEL_SOURCE(block, block->successors[1]);
					if (last_op->opcode == GEAR_JMPZ) {
						block->successors[1] = target_block->successors[0];
						ADD_SOURCE(block, block->successors[1]);
					} else {
						block->successors[1] = block->successors[0];
						block->successors[0] = target_block->successors[0];
						ADD_SOURCE(block, block->successors[0]);
					}
					last_op->opcode = GEAR_JMPZNZ;
					++(*opt_count);
				}
			}
			break;

		case GEAR_JMPNZ_EX:
		case GEAR_JMPZ_EX:
			/* constant conditional JMPs */
			if (last_op->op1_type == IS_CONST) {
				int should_jmp = gear_is_true(&GEAR_OP1_LITERAL(last_op));

				if (last_op->opcode == GEAR_JMPZ_EX) {
					should_jmp = !should_jmp;
				}
				if (!should_jmp) {
					/* T = JMPZ_EX(true,L)   -> T = QM_ASSIGN(true)
					 * T = JMPNZ_EX(false,L) -> T = QM_ASSIGN(false)
					 */
					last_op->opcode = GEAR_QM_ASSIGN;
					SET_UNUSED(last_op->op2);
					DEL_SOURCE(block, block->successors[0]);
					block->successors_count = 1;
					block->successors[0] = block->successors[1];
					++(*opt_count);
				}
				break;
			}

			if (1) {
				gear_op *target, *target_end;
				gear_basic_block *target_block;
				int var_num = op_array->last_var + op_array->T;

				if (var_num <= 0) {
   					return;
				}
				memset(same_t, 0, var_num);
				same_t[VAR_NUM_EX(last_op->op1)] |= last_op->op1_type;
				same_t[VAR_NUM_EX(last_op->result)] |= last_op->result_type;
				target_block = blocks + block->successors[0];
next_target_ex:
				target = op_array->opcodes + target_block->start;
				target_end = target + target_block->len;
				while (target < target_end && target->opcode == GEAR_NOP) {
					target++;
				}
 				/* next block is only NOP's */
				if (target == target_end) {
					target_block = blocks + target_block->successors[0];
					++(*opt_count);
					goto next_target_ex;
				} else if (target->opcode == last_op->opcode-3 &&
						   (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
						   (same_t[VAR_NUM_EX(target->op1)] & target->op1_type) != 0 &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* T = JMPZ_EX(X, L1), L1: JMPZ({X|T}, L2) -> T = JMPZ_EX(X, L2) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[0];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == INV_EX_COND(last_op->opcode) &&
					   	   (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
						   (same_t[VAR_NUM_EX(target->op1)] & target->op1_type) != 0 &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* T = JMPZ_EX(X, L1), L1: JMPNZ({X|T1}, L2) -> T = JMPZ_EX(X, L1+1) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[1];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == INV_EX_COND_EX(last_op->opcode) &&
					       (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
						   (same_t[VAR_NUM_EX(target->op1)] & target->op1_type) != 0 &&
						   (same_t[VAR_NUM_EX(target->result)] & target->result_type) != 0 &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* T = JMPZ_EX(X, L1), L1: T = JMPNZ_EX(T, L2) -> T = JMPZ_EX(X, L1+1) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[1];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == last_op->opcode &&
						   (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
						   (same_t[VAR_NUM_EX(target->op1)] & target->op1_type) != 0 &&
						   (same_t[VAR_NUM_EX(target->result)] & target->result_type) != 0 &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* T = JMPZ_EX(X, L1), L1: T = JMPZ({X|T}, L2) -> T = JMPZ_EX(X, L2) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[0];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == GEAR_JMP &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* T = JMPZ_EX(X, L), L: JMP(L2) -> T = JMPZ(X, L2) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[0];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == GEAR_JMPZNZ &&
						   (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
						   (same_t[VAR_NUM_EX(target->op1)] & target->op1_type) != 0 &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
					/* T = JMPZ_EX(X, L), L: JMPZNZ({X|T}, L2, L3) -> T = JMPZ_EX(X, L2) */
					DEL_SOURCE(block, block->successors[0]);
					if (last_op->opcode == GEAR_JMPZ_EX) {
						block->successors[0] = target_block->successors[0];
					} else {
						block->successors[0] = target_block->successors[1];
					}
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				}
			}
			break;

		case GEAR_JMPZNZ: {
			int next = (block - blocks) + 1;

			while (next < cfg->blocks_count && !(blocks[next].flags & GEAR_BB_REACHABLE)) {
				/* find first accessed one */
				next++;
			}

			if (last_op->op1_type == IS_CONST) {
				if (!gear_is_true(&GEAR_OP1_LITERAL(last_op))) {
					/* JMPZNZ(false,L1,L2) -> JMP(L1) */
					literal_dtor(&GEAR_OP1_LITERAL(last_op));
					last_op->opcode = GEAR_JMP;
					SET_UNUSED(last_op->op1);
					SET_UNUSED(last_op->op2);
					DEL_SOURCE(block, block->successors[1]);
					block->successors_count = 1;
				} else {
					/* JMPZNZ(true,L1,L2) -> JMP(L2) */
					literal_dtor(&GEAR_OP1_LITERAL(last_op));
					last_op->opcode = GEAR_JMP;
					SET_UNUSED(last_op->op1);
					SET_UNUSED(last_op->op2);
					DEL_SOURCE(block, block->successors[0]);
					block->successors_count = 1;
					block->successors[0] = block->successors[1];
				}
				++(*opt_count);
			} else if (block->successors[0] == block->successors[1]) {
				/* both goto the same one - it's JMP */
				if (!(last_op->op1_type & (IS_VAR|IS_TMP_VAR))) {
					/* JMPZNZ(?,L,L) -> JMP(L) */
					last_op->opcode = GEAR_JMP;
					SET_UNUSED(last_op->op1);
					SET_UNUSED(last_op->op2);
					block->successors_count = 1;
					++(*opt_count);
				}
			} else if (block->successors[0] == next) {
				/* jumping to next on Z - can follow to it and jump only on NZ */
				/* JMPZNZ(X,L1,L2) L1: -> JMPNZ(X,L2) */
				last_op->opcode = GEAR_JMPNZ;
				block->successors[0] = block->successors[1];
				block->successors[1] = next;
				++(*opt_count);
				/* no need to add source */
			} else if (block->successors[1] == next) {
				/* jumping to next on NZ - can follow to it and jump only on Z */
				/* JMPZNZ(X,L1,L2) L2: -> JMPZ(X,L1) */
				last_op->opcode = GEAR_JMPZ;
				++(*opt_count);
				/* no need to add source */
			}

			if (last_op->opcode == GEAR_JMPZNZ) {
				gear_uchar same_type = last_op->op1_type;
				gear_uchar same_var = VAR_NUM_EX(last_op->op1);
				gear_op *target;
				gear_op *target_end;
				gear_basic_block *target_block = blocks + block->successors[0];

next_target_znz:
				target = op_array->opcodes + target_block->start;
				target_end = target + target_block->len;
				while (target < target_end && target->opcode == GEAR_NOP) {
					target++;
				}
				/* next block is only NOP's */
				if (target == target_end) {
					target_block = blocks + target_block->successors[0];
					++(*opt_count);
					goto next_target_znz;
				} else if ((target->opcode == GEAR_JMPZ || target->opcode == GEAR_JMPZNZ) &&
						   (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
						   same_type == target->op1_type &&
						   same_var == VAR_NUM_EX(target->op1) &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
				    /* JMPZNZ(X, L1, L2), L1: JMPZ(X, L3) -> JMPZNZ(X, L3, L2) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[0];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == GEAR_JMPNZ &&
						   (target->op1_type & (IS_TMP_VAR|IS_CV)) &&
						   same_type == target->op1_type &&
						   same_var == VAR_NUM_EX(target->op1) &&
						   !(target_block->flags & GEAR_BB_PROTECTED)) {
                    /* JMPZNZ(X, L1, L2), L1: X = JMPNZ(X, L3) -> JMPZNZ(X, L1+1, L2) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[1];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				} else if (target->opcode == GEAR_JMP &&
					       !(target_block->flags & GEAR_BB_PROTECTED)) {
                    /* JMPZNZ(X, L1, L2), L1: JMP(L3) -> JMPZNZ(X, L3, L2) */
					DEL_SOURCE(block, block->successors[0]);
					block->successors[0] = target_block->successors[0];
					ADD_SOURCE(block, block->successors[0]);
					++(*opt_count);
				}
			}
			break;
		}
	}
}

/* Global data dependencies */

/* Find a set of variables which are used outside of the block where they are
 * defined. We won't apply some optimization patterns for such variables. */
static void gear_t_usage(gear_cfg *cfg, gear_op_array *op_array, gear_bitset used_ext, gear_optimizer_ctx *ctx)
{
	int n;
	gear_basic_block *block, *next_block;
	uint32_t var_num;
	uint32_t bitset_len;
	gear_bitset usage;
	gear_bitset defined_here;
	void *checkpoint;
	gear_op *opline, *end;


	if (op_array->T == 0) {
		/* shortcut - if no Ts, nothing to do */
		return;
	}

	checkpoint = gear_arena_checkpoint(ctx->arena);
	bitset_len = gear_bitset_len(op_array->last_var + op_array->T);
	defined_here = gear_arena_alloc(&ctx->arena, bitset_len * GEAR_BITSET_ELM_SIZE);

	gear_bitset_clear(defined_here, bitset_len);
	for (n = 1; n < cfg->blocks_count; n++) {
		block = cfg->blocks + n;

		if (!(block->flags & GEAR_BB_REACHABLE)) {
			continue;
		}

		opline = op_array->opcodes + block->start;
		end = opline + block->len;
		if (!(block->flags & GEAR_BB_FOLLOW) ||
		    (block->flags & GEAR_BB_TARGET)) {
			/* Skip continuation of "extended" BB */
			gear_bitset_clear(defined_here, bitset_len);
		}

		while (opline<end) {
			if (opline->op1_type & (IS_VAR|IS_TMP_VAR)) {
				var_num = VAR_NUM(opline->op1.var);
				if (!gear_bitset_in(defined_here, var_num)) {
					gear_bitset_incl(used_ext, var_num);
				}
			}
			if (opline->op2_type == IS_VAR) {
				var_num = VAR_NUM(opline->op2.var);
				if (opline->opcode == GEAR_FE_FETCH_R ||
				    opline->opcode == GEAR_FE_FETCH_RW) {
					/* these opcode use the op2 as result */
					gear_bitset_incl(defined_here, var_num);
				} else if (!gear_bitset_in(defined_here, var_num)) {
					gear_bitset_incl(used_ext, var_num);
				}
			} else if (opline->op2_type == IS_TMP_VAR) {
				var_num = VAR_NUM(opline->op2.var);
				if (!gear_bitset_in(defined_here, var_num)) {
					gear_bitset_incl(used_ext, var_num);
				}
			}

			if (opline->result_type == IS_VAR) {
				var_num = VAR_NUM(opline->result.var);
				gear_bitset_incl(defined_here, var_num);
			} else if (opline->result_type == IS_TMP_VAR) {
				var_num = VAR_NUM(opline->result.var);
				switch (opline->opcode) {
					case GEAR_ADD_ARRAY_ELEMENT:
					case GEAR_ROPE_ADD:
						/* these opcodes use the result as argument */
						if (!gear_bitset_in(defined_here, var_num)) {
							gear_bitset_incl(used_ext, var_num);
						}
						break;
					default :
						gear_bitset_incl(defined_here, var_num);
				}
			}
			opline++;
		}
	}

	if (ctx->debug_level & GEAR_DUMP_BLOCK_PASS_VARS) {
		int printed = 0;
		uint32_t i;

		for (i = op_array->last_var; i< op_array->T; i++) {
			if (gear_bitset_in(used_ext, i)) {
				if (!printed) {
					fprintf(stderr, "NON-LOCAL-VARS: %d", i);
					printed = 1;
				} else {
					fprintf(stderr, ", %d", i);
				}
			}
		}
		if (printed) {
			fprintf(stderr, "\n");
		}
	}

	usage = defined_here;
	next_block = NULL;
	for (n = cfg->blocks_count; n > 0;) {
		block = cfg->blocks + (--n);

		if (!(block->flags & GEAR_BB_REACHABLE) || block->len == 0) {
			continue;
		}

		end = op_array->opcodes + block->start;
		opline = end + block->len - 1;
		if (!next_block ||
		    !(next_block->flags & GEAR_BB_FOLLOW) ||
		    (next_block->flags & GEAR_BB_TARGET)) {
			/* Skip continuation of "extended" BB */
			gear_bitset_copy(usage, used_ext, bitset_len);
		} else if (block->successors_count > 1) {
			gear_bitset_union(usage, used_ext, bitset_len);
		}
		next_block = block;

		while (opline >= end) {
			/* usage checks */
			if (opline->result_type == IS_VAR) {
				if (!gear_bitset_in(usage, VAR_NUM(opline->result.var))) {
					switch (opline->opcode) {
						case GEAR_ASSIGN_ADD:
						case GEAR_ASSIGN_SUB:
						case GEAR_ASSIGN_MUL:
						case GEAR_ASSIGN_DIV:
						case GEAR_ASSIGN_POW:
						case GEAR_ASSIGN_MOD:
						case GEAR_ASSIGN_SL:
						case GEAR_ASSIGN_SR:
						case GEAR_ASSIGN_CONCAT:
						case GEAR_ASSIGN_BW_OR:
						case GEAR_ASSIGN_BW_AND:
						case GEAR_ASSIGN_BW_XOR:
						case GEAR_PRE_INC:
						case GEAR_PRE_DEC:
						case GEAR_ASSIGN:
						case GEAR_ASSIGN_REF:
						case GEAR_DO_FCALL:
						case GEAR_DO_ICALL:
						case GEAR_DO_UCALL:
						case GEAR_DO_FCALL_BY_NAME:
							opline->result_type = IS_UNUSED;
							break;
					}
				} else {
					gear_bitset_excl(usage, VAR_NUM(opline->result.var));
				}
			} else if (opline->result_type == IS_TMP_VAR) {
				if (!gear_bitset_in(usage, VAR_NUM(opline->result.var))) {
					switch (opline->opcode) {
						case GEAR_POST_INC:
						case GEAR_POST_DEC:
						case GEAR_POST_INC_OBJ:
						case GEAR_POST_DEC_OBJ:
							opline->opcode -= 2;
							opline->result_type = IS_UNUSED;
							break;
						case GEAR_QM_ASSIGN:
						case GEAR_BOOL:
						case GEAR_BOOL_NOT:
							if (opline->op1_type == IS_CV) {
								opline->opcode = GEAR_CHECK_VAR;
								SET_UNUSED(opline->result);
							} else if (opline->op1_type & (IS_TMP_VAR|IS_VAR)) {
								opline->opcode = GEAR_FREE;
								SET_UNUSED(opline->result);
							} else {
								if (opline->op1_type == IS_CONST) {
									literal_dtor(&GEAR_OP1_LITERAL(opline));
								}
								MAKE_NOP(opline);
							}
							break;
						case GEAR_JMPZ_EX:
						case GEAR_JMPNZ_EX:
							opline->opcode -= 3;
							SET_UNUSED(opline->result);
							break;
						case GEAR_ADD_ARRAY_ELEMENT:
						case GEAR_ROPE_ADD:
							gear_bitset_incl(usage, VAR_NUM(opline->result.var));
							break;
					}
				} else {
					switch (opline->opcode) {
						case GEAR_ADD_ARRAY_ELEMENT:
						case GEAR_ROPE_ADD:
							break;
						default:
							gear_bitset_excl(usage, VAR_NUM(opline->result.var));
							break;
					}
				}
			}

			if (opline->op2_type == IS_VAR) {
				switch (opline->opcode) {
					case GEAR_FE_FETCH_R:
					case GEAR_FE_FETCH_RW:
						gear_bitset_excl(usage, VAR_NUM(opline->op2.var));
						break;
					default:
						gear_bitset_incl(usage, VAR_NUM(opline->op2.var));
						break;
				}
			} else if (opline->op2_type == IS_TMP_VAR) {
				gear_bitset_incl(usage, VAR_NUM(opline->op2.var));
			}

			if (opline->op1_type & (IS_VAR|IS_TMP_VAR)) {
				gear_bitset_incl(usage, VAR_NUM(opline->op1.var));
			}

			opline--;
		}
	}

	gear_arena_release(&ctx->arena, checkpoint);
}

static void gear_merge_blocks(gear_op_array *op_array, gear_cfg *cfg, uint32_t *opt_count)
{
	int i;
	gear_basic_block *b, *bb;
	gear_basic_block *prev = NULL;

	for (i = 0; i < cfg->blocks_count; i++) {
		b = cfg->blocks + i;
		if (b->flags & GEAR_BB_REACHABLE) {
			if ((b->flags & GEAR_BB_FOLLOW) &&
			    !(b->flags & (GEAR_BB_TARGET | GEAR_BB_PROTECTED)) &&
			    prev && prev->successors_count == 1 && prev->successors[0] == i)
			{
				gear_op *last_op = op_array->opcodes + prev->start + prev->len - 1;
				if (prev->len != 0 && last_op->opcode == GEAR_JMP) {
					MAKE_NOP(last_op);
				}

				for (bb = prev + 1; bb != b; bb++) {
					gear_op *op = op_array->opcodes + bb->start;
					gear_op *end = op + bb->len;
					while (op < end) {
						if (op->op1_type == IS_CONST) {
							literal_dtor(&GEAR_OP1_LITERAL(op));
						}
						if (op->op2_type == IS_CONST) {
							literal_dtor(&GEAR_OP2_LITERAL(op));
						}
						MAKE_NOP(op);
						op++;
					}
					/* make block empty */
					bb->len = 0;
				}

				/* re-link */
				prev->flags |= (b->flags & GEAR_BB_EXIT);
				prev->len = b->start + b->len - prev->start;
				prev->successors_count = b->successors_count;
				if (b->successors != b->successors_storage) {
					prev->successors = b->successors;
					b->successors = b->successors_storage;
				} else {
					memcpy(prev->successors, b->successors, b->successors_count * sizeof(int));
				}

				/* unlink & make block empty and unreachable */
				b->flags = 0;
				b->len = 0;
				b->successors_count = 0;
				++(*opt_count);
			} else {
				prev = b;
			}
		}
	}
}

#define PASSES 3

void gear_optimize_cfg(gear_op_array *op_array, gear_optimizer_ctx *ctx)
{
	gear_cfg cfg;
	gear_basic_block *blocks, *end, *b;
	int pass;
	uint32_t bitset_len;
	gear_bitset usage;
	void *checkpoint;
	gear_op **Tsource;
	gear_uchar *same_t;
	uint32_t opt_count;

    /* Build CFG */
	checkpoint = gear_arena_checkpoint(ctx->arena);
	if (gear_build_cfg(&ctx->arena, op_array, GEAR_CFG_SPLIT_AT_LIVE_RANGES, &cfg) != SUCCESS) {
		gear_arena_release(&ctx->arena, checkpoint);
		return;
	}

	if (cfg.blocks_count * (op_array->last_var + op_array->T) > 64 * 1024 * 1024) {
		gear_arena_release(&ctx->arena, checkpoint);
		return;
	}

	if (ctx->debug_level & GEAR_DUMP_BEFORE_BLOCK_PASS) {
		gear_dump_op_array(op_array, GEAR_DUMP_CFG, "before block pass", &cfg);
	}

	if (op_array->last_var || op_array->T) {
		bitset_len = gear_bitset_len(op_array->last_var + op_array->T);
		Tsource = gear_arena_calloc(&ctx->arena, op_array->last_var + op_array->T, sizeof(gear_op *));
		same_t = gear_arena_alloc(&ctx->arena, op_array->last_var + op_array->T);
		usage = gear_arena_alloc(&ctx->arena, bitset_len * GEAR_BITSET_ELM_SIZE);
	} else {
		bitset_len = 0;
		Tsource = NULL;
		same_t = NULL;
		usage = NULL;
	}
	blocks = cfg.blocks;
	end = blocks + cfg.blocks_count;
	for (pass = 0; pass < PASSES; pass++) {
		opt_count = 0;

		/* Compute data dependencies */
		gear_bitset_clear(usage, bitset_len);
		gear_t_usage(&cfg, op_array, usage, ctx);

		/* optimize each basic block separately */
		for (b = blocks; b < end; b++) {
			if (!(b->flags & GEAR_BB_REACHABLE)) {
				continue;
			}
			/* we track data dependencies only insight a single basic block */
			if (!(b->flags & GEAR_BB_FOLLOW) ||
			    (b->flags & GEAR_BB_TARGET)) {
				/* Skip continuation of "extended" BB */
				memset(Tsource, 0, (op_array->last_var + op_array->T) * sizeof(gear_op *));
			}
			gear_optimize_block(b, op_array, usage, &cfg, Tsource, &opt_count);
		}

		/* Eliminate NOPs */
		for (b = blocks; b < end; b++) {
			if (b->flags & GEAR_BB_REACHABLE) {
				strip_nops(op_array, b);
			}
		}

		/* Jump optimization for each block */
		for (b = blocks; b < end; b++) {
			if (b->flags & GEAR_BB_REACHABLE) {
				gear_jmp_optimization(b, op_array, &cfg, same_t, &opt_count);
			}
		}

		/* Eliminate unreachable basic blocks */
		gear_cfg_remark_reachable_blocks(op_array, &cfg);

		/* Merge Blocks */
		gear_merge_blocks(op_array, &cfg, &opt_count);

		if (opt_count == 0) {
			break;
		}
	}

	gear_bitset_clear(usage, bitset_len);
	gear_t_usage(&cfg, op_array, usage, ctx);
	assemble_code_blocks(&cfg, op_array, ctx);

	if (ctx->debug_level & GEAR_DUMP_AFTER_BLOCK_PASS) {
		gear_dump_op_array(op_array, GEAR_DUMP_CFG | GEAR_DUMP_HIDE_UNREACHABLE, "after block pass", &cfg);
	}

	/* Destroy CFG */
	gear_arena_release(&ctx->arena, checkpoint);
}
