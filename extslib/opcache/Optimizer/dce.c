/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GearAccelerator.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "Optimizer/gear_inference.h"
#include "Optimizer/gear_ssa.h"
#include "Optimizer/gear_func_info.h"
#include "Optimizer/gear_call_graph.h"
#include "gear_bitset.h"

/* This pass implements a form of dead code elimination (DCE). The algorithm optimistically assumes
 * that all instructions and phis are dead. Instructions with immediate side-effects are then marked
 * as live. We then recursively (using a worklist) propagate liveness to the instructions that def
 * the used operands.
 *
 * Notes:
 *  * This pass does not perform unreachable code elimination. This happens as part of the SCCP
 *    pass.
 *  * The DCE is performed without taking control-dependence into account, i.e. all conditional
 *    branches are assumed to be live. It's possible to take control-dependence into account using
 *    the DCE algorithm described by Cytron et al., however it requires the construction of a
 *    postdominator tree and of postdominance frontiers, which does not seem worthwhile at this
 *    point.
 *  * We separate intrinsic side-effects from potential side-effects in the form of notices thrown
 *    by the instruction (in case we want to make this configurable). See may_have_side_effects() and
 *    gear_may_throw().
 *  * We often cannot DCE assignments and unsets while guaranteeing that dtors run in the same
 *    order. There is an optimization option to allow reordering of dtor effects.
 *  * The algorithm is able to eliminate dead modifications of non-escaping arrays
 *    and objects as well as dead arrays and objects allocations.
 */

typedef struct {
	gear_ssa *ssa;
	gear_op_array *op_array;
	gear_bitset instr_dead;
	gear_bitset phi_dead;
	gear_bitset instr_worklist;
	gear_bitset phi_worklist;
	gear_bitset phi_worklist_no_val;
	uint32_t instr_worklist_len;
	uint32_t phi_worklist_len;
	unsigned reorder_dtor_effects : 1;
} context;

static inline gear_bool is_bad_mod(const gear_ssa *ssa, int use, int def) {
	if (def < 0) {
		/* This modification is not tracked by SSA, assume the worst */
		return 1;
	}
	if (ssa->var_info[use].type & MAY_BE_REF) {
		/* Modification of reference may have side-effect */
		return 1;
	}
	return 0;
}

static inline gear_bool may_have_side_effects(
		gear_op_array *op_array, gear_ssa *ssa,
		const gear_op *opline, const gear_ssa_op *ssa_op,
		gear_bool reorder_dtor_effects) {
	switch (opline->opcode) {
		case GEAR_NOP:
		case GEAR_IS_IDENTICAL:
		case GEAR_IS_NOT_IDENTICAL:
		case GEAR_QM_ASSIGN:
		case GEAR_FREE:
		case GEAR_TYPE_CHECK:
		case GEAR_DEFINED:
		case GEAR_ADD:
		case GEAR_SUB:
		case GEAR_MUL:
		case GEAR_POW:
		case GEAR_BW_OR:
		case GEAR_BW_AND:
		case GEAR_BW_XOR:
		case GEAR_CONCAT:
		case GEAR_FAST_CONCAT:
		case GEAR_DIV:
		case GEAR_MOD:
		case GEAR_BOOL_XOR:
		case GEAR_BOOL:
		case GEAR_BOOL_NOT:
		case GEAR_BW_NOT:
		case GEAR_SL:
		case GEAR_SR:
		case GEAR_IS_EQUAL:
		case GEAR_IS_NOT_EQUAL:
		case GEAR_IS_SMALLER:
		case GEAR_IS_SMALLER_OR_EQUAL:
		case GEAR_CASE:
		case GEAR_CAST:
		case GEAR_ROPE_INIT:
		case GEAR_ROPE_ADD:
		case GEAR_INIT_ARRAY:
		case GEAR_ADD_ARRAY_ELEMENT:
		case GEAR_SPACESHIP:
		case GEAR_STRLEN:
		case GEAR_COUNT:
		case GEAR_GET_TYPE:
		case GEAR_ISSET_ISEMPTY_THIS:
		case GEAR_ISSET_ISEMPTY_DIM_OBJ:
		case GEAR_FETCH_DIM_IS:
		case GEAR_ISSET_ISEMPTY_CV:
		case GEAR_ISSET_ISEMPTY_VAR:
		case GEAR_FETCH_IS:
		case GEAR_IN_ARRAY:
		case GEAR_FUNC_NUM_ARGS:
		case GEAR_FUNC_GET_ARGS:
			/* No side effects */
			return 0;
		case GEAR_ROPE_END:
			/* TODO: Rope dce optmization, see #76446 */
			return 1;
		case GEAR_JMP:
		case GEAR_JMPZ:
		case GEAR_JMPNZ:
		case GEAR_JMPZNZ:
		case GEAR_JMPZ_EX:
		case GEAR_JMPNZ_EX:
		case GEAR_JMP_SET:
		case GEAR_COALESCE:
		case GEAR_ASSERT_CHECK:
			/* For our purposes a jumps and branches are side effects. */
			return 1;
		case GEAR_BEGIN_SILENCE:
		case GEAR_END_SILENCE:
		case GEAR_ECHO:
		case GEAR_INCLUDE_OR_EVAL:
		case GEAR_THROW:
		case GEAR_EXT_STMT:
		case GEAR_EXT_FCALL_BEGIN:
		case GEAR_EXT_FCALL_END:
		case GEAR_EXT_NOP:
		case GEAR_TICKS:
		case GEAR_YIELD:
		case GEAR_YIELD_FROM:
			/* Intrinsic side effects */
			return 1;
		case GEAR_DO_FCALL:
		case GEAR_DO_FCALL_BY_NAME:
		case GEAR_DO_ICALL:
		case GEAR_DO_UCALL:
			/* For now assume all calls have side effects */
			return 1;
		case GEAR_RECV:
		case GEAR_RECV_INIT:
			/* Even though RECV_INIT can be side-effect free, these cannot be simply dropped
			 * due to the prologue skipping code. */
			return 1;
		case GEAR_ASSIGN_REF:
			return 1;
		case GEAR_ASSIGN:
		{
			if (is_bad_mod(ssa, ssa_op->op1_use, ssa_op->op1_def)) {
				return 1;
			}
			if (!reorder_dtor_effects) {
				if (opline->op2_type != IS_CONST
					&& (OP2_INFO() & MAY_HAVE_DTOR)
					&& ssa->vars[ssa_op->op2_use].escape_state != ESCAPE_STATE_NO_ESCAPE) {
					/* DCE might shorten lifetime */
					return 1;
				}
			}
			return 0;
		}
		case GEAR_UNSET_VAR:
			return 1;
		case GEAR_UNSET_CV:
		{
			uint32_t t1 = OP1_INFO();
			if (t1 & MAY_BE_REF) {
				/* We don't consider uses as the LHS of an assignment as real uses during DCE, so
				 * an unset may be considered dead even if there is a later assignment to the
				 * variable. Removing the unset in this case would not be correct if the variable
				 * is a reference, because unset breaks references. */
				return 1;
			}
			return 0;
		}
		case GEAR_PRE_INC:
		case GEAR_POST_INC:
		case GEAR_PRE_DEC:
		case GEAR_POST_DEC:
			return is_bad_mod(ssa, ssa_op->op1_use, ssa_op->op1_def);
		case GEAR_ASSIGN_ADD:
		case GEAR_ASSIGN_SUB:
		case GEAR_ASSIGN_MUL:
		case GEAR_ASSIGN_DIV:
		case GEAR_ASSIGN_MOD:
		case GEAR_ASSIGN_SL:
		case GEAR_ASSIGN_SR:
		case GEAR_ASSIGN_CONCAT:
		case GEAR_ASSIGN_BW_OR:
		case GEAR_ASSIGN_BW_AND:
		case GEAR_ASSIGN_BW_XOR:
		case GEAR_ASSIGN_POW:
			return is_bad_mod(ssa, ssa_op->op1_use, ssa_op->op1_def)
				|| (opline->extended_value
					&& ssa->vars[ssa_op->op1_def].escape_state != ESCAPE_STATE_NO_ESCAPE);
		case GEAR_ASSIGN_DIM:
		case GEAR_ASSIGN_OBJ:
			if (is_bad_mod(ssa, ssa_op->op1_use, ssa_op->op1_def)
				|| ssa->vars[ssa_op->op1_def].escape_state != ESCAPE_STATE_NO_ESCAPE) {
				return 1;
			}
			if (!reorder_dtor_effects) {
				opline++;
				ssa_op++;
				if (opline->op1_type != IS_CONST
					&& (OP1_INFO() & MAY_HAVE_DTOR)) {
					/* DCE might shorten lifetime */
					return 1;
				}
			}
			return 0;
		case GEAR_PRE_INC_OBJ:
		case GEAR_PRE_DEC_OBJ:
		case GEAR_POST_INC_OBJ:
		case GEAR_POST_DEC_OBJ:
			if (is_bad_mod(ssa, ssa_op->op1_use, ssa_op->op1_def)
				|| ssa->vars[ssa_op->op1_def].escape_state != ESCAPE_STATE_NO_ESCAPE) {
				return 1;
			}
			return 0;
		default:
			/* For everything we didn't handle, assume a side-effect */
			return 1;
	}
}

static gear_always_inline void add_to_worklists(context *ctx, int var_num, int check) {
	gear_ssa_var *var = &ctx->ssa->vars[var_num];
	if (var->definition >= 0) {
		if (!check || gear_bitset_in(ctx->instr_dead, var->definition)) {
			gear_bitset_incl(ctx->instr_worklist, var->definition);
		}
	} else if (var->definition_phi) {
		if (!check || gear_bitset_in(ctx->phi_dead, var_num)) {
			gear_bitset_incl(ctx->phi_worklist, var_num);
		}
	}
}

static inline void add_to_phi_worklist_no_val(context *ctx, int var_num) {
	gear_ssa_var *var = &ctx->ssa->vars[var_num];
	if (var->definition_phi && gear_bitset_in(ctx->phi_dead, var_num)) {
		gear_bitset_incl(ctx->phi_worklist_no_val, var_num);
	}
}

static gear_always_inline void add_operands_to_worklists(context *ctx, gear_op *opline, gear_ssa_op *ssa_op, int check) {
	if (ssa_op->result_use >= 0) {
		add_to_worklists(ctx, ssa_op->result_use, check);
	}
	if (ssa_op->op1_use >= 0) {
		if (!gear_ssa_is_no_val_use(opline, ssa_op, ssa_op->op1_use)) {
			add_to_worklists(ctx, ssa_op->op1_use, check);
		} else {
			add_to_phi_worklist_no_val(ctx, ssa_op->op1_use);
		}
	}
	if (ssa_op->op2_use >= 0) {
		if (!gear_ssa_is_no_val_use(opline, ssa_op, ssa_op->op2_use)) {
			add_to_worklists(ctx, ssa_op->op2_use, check);
		} else {
			add_to_phi_worklist_no_val(ctx, ssa_op->op2_use);
		}
	}
}

static gear_always_inline void add_phi_sources_to_worklists(context *ctx, gear_ssa_phi *phi, int check) {
	gear_ssa *ssa = ctx->ssa;
	int source;
	FOREACH_PHI_SOURCE(phi, source) {
		add_to_worklists(ctx, source, check);
	} FOREACH_PHI_SOURCE_END();
}

static inline gear_bool is_var_dead(context *ctx, int var_num) {
	gear_ssa_var *var = &ctx->ssa->vars[var_num];
	if (var->definition_phi) {
		return gear_bitset_in(ctx->phi_dead, var_num);
	} else if (var->definition >= 0) {
		return gear_bitset_in(ctx->instr_dead, var->definition);
	} else {
		/* Variable has no definition, so either the definition has already been removed (var is
		 * dead) or this is one of the implicit variables at the start of the function (for our
		 * purposes live) */
		return var_num >= ctx->op_array->last_var;
	}
}

// Sometimes we can mark the var as EXT_UNUSED
static gear_bool try_remove_var_def(context *ctx, int free_var, int use_chain, gear_op *opline) {
	if (use_chain >= 0) {
		return 0;
	}
	gear_ssa_var *var = &ctx->ssa->vars[free_var];
	int def = var->definition;

	if (def >= 0) {
		gear_ssa_op *def_op = &ctx->ssa->ops[def];

		if (def_op->result_def == free_var
				&& var->phi_use_chain == NULL
				&& var->use_chain == (opline - ctx->op_array->opcodes)) {
			gear_op *def_opline = &ctx->op_array->opcodes[def];

			switch (def_opline->opcode) {
				case GEAR_ASSIGN:
				case GEAR_ASSIGN_REF:
				case GEAR_ASSIGN_DIM:
				case GEAR_ASSIGN_OBJ:
				case GEAR_ASSIGN_ADD:
				case GEAR_ASSIGN_SUB:
				case GEAR_ASSIGN_MUL:
				case GEAR_ASSIGN_DIV:
				case GEAR_ASSIGN_MOD:
				case GEAR_ASSIGN_SL:
				case GEAR_ASSIGN_SR:
				case GEAR_ASSIGN_CONCAT:
				case GEAR_ASSIGN_BW_OR:
				case GEAR_ASSIGN_BW_AND:
				case GEAR_ASSIGN_BW_XOR:
				case GEAR_ASSIGN_POW:
				case GEAR_PRE_INC:
				case GEAR_PRE_DEC:
				case GEAR_PRE_INC_OBJ:
				case GEAR_POST_INC_OBJ:
				case GEAR_PRE_DEC_OBJ:
				case GEAR_POST_DEC_OBJ:
				case GEAR_DO_ICALL:
				case GEAR_DO_UCALL:
				case GEAR_DO_FCALL_BY_NAME:
				case GEAR_DO_FCALL:
				case GEAR_INCLUDE_OR_EVAL:
				case GEAR_YIELD:
				case GEAR_YIELD_FROM:
				case GEAR_ASSERT_CHECK:
					def_opline->result_type = IS_UNUSED;
					def_opline->result.var = 0;
					def_op->result_def = -1;
					var->definition = -1;
					return 1;
				default:
					break;
			}
		}
	}
	return 0;
}

/* Returns whether the instruction has been DCEd */
static gear_bool dce_instr(context *ctx, gear_op *opline, gear_ssa_op *ssa_op) {
	gear_ssa *ssa = ctx->ssa;
	int free_var = -1;
	gear_uchar free_var_type;

	if (opline->opcode == GEAR_NOP) {
		return 0;
	}

	/* We mark FREEs as dead, but they're only really dead if the destroyed var is dead */
	if (opline->opcode == GEAR_FREE
			&& (ssa->var_info[ssa_op->op1_use].type & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))
			&& !is_var_dead(ctx, ssa_op->op1_use)) {
		return 0;
	}

	if ((opline->op1_type & (IS_VAR|IS_TMP_VAR))&& !is_var_dead(ctx, ssa_op->op1_use)) {
		if (!try_remove_var_def(ctx, ssa_op->op1_use, ssa_op->op1_use_chain, opline)) {
			if (ssa->var_info[ssa_op->op1_use].type & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF)
				&& opline->opcode != GEAR_CASE) {
				free_var = ssa_op->op1_use;
				free_var_type = opline->op1_type;
			}
		}
	}
	if ((opline->op2_type & (IS_VAR|IS_TMP_VAR)) && !is_var_dead(ctx, ssa_op->op2_use)) {
		if (!try_remove_var_def(ctx, ssa_op->op2_use, ssa_op->op2_use_chain, opline)) {
			if (ssa->var_info[ssa_op->op2_use].type & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF)) {
				if (free_var >= 0) {
					// TODO: We can't free two vars. Keep instruction alive.
					gear_bitset_excl(ctx->instr_dead, opline - ctx->op_array->opcodes);
					return 0;
				}
				free_var = ssa_op->op2_use;
				free_var_type = opline->op2_type;
			}
		}
	}

	gear_ssa_rename_defs_of_instr(ctx->ssa, ssa_op);
	gear_ssa_remove_instr(ctx->ssa, opline, ssa_op);

	if (free_var >= 0) {
		opline->opcode = GEAR_FREE;
		opline->op1.var = (uintptr_t) GEAR_CALL_VAR_NUM(NULL, ssa->vars[free_var].var);
		opline->op1_type = free_var_type;

		ssa_op->op1_use = free_var;
		ssa_op->op1_use_chain = ssa->vars[free_var].use_chain;
		ssa->vars[free_var].use_chain = ssa_op - ssa->ops;
		return 0;
	}
	return 1;
}

static inline int get_common_phi_source(gear_ssa *ssa, gear_ssa_phi *phi) {
	int common_source = -1;
	int source;
	FOREACH_PHI_SOURCE(phi, source) {
		if (common_source == -1) {
			common_source = source;
		} else if (common_source != source && source != phi->ssa_var) {
			return -1;
		}
	} FOREACH_PHI_SOURCE_END();
	GEAR_ASSERT(common_source != -1);
	return common_source;
}

static void try_remove_trivial_phi(context *ctx, gear_ssa_phi *phi) {
	gear_ssa *ssa = ctx->ssa;
	if (phi->pi < 0) {
		/* Phi assignment with identical source operands */
		int common_source = get_common_phi_source(ssa, phi);
		if (common_source >= 0) {
			gear_ssa_rename_var_uses(ssa, phi->ssa_var, common_source, 1);
			gear_ssa_remove_phi(ssa, phi);
		}
	} else {
		/* Pi assignment that is only used in Phi/Pi assignments */
		// TODO What if we want to rerun type inference after DCE? Maybe separate this?
		/*GEAR_ASSERT(phi->sources[0] != -1);
		if (ssa->vars[phi->ssa_var].use_chain < 0) {
			gear_ssa_rename_var_uses_keep_types(ssa, phi->ssa_var, phi->sources[0], 1);
			gear_ssa_remove_phi(ssa, phi);
		}*/
	}
}

static inline gear_bool may_break_varargs(const gear_op_array *op_array, const gear_ssa *ssa, const gear_ssa_op *ssa_op) {
	if (ssa_op->op1_def >= 0
			&& ssa->vars[ssa_op->op1_def].var < op_array->num_args) {
		return 1;
	}
	if (ssa_op->op2_def >= 0
			&& ssa->vars[ssa_op->op2_def].var < op_array->num_args) {
		return 1;
	}
	if (ssa_op->result_def >= 0
			&& ssa->vars[ssa_op->result_def].var < op_array->num_args) {
		return 1;
	}
	return 0;
}

static void dce_live_ranges(context *ctx, gear_op_array *op_array, gear_ssa *ssa)
{
	int i = 0;
	int j = 0;
	gear_live_range *live_range = op_array->live_range;

	while (i < op_array->last_live_range) {
		if ((live_range->var & GEAR_LIVE_MASK) != GEAR_LIVE_TMPVAR) {
			/* keep */
			j++;
		} else {
			uint32_t var = live_range->var & ~GEAR_LIVE_MASK;
			uint32_t def = live_range->start - 1;

			if ((op_array->opcodes[def].result_type == IS_UNUSED) &&
					(UNEXPECTED(op_array->opcodes[def].opcode == GEAR_EXT_STMT) ||
					UNEXPECTED(op_array->opcodes[def].opcode == GEAR_EXT_FCALL_END))) {
				def--;
			}

			if (op_array->opcodes[def].result_type == IS_UNUSED) {
				if (op_array->opcodes[def].opcode == GEAR_DO_FCALL) {
					/* constructor call */
					do {
						def--;
						if ((op_array->opcodes[def].result_type & (IS_TMP_VAR|IS_VAR))
								&& op_array->opcodes[def].result.var == var) {
							GEAR_ASSERT(op_array->opcodes[def].opcode == GEAR_NEW);
							break;
						}
					} while (def > 0);
				} else if (op_array->opcodes[def].opcode == GEAR_OP_DATA) {
					def--;
				}
			}

#if GEAR_DEBUG
			GEAR_ASSERT(op_array->opcodes[def].result_type & (IS_TMP_VAR|IS_VAR));
			GEAR_ASSERT(op_array->opcodes[def].result.var == var);
			GEAR_ASSERT(ssa->ops[def].result_def >= 0);
#else
			if (!(op_array->opcodes[def].result_type & (IS_TMP_VAR|IS_VAR))
					|| op_array->opcodes[def].result.var != var
					|| ssa->ops[def].result_def < 0) {
				/* TODO: Some wrong live-range? keep it. */
				j++;
				live_range++;
				i++;
				continue;
			}
#endif

			var = ssa->ops[def].result_def;

			if ((ssa->var_info[var].type & (MAY_BE_STRING|MAY_BE_ARRAY|MAY_BE_OBJECT|MAY_BE_RESOURCE|MAY_BE_REF))
					&& !is_var_dead(ctx, var)) {
				/* keep */
				j++;
			} else if (i != j) {
				op_array->live_range[j] = *live_range;
			}
		}

		live_range++;
		i++;
	}
	op_array->last_live_range = j;
	if (op_array->last_live_range == 0) {
		efree(op_array->live_range);
		op_array->live_range = NULL;
	}
}

int dce_optimize_op_array(gear_op_array *op_array, gear_ssa *ssa, gear_bool reorder_dtor_effects) {
	int i;
	gear_ssa_phi *phi;
	int removed_ops = 0;

	/* DCE of CV operations that changes arguments may affect vararg functions. */
	gear_bool has_varargs = (ssa->cfg.flags & GEAR_FUNC_VARARG) != 0;

	context ctx;
	ctx.ssa = ssa;
	ctx.op_array = op_array;
	ctx.reorder_dtor_effects = reorder_dtor_effects;

	/* We have no dedicated phi vector, so we use the whole ssa var vector instead */
	ctx.instr_worklist_len = gear_bitset_len(op_array->last);
	ctx.instr_worklist = alloca(sizeof(gear_ulong) * ctx.instr_worklist_len);
	memset(ctx.instr_worklist, 0, sizeof(gear_ulong) * ctx.instr_worklist_len);
	ctx.phi_worklist_len = gear_bitset_len(ssa->vars_count);
	ctx.phi_worklist = alloca(sizeof(gear_ulong) * ctx.phi_worklist_len);
	memset(ctx.phi_worklist, 0, sizeof(gear_ulong) * ctx.phi_worklist_len);
	ctx.phi_worklist_no_val = alloca(sizeof(gear_ulong) * ctx.phi_worklist_len);
	memset(ctx.phi_worklist_no_val, 0, sizeof(gear_ulong) * ctx.phi_worklist_len);

	/* Optimistically assume all instructions and phis to be dead */
	ctx.instr_dead = alloca(sizeof(gear_ulong) * ctx.instr_worklist_len);
	memset(ctx.instr_dead, 0, sizeof(gear_ulong) * ctx.instr_worklist_len);
	ctx.phi_dead = alloca(sizeof(gear_ulong) * ctx.phi_worklist_len);
	memset(ctx.phi_dead, 0xff, sizeof(gear_ulong) * ctx.phi_worklist_len);

	/* Mark reacable instruction without side effects as dead */
	int b = ssa->cfg.blocks_count;
	while (b > 0) {
		int	op_data = -1;

		b--;
		gear_basic_block *block = &ssa->cfg.blocks[b];
		if (!(block->flags & GEAR_BB_REACHABLE)) {
			continue;
		}
		i = block->start + block->len;
		while (i > block->start) {
			i--;

			if (op_array->opcodes[i].opcode == GEAR_OP_DATA) {
				op_data = i;
				continue;
			}

			if (gear_bitset_in(ctx.instr_worklist, i)) {
				gear_bitset_excl(ctx.instr_worklist, i);
				add_operands_to_worklists(&ctx, &op_array->opcodes[i], &ssa->ops[i], 0);
				if (op_data >= 0) {
					add_operands_to_worklists(&ctx, &op_array->opcodes[op_data], &ssa->ops[op_data], 0);
				}
			} else if (may_have_side_effects(op_array, ssa, &op_array->opcodes[i], &ssa->ops[i], ctx.reorder_dtor_effects)
					|| gear_may_throw(&op_array->opcodes[i], op_array, ssa)
					|| (has_varargs && may_break_varargs(op_array, ssa, &ssa->ops[i]))) {
				if (op_array->opcodes[i].opcode == GEAR_NEW
						&& op_array->opcodes[i+1].opcode == GEAR_DO_FCALL
						&& ssa->ops[i].result_def >= 0
						&& ssa->vars[ssa->ops[i].result_def].escape_state == ESCAPE_STATE_NO_ESCAPE) {
					gear_bitset_incl(ctx.instr_dead, i);
					gear_bitset_incl(ctx.instr_dead, i+1);
				} else {
					add_operands_to_worklists(&ctx, &op_array->opcodes[i], &ssa->ops[i], 0);
					if (op_data >= 0) {
						add_operands_to_worklists(&ctx, &op_array->opcodes[op_data], &ssa->ops[op_data], 0);
					}
				}
			} else {
				gear_bitset_incl(ctx.instr_dead, i);
				if (op_data >= 0) {
					gear_bitset_incl(ctx.instr_dead, op_data);
				}
			}
			op_data = -1;
		}
	}

	/* Propagate liveness backwards to all definitions of used vars */
	while (!gear_bitset_empty(ctx.instr_worklist, ctx.instr_worklist_len)
			|| !gear_bitset_empty(ctx.phi_worklist, ctx.phi_worklist_len)) {
		while ((i = gear_bitset_pop_first(ctx.instr_worklist, ctx.instr_worklist_len)) >= 0) {
			gear_bitset_excl(ctx.instr_dead, i);
			add_operands_to_worklists(&ctx, &op_array->opcodes[i], &ssa->ops[i], 1);
			if (i < op_array->last && op_array->opcodes[i+1].opcode == GEAR_OP_DATA) {
				gear_bitset_excl(ctx.instr_dead, i+1);
				add_operands_to_worklists(&ctx, &op_array->opcodes[i+1], &ssa->ops[i+1], 1);
			}
		}
		while ((i = gear_bitset_pop_first(ctx.phi_worklist, ctx.phi_worklist_len)) >= 0) {
			gear_bitset_excl(ctx.phi_dead, i);
			gear_bitset_excl(ctx.phi_worklist_no_val, i);
			add_phi_sources_to_worklists(&ctx, ssa->vars[i].definition_phi, 1);
		}
	}

	if (op_array->live_range) {
		dce_live_ranges(&ctx, op_array, ssa);
	}

	/* Eliminate dead instructions */
	GEAR_BITSET_FOREACH(ctx.instr_dead, ctx.instr_worklist_len, i) {
		removed_ops += dce_instr(&ctx, &op_array->opcodes[i], &ssa->ops[i]);
	} GEAR_BITSET_FOREACH_END();

	/* Improper uses don't count as "uses" for the purpose of instruction elimination,
	 * but we have to retain phis defining them.
	 * Propagate this information backwards, marking any phi with an improperly used
	 * target as non-dead. */
	while ((i = gear_bitset_pop_first(ctx.phi_worklist_no_val, ctx.phi_worklist_len)) >= 0) {
		gear_ssa_phi *phi = ssa->vars[i].definition_phi;
		int source;
		gear_bitset_excl(ctx.phi_dead, i);
		FOREACH_PHI_SOURCE(phi, source) {
			add_to_phi_worklist_no_val(&ctx, source);
		} FOREACH_PHI_SOURCE_END();
	}

	/* Now collect the actually dead phis */
	FOREACH_PHI(phi) {
		if (gear_bitset_in(ctx.phi_dead, phi->ssa_var)) {
			gear_ssa_remove_uses_of_var(ssa, phi->ssa_var);
			gear_ssa_remove_phi(ssa, phi);
		} else {
			/* Remove trivial phis (phis with identical source operands) */
			try_remove_trivial_phi(&ctx, phi);
		}
	} FOREACH_PHI_END();

	return removed_ops;
}
