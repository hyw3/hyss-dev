/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "gear_compile.h"
#include "gear_extensions.h"
#include "Optimizer/gear_optimizer.h"
#include "gear_optimizer_internal.h"
#include "gear_inference.h"
#include "gear_call_graph.h"
#include "gear_func_info.h"
#include "gear_inference.h"
#include "gear_call_graph.h"

typedef int (*gear_op_array_func_t)(gear_call_graph *call_graph, gear_op_array *op_array);

static int gear_op_array_calc(gear_call_graph *call_graph, gear_op_array *op_array)
{
	(void) op_array;

	call_graph->op_arrays_count++;
	return SUCCESS;
}

static int gear_op_array_collect(gear_call_graph *call_graph, gear_op_array *op_array)
{
    gear_func_info *func_info = call_graph->func_infos + call_graph->op_arrays_count;

	GEAR_SET_FUNC_INFO(op_array, func_info);
	call_graph->op_arrays[call_graph->op_arrays_count] = op_array;
	func_info->num = call_graph->op_arrays_count;
	func_info->num_args = -1;
	func_info->return_value_used = -1;
	call_graph->op_arrays_count++;
	return SUCCESS;
}

static int gear_foreach_op_array(gear_call_graph *call_graph, gear_script *script, gear_op_array_func_t func)
{
	gear_class_entry *ce;
	gear_op_array *op_array;

	if (func(call_graph, &script->main_op_array) != SUCCESS) {
		return FAILURE;
	}

	GEAR_HASH_FOREACH_PTR(&script->function_table, op_array) {
		if (func(call_graph, op_array) != SUCCESS) {
			return FAILURE;
		}
	} GEAR_HASH_FOREACH_END();

	GEAR_HASH_FOREACH_PTR(&script->class_table, ce) {
		GEAR_HASH_FOREACH_PTR(&ce->function_table, op_array) {
			if (op_array->scope == ce) {
				if (func(call_graph, op_array) != SUCCESS) {
					return FAILURE;
				}
			}
		} GEAR_HASH_FOREACH_END();
	} GEAR_HASH_FOREACH_END();

	return SUCCESS;
}

int gear_analyze_calls(gear_arena **arena, gear_script *script, uint32_t build_flags, gear_op_array *op_array, gear_func_info *func_info)
{
	gear_op *opline = op_array->opcodes;
	gear_op *end = opline + op_array->last;
	gear_function *func;
	gear_call_info *call_info;
	int call = 0;
	gear_call_info **call_stack;
	ALLOCA_FLAG(use_heap);

	call_stack = do_alloca((op_array->last / 2) * sizeof(gear_call_info*), use_heap);
	call_info = NULL;
	while (opline != end) {
		switch (opline->opcode) {
			case GEAR_INIT_FCALL:
			case GEAR_INIT_METHOD_CALL:
			case GEAR_INIT_STATIC_METHOD_CALL:
				call_stack[call] = call_info;
				func = gear_optimizer_get_called_func(
					script, op_array, opline, (build_flags & GEAR_RT_CONSTANTS) != 0);
				if (func) {
					call_info = gear_arena_calloc(arena, 1, sizeof(gear_call_info) + (sizeof(gear_send_arg_info) * ((int)opline->extended_value - 1)));
					call_info->caller_op_array = op_array;
					call_info->caller_init_opline = opline;
					call_info->caller_call_opline = NULL;
					call_info->callee_func = func;
					call_info->num_args = opline->extended_value;
					call_info->next_callee = func_info->callee_info;
					func_info->callee_info = call_info;

					if (build_flags & GEAR_CALL_TREE) {
						call_info->next_caller = NULL;
					} else if (func->type == GEAR_INTERNAL_FUNCTION) {
						call_info->next_caller = NULL;
					} else {
						gear_func_info *callee_func_info = GEAR_FUNC_INFO(&func->op_array);
						if (callee_func_info) {
							call_info->next_caller = callee_func_info->caller_info;
							callee_func_info->caller_info = call_info;
						} else {
							call_info->next_caller = NULL;
						}
					}
				} else {
					call_info = NULL;
				}
				call++;
				break;
			case GEAR_INIT_FCALL_BY_NAME:
			case GEAR_INIT_NS_FCALL_BY_NAME:
			case GEAR_INIT_DYNAMIC_CALL:
			case GEAR_NEW:
			case GEAR_INIT_USER_CALL:
				call_stack[call] = call_info;
				call_info = NULL;
				call++;
				break;
			case GEAR_DO_FCALL:
			case GEAR_DO_ICALL:
			case GEAR_DO_UCALL:
			case GEAR_DO_FCALL_BY_NAME:
				func_info->flags |= GEAR_FUNC_HAS_CALLS;
				if (call_info) {
					call_info->caller_call_opline = opline;
				}
				call--;
				call_info = call_stack[call];
				break;
			case GEAR_SEND_VAL:
			case GEAR_SEND_VAR:
			case GEAR_SEND_VAL_EX:
			case GEAR_SEND_VAR_EX:
			case GEAR_SEND_FUNC_ARG:
			case GEAR_SEND_REF:
			case GEAR_SEND_VAR_NO_REF:
			case GEAR_SEND_VAR_NO_REF_EX:
			case GEAR_SEND_USER:
				if (call_info) {
					uint32_t num = opline->op2.num;

					if (num > 0) {
						num--;
					}
					call_info->arg_info[num].opline = opline;
				}
				break;
			case GEAR_SEND_ARRAY:
			case GEAR_SEND_UNPACK:
				/* TODO: set info about var_arg call ??? */
				if (call_info) {
					call_info->num_args = -1;
				}
				break;
		}
		opline++;
	}
	free_alloca(call_stack, use_heap);
	return SUCCESS;
}

static int gear_is_indirectly_recursive(gear_op_array *root, gear_op_array *op_array, gear_bitset visited)
{
	gear_func_info *func_info;
	gear_call_info *call_info;
	int ret = 0;

	if (op_array == root) {
		return 1;
	}

	func_info = GEAR_FUNC_INFO(op_array);
	if (gear_bitset_in(visited, func_info->num)) {
		return 0;
	}
	gear_bitset_incl(visited, func_info->num);
	call_info = func_info->caller_info;
	while (call_info) {
		if (gear_is_indirectly_recursive(root, call_info->caller_op_array, visited)) {
			call_info->recursive = 1;
			ret = 1;
		}
		call_info = call_info->next_caller;
	}
	return ret;
}

static void gear_analyze_recursion(gear_call_graph *call_graph)
{
	gear_op_array *op_array;
	gear_func_info *func_info;
	gear_call_info *call_info;
	int i;
	int set_len = gear_bitset_len(call_graph->op_arrays_count);
	gear_bitset visited;
	ALLOCA_FLAG(use_heap);

	visited = GEAR_BITSET_ALLOCA(set_len, use_heap);
	for (i = 0; i < call_graph->op_arrays_count; i++) {
		op_array = call_graph->op_arrays[i];
		func_info = call_graph->func_infos + i;
		call_info = func_info->caller_info;
		while (call_info) {
			if (call_info->caller_op_array == op_array) {
				call_info->recursive = 1;
				func_info->flags |= GEAR_FUNC_RECURSIVE | GEAR_FUNC_RECURSIVE_DIRECTLY;
			} else {
				memset(visited, 0, sizeof(gear_ulong) * set_len);
				if (gear_is_indirectly_recursive(op_array, call_info->caller_op_array, visited)) {
					call_info->recursive = 1;
					func_info->flags |= GEAR_FUNC_RECURSIVE | GEAR_FUNC_RECURSIVE_INDIRECTLY;
				}
			}
			call_info = call_info->next_caller;
		}
	}

	free_alloca(visited, use_heap);
}

static void gear_sort_op_arrays(gear_call_graph *call_graph)
{
	(void) call_graph;

	// TODO: perform topological sort of cyclic call graph
}

int gear_build_call_graph(gear_arena **arena, gear_script *script, uint32_t build_flags, gear_call_graph *call_graph) /* {{{ */
{
	int i;

	call_graph->op_arrays_count = 0;
	if (gear_foreach_op_array(call_graph, script, gear_op_array_calc) != SUCCESS) {
		return FAILURE;
	}
	call_graph->op_arrays = (gear_op_array**)gear_arena_calloc(arena, call_graph->op_arrays_count, sizeof(gear_op_array*));
	call_graph->func_infos = (gear_func_info*)gear_arena_calloc(arena, call_graph->op_arrays_count, sizeof(gear_func_info));
	call_graph->op_arrays_count = 0;
	if (gear_foreach_op_array(call_graph, script, gear_op_array_collect) != SUCCESS) {
		return FAILURE;
	}
	for (i = 0; i < call_graph->op_arrays_count; i++) {
		gear_analyze_calls(arena, script, build_flags, call_graph->op_arrays[i], call_graph->func_infos + i);
	}
	gear_analyze_recursion(call_graph);
	gear_sort_op_arrays(call_graph);

	return SUCCESS;
}
/* }}} */

gear_call_info **gear_build_call_map(gear_arena **arena, gear_func_info *info, gear_op_array *op_array) /* {{{ */
{
	gear_call_info **map, *call;
	if (!info->callee_info) {
		/* Don't build call map if function contains no calls */
		return NULL;
	}

	map = gear_arena_calloc(arena, sizeof(gear_call_info *), op_array->last);
	for (call = info->callee_info; call; call = call->next_callee) {
		int i;
		map[call->caller_init_opline - op_array->opcodes] = call;
		map[call->caller_call_opline - op_array->opcodes] = call;
		for (i = 0; i < call->num_args; i++) {
			if (call->arg_info[i].opline) {
				map[call->arg_info[i].opline - op_array->opcodes] = call;
			}
		}
	}
	return map;
}
/* }}} */

