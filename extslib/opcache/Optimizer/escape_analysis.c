/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_bitset.h"
#include "gear_cfg.h"
#include "gear_ssa.h"
#include "gear_inference.h"
#include "gear_dump.h"

/*
 * T. Kotzmann and H. Mossenbock. Escape analysis  in the context of dynamic
 * compilation and deoptimization. In Proceedings of the International
 * Conference on Virtual Execution Environments, pages 111-120, Chicago,
 * June 2005
 */

static gear_always_inline void union_find_init(int *parent, int *size, int count) /* {{{ */
{
	int i;

	for (i = 0; i < count; i++) {
		parent[i] = i;
		size[i] = 1;
	}
}
/* }}} */

static gear_always_inline int union_find_root(int *parent, int i) /* {{{ */
{
	int p = parent[i];

	while (i != p) {
		p = parent[p];
		parent[i] = p;
		i = p;
		p = parent[i];
	}
	return i;
}
/* }}} */

static gear_always_inline void union_find_unite(int *parent, int *size, int i, int j) /* {{{ */
{
	int r1 = union_find_root(parent, i);
	int r2 = union_find_root(parent, j);

	if (r1 != r2) {
		if (size[r1] < size[r2]) {
			parent[r1] = r2;
			size[r2] += size[r1];
		} else {
			parent[r2] = r1;
			size[r1] += size[r2];
		}
	}
}
/* }}} */

static int gear_build_equi_escape_sets(int *parent, gear_op_array *op_array, gear_ssa *ssa) /* {{{ */
{
	gear_ssa_var *ssa_vars = ssa->vars;
	int ssa_vars_count = ssa->vars_count;
	gear_ssa_phi *p;
	int i, j;
	int *size;
	ALLOCA_FLAG(use_heap)

	size = do_alloca(sizeof(int) * ssa_vars_count, use_heap);
	if (!size) {
		return FAILURE;
	}
	union_find_init(parent, size, ssa_vars_count);

	for (i = 0; i < ssa_vars_count; i++) {
		if (ssa_vars[i].definition_phi) {
			p = ssa_vars[i].definition_phi;
			if (p->pi >= 0) {
				union_find_unite(parent, size, i, p->sources[0]);
			} else {
				for (j = 0; j < ssa->cfg.blocks[p->block].predecessors_count; j++) {
					union_find_unite(parent, size, i, p->sources[j]);
				}
			}
		} else if (ssa_vars[i].definition >= 0) {
			int def = ssa_vars[i].definition;
			gear_ssa_op *op = ssa->ops + def;
			gear_op *opline =  op_array->opcodes + def;

			if (op->op1_def >= 0) {
				if (op->op1_use >= 0) {
					if (opline->opcode != GEAR_ASSIGN) {
						union_find_unite(parent, size, op->op1_def, op->op1_use);
					}
				}
				if (opline->opcode == GEAR_ASSIGN && op->op2_use >= 0) {
					union_find_unite(parent, size, op->op1_def, op->op2_use);
				}
			}
			if (op->op2_def >= 0) {
				if (op->op2_use >= 0) {
					union_find_unite(parent, size, op->op2_def, op->op2_use);
				}
			}
			if (op->result_def >= 0) {
				if (op->result_use >= 0) {
					if (opline->opcode != GEAR_QM_ASSIGN) {
						union_find_unite(parent, size, op->result_def, op->result_use);
					}
				}
				if (opline->opcode == GEAR_QM_ASSIGN && op->op1_use >= 0) {
					union_find_unite(parent, size, op->result_def, op->op1_use);
				}
				if (opline->opcode == GEAR_ASSIGN && op->op2_use >= 0) {
					union_find_unite(parent, size, op->result_def, op->op2_use);
				}
				if (opline->opcode == GEAR_ASSIGN && op->op1_def >= 0) {
					union_find_unite(parent, size, op->result_def, op->op1_def);
				}
			}
		}
	}

	for (i = 0; i < ssa_vars_count; i++) {
		parent[i] = union_find_root(parent, i);
	}

	free_alloca(size, use_heap);

	return SUCCESS;
}
/* }}} */

static inline gear_class_entry *get_class_entry(const gear_script *script, gear_string *lcname) /* {{{ */
{
	gear_class_entry *ce = script ? gear_hash_find_ptr(&script->class_table, lcname) : NULL;
	if (ce) {
		return ce;
	}

	ce = gear_hash_find_ptr(CG(class_table), lcname);
	if (ce && ce->type == GEAR_INTERNAL_CLASS) {
		return ce;
	}

	return NULL;
}
/* }}} */

static int is_allocation_def(gear_op_array *op_array, gear_ssa *ssa, int def, int var, const gear_script *script) /* {{{ */
{
	gear_ssa_op *op = ssa->ops + def;
	gear_op *opline = op_array->opcodes + def;

	if (op->result_def == var) {
		switch (opline->opcode) {
			case GEAR_INIT_ARRAY:
				return 1;
			case GEAR_NEW:
			    /* objects with destructors should escape */
				if (opline->op1_type == IS_CONST) {
					gear_class_entry *ce = get_class_entry(script, Z_STR_P(CRT_CONSTANT_EX(op_array, opline, opline->op1, ssa->rt_constants)+1));
					uint32_t forbidden_flags = GEAR_ACC_INHERITED
						/* These flags will always cause an exception */
						| GEAR_ACC_IMPLICIT_ABSTRACT_CLASS | GEAR_ACC_EXPLICIT_ABSTRACT_CLASS
						| GEAR_ACC_INTERFACE | GEAR_ACC_TRAIT;
					if (ce && !ce->create_object && !ce->constructor &&
					    !ce->destructor && !ce->__get && !ce->__set &&
					    !(ce->ce_flags & forbidden_flags) &&
						(ce->ce_flags & GEAR_ACC_CONSTANTS_UPDATED)) {
						return 1;
					}
				}
				break;
			case GEAR_QM_ASSIGN:
				if (opline->op1_type == IS_CONST
				 && Z_TYPE_P(CRT_CONSTANT_EX(op_array, opline, opline->op1, ssa->rt_constants)) == IS_ARRAY) {
					return 1;
				}
				if (opline->op1_type == IS_CV && (OP1_INFO() & MAY_BE_ARRAY)) {
					return 1;
				}
				break;
			case GEAR_ASSIGN:
				if (opline->op1_type == IS_CV && (OP1_INFO() & MAY_BE_ARRAY)) {
					return 1;
				}
				break;
		}
    } else if (op->op1_def == var) {
		switch (opline->opcode) {
			case GEAR_ASSIGN:
				if (opline->op2_type == IS_CONST
				 && Z_TYPE_P(CRT_CONSTANT_EX(op_array, opline, opline->op2, ssa->rt_constants)) == IS_ARRAY) {
					return 1;
				}
				if (opline->op2_type == IS_CV && (OP2_INFO() & MAY_BE_ARRAY)) {
					return 1;
				}
				break;
			case GEAR_ASSIGN_DIM:
			case GEAR_ASSIGN_OBJ:
				if (OP1_INFO() & (MAY_BE_UNDEF | MAY_BE_NULL | MAY_BE_FALSE)) {
					/* implicit object/array allocation */
					return 1;
				}
				break;
		}
	}

    return 0;
}
/* }}} */

static int is_local_def(gear_op_array *op_array, gear_ssa *ssa, int def, int var, const gear_script *script) /* {{{ */
{
	gear_ssa_op *op = ssa->ops + def;
	gear_op *opline = op_array->opcodes + def;

	if (op->result_def == var) {
		switch (opline->opcode) {
			case GEAR_INIT_ARRAY:
			case GEAR_ADD_ARRAY_ELEMENT:
			case GEAR_QM_ASSIGN:
			case GEAR_ASSIGN:
				return 1;
			case GEAR_NEW:
				/* objects with destructors should escape */
				if (opline->op1_type == IS_CONST) {
					gear_class_entry *ce = get_class_entry(script, Z_STR_P(CRT_CONSTANT_EX(op_array, opline, opline->op1, ssa->rt_constants)+1));
					if (ce && !ce->create_object && !ce->constructor &&
					    !ce->destructor && !ce->__get && !ce->__set &&
					    !(ce->ce_flags & GEAR_ACC_INHERITED)) {
						return 1;
					}
				}
				break;
		}
	} else if (op->op1_def == var) {
		switch (opline->opcode) {
			case GEAR_ASSIGN:
				return 1;
			case GEAR_ASSIGN_DIM:
			case GEAR_ASSIGN_OBJ:
				return 1;
			case GEAR_ASSIGN_ADD:
			case GEAR_ASSIGN_SUB:
			case GEAR_ASSIGN_MUL:
			case GEAR_ASSIGN_DIV:
			case GEAR_ASSIGN_MOD:
			case GEAR_ASSIGN_SL:
			case GEAR_ASSIGN_SR:
			case GEAR_ASSIGN_CONCAT:
			case GEAR_ASSIGN_BW_OR:
			case GEAR_ASSIGN_BW_AND:
			case GEAR_ASSIGN_BW_XOR:
			case GEAR_ASSIGN_POW:
				return (opline->extended_value != 0);
			case GEAR_PRE_INC_OBJ:
			case GEAR_PRE_DEC_OBJ:
			case GEAR_POST_INC_OBJ:
			case GEAR_POST_DEC_OBJ:
				return 1;
		}
	}

	return 0;
}
/* }}} */

static int is_escape_use(gear_op_array *op_array, gear_ssa *ssa, int use, int var) /* {{{ */
{
	gear_ssa_op *op = ssa->ops + use;
	gear_op *opline = op_array->opcodes + use;

	if (op->op1_use == var) {
		switch (opline->opcode) {
			case GEAR_ASSIGN:
				/* no_val */
				break;
			case GEAR_QM_ASSIGN:
				if (opline->op1_type == IS_CV) {
					if (OP1_INFO() & MAY_BE_OBJECT) {
						/* object aliasing */
						return 1;
					}
				}
				break;
			case GEAR_ISSET_ISEMPTY_DIM_OBJ:
			case GEAR_ISSET_ISEMPTY_PROP_OBJ:
			case GEAR_FETCH_DIM_R:
			case GEAR_FETCH_OBJ_R:
			case GEAR_FETCH_DIM_IS:
			case GEAR_FETCH_OBJ_IS:
				break;
			case GEAR_ASSIGN_ADD:
			case GEAR_ASSIGN_SUB:
			case GEAR_ASSIGN_MUL:
			case GEAR_ASSIGN_DIV:
			case GEAR_ASSIGN_MOD:
			case GEAR_ASSIGN_SL:
			case GEAR_ASSIGN_SR:
			case GEAR_ASSIGN_CONCAT:
			case GEAR_ASSIGN_BW_OR:
			case GEAR_ASSIGN_BW_AND:
			case GEAR_ASSIGN_BW_XOR:
			case GEAR_ASSIGN_POW:
				if (!opline->extended_value) {
					return 1;
				}
				/* break missing intentionally */
			case GEAR_ASSIGN_DIM:
			case GEAR_ASSIGN_OBJ:
				break;
			case GEAR_PRE_INC_OBJ:
			case GEAR_PRE_DEC_OBJ:
			case GEAR_POST_INC_OBJ:
			case GEAR_POST_DEC_OBJ:
				break;
			case GEAR_INIT_ARRAY:
			case GEAR_ADD_ARRAY_ELEMENT:
				if (opline->extended_value & GEAR_ARRAY_ELEMENT_REF) {
					return 1;
				}
				if (OP1_INFO() & MAY_BE_OBJECT) {
					/* object aliasing */
					return 1;
				}
				/* reference dependencies processed separately */
				break;
			case GEAR_OP_DATA:
				if ((opline-1)->opcode != GEAR_ASSIGN_DIM
				 && (opline-1)->opcode != GEAR_ASSIGN_OBJ) {
					return 1;
				}
				if (OP1_INFO() & MAY_BE_OBJECT) {
					/* object aliasing */
					return 1;
				}
				opline--;
				op--;
				if (opline->op1_type != IS_CV
				 || (OP1_INFO() & MAY_BE_REF)
				 || (op->op1_def >= 0 && ssa->vars[op->op1_def].alias)) {
					/* asignment into escaping structure */
					return 1;
				}
				/* reference dependencies processed separately */
				break;
			default:
				return 1;
		}
	}

	if (op->op2_use == var) {
		switch (opline->opcode) {
			case GEAR_ASSIGN:
				if (opline->op1_type != IS_CV
				 || (OP1_INFO() & MAY_BE_REF)
				 || (op->op1_def >= 0 && ssa->vars[op->op1_def].alias)) {
					/* asignment into escaping variable */
					return 1;
				}
				if (opline->op2_type == IS_CV || opline->result_type != IS_UNUSED) {
					if (OP2_INFO() & MAY_BE_OBJECT) {
						/* object aliasing */
						return 1;
					}
				}
				break;
			default:
				return 1;
		}
	}

	if (op->result_use == var) {
		switch (opline->opcode) {
			case GEAR_ASSIGN:
			case GEAR_QM_ASSIGN:
			case GEAR_INIT_ARRAY:
			case GEAR_ADD_ARRAY_ELEMENT:
				break;
			default:
				return 1;
		}
	}

	return 0;
}
/* }}} */

int gear_ssa_escape_analysis(const gear_script *script, gear_op_array *op_array, gear_ssa *ssa) /* {{{ */
{
	gear_ssa_var *ssa_vars = ssa->vars;
	int ssa_vars_count = ssa->vars_count;
	int i, root, use;
	int *ees;
	gear_bool has_allocations;
	int num_non_escaped;
	ALLOCA_FLAG(use_heap)

	if (!ssa_vars) {
		return SUCCESS;
	}

	has_allocations = 0;
	for (i = op_array->last_var; i < ssa_vars_count; i++) {
		if (ssa_vars[i].definition >= 0
		  && (ssa->var_info[i].type & (MAY_BE_ARRAY|MAY_BE_OBJECT))
		  && is_allocation_def(op_array, ssa, ssa_vars[i].definition, i, script)) {
			has_allocations = 1;
			break;
		}
	}
	if (!has_allocations) {
		return SUCCESS;
	}


	/* 1. Build EES (Equi-Esape Sets) */
	ees = do_alloca(sizeof(int) * ssa_vars_count, use_heap);
	if (!ees) {
		return FAILURE;
	}

	if (gear_build_equi_escape_sets(ees, op_array, ssa) != SUCCESS) {
		return FAILURE;
	}

	/* 2. Identify Allocations */
	num_non_escaped = 0;
	for (i = op_array->last_var; i < ssa_vars_count; i++) {
		root = ees[i];
		if (ssa_vars[root].escape_state > ESCAPE_STATE_NO_ESCAPE) {
			/* already escape. skip */
		} else if (ssa_vars[i].alias && (ssa->var_info[i].type & MAY_BE_REF)) {
			if (ssa_vars[root].escape_state == ESCAPE_STATE_NO_ESCAPE) {
				num_non_escaped--;
			}
			ssa_vars[root].escape_state = ESCAPE_STATE_GLOBAL_ESCAPE;
		} else if (ssa_vars[i].definition >= 0
			 && (ssa->var_info[i].type & (MAY_BE_ARRAY|MAY_BE_OBJECT))) {
			if (!is_local_def(op_array, ssa, ssa_vars[i].definition, i, script)) {
				if (ssa_vars[root].escape_state == ESCAPE_STATE_NO_ESCAPE) {
					num_non_escaped--;
				}
				ssa_vars[root].escape_state = ESCAPE_STATE_GLOBAL_ESCAPE;
			} else if (ssa_vars[root].escape_state == ESCAPE_STATE_UNKNOWN
			 && is_allocation_def(op_array, ssa, ssa_vars[i].definition, i, script)) {
				ssa_vars[root].escape_state = ESCAPE_STATE_NO_ESCAPE;
				num_non_escaped++;
			}
		}
	}

	/* 3. Mark escaped EES */
	if (num_non_escaped) {
		for (i = 0; i < ssa_vars_count; i++) {
			if (ssa_vars[i].use_chain >= 0) {
				root = ees[i];
				if (ssa_vars[root].escape_state == ESCAPE_STATE_NO_ESCAPE) {
					FOREACH_USE(ssa_vars + i, use) {
						if (is_escape_use(op_array, ssa, use, i)) {
							ssa_vars[root].escape_state = ESCAPE_STATE_GLOBAL_ESCAPE;
							num_non_escaped--;
							if (num_non_escaped == 0) {
								i = ssa_vars_count;
							}
							break;
						}
					} FOREACH_USE_END();
				}
			}
		}
	}

	/* 4. Process referential dependencies */
	if (num_non_escaped) {
		gear_bool changed;

		do {
			changed = 0;
			for (i = 0; i < ssa_vars_count; i++) {
				if (ssa_vars[i].use_chain >= 0) {
					root = ees[i];
					if (ssa_vars[root].escape_state == ESCAPE_STATE_NO_ESCAPE) {
						FOREACH_USE(ssa_vars + i, use) {
							gear_ssa_op *op = ssa->ops + use;
							gear_op *opline = op_array->opcodes + use;
							int enclosing_root;

							if (opline->opcode == GEAR_OP_DATA &&
							    ((opline-1)->opcode == GEAR_ASSIGN_DIM ||
							     (opline-1)->opcode == GEAR_ASSIGN_OBJ) &&
							    op->op1_use == i &&
							    (op-1)->op1_use >= 0) {
								enclosing_root = ees[(op-1)->op1_use];
							} else if ((opline->opcode == GEAR_INIT_ARRAY ||
							     opline->opcode == GEAR_ADD_ARRAY_ELEMENT) &&
							    op->op1_use == i &&
							    op->result_def >= 0) {
								enclosing_root = ees[op->result_def];
							} else {
								continue;
							}

							if (ssa_vars[enclosing_root].escape_state == ESCAPE_STATE_UNKNOWN ||
							    ssa_vars[enclosing_root].escape_state > ssa_vars[root].escape_state) {
							    if (ssa_vars[enclosing_root].escape_state == ESCAPE_STATE_UNKNOWN) {
									ssa_vars[root].escape_state = ESCAPE_STATE_GLOBAL_ESCAPE;
							    } else {
									ssa_vars[root].escape_state = ssa_vars[enclosing_root].escape_state;
								}
								if (ssa_vars[root].escape_state == ESCAPE_STATE_GLOBAL_ESCAPE) {
									num_non_escaped--;
									if (num_non_escaped == 0) {
										i = ssa_vars_count;
										changed = 0;
									} else {
										changed = 1;
									}
									break;
								} else {
									changed = 1;
								}
							}
						} FOREACH_USE_END();
					}
				}
			}
		} while (changed);
	}

	/* 5. Propagate values of escape sets to variables */
	for (i = 0; i < ssa_vars_count; i++) {
		root = ees[i];
		if (i != root) {
			ssa_vars[i].escape_state = ssa_vars[root].escape_state;
		}
	}

	free_alloca(ees, use_heap);

	return SUCCESS;
}
/* }}} */

