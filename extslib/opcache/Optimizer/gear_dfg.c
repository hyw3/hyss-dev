/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "gear_compile.h"
#include "gear_dfg.h"

int gear_build_dfg(const gear_op_array *op_array, const gear_cfg *cfg, gear_dfg *dfg, uint32_t build_flags) /* {{{ */
{
	int set_size;
	gear_basic_block *blocks = cfg->blocks;
	int blocks_count = cfg->blocks_count;
	gear_bitset tmp, def, use, in, out;
	int k;
	uint32_t var_num;
	int j;

	set_size = dfg->size;
	tmp = dfg->tmp;
	def = dfg->def;
	use = dfg->use;
	in  = dfg->in;
	out = dfg->out;

	/* Collect "def" and "use" sets */
	for (j = 0; j < blocks_count; j++) {
		gear_op *opline, *end;
		if ((blocks[j].flags & GEAR_BB_REACHABLE) == 0) {
			continue;
		}

		opline = op_array->opcodes + blocks[j].start;
		end = opline + blocks[j].len;
		for (; opline < end; opline++) {
			if (opline->opcode != GEAR_OP_DATA) {
				gear_op *next = opline + 1;
				if (next < end && next->opcode == GEAR_OP_DATA) {
					if (next->op1_type & (IS_CV|IS_VAR|IS_TMP_VAR)) {
						var_num = EX_VAR_TO_NUM(next->op1.var);
						if (!DFG_ISSET(def, set_size, j, var_num)) {
							DFG_SET(use, set_size, j, var_num);
						}
					}
					if (next->op2_type & (IS_CV|IS_VAR|IS_TMP_VAR)) {
						var_num = EX_VAR_TO_NUM(next->op2.var);
						if (!DFG_ISSET(def, set_size, j, var_num)) {
							DFG_SET(use, set_size, j, var_num);
						}
					}
				}
				if (opline->op1_type == IS_CV) {
					var_num = EX_VAR_TO_NUM(opline->op1.var);
					switch (opline->opcode) {
					case GEAR_ADD_ARRAY_ELEMENT:
					case GEAR_INIT_ARRAY:
						if ((build_flags & GEAR_SSA_RC_INFERENCE)
								|| (opline->extended_value & GEAR_ARRAY_ELEMENT_REF)) {
							goto op1_def;
						}
						goto op1_use;
					case GEAR_FE_RESET_R:
					case GEAR_SEND_VAR:
					case GEAR_CAST:
					case GEAR_QM_ASSIGN:
					case GEAR_JMP_SET:
					case GEAR_COALESCE:
						if (build_flags & GEAR_SSA_RC_INFERENCE) {
							goto op1_def;
						}
						goto op1_use;
					case GEAR_YIELD:
						if ((build_flags & GEAR_SSA_RC_INFERENCE)
								|| (op_array->fn_flags & GEAR_ACC_RETURN_REFERENCE)) {
							goto op1_def;
						}
						goto op1_use;
					case GEAR_UNSET_CV:
					case GEAR_ASSIGN:
					case GEAR_ASSIGN_REF:
					case GEAR_BIND_GLOBAL:
					case GEAR_BIND_STATIC:
					case GEAR_SEND_VAR_EX:
					case GEAR_SEND_FUNC_ARG:
					case GEAR_SEND_REF:
					case GEAR_SEND_VAR_NO_REF:
					case GEAR_SEND_VAR_NO_REF_EX:
					case GEAR_FE_RESET_RW:
					case GEAR_ASSIGN_ADD:
					case GEAR_ASSIGN_SUB:
					case GEAR_ASSIGN_MUL:
					case GEAR_ASSIGN_DIV:
					case GEAR_ASSIGN_MOD:
					case GEAR_ASSIGN_SL:
					case GEAR_ASSIGN_SR:
					case GEAR_ASSIGN_CONCAT:
					case GEAR_ASSIGN_BW_OR:
					case GEAR_ASSIGN_BW_AND:
					case GEAR_ASSIGN_BW_XOR:
					case GEAR_ASSIGN_POW:
					case GEAR_PRE_INC:
					case GEAR_PRE_DEC:
					case GEAR_POST_INC:
					case GEAR_POST_DEC:
					case GEAR_ASSIGN_DIM:
					case GEAR_ASSIGN_OBJ:
					case GEAR_UNSET_DIM:
					case GEAR_UNSET_OBJ:
					case GEAR_FETCH_DIM_W:
					case GEAR_FETCH_DIM_RW:
					case GEAR_FETCH_DIM_FUNC_ARG:
					case GEAR_FETCH_DIM_UNSET:
					case GEAR_FETCH_OBJ_W:
					case GEAR_FETCH_OBJ_RW:
					case GEAR_FETCH_OBJ_FUNC_ARG:
					case GEAR_FETCH_OBJ_UNSET:
					case GEAR_FETCH_LIST_W:
					case GEAR_VERIFY_RETURN_TYPE:
					case GEAR_PRE_INC_OBJ:
					case GEAR_PRE_DEC_OBJ:
					case GEAR_POST_INC_OBJ:
					case GEAR_POST_DEC_OBJ:
op1_def:
						/* `def` always come along with dtor or separation,
						 * thus the origin var info might be also `use`d in the feature(CG) */
						DFG_SET(use, set_size, j, var_num);
						DFG_SET(def, set_size, j, var_num);
						break;
					default:
op1_use:
						if (!DFG_ISSET(def, set_size, j, var_num)) {
							DFG_SET(use, set_size, j, var_num);
						}
					}
				} else if (opline->op1_type & (IS_VAR|IS_TMP_VAR)) {
					var_num = EX_VAR_TO_NUM(opline->op1.var);
					if (!DFG_ISSET(def, set_size, j, var_num)) {
						DFG_SET(use, set_size, j, var_num);
					}
					if (opline->opcode == GEAR_VERIFY_RETURN_TYPE) {
						DFG_SET(def, set_size, j, var_num);
					}
				}
				if (opline->op2_type == IS_CV) {
					var_num = EX_VAR_TO_NUM(opline->op2.var);
					switch (opline->opcode) {
						case GEAR_ASSIGN:
							if (build_flags & GEAR_SSA_RC_INFERENCE) {
								goto op2_def;
							}
							goto op2_use;
						case GEAR_BIND_LEXICAL:
							if ((build_flags & GEAR_SSA_RC_INFERENCE) || (opline->extended_value & GEAR_BIND_REF)) {
								goto op2_def;
							}
							goto op2_use;
						case GEAR_ASSIGN_REF:
						case GEAR_FE_FETCH_R:
						case GEAR_FE_FETCH_RW:
op2_def:
							// FIXME: include into "use" too ...?
							DFG_SET(use, set_size, j, var_num);
							DFG_SET(def, set_size, j, var_num);
							break;
						default:
op2_use:
							if (!DFG_ISSET(def, set_size, j, var_num)) {
								DFG_SET(use, set_size, j, var_num);
							}
							break;
					}
				} else if (opline->op2_type & (IS_VAR|IS_TMP_VAR)) {
					var_num = EX_VAR_TO_NUM(opline->op2.var);
					if (opline->opcode == GEAR_FE_FETCH_R || opline->opcode == GEAR_FE_FETCH_RW) {
						DFG_SET(def, set_size, j, var_num);
					} else {
						if (!DFG_ISSET(def, set_size, j, var_num)) {
							DFG_SET(use, set_size, j, var_num);
						}
					}
				}
				if (opline->result_type & (IS_CV|IS_VAR|IS_TMP_VAR)) {
					var_num = EX_VAR_TO_NUM(opline->result.var);
					if ((build_flags & GEAR_SSA_USE_CV_RESULTS)
					 && opline->result_type == IS_CV) {
						DFG_SET(use, set_size, j, var_num);
					}
					DFG_SET(def, set_size, j, var_num);
				}
			}
		}
	}

	/* Calculate "in" and "out" sets */
	{
		uint32_t worklist_len = gear_bitset_len(blocks_count);
		gear_bitset worklist;
		ALLOCA_FLAG(use_heap);
		worklist = GEAR_BITSET_ALLOCA(worklist_len, use_heap);
		memset(worklist, 0, worklist_len * GEAR_BITSET_ELM_SIZE);
		for (j = 0; j < blocks_count; j++) {
			gear_bitset_incl(worklist, j);
		}
		while (!gear_bitset_empty(worklist, worklist_len)) {
			/* We use the last block on the worklist, because predecessors tend to be located
			 * before the succeeding block, so this converges faster. */
			j = gear_bitset_last(worklist, worklist_len);
			gear_bitset_excl(worklist, j);

			if ((blocks[j].flags & GEAR_BB_REACHABLE) == 0) {
				continue;
			}
			if (blocks[j].successors_count != 0) {
				gear_bitset_copy(DFG_BITSET(out, set_size, j), DFG_BITSET(in, set_size, blocks[j].successors[0]), set_size);
				for (k = 1; k < blocks[j].successors_count; k++) {
					gear_bitset_union(DFG_BITSET(out, set_size, j), DFG_BITSET(in, set_size, blocks[j].successors[k]), set_size);
				}
			} else {
				gear_bitset_clear(DFG_BITSET(out, set_size, j), set_size);
			}
			gear_bitset_union_with_difference(tmp, DFG_BITSET(use, set_size, j), DFG_BITSET(out, set_size, j), DFG_BITSET(def, set_size, j), set_size);
			if (!gear_bitset_equal(DFG_BITSET(in, set_size, j), tmp, set_size)) {
				gear_bitset_copy(DFG_BITSET(in, set_size, j), tmp, set_size);

				/* Add predecessors of changed block to worklist */
				{
					int *predecessors = &cfg->predecessors[blocks[j].predecessor_offset];
					for (k = 0; k < blocks[j].predecessors_count; k++) {
						gear_bitset_incl(worklist, predecessors[k]);
					}
				}
			}
		}

		free_alloca(worklist, use_heap);
	}

	return SUCCESS;
}
/* }}} */

