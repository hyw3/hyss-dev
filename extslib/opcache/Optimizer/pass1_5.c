/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* pass 1
 * - substitute persistent constants (true, false, null, etc)
 * - perform compile-time evaluation of constant binary and unary operations
 * - convert CAST(IS_BOOL,x) into BOOL(x)
 * - pre-evaluate constant function calls
 */

#include "hyss.h"
#include "Optimizer/gear_optimizer.h"
#include "Optimizer/gear_optimizer_internal.h"
#include "gear_API.h"
#include "gear_constants.h"
#include "gear_execute.h"
#include "gear_vm.h"

void gear_optimizer_pass1(gear_op_array *op_array, gear_optimizer_ctx *ctx)
{
	int i = 0;
	gear_op *opline = op_array->opcodes;
	gear_op *end = opline + op_array->last;
	gear_bool collect_constants = (GEAR_OPTIMIZER_PASS_15 & ctx->optimization_level)?
		(op_array == &ctx->script->main_op_array) : 0;

	while (opline < end) {
		switch (opline->opcode) {
		case GEAR_ADD:
		case GEAR_SUB:
		case GEAR_MUL:
		case GEAR_DIV:
		case GEAR_MOD:
		case GEAR_POW:
		case GEAR_SL:
		case GEAR_SR:
		case GEAR_CONCAT:
		case GEAR_FAST_CONCAT:
		case GEAR_IS_EQUAL:
		case GEAR_IS_NOT_EQUAL:
		case GEAR_IS_SMALLER:
		case GEAR_IS_SMALLER_OR_EQUAL:
		case GEAR_IS_IDENTICAL:
		case GEAR_IS_NOT_IDENTICAL:
		case GEAR_BW_OR:
		case GEAR_BW_AND:
		case GEAR_BW_XOR:
		case GEAR_BOOL_XOR:
		case GEAR_SPACESHIP:
		case GEAR_CASE:
			if (opline->op1_type == IS_CONST &&
				opline->op2_type == IS_CONST) {
				/* binary operation with constant operands */
				zval result;

				if (gear_optimizer_eval_binary_op(&result, opline->opcode, &GEAR_OP1_LITERAL(opline), &GEAR_OP2_LITERAL(opline)) == SUCCESS) {
					literal_dtor(&GEAR_OP1_LITERAL(opline));
					literal_dtor(&GEAR_OP2_LITERAL(opline));
					if (gear_optimizer_replace_by_const(op_array, opline + 1, IS_TMP_VAR, opline->result.var, &result)) {
						MAKE_NOP(opline);
					} else {
						opline->opcode = GEAR_QM_ASSIGN;
						SET_UNUSED(opline->op2);
						gear_optimizer_update_op1_const(op_array, opline, &result);
					}
				}
			}
			break;

		case GEAR_CAST:
			if (opline->op1_type == IS_CONST) {
				/* cast of constant operand */
				zval result;

				if (gear_optimizer_eval_cast(&result, opline->extended_value, &GEAR_OP1_LITERAL(opline)) == SUCCESS) {
					literal_dtor(&GEAR_OP1_LITERAL(opline));
					if (gear_optimizer_replace_by_const(op_array, opline + 1, opline->result_type, opline->result.var, &result)) {
						MAKE_NOP(opline);
					} else {
						opline->opcode = GEAR_QM_ASSIGN;
						opline->extended_value = 0;
						gear_optimizer_update_op1_const(op_array, opline, &result);
					}
					break;
				}
			}

			if (opline->extended_value == _IS_BOOL) {
				/* T = CAST(X, IS_BOOL) => T = BOOL(X) */
				opline->opcode = GEAR_BOOL;
				opline->extended_value = 0;
			}
			break;

		case GEAR_BW_NOT:
		case GEAR_BOOL_NOT:
			if (opline->op1_type == IS_CONST) {
				/* unary operation on constant operand */
				zval result;

				if (gear_optimizer_eval_unary_op(&result, opline->opcode, &GEAR_OP1_LITERAL(opline)) == SUCCESS) {
					literal_dtor(&GEAR_OP1_LITERAL(opline));
					if (gear_optimizer_replace_by_const(op_array, opline + 1, IS_TMP_VAR, opline->result.var, &result)) {
						MAKE_NOP(opline);
					} else {
						opline->opcode = GEAR_QM_ASSIGN;
						gear_optimizer_update_op1_const(op_array, opline, &result);
					}
				}
			}
			break;

		case GEAR_FETCH_CONSTANT:
			if (opline->op2_type == IS_CONST &&
				Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_STRING &&
				Z_STRLEN(GEAR_OP2_LITERAL(opline)) == sizeof("__COMPILER_HALT_OFFSET__") - 1 &&
				memcmp(Z_STRVAL(GEAR_OP2_LITERAL(opline)), "__COMPILER_HALT_OFFSET__", sizeof("__COMPILER_HALT_OFFSET__") - 1) == 0) {
				/* substitute __COMPILER_HALT_OFFSET__ constant */
				gear_execute_data *orig_execute_data = EG(current_execute_data);
				gear_execute_data fake_execute_data;
				zval *offset;

				memset(&fake_execute_data, 0, sizeof(gear_execute_data));
				fake_execute_data.func = (gear_function*)op_array;
				EG(current_execute_data) = &fake_execute_data;
				if ((offset = gear_get_constant_str("__COMPILER_HALT_OFFSET__", sizeof("__COMPILER_HALT_OFFSET__") - 1)) != NULL) {

					literal_dtor(&GEAR_OP2_LITERAL(opline));
					if (gear_optimizer_replace_by_const(op_array, opline, IS_TMP_VAR, opline->result.var, offset)) {
						MAKE_NOP(opline);
					} else {
						opline->opcode = GEAR_QM_ASSIGN;
						opline->extended_value = 0;
						SET_UNUSED(opline->op2);
						gear_optimizer_update_op1_const(op_array, opline, offset);
					}
				}
				EG(current_execute_data) = orig_execute_data;
				break;
			}

			if (opline->op2_type == IS_CONST &&
				Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_STRING) {
				/* substitute persistent constants */
				zval c;

				if (!gear_optimizer_get_persistent_constant(Z_STR(GEAR_OP2_LITERAL(opline)), &c, 1)) {
					if (!ctx->constants || !gear_optimizer_get_collected_constant(ctx->constants, &GEAR_OP2_LITERAL(opline), &c)) {
						break;
					}
				}
				if (Z_TYPE(c) == IS_CONSTANT_AST) {
					break;
				}
				literal_dtor(&GEAR_OP2_LITERAL(opline));
				if (gear_optimizer_replace_by_const(op_array, opline, IS_TMP_VAR, opline->result.var, &c)) {
					MAKE_NOP(opline);
				} else {
					opline->opcode = GEAR_QM_ASSIGN;
					opline->extended_value = 0;
					SET_UNUSED(opline->op2);
					gear_optimizer_update_op1_const(op_array, opline, &c);
				}
			}
			break;

		case GEAR_FETCH_CLASS_CONSTANT:
			if (opline->op2_type == IS_CONST &&
				Z_TYPE(GEAR_OP2_LITERAL(opline)) == IS_STRING) {

				gear_class_entry *ce = NULL;

				if (opline->op1_type == IS_CONST &&
			        Z_TYPE(GEAR_OP1_LITERAL(opline)) == IS_STRING) {
					/* for A::B */
					if (op_array->scope &&
						!strncasecmp(Z_STRVAL(GEAR_OP1_LITERAL(opline)),
						ZSTR_VAL(op_array->scope->name), Z_STRLEN(GEAR_OP1_LITERAL(opline)) + 1)) {
						ce = op_array->scope;
					} else {
						if ((ce = gear_hash_find_ptr(EG(class_table),
								Z_STR(op_array->literals[opline->op1.constant + 1]))) == NULL ||
								(ce->type == GEAR_INTERNAL_CLASS &&
								 ce->info.internal.cAPI->type != CAPI_PERSISTENT) ||
								(ce->type == GEAR_USER_CLASS &&
								 ce->info.user.filename != op_array->filename)) {
							break;
						}
					}
				} else if (op_array->scope &&
					opline->op1_type == IS_UNUSED &&
					(opline->op1.num & GEAR_FETCH_CLASS_MASK) == GEAR_FETCH_CLASS_SELF) {
					/* for self::B */
					ce = op_array->scope;
				} else if (op_array->scope &&
					opline->op1_type == IS_VAR &&
					(opline - 1)->opcode == GEAR_FETCH_CLASS &&
					((opline - 1)->op2_type == IS_UNUSED &&
					((opline - 1)->op1.num & GEAR_FETCH_CLASS_MASK) == GEAR_FETCH_CLASS_SELF) &&
					(opline - 1)->result.var == opline->op1.var) {
					/* for self::B */
					ce = op_array->scope;
				}

				if (ce) {
					gear_class_constant *cc;
					zval *c, t;

					if ((cc = gear_hash_find_ptr(&ce->constants_table,
							Z_STR(GEAR_OP2_LITERAL(opline)))) != NULL &&
						(Z_ACCESS_FLAGS(cc->value) & GEAR_ACC_PPP_MASK) == GEAR_ACC_PUBLIC) {
						c = &cc->value;
						if (Z_TYPE_P(c) == IS_CONSTANT_AST) {
							gear_ast *ast = Z_ASTVAL_P(c);
							if (ast->kind != GEAR_AST_CONSTANT
							 || !gear_optimizer_get_persistent_constant(gear_ast_get_constant_name(ast), &t, 1)
							 || Z_TYPE(t) == IS_CONSTANT_AST) {
								break;
							}
						} else {
							ZVAL_COPY_OR_DUP(&t, c);
						}

						if (opline->op1_type == IS_CONST) {
							literal_dtor(&GEAR_OP1_LITERAL(opline));
						} else if (opline->op1_type == IS_VAR) {
							MAKE_NOP((opline - 1));
						}
						literal_dtor(&GEAR_OP2_LITERAL(opline));

						if (gear_optimizer_replace_by_const(op_array, opline, IS_TMP_VAR, opline->result.var, &t)) {
							MAKE_NOP(opline);
						} else {
							opline->opcode = GEAR_QM_ASSIGN;
							opline->extended_value = 0;
							SET_UNUSED(opline->op2);
							gear_optimizer_update_op1_const(op_array, opline, &t);
						}
					}
				}
			}
			break;

		case GEAR_DO_ICALL: {
			gear_op *send1_opline = opline - 1;
			gear_op *send2_opline = NULL;
			gear_op *init_opline = NULL;

			while (send1_opline->opcode == GEAR_NOP) {
				send1_opline--;
			}
			if (send1_opline->opcode != GEAR_SEND_VAL ||
			    send1_opline->op1_type != IS_CONST) {
				/* don't colllect constants after unknown function call */
				collect_constants = 0;
				break;
			}
			if (send1_opline->op2.num == 2) {
				send2_opline = send1_opline;
				send1_opline--;
				while (send1_opline->opcode == GEAR_NOP) {
					send1_opline--;
				}
				if (send1_opline->opcode != GEAR_SEND_VAL ||
				    send1_opline->op1_type != IS_CONST) {
					/* don't colllect constants after unknown function call */
					collect_constants = 0;
					break;
				}
			}
			init_opline = send1_opline - 1;
			while (init_opline->opcode == GEAR_NOP) {
				init_opline--;
			}
			if (init_opline->opcode != GEAR_INIT_FCALL ||
			    init_opline->op2_type != IS_CONST ||
			    Z_TYPE(GEAR_OP2_LITERAL(init_opline)) != IS_STRING) {
				/* don't colllect constants after unknown function call */
				collect_constants = 0;
				break;
			}

			/* define("name", scalar); */
			if (Z_STRLEN(GEAR_OP2_LITERAL(init_opline)) == sizeof("define")-1 &&
			    gear_binary_strcasecmp(Z_STRVAL(GEAR_OP2_LITERAL(init_opline)), Z_STRLEN(GEAR_OP2_LITERAL(init_opline)), "define", sizeof("define")-1) == 0) {

				if (Z_TYPE(GEAR_OP1_LITERAL(send1_opline)) == IS_STRING &&
				    send2_opline &&
				    Z_TYPE(GEAR_OP1_LITERAL(send2_opline)) <= IS_STRING) {

					if (collect_constants) {
						gear_optimizer_collect_constant(ctx, &GEAR_OP1_LITERAL(send1_opline), &GEAR_OP1_LITERAL(send2_opline));
					}

					if (RESULT_UNUSED(opline) &&
					    !gear_memnstr(Z_STRVAL(GEAR_OP1_LITERAL(send1_opline)), "::", sizeof("::") - 1, Z_STRVAL(GEAR_OP1_LITERAL(send1_opline)) + Z_STRLEN(GEAR_OP1_LITERAL(send1_opline)))) {

						opline->opcode = GEAR_DECLARE_CONST;
						opline->op1_type = IS_CONST;
						opline->op2_type = IS_CONST;
						opline->result_type = IS_UNUSED;
						opline->op1.constant = send1_opline->op1.constant;
						opline->op2.constant = send2_opline->op1.constant;
						opline->result.num = 0;

						literal_dtor(&GEAR_OP2_LITERAL(init_opline));
						MAKE_NOP(init_opline);
						MAKE_NOP(send1_opline);
						MAKE_NOP(send2_opline);
					}
					break;
				}
			}

			/* pre-evaluate constant functions:
			   constant(x)
			   function_exists(x)
			   is_callable(x)
			   extension_loaded(x)
			*/
			if (!send2_opline &&
			    Z_TYPE(GEAR_OP1_LITERAL(send1_opline)) == IS_STRING) {
				if ((Z_STRLEN(GEAR_OP2_LITERAL(init_opline)) == sizeof("function_exists")-1 &&
					!memcmp(Z_STRVAL(GEAR_OP2_LITERAL(init_opline)),
						"function_exists", sizeof("function_exists")-1) &&
					!gear_optimizer_is_disabled_func("function_exists", sizeof("function_exists") - 1)) ||
					(Z_STRLEN(GEAR_OP2_LITERAL(init_opline)) == sizeof("is_callable")-1 &&
					!memcmp(Z_STRVAL(GEAR_OP2_LITERAL(init_opline)),
						"is_callable", sizeof("is_callable")) &&
					!gear_optimizer_is_disabled_func("is_callable", sizeof("is_callable") - 1))) {
					gear_internal_function *func;
					gear_string *lc_name = gear_string_tolower(
							Z_STR(GEAR_OP1_LITERAL(send1_opline)));

					if ((func = gear_hash_find_ptr(EG(function_table), lc_name)) != NULL
						 && func->type == GEAR_INTERNAL_FUNCTION
						 && func->cAPI->type == CAPI_PERSISTENT
#ifdef GEAR_WIN32
						 && func->cAPI->handle == NULL
#endif
						) {
						zval t;
						if (Z_STRLEN(GEAR_OP2_LITERAL(init_opline)) == sizeof("is_callable") - 1 ||
								func->handler != GEAR_FN(display_disabled_function)) {
							ZVAL_TRUE(&t);
						} else {
							ZVAL_FALSE(&t);
						}
						literal_dtor(&GEAR_OP2_LITERAL(init_opline));
						MAKE_NOP(init_opline);
						literal_dtor(&GEAR_OP1_LITERAL(send1_opline));
						MAKE_NOP(send1_opline);
						if (gear_optimizer_replace_by_const(op_array, opline + 1, IS_VAR, opline->result.var, &t)) {
							MAKE_NOP(opline);
						} else {
							opline->opcode = GEAR_QM_ASSIGN;
							opline->extended_value = 0;
							SET_UNUSED(opline->op2);
							gear_optimizer_update_op1_const(op_array, opline, &t);
						}
					}
					gear_string_release_ex(lc_name, 0);
					break;
				} else if (Z_STRLEN(GEAR_OP2_LITERAL(init_opline)) == sizeof("extension_loaded")-1 &&
					!memcmp(Z_STRVAL(GEAR_OP2_LITERAL(init_opline)),
						"extension_loaded", sizeof("extension_loaded")-1) &&
					!gear_optimizer_is_disabled_func("extension_loaded", sizeof("extension_loaded") - 1)) {
					zval t;
					gear_string *lc_name = gear_string_tolower(
							Z_STR(GEAR_OP1_LITERAL(send1_opline)));
					gear_capi_entry *m = gear_hash_find_ptr(&capi_registry,
							lc_name);

					gear_string_release_ex(lc_name, 0);
					if (!m) {
						if (PG(enable_dl)) {
							break;
						} else {
							ZVAL_FALSE(&t);
						}
					} else {
						if (m->type == CAPI_PERSISTENT
#ifdef GEAR_WIN32
						 && m->handle == NULL
#endif
						) {
							ZVAL_TRUE(&t);
						} else {
							break;
						}
					}

					literal_dtor(&GEAR_OP2_LITERAL(init_opline));
					MAKE_NOP(init_opline);
					literal_dtor(&GEAR_OP1_LITERAL(send1_opline));
					MAKE_NOP(send1_opline);
					if (gear_optimizer_replace_by_const(op_array, opline + 1, IS_VAR, opline->result.var, &t)) {
						MAKE_NOP(opline);
					} else {
						opline->opcode = GEAR_QM_ASSIGN;
						opline->extended_value = 0;
						SET_UNUSED(opline->op2);
						gear_optimizer_update_op1_const(op_array, opline, &t);
					}
					break;
				} else if (Z_STRLEN(GEAR_OP2_LITERAL(init_opline)) == sizeof("constant")-1 &&
					!memcmp(Z_STRVAL(GEAR_OP2_LITERAL(init_opline)),
						"constant", sizeof("constant")-1) &&
					!gear_optimizer_is_disabled_func("constant", sizeof("constant") - 1)) {
					zval t;

					if (gear_optimizer_get_persistent_constant(Z_STR(GEAR_OP1_LITERAL(send1_opline)), &t, 1)) {
						literal_dtor(&GEAR_OP2_LITERAL(init_opline));
						MAKE_NOP(init_opline);
						literal_dtor(&GEAR_OP1_LITERAL(send1_opline));
						MAKE_NOP(send1_opline);
						if (gear_optimizer_replace_by_const(op_array, opline + 1, IS_VAR, opline->result.var, &t)) {
							MAKE_NOP(opline);
						} else {
							opline->opcode = GEAR_QM_ASSIGN;
							opline->extended_value = 0;
							SET_UNUSED(opline->op2);
							gear_optimizer_update_op1_const(op_array, opline, &t);
						}
					}
					break;
				/* dirname(IS_CONST/IS_STRING) -> IS_CONST/IS_STRING */
				} else if (Z_STRLEN(GEAR_OP2_LITERAL(init_opline)) == sizeof("dirname")-1 &&
					!memcmp(Z_STRVAL(GEAR_OP2_LITERAL(init_opline)),
						"dirname", sizeof("dirname") - 1) &&
					!gear_optimizer_is_disabled_func("dirname", sizeof("dirname") - 1) &&
					IS_ABSOLUTE_PATH(Z_STRVAL(GEAR_OP1_LITERAL(send1_opline)), Z_STRLEN(GEAR_OP1_LITERAL(send1_opline)))) {
					gear_string *dirname = gear_string_init(Z_STRVAL(GEAR_OP1_LITERAL(send1_opline)), Z_STRLEN(GEAR_OP1_LITERAL(send1_opline)), 0);
					ZSTR_LEN(dirname) = gear_dirname(ZSTR_VAL(dirname), ZSTR_LEN(dirname));
					if (IS_ABSOLUTE_PATH(ZSTR_VAL(dirname), ZSTR_LEN(dirname))) {
						zval t;

						ZVAL_STR(&t, dirname);
						literal_dtor(&GEAR_OP2_LITERAL(init_opline));
						MAKE_NOP(init_opline);
						literal_dtor(&GEAR_OP1_LITERAL(send1_opline));
						MAKE_NOP(send1_opline);
						if (gear_optimizer_replace_by_const(op_array, opline + 1, IS_VAR, opline->result.var, &t)) {
							MAKE_NOP(opline);
						} else {
							opline->opcode = GEAR_QM_ASSIGN;
							opline->extended_value = 0;
							SET_UNUSED(opline->op2);
							gear_optimizer_update_op1_const(op_array, opline, &t);
						}
					} else {
						gear_string_release_ex(dirname, 0);
					}
					break;
				}
			}
			/* don't colllect constants after any other function call */
			collect_constants = 0;
			break;
		}
		case GEAR_STRLEN:
			if (opline->op1_type == IS_CONST) {
				zval t;

				if (gear_optimizer_eval_strlen(&t, &GEAR_OP1_LITERAL(opline)) == SUCCESS) {
					literal_dtor(&GEAR_OP1_LITERAL(opline));
					if (gear_optimizer_replace_by_const(op_array, opline + 1, IS_TMP_VAR, opline->result.var, &t)) {
						MAKE_NOP(opline);
					} else {
						opline->opcode = GEAR_QM_ASSIGN;
						gear_optimizer_update_op1_const(op_array, opline, &t);
					}
				}
			}
			break;
		case GEAR_DEFINED:
			{
				zval c;
				if (!gear_optimizer_get_persistent_constant(Z_STR(GEAR_OP1_LITERAL(opline)), &c, 0)) {
					break;
				}
				ZVAL_TRUE(&c);
				literal_dtor(&GEAR_OP1_LITERAL(opline));
				if (gear_optimizer_replace_by_const(op_array, opline, IS_TMP_VAR, opline->result.var, &c)) {
					MAKE_NOP(opline);
				} else {
					opline->opcode = GEAR_QM_ASSIGN;
					gear_optimizer_update_op1_const(op_array, opline, &c);
				}
			}
			break;
		case GEAR_DECLARE_CONST:
			if (collect_constants &&
			    Z_TYPE(GEAR_OP1_LITERAL(opline)) == IS_STRING &&
			    Z_TYPE(GEAR_OP2_LITERAL(opline)) <= IS_STRING) {
				gear_optimizer_collect_constant(ctx, &GEAR_OP1_LITERAL(opline), &GEAR_OP2_LITERAL(opline));
			}
			break;

		case GEAR_RETURN:
		case GEAR_RETURN_BY_REF:
		case GEAR_GENERATOR_RETURN:
		case GEAR_EXIT:
		case GEAR_THROW:
		case GEAR_CATCH:
		case GEAR_FAST_CALL:
		case GEAR_FAST_RET:
		case GEAR_JMP:
		case GEAR_JMPZNZ:
		case GEAR_JMPZ:
		case GEAR_JMPNZ:
		case GEAR_JMPZ_EX:
		case GEAR_JMPNZ_EX:
		case GEAR_FE_RESET_R:
		case GEAR_FE_RESET_RW:
		case GEAR_FE_FETCH_R:
		case GEAR_FE_FETCH_RW:
		case GEAR_JMP_SET:
		case GEAR_COALESCE:
		case GEAR_ASSERT_CHECK:
			collect_constants = 0;
			break;
		}
		opline++;
		i++;
	}
}
