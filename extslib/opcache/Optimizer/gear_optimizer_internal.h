/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_OPTIMIZER_INTERNAL_H
#define GEAR_OPTIMIZER_INTERNAL_H

#include "gear_ssa.h"
#include "gear_func_info.h"

#define GEAR_OP1_LITERAL(opline)		(op_array)->literals[(opline)->op1.constant]
#define GEAR_OP1_JMP_ADDR(opline)		OP_JMP_ADDR(opline, (opline)->op1)
#define GEAR_OP2_LITERAL(opline)		(op_array)->literals[(opline)->op2.constant]
#define GEAR_OP2_JMP_ADDR(opline)		OP_JMP_ADDR(opline, (opline)->op2)

#define VAR_NUM(v) EX_VAR_TO_NUM(v)
#define NUM_VAR(v) ((uint32_t)(gear_uintptr_t)GEAR_CALL_VAR_NUM(0, v))

#define INV_COND(op)       ((op) == GEAR_JMPZ    ? GEAR_JMPNZ    : GEAR_JMPZ)
#define INV_EX_COND(op)    ((op) == GEAR_JMPZ_EX ? GEAR_JMPNZ    : GEAR_JMPZ)
#define INV_COND_EX(op)    ((op) == GEAR_JMPZ    ? GEAR_JMPNZ_EX : GEAR_JMPZ_EX)
#define INV_EX_COND_EX(op) ((op) == GEAR_JMPZ_EX ? GEAR_JMPNZ_EX : GEAR_JMPZ_EX)

#define RESULT_UNUSED(op)	(op->result_type == IS_UNUSED)
#define SAME_VAR(op1, op2)  (op1 ## _type == op2 ## _type && op1.var == op2.var)

typedef struct _gear_optimizer_ctx {
	gear_arena             *arena;
	gear_script            *script;
	HashTable              *constants;
	gear_long               optimization_level;
	gear_long               debug_level;
} gear_optimizer_ctx;

#define LITERAL_LONG(op, val) do { \
		zval _c; \
		ZVAL_LONG(&_c, val); \
		op.constant = gear_optimizer_add_literal(op_array, &_c); \
	} while (0)

#define LITERAL_BOOL(op, val) do { \
		zval _c; \
		ZVAL_BOOL(&_c, val); \
		op.constant = gear_optimizer_add_literal(op_array, &_c); \
	} while (0)

#define literal_dtor(zv) do { \
		zval_ptr_dtor_nogc(zv); \
		ZVAL_NULL(zv); \
	} while (0)

#define COPY_NODE(target, src) do { \
		target ## _type = src ## _type; \
		target = src; \
	} while (0)

int  gear_optimizer_add_literal(gear_op_array *op_array, zval *zv);
int  gear_optimizer_get_persistent_constant(gear_string *name, zval *result, int copy);
void gear_optimizer_collect_constant(gear_optimizer_ctx *ctx, zval *name, zval* value);
int  gear_optimizer_get_collected_constant(HashTable *constants, zval *name, zval* value);
int gear_optimizer_eval_binary_op(zval *result, gear_uchar opcode, zval *op1, zval *op2);
int gear_optimizer_eval_unary_op(zval *result, gear_uchar opcode, zval *op1);
int gear_optimizer_eval_cast(zval *result, uint32_t type, zval *op1);
int gear_optimizer_eval_strlen(zval *result, zval *op1);
int gear_optimizer_update_op1_const(gear_op_array *op_array,
                                    gear_op       *opline,
                                    zval          *val);
int gear_optimizer_update_op2_const(gear_op_array *op_array,
                                    gear_op       *opline,
                                    zval          *val);
int  gear_optimizer_replace_by_const(gear_op_array *op_array,
                                     gear_op       *opline,
                                     gear_uchar     type,
                                     uint32_t       var,
                                     zval          *val);

void gear_optimizer_remove_live_range(gear_op_array *op_array, uint32_t var);
void gear_optimizer_remove_live_range_ex(gear_op_array *op_array, uint32_t var, uint32_t start);
void gear_optimizer_pass1(gear_op_array *op_array, gear_optimizer_ctx *ctx);
void gear_optimizer_pass2(gear_op_array *op_array);
void gear_optimizer_pass3(gear_op_array *op_array, gear_optimizer_ctx *ctx);
void gear_optimize_func_calls(gear_op_array *op_array, gear_optimizer_ctx *ctx);
void gear_optimize_cfg(gear_op_array *op_array, gear_optimizer_ctx *ctx);
void gear_optimize_dfa(gear_op_array *op_array, gear_optimizer_ctx *ctx);
int  gear_dfa_analyze_op_array(gear_op_array *op_array, gear_optimizer_ctx *ctx, gear_ssa *ssa);
void gear_dfa_optimize_op_array(gear_op_array *op_array, gear_optimizer_ctx *ctx, gear_ssa *ssa, gear_call_info **call_map);
void gear_optimize_temporary_variables(gear_op_array *op_array, gear_optimizer_ctx *ctx);
void gear_optimizer_nop_removal(gear_op_array *op_array, gear_optimizer_ctx *ctx);
void gear_optimizer_compact_literals(gear_op_array *op_array, gear_optimizer_ctx *ctx);
void gear_optimizer_compact_vars(gear_op_array *op_array);
int gear_optimizer_is_disabled_func(const char *name, size_t len);
gear_function *gear_optimizer_get_called_func(
		gear_script *script, gear_op_array *op_array, gear_op *opline, gear_bool rt_constants);
uint32_t gear_optimizer_classify_function(gear_string *name, uint32_t num_args);
void gear_optimizer_migrate_jump(gear_op_array *op_array, gear_op *new_opline, gear_op *opline);
void gear_optimizer_shift_jump(gear_op_array *op_array, gear_op *opline, uint32_t *shiftlist);
gear_uchar gear_compound_assign_to_binary_op(gear_uchar opcode);
int sccp_optimize_op_array(gear_optimizer_ctx *ctx, gear_op_array *op_arrya, gear_ssa *ssa, gear_call_info **call_map);
int dce_optimize_op_array(gear_op_array *op_array, gear_ssa *ssa, gear_bool reorder_dtor_effects);
int gear_ssa_escape_analysis(const gear_script *script, gear_op_array *op_array, gear_ssa *ssa);

#endif
