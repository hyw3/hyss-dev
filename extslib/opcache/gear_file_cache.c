/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_virtual_cwd.h"
#include "gear_compile.h"
#include "gear_vm.h"
#include "gear_interfaces.h"

#include "hyss.h"
#ifdef GEAR_WIN32
#include "extslib/standard/md5.h"
#endif

#ifdef HAVE_OPCACHE_FILE_CACHE

#include "GearAccelerator.h"
#include "gear_file_cache.h"
#include "gear_shared_alloc.h"
#include "gear_accelerator_util_funcs.h"
#include "gear_accelerator_hash.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_SYS_UIO_H
# include <sys/uio.h>
#endif

#ifdef HAVE_SYS_FILE_H
# include <sys/file.h>
#endif

#ifndef GEAR_WIN32
#define gear_file_cache_unlink unlink
#define gear_file_cache_open open
#else
#define gear_file_cache_unlink hyss_win32_ioutil_unlink
#define gear_file_cache_open hyss_win32_ioutil_open
#endif

#ifdef GEAR_WIN32
# define LOCK_SH 0
# define LOCK_EX 1
# define LOCK_UN 2
static int gear_file_cache_flock(int fd, int op)
{
	OVERLAPPED offset = {0,0,0,0,NULL};
	if (op == LOCK_EX) {
		if (LockFileEx((HANDLE)_get_osfhandle(fd),
		               LOCKFILE_EXCLUSIVE_LOCK, 0, 1, 0, &offset) == TRUE) {
			return 0;
		}
	} else if (op == LOCK_SH) {
		if (LockFileEx((HANDLE)_get_osfhandle(fd),
		               0, 0, 1, 0, &offset) == TRUE) {
			return 0;
		}
	} else if (op == LOCK_UN) {
		if (UnlockFileEx((HANDLE)_get_osfhandle(fd),
		                 0, 1, 0, &offset) == TRUE) {
			return 0;
		}
	}
	return -1;
}
#elif defined(HAVE_FLOCK)
# define gear_file_cache_flock flock
#else
# define LOCK_SH 0
# define LOCK_EX 1
# define LOCK_UN 2
static int gear_file_cache_flock(int fd, int type)
{
	return 0;
}
#endif

#ifndef O_BINARY
#  define O_BINARY 0
#endif

#define SUFFIX ".bin"

#define IS_SERIALIZED_INTERNED(ptr) \
	((size_t)(ptr) & Z_UL(1))

/* Allowing == here to account for a potential empty allocation at the end of the memory */
#define IS_SERIALIZED(ptr) \
	((char*)(ptr) <= (char*)script->size)
#define IS_UNSERIALIZED(ptr) \
	(((char*)(ptr) >= (char*)script->mem && (char*)(ptr) < (char*)script->mem + script->size) || \
	 IS_ACCEL_INTERNED(ptr))
#define SERIALIZE_PTR(ptr) do { \
		if (ptr) { \
			GEAR_ASSERT(IS_UNSERIALIZED(ptr)); \
			(ptr) = (void*)((char*)(ptr) - (char*)script->mem); \
		} \
	} while (0)
#define UNSERIALIZE_PTR(ptr) do { \
		if (ptr) { \
			GEAR_ASSERT(IS_SERIALIZED(ptr)); \
			(ptr) = (void*)((char*)buf + (size_t)(ptr)); \
		} \
	} while (0)
#define SERIALIZE_STR(ptr) do { \
		if (ptr) { \
			if (IS_ACCEL_INTERNED(ptr)) { \
				(ptr) = gear_file_cache_serialize_interned((gear_string*)(ptr), info); \
			} else { \
				GEAR_ASSERT(IS_UNSERIALIZED(ptr)); \
				/* script->corrupted shows if the script in SHM or not */ \
				if (EXPECTED(script->corrupted)) { \
					GC_ADD_FLAGS(ptr, IS_STR_INTERNED); \
					GC_DEL_FLAGS(ptr, IS_STR_PERMANENT); \
				} \
				(ptr) = (void*)((char*)(ptr) - (char*)script->mem); \
			} \
		} \
	} while (0)
#define UNSERIALIZE_STR(ptr) do { \
		if (ptr) { \
			if (IS_SERIALIZED_INTERNED(ptr)) { \
				(ptr) = (void*)gear_file_cache_unserialize_interned((gear_string*)(ptr), !script->corrupted); \
			} else { \
				GEAR_ASSERT(IS_SERIALIZED(ptr)); \
				(ptr) = (void*)((char*)buf + (size_t)(ptr)); \
				/* script->corrupted shows if the script in SHM or not */ \
				if (EXPECTED(!script->corrupted)) { \
					GC_ADD_FLAGS(ptr, IS_STR_INTERNED | IS_STR_PERMANENT); \
				} else { \
					GC_ADD_FLAGS(ptr, IS_STR_INTERNED); \
					GC_DEL_FLAGS(ptr, IS_STR_PERMANENT); \
				} \
			} \
		} \
	} while (0)

static const uint32_t uninitialized_bucket[-HT_MIN_MASK] =
	{HT_INVALID_IDX, HT_INVALID_IDX};

typedef struct _gear_file_cache_metainfo {
	char         magic[8];
	char         system_id[32];
	size_t       mem_size;
	size_t       str_size;
	size_t       script_offset;
	accel_time_t timestamp;
	uint32_t     checksum;
} gear_file_cache_metainfo;

static int gear_file_cache_mkdir(char *filename, size_t start)
{
	char *s = filename + start;

	while (*s) {
		if (IS_SLASH(*s)) {
			char old = *s;
			*s = '\000';
#ifndef GEAR_WIN32
			if (mkdir(filename, S_IRWXU) < 0 && errno != EEXIST) {
#else
			if (hyss_win32_ioutil_mkdir(filename, 0700) < 0 && errno != EEXIST) {
#endif
				*s = old;
				return FAILURE;
			}
			*s = old;
		}
		s++;
	}
	return SUCCESS;
}

typedef void (*serialize_callback_t)(zval                     *zv,
                                     gear_persistent_script   *script,
                                     gear_file_cache_metainfo *info,
                                     void                     *buf);

typedef void (*unserialize_callback_t)(zval                    *zv,
                                       gear_persistent_script  *script,
                                       void                    *buf);

static void gear_file_cache_serialize_zval(zval                     *zv,
                                           gear_persistent_script   *script,
                                           gear_file_cache_metainfo *info,
                                           void                     *buf);
static void gear_file_cache_unserialize_zval(zval                    *zv,
                                             gear_persistent_script  *script,
                                             void                    *buf);

static void *gear_file_cache_serialize_interned(gear_string              *str,
                                                gear_file_cache_metainfo *info)
{
	size_t len;
	void *ret;

	/* check if the same interned string was already stored */
	ret = gear_shared_alloc_get_xlat_entry(str);
	if (ret) {
		return ret;
	}

	len = GEAR_MM_ALIGNED_SIZE(_ZSTR_STRUCT_SIZE(ZSTR_LEN(str)));
	ret = (void*)(info->str_size | Z_UL(1));
	gear_shared_alloc_register_xlat_entry(str, ret);
	if (info->str_size + len > ZSTR_LEN((gear_string*)ZCG(mem))) {
		size_t new_len = info->str_size + len;
		ZCG(mem) = (void*)gear_string_realloc(
			(gear_string*)ZCG(mem),
			((_ZSTR_HEADER_SIZE + 1 + new_len + 4095) & ~0xfff) - (_ZSTR_HEADER_SIZE + 1),
			0);
	}
	memcpy(ZSTR_VAL((gear_string*)ZCG(mem)) + info->str_size, str, len);
	info->str_size += len;
	return ret;
}

static void *gear_file_cache_unserialize_interned(gear_string *str, int in_shm)
{
	gear_string *ret;

	str = (gear_string*)((char*)ZCG(mem) + ((size_t)(str) & ~Z_UL(1)));
	if (in_shm) {
		ret = accel_new_interned_string(str);
		if (ret == str) {
			/* We have to create new SHM allocated string */
			size_t size = _ZSTR_STRUCT_SIZE(ZSTR_LEN(str));
			ret = gear_shared_alloc(size);
			if (!ret) {
				gear_accel_schedule_restart_if_necessary(ACCEL_RESTART_OOM);
				LONGJMP(*EG(bailout), FAILURE);
			}
			memcpy(ret, str, size);
			/* String wasn't interned but we will use it as interned anyway */
			GC_SET_REFCOUNT(ret, 1);
			GC_TYPE_INFO(ret) = IS_STRING | ((IS_STR_INTERNED | IS_STR_PERSISTENT | IS_STR_PERMANENT) << GC_FLAGS_SHIFT);
		}
	} else {
		ret = str;
		GC_ADD_FLAGS(ret, IS_STR_INTERNED);
		GC_DEL_FLAGS(ret, IS_STR_PERMANENT);
	}
	return ret;
}

static void gear_file_cache_serialize_hash(HashTable                *ht,
                                           gear_persistent_script   *script,
                                           gear_file_cache_metainfo *info,
                                           void                     *buf,
                                           serialize_callback_t      func)
{
	Bucket *p, *end;

	if (!(HT_FLAGS(ht) & HASH_FLAG_INITIALIZED)) {
		ht->arData = NULL;
		return;
	}
	if (IS_SERIALIZED(ht->arData)) {
		return;
	}
	SERIALIZE_PTR(ht->arData);
	p = ht->arData;
	UNSERIALIZE_PTR(p);
	end = p + ht->nNumUsed;
	while (p < end) {
		if (Z_TYPE(p->val) != IS_UNDEF) {
			SERIALIZE_STR(p->key);
			func(&p->val, script, info, buf);
		}
		p++;
	}
}

static void gear_file_cache_serialize_ast(gear_ast                 *ast,
                                          gear_persistent_script   *script,
                                          gear_file_cache_metainfo *info,
                                          void                     *buf)
{
	uint32_t i;
	gear_ast *tmp;

	if (ast->kind == GEAR_AST_ZVAL || ast->kind == GEAR_AST_CONSTANT) {
		gear_file_cache_serialize_zval(&((gear_ast_zval*)ast)->val, script, info, buf);
	} else if (gear_ast_is_list(ast)) {
		gear_ast_list *list = gear_ast_get_list(ast);
		for (i = 0; i < list->children; i++) {
			if (list->child[i] && !IS_SERIALIZED(list->child[i])) {
				SERIALIZE_PTR(list->child[i]);
				tmp = list->child[i];
				UNSERIALIZE_PTR(tmp);
				gear_file_cache_serialize_ast(tmp, script, info, buf);
			}
		}
	} else {
		uint32_t children = gear_ast_get_num_children(ast);
		for (i = 0; i < children; i++) {
			if (ast->child[i] && !IS_SERIALIZED(ast->child[i])) {
				SERIALIZE_PTR(ast->child[i]);
				tmp = ast->child[i];
				UNSERIALIZE_PTR(tmp);
				gear_file_cache_serialize_ast(tmp, script, info, buf);
			}
		}
	}
}

static void gear_file_cache_serialize_zval(zval                     *zv,
                                           gear_persistent_script   *script,
                                           gear_file_cache_metainfo *info,
                                           void                     *buf)
{
	switch (Z_TYPE_P(zv)) {
		case IS_STRING:
			if (!IS_SERIALIZED(Z_STR_P(zv))) {
				SERIALIZE_STR(Z_STR_P(zv));
			}
			break;
		case IS_ARRAY:
			if (!IS_SERIALIZED(Z_ARR_P(zv))) {
				HashTable *ht;

				SERIALIZE_PTR(Z_ARR_P(zv));
				ht = Z_ARR_P(zv);
				UNSERIALIZE_PTR(ht);
				gear_file_cache_serialize_hash(ht, script, info, buf, gear_file_cache_serialize_zval);
			}
			break;
		case IS_REFERENCE:
			if (!IS_SERIALIZED(Z_REF_P(zv))) {
				gear_reference *ref;

				SERIALIZE_PTR(Z_REF_P(zv));
				ref = Z_REF_P(zv);
				UNSERIALIZE_PTR(ref);
				gear_file_cache_serialize_zval(&ref->val, script, info, buf);
			}
			break;
		case IS_CONSTANT_AST:
			if (!IS_SERIALIZED(Z_AST_P(zv))) {
				gear_ast_ref *ast;

				SERIALIZE_PTR(Z_AST_P(zv));
				ast = Z_AST_P(zv);
				UNSERIALIZE_PTR(ast);
				gear_file_cache_serialize_ast(GC_AST(ast), script, info, buf);
			}
			break;
	}
}

static void gear_file_cache_serialize_op_array(gear_op_array            *op_array,
                                               gear_persistent_script   *script,
                                               gear_file_cache_metainfo *info,
                                               void                     *buf)
{
	if (op_array->static_variables && !IS_SERIALIZED(op_array->static_variables)) {
		HashTable *ht;

		SERIALIZE_PTR(op_array->static_variables);
		ht = op_array->static_variables;
		UNSERIALIZE_PTR(ht);
		gear_file_cache_serialize_hash(ht, script, info, buf, gear_file_cache_serialize_zval);
	}

	if (op_array->scope && !IS_SERIALIZED(op_array->opcodes)) {
		if (UNEXPECTED(gear_shared_alloc_get_xlat_entry(op_array->opcodes))) {
			op_array->refcount = (uint32_t*)(intptr_t)-1;
			SERIALIZE_PTR(op_array->literals);
			SERIALIZE_PTR(op_array->opcodes);
			SERIALIZE_PTR(op_array->arg_info);
			SERIALIZE_PTR(op_array->vars);
			SERIALIZE_STR(op_array->function_name);
			SERIALIZE_STR(op_array->filename);
			SERIALIZE_PTR(op_array->live_range);
			SERIALIZE_PTR(op_array->scope);
			SERIALIZE_STR(op_array->doc_comment);
			SERIALIZE_PTR(op_array->try_catch_array);
			SERIALIZE_PTR(op_array->prototype);
			return;
		}
		gear_shared_alloc_register_xlat_entry(op_array->opcodes, op_array->opcodes);
	}

	if (op_array->literals && !IS_SERIALIZED(op_array->literals)) {
		zval *p, *end;

		SERIALIZE_PTR(op_array->literals);
		p = op_array->literals;
		UNSERIALIZE_PTR(p);
		end = p + op_array->last_literal;
		while (p < end) {
			gear_file_cache_serialize_zval(p, script, info, buf);
			p++;
		}
	}

	if (!IS_SERIALIZED(op_array->opcodes)) {
		gear_op *opline, *end;

#if !GEAR_USE_ABS_CONST_ADDR
		zval *literals = op_array->literals;
		UNSERIALIZE_PTR(literals);
#endif

		SERIALIZE_PTR(op_array->opcodes);
		opline = op_array->opcodes;
		UNSERIALIZE_PTR(opline);
		end = opline + op_array->last;
		while (opline < end) {
#if GEAR_USE_ABS_CONST_ADDR
			if (opline->op1_type == IS_CONST) {
				SERIALIZE_PTR(opline->op1.zv);
			}
			if (opline->op2_type == IS_CONST) {
				SERIALIZE_PTR(opline->op2.zv);
			}
#else
			if (opline->op1_type == IS_CONST) {
				opline->op1.constant = RT_CONSTANT(opline, opline->op1) - literals;
			}
			if (opline->op2_type == IS_CONST) {
				opline->op2.constant = RT_CONSTANT(opline, opline->op2) - literals;
			}
#endif
#if GEAR_USE_ABS_JMP_ADDR
			switch (opline->opcode) {
				case GEAR_JMP:
				case GEAR_FAST_CALL:
					SERIALIZE_PTR(opline->op1.jmp_addr);
					break;
				case GEAR_JMPZNZ:
					/* relative extended_value don't have to be changed */
					/* break omitted intentionally */
				case GEAR_JMPZ:
				case GEAR_JMPNZ:
				case GEAR_JMPZ_EX:
				case GEAR_JMPNZ_EX:
				case GEAR_JMP_SET:
				case GEAR_COALESCE:
				case GEAR_FE_RESET_R:
				case GEAR_FE_RESET_RW:
				case GEAR_ASSERT_CHECK:
					SERIALIZE_PTR(opline->op2.jmp_addr);
					break;
				case GEAR_CATCH:
					if (!(opline->extended_value & GEAR_LAST_CATCH)) {
						SERIALIZE_PTR(opline->op2.jmp_addr);
					}
					break;
				case GEAR_DECLARE_ANON_CLASS:
				case GEAR_DECLARE_ANON_INHERITED_CLASS:
				case GEAR_FE_FETCH_R:
				case GEAR_FE_FETCH_RW:
				case GEAR_SWITCH_LONG:
				case GEAR_SWITCH_STRING:
					/* relative extended_value don't have to be changed */
					break;
			}
#endif
			gear_serialize_opcode_handler(opline);
			opline++;
		}

		if (op_array->arg_info) {
			gear_arg_info *p, *end;
			SERIALIZE_PTR(op_array->arg_info);
			p = op_array->arg_info;
			UNSERIALIZE_PTR(p);
			end = p + op_array->num_args;
			if (op_array->fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
				p--;
			}
			if (op_array->fn_flags & GEAR_ACC_VARIADIC) {
				end++;
			}
			while (p < end) {
				if (!IS_SERIALIZED(p->name)) {
					SERIALIZE_STR(p->name);
				}
				if (GEAR_TYPE_IS_CLASS(p->type)) {
					gear_bool allow_null = GEAR_TYPE_ALLOW_NULL(p->type);
					gear_string *type_name = GEAR_TYPE_NAME(p->type);

					SERIALIZE_STR(type_name);
					p->type =
						(Z_UL(1) << (sizeof(gear_type)*8-1)) | /* type is class */
						(allow_null ? (Z_UL(1) << (sizeof(gear_type)*8-2)) : Z_UL(0)) | /* type allow null */
						(gear_type)type_name;
				}
				p++;
			}
		}

		if (op_array->vars) {
			gear_string **p, **end;

			SERIALIZE_PTR(op_array->vars);
			p = op_array->vars;
			UNSERIALIZE_PTR(p);
			end = p + op_array->last_var;
			while (p < end) {
				if (!IS_SERIALIZED(*p)) {
					SERIALIZE_STR(*p);
				}
				p++;
			}
		}

		SERIALIZE_STR(op_array->function_name);
		SERIALIZE_STR(op_array->filename);
		SERIALIZE_PTR(op_array->live_range);
		SERIALIZE_PTR(op_array->scope);
		SERIALIZE_STR(op_array->doc_comment);
		SERIALIZE_PTR(op_array->try_catch_array);
		SERIALIZE_PTR(op_array->prototype);
	}
}

static void gear_file_cache_serialize_func(zval                     *zv,
                                           gear_persistent_script   *script,
                                           gear_file_cache_metainfo *info,
                                           void                     *buf)
{
	gear_op_array *op_array;

	SERIALIZE_PTR(Z_PTR_P(zv));
	op_array = Z_PTR_P(zv);
	UNSERIALIZE_PTR(op_array);
	gear_file_cache_serialize_op_array(op_array, script, info, buf);
}

static void gear_file_cache_serialize_prop_info(zval                     *zv,
                                                gear_persistent_script   *script,
                                                gear_file_cache_metainfo *info,
                                                void                     *buf)
{
	if (!IS_SERIALIZED(Z_PTR_P(zv))) {
		gear_property_info *prop;

		SERIALIZE_PTR(Z_PTR_P(zv));
		prop = Z_PTR_P(zv);
		UNSERIALIZE_PTR(prop);

		GEAR_ASSERT(prop->ce != NULL && prop->name != NULL);
		if (!IS_SERIALIZED(prop->ce)) {
			SERIALIZE_PTR(prop->ce);
			SERIALIZE_STR(prop->name);
			if (prop->doc_comment) {
				SERIALIZE_STR(prop->doc_comment);
			}
		}
	}
}

static void gear_file_cache_serialize_class_constant(zval                     *zv,
                                                     gear_persistent_script   *script,
                                                     gear_file_cache_metainfo *info,
                                                     void                     *buf)
{
	if (!IS_SERIALIZED(Z_PTR_P(zv))) {
		gear_class_constant *c;

		SERIALIZE_PTR(Z_PTR_P(zv));
		c = Z_PTR_P(zv);
		UNSERIALIZE_PTR(c);

		GEAR_ASSERT(c->ce != NULL);
		if (!IS_SERIALIZED(c->ce)) {
			SERIALIZE_PTR(c->ce);

			gear_file_cache_serialize_zval(&c->value, script, info, buf);

			if (c->doc_comment) {
				SERIALIZE_STR(c->doc_comment);
			}
		}
	}
}

static void gear_file_cache_serialize_class(zval                     *zv,
                                            gear_persistent_script   *script,
                                            gear_file_cache_metainfo *info,
                                            void                     *buf)
{
	gear_class_entry *ce;

	SERIALIZE_PTR(Z_PTR_P(zv));
	ce = Z_PTR_P(zv);
	UNSERIALIZE_PTR(ce);

	SERIALIZE_STR(ce->name);
	gear_file_cache_serialize_hash(&ce->function_table, script, info, buf, gear_file_cache_serialize_func);
	if (ce->default_properties_table) {
		zval *p, *end;

		SERIALIZE_PTR(ce->default_properties_table);
		p = ce->default_properties_table;
		UNSERIALIZE_PTR(p);
		end = p + ce->default_properties_count;
		while (p < end) {
			gear_file_cache_serialize_zval(p, script, info, buf);
			p++;
		}
	}
	if (ce->default_static_members_table) {
		zval *table, *p, *end;

		SERIALIZE_PTR(ce->default_static_members_table);
		table = ce->default_static_members_table;
		UNSERIALIZE_PTR(table);

		/* Serialize only static properties in this class.
		 * Static properties from parent classes will be handled in class_copy_ctor */
		p = table + (ce->parent ? ce->parent->default_static_members_count : 0);
		end = table + ce->default_static_members_count;
		while (p < end) {
			gear_file_cache_serialize_zval(p, script, info, buf);
			p++;
		}
	}
	gear_file_cache_serialize_hash(&ce->constants_table, script, info, buf, gear_file_cache_serialize_class_constant);
	SERIALIZE_STR(ce->info.user.filename);
	SERIALIZE_STR(ce->info.user.doc_comment);
	gear_file_cache_serialize_hash(&ce->properties_info, script, info, buf, gear_file_cache_serialize_prop_info);

	if (ce->trait_aliases) {
		gear_trait_alias **p, *q;

		SERIALIZE_PTR(ce->trait_aliases);
		p = ce->trait_aliases;
		UNSERIALIZE_PTR(p);

		while (*p) {
			SERIALIZE_PTR(*p);
			q = *p;
			UNSERIALIZE_PTR(q);

			if (q->trait_method.method_name) {
				SERIALIZE_STR(q->trait_method.method_name);
			}
			if (q->trait_method.class_name) {
				SERIALIZE_STR(q->trait_method.class_name);
			}

			if (q->alias) {
				SERIALIZE_STR(q->alias);
			}
			p++;
		}
	}

	if (ce->trait_precedences) {
		gear_trait_precedence **p, *q;
		int j;

		SERIALIZE_PTR(ce->trait_precedences);
		p = ce->trait_precedences;
		UNSERIALIZE_PTR(p);

		while (*p) {
			SERIALIZE_PTR(*p);
			q = *p;
			UNSERIALIZE_PTR(q);

			if (q->trait_method.method_name) {
				SERIALIZE_STR(q->trait_method.method_name);
			}
			if (q->trait_method.class_name) {
				SERIALIZE_STR(q->trait_method.class_name);
			}

			for (j = 0; j < q->num_excludes; j++) {
				SERIALIZE_STR(q->exclude_class_names[j]);
			}
			p++;
		}
	}

	SERIALIZE_PTR(ce->parent);
	SERIALIZE_PTR(ce->constructor);
	SERIALIZE_PTR(ce->destructor);
	SERIALIZE_PTR(ce->clone);
	SERIALIZE_PTR(ce->__get);
	SERIALIZE_PTR(ce->__set);
	SERIALIZE_PTR(ce->__call);
	SERIALIZE_PTR(ce->serialize_func);
	SERIALIZE_PTR(ce->unserialize_func);
	SERIALIZE_PTR(ce->__isset);
	SERIALIZE_PTR(ce->__unset);
	SERIALIZE_PTR(ce->__tostring);
	SERIALIZE_PTR(ce->__callstatic);
	SERIALIZE_PTR(ce->__debugInfo);
}

static void gear_file_cache_serialize(gear_persistent_script   *script,
                                      gear_file_cache_metainfo *info,
                                      void                     *buf)
{
	gear_persistent_script *new_script;

	memcpy(info->magic, "OPCACHE", 8);
	memcpy(info->system_id, ZCG(system_id), 32);
	info->mem_size = script->size;
	info->str_size = 0;
	info->script_offset = (char*)script - (char*)script->mem;
	info->timestamp = script->timestamp;

	memcpy(buf, script->mem, script->size);

	new_script = (gear_persistent_script*)((char*)buf + info->script_offset);
	SERIALIZE_STR(new_script->script.filename);

	gear_file_cache_serialize_hash(&new_script->script.class_table, script, info, buf, gear_file_cache_serialize_class);
	gear_file_cache_serialize_hash(&new_script->script.function_table, script, info, buf, gear_file_cache_serialize_func);
	gear_file_cache_serialize_op_array(&new_script->script.main_op_array, script, info, buf);

	SERIALIZE_PTR(new_script->arena_mem);
	new_script->mem = NULL;
}

static char *gear_file_cache_get_bin_file_path(gear_string *script_path)
{
	size_t len;
	char *filename;

#ifndef GEAR_WIN32
	len = strlen(ZCG(accel_directives).file_cache);
	filename = emalloc(len + 33 + ZSTR_LEN(script_path) + sizeof(SUFFIX));
	memcpy(filename, ZCG(accel_directives).file_cache, len);
	filename[len] = '/';
	memcpy(filename + len + 1, ZCG(system_id), 32);
	memcpy(filename + len + 33, ZSTR_VAL(script_path), ZSTR_LEN(script_path));
	memcpy(filename + len + 33 + ZSTR_LEN(script_path), SUFFIX, sizeof(SUFFIX));
#else
	HYSS_MD5_CTX ctx;
	char md5uname[32];
	unsigned char digest[16], c;
	size_t i;
	char *uname = hyss_win32_get_username();

	HYSS_MD5Init(&ctx);
	HYSS_MD5Update(&ctx, uname, strlen(uname));
	HYSS_MD5Final(digest, &ctx);
	for (i = 0; i < 16; i++) {
		c = digest[i] >> 4;
		c = (c <= 9) ? c + '0' : c - 10 + 'a';
		md5uname[i * 2] = c;
		c = digest[i] &  0x0f;
		c = (c <= 9) ? c + '0' : c - 10 + 'a';
		md5uname[(i * 2) + 1] = c;
	}

	len = strlen(ZCG(accel_directives).file_cache);

	filename = emalloc(len + 33 + 33 + ZSTR_LEN(script_path) + sizeof(SUFFIX));

	memcpy(filename, ZCG(accel_directives).file_cache, len);
	filename[len] = '\\';
	memcpy(filename + 1 + len, md5uname, 32);
	len += 32;
	filename[len] = '\\';

	memcpy(filename + len + 1, ZCG(system_id), 32);

	if (ZSTR_LEN(script_path) >= 7 && ':' == ZSTR_VAL(script_path)[4] && '/' == ZSTR_VAL(script_path)[5]  && '/' == ZSTR_VAL(script_path)[6]) {
		/* archy:// or file:// */
		*(filename + len + 33) = '\\';
		memcpy(filename + len + 34, ZSTR_VAL(script_path), 4);
		if (ZSTR_LEN(script_path) - 7 >= 2 && ':' == ZSTR_VAL(script_path)[8]) {
			*(filename + len + 38) = '\\';
			*(filename + len + 39) = ZSTR_VAL(script_path)[7];
			memcpy(filename + len + 40, ZSTR_VAL(script_path) + 9, ZSTR_LEN(script_path) - 9);
			memcpy(filename + len + 40 + ZSTR_LEN(script_path) - 9, SUFFIX, sizeof(SUFFIX));
		} else {
			memcpy(filename + len + 38, ZSTR_VAL(script_path) + 7, ZSTR_LEN(script_path) - 7);
			memcpy(filename + len + 38 + ZSTR_LEN(script_path) - 7, SUFFIX, sizeof(SUFFIX));
		}
	} else if (ZSTR_LEN(script_path) >= 2 && ':' == ZSTR_VAL(script_path)[1]) {
		/* local fs */
		*(filename + len + 33) = '\\';
		*(filename + len + 34) = ZSTR_VAL(script_path)[0];
		memcpy(filename + len + 35, ZSTR_VAL(script_path) + 2, ZSTR_LEN(script_path) - 2);
		memcpy(filename + len + 35 + ZSTR_LEN(script_path) - 2, SUFFIX, sizeof(SUFFIX));
	} else {
		/* network path */
		memcpy(filename + len + 33, ZSTR_VAL(script_path), ZSTR_LEN(script_path));
		memcpy(filename + len + 33 + ZSTR_LEN(script_path), SUFFIX, sizeof(SUFFIX));
	}
	free(uname);
#endif

	return filename;
}

int gear_file_cache_script_store(gear_persistent_script *script, int in_shm)
{
	int fd;
	char *filename;
	gear_file_cache_metainfo info;
#ifdef HAVE_SYS_UIO_H
	struct iovec vec[3];
#endif
	void *mem, *buf;

	filename = gear_file_cache_get_bin_file_path(script->script.filename);

	if (gear_file_cache_mkdir(filename, strlen(ZCG(accel_directives).file_cache)) != SUCCESS) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot create directory for file '%s', %s\n", filename, strerror(errno));
		efree(filename);
		return FAILURE;
	}

	fd = gear_file_cache_open(filename, O_CREAT | O_EXCL | O_RDWR | O_BINARY, S_IRUSR | S_IWUSR);
	if (fd < 0) {
		if (errno != EEXIST) {
			gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot create file '%s', %s\n", filename, strerror(errno));
		}
		efree(filename);
		return FAILURE;
	}

	if (gear_file_cache_flock(fd, LOCK_EX) != 0) {
		close(fd);
		efree(filename);
		return FAILURE;
	}

#if defined(__AVX__) || defined(__SSE2__)
	/* Align to 64-byte boundary */
	mem = emalloc(script->size + 64);
	buf = (void*)(((gear_uintptr_t)mem + 63L) & ~63L);
#else
	mem = buf = emalloc(script->size);
#endif

	ZCG(mem) = gear_string_alloc(4096 - (_ZSTR_HEADER_SIZE + 1), 0);

	gear_shared_alloc_init_xlat_table();
	if (!in_shm) {
		script->corrupted = 1; /* used to check if script restored to SHM or process memory */
	}
	gear_file_cache_serialize(script, &info, buf);
	if (!in_shm) {
		script->corrupted = 0;
	}
	gear_shared_alloc_destroy_xlat_table();

	info.checksum = gear_adler32(ADLER32_INIT, buf, script->size);
	info.checksum = gear_adler32(info.checksum, (signed char*)ZSTR_VAL((gear_string*)ZCG(mem)), info.str_size);

#ifdef HAVE_SYS_UIO_H
	vec[0].iov_base = &info;
	vec[0].iov_len = sizeof(info);
	vec[1].iov_base = buf;
	vec[1].iov_len = script->size;
	vec[2].iov_base = ZSTR_VAL((gear_string*)ZCG(mem));
	vec[2].iov_len = info.str_size;

	if (writev(fd, vec, 3) != (ssize_t)(sizeof(info) + script->size + info.str_size)) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot write to file '%s'\n", filename);
		gear_string_release_ex((gear_string*)ZCG(mem), 0);
		close(fd);
		efree(mem);
		gear_file_cache_unlink(filename);
		efree(filename);
		return FAILURE;
	}
#else
	if (GEAR_LONG_MAX < (gear_long)(sizeof(info) + script->size + info.str_size) ||
		write(fd, &info, sizeof(info)) != sizeof(info) ||
		write(fd, buf, script->size) != script->size ||
		write(fd, ((gear_string*)ZCG(mem))->val, info.str_size) != info.str_size
		) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot write to file '%s'\n", filename);
		gear_string_release_ex((gear_string*)ZCG(mem), 0);
		close(fd);
		efree(mem);
		gear_file_cache_unlink(filename);
		efree(filename);
		return FAILURE;
	}
#endif

	gear_string_release_ex((gear_string*)ZCG(mem), 0);
	efree(mem);
	if (gear_file_cache_flock(fd, LOCK_UN) != 0) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot unlock file '%s'\n", filename);
	}
	close(fd);
	efree(filename);

	return SUCCESS;
}

static void gear_file_cache_unserialize_hash(HashTable               *ht,
                                             gear_persistent_script  *script,
                                             void                    *buf,
                                             unserialize_callback_t   func,
                                             dtor_func_t              dtor)
{
	Bucket *p, *end;

	ht->pDestructor = dtor;
	if (!(HT_FLAGS(ht) & HASH_FLAG_INITIALIZED)) {
		if (EXPECTED(!file_cache_only)) {
			HT_SET_DATA_ADDR(ht, &ZCSG(uninitialized_bucket));
		} else {
			HT_SET_DATA_ADDR(ht, &uninitialized_bucket);
		}
		return;
	}
	if (IS_UNSERIALIZED(ht->arData)) {
		return;
	}
	UNSERIALIZE_PTR(ht->arData);
	p = ht->arData;
	end = p + ht->nNumUsed;
	while (p < end) {
		if (Z_TYPE(p->val) != IS_UNDEF) {
			UNSERIALIZE_STR(p->key);
			func(&p->val, script, buf);
		}
		p++;
	}
}

static void gear_file_cache_unserialize_ast(gear_ast                *ast,
                                            gear_persistent_script  *script,
                                            void                    *buf)
{
	uint32_t i;

	if (ast->kind == GEAR_AST_ZVAL || ast->kind == GEAR_AST_CONSTANT) {
		gear_file_cache_unserialize_zval(&((gear_ast_zval*)ast)->val, script, buf);
	} else if (gear_ast_is_list(ast)) {
		gear_ast_list *list = gear_ast_get_list(ast);
		for (i = 0; i < list->children; i++) {
			if (list->child[i] && !IS_UNSERIALIZED(list->child[i])) {
				UNSERIALIZE_PTR(list->child[i]);
				gear_file_cache_unserialize_ast(list->child[i], script, buf);
			}
		}
	} else {
		uint32_t children = gear_ast_get_num_children(ast);
		for (i = 0; i < children; i++) {
			if (ast->child[i] && !IS_UNSERIALIZED(ast->child[i])) {
				UNSERIALIZE_PTR(ast->child[i]);
				gear_file_cache_unserialize_ast(ast->child[i], script, buf);
			}
		}
	}
}

static void gear_file_cache_unserialize_zval(zval                    *zv,
                                             gear_persistent_script  *script,
                                             void                    *buf)
{
	switch (Z_TYPE_P(zv)) {
		case IS_STRING:
			if (!IS_UNSERIALIZED(Z_STR_P(zv))) {
				UNSERIALIZE_STR(Z_STR_P(zv));
			}
			break;
		case IS_ARRAY:
			if (!IS_UNSERIALIZED(Z_ARR_P(zv))) {
				HashTable *ht;

				UNSERIALIZE_PTR(Z_ARR_P(zv));
				ht = Z_ARR_P(zv);
				gear_file_cache_unserialize_hash(ht,
						script, buf, gear_file_cache_unserialize_zval, ZVAL_PTR_DTOR);
			}
			break;
		case IS_REFERENCE:
			if (!IS_UNSERIALIZED(Z_REF_P(zv))) {
				gear_reference *ref;

				UNSERIALIZE_PTR(Z_REF_P(zv));
				ref = Z_REF_P(zv);
				gear_file_cache_unserialize_zval(&ref->val, script, buf);
			}
			break;
		case IS_CONSTANT_AST:
			if (!IS_UNSERIALIZED(Z_AST_P(zv))) {
				UNSERIALIZE_PTR(Z_AST_P(zv));
				gear_file_cache_unserialize_ast(Z_ASTVAL_P(zv), script, buf);
			}
			break;
	}
}

static void gear_file_cache_unserialize_op_array(gear_op_array           *op_array,
                                                 gear_persistent_script  *script,
                                                 void                    *buf)
{
	if (op_array->static_variables && !IS_UNSERIALIZED(op_array->static_variables)) {
		HashTable *ht;

		UNSERIALIZE_PTR(op_array->static_variables);
		ht = op_array->static_variables;
		gear_file_cache_unserialize_hash(ht,
				script, buf, gear_file_cache_unserialize_zval, ZVAL_PTR_DTOR);
	}

	if (op_array->refcount) {
		op_array->refcount = NULL;
		UNSERIALIZE_PTR(op_array->literals);
		UNSERIALIZE_PTR(op_array->opcodes);
		UNSERIALIZE_PTR(op_array->arg_info);
		UNSERIALIZE_PTR(op_array->vars);
		UNSERIALIZE_STR(op_array->function_name);
		UNSERIALIZE_STR(op_array->filename);
		UNSERIALIZE_PTR(op_array->live_range);
		UNSERIALIZE_PTR(op_array->scope);
		UNSERIALIZE_STR(op_array->doc_comment);
		UNSERIALIZE_PTR(op_array->try_catch_array);
		UNSERIALIZE_PTR(op_array->prototype);
		return;
	}

	if (op_array->literals && !IS_UNSERIALIZED(op_array->literals)) {
		zval *p, *end;

		UNSERIALIZE_PTR(op_array->literals);
		p = op_array->literals;
		end = p + op_array->last_literal;
		while (p < end) {
			gear_file_cache_unserialize_zval(p, script, buf);
			p++;
		}
	}

	if (!IS_UNSERIALIZED(op_array->opcodes)) {
		gear_op *opline, *end;

		UNSERIALIZE_PTR(op_array->opcodes);
		opline = op_array->opcodes;
		end = opline + op_array->last;
		while (opline < end) {
#if GEAR_USE_ABS_CONST_ADDR
			if (opline->op1_type == IS_CONST) {
				UNSERIALIZE_PTR(opline->op1.zv);
			}
			if (opline->op2_type == IS_CONST) {
				UNSERIALIZE_PTR(opline->op2.zv);
			}
#else
			if (opline->op1_type == IS_CONST) {
				GEAR_PASS_TWO_UPDATE_CONSTANT(op_array, opline, opline->op1);
			}
			if (opline->op2_type == IS_CONST) {
				GEAR_PASS_TWO_UPDATE_CONSTANT(op_array, opline, opline->op2);
			}
#endif
#if GEAR_USE_ABS_JMP_ADDR
			switch (opline->opcode) {
				case GEAR_JMP:
				case GEAR_FAST_CALL:
					UNSERIALIZE_PTR(opline->op1.jmp_addr);
					break;
				case GEAR_JMPZNZ:
					/* relative extended_value don't have to be changed */
					/* break omitted intentionally */
				case GEAR_JMPZ:
				case GEAR_JMPNZ:
				case GEAR_JMPZ_EX:
				case GEAR_JMPNZ_EX:
				case GEAR_JMP_SET:
				case GEAR_COALESCE:
				case GEAR_FE_RESET_R:
				case GEAR_FE_RESET_RW:
				case GEAR_ASSERT_CHECK:
					UNSERIALIZE_PTR(opline->op2.jmp_addr);
					break;
				case GEAR_CATCH:
					if (!(opline->extended_value & GEAR_LAST_CATCH)) {
						UNSERIALIZE_PTR(opline->op2.jmp_addr);
					}
					break;
				case GEAR_DECLARE_ANON_CLASS:
				case GEAR_DECLARE_ANON_INHERITED_CLASS:
				case GEAR_FE_FETCH_R:
				case GEAR_FE_FETCH_RW:
				case GEAR_SWITCH_LONG:
				case GEAR_SWITCH_STRING:
					/* relative extended_value don't have to be changed */
					break;
			}
#endif
			gear_deserialize_opcode_handler(opline);
			opline++;
		}

		if (op_array->arg_info) {
			gear_arg_info *p, *end;
			UNSERIALIZE_PTR(op_array->arg_info);
			p = op_array->arg_info;
			end = p + op_array->num_args;
			if (op_array->fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
				p--;
			}
			if (op_array->fn_flags & GEAR_ACC_VARIADIC) {
				end++;
			}
			while (p < end) {
				if (!IS_UNSERIALIZED(p->name)) {
					UNSERIALIZE_STR(p->name);
				}
				if (p->type & (Z_UL(1) << (sizeof(gear_type)*8-1))) { /* type is class */
					gear_bool allow_null = (p->type & (Z_UL(1) << (sizeof(gear_type)*8-2))) != 0; /* type allow null */
					gear_string *type_name = (gear_string*)(p->type & ~(((Z_UL(1) << (sizeof(gear_type)*8-1))) | ((Z_UL(1) << (sizeof(gear_type)*8-2)))));

					UNSERIALIZE_STR(type_name);
					p->type = GEAR_TYPE_ENCODE_CLASS(type_name, allow_null);
				}
				p++;
			}
		}

		if (op_array->vars) {
			gear_string **p, **end;

			UNSERIALIZE_PTR(op_array->vars);
			p = op_array->vars;
			end = p + op_array->last_var;
			while (p < end) {
				if (!IS_UNSERIALIZED(*p)) {
					UNSERIALIZE_STR(*p);
				}
				p++;
			}
		}

		UNSERIALIZE_STR(op_array->function_name);
		UNSERIALIZE_STR(op_array->filename);
		UNSERIALIZE_PTR(op_array->live_range);
		UNSERIALIZE_PTR(op_array->scope);
		UNSERIALIZE_STR(op_array->doc_comment);
		UNSERIALIZE_PTR(op_array->try_catch_array);
		UNSERIALIZE_PTR(op_array->prototype);
	}
}

static void gear_file_cache_unserialize_func(zval                    *zv,
                                             gear_persistent_script  *script,
                                             void                    *buf)
{
	gear_op_array *op_array;

	UNSERIALIZE_PTR(Z_PTR_P(zv));
	op_array = Z_PTR_P(zv);
	gear_file_cache_unserialize_op_array(op_array, script, buf);
}

static void gear_file_cache_unserialize_prop_info(zval                    *zv,
                                                  gear_persistent_script  *script,
                                                  void                    *buf)
{
	if (!IS_UNSERIALIZED(Z_PTR_P(zv))) {
		gear_property_info *prop;

		UNSERIALIZE_PTR(Z_PTR_P(zv));
		prop = Z_PTR_P(zv);

		GEAR_ASSERT(prop->ce != NULL && prop->name != NULL);
		if (!IS_UNSERIALIZED(prop->ce)) {
			UNSERIALIZE_PTR(prop->ce);
			UNSERIALIZE_STR(prop->name);
			if (prop->doc_comment) {
				UNSERIALIZE_STR(prop->doc_comment);
			}
		}
	}
}

static void gear_file_cache_unserialize_class_constant(zval                    *zv,
                                                       gear_persistent_script  *script,
                                                       void                    *buf)
{
	if (!IS_UNSERIALIZED(Z_PTR_P(zv))) {
		gear_class_constant *c;

		UNSERIALIZE_PTR(Z_PTR_P(zv));
		c = Z_PTR_P(zv);

		GEAR_ASSERT(c->ce != NULL);
		if (!IS_UNSERIALIZED(c->ce)) {
			UNSERIALIZE_PTR(c->ce);

			gear_file_cache_unserialize_zval(&c->value, script, buf);

			if (c->doc_comment) {
				UNSERIALIZE_STR(c->doc_comment);
			}
		}
	}
}

static void gear_file_cache_unserialize_class(zval                    *zv,
                                              gear_persistent_script  *script,
                                              void                    *buf)
{
	gear_class_entry *ce;

	UNSERIALIZE_PTR(Z_PTR_P(zv));
	ce = Z_PTR_P(zv);

	UNSERIALIZE_STR(ce->name);
	UNSERIALIZE_PTR(ce->parent);
	gear_file_cache_unserialize_hash(&ce->function_table,
			script, buf, gear_file_cache_unserialize_func, GEAR_FUNCTION_DTOR);
	if (ce->default_properties_table) {
		zval *p, *end;

		UNSERIALIZE_PTR(ce->default_properties_table);
		p = ce->default_properties_table;
		end = p + ce->default_properties_count;
		while (p < end) {
			gear_file_cache_unserialize_zval(p, script, buf);
			p++;
		}
	}
	if (ce->default_static_members_table) {
		zval *table, *p, *end;

		/* Unserialize only static properties in this class.
		 * Static properties from parent classes will be handled in class_copy_ctor */
		UNSERIALIZE_PTR(ce->default_static_members_table);
		table = ce->default_static_members_table;
		p = table + (ce->parent ? ce->parent->default_static_members_count : 0);
		end = table + ce->default_static_members_count;
		while (p < end) {
			gear_file_cache_unserialize_zval(p, script, buf);
			p++;
		}
	}
	gear_file_cache_unserialize_hash(&ce->constants_table,
			script, buf, gear_file_cache_unserialize_class_constant, NULL);
	UNSERIALIZE_STR(ce->info.user.filename);
	UNSERIALIZE_STR(ce->info.user.doc_comment);
	gear_file_cache_unserialize_hash(&ce->properties_info,
			script, buf, gear_file_cache_unserialize_prop_info, NULL);

	if (ce->trait_aliases) {
		gear_trait_alias **p, *q;

		UNSERIALIZE_PTR(ce->trait_aliases);
		p = ce->trait_aliases;

		while (*p) {
			UNSERIALIZE_PTR(*p);
			q = *p;

			if (q->trait_method.method_name) {
				UNSERIALIZE_STR(q->trait_method.method_name);
			}
			if (q->trait_method.class_name) {
				UNSERIALIZE_STR(q->trait_method.class_name);
			}

			if (q->alias) {
				UNSERIALIZE_STR(q->alias);
			}
			p++;
		}
	}

	if (ce->trait_precedences) {
		gear_trait_precedence **p, *q;
		int j;

		UNSERIALIZE_PTR(ce->trait_precedences);
		p = ce->trait_precedences;

		while (*p) {
			UNSERIALIZE_PTR(*p);
			q = *p;

			if (q->trait_method.method_name) {
				UNSERIALIZE_STR(q->trait_method.method_name);
			}
			if (q->trait_method.class_name) {
				UNSERIALIZE_STR(q->trait_method.class_name);
			}

			for (j = 0; j < q->num_excludes; j++) {
				UNSERIALIZE_STR(q->exclude_class_names[j]);
			}
			p++;
		}
	}

	UNSERIALIZE_PTR(ce->constructor);
	UNSERIALIZE_PTR(ce->destructor);
	UNSERIALIZE_PTR(ce->clone);
	UNSERIALIZE_PTR(ce->__get);
	UNSERIALIZE_PTR(ce->__set);
	UNSERIALIZE_PTR(ce->__call);
	UNSERIALIZE_PTR(ce->serialize_func);
	UNSERIALIZE_PTR(ce->unserialize_func);
	UNSERIALIZE_PTR(ce->__isset);
	UNSERIALIZE_PTR(ce->__unset);
	UNSERIALIZE_PTR(ce->__tostring);
	UNSERIALIZE_PTR(ce->__callstatic);
	UNSERIALIZE_PTR(ce->__debugInfo);

	if (UNEXPECTED((ce->ce_flags & GEAR_ACC_ANON_CLASS))) {
		ce->serialize = gear_class_serialize_deny;
		ce->unserialize = gear_class_unserialize_deny;
	}
}

static void gear_file_cache_unserialize(gear_persistent_script  *script,
                                        void                    *buf)
{
	script->mem = buf;

	UNSERIALIZE_STR(script->script.filename);

	gear_file_cache_unserialize_hash(&script->script.class_table,
			script, buf, gear_file_cache_unserialize_class, GEAR_CLASS_DTOR);
	gear_file_cache_unserialize_hash(&script->script.function_table,
			script, buf, gear_file_cache_unserialize_func, GEAR_FUNCTION_DTOR);
	gear_file_cache_unserialize_op_array(&script->script.main_op_array, script, buf);

	UNSERIALIZE_PTR(script->arena_mem);
}

gear_persistent_script *gear_file_cache_script_load(gear_file_handle *file_handle)
{
	gear_string *full_path = file_handle->opened_path;
	int fd;
	char *filename;
	gear_persistent_script *script;
	gear_file_cache_metainfo info;
	gear_accel_hash_entry *bucket;
	void *mem, *checkpoint, *buf;
	int cache_it = 1;
	int ok;

	if (!full_path) {
		return NULL;
	}
	filename = gear_file_cache_get_bin_file_path(full_path);

	fd = gear_file_cache_open(filename, O_RDONLY | O_BINARY);
	if (fd < 0) {
		efree(filename);
		return NULL;
	}

	if (gear_file_cache_flock(fd, LOCK_SH) != 0) {
		close(fd);
		efree(filename);
		return NULL;
	}

	if (read(fd, &info, sizeof(info)) != sizeof(info)) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot read from file '%s' (info)\n", filename);
		gear_file_cache_flock(fd, LOCK_UN);
		close(fd);
		gear_file_cache_unlink(filename);
		efree(filename);
		return NULL;
	}

	/* verify header */
	if (memcmp(info.magic, "OPCACHE", 8) != 0) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot read from file '%s' (wrong header)\n", filename);
		gear_file_cache_flock(fd, LOCK_UN);
		close(fd);
		gear_file_cache_unlink(filename);
		efree(filename);
		return NULL;
	}
	if (memcmp(info.system_id, ZCG(system_id), 32) != 0) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot read from file '%s' (wrong \"system_id\")\n", filename);
		gear_file_cache_flock(fd, LOCK_UN);
		close(fd);
		gear_file_cache_unlink(filename);
		efree(filename);
		return NULL;
	}

	/* verify timestamp */
	if (ZCG(accel_directives).validate_timestamps &&
	    gear_get_file_handle_timestamp(file_handle, NULL) != info.timestamp) {
		if (gear_file_cache_flock(fd, LOCK_UN) != 0) {
			gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot unlock file '%s'\n", filename);
		}
		close(fd);
		gear_file_cache_unlink(filename);
		efree(filename);
		return NULL;
	}

	checkpoint = gear_arena_checkpoint(CG(arena));
#if defined(__AVX__) || defined(__SSE2__)
	/* Align to 64-byte boundary */
	mem = gear_arena_alloc(&CG(arena), info.mem_size + info.str_size + 64);
	mem = (void*)(((gear_uintptr_t)mem + 63L) & ~63L);
#else
	mem = gear_arena_alloc(&CG(arena), info.mem_size + info.str_size);
#endif

	if (read(fd, mem, info.mem_size + info.str_size) != (ssize_t)(info.mem_size + info.str_size)) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot read from file '%s' (mem)\n", filename);
		gear_file_cache_flock(fd, LOCK_UN);
		close(fd);
		gear_file_cache_unlink(filename);
		gear_arena_release(&CG(arena), checkpoint);
		efree(filename);
		return NULL;
	}
	if (gear_file_cache_flock(fd, LOCK_UN) != 0) {
		gear_accel_error(ACCEL_LOG_WARNING, "opcache cannot unlock file '%s'\n", filename);
	}
	close(fd);

	/* verify checksum */
	if (ZCG(accel_directives).file_cache_consistency_checks &&
	    gear_adler32(ADLER32_INIT, mem, info.mem_size + info.str_size) != info.checksum) {
		gear_accel_error(ACCEL_LOG_WARNING, "corrupted file '%s'\n", filename);
		gear_file_cache_unlink(filename);
		gear_arena_release(&CG(arena), checkpoint);
		efree(filename);
		return NULL;
	}

	if (!file_cache_only &&
	    !ZCSG(restart_in_progress) &&
		!ZSMMG(memory_exhausted) &&
	    accelerator_shm_read_lock() == SUCCESS) {
		/* exclusive lock */
		gear_shared_alloc_lock();

		/* Check if we still need to put the file into the cache (may be it was
		 * already stored by another process. This final check is done under
		 * exclusive lock) */
		bucket = gear_accel_hash_find_entry(&ZCSG(hash), full_path);
		if (bucket) {
			script = (gear_persistent_script *)bucket->data;
			if (!script->corrupted) {
				gear_shared_alloc_unlock();
				gear_arena_release(&CG(arena), checkpoint);
				efree(filename);
				return script;
			}
		}

		if (gear_accel_hash_is_full(&ZCSG(hash))) {
			gear_accel_error(ACCEL_LOG_DEBUG, "No more entries in hash table!");
			ZSMMG(memory_exhausted) = 1;
			gear_accel_schedule_restart_if_necessary(ACCEL_RESTART_HASH);
			gear_shared_alloc_unlock();
			goto use_process_mem;
		}

#if defined(__AVX__) || defined(__SSE2__)
		/* Align to 64-byte boundary */
		buf = gear_shared_alloc(info.mem_size + 64);
		buf = (void*)(((gear_uintptr_t)buf + 63L) & ~63L);
#else
		buf = gear_shared_alloc(info.mem_size);
#endif

		if (!buf) {
			gear_accel_schedule_restart_if_necessary(ACCEL_RESTART_OOM);
			gear_shared_alloc_unlock();
			goto use_process_mem;
		}
		memcpy(buf, mem, info.mem_size);
	} else {
use_process_mem:
		buf = mem;
		cache_it = 0;
	}

	ZCG(mem) = ((char*)mem + info.mem_size);
	script = (gear_persistent_script*)((char*)buf + info.script_offset);
	script->corrupted = !cache_it; /* used to check if script restored to SHM or process memory */

	ok = 1;
	gear_try {
		gear_file_cache_unserialize(script, buf);
	} gear_catch {
		ok = 0;
	} gear_end_try();
	if (!ok) {
		if (cache_it) {
			gear_shared_alloc_unlock();
			goto use_process_mem;
		} else {
			gear_arena_release(&CG(arena), checkpoint);
			efree(filename);
			return NULL;
		}
	}

	script->corrupted = 0;

	if (cache_it) {
		script->dynamic_members.checksum = gear_accel_script_checksum(script);
		script->dynamic_members.last_used = ZCG(request_time);

		gear_accel_hash_update(&ZCSG(hash), ZSTR_VAL(script->script.filename), ZSTR_LEN(script->script.filename), 0, script);

		gear_shared_alloc_unlock();
		gear_arena_release(&CG(arena), checkpoint);
	}
	efree(filename);

	return script;
}

void gear_file_cache_invalidate(gear_string *full_path)
{
	char *filename;

	filename = gear_file_cache_get_bin_file_path(full_path);

	gear_file_cache_unlink(filename);
	efree(filename);
}

#endif /* HAVE_OPCACHE_FILE_CACHE */
