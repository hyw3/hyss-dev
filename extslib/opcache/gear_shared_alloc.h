/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_SHARED_ALLOC_H
#define GEAR_SHARED_ALLOC_H

#include "gear.h"
#include "GearAccelerator.h"

#if defined(__APPLE__) && defined(__MACH__) /* darwin */
# ifdef HAVE_SHM_MMAP_POSIX
#  define USE_SHM_OPEN  1
# endif
# ifdef HAVE_SHM_MMAP_ANON
#  define USE_MMAP      1
# endif
#elif defined(__linux__) || defined(_AIX)
# ifdef HAVE_SHM_IPC
#  define USE_SHM       1
# endif
# ifdef HAVE_SHM_MMAP_ANON
#  define USE_MMAP      1
# endif
#elif defined(__sparc) || defined(__sun)
# ifdef HAVE_SHM_MMAP_POSIX
#  define USE_SHM_OPEN  1
# endif
# ifdef HAVE_SHM_IPC
#  define USE_SHM       1
# endif
# if defined(__i386)
#  ifdef HAVE_SHM_MMAP_ANON
#   define USE_MMAP     1
#  endif
# endif
#else
# ifdef HAVE_SHM_MMAP_POSIX
#  define USE_SHM_OPEN  1
# endif
# ifdef HAVE_SHM_MMAP_ANON
#  define USE_MMAP      1
# endif
# ifdef HAVE_SHM_IPC
#  define USE_SHM       1
# endif
#endif

#define ALLOC_FAILURE           0
#define ALLOC_SUCCESS           1
#define FAILED_REATTACHED       2
#define SUCCESSFULLY_REATTACHED 4
#define ALLOC_FAIL_MAPPING      8
#define ALLOC_FALLBACK          9

typedef struct _gear_shared_segment {
    size_t  size;
    size_t  pos;  /* position for simple stack allocator */
    void   *p;
} gear_shared_segment;

typedef int (*create_segments_t)(size_t requested_size, gear_shared_segment ***shared_segments, int *shared_segment_count, char **error_in);
typedef int (*detach_segment_t)(gear_shared_segment *shared_segment);

typedef struct {
	create_segments_t create_segments;
	detach_segment_t detach_segment;
	size_t (*segment_type_size)(void);
} gear_shared_memory_handlers;

typedef struct _handler_entry {
	const char                  *name;
	gear_shared_memory_handlers *handler;
} gear_shared_memory_handler_entry;

typedef struct _gear_shared_memory_state {
	int *positions;   /* current positions for each segment */
	size_t shared_free; /* amount of free shared memory */
} gear_shared_memory_state;

typedef struct _gear_smm_shared_globals {
    /* Shared Memory Manager */
    gear_shared_segment      **shared_segments;
    /* Number of allocated shared segments */
    int                        shared_segments_count;
    /* Amount of free shared memory */
    size_t                     shared_free;
    /* Amount of shared memory allocated by garbage */
    size_t                     wasted_shared_memory;
    /* No more shared memory flag */
    gear_bool                  memory_exhausted;
    /* Saved Shared Allocator State */
    gear_shared_memory_state   shared_memory_state;
	/* Pointer to the application's shared data structures */
	void                      *app_shared_globals;
} gear_smm_shared_globals;

extern gear_smm_shared_globals *smm_shared_globals;

#define ZSMMG(element)		(smm_shared_globals->element)

#define SHARED_ALLOC_REATTACHED		(SUCCESS+1)

int gear_shared_alloc_startup(size_t requested_size);
void gear_shared_alloc_shutdown(void);

/* allocate shared memory block */
void *gear_shared_alloc(size_t size);

/* copy into shared memory */
void *_gear_shared_memdup(void *p, size_t size, gear_bool free_source);
int  gear_shared_memdup_size(void *p, size_t size);

int gear_accel_in_shm(void *ptr);

typedef union _align_test {
	void   *ptr;
	double  dbl;
	gear_long  lng;
} align_test;

#if GEAR_GCC_VERSION >= 2000
# define PLATFORM_ALIGNMENT (__alignof__(align_test) < 8 ? 8 : __alignof__(align_test))
#else
# define PLATFORM_ALIGNMENT (sizeof(align_test))
#endif

#define GEAR_ALIGNED_SIZE(size) \
	GEAR_MM_ALIGNED_SIZE_EX(size, PLATFORM_ALIGNMENT)

/* exclusive locking */
void gear_shared_alloc_lock(void);
void gear_shared_alloc_unlock(void); /* returns the allocated size during lock..unlock */
void gear_shared_alloc_safe_unlock(void);

/* old/new mapping functions */
void gear_shared_alloc_init_xlat_table(void);
void gear_shared_alloc_destroy_xlat_table(void);
void gear_shared_alloc_clear_xlat_table(void);
void gear_shared_alloc_register_xlat_entry(const void *old, const void *new);
void *gear_shared_alloc_get_xlat_entry(const void *old);

size_t gear_shared_alloc_get_free_memory(void);
void gear_shared_alloc_save_state(void);
void gear_shared_alloc_restore_state(void);
const char *gear_accel_get_shared_model(void);

/* memory write protection */
void gear_accel_shared_protect(int mode);

#ifdef USE_MMAP
extern gear_shared_memory_handlers gear_alloc_mmap_handlers;
#endif

#ifdef USE_SHM
extern gear_shared_memory_handlers gear_alloc_shm_handlers;
#endif

#ifdef USE_SHM_OPEN
extern gear_shared_memory_handlers gear_alloc_posix_handlers;
#endif

#ifdef GEAR_WIN32
extern gear_shared_memory_handlers gear_alloc_win32_handlers;
void gear_shared_alloc_create_lock(void);
void gear_shared_alloc_lock_win32(void);
void gear_shared_alloc_unlock_win32(void);
#endif

#endif /* GEAR_SHARED_ALLOC_H */
