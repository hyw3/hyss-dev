/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GearAccelerator.h"
#include "gear_accelerator_hash.h"
#include "gear_hash.h"
#include "gear_shared_alloc.h"

/* Generated on an Octa-ALPHA 300MHz CPU & 2.5GB RAM monster */
static uint32_t prime_numbers[] =
	{5, 11, 19, 53, 107, 223, 463, 983, 1979, 3907, 7963, 16229, 32531, 65407, 130987, 262237, 524521, 1048793 };
static uint32_t num_prime_numbers = sizeof(prime_numbers) / sizeof(uint32_t);

void gear_accel_hash_clean(gear_accel_hash *accel_hash)
{
	accel_hash->num_entries = 0;
	accel_hash->num_direct_entries = 0;
	memset(accel_hash->hash_table, 0, sizeof(gear_accel_hash_entry *)*accel_hash->max_num_entries);
}

void gear_accel_hash_init(gear_accel_hash *accel_hash, uint32_t hash_size)
{
	uint32_t i;

	for (i=0; i<num_prime_numbers; i++) {
		if (hash_size <= prime_numbers[i]) {
			hash_size = prime_numbers[i];
			break;
		}
	}

	accel_hash->num_entries = 0;
	accel_hash->num_direct_entries = 0;
	accel_hash->max_num_entries = hash_size;

	/* set up hash pointers table */
	accel_hash->hash_table = gear_shared_alloc(sizeof(gear_accel_hash_entry *)*accel_hash->max_num_entries);
	if (!accel_hash->hash_table) {
		gear_accel_error(ACCEL_LOG_FATAL, "Insufficient shared memory!");
		return;
	}

	/* set up hash values table */
	accel_hash->hash_entries = gear_shared_alloc(sizeof(gear_accel_hash_entry)*accel_hash->max_num_entries);
	if (!accel_hash->hash_entries) {
		gear_accel_error(ACCEL_LOG_FATAL, "Insufficient shared memory!");
		return;
	}
	memset(accel_hash->hash_table, 0, sizeof(gear_accel_hash_entry *)*accel_hash->max_num_entries);
}

/* Returns NULL if hash is full
 * Returns pointer the actual hash entry on success
 * key needs to be already allocated as it is not copied
 */
gear_accel_hash_entry* gear_accel_hash_update(gear_accel_hash *accel_hash, const char *key, uint32_t key_length, gear_bool indirect, void *data)
{
	gear_ulong hash_value;
	gear_ulong index;
	gear_accel_hash_entry *entry;
	gear_accel_hash_entry *indirect_bucket = NULL;

	if (indirect) {
		indirect_bucket = (gear_accel_hash_entry*)data;
		while (indirect_bucket->indirect) {
			indirect_bucket = (gear_accel_hash_entry*)indirect_bucket->data;
		}
	}

	hash_value = gear_inline_hash_func(key, key_length);
#ifndef GEAR_WIN32
	hash_value ^= ZCG(root_hash);
#endif
	index = hash_value % accel_hash->max_num_entries;

	/* try to see if the element already exists in the hash */
	entry = accel_hash->hash_table[index];
	while (entry) {
		if (entry->hash_value == hash_value
			&& entry->key_length == key_length
			&& !memcmp(entry->key, key, key_length)) {

			if (entry->indirect) {
				if (indirect_bucket) {
					entry->data = indirect_bucket;
				} else {
					((gear_accel_hash_entry*)entry->data)->data = data;
				}
			} else {
				if (indirect_bucket) {
					accel_hash->num_direct_entries--;
					entry->data = indirect_bucket;
					entry->indirect = 1;
				} else {
					entry->data = data;
				}
			}
			return entry;
		}
		entry = entry->next;
	}

	/* Does not exist, add a new entry */
	if (accel_hash->num_entries == accel_hash->max_num_entries) {
		return NULL;
	}

	entry = &accel_hash->hash_entries[accel_hash->num_entries++];
	if (indirect) {
		entry->data = indirect_bucket;
		entry->indirect = 1;
	} else {
		accel_hash->num_direct_entries++;
		entry->data = data;
		entry->indirect = 0;
	}
	entry->hash_value = hash_value;
	entry->key = key;
	entry->key_length = key_length;
	entry->next = accel_hash->hash_table[index];
	accel_hash->hash_table[index] = entry;
	return entry;
}

static gear_always_inline void* gear_accel_hash_find_ex(gear_accel_hash *accel_hash, const char *key, uint32_t key_length, gear_ulong hash_value, int data)
{
	gear_ulong index;
	gear_accel_hash_entry *entry;

#ifndef GEAR_WIN32
	hash_value ^= ZCG(root_hash);
#endif
	index = hash_value % accel_hash->max_num_entries;

	entry = accel_hash->hash_table[index];
	while (entry) {
		if (entry->hash_value == hash_value
			&& entry->key_length == key_length
			&& !memcmp(entry->key, key, key_length)) {
			if (entry->indirect) {
				if (data) {
					return ((gear_accel_hash_entry*)entry->data)->data;
				} else {
					return entry->data;
				}
			} else {
				if (data) {
					return entry->data;
				} else {
					return entry;
				}
			}
		}
		entry = entry->next;
	}
	return NULL;
}

/* Returns the data associated with key on success
 * Returns NULL if data doesn't exist
 */
void* gear_accel_hash_find(gear_accel_hash *accel_hash, gear_string *key)
{
	return gear_accel_hash_find_ex(
		accel_hash,
		ZSTR_VAL(key),
		ZSTR_LEN(key),
		gear_string_hash_val(key),
		1);
}

/* Returns the hash entry associated with key on success
 * Returns NULL if it doesn't exist
 */
gear_accel_hash_entry* gear_accel_hash_find_entry(gear_accel_hash *accel_hash, gear_string *key)
{
	return (gear_accel_hash_entry *)gear_accel_hash_find_ex(
		accel_hash,
		ZSTR_VAL(key),
		ZSTR_LEN(key),
		gear_string_hash_val(key),
		0);
}

/* Returns the data associated with key on success
 * Returns NULL if data doesn't exist
 */
void* gear_accel_hash_str_find(gear_accel_hash *accel_hash, const char *key, uint32_t key_length)
{
	return gear_accel_hash_find_ex(
		accel_hash,
		key,
		key_length,
		gear_inline_hash_func(key, key_length),
		1);
}

/* Returns the hash entry associated with key on success
 * Returns NULL if it doesn't exist
 */
gear_accel_hash_entry* gear_accel_hash_str_find_entry(gear_accel_hash *accel_hash, const char *key, uint32_t key_length)
{
	return (gear_accel_hash_entry *)gear_accel_hash_find_ex(
		accel_hash,
		key,
		key_length,
		gear_inline_hash_func(key, key_length),
		0);
}

int gear_accel_hash_unlink(gear_accel_hash *accel_hash, const char *key, uint32_t key_length)
{
    gear_ulong hash_value;
    gear_ulong index;
    gear_accel_hash_entry *entry, *last_entry=NULL;

	hash_value = gear_inline_hash_func(key, key_length);
#ifndef GEAR_WIN32
	hash_value ^= ZCG(root_hash);
#endif
	index = hash_value % accel_hash->max_num_entries;

	entry = accel_hash->hash_table[index];
	while (entry) {
		if (entry->hash_value == hash_value
			&& entry->key_length == key_length
			&& !memcmp(entry->key, key, key_length)) {
			if (!entry->indirect) {
				accel_hash->num_direct_entries--;
			}
			if (last_entry) {
				last_entry->next = entry->next;
			} else {
				accel_hash->hash_table[index] = entry->next;
			}
			return SUCCESS;
		}
		last_entry = entry;
		entry = entry->next;
	}
	return FAILURE;
}
