/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#ifdef GEAR_WIN32
# include <process.h>
#endif
#include "GearAccelerator.h"

void gear_accel_error(int type, const char *format, ...)
{
	va_list args;
	time_t timestamp;
	char *time_string;
	FILE * fLog = NULL;

	if (type <= ZCG(accel_directives).log_verbosity_level) {

	timestamp = time(NULL);
	time_string = asctime(localtime(&timestamp));
	time_string[24] = 0;

	if (!ZCG(accel_directives).error_log ||
		!*ZCG(accel_directives).error_log ||
		strcmp(ZCG(accel_directives).error_log, "stderr") == 0) {

		fLog = stderr;
	} else {
		fLog = fopen(ZCG(accel_directives).error_log, "a+");
		if (!fLog) {
			fLog = stderr;
		}
	}

#ifdef ZTS
		fprintf(fLog, "%s (" GEAR_ULONG_FMT "): ", time_string, (gear_ulong)pbc_thread_id());
#else
		fprintf(fLog, "%s (%d): ", time_string, getpid());
#endif

		switch (type) {
			case ACCEL_LOG_FATAL:
				fprintf(fLog, "Fatal Error ");
				break;
			case ACCEL_LOG_ERROR:
				fprintf(fLog, "Error ");
				break;
			case ACCEL_LOG_WARNING:
				fprintf(fLog, "Warning ");
				break;
			case ACCEL_LOG_INFO:
				fprintf(fLog, "Message ");
				break;
			case ACCEL_LOG_DEBUG:
				fprintf(fLog, "Debug ");
				break;
		}

		va_start(args, format);
		vfprintf(fLog, format, args);
		va_end(args);
		fprintf(fLog, "\n");

		fflush(fLog);
		if (fLog != stderr) {
			fclose(fLog);
		}
	}
	/* perform error handling even without logging the error */
	switch (type) {
		case ACCEL_LOG_ERROR:
			gear_bailout();
			break;
		case ACCEL_LOG_FATAL:
			exit(-2);
			break;
	}

}
