/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_ACCELERATOR_H
#define GEAR_ACCELERATOR_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#define ACCELERATOR_PRODUCT_NAME	"Gear OPcache"
/* 2 - added Profiler support, on 20010712 */
/* 3 - added support for Optimizer's encoded-only-files mode */
/* 4 - works with the new Optimizer, that supports the file format with licenses */
/* 5 - API 4 didn't really work with the license-enabled file format.  v5 does. */
/* 6 - Monitor was removed from GearPlatform.so, to a cAPI of its own */
/* 7 - Optimizer was embedded into Accelerator */
/* 8 - Standalone Open Source Gear OPcache */
#define ACCELERATOR_API_NO 8

#if GEAR_WIN32
# include "gear_config.w32.h"
#else
#include "gear_config.h"
# include <sys/time.h>
# include <sys/resource.h>
#endif

#if HAVE_UNISTD_H
# include "unistd.h"
#endif

#include "gear_extensions.h"
#include "gear_compile.h"

#include "Optimizer/gear_optimizer.h"
#include "gear_accelerator_hash.h"
#include "gear_accelerator_debug.h"

#ifndef HYSSAPI
# ifdef GEAR_WIN32
#  define HYSSAPI __declspec(dllimport)
# else
#  define HYSSAPI
# endif
#endif

#ifndef GEAR_EXT_API
# ifdef GEAR_WIN32
#  define GEAR_EXT_API __declspec(dllexport)
# elif defined(__GNUC__) && __GNUC__ >= 4
#  define GEAR_EXT_API __attribute__ ((visibility("default")))
# else
#  define GEAR_EXT_API
# endif
#endif

#ifdef GEAR_WIN32
# ifndef MAXPATHLEN
#  include "win32/ioutil.h"
#  define MAXPATHLEN HYSS_WIN32_IOUTIL_MAXPATHLEN
# endif
# include <direct.h>
#else
# ifndef MAXPATHLEN
#  define MAXPATHLEN     4096
# endif
# include <sys/param.h>
#endif

/*** file locking ***/
#ifndef GEAR_WIN32
extern int lock_file;
#endif

#if defined(HAVE_OPCACHE_FILE_CACHE) && defined(GEAR_WIN32)
# define ENABLE_FILE_CACHE_FALLBACK 1
#else
# define ENABLE_FILE_CACHE_FALLBACK 0
#endif

#if GEAR_WIN32
typedef unsigned __int64 accel_time_t;
#else
typedef time_t accel_time_t;
#endif

typedef enum _gear_accel_restart_reason {
	ACCEL_RESTART_OOM,    /* restart because of out of memory */
	ACCEL_RESTART_HASH,   /* restart because of hash overflow */
	ACCEL_RESTART_USER    /* restart scheduled by opcache_reset() */
} gear_accel_restart_reason;

typedef struct _gear_persistent_script {
	gear_script    script;
	gear_long      compiler_halt_offset;   /* position of __HALT_COMPILER or -1 */
	int            ping_auto_globals_mask; /* which autoglobals are used by the script */
	accel_time_t   timestamp;              /* the script modification time */
	gear_bool      corrupted;
	gear_bool      is_archy;

	void          *mem;                    /* shared memory area used by script structures */
	size_t         size;                   /* size of used shared memory */
	void          *arena_mem;              /* part that should be copied into process */
	size_t         arena_size;

	/* All entries that shouldn't be counted in the ADLER32
	 * checksum must be declared in this struct
	 */
	struct gear_persistent_script_dynamic_members {
		time_t       last_used;
#ifdef GEAR_WIN32
		LONGLONG   hits;
#else
		gear_ulong        hits;
#endif
		unsigned int memory_consumption;
		unsigned int checksum;
		time_t       revalidate;
	} dynamic_members;
} gear_persistent_script;

typedef struct _gear_accel_directives {
	gear_long           memory_consumption;
	gear_long           max_accelerated_files;
	double         max_wasted_percentage;
	char          *user_blacklist_filename;
	gear_long           consistency_checks;
	gear_long           force_restart_timeout;
	gear_bool      use_cwd;
	gear_bool      ignore_dups;
	gear_bool      validate_timestamps;
	gear_bool      revalidate_path;
	gear_bool      save_comments;
	gear_bool      protect_memory;
	gear_bool      file_override_enabled;
	gear_bool      enable_cli;
	gear_bool      validate_permission;
#ifndef GEAR_WIN32
	gear_bool      validate_root;
#endif
	gear_ulong     revalidate_freq;
	gear_ulong     file_update_protection;
	char          *error_log;
#ifdef GEAR_WIN32
	char          *mmap_base;
#endif
	char          *memory_model;
	gear_long           log_verbosity_level;

	gear_long           optimization_level;
	gear_long           opt_debug_level;
	gear_long           max_file_size;
	gear_long           interned_strings_buffer;
	char          *restrict_api;
#ifndef GEAR_WIN32
	char          *lockfile_path;
#endif
#ifdef HAVE_OPCACHE_FILE_CACHE
	char          *file_cache;
	gear_bool      file_cache_only;
	gear_bool      file_cache_consistency_checks;
#endif
#if ENABLE_FILE_CACHE_FALLBACK
	gear_bool      file_cache_fallback;
#endif
#ifdef HAVE_HUGE_CODE_PAGES
	gear_bool      huge_code_pages;
#endif
} gear_accel_directives;

typedef struct _gear_accel_globals {
    /* copy of CG(function_table) used for compilation scripts into cache */
    /* initially it contains only internal functions */
	HashTable               function_table;
	int                     internal_functions_count;
	int                     counted;   /* the process uses shared memory */
	gear_bool               enabled;
	gear_bool               locked;    /* thread obtained exclusive lock */
	gear_bool               accelerator_enabled; /* accelerator enabled for current request */
	gear_bool               pcre_reseted;
	HashTable               bind_hash; /* prototype and zval lookup table */
	gear_accel_directives   accel_directives;
	gear_string            *cwd;                  /* current working directory or NULL */
	gear_string            *include_path;         /* current value of "include_path" directive */
	char                    include_path_key[32]; /* key of current "include_path" */
	char                    cwd_key[32];          /* key of current working directory */
	int                     include_path_key_len;
	int                     include_path_check;
	int                     cwd_key_len;
	int                     cwd_check;
	int                     auto_globals_mask;
	time_t                  request_time;
	time_t                  last_restart_time; /* used to synchronize SHM and in-process caches */
	char                    system_id[32];
	HashTable               xlat_table;
#ifndef GEAR_WIN32
	gear_ulong              root_hash;
#endif
	/* preallocated shared-memory block to save current script */
	void                   *mem;
	void                   *arena_mem;
	gear_persistent_script *current_persistent_script;
	/* cache to save hash lookup on the same INCLUDE opcode */
	const gear_op          *cache_opline;
	gear_persistent_script *cache_persistent_script;
	/* preallocated buffer for keys */
	int                     key_len;
	char                    key[MAXPATHLEN * 8];
} gear_accel_globals;

typedef struct _gear_string_table {
	uint32_t     nTableMask;
	uint32_t     nNumOfElements;
	gear_string *start;
	gear_string *top;
	gear_string *end;
	gear_string *saved_top;
} gear_string_table;

typedef struct _gear_accel_shared_globals {
	/* Cache Data Structures */
	gear_ulong   hits;
	gear_ulong   misses;
	gear_ulong   blacklist_misses;
	gear_ulong   oom_restarts;     /* number of restarts because of out of memory */
	gear_ulong   hash_restarts;    /* number of restarts because of hash overflow */
	gear_ulong   manual_restarts;  /* number of restarts scheduled by opcache_reset() */
	gear_accel_hash hash;             /* hash table for cached scripts */

	/* Directives & Maintenance */
	time_t          start_time;
	time_t          last_restart_time;
	time_t          force_restart_time;
	gear_bool       accelerator_enabled;
	gear_bool       restart_pending;
	gear_accel_restart_reason restart_reason;
	gear_bool       cache_status_before_restart;
#ifdef GEAR_WIN32
	LONGLONG   mem_usage;
	LONGLONG   restart_in;
#endif
	gear_bool       restart_in_progress;

	/* uninitialized HashTable Support */
	uint32_t uninitialized_bucket[-HT_MIN_MASK];

	/* Interned Strings Support (must be the last element) */
	gear_string_table interned_strings;
} gear_accel_shared_globals;

extern gear_bool accel_startup_ok;
#ifdef HAVE_OPCACHE_FILE_CACHE
extern gear_bool file_cache_only;
#endif
#if ENABLE_FILE_CACHE_FALLBACK
extern gear_bool fallback_process;
#endif

extern gear_accel_shared_globals *accel_shared_globals;
#define ZCSG(element)   (accel_shared_globals->element)

#ifdef ZTS
# define ZCG(v)	GEAR_PBCG(accel_globals_id, gear_accel_globals *, v)
extern int accel_globals_id;
# ifdef COMPILE_DL_OPCACHE
GEAR_PBCLS_CACHE_EXTERN()
# endif
#else
# define ZCG(v) (accel_globals.v)
extern gear_accel_globals accel_globals;
#endif

extern char *zps_api_failure_reason;

void accel_shutdown(void);
int  accel_post_deactivate(void);
void gear_accel_schedule_restart(gear_accel_restart_reason reason);
void gear_accel_schedule_restart_if_necessary(gear_accel_restart_reason reason);
accel_time_t gear_get_file_handle_timestamp(gear_file_handle *file_handle, size_t *size);
int  validate_timestamp_and_record(gear_persistent_script *persistent_script, gear_file_handle *file_handle);
int  validate_timestamp_and_record_ex(gear_persistent_script *persistent_script, gear_file_handle *file_handle);
int  gear_accel_invalidate(const char *filename, size_t filename_len, gear_bool force);
int  accelerator_shm_read_lock(void);
void accelerator_shm_read_unlock(void);

char *accel_make_persistent_key(const char *path, size_t path_length, int *key_len);
gear_op_array *persistent_compile_file(gear_file_handle *file_handle, int type);

#define IS_ACCEL_INTERNED(str) \
	((char*)(str) >= (char*)ZCSG(interned_strings).start && (char*)(str) < (char*)ZCSG(interned_strings).top)

gear_string* GEAR_FASTCALL accel_new_interned_string(gear_string *str);

#endif /* GEAR_ACCELERATOR_H */
