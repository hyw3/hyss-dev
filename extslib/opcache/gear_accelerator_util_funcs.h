/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_ACCELERATOR_UTIL_FUNCS_H
#define GEAR_ACCELERATOR_UTIL_FUNCS_H

#include "gear.h"
#include "GearAccelerator.h"

void gear_accel_copy_internal_functions(void);

gear_persistent_script* create_persistent_script(void);
void free_persistent_script(gear_persistent_script *persistent_script, int destroy_elements);

void gear_accel_free_user_functions(HashTable *ht);
void gear_accel_move_user_functions(HashTable *str, HashTable *dst);

gear_op_array* gear_accel_load_script(gear_persistent_script *persistent_script, int from_shared_memory);

#define ADLER32_INIT 1     /* initial Adler-32 value */

unsigned int gear_adler32(unsigned int checksum, signed char *buf, uint32_t len);

unsigned int gear_accel_script_checksum(gear_persistent_script *persistent_script);

#endif /* GEAR_ACCELERATOR_UTIL_FUNCS_H */

