/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEAR_FILE_CACHE_H
#define GEAR_FILE_CACHE_H

int gear_file_cache_script_store(gear_persistent_script *script, int in_shm);
gear_persistent_script *gear_file_cache_script_load(gear_file_handle *file_handle);
void gear_file_cache_invalidate(gear_string *full_path);

#endif /* GEAR_FILE_CACHE_H */
