/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_OPENSSL_H
#define HYSS_OPENSSL_H
/* HAVE_OPENSSL would include SSL MySQL stuff */
#ifdef HAVE_OPENSSL_EXT
extern gear_capi_entry openssl_capi_entry;
#define hyssext_openssl_ptr &openssl_capi_entry

#include "hyss_version.h"
#define HYSS_OPENSSL_VERSION HYSS_VERSION

#include <openssl/opensslv.h>
#if defined(LIBRESSL_VERSION_NUMBER)
/* LibreSSL version check */
#if LIBRESSL_VERSION_NUMBER < 0x20700000L
#define HYSS_OPENSSL_API_VERSION 0x10001
#else
#define HYSS_OPENSSL_API_VERSION 0x10100
#endif
#else
/* OpenSSL version check */
#if OPENSSL_VERSION_NUMBER < 0x10002000L
#define HYSS_OPENSSL_API_VERSION 0x10001
#elif OPENSSL_VERSION_NUMBER < 0x10100000L
#define HYSS_OPENSSL_API_VERSION 0x10002
#else
#define HYSS_OPENSSL_API_VERSION 0x10100
#endif
#endif

#define OPENSSL_RAW_DATA 1
#define OPENSSL_ZERO_PADDING 2
#define OPENSSL_DONT_ZERO_PAD_KEY 4

#define OPENSSL_ERROR_X509_PRIVATE_KEY_VALUES_MISMATCH 0x0B080074

/* Used for client-initiated handshake renegotiation DoS protection*/
#define OPENSSL_DEFAULT_RENEG_LIMIT 2
#define OPENSSL_DEFAULT_RENEG_WINDOW 300
#define OPENSSL_DEFAULT_STREAM_VERIFY_DEPTH 9
#define OPENSSL_DEFAULT_STREAM_CIPHERS "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:" \
	"ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:" \
	"DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:" \
	"ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:" \
	"ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:" \
	"DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:" \
	"AES256-GCM-SHA384:AES128:AES256:HIGH:!SSLv2:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!RC4:!ADH"

#include <openssl/err.h>

struct hyss_openssl_errors {
	int buffer[ERR_NUM_ERRORS];
	int top;
	int bottom;
};

GEAR_BEGIN_CAPI_GLOBALS(openssl)
	struct hyss_openssl_errors *errors;
GEAR_END_CAPI_GLOBALS(openssl)

#define OPENSSL_G(v) GEAR_CAPI_GLOBALS_ACCESSOR(openssl, v)

#if defined(ZTS) && defined(COMPILE_DL_OPENSSL)
GEAR_PBCLS_CACHE_EXTERN();
#endif

hyss_stream_transport_factory_func hyss_openssl_ssl_socket_factory;

void hyss_openssl_store_errors();

HYSS_MINIT_FUNCTION(openssl);
HYSS_MSHUTDOWN_FUNCTION(openssl);
HYSS_MINFO_FUNCTION(openssl);
HYSS_GINIT_FUNCTION(openssl);
HYSS_GSHUTDOWN_FUNCTION(openssl);

HYSS_FUNCTION(openssl_pkey_get_private);
HYSS_FUNCTION(openssl_pkey_get_public);
HYSS_FUNCTION(openssl_pkey_free);
HYSS_FUNCTION(openssl_pkey_new);
HYSS_FUNCTION(openssl_pkey_export);
HYSS_FUNCTION(openssl_pkey_export_to_file);
HYSS_FUNCTION(openssl_pkey_get_details);

HYSS_FUNCTION(openssl_sign);
HYSS_FUNCTION(openssl_verify);
HYSS_FUNCTION(openssl_seal);
HYSS_FUNCTION(openssl_open);
HYSS_FUNCTION(openssl_private_encrypt);
HYSS_FUNCTION(openssl_private_decrypt);
HYSS_FUNCTION(openssl_public_encrypt);
HYSS_FUNCTION(openssl_public_decrypt);

HYSS_FUNCTION(openssl_pbkdf2);

HYSS_FUNCTION(openssl_pkcs7_verify);
HYSS_FUNCTION(openssl_pkcs7_decrypt);
HYSS_FUNCTION(openssl_pkcs7_sign);
HYSS_FUNCTION(openssl_pkcs7_encrypt);
HYSS_FUNCTION(openssl_pkcs7_read);

HYSS_FUNCTION(openssl_error_string);

HYSS_FUNCTION(openssl_x509_read);
HYSS_FUNCTION(openssl_x509_free);
HYSS_FUNCTION(openssl_x509_parse);
HYSS_FUNCTION(openssl_x509_checkpurpose);
HYSS_FUNCTION(openssl_x509_export);
HYSS_FUNCTION(openssl_x509_fingerprint);
HYSS_FUNCTION(openssl_x509_export_to_file);
HYSS_FUNCTION(openssl_x509_check_private_key);

HYSS_FUNCTION(openssl_pkcs12_export);
HYSS_FUNCTION(openssl_pkcs12_export_to_file);
HYSS_FUNCTION(openssl_pkcs12_read);

HYSS_FUNCTION(openssl_csr_new);
HYSS_FUNCTION(openssl_csr_export);
HYSS_FUNCTION(openssl_csr_export_to_file);
HYSS_FUNCTION(openssl_csr_sign);
HYSS_FUNCTION(openssl_csr_get_subject);
HYSS_FUNCTION(openssl_csr_get_public_key);

HYSS_FUNCTION(openssl_spki_new);
HYSS_FUNCTION(openssl_spki_verify);
HYSS_FUNCTION(openssl_spki_export);
HYSS_FUNCTION(openssl_spki_export_challenge);

HYSS_FUNCTION(openssl_get_cert_locations);

#ifdef HYSS_WIN32
#define HYSS_OPENSSL_BIO_MODE_R(flags) (((flags) & PKCS7_BINARY) ? "rb" : "r")
#define HYSS_OPENSSL_BIO_MODE_W(flags) (((flags) & PKCS7_BINARY) ? "wb" : "w")
#else
#define HYSS_OPENSSL_BIO_MODE_R(flags) "r"
#define HYSS_OPENSSL_BIO_MODE_W(flags) "w"
#endif

#else

#define hyssext_openssl_ptr NULL

#endif


#endif

