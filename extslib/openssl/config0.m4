dnl config.m4 for extension openssl

HYSS_ARG_WITH(openssl, for OpenSSL support,
[  --with-openssl[=DIR]      Include OpenSSL support (requires OpenSSL >= 1.0.1)])

HYSS_ARG_WITH(kerberos, for Kerberos support,
[  --with-kerberos[=DIR]     OPENSSL: Include Kerberos support], no, no)

HYSS_ARG_WITH(system-ciphers, whether to use system default cipher list instead of hardcoded value,
[  --with-system-ciphers   OPENSSL: Use system default cipher list instead of hardcoded value], no, no)

if test "$HYSS_OPENSSL" != "no"; then
  HYSS_NEW_EXTENSION(openssl, openssl.c xp_ssl.c, $ext_shared)
  HYSS_SUBST(OPENSSL_SHARED_LIBADD)

  if test "$HYSS_KERBEROS" != "no"; then
    HYSS_SETUP_KERBEROS(OPENSSL_SHARED_LIBADD)
  fi

  AC_CHECK_FUNCS([RAND_egd])

  HYSS_SETUP_OPENSSL(OPENSSL_SHARED_LIBADD,
  [
    AC_DEFINE(HAVE_OPENSSL_EXT,1,[ ])
  ], [
    AC_MSG_ERROR([OpenSSL check failed. Please check config.log for more information.])
  ])
  if test "$HYSS_SYSTEM_CIPHERS" != "no"; then
    AC_DEFINE(USE_OPENSSL_SYSTEM_CIPHERS,1,[ Use system default cipher list instead of hardcoded value ])
  fi
fi
