/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_REFLECTION_H
#define HYSS_REFLECTION_H

#include "hyss.h"

extern gear_capi_entry reflection_capi_entry;
#define hyssext_reflection_ptr &reflection_capi_entry

#define HYSS_REFLECTION_VERSION HYSS_VERSION

BEGIN_EXTERN_C()

/* Class entry pointers */
extern HYSSAPI gear_class_entry *reflector_ptr;
extern HYSSAPI gear_class_entry *reflection_exception_ptr;
extern HYSSAPI gear_class_entry *reflection_ptr;
extern HYSSAPI gear_class_entry *reflection_function_abstract_ptr;
extern HYSSAPI gear_class_entry *reflection_function_ptr;
extern HYSSAPI gear_class_entry *reflection_parameter_ptr;
extern HYSSAPI gear_class_entry *reflection_type_ptr;
extern HYSSAPI gear_class_entry *reflection_named_type_ptr;
extern HYSSAPI gear_class_entry *reflection_class_ptr;
extern HYSSAPI gear_class_entry *reflection_object_ptr;
extern HYSSAPI gear_class_entry *reflection_method_ptr;
extern HYSSAPI gear_class_entry *reflection_property_ptr;
extern HYSSAPI gear_class_entry *reflection_extension_ptr;
extern HYSSAPI gear_class_entry *reflection_gear_extension_ptr;

HYSSAPI void gear_reflection_class_factory(gear_class_entry *ce, zval *object);

END_EXTERN_C()

#endif /* HYSS_REFLECTION_H */

