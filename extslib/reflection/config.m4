dnl config.m4 for extension reflection

AC_DEFINE(HAVE_REFLECTION, 1, [Whether Reflection is enabled])
HYSS_NEW_EXTENSION(reflection, hyss_reflection.c, no,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
