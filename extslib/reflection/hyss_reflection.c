/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "hyss_reflection.h"
#include "extslib/standard/info.h"

#include "gear.h"
#include "gear_API.h"
#include "gear_exceptions.h"
#include "gear_operators.h"
#include "gear_constants.h"
#include "gear_ics.h"
#include "gear_interfaces.h"
#include "gear_closures.h"
#include "gear_generators.h"
#include "gear_extensions.h"
#include "gear_builtin_functions.h"
#include "gear_smart_str.h"

#define reflection_update_property(object, name, value) do { \
		zval member; \
		ZVAL_STR(&member, name); \
		gear_std_write_property(object, &member, value, NULL); \
		Z_TRY_DELREF_P(value); \
		zval_ptr_dtor(&member); \
	} while (0)

#define reflection_update_property_name(object, value) \
	reflection_update_property(object, ZSTR_KNOWN(GEAR_STR_NAME), value)

#define reflection_update_property_class(object, value) \
	reflection_update_property(object, ZSTR_KNOWN(GEAR_STR_CLASS), value)

/* Class entry pointers */
HYSSAPI gear_class_entry *reflector_ptr;
HYSSAPI gear_class_entry *reflection_exception_ptr;
HYSSAPI gear_class_entry *reflection_ptr;
HYSSAPI gear_class_entry *reflection_function_abstract_ptr;
HYSSAPI gear_class_entry *reflection_function_ptr;
HYSSAPI gear_class_entry *reflection_generator_ptr;
HYSSAPI gear_class_entry *reflection_parameter_ptr;
HYSSAPI gear_class_entry *reflection_type_ptr;
HYSSAPI gear_class_entry *reflection_named_type_ptr;
HYSSAPI gear_class_entry *reflection_class_ptr;
HYSSAPI gear_class_entry *reflection_object_ptr;
HYSSAPI gear_class_entry *reflection_method_ptr;
HYSSAPI gear_class_entry *reflection_property_ptr;
HYSSAPI gear_class_entry *reflection_class_constant_ptr;
HYSSAPI gear_class_entry *reflection_extension_ptr;
HYSSAPI gear_class_entry *reflection_gear_extension_ptr;

/* Exception throwing macro */
#define _DO_THROW(msg)                                                                                      \
	gear_throw_exception(reflection_exception_ptr, msg, 0);                                       \
	return;                                                                                                 \

#define RETURN_ON_EXCEPTION                                                                                 \
	if (EG(exception) && EG(exception)->ce == reflection_exception_ptr) {                            \
		return;                                                                                             \
	}

#define GET_REFLECTION_OBJECT()	                                                                   			\
	intern = Z_REFLECTION_P(getThis());                                                      				\
	if (intern->ptr == NULL) {                                                            \
		RETURN_ON_EXCEPTION                                                                                 \
		gear_throw_error(NULL, "Internal error: Failed to retrieve the reflection object");        \
		return;                                                                                             \
	}                                                                                                       \

#define GET_REFLECTION_OBJECT_PTR(target)                                                                   \
	GET_REFLECTION_OBJECT()																					\
	target = intern->ptr;                                                                                   \

/* Class constants */
#define REGISTER_REFLECTION_CLASS_CONST_LONG(class_name, const_name, value)                                        \
	gear_declare_class_constant_long(reflection_ ## class_name ## _ptr, const_name, sizeof(const_name)-1, (gear_long)value);

/* {{{ Object structure */

/* Struct for properties */
typedef struct _property_reference {
	gear_class_entry *ce;
	gear_property_info prop;
	gear_string *unmangled_name;
} property_reference;

/* Struct for parameters */
typedef struct _parameter_reference {
	uint32_t offset;
	gear_bool required;
	struct _gear_arg_info *arg_info;
	gear_function *fptr;
} parameter_reference;

/* Struct for type hints */
typedef struct _type_reference {
	struct _gear_arg_info *arg_info;
	gear_function *fptr;
} type_reference;

typedef enum {
	REF_TYPE_OTHER,      /* Must be 0 */
	REF_TYPE_FUNCTION,
	REF_TYPE_GENERATOR,
	REF_TYPE_PARAMETER,
	REF_TYPE_TYPE,
	REF_TYPE_PROPERTY,
	REF_TYPE_CLASS_CONSTANT
} reflection_type_t;

/* Struct for reflection objects */
typedef struct {
	zval dummy; /* holder for the second property */
	zval obj;
	void *ptr;
	gear_class_entry *ce;
	reflection_type_t ref_type;
	unsigned int ignore_visibility:1;
	gear_object zo;
} reflection_object;

static inline reflection_object *reflection_object_from_obj(gear_object *obj) {
	return (reflection_object*)((char*)(obj) - XtOffsetOf(reflection_object, zo));
}

#define Z_REFLECTION_P(zv)  reflection_object_from_obj(Z_OBJ_P((zv)))
/* }}} */

static gear_object_handlers reflection_object_handlers;

static zval *_default_load_name(zval *object) /* {{{ */
{
	return gear_hash_find_ex_ind(Z_OBJPROP_P(object), ZSTR_KNOWN(GEAR_STR_NAME), 1);
}
/* }}} */

static void _default_get_name(zval *object, zval *return_value) /* {{{ */
{
	zval *value;

	if ((value = _default_load_name(object)) == NULL) {
		RETURN_FALSE;
	}
	ZVAL_COPY(return_value, value);
}
/* }}} */

static gear_function *_copy_function(gear_function *fptr) /* {{{ */
{
	if (fptr
		&& (fptr->internal_function.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE))
	{
		gear_function *copy_fptr;
		copy_fptr = emalloc(sizeof(gear_function));
		memcpy(copy_fptr, fptr, sizeof(gear_function));
		copy_fptr->internal_function.function_name = gear_string_copy(fptr->internal_function.function_name);
		return copy_fptr;
	} else {
		/* no copy needed */
		return fptr;
	}
}
/* }}} */

static void _free_function(gear_function *fptr) /* {{{ */
{
	if (fptr
		&& (fptr->internal_function.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE))
	{
		gear_string_release_ex(fptr->internal_function.function_name, 0);
		gear_free_trampoline(fptr);
	}
}
/* }}} */

static void reflection_free_objects_storage(gear_object *object) /* {{{ */
{
	reflection_object *intern = reflection_object_from_obj(object);
	parameter_reference *reference;
	property_reference *prop_reference;
	type_reference *typ_reference;

	if (intern->ptr) {
		switch (intern->ref_type) {
		case REF_TYPE_PARAMETER:
			reference = (parameter_reference*)intern->ptr;
			_free_function(reference->fptr);
			efree(intern->ptr);
			break;
		case REF_TYPE_TYPE:
			typ_reference = (type_reference*)intern->ptr;
			_free_function(typ_reference->fptr);
			efree(intern->ptr);
			break;
		case REF_TYPE_FUNCTION:
			_free_function(intern->ptr);
			break;
		case REF_TYPE_PROPERTY:
			prop_reference = (property_reference*)intern->ptr;
			gear_string_release_ex(prop_reference->unmangled_name, 0);
			efree(intern->ptr);
			break;
		case REF_TYPE_GENERATOR:
		case REF_TYPE_CLASS_CONSTANT:
		case REF_TYPE_OTHER:
			break;
		}
	}
	intern->ptr = NULL;
	zval_ptr_dtor(&intern->obj);
	gear_object_std_dtor(object);
}
/* }}} */

static HashTable *reflection_get_gc(zval *obj, zval **gc_data, int *gc_data_count) /* {{{ */
{
	reflection_object *intern = Z_REFLECTION_P(obj);
	*gc_data = &intern->obj;
	*gc_data_count = 1;
	return gear_std_get_properties(obj);
}
/* }}} */

static gear_object *reflection_objects_new(gear_class_entry *class_type) /* {{{ */
{
	reflection_object *intern = gear_object_alloc(sizeof(reflection_object), class_type);

	gear_object_std_init(&intern->zo, class_type);
	object_properties_init(&intern->zo, class_type);
	intern->zo.handlers = &reflection_object_handlers;
	return &intern->zo;
}
/* }}} */

static zval *reflection_instantiate(gear_class_entry *pce, zval *object) /* {{{ */
{
	object_init_ex(object, pce);
	return object;
}
/* }}} */

static void _const_string(smart_str *str, char *name, zval *value, char *indent);
static void _function_string(smart_str *str, gear_function *fptr, gear_class_entry *scope, char* indent);
static void _property_string(smart_str *str, gear_property_info *prop, const char *prop_name, char* indent);
static void _class_const_string(smart_str *str, char *name, gear_class_constant *c, char* indent);
static void _class_string(smart_str *str, gear_class_entry *ce, zval *obj, char *indent);
static void _extension_string(smart_str *str, gear_capi_entry *cAPI, char *indent);
static void _gear_extension_string(smart_str *str, gear_extension *extension, char *indent);

/* {{{ _class_string */
static void _class_string(smart_str *str, gear_class_entry *ce, zval *obj, char *indent)
{
	int count, count_static_props = 0, count_static_funcs = 0, count_shadow_props = 0;
	gear_string *sub_indent = strpprintf(0, "%s    ", indent);

	/* TBD: Repair indenting of doc comment (or is this to be done in the parser?) */
	if (ce->type == GEAR_USER_CLASS && ce->info.user.doc_comment) {
		smart_str_append_printf(str, "%s%s", indent, ZSTR_VAL(ce->info.user.doc_comment));
		smart_str_appendc(str, '\n');
	}

	if (obj && Z_TYPE_P(obj) == IS_OBJECT) {
		smart_str_append_printf(str, "%sObject of class [ ", indent);
	} else {
		char *kind = "Class";
		if (ce->ce_flags & GEAR_ACC_INTERFACE) {
			kind = "Interface";
		} else if (ce->ce_flags & GEAR_ACC_TRAIT) {
			kind = "Trait";
		}
		smart_str_append_printf(str, "%s%s [ ", indent, kind);
	}
	smart_str_append_printf(str, (ce->type == GEAR_USER_CLASS) ? "<user" : "<internal");
	if (ce->type == GEAR_INTERNAL_CLASS && ce->info.internal.cAPI) {
		smart_str_append_printf(str, ":%s", ce->info.internal.cAPI->name);
	}
	smart_str_append_printf(str, "> ");
	if (ce->get_iterator != NULL) {
		smart_str_append_printf(str, "<iterateable> ");
	}
	if (ce->ce_flags & GEAR_ACC_INTERFACE) {
		smart_str_append_printf(str, "interface ");
	} else if (ce->ce_flags & GEAR_ACC_TRAIT) {
		smart_str_append_printf(str, "trait ");
	} else {
		if (ce->ce_flags & (GEAR_ACC_IMPLICIT_ABSTRACT_CLASS|GEAR_ACC_EXPLICIT_ABSTRACT_CLASS)) {
			smart_str_append_printf(str, "abstract ");
		}
		if (ce->ce_flags & GEAR_ACC_FINAL) {
			smart_str_append_printf(str, "final ");
		}
		smart_str_append_printf(str, "class ");
	}
	smart_str_append_printf(str, "%s", ZSTR_VAL(ce->name));
	if (ce->parent) {
		smart_str_append_printf(str, " extends %s", ZSTR_VAL(ce->parent->name));
	}

	if (ce->num_interfaces) {
		uint32_t i;

		if (ce->ce_flags & GEAR_ACC_INTERFACE) {
			smart_str_append_printf(str, " extends %s", ZSTR_VAL(ce->interfaces[0]->name));
		} else {
			smart_str_append_printf(str, " implements %s", ZSTR_VAL(ce->interfaces[0]->name));
		}
		for (i = 1; i < ce->num_interfaces; ++i) {
			smart_str_append_printf(str, ", %s", ZSTR_VAL(ce->interfaces[i]->name));
		}
	}
	smart_str_append_printf(str, " ] {\n");

	/* The information where a class is declared is only available for user classes */
	if (ce->type == GEAR_USER_CLASS) {
		smart_str_append_printf(str, "%s  @@ %s %d-%d\n", indent, ZSTR_VAL(ce->info.user.filename),
						ce->info.user.line_start, ce->info.user.line_end);
	}

	/* Constants */
	smart_str_append_printf(str, "\n");
	count = gear_hash_num_elements(&ce->constants_table);
	smart_str_append_printf(str, "%s  - Constants [%d] {\n", indent, count);
	if (count > 0) {
		gear_string *key;
		gear_class_constant *c;

		GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->constants_table, key, c) {
			_class_const_string(str, ZSTR_VAL(key), c, ZSTR_VAL(sub_indent));
			if (UNEXPECTED(EG(exception))) {
				return;
			}
		} GEAR_HASH_FOREACH_END();
	}
	smart_str_append_printf(str, "%s  }\n", indent);

	/* Static properties */
	/* counting static properties */
	count = gear_hash_num_elements(&ce->properties_info);
	if (count > 0) {
		gear_property_info *prop;

		GEAR_HASH_FOREACH_PTR(&ce->properties_info, prop) {
			if(prop->flags & GEAR_ACC_SHADOW) {
				count_shadow_props++;
			} else if (prop->flags & GEAR_ACC_STATIC) {
				count_static_props++;
			}
		} GEAR_HASH_FOREACH_END();
	}

	/* static properties */
	smart_str_append_printf(str, "\n%s  - Static properties [%d] {\n", indent, count_static_props);
	if (count_static_props > 0) {
		gear_property_info *prop;

		GEAR_HASH_FOREACH_PTR(&ce->properties_info, prop) {
			if ((prop->flags & GEAR_ACC_STATIC) && !(prop->flags & GEAR_ACC_SHADOW)) {
				_property_string(str, prop, NULL, ZSTR_VAL(sub_indent));
			}
		} GEAR_HASH_FOREACH_END();
	}
	smart_str_append_printf(str, "%s  }\n", indent);

	/* Static methods */
	/* counting static methods */
	count = gear_hash_num_elements(&ce->function_table);
	if (count > 0) {
		gear_function *mptr;

		GEAR_HASH_FOREACH_PTR(&ce->function_table, mptr) {
			if (mptr->common.fn_flags & GEAR_ACC_STATIC
				&& ((mptr->common.fn_flags & GEAR_ACC_PRIVATE) == 0 || mptr->common.scope == ce))
			{
				count_static_funcs++;
			}
		} GEAR_HASH_FOREACH_END();
	}

	/* static methods */
	smart_str_append_printf(str, "\n%s  - Static methods [%d] {", indent, count_static_funcs);
	if (count_static_funcs > 0) {
		gear_function *mptr;

		GEAR_HASH_FOREACH_PTR(&ce->function_table, mptr) {
			if (mptr->common.fn_flags & GEAR_ACC_STATIC
				&& ((mptr->common.fn_flags & GEAR_ACC_PRIVATE) == 0 || mptr->common.scope == ce))
			{
				smart_str_append_printf(str, "\n");
				_function_string(str, mptr, ce, ZSTR_VAL(sub_indent));
			}
		} GEAR_HASH_FOREACH_END();
	} else {
		smart_str_append_printf(str, "\n");
	}
	smart_str_append_printf(str, "%s  }\n", indent);

	/* Default/Implicit properties */
	count = gear_hash_num_elements(&ce->properties_info) - count_static_props - count_shadow_props;
	smart_str_append_printf(str, "\n%s  - Properties [%d] {\n", indent, count);
	if (count > 0) {
		gear_property_info *prop;

		GEAR_HASH_FOREACH_PTR(&ce->properties_info, prop) {
			if (!(prop->flags & (GEAR_ACC_STATIC|GEAR_ACC_SHADOW))) {
				_property_string(str, prop, NULL, ZSTR_VAL(sub_indent));
			}
		} GEAR_HASH_FOREACH_END();
	}
	smart_str_append_printf(str, "%s  }\n", indent);

	if (obj && Z_TYPE_P(obj) == IS_OBJECT && Z_OBJ_HT_P(obj)->get_properties) {
		HashTable    *properties = Z_OBJ_HT_P(obj)->get_properties(obj);
		gear_string  *prop_name;
		smart_str prop_str = {0};

		count = 0;
		if (properties && gear_hash_num_elements(properties)) {
			GEAR_HASH_FOREACH_STR_KEY(properties, prop_name) {
				if (prop_name && ZSTR_LEN(prop_name) && ZSTR_VAL(prop_name)[0]) { /* skip all private and protected properties */
					if (!gear_hash_exists(&ce->properties_info, prop_name)) {
						count++;
						_property_string(&prop_str, NULL, ZSTR_VAL(prop_name), ZSTR_VAL(sub_indent));
					}
				}
			} GEAR_HASH_FOREACH_END();
		}

		smart_str_append_printf(str, "\n%s  - Dynamic properties [%d] {\n", indent, count);
		smart_str_append_smart_str(str, &prop_str);
		smart_str_append_printf(str, "%s  }\n", indent);
		smart_str_free(&prop_str);
	}

	/* Non static methods */
	count = gear_hash_num_elements(&ce->function_table) - count_static_funcs;
	if (count > 0) {
		gear_function *mptr;
		gear_string *key;
		smart_str method_str = {0};

		count = 0;
		GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->function_table, key, mptr) {
			if ((mptr->common.fn_flags & GEAR_ACC_STATIC) == 0
				&& ((mptr->common.fn_flags & GEAR_ACC_PRIVATE) == 0 || mptr->common.scope == ce))
			{
				size_t len = ZSTR_LEN(mptr->common.function_name);

				/* Do not display old-style inherited constructors */
				if ((mptr->common.fn_flags & GEAR_ACC_CTOR) == 0
					|| mptr->common.scope == ce
					|| !key
					|| gear_binary_strcasecmp(ZSTR_VAL(key), ZSTR_LEN(key), ZSTR_VAL(mptr->common.function_name), len) == 0)
				{
					gear_function *closure;
					/* see if this is a closure */
					if (ce == gear_ce_closure && obj && (len == sizeof(GEAR_INVOKE_FUNC_NAME)-1)
						&& memcmp(ZSTR_VAL(mptr->common.function_name), GEAR_INVOKE_FUNC_NAME, sizeof(GEAR_INVOKE_FUNC_NAME)-1) == 0
						&& (closure = gear_get_closure_invoke_method(Z_OBJ_P(obj))) != NULL)
					{
						mptr = closure;
					} else {
						closure = NULL;
					}
					smart_str_appendc(&method_str, '\n');
					_function_string(&method_str, mptr, ce, ZSTR_VAL(sub_indent));
					count++;
					_free_function(closure);
				}
			}
		} GEAR_HASH_FOREACH_END();
		smart_str_append_printf(str, "\n%s  - Methods [%d] {", indent, count);
		smart_str_append_smart_str(str, &method_str);
		if (!count) {
			smart_str_append_printf(str, "\n");
		}
		smart_str_free(&method_str);
	} else {
		smart_str_append_printf(str, "\n%s  - Methods [0] {\n", indent);
	}
	smart_str_append_printf(str, "%s  }\n", indent);

	smart_str_append_printf(str, "%s}\n", indent);
	gear_string_release_ex(sub_indent, 0);
}
/* }}} */

/* {{{ _const_string */
static void _const_string(smart_str *str, char *name, zval *value, char *indent)
{
	char *type = gear_zval_type_name(value);

	if (Z_TYPE_P(value) == IS_ARRAY) {
		smart_str_append_printf(str, "%s    Constant [ %s %s ] { Array }\n",
						indent, type, name);
	} else if (Z_TYPE_P(value) == IS_STRING) {
		smart_str_append_printf(str, "%s    Constant [ %s %s ] { %s }\n",
						indent, type, name, Z_STRVAL_P(value));
	} else {
		gear_string *tmp_value_str;
		gear_string *value_str = zval_get_tmp_string(value, &tmp_value_str);
		smart_str_append_printf(str, "%s    Constant [ %s %s ] { %s }\n",
						indent, type, name, ZSTR_VAL(value_str));
		gear_tmp_string_release(tmp_value_str);
	}
}
/* }}} */

/* {{{ _class_const_string */
static void _class_const_string(smart_str *str, char *name, gear_class_constant *c, char *indent)
{
	char *visibility = gear_visibility_string(Z_ACCESS_FLAGS(c->value));
	char *type;

	zval_update_constant_ex(&c->value, c->ce);
	type = gear_zval_type_name(&c->value);

	if (Z_TYPE(c->value) == IS_ARRAY) {
		smart_str_append_printf(str, "%sConstant [ %s %s %s ] { Array }\n",
						indent, visibility, type, name);
	} else {
		gear_string *tmp_value_str;
		gear_string *value_str = zval_get_tmp_string(&c->value, &tmp_value_str);

		smart_str_append_printf(str, "%sConstant [ %s %s %s ] { %s }\n",
						indent, visibility, type, name, ZSTR_VAL(value_str));

		gear_tmp_string_release(tmp_value_str);
	}
}
/* }}} */

/* {{{ _get_recv_opcode */
static gear_op* _get_recv_op(gear_op_array *op_array, uint32_t offset)
{
	gear_op *op = op_array->opcodes;
	gear_op *end = op + op_array->last;

	++offset;
	while (op < end) {
		if ((op->opcode == GEAR_RECV || op->opcode == GEAR_RECV_INIT
		    || op->opcode == GEAR_RECV_VARIADIC) && op->op1.num == offset)
		{
			return op;
		}
		++op;
	}
	return NULL;
}
/* }}} */

/* {{{ _parameter_string */
static void _parameter_string(smart_str *str, gear_function *fptr, struct _gear_arg_info *arg_info, uint32_t offset, gear_bool required, char* indent)
{
	smart_str_append_printf(str, "Parameter #%d [ ", offset);
	if (!required) {
		smart_str_append_printf(str, "<optional> ");
	} else {
		smart_str_append_printf(str, "<required> ");
	}
	if (GEAR_TYPE_IS_CLASS(arg_info->type)) {
		smart_str_append_printf(str, "%s ",
			ZSTR_VAL(GEAR_TYPE_NAME(arg_info->type)));
		if (GEAR_TYPE_ALLOW_NULL(arg_info->type)) {
			smart_str_append_printf(str, "or NULL ");
		}
	} else if (GEAR_TYPE_IS_CODE(arg_info->type)) {
		smart_str_append_printf(str, "%s ", gear_get_type_by_const(GEAR_TYPE_CODE(arg_info->type)));
		if (GEAR_TYPE_ALLOW_NULL(arg_info->type)) {
			smart_str_append_printf(str, "or NULL ");
		}
	}
	if (arg_info->pass_by_reference) {
		smart_str_appendc(str, '&');
	}
	if (arg_info->is_variadic) {
		smart_str_appends(str, "...");
	}
	if (arg_info->name) {
		smart_str_append_printf(str, "$%s",
			(fptr->type == GEAR_INTERNAL_FUNCTION &&
			 !(fptr->common.fn_flags & GEAR_ACC_USER_ARG_INFO)) ?
			((gear_internal_arg_info*)arg_info)->name :
			ZSTR_VAL(arg_info->name));
	} else {
		smart_str_append_printf(str, "$param%d", offset);
	}
	if (fptr->type == GEAR_USER_FUNCTION && !required) {
		gear_op *precv = _get_recv_op((gear_op_array*)fptr, offset);
		if (precv && precv->opcode == GEAR_RECV_INIT && precv->op2_type != IS_UNUSED) {
			zval zv;

			smart_str_appends(str, " = ");
			ZVAL_COPY(&zv, RT_CONSTANT(precv, precv->op2));
			if (UNEXPECTED(zval_update_constant_ex(&zv, fptr->common.scope) == FAILURE)) {
				zval_ptr_dtor(&zv);
				return;
			}
			if (Z_TYPE(zv) == IS_TRUE) {
				smart_str_appends(str, "true");
			} else if (Z_TYPE(zv) == IS_FALSE) {
				smart_str_appends(str, "false");
			} else if (Z_TYPE(zv) == IS_NULL) {
				smart_str_appends(str, "NULL");
			} else if (Z_TYPE(zv) == IS_STRING) {
				smart_str_appendc(str, '\'');
				smart_str_appendl(str, Z_STRVAL(zv), MIN(Z_STRLEN(zv), 15));
				if (Z_STRLEN(zv) > 15) {
					smart_str_appends(str, "...");
				}
				smart_str_appendc(str, '\'');
			} else if (Z_TYPE(zv) == IS_ARRAY) {
				smart_str_appends(str, "Array");
			} else {
				gear_string *tmp_zv_str;
				gear_string *zv_str = zval_get_tmp_string(&zv, &tmp_zv_str);
				smart_str_append(str, zv_str);
				gear_tmp_string_release(tmp_zv_str);
			}
			zval_ptr_dtor(&zv);
		}
	}
	smart_str_appends(str, " ]");
}
/* }}} */

/* {{{ _function_parameter_string */
static void _function_parameter_string(smart_str *str, gear_function *fptr, char* indent)
{
	struct _gear_arg_info *arg_info = fptr->common.arg_info;
	uint32_t i, num_args, num_required = fptr->common.required_num_args;

	if (!arg_info) {
		return;
	}

	num_args = fptr->common.num_args;
	if (fptr->common.fn_flags & GEAR_ACC_VARIADIC) {
		num_args++;
	}
	smart_str_appendc(str, '\n');
	smart_str_append_printf(str, "%s- Parameters [%d] {\n", indent, num_args);
	for (i = 0; i < num_args; i++) {
		smart_str_append_printf(str, "%s  ", indent);
		_parameter_string(str, fptr, arg_info, i, i < num_required, indent);
		smart_str_appendc(str, '\n');
		arg_info++;
	}
	smart_str_append_printf(str, "%s}\n", indent);
}
/* }}} */

/* {{{ _function_closure_string */
static void _function_closure_string(smart_str *str, gear_function *fptr, char* indent)
{
	uint32_t i, count;
	gear_string *key;
	HashTable *static_variables;

	if (fptr->type != GEAR_USER_FUNCTION || !fptr->op_array.static_variables) {
		return;
	}

	static_variables = fptr->op_array.static_variables;
	count = gear_hash_num_elements(static_variables);

	if (!count) {
		return;
	}

	smart_str_append_printf(str, "\n");
	smart_str_append_printf(str, "%s- Bound Variables [%d] {\n", indent, gear_hash_num_elements(static_variables));
	i = 0;
	GEAR_HASH_FOREACH_STR_KEY(static_variables, key) {
		smart_str_append_printf(str, "%s    Variable #%d [ $%s ]\n", indent, i++, ZSTR_VAL(key));
	} GEAR_HASH_FOREACH_END();
	smart_str_append_printf(str, "%s}\n", indent);
}
/* }}} */

/* {{{ _function_string */
static void _function_string(smart_str *str, gear_function *fptr, gear_class_entry *scope, char* indent)
{
	smart_str param_indent = {0};
	gear_function *overwrites;
	gear_string *lc_name;

	/* TBD: Repair indenting of doc comment (or is this to be done in the parser?)
	 * What's "wrong" is that any whitespace before the doc comment start is
	 * swallowed, leading to an unaligned comment.
	 */
	if (fptr->type == GEAR_USER_FUNCTION && fptr->op_array.doc_comment) {
		smart_str_append_printf(str, "%s%s\n", indent, ZSTR_VAL(fptr->op_array.doc_comment));
	}

	smart_str_appendl(str, indent, strlen(indent));
	smart_str_append_printf(str, fptr->common.fn_flags & GEAR_ACC_CLOSURE ? "Closure [ " : (fptr->common.scope ? "Method [ " : "Function [ "));
	smart_str_append_printf(str, (fptr->type == GEAR_USER_FUNCTION) ? "<user" : "<internal");
	if (fptr->common.fn_flags & GEAR_ACC_DEPRECATED) {
		smart_str_appends(str, ", deprecated");
	}
	if (fptr->type == GEAR_INTERNAL_FUNCTION && ((gear_internal_function*)fptr)->cAPI) {
		smart_str_append_printf(str, ":%s", ((gear_internal_function*)fptr)->cAPI->name);
	}

	if (scope && fptr->common.scope) {
		if (fptr->common.scope != scope) {
			smart_str_append_printf(str, ", inherits %s", ZSTR_VAL(fptr->common.scope->name));
		} else if (fptr->common.scope->parent) {
			lc_name = gear_string_tolower(fptr->common.function_name);
			if ((overwrites = gear_hash_find_ptr(&fptr->common.scope->parent->function_table, lc_name)) != NULL) {
				if (fptr->common.scope != overwrites->common.scope) {
					smart_str_append_printf(str, ", overwrites %s", ZSTR_VAL(overwrites->common.scope->name));
				}
			}
			gear_string_release_ex(lc_name, 0);
		}
	}
	if (fptr->common.prototype && fptr->common.prototype->common.scope) {
		smart_str_append_printf(str, ", prototype %s", ZSTR_VAL(fptr->common.prototype->common.scope->name));
	}
	if (fptr->common.fn_flags & GEAR_ACC_CTOR) {
		smart_str_appends(str, ", ctor");
	}
	if (fptr->common.fn_flags & GEAR_ACC_DTOR) {
		smart_str_appends(str, ", dtor");
	}
	smart_str_appends(str, "> ");

	if (fptr->common.fn_flags & GEAR_ACC_ABSTRACT) {
		smart_str_appends(str, "abstract ");
	}
	if (fptr->common.fn_flags & GEAR_ACC_FINAL) {
		smart_str_appends(str, "final ");
	}
	if (fptr->common.fn_flags & GEAR_ACC_STATIC) {
		smart_str_appends(str, "static ");
	}

	if (fptr->common.scope) {
		/* These are mutually exclusive */
		switch (fptr->common.fn_flags & GEAR_ACC_PPP_MASK) {
			case GEAR_ACC_PUBLIC:
				smart_str_appends(str, "public ");
				break;
			case GEAR_ACC_PRIVATE:
				smart_str_appends(str, "private ");
				break;
			case GEAR_ACC_PROTECTED:
				smart_str_appends(str, "protected ");
				break;
			default:
				smart_str_appends(str, "<visibility error> ");
				break;
		}
		smart_str_appends(str, "method ");
	} else {
		smart_str_appends(str, "function ");
	}

	if (fptr->op_array.fn_flags & GEAR_ACC_RETURN_REFERENCE) {
		smart_str_appendc(str, '&');
	}
	smart_str_append_printf(str, "%s ] {\n", ZSTR_VAL(fptr->common.function_name));
	/* The information where a function is declared is only available for user classes */
	if (fptr->type == GEAR_USER_FUNCTION) {
		smart_str_append_printf(str, "%s  @@ %s %d - %d\n", indent,
						ZSTR_VAL(fptr->op_array.filename),
						fptr->op_array.line_start,
						fptr->op_array.line_end);
	}
	smart_str_append_printf(&param_indent, "%s  ", indent);
	smart_str_0(&param_indent);
	if (fptr->common.fn_flags & GEAR_ACC_CLOSURE) {
		_function_closure_string(str, fptr, ZSTR_VAL(param_indent.s));
	}
	_function_parameter_string(str, fptr, ZSTR_VAL(param_indent.s));
	smart_str_free(&param_indent);
	if (fptr->op_array.fn_flags & GEAR_ACC_HAS_RETURN_TYPE) {
		smart_str_append_printf(str, "  %s- Return [ ", indent);
		if (GEAR_TYPE_IS_CLASS(fptr->common.arg_info[-1].type)) {
			smart_str_append_printf(str, "%s ",
				ZSTR_VAL(GEAR_TYPE_NAME(fptr->common.arg_info[-1].type)));
			if (GEAR_TYPE_ALLOW_NULL(fptr->common.arg_info[-1].type)) {
				smart_str_appends(str, "or NULL ");
			}
		} else if (GEAR_TYPE_IS_CODE(fptr->common.arg_info[-1].type)) {
			smart_str_append_printf(str, "%s ", gear_get_type_by_const(GEAR_TYPE_CODE(fptr->common.arg_info[-1].type)));
			if (GEAR_TYPE_ALLOW_NULL(fptr->common.arg_info[-1].type)) {
				smart_str_appends(str, "or NULL ");
			}
		}
		smart_str_appends(str, "]\n");
	}
	smart_str_append_printf(str, "%s}\n", indent);
}
/* }}} */

/* {{{ _property_string */
static void _property_string(smart_str *str, gear_property_info *prop, const char *prop_name, char* indent)
{
	smart_str_append_printf(str, "%sProperty [ ", indent);
	if (!prop) {
		smart_str_append_printf(str, "<dynamic> public $%s", prop_name);
	} else {
		if (!(prop->flags & GEAR_ACC_STATIC)) {
			if (prop->flags & GEAR_ACC_IMPLICIT_PUBLIC) {
				smart_str_appends(str, "<implicit> ");
			} else {
				smart_str_appends(str, "<default> ");
			}
		}

		/* These are mutually exclusive */
		switch (prop->flags & GEAR_ACC_PPP_MASK) {
			case GEAR_ACC_PUBLIC:
				smart_str_appends(str, "public ");
				break;
			case GEAR_ACC_PRIVATE:
				smart_str_appends(str, "private ");
				break;
			case GEAR_ACC_PROTECTED:
				smart_str_appends(str, "protected ");
				break;
		}
		if (prop->flags & GEAR_ACC_STATIC) {
			smart_str_appends(str, "static ");
		}
		if (!prop_name) {
			const char *class_name;
			gear_unmangle_property_name(prop->name, &class_name, &prop_name);
		}
		smart_str_append_printf(str, "$%s", prop_name);
	}

	smart_str_appends(str, " ]\n");
}
/* }}} */

static int _extension_ics_string(zval *el, int num_args, va_list args, gear_hash_key *hash_key) /* {{{ */
{
	gear_ics_entry *ics_entry = (gear_ics_entry*)Z_PTR_P(el);
	smart_str *str = va_arg(args, smart_str *);
	char *indent = va_arg(args, char *);
	int number = va_arg(args, int);
	char *comma = "";

	if (number == ics_entry->capi_number) {
		smart_str_append_printf(str, "    %sEntry [ %s <", indent, ZSTR_VAL(ics_entry->name));
		if (ics_entry->modifiable == GEAR_ICS_ALL) {
			smart_str_appends(str, "ALL");
		} else {
			if (ics_entry->modifiable & GEAR_ICS_USER) {
				smart_str_appends(str, "USER");
				comma = ",";
			}
			if (ics_entry->modifiable & GEAR_ICS_PERDIR) {
				smart_str_append_printf(str, "%sPERDIR", comma);
				comma = ",";
			}
			if (ics_entry->modifiable & GEAR_ICS_SYSTEM) {
				smart_str_append_printf(str, "%sSYSTEM", comma);
			}
		}

		smart_str_appends(str, "> ]\n");
		smart_str_append_printf(str, "    %s  Current = '%s'\n", indent, ics_entry->value ? ZSTR_VAL(ics_entry->value) : "");
		if (ics_entry->modified) {
			smart_str_append_printf(str, "    %s  Default = '%s'\n", indent, ics_entry->orig_value ? ZSTR_VAL(ics_entry->orig_value) : "");
		}
		smart_str_append_printf(str, "    %s}\n", indent);
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

static int _extension_class_string(zval *el, int num_args, va_list args, gear_hash_key *hash_key) /* {{{ */
{
	gear_class_entry *ce = (gear_class_entry*)Z_PTR_P(el);
	smart_str *str = va_arg(args, smart_str *);
	char *indent = va_arg(args, char *);
	struct _gear_capi_entry *cAPI = va_arg(args, struct _gear_capi_entry*);
	int *num_classes = va_arg(args, int*);

	if ((ce->type == GEAR_INTERNAL_CLASS) && ce->info.internal.cAPI && !strcasecmp(ce->info.internal.cAPI->name, cAPI->name)) {
		/* dump class if it is not an alias */
		if (!gear_binary_strcasecmp(ZSTR_VAL(ce->name), ZSTR_LEN(ce->name), ZSTR_VAL(hash_key->key), ZSTR_LEN(hash_key->key))) {
			smart_str_append_printf(str, "\n");
			_class_string(str, ce, NULL, indent);
			(*num_classes)++;
		}
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

static int _extension_const_string(zval *el, int num_args, va_list args, gear_hash_key *hash_key) /* {{{ */
{
	gear_constant *constant = (gear_constant*)Z_PTR_P(el);
	smart_str *str = va_arg(args, smart_str *);
	char *indent = va_arg(args, char *);
	struct _gear_capi_entry *cAPI = va_arg(args, struct _gear_capi_entry*);
	int *num_classes = va_arg(args, int*);

	if (GEAR_CONSTANT_CAPI_NUMBER(constant)  == cAPI->capi_number) {
		_const_string(str, ZSTR_VAL(constant->name), &constant->value, indent);
		(*num_classes)++;
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

static void _extension_string(smart_str *str, gear_capi_entry *cAPI, char *indent) /* {{{ */
{
	smart_str_append_printf(str, "%sExtension [ ", indent);
	if (cAPI->type == CAPI_PERSISTENT) {
		smart_str_appends(str, "<persistent>");
	}
	if (cAPI->type == CAPI_TEMPORARY) {
		smart_str_appends(str, "<temporary>" );
	}
	smart_str_append_printf(str, " extension #%d %s version %s ] {\n",
					cAPI->capi_number, cAPI->name,
					(cAPI->version == NO_VERSION_YET) ? "<no_version>" : cAPI->version);

	if (cAPI->deps) {
		const gear_capi_dep* dep = cAPI->deps;

		smart_str_appends(str, "\n  - Dependencies {\n");

		while(dep->name) {
			smart_str_append_printf(str, "%s    Dependency [ %s (", indent, dep->name);

			switch(dep->type) {
			case CAPI_DEP_REQUIRED:
				smart_str_appends(str, "Required");
				break;
			case CAPI_DEP_CONFLICTS:
				smart_str_appends(str, "Conflicts");
				break;
			case CAPI_DEP_OPTIONAL:
				smart_str_appends(str, "Optional");
				break;
			default:
				smart_str_appends(str, "Error"); /* shouldn't happen */
				break;
			}

			if (dep->rel) {
				smart_str_append_printf(str, " %s", dep->rel);
			}
			if (dep->version) {
				smart_str_append_printf(str, " %s", dep->version);
			}
			smart_str_appends(str, ") ]\n");
			dep++;
		}
		smart_str_append_printf(str, "%s  }\n", indent);
	}

	{
		smart_str str_ics = {0};
		gear_hash_apply_with_arguments(EG(ics_directives), (apply_func_args_t) _extension_ics_string, 3, &str_ics, indent, cAPI->capi_number);
		if (smart_str_get_len(&str_ics) > 0) {
			smart_str_append_printf(str, "\n  - ICS {\n");
			smart_str_append_smart_str(str, &str_ics);
			smart_str_append_printf(str, "%s  }\n", indent);
		}
		smart_str_free(&str_ics);
	}

	{
		smart_str str_constants = {0};
		int num_constants = 0;

		gear_hash_apply_with_arguments(EG(gear_constants), (apply_func_args_t) _extension_const_string, 4, &str_constants, indent, cAPI, &num_constants);
		if (num_constants) {
			smart_str_append_printf(str, "\n  - Constants [%d] {\n", num_constants);
			smart_str_append_smart_str(str, &str_constants);
			smart_str_append_printf(str, "%s  }\n", indent);
		}
		smart_str_free(&str_constants);
	}

	{
		gear_function *fptr;
		int first = 1;

		GEAR_HASH_FOREACH_PTR(CG(function_table), fptr) {
			if (fptr->common.type==GEAR_INTERNAL_FUNCTION
				&& fptr->internal_function.cAPI == cAPI) {
				if (first) {
					smart_str_append_printf(str, "\n  - Functions {\n");
					first = 0;
				}
				_function_string(str, fptr, NULL, "    ");
			}
		} GEAR_HASH_FOREACH_END();
		if (!first) {
			smart_str_append_printf(str, "%s  }\n", indent);
		}
	}

	{
		gear_string *sub_indent = strpprintf(0, "%s    ", indent);
		smart_str str_classes = {0};
		int num_classes = 0;

		gear_hash_apply_with_arguments(EG(class_table), (apply_func_args_t) _extension_class_string, 4, &str_classes, ZSTR_VAL(sub_indent), cAPI, &num_classes);
		if (num_classes) {
			smart_str_append_printf(str, "\n  - Classes [%d] {", num_classes);
			smart_str_append_smart_str(str, &str_classes);
			smart_str_append_printf(str, "%s  }\n", indent);
		}
		smart_str_free(&str_classes);
		gear_string_release_ex(sub_indent, 0);
	}

	smart_str_append_printf(str, "%s}\n", indent);
}
/* }}} */

static void _gear_extension_string(smart_str *str, gear_extension *extension, char *indent) /* {{{ */
{
	smart_str_append_printf(str, "%sGear Extension [ %s ", indent, extension->name);

	if (extension->version) {
		smart_str_append_printf(str, "%s ", extension->version);
	}
	if (extension->copyright) {
		smart_str_append_printf(str, "%s ", extension->copyright);
	}
	if (extension->author) {
		smart_str_append_printf(str, "by %s ", extension->author);
	}
	if (extension->URL) {
		smart_str_append_printf(str, "<%s> ", extension->URL);
	}

	smart_str_appends(str, "]\n");
}
/* }}} */

/* {{{ _function_check_flag */
static void _function_check_flag(INTERNAL_FUNCTION_PARAMETERS, int mask)
{
	reflection_object *intern;
	gear_function *mptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(mptr);
	RETURN_BOOL(mptr->common.fn_flags & mask);
}
/* }}} */

/* {{{ gear_reflection_class_factory */
HYSSAPI void gear_reflection_class_factory(gear_class_entry *ce, zval *object)
{
	reflection_object *intern;
	zval name;

	ZVAL_STR_COPY(&name, ce->name);
	reflection_instantiate(reflection_class_ptr, object);
	intern = Z_REFLECTION_P(object);
	intern->ptr = ce;
	intern->ref_type = REF_TYPE_OTHER;
	intern->ce = ce;
	reflection_update_property_name(object, &name);
}
/* }}} */

/* {{{ reflection_extension_factory */
static void reflection_extension_factory(zval *object, const char *name_str)
{
	reflection_object *intern;
	zval name;
	size_t name_len = strlen(name_str);
	gear_string *lcname;
	struct _gear_capi_entry *cAPI;

	lcname = gear_string_alloc(name_len, 0);
	gear_str_tolower_copy(ZSTR_VAL(lcname), name_str, name_len);
	cAPI = gear_hash_find_ptr(&capi_registry, lcname);
	gear_string_efree(lcname);
	if (!cAPI) {
		return;
	}

	reflection_instantiate(reflection_extension_ptr, object);
	intern = Z_REFLECTION_P(object);
	ZVAL_STRINGL(&name, cAPI->name, name_len);
	intern->ptr = cAPI;
	intern->ref_type = REF_TYPE_OTHER;
	intern->ce = NULL;
	reflection_update_property_name(object, &name);
}
/* }}} */

/* {{{ reflection_parameter_factory */
static void reflection_parameter_factory(gear_function *fptr, zval *closure_object, struct _gear_arg_info *arg_info, uint32_t offset, gear_bool required, zval *object)
{
	reflection_object *intern;
	parameter_reference *reference;
	zval name;

	if (arg_info->name) {
		if (fptr->type == GEAR_INTERNAL_FUNCTION &&
		    !(fptr->common.fn_flags & GEAR_ACC_USER_ARG_INFO)) {
			ZVAL_STRING(&name, ((gear_internal_arg_info*)arg_info)->name);
		} else {
			ZVAL_STR_COPY(&name, arg_info->name);
		}
	} else {
		ZVAL_NULL(&name);
	}
	reflection_instantiate(reflection_parameter_ptr, object);
	intern = Z_REFLECTION_P(object);
	reference = (parameter_reference*) emalloc(sizeof(parameter_reference));
	reference->arg_info = arg_info;
	reference->offset = offset;
	reference->required = required;
	reference->fptr = fptr;
	intern->ptr = reference;
	intern->ref_type = REF_TYPE_PARAMETER;
	intern->ce = fptr->common.scope;
	if (closure_object) {
		Z_ADDREF_P(closure_object);
		ZVAL_COPY_VALUE(&intern->obj, closure_object);
	}
	reflection_update_property_name(object, &name);
}
/* }}} */

/* {{{ reflection_type_factory */
static void reflection_type_factory(gear_function *fptr, zval *closure_object, struct _gear_arg_info *arg_info, zval *object)
{
	reflection_object *intern;
	type_reference *reference;

	reflection_instantiate(reflection_named_type_ptr, object);
	intern = Z_REFLECTION_P(object);
	reference = (type_reference*) emalloc(sizeof(type_reference));
	reference->arg_info = arg_info;
	reference->fptr = fptr;
	intern->ptr = reference;
	intern->ref_type = REF_TYPE_TYPE;
	intern->ce = fptr->common.scope;
	if (closure_object) {
		Z_ADDREF_P(closure_object);
		ZVAL_COPY_VALUE(&intern->obj, closure_object);
	}
}
/* }}} */

/* {{{ reflection_function_factory */
static void reflection_function_factory(gear_function *function, zval *closure_object, zval *object)
{
	reflection_object *intern;
	zval name;

	ZVAL_STR_COPY(&name, function->common.function_name);

	reflection_instantiate(reflection_function_ptr, object);
	intern = Z_REFLECTION_P(object);
	intern->ptr = function;
	intern->ref_type = REF_TYPE_FUNCTION;
	intern->ce = NULL;
	if (closure_object) {
		Z_ADDREF_P(closure_object);
		ZVAL_COPY_VALUE(&intern->obj, closure_object);
	}
	reflection_update_property_name(object, &name);
}
/* }}} */

/* {{{ reflection_method_factory */
static void reflection_method_factory(gear_class_entry *ce, gear_function *method, zval *closure_object, zval *object)
{
	reflection_object *intern;
	zval name;
	zval classname;

	ZVAL_STR_COPY(&name, (method->common.scope && method->common.scope->trait_aliases)?
			gear_resolve_method_name(ce, method) : method->common.function_name);
	ZVAL_STR_COPY(&classname, method->common.scope->name);
	reflection_instantiate(reflection_method_ptr, object);
	intern = Z_REFLECTION_P(object);
	intern->ptr = method;
	intern->ref_type = REF_TYPE_FUNCTION;
	intern->ce = ce;
	if (closure_object) {
		Z_ADDREF_P(closure_object);
		ZVAL_COPY_VALUE(&intern->obj, closure_object);
	}
	reflection_update_property_name(object, &name);
	reflection_update_property_class(object, &classname);
}
/* }}} */

/* {{{ reflection_property_factory */
static void reflection_property_factory(gear_class_entry *ce, gear_string *name, gear_property_info *prop, zval *object)
{
	reflection_object *intern;
	zval propname;
	zval classname;
	property_reference *reference;

	if (!(prop->flags & GEAR_ACC_PRIVATE)) {
		/* we have to search the class hierarchy for this (implicit) public or protected property */
		gear_class_entry *tmp_ce = ce, *store_ce = ce;
		gear_property_info *tmp_info = NULL;

		while (tmp_ce && (tmp_info = gear_hash_find_ptr(&tmp_ce->properties_info, name)) == NULL) {
			ce = tmp_ce;
			tmp_ce = tmp_ce->parent;
		}

		if (tmp_info && !(tmp_info->flags & GEAR_ACC_SHADOW)) { /* found something and it's not a parent's private */
			prop = tmp_info;
		} else { /* not found, use initial value */
			ce = store_ce;
		}
	}

	ZVAL_STR_COPY(&propname, name);
	ZVAL_STR_COPY(&classname, prop->ce->name);

	reflection_instantiate(reflection_property_ptr, object);
	intern = Z_REFLECTION_P(object);
	reference = (property_reference*) emalloc(sizeof(property_reference));
	reference->ce = ce;
	reference->prop = *prop;
	reference->unmangled_name = gear_string_copy(name);
	intern->ptr = reference;
	intern->ref_type = REF_TYPE_PROPERTY;
	intern->ce = ce;
	intern->ignore_visibility = 0;
	reflection_update_property_name(object, &propname);
	reflection_update_property_class(object, &classname);
}
/* }}} */

static void reflection_property_factory_str(gear_class_entry *ce, const char *name_str, size_t name_len, gear_property_info *prop, zval *object)
{
	gear_string *name = gear_string_init(name_str, name_len, 0);
	reflection_property_factory(ce, name, prop, object);
	gear_string_release(name);
}

/* {{{ reflection_class_constant_factory */
static void reflection_class_constant_factory(gear_class_entry *ce, gear_string *name_str, gear_class_constant *constant, zval *object)
{
	reflection_object *intern;
	zval name;
	zval classname;

	ZVAL_STR_COPY(&name, name_str);
	ZVAL_STR_COPY(&classname, ce->name);

	reflection_instantiate(reflection_class_constant_ptr, object);
	intern = Z_REFLECTION_P(object);
	intern->ptr = constant;
	intern->ref_type = REF_TYPE_CLASS_CONSTANT;
	intern->ce = constant->ce;
	intern->ignore_visibility = 0;
	reflection_update_property_name(object, &name);
	reflection_update_property_class(object, &classname);
}
/* }}} */

/* {{{ _reflection_export */
static void _reflection_export(INTERNAL_FUNCTION_PARAMETERS, gear_class_entry *ce_ptr, int ctor_argc)
{
	zval reflector;
	zval *argument_ptr, *argument2_ptr;
	zval retval, params[2];
	int result;
	int return_output = 0;
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;

	if (ctor_argc == 1) {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "z|b", &argument_ptr, &return_output) == FAILURE) {
			return;
		}
		ZVAL_COPY_VALUE(&params[0], argument_ptr);
		ZVAL_NULL(&params[1]);
	} else {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "zz|b", &argument_ptr, &argument2_ptr, &return_output) == FAILURE) {
			return;
		}
		ZVAL_COPY_VALUE(&params[0], argument_ptr);
		ZVAL_COPY_VALUE(&params[1], argument2_ptr);
	}

	/* Create object */
	if (object_and_properties_init(&reflector, ce_ptr, NULL) == FAILURE) {
		_DO_THROW("Could not create reflector");
	}

	/* Call __construct() */

	fci.size = sizeof(fci);
	ZVAL_UNDEF(&fci.function_name);
	fci.object = Z_OBJ(reflector);
	fci.retval = &retval;
	fci.param_count = ctor_argc;
	fci.params = params;
	fci.no_separation = 1;

	fcc.function_handler = ce_ptr->constructor;
	fcc.called_scope = Z_OBJCE(reflector);
	fcc.object = Z_OBJ(reflector);

	result = gear_call_function(&fci, &fcc);

	zval_ptr_dtor(&retval);

	if (EG(exception)) {
		zval_ptr_dtor(&reflector);
		return;
	}
	if (result == FAILURE) {
		zval_ptr_dtor(&reflector);
		_DO_THROW("Could not create reflector");
	}

	/* Call static reflection::export */
	ZVAL_COPY_VALUE(&params[0], &reflector);
	ZVAL_BOOL(&params[1], return_output);

	ZVAL_STRINGL(&fci.function_name, "reflection::export", sizeof("reflection::export") - 1);
	fci.object = NULL;
	fci.retval = &retval;
	fci.param_count = 2;
	fci.params = params;
	fci.no_separation = 1;

	result = gear_call_function(&fci, NULL);

	zval_ptr_dtor(&fci.function_name);

	if (result == FAILURE && EG(exception) == NULL) {
		zval_ptr_dtor(&reflector);
		zval_ptr_dtor(&retval);
		_DO_THROW("Could not execute reflection::export()");
	}

	if (return_output) {
		ZVAL_COPY_VALUE(return_value, &retval);
	} else {
		zval_ptr_dtor(&retval);
	}

	/* Destruct reflector which is no longer needed */
	zval_ptr_dtor(&reflector);
}
/* }}} */

/* {{{ _reflection_param_get_default_param */
static parameter_reference *_reflection_param_get_default_param(INTERNAL_FUNCTION_PARAMETERS)
{
	reflection_object *intern;
	parameter_reference *param;

	intern = Z_REFLECTION_P(getThis());
	if (intern->ptr == NULL) {
		if (EG(exception) && EG(exception)->ce == reflection_exception_ptr) {
			return NULL;
		}
		gear_throw_error(NULL, "Internal error: Failed to retrieve the reflection object");
		return NULL;
	}

	param = intern->ptr;
	if (param->fptr->type != GEAR_USER_FUNCTION) {
		gear_throw_exception_ex(reflection_exception_ptr, 0, "Cannot determine default value for internal functions");
		return NULL;
	}

	return param;
}
/* }}} */

/* {{{ _reflection_param_get_default_precv */
static gear_op *_reflection_param_get_default_precv(INTERNAL_FUNCTION_PARAMETERS, parameter_reference *param)
{
	gear_op *precv;

	if (param == NULL) {
		return NULL;
	}

	precv = _get_recv_op((gear_op_array*)param->fptr, param->offset);
	if (!precv || precv->opcode != GEAR_RECV_INIT || precv->op2_type == IS_UNUSED) {
		gear_throw_exception_ex(reflection_exception_ptr, 0, "Internal error: Failed to retrieve the default value");
		return NULL;
	}

	return precv;
}
/* }}} */

/* {{{ Preventing __clone from being called */
GEAR_METHOD(reflection, __clone)
{
	/* Should never be executable */
	_DO_THROW("Cannot clone object using __clone()");
}
/* }}} */

/* {{{ proto public static mixed Reflection::export(Reflector r [, bool return])
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection, export)
{
	zval *object, fname, retval;
	int result;
	gear_bool return_output = 0;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_OBJECT_OF_CLASS(object, reflector_ptr)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL(return_output)
	GEAR_PARSE_PARAMETERS_END();

	/* Invoke the __toString() method */
	ZVAL_STRINGL(&fname, "__tostring", sizeof("__tostring") - 1);
	result= call_user_function(NULL, object, &fname, &retval, 0, NULL);
	zval_ptr_dtor_str(&fname);

	if (result == FAILURE) {
		_DO_THROW("Invocation of method __toString() failed");
		/* Returns from this function */
	}

	if (Z_TYPE(retval) == IS_UNDEF) {
		hyss_error_docref(NULL, E_WARNING, "%s::__toString() did not return anything", ZSTR_VAL(Z_OBJCE_P(object)->name));
		RETURN_FALSE;
	}

	if (return_output) {
		ZVAL_COPY_VALUE(return_value, &retval);
	} else {
		/* No need for _r variant, return of __toString should always be a string */
		gear_print_zval(&retval, 0);
		gear_printf("\n");
		zval_ptr_dtor(&retval);
	}
}
/* }}} */

/* {{{ proto public static array Reflection::getModifierNames(int modifiers)
   Returns an array of modifier names */
GEAR_METHOD(reflection, getModifierNames)
{
	gear_long modifiers;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &modifiers) == FAILURE) {
		return;
	}

	array_init(return_value);

	if (modifiers & (GEAR_ACC_ABSTRACT | GEAR_ACC_EXPLICIT_ABSTRACT_CLASS)) {
		add_next_index_stringl(return_value, "abstract", sizeof("abstract")-1);
	}
	if (modifiers & GEAR_ACC_FINAL) {
		add_next_index_stringl(return_value, "final", sizeof("final")-1);
	}
	if (modifiers & GEAR_ACC_IMPLICIT_PUBLIC) {
		add_next_index_stringl(return_value, "public", sizeof("public")-1);
	}

	/* These are mutually exclusive */
	switch (modifiers & GEAR_ACC_PPP_MASK) {
		case GEAR_ACC_PUBLIC:
			add_next_index_stringl(return_value, "public", sizeof("public")-1);
			break;
		case GEAR_ACC_PRIVATE:
			add_next_index_stringl(return_value, "private", sizeof("private")-1);
			break;
		case GEAR_ACC_PROTECTED:
			add_next_index_stringl(return_value, "protected", sizeof("protected")-1);
			break;
	}

	if (modifiers & GEAR_ACC_STATIC) {
		add_next_index_stringl(return_value, "static", sizeof("static")-1);
	}
}
/* }}} */

/* {{{ proto public static mixed ReflectionFunction::export(string name [, bool return])
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_function, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_function_ptr, 1);
}
/* }}} */

/* {{{ proto public void ReflectionFunction::__construct(string name)
   Constructor. Throws an Exception in case the given function does not exist */
GEAR_METHOD(reflection_function, __construct)
{
	zval name;
	zval *object;
	zval *closure = NULL;
	reflection_object *intern;
	gear_function *fptr;
	gear_string *fname, *lcname;

	object = getThis();
	intern = Z_REFLECTION_P(object);

	if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, GEAR_NUM_ARGS(), "O", &closure, gear_ce_closure) == SUCCESS) {
		fptr = (gear_function*)gear_get_closure_method_def(closure);
		Z_ADDREF_P(closure);
	} else {
		ALLOCA_FLAG(use_heap)

		if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "S", &fname) == FAILURE) {
			return;
		}

		if (UNEXPECTED(ZSTR_VAL(fname)[0] == '\\')) {
			/* Ignore leading "\" */
			ZSTR_ALLOCA_ALLOC(lcname, ZSTR_LEN(fname) - 1, use_heap);
			gear_str_tolower_copy(ZSTR_VAL(lcname), ZSTR_VAL(fname) + 1, ZSTR_LEN(fname) - 1);
			fptr = gear_fetch_function(lcname);
			ZSTR_ALLOCA_FREE(lcname, use_heap);
		} else {
			lcname = gear_string_tolower(fname);
			fptr = gear_fetch_function(lcname);
			gear_string_release(lcname);
		}

		if (fptr == NULL) {
			gear_throw_exception_ex(reflection_exception_ptr, 0,
				"Function %s() does not exist", ZSTR_VAL(fname));
			return;
		}
	}

	ZVAL_STR_COPY(&name, fptr->common.function_name);
	reflection_update_property_name(object, &name);
	intern->ptr = fptr;
	intern->ref_type = REF_TYPE_FUNCTION;
	if (closure) {
		ZVAL_COPY_VALUE(&intern->obj, closure);
	} else {
		ZVAL_UNDEF(&intern->obj);
	}
	intern->ce = NULL;
}
/* }}} */

/* {{{ proto public string ReflectionFunction::__toString()
   Returns a string representation */
GEAR_METHOD(reflection_function, __toString)
{
	reflection_object *intern;
	gear_function *fptr;
	smart_str str = {0};

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);
	_function_string(&str, fptr, intern->ce, "");
	RETURN_STR(smart_str_extract(&str));
}
/* }}} */

/* {{{ proto public string ReflectionFunction::getName()
   Returns this function's name */
GEAR_METHOD(reflection_function, getName)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	_default_get_name(getThis(), return_value);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::isClosure()
   Returns whether this is a closure */
GEAR_METHOD(reflection_function, isClosure)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);
	RETURN_BOOL(fptr->common.fn_flags & GEAR_ACC_CLOSURE);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::getClosureThis()
   Returns this pointer bound to closure */
GEAR_METHOD(reflection_function, getClosureThis)
{
	reflection_object *intern;
	zval* closure_this;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT();
	if (!Z_ISUNDEF(intern->obj)) {
		closure_this = gear_get_closure_this_ptr(&intern->obj);
		if (!Z_ISUNDEF_P(closure_this)) {
			ZVAL_COPY(return_value, closure_this);
		}
	}
}
/* }}} */

/* {{{ proto public ReflectionClass ReflectionFunction::getClosureScopeClass()
   Returns the scope associated to the closure */
GEAR_METHOD(reflection_function, getClosureScopeClass)
{
	reflection_object *intern;
	const gear_function *closure_func;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT();
	if (!Z_ISUNDEF(intern->obj)) {
		closure_func = gear_get_closure_method_def(&intern->obj);
		if (closure_func && closure_func->common.scope) {
			gear_reflection_class_factory(closure_func->common.scope, return_value);
		}
	}
}
/* }}} */

/* {{{ proto public mixed ReflectionFunction::getClosure()
   Returns a dynamically created closure for the function */
GEAR_METHOD(reflection_function, getClosure)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);

	if (!Z_ISUNDEF(intern->obj)) {
		/* Closures are immutable objects */
		ZVAL_COPY(return_value, &intern->obj);
	} else {
		gear_create_fake_closure(return_value, fptr, NULL, NULL, NULL);
	}
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::isInternal()
   Returns whether this is an internal function */
GEAR_METHOD(reflection_function, isInternal)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);
	RETURN_BOOL(fptr->type == GEAR_INTERNAL_FUNCTION);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::isUserDefined()
   Returns whether this is a user-defined function */
GEAR_METHOD(reflection_function, isUserDefined)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);
	RETURN_BOOL(fptr->type == GEAR_USER_FUNCTION);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::isDisabled()
   Returns whether this function has been disabled or not */
GEAR_METHOD(reflection_function, isDisabled)
{
	reflection_object *intern;
	gear_function *fptr;

	GET_REFLECTION_OBJECT_PTR(fptr);
	RETURN_BOOL(fptr->type == GEAR_INTERNAL_FUNCTION && fptr->internal_function.handler == zif_display_disabled_function);
}
/* }}} */

/* {{{ proto public string ReflectionFunction::getFileName()
   Returns the filename of the file this function was declared in */
GEAR_METHOD(reflection_function, getFileName)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);
	if (fptr->type == GEAR_USER_FUNCTION) {
		RETURN_STR_COPY(fptr->op_array.filename);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public int ReflectionFunction::getStartLine()
   Returns the line this function's declaration starts at */
GEAR_METHOD(reflection_function, getStartLine)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);
	if (fptr->type == GEAR_USER_FUNCTION) {
		RETURN_LONG(fptr->op_array.line_start);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public int ReflectionFunction::getEndLine()
   Returns the line this function's declaration ends at */
GEAR_METHOD(reflection_function, getEndLine)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);
	if (fptr->type == GEAR_USER_FUNCTION) {
		RETURN_LONG(fptr->op_array.line_end);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public string ReflectionFunction::getDocComment()
   Returns the doc comment for this function */
GEAR_METHOD(reflection_function, getDocComment)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);
	if (fptr->type == GEAR_USER_FUNCTION && fptr->op_array.doc_comment) {
		RETURN_STR_COPY(fptr->op_array.doc_comment);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public array ReflectionFunction::getStaticVariables()
   Returns an associative array containing this function's static variables and their values */
GEAR_METHOD(reflection_function, getStaticVariables)
{
	reflection_object *intern;
	gear_function *fptr;
	zval *val;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(fptr);

	/* Return an empty array in case no static variables exist */
	if (fptr->type == GEAR_USER_FUNCTION && fptr->op_array.static_variables != NULL) {
		array_init(return_value);
		if (GC_REFCOUNT(fptr->op_array.static_variables) > 1) {
			if (!(GC_FLAGS(fptr->op_array.static_variables) & IS_ARRAY_IMMUTABLE)) {
				GC_DELREF(fptr->op_array.static_variables);
			}
			fptr->op_array.static_variables = gear_array_dup(fptr->op_array.static_variables);
		}
		GEAR_HASH_FOREACH_VAL(fptr->op_array.static_variables, val) {
			if (UNEXPECTED(zval_update_constant_ex(val, fptr->common.scope) != SUCCESS)) {
				return;
			}
		} GEAR_HASH_FOREACH_END();
		gear_hash_copy(Z_ARRVAL_P(return_value), fptr->op_array.static_variables, zval_add_ref);
	} else {
		ZVAL_EMPTY_ARRAY(return_value);
	}
}
/* }}} */

/* {{{ proto public mixed ReflectionFunction::invoke([mixed* args])
   Invokes the function */
GEAR_METHOD(reflection_function, invoke)
{
	zval retval;
	zval *params = NULL;
	int result, num_args = 0;
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;
	reflection_object *intern;
	gear_function *fptr;

	GET_REFLECTION_OBJECT_PTR(fptr);

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "*", &params, &num_args) == FAILURE) {
		return;
	}

	fci.size = sizeof(fci);
	ZVAL_UNDEF(&fci.function_name);
	fci.object = NULL;
	fci.retval = &retval;
	fci.param_count = num_args;
	fci.params = params;
	fci.no_separation = 1;

	fcc.function_handler = fptr;
	fcc.called_scope = NULL;
	fcc.object = NULL;

	if (!Z_ISUNDEF(intern->obj)) {
		Z_OBJ_HT(intern->obj)->get_closure(
			&intern->obj, &fcc.called_scope, &fcc.function_handler, &fcc.object);
	}

	result = gear_call_function(&fci, &fcc);

	if (result == FAILURE) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Invocation of function %s() failed", ZSTR_VAL(fptr->common.function_name));
		return;
	}

	if (Z_TYPE(retval) != IS_UNDEF) {
		if (Z_ISREF(retval)) {
			gear_unwrap_reference(&retval);
		}
		ZVAL_COPY_VALUE(return_value, &retval);
	}
}
/* }}} */

/* {{{ proto public mixed ReflectionFunction::invokeArgs(array args)
   Invokes the function and pass its arguments as array. */
GEAR_METHOD(reflection_function, invokeArgs)
{
	zval retval;
	zval *params, *val;
	int result;
	int i, argc;
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;
	reflection_object *intern;
	gear_function *fptr;
	zval *param_array;

	GET_REFLECTION_OBJECT_PTR(fptr);

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "a", &param_array) == FAILURE) {
		return;
	}

	argc = gear_hash_num_elements(Z_ARRVAL_P(param_array));

	params = safe_emalloc(sizeof(zval), argc, 0);
	argc = 0;
	GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(param_array), val) {
		ZVAL_COPY(&params[argc], val);
		argc++;
	} GEAR_HASH_FOREACH_END();

	fci.size = sizeof(fci);
	ZVAL_UNDEF(&fci.function_name);
	fci.object = NULL;
	fci.retval = &retval;
	fci.param_count = argc;
	fci.params = params;
	fci.no_separation = 1;

	fcc.function_handler = fptr;
	fcc.called_scope = NULL;
	fcc.object = NULL;

	if (!Z_ISUNDEF(intern->obj)) {
		Z_OBJ_HT(intern->obj)->get_closure(
			&intern->obj, &fcc.called_scope, &fcc.function_handler, &fcc.object);
	}

	result = gear_call_function(&fci, &fcc);

	for (i = 0; i < argc; i++) {
		zval_ptr_dtor(&params[i]);
	}
	efree(params);

	if (result == FAILURE) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Invocation of function %s() failed", ZSTR_VAL(fptr->common.function_name));
		return;
	}

	if (Z_TYPE(retval) != IS_UNDEF) {
		if (Z_ISREF(retval)) {
			gear_unwrap_reference(&retval);
		}
		ZVAL_COPY_VALUE(return_value, &retval);
	}
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::returnsReference()
   Gets whether this function returns a reference */
GEAR_METHOD(reflection_function, returnsReference)
{
	reflection_object *intern;
	gear_function *fptr;

	GET_REFLECTION_OBJECT_PTR(fptr);

	RETURN_BOOL((fptr->op_array.fn_flags & GEAR_ACC_RETURN_REFERENCE) != 0);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::getNumberOfParameters()
   Gets the number of parameters */
GEAR_METHOD(reflection_function, getNumberOfParameters)
{
	reflection_object *intern;
	gear_function *fptr;
	uint32_t num_args;

	GET_REFLECTION_OBJECT_PTR(fptr);

	num_args = fptr->common.num_args;
	if (fptr->common.fn_flags & GEAR_ACC_VARIADIC) {
		num_args++;
	}

	RETURN_LONG(num_args);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::getNumberOfRequiredParameters()
   Gets the number of required parameters */
GEAR_METHOD(reflection_function, getNumberOfRequiredParameters)
{
	reflection_object *intern;
	gear_function *fptr;

	GET_REFLECTION_OBJECT_PTR(fptr);

	RETURN_LONG(fptr->common.required_num_args);
}
/* }}} */

/* {{{ proto public ReflectionParameter[] ReflectionFunction::getParameters()
   Returns an array of parameter objects for this function */
GEAR_METHOD(reflection_function, getParameters)
{
	reflection_object *intern;
	gear_function *fptr;
	uint32_t i, num_args;
	struct _gear_arg_info *arg_info;

	GET_REFLECTION_OBJECT_PTR(fptr);

	arg_info= fptr->common.arg_info;
	num_args = fptr->common.num_args;
	if (fptr->common.fn_flags & GEAR_ACC_VARIADIC) {
		num_args++;
	}

	if (!num_args) {
		ZVAL_EMPTY_ARRAY(return_value);
		return;
	}

	array_init(return_value);
	for (i = 0; i < num_args; i++) {
		zval parameter;

		reflection_parameter_factory(
			_copy_function(fptr),
			Z_ISUNDEF(intern->obj) ? NULL : &intern->obj,
			arg_info,
			i,
			i < fptr->common.required_num_args,
			&parameter
		);
		add_next_index_zval(return_value, &parameter);

		arg_info++;
	}
}
/* }}} */

/* {{{ proto public ReflectionExtension|NULL ReflectionFunction::getExtension()
   Returns NULL or the extension the function belongs to */
GEAR_METHOD(reflection_function, getExtension)
{
	reflection_object *intern;
	gear_function *fptr;
	gear_internal_function *internal;

	GET_REFLECTION_OBJECT_PTR(fptr);

	if (fptr->type != GEAR_INTERNAL_FUNCTION) {
		RETURN_NULL();
	}

	internal = (gear_internal_function *)fptr;
	if (internal->cAPI) {
		reflection_extension_factory(return_value, internal->cAPI->name);
	} else {
		RETURN_NULL();
	}
}
/* }}} */

/* {{{ proto public string|false ReflectionFunction::getExtensionName()
   Returns false or the name of the extension the function belongs to */
GEAR_METHOD(reflection_function, getExtensionName)
{
	reflection_object *intern;
	gear_function *fptr;
	gear_internal_function *internal;

	GET_REFLECTION_OBJECT_PTR(fptr);

	if (fptr->type != GEAR_INTERNAL_FUNCTION) {
		RETURN_FALSE;
	}

	internal = (gear_internal_function *)fptr;
	if (internal->cAPI) {
		RETURN_STRING(internal->cAPI->name);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto public void ReflectionGenerator::__construct(object Generator) */
GEAR_METHOD(reflection_generator, __construct)
{
	zval *generator, *object;
	reflection_object *intern;
	gear_execute_data *ex;

	object = getThis();
	intern = Z_REFLECTION_P(object);

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "O", &generator, gear_ce_generator) == FAILURE) {
		return;
	}

	ex = ((gear_generator *) Z_OBJ_P(generator))->execute_data;
	if (!ex) {
		_DO_THROW("Cannot create ReflectionGenerator based on a terminated Generator");
		return;
	}

	intern->ref_type = REF_TYPE_GENERATOR;
	ZVAL_COPY(&intern->obj, generator);
	intern->ce = gear_ce_generator;
}
/* }}} */

#define REFLECTION_CHECK_VALID_GENERATOR(ex) \
	if (!ex) { \
		_DO_THROW("Cannot fetch information from a terminated Generator"); \
		return; \
	}

/* {{{ proto public array ReflectionGenerator::getTrace($options = DEBUG_BACKTRACE_PROVIDE_OBJECT) */
GEAR_METHOD(reflection_generator, getTrace)
{
	gear_long options = DEBUG_BACKTRACE_PROVIDE_OBJECT;
	gear_generator *generator = (gear_generator *) Z_OBJ(Z_REFLECTION_P(getThis())->obj);
	gear_generator *root_generator;
	gear_execute_data *ex_backup = EG(current_execute_data);
	gear_execute_data *ex = generator->execute_data;
	gear_execute_data *root_prev = NULL, *cur_prev;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|l", &options) == FAILURE) {
		return;
	}

	REFLECTION_CHECK_VALID_GENERATOR(ex)

	root_generator = gear_generator_get_current(generator);

	cur_prev = generator->execute_data->prev_execute_data;
	if (generator == root_generator) {
		generator->execute_data->prev_execute_data = NULL;
	} else {
		root_prev = root_generator->execute_data->prev_execute_data;
		generator->execute_fake.prev_execute_data = NULL;
		root_generator->execute_data->prev_execute_data = &generator->execute_fake;
	}

	EG(current_execute_data) = root_generator->execute_data;
	gear_fetch_debug_backtrace(return_value, 0, options, 0);
	EG(current_execute_data) = ex_backup;

	root_generator->execute_data->prev_execute_data = root_prev;
	generator->execute_data->prev_execute_data = cur_prev;
}
/* }}} */

/* {{{ proto public int ReflectionGenerator::getExecutingLine() */
GEAR_METHOD(reflection_generator, getExecutingLine)
{
	gear_generator *generator = (gear_generator *) Z_OBJ(Z_REFLECTION_P(getThis())->obj);
	gear_execute_data *ex = generator->execute_data;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	REFLECTION_CHECK_VALID_GENERATOR(ex)

	ZVAL_LONG(return_value, ex->opline->lineno);
}
/* }}} */

/* {{{ proto public string ReflectionGenerator::getExecutingFile() */
GEAR_METHOD(reflection_generator, getExecutingFile)
{
	gear_generator *generator = (gear_generator *) Z_OBJ(Z_REFLECTION_P(getThis())->obj);
	gear_execute_data *ex = generator->execute_data;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	REFLECTION_CHECK_VALID_GENERATOR(ex)

	ZVAL_STR_COPY(return_value, ex->func->op_array.filename);
}
/* }}} */

/* {{{ proto public ReflectionFunctionAbstract ReflectionGenerator::getFunction() */
GEAR_METHOD(reflection_generator, getFunction)
{
	gear_generator *generator = (gear_generator *) Z_OBJ(Z_REFLECTION_P(getThis())->obj);
	gear_execute_data *ex = generator->execute_data;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	REFLECTION_CHECK_VALID_GENERATOR(ex)

	if (ex->func->common.fn_flags & GEAR_ACC_CLOSURE) {
		zval closure;
		ZVAL_OBJ(&closure, GEAR_CLOSURE_OBJECT(ex->func));
		reflection_function_factory(ex->func, &closure, return_value);
	} else if (ex->func->op_array.scope) {
		reflection_method_factory(ex->func->op_array.scope, ex->func, NULL, return_value);
	} else {
		reflection_function_factory(ex->func, NULL, return_value);
	}
}
/* }}} */

/* {{{ proto public object ReflectionGenerator::getThis() */
GEAR_METHOD(reflection_generator, getThis)
{
	gear_generator *generator = (gear_generator *) Z_OBJ(Z_REFLECTION_P(getThis())->obj);
	gear_execute_data *ex = generator->execute_data;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	REFLECTION_CHECK_VALID_GENERATOR(ex)

	if (Z_TYPE(ex->This) == IS_OBJECT) {
		ZVAL_COPY(return_value, &ex->This);
	} else {
		ZVAL_NULL(return_value);
	}
}
/* }}} */

/* {{{ proto public Generator ReflectionGenerator::getExecutingGenerator() */
GEAR_METHOD(reflection_generator, getExecutingGenerator)
{
	gear_generator *generator = (gear_generator *) Z_OBJ(Z_REFLECTION_P(getThis())->obj);
	gear_execute_data *ex = generator->execute_data;
	gear_generator *current;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	REFLECTION_CHECK_VALID_GENERATOR(ex)

	current = gear_generator_get_current(generator);
	GC_ADDREF(&current->std);

	ZVAL_OBJ(return_value, (gear_object *) current);
}
/* }}} */

/* {{{ proto public static mixed ReflectionParameter::export(mixed function, mixed parameter [, bool return]) throws ReflectionException
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_parameter, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_parameter_ptr, 2);
}
/* }}} */

/* {{{ proto public void ReflectionParameter::__construct(mixed function, mixed parameter)
   Constructor. Throws an Exception in case the given method does not exist */
GEAR_METHOD(reflection_parameter, __construct)
{
	parameter_reference *ref;
	zval *reference, *parameter;
	zval *object;
	zval name;
	reflection_object *intern;
	gear_function *fptr;
	struct _gear_arg_info *arg_info;
	int position;
	uint32_t num_args;
	gear_class_entry *ce = NULL;
	gear_bool is_closure = 0;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "zz", &reference, &parameter) == FAILURE) {
		return;
	}

	object = getThis();
	intern = Z_REFLECTION_P(object);

	/* First, find the function */
	switch (Z_TYPE_P(reference)) {
		case IS_STRING: {
				size_t lcname_len;
				char *lcname;

				lcname_len = Z_STRLEN_P(reference);
				lcname = gear_str_tolower_dup(Z_STRVAL_P(reference), lcname_len);
				if ((fptr = gear_hash_str_find_ptr(EG(function_table), lcname, lcname_len)) == NULL) {
					efree(lcname);
					gear_throw_exception_ex(reflection_exception_ptr, 0,
						"Function %s() does not exist", Z_STRVAL_P(reference));
					return;
				}
				efree(lcname);
			}
			ce = fptr->common.scope;
			break;

		case IS_ARRAY: {
				zval *classref;
				zval *method;
				size_t lcname_len;
				char *lcname;

				if (((classref =gear_hash_index_find(Z_ARRVAL_P(reference), 0)) == NULL)
					|| ((method = gear_hash_index_find(Z_ARRVAL_P(reference), 1)) == NULL))
				{
					_DO_THROW("Expected array($object, $method) or array($classname, $method)");
					/* returns out of this function */
				}

				if (Z_TYPE_P(classref) == IS_OBJECT) {
					ce = Z_OBJCE_P(classref);
				} else {
					convert_to_string_ex(classref);
					if ((ce = gear_lookup_class(Z_STR_P(classref))) == NULL) {
						gear_throw_exception_ex(reflection_exception_ptr, 0,
								"Class %s does not exist", Z_STRVAL_P(classref));
						return;
					}
				}

				convert_to_string_ex(method);
				lcname_len = Z_STRLEN_P(method);
				lcname = gear_str_tolower_dup(Z_STRVAL_P(method), lcname_len);
				if (ce == gear_ce_closure && Z_TYPE_P(classref) == IS_OBJECT
					&& (lcname_len == sizeof(GEAR_INVOKE_FUNC_NAME)-1)
					&& memcmp(lcname, GEAR_INVOKE_FUNC_NAME, sizeof(GEAR_INVOKE_FUNC_NAME)-1) == 0
					&& (fptr = gear_get_closure_invoke_method(Z_OBJ_P(classref))) != NULL)
				{
					/* nothing to do. don't set is_closure since is the invoke handler,
					   not the closure itself */
				} else if ((fptr = gear_hash_str_find_ptr(&ce->function_table, lcname, lcname_len)) == NULL) {
					efree(lcname);
					gear_throw_exception_ex(reflection_exception_ptr, 0,
						"Method %s::%s() does not exist", ZSTR_VAL(ce->name), Z_STRVAL_P(method));
					return;
				}
				efree(lcname);
			}
			break;

		case IS_OBJECT: {
				ce = Z_OBJCE_P(reference);

				if (instanceof_function(ce, gear_ce_closure)) {
					fptr = (gear_function *)gear_get_closure_method_def(reference);
					Z_ADDREF_P(reference);
					is_closure = 1;
				} else if ((fptr = gear_hash_str_find_ptr(&ce->function_table, GEAR_INVOKE_FUNC_NAME, sizeof(GEAR_INVOKE_FUNC_NAME))) == NULL) {
					gear_throw_exception_ex(reflection_exception_ptr, 0,
						"Method %s::%s() does not exist", ZSTR_VAL(ce->name), GEAR_INVOKE_FUNC_NAME);
					return;
				}
			}
			break;

		default:
			_DO_THROW("The parameter class is expected to be either a string, an array(class, method) or a callable object");
			/* returns out of this function */
	}

	/* Now, search for the parameter */
	arg_info = fptr->common.arg_info;
	num_args = fptr->common.num_args;
	if (fptr->common.fn_flags & GEAR_ACC_VARIADIC) {
		num_args++;
	}
	if (Z_TYPE_P(parameter) == IS_LONG) {
		position= (int)Z_LVAL_P(parameter);
		if (position < 0 || (uint32_t)position >= num_args) {
			if (fptr->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) {
				if (fptr->type != GEAR_OVERLOADED_FUNCTION) {
					gear_string_release_ex(fptr->common.function_name, 0);
				}
				gear_free_trampoline(fptr);
			}
			if (is_closure) {
				zval_ptr_dtor(reference);
			}
			_DO_THROW("The parameter specified by its offset could not be found");
			/* returns out of this function */
		}
	} else {
		uint32_t i;

		position= -1;
		convert_to_string_ex(parameter);
		if (fptr->type == GEAR_INTERNAL_FUNCTION &&
		    !(fptr->common.fn_flags & GEAR_ACC_USER_ARG_INFO)) {
			for (i = 0; i < num_args; i++) {
				if (arg_info[i].name) {
					if (strcmp(((gear_internal_arg_info*)arg_info)[i].name, Z_STRVAL_P(parameter)) == 0) {
						position= i;
						break;
					}

				}
			}
		} else {
			for (i = 0; i < num_args; i++) {
				if (arg_info[i].name) {
					if (strcmp(ZSTR_VAL(arg_info[i].name), Z_STRVAL_P(parameter)) == 0) {
						position= i;
						break;
					}
				}
			}
		}
		if (position == -1) {
			if (fptr->common.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE) {
				if (fptr->type != GEAR_OVERLOADED_FUNCTION) {
					gear_string_release_ex(fptr->common.function_name, 0);
				}
				gear_free_trampoline(fptr);
			}
			if (is_closure) {
				zval_ptr_dtor(reference);
			}
			_DO_THROW("The parameter specified by its name could not be found");
			/* returns out of this function */
		}
	}

	if (arg_info[position].name) {
		if (fptr->type == GEAR_INTERNAL_FUNCTION &&
		    !(fptr->common.fn_flags & GEAR_ACC_USER_ARG_INFO)) {
			ZVAL_STRING(&name, ((gear_internal_arg_info*)arg_info)[position].name);
		} else {
			ZVAL_STR_COPY(&name, arg_info[position].name);
		}
	} else {
		ZVAL_NULL(&name);
	}
	reflection_update_property_name(object, &name);

	ref = (parameter_reference*) emalloc(sizeof(parameter_reference));
	ref->arg_info = &arg_info[position];
	ref->offset = (uint32_t)position;
	ref->required = position < fptr->common.required_num_args;
	ref->fptr = fptr;
	/* TODO: copy fptr */
	intern->ptr = ref;
	intern->ref_type = REF_TYPE_PARAMETER;
	intern->ce = ce;
	if (reference && is_closure) {
		ZVAL_COPY_VALUE(&intern->obj, reference);
	}
}
/* }}} */

/* {{{ proto public string ReflectionParameter::__toString()
   Returns a string representation */
GEAR_METHOD(reflection_parameter, __toString)
{
	reflection_object *intern;
	parameter_reference *param;
	smart_str str = {0};

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);
	_parameter_string(&str, param->fptr, param->arg_info, param->offset, param->required, "");
	RETURN_STR(smart_str_extract(&str));
}

/* }}} */

/* {{{ proto public string ReflectionParameter::getName()
   Returns this parameters's name */
GEAR_METHOD(reflection_parameter, getName)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	_default_get_name(getThis(), return_value);
}
/* }}} */

/* {{{ proto public ReflectionFunction ReflectionParameter::getDeclaringFunction()
   Returns the ReflectionFunction for the function of this parameter */
GEAR_METHOD(reflection_parameter, getDeclaringFunction)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	if (!param->fptr->common.scope) {
		reflection_function_factory(_copy_function(param->fptr), Z_ISUNDEF(intern->obj)? NULL : &intern->obj, return_value);
	} else {
		reflection_method_factory(param->fptr->common.scope, _copy_function(param->fptr), Z_ISUNDEF(intern->obj)? NULL : &intern->obj, return_value);
	}
}
/* }}} */

/* {{{ proto public ReflectionClass|NULL ReflectionParameter::getDeclaringClass()
   Returns in which class this parameter is defined (not the type of the parameter) */
GEAR_METHOD(reflection_parameter, getDeclaringClass)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	if (param->fptr->common.scope) {
		gear_reflection_class_factory(param->fptr->common.scope, return_value);
	}
}
/* }}} */

/* {{{ proto public ReflectionClass|NULL ReflectionParameter::getClass()
   Returns this parameters's class hint or NULL if there is none */
GEAR_METHOD(reflection_parameter, getClass)
{
	reflection_object *intern;
	parameter_reference *param;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	if (GEAR_TYPE_IS_CLASS(param->arg_info->type)) {
		/* Class name is stored as a string, we might also get "self" or "parent"
		 * - For "self", simply use the function scope. If scope is NULL then
		 *   the function is global and thus self does not make any sense
		 *
		 * - For "parent", use the function scope's parent. If scope is NULL then
		 *   the function is global and thus parent does not make any sense.
		 *   If the parent is NULL then the class does not extend anything and
		 *   thus parent does not make any sense, either.
		 *
		 * TODO: Think about moving these checks to the compiler or some sort of
		 * lint-mode.
		 */
		gear_string *class_name;

		class_name = GEAR_TYPE_NAME(param->arg_info->type);
		if (0 == gear_binary_strcasecmp(ZSTR_VAL(class_name), ZSTR_LEN(class_name), "self", sizeof("self")- 1)) {
			ce = param->fptr->common.scope;
			if (!ce) {
				gear_throw_exception_ex(reflection_exception_ptr, 0,
					"Parameter uses 'self' as type hint but function is not a class member!");
				return;
			}
		} else if (0 == gear_binary_strcasecmp(ZSTR_VAL(class_name), ZSTR_LEN(class_name), "parent", sizeof("parent")- 1)) {
			ce = param->fptr->common.scope;
			if (!ce) {
				gear_throw_exception_ex(reflection_exception_ptr, 0,
					"Parameter uses 'parent' as type hint but function is not a class member!");
				return;
			}
			if (!ce->parent) {
				gear_throw_exception_ex(reflection_exception_ptr, 0,
					"Parameter uses 'parent' as type hint although class does not have a parent!");
				return;
			}
			ce = ce->parent;
		} else {
			ce = gear_lookup_class(class_name);
			if (!ce) {
				gear_throw_exception_ex(reflection_exception_ptr, 0,
					"Class %s does not exist", ZSTR_VAL(class_name));
				return;
			}
		}
		gear_reflection_class_factory(ce, return_value);
	}
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::hasType()
   Returns whether parameter has a type */
GEAR_METHOD(reflection_parameter, hasType)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(GEAR_TYPE_IS_SET(param->arg_info->type));
}
/* }}} */

/* {{{ proto public ReflectionType ReflectionParameter::getType()
   Returns the type associated with the parameter */
GEAR_METHOD(reflection_parameter, getType)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	if (!GEAR_TYPE_IS_SET(param->arg_info->type)) {
		RETURN_NULL();
	}
	reflection_type_factory(_copy_function(param->fptr), Z_ISUNDEF(intern->obj)? NULL : &intern->obj, param->arg_info, return_value);
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::isArray()
   Returns whether parameter MUST be an array */
GEAR_METHOD(reflection_parameter, isArray)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(GEAR_TYPE_CODE(param->arg_info->type) == IS_ARRAY);
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::isCallable()
   Returns whether parameter MUST be callable */
GEAR_METHOD(reflection_parameter, isCallable)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(GEAR_TYPE_CODE(param->arg_info->type) == IS_CALLABLE);
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::allowsNull()
   Returns whether NULL is allowed as this parameters's value */
GEAR_METHOD(reflection_parameter, allowsNull)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(GEAR_TYPE_ALLOW_NULL(param->arg_info->type));
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::isPassedByReference()
   Returns whether this parameters is passed to by reference */
GEAR_METHOD(reflection_parameter, isPassedByReference)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(param->arg_info->pass_by_reference);
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::canBePassedByValue()
   Returns whether this parameter can be passed by value */
GEAR_METHOD(reflection_parameter, canBePassedByValue)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	/* true if it's GEAR_SEND_BY_VAL or GEAR_SEND_PREFER_REF */
	RETVAL_BOOL(param->arg_info->pass_by_reference != GEAR_SEND_BY_REF);
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::getPosition()
   Returns whether this parameter is an optional parameter */
GEAR_METHOD(reflection_parameter, getPosition)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_LONG(param->offset);
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::isOptional()
   Returns whether this parameter is an optional parameter */
GEAR_METHOD(reflection_parameter, isOptional)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(!param->required);
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::isDefaultValueAvailable()
   Returns whether the default value of this parameter is available */
GEAR_METHOD(reflection_parameter, isDefaultValueAvailable)
{
	reflection_object *intern;
	parameter_reference *param;
	gear_op *precv;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	if (param->fptr->type != GEAR_USER_FUNCTION)
	{
		RETURN_FALSE;
	}

	precv = _get_recv_op((gear_op_array*)param->fptr, param->offset);
	if (!precv || precv->opcode != GEAR_RECV_INIT || precv->op2_type == IS_UNUSED) {
		RETURN_FALSE;
	}
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::getDefaultValue()
   Returns the default value of this parameter or throws an exception */
GEAR_METHOD(reflection_parameter, getDefaultValue)
{
	parameter_reference *param;
	gear_op *precv;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	param = _reflection_param_get_default_param(INTERNAL_FUNCTION_PARAM_PASSTHRU);
	if (!param) {
		return;
	}

	precv = _reflection_param_get_default_precv(INTERNAL_FUNCTION_PARAM_PASSTHRU, param);
	if (!precv) {
		return;
	}

	ZVAL_COPY(return_value, RT_CONSTANT(precv, precv->op2));
	if (Z_TYPE_P(return_value) == IS_CONSTANT_AST) {
		zval_update_constant_ex(return_value, param->fptr->common.scope);
	}
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::isDefaultValueConstant()
   Returns whether the default value of this parameter is constant */
GEAR_METHOD(reflection_parameter, isDefaultValueConstant)
{
	gear_op *precv;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	param = _reflection_param_get_default_param(INTERNAL_FUNCTION_PARAM_PASSTHRU);
	if (!param) {
		RETURN_FALSE;
	}

	precv = _reflection_param_get_default_precv(INTERNAL_FUNCTION_PARAM_PASSTHRU, param);
	if (precv && Z_TYPE_P(RT_CONSTANT(precv, precv->op2)) == IS_CONSTANT_AST) {
		gear_ast *ast = Z_ASTVAL_P(RT_CONSTANT(precv, precv->op2));

		if (ast->kind == GEAR_AST_CONSTANT
		 || ast->kind == GEAR_AST_CONSTANT_CLASS) {
			RETURN_TRUE;
		}
	}

	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public mixed ReflectionParameter::getDefaultValueConstantName()
   Returns the default value's constant name if default value is constant or null */
GEAR_METHOD(reflection_parameter, getDefaultValueConstantName)
{
	gear_op *precv;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	param = _reflection_param_get_default_param(INTERNAL_FUNCTION_PARAM_PASSTHRU);
	if (!param) {
		return;
	}

	precv = _reflection_param_get_default_precv(INTERNAL_FUNCTION_PARAM_PASSTHRU, param);
	if (precv && Z_TYPE_P(RT_CONSTANT(precv, precv->op2)) == IS_CONSTANT_AST) {
		gear_ast *ast = Z_ASTVAL_P(RT_CONSTANT(precv, precv->op2));

		if (ast->kind == GEAR_AST_CONSTANT) {
			RETURN_STR_COPY(gear_ast_get_constant_name(ast));
		} else if (ast->kind == GEAR_AST_CONSTANT_CLASS) {
			RETURN_STRINGL("__CLASS__", sizeof("__CLASS__")-1);
		}
	}
}
/* }}} */

/* {{{ proto public bool ReflectionParameter::isVariadic()
   Returns whether this parameter is a variadic parameter */
GEAR_METHOD(reflection_parameter, isVariadic)
{
	reflection_object *intern;
	parameter_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(param->arg_info->is_variadic);
}
/* }}} */

/* {{{ proto public bool ReflectionType::allowsNull()
  Returns whether parameter MAY be null */
GEAR_METHOD(reflection_type, allowsNull)
{
	reflection_object *intern;
	type_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(GEAR_TYPE_ALLOW_NULL(param->arg_info->type));
}
/* }}} */

/* {{{ proto public bool ReflectionType::isBuiltin()
  Returns whether parameter is a builtin type */
GEAR_METHOD(reflection_type, isBuiltin)
{
	reflection_object *intern;
	type_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETVAL_BOOL(GEAR_TYPE_IS_CODE(param->arg_info->type));
}
/* }}} */

/* {{{ reflection_type_name */
static gear_string *reflection_type_name(type_reference *param) {
	if (GEAR_TYPE_IS_CLASS(param->arg_info->type)) {
		return gear_string_copy(GEAR_TYPE_NAME(param->arg_info->type));
	} else {
		char *name = gear_get_type_by_const(GEAR_TYPE_CODE(param->arg_info->type));
		return gear_string_init(name, strlen(name), 0);
	}
}
/* }}} */

/* {{{ proto public string ReflectionType::__toString()
   Return the text of the type hint */
GEAR_METHOD(reflection_type, __toString)
{
	reflection_object *intern;
	type_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETURN_STR(reflection_type_name(param));
}
/* }}} */

/* {{{ proto public string ReflectionNamedType::getName()
 Return the text of the type hint */
GEAR_METHOD(reflection_named_type, getName)
{
	reflection_object *intern;
	type_reference *param;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(param);

	RETURN_STR(reflection_type_name(param));
}
/* }}} */

/* {{{ proto public static mixed ReflectionMethod::export(mixed class, string name [, bool return]) throws ReflectionException
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_method, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_method_ptr, 2);
}
/* }}} */

/* {{{ proto public void ReflectionMethod::__construct(mixed class_or_method [, string name])
   Constructor. Throws an Exception in case the given method does not exist */
GEAR_METHOD(reflection_method, __construct)
{
	zval name, *classname;
	zval *object, *orig_obj;
	reflection_object *intern;
	char *lcname;
	gear_class_entry *ce;
	gear_function *mptr;
	char *name_str, *tmp;
	size_t name_len, tmp_len;
	zval ztmp;

	if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, GEAR_NUM_ARGS(), "zs", &classname, &name_str, &name_len) == FAILURE) {
		if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "s", &name_str, &name_len) == FAILURE) {
			return;
		}

		if ((tmp = strstr(name_str, "::")) == NULL) {
			gear_throw_exception_ex(reflection_exception_ptr, 0,
				"Invalid method name %s", name_str);
			return;
		}
		classname = &ztmp;
		tmp_len = tmp - name_str;
		ZVAL_STRINGL(classname, name_str, tmp_len);
		name_len = name_len - (tmp_len + 2);
		name_str = tmp + 2;
		orig_obj = NULL;
	} else if (Z_TYPE_P(classname) == IS_OBJECT) {
		orig_obj = classname;
	} else {
		orig_obj = NULL;
	}

	object = getThis();
	intern = Z_REFLECTION_P(object);

	/* Find the class entry */
	switch (Z_TYPE_P(classname)) {
		case IS_STRING:
			if ((ce = gear_lookup_class(Z_STR_P(classname))) == NULL) {
				if (!EG(exception)) {
					gear_throw_exception_ex(reflection_exception_ptr, 0,
							"Class %s does not exist", Z_STRVAL_P(classname));
				}
				if (classname == &ztmp) {
					zval_ptr_dtor_str(&ztmp);
				}
				return;
			}
			break;

		case IS_OBJECT:
			ce = Z_OBJCE_P(classname);
			break;

		default:
			if (classname == &ztmp) {
				zval_ptr_dtor_str(&ztmp);
			}
			_DO_THROW("The parameter class is expected to be either a string or an object");
			/* returns out of this function */
	}

	if (classname == &ztmp) {
		zval_ptr_dtor_str(&ztmp);
	}

	lcname = gear_str_tolower_dup(name_str, name_len);

	if (ce == gear_ce_closure && orig_obj && (name_len == sizeof(GEAR_INVOKE_FUNC_NAME)-1)
		&& memcmp(lcname, GEAR_INVOKE_FUNC_NAME, sizeof(GEAR_INVOKE_FUNC_NAME)-1) == 0
		&& (mptr = gear_get_closure_invoke_method(Z_OBJ_P(orig_obj))) != NULL)
	{
		/* do nothing, mptr already set */
	} else if ((mptr = gear_hash_str_find_ptr(&ce->function_table, lcname, name_len)) == NULL) {
		efree(lcname);
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Method %s::%s() does not exist", ZSTR_VAL(ce->name), name_str);
		return;
	}
	efree(lcname);

	ZVAL_STR_COPY(&name, mptr->common.scope->name);
	reflection_update_property_class(object, &name);
	ZVAL_STR_COPY(&name, mptr->common.function_name);
	reflection_update_property_name(object, &name);
	intern->ptr = mptr;
	intern->ref_type = REF_TYPE_FUNCTION;
	intern->ce = ce;
}
/* }}} */

/* {{{ proto public string ReflectionMethod::__toString()
   Returns a string representation */
GEAR_METHOD(reflection_method, __toString)
{
	reflection_object *intern;
	gear_function *mptr;
	smart_str str = {0};

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(mptr);
	_function_string(&str, mptr, intern->ce, "");
	RETURN_STR(smart_str_extract(&str));
}
/* }}} */

/* {{{ proto public mixed ReflectionMethod::getClosure([mixed object])
   Invokes the function */
GEAR_METHOD(reflection_method, getClosure)
{
	reflection_object *intern;
	zval *obj;
	gear_function *mptr;

	GET_REFLECTION_OBJECT_PTR(mptr);

	if (mptr->common.fn_flags & GEAR_ACC_STATIC)  {
		gear_create_fake_closure(return_value, mptr, mptr->common.scope, mptr->common.scope, NULL);
	} else {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "o", &obj) == FAILURE) {
			return;
		}

		if (!instanceof_function(Z_OBJCE_P(obj), mptr->common.scope)) {
			_DO_THROW("Given object is not an instance of the class this method was declared in");
			/* Returns from this function */
		}

		/* This is an original closure object and __invoke is to be called. */
		if (Z_OBJCE_P(obj) == gear_ce_closure &&
			(mptr->internal_function.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE))
		{
			ZVAL_COPY(return_value, obj);
		} else {
			gear_create_fake_closure(return_value, mptr, mptr->common.scope, Z_OBJCE_P(obj), obj);
		}
	}
}
/* }}} */

/* {{{ reflection_method_invoke */
static void reflection_method_invoke(INTERNAL_FUNCTION_PARAMETERS, int variadic)
{
	zval retval;
	zval *params = NULL, *val, *object;
	reflection_object *intern;
	gear_function *mptr;
	int i, argc = 0, result;
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;
	gear_class_entry *obj_ce;
	zval *param_array;

	GET_REFLECTION_OBJECT_PTR(mptr);

	if (mptr->common.fn_flags & GEAR_ACC_ABSTRACT) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Trying to invoke abstract method %s::%s()",
			ZSTR_VAL(mptr->common.scope->name), ZSTR_VAL(mptr->common.function_name));
		return;
	}

	if (!(mptr->common.fn_flags & GEAR_ACC_PUBLIC) && intern->ignore_visibility == 0) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Trying to invoke %s method %s::%s() from scope %s",
			mptr->common.fn_flags & GEAR_ACC_PROTECTED ? "protected" : "private",
			ZSTR_VAL(mptr->common.scope->name), ZSTR_VAL(mptr->common.function_name),
			ZSTR_VAL(Z_OBJCE_P(getThis())->name));
		return;
	}

	if (variadic) {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "o!*", &object, &params, &argc) == FAILURE) {
			return;
		}
	} else {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "o!a", &object, &param_array) == FAILURE) {
			return;
		}

		argc = gear_hash_num_elements(Z_ARRVAL_P(param_array));

		params = safe_emalloc(sizeof(zval), argc, 0);
		argc = 0;
		GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(param_array), val) {
			ZVAL_COPY(&params[argc], val);
			argc++;
		} GEAR_HASH_FOREACH_END();
	}

	/* In case this is a static method, we should'nt pass an object_ptr
	 * (which is used as calling context aka $this). We can thus ignore the
	 * first parameter.
	 *
	 * Else, we verify that the given object is an instance of the class.
	 */
	if (mptr->common.fn_flags & GEAR_ACC_STATIC) {
		object = NULL;
		obj_ce = mptr->common.scope;
	} else {
		if (!object) {
			gear_throw_exception_ex(reflection_exception_ptr, 0,
				"Trying to invoke non static method %s::%s() without an object",
				ZSTR_VAL(mptr->common.scope->name), ZSTR_VAL(mptr->common.function_name));
			return;
		}

		obj_ce = Z_OBJCE_P(object);

		if (!instanceof_function(obj_ce, mptr->common.scope)) {
			if (!variadic) {
				efree(params);
			}
			_DO_THROW("Given object is not an instance of the class this method was declared in");
			/* Returns from this function */
		}
	}

	fci.size = sizeof(fci);
	ZVAL_UNDEF(&fci.function_name);
	fci.object = object ? Z_OBJ_P(object) : NULL;
	fci.retval = &retval;
	fci.param_count = argc;
	fci.params = params;
	fci.no_separation = 1;

	fcc.function_handler = mptr;
	fcc.called_scope = intern->ce;
	fcc.object = object ? Z_OBJ_P(object) : NULL;

	/*
	 * Copy the gear_function when calling via handler (e.g. Closure::__invoke())
	 */
	if ((mptr->internal_function.fn_flags & GEAR_ACC_CALL_VIA_TRAMPOLINE)) {
		fcc.function_handler = _copy_function(mptr);
	}

	result = gear_call_function(&fci, &fcc);

	if (!variadic) {
		for (i = 0; i < argc; i++) {
			zval_ptr_dtor(&params[i]);
		}
		efree(params);
	}

	if (result == FAILURE) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Invocation of method %s::%s() failed", ZSTR_VAL(mptr->common.scope->name), ZSTR_VAL(mptr->common.function_name));
		return;
	}

	if (Z_TYPE(retval) != IS_UNDEF) {
		if (Z_ISREF(retval)) {
			gear_unwrap_reference(&retval);
		}
		ZVAL_COPY_VALUE(return_value, &retval);
	}
}
/* }}} */

/* {{{ proto public mixed ReflectionMethod::invoke(mixed object, mixed* args)
   Invokes the method. */
GEAR_METHOD(reflection_method, invoke)
{
	reflection_method_invoke(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto public mixed ReflectionMethod::invokeArgs(mixed object, array args)
   Invokes the function and pass its arguments as array. */
GEAR_METHOD(reflection_method, invokeArgs)
{
	reflection_method_invoke(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto public bool ReflectionMethod::isFinal()
   Returns whether this method is final */
GEAR_METHOD(reflection_method, isFinal)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_FINAL);
}
/* }}} */

/* {{{ proto public bool ReflectionMethod::isAbstract()
   Returns whether this method is abstract */
GEAR_METHOD(reflection_method, isAbstract)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_ABSTRACT);
}
/* }}} */

/* {{{ proto public bool ReflectionMethod::isPublic()
   Returns whether this method is public */
GEAR_METHOD(reflection_method, isPublic)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PUBLIC);
}
/* }}} */

/* {{{ proto public bool ReflectionMethod::isPrivate()
   Returns whether this method is private */
GEAR_METHOD(reflection_method, isPrivate)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PRIVATE);
}
/* }}} */

/* {{{ proto public bool ReflectionMethod::isProtected()
   Returns whether this method is protected */
GEAR_METHOD(reflection_method, isProtected)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PROTECTED);
}
/* }}} */

/* {{{ proto public bool ReflectionMethod::isStatic()
   Returns whether this method is static */
GEAR_METHOD(reflection_method, isStatic)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_STATIC);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::isDeprecated()
   Returns whether this function is deprecated */
GEAR_METHOD(reflection_function, isDeprecated)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_DEPRECATED);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::isGenerator()
   Returns whether this function is a generator */
GEAR_METHOD(reflection_function, isGenerator)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_GENERATOR);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::isVariadic()
   Returns whether this function is variadic */
GEAR_METHOD(reflection_function, isVariadic)
{
	_function_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_VARIADIC);
}
/* }}} */

/* {{{ proto public bool ReflectionFunction::inNamespace()
   Returns whether this function is defined in namespace */
GEAR_METHOD(reflection_function, inNamespace)
{
	zval *name;
	const char *backslash;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	if ((name = _default_load_name(getThis())) == NULL) {
		RETURN_FALSE;
	}
	if (Z_TYPE_P(name) == IS_STRING
		&& (backslash = gear_memrchr(Z_STRVAL_P(name), '\\', Z_STRLEN_P(name)))
		&& backslash > Z_STRVAL_P(name))
	{
		RETURN_TRUE;
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public string ReflectionFunction::getNamespaceName()
   Returns the name of namespace where this function is defined */
GEAR_METHOD(reflection_function, getNamespaceName)
{
	zval *name;
	const char *backslash;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	if ((name = _default_load_name(getThis())) == NULL) {
		RETURN_FALSE;
	}
	if (Z_TYPE_P(name) == IS_STRING
		&& (backslash = gear_memrchr(Z_STRVAL_P(name), '\\', Z_STRLEN_P(name)))
		&& backslash > Z_STRVAL_P(name))
	{
		RETURN_STRINGL(Z_STRVAL_P(name), backslash - Z_STRVAL_P(name));
	}
	RETURN_EMPTY_STRING();
}
/* }}} */

/* {{{ proto public string ReflectionFunction::getShortName()
   Returns the short name of the function (without namespace part) */
GEAR_METHOD(reflection_function, getShortName)
{
	zval *name;
	const char *backslash;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	if ((name = _default_load_name(getThis())) == NULL) {
		RETURN_FALSE;
	}
	if (Z_TYPE_P(name) == IS_STRING
		&& (backslash = gear_memrchr(Z_STRVAL_P(name), '\\', Z_STRLEN_P(name)))
		&& backslash > Z_STRVAL_P(name))
	{
		RETURN_STRINGL(backslash + 1, Z_STRLEN_P(name) - (backslash - Z_STRVAL_P(name) + 1));
	}
	ZVAL_COPY_DEREF(return_value, name);
}
/* }}} */

/* {{{ proto public bool ReflectionFunctionAbstract:hasReturnType()
   Return whether the function has a return type */
GEAR_METHOD(reflection_function, hasReturnType)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(fptr);

	RETVAL_BOOL(fptr->op_array.fn_flags & GEAR_ACC_HAS_RETURN_TYPE);
}
/* }}} */

/* {{{ proto public string ReflectionFunctionAbstract::getReturnType()
   Returns the return type associated with the function */
GEAR_METHOD(reflection_function, getReturnType)
{
	reflection_object *intern;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(fptr);

	if (!(fptr->op_array.fn_flags & GEAR_ACC_HAS_RETURN_TYPE)) {
		RETURN_NULL();
	}

	reflection_type_factory(_copy_function(fptr), Z_ISUNDEF(intern->obj)? NULL : &intern->obj, &fptr->common.arg_info[-1], return_value);
}
/* }}} */

/* {{{ proto public bool ReflectionMethod::isConstructor()
   Returns whether this method is the constructor */
GEAR_METHOD(reflection_method, isConstructor)
{
	reflection_object *intern;
	gear_function *mptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(mptr);
	/* we need to check if the ctor is the ctor of the class level we we
	 * looking at since we might be looking at an inherited old style ctor
	 * defined in base class. */
	RETURN_BOOL(mptr->common.fn_flags & GEAR_ACC_CTOR && intern->ce->constructor && intern->ce->constructor->common.scope == mptr->common.scope);
}
/* }}} */

/* {{{ proto public bool ReflectionMethod::isDestructor()
   Returns whether this method is static */
GEAR_METHOD(reflection_method, isDestructor)
{
	reflection_object *intern;
	gear_function *mptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(mptr);
	RETURN_BOOL(mptr->common.fn_flags & GEAR_ACC_DTOR);
}
/* }}} */

/* {{{ proto public int ReflectionMethod::getModifiers()
   Returns a bitfield of the access modifiers for this method */
GEAR_METHOD(reflection_method, getModifiers)
{
	reflection_object *intern;
	gear_function *mptr;
	uint32_t keep_flags = GEAR_ACC_PPP_MASK | GEAR_ACC_IMPLICIT_PUBLIC
		| GEAR_ACC_STATIC | GEAR_ACC_ABSTRACT | GEAR_ACC_FINAL;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(mptr);

	RETURN_LONG((mptr->common.fn_flags & keep_flags));
}
/* }}} */

/* {{{ proto public ReflectionClass ReflectionMethod::getDeclaringClass()
   Get the declaring class */
GEAR_METHOD(reflection_method, getDeclaringClass)
{
	reflection_object *intern;
	gear_function *mptr;

	GET_REFLECTION_OBJECT_PTR(mptr);

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	gear_reflection_class_factory(mptr->common.scope, return_value);
}
/* }}} */

/* {{{ proto public ReflectionClass ReflectionMethod::getPrototype()
   Get the prototype */
GEAR_METHOD(reflection_method, getPrototype)
{
	reflection_object *intern;
	gear_function *mptr;

	GET_REFLECTION_OBJECT_PTR(mptr);

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (!mptr->common.prototype) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Method %s::%s does not have a prototype", ZSTR_VAL(intern->ce->name), ZSTR_VAL(mptr->common.function_name));
		return;
	}

	reflection_method_factory(mptr->common.prototype->common.scope, mptr->common.prototype, NULL, return_value);
}
/* }}} */

/* {{{ proto public void ReflectionMethod::setAccessible(bool visible)
   Sets whether non-public methods can be invoked */
GEAR_METHOD(reflection_method, setAccessible)
{
	reflection_object *intern;
	gear_bool visible;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "b", &visible) == FAILURE) {
		return;
	}

	intern = Z_REFLECTION_P(getThis());

	intern->ignore_visibility = visible;
}
/* }}} */

/* {{{ proto public void ReflectionClassConstant::__construct(mixed class, string name)
   Constructor. Throws an Exception in case the given class constant does not exist */
GEAR_METHOD(reflection_class_constant, __construct)
{
	zval *classname, *object, name, cname;
	gear_string *constname;
	reflection_object *intern;
	gear_class_entry *ce;
	gear_class_constant *constant = NULL;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "zS", &classname, &constname) == FAILURE) {
		return;
	}

	object = getThis();
	intern = Z_REFLECTION_P(object);

	/* Find the class entry */
	switch (Z_TYPE_P(classname)) {
		case IS_STRING:
			if ((ce = gear_lookup_class(Z_STR_P(classname))) == NULL) {
				gear_throw_exception_ex(reflection_exception_ptr, 0,
						"Class %s does not exist", Z_STRVAL_P(classname));
				return;
			}
			break;

		case IS_OBJECT:
			ce = Z_OBJCE_P(classname);
			break;

		default:
			_DO_THROW("The parameter class is expected to be either a string or an object");
			/* returns out of this function */
	}

	if ((constant = gear_hash_find_ptr(&ce->constants_table, constname)) == NULL) {
		gear_throw_exception_ex(reflection_exception_ptr, 0, "Class Constant %s::%s does not exist", ZSTR_VAL(ce->name), ZSTR_VAL(constname));
		return;
	}

	ZVAL_STR_COPY(&name, constname);
	ZVAL_STR_COPY(&cname, ce->name);

	intern->ptr = constant;
	intern->ref_type = REF_TYPE_CLASS_CONSTANT;
	intern->ce = constant->ce;
	intern->ignore_visibility = 0;
	reflection_update_property_name(object, &name);
	reflection_update_property_class(object, &cname);
}
/* }}} */

/* {{{ proto public string ReflectionClassConstant::__toString()
   Returns a string representation */
GEAR_METHOD(reflection_class_constant, __toString)
{
	reflection_object *intern;
	gear_class_constant *ref;
	smart_str str = {0};
	zval name;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);
	_default_get_name(getThis(), &name);
	_class_const_string(&str, Z_STRVAL(name), ref, "");
	zval_ptr_dtor(&name);
	RETURN_STR(smart_str_extract(&str));
}
/* }}} */

/* {{{ proto public string ReflectionClassConstant::getName()
   Returns the constant' name */
GEAR_METHOD(reflection_class_constant, getName)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	_default_get_name(getThis(), return_value);
}
/* }}} */

static void _class_constant_check_flag(INTERNAL_FUNCTION_PARAMETERS, int mask) /* {{{ */
{
	reflection_object *intern;
	gear_class_constant *ref;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);
	RETURN_BOOL(Z_ACCESS_FLAGS(ref->value) & mask);
}
/* }}} */

/* {{{ proto public bool ReflectionClassConstant::isPublic()
   Returns whether this constant is public */
GEAR_METHOD(reflection_class_constant, isPublic)
{
	_class_constant_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PUBLIC | GEAR_ACC_IMPLICIT_PUBLIC);
}
/* }}} */

/* {{{ proto public bool ReflectionClassConstant::isPrivate()
   Returns whether this constant is private */
GEAR_METHOD(reflection_class_constant, isPrivate)
{
	_class_constant_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PRIVATE);
}
/* }}} */

/* {{{ proto public bool ReflectionClassConstant::isProtected()
   Returns whether this constant is protected */
GEAR_METHOD(reflection_class_constant, isProtected)
{
	_class_constant_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PROTECTED);
}
/* }}} */

/* {{{ proto public int ReflectionClassConstant::getModifiers()
   Returns a bitfield of the access modifiers for this constant */
GEAR_METHOD(reflection_class_constant, getModifiers)
{
	reflection_object *intern;
	gear_class_constant *ref;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);

	RETURN_LONG(Z_ACCESS_FLAGS(ref->value));
}
/* }}} */

/* {{{ proto public mixed ReflectionClassConstant::getValue()
   Returns this constant's value */
GEAR_METHOD(reflection_class_constant, getValue)
{
	reflection_object *intern;
	gear_class_constant *ref;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);

	ZVAL_COPY_OR_DUP(return_value, &ref->value);
	if (Z_TYPE_P(return_value) == IS_CONSTANT_AST) {
		zval_update_constant_ex(return_value, ref->ce);
	}
}
/* }}} */

/* {{{ proto public ReflectionClass ReflectionClassConstant::getDeclaringClass()
   Get the declaring class */
GEAR_METHOD(reflection_class_constant, getDeclaringClass)
{
	reflection_object *intern;
	gear_class_constant *ref;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);

	gear_reflection_class_factory(ref->ce, return_value);
}
/* }}} */

/* {{{ proto public string ReflectionClassConstant::getDocComment()
   Returns the doc comment for this constant */
GEAR_METHOD(reflection_class_constant, getDocComment)
{
	reflection_object *intern;
	gear_class_constant *ref;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);
	if (ref->doc_comment) {
		RETURN_STR_COPY(ref->doc_comment);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public static mixed ReflectionClass::export(mixed argument [, bool return]) throws ReflectionException
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_class, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_class_ptr, 1);
}
/* }}} */

/* {{{ reflection_class_object_ctor */
static void reflection_class_object_ctor(INTERNAL_FUNCTION_PARAMETERS, int is_object)
{
	zval *argument;
	zval *object;
	zval classname;
	reflection_object *intern;
	gear_class_entry *ce;

	if (is_object) {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "o", &argument) == FAILURE) {
			return;
		}
	} else {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "z", &argument) == FAILURE) {
			return;
		}
	}

	object = getThis();
	intern = Z_REFLECTION_P(object);

	if (Z_TYPE_P(argument) == IS_OBJECT) {
		ZVAL_STR_COPY(&classname, Z_OBJCE_P(argument)->name);
		reflection_update_property_name(object, &classname);
		intern->ptr = Z_OBJCE_P(argument);
		if (is_object) {
			ZVAL_COPY(&intern->obj, argument);
		}
	} else {
		convert_to_string_ex(argument);
		if ((ce = gear_lookup_class(Z_STR_P(argument))) == NULL) {
			if (!EG(exception)) {
				gear_throw_exception_ex(reflection_exception_ptr, -1, "Class %s does not exist", Z_STRVAL_P(argument));
			}
			return;
		}

		ZVAL_STR_COPY(&classname, ce->name);
		reflection_update_property_name(object, &classname);

		intern->ptr = ce;
	}
	intern->ref_type = REF_TYPE_OTHER;
}
/* }}} */

/* {{{ proto public void ReflectionClass::__construct(mixed argument) throws ReflectionException
   Constructor. Takes a string or an instance as an argument */
GEAR_METHOD(reflection_class, __construct)
{
	reflection_class_object_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ add_class_vars */
static void add_class_vars(gear_class_entry *ce, int statics, zval *return_value)
{
	gear_property_info *prop_info;
	zval *prop, prop_copy;
	gear_string *key;

	GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->properties_info, key, prop_info) {
		if (((prop_info->flags & GEAR_ACC_SHADOW) &&
		     prop_info->ce != ce) ||
		    ((prop_info->flags & GEAR_ACC_PROTECTED) &&
		     !gear_check_protected(prop_info->ce, ce)) ||
		    ((prop_info->flags & GEAR_ACC_PRIVATE) &&
		     prop_info->ce != ce)) {
			continue;
		}
		prop = NULL;
		if (statics && (prop_info->flags & GEAR_ACC_STATIC) != 0) {
			prop = &ce->default_static_members_table[prop_info->offset];
			ZVAL_DEINDIRECT(prop);
		} else if (!statics && (prop_info->flags & GEAR_ACC_STATIC) == 0) {
			prop = &ce->default_properties_table[OBJ_PROP_TO_NUM(prop_info->offset)];
		}
		if (!prop) {
			continue;
		}

		/* copy: enforce read only access */
		ZVAL_DEREF(prop);
		ZVAL_COPY_OR_DUP(&prop_copy, prop);

		/* this is necessary to make it able to work with default array
		* properties, returned to user */
		if (Z_TYPE(prop_copy) == IS_CONSTANT_AST) {
			if (UNEXPECTED(zval_update_constant_ex(&prop_copy, NULL) != SUCCESS)) {
				return;
			}
		}

		gear_hash_update(Z_ARRVAL_P(return_value), key, &prop_copy);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto public array ReflectionClass::getStaticProperties()
   Returns an associative array containing all static property values of the class */
GEAR_METHOD(reflection_class, getStaticProperties)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);

	if (UNEXPECTED(gear_update_class_constants(ce) != SUCCESS)) {
		return;
	}

	array_init(return_value);
	add_class_vars(ce, 1, return_value);
}
/* }}} */

/* {{{ proto public mixed ReflectionClass::getStaticPropertyValue(string name [, mixed default])
   Returns the value of a static property */
GEAR_METHOD(reflection_class, getStaticPropertyValue)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_string *name;
	zval *prop, *def_value = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S|z", &name, &def_value) == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);

	if (UNEXPECTED(gear_update_class_constants(ce) != SUCCESS)) {
		return;
	}
	prop = gear_std_get_static_property(ce, name, 1);
	if (!prop) {
		if (def_value) {
			ZVAL_COPY(return_value, def_value);
		} else {
			gear_throw_exception_ex(reflection_exception_ptr, 0,
				"Class %s does not have a property named %s", ZSTR_VAL(ce->name), ZSTR_VAL(name));
		}
		return;
	} else {
		ZVAL_COPY_DEREF(return_value, prop);
	}
}
/* }}} */

/* {{{ proto public void ReflectionClass::setStaticPropertyValue(string $name, mixed $value)
   Sets the value of a static property */
GEAR_METHOD(reflection_class, setStaticPropertyValue)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_string *name;
	zval *variable_ptr, *value;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "Sz", &name, &value) == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);

	if (UNEXPECTED(gear_update_class_constants(ce) != SUCCESS)) {
		return;
	}
	variable_ptr = gear_std_get_static_property(ce, name, 1);
	if (!variable_ptr) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
				"Class %s does not have a property named %s", ZSTR_VAL(ce->name), ZSTR_VAL(name));
		return;
	}
	ZVAL_DEREF(variable_ptr);
	zval_ptr_dtor(variable_ptr);
	ZVAL_COPY(variable_ptr, value);
}
/* }}} */

/* {{{ proto public array ReflectionClass::getDefaultProperties()
   Returns an associative array containing copies of all default property values of the class */
GEAR_METHOD(reflection_class, getDefaultProperties)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	array_init(return_value);
	if (UNEXPECTED(gear_update_class_constants(ce) != SUCCESS)) {
		return;
	}
	add_class_vars(ce, 1, return_value);
	add_class_vars(ce, 0, return_value);
}
/* }}} */

/* {{{ proto public string ReflectionClass::__toString()
   Returns a string representation */
GEAR_METHOD(reflection_class, __toString)
{
	reflection_object *intern;
	gear_class_entry *ce;
	smart_str str = {0};

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	_class_string(&str, ce, &intern->obj, "");
	RETURN_STR(smart_str_extract(&str));
}
/* }}} */

/* {{{ proto public string ReflectionClass::getName()
   Returns the class' name */
GEAR_METHOD(reflection_class, getName)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	_default_get_name(getThis(), return_value);
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isInternal()
   Returns whether this class is an internal class */
GEAR_METHOD(reflection_class, isInternal)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	RETURN_BOOL(ce->type == GEAR_INTERNAL_CLASS);
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isUserDefined()
   Returns whether this class is user-defined */
GEAR_METHOD(reflection_class, isUserDefined)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	RETURN_BOOL(ce->type == GEAR_USER_CLASS);
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isAnonymous()
   Returns whether this class is anonymous */
GEAR_METHOD(reflection_class, isAnonymous)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	RETURN_BOOL(ce->ce_flags & GEAR_ACC_ANON_CLASS);
}
/* }}} */

/* {{{ proto public string ReflectionClass::getFileName()
   Returns the filename of the file this class was declared in */
GEAR_METHOD(reflection_class, getFileName)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	if (ce->type == GEAR_USER_CLASS) {
		RETURN_STR_COPY(ce->info.user.filename);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public int ReflectionClass::getStartLine()
   Returns the line this class' declaration starts at */
GEAR_METHOD(reflection_class, getStartLine)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	if (ce->type == GEAR_USER_CLASS) {
		RETURN_LONG(ce->info.user.line_start);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public int ReflectionClass::getEndLine()
   Returns the line this class' declaration ends at */
GEAR_METHOD(reflection_class, getEndLine)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	if (ce->type == GEAR_USER_CLASS) {
		RETURN_LONG(ce->info.user.line_end);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public string ReflectionClass::getDocComment()
   Returns the doc comment for this class */
GEAR_METHOD(reflection_class, getDocComment)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	if (ce->type == GEAR_USER_CLASS && ce->info.user.doc_comment) {
		RETURN_STR_COPY(ce->info.user.doc_comment);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public ReflectionMethod ReflectionClass::getConstructor()
   Returns the class' constructor if there is one, NULL otherwise */
GEAR_METHOD(reflection_class, getConstructor)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);

	if (ce->constructor) {
		reflection_method_factory(ce, ce->constructor, NULL, return_value);
	} else {
		RETURN_NULL();
	}
}
/* }}} */

/* {{{ proto public bool ReflectionClass::hasMethod(string name)
   Returns whether a method exists or not */
GEAR_METHOD(reflection_class, hasMethod)
{
	reflection_object *intern;
	gear_class_entry *ce;
	char *name, *lc_name;
	size_t name_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &name, &name_len) == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);
	lc_name = gear_str_tolower_dup(name, name_len);
	if ((ce == gear_ce_closure && (name_len == sizeof(GEAR_INVOKE_FUNC_NAME)-1)
		&& memcmp(lc_name, GEAR_INVOKE_FUNC_NAME, sizeof(GEAR_INVOKE_FUNC_NAME)-1) == 0)
		|| gear_hash_str_exists(&ce->function_table, lc_name, name_len)) {
		efree(lc_name);
		RETURN_TRUE;
	} else {
		efree(lc_name);
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto public ReflectionMethod ReflectionClass::getMethod(string name) throws ReflectionException
   Returns the class' method specified by its name */
GEAR_METHOD(reflection_class, getMethod)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_function *mptr;
	zval obj_tmp;
	char *name, *lc_name;
	size_t name_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &name, &name_len) == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);
	lc_name = gear_str_tolower_dup(name, name_len);
	if (ce == gear_ce_closure && !Z_ISUNDEF(intern->obj) && (name_len == sizeof(GEAR_INVOKE_FUNC_NAME)-1)
		&& memcmp(lc_name, GEAR_INVOKE_FUNC_NAME, sizeof(GEAR_INVOKE_FUNC_NAME)-1) == 0
		&& (mptr = gear_get_closure_invoke_method(Z_OBJ(intern->obj))) != NULL)
	{
		/* don't assign closure_object since we only reflect the invoke handler
		   method and not the closure definition itself */
		reflection_method_factory(ce, mptr, NULL, return_value);
		efree(lc_name);
	} else if (ce == gear_ce_closure && Z_ISUNDEF(intern->obj) && (name_len == sizeof(GEAR_INVOKE_FUNC_NAME)-1)
		&& memcmp(lc_name, GEAR_INVOKE_FUNC_NAME, sizeof(GEAR_INVOKE_FUNC_NAME)-1) == 0
		&& object_init_ex(&obj_tmp, ce) == SUCCESS && (mptr = gear_get_closure_invoke_method(Z_OBJ(obj_tmp))) != NULL) {
		/* don't assign closure_object since we only reflect the invoke handler
		   method and not the closure definition itself */
		reflection_method_factory(ce, mptr, NULL, return_value);
		zval_ptr_dtor(&obj_tmp);
		efree(lc_name);
	} else if ((mptr = gear_hash_str_find_ptr(&ce->function_table, lc_name, name_len)) != NULL) {
		reflection_method_factory(ce, mptr, NULL, return_value);
		efree(lc_name);
	} else {
		efree(lc_name);
		gear_throw_exception_ex(reflection_exception_ptr, 0,
				"Method %s does not exist", name);
		return;
	}
}
/* }}} */

/* {{{ _addmethod */
static void _addmethod(gear_function *mptr, gear_class_entry *ce, zval *retval, gear_long filter)
{
	if (mptr->common.fn_flags & filter) {
		zval method;
		reflection_method_factory(ce, mptr, NULL, &method);
		add_next_index_zval(retval, &method);
	}
}
/* }}} */

/* {{{ _addmethod */
static int _addmethod_va(zval *el, int num_args, va_list args, gear_hash_key *hash_key)
{
	gear_function *mptr = (gear_function*)Z_PTR_P(el);
	gear_class_entry *ce = *va_arg(args, gear_class_entry**);
	zval *retval = va_arg(args, zval*);
	long filter = va_arg(args, long);

	_addmethod(mptr, ce, retval, filter);
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

/* {{{ proto public ReflectionMethod[] ReflectionClass::getMethods([long $filter])
   Returns an array of this class' methods */
GEAR_METHOD(reflection_class, getMethods)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_long filter = 0;
	gear_bool filter_is_null = 1;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|l!", &filter, &filter_is_null) == FAILURE) {
		return;
	}

	if (filter_is_null) {
		filter = GEAR_ACC_PPP_MASK | GEAR_ACC_ABSTRACT | GEAR_ACC_FINAL | GEAR_ACC_STATIC;
	}

	GET_REFLECTION_OBJECT_PTR(ce);

	array_init(return_value);
	gear_hash_apply_with_arguments(&ce->function_table, (apply_func_args_t) _addmethod_va, 4, &ce, return_value, filter);

	if (instanceof_function(ce, gear_ce_closure)) {
		gear_bool has_obj = Z_TYPE(intern->obj) != IS_UNDEF;
		zval obj_tmp;
		gear_object *obj;
		if (!has_obj) {
			object_init_ex(&obj_tmp, ce);
			obj = Z_OBJ(obj_tmp);
		} else {
			obj = Z_OBJ(intern->obj);
		}
		gear_function *closure = gear_get_closure_invoke_method(obj);
		if (closure) {
			_addmethod(closure, ce, return_value, filter);
		}
		if (!has_obj) {
			zval_ptr_dtor(&obj_tmp);
		}
	}
}
/* }}} */

/* {{{ proto public bool ReflectionClass::hasProperty(string name)
   Returns whether a property exists or not */
GEAR_METHOD(reflection_class, hasProperty)
{
	reflection_object *intern;
	gear_property_info *property_info;
	gear_class_entry *ce;
	gear_string *name;
	zval property;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &name) == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);
	if ((property_info = gear_hash_find_ptr(&ce->properties_info, name)) != NULL) {
		if (property_info->flags & GEAR_ACC_SHADOW) {
			RETURN_FALSE;
		}
		RETURN_TRUE;
	} else {
		if (Z_TYPE(intern->obj) != IS_UNDEF && Z_OBJ_HANDLER(intern->obj, has_property)) {
			ZVAL_STR_COPY(&property, name);
			if (Z_OBJ_HANDLER(intern->obj, has_property)(&intern->obj, &property, 2, NULL)) {
				zval_ptr_dtor(&property);
				RETURN_TRUE;
			}
			zval_ptr_dtor(&property);
		}
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto public ReflectionProperty ReflectionClass::getProperty(string name) throws ReflectionException
   Returns the class' property specified by its name */
GEAR_METHOD(reflection_class, getProperty)
{
	reflection_object *intern;
	gear_class_entry *ce, *ce2;
	gear_property_info *property_info;
	gear_string *name, *classname;
	char *tmp, *str_name;
	size_t classname_len, str_name_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &name) == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);
	if ((property_info = gear_hash_find_ptr(&ce->properties_info, name)) != NULL) {
		if ((property_info->flags & GEAR_ACC_SHADOW) == 0) {
			reflection_property_factory(ce, name, property_info, return_value);
			return;
		}
	} else if (Z_TYPE(intern->obj) != IS_UNDEF) {
		/* Check for dynamic properties */
		if (gear_hash_exists(Z_OBJ_HT(intern->obj)->get_properties(&intern->obj), name)) {
			gear_property_info property_info_tmp;
			property_info_tmp.flags = GEAR_ACC_IMPLICIT_PUBLIC;
			property_info_tmp.name = name;
			property_info_tmp.doc_comment = NULL;
			property_info_tmp.ce = ce;

			reflection_property_factory(ce, name, &property_info_tmp, return_value);
			return;
		}
	}
	str_name = ZSTR_VAL(name);
	if ((tmp = strstr(ZSTR_VAL(name), "::")) != NULL) {
		classname_len = tmp - ZSTR_VAL(name);
		classname = gear_string_alloc(classname_len, 0);
		gear_str_tolower_copy(ZSTR_VAL(classname), ZSTR_VAL(name), classname_len);
		ZSTR_VAL(classname)[classname_len] = '\0';
		str_name_len = ZSTR_LEN(name) - (classname_len + 2);
		str_name = tmp + 2;

		ce2 = gear_lookup_class(classname);
		if (!ce2) {
			if (!EG(exception)) {
				gear_throw_exception_ex(reflection_exception_ptr, -1, "Class %s does not exist", ZSTR_VAL(classname));
			}
			gear_string_release_ex(classname, 0);
			return;
		}
		gear_string_release_ex(classname, 0);

		if (!instanceof_function(ce, ce2)) {
			gear_throw_exception_ex(reflection_exception_ptr, -1, "Fully qualified property name %s::%s does not specify a base class of %s", ZSTR_VAL(ce2->name), str_name, ZSTR_VAL(ce->name));
			return;
		}
		ce = ce2;

		if ((property_info = gear_hash_str_find_ptr(&ce->properties_info, str_name, str_name_len)) != NULL && (property_info->flags & GEAR_ACC_SHADOW) == 0) {
			reflection_property_factory_str(ce, str_name, str_name_len, property_info, return_value);
			return;
		}
	}
	gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Property %s does not exist", str_name);
}
/* }}} */

/* {{{ _addproperty */
static int _addproperty(zval *el, int num_args, va_list args, gear_hash_key *hash_key)
{
	zval property;
	gear_property_info *pptr = (gear_property_info*)Z_PTR_P(el);
	gear_class_entry *ce = *va_arg(args, gear_class_entry**);
	zval *retval = va_arg(args, zval*);
	long filter = va_arg(args, long);

	if (pptr->flags	& GEAR_ACC_SHADOW) {
		return 0;
	}

	if (pptr->flags	& filter) {
		const char *class_name, *prop_name;
		size_t prop_name_len;
		gear_unmangle_property_name_ex(pptr->name, &class_name, &prop_name, &prop_name_len);
		reflection_property_factory_str(ce, prop_name, prop_name_len, pptr, &property);
		add_next_index_zval(retval, &property);
	}
	return 0;
}
/* }}} */

/* {{{ _adddynproperty */
static int _adddynproperty(zval *ptr, int num_args, va_list args, gear_hash_key *hash_key)
{
	zval property;
	gear_class_entry *ce = *va_arg(args, gear_class_entry**);
	zval *retval = va_arg(args, zval*);

	/* under some circumstances, the properties hash table may contain numeric
	 * properties (e.g. when casting from array). This is a WONT FIX bug, at
	 * least for the moment. Ignore these */
	if (hash_key->key == NULL) {
		return 0;
	}

	if (ZSTR_VAL(hash_key->key)[0] == '\0') {
		return 0; /* non public cannot be dynamic */
	}

	if (gear_get_property_info(ce, hash_key->key, 1) == NULL) {
		gear_property_info property_info;

		property_info.doc_comment = NULL;
		property_info.flags = GEAR_ACC_IMPLICIT_PUBLIC;
		property_info.name = hash_key->key;
		property_info.ce = ce;
		property_info.offset = -1;
		reflection_property_factory(ce, hash_key->key, &property_info, &property);
		add_next_index_zval(retval, &property);
	}
	return 0;
}
/* }}} */

/* {{{ proto public ReflectionProperty[] ReflectionClass::getProperties([long $filter])
   Returns an array of this class' properties */
GEAR_METHOD(reflection_class, getProperties)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_long filter = 0;
	gear_bool filter_is_null = 1;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|l!", &filter, &filter_is_null) == FAILURE) {
		return;
	}
	
	if (filter_is_null) {
		filter = GEAR_ACC_PPP_MASK | GEAR_ACC_STATIC;
	}

	GET_REFLECTION_OBJECT_PTR(ce);

	array_init(return_value);
	gear_hash_apply_with_arguments(&ce->properties_info, (apply_func_args_t) _addproperty, 3, &ce, return_value, filter);

	if (Z_TYPE(intern->obj) != IS_UNDEF && (filter & GEAR_ACC_PUBLIC) != 0 && Z_OBJ_HT(intern->obj)->get_properties) {
		HashTable *properties = Z_OBJ_HT(intern->obj)->get_properties(&intern->obj);
		gear_hash_apply_with_arguments(properties, (apply_func_args_t) _adddynproperty, 2, &ce, return_value);
	}
}
/* }}} */

/* {{{ proto public bool ReflectionClass::hasConstant(string name)
   Returns whether a constant exists or not */
GEAR_METHOD(reflection_class, hasConstant)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_string *name;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &name) == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);
	if (gear_hash_exists(&ce->constants_table, name)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto public array ReflectionClass::getConstants()
   Returns an associative array containing this class' constants and their values */
GEAR_METHOD(reflection_class, getConstants)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_string *key;
	gear_class_constant *c;
	zval val;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	array_init(return_value);
	GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->constants_table, key, c) {
		if (UNEXPECTED(zval_update_constant_ex(&c->value, ce) != SUCCESS)) {
			gear_array_destroy(Z_ARRVAL_P(return_value));
			RETURN_NULL();
		}
		ZVAL_COPY_OR_DUP(&val, &c->value);
		gear_hash_add_new(Z_ARRVAL_P(return_value), key, &val);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto public array ReflectionClass::getReflectionConstants()
   Returns an associative array containing this class' constants as ReflectionClassConstant objects */
GEAR_METHOD(reflection_class, getReflectionConstants)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_string *name;
	gear_class_constant *constant;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	array_init(return_value);
	GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->constants_table, name, constant) {
		zval class_const;
		reflection_class_constant_factory(ce, name, constant, &class_const);
		gear_hash_next_index_insert(Z_ARRVAL_P(return_value), &class_const);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto public mixed ReflectionClass::getConstant(string name)
   Returns the class' constant specified by its name */
GEAR_METHOD(reflection_class, getConstant)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_class_constant *c;
	gear_string *name;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &name) == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);
	GEAR_HASH_FOREACH_PTR(&ce->constants_table, c) {
		if (UNEXPECTED(zval_update_constant_ex(&c->value, ce) != SUCCESS)) {
			return;
		}
	} GEAR_HASH_FOREACH_END();
	if ((c = gear_hash_find_ptr(&ce->constants_table, name)) == NULL) {
		RETURN_FALSE;
	}
	ZVAL_COPY_OR_DUP(return_value, &c->value);
}
/* }}} */

/* {{{ proto public mixed ReflectionClass::getReflectionConstant(string name)
   Returns the class' constant as ReflectionClassConstant objects */
GEAR_METHOD(reflection_class, getReflectionConstant)
{
	reflection_object *intern;
	gear_class_entry *ce;
	gear_class_constant *constant;
	gear_string *name;

	GET_REFLECTION_OBJECT_PTR(ce);
	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &name) == FAILURE) {
		return;
	}

	if ((constant = gear_hash_find_ptr(&ce->constants_table, name)) == NULL) {
		RETURN_FALSE;
	}
	reflection_class_constant_factory(ce, name, constant, return_value);
}
/* }}} */

/* {{{ _class_check_flag */
static void _class_check_flag(INTERNAL_FUNCTION_PARAMETERS, int mask)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	RETVAL_BOOL(ce->ce_flags & mask);
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isInstantiable()
   Returns whether this class is instantiable */
GEAR_METHOD(reflection_class, isInstantiable)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	if (ce->ce_flags & (GEAR_ACC_INTERFACE | GEAR_ACC_TRAIT | GEAR_ACC_EXPLICIT_ABSTRACT_CLASS | GEAR_ACC_IMPLICIT_ABSTRACT_CLASS)) {
		RETURN_FALSE;
	}

	/* Basically, the class is instantiable. Though, if there is a constructor
	 * and it is not publicly accessible, it isn't! */
	if (!ce->constructor) {
		RETURN_TRUE;
	}

	RETURN_BOOL(ce->constructor->common.fn_flags & GEAR_ACC_PUBLIC);
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isCloneable()
   Returns whether this class is cloneable */
GEAR_METHOD(reflection_class, isCloneable)
{
	reflection_object *intern;
	gear_class_entry *ce;
	zval obj;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	if (ce->ce_flags & (GEAR_ACC_INTERFACE | GEAR_ACC_TRAIT | GEAR_ACC_EXPLICIT_ABSTRACT_CLASS | GEAR_ACC_IMPLICIT_ABSTRACT_CLASS)) {
		RETURN_FALSE;
	}
	if (!Z_ISUNDEF(intern->obj)) {
		if (ce->clone) {
			RETURN_BOOL(ce->clone->common.fn_flags & GEAR_ACC_PUBLIC);
		} else {
			RETURN_BOOL(Z_OBJ_HANDLER(intern->obj, clone_obj) != NULL);
		}
	} else {
		if (ce->clone) {
			RETURN_BOOL(ce->clone->common.fn_flags & GEAR_ACC_PUBLIC);
		} else {
			if (UNEXPECTED(object_init_ex(&obj, ce) != SUCCESS)) {
				return;
			}
			RETVAL_BOOL(Z_OBJ_HANDLER(obj, clone_obj) != NULL);
			zval_ptr_dtor(&obj);
		}
	}
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isInterface()
   Returns whether this is an interface or a class */
GEAR_METHOD(reflection_class, isInterface)
{
	_class_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_INTERFACE);
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isTrait()
   Returns whether this is a trait */
GEAR_METHOD(reflection_class, isTrait)
{
	_class_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_TRAIT);
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isFinal()
   Returns whether this class is final */
GEAR_METHOD(reflection_class, isFinal)
{
	_class_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_FINAL);
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isAbstract()
   Returns whether this class is abstract */
GEAR_METHOD(reflection_class, isAbstract)
{
	_class_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_IMPLICIT_ABSTRACT_CLASS|GEAR_ACC_EXPLICIT_ABSTRACT_CLASS);
}
/* }}} */

/* {{{ proto public int ReflectionClass::getModifiers()
   Returns a bitfield of the access modifiers for this class */
GEAR_METHOD(reflection_class, getModifiers)
{
	reflection_object *intern;
	gear_class_entry *ce;
	uint32_t keep_flags = GEAR_ACC_FINAL
		| GEAR_ACC_EXPLICIT_ABSTRACT_CLASS | GEAR_ACC_IMPLICIT_ABSTRACT_CLASS;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);

	RETURN_LONG((ce->ce_flags & keep_flags));
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isInstance(stdclass object)
   Returns whether the given object is an instance of this class */
GEAR_METHOD(reflection_class, isInstance)
{
	reflection_object *intern;
	gear_class_entry *ce;
	zval *object;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "o", &object) == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);
	RETURN_BOOL(instanceof_function(Z_OBJCE_P(object), ce));
}
/* }}} */

/* {{{ proto public stdclass ReflectionClass::newInstance(mixed* args, ...)
   Returns an instance of this class */
GEAR_METHOD(reflection_class, newInstance)
{
	zval retval;
	reflection_object *intern;
	gear_class_entry *ce, *old_scope;
	gear_function *constructor;

	GET_REFLECTION_OBJECT_PTR(ce);

	if (UNEXPECTED(object_init_ex(return_value, ce) != SUCCESS)) {
		return;
	}

	old_scope = EG(fake_scope);
	EG(fake_scope) = ce;
	constructor = Z_OBJ_HT_P(return_value)->get_constructor(Z_OBJ_P(return_value));
	EG(fake_scope) = old_scope;

	/* Run the constructor if there is one */
	if (constructor) {
		zval *params = NULL;
		int ret, i, num_args = 0;
		gear_fcall_info fci;
		gear_fcall_info_cache fcc;

		if (!(constructor->common.fn_flags & GEAR_ACC_PUBLIC)) {
			gear_throw_exception_ex(reflection_exception_ptr, 0, "Access to non-public constructor of class %s", ZSTR_VAL(ce->name));
			zval_ptr_dtor(return_value);
			RETURN_NULL();
		}

		if (gear_parse_parameters(GEAR_NUM_ARGS(), "*", &params, &num_args) == FAILURE) {
			zval_ptr_dtor(return_value);
			RETURN_FALSE;
		}

		for (i = 0; i < num_args; i++) {
			Z_TRY_ADDREF(params[i]);
		}

		fci.size = sizeof(fci);
		ZVAL_UNDEF(&fci.function_name);
		fci.object = Z_OBJ_P(return_value);
		fci.retval = &retval;
		fci.param_count = num_args;
		fci.params = params;
		fci.no_separation = 1;

		fcc.function_handler = constructor;
		fcc.called_scope = Z_OBJCE_P(return_value);
		fcc.object = Z_OBJ_P(return_value);

		ret = gear_call_function(&fci, &fcc);
		zval_ptr_dtor(&retval);
		for (i = 0; i < num_args; i++) {
			zval_ptr_dtor(&params[i]);
		}

		if (EG(exception)) {
			gear_object_store_ctor_failed(Z_OBJ_P(return_value));
		}
		if (ret == FAILURE) {
			hyss_error_docref(NULL, E_WARNING, "Invocation of %s's constructor failed", ZSTR_VAL(ce->name));
			zval_ptr_dtor(return_value);
			RETURN_NULL();
		}
	} else if (GEAR_NUM_ARGS()) {
		gear_throw_exception_ex(reflection_exception_ptr, 0, "Class %s does not have a constructor, so you cannot pass any constructor arguments", ZSTR_VAL(ce->name));
	}
}
/* }}} */

/* {{{ proto public stdclass ReflectionClass::newInstanceWithoutConstructor()
   Returns an instance of this class without invoking its constructor */
GEAR_METHOD(reflection_class, newInstanceWithoutConstructor)
{
	reflection_object *intern;
	gear_class_entry *ce;

	GET_REFLECTION_OBJECT_PTR(ce);

	if (ce->create_object != NULL && ce->ce_flags & GEAR_ACC_FINAL) {
		gear_throw_exception_ex(reflection_exception_ptr, 0, "Class %s is an internal class marked as final that cannot be instantiated without invoking its constructor", ZSTR_VAL(ce->name));
		return;
	}

	object_init_ex(return_value, ce);
}
/* }}} */

/* {{{ proto public stdclass ReflectionClass::newInstanceArgs([array args])
   Returns an instance of this class */
GEAR_METHOD(reflection_class, newInstanceArgs)
{
	zval retval, *val;
	reflection_object *intern;
	gear_class_entry *ce, *old_scope;
	int ret, i, argc = 0;
	HashTable *args;
	gear_function *constructor;

	GET_REFLECTION_OBJECT_PTR(ce);

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|h", &args) == FAILURE) {
		return;
	}

	if (GEAR_NUM_ARGS() > 0) {
		argc = args->nNumOfElements;
	}

	if (UNEXPECTED(object_init_ex(return_value, ce) != SUCCESS)) {
		return;
	}

	old_scope = EG(fake_scope);
	EG(fake_scope) = ce;
	constructor = Z_OBJ_HT_P(return_value)->get_constructor(Z_OBJ_P(return_value));
	EG(fake_scope) = old_scope;

	/* Run the constructor if there is one */
	if (constructor) {
		zval *params = NULL;
		gear_fcall_info fci;
		gear_fcall_info_cache fcc;

		if (!(constructor->common.fn_flags & GEAR_ACC_PUBLIC)) {
			gear_throw_exception_ex(reflection_exception_ptr, 0, "Access to non-public constructor of class %s", ZSTR_VAL(ce->name));
			zval_ptr_dtor(return_value);
			RETURN_NULL();
		}

		if (argc) {
			params = safe_emalloc(sizeof(zval), argc, 0);
			argc = 0;
			GEAR_HASH_FOREACH_VAL(args, val) {
				ZVAL_COPY(&params[argc], val);
				argc++;
			} GEAR_HASH_FOREACH_END();
		}

		fci.size = sizeof(fci);
		ZVAL_UNDEF(&fci.function_name);
		fci.object = Z_OBJ_P(return_value);
		fci.retval = &retval;
		fci.param_count = argc;
		fci.params = params;
		fci.no_separation = 1;

		fcc.function_handler = constructor;
		fcc.called_scope = Z_OBJCE_P(return_value);
		fcc.object = Z_OBJ_P(return_value);

		ret = gear_call_function(&fci, &fcc);
		zval_ptr_dtor(&retval);
		if (params) {
			for (i = 0; i < argc; i++) {
				zval_ptr_dtor(&params[i]);
			}
			efree(params);
		}

		if (EG(exception)) {
			gear_object_store_ctor_failed(Z_OBJ_P(return_value));
		}
		if (ret == FAILURE) {
			zval_ptr_dtor(&retval);
			hyss_error_docref(NULL, E_WARNING, "Invocation of %s's constructor failed", ZSTR_VAL(ce->name));
			zval_ptr_dtor(return_value);
			RETURN_NULL();
		}
	} else if (argc) {
		gear_throw_exception_ex(reflection_exception_ptr, 0, "Class %s does not have a constructor, so you cannot pass any constructor arguments", ZSTR_VAL(ce->name));
	}
}
/* }}} */

/* {{{ proto public ReflectionClass[] ReflectionClass::getInterfaces()
   Returns an array of interfaces this class implements */
GEAR_METHOD(reflection_class, getInterfaces)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);

	if (ce->num_interfaces) {
		uint32_t i;

		array_init(return_value);
		for (i=0; i < ce->num_interfaces; i++) {
			zval interface;
			gear_reflection_class_factory(ce->interfaces[i], &interface);
			gear_hash_update(Z_ARRVAL_P(return_value), ce->interfaces[i]->name, &interface);
		}
	} else {
		ZVAL_EMPTY_ARRAY(return_value);
	}
}
/* }}} */

/* {{{ proto public String[] ReflectionClass::getInterfaceNames()
   Returns an array of names of interfaces this class implements */
GEAR_METHOD(reflection_class, getInterfaceNames)
{
	reflection_object *intern;
	gear_class_entry *ce;
	uint32_t i;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);

	if (!ce->num_interfaces) {
		/* Return an empty array if this class implements no interfaces */
		ZVAL_EMPTY_ARRAY(return_value);
		return;
	}

	array_init(return_value);

	for (i=0; i < ce->num_interfaces; i++) {
		add_next_index_str(return_value, gear_string_copy(ce->interfaces[i]->name));
	}
}
/* }}} */

/* {{{ proto public ReflectionClass[] ReflectionClass::getTraits()
   Returns an array of traits used by this class */
GEAR_METHOD(reflection_class, getTraits)
{
	reflection_object *intern;
	gear_class_entry *ce;
	uint32_t i;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);

	if (!ce->num_traits) {
		ZVAL_EMPTY_ARRAY(return_value);
		return;
	}

	array_init(return_value);

	for (i=0; i < ce->num_traits; i++) {
		zval trait;
		gear_reflection_class_factory(ce->traits[i], &trait);
		gear_hash_update(Z_ARRVAL_P(return_value), ce->traits[i]->name, &trait);
	}
}
/* }}} */

/* {{{ proto public String[] ReflectionClass::getTraitNames()
   Returns an array of names of traits used by this class */
GEAR_METHOD(reflection_class, getTraitNames)
{
	reflection_object *intern;
	gear_class_entry *ce;
	uint32_t i;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);

	if (!ce->num_traits) {
		ZVAL_EMPTY_ARRAY(return_value);
		return;
	}

	array_init(return_value);

	for (i=0; i < ce->num_traits; i++) {
		add_next_index_str(return_value, gear_string_copy(ce->traits[i]->name));
	}
}
/* }}} */

/* {{{ proto public array ReflectionClass::getTraitAliases()
   Returns an array of trait aliases */
GEAR_METHOD(reflection_class, getTraitAliases)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);


	if (ce->trait_aliases) {
		uint32_t i = 0;

		array_init(return_value);
		while (ce->trait_aliases[i]) {
			gear_string *mname;
			gear_trait_method_reference *cur_ref = &ce->trait_aliases[i]->trait_method;

			if (ce->trait_aliases[i]->alias) {

				mname = gear_string_alloc(ZSTR_LEN(cur_ref->class_name) + ZSTR_LEN(cur_ref->method_name) + 2, 0);
				snprintf(ZSTR_VAL(mname), ZSTR_LEN(mname) + 1, "%s::%s", ZSTR_VAL(cur_ref->class_name), ZSTR_VAL(cur_ref->method_name));
				add_assoc_str_ex(return_value, ZSTR_VAL(ce->trait_aliases[i]->alias), ZSTR_LEN(ce->trait_aliases[i]->alias), mname);
			}
			i++;
		}
	} else {
		ZVAL_EMPTY_ARRAY(return_value);
	}
}
/* }}} */

/* {{{ proto public ReflectionClass ReflectionClass::getParentClass()
   Returns the class' parent class, or, if none exists, FALSE */
GEAR_METHOD(reflection_class, getParentClass)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ce);

	if (ce->parent) {
		gear_reflection_class_factory(ce->parent, return_value);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isSubclassOf(string|ReflectionClass class)
   Returns whether this class is a subclass of another class */
GEAR_METHOD(reflection_class, isSubclassOf)
{
	reflection_object *intern, *argument;
	gear_class_entry *ce, *class_ce;
	zval *class_name;

	GET_REFLECTION_OBJECT_PTR(ce);

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "z", &class_name) == FAILURE) {
		return;
	}

	switch (Z_TYPE_P(class_name)) {
		case IS_STRING:
			if ((class_ce = gear_lookup_class(Z_STR_P(class_name))) == NULL) {
				gear_throw_exception_ex(reflection_exception_ptr, 0,
						"Class %s does not exist", Z_STRVAL_P(class_name));
				return;
			}
			break;
		case IS_OBJECT:
			if (instanceof_function(Z_OBJCE_P(class_name), reflection_class_ptr)) {
				argument = Z_REFLECTION_P(class_name);
				if (argument->ptr == NULL) {
					gear_throw_error(NULL, "Internal error: Failed to retrieve the argument's reflection object");
					return;
				}
				class_ce = argument->ptr;
				break;
			}
			/* no break */
		default:
			gear_throw_exception_ex(reflection_exception_ptr, 0,
					"Parameter one must either be a string or a ReflectionClass object");
			return;
	}

	RETURN_BOOL((ce != class_ce && instanceof_function(ce, class_ce)));
}
/* }}} */

/* {{{ proto public bool ReflectionClass::implementsInterface(string|ReflectionClass interface_name)
   Returns whether this class is a subclass of another class */
GEAR_METHOD(reflection_class, implementsInterface)
{
	reflection_object *intern, *argument;
	gear_class_entry *ce, *interface_ce;
	zval *interface;

	GET_REFLECTION_OBJECT_PTR(ce);

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "z", &interface) == FAILURE) {
		return;
	}

	switch (Z_TYPE_P(interface)) {
		case IS_STRING:
			if ((interface_ce = gear_lookup_class(Z_STR_P(interface))) == NULL) {
				gear_throw_exception_ex(reflection_exception_ptr, 0,
						"Interface %s does not exist", Z_STRVAL_P(interface));
				return;
			}
			break;
		case IS_OBJECT:
			if (instanceof_function(Z_OBJCE_P(interface), reflection_class_ptr)) {
				argument = Z_REFLECTION_P(interface);
				if (argument->ptr == NULL) {
					gear_throw_error(NULL, "Internal error: Failed to retrieve the argument's reflection object");
					return;
				}
				interface_ce = argument->ptr;
				break;
			}
			/* no break */
		default:
			gear_throw_exception_ex(reflection_exception_ptr, 0,
					"Parameter one must either be a string or a ReflectionClass object");
			return;
	}

	if (!(interface_ce->ce_flags & GEAR_ACC_INTERFACE)) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
				"Interface %s is a Class", ZSTR_VAL(interface_ce->name));
		return;
	}
	RETURN_BOOL(instanceof_function(ce, interface_ce));
}
/* }}} */

/* {{{ proto public bool ReflectionClass::isIterable()
   Returns whether this class is iterable (can be used inside foreach) */
GEAR_METHOD(reflection_class, isIterable)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);

	if (ce->ce_flags & (GEAR_ACC_INTERFACE | GEAR_ACC_IMPLICIT_ABSTRACT_CLASS |
	                    GEAR_ACC_TRAIT     | GEAR_ACC_EXPLICIT_ABSTRACT_CLASS)) {
		RETURN_FALSE;
	}

	RETURN_BOOL(ce->get_iterator || instanceof_function(ce, gear_ce_traversable));
}
/* }}} */

/* {{{ proto public ReflectionExtension|NULL ReflectionClass::getExtension()
   Returns NULL or the extension the class belongs to */
GEAR_METHOD(reflection_class, getExtension)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);

	if ((ce->type == GEAR_INTERNAL_CLASS) && ce->info.internal.cAPI) {
		reflection_extension_factory(return_value, ce->info.internal.cAPI->name);
	}
}
/* }}} */

/* {{{ proto public string|false ReflectionClass::getExtensionName()
   Returns false or the name of the extension the class belongs to */
GEAR_METHOD(reflection_class, getExtensionName)
{
	reflection_object *intern;
	gear_class_entry *ce;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	GET_REFLECTION_OBJECT_PTR(ce);

	if ((ce->type == GEAR_INTERNAL_CLASS) && ce->info.internal.cAPI) {
		RETURN_STRING(ce->info.internal.cAPI->name);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto public bool ReflectionClass::inNamespace()
   Returns whether this class is defined in namespace */
GEAR_METHOD(reflection_class, inNamespace)
{
	zval *name;
	const char *backslash;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	if ((name = _default_load_name(getThis())) == NULL) {
		RETURN_FALSE;
	}
	if (Z_TYPE_P(name) == IS_STRING
		&& (backslash = gear_memrchr(Z_STRVAL_P(name), '\\', Z_STRLEN_P(name)))
		&& backslash > Z_STRVAL_P(name))
	{
		RETURN_TRUE;
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public string ReflectionClass::getNamespaceName()
   Returns the name of namespace where this class is defined */
GEAR_METHOD(reflection_class, getNamespaceName)
{
	zval *name;
	const char *backslash;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	if ((name = _default_load_name(getThis())) == NULL) {
		RETURN_FALSE;
	}
	if (Z_TYPE_P(name) == IS_STRING
		&& (backslash = gear_memrchr(Z_STRVAL_P(name), '\\', Z_STRLEN_P(name)))
		&& backslash > Z_STRVAL_P(name))
	{
		RETURN_STRINGL(Z_STRVAL_P(name), backslash - Z_STRVAL_P(name));
	}
	RETURN_EMPTY_STRING();
}
/* }}} */

/* {{{ proto public string ReflectionClass::getShortName()
   Returns the short name of the class (without namespace part) */
GEAR_METHOD(reflection_class, getShortName)
{
	zval *name;
	const char *backslash;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	if ((name = _default_load_name(getThis())) == NULL) {
		RETURN_FALSE;
	}
	if (Z_TYPE_P(name) == IS_STRING
		&& (backslash = gear_memrchr(Z_STRVAL_P(name), '\\', Z_STRLEN_P(name)))
		&& backslash > Z_STRVAL_P(name))
	{
		RETURN_STRINGL(backslash + 1, Z_STRLEN_P(name) - (backslash - Z_STRVAL_P(name) + 1));
	}
	ZVAL_COPY_DEREF(return_value, name);
}
/* }}} */

/* {{{ proto public static mixed ReflectionObject::export(mixed argument [, bool return]) throws ReflectionException
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_object, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_object_ptr, 1);
}
/* }}} */

/* {{{ proto public void ReflectionObject::__construct(mixed argument) throws ReflectionException
   Constructor. Takes an instance as an argument */
GEAR_METHOD(reflection_object, __construct)
{
	reflection_class_object_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto public static mixed ReflectionProperty::export(mixed class, string name [, bool return]) throws ReflectionException
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_property, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_property_ptr, 2);
}
/* }}} */

/* {{{ proto public static mixed ReflectionClassConstant::export(mixed class, string name [, bool return]) throws ReflectionException
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_class_constant, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_class_constant_ptr, 2);
}
/* }}} */

/* {{{ proto public void ReflectionProperty::__construct(mixed class, string name)
   Constructor. Throws an Exception in case the given property does not exist */
GEAR_METHOD(reflection_property, __construct)
{
	zval propname, cname, *classname;
	gear_string *name;
	int dynam_prop = 0;
	zval *object;
	reflection_object *intern;
	gear_class_entry *ce;
	gear_property_info *property_info = NULL;
	property_reference *reference;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "zS", &classname, &name) == FAILURE) {
		return;
	}

	object = getThis();
	intern = Z_REFLECTION_P(object);

	/* Find the class entry */
	switch (Z_TYPE_P(classname)) {
		case IS_STRING:
			if ((ce = gear_lookup_class(Z_STR_P(classname))) == NULL) {
				gear_throw_exception_ex(reflection_exception_ptr, 0,
						"Class %s does not exist", Z_STRVAL_P(classname));
				return;
			}
			break;

		case IS_OBJECT:
			ce = Z_OBJCE_P(classname);
			break;

		default:
			_DO_THROW("The parameter class is expected to be either a string or an object");
			/* returns out of this function */
	}

	if ((property_info = gear_hash_find_ptr(&ce->properties_info, name)) == NULL || (property_info->flags & GEAR_ACC_SHADOW)) {
		/* Check for dynamic properties */
		if (property_info == NULL && Z_TYPE_P(classname) == IS_OBJECT && Z_OBJ_HT_P(classname)->get_properties) {
			if (gear_hash_exists(Z_OBJ_HT_P(classname)->get_properties(classname), name)) {
				dynam_prop = 1;
			}
		}
		if (dynam_prop == 0) {
			gear_throw_exception_ex(reflection_exception_ptr, 0, "Property %s::$%s does not exist", ZSTR_VAL(ce->name), ZSTR_VAL(name));
			return;
		}
	}

	if (dynam_prop == 0 && (property_info->flags & GEAR_ACC_PRIVATE) == 0) {
		/* we have to search the class hierarchy for this (implicit) public or protected property */
		gear_class_entry *tmp_ce = ce;
		gear_property_info *tmp_info;

		while (tmp_ce && (tmp_info = gear_hash_find_ptr(&tmp_ce->properties_info, name)) == NULL) {
			ce = tmp_ce;
			property_info = tmp_info;
			tmp_ce = tmp_ce->parent;
		}
	}

	if (dynam_prop == 0) {
		ZVAL_STR_COPY(&cname, property_info->ce->name);
	} else {
		ZVAL_STR_COPY(&cname, ce->name);
	}
	reflection_update_property_class(object, &cname);

	ZVAL_STR_COPY(&propname, name);
	reflection_update_property_name(object, &propname);

	reference = (property_reference*) emalloc(sizeof(property_reference));
	if (dynam_prop) {
		reference->prop.flags = GEAR_ACC_IMPLICIT_PUBLIC;
		reference->prop.name = name;
		reference->prop.doc_comment = NULL;
		reference->prop.ce = ce;
	} else {
		reference->prop = *property_info;
	}
	reference->ce = ce;
	reference->unmangled_name = gear_string_copy(name);
	intern->ptr = reference;
	intern->ref_type = REF_TYPE_PROPERTY;
	intern->ce = ce;
	intern->ignore_visibility = 0;
}
/* }}} */

/* {{{ proto public string ReflectionProperty::__toString()
   Returns a string representation */
GEAR_METHOD(reflection_property, __toString)
{
	reflection_object *intern;
	property_reference *ref;
	smart_str str = {0};

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);
	_property_string(&str, &ref->prop, ZSTR_VAL(ref->unmangled_name), "");
	RETURN_STR(smart_str_extract(&str));
}
/* }}} */

/* {{{ proto public string ReflectionProperty::getName()
   Returns the class' name */
GEAR_METHOD(reflection_property, getName)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	_default_get_name(getThis(), return_value);
}
/* }}} */

static void _property_check_flag(INTERNAL_FUNCTION_PARAMETERS, int mask) /* {{{ */
{
	reflection_object *intern;
	property_reference *ref;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);
	RETURN_BOOL(ref->prop.flags & mask);
}
/* }}} */

/* {{{ proto public bool ReflectionProperty::isPublic()
   Returns whether this property is public */
GEAR_METHOD(reflection_property, isPublic)
{
	_property_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PUBLIC | GEAR_ACC_IMPLICIT_PUBLIC);
}
/* }}} */

/* {{{ proto public bool ReflectionProperty::isPrivate()
   Returns whether this property is private */
GEAR_METHOD(reflection_property, isPrivate)
{
	_property_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PRIVATE);
}
/* }}} */

/* {{{ proto public bool ReflectionProperty::isProtected()
   Returns whether this property is protected */
GEAR_METHOD(reflection_property, isProtected)
{
	_property_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_PROTECTED);
}
/* }}} */

/* {{{ proto public bool ReflectionProperty::isStatic()
   Returns whether this property is static */
GEAR_METHOD(reflection_property, isStatic)
{
	_property_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, GEAR_ACC_STATIC);
}
/* }}} */

/* {{{ proto public bool ReflectionProperty::isDefault()
   Returns whether this property is default (declared at compilation time). */
GEAR_METHOD(reflection_property, isDefault)
{
	_property_check_flag(INTERNAL_FUNCTION_PARAM_PASSTHRU, ~GEAR_ACC_IMPLICIT_PUBLIC);
}
/* }}} */

/* {{{ proto public int ReflectionProperty::getModifiers()
   Returns a bitfield of the access modifiers for this property */
GEAR_METHOD(reflection_property, getModifiers)
{
	reflection_object *intern;
	property_reference *ref;
	uint32_t keep_flags = GEAR_ACC_PPP_MASK | GEAR_ACC_IMPLICIT_PUBLIC | GEAR_ACC_STATIC;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);

	RETURN_LONG((ref->prop.flags & keep_flags));
}
/* }}} */

/* {{{ proto public mixed ReflectionProperty::getValue([stdclass object])
   Returns this property's value */
GEAR_METHOD(reflection_property, getValue)
{
	reflection_object *intern;
	property_reference *ref;
	zval *object, *name;
	zval *member_p = NULL;

	GET_REFLECTION_OBJECT_PTR(ref);

	if (!(ref->prop.flags & (GEAR_ACC_PUBLIC | GEAR_ACC_IMPLICIT_PUBLIC)) && intern->ignore_visibility == 0) {
		name = _default_load_name(getThis());
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Cannot access non-public member %s::$%s", ZSTR_VAL(intern->ce->name), Z_STRVAL_P(name));
		return;
	}

	if (ref->prop.flags & GEAR_ACC_STATIC) {
		member_p = gear_read_static_property_ex(ref->ce, ref->unmangled_name, 0);
		if (member_p) {
			ZVAL_COPY_DEREF(return_value, member_p);
		}
	} else {
		zval rv;

		if (gear_parse_parameters(GEAR_NUM_ARGS(), "o", &object) == FAILURE) {
			return;
		}

		if (!instanceof_function(Z_OBJCE_P(object), ref->prop.ce)) {
			_DO_THROW("Given object is not an instance of the class this property was declared in");
			/* Returns from this function */
		}

		member_p = gear_read_property_ex(ref->ce, object, ref->unmangled_name, 0, &rv);
		if (member_p != &rv) {
			ZVAL_COPY_DEREF(return_value, member_p);
		} else {
			if (Z_ISREF_P(member_p)) {
				gear_unwrap_reference(member_p);
			}
			ZVAL_COPY_VALUE(return_value, member_p);
		}
	}
}
/* }}} */

/* {{{ proto public void ReflectionProperty::setValue([stdclass object,] mixed value)
   Sets this property's value */
GEAR_METHOD(reflection_property, setValue)
{
	reflection_object *intern;
	property_reference *ref;
	zval *object, *name;
	zval *value;
	zval *tmp;

	GET_REFLECTION_OBJECT_PTR(ref);

	if (!(ref->prop.flags & GEAR_ACC_PUBLIC) && intern->ignore_visibility == 0) {
		name = _default_load_name(getThis());
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Cannot access non-public member %s::$%s", ZSTR_VAL(intern->ce->name), Z_STRVAL_P(name));
		return;
	}

	if (ref->prop.flags & GEAR_ACC_STATIC) {
		if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, GEAR_NUM_ARGS(), "z", &value) == FAILURE) {
			if (gear_parse_parameters(GEAR_NUM_ARGS(), "zz", &tmp, &value) == FAILURE) {
				return;
			}
		}

		gear_update_static_property_ex(ref->ce, ref->unmangled_name, value);
	} else {
		if (gear_parse_parameters(GEAR_NUM_ARGS(), "oz", &object, &value) == FAILURE) {
			return;
		}

		gear_update_property_ex(ref->ce, object, ref->unmangled_name, value);
	}
}
/* }}} */

/* {{{ proto public ReflectionClass ReflectionProperty::getDeclaringClass()
   Get the declaring class */
GEAR_METHOD(reflection_property, getDeclaringClass)
{
	reflection_object *intern;
	property_reference *ref;
	gear_class_entry *tmp_ce, *ce;
	gear_property_info *tmp_info;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);

	ce = tmp_ce = ref->ce;
	while (tmp_ce && (tmp_info = gear_hash_find_ptr(&tmp_ce->properties_info, ref->unmangled_name)) != NULL) {
		if (tmp_info->flags & GEAR_ACC_PRIVATE || tmp_info->flags & GEAR_ACC_SHADOW) {
			/* it's a private property, so it can't be inherited */
			break;
		}
		ce = tmp_ce;
		if (tmp_ce == tmp_info->ce) {
			/* declared in this class, done */
			break;
		}
		tmp_ce = tmp_ce->parent;
	}

	gear_reflection_class_factory(ce, return_value);
}
/* }}} */

/* {{{ proto public string ReflectionProperty::getDocComment()
   Returns the doc comment for this property */
GEAR_METHOD(reflection_property, getDocComment)
{
	reflection_object *intern;
	property_reference *ref;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(ref);
	if (ref->prop.doc_comment) {
		RETURN_STR_COPY(ref->prop.doc_comment);
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto public int ReflectionProperty::setAccessible(bool visible)
   Sets whether non-public properties can be requested */
GEAR_METHOD(reflection_property, setAccessible)
{
	reflection_object *intern;
	gear_bool visible;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "b", &visible) == FAILURE) {
		return;
	}

	intern = Z_REFLECTION_P(getThis());

	intern->ignore_visibility = visible;
}
/* }}} */

/* {{{ proto public static mixed ReflectionExtension::export(string name [, bool return]) throws ReflectionException
   Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_extension, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_extension_ptr, 1);
}
/* }}} */

/* {{{ proto public void ReflectionExtension::__construct(string name)
   Constructor. Throws an Exception in case the given extension does not exist */
GEAR_METHOD(reflection_extension, __construct)
{
	zval name;
	zval *object;
	char *lcname;
	reflection_object *intern;
	gear_capi_entry *cAPI;
	char *name_str;
	size_t name_len;
	ALLOCA_FLAG(use_heap)

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "s", &name_str, &name_len) == FAILURE) {
		return;
	}

	object = getThis();
	intern = Z_REFLECTION_P(object);
	lcname = do_alloca(name_len + 1, use_heap);
	gear_str_tolower_copy(lcname, name_str, name_len);
	if ((cAPI = gear_hash_str_find_ptr(&capi_registry, lcname, name_len)) == NULL) {
		free_alloca(lcname, use_heap);
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Extension %s does not exist", name_str);
		return;
	}
	free_alloca(lcname, use_heap);
	ZVAL_STRING(&name, cAPI->name);
	reflection_update_property_name(object, &name);
	intern->ptr = cAPI;
	intern->ref_type = REF_TYPE_OTHER;
	intern->ce = NULL;
}
/* }}} */

/* {{{ proto public string ReflectionExtension::__toString()
   Returns a string representation */
GEAR_METHOD(reflection_extension, __toString)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;
	smart_str str = {0};

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);
	_extension_string(&str, cAPI, "");
	RETURN_STR(smart_str_extract(&str));
}
/* }}} */

/* {{{ proto public string ReflectionExtension::getName()
   Returns this extension's name */
GEAR_METHOD(reflection_extension, getName)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	_default_get_name(getThis(), return_value);
}
/* }}} */

/* {{{ proto public string ReflectionExtension::getVersion()
   Returns this extension's version */
GEAR_METHOD(reflection_extension, getVersion)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	/* An extension does not necessarily have a version number */
	if (cAPI->version == NO_VERSION_YET) {
		RETURN_NULL();
	} else {
		RETURN_STRING(cAPI->version);
	}
}
/* }}} */

/* {{{ proto public ReflectionFunction[] ReflectionExtension::getFunctions()
   Returns an array of this extension's functions */
GEAR_METHOD(reflection_extension, getFunctions)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;
	zval function;
	gear_function *fptr;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	array_init(return_value);
	GEAR_HASH_FOREACH_PTR(CG(function_table), fptr) {
		if (fptr->common.type==GEAR_INTERNAL_FUNCTION
			&& fptr->internal_function.cAPI == cAPI) {
			reflection_function_factory(fptr, NULL, &function);
			gear_hash_update(Z_ARRVAL_P(return_value), fptr->common.function_name, &function);
		}
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

static int _addconstant(zval *el, int num_args, va_list args, gear_hash_key *hash_key) /* {{{ */
{
	zval const_val;
	gear_constant *constant = (gear_constant*)Z_PTR_P(el);
	zval *retval = va_arg(args, zval*);
	int number = va_arg(args, int);

	if (number == GEAR_CONSTANT_CAPI_NUMBER(constant)) {
		ZVAL_COPY_OR_DUP(&const_val, &constant->value);
		gear_hash_update(Z_ARRVAL_P(retval), constant->name, &const_val);
	}
	return 0;
}
/* }}} */

/* {{{ proto public array ReflectionExtension::getConstants()
   Returns an associative array containing this extension's constants and their values */
GEAR_METHOD(reflection_extension, getConstants)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	array_init(return_value);
	gear_hash_apply_with_arguments(EG(gear_constants), (apply_func_args_t) _addconstant, 2, return_value, cAPI->capi_number);
}
/* }}} */

/* {{{ _addinientry */
static int _addinientry(zval *el, int num_args, va_list args, gear_hash_key *hash_key)
{
	gear_ics_entry *ics_entry = (gear_ics_entry*)Z_PTR_P(el);
	zval *retval = va_arg(args, zval*);
	int number = va_arg(args, int);

	if (number == ics_entry->capi_number) {
		if (ics_entry->value) {
			zval zv;

			ZVAL_STR_COPY(&zv, ics_entry->value);
			gear_symtable_update(Z_ARRVAL_P(retval), ics_entry->name, &zv);
		} else {
			gear_symtable_update(Z_ARRVAL_P(retval), ics_entry->name, &EG(uninitialized_zval));
		}
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

/* {{{ proto public array ReflectionExtension::getINIEntries()
   Returns an associative array containing this extension's ICS entries and their values */
GEAR_METHOD(reflection_extension, getINIEntries)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	array_init(return_value);
	gear_hash_apply_with_arguments(EG(ics_directives), (apply_func_args_t) _addinientry, 2, return_value, cAPI->capi_number);
}
/* }}} */

/* {{{ add_extension_class */
static int add_extension_class(zval *zv, int num_args, va_list args, gear_hash_key *hash_key)
{
	gear_class_entry *ce = Z_PTR_P(zv);
	zval *class_array = va_arg(args, zval*), zclass;
	struct _gear_capi_entry *cAPI = va_arg(args, struct _gear_capi_entry*);
	int add_reflection_class = va_arg(args, int);

	if ((ce->type == GEAR_INTERNAL_CLASS) && ce->info.internal.cAPI && !strcasecmp(ce->info.internal.cAPI->name, cAPI->name)) {
		gear_string *name;

		if (gear_binary_strcasecmp(ZSTR_VAL(ce->name), ZSTR_LEN(ce->name), ZSTR_VAL(hash_key->key), ZSTR_LEN(hash_key->key))) {
			/* This is an class alias, use alias name */
			name = hash_key->key;
		} else {
			/* Use class name */
			name = ce->name;
		}
		if (add_reflection_class) {
			gear_reflection_class_factory(ce, &zclass);
			gear_hash_update(Z_ARRVAL_P(class_array), name, &zclass);
		} else {
			add_next_index_str(class_array, gear_string_copy(name));
		}
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

/* {{{ proto public ReflectionClass[] ReflectionExtension::getClasses()
   Returns an array containing ReflectionClass objects for all classes of this extension */
GEAR_METHOD(reflection_extension, getClasses)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	array_init(return_value);
	gear_hash_apply_with_arguments(EG(class_table), (apply_func_args_t) add_extension_class, 3, return_value, cAPI, 1);
}
/* }}} */

/* {{{ proto public array ReflectionExtension::getClassNames()
   Returns an array containing all names of all classes of this extension */
GEAR_METHOD(reflection_extension, getClassNames)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	array_init(return_value);
	gear_hash_apply_with_arguments(EG(class_table), (apply_func_args_t) add_extension_class, 3, return_value, cAPI, 0);
}
/* }}} */

/* {{{ proto public array ReflectionExtension::getDependencies()
   Returns an array containing all names of all extensions this extension depends on */
GEAR_METHOD(reflection_extension, getDependencies)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;
	const gear_capi_dep *dep;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	dep = cAPI->deps;

	if (!dep)
	{
		ZVAL_EMPTY_ARRAY(return_value);
		return;
	}

	array_init(return_value);
	while(dep->name) {
		gear_string *relation;
		char *rel_type;
		size_t len = 0;

		switch(dep->type) {
			case CAPI_DEP_REQUIRED:
				rel_type = "Required";
				len += sizeof("Required") - 1;
				break;
			case CAPI_DEP_CONFLICTS:
				rel_type = "Conflicts";
				len += sizeof("Conflicts") - 1;
				break;
			case CAPI_DEP_OPTIONAL:
				rel_type = "Optional";
				len += sizeof("Optional") - 1;
				break;
			default:
				rel_type = "Error"; /* shouldn't happen */
				len += sizeof("Error") - 1;
				break;
		}

		if (dep->rel) {
			len += strlen(dep->rel) + 1;
		}

		if (dep->version) {
			len += strlen(dep->version) + 1;
		}

		relation = gear_string_alloc(len, 0);
		snprintf(ZSTR_VAL(relation), ZSTR_LEN(relation) + 1, "%s%s%s%s%s",
						rel_type,
						dep->rel ? " " : "",
						dep->rel ? dep->rel : "",
						dep->version ? " " : "",
						dep->version ? dep->version : "");
		add_assoc_str(return_value, dep->name, relation);
		dep++;
	}
}
/* }}} */

/* {{{ proto public void ReflectionExtension::info()
       Prints hyssinfo block for the extension */
GEAR_METHOD(reflection_extension, info)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	hyss_info_print_capi(cAPI);
}
/* }}} */

/* {{{ proto public bool ReflectionExtension::isPersistent()
       Returns whether this extension is persistent */
GEAR_METHOD(reflection_extension, isPersistent)
{
	reflection_object *intern;
    gear_capi_entry *cAPI;

    if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	RETURN_BOOL(cAPI->type == CAPI_PERSISTENT);
}
/* }}} */

/* {{{ proto public bool ReflectionExtension::isTemporary()
       Returns whether this extension is temporary */
GEAR_METHOD(reflection_extension, isTemporary)
{
	reflection_object *intern;
	gear_capi_entry *cAPI;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(cAPI);

	RETURN_BOOL(cAPI->type == CAPI_TEMPORARY);
}
/* }}} */

/* {{{ proto public static mixed ReflectionGearExtension::export(string name [, bool return]) throws ReflectionException
 *    Exports a reflection object. Returns the output if TRUE is specified for return, printing it otherwise. */
GEAR_METHOD(reflection_gear_extension, export)
{
	_reflection_export(INTERNAL_FUNCTION_PARAM_PASSTHRU, reflection_gear_extension_ptr, 1);
}
/* }}} */

/* {{{ proto public void ReflectionGearExtension::__construct(string name)
       Constructor. Throws an Exception in case the given Gear extension does not exist */
GEAR_METHOD(reflection_gear_extension, __construct)
{
	zval name;
	zval *object;
	reflection_object *intern;
	gear_extension *extension;
	char *name_str;
	size_t name_len;

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "s", &name_str, &name_len) == FAILURE) {
		return;
	}

	object = getThis();
	intern = Z_REFLECTION_P(object);

	extension = gear_get_extension(name_str);
	if (!extension) {
		gear_throw_exception_ex(reflection_exception_ptr, 0,
				"Gear Extension %s does not exist", name_str);
		return;
	}
	ZVAL_STRING(&name, extension->name);
	reflection_update_property_name(object, &name);
	intern->ptr = extension;
	intern->ref_type = REF_TYPE_OTHER;
	intern->ce = NULL;
}
/* }}} */

/* {{{ proto public string ReflectionGearExtension::__toString()
       Returns a string representation */
GEAR_METHOD(reflection_gear_extension, __toString)
{
	reflection_object *intern;
	gear_extension *extension;
	smart_str str = {0};

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(extension);
	_gear_extension_string(&str, extension, "");
	RETURN_STR(smart_str_extract(&str));
}
/* }}} */

/* {{{ proto public string ReflectionGearExtension::getName()
       Returns the name of this Gear extension */
GEAR_METHOD(reflection_gear_extension, getName)
{
	reflection_object *intern;
	gear_extension *extension;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(extension);

	RETURN_STRING(extension->name);
}
/* }}} */

/* {{{ proto public string ReflectionGearExtension::getVersion()
       Returns the version information of this Gear extension */
GEAR_METHOD(reflection_gear_extension, getVersion)
{
	reflection_object *intern;
	gear_extension *extension;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(extension);

	if (extension->version) {
		RETURN_STRING(extension->version);
	} else {
		RETURN_EMPTY_STRING();
	}
}
/* }}} */

/* {{{ proto public void ReflectionGearExtension::getAuthor()
 * Returns the name of this Gear extension's author */
GEAR_METHOD(reflection_gear_extension, getAuthor)
{
	reflection_object *intern;
	gear_extension *extension;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(extension);

	if (extension->author) {
		RETURN_STRING(extension->author);
	} else {
		RETURN_EMPTY_STRING();
	}
}
/* }}} */

/* {{{ proto public void ReflectionGearExtension::getURL()
       Returns this Gear extension's URL*/
GEAR_METHOD(reflection_gear_extension, getURL)
{
	reflection_object *intern;
	gear_extension *extension;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(extension);

	if (extension->URL) {
		RETURN_STRING(extension->URL);
	} else {
		RETURN_EMPTY_STRING();
	}
}
/* }}} */

/* {{{ proto public void ReflectionGearExtension::getCopyright()
       Returns this Gear extension's copyright information */
GEAR_METHOD(reflection_gear_extension, getCopyright)
{
	reflection_object *intern;
	gear_extension *extension;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	GET_REFLECTION_OBJECT_PTR(extension);

	if (extension->copyright) {
		RETURN_STRING(extension->copyright);
	} else {
		RETURN_EMPTY_STRING();
	}
}
/* }}} */

/* {{{ method tables */
static const gear_function_entry reflection_exception_functions[] = {
	HYSS_FE_END
};

GEAR_BEGIN_ARG_INFO(arginfo_reflection__void, 0)
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO(arginfo_reflection_getModifierNames, 0)
	GEAR_ARG_INFO(0, modifiers)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_export, 0, 0, 1)
	GEAR_ARG_OBJ_INFO(0, reflector, Reflector, 0)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_functions[] = {
	GEAR_ME(reflection, getModifierNames, arginfo_reflection_getModifierNames, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	GEAR_ME(reflection, export, arginfo_reflection_export, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_FE_END
};

static const gear_function_entry reflector_functions[] = {
	GEAR_FENTRY(export, NULL, NULL, GEAR_ACC_STATIC|GEAR_ACC_ABSTRACT|GEAR_ACC_PUBLIC)
	GEAR_ABSTRACT_ME(reflector, __toString, arginfo_reflection__void)
	HYSS_FE_END
};

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_function_export, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_function___construct, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_function_invoke, 0, 0, 0)
	GEAR_ARG_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_function_invokeArgs, 0)
	GEAR_ARG_ARRAY_INFO(0, args, 0)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_function_abstract_functions[] = {
	GEAR_ME(reflection, __clone, arginfo_reflection__void, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(reflection_function, inNamespace, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, isClosure, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, isDeprecated, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, isInternal, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, isUserDefined, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, isGenerator, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, isVariadic, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getClosureThis, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getClosureScopeClass, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getDocComment, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getEndLine, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getExtension, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getExtensionName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getFileName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getNamespaceName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getNumberOfParameters, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getNumberOfRequiredParameters, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getParameters, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getShortName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getStartLine, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getStaticVariables, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, returnsReference, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, hasReturnType, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, getReturnType, arginfo_reflection__void, 0)
	HYSS_FE_END
};

static const gear_function_entry reflection_function_functions[] = {
	GEAR_ME(reflection_function, __construct, arginfo_reflection_function___construct, 0)
	GEAR_ME(reflection_function, __toString, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, export, arginfo_reflection_function_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_function, isDisabled, arginfo_reflection__void, 0)
	GEAR_ME(reflection_function, invoke, arginfo_reflection_function_invoke, 0)
	GEAR_ME(reflection_function, invokeArgs, arginfo_reflection_function_invokeArgs, 0)
	GEAR_ME(reflection_function, getClosure, arginfo_reflection__void, 0)
	HYSS_FE_END
};

GEAR_BEGIN_ARG_INFO(arginfo_reflection_generator___construct, 0)
	GEAR_ARG_INFO(0, generator)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_generator_trace, 0, 0, 0)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_generator_functions[] = {
	GEAR_ME(reflection_generator, __construct, arginfo_reflection_generator___construct, 0)
	GEAR_ME(reflection_generator, getExecutingLine, arginfo_reflection__void, 0)
	GEAR_ME(reflection_generator, getExecutingFile, arginfo_reflection__void, 0)
	GEAR_ME(reflection_generator, getTrace, arginfo_reflection_generator_trace, 0)
	GEAR_ME(reflection_generator, getFunction, arginfo_reflection__void, 0)
	GEAR_ME(reflection_generator, getThis, arginfo_reflection__void, 0)
	GEAR_ME(reflection_generator, getExecutingGenerator, arginfo_reflection__void, 0)
	HYSS_FE_END
};

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_method_export, 0, 0, 2)
	GEAR_ARG_INFO(0, class)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_method___construct, 0, 0, 1)
	GEAR_ARG_INFO(0, class_or_method)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_method_invoke, 0)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_method_invokeArgs, 0)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_ARRAY_INFO(0, args, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_method_setAccessible, 0)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_method_getClosure, 0)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_method_functions[] = {
	GEAR_ME(reflection_method, export, arginfo_reflection_method_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_method, __construct, arginfo_reflection_method___construct, 0)
	GEAR_ME(reflection_method, __toString, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, isPublic, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, isPrivate, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, isProtected, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, isAbstract, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, isFinal, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, isStatic, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, isConstructor, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, isDestructor, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, getClosure, arginfo_reflection_method_getClosure, 0)
	GEAR_ME(reflection_method, getModifiers, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, invoke, arginfo_reflection_method_invoke, 0)
	GEAR_ME(reflection_method, invokeArgs, arginfo_reflection_method_invokeArgs, 0)
	GEAR_ME(reflection_method, getDeclaringClass, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, getPrototype, arginfo_reflection__void, 0)
	GEAR_ME(reflection_method, setAccessible, arginfo_reflection_method_setAccessible, 0)
	HYSS_FE_END
};


GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_class_export, 0, 0, 1)
	GEAR_ARG_INFO(0, argument)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class___construct, 0)
	GEAR_ARG_INFO(0, argument)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_class_getStaticPropertyValue, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, default)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_setStaticPropertyValue, 0)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_hasMethod, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_getMethod, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_class_getMethods, 0, 0, 0)
	GEAR_ARG_INFO(0, filter)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_hasProperty, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_getProperty, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_class_getProperties, 0, 0, 0)
	GEAR_ARG_INFO(0, filter)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_hasConstant, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_getConstant, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_isInstance, 0)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_newInstance, 0)
	GEAR_ARG_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_newInstanceWithoutConstructor, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_class_newInstanceArgs, 0, 0, 0)
	GEAR_ARG_ARRAY_INFO(0, args, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_isSubclassOf, 0)
	GEAR_ARG_INFO(0, class)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_class_implementsInterface, 0)
	GEAR_ARG_INFO(0, interface)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_class_functions[] = {
	GEAR_ME(reflection, __clone, arginfo_reflection__void, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(reflection_class, export, arginfo_reflection_class_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_class, __construct, arginfo_reflection_class___construct, 0)
	GEAR_ME(reflection_class, __toString, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isInternal, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isUserDefined, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isAnonymous, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isInstantiable, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isCloneable, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getFileName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getStartLine, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getEndLine, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getDocComment, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getConstructor, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, hasMethod, arginfo_reflection_class_hasMethod, 0)
	GEAR_ME(reflection_class, getMethod, arginfo_reflection_class_getMethod, 0)
	GEAR_ME(reflection_class, getMethods, arginfo_reflection_class_getMethods, 0)
	GEAR_ME(reflection_class, hasProperty, arginfo_reflection_class_hasProperty, 0)
	GEAR_ME(reflection_class, getProperty, arginfo_reflection_class_getProperty, 0)
	GEAR_ME(reflection_class, getProperties, arginfo_reflection_class_getProperties, 0)
	GEAR_ME(reflection_class, hasConstant, arginfo_reflection_class_hasConstant, 0)
	GEAR_ME(reflection_class, getConstants, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getReflectionConstants, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getConstant, arginfo_reflection_class_getConstant, 0)
	GEAR_ME(reflection_class, getReflectionConstant, arginfo_reflection_class_getConstant, 0)
	GEAR_ME(reflection_class, getInterfaces, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getInterfaceNames, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isInterface, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getTraits, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getTraitNames, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getTraitAliases, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isTrait, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isAbstract, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isFinal, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getModifiers, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isInstance, arginfo_reflection_class_isInstance, 0)
	GEAR_ME(reflection_class, newInstance, arginfo_reflection_class_newInstance, 0)
	GEAR_ME(reflection_class, newInstanceWithoutConstructor, arginfo_reflection_class_newInstanceWithoutConstructor, 0)
	GEAR_ME(reflection_class, newInstanceArgs, arginfo_reflection_class_newInstanceArgs, 0)
	GEAR_ME(reflection_class, getParentClass, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isSubclassOf, arginfo_reflection_class_isSubclassOf, 0)
	GEAR_ME(reflection_class, getStaticProperties, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getStaticPropertyValue, arginfo_reflection_class_getStaticPropertyValue, 0)
	GEAR_ME(reflection_class, setStaticPropertyValue, arginfo_reflection_class_setStaticPropertyValue, 0)
	GEAR_ME(reflection_class, getDefaultProperties, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, isIterable, arginfo_reflection__void, 0)
	GEAR_MALIAS(reflection_class, isIterateable, isIterable, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, implementsInterface, arginfo_reflection_class_implementsInterface, 0)
	GEAR_ME(reflection_class, getExtension, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getExtensionName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, inNamespace, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getNamespaceName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class, getShortName, arginfo_reflection__void, 0)
	HYSS_FE_END
};


GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_object_export, 0, 0, 1)
	GEAR_ARG_INFO(0, argument)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_object___construct, 0)
	GEAR_ARG_INFO(0, argument)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_object_functions[] = {
	GEAR_ME(reflection_object, export, arginfo_reflection_object_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_object, __construct, arginfo_reflection_object___construct, 0)
	HYSS_FE_END
};


GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_property_export, 0, 0, 2)
	GEAR_ARG_INFO(0, class)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_property___construct, 0, 0, 2)
	GEAR_ARG_INFO(0, class)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_property_getValue, 0, 0, 0)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_property_setValue, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_property_setAccessible, 0)
	GEAR_ARG_INFO(0, visible)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_property_functions[] = {
	GEAR_ME(reflection, __clone, arginfo_reflection__void, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(reflection_property, export, arginfo_reflection_property_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_property, __construct, arginfo_reflection_property___construct, 0)
	GEAR_ME(reflection_property, __toString, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, getName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, getValue, arginfo_reflection_property_getValue, 0)
	GEAR_ME(reflection_property, setValue, arginfo_reflection_property_setValue, 0)
	GEAR_ME(reflection_property, isPublic, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, isPrivate, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, isProtected, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, isStatic, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, isDefault, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, getModifiers, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, getDeclaringClass, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, getDocComment, arginfo_reflection__void, 0)
	GEAR_ME(reflection_property, setAccessible, arginfo_reflection_property_setAccessible, 0)
	HYSS_FE_END
};

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_class_constant_export, 0, 0, 2)
	GEAR_ARG_INFO(0, class)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_class_constant___construct, 0, 0, 2)
	GEAR_ARG_INFO(0, class)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_class_constant_functions[] = {
	GEAR_ME(reflection, __clone, arginfo_reflection__void, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(reflection_class_constant, export, arginfo_reflection_class_constant_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_class_constant, __construct, arginfo_reflection_class_constant___construct, 0)
	GEAR_ME(reflection_class_constant, __toString, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class_constant, getName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class_constant, getValue, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class_constant, isPublic, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class_constant, isPrivate, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class_constant, isProtected, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class_constant, getModifiers, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class_constant, getDeclaringClass, arginfo_reflection__void, 0)
	GEAR_ME(reflection_class_constant, getDocComment, arginfo_reflection__void, 0)
	HYSS_FE_END
};

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_parameter_export, 0, 0, 2)
	GEAR_ARG_INFO(0, function)
	GEAR_ARG_INFO(0, parameter)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_parameter___construct, 0)
	GEAR_ARG_INFO(0, function)
	GEAR_ARG_INFO(0, parameter)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_parameter_functions[] = {
	GEAR_ME(reflection, __clone, arginfo_reflection__void, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(reflection_parameter, export, arginfo_reflection_parameter_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_parameter, __construct, arginfo_reflection_parameter___construct, 0)
	GEAR_ME(reflection_parameter, __toString, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, getName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, isPassedByReference, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, canBePassedByValue, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, getDeclaringFunction, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, getDeclaringClass, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, getClass, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, hasType, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, getType, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, isArray, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, isCallable, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, allowsNull, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, getPosition, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, isOptional, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, isDefaultValueAvailable, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, getDefaultValue, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, isDefaultValueConstant, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, getDefaultValueConstantName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_parameter, isVariadic, arginfo_reflection__void, 0)
	HYSS_FE_END
};

static const gear_function_entry reflection_type_functions[] = {
	GEAR_ME(reflection, __clone, arginfo_reflection__void, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(reflection_type, allowsNull, arginfo_reflection__void, 0)
	GEAR_ME(reflection_type, isBuiltin, arginfo_reflection__void, 0)
	/* ReflectionType::__toString() is deprecated, but we currently do not mark it as such
	 * due to bad interaction with the HYSSUnit error handler and exceptions in __toString().
	 * See PR2137. */
	GEAR_ME(reflection_type, __toString, arginfo_reflection__void, 0)
	HYSS_FE_END
};

static const gear_function_entry reflection_named_type_functions[] = {
	GEAR_ME(reflection_named_type, getName, arginfo_reflection__void, 0)
	HYSS_FE_END
};

GEAR_BEGIN_ARG_INFO_EX(arginfo_reflection_extension_export, 0, 0, 1)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, return)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_reflection_extension___construct, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_extension_functions[] = {
	GEAR_ME(reflection, __clone, arginfo_reflection__void, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(reflection_extension, export, arginfo_reflection_extension_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_extension, __construct, arginfo_reflection_extension___construct, 0)
	GEAR_ME(reflection_extension, __toString, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, getName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, getVersion, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, getFunctions, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, getConstants, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, getINIEntries, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, getClasses, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, getClassNames, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, getDependencies, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, info, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, isPersistent, arginfo_reflection__void, 0)
	GEAR_ME(reflection_extension, isTemporary, arginfo_reflection__void, 0)
	HYSS_FE_END
};

GEAR_BEGIN_ARG_INFO(arginfo_reflection_gear_extension___construct, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

static const gear_function_entry reflection_gear_extension_functions[] = {
	GEAR_ME(reflection, __clone, arginfo_reflection__void, GEAR_ACC_PRIVATE|GEAR_ACC_FINAL)
	GEAR_ME(reflection_gear_extension, export, arginfo_reflection_extension_export, GEAR_ACC_STATIC|GEAR_ACC_PUBLIC)
	GEAR_ME(reflection_gear_extension, __construct, arginfo_reflection_gear_extension___construct, 0)
	GEAR_ME(reflection_gear_extension, __toString, arginfo_reflection__void, 0)
	GEAR_ME(reflection_gear_extension, getName, arginfo_reflection__void, 0)
	GEAR_ME(reflection_gear_extension, getVersion, arginfo_reflection__void, 0)
	GEAR_ME(reflection_gear_extension, getAuthor, arginfo_reflection__void, 0)
	GEAR_ME(reflection_gear_extension, getURL, arginfo_reflection__void, 0)
	GEAR_ME(reflection_gear_extension, getCopyright, arginfo_reflection__void, 0)
	HYSS_FE_END
};
/* }}} */

static const gear_function_entry reflection_ext_functions[] = { /* {{{ */
	HYSS_FE_END
}; /* }}} */

/* {{{ _reflection_write_property */
static void _reflection_write_property(zval *object, zval *member, zval *value, void **cache_slot)
{
	if ((Z_TYPE_P(member) == IS_STRING)
		&& gear_hash_exists(&Z_OBJCE_P(object)->properties_info, Z_STR_P(member))
		&& ((Z_STRLEN_P(member) == sizeof("name") - 1  && !memcmp(Z_STRVAL_P(member), "name",  sizeof("name")))
			|| (Z_STRLEN_P(member) == sizeof("class") - 1 && !memcmp(Z_STRVAL_P(member), "class", sizeof("class")))))
	{
		gear_throw_exception_ex(reflection_exception_ptr, 0,
			"Cannot set read-only property %s::$%s", ZSTR_VAL(Z_OBJCE_P(object)->name), Z_STRVAL_P(member));
	}
	else
	{
		gear_std_write_property(object, member, value, cache_slot);
	}
}
/* }}} */

HYSS_MINIT_FUNCTION(reflection) /* {{{ */
{
	gear_class_entry _reflection_entry;

	memcpy(&reflection_object_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	reflection_object_handlers.offset = XtOffsetOf(reflection_object, zo);
	reflection_object_handlers.free_obj = reflection_free_objects_storage;
	reflection_object_handlers.clone_obj = NULL;
	reflection_object_handlers.write_property = _reflection_write_property;
	reflection_object_handlers.get_gc = reflection_get_gc;

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionException", reflection_exception_functions);
	reflection_exception_ptr = gear_register_internal_class_ex(&_reflection_entry, gear_ce_exception);

	INIT_CLASS_ENTRY(_reflection_entry, "Reflection", reflection_functions);
	reflection_ptr = gear_register_internal_class(&_reflection_entry);

	INIT_CLASS_ENTRY(_reflection_entry, "Reflector", reflector_functions);
	reflector_ptr = gear_register_internal_interface(&_reflection_entry);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionFunctionAbstract", reflection_function_abstract_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_function_abstract_ptr = gear_register_internal_class(&_reflection_entry);
	gear_class_implements(reflection_function_abstract_ptr, 1, reflector_ptr);
	gear_declare_property_string(reflection_function_abstract_ptr, "name", sizeof("name")-1, "", GEAR_ACC_ABSTRACT);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionFunction", reflection_function_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_function_ptr = gear_register_internal_class_ex(&_reflection_entry, reflection_function_abstract_ptr);
	gear_declare_property_string(reflection_function_ptr, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);

	REGISTER_REFLECTION_CLASS_CONST_LONG(function, "IS_DEPRECATED", GEAR_ACC_DEPRECATED);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionGenerator", reflection_generator_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_generator_ptr = gear_register_internal_class(&_reflection_entry);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionParameter", reflection_parameter_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_parameter_ptr = gear_register_internal_class(&_reflection_entry);
	gear_class_implements(reflection_parameter_ptr, 1, reflector_ptr);
	gear_declare_property_string(reflection_parameter_ptr, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionType", reflection_type_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_type_ptr = gear_register_internal_class(&_reflection_entry);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionNamedType", reflection_named_type_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_named_type_ptr = gear_register_internal_class_ex(&_reflection_entry, reflection_type_ptr);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionMethod", reflection_method_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_method_ptr = gear_register_internal_class_ex(&_reflection_entry, reflection_function_abstract_ptr);
	gear_declare_property_string(reflection_method_ptr, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);
	gear_declare_property_string(reflection_method_ptr, "class", sizeof("class")-1, "", GEAR_ACC_PUBLIC);

	REGISTER_REFLECTION_CLASS_CONST_LONG(method, "IS_STATIC", GEAR_ACC_STATIC);
	REGISTER_REFLECTION_CLASS_CONST_LONG(method, "IS_PUBLIC", GEAR_ACC_PUBLIC);
	REGISTER_REFLECTION_CLASS_CONST_LONG(method, "IS_PROTECTED", GEAR_ACC_PROTECTED);
	REGISTER_REFLECTION_CLASS_CONST_LONG(method, "IS_PRIVATE", GEAR_ACC_PRIVATE);
	REGISTER_REFLECTION_CLASS_CONST_LONG(method, "IS_ABSTRACT", GEAR_ACC_ABSTRACT);
	REGISTER_REFLECTION_CLASS_CONST_LONG(method, "IS_FINAL", GEAR_ACC_FINAL);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionClass", reflection_class_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_class_ptr = gear_register_internal_class(&_reflection_entry);
	gear_class_implements(reflection_class_ptr, 1, reflector_ptr);
	gear_declare_property_string(reflection_class_ptr, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);

	REGISTER_REFLECTION_CLASS_CONST_LONG(class, "IS_IMPLICIT_ABSTRACT", GEAR_ACC_IMPLICIT_ABSTRACT_CLASS);
	REGISTER_REFLECTION_CLASS_CONST_LONG(class, "IS_EXPLICIT_ABSTRACT", GEAR_ACC_EXPLICIT_ABSTRACT_CLASS);
	REGISTER_REFLECTION_CLASS_CONST_LONG(class, "IS_FINAL", GEAR_ACC_FINAL);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionObject", reflection_object_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_object_ptr = gear_register_internal_class_ex(&_reflection_entry, reflection_class_ptr);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionProperty", reflection_property_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_property_ptr = gear_register_internal_class(&_reflection_entry);
	gear_class_implements(reflection_property_ptr, 1, reflector_ptr);
	gear_declare_property_string(reflection_property_ptr, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);
	gear_declare_property_string(reflection_property_ptr, "class", sizeof("class")-1, "", GEAR_ACC_PUBLIC);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionClassConstant", reflection_class_constant_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_class_constant_ptr = gear_register_internal_class(&_reflection_entry);
	gear_class_implements(reflection_class_constant_ptr, 1, reflector_ptr);
	gear_declare_property_string(reflection_class_constant_ptr, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);
	gear_declare_property_string(reflection_class_constant_ptr, "class", sizeof("class")-1, "", GEAR_ACC_PUBLIC);

	REGISTER_REFLECTION_CLASS_CONST_LONG(property, "IS_STATIC", GEAR_ACC_STATIC);
	REGISTER_REFLECTION_CLASS_CONST_LONG(property, "IS_PUBLIC", GEAR_ACC_PUBLIC);
	REGISTER_REFLECTION_CLASS_CONST_LONG(property, "IS_PROTECTED", GEAR_ACC_PROTECTED);
	REGISTER_REFLECTION_CLASS_CONST_LONG(property, "IS_PRIVATE", GEAR_ACC_PRIVATE);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionExtension", reflection_extension_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_extension_ptr = gear_register_internal_class(&_reflection_entry);
	gear_class_implements(reflection_extension_ptr, 1, reflector_ptr);
	gear_declare_property_string(reflection_extension_ptr, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);

	INIT_CLASS_ENTRY(_reflection_entry, "ReflectionGearExtension", reflection_gear_extension_functions);
	_reflection_entry.create_object = reflection_objects_new;
	reflection_gear_extension_ptr = gear_register_internal_class(&_reflection_entry);
	gear_class_implements(reflection_gear_extension_ptr, 1, reflector_ptr);
	gear_declare_property_string(reflection_gear_extension_ptr, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);

	return SUCCESS;
} /* }}} */

HYSS_MINFO_FUNCTION(reflection) /* {{{ */
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "Reflection", "enabled");
	hyss_info_print_table_end();
} /* }}} */

gear_capi_entry reflection_capi_entry = { /* {{{ */
	STANDARD_CAPI_HEADER,
	"Reflection",
	reflection_ext_functions,
	HYSS_MINIT(reflection),
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(reflection),
	HYSS_REFLECTION_VERSION,
	STANDARD_CAPI_PROPERTIES
}; /* }}} */

