/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes & prototypes */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_readline.h"
#include "readline_cli.h"

#if HAVE_LIBREADLINE || HAVE_LIBEDIT

#ifndef HAVE_RL_COMPLETION_MATCHES
#define rl_completion_matches completion_matches
#endif

#ifdef HAVE_LIBEDIT
#include <editline/readline.h>
#else
#include <readline/readline.h>
#include <readline/history.h>
#endif

HYSS_FUNCTION(readline);
HYSS_FUNCTION(readline_add_history);
HYSS_FUNCTION(readline_info);
HYSS_FUNCTION(readline_clear_history);
#ifndef HAVE_LIBEDIT
HYSS_FUNCTION(readline_list_history);
#endif
HYSS_FUNCTION(readline_read_history);
HYSS_FUNCTION(readline_write_history);
HYSS_FUNCTION(readline_completion_function);

#if HAVE_RL_CALLBACK_READ_CHAR
HYSS_FUNCTION(readline_callback_handler_install);
HYSS_FUNCTION(readline_callback_read_char);
HYSS_FUNCTION(readline_callback_handler_remove);
HYSS_FUNCTION(readline_redisplay);
HYSS_FUNCTION(readline_on_new_line);

static zval _prepped_callback;

#endif

static zval _readline_completion;
static zval _readline_array;

HYSS_MINIT_FUNCTION(readline);
HYSS_MSHUTDOWN_FUNCTION(readline);
HYSS_RSHUTDOWN_FUNCTION(readline);
HYSS_MINFO_FUNCTION(readline);

/* }}} */

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_readline, 0, 0, 0)
	GEAR_ARG_INFO(0, prompt)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_readline_info, 0, 0, 0)
	GEAR_ARG_INFO(0, varname)
	GEAR_ARG_INFO(0, newvalue)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_readline_add_history, 0, 0, 1)
	GEAR_ARG_INFO(0, prompt)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_readline_clear_history, 0)
GEAR_END_ARG_INFO()

#ifndef HAVE_LIBEDIT
GEAR_BEGIN_ARG_INFO(arginfo_readline_list_history, 0)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_readline_read_history, 0, 0, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_readline_write_history, 0, 0, 0)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_readline_completion_function, 0, 0, 1)
	GEAR_ARG_INFO(0, funcname)
GEAR_END_ARG_INFO()

#if HAVE_RL_CALLBACK_READ_CHAR
GEAR_BEGIN_ARG_INFO_EX(arginfo_readline_callback_handler_install, 0, 0, 2)
	GEAR_ARG_INFO(0, prompt)
	GEAR_ARG_INFO(0, callback)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_readline_callback_read_char, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_readline_callback_handler_remove, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_readline_redisplay, 0)
GEAR_END_ARG_INFO()

#if HAVE_RL_ON_NEW_LINE
GEAR_BEGIN_ARG_INFO(arginfo_readline_on_new_line, 0)
GEAR_END_ARG_INFO()
#endif
#endif
/* }}} */

/* {{{ cAPI stuff */
static const gear_function_entry hyss_readline_functions[] = {
	HYSS_FE(readline,	   		        arginfo_readline)
	HYSS_FE(readline_info,  	            arginfo_readline_info)
	HYSS_FE(readline_add_history, 		arginfo_readline_add_history)
	HYSS_FE(readline_clear_history, 		arginfo_readline_clear_history)
#ifndef HAVE_LIBEDIT
	HYSS_FE(readline_list_history, 		arginfo_readline_list_history)
#endif
	HYSS_FE(readline_read_history, 		arginfo_readline_read_history)
	HYSS_FE(readline_write_history, 		arginfo_readline_write_history)
	HYSS_FE(readline_completion_function,arginfo_readline_completion_function)
#if HAVE_RL_CALLBACK_READ_CHAR
	HYSS_FE(readline_callback_handler_install, arginfo_readline_callback_handler_install)
	HYSS_FE(readline_callback_read_char,			arginfo_readline_callback_read_char)
	HYSS_FE(readline_callback_handler_remove,	arginfo_readline_callback_handler_remove)
	HYSS_FE(readline_redisplay, arginfo_readline_redisplay)
#endif
#if HAVE_RL_ON_NEW_LINE
	HYSS_FE(readline_on_new_line, arginfo_readline_on_new_line)
#endif
	HYSS_FE_END
};

gear_capi_entry readline_capi_entry = {
	STANDARD_CAPI_HEADER,
	"readline",
	hyss_readline_functions,
	HYSS_MINIT(readline),
	HYSS_MSHUTDOWN(readline),
	NULL,
	HYSS_RSHUTDOWN(readline),
	HYSS_MINFO(readline),
	HYSS_READLINE_VERSION,
	STANDARD_CAPI_PROPERTIES
};

#ifdef COMPILE_DL_READLINE
GEAR_GET_CAPI(readline)
#endif

HYSS_MINIT_FUNCTION(readline)
{
#if HAVE_LIBREADLINE
		/* libedit don't need this call which set the tty in cooked mode */
	using_history();
#endif
	ZVAL_UNDEF(&_readline_completion);
#if HAVE_RL_CALLBACK_READ_CHAR
	ZVAL_UNDEF(&_prepped_callback);
#endif
   	return HYSS_MINIT(cli_readline)(INIT_FUNC_ARGS_PASSTHRU);
}

HYSS_MSHUTDOWN_FUNCTION(readline)
{
	return HYSS_MSHUTDOWN(cli_readline)(SHUTDOWN_FUNC_ARGS_PASSTHRU);
}

HYSS_RSHUTDOWN_FUNCTION(readline)
{
	zval_ptr_dtor(&_readline_completion);
	ZVAL_UNDEF(&_readline_completion);
#if HAVE_RL_CALLBACK_READ_CHAR
	if (Z_TYPE(_prepped_callback) != IS_UNDEF) {
		rl_callback_handler_remove();
		zval_ptr_dtor(&_prepped_callback);
		ZVAL_UNDEF(&_prepped_callback);
	}
#endif

	return SUCCESS;
}

HYSS_MINFO_FUNCTION(readline)
{
	HYSS_MINFO(cli_readline)(GEAR_CAPI_INFO_FUNC_ARGS_PASSTHRU);
}

/* }}} */

/* {{{ proto string readline([string prompt])
   Reads a line */
HYSS_FUNCTION(readline)
{
	char *prompt = NULL;
	size_t prompt_len;
	char *result;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "|s!", &prompt, &prompt_len)) {
		RETURN_FALSE;
	}

	result = readline(prompt);

	if (! result) {
		RETURN_FALSE;
	} else {
		RETVAL_STRING(result);
		free(result);
	}
}

/* }}} */

#define SAFE_STRING(s) ((s)?(char*)(s):"")

/* {{{ proto mixed readline_info([string varname [, string newvalue]])
   Gets/sets various internal readline variables. */
HYSS_FUNCTION(readline_info)
{
	char *what = NULL;
	zval *value = NULL;
	size_t what_len, oldval;
	char *oldstr;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|sz", &what, &what_len, &value) == FAILURE) {
		return;
	}

	if (!what) {
		array_init(return_value);
		add_assoc_string(return_value,"line_buffer",SAFE_STRING(rl_line_buffer));
		add_assoc_long(return_value,"point",rl_point);
#ifndef HYSS_WIN32
		add_assoc_long(return_value,"end",rl_end);
#endif
#ifdef HAVE_LIBREADLINE
		add_assoc_long(return_value,"mark",rl_mark);
		add_assoc_long(return_value,"done",rl_done);
		add_assoc_long(return_value,"pending_input",rl_pending_input);
		add_assoc_string(return_value,"prompt",SAFE_STRING(rl_prompt));
		add_assoc_string(return_value,"terminal_name",(char *)SAFE_STRING(rl_terminal_name));
		add_assoc_str(return_value, "completion_append_character",
			rl_completion_append_character == 0
				? ZSTR_EMPTY_ALLOC()
				: ZSTR_CHAR(rl_completion_append_character));
		add_assoc_bool(return_value,"completion_suppress_append",rl_completion_suppress_append);
#endif
#if HAVE_ERASE_EMPTY_LINE
		add_assoc_long(return_value,"erase_empty_line",rl_erase_empty_line);
#endif
#ifndef HYSS_WIN32
		add_assoc_string(return_value,"library_version",(char *)SAFE_STRING(rl_library_version));
#endif
		add_assoc_string(return_value,"readline_name",(char *)SAFE_STRING(rl_readline_name));
		add_assoc_long(return_value,"attempted_completion_over",rl_attempted_completion_over);
	} else {
		if (!strcasecmp(what,"line_buffer")) {
			oldstr = rl_line_buffer;
			if (value) {
				/* XXX if (rl_line_buffer) free(rl_line_buffer); */
				convert_to_string_ex(value);
				rl_line_buffer = strdup(Z_STRVAL_P(value));
			}
			RETVAL_STRING(SAFE_STRING(oldstr));
		} else if (!strcasecmp(what, "point")) {
			RETVAL_LONG(rl_point);
#ifndef HYSS_WIN32
		} else if (!strcasecmp(what, "end")) {
			RETVAL_LONG(rl_end);
#endif
#ifdef HAVE_LIBREADLINE
		} else if (!strcasecmp(what, "mark")) {
			RETVAL_LONG(rl_mark);
		} else if (!strcasecmp(what, "done")) {
			oldval = rl_done;
			if (value) {
				convert_to_long_ex(value);
				rl_done = Z_LVAL_P(value);
			}
			RETVAL_LONG(oldval);
		} else if (!strcasecmp(what, "pending_input")) {
			oldval = rl_pending_input;
			if (value) {
				convert_to_string_ex(value);
				rl_pending_input = Z_STRVAL_P(value)[0];
			}
			RETVAL_LONG(oldval);
		} else if (!strcasecmp(what, "prompt")) {
			RETVAL_STRING(SAFE_STRING(rl_prompt));
		} else if (!strcasecmp(what, "terminal_name")) {
			RETVAL_STRING((char *)SAFE_STRING(rl_terminal_name));
		} else if (!strcasecmp(what, "completion_suppress_append")) {
			oldval = rl_completion_suppress_append;
			if (value) {
				rl_completion_suppress_append = gear_is_true(value);
			}
			RETVAL_BOOL(oldval);
		} else if (!strcasecmp(what, "completion_append_character")) {
			oldval = rl_completion_append_character;
			if (value) {
				convert_to_string_ex(value)
				rl_completion_append_character = (int)Z_STRVAL_P(value)[0];
			}
			RETVAL_INTERNED_STR(
				oldval == 0 ? ZSTR_EMPTY_ALLOC() : ZSTR_CHAR(oldval));
#endif
#if HAVE_ERASE_EMPTY_LINE
		} else if (!strcasecmp(what, "erase_empty_line")) {
			oldval = rl_erase_empty_line;
			if (value) {
				convert_to_long_ex(value);
				rl_erase_empty_line = Z_LVAL_P(value);
			}
			RETVAL_LONG(oldval);
#endif
#ifndef HYSS_WIN32
		} else if (!strcasecmp(what,"library_version")) {
			RETVAL_STRING((char *)SAFE_STRING(rl_library_version));
#endif
		} else if (!strcasecmp(what, "readline_name")) {
			oldstr = (char*)rl_readline_name;
			if (value) {
				/* XXX if (rl_readline_name) free(rl_readline_name); */
				convert_to_string_ex(value);
				rl_readline_name = strdup(Z_STRVAL_P(value));
			}
			RETVAL_STRING(SAFE_STRING(oldstr));
		} else if (!strcasecmp(what, "attempted_completion_over")) {
			oldval = rl_attempted_completion_over;
			if (value) {
				convert_to_long_ex(value);
				rl_attempted_completion_over = Z_LVAL_P(value);
			}
			RETVAL_LONG(oldval);
		}
	}
}

/* }}} */
/* {{{ proto bool readline_add_history(string prompt)
   Adds a line to the history */
HYSS_FUNCTION(readline_add_history)
{
	char *arg;
	size_t arg_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &arg, &arg_len) == FAILURE) {
		return;
	}

	add_history(arg);

	RETURN_TRUE;
}

/* }}} */
/* {{{ proto bool readline_clear_history(void)
   Clears the history */
HYSS_FUNCTION(readline_clear_history)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

#if HAVE_LIBEDIT
	/* clear_history is the only function where rl_initialize
	   is not call to ensure correct allocation */
	using_history();
#endif
	clear_history();

	RETURN_TRUE;
}

/* }}} */
/* {{{ proto array readline_list_history(void)
   Lists the history */
#ifndef HAVE_LIBEDIT
HYSS_FUNCTION(readline_list_history)
{
	HIST_ENTRY **history;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	history = history_list();

	array_init(return_value);

	if (history) {
		int i;
		for (i = 0; history[i]; i++) {
			add_next_index_string(return_value,history[i]->line);
		}
	}
}
#endif
/* }}} */
/* {{{ proto bool readline_read_history([string filename])
   Reads the history */
HYSS_FUNCTION(readline_read_history)
{
	char *arg = NULL;
	size_t arg_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|p", &arg, &arg_len) == FAILURE) {
		return;
	}

	if (arg && hyss_check_open_basedir(arg)) {
		RETURN_FALSE;
	}

	/* XXX from & to NYI */
	if (read_history(arg)) {
		/* If filename is NULL, then read from `~/.history' */
		RETURN_FALSE;
	} else {
		RETURN_TRUE;
	}
}

/* }}} */
/* {{{ proto bool readline_write_history([string filename])
   Writes the history */
HYSS_FUNCTION(readline_write_history)
{
	char *arg = NULL;
	size_t arg_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|p", &arg, &arg_len) == FAILURE) {
		return;
	}

	if (arg && hyss_check_open_basedir(arg)) {
		RETURN_FALSE;
	}

	if (write_history(arg)) {
		RETURN_FALSE;
	} else {
		RETURN_TRUE;
	}
}

/* }}} */
/* {{{ proto bool readline_completion_function(string funcname)
   Readline completion function? */

static char *_readline_command_generator(const char *text, int state)
{
	HashTable  *myht = Z_ARRVAL(_readline_array);
	zval *entry;

	if (!state) {
		gear_hash_internal_pointer_reset(myht);
	}

	while ((entry = gear_hash_get_current_data(myht)) != NULL) {
		gear_hash_move_forward(myht);

		convert_to_string_ex(entry);
		if (strncmp (Z_STRVAL_P(entry), text, strlen(text)) == 0) {
			return (strdup(Z_STRVAL_P(entry)));
		}
	}

	return NULL;
}

static void _readline_string_zval(zval *ret, const char *str)
{
	if (str) {
		ZVAL_STRING(ret, (char*)str);
	} else {
		ZVAL_NULL(ret);
	}
}

static void _readline_long_zval(zval *ret, long l)
{
	ZVAL_LONG(ret, l);
}

static char **_readline_completion_cb(const char *text, int start, int end)
{
	zval params[3];
	char **matches = NULL;

	_readline_string_zval(&params[0], text);
	_readline_long_zval(&params[1], start);
	_readline_long_zval(&params[2], end);

	if (call_user_function(CG(function_table), NULL, &_readline_completion, &_readline_array, 3, params) == SUCCESS) {
		if (Z_TYPE(_readline_array) == IS_ARRAY) {
			if (gear_hash_num_elements(Z_ARRVAL(_readline_array))) {
				matches = rl_completion_matches(text,_readline_command_generator);
			} else {
				matches = malloc(sizeof(char *) * 2);
				if (!matches) {
					return NULL;
				}
				matches[0] = strdup("");
				matches[1] = NULL;
			}
		}
	}

	zval_ptr_dtor(&params[0]);
	zval_ptr_dtor(&_readline_array);

	return matches;
}

HYSS_FUNCTION(readline_completion_function)
{
	zval *arg;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "z", &arg)) {
		RETURN_FALSE;
	}

	if (!gear_is_callable(arg, 0, NULL)) {
		gear_string *name = gear_get_callable_name(arg);
		hyss_error_docref(NULL, E_WARNING, "%s is not callable", ZSTR_VAL(name));
		gear_string_release_ex(name, 0);
		RETURN_FALSE;
	}

	zval_ptr_dtor(&_readline_completion);
	ZVAL_COPY(&_readline_completion, arg);

	rl_attempted_completion_function = _readline_completion_cb;
	if (rl_attempted_completion_function == NULL) {
		RETURN_FALSE;
	}
	RETURN_TRUE;
}

/* }}} */

#if HAVE_RL_CALLBACK_READ_CHAR

static void hyss_rl_callback_handler(char *the_line)
{
	zval params[1];
	zval dummy;

	ZVAL_NULL(&dummy);

	_readline_string_zval(&params[0], the_line);

	call_user_function(CG(function_table), NULL, &_prepped_callback, &dummy, 1, params);

	zval_ptr_dtor(&params[0]);
	zval_ptr_dtor(&dummy);
}

/* {{{ proto void readline_callback_handler_install(string prompt, mixed callback)
   Initializes the readline callback interface and terminal, prints the prompt and returns immediately */
HYSS_FUNCTION(readline_callback_handler_install)
{
	zval *callback;
	char *prompt;
	size_t prompt_len;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "sz", &prompt, &prompt_len, &callback)) {
		return;
	}

	if (!gear_is_callable(callback, 0, NULL)) {
		gear_string *name = gear_get_callable_name(callback);
		hyss_error_docref(NULL, E_WARNING, "%s is not callable", ZSTR_VAL(name));
		gear_string_release_ex(name, 0);
		RETURN_FALSE;
	}

	if (Z_TYPE(_prepped_callback) != IS_UNDEF) {
		rl_callback_handler_remove();
		zval_ptr_dtor(&_prepped_callback);
	}

	ZVAL_COPY(&_prepped_callback, callback);

	rl_callback_handler_install(prompt, hyss_rl_callback_handler);

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto void readline_callback_read_char()
   Informs the readline callback interface that a character is ready for input */
HYSS_FUNCTION(readline_callback_read_char)
{
	if (Z_TYPE(_prepped_callback) != IS_UNDEF) {
		rl_callback_read_char();
	}
}
/* }}} */

/* {{{ proto bool readline_callback_handler_remove()
   Removes a previously installed callback handler and restores terminal settings */
HYSS_FUNCTION(readline_callback_handler_remove)
{
	if (Z_TYPE(_prepped_callback) != IS_UNDEF) {
		rl_callback_handler_remove();
		zval_ptr_dtor(&_prepped_callback);
		ZVAL_UNDEF(&_prepped_callback);
		RETURN_TRUE;
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto void readline_redisplay(void)
   Ask readline to redraw the display */
HYSS_FUNCTION(readline_redisplay)
{
#if HAVE_LIBEDIT
	/* seems libedit doesn't take care of rl_initialize in rl_redisplay
	 * see bug #72538 */
	using_history();
#endif
	rl_redisplay();
}
/* }}} */

#endif

#if HAVE_RL_ON_NEW_LINE
/* {{{ proto void readline_on_new_line(void)
   Inform readline that the cursor has moved to a new line */
HYSS_FUNCTION(readline_on_new_line)
{
	rl_on_new_line();
}
/* }}} */

#endif


#endif /* HAVE_LIBREADLINE */


