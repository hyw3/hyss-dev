/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "gear_smart_str_public.h"

GEAR_BEGIN_CAPI_GLOBALS(cli_readline)
	char *pager;
	char *prompt;
	smart_str *prompt_str;
GEAR_END_CAPI_GLOBALS(cli_readline)

#ifdef ZTS
# define CLIR_G(v) PBCG(cli_readline_globals_id, gear_cli_readline_globals *, v)
#else
# define CLIR_G(v) (cli_readline_globals.v)
#endif

extern HYSS_MINIT_FUNCTION(cli_readline);
extern HYSS_MSHUTDOWN_FUNCTION(cli_readline);
extern HYSS_MINFO_FUNCTION(cli_readline);

GEAR_EXTERN_CAPI_GLOBALS(cli_readline)
