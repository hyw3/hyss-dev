dnl config.m4 for extension pdo
dnl vim:se ts=2 sw=2 et:

AC_DEFUN([HYSS_PDO_HEXA_CHECK],[
  pdo_running_under_hexa=0
  case `pwd` in
    /var/tmp/hexa-build-*)
      pdo_running_under_hexa=1
      ;;
  esac

  if test "$pdo_running_under_hexa$HYSS_HEXA_VERSION" = "1"; then
    # we're running in an environment that smells like hexa,
    # and the HYSS_HEXA_VERSION env var is not set.  That implies
    # that we're running under a slightly broken hexa installer
    AC_MSG_ERROR([
PDO requires that you upgrade your HEXA installer tools. Please
do so now by running:

  % sudo hexa upgrade hexa

or by manually downloading and installing HEXA version 1.3.5 or higher.

Once you've upgraded, please re-try your PDO install.
    ])
  fi
])

HYSS_ARG_ENABLE(pdo, whether to enable PDO support,
[  --disable-pdo           Disable HYSS Data Objects support], yes)

if test "$HYSS_PDO" != "no"; then

  dnl Make sure $HYSS_PDO is 'yes' when it's not 'no' :)
  HYSS_PDO=yes

  HYSS_PDO_HEXA_CHECK

  HYSS_NEW_EXTENSION(pdo, pdo.c pdo_dbh.c pdo_stmt.c pdo_sql_parser.c pdo_sqlstate.c, $ext_shared)
  ifdef([HYSS_ADD_EXTENSION_DEP],
  [
    HYSS_ADD_EXTENSION_DEP(pdo, spl, true)
  ])

  ifdef([HYSS_INSTALL_HEADERS],
  [
    dnl Sadly, this is a complete NOP for pecl extensions
    HYSS_INSTALL_HEADERS(extslib/pdo, [hyss_pdo.h hyss_pdo_driver.h hyss_pdo_error.h])
  ])

  dnl so we always include the known-good working hack.
  HYSS_ADD_MAKEFILE_FRAGMENT
fi
