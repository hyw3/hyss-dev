/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PDO_H
#define HYSS_PDO_H

#include "gear.h"

extern gear_capi_entry pdo_capi_entry;
#define hyssext_pdo_ptr &pdo_capi_entry

#include "hyss_version.h"
#define HYSS_PDO_VERSION HYSS_VERSION

#ifdef HYSS_WIN32
#	if defined(PDO_EXPORTS) || (!defined(COMPILE_DL_PDO))
#		define PDO_API __declspec(dllexport)
#	elif defined(COMPILE_DL_PDO)
#		define PDO_API __declspec(dllimport)
#	else
#		define PDO_API /* nothing special */
#	endif
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PDO_API __attribute__ ((visibility("default")))
#else
#	define PDO_API /* nothing special */
#endif

#ifdef ZTS
# include "hypbc.h"
#endif

HYSS_MINIT_FUNCTION(pdo);
HYSS_MSHUTDOWN_FUNCTION(pdo);
HYSS_MINFO_FUNCTION(pdo);

GEAR_BEGIN_CAPI_GLOBALS(pdo)
	gear_long  global_value;
GEAR_END_CAPI_GLOBALS(pdo)

#ifdef ZTS
# define PDOG(v) PBCG(pdo_globals_id, gear_pdo_globals *, v)
#else
# define PDOG(v) (pdo_globals.v)
#endif

#define REGISTER_PDO_CLASS_CONST_LONG(const_name, value) \
	gear_declare_class_constant_long(hyss_pdo_get_dbh_ce(), const_name, sizeof(const_name)-1, (gear_long)value);

#define REGISTER_PDO_CLASS_CONST_STRING(const_name, value) \
	gear_declare_class_constant_stringl(hyss_pdo_get_dbh_ce(), const_name, sizeof(const_name)-1, value, sizeof(value)-1);

#define PDO_CONSTRUCT_CHECK	\
	if (!dbh->driver) {	\
		pdo_raise_impl_error(dbh, NULL, "00000", "PDO constructor was not called");	\
		return;	\
	}	\


#endif	/* HYSS_PDO_H */

