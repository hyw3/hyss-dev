/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <ctype.h>
#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_pdo.h"
#include "hyss_pdo_driver.h"
#include "hyss_pdo_int.h"
#include "gear_exceptions.h"
#include "extslib/spl/spl_exceptions.h"

gear_class_entry *pdo_dbh_ce, *pdo_dbstmt_ce, *pdo_row_ce;

/* for exceptional circumstances */
gear_class_entry *pdo_exception_ce;

GEAR_DECLARE_CAPI_GLOBALS(pdo)
static HYSS_GINIT_FUNCTION(pdo);

/* True global resources - no need for thread safety here */

/* the registry of PDO drivers */
HashTable pdo_driver_hash;

/* we use persistent resources for the driver connection stuff */
static int le_ppdo;

int hyss_pdo_list_entry(void) /* {{{ */
{
	return le_ppdo;
}
/* }}} */

PDO_API gear_class_entry *hyss_pdo_get_dbh_ce(void) /* {{{ */
{
	return pdo_dbh_ce;
}
/* }}} */

PDO_API gear_class_entry *hyss_pdo_get_exception(void) /* {{{ */
{
	return pdo_exception_ce;
}
/* }}} */

PDO_API char *hyss_pdo_str_tolower_dup(const char *src, int len) /* {{{ */
{
	char *dest = emalloc(len + 1);
	gear_str_tolower_copy(dest, src, len);
	return dest;
}
/* }}} */

PDO_API gear_class_entry *hyss_pdo_get_exception_base(int root) /* {{{ */
{
	if (!root) {
		return spl_ce_RuntimeException;
	}
	return gear_ce_exception;
}
/* }}} */

/* {{{ proto array pdo_drivers()
 Return array of available PDO drivers */
HYSS_FUNCTION(pdo_drivers)
{
	pdo_driver_t *pdriver;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);

	GEAR_HASH_FOREACH_PTR(&pdo_driver_hash, pdriver) {
		add_next_index_stringl(return_value, (char*)pdriver->driver_name, pdriver->driver_name_len);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO(arginfo_pdo_drivers, 0)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ pdo_functions[] */
const gear_function_entry pdo_functions[] = {
	HYSS_FE(pdo_drivers,             arginfo_pdo_drivers)
	HYSS_FE_END
};
/* }}} */

/* {{{ pdo_functions[] */
static const gear_capi_dep pdo_deps[] = {
	GEAR_CAPI_REQUIRED("spl")
	GEAR_CAPI_END
};
/* }}} */

/* {{{ pdo_capi_entry */
gear_capi_entry pdo_capi_entry = {
	STANDARD_CAPI_HEADER_EX, NULL,
	pdo_deps,
	"PDO",
	pdo_functions,
	HYSS_MINIT(pdo),
	HYSS_MSHUTDOWN(pdo),
	NULL,
	NULL,
	HYSS_MINFO(pdo),
	HYSS_PDO_VERSION,
	HYSS_CAPI_GLOBALS(pdo),
	HYSS_GINIT(pdo),
	NULL,
	NULL,
	STANDARD_CAPI_PROPERTIES_EX
};
/* }}} */

/* TODO: visit persistent handles: for each persistent statement handle,
 * remove bound parameter associations */

#ifdef COMPILE_DL_PDO
GEAR_GET_CAPI(pdo)
#endif

/* {{{ HYSS_GINIT_FUNCTION */
static HYSS_GINIT_FUNCTION(pdo)
{
	pdo_globals->global_value = 0;
}
/* }}} */

PDO_API int hyss_pdo_register_driver(const pdo_driver_t *driver) /* {{{ */
{
	if (driver->api_version != PDO_DRIVER_API) {
		gear_error(E_ERROR, "PDO: driver %s requires PDO API version " GEAR_ULONG_FMT "; this is PDO version %d",
			driver->driver_name, driver->api_version, PDO_DRIVER_API);
		return FAILURE;
	}
	if (!gear_hash_str_exists(&capi_registry, "pdo", sizeof("pdo") - 1)) {
		gear_error(E_ERROR, "You MUST load PDO before loading any PDO drivers");
		return FAILURE;	/* NOTREACHED */
	}

	return gear_hash_str_add_ptr(&pdo_driver_hash, (char*)driver->driver_name, driver->driver_name_len, (void*)driver) != NULL ? SUCCESS : FAILURE;
}
/* }}} */

PDO_API void hyss_pdo_unregister_driver(const pdo_driver_t *driver) /* {{{ */
{
	if (!gear_hash_str_exists(&capi_registry, "pdo", sizeof("pdo") - 1)) {
		return;
	}

	gear_hash_str_del(&pdo_driver_hash, (char*)driver->driver_name, driver->driver_name_len);
}
/* }}} */

pdo_driver_t *pdo_find_driver(const char *name, int namelen) /* {{{ */
{
	return gear_hash_str_find_ptr(&pdo_driver_hash, (char*)name, namelen);
}
/* }}} */

PDO_API int hyss_pdo_parse_data_source(const char *data_source, gear_ulong data_source_len, struct pdo_data_src_parser *parsed, int nparams) /* {{{ */
{
	gear_ulong i;
	int j;
	int valstart = -1;
	int semi = -1;
	int optstart = 0;
	int nlen;
	int n_matches = 0;
	int n_semicolumns = 0;

	i = 0;
	while (i < data_source_len) {
		/* looking for NAME= */

		if (data_source[i] == '\0') {
			break;
		}

		if (data_source[i] != '=') {
			++i;
			continue;
		}

		valstart = ++i;

		/* now we're looking for VALUE; or just VALUE<NUL> */
		semi = -1;
		n_semicolumns = 0;
		while (i < data_source_len) {
			if (data_source[i] == '\0') {
				semi = i++;
				break;
			}
			if (data_source[i] == ';') {
				if ((i + 1 >= data_source_len) || data_source[i+1] != ';') {
					semi = i++;
					break;
				} else {
					n_semicolumns++;
					i += 2;
					continue;
				}
			}
			++i;
		}

		if (semi == -1) {
			semi = i;
		}

		/* find the entry in the array */
		nlen = valstart - optstart - 1;
		for (j = 0; j < nparams; j++) {
			if (0 == strncmp(data_source + optstart, parsed[j].optname, nlen) && parsed[j].optname[nlen] == '\0') {
				/* got a match */
				if (parsed[j].freeme) {
					efree(parsed[j].optval);
				}

				if (n_semicolumns == 0) {
					parsed[j].optval = estrndup(data_source + valstart, semi - valstart - n_semicolumns);
				} else {
					int vlen = semi - valstart;
					const char *orig_val = data_source + valstart;
					char *new_val  = (char *) emalloc(vlen - n_semicolumns + 1);

					parsed[j].optval = new_val;

					while (vlen && *orig_val) {
						*new_val = *orig_val;
						new_val++;

						if (*orig_val == ';') {
							orig_val+=2;
							vlen-=2;
						} else {
							orig_val++;
							vlen--;
						}
					}
					*new_val = '\0';
				}

				parsed[j].freeme = 1;
				++n_matches;
				break;
			}
		}

		while (i < data_source_len && isspace(data_source[i])) {
			i++;
		}

		optstart = i;
	}

	return n_matches;
}
/* }}} */

static const char digit_vec[] = "0123456789";
PDO_API char *hyss_pdo_int64_to_str(pdo_int64_t i64) /* {{{ */
{
	char buffer[65];
	char outbuf[65] = "";
	register char *p;
	gear_long long_val;
	char *dst = outbuf;

	if (i64 < 0) {
		i64 = -i64;
		*dst++ = '-';
	}

	if (i64 == 0) {
		*dst++ = '0';
		*dst++ = '\0';
		return estrdup(outbuf);
	}

	p = &buffer[sizeof(buffer)-1];
	*p = '\0';

	while ((pdo_uint64_t)i64 > (pdo_uint64_t)GEAR_LONG_MAX) {
		pdo_uint64_t quo = (pdo_uint64_t)i64 / (unsigned int)10;
		unsigned int rem = (unsigned int)(i64 - quo*10U);
		*--p = digit_vec[rem];
		i64 = (pdo_int64_t)quo;
	}
	long_val = (gear_long)i64;
	while (long_val != 0) {
		gear_long quo = long_val / 10;
		*--p = digit_vec[(unsigned int)(long_val - quo * 10)];
		long_val = quo;
	}
	while ((*dst++ = *p++) != 0)
		;
	*dst = '\0';
	return estrdup(outbuf);
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION */
HYSS_MINIT_FUNCTION(pdo)
{
	gear_class_entry ce;

	if (FAILURE == pdo_sqlstate_init_error_table()) {
		return FAILURE;
	}

	gear_hash_init(&pdo_driver_hash, 0, NULL, NULL, 1);

	le_ppdo = gear_register_list_destructors_ex(NULL, hyss_pdo_pdbh_dtor,
		"PDO persistent database", capi_number);

	INIT_CLASS_ENTRY(ce, "PDOException", NULL);

 	pdo_exception_ce = gear_register_internal_class_ex(&ce, hyss_pdo_get_exception_base(0));

	gear_declare_property_null(pdo_exception_ce, "errorInfo", sizeof("errorInfo")-1, GEAR_ACC_PUBLIC);

	pdo_dbh_init();
	pdo_stmt_init();

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION */
HYSS_MSHUTDOWN_FUNCTION(pdo)
{
	gear_hash_destroy(&pdo_driver_hash);
	pdo_sqlstate_fics_error_table();
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION */
HYSS_MINFO_FUNCTION(pdo)
{
	char *drivers = NULL, *ldrivers = estrdup("");
	pdo_driver_t *pdriver;

	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "PDO support", "enabled");

	GEAR_HASH_FOREACH_PTR(&pdo_driver_hash, pdriver) {
		spprintf(&drivers, 0, "%s, %s", ldrivers, pdriver->driver_name);
		efree(ldrivers);
		ldrivers = drivers;
	} GEAR_HASH_FOREACH_END();

	hyss_info_print_table_row(2, "PDO drivers", drivers ? drivers + 2 : "");

	if (drivers) {
		efree(drivers);
	} else {
		efree(ldrivers);
	}

	hyss_info_print_table_end();

}
/* }}} */

