/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Stuff private to the PDO extension and not for consumption by PDO drivers
 * */

#include "hyss_pdo_error.h"

extern HashTable pdo_driver_hash;
extern gear_class_entry *pdo_exception_ce;
PDO_API gear_class_entry *hyss_pdo_get_exception_base(int root);
int hyss_pdo_list_entry(void);

void pdo_dbh_init(void);
void pdo_stmt_init(void);

extern gear_object *pdo_dbh_new(gear_class_entry *ce);
extern const gear_function_entry pdo_dbh_functions[];
extern gear_class_entry *pdo_dbh_ce;
extern GEAR_RSRC_DTOR_FUNC(hyss_pdo_pdbh_dtor);

extern gear_object *pdo_dbstmt_new(gear_class_entry *ce);
extern const gear_function_entry pdo_dbstmt_functions[];
extern gear_class_entry *pdo_dbstmt_ce;
void pdo_dbstmt_free_storage(gear_object *std);
gear_object_iterator *pdo_stmt_iter_get(gear_class_entry *ce, zval *object, int by_ref);
extern gear_object_handlers pdo_dbstmt_object_handlers;
int pdo_stmt_describe_columns(pdo_stmt_t *stmt);
int pdo_stmt_setup_fetch_mode(INTERNAL_FUNCTION_PARAMETERS, pdo_stmt_t *stmt, int skip_first_arg);

extern gear_object *pdo_row_new(gear_class_entry *ce);
extern const gear_function_entry pdo_row_functions[];
extern gear_class_entry *pdo_row_ce;
void pdo_row_free_storage(gear_object *std);
extern const gear_object_handlers pdo_row_object_handlers;

gear_object_iterator *hyss_pdo_dbstmt_iter_get(gear_class_entry *ce, zval *object);

extern pdo_driver_t *pdo_find_driver(const char *name, int namelen);

int pdo_sqlstate_init_error_table(void);
void pdo_sqlstate_fics_error_table(void);
const char *pdo_sqlstate_state_to_description(char *state);
int pdo_hash_methods(pdo_dbh_object_t *dbh, int kind);

