hyssincludedir=$(prefix)/include/hyss

PDO_HEADER_FILES= \
	hyss_pdo.h \
	hyss_pdo_driver.h \
	hyss_pdo_error.h


$(srcdir)/pdo_sql_parser.c: $(srcdir)/pdo_sql_parser.re
	@(cd $(top_srcdir); \
	if test -f ./pdo_sql_parser.re; then \
		$(RE2C) $(RE2C_FLAGS) --no-generation-date -o pdo_sql_parser.c pdo_sql_parser.re; \
	else \
		$(RE2C) $(RE2C_FLAGS) --no-generation-date -o extslib/pdo/pdo_sql_parser.c extslib/pdo/pdo_sql_parser.re; \
	fi)

install-pdo-headers:
	@echo "Installing PDO headers:           $(INSTALL_ROOT)$(hyssincludedir)/extslib/pdo/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(hyssincludedir)/extslib/pdo
	@for f in $(PDO_HEADER_FILES); do \
		if test -f "$(top_srcdir)/$$f"; then \
			$(INSTALL_DATA) $(top_srcdir)/$$f $(INSTALL_ROOT)$(hyssincludedir)/extslib/pdo; \
		elif test -f "$(top_builddir)/$$f"; then \
			$(INSTALL_DATA) $(top_builddir)/$$f $(INSTALL_ROOT)$(hyssincludedir)/extslib/pdo; \
		elif test -f "$(top_srcdir)/extslib/pdo/$$f"; then \
			$(INSTALL_DATA) $(top_srcdir)/extslib/pdo/$$f $(INSTALL_ROOT)$(hyssincludedir)/extslib/pdo; \
		elif test -f "$(top_builddir)/extslib/pdo/$$f"; then \
			$(INSTALL_DATA) $(top_builddir)/extslib/pdo/$$f $(INSTALL_ROOT)$(hyssincludedir)/extslib/pdo; \
		else \
			echo "hmmm"; \
		fi \
	done;

# mini hack
install: $(all_targets) $(install_targets) install-pdo-headers
