/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_LIBXML_H
#define HYSS_LIBXML_H

#if HAVE_LIBXML
extern gear_capi_entry libxml_capi_entry;
#define libxml_capi_ptr &libxml_capi_entry

#include "hyss_version.h"
#define HYSS_LIBXML_VERSION HYSS_VERSION

#ifdef HYSS_WIN32
#	define HYSS_LIBXML_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define HYSS_LIBXML_API __attribute__ ((visibility("default")))
#else
#	define HYSS_LIBXML_API
#endif

#include "gear_smart_str.h"
#include <libxml/tree.h>

#define LIBXML_SAVE_NOEMPTYTAG 1<<2

GEAR_BEGIN_CAPI_GLOBALS(libxml)
	zval stream_context;
	smart_str error_buffer;
	gear_llist *error_list;
	struct _hyss_libxml_entity_resolver {
		zval                    object;
		gear_fcall_info			fci;
		gear_fcall_info_cache	fcc;
	} entity_loader;
	gear_bool entity_loader_disabled;
GEAR_END_CAPI_GLOBALS(libxml)

typedef struct _libxml_doc_props {
	int formatoutput;
	int validateonparse;
	int resolveexternals;
	int preservewhitespace;
	int substituteentities;
	int stricterror;
	int recover;
	HashTable *classmap;
} libxml_doc_props;

typedef struct _hyss_libxml_ref_obj {
	void *ptr;
	int   refcount;
	libxml_doc_props *doc_props;
} hyss_libxml_ref_obj;

typedef struct _hyss_libxml_node_ptr {
	xmlNodePtr node;
	int	refcount;
	void *_private;
} hyss_libxml_node_ptr;

typedef struct _hyss_libxml_node_object {
	hyss_libxml_node_ptr *node;
	hyss_libxml_ref_obj *document;
	HashTable *properties;
	gear_object  std;
} hyss_libxml_node_object;


static inline hyss_libxml_node_object *hyss_libxml_node_fetch_object(gear_object *obj) {
	return (hyss_libxml_node_object *)((char*)(obj) - obj->handlers->offset);
}

#define Z_LIBXML_NODE_P(zv) hyss_libxml_node_fetch_object(Z_OBJ_P((zv)))

typedef void * (*hyss_libxml_export_node) (zval *object);

HYSS_LIBXML_API int hyss_libxml_increment_node_ptr(hyss_libxml_node_object *object, xmlNodePtr node, void *private_data);
HYSS_LIBXML_API int hyss_libxml_decrement_node_ptr(hyss_libxml_node_object *object);
HYSS_LIBXML_API int hyss_libxml_increment_doc_ref(hyss_libxml_node_object *object, xmlDocPtr docp);
HYSS_LIBXML_API int hyss_libxml_decrement_doc_ref(hyss_libxml_node_object *object);
HYSS_LIBXML_API xmlNodePtr hyss_libxml_import_node(zval *object);
HYSS_LIBXML_API zval *hyss_libxml_register_export(gear_class_entry *ce, hyss_libxml_export_node export_function);
/* When an explicit freeing of node and children is required */
HYSS_LIBXML_API void hyss_libxml_node_free_list(xmlNodePtr node);
HYSS_LIBXML_API void hyss_libxml_node_free_resource(xmlNodePtr node);
/* When object dtor is called as node may still be referenced */
HYSS_LIBXML_API void hyss_libxml_node_decrement_resource(hyss_libxml_node_object *object);
HYSS_LIBXML_API void hyss_libxml_error_handler(void *ctx, const char *msg, ...);
HYSS_LIBXML_API void hyss_libxml_ctx_warning(void *ctx, const char *msg, ...);
HYSS_LIBXML_API void hyss_libxml_ctx_error(void *ctx, const char *msg, ...);
HYSS_LIBXML_API int hyss_libxml_xmlCheckUTF8(const unsigned char *s);
HYSS_LIBXML_API void hyss_libxml_switch_context(zval *context, zval *oldcontext);
HYSS_LIBXML_API void hyss_libxml_issue_error(int level, const char *msg);
HYSS_LIBXML_API gear_bool hyss_libxml_disable_entity_loader(gear_bool disable);

/* Init/shutdown functions*/
HYSS_LIBXML_API void hyss_libxml_initialize(void);
HYSS_LIBXML_API void hyss_libxml_shutdown(void);

#define LIBXML(v) GEAR_CAPI_GLOBALS_ACCESSOR(libxml, v)

#if defined(ZTS) && defined(COMPILE_DL_LIBXML)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#else /* HAVE_LIBXML */
#define libxml_capi_ptr NULL
#endif

#define hyssext_libxml_ptr libxml_capi_ptr

#endif /* HYSS_LIBXML_H */

