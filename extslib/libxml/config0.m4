dnl config.m4 for extension libxml

HYSS_ARG_ENABLE(libxml, whether to enable LIBXML support,
[  --disable-libxml        Disable LIBXML support], yes)

if test -z "$HYSS_LIBXML_DIR"; then
  HYSS_ARG_WITH(libxml-dir, libxml2 install dir,
  [  --with-libxml-dir[=DIR]   LIBXML: libxml2 install prefix], no, no)
fi

if test "$HYSS_LIBXML" != "no"; then

  dnl This extension can not be build as shared
  ext_shared=no

  HYSS_SETUP_LIBXML(LIBXML_SHARED_LIBADD, [
    AC_DEFINE(HAVE_LIBXML,1,[ ])
    HYSS_NEW_EXTENSION(libxml, [libxml.c], $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
    HYSS_INSTALL_HEADERS([extslib/libxml/hyss_libxml.h])
  ], [
    AC_MSG_ERROR([libxml2 not found. Please check your libxml2 installation.])
  ])
fi
