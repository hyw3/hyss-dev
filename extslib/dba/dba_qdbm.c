/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if DBA_QDBM
#include "hyss_qdbm.h"

#ifdef QDBM_INCLUDE_FILE
#include QDBM_INCLUDE_FILE
#endif

#define QDBM_DATA dba_qdbm_data *dba = info->dbf

typedef struct {
	DEPOT *dbf;
} dba_qdbm_data;

DBA_OPEN_FUNC(qdbm)
{
	DEPOT *dbf;

	switch(info->mode) {
		case DBA_READER:
			dbf = dpopen(info->path, DP_OREADER, 0);
			break;
		case DBA_WRITER:
			dbf = dpopen(info->path, DP_OWRITER, 0);
			break;
		case DBA_CREAT:
			dbf = dpopen(info->path, DP_OWRITER | DP_OCREAT, 0);
			break;
		case DBA_TRUNC:
			dbf = dpopen(info->path, DP_OWRITER | DP_OCREAT | DP_OTRUNC, 0);
			break;
		default:
			return FAILURE;
	}

	if (dbf) {
		info->dbf = pemalloc(sizeof(dba_qdbm_data), info->flags & DBA_PERSISTENT);
		memset(info->dbf, 0, sizeof(dba_qdbm_data));
		((dba_qdbm_data *) info->dbf)->dbf = dbf;
		return SUCCESS;
	}

	*error = (char *) dperrmsg(dpecode);
	return FAILURE;
}

DBA_CLOSE_FUNC(qdbm)
{
	QDBM_DATA;

	dpclose(dba->dbf);
	pefree(dba, info->flags & DBA_PERSISTENT);
}

DBA_FETCH_FUNC(qdbm)
{
	QDBM_DATA;
	char *value, *new = NULL;
	int value_size;

	value = dpget(dba->dbf, key, keylen, 0, -1, &value_size);
	if (value) {
		if (newlen) *newlen = value_size;
		new = estrndup(value, value_size);
		free(value);
	}

	return new;
}

DBA_UPDATE_FUNC(qdbm)
{
	QDBM_DATA;

	if (dpput(dba->dbf, key, keylen, val, vallen, mode == 1 ? DP_DKEEP : DP_DOVER)) {
		return SUCCESS;
	}

	if (dpecode != DP_EKEEP) {
		hyss_error_docref2(NULL, key, val, E_WARNING, "%s", dperrmsg(dpecode));
	}

	return FAILURE;
}

DBA_EXISTS_FUNC(qdbm)
{
	QDBM_DATA;
	char *value;

	value = dpget(dba->dbf, key, keylen, 0, -1, NULL);
	if (value) {
		free(value);
		return SUCCESS;
	}

	return FAILURE;
}

DBA_DELETE_FUNC(qdbm)
{
	QDBM_DATA;

	return dpout(dba->dbf, key, keylen) ? SUCCESS : FAILURE;
}

DBA_FIRSTKEY_FUNC(qdbm)
{
	QDBM_DATA;
	int value_size;
	char *value, *new = NULL;

	dpiterinit(dba->dbf);

	value = dpiternext(dba->dbf, &value_size);
	if (value) {
		if (newlen) *newlen = value_size;
		new = estrndup(value, value_size);
		free(value);
	}

	return new;
}

DBA_NEXTKEY_FUNC(qdbm)
{
	QDBM_DATA;
	int value_size;
	char *value, *new = NULL;

	value = dpiternext(dba->dbf, &value_size);
	if (value) {
		if (newlen) *newlen = value_size;
		new = estrndup(value, value_size);
		free(value);
	}

	return new;
}

DBA_OPTIMIZE_FUNC(qdbm)
{
	QDBM_DATA;

	dpoptimize(dba->dbf, 0);
	return SUCCESS;
}

DBA_SYNC_FUNC(qdbm)
{
	QDBM_DATA;

	dpsync(dba->dbf);
	return SUCCESS;
}

DBA_INFO_FUNC(qdbm)
{
	return estrdup(dpversion);
}

#endif

