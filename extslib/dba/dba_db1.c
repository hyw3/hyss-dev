/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if DBA_DB1
#include "hyss_db1.h"

#ifdef DB1_INCLUDE_FILE
#include DB1_INCLUDE_FILE
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define DB1_DATA dba_db1_data *dba = info->dbf
#define DB1_GKEY DBT gkey; gkey.data = (char *) key; gkey.size = keylen

typedef struct {
	DB  *dbp;
} dba_db1_data;

DBA_OPEN_FUNC(db1)
{
	dba_db1_data	*dba;
	DB		*db;

	int gmode;
	int filemode = 0644;

	if (info->argc > 0) {
		filemode = zval_get_long(&info->argv[0]);
	}

	gmode = 0;
	switch (info->mode) {
		case DBA_READER:
			gmode = O_RDONLY;
			break;
		case DBA_WRITER:
			gmode = O_RDWR;
			break;
		case DBA_CREAT:
			gmode = O_RDWR | O_CREAT;
			break;
		case DBA_TRUNC:
			gmode = O_RDWR | O_CREAT | O_TRUNC;
			break;
		default:
			return FAILURE; /* not possible */
	}

	db = dbopen((char *)info->path, gmode, filemode, DB_HASH, NULL);

	if (db == NULL) {
		return FAILURE;
	}

	dba = pemalloc(sizeof(*dba), info->flags&DBA_PERSISTENT);
	dba->dbp = db;

	info->dbf = dba;

	return SUCCESS;
}

DBA_CLOSE_FUNC(db1)
{
	DB1_DATA;
	dba->dbp->close(dba->dbp);
	pefree(info->dbf, info->flags&DBA_PERSISTENT);
}

DBA_FETCH_FUNC(db1)
{
	DBT gval;
	DB1_DATA;
	DB1_GKEY;

	memset(&gval, 0, sizeof(gval));
	if (dba->dbp->get(dba->dbp, &gkey, &gval, 0) == RET_SUCCESS) {
		if (newlen) *newlen = gval.size;
		return estrndup(gval.data, gval.size);
	}
	return NULL;
}

DBA_UPDATE_FUNC(db1)
{
	DBT gval;
	DB1_DATA;
	DB1_GKEY;

	gval.data = (char *) val;
	gval.size = vallen;

	return dba->dbp->put(dba->dbp, &gkey, &gval, mode == 1 ? R_NOOVERWRITE : 0) != RET_SUCCESS ? FAILURE : SUCCESS;
}

DBA_EXISTS_FUNC(db1)
{
	DBT gval;
	DB1_DATA;
	DB1_GKEY;

	return dba->dbp->get(dba->dbp, &gkey, &gval, 0) != RET_SUCCESS ? FAILURE : SUCCESS;
}

DBA_DELETE_FUNC(db1)
{
	DB1_DATA;
	DB1_GKEY;

	return dba->dbp->del(dba->dbp, &gkey, 0) != RET_SUCCESS ? FAILURE : SUCCESS;
}

DBA_FIRSTKEY_FUNC(db1)
{
	DBT gkey;
	DBT gval;
	DB1_DATA;

	memset(&gkey, 0, sizeof(gkey));
	memset(&gval, 0, sizeof(gval));

	if (dba->dbp->seq(dba->dbp, &gkey, &gval, R_FIRST) == RET_SUCCESS) {
		if (newlen) *newlen = gkey.size;
		return estrndup(gkey.data, gkey.size);
	}
	return NULL;
}

DBA_NEXTKEY_FUNC(db1)
{
	DBT gkey;
	DBT gval;
	DB1_DATA;

	memset(&gkey, 0, sizeof(gkey));
	memset(&gval, 0, sizeof(gval));

	if (dba->dbp->seq(dba->dbp, &gkey, &gval, R_NEXT) == RET_SUCCESS) {
		if (newlen) *newlen = gkey.size;
		return estrndup(gkey.data, gkey.size);
	}
	return NULL;
}

DBA_OPTIMIZE_FUNC(db1)
{
	/* dummy */
	return SUCCESS;
}

DBA_SYNC_FUNC(db1)
{
	return SUCCESS;
}

DBA_INFO_FUNC(db1)
{
	return estrdup(DB1_VERSION);
}

#endif

