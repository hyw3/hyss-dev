/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* $Id: 9e3608810250a9e29249b953b4a3f18fa4eac0fb $ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_globals.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "icsfile.h"

/* ret = -1 means that database was opened for read-only
 * ret = 0  success
 * ret = 1  key already exists - nothing done
 */

/* {{{ icsfile_version */
char *icsfile_version()
{
	return "1.0, $Id: 9e3608810250a9e29249b953b4a3f18fa4eac0fb $";
}
/* }}} */

/* {{{ icsfile_free_key */
void icsfile_key_free(key_type *key)
{
	if (key->group) {
		efree(key->group);
	}
	if (key->name) {
		efree(key->name);
	}
	memset(key, 0, sizeof(key_type));
}
/* }}} */

/* {{{ icsfile_free_val */
void icsfile_val_free(val_type *val)
{
	if (val->value) {
		efree(val->value);
	}
	memset(val, 0, sizeof(val_type));
}
/* }}} */

/* {{{ icsfile_free_val */
void icsfile_line_free(line_type *ln)
{
	icsfile_key_free(&ln->key);
	icsfile_val_free(&ln->val);
	ln->pos = 0;
}
/* }}} */

/* {{{ icsfile_alloc */
icsfile * icsfile_alloc(hyss_stream *fp, int readonly, int persistent)
{
	icsfile *dba;

	if (!readonly) {
		if (!hyss_stream_truncate_supported(fp)) {
			hyss_error_docref(NULL, E_WARNING, "Can't truncate this stream");
			return NULL;
		}
	}

	dba = pemalloc(sizeof(icsfile), persistent);
	memset(dba, 0, sizeof(icsfile));
	dba->fp = fp;
	dba->readonly = readonly;
	return dba;
}
/* }}} */

/* {{{ icsfile_free */
void icsfile_free(icsfile *dba, int persistent)
{
	if (dba) {
		icsfile_line_free(&dba->curr);
		icsfile_line_free(&dba->next);
		pefree(dba, persistent);
	}
}
/* }}} */

/* {{{ icsfile_key_split */
key_type icsfile_key_split(const char *group_name)
{
	key_type key;
	char *name;

	if (group_name[0] == '[' && (name = strchr(group_name, ']')) != NULL) {
		key.group = estrndup(group_name+1, name - (group_name + 1));
		key.name = estrdup(name+1);
	} else {
		key.group = estrdup("");
		key.name = estrdup(group_name);
	}
	return key;
}
/* }}} */

/* {{{ icsfile_key_string */
char * icsfile_key_string(const key_type *key)
{
	if (key->group && *key->group) {
		char *result;
		spprintf(&result, 0, "[%s]%s", key->group, key->name ? key->name : "");
		return result;
	} else if (key->name) {
		return estrdup(key->name);
	} else {
		return NULL;
	}
}
/* }}} */

/* {{{ etrim */
static char *etrim(const char *str)
{
	char *val;
	size_t l;

	if (!str) {
		return NULL;
	}
	val = (char*)str;
	while (*val && strchr(" \t\r\n", *val)) {
		val++;
	}
	l = strlen(val);
	while (l && (strchr(" \t\r\n", val[l-1]))) {
		l--;
	}
	return estrndup(val, l);
}
/* }}} */

/* {{{ icsfile_findkey
 */
static int icsfile_read(icsfile *dba, line_type *ln) {
	char *fline;
	char *pos;

	icsfile_val_free(&ln->val);
	while ((fline = hyss_stream_gets(dba->fp, NULL, 0)) != NULL) {
		if (fline) {
			if (fline[0] == '[') {
				/* A value name cannot start with '['
				 * So either we find a ']' or we found an error
				 */
				pos = strchr(fline+1, ']');
				if (pos) {
					*pos = '\0';
					icsfile_key_free(&ln->key);
					ln->key.group = etrim(fline+1);
					ln->key.name = estrdup("");
					ln->pos = hyss_stream_tell(dba->fp);
					efree(fline);
					return 1;
				} else {
					efree(fline);
					continue;
				}
			} else {
				pos = strchr(fline, '=');
				if (pos) {
					*pos = '\0';
					/* keep group or make empty if not existent */
					if (!ln->key.group) {
						ln->key.group = estrdup("");
					}
					if (ln->key.name) {
						efree(ln->key.name);
					}
					ln->key.name = etrim(fline);
					ln->val.value = etrim(pos+1);
					ln->pos = hyss_stream_tell(dba->fp);
					efree(fline);
					return 1;
				} else {
					/* simply ignore lines without '='
					 * those should be comments
					 */
					 efree(fline);
					 continue;
				}
			}
		}
	}
	icsfile_line_free(ln);
	return 0;
}
/* }}} */

/* {{{ icsfile_key_cmp */
/* 0 = EQUAL
 * 1 = GROUP-EQUAL,NAME-DIFFERENT
 * 2 = DIFFERENT
 */
static int icsfile_key_cmp(const key_type *k1, const key_type *k2)
{
	assert(k1->group && k1->name && k2->group && k2->name);

	if (!strcasecmp(k1->group, k2->group)) {
		if (!strcasecmp(k1->name, k2->name)) {
			return 0;
		} else {
			return 1;
		}
	} else {
		return 2;
	}
}
/* }}} */

/* {{{ icsfile_fetch
 */
val_type icsfile_fetch(icsfile *dba, const key_type *key, int skip) {
	line_type ln = {{NULL,NULL},{NULL}};
	val_type val;
	int res, grp_eq = 0;

	if (skip == -1 && dba->next.key.group && dba->next.key.name && !icsfile_key_cmp(&dba->next.key, key)) {
		/* we got position already from last fetch */
		hyss_stream_seek(dba->fp, dba->next.pos, SEEK_SET);
		ln.key.group = estrdup(dba->next.key.group);
	} else {
		/* specific instance or not same key -> restart search */
		/* the slow way: restart and seacrch */
		hyss_stream_rewind(dba->fp);
		icsfile_line_free(&dba->next);
	}
	if (skip == -1) {
		skip = 0;
	}
	while(icsfile_read(dba, &ln)) {
		if (!(res=icsfile_key_cmp(&ln.key, key))) {
			if (!skip) {
				val.value = estrdup(ln.val.value ? ln.val.value : "");
				/* allow faster access by updating key read into next */
				icsfile_line_free(&dba->next);
				dba->next = ln;
				dba->next.pos = hyss_stream_tell(dba->fp);
				return val;
			}
			skip--;
		} else if (res == 1) {
			grp_eq = 1;
		} else if (grp_eq) {
			/* we are leaving group now: that means we cannot find the key */
			break;
		}
	}
	icsfile_line_free(&ln);
	dba->next.pos = hyss_stream_tell(dba->fp);
	return ln.val;
}
/* }}} */

/* {{{ icsfile_firstkey
 */
int icsfile_firstkey(icsfile *dba) {
	icsfile_line_free(&dba->curr);
	dba->curr.pos = 0;
	return icsfile_nextkey(dba);
}
/* }}} */

/* {{{ icsfile_nextkey
 */
int icsfile_nextkey(icsfile *dba) {
	line_type ln = {{NULL,NULL},{NULL}};

	/*icsfile_line_free(&dba->next); ??? */
	hyss_stream_seek(dba->fp, dba->curr.pos, SEEK_SET);
	ln.key.group = estrdup(dba->curr.key.group ? dba->curr.key.group : "");
	icsfile_read(dba, &ln);
	icsfile_line_free(&dba->curr);
	dba->curr = ln;
	return ln.key.group || ln.key.name;
}
/* }}} */

/* {{{ icsfile_truncate
 */
static int icsfile_truncate(icsfile *dba, size_t size)
{
	int res;

	if ((res=hyss_stream_truncate_set_size(dba->fp, size)) != 0) {
		hyss_error_docref(NULL, E_WARNING, "Error in ftruncate: %d", res);
		return FAILURE;
	}
	hyss_stream_seek(dba->fp, size, SEEK_SET);
	return SUCCESS;
}
/* }}} */

/* {{{ icsfile_find_group
 * if found pos_grp_start points to "[group_name]"
 */
static int icsfile_find_group(icsfile *dba, const key_type *key, size_t *pos_grp_start)
{
	int ret = FAILURE;

	hyss_stream_flush(dba->fp);
	hyss_stream_seek(dba->fp, 0, SEEK_SET);
	icsfile_line_free(&dba->curr);
	icsfile_line_free(&dba->next);

	if (key->group && strlen(key->group)) {
		int res;
		line_type ln = {{NULL,NULL},{NULL}};

		res = 1;
		while(icsfile_read(dba, &ln)) {
			if ((res=icsfile_key_cmp(&ln.key, key)) < 2) {
				ret = SUCCESS;
				break;
			}
			*pos_grp_start = hyss_stream_tell(dba->fp);
		}
		icsfile_line_free(&ln);
	} else {
		*pos_grp_start = 0;
		ret = SUCCESS;
	}
	if (ret == FAILURE) {
		*pos_grp_start = hyss_stream_tell(dba->fp);
	}
	return ret;
}
/* }}} */

/* {{{ icsfile_next_group
 * only valid after a call to icsfile_find_group
 * if any next group is found pos_grp_start points to "[group_name]" or whitespace before that
 */
static int icsfile_next_group(icsfile *dba, const key_type *key, size_t *pos_grp_start)
{
	int ret = FAILURE;
	line_type ln = {{NULL,NULL},{NULL}};

	*pos_grp_start = hyss_stream_tell(dba->fp);
	ln.key.group = estrdup(key->group);
	while(icsfile_read(dba, &ln)) {
		if (icsfile_key_cmp(&ln.key, key) == 2) {
			ret = SUCCESS;
			break;
		}
		*pos_grp_start = hyss_stream_tell(dba->fp);
	}
	icsfile_line_free(&ln);
	return ret;
}
/* }}} */

/* {{{ icsfile_copy_to
 */
static int icsfile_copy_to(icsfile *dba, size_t pos_start, size_t pos_end, icsfile **ics_copy)
{
	hyss_stream *fp;

	if (pos_start == pos_end) {
		*ics_copy = NULL;
		return SUCCESS;
	}
	if ((fp = hyss_stream_temp_create(0, 64 * 1024)) == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Could not create temporary stream");
		*ics_copy = NULL;
		return FAILURE;
	}

	if ((*ics_copy = icsfile_alloc(fp, 1, 0)) == NULL) {
		/* writes error */
		return FAILURE;
	}
	hyss_stream_seek(dba->fp, pos_start, SEEK_SET);
	if (SUCCESS != hyss_stream_copy_to_stream_ex(dba->fp, fp, pos_end - pos_start, NULL)) {
		hyss_error_docref(NULL, E_WARNING, "Could not copy group [%zu - %zu] to temporary stream", pos_start, pos_end);
		return FAILURE;
	}
	return SUCCESS;
}
/* }}} */

/* {{{ icsfile_filter
 * copy from to dba while ignoring key name (group must equal)
 */
static int icsfile_filter(icsfile *dba, icsfile *from, const key_type *key, gear_bool *found)
{
	size_t pos_start = 0, pos_next = 0, pos_curr;
	int ret = SUCCESS;
	line_type ln = {{NULL,NULL},{NULL}};

	hyss_stream_seek(from->fp, 0, SEEK_SET);
	hyss_stream_seek(dba->fp, 0, SEEK_END);
	while(icsfile_read(from, &ln)) {
		switch(icsfile_key_cmp(&ln.key, key)) {
		case 0:
			if (found) {
				*found = (gear_bool) 1;
			}
			pos_curr = hyss_stream_tell(from->fp);
			if (pos_start != pos_next) {
				hyss_stream_seek(from->fp, pos_start, SEEK_SET);
				if (SUCCESS != hyss_stream_copy_to_stream_ex(from->fp, dba->fp, pos_next - pos_start, NULL)) {
					hyss_error_docref(NULL, E_WARNING, "Could not copy [%zu - %zu] from temporary stream", pos_next, pos_start);
					ret = FAILURE;
				}
				hyss_stream_seek(from->fp, pos_curr, SEEK_SET);
			}
			pos_next = pos_start = pos_curr;
			break;
		case 1:
			pos_next = hyss_stream_tell(from->fp);
			break;
		case 2:
			/* the function is meant to process only entries from same group */
			assert(0);
			break;
		}
	}
	if (pos_start != pos_next) {
		hyss_stream_seek(from->fp, pos_start, SEEK_SET);
		if (SUCCESS != hyss_stream_copy_to_stream_ex(from->fp, dba->fp, pos_next - pos_start, NULL)) {
			hyss_error_docref(NULL, E_WARNING, "Could not copy [%zu - %zu] from temporary stream", pos_next, pos_start);
			ret = FAILURE;
		}
	}
	icsfile_line_free(&ln);
	return ret;
}
/* }}} */

/* {{{ icsfile_delete_replace_append
 */
static int icsfile_delete_replace_append(icsfile *dba, const key_type *key, const val_type *value, int append, gear_bool *found)
{
	size_t pos_grp_start=0, pos_grp_next;
	icsfile *ics_tmp = NULL;
	hyss_stream *fp_tmp = NULL;
	int ret;

	/* 1) Search group start
	 * 2) Search next group
	 * 3) If not append: Copy group to ics_tmp
	 * 4) Open temp_stream and copy remainder
	 * 5) Truncate stream
	 * 6) If not append AND key.name given: Filtered copy back from ics_tmp
	 *    to stream. Otherwise the user wanted to delete the group.
	 * 7) Append value if given
	 * 8) Append temporary stream
	 */

	assert(!append || (key->name && value)); /* missuse */

	/* 1 - 3 */
	icsfile_find_group(dba, key, &pos_grp_start);
	icsfile_next_group(dba, key, &pos_grp_next);
	if (append) {
		ret = SUCCESS;
	} else {
		ret = icsfile_copy_to(dba, pos_grp_start, pos_grp_next, &ics_tmp);
	}

	/* 4 */
	if (ret == SUCCESS) {
		fp_tmp = hyss_stream_temp_create(0, 64 * 1024);
		if (!fp_tmp) {
			hyss_error_docref(NULL, E_WARNING, "Could not create temporary stream");
			ret = FAILURE;
		} else {
			hyss_stream_seek(dba->fp, 0, SEEK_END);
			if (pos_grp_next != (size_t)hyss_stream_tell(dba->fp)) {
				hyss_stream_seek(dba->fp, pos_grp_next, SEEK_SET);
				if (SUCCESS != hyss_stream_copy_to_stream_ex(dba->fp, fp_tmp, HYSS_STREAM_COPY_ALL, NULL)) {
					hyss_error_docref(NULL, E_WARNING, "Could not copy remainder to temporary stream");
					ret = FAILURE;
				}
			}
		}
	}

	/* 5 */
	if (ret == SUCCESS) {
		if (!value || (key->name && strlen(key->name))) {
			ret = icsfile_truncate(dba, append ? pos_grp_next : pos_grp_start); /* writes error on fail */
		}
	}

	if (ret == SUCCESS) {
		if (key->name && strlen(key->name)) {
			/* 6 */
			if (!append && ics_tmp) {
				ret = icsfile_filter(dba, ics_tmp, key, found);
			}

			/* 7 */
			/* important: do not query ret==SUCCESS again: icsfile_filter might fail but
			 * however next operation must be done.
			 */
			if (value) {
				if (pos_grp_start == pos_grp_next && key->group && strlen(key->group)) {
					hyss_stream_printf(dba->fp, "[%s]\n", key->group);
				}
				hyss_stream_printf(dba->fp, "%s=%s\n", key->name, value->value ? value->value : "");
			}
		}

		/* 8 */
		/* important: do not query ret==SUCCESS again: icsfile_filter might fail but
		 * however next operation must be done.
		 */
		if (fp_tmp && hyss_stream_tell(fp_tmp)) {
			hyss_stream_seek(fp_tmp, 0, SEEK_SET);
			hyss_stream_seek(dba->fp, 0, SEEK_END);
			if (SUCCESS != hyss_stream_copy_to_stream_ex(fp_tmp, dba->fp, HYSS_STREAM_COPY_ALL, NULL)) {
				gear_throw_error(NULL, "Could not copy from temporary stream - ics file truncated");
				ret = FAILURE;
			}
		}
	}

	if (ics_tmp) {
		hyss_stream_close(ics_tmp->fp);
		icsfile_free(ics_tmp, 0);
	}
	if (fp_tmp) {
		hyss_stream_close(fp_tmp);
	}
	hyss_stream_flush(dba->fp);
	hyss_stream_seek(dba->fp, 0, SEEK_SET);

	return ret;
}
/* }}} */

/* {{{ icsfile_delete
 */
int icsfile_delete(icsfile *dba, const key_type *key)
{
	return icsfile_delete_replace_append(dba, key, NULL, 0, NULL);
}
/* }}} */

/* {{{ icsfile_delete_ex
 */
int icsfile_delete_ex(icsfile *dba, const key_type *key, gear_bool *found)
{
	return icsfile_delete_replace_append(dba, key, NULL, 0, found);
}
/* }}} */

/* {{{ icsfile_relace
 */
int icsfile_replace(icsfile *dba, const key_type *key, const val_type *value)
{
	return icsfile_delete_replace_append(dba, key, value, 0, NULL);
}
/* }}} */

/* {{{ icsfile_replace_ex
 */
int icsfile_replace_ex(icsfile *dba, const key_type *key, const val_type *value, gear_bool *found)
{
	return icsfile_delete_replace_append(dba, key, value, 0, found);
}
/* }}} */

/* {{{ icsfile_append
 */
int icsfile_append(icsfile *dba, const key_type *key, const val_type *value)
{
	return icsfile_delete_replace_append(dba, key, value, 1, NULL);
}
/* }}} */

