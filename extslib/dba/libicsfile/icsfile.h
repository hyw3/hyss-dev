/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_LIB_ICSFILE_H
#define HYSS_LIB_ICSFILE_H

typedef struct {
	char *group;
	char *name;
} key_type;

typedef struct {
	char *value;
} val_type;

typedef struct {
	key_type key;
	val_type val;
	size_t pos;
} line_type;

typedef struct {
	char *lockfn;
	int lockfd;
	hyss_stream *fp;
	int readonly;
	line_type curr;
	line_type next;
} icsfile;

val_type icsfile_fetch(icsfile *dba, const key_type *key, int skip);
int icsfile_firstkey(icsfile *dba);
int icsfile_nextkey(icsfile *dba);
int icsfile_delete(icsfile *dba, const key_type *key);
int icsfile_delete_ex(icsfile *dba, const key_type *key, gear_bool *found);
int icsfile_replace(icsfile *dba, const key_type *key, const val_type *val);
int icsfile_replace_ex(icsfile *dba, const key_type *key, const val_type *val, gear_bool *found);
int icsfile_append(icsfile *dba, const key_type *key, const val_type *val);
char *icsfile_version();

key_type icsfile_key_split(const char *group_name);
char * icsfile_key_string(const key_type *key);

void icsfile_key_free(key_type *key);
void icsfile_val_free(val_type *val);
void icsfile_line_free(line_type *ln);

icsfile * icsfile_alloc(hyss_stream *fp, int readonly, int persistent);
void icsfile_free(icsfile *dba, int persistent);

#endif
