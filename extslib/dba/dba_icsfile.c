/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if DBA_ICSFILE
#include "hyss_icsfile.h"

#include "libicsfile/icsfile.h"

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define ICSFILE_DATA \
	icsfile *dba = info->dbf

#define ICSFILE_GKEY \
	key_type ics_key; \
	if (!key) { \
		hyss_error_docref(NULL, E_WARNING, "No key specified"); \
		return 0; \
	} \
	ics_key = icsfile_key_split((char*)key) /* keylen not needed here */

#define ICSFILE_DONE \
	icsfile_key_free(&ics_key)

DBA_OPEN_FUNC(icsfile)
{
	info->dbf = icsfile_alloc(info->fp, info->mode == DBA_READER, info->flags&DBA_PERSISTENT);

	return info->dbf ? SUCCESS : FAILURE;
}

DBA_CLOSE_FUNC(icsfile)
{
	ICSFILE_DATA;

	icsfile_free(dba, info->flags&DBA_PERSISTENT);
}

DBA_FETCH_FUNC(icsfile)
{
	val_type ics_val;

	ICSFILE_DATA;
	ICSFILE_GKEY;

	ics_val = icsfile_fetch(dba, &ics_key, skip);
	*newlen = ics_val.value ? strlen(ics_val.value) : 0;
	ICSFILE_DONE;
	return ics_val.value;
}

DBA_UPDATE_FUNC(icsfile)
{
	val_type ics_val;
	int res;

	ICSFILE_DATA;
	ICSFILE_GKEY;

	ics_val.value = val;

	if (mode == 1) {
		res = icsfile_append(dba, &ics_key, &ics_val);
	} else {
		res = icsfile_replace(dba, &ics_key, &ics_val);
	}
	ICSFILE_DONE;
	switch(res) {
	case -1:
		hyss_error_docref1(NULL, key, E_WARNING, "Operation not possible");
		return FAILURE;
	default:
	case 0:
		return SUCCESS;
	case 1:
		return FAILURE;
	}
}

DBA_EXISTS_FUNC(icsfile)
{
	val_type ics_val;

	ICSFILE_DATA;
	ICSFILE_GKEY;

	ics_val = icsfile_fetch(dba, &ics_key, 0);
	ICSFILE_DONE;
	if (ics_val.value) {
		icsfile_val_free(&ics_val);
		return SUCCESS;
	}
	return FAILURE;
}

DBA_DELETE_FUNC(icsfile)
{
	int res;
	gear_bool found = 0;

	ICSFILE_DATA;
	ICSFILE_GKEY;

	res =  icsfile_delete_ex(dba, &ics_key, &found);

	ICSFILE_DONE;
	return (res == -1 || !found ? FAILURE : SUCCESS);
}

DBA_FIRSTKEY_FUNC(icsfile)
{
	ICSFILE_DATA;

	if (icsfile_firstkey(dba)) {
		char *result = icsfile_key_string(&dba->curr.key);
		*newlen = strlen(result);
		return result;
	} else {
		return NULL;
	}
}

DBA_NEXTKEY_FUNC(icsfile)
{
	ICSFILE_DATA;

	if (!dba->curr.key.group && !dba->curr.key.name) {
		return NULL;
	}

	if (icsfile_nextkey(dba)) {
		char *result = icsfile_key_string(&dba->curr.key);
		*newlen = strlen(result);
		return result;
	} else {
		return NULL;
	}
}

DBA_OPTIMIZE_FUNC(icsfile)
{
	/* dummy */
	return SUCCESS;
}

DBA_SYNC_FUNC(icsfile)
{
	/* dummy */
	return SUCCESS;
}

DBA_INFO_FUNC(icsfile)
{
	return estrdup(icsfile_version());
}

#endif

