/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* incorporated from D.J.Bernstein's cdb-0.75 (http://cr.yp.to/cdb.html)*/

#ifndef CDB_MAKE_H
#define CDB_MAKE_H

#include <stdio.h>
#include "uint32.h"

#define CDB_HPLIST 1000

struct cdb_hp {
	uint32 h;
	uint32 p;
};

struct cdb_hplist {
	struct cdb_hp hp[CDB_HPLIST];
	struct cdb_hplist *next;
	int num;
} ;

struct cdb_make {
	/* char bspace[8192]; */
	char final[2048];
	uint32 count[256];
	uint32 start[256];
	struct cdb_hplist *head;
	struct cdb_hp *split; /* includes space for hash */
	struct cdb_hp *hash;
	uint32 numentries;
	/* buffer b; */
	uint32 pos;
	/* int fd; */
	hyss_stream * fp;
};

int cdb_make_start(struct cdb_make *, hyss_stream *);
int cdb_make_addbegin(struct cdb_make *, unsigned int, unsigned int);
int cdb_make_addend(struct cdb_make *, unsigned int, unsigned int, uint32);
int cdb_make_add(struct cdb_make *, char *, unsigned int, char *, unsigned int);
int cdb_make_finish(struct cdb_make *);
char *cdb_make_version();

#endif
