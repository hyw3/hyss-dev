/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* incorporated from D.J.Bernstein's cdb-0.75 (http://cr.yp.to/cdb.html)*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#include "uint32.h"

/* {{{ uint32_pack */
void uint32_pack(char *out, uint32 in)
{
	out[0] = in&0xff; in>>=8;
	out[1] = in&0xff; in>>=8;
	out[2] = in&0xff; in>>=8;
	out[3] = in&0xff;
}
/* }}} */

/* {{{ uint32_unpack */
void uint32_unpack(const char *in, uint32 *out)
{
	*out = (((uint32)(unsigned char)in[3])<<24) |
	       (((uint32)(unsigned char)in[2])<<16) |
	       (((uint32)(unsigned char)in[1])<<8) |
	       (((uint32)(unsigned char)in[0]));
}
/* }}} */
