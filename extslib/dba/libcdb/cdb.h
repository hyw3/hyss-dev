/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* incorporated from D.J.Bernstein's cdb-0.75 (http://cr.yp.to/cdb.html)*/

#ifndef CDB_H
#define CDB_H

#include "uint32.h"

#define CDB_HASHSTART 5381

struct cdb {
	hyss_stream *fp;
	uint32 loop; /* number of hash slots searched under this key */
	uint32 khash; /* initialized if loop is nonzero */
	uint32 kpos; /* initialized if loop is nonzero */
	uint32 hpos; /* initialized if loop is nonzero */
	uint32 hslots; /* initialized if loop is nonzero */
	uint32 dpos; /* initialized if cdb_findnext() returns 1 */
	uint32 dlen; /* initialized if cdb_findnext() returns 1 */
};

uint32 cdb_hash(char *, unsigned int);

void cdb_free(struct cdb *);
void cdb_init(struct cdb *, hyss_stream *fp);

int cdb_read(struct cdb *, char *, unsigned int, uint32);

void cdb_findstart(struct cdb *);
int cdb_findnext(struct cdb *, char *, unsigned int);
int cdb_find(struct cdb *, char *, unsigned int);

#define cdb_datapos(c) ((c)->dpos)
#define cdb_datalen(c) ((c)->dlen)

char *cdb_version();

#endif
