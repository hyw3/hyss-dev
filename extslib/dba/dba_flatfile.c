/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if DBA_FLATFILE
#include "hyss_flatfile.h"

#include "libflatfile/flatfile.h"

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define FLATFILE_DATA flatfile *dba = info->dbf
#define FLATFILE_GKEY datum gkey; gkey.dptr = (char *) key; gkey.dsize = keylen

DBA_OPEN_FUNC(flatfile)
{
	info->dbf = pemalloc(sizeof(flatfile), info->flags&DBA_PERSISTENT);
	memset(info->dbf, 0, sizeof(flatfile));

	((flatfile*)info->dbf)->fp = info->fp;

	return SUCCESS;
}

DBA_CLOSE_FUNC(flatfile)
{
	FLATFILE_DATA;

	if (dba->nextkey.dptr) {
		efree(dba->nextkey.dptr);
	}
	pefree(dba, info->flags&DBA_PERSISTENT);
}

DBA_FETCH_FUNC(flatfile)
{
	datum gval;
	char *new = NULL;

	FLATFILE_DATA;
	FLATFILE_GKEY;

	gval = flatfile_fetch(dba, gkey);
	if (gval.dptr) {
		if (newlen) {
			*newlen = gval.dsize;
		}
		new = estrndup(gval.dptr, gval.dsize);
		efree(gval.dptr);
	}
	return new;
}

DBA_UPDATE_FUNC(flatfile)
{
	datum gval;

	FLATFILE_DATA;
	FLATFILE_GKEY;
	gval.dptr = (char *) val;
	gval.dsize = vallen;

	switch(flatfile_store(dba, gkey, gval, mode==1 ? FLATFILE_INSERT : FLATFILE_REPLACE)) {
		case 0:
			return SUCCESS;
		case 1:
			return FAILURE;
		case -1:
			hyss_error_docref1(NULL, key, E_WARNING, "Operation not possible");
			return FAILURE;
		default:
			hyss_error_docref2(NULL, key, val, E_WARNING, "Unknown return value");
			return FAILURE;
	}
}

DBA_EXISTS_FUNC(flatfile)
{
	datum gval;
	FLATFILE_DATA;
	FLATFILE_GKEY;

	gval = flatfile_fetch(dba, gkey);
	if (gval.dptr) {
		efree(gval.dptr);
		return SUCCESS;
	}
	return FAILURE;
}

DBA_DELETE_FUNC(flatfile)
{
	FLATFILE_DATA;
	FLATFILE_GKEY;
	return(flatfile_delete(dba, gkey) == -1 ? FAILURE : SUCCESS);
}

DBA_FIRSTKEY_FUNC(flatfile)
{
	FLATFILE_DATA;

	if (dba->nextkey.dptr) {
		efree(dba->nextkey.dptr);
	}
	dba->nextkey = flatfile_firstkey(dba);
	if (dba->nextkey.dptr) {
		if (newlen)  {
			*newlen = dba->nextkey.dsize;
		}
		return estrndup(dba->nextkey.dptr, dba->nextkey.dsize);
	}
	return NULL;
}

DBA_NEXTKEY_FUNC(flatfile)
{
	FLATFILE_DATA;

	if (!dba->nextkey.dptr) {
		return NULL;
	}

	if (dba->nextkey.dptr) {
		efree(dba->nextkey.dptr);
	}
	dba->nextkey = flatfile_nextkey(dba);
	if (dba->nextkey.dptr) {
		if (newlen) {
			*newlen = dba->nextkey.dsize;
		}
		return estrndup(dba->nextkey.dptr, dba->nextkey.dsize);
	}
	return NULL;
}

DBA_OPTIMIZE_FUNC(flatfile)
{
	/* dummy */
	return SUCCESS;
}

DBA_SYNC_FUNC(flatfile)
{
	/* dummy */
	return SUCCESS;
}

DBA_INFO_FUNC(flatfile)
{
	return estrdup(flatfile_version());
}

#endif

