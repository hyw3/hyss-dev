/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_DBA_H
#define HYSS_DBA_H

#include "hyss_version.h"
#define HYSS_DBA_VERSION HYSS_VERSION

#if HAVE_DBA

typedef enum {
	/* do not allow 0 here */
	DBA_READER = 1,
	DBA_WRITER,
	DBA_TRUNC,
	DBA_CREAT
} dba_mode_t;

typedef struct dba_lock {
	hyss_stream *fp;
	char *name;
	int mode; /* LOCK_EX,LOCK_SH */
} dba_lock;

typedef struct dba_info {
	/* public */
	void *dbf;               /* ptr to private data or whatever */
	char *path;
	dba_mode_t mode;
	hyss_stream *fp;  /* this is the database stream for builtin handlers */
	int fd;
	/* arg[cv] are only available when the dba_open handler is called! */
	int argc;
	zval *argv;
	/* private */
	int flags; /* whether and how dba did locking and other flags*/
	struct dba_handler *hnd;
	dba_lock lock;
} dba_info;

#define DBA_LOCK_READER  (0x0001)
#define DBA_LOCK_WRITER  (0x0002)
#define DBA_LOCK_CREAT   (0x0004)
#define DBA_LOCK_TRUNC   (0x0008)

#define DBA_LOCK_EXT     (0)
#define DBA_LOCK_ALL     (DBA_LOCK_READER|DBA_LOCK_WRITER|DBA_LOCK_CREAT|DBA_LOCK_TRUNC)
#define DBA_LOCK_WCT     (DBA_LOCK_WRITER|DBA_LOCK_CREAT|DBA_LOCK_TRUNC)

#define DBA_STREAM_OPEN  (0x0010)
#define DBA_PERSISTENT   (0x0020)

#define DBA_CAST_AS_FD   (0x0050)
#define DBA_NO_APPEND    (0x00D0)

extern gear_capi_entry dba_capi_entry;
#define dba_capi_ptr &dba_capi_entry

typedef struct dba_handler {
	char *name; /* handler name */
	int flags; /* whether and how dba does locking and other flags*/
	int (*open)(dba_info *, char **error);
	void (*close)(dba_info *);
	char* (*fetch)(dba_info *, char *, size_t, int, size_t *);
	int (*update)(dba_info *, char *, size_t, char *, size_t, int);
	int (*exists)(dba_info *, char *, size_t);
	int (*delete)(dba_info *, char *, size_t);
	char* (*firstkey)(dba_info *, size_t *);
	char* (*nextkey)(dba_info *, size_t *);
	int (*optimize)(dba_info *);
	int (*sync)(dba_info *);
	char* (*info)(struct dba_handler *hnd, dba_info *);
		/* dba_info==NULL: Handler info, dba_info!=NULL: Database info */
} dba_handler;

/* common prototypes which must be supplied by cAPIs */

#define DBA_OPEN_FUNC(x) \
	int dba_open_##x(dba_info *info, char **error)
#define DBA_CLOSE_FUNC(x) \
	void dba_close_##x(dba_info *info)
#define DBA_FETCH_FUNC(x) \
	char *dba_fetch_##x(dba_info *info, char *key, size_t keylen, int skip, size_t *newlen)
#define DBA_UPDATE_FUNC(x) \
	int dba_update_##x(dba_info *info, char *key, size_t keylen, char *val, size_t vallen, int mode)
#define DBA_EXISTS_FUNC(x) \
	int dba_exists_##x(dba_info *info, char *key, size_t keylen)
#define DBA_DELETE_FUNC(x) \
	int dba_delete_##x(dba_info *info, char *key, size_t keylen)
#define DBA_FIRSTKEY_FUNC(x) \
	char *dba_firstkey_##x(dba_info *info, size_t *newlen)
#define DBA_NEXTKEY_FUNC(x) \
	char *dba_nextkey_##x(dba_info *info, size_t *newlen)
#define DBA_OPTIMIZE_FUNC(x) \
	int dba_optimize_##x(dba_info *info)
#define DBA_SYNC_FUNC(x) \
	int dba_sync_##x(dba_info *info)
#define DBA_INFO_FUNC(x) \
	char *dba_info_##x(dba_handler *hnd, dba_info *info)

#define DBA_FUNCS(x) \
	DBA_OPEN_FUNC(x); \
	DBA_CLOSE_FUNC(x); \
	DBA_FETCH_FUNC(x); \
	DBA_UPDATE_FUNC(x); \
	DBA_DELETE_FUNC(x); \
	DBA_EXISTS_FUNC(x); \
	DBA_FIRSTKEY_FUNC(x); \
	DBA_NEXTKEY_FUNC(x); \
	DBA_OPTIMIZE_FUNC(x); \
	DBA_SYNC_FUNC(x); \
	DBA_INFO_FUNC(x)

HYSS_FUNCTION(dba_open);
HYSS_FUNCTION(dba_popen);
HYSS_FUNCTION(dba_close);
HYSS_FUNCTION(dba_firstkey);
HYSS_FUNCTION(dba_nextkey);
HYSS_FUNCTION(dba_replace);
HYSS_FUNCTION(dba_insert);
HYSS_FUNCTION(dba_delete);
HYSS_FUNCTION(dba_exists);
HYSS_FUNCTION(dba_fetch);
HYSS_FUNCTION(dba_optimize);
HYSS_FUNCTION(dba_sync);
HYSS_FUNCTION(dba_handlers);
HYSS_FUNCTION(dba_list);
HYSS_FUNCTION(dba_key_split);

#else
#define dba_capi_ptr NULL
#endif

#define hyssext_dba_ptr dba_capi_ptr

#endif
