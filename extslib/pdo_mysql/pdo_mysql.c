/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "pdo/hyss_pdo.h"
#include "pdo/hyss_pdo_driver.h"
#include "hyss_pdo_mysql.h"
#include "hyss_pdo_mysql_int.h"

#ifdef COMPILE_DL_PDO_MYSQL
#ifdef ZTS
GEAR_PBCLS_CACHE_DEFINE()
#endif
GEAR_GET_CAPI(pdo_mysql)
#endif

GEAR_DECLARE_CAPI_GLOBALS(pdo_mysql)

/*
 The default socket location is sometimes defined by configure.
 With libmysql `mysql_config --socket` will fill PDO_MYSQL_UNIX_ADDR
 and the user can use --with-mysql-sock=SOCKET which will fill
 PDO_MYSQL_UNIX_ADDR. If both aren't set we're using mysqlnd and use
 /tmp/mysql.sock as default on *nix and NULL for Windows (default
 named pipe name is set in mysqlnd).
*/
#ifndef PDO_MYSQL_UNIX_ADDR
# ifdef HYSS_MYSQL_UNIX_SOCK_ADDR
#  define PDO_MYSQL_UNIX_ADDR HYSS_MYSQL_UNIX_SOCK_ADDR
# else
#  if !HYSS_WIN32
#   define PDO_MYSQL_UNIX_ADDR "/tmp/mysql.sock"
#  else
#   define PDO_MYSQL_UNIX_ADDR NULL
#  endif
# endif
#endif

#ifdef PDO_USE_MYSQLND
#include "extslib/mysqlnd/mysqlnd_reverse_api.h"
static MYSQLND * pdo_mysql_convert_zv_to_mysqlnd(zval * zv)
{
	if (Z_TYPE_P(zv) == IS_OBJECT && instanceof_function(Z_OBJCE_P(zv), hyss_pdo_get_dbh_ce())) {
		pdo_dbh_t * dbh = Z_PDO_DBH_P(zv);

		if (!dbh) {
			hyss_error_docref(NULL, E_WARNING, "Failed to retrieve handle from object store");
			return NULL;
		}

		if (dbh->driver != &pdo_mysql_driver) {
			hyss_error_docref(NULL, E_WARNING, "Provided PDO instance is not using MySQL but %s", dbh->driver->driver_name);
			return NULL;
		}

		return ((pdo_mysql_db_handle *)dbh->driver_data)->server;
	}
	return NULL;
}

static const MYSQLND_REVERSE_API pdo_mysql_reverse_api = {
	&pdo_mysql_capi_entry,
	pdo_mysql_convert_zv_to_mysqlnd
};
#endif


/* {{{ HYSS_ICS_BEGIN
*/
HYSS_ICS_BEGIN()
#ifndef HYSS_WIN32
	STD_HYSS_ICS_ENTRY("pdo_mysql.default_socket", PDO_MYSQL_UNIX_ADDR, HYSS_ICS_SYSTEM, OnUpdateStringUnempty, default_socket, gear_pdo_mysql_globals, pdo_mysql_globals)
#endif
#if PDO_DBG_ENABLED
	STD_HYSS_ICS_ENTRY("pdo_mysql.debug",	NULL, HYSS_ICS_SYSTEM, OnUpdateString, debug, gear_pdo_mysql_globals, pdo_mysql_globals)
#endif
HYSS_ICS_END()
/* }}} */

/* true global environment */

/* {{{ HYSS_MINIT_FUNCTION
 */
static HYSS_MINIT_FUNCTION(pdo_mysql)
{
	REGISTER_ICS_ENTRIES();

	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_USE_BUFFERED_QUERY", (gear_long)PDO_MYSQL_ATTR_USE_BUFFERED_QUERY);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_LOCAL_INFILE", (gear_long)PDO_MYSQL_ATTR_LOCAL_INFILE);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_INIT_COMMAND", (gear_long)PDO_MYSQL_ATTR_INIT_COMMAND);
#ifndef PDO_USE_MYSQLND
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_MAX_BUFFER_SIZE", (gear_long)PDO_MYSQL_ATTR_MAX_BUFFER_SIZE);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_READ_DEFAULT_FILE", (gear_long)PDO_MYSQL_ATTR_READ_DEFAULT_FILE);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_READ_DEFAULT_GROUP", (gear_long)PDO_MYSQL_ATTR_READ_DEFAULT_GROUP);
#endif
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_COMPRESS", (gear_long)PDO_MYSQL_ATTR_COMPRESS);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_DIRECT_QUERY", (gear_long)PDO_MYSQL_ATTR_DIRECT_QUERY);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_FOUND_ROWS", (gear_long)PDO_MYSQL_ATTR_FOUND_ROWS);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_IGNORE_SPACE", (gear_long)PDO_MYSQL_ATTR_IGNORE_SPACE);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_SSL_KEY", (gear_long)PDO_MYSQL_ATTR_SSL_KEY);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_SSL_CERT", (gear_long)PDO_MYSQL_ATTR_SSL_CERT);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_SSL_CA", (gear_long)PDO_MYSQL_ATTR_SSL_CA);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_SSL_CAPATH", (gear_long)PDO_MYSQL_ATTR_SSL_CAPATH);
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_SSL_CIPHER", (gear_long)PDO_MYSQL_ATTR_SSL_CIPHER);
#if MYSQL_VERSION_ID > 50605 || defined(PDO_USE_MYSQLND)
	 REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_SERVER_PUBLIC_KEY", (gear_long)PDO_MYSQL_ATTR_SERVER_PUBLIC_KEY);
#endif
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_MULTI_STATEMENTS", (gear_long)PDO_MYSQL_ATTR_MULTI_STATEMENTS);
#ifdef PDO_USE_MYSQLND
	REGISTER_PDO_CLASS_CONST_LONG("MYSQL_ATTR_SSL_VERIFY_SERVER_CERT", (gear_long)PDO_MYSQL_ATTR_SSL_VERIFY_SERVER_CERT);
#endif

#ifdef PDO_USE_MYSQLND
	mysqlnd_reverse_api_register_api(&pdo_mysql_reverse_api);
#endif

	return hyss_pdo_register_driver(&pdo_mysql_driver);
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
static HYSS_MSHUTDOWN_FUNCTION(pdo_mysql)
{
	hyss_pdo_unregister_driver(&pdo_mysql_driver);
#if PDO_USE_MYSQLND
	UNREGISTER_ICS_ENTRIES();
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
static HYSS_MINFO_FUNCTION(pdo_mysql)
{
	hyss_info_print_table_start();

	hyss_info_print_table_header(2, "PDO Driver for MySQL", "enabled");
	hyss_info_print_table_row(2, "Client API version", mysql_get_client_info());

	hyss_info_print_table_end();

#ifndef HYSS_WIN32
	DISPLAY_ICS_ENTRIES();
#endif
}
/* }}} */


#if PDO_USE_MYSQLND && PDO_DBG_ENABLED
/* {{{ HYSS_RINIT_FUNCTION
 */
static HYSS_RINIT_FUNCTION(pdo_mysql)
{
	if (PDO_MYSQL_G(debug)) {
		MYSQLND_DEBUG *dbg = mysqlnd_debug_init(mysqlnd_debug_std_no_trace_funcs);
		if (!dbg) {
			return FAILURE;
		}
		dbg->m->set_mode(dbg, PDO_MYSQL_G(debug));
		PDO_MYSQL_G(dbg) = dbg;
	}

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_RSHUTDOWN_FUNCTION
 */
static HYSS_RSHUTDOWN_FUNCTION(pdo_mysql)
{
	MYSQLND_DEBUG *dbg = PDO_MYSQL_G(dbg);
	PDO_DBG_ENTER("RSHUTDOWN");
	if (dbg) {
		dbg->m->close(dbg);
		dbg->m->free_handle(dbg);
		PDO_MYSQL_G(dbg) = NULL;
	}

	return SUCCESS;
}
/* }}} */
#endif

/* {{{ HYSS_GINIT_FUNCTION
 */
static HYSS_GINIT_FUNCTION(pdo_mysql)
{
#if defined(COMPILE_DL_PDO_MYSQL) && defined(ZTS)
GEAR_PBCLS_CACHE_UPDATE();
#endif
#ifndef HYSS_WIN32
	pdo_mysql_globals->default_socket = NULL;
#endif
#if PDO_DBG_ENABLED
	pdo_mysql_globals->debug = NULL;	/* The actual string */
	pdo_mysql_globals->dbg = NULL;	/* The DBG object*/
#endif
}
/* }}} */

/* {{{ pdo_mysql_functions[] */
static const gear_function_entry pdo_mysql_functions[] = {
	HYSS_FE_END
};
/* }}} */

/* {{{ pdo_mysql_deps[] */
static const gear_capi_dep pdo_mysql_deps[] = {
	GEAR_CAPI_REQUIRED("pdo")
#ifdef PDO_USE_MYSQLND
	GEAR_CAPI_REQUIRED("mysqlnd")
#endif
	GEAR_CAPI_END
};
/* }}} */

/* {{{ pdo_mysql_capi_entry */
gear_capi_entry pdo_mysql_capi_entry = {
	STANDARD_CAPI_HEADER_EX, NULL,
	pdo_mysql_deps,
	"pdo_mysql",
	pdo_mysql_functions,
	HYSS_MINIT(pdo_mysql),
	HYSS_MSHUTDOWN(pdo_mysql),
#if PDO_USE_MYSQLND && PDO_DBG_ENABLED
	HYSS_RINIT(pdo_mysql),
	HYSS_RSHUTDOWN(pdo_mysql),
#else
	NULL,
	NULL,
#endif
	HYSS_MINFO(pdo_mysql),
	HYSS_PDO_MYSQL_VERSION,
	HYSS_CAPI_GLOBALS(pdo_mysql),
	HYSS_GINIT(pdo_mysql),
	NULL,
	NULL,
	STANDARD_CAPI_PROPERTIES_EX
};
/* }}} */

