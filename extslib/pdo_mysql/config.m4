dnl config.m4 for extension pdo_mysql
dnl vim: se ts=2 sw=2 et:

HYSS_ARG_WITH(pdo-mysql, for MySQL support for PDO,
[  --with-pdo-mysql[=DIR]    PDO: MySQL support. DIR is the MySQL base directory
                          If no value or mysqlnd is passed as DIR, the
                          MySQL native driver will be used])

if test -z "$HYSS_ZLIB_DIR"; then
  HYSS_ARG_WITH(zlib-dir, for the location of libz,
  [  --with-zlib-dir[=DIR]     PDO_MySQL: Set the path to libz install prefix], no, no)
fi

if test "$HYSS_PDO_MYSQL" != "no"; then
  dnl This depends on extslib/mysqli/config.m4 providing the
  dnl HYSS_MYSQL_SOCKET_SEARCH macro and --with-mysql-sock configure option.
  AC_MSG_CHECKING([for MySQL UNIX socket location])
  if test "$HYSS_MYSQL_SOCK" != "no" && test "$HYSS_MYSQL_SOCK" != "yes"; then
    MYSQL_SOCK=$HYSS_MYSQL_SOCK
    AC_DEFINE_UNQUOTED(HYSS_MYSQL_UNIX_SOCK_ADDR, "$MYSQL_SOCK", [ ])
    AC_MSG_RESULT([$MYSQL_SOCK])
  elif test "$HYSS_MYSQL_SOCK" = "yes"; then
    HYSS_MYSQL_SOCKET_SEARCH
  else
    AC_MSG_RESULT([no])
  fi

  if test "$HYSS_PDO" = "no" && test "$ext_shared" = "no"; then
    AC_MSG_ERROR([PDO is not enabled! Add --enable-pdo to your configure line.])
  fi

  AC_DEFUN([PDO_MYSQL_LIB_CHK], [
    str="$PDO_MYSQL_DIR/$1/libmysqlclient*"
    for j in `echo $str`; do
      if test -r $j; then
        PDO_MYSQL_LIB_DIR=$PDO_MYSQL_DIR/$1
        break 2
      fi
    done
  ])

  if test "$HYSS_PDO_MYSQL" != "yes" && test "$HYSS_PDO_MYSQL" != "mysqlnd"; then
    if test -f $HYSS_PDO_MYSQL && test -x $HYSS_PDO_MYSQL ; then
      PDO_MYSQL_CONFIG=$HYSS_PDO_MYSQL
    else
      if test -d "$HYSS_PDO_MYSQL" ; then
        if test -x "$HYSS_PDO_MYSQL/bin/mysql_config" ; then
          PDO_MYSQL_CONFIG="$HYSS_PDO_MYSQL/bin/mysql_config"
        else
          PDO_MYSQL_DIR="$HYSS_PDO_MYSQL"
        fi
      fi
    fi
  fi

  if test "$HYSS_PDO_MYSQL" = "yes" || test "$HYSS_PDO_MYSQL" = "mysqlnd"; then
    dnl enables build of mysqnd library
    HYSS_MYSQLND_ENABLED=yes
    AC_DEFINE([PDO_USE_MYSQLND], 1, [Whether pdo_mysql uses mysqlnd])
  else
    AC_DEFINE(HAVE_MYSQL, 1, [Whether you have MySQL])

    AC_MSG_CHECKING([for mysql_config])
    if test -n "$PDO_MYSQL_CONFIG"; then
      AC_MSG_RESULT($PDO_MYSQL_CONFIG)
      if test "x$SED" = "x"; then
        AC_PATH_PROG(SED, sed)
      fi
      if test "$enable_maintainer_zts" = "yes"; then
        PDO_MYSQL_LIBNAME=mysqlclient_r
        PDO_MYSQL_LIBS=`$PDO_MYSQL_CONFIG --libs_r | $SED -e "s/'//g"`
      else
        PDO_MYSQL_LIBNAME=mysqlclient
        PDO_MYSQL_LIBS=`$PDO_MYSQL_CONFIG --libs | $SED -e "s/'//g"`
      fi
      PDO_MYSQL_INCLUDE=`$PDO_MYSQL_CONFIG --cflags | $SED -e "s/'//g"`
    elif test -n "$PDO_MYSQL_DIR"; then
      AC_MSG_RESULT([not found])
      AC_MSG_CHECKING([for mysql install under $PDO_MYSQL_DIR])
      if test -r $PDO_MYSQL_DIR/include/mysql; then
        PDO_MYSQL_INC_DIR=$PDO_MYSQL_DIR/include/mysql
      else
        PDO_MYSQL_INC_DIR=$PDO_MYSQL_DIR/include
      fi
      if test -r $PDO_MYSQL_DIR/$HYSS_LIBDIR/mysql; then
        PDO_MYSQL_LIB_DIR=$PDO_MYSQL_DIR/$HYSS_LIBDIR/mysql
      else
        PDO_MYSQL_LIB_DIR=$PDO_MYSQL_DIR/$HYSS_LIBDIR
      fi

      if test -r "$PDO_MYSQL_LIB_DIR"; then
        AC_MSG_RESULT([libs under $PDO_MYSQL_LIB_DIR; seems promising])
      else
        AC_MSG_RESULT([can not find it])
        AC_MSG_ERROR([Unable to find your mysql installation])
      fi

      HYSS_ADD_INCLUDE($PDO_MYSQL_INC_DIR)
      PDO_MYSQL_INCLUDE=-I$PDO_MYSQL_INC_DIR
    else
      AC_MSG_RESULT([not found])
      AC_MSG_ERROR([Unable to find your mysql installation])
    fi

    HYSS_CHECK_LIBRARY($PDO_MYSQL_LIBNAME, mysql_commit,
    [
      HYSS_EVAL_INCLINE($PDO_MYSQL_INCLUDE)
      HYSS_EVAL_LIBLINE($PDO_MYSQL_LIBS, PDO_MYSQL_SHARED_LIBADD)
    ],[
      if test "$HYSS_ZLIB_DIR" != "no"; then
        HYSS_ADD_LIBRARY_WITH_PATH(z, $HYSS_ZLIB_DIR, PDO_MYSQL_SHARED_LIBADD)
        HYSS_CHECK_LIBRARY($PDO_MYSQL_LIBNAME, mysql_commit, [], [
          AC_MSG_ERROR([PDO_MYSQL configure failed, MySQL 4.1 needed. Please check config.log for more information.])
        ], [
          -L$HYSS_ZLIB_DIR/$HYSS_LIBDIR -L$PDO_MYSQL_LIB_DIR
        ])
        PDO_MYSQL_LIBS="$PDO_MYSQL_LIBS -L$HYSS_ZLIB_DIR/$HYSS_LIBDIR -lz"
      else
        HYSS_ADD_LIBRARY(z,, PDO_MYSQL_SHARED_LIBADD)
        HYSS_CHECK_LIBRARY($PDO_MYSQL_LIBNAME, mysql_query, [], [
          AC_MSG_ERROR([Try adding --with-zlib-dir=<DIR>. Please check config.log for more information.])
        ], [
          -L$PDO_MYSQL_LIB_DIR
        ])
        PDO_MYSQL_LIBS="$PDO_MYSQL_LIBS -lz"
      fi

      HYSS_EVAL_INCLINE($PDO_MYSQL_INCLUDE)
      HYSS_EVAL_LIBLINE($PDO_MYSQL_LIBS, PDO_MYSQL_SHARED_LIBADD)
    ],[
      $PDO_MYSQL_LIBS
    ])
  fi

  ifdef([HYSS_CHECK_PDO_INCLUDES],
  [
    HYSS_CHECK_PDO_INCLUDES
  ],[
    AC_MSG_CHECKING([for PDO includes])
    if test -f $abs_srcdir/include/hyss/extslib/pdo/hyss_pdo_driver.h; then
      pdo_cv_inc_path=$abs_srcdir/extslib
    elif test -f $abs_srcdir/extslib/pdo/hyss_pdo_driver.h; then
      pdo_cv_inc_path=$abs_srcdir/extslib
    elif test -f $hyssincludedir/extslib/pdo/hyss_pdo_driver.h; then
      pdo_cv_inc_path=$hyssincludedir/extslib
    else
      AC_MSG_ERROR([Cannot find hyss_pdo_driver.h.])
    fi
    AC_MSG_RESULT($pdo_cv_inc_path)
  ])

  if test -n "$PDO_MYSQL_CONFIG"; then
    PDO_MYSQL_SOCKET=`$PDO_MYSQL_CONFIG --socket`
    AC_DEFINE_UNQUOTED(PDO_MYSQL_UNIX_ADDR, "$PDO_MYSQL_SOCKET", [ ])
  fi

  dnl fix after renaming to pdo_mysql
  HYSS_NEW_EXTENSION(pdo_mysql, pdo_mysql.c mysql_driver.c mysql_statement.c, $ext_shared,,-I$pdo_cv_inc_path -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
  ifdef([HYSS_ADD_EXTENSION_DEP],
  [
    HYSS_ADD_EXTENSION_DEP(pdo_mysql, pdo)
    if test "$HYSS_MYSQL" = "mysqlnd"; then
      HYSS_ADD_EXTENSION_DEP(pdo_mysql, mysqlnd)
    fi
  ])
  PDO_MYSQL_CAPI_TYPE=external

  HYSS_SUBST(PDO_MYSQL_SHARED_LIBADD)
  HYSS_SUBST_OLD(PDO_MYSQL_CAPI_TYPE)
fi
