/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PGSQL_H
#define HYSS_PGSQL_H

#if HAVE_PGSQL

#define HYSS_PGSQL_API_VERSION 20140217

extern gear_capi_entry pgsql_capi_entry;
#define pgsql_capi_ptr &pgsql_capi_entry

#include "hyss_version.h"
#define HYSS_PGSQL_VERSION HYSS_VERSION

#ifdef HYSS_PGSQL_PRIVATE
#undef SOCKET_SIZE_TYPE
#include <libpq-fe.h>

#ifdef HYSS_WIN32
#define INV_WRITE            0x00020000
#define INV_READ             0x00040000
#undef HYSS_PGSQL_API
#ifdef PGSQL_EXPORTS
#define HYSS_PGSQL_API __declspec(dllexport)
#else
#define HYSS_PGSQL_API __declspec(dllimport)
#endif
#else
#include <libpq/libpq-fs.h>
# if defined(__GNUC__) && __GNUC__ >= 4
#  define HYSS_PGSQL_API __attribute__ ((visibility("default")))
# else
#  define HYSS_PGSQL_API
# endif
#endif

#ifdef HAVE_PG_CONFIG_H
#include <pg_config.h>
#endif

#ifdef HAVE_PGSQL_WITH_MULTIBYTE_SUPPORT
const char * pg_encoding_to_char(int encoding);
#endif

HYSS_MINIT_FUNCTION(pgsql);
HYSS_MSHUTDOWN_FUNCTION(pgsql);
HYSS_RINIT_FUNCTION(pgsql);
HYSS_RSHUTDOWN_FUNCTION(pgsql);
HYSS_MINFO_FUNCTION(pgsql);
/* connection functions */
HYSS_FUNCTION(pg_connect);
HYSS_FUNCTION(pg_pconnect);
HYSS_FUNCTION(pg_connect_poll);
HYSS_FUNCTION(pg_close);
HYSS_FUNCTION(pg_connection_reset);
HYSS_FUNCTION(pg_connection_status);
HYSS_FUNCTION(pg_connection_busy);
HYSS_FUNCTION(pg_host);
HYSS_FUNCTION(pg_dbname);
HYSS_FUNCTION(pg_port);
HYSS_FUNCTION(pg_tty);
HYSS_FUNCTION(pg_options);
HYSS_FUNCTION(pg_version);
HYSS_FUNCTION(pg_ping);
#if HAVE_PQPARAMETERSTATUS
HYSS_FUNCTION(pg_parameter_status);
#endif
#if HAVE_PGTRANSACTIONSTATUS
HYSS_FUNCTION(pg_transaction_status);
#endif
/* query functions */
HYSS_FUNCTION(pg_query);
#if HAVE_PQEXECPARAMS
HYSS_FUNCTION(pg_query_params);
#endif
#if HAVE_PQPREPARE
HYSS_FUNCTION(pg_prepare);
#endif
#if HAVE_PQEXECPREPARED
HYSS_FUNCTION(pg_execute);
#endif
HYSS_FUNCTION(pg_send_query);
#if HAVE_PQSENDQUERYPARAMS
HYSS_FUNCTION(pg_send_query_params);
#endif
#if HAVE_PQSENDPREPARE
HYSS_FUNCTION(pg_send_prepare);
#endif
#if HAVE_PQSENDQUERYPREPARED
HYSS_FUNCTION(pg_send_execute);
#endif
HYSS_FUNCTION(pg_cancel_query);
/* result functions */
HYSS_FUNCTION(pg_fetch_assoc);
HYSS_FUNCTION(pg_fetch_array);
HYSS_FUNCTION(pg_fetch_object);
HYSS_FUNCTION(pg_fetch_result);
HYSS_FUNCTION(pg_fetch_row);
HYSS_FUNCTION(pg_fetch_all);
HYSS_FUNCTION(pg_fetch_all_columns);
#if HAVE_PQCMDTUPLES
HYSS_FUNCTION(pg_affected_rows);
#endif
HYSS_FUNCTION(pg_get_result);
HYSS_FUNCTION(pg_result_seek);
HYSS_FUNCTION(pg_result_status);
HYSS_FUNCTION(pg_free_result);
HYSS_FUNCTION(pg_last_oid);
HYSS_FUNCTION(pg_num_rows);
HYSS_FUNCTION(pg_num_fields);
HYSS_FUNCTION(pg_field_name);
HYSS_FUNCTION(pg_field_num);
HYSS_FUNCTION(pg_field_size);
HYSS_FUNCTION(pg_field_type);
HYSS_FUNCTION(pg_field_type_oid);
HYSS_FUNCTION(pg_field_prtlen);
HYSS_FUNCTION(pg_field_is_null);
HYSS_FUNCTION(pg_field_table);
/* async message functions */
HYSS_FUNCTION(pg_get_notify);
HYSS_FUNCTION(pg_socket);
HYSS_FUNCTION(pg_consume_input);
HYSS_FUNCTION(pg_flush);
HYSS_FUNCTION(pg_get_pid);
/* error message functions */
HYSS_FUNCTION(pg_result_error);
#if HAVE_PQRESULTERRORFIELD
HYSS_FUNCTION(pg_result_error_field);
#endif
HYSS_FUNCTION(pg_last_error);
HYSS_FUNCTION(pg_last_notice);
/* copy functions */
HYSS_FUNCTION(pg_put_line);
HYSS_FUNCTION(pg_end_copy);
HYSS_FUNCTION(pg_copy_to);
HYSS_FUNCTION(pg_copy_from);
/* large object functions */
HYSS_FUNCTION(pg_lo_create);
HYSS_FUNCTION(pg_lo_unlink);
HYSS_FUNCTION(pg_lo_open);
HYSS_FUNCTION(pg_lo_close);
HYSS_FUNCTION(pg_lo_read);
HYSS_FUNCTION(pg_lo_write);
HYSS_FUNCTION(pg_lo_read_all);
HYSS_FUNCTION(pg_lo_import);
HYSS_FUNCTION(pg_lo_export);
HYSS_FUNCTION(pg_lo_seek);
HYSS_FUNCTION(pg_lo_tell);
#if HAVE_PG_LO_TRUNCATE
HYSS_FUNCTION(pg_lo_truncate);
#endif

/* debugging functions */
HYSS_FUNCTION(pg_trace);
HYSS_FUNCTION(pg_untrace);

/* utility functions */
HYSS_FUNCTION(pg_client_encoding);
HYSS_FUNCTION(pg_set_client_encoding);
#if HAVE_PQSETERRORVERBOSITY
HYSS_FUNCTION(pg_set_error_verbosity);
#endif
#if HAVE_PQESCAPE
HYSS_FUNCTION(pg_escape_string);
HYSS_FUNCTION(pg_escape_bytea);
HYSS_FUNCTION(pg_unescape_bytea);
HYSS_FUNCTION(pg_escape_literal);
HYSS_FUNCTION(pg_escape_identifier);
#endif

/* misc functions */
HYSS_FUNCTION(pg_meta_data);
HYSS_FUNCTION(pg_convert);
HYSS_FUNCTION(pg_insert);
HYSS_FUNCTION(pg_update);
HYSS_FUNCTION(pg_delete);
HYSS_FUNCTION(pg_select);

/* connection options - ToDo: Add async connection option */
#define PGSQL_CONNECT_FORCE_NEW     (1<<1)
#define PGSQL_CONNECT_ASYNC         (1<<2)
/* hyss_pgsql_convert options */
#define PGSQL_CONV_IGNORE_DEFAULT   (1<<1)     /* Do not use DEAFULT value by removing field from returned array */
#define PGSQL_CONV_FORCE_NULL       (1<<2)     /* Convert to NULL if string is null string */
#define PGSQL_CONV_IGNORE_NOT_NULL  (1<<3)     /* Ignore NOT NULL constraints */
#define PGSQL_CONV_OPTS             (PGSQL_CONV_IGNORE_DEFAULT|PGSQL_CONV_FORCE_NULL|PGSQL_CONV_IGNORE_NOT_NULL)
/* hyss_pgsql_insert/update/select/delete options */
#define PGSQL_DML_NO_CONV           (1<<8)     /* Do not call hyss_pgsql_convert() */
#define PGSQL_DML_EXEC              (1<<9)     /* Execute query */
#define PGSQL_DML_ASYNC             (1<<10)    /* Do async query */
#define PGSQL_DML_STRING            (1<<11)    /* Return query string */
#define PGSQL_DML_ESCAPE            (1<<12)    /* No convert, but escape only */

/* exported functions */
HYSS_PGSQL_API int hyss_pgsql_meta_data(PGconn *pg_link, const char *table_name, zval *meta, gear_bool extended);
HYSS_PGSQL_API int hyss_pgsql_convert(PGconn *pg_link, const char *table_name, const zval *values, zval *result, gear_ulong opt);
HYSS_PGSQL_API int hyss_pgsql_insert(PGconn *pg_link, const char *table, zval *values, gear_ulong opt, gear_string **sql);
HYSS_PGSQL_API int hyss_pgsql_update(PGconn *pg_link, const char *table, zval *values, zval *ids, gear_ulong opt , gear_string **sql);
HYSS_PGSQL_API int hyss_pgsql_delete(PGconn *pg_link, const char *table, zval *ids, gear_ulong opt, gear_string **sql);
HYSS_PGSQL_API int hyss_pgsql_select(PGconn *pg_link, const char *table, zval *ids, zval *ret_array, gear_ulong opt, long fetch_option, gear_string **sql );
HYSS_PGSQL_API int hyss_pgsql_result2array(PGresult *pg_result, zval *ret_array, long fetch_option);

/* internal functions */
static void hyss_pgsql_do_connect(INTERNAL_FUNCTION_PARAMETERS,int persistent);
static void hyss_pgsql_get_link_info(INTERNAL_FUNCTION_PARAMETERS, int entry_type);
static void hyss_pgsql_get_result_info(INTERNAL_FUNCTION_PARAMETERS, int entry_type);
static char *get_field_name(PGconn *pgsql, Oid oid, HashTable *list);
static void hyss_pgsql_get_field_info(INTERNAL_FUNCTION_PARAMETERS, int entry_type);
static void hyss_pgsql_data_info(INTERNAL_FUNCTION_PARAMETERS, int entry_type);
static void hyss_pgsql_do_async(INTERNAL_FUNCTION_PARAMETERS,int entry_type);

static size_t hyss_pgsql_fd_write(hyss_stream *stream, const char *buf, size_t count);
static size_t hyss_pgsql_fd_read(hyss_stream *stream, char *buf, size_t count);
static int hyss_pgsql_fd_close(hyss_stream *stream, int close_handle);
static int hyss_pgsql_fd_flush(hyss_stream *stream);
static int hyss_pgsql_fd_set_option(hyss_stream *stream, int option, int value, void *ptrparam);
static int hyss_pgsql_fd_cast(hyss_stream *stream, int cast_as, void **ret);

typedef enum _hyss_pgsql_data_type {
	/* boolean */
	PG_BOOL,
	/* number */
	PG_OID,
	PG_INT2,
	PG_INT4,
	PG_INT8,
	PG_FLOAT4,
	PG_FLOAT8,
	PG_NUMERIC,
	PG_MONEY,
	/* character */
	PG_TEXT,
	PG_CHAR,
	PG_VARCHAR,
	/* time and interval */
	PG_UNIX_TIME,
	PG_UNIX_TIME_INTERVAL,
	PG_DATE,
	PG_TIME,
	PG_TIME_WITH_TIMEZONE,
	PG_TIMESTAMP,
	PG_TIMESTAMP_WITH_TIMEZONE,
	PG_INTERVAL,
	/* binary */
	PG_BYTEA,
	/* network */
	PG_CIDR,
	PG_INET,
	PG_MACADDR,
	/* bit */
	PG_BIT,
	PG_VARBIT,
	/* geometoric */
	PG_LINE,
	PG_LSEG,
	PG_POINT,
	PG_BOX,
	PG_PATH,
	PG_POLYGON,
	PG_CIRCLE,
	/* unknown and system */
	PG_UNKNOWN
} hyss_pgsql_data_type;

typedef struct pgLofp {
	PGconn *conn;
	int lofd;
} pgLofp;

typedef struct _hyss_pgsql_result_handle {
	PGconn *conn;
	PGresult *result;
	int row;
} pgsql_result_handle;

typedef struct _hyss_pgsql_notice {
	char *message;
	size_t len;
} hyss_pgsql_notice;

static const hyss_stream_ops hyss_stream_pgsql_fd_ops = {
	hyss_pgsql_fd_write,
	hyss_pgsql_fd_read,
	hyss_pgsql_fd_close,
	hyss_pgsql_fd_flush,
	"PostgreSQL link",
	NULL, /* seek */
	hyss_pgsql_fd_cast, /* cast */
	NULL, /* stat */
	hyss_pgsql_fd_set_option
};

GEAR_BEGIN_CAPI_GLOBALS(pgsql)
	gear_long num_links,num_persistent;
	gear_long max_links,max_persistent;
	gear_long allow_persistent;
	gear_long auto_reset_persistent;
	int le_lofp,le_string;
	int ignore_notices,log_notices;
	HashTable notices;  /* notice message for each connection */
	gear_resource *default_link; /* default link when connection is omitted */
	HashTable hashes; /* hashes for each connection */
GEAR_END_CAPI_GLOBALS(pgsql)

GEAR_EXTERN_CAPI_GLOBALS(pgsql)
# define PGG(v) GEAR_CAPI_GLOBALS_ACCESSOR(pgsql, v)

#if defined(ZTS) && defined(COMPILE_DL_PGSQL)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#endif

#else

#define pgsql_capi_ptr NULL

#endif

#define hyssext_pgsql_ptr pgsql_capi_ptr

#endif /* HYSS_PGSQL_H */
