/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SOCKETS_H
#define HYSS_SOCKETS_H

#if HAVE_CONFIG_H
# include "config.h"
#endif

#if HAVE_SOCKETS

#include <hyss.h>
#ifdef HYSS_WIN32
# include "windows_common.h"
#endif

#define HYSS_SOCKETS_VERSION HYSS_VERSION

extern gear_capi_entry sockets_capi_entry;
#define hyssext_sockets_ptr &sockets_capi_entry

#ifdef HYSS_WIN32
#include <Winsock2.h>
#else
#if HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#endif

#ifndef HYSS_WIN32
typedef int HYSS_SOCKET;
# define HYSS_SOCKETS_API HYSSAPI
#else
# define HYSS_SOCKETS_API __declspec(dllexport)
typedef SOCKET HYSS_SOCKET;
#endif

typedef struct {
	HYSS_SOCKET	bsd_socket;
	int			type;
	int			error;
	int			blocking;
	zval		zstream;
} hyss_socket;

#ifdef HYSS_WIN32
struct	sockaddr_un {
	short	sun_family;
	char	sun_path[108];
};
#endif

HYSS_SOCKETS_API int hyss_sockets_le_socket(void);
HYSS_SOCKETS_API hyss_socket *hyss_create_socket(void);
HYSS_SOCKETS_API void hyss_destroy_socket(gear_resource *rsrc);
HYSS_SOCKETS_API void hyss_destroy_sockaddr(gear_resource *rsrc);

#define hyss_sockets_le_socket_name "Socket"
#define hyss_sockets_le_addrinfo_name "AddressInfo"

#define HYSS_SOCKET_ERROR(socket, msg, errn) \
		do { \
			int _err = (errn); /* save value to avoid repeated calls to WSAGetLastError() on Windows */ \
			(socket)->error = _err; \
			SOCKETS_G(last_error) = _err; \
			if (_err != EAGAIN && _err != EWOULDBLOCK && _err != EINPROGRESS) { \
				hyss_error_docref(NULL, E_WARNING, "%s [%d]: %s", msg, _err, sockets_strerror(_err)); \
			} \
		} while (0)

GEAR_BEGIN_CAPI_GLOBALS(sockets)
	int last_error;
	char *strerror_buf;
#ifdef HYSS_WIN32
	uint32_t wsa_child_count;
	HashTable wsa_info;
#endif
GEAR_END_CAPI_GLOBALS(sockets)

GEAR_EXTERN_CAPI_GLOBALS(sockets)
#define SOCKETS_G(v) GEAR_CAPI_GLOBALS_ACCESSOR(sockets, v)

enum sockopt_return {
	SOCKOPT_ERROR,
	SOCKOPT_CONTINUE,
	SOCKOPT_SUCCESS
};

char *sockets_strerror(int error);
hyss_socket *socket_import_file_descriptor(HYSS_SOCKET sock);

#else
#define hyssext_sockets_ptr NULL
#endif

#if defined(_AIX) && !defined(HAVE_SA_SS_FAMILY)
# define ss_family __ss_family
#endif

#endif

