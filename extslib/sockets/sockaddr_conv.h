/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SOCKADR_CONV_H
#define HYSS_SOCKADR_CONV_H

#include <hyss_network.h>
#include "hyss_sockets.h" /* hyss_socket */

#ifndef HYSS_WIN32
# include <netinet/in.h>
#else
# include <Winsock2.h>
#endif


/*
 * Convert an IPv6 literal or a hostname info a sockaddr_in6.
 * The IPv6 literal can be a IPv4 mapped address (like ::ffff:127.0.0.1).
 * If the hostname yields no IPv6 addresses, a mapped IPv4 address may be returned (AI_V4MAPPED)
 */
int hyss_set_inet6_addr(struct sockaddr_in6 *sin6, char *string, hyss_socket *hyss_sock);

/*
 * Convert an IPv4 literal or a hostname into a sockaddr_in.
 */
int hyss_set_inet_addr(struct sockaddr_in *sin, char *string, hyss_socket *hyss_sock);

/*
 * Calls either hyss_set_inet6_addr() or hyss_set_inet_addr(), depending on the type of the socket.
 */
int hyss_set_inet46_addr(hyss_sockaddr_storage *ss, socklen_t *ss_len, char *string, hyss_socket *hyss_sock);

#endif
