/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyss.h>
#include <hyss_network.h>
#include "hyss_sockets.h"

#ifdef HYSS_WIN32
#include "windows_common.h"
#else
#include <netdb.h>
#include <arpa/inet.h>
#endif

extern int hyss_string_to_if_index(const char *val, unsigned *out);

#if HAVE_IPV6
/* Sets addr by hostname, or by ip in string form (AF_INET6) */
int hyss_set_inet6_addr(struct sockaddr_in6 *sin6, char *string, hyss_socket *hyss_sock) /* {{{ */
{
	struct in6_addr tmp;
#if HAVE_GETADDRINFO
	struct addrinfo hints;
	struct addrinfo *addrinfo = NULL;
#endif
	char *scope = strchr(string, '%');

	if (inet_pton(AF_INET6, string, &tmp)) {
		memcpy(&(sin6->sin6_addr.s6_addr), &(tmp.s6_addr), sizeof(struct in6_addr));
	} else {
#if HAVE_GETADDRINFO

		memset(&hints, 0, sizeof(struct addrinfo));
		hints.ai_family = AF_INET6;
#if HAVE_AI_V4MAPPED
		hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG;
#else
		hints.ai_flags = AI_ADDRCONFIG;
#endif
		getaddrinfo(string, NULL, &hints, &addrinfo);
		if (!addrinfo) {
#ifdef HYSS_WIN32
			HYSS_SOCKET_ERROR(hyss_sock, "Host lookup failed", WSAGetLastError());
#else
			HYSS_SOCKET_ERROR(hyss_sock, "Host lookup failed", (-10000 - h_errno));
#endif
			return 0;
		}
		if (addrinfo->ai_family != PF_INET6 || addrinfo->ai_addrlen != sizeof(struct sockaddr_in6)) {
			hyss_error_docref(NULL, E_WARNING, "Host lookup failed: Non AF_INET6 domain returned on AF_INET6 socket");
			freeaddrinfo(addrinfo);
			return 0;
		}

		memcpy(&(sin6->sin6_addr.s6_addr), ((struct sockaddr_in6*)(addrinfo->ai_addr))->sin6_addr.s6_addr, sizeof(struct in6_addr));
		freeaddrinfo(addrinfo);

#else
		/* No IPv6 specific hostname resolution is available on this system? */
		hyss_error_docref(NULL, E_WARNING, "Host lookup failed: getaddrinfo() not available on this system");
		return 0;
#endif

	}

	if (scope++) {
		gear_long lval = 0;
		double dval = 0;
		unsigned scope_id = 0;

		if (IS_LONG == is_numeric_string(scope, strlen(scope), &lval, &dval, 0)) {
			if (lval > 0 && (gear_ulong)lval <= UINT_MAX) {
				scope_id = lval;
			}
		} else {
			hyss_string_to_if_index(scope, &scope_id);
		}

		sin6->sin6_scope_id = scope_id;
	}

	return 1;
}
/* }}} */
#endif

/* Sets addr by hostname, or by ip in string form (AF_INET)  */
int hyss_set_inet_addr(struct sockaddr_in *sin, char *string, hyss_socket *hyss_sock) /* {{{ */
{
	struct in_addr tmp;
	struct hostent *host_entry;

	if (inet_aton(string, &tmp)) {
		sin->sin_addr.s_addr = tmp.s_addr;
	} else {
		if (strlen(string) > MAXFQDNLEN || ! (host_entry = hyss_network_gethostbyname(string))) {
			/* Note: < -10000 indicates a host lookup error */
#ifdef HYSS_WIN32
			HYSS_SOCKET_ERROR(hyss_sock, "Host lookup failed", WSAGetLastError());
#else
			HYSS_SOCKET_ERROR(hyss_sock, "Host lookup failed", (-10000 - h_errno));
#endif
			return 0;
		}
		if (host_entry->h_addrtype != AF_INET) {
			hyss_error_docref(NULL, E_WARNING, "Host lookup failed: Non AF_INET domain returned on AF_INET socket");
			return 0;
		}
		memcpy(&(sin->sin_addr.s_addr), host_entry->h_addr_list[0], host_entry->h_length);
	}

	return 1;
}
/* }}} */

/* Sets addr by hostname or by ip in string form (AF_INET or AF_INET6,
 * depending on the socket) */
int hyss_set_inet46_addr(hyss_sockaddr_storage *ss, socklen_t *ss_len, char *string, hyss_socket *hyss_sock) /* {{{ */
{
	if (hyss_sock->type == AF_INET) {
		struct sockaddr_in t = {0};
		if (hyss_set_inet_addr(&t, string, hyss_sock)) {
			memcpy(ss, &t, sizeof t);
			ss->ss_family = AF_INET;
			*ss_len = sizeof(t);
			return 1;
		}
	}
#if HAVE_IPV6
	else if (hyss_sock->type == AF_INET6) {
		struct sockaddr_in6 t = {0};
		if (hyss_set_inet6_addr(&t, string, hyss_sock)) {
			memcpy(ss, &t, sizeof t);
			ss->ss_family = AF_INET6;
			*ss_len = sizeof(t);
			return 1;
		}
	}
#endif
	else {
		hyss_error_docref(NULL, E_WARNING,
			"IP address used in the context of an unexpected type of socket");
	}
	return 0;
}
