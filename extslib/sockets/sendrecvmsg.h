/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SENDRECVMSG_H
#define HYSS_SENDRECVMSG_H 1

#include <hyss.h>
#include "conversions.h"

/* for sockets.c */
HYSS_FUNCTION(socket_sendmsg);
HYSS_FUNCTION(socket_recvmsg);
HYSS_FUNCTION(socket_cmsg_space);

void hyss_socket_sendrecvmsg_init(INIT_FUNC_ARGS);
void hyss_socket_sendrecvmsg_shutdown(SHUTDOWN_FUNC_ARGS);

int hyss_do_setsockopt_ipv6_rfc3542(hyss_socket *hyss_sock, int level, int optname, zval *arg4);
int hyss_do_getsockopt_ipv6_rfc3542(hyss_socket *hyss_sock, int level, int optname, zval *result);

/* for conversions.c */
typedef struct {
	int	cmsg_level;	/* originating protocol */
	int	cmsg_type;	/* protocol-specific type */
} anc_reg_key;

typedef size_t (calculate_req_space)(const zval *value, ser_context *ctx);

typedef struct {
	socklen_t size; /* size of native structure */
	socklen_t var_el_size; /* size of repeatable component */
	calculate_req_space *calc_space;
	from_zval_write_field *from_array;
	to_zval_read_field *to_array;
} ancillary_reg_entry;

ancillary_reg_entry *get_ancillary_reg_entry(int cmsg_level, int msg_type);

#endif
