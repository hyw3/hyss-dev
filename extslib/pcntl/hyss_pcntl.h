/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PCNTL_H
#define HYSS_PCNTL_H

#if defined(WCONTINUED) && defined(WIFCONTINUED)
#define HAVE_WCONTINUED 1
#endif

extern gear_capi_entry pcntl_capi_entry;
#define hyssext_pcntl_ptr &pcntl_capi_entry

#include "hyss_version.h"
#define HYSS_PCNTL_VERSION HYSS_VERSION

HYSS_MINIT_FUNCTION(pcntl);
HYSS_MSHUTDOWN_FUNCTION(pcntl);
HYSS_RINIT_FUNCTION(pcntl);
HYSS_RSHUTDOWN_FUNCTION(pcntl);
HYSS_MINFO_FUNCTION(pcntl);

HYSS_FUNCTION(pcntl_alarm);
HYSS_FUNCTION(pcntl_fork);
HYSS_FUNCTION(pcntl_waitpid);
HYSS_FUNCTION(pcntl_wait);
HYSS_FUNCTION(pcntl_wifexited);
HYSS_FUNCTION(pcntl_wifstopped);
HYSS_FUNCTION(pcntl_wifsignaled);
#ifdef HAVE_WCONTINUED
HYSS_FUNCTION(pcntl_wifcontinued);
#endif
HYSS_FUNCTION(pcntl_wexitstatus);
HYSS_FUNCTION(pcntl_wtermsig);
HYSS_FUNCTION(pcntl_wstopsig);
HYSS_FUNCTION(pcntl_signal);
HYSS_FUNCTION(pcntl_signal_get_handler);
HYSS_FUNCTION(pcntl_signal_dispatch);
HYSS_FUNCTION(pcntl_get_last_error);
HYSS_FUNCTION(pcntl_strerror);
#ifdef HAVE_SIGPROCMASK
HYSS_FUNCTION(pcntl_sigprocmask);
#endif
#ifdef HAVE_STRUCT_SIGINFO_T
# if HAVE_SIGWAITINFO && HAVE_SIGTIMEDWAIT
HYSS_FUNCTION(pcntl_sigwaitinfo);
HYSS_FUNCTION(pcntl_sigtimedwait);
# endif
#endif
HYSS_FUNCTION(pcntl_exec);
#ifdef HAVE_GETPRIORITY
HYSS_FUNCTION(pcntl_getpriority);
#endif
#ifdef HAVE_SETPRIORITY
HYSS_FUNCTION(pcntl_setpriority);
#endif
HYSS_FUNCTION(pcntl_async_signals);

struct hyss_pcntl_pending_signal {
	struct hyss_pcntl_pending_signal *next;
	gear_long signo;
#ifdef HAVE_STRUCT_SIGINFO_T
	siginfo_t siginfo;
#endif
};

GEAR_BEGIN_CAPI_GLOBALS(pcntl)
	HashTable hyss_signal_table;
	int processing_signal_queue;
	struct hyss_pcntl_pending_signal *head, *tail, *spares;
	int last_error;
	volatile char pending_signals;
	gear_bool async_signals;
GEAR_END_CAPI_GLOBALS(pcntl)

#ifdef ZTS
#define PCNTL_G(v) PBCG(pcntl_globals_id, gear_pcntl_globals *, v)
#else
#define PCNTL_G(v)	(pcntl_globals.v)
#endif

#define REGISTER_PCNTL_ERRNO_CONSTANT(name) REGISTER_LONG_CONSTANT("PCNTL_" #name, name, CONST_CS | CONST_PERSISTENT)

#endif	/* HYSS_PCNTL_H */

