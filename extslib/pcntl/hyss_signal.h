/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <signal.h>
#ifndef HYSS_SIGNAL_H
#define HYSS_SIGNAL_H

#ifdef HAVE_STRUCT_SIGINFO_T
typedef void Sigfunc(int, siginfo_t*, void*);
#else
typedef void Sigfunc(int);
#endif
Sigfunc *hyss_signal(int signo, Sigfunc *func, int restart);
Sigfunc *hyss_signal4(int signo, Sigfunc *func, int restart, int mask_all);

#endif
