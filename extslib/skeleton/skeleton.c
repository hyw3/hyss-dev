%HEADER%

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "hyss.h"
#include "extslib/standard/info.h"
#include "hyss_%EXTNAME%.h"

/* For compatibility with older HYSS versions */
#ifndef GEAR_PARSE_PARAMETERS_NONE
#define GEAR_PARSE_PARAMETERS_NONE() \
	GEAR_PARSE_PARAMETERS_START(0, 0) \
	GEAR_PARSE_PARAMETERS_END()
#endif

/* {{{ void %EXTNAME%_test1()
 */
HYSS_FUNCTION(%EXTNAME%_test1)
{
	GEAR_PARSE_PARAMETERS_NONE();

	hyss_printf("The extension %s is loaded and working!\r\n", "%EXTNAME%");
}
/* }}} */

/* {{{ string %EXTNAME%_test2( [ string $var ] )
 */
HYSS_FUNCTION(%EXTNAME%_test2)
{
	char *var = "World";
	size_t var_len = sizeof("World") - 1;
	gear_string *retval;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(var, var_len)
	GEAR_PARSE_PARAMETERS_END();

	retval = strpprintf(0, "Hello %s", var);

	RETURN_STR(retval);
}
/* }}}*/

/* {{{ HYSS_RINIT_FUNCTION
 */
HYSS_RINIT_FUNCTION(%EXTNAME%)
{
#if defined(ZTS) && defined(COMPILE_DL_%EXTNAMECAPS%)
	GEAR_PBCLS_CACHE_UPDATE();
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(%EXTNAME%)
{
	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "%EXTNAME% support", "enabled");
	hyss_info_print_table_end();
}
/* }}} */

/* {{{ arginfo
 */
GEAR_BEGIN_ARG_INFO(arginfo_%EXTNAME%_test1, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_%EXTNAME%_test2, 0)
	GEAR_ARG_INFO(0, str)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ %EXTNAME%_functions[]
 */
static const gear_function_entry %EXTNAME%_functions[] = {
	HYSS_FE(%EXTNAME%_test1,		arginfo_%EXTNAME%_test1)
	HYSS_FE(%EXTNAME%_test2,		arginfo_%EXTNAME%_test2)
	HYSS_FE_END
};
/* }}} */

/* {{{ %EXTNAME%_capi_entry
 */
gear_capi_entry %EXTNAME%_capi_entry = {
	STANDARD_CAPI_HEADER,
	"%EXTNAME%",					/* Extension name */
	%EXTNAME%_functions,			/* gear_function_entry */
	NULL,							/* HYSS_MINIT - cAPI initialization */
	NULL,							/* HYSS_MSHUTDOWN - cAPI shutdown */
	HYSS_RINIT(%EXTNAME%),			/* HYSS_RINIT - Request initialization */
	NULL,							/* HYSS_RSHUTDOWN - Request shutdown */
	HYSS_MINFO(%EXTNAME%),			/* HYSS_MINFO - cAPI info */
	HYSS_%EXTNAMECAPS%_VERSION,		/* Version */
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_%EXTNAMECAPS%
# ifdef ZTS
GEAR_PBCLS_CACHE_DEFINE()
# endif
GEAR_GET_CAPI(%EXTNAME%)
#endif
%FOOTER%
