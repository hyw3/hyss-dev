%HEADER%

#ifndef HYSS_%EXTNAMECAPS%_H
# define HYSS_%EXTNAMECAPS%_H

extern gear_capi_entry %EXTNAME%_capi_entry;
# define hyssext_%EXTNAME%_ptr &%EXTNAME%_capi_entry

# define HYSS_%EXTNAMECAPS%_VERSION "0.1.0"

# if defined(ZTS) && defined(COMPILE_DL_%EXTNAMECAPS%)
GEAR_PBCLS_CACHE_EXTERN()
# endif

#endif	/* HYSS_%EXTNAMECAPS%_H */
%FOOTER%
