/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_ICONV_H
#define HYSS_ICONV_H

#ifdef HYSS_WIN32
# ifdef HYSS_ICONV_EXPORTS
#  define HYSS_ICONV_API __declspec(dllexport)
# else
#  define HYSS_ICONV_API __declspec(dllimport)
# endif
#elif defined(__GNUC__) && __GNUC__ >= 4
# define HYSS_ICONV_API __attribute__ ((visibility("default")))
#else
# define HYSS_ICONV_API
#endif

#include "hyss_version.h"
#define HYSS_ICONV_VERSION HYSS_VERSION

#ifdef HYSS_ATOM_INC
#include "extslib/iconv/hyss_have_iconv.h"
#include "extslib/iconv/hyss_have_libiconv.h"
#include "extslib/iconv/hyss_iconv_aliased_libiconv.h"
#include "extslib/iconv/hyss_have_glibc_iconv.h"
#include "extslib/iconv/hyss_have_bsd_iconv.h"
#include "extslib/iconv/hyss_have_ibm_iconv.h"
#include "extslib/iconv/hyss_iconv_supports_errno.h"
#include "extslib/iconv/hyss_hyss_iconv_impl.h"
#include "extslib/iconv/hyss_hyss_iconv_h_path.h"
#endif

#ifdef HAVE_ICONV
extern gear_capi_entry iconv_capi_entry;
#define iconv_capi_ptr &iconv_capi_entry

HYSS_MINIT_FUNCTION(miconv);
HYSS_MSHUTDOWN_FUNCTION(miconv);
HYSS_MINFO_FUNCTION(miconv);

HYSS_NAMED_FUNCTION(hyss_if_iconv);
HYSS_FUNCTION(ob_iconv_handler);
HYSS_FUNCTION(iconv_get_encoding);
HYSS_FUNCTION(iconv_set_encoding);
HYSS_FUNCTION(iconv_strlen);
HYSS_FUNCTION(iconv_substr);
HYSS_FUNCTION(iconv_strpos);
HYSS_FUNCTION(iconv_strrpos);
HYSS_FUNCTION(iconv_mime_encode);
HYSS_FUNCTION(iconv_mime_decode);
HYSS_FUNCTION(iconv_mime_decode_headers);

GEAR_BEGIN_CAPI_GLOBALS(iconv)
	char *input_encoding;
	char *internal_encoding;
	char *output_encoding;
GEAR_END_CAPI_GLOBALS(iconv)

#define ICONVG(v) GEAR_CAPI_GLOBALS_ACCESSOR(iconv, v)

#if defined(ZTS) && defined(COMPILE_DL_ICONV)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#ifdef HAVE_IBM_ICONV
# define ICONV_ASCII_ENCODING "IBM-850"
# define ICONV_UCS4_ENCODING "UCS-4"
#else
# define ICONV_ASCII_ENCODING "ASCII"
# define ICONV_UCS4_ENCODING "UCS-4LE"
#endif

#ifndef ICONV_CSNMAXLEN
#define ICONV_CSNMAXLEN 64
#endif

/* {{{ typedef enum hyss_iconv_err_t */
typedef enum _hyss_iconv_err_t {
	HYSS_ICONV_ERR_SUCCESS           = SUCCESS,
	HYSS_ICONV_ERR_CONVERTER         = 1,
	HYSS_ICONV_ERR_WRONG_CHARSET     = 2,
	HYSS_ICONV_ERR_TOO_BIG           = 3,
	HYSS_ICONV_ERR_ILLEGAL_SEQ       = 4,
	HYSS_ICONV_ERR_ILLEGAL_CHAR      = 5,
	HYSS_ICONV_ERR_UNKNOWN           = 6,
	HYSS_ICONV_ERR_MALFORMED         = 7,
	HYSS_ICONV_ERR_ALLOC             = 8
} hyss_iconv_err_t;
/* }}} */

HYSS_ICONV_API hyss_iconv_err_t hyss_iconv_string(const char * in_p, size_t in_len, gear_string **out, const char *out_charset, const char *in_charset);

#else

#define iconv_capi_ptr NULL

#endif /* HAVE_ICONV */

#define hyssext_iconv_ptr iconv_capi_ptr

#endif	/* HYSS_ICONV_H */

