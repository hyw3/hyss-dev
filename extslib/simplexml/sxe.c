/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "gear_interfaces.h"

#include "hyss_simplexml.h"
#include "extslib/spl/hyss_spl.h"
#include "extslib/spl/spl_iterators.h"
#include "sxe.h"

HYSS_SXE_API gear_class_entry *ce_SimpleXMLIterator = NULL;
HYSS_SXE_API gear_class_entry *ce_SimpleXMLElement;

#include "hyss_simplexml_exports.h"

/* {{{ proto void SimpleXMLIterator::rewind()
 Rewind to first element */
HYSS_METHOD(ce_SimpleXMLIterator, rewind)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	hyss_sxe_rewind_iterator(Z_SXEOBJ_P(getThis()));
}
/* }}} */

/* {{{ proto bool SimpleXMLIterator::valid()
 Check whether iteration is valid */
HYSS_METHOD(ce_SimpleXMLIterator, valid)
{
	hyss_sxe_object *sxe = Z_SXEOBJ_P(getThis());

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_BOOL(!Z_ISUNDEF(sxe->iter.data));
}
/* }}} */

/* {{{ proto SimpleXMLIterator SimpleXMLIterator::current()
 Get current element */
HYSS_METHOD(ce_SimpleXMLIterator, current)
{
	hyss_sxe_object *sxe = Z_SXEOBJ_P(getThis());
	zval *data;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (Z_ISUNDEF(sxe->iter.data)) {
		return; /* return NULL */
	}

	data = &sxe->iter.data;
	ZVAL_COPY_DEREF(return_value, data);
}
/* }}} */

/* {{{ proto string SimpleXMLIterator::key()
 Get name of current child element */
HYSS_METHOD(ce_SimpleXMLIterator, key)
{
	xmlNodePtr curnode;
	hyss_sxe_object *intern;
	hyss_sxe_object *sxe = Z_SXEOBJ_P(getThis());

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (Z_ISUNDEF(sxe->iter.data)) {
		RETURN_FALSE;
	}

	intern = Z_SXEOBJ_P(&sxe->iter.data);
	if (intern != NULL && intern->node != NULL) {
		curnode = (xmlNodePtr)((hyss_libxml_node_ptr *)intern->node)->node;
		RETURN_STRINGL((char*)curnode->name, xmlStrlen(curnode->name));
	}

	RETURN_FALSE;
}
/* }}} */

/* {{{ proto void SimpleXMLIterator::next()
 Move to next element */
HYSS_METHOD(ce_SimpleXMLIterator, next)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	hyss_sxe_move_forward_iterator(Z_SXEOBJ_P(getThis()));
}
/* }}} */

/* {{{ proto bool SimpleXMLIterator::hasChildren()
 Check whether element has children (elements) */
HYSS_METHOD(ce_SimpleXMLIterator, hasChildren)
{
	hyss_sxe_object *sxe = Z_SXEOBJ_P(getThis());
	hyss_sxe_object *child;
	xmlNodePtr      node;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (Z_ISUNDEF(sxe->iter.data) || sxe->iter.type == SXE_ITER_ATTRLIST) {
		RETURN_FALSE;
	}
	child = Z_SXEOBJ_P(&sxe->iter.data);

	GET_NODE(child, node);
	if (node) {
		node = node->children;
	}
	while (node && node->type != XML_ELEMENT_NODE) {
		node = node->next;
	}
	RETURN_BOOL(node ? 1 : 0);
}
/* }}} */

/* {{{ proto SimpleXMLIterator SimpleXMLIterator::getChildren()
 Get child element iterator */
HYSS_METHOD(ce_SimpleXMLIterator, getChildren)
{
	hyss_sxe_object *sxe = Z_SXEOBJ_P(getThis());
	zval *data;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	if (Z_ISUNDEF(sxe->iter.data) || sxe->iter.type == SXE_ITER_ATTRLIST) {
		return; /* return NULL */
	}

	data = &sxe->iter.data;
	ZVAL_COPY_DEREF(return_value, data);
}

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO(arginfo_simplexmliterator__void, 0)
GEAR_END_ARG_INFO()
/* }}} */

static const gear_function_entry funcs_SimpleXMLIterator[] = {
	HYSS_ME(ce_SimpleXMLIterator, rewind,                 arginfo_simplexmliterator__void, GEAR_ACC_PUBLIC)
	HYSS_ME(ce_SimpleXMLIterator, valid,                  arginfo_simplexmliterator__void, GEAR_ACC_PUBLIC)
	HYSS_ME(ce_SimpleXMLIterator, current,                arginfo_simplexmliterator__void, GEAR_ACC_PUBLIC)
	HYSS_ME(ce_SimpleXMLIterator, key,                    arginfo_simplexmliterator__void, GEAR_ACC_PUBLIC)
	HYSS_ME(ce_SimpleXMLIterator, next,                   arginfo_simplexmliterator__void, GEAR_ACC_PUBLIC)
	HYSS_ME(ce_SimpleXMLIterator, hasChildren,            arginfo_simplexmliterator__void, GEAR_ACC_PUBLIC)
	HYSS_ME(ce_SimpleXMLIterator, getChildren,            arginfo_simplexmliterator__void, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};
/* }}} */

HYSS_MINIT_FUNCTION(sxe) /* {{{ */
{
	gear_class_entry *pce;
	gear_class_entry sxi;

	if ((pce = gear_hash_str_find_ptr(CG(class_table), "simplexmlelement", sizeof("SimpleXMLElement") - 1)) == NULL) {
		ce_SimpleXMLElement  = NULL;
		ce_SimpleXMLIterator = NULL;
		return SUCCESS; /* SimpleXML must be initialized before */
	}

	ce_SimpleXMLElement = pce;

	INIT_CLASS_ENTRY_EX(sxi, "SimpleXMLIterator", sizeof("SimpleXMLIterator") - 1, funcs_SimpleXMLIterator);
	ce_SimpleXMLIterator = gear_register_internal_class_ex(&sxi, ce_SimpleXMLElement);
	ce_SimpleXMLIterator->create_object = ce_SimpleXMLElement->create_object;

	gear_class_implements(ce_SimpleXMLIterator, 1, spl_ce_RecursiveIterator);
	gear_class_implements(ce_SimpleXMLIterator, 1, gear_ce_countable);

	return SUCCESS;
}
/* }}} */

