/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SIMPLEXML_EXPORTS_H
#define HYSS_SIMPLEXML_EXPORTS_H

#include "hyss_simplexml.h"

#define SKIP_TEXT(__p) \
	if ((__p)->type == XML_TEXT_NODE) { \
		goto next_iter; \
	}

#define GET_NODE(__s, __n) { \
	if ((__s)->node && (__s)->node->node) { \
		__n = (__s)->node->node; \
	} else { \
		__n = NULL; \
		hyss_error_docref(NULL, E_WARNING, "Node no longer exists"); \
	} \
}

HYSS_SXE_API gear_object *sxe_object_new(gear_class_entry *ce);

static inline hyss_sxe_object *hyss_sxe_fetch_object(gear_object *obj) /* {{{ */ {
	return (hyss_sxe_object *)((char*)(obj) - XtOffsetOf(hyss_sxe_object, zo));
}
/* }}} */

#define Z_SXEOBJ_P(zv) hyss_sxe_fetch_object(Z_OBJ_P((zv)))

typedef struct {
	gear_object_iterator  intern;
	hyss_sxe_object        *sxe;
} hyss_sxe_iterator;

HYSS_SXE_API void hyss_sxe_rewind_iterator(hyss_sxe_object *sxe);
HYSS_SXE_API void hyss_sxe_move_forward_iterator(hyss_sxe_object *sxe);

#endif /* HYSS_SIMPLEXML_EXPORTS_H */

