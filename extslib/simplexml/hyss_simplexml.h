/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SIMPLEXML_H
#define HYSS_SIMPLEXML_H

extern gear_capi_entry simplexml_capi_entry;
#define hyssext_simplexml_ptr &simplexml_capi_entry

#include "hyss_version.h"
#define HYSS_SIMPLEXML_VERSION HYSS_VERSION

#ifdef ZTS
#include "hypbc.h"
#endif

#include "extslib/libxml/hyss_libxml.h"
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/tree.h>
#include <libxml/uri.h>
#include <libxml/xmlerror.h>
#include <libxml/xinclude.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/xpointer.h>
#include <libxml/xmlschemas.h>

HYSS_MINIT_FUNCTION(simplexml);
HYSS_MSHUTDOWN_FUNCTION(simplexml);
HYSS_MINFO_FUNCTION(simplexml);

typedef enum {
	SXE_ITER_NONE     = 0,
	SXE_ITER_ELEMENT  = 1,
	SXE_ITER_CHILD    = 2,
	SXE_ITER_ATTRLIST = 3
} SXE_ITER;

typedef struct {
	hyss_libxml_node_ptr *node;
	hyss_libxml_ref_obj *document;
	HashTable *properties;
	xmlXPathContextPtr xpath;
	struct {
		xmlChar               *name;
		xmlChar               *nsprefix;
		int                   isprefix;
		SXE_ITER              type;
		zval                  data;
	} iter;
	zval tmp;
	gear_function *fptr_count;
	gear_object zo;
} hyss_sxe_object;

#ifdef ZTS
#define SIMPLEXML_G(v) PBCG(simplexml_globals_id, gear_simplexml_globals *, v)
#else
#define SIMPLEXML_G(v) (simplexml_globals.v)
#endif

#ifdef HYSS_WIN32
#	ifdef HYSS_SIMPLEXML_EXPORTS
#		define HYSS_SXE_API __declspec(dllexport)
#	else
#		define HYSS_SXE_API __declspec(dllimport)
#	endif
#else
#	define HYSS_SXE_API GEAR_API
#endif

HYSS_SXE_API gear_class_entry *sxe_get_element_class_entry();

#endif

