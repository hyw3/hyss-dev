dnl config.m4 for extension simplexml

HYSS_ARG_ENABLE(simplexml, whether to enable SimpleXML support,
[  --disable-simplexml     Disable SimpleXML support], yes)

if test -z "$HYSS_LIBXML_DIR"; then
  HYSS_ARG_WITH(libxml-dir, libxml2 install dir,
  [  --with-libxml-dir=DIR   SimpleXML: libxml2 install prefix], no, no)
fi

if test "$HYSS_SIMPLEXML" != "no"; then

  if test "$HYSS_LIBXML" = "no"; then
    AC_MSG_ERROR([SimpleXML extension requires LIBXML extension, add --enable-libxml])
  fi

  HYSS_SETUP_LIBXML(SIMPLEXML_SHARED_LIBADD, [
    AC_DEFINE(HAVE_SIMPLEXML,1,[ ])
    HYSS_NEW_EXTENSION(simplexml, simplexml.c sxe.c, $ext_shared)
    HYSS_INSTALL_HEADERS([extslib/simplexml/hyss_simplexml.h extslib/simplexml/hyss_simplexml_exports.h])
    HYSS_SUBST(SIMPLEXML_SHARED_LIBADD)
  ], [
    AC_MSG_ERROR([libxml2 not found. Please check your libxml2 installation.])
  ])
  HYSS_ADD_EXTENSION_DEP(simplexml, libxml)
  HYSS_ADD_EXTENSION_DEP(simplexml, spl, true)
fi
