/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_zlib.h"

/* {{{ data structure */

/* Passed as opaque in malloc callbacks */
typedef struct _hyss_zlib_filter_data {
	z_stream strm;
	unsigned char *inbuf;
	size_t inbuf_len;
	unsigned char *outbuf;
	size_t outbuf_len;
	int persistent;
	gear_bool finished;
} hyss_zlib_filter_data;

/* }}} */

/* {{{ Memory management wrappers */

static voidpf hyss_zlib_alloc(voidpf opaque, uInt items, uInt size)
{
	return (voidpf)safe_pemalloc(items, size, 0, ((hyss_zlib_filter_data*)opaque)->persistent);
}

static void hyss_zlib_free(voidpf opaque, voidpf address)
{
	pefree((void*)address, ((hyss_zlib_filter_data*)opaque)->persistent);
}
/* }}} */

/* {{{ zlib.inflate filter implementation */

static hyss_stream_filter_status_t hyss_zlib_inflate_filter(
	hyss_stream *stream,
	hyss_stream_filter *thisfilter,
	hyss_stream_bucket_brigade *buckets_in,
	hyss_stream_bucket_brigade *buckets_out,
	size_t *bytes_consumed,
	int flags
	)
{
	hyss_zlib_filter_data *data;
	hyss_stream_bucket *bucket;
	size_t consumed = 0;
	int status;
	hyss_stream_filter_status_t exit_status = PSFS_FEED_ME;

	if (!thisfilter || !Z_PTR(thisfilter->abstract)) {
		/* Should never happen */
		return PSFS_ERR_FATAL;
	}

	data = (hyss_zlib_filter_data *)(Z_PTR(thisfilter->abstract));

	while (buckets_in->head) {
		size_t bin = 0, desired;

		bucket = hyss_stream_bucket_make_writeable(buckets_in->head);

		while (bin < (unsigned int) bucket->buflen && !data->finished) {

			desired = bucket->buflen - bin;
			if (desired > data->inbuf_len) {
				desired = data->inbuf_len;
			}
			memcpy(data->strm.next_in, bucket->buf + bin, desired);
			data->strm.avail_in = desired;

			status = inflate(&(data->strm), flags & PSFS_FLAG_FLUSH_CLOSE ? Z_FINISH : Z_SYNC_FLUSH);
			if (status == Z_STREAM_END) {
				inflateEnd(&(data->strm));
				data->finished = '\1';
				exit_status = PSFS_PASS_ON;
			} else if (status != Z_OK) {
				/* Something bad happened */
				hyss_stream_bucket_delref(bucket);
				/* reset these because despite the error the filter may be used again */
				data->strm.next_in = data->inbuf;
				data->strm.avail_in = 0;
				return PSFS_ERR_FATAL;
			}
			desired -= data->strm.avail_in; /* desired becomes what we consumed this round through */
			data->strm.next_in = data->inbuf;
			data->strm.avail_in = 0;
			bin += desired;

			if (data->strm.avail_out < data->outbuf_len) {
				hyss_stream_bucket *out_bucket;
				size_t bucketlen = data->outbuf_len - data->strm.avail_out;
				out_bucket = hyss_stream_bucket_new(
					stream, estrndup((char *) data->outbuf, bucketlen), bucketlen, 1, 0);
				hyss_stream_bucket_append(buckets_out, out_bucket);
				data->strm.avail_out = data->outbuf_len;
				data->strm.next_out = data->outbuf;
				exit_status = PSFS_PASS_ON;
			}

		}
		consumed += bucket->buflen;
		hyss_stream_bucket_delref(bucket);
	}

	if (!data->finished && flags & PSFS_FLAG_FLUSH_CLOSE) {
		/* Spit it out! */
		status = Z_OK;
		while (status == Z_OK) {
			status = inflate(&(data->strm), Z_FINISH);
			if (data->strm.avail_out < data->outbuf_len) {
				size_t bucketlen = data->outbuf_len - data->strm.avail_out;

				bucket = hyss_stream_bucket_new(
					stream, estrndup((char *) data->outbuf, bucketlen), bucketlen, 1, 0);
				hyss_stream_bucket_append(buckets_out, bucket);
				data->strm.avail_out = data->outbuf_len;
				data->strm.next_out = data->outbuf;
				exit_status = PSFS_PASS_ON;
			}
		}
	}

	if (bytes_consumed) {
		*bytes_consumed = consumed;
	}

	return exit_status;
}

static void hyss_zlib_inflate_dtor(hyss_stream_filter *thisfilter)
{
	if (thisfilter && Z_PTR(thisfilter->abstract)) {
		hyss_zlib_filter_data *data = Z_PTR(thisfilter->abstract);
		if (!data->finished) {
			inflateEnd(&(data->strm));
		}
		pefree(data->inbuf, data->persistent);
		pefree(data->outbuf, data->persistent);
		pefree(data, data->persistent);
	}
}

static const hyss_stream_filter_ops hyss_zlib_inflate_ops = {
	hyss_zlib_inflate_filter,
	hyss_zlib_inflate_dtor,
	"zlib.inflate"
};
/* }}} */

/* {{{ zlib.deflate filter implementation */

static hyss_stream_filter_status_t hyss_zlib_deflate_filter(
	hyss_stream *stream,
	hyss_stream_filter *thisfilter,
	hyss_stream_bucket_brigade *buckets_in,
	hyss_stream_bucket_brigade *buckets_out,
	size_t *bytes_consumed,
	int flags
	)
{
	hyss_zlib_filter_data *data;
	hyss_stream_bucket *bucket;
	size_t consumed = 0;
	int status;
	hyss_stream_filter_status_t exit_status = PSFS_FEED_ME;

	if (!thisfilter || !Z_PTR(thisfilter->abstract)) {
		/* Should never happen */
		return PSFS_ERR_FATAL;
	}

	data = (hyss_zlib_filter_data *)(Z_PTR(thisfilter->abstract));

	while (buckets_in->head) {
		size_t bin = 0, desired;

		bucket = buckets_in->head;

		bucket = hyss_stream_bucket_make_writeable(bucket);

		while (bin < (unsigned int) bucket->buflen) {
			desired = bucket->buflen - bin;
			if (desired > data->inbuf_len) {
				desired = data->inbuf_len;
			}
			memcpy(data->strm.next_in, bucket->buf + bin, desired);
			data->strm.avail_in = desired;

			status = deflate(&(data->strm), flags & PSFS_FLAG_FLUSH_CLOSE ? Z_FULL_FLUSH : (flags & PSFS_FLAG_FLUSH_INC ? Z_SYNC_FLUSH : Z_NO_FLUSH));
			if (status != Z_OK) {
				/* Something bad happened */
				hyss_stream_bucket_delref(bucket);
				return PSFS_ERR_FATAL;
			}
			desired -= data->strm.avail_in; /* desired becomes what we consumed this round through */
			data->strm.next_in = data->inbuf;
			data->strm.avail_in = 0;
			bin += desired;

			if (data->strm.avail_out < data->outbuf_len) {
				hyss_stream_bucket *out_bucket;
				size_t bucketlen = data->outbuf_len - data->strm.avail_out;

				out_bucket = hyss_stream_bucket_new(
					stream, estrndup((char *) data->outbuf, bucketlen), bucketlen, 1, 0);
				hyss_stream_bucket_append(buckets_out, out_bucket);
				data->strm.avail_out = data->outbuf_len;
				data->strm.next_out = data->outbuf;
				exit_status = PSFS_PASS_ON;
			}
		}
		consumed += bucket->buflen;
		hyss_stream_bucket_delref(bucket);
	}

	if (flags & PSFS_FLAG_FLUSH_CLOSE) {
		/* Spit it out! */
		status = Z_OK;
		while (status == Z_OK) {
			status = deflate(&(data->strm), Z_FINISH);
			if (data->strm.avail_out < data->outbuf_len) {
				size_t bucketlen = data->outbuf_len - data->strm.avail_out;

				bucket = hyss_stream_bucket_new(
					stream, estrndup((char *) data->outbuf, bucketlen), bucketlen, 1, 0);
				hyss_stream_bucket_append(buckets_out, bucket);
				data->strm.avail_out = data->outbuf_len;
				data->strm.next_out = data->outbuf;
				exit_status = PSFS_PASS_ON;
			}
		}
	}

	if (bytes_consumed) {
		*bytes_consumed = consumed;
	}

	return exit_status;
}

static void hyss_zlib_deflate_dtor(hyss_stream_filter *thisfilter)
{
	if (thisfilter && Z_PTR(thisfilter->abstract)) {
		hyss_zlib_filter_data *data = Z_PTR(thisfilter->abstract);
		deflateEnd(&(data->strm));
		pefree(data->inbuf, data->persistent);
		pefree(data->outbuf, data->persistent);
		pefree(data, data->persistent);
	}
}

static const hyss_stream_filter_ops hyss_zlib_deflate_ops = {
	hyss_zlib_deflate_filter,
	hyss_zlib_deflate_dtor,
	"zlib.deflate"
};

/* }}} */

/* {{{ zlib.* common factory */

static hyss_stream_filter *hyss_zlib_filter_create(const char *filtername, zval *filterparams, uint8_t persistent)
{
	const hyss_stream_filter_ops *fops = NULL;
	hyss_zlib_filter_data *data;
	int status;

	/* Create this filter */
	data = pecalloc(1, sizeof(hyss_zlib_filter_data), persistent);
	if (!data) {
		hyss_error_docref(NULL, E_WARNING, "Failed allocating %zd bytes", sizeof(hyss_zlib_filter_data));
		return NULL;
	}

	/* Circular reference */
	data->strm.opaque = (voidpf) data;

	data->strm.zalloc = (alloc_func) hyss_zlib_alloc;
	data->strm.zfree = (free_func) hyss_zlib_free;
	data->strm.avail_out = data->outbuf_len = data->inbuf_len = 0x8000;
	data->strm.next_in = data->inbuf = (Bytef *) pemalloc(data->inbuf_len, persistent);
	if (!data->inbuf) {
		hyss_error_docref(NULL, E_WARNING, "Failed allocating %zd bytes", data->inbuf_len);
		pefree(data, persistent);
		return NULL;
	}
	data->strm.avail_in = 0;
	data->strm.next_out = data->outbuf = (Bytef *) pemalloc(data->outbuf_len, persistent);
	if (!data->outbuf) {
		hyss_error_docref(NULL, E_WARNING, "Failed allocating %zd bytes", data->outbuf_len);
		pefree(data->inbuf, persistent);
		pefree(data, persistent);
		return NULL;
	}

	data->strm.data_type = Z_ASCII;

	if (strcasecmp(filtername, "zlib.inflate") == 0) {
		int windowBits = -MAX_WBITS;

		if (filterparams) {
			zval *tmpzval;

			if ((Z_TYPE_P(filterparams) == IS_ARRAY || Z_TYPE_P(filterparams) == IS_OBJECT) &&
				(tmpzval = gear_hash_str_find(HASH_OF(filterparams), "window", sizeof("window") - 1))) {
				/* log-2 base of history window (9 - 15) */
				gear_long tmp = zval_get_long(tmpzval);
				if (tmp < -MAX_WBITS || tmp > MAX_WBITS + 32) {
					hyss_error_docref(NULL, E_WARNING, "Invalid parameter give for window size. (" GEAR_LONG_FMT ")", tmp);
				} else {
					windowBits = tmp;
				}
			}
		}

		/* RFC 1951 Inflate */
		data->finished = '\0';
		status = inflateInit2(&(data->strm), windowBits);
		fops = &hyss_zlib_inflate_ops;
	} else if (strcasecmp(filtername, "zlib.deflate") == 0) {
		/* RFC 1951 Deflate */
		int level = Z_DEFAULT_COMPRESSION;
		int windowBits = -MAX_WBITS;
		int memLevel = MAX_MEM_LEVEL;


		if (filterparams) {
			zval *tmpzval;
			gear_long tmp;

			/* filterparams can either be a scalar value to indicate compression level (shortcut method)
               Or can be a hash containing one or more of 'window', 'memory', and/or 'level' members. */

			switch (Z_TYPE_P(filterparams)) {
				case IS_ARRAY:
				case IS_OBJECT:
					if ((tmpzval = gear_hash_str_find(HASH_OF(filterparams), "memory", sizeof("memory") -1))) {
						/* Memory Level (1 - 9) */
						tmp = zval_get_long(tmpzval);
						if (tmp < 1 || tmp > MAX_MEM_LEVEL) {
							hyss_error_docref(NULL, E_WARNING, "Invalid parameter give for memory level. (" GEAR_LONG_FMT ")", tmp);
						} else {
							memLevel = tmp;
						}
					}

					if ((tmpzval = gear_hash_str_find(HASH_OF(filterparams), "window", sizeof("window") - 1))) {
						/* log-2 base of history window (9 - 15) */
						tmp = zval_get_long(tmpzval);
						if (tmp < -MAX_WBITS || tmp > MAX_WBITS + 16) {
							hyss_error_docref(NULL, E_WARNING, "Invalid parameter give for window size. (" GEAR_LONG_FMT ")", tmp);
						} else {
							windowBits = tmp;
						}
					}

					if ((tmpzval = gear_hash_str_find(HASH_OF(filterparams), "level", sizeof("level") - 1))) {
						tmp = zval_get_long(tmpzval);

						/* Pseudo pass through to catch level validating code */
						goto factory_setlevel;
					}
					break;
				case IS_STRING:
				case IS_DOUBLE:
				case IS_LONG:
					tmp = zval_get_long(filterparams);
factory_setlevel:
					/* Set compression level within reason (-1 == default, 0 == none, 1-9 == least to most compression */
					if (tmp < -1 || tmp > 9) {
						hyss_error_docref(NULL, E_WARNING, "Invalid compression level specified. (" GEAR_LONG_FMT ")", tmp);
					} else {
						level = tmp;
					}
					break;
				default:
					hyss_error_docref(NULL, E_WARNING, "Invalid filter parameter, ignored");
			}
		}
		status = deflateInit2(&(data->strm), level, Z_DEFLATED, windowBits, memLevel, 0);
		fops = &hyss_zlib_deflate_ops;
	} else {
		status = Z_DATA_ERROR;
	}

	if (status != Z_OK) {
		/* Unspecified (probably strm) error, let stream-filter error do its own whining */
		pefree(data->strm.next_in, persistent);
		pefree(data->strm.next_out, persistent);
		pefree(data, persistent);
		return NULL;
	}

	return hyss_stream_filter_alloc(fops, data, persistent);
}

const hyss_stream_filter_factory hyss_zlib_filter_factory = {
	hyss_zlib_filter_create
};
/* }}} */

