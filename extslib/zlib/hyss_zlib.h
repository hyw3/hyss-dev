/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_ZLIB_H
#define HYSS_ZLIB_H

#include "hyss_version.h"
#define HYSS_ZLIB_VERSION HYSS_VERSION

#include <zlib.h>

#define HYSS_ZLIB_ENCODING_RAW		-0xf
#define HYSS_ZLIB_ENCODING_GZIP		0x1f
#define HYSS_ZLIB_ENCODING_DEFLATE	0x0f

#define HYSS_ZLIB_ENCODING_ANY		0x2f

#define HYSS_ZLIB_OUTPUT_HANDLER_NAME "zlib output compression"
#define HYSS_ZLIB_BUFFER_SIZE_GUESS(in_len) (((size_t) ((double) in_len * (double) 1.015)) + 10 + 8 + 4 + 1)

typedef struct _hyss_zlib_buffer {
	char *data;
	char *aptr;
	size_t used;
	size_t free;
	size_t size;
} hyss_zlib_buffer;

typedef struct _hyss_zlib_context {
	z_stream Z;
	char *inflateDict;
	int status;
	size_t inflateDictlen;
	hyss_zlib_buffer buffer;
} hyss_zlib_context;

GEAR_BEGIN_CAPI_GLOBALS(zlib)
	/* variables for transparent gzip encoding */
	gear_long output_compression;
	gear_long output_compression_level;
	char *output_handler;
	hyss_zlib_context *ob_gzhandler;
	gear_long output_compression_default;
	gear_bool handler_registered;
	int compression_coding;
GEAR_END_CAPI_GLOBALS(zlib);

#define ZLIBG(v) GEAR_CAPI_GLOBALS_ACCESSOR(zlib, v)

hyss_stream *hyss_stream_gzopen(hyss_stream_wrapper *wrapper, const char *path, const char *mode, int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC);
extern const hyss_stream_ops hyss_stream_gzio_ops;
extern const hyss_stream_wrapper hyss_stream_gzip_wrapper;
extern const hyss_stream_filter_factory hyss_zlib_filter_factory;
extern gear_capi_entry hyss_zlib_capi_entry;
#define zlib_capi_ptr &hyss_zlib_capi_entry
#define hyssext_zlib_ptr zlib_capi_ptr

#endif /* HYSS_ZLIB_H */

