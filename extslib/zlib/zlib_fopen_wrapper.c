/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include "hyss.h"
#include "hyss_zlib.h"
#include "fopen_wrappers.h"

#include "main/hyss_network.h"

struct hyss_gz_stream_data_t	{
	gzFile gz_file;
	hyss_stream *stream;
};

static size_t hyss_gziop_read(hyss_stream *stream, char *buf, size_t count)
{
	struct hyss_gz_stream_data_t *self = (struct hyss_gz_stream_data_t *) stream->abstract;
	int read;

	/* XXX this needs to be looped for the case count > UINT_MAX */
	read = gzread(self->gz_file, buf, count);

	if (gzeof(self->gz_file)) {
		stream->eof = 1;
	}

	return (size_t)((read < 0) ? 0 : read);
}

static size_t hyss_gziop_write(hyss_stream *stream, const char *buf, size_t count)
{
	struct hyss_gz_stream_data_t *self = (struct hyss_gz_stream_data_t *) stream->abstract;
	int wrote;

	/* XXX this needs to be looped for the case count > UINT_MAX */
	wrote = gzwrite(self->gz_file, (char *) buf, count);

	return (size_t)((wrote < 0) ? 0 : wrote);
}

static int hyss_gziop_seek(hyss_stream *stream, gear_off_t offset, int whence, gear_off_t *newoffs)
{
	struct hyss_gz_stream_data_t *self = (struct hyss_gz_stream_data_t *) stream->abstract;

	assert(self != NULL);

	if (whence == SEEK_END) {
		hyss_error_docref(NULL, E_WARNING, "SEEK_END is not supported");
		return -1;
	}
	*newoffs = gzseek(self->gz_file, offset, whence);

	return (*newoffs < 0) ? -1 : 0;
}

static int hyss_gziop_close(hyss_stream *stream, int close_handle)
{
	struct hyss_gz_stream_data_t *self = (struct hyss_gz_stream_data_t *) stream->abstract;
	int ret = EOF;

	if (close_handle) {
		if (self->gz_file) {
			ret = gzclose(self->gz_file);
			self->gz_file = NULL;
		}
		if (self->stream) {
			hyss_stream_close(self->stream);
			self->stream = NULL;
		}
	}
	efree(self);

	return ret;
}

static int hyss_gziop_flush(hyss_stream *stream)
{
	struct hyss_gz_stream_data_t *self = (struct hyss_gz_stream_data_t *) stream->abstract;

	return gzflush(self->gz_file, Z_SYNC_FLUSH);
}

const hyss_stream_ops hyss_stream_gzio_ops = {
	hyss_gziop_write, hyss_gziop_read,
	hyss_gziop_close, hyss_gziop_flush,
	"ZLIB",
	hyss_gziop_seek,
	NULL, /* cast */
	NULL, /* stat */
	NULL  /* set_option */
};

hyss_stream *hyss_stream_gzopen(hyss_stream_wrapper *wrapper, const char *path, const char *mode, int options,
							  gear_string **opened_path, hyss_stream_context *context STREAMS_DC)
{
	struct hyss_gz_stream_data_t *self;
	hyss_stream *stream = NULL, *innerstream = NULL;

	/* sanity check the stream: it can be either read-only or write-only */
	if (strchr(mode, '+')) {
		if (options & REPORT_ERRORS) {
			hyss_error_docref(NULL, E_WARNING, "cannot open a zlib stream for reading and writing at the same time!");
		}
		return NULL;
	}

	if (strncasecmp("compress.zlib://", path, 16) == 0) {
		path += 16;
	} else if (strncasecmp("zlib:", path, 5) == 0) {
		path += 5;
	}

	innerstream = hyss_stream_open_wrapper_ex(path, mode, STREAM_MUST_SEEK | options | STREAM_WILL_CAST, opened_path, context);

	if (innerstream) {
		hyss_socket_t fd;

		if (SUCCESS == hyss_stream_cast(innerstream, HYSS_STREAM_AS_FD, (void **) &fd, REPORT_ERRORS)) {
			self = emalloc(sizeof(*self));
			self->stream = innerstream;
			self->gz_file = gzdopen(dup(fd), mode);

			if (self->gz_file) {
				zval *zlevel = context ? hyss_stream_context_get_option(context, "zlib", "level") : NULL;
				if (zlevel && (Z_OK != gzsetparams(self->gz_file, zval_get_long(zlevel), Z_DEFAULT_STRATEGY))) {
					hyss_error(E_WARNING, "failed setting compression level");
				}

				stream = hyss_stream_alloc_rel(&hyss_stream_gzio_ops, self, 0, mode);
				if (stream) {
					stream->flags |= HYSS_STREAM_FLAG_NO_BUFFER;
					return stream;
				}

				gzclose(self->gz_file);
			}

			efree(self);
			if (options & REPORT_ERRORS) {
				hyss_error_docref(NULL, E_WARNING, "gzopen failed");
			}
		}

		hyss_stream_close(innerstream);
	}

	return NULL;
}

static const hyss_stream_wrapper_ops gzip_stream_wops = {
	hyss_stream_gzopen,
	NULL, /* close */
	NULL, /* stat */
	NULL, /* stat_url */
	NULL, /* opendir */
	"ZLIB",
	NULL, /* unlink */
	NULL, /* rename */
	NULL, /* mkdir */
	NULL, /* rmdir */
	NULL
};

const hyss_stream_wrapper hyss_stream_gzip_wrapper =	{
	&gzip_stream_wops,
	NULL,
	0, /* is_url */
};

