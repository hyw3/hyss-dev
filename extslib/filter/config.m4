dnl config.m4 for input filtering extension

HYSS_ARG_ENABLE(filter, whether to enable input filter support,
[  --disable-filter        Disable input filter support], yes)

HYSS_ARG_WITH(pcre-dir, pcre install prefix,
[  --with-pcre-dir         FILTER: pcre install prefix], no, no)

if test "$HYSS_FILTER" != "no"; then

  dnl Check if configure is the HYSS core configure
  if test -n "$HYSS_VERSION"; then
    dnl This extension can not be build as shared when in HYSS core
    ext_shared=no
  else
    dnl This is PECL build, check if bundled PCRE library is used
    old_CPPFLAGS=$CPPFLAGS
    CPPFLAGS=$INCLUDES
    AC_EGREP_CPP(yes,[
#include <main/hyss_config.h>
#if defined(HAVE_BUNDLED_PCRE) && !defined(COMPILE_DL_PCRE)
yes
#endif
    ],[
      HYSS_PCRE_REGEX=yes
    ],[
      AC_EGREP_CPP(yes,[
#include <main/hyss_config.h>
#if defined(HAVE_PCRE) && !defined(COMPILE_DL_PCRE)
yes
#endif
      ],[
        HYSS_PCRE_REGEX=pecl
      ],[
        HYSS_PCRE_REGEX=no
      ])
    ])
    CPPFLAGS=$old_CPPFLAGS
  fi

  HYSS_NEW_EXTENSION(filter, filter.c sanitizing_filters.c logical_filters.c callback_filter.c, $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
  HYSS_SUBST(FILTER_SHARED_LIBADD)

  HYSS_INSTALL_HEADERS([extslib/filter/hyss_filter.h])
  HYSS_ADD_EXTENSION_DEP(filter, pcre)
fi
