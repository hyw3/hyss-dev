/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_FILTER_H
#define HYSS_FILTER_H

#include "SAPI.h"
#include "gear_API.h"
#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "extslib/standard/hyss_string.h"
#include "extslib/standard/html.h"
#include "hyss_variables.h"

extern gear_capi_entry filter_capi_entry;
#define hyssext_filter_ptr &filter_capi_entry

#ifdef ZTS
#include "hypbc.h"
#endif

#define HYSS_FILTER_VERSION HYSS_VERSION

HYSS_MINIT_FUNCTION(filter);
HYSS_MSHUTDOWN_FUNCTION(filter);
HYSS_RINIT_FUNCTION(filter);
HYSS_RSHUTDOWN_FUNCTION(filter);
HYSS_MINFO_FUNCTION(filter);

HYSS_FUNCTION(filter_input);
HYSS_FUNCTION(filter_var);
HYSS_FUNCTION(filter_input_array);
HYSS_FUNCTION(filter_var_array);
HYSS_FUNCTION(filter_list);
HYSS_FUNCTION(filter_has_var);
HYSS_FUNCTION(filter_id);

GEAR_BEGIN_CAPI_GLOBALS(filter)
	zval post_array;
	zval get_array;
	zval cookie_array;
	zval env_array;
	zval server_array;
#if 0
	zval session_array;
#endif
	gear_long default_filter;
	gear_long default_filter_flags;
GEAR_END_CAPI_GLOBALS(filter)

#if defined(COMPILE_DL_FILTER) && defined(ZTS)
GEAR_PBCLS_CACHE_EXTERN()
#endif

#define IF_G(v) GEAR_CAPI_GLOBALS_ACCESSOR(filter, v)

#define HYSS_INPUT_FILTER_PARAM_DECL zval *value, gear_long flags, zval *option_array, char *charset
void hyss_filter_int(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_boolean(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_float(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_validate_regexp(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_validate_domain(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_validate_url(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_validate_email(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_validate_ip(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_validate_mac(HYSS_INPUT_FILTER_PARAM_DECL);

void hyss_filter_string(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_encoded(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_special_chars(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_full_special_chars(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_unsafe_raw(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_email(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_url(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_number_int(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_number_float(HYSS_INPUT_FILTER_PARAM_DECL);
void hyss_filter_add_slashes(HYSS_INPUT_FILTER_PARAM_DECL);

void hyss_filter_callback(HYSS_INPUT_FILTER_PARAM_DECL);

#endif	/* FILTER_H */

