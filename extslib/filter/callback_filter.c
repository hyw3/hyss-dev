/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss_filter.h"

void hyss_filter_callback(HYSS_INPUT_FILTER_PARAM_DECL)
{
	zval retval;
	zval args[1];
	int status;

	if (!option_array || !gear_is_callable(option_array, IS_CALLABLE_CHECK_NO_ACCESS, NULL)) {
		hyss_error_docref(NULL, E_WARNING, "First argument is expected to be a valid callback");
		zval_ptr_dtor(value);
		ZVAL_NULL(value);
		return;
	}

	ZVAL_COPY(&args[0], value);
	status = call_user_function_ex(EG(function_table), NULL, option_array, &retval, 1, args, 0, NULL);

	if (status == SUCCESS && !Z_ISUNDEF(retval)) {
		zval_ptr_dtor(value);
		ZVAL_COPY_VALUE(value, &retval);
	} else {
		zval_ptr_dtor(value);
		ZVAL_NULL(value);
	}

	zval_ptr_dtor(&args[0]);
}

