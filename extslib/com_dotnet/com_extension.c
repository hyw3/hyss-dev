/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <intsafe.h>

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_com_dotnet.h"
#include "hyss_com_dotnet_internal.h"
#include "Gear/gear_exceptions.h"
#include "Gear/gear_interfaces.h"

GEAR_DECLARE_CAPI_GLOBALS(com_dotnet)
static HYSS_GINIT_FUNCTION(com_dotnet);

TsHashTable hyss_com_typelibraries;

gear_class_entry
	*hyss_com_variant_class_entry,
   	*hyss_com_exception_class_entry,
	*hyss_com_saproxy_class_entry;

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_set, 0, 0, 2)
	GEAR_ARG_INFO(0, variant)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_left_right, 0, 0, 2)
	GEAR_ARG_INFO(0, left)
	GEAR_ARG_INFO(0, right)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_abs, 0, 0, 1)
	GEAR_ARG_INFO(0, left)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_fix, 0, 0, 1)
	GEAR_ARG_INFO(0, left)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_int, 0, 0, 1)
	GEAR_ARG_INFO(0, left)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_neg, 0, 0, 1)
	GEAR_ARG_INFO(0, left)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_not, 0, 0, 1)
	GEAR_ARG_INFO(0, left)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_round, 0, 0, 2)
	GEAR_ARG_INFO(0, left)
	GEAR_ARG_INFO(0, decimals)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_cmp, 0, 0, 2)
	GEAR_ARG_INFO(0, left)
	GEAR_ARG_INFO(0, right)
	GEAR_ARG_INFO(0, lcid)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_date_to_timestamp, 0, 0, 1)
	GEAR_ARG_INFO(0, variant)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_date_from_timestamp, 0, 0, 1)
	GEAR_ARG_INFO(0, timestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_get_type, 0, 0, 1)
	GEAR_ARG_INFO(0, variant)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_set_type, 0, 0, 2)
	GEAR_ARG_INFO(0, variant)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_variant_cast, 0, 0, 2)
	GEAR_ARG_INFO(0, variant)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_com_get_active_object, 0, 0, 1)
	GEAR_ARG_INFO(0, progid)
	GEAR_ARG_INFO(0, code_page)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_com_create_guid, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_com_event_sink, 0, 0, 2)
	GEAR_ARG_INFO(0, comobject)
	GEAR_ARG_INFO(0, sinkobject)
	GEAR_ARG_INFO(0, sinkinterface)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_com_print_typeinfo, 0, 0, 1)
	GEAR_ARG_INFO(0, comobject)
	GEAR_ARG_INFO(0, dispinterface)
	GEAR_ARG_INFO(0, wantsink)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_com_message_pump, 0, 0, 0)
	GEAR_ARG_INFO(0, timeoutms)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_com_load_typelib, 0, 0, 1)
	GEAR_ARG_INFO(0, typelib_name)
	GEAR_ARG_INFO(0, case_insensitive)
GEAR_END_ARG_INFO()
/* }}} */

static const gear_function_entry com_dotnet_functions[] = {
	HYSS_FE(variant_set, arginfo_variant_set)
	HYSS_FE(variant_add, arginfo_left_right)
	HYSS_FE(variant_cat, arginfo_left_right)
	HYSS_FE(variant_sub, arginfo_left_right)
	HYSS_FE(variant_mul, arginfo_left_right)
	HYSS_FE(variant_and, arginfo_left_right)
	HYSS_FE(variant_div, arginfo_left_right)
	HYSS_FE(variant_eqv, arginfo_left_right)
	HYSS_FE(variant_idiv, arginfo_left_right)
	HYSS_FE(variant_imp, arginfo_left_right)
	HYSS_FE(variant_mod, arginfo_left_right)
	HYSS_FE(variant_or, arginfo_left_right)
	HYSS_FE(variant_pow, arginfo_left_right)
	HYSS_FE(variant_xor, arginfo_left_right)
	HYSS_FE(variant_abs, arginfo_variant_abs)
	HYSS_FE(variant_fix, arginfo_variant_fix)
	HYSS_FE(variant_int, arginfo_variant_int)
	HYSS_FE(variant_neg, arginfo_variant_neg)
	HYSS_FE(variant_not, arginfo_variant_not)
	HYSS_FE(variant_round, arginfo_variant_round)
	HYSS_FE(variant_cmp, arginfo_variant_cmp)
	HYSS_FE(variant_date_to_timestamp, arginfo_variant_date_to_timestamp)
	HYSS_FE(variant_date_from_timestamp, arginfo_variant_date_from_timestamp)
	HYSS_FE(variant_get_type, arginfo_variant_get_type)
	HYSS_FE(variant_set_type, arginfo_variant_set_type)
	HYSS_FE(variant_cast, arginfo_variant_cast)
	/* com_com.c */
	HYSS_FE(com_create_guid, arginfo_com_create_guid)
	HYSS_FE(com_event_sink, arginfo_com_event_sink)
	HYSS_FE(com_print_typeinfo, arginfo_com_print_typeinfo)
	HYSS_FE(com_message_pump, arginfo_com_message_pump)
	HYSS_FE(com_load_typelib, arginfo_com_load_typelib)
	HYSS_FE(com_get_active_object, arginfo_com_get_active_object)
	HYSS_FE_END
};

/* {{{ com_dotnet_capi_entry
 */
gear_capi_entry com_dotnet_capi_entry = {
	STANDARD_CAPI_HEADER,
	"com_dotnet",
	com_dotnet_functions,
	HYSS_MINIT(com_dotnet),
	HYSS_MSHUTDOWN(com_dotnet),
	HYSS_RINIT(com_dotnet),
	HYSS_RSHUTDOWN(com_dotnet),
	HYSS_MINFO(com_dotnet),
	HYSS_COM_DOTNET_VERSION,
	HYSS_CAPI_GLOBALS(com_dotnet),
	HYSS_GINIT(com_dotnet),
	NULL,
	NULL,
	STANDARD_CAPI_PROPERTIES_EX
};
/* }}} */

#ifdef COMPILE_DL_COM_DOTNET
#ifdef ZTS
GEAR_PBCLS_CACHE_DEFINE()
#endif
GEAR_GET_CAPI(com_dotnet)
#endif

/* {{{ HYSS_ICS
 */

/* com.typelib_file is the path to a file containing a
 * list of typelibraries to register *persistently*.
 * lines starting with ; are comments
 * append #cis to end of typelib name to cause its constants
 * to be loaded case insensitively */
static HYSS_ICS_MH(OnTypeLibFileUpdate)
{
	FILE *typelib_file;
	char *typelib_name_buffer;
	char *strtok_buf = NULL;
	int cached;

	if (NULL == new_value || !new_value->val[0] || (typelib_file = VCWD_FOPEN(new_value->val, "r"))==NULL) {
		return FAILURE;
	}

	typelib_name_buffer = (char *) emalloc(sizeof(char)*1024);

	while (fgets(typelib_name_buffer, 1024, typelib_file)) {
		ITypeLib *pTL;
		char *typelib_name;
		char *modifier, *ptr;
		int mode = CONST_CS | CONST_PERSISTENT;	/* CONST_PERSISTENT is ok here */

		if (typelib_name_buffer[0]==';') {
			continue;
		}
		typelib_name = hyss_strtok_r(typelib_name_buffer, "\r\n", &strtok_buf); /* get rid of newlines */
		if (typelib_name == NULL) {
			continue;
		}
		typelib_name = hyss_strtok_r(typelib_name, "#", &strtok_buf);
		modifier = hyss_strtok_r(NULL, "#", &strtok_buf);
		if (modifier != NULL) {
			if (!strcmp(modifier, "cis") || !strcmp(modifier, "case_insensitive")) {
				mode &= ~CONST_CS;
			}
		}

		/* Remove leading/training white spaces on search_string */
		while (isspace(*typelib_name)) {/* Ends on '\0' in worst case */
			typelib_name ++;
		}
		ptr = typelib_name + strlen(typelib_name) - 1;
		while ((ptr != typelib_name) && isspace(*ptr)) {
			*ptr = '\0';
			ptr--;
		}

		if ((pTL = hyss_com_load_typelib_via_cache(typelib_name, COMG(code_page), &cached)) != NULL) {
			if (!cached) {
				hyss_com_import_typelib(pTL, mode, COMG(code_page));
			}
			ITypeLib_Release(pTL);
		}
	}

	efree(typelib_name_buffer);
	fclose(typelib_file);

	return SUCCESS;
}

HYSS_ICS_BEGIN()
    STD_HYSS_ICS_ENTRY("com.allow_dcom",		"0", HYSS_ICS_SYSTEM, OnUpdateBool, allow_dcom, gear_com_dotnet_globals, com_dotnet_globals)
    STD_HYSS_ICS_ENTRY("com.autoregister_verbose",	"0", HYSS_ICS_ALL, OnUpdateBool, autoreg_verbose, gear_com_dotnet_globals, com_dotnet_globals)
    STD_HYSS_ICS_ENTRY("com.autoregister_typelib",	"0", HYSS_ICS_ALL, OnUpdateBool, autoreg_on, gear_com_dotnet_globals, com_dotnet_globals)
    STD_HYSS_ICS_ENTRY("com.autoregister_casesensitive", "1", HYSS_ICS_ALL, OnUpdateBool, autoreg_case_sensitive, gear_com_dotnet_globals, com_dotnet_globals)
	STD_HYSS_ICS_ENTRY("com.code_page", "", HYSS_ICS_ALL, OnUpdateLong, code_page, gear_com_dotnet_globals, com_dotnet_globals)
	HYSS_ICS_ENTRY("com.typelib_file", "", HYSS_ICS_SYSTEM, OnTypeLibFileUpdate)
HYSS_ICS_END()
/* }}} */

/* {{{ HYSS_GINIT_FUNCTION
 */
static HYSS_GINIT_FUNCTION(com_dotnet)
{
#if defined(COMPILE_DL_COM_DOTNET) && defined(ZTS)
	GEAR_PBCLS_CACHE_UPDATE();
#endif
	memset(com_dotnet_globals, 0, sizeof(*com_dotnet_globals));
	com_dotnet_globals->code_page = CP_ACP;
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(com_dotnet)
{
	gear_class_entry ce, *tmp;

	hyss_com_wrapper_minit(INIT_FUNC_ARGS_PASSTHRU);
	hyss_com_persist_minit(INIT_FUNC_ARGS_PASSTHRU);

	INIT_CLASS_ENTRY(ce, "com_exception", NULL);
	hyss_com_exception_class_entry = gear_register_internal_class_ex(&ce, gear_ce_exception);
	hyss_com_exception_class_entry->ce_flags |= GEAR_ACC_FINAL;
/*	hyss_com_exception_class_entry->constructor->common.fn_flags |= GEAR_ACC_PROTECTED; */

	INIT_CLASS_ENTRY(ce, "com_safearray_proxy", NULL);
	hyss_com_saproxy_class_entry = gear_register_internal_class(&ce);
	hyss_com_saproxy_class_entry->ce_flags |= GEAR_ACC_FINAL;
/*	hyss_com_saproxy_class_entry->constructor->common.fn_flags |= GEAR_ACC_PROTECTED; */
	hyss_com_saproxy_class_entry->get_iterator = hyss_com_saproxy_iter_get;

	INIT_CLASS_ENTRY(ce, "variant", NULL);
	ce.create_object = hyss_com_object_new;
	hyss_com_variant_class_entry = gear_register_internal_class(&ce);
	hyss_com_variant_class_entry->get_iterator = hyss_com_iter_get;
	hyss_com_variant_class_entry->serialize = gear_class_serialize_deny;
	hyss_com_variant_class_entry->unserialize = gear_class_unserialize_deny;

	INIT_CLASS_ENTRY(ce, "com", NULL);
	ce.create_object = hyss_com_object_new;
	tmp = gear_register_internal_class_ex(&ce, hyss_com_variant_class_entry);
	tmp->get_iterator = hyss_com_iter_get;
	tmp->serialize = gear_class_serialize_deny;
	tmp->unserialize = gear_class_unserialize_deny;

	gear_ts_hash_init(&hyss_com_typelibraries, 0, NULL, hyss_com_typelibrary_dtor, 1);

#if HAVE_MSCOREE_H
	INIT_CLASS_ENTRY(ce, "dotnet", NULL);
	ce.create_object = hyss_com_object_new;
	tmp = gear_register_internal_class_ex(&ce, hyss_com_variant_class_entry);
	tmp->get_iterator = hyss_com_iter_get;
	tmp->serialize = gear_class_serialize_deny;
	tmp->unserialize = gear_class_unserialize_deny;
#endif

	REGISTER_ICS_ENTRIES();

#define COM_CONST(x) REGISTER_LONG_CONSTANT(#x, x, CONST_CS|CONST_PERSISTENT)

#define COM_ERR_CONST(x) { \
	gear_long __tmp; \
	ULongToIntPtr(x, &__tmp); \
	REGISTER_LONG_CONSTANT(#x, __tmp, CONST_CS|CONST_PERSISTENT); \
}

	COM_CONST(CLSCTX_INPROC_SERVER);
	COM_CONST(CLSCTX_INPROC_HANDLER);
	COM_CONST(CLSCTX_LOCAL_SERVER);
	COM_CONST(CLSCTX_REMOTE_SERVER);
	COM_CONST(CLSCTX_SERVER);
	COM_CONST(CLSCTX_ALL);

#if 0
	COM_CONST(DISPATCH_METHOD);
	COM_CONST(DISPATCH_PROPERTYGET);
	COM_CONST(DISPATCH_PROPERTYPUT);
#endif

	COM_CONST(VT_NULL);
	COM_CONST(VT_EMPTY);
	COM_CONST(VT_UI1);
	COM_CONST(VT_I1);
	COM_CONST(VT_UI2);
	COM_CONST(VT_I2);
	COM_CONST(VT_UI4);
	COM_CONST(VT_I4);
	COM_CONST(VT_R4);
	COM_CONST(VT_R8);
	COM_CONST(VT_BOOL);
	COM_CONST(VT_ERROR);
	COM_CONST(VT_CY);
	COM_CONST(VT_DATE);
	COM_CONST(VT_BSTR);
	COM_CONST(VT_DECIMAL);
	COM_CONST(VT_UNKNOWN);
	COM_CONST(VT_DISPATCH);
	COM_CONST(VT_VARIANT);
	COM_CONST(VT_INT);
	COM_CONST(VT_UINT);
	COM_CONST(VT_ARRAY);
	COM_CONST(VT_BYREF);

	COM_CONST(CP_ACP);
	COM_CONST(CP_MACCP);
	COM_CONST(CP_OEMCP);
	COM_CONST(CP_UTF7);
	COM_CONST(CP_UTF8);
	COM_CONST(CP_SYMBOL);
	COM_CONST(CP_THREAD_ACP);

	COM_CONST(VARCMP_LT);
	COM_CONST(VARCMP_EQ);
	COM_CONST(VARCMP_GT);
	COM_CONST(VARCMP_NULL);

	COM_CONST(NORM_IGNORECASE);
	COM_CONST(NORM_IGNORENONSPACE);
	COM_CONST(NORM_IGNORESYMBOLS);
	COM_CONST(NORM_IGNOREWIDTH);
	COM_CONST(NORM_IGNOREKANATYPE);
#ifdef NORM_IGNOREKASHIDA
	COM_CONST(NORM_IGNOREKASHIDA);
#endif
	COM_ERR_CONST(DISP_E_DIVBYZERO);
	COM_ERR_CONST(DISP_E_OVERFLOW);
	COM_ERR_CONST(DISP_E_BADINDEX);
	COM_ERR_CONST(MK_E_UNAVAILABLE);

#if SIZEOF_GEAR_LONG == 8
	COM_CONST(VT_UI8);
	COM_CONST(VT_I8);
#endif
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION(com_dotnet)
{
	UNREGISTER_ICS_ENTRIES();
#if HAVE_MSCOREE_H
	if (COMG(dotnet_runtime_stuff)) {
		hyss_com_dotnet_mshutdown();
	}
#endif

	gear_ts_hash_destroy(&hyss_com_typelibraries);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_RINIT_FUNCTION
 */
HYSS_RINIT_FUNCTION(com_dotnet)
{
	COMG(rshutdown_started) = 0;
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_RSHUTDOWN_FUNCTION
 */
HYSS_RSHUTDOWN_FUNCTION(com_dotnet)
{
#if HAVE_MSCOREE_H
	if (COMG(dotnet_runtime_stuff)) {
		hyss_com_dotnet_rshutdown();
	}
#endif
	COMG(rshutdown_started) = 1;
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(com_dotnet)
{
	hyss_info_print_table_start();

	hyss_info_print_table_header(2, "COM support", "enabled");
	hyss_info_print_table_header(2, "DCOM support", COMG(allow_dcom) ? "enabled" : "disabled");

#if HAVE_MSCOREE_H
	hyss_info_print_table_header(2, ".Net support", "enabled");
#else
	hyss_info_print_table_header(2, ".Net support", "not present in this build");
#endif

	hyss_info_print_table_end();

	DISPLAY_ICS_ENTRIES();
}
/* }}} */

