/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_COM_DOTNET_INTERNAL_H
#define HYSS_COM_DOTNET_INTERNAL_H

#define _WIN32_DCOM
#define COBJMACROS
#include <ocidl.h>
#include <oleauto.h>
#include <unknwn.h>
#include <dispex.h>
#include "win32/winutil.h"

#include "gear_ts_hash.h"

typedef struct _hyss_com_dotnet_object {
	gear_object zo;

	VARIANT v;
	int modified;

	int code_page;

	ITypeInfo *typeinfo;

	gear_class_entry *ce;

   	/* associated event sink */
	IDispatch *sink_dispatch;
	GUID sink_id;
	DWORD sink_cookie;

	/* cache for method signatures */
	HashTable *method_cache;
	/* cache for name -> DISPID */
	HashTable *id_of_name_cache;
} hyss_com_dotnet_object;

static inline int hyss_com_is_valid_object(zval *zv)
{
	gear_class_entry *ce = Z_OBJCE_P(zv);
	return strcmp("com", ce->name->val) == 0 ||
		strcmp("dotnet", ce->name->val) == 0 ||
		strcmp("variant", ce->name->val) == 0;
}

#define CDNO_FETCH(zv)			(hyss_com_dotnet_object*)Z_OBJ_P(zv)
#define CDNO_FETCH_VERIFY(obj, zv)	do { \
	if (!hyss_com_is_valid_object(zv)) { \
		hyss_com_throw_exception(E_UNEXPECTED, "expected a variant object"); \
		return; \
	} \
	obj = (hyss_com_dotnet_object*)Z_OBJ_P(zv); \
} while(0)

/* com_extension.c */
TsHashTable hyss_com_typelibraries;
gear_class_entry *hyss_com_variant_class_entry, *hyss_com_exception_class_entry, *hyss_com_saproxy_class_entry;

/* com_handlers.c */
gear_object* hyss_com_object_new(gear_class_entry *ce);
gear_object* hyss_com_object_clone(zval *object);
void hyss_com_object_free_storage(gear_object *object);
gear_object_handlers hyss_com_object_handlers;
void hyss_com_object_enable_event_sink(hyss_com_dotnet_object *obj, int enable);

/* com_saproxy.c */
gear_object_iterator *hyss_com_saproxy_iter_get(gear_class_entry *ce, zval *object, int by_ref);
int hyss_com_saproxy_create(zval *com_object, zval *proxy_out, zval *index);

/* com_olechar.c */
HYSS_COM_DOTNET_API char *hyss_com_olestring_to_string(OLECHAR *olestring,
		size_t *string_len, int codepage);
HYSS_COM_DOTNET_API OLECHAR *hyss_com_string_to_olestring(char *string,
		size_t string_len, int codepage);


/* com_com.c */
HYSS_FUNCTION(com_create_instance);
HYSS_FUNCTION(com_event_sink);
HYSS_FUNCTION(com_create_guid);
HYSS_FUNCTION(com_print_typeinfo);
HYSS_FUNCTION(com_message_pump);
HYSS_FUNCTION(com_load_typelib);
HYSS_FUNCTION(com_get_active_object);

HRESULT hyss_com_invoke_helper(hyss_com_dotnet_object *obj, DISPID id_member,
		WORD flags, DISPPARAMS *disp_params, VARIANT *v, int silent, int allow_noarg);
HRESULT hyss_com_get_id_of_name(hyss_com_dotnet_object *obj, char *name,
		size_t namelen, DISPID *dispid);
int hyss_com_do_invoke_by_id(hyss_com_dotnet_object *obj, DISPID dispid,
		WORD flags,	VARIANT *v, int nargs, zval *args, int silent, int allow_noarg);
int hyss_com_do_invoke(hyss_com_dotnet_object *obj, char *name, size_t namelen,
		WORD flags,	VARIANT *v, int nargs, zval *args, int allow_noarg);
int hyss_com_do_invoke_byref(hyss_com_dotnet_object *obj, gear_internal_function *f,
		WORD flags,	VARIANT *v, int nargs, zval *args);

/* com_wrapper.c */
int hyss_com_wrapper_minit(INIT_FUNC_ARGS);
HYSS_COM_DOTNET_API IDispatch *hyss_com_wrapper_export_as_sink(zval *val, GUID *sinkid, HashTable *id_to_name);
HYSS_COM_DOTNET_API IDispatch *hyss_com_wrapper_export(zval *val);

/* com_persist.c */
int hyss_com_persist_minit(INIT_FUNC_ARGS);

/* com_variant.c */
HYSS_FUNCTION(com_variant_create_instance);
HYSS_FUNCTION(variant_set);
HYSS_FUNCTION(variant_add);
HYSS_FUNCTION(variant_cat);
HYSS_FUNCTION(variant_sub);
HYSS_FUNCTION(variant_mul);
HYSS_FUNCTION(variant_and);
HYSS_FUNCTION(variant_div);
HYSS_FUNCTION(variant_eqv);
HYSS_FUNCTION(variant_idiv);
HYSS_FUNCTION(variant_imp);
HYSS_FUNCTION(variant_mod);
HYSS_FUNCTION(variant_or);
HYSS_FUNCTION(variant_pow);
HYSS_FUNCTION(variant_xor);
HYSS_FUNCTION(variant_abs);
HYSS_FUNCTION(variant_fix);
HYSS_FUNCTION(variant_int);
HYSS_FUNCTION(variant_neg);
HYSS_FUNCTION(variant_not);
HYSS_FUNCTION(variant_round);
HYSS_FUNCTION(variant_cmp);
HYSS_FUNCTION(variant_date_to_timestamp);
HYSS_FUNCTION(variant_date_from_timestamp);
HYSS_FUNCTION(variant_get_type);
HYSS_FUNCTION(variant_set_type);
HYSS_FUNCTION(variant_cast);

HYSS_COM_DOTNET_API void hyss_com_variant_from_zval_with_type(VARIANT *v, zval *z, VARTYPE type, int codepage);
HYSS_COM_DOTNET_API void hyss_com_variant_from_zval(VARIANT *v, zval *z, int codepage);
HYSS_COM_DOTNET_API int hyss_com_zval_from_variant(zval *z, VARIANT *v, int codepage);
HYSS_COM_DOTNET_API int hyss_com_copy_variant(VARIANT *dst, VARIANT *src);

/* com_dotnet.c */
HYSS_FUNCTION(com_dotnet_create_instance);
void hyss_com_dotnet_rshutdown(void);
void hyss_com_dotnet_mshutdown(void);

/* com_misc.c */
void hyss_com_throw_exception(HRESULT code, char *message);
HYSS_COM_DOTNET_API void hyss_com_wrap_dispatch(zval *z, IDispatch *disp,
		int codepage);
HYSS_COM_DOTNET_API void hyss_com_wrap_variant(zval *z, VARIANT *v,
		int codepage);
HYSS_COM_DOTNET_API int hyss_com_safearray_get_elem(VARIANT *array, VARIANT *dest, LONG dim1);

/* com_typeinfo.c */
HYSS_COM_DOTNET_API ITypeLib *hyss_com_load_typelib_via_cache(char *search_string,
		int codepage, int *cached);
HYSS_COM_DOTNET_API ITypeLib *hyss_com_load_typelib(char *search_string, int codepage);
HYSS_COM_DOTNET_API int hyss_com_import_typelib(ITypeLib *TL, int mode,
		int codepage);
void hyss_com_typelibrary_dtor(zval *pDest);
ITypeInfo *hyss_com_locate_typeinfo(char *typelibname, hyss_com_dotnet_object *obj, char *dispname, int sink);
int hyss_com_process_typeinfo(ITypeInfo *typeinfo, HashTable *id_to_name, int printdef, GUID *guid, int codepage);

/* com_iterator.c */
gear_object_iterator *hyss_com_iter_get(gear_class_entry *ce, zval *object, int by_ref);


#endif
