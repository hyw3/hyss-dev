/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Infrastructure for working with persistent COM objects.
 * Implements: IStream* wrapper for HYSS streams.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_com_dotnet.h"
#include "hyss_com_dotnet_internal.h"
#include "Gear/gear_exceptions.h"

/* {{{ expose hyss_stream as a COM IStream */

typedef struct {
	CONST_VTBL struct IStreamVtbl *lpVtbl;
	DWORD engine_thread;
	LONG refcount;
	hyss_stream *stream;
	gear_resource *res;
} hyss_istream;

static int le_istream;
static void istream_destructor(hyss_istream *stm);

static void istream_dtor(gear_resource *rsrc)
{
	hyss_istream *stm = (hyss_istream *)rsrc->ptr;
	istream_destructor(stm);
}

#define FETCH_STM()	\
	hyss_istream *stm = (hyss_istream*)This; \
	if (GetCurrentThreadId() != stm->engine_thread) \
		return RPC_E_WRONG_THREAD;

#define FETCH_STM_EX()	\
	hyss_istream *stm = (hyss_istream*)This;	\
	if (GetCurrentThreadId() != stm->engine_thread)	\
		return RPC_E_WRONG_THREAD;

static HRESULT STDMETHODCALLTYPE stm_queryinterface(
	IStream *This,
	/* [in] */ REFIID riid,
	/* [iid_is][out] */ void **ppvObject)
{
	FETCH_STM_EX();

	if (IsEqualGUID(&IID_IUnknown, riid) ||
			IsEqualGUID(&IID_IStream, riid)) {
		*ppvObject = This;
		InterlockedIncrement(&stm->refcount);
		return S_OK;
	}

	*ppvObject = NULL;
	return E_NOINTERFACE;
}

static ULONG STDMETHODCALLTYPE stm_addref(IStream *This)
{
	FETCH_STM_EX();

	return InterlockedIncrement(&stm->refcount);
}

static ULONG STDMETHODCALLTYPE stm_release(IStream *This)
{
	ULONG ret;
	FETCH_STM();

	ret = InterlockedDecrement(&stm->refcount);
	if (ret == 0) {
		/* destroy it */
		if (stm->res)
			gear_list_delete(stm->res);
	}
	return ret;
}

static HRESULT STDMETHODCALLTYPE stm_read(IStream *This, void *pv, ULONG cb, ULONG *pcbRead)
{
	ULONG nread;
	FETCH_STM();

	nread = (ULONG)hyss_stream_read(stm->stream, pv, cb);

	if (pcbRead) {
		*pcbRead = nread > 0 ? nread : 0;
	}
	if (nread > 0) {
		return S_OK;
	}
	return S_FALSE;
}

static HRESULT STDMETHODCALLTYPE stm_write(IStream *This, void const *pv, ULONG cb, ULONG *pcbWritten)
{
	ULONG nwrote;
	FETCH_STM();

	nwrote = (ULONG)hyss_stream_write(stm->stream, pv, cb);

	if (pcbWritten) {
		*pcbWritten = nwrote > 0 ? nwrote : 0;
	}
	if (nwrote > 0) {
		return S_OK;
	}
	return S_FALSE;
}

static HRESULT STDMETHODCALLTYPE stm_seek(IStream *This, LARGE_INTEGER dlibMove,
		DWORD dwOrigin, ULARGE_INTEGER *plibNewPosition)
{
	off_t offset;
	int whence;
	int ret;
	FETCH_STM();

	switch (dwOrigin) {
		case STREAM_SEEK_SET:	whence = SEEK_SET;	break;
		case STREAM_SEEK_CUR:	whence = SEEK_CUR;	break;
		case STREAM_SEEK_END:	whence = SEEK_END;	break;
		default:
			return STG_E_INVALIDFUNCTION;
	}

	if (dlibMove.HighPart) {
		/* we don't support 64-bit offsets */
		return STG_E_INVALIDFUNCTION;
	}

	offset = (off_t) dlibMove.QuadPart;

	ret = hyss_stream_seek(stm->stream, offset, whence);

	if (plibNewPosition) {
		plibNewPosition->QuadPart = (ULONGLONG)(ret >= 0 ? ret : 0);
	}

	return ret >= 0 ? S_OK : STG_E_INVALIDFUNCTION;
}

static HRESULT STDMETHODCALLTYPE stm_set_size(IStream *This, ULARGE_INTEGER libNewSize)
{
	FETCH_STM();

	if (libNewSize.HighPart) {
		return STG_E_INVALIDFUNCTION;
	}

	if (hyss_stream_truncate_supported(stm->stream)) {
		int ret = hyss_stream_truncate_set_size(stm->stream, (size_t)libNewSize.QuadPart);

		if (ret == 0) {
			return S_OK;
		}
	}

	return STG_E_INVALIDFUNCTION;
}

static HRESULT STDMETHODCALLTYPE stm_copy_to(IStream *This, IStream *pstm, ULARGE_INTEGER cb,
		ULARGE_INTEGER *pcbRead, ULARGE_INTEGER *pcbWritten)
{
	FETCH_STM_EX();

	return E_NOTIMPL;
}

static HRESULT STDMETHODCALLTYPE stm_commit(IStream *This, DWORD grfCommitFlags)
{
	FETCH_STM();

	hyss_stream_flush(stm->stream);

	return S_OK;
}

static HRESULT STDMETHODCALLTYPE stm_revert(IStream *This)
{
	/* NOP */
	return S_OK;
}

static HRESULT STDMETHODCALLTYPE stm_lock_region(IStream *This,
   	ULARGE_INTEGER libOffset, ULARGE_INTEGER cb, DWORD lockType)
{
	return STG_E_INVALIDFUNCTION;
}

static HRESULT STDMETHODCALLTYPE stm_unlock_region(IStream *This,
		ULARGE_INTEGER libOffset, ULARGE_INTEGER cb, DWORD lockType)
{
	return STG_E_INVALIDFUNCTION;
}

static HRESULT STDMETHODCALLTYPE stm_stat(IStream *This,
		STATSTG *pstatstg, DWORD grfStatFlag)
{
	return STG_E_INVALIDFUNCTION;
}

static HRESULT STDMETHODCALLTYPE stm_clone(IStream *This, IStream **ppstm)
{
	return STG_E_INVALIDFUNCTION;
}

static struct IStreamVtbl hyss_istream_vtbl = {
	stm_queryinterface,
	stm_addref,
	stm_release,
	stm_read,
	stm_write,
	stm_seek,
	stm_set_size,
	stm_copy_to,
	stm_commit,
	stm_revert,
	stm_lock_region,
	stm_unlock_region,
	stm_stat,
	stm_clone
};

static void istream_destructor(hyss_istream *stm)
{
	if (stm->res) {
		gear_resource *res = stm->res;
		stm->res = NULL;
		gear_list_delete(res);
		return;
	}

	if (stm->refcount > 0) {
		CoDisconnectObject((IUnknown*)stm, 0);
	}

	gear_list_delete(stm->stream->res);

	CoTaskMemFree(stm);
}
/* }}} */

HYSS_COM_DOTNET_API IStream *hyss_com_wrapper_export_stream(hyss_stream *stream)
{
	hyss_istream *stm = (hyss_istream*)CoTaskMemAlloc(sizeof(*stm));
	zval *tmp;

	if (stm == NULL)
		return NULL;

	memset(stm, 0, sizeof(*stm));
	stm->engine_thread = GetCurrentThreadId();
	stm->lpVtbl = &hyss_istream_vtbl;
	stm->refcount = 1;
	stm->stream = stream;

	GC_ADDREF(stream->res);
	tmp = gear_list_insert(stm, le_istream);
	stm->res = Z_RES_P(tmp);

	return (IStream*)stm;
}

#define CPH_ME(fname, arginfo)	HYSS_ME(com_persist, fname, arginfo, GEAR_ACC_PUBLIC)
#define CPH_SME(fname, arginfo)	HYSS_ME(com_persist, fname, arginfo, GEAR_ACC_ALLOW_STATIC|GEAR_ACC_PUBLIC)
#define CPH_METHOD(fname)		static HYSS_METHOD(com_persist, fname)

#define CPH_FETCH()				hyss_com_persist_helper *helper = (hyss_com_persist_helper*)Z_OBJ_P(getThis());

#define CPH_NO_OBJ()			if (helper->unk == NULL) { hyss_com_throw_exception(E_INVALIDARG, "No COM object is associated with this helper instance"); return; }

typedef struct {
	gear_object			std;
	long codepage;
	IUnknown 			*unk;
	IPersistStream 		*ips;
	IPersistStreamInit	*ipsi;
	IPersistFile		*ipf;
} hyss_com_persist_helper;

static gear_object_handlers helper_handlers;
static gear_class_entry *helper_ce;

static inline HRESULT get_persist_stream(hyss_com_persist_helper *helper)
{
	if (!helper->ips && helper->unk) {
		return IUnknown_QueryInterface(helper->unk, &IID_IPersistStream, &helper->ips);
	}
	return helper->ips ? S_OK : E_NOTIMPL;
}

static inline HRESULT get_persist_stream_init(hyss_com_persist_helper *helper)
{
	if (!helper->ipsi && helper->unk) {
		return IUnknown_QueryInterface(helper->unk, &IID_IPersistStreamInit, &helper->ipsi);
	}
	return helper->ipsi ? S_OK : E_NOTIMPL;
}

static inline HRESULT get_persist_file(hyss_com_persist_helper *helper)
{
	if (!helper->ipf && helper->unk) {
		return IUnknown_QueryInterface(helper->unk, &IID_IPersistFile, &helper->ipf);
	}
	return helper->ipf ? S_OK : E_NOTIMPL;
}


/* {{{ proto string COMPersistHelper::GetCurFile()
   Determines the filename into which an object will be saved, or false if none is set, via IPersistFile::GetCurFile */
CPH_METHOD(GetCurFileName)
{
	HRESULT res;
	OLECHAR *olename = NULL;
	CPH_FETCH();

	CPH_NO_OBJ();

	res = get_persist_file(helper);
	if (helper->ipf) {
		res = IPersistFile_GetCurFile(helper->ipf, &olename);

		if (res == S_OK) {
			size_t len;
			char *str = hyss_com_olestring_to_string(olename,
				   &len, helper->codepage);
			RETVAL_STRINGL(str, len);
			// TODO: avoid reallocarion???
			efree(str);
			CoTaskMemFree(olename);
			return;
		} else if (res == S_FALSE) {
			CoTaskMemFree(olename);
			RETURN_FALSE;
		}
		hyss_com_throw_exception(res, NULL);
	} else {
		hyss_com_throw_exception(res, NULL);
	}
}
/* }}} */


/* {{{ proto bool COMPersistHelper::SaveToFile(string filename [, bool remember])
   Persist object data to file, via IPersistFile::Save */
CPH_METHOD(SaveToFile)
{
	HRESULT res;
	char *filename, *fullpath = NULL;
	size_t filename_len;
	gear_bool remember = TRUE;
	OLECHAR *olefilename = NULL;
	CPH_FETCH();

	CPH_NO_OBJ();

	res = get_persist_file(helper);
	if (helper->ipf) {
		if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "p!|b",
					&filename, &filename_len, &remember)) {
			hyss_com_throw_exception(E_INVALIDARG, "Invalid arguments");
			return;
		}

		if (filename) {
			fullpath = expand_filepath(filename, NULL);
			if (!fullpath) {
				RETURN_FALSE;
			}

			if (hyss_check_open_basedir(fullpath)) {
				efree(fullpath);
				RETURN_FALSE;
			}

			olefilename = hyss_com_string_to_olestring(filename, strlen(fullpath), helper->codepage);
			efree(fullpath);
		}
		res = IPersistFile_Save(helper->ipf, olefilename, remember);
		if (SUCCEEDED(res)) {
			if (!olefilename) {
				res = IPersistFile_GetCurFile(helper->ipf, &olefilename);
				if (S_OK == res) {
					IPersistFile_SaveCompleted(helper->ipf, olefilename);
					CoTaskMemFree(olefilename);
					olefilename = NULL;
				}
			} else if (remember) {
				IPersistFile_SaveCompleted(helper->ipf, olefilename);
			}
		}

		if (olefilename) {
			efree(olefilename);
		}

		if (FAILED(res)) {
			hyss_com_throw_exception(res, NULL);
		}

	} else {
		hyss_com_throw_exception(res, NULL);
	}
}
/* }}} */

/* {{{ proto bool COMPersistHelper::LoadFromFile(string filename [, int flags])
   Load object data from file, via IPersistFile::Load */
CPH_METHOD(LoadFromFile)
{
	HRESULT res;
	char *filename, *fullpath;
	size_t filename_len;
	gear_long flags = 0;
	OLECHAR *olefilename;
	CPH_FETCH();

	CPH_NO_OBJ();

	res = get_persist_file(helper);
	if (helper->ipf) {

		if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "p|l",
					&filename, &filename_len, &flags)) {
			hyss_com_throw_exception(E_INVALIDARG, "Invalid arguments");
			return;
		}

		if (!(fullpath = expand_filepath(filename, NULL))) {
			RETURN_FALSE;
		}

		if (hyss_check_open_basedir(fullpath)) {
			efree(fullpath);
			RETURN_FALSE;
		}

		olefilename = hyss_com_string_to_olestring(fullpath, strlen(fullpath), helper->codepage);
		efree(fullpath);

		res = IPersistFile_Load(helper->ipf, olefilename, (DWORD)flags);
		efree(olefilename);

		if (FAILED(res)) {
			hyss_com_throw_exception(res, NULL);
		}

	} else {
		hyss_com_throw_exception(res, NULL);
	}
}
/* }}} */

/* {{{ proto int COMPersistHelper::GetMaxStreamSize()
   Gets maximum stream size required to store the object data, via IPersistStream::GetSizeMax (or IPersistStreamInit::GetSizeMax) */
CPH_METHOD(GetMaxStreamSize)
{
	HRESULT res;
	ULARGE_INTEGER size;
	CPH_FETCH();

	CPH_NO_OBJ();

	res = get_persist_stream_init(helper);
	if (helper->ipsi) {
		res = IPersistStreamInit_GetSizeMax(helper->ipsi, &size);
	} else {
		res = get_persist_stream(helper);
		if (helper->ips) {
			res = IPersistStream_GetSizeMax(helper->ips, &size);
		} else {
			hyss_com_throw_exception(res, NULL);
			return;
		}
	}

	if (res != S_OK) {
		hyss_com_throw_exception(res, NULL);
	} else {
		/* TODO: handle 64 bit properly */
		RETURN_LONG((gear_long)size.QuadPart);
	}
}
/* }}} */

/* {{{ proto int COMPersistHelper::InitNew()
   Initializes the object to a default state, via IPersistStreamInit::InitNew */
CPH_METHOD(InitNew)
{
	HRESULT res;
	CPH_FETCH();

	CPH_NO_OBJ();

	res = get_persist_stream_init(helper);
	if (helper->ipsi) {
		res = IPersistStreamInit_InitNew(helper->ipsi);

		if (res != S_OK) {
			hyss_com_throw_exception(res, NULL);
		} else {
			RETURN_TRUE;
		}
	} else {
		hyss_com_throw_exception(res, NULL);
	}
}
/* }}} */

/* {{{ proto mixed COMPersistHelper::LoadFromStream(resource stream)
   Initializes an object from the stream where it was previously saved, via IPersistStream::Load or OleLoadFromStream */
CPH_METHOD(LoadFromStream)
{
	zval *zstm;
	hyss_stream *stream;
	IStream *stm = NULL;
	HRESULT res;
	CPH_FETCH();

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "r", &zstm)) {
		hyss_com_throw_exception(E_INVALIDARG, "invalid arguments");
		return;
	}

	hyss_stream_from_zval_no_verify(stream, zstm);

	if (stream == NULL) {
		hyss_com_throw_exception(E_INVALIDARG, "expected a stream");
		return;
	}

	stm = hyss_com_wrapper_export_stream(stream);
	if (stm == NULL) {
		hyss_com_throw_exception(E_UNEXPECTED, "failed to wrap stream");
		return;
	}

	res = S_OK;
	RETVAL_TRUE;

	if (helper->unk == NULL) {
		IDispatch *disp = NULL;

		/* we need to create an object and load using OleLoadFromStream */
		res = OleLoadFromStream(stm, &IID_IDispatch, &disp);

		if (SUCCEEDED(res)) {
			hyss_com_wrap_dispatch(return_value, disp, COMG(code_page));
		}
	} else {
		res = get_persist_stream_init(helper);
		if (helper->ipsi) {
			res = IPersistStreamInit_Load(helper->ipsi, stm);
		} else {
			res = get_persist_stream(helper);
			if (helper->ips) {
				res = IPersistStreamInit_Load(helper->ipsi, stm);
			}
		}
	}
	IStream_Release(stm);

	if (FAILED(res)) {
		hyss_com_throw_exception(res, NULL);
		RETURN_NULL();
	}
}
/* }}} */

/* {{{ proto int COMPersistHelper::SaveToStream(resource stream)
   Saves the object to a stream, via IPersistStream::Save */
CPH_METHOD(SaveToStream)
{
	zval *zstm;
	hyss_stream *stream;
	IStream *stm = NULL;
	HRESULT res;
	CPH_FETCH();

	CPH_NO_OBJ();

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "r", &zstm)) {
		hyss_com_throw_exception(E_INVALIDARG, "invalid arguments");
		return;
	}

	hyss_stream_from_zval_no_verify(stream, zstm);

	if (stream == NULL) {
		hyss_com_throw_exception(E_INVALIDARG, "expected a stream");
		return;
	}

	stm = hyss_com_wrapper_export_stream(stream);
	if (stm == NULL) {
		hyss_com_throw_exception(E_UNEXPECTED, "failed to wrap stream");
		return;
	}

	res = get_persist_stream_init(helper);
	if (helper->ipsi) {
		res = IPersistStreamInit_Save(helper->ipsi, stm, TRUE);
	} else {
		res = get_persist_stream(helper);
		if (helper->ips) {
			res = IPersistStream_Save(helper->ips, stm, TRUE);
		}
	}

	IStream_Release(stm);

	if (FAILED(res)) {
		hyss_com_throw_exception(res, NULL);
		return;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto COMPersistHelper::__construct([object com_object])
   Creates a persistence helper object, usually associated with a com_object */
CPH_METHOD(__construct)
{
	hyss_com_dotnet_object *obj = NULL;
	zval *zobj = NULL;
	CPH_FETCH();

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "|O!",
				&zobj, hyss_com_variant_class_entry)) {
		hyss_com_throw_exception(E_INVALIDARG, "invalid arguments");
		return;
	}

	if (!zobj) {
		return;
	}

	obj = CDNO_FETCH(zobj);

	if (V_VT(&obj->v) != VT_DISPATCH || V_DISPATCH(&obj->v) == NULL) {
		hyss_com_throw_exception(E_INVALIDARG, "parameter must represent an IDispatch COM object");
		return;
	}

	/* it is always safe to cast an interface to IUnknown */
	helper->unk = (IUnknown*)V_DISPATCH(&obj->v);
	IUnknown_AddRef(helper->unk);
	helper->codepage = obj->code_page;
}
/* }}} */


static const gear_function_entry com_persist_helper_methods[] = {
	CPH_ME(__construct, NULL)
	CPH_ME(GetCurFileName, NULL)
	CPH_ME(SaveToFile, NULL)
	CPH_ME(LoadFromFile, NULL)
	CPH_ME(GetMaxStreamSize, NULL)
	CPH_ME(InitNew, NULL)
	CPH_ME(LoadFromStream, NULL)
	CPH_ME(SaveToStream, NULL)
	HYSS_FE_END
};

static void helper_free_storage(gear_object *obj)
{
	hyss_com_persist_helper *object = (hyss_com_persist_helper*)obj;

	if (object->ipf) {
		IPersistFile_Release(object->ipf);
	}
	if (object->ips) {
		IPersistStream_Release(object->ips);
	}
	if (object->ipsi) {
		IPersistStreamInit_Release(object->ipsi);
	}
	if (object->unk) {
		IUnknown_Release(object->unk);
	}
	gear_object_std_dtor(&object->std);
}


static gear_object* helper_clone(zval *obj)
{
	hyss_com_persist_helper *clone, *object = (hyss_com_persist_helper*)Z_OBJ_P(obj);

	clone = emalloc(sizeof(*object));
	memcpy(clone, object, sizeof(*object));

	gear_object_std_init(&clone->std, object->std.ce);

	if (clone->ipf) {
		IPersistFile_AddRef(clone->ipf);
	}
	if (clone->ips) {
		IPersistStream_AddRef(clone->ips);
	}
	if (clone->ipsi) {
		IPersistStreamInit_AddRef(clone->ipsi);
	}
	if (clone->unk) {
		IUnknown_AddRef(clone->unk);
	}
	return (gear_object*)clone;
}

static gear_object* helper_new(gear_class_entry *ce)
{
	hyss_com_persist_helper *helper;

	helper = emalloc(sizeof(*helper));
	memset(helper, 0, sizeof(*helper));

	gear_object_std_init(&helper->std, helper_ce);
	helper->std.handlers = &helper_handlers;

	return &helper->std;
}

int hyss_com_persist_minit(INIT_FUNC_ARGS)
{
	gear_class_entry ce;

	memcpy(&helper_handlers, &std_object_handlers, sizeof(helper_handlers));
	helper_handlers.free_obj = helper_free_storage;
	helper_handlers.clone_obj = helper_clone;

	INIT_CLASS_ENTRY(ce, "COMPersistHelper", com_persist_helper_methods);
	ce.create_object = helper_new;
	helper_ce = gear_register_internal_class(&ce);
	helper_ce->ce_flags |= GEAR_ACC_FINAL;

	le_istream = gear_register_list_destructors_ex(istream_dtor,
			NULL, "com_dotnet_istream_wrapper", capi_number);

	return SUCCESS;
}

