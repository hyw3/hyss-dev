/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_com_dotnet.h"
#include "hyss_com_dotnet_internal.h"
#include "Gear/gear_exceptions.h"

void hyss_com_throw_exception(HRESULT code, char *message)
{
	int free_msg = 0;
	if (message == NULL) {
		message = hyss_win32_error_to_msg(code);
		free_msg = 1;
	}
#if SIZEOF_GEAR_LONG == 8
	gear_throw_exception(hyss_com_exception_class_entry, message, (gear_long)(uint32_t)code);
#else
	gear_throw_exception(hyss_com_exception_class_entry, message, (gear_long)code);
#endif
	if (free_msg) {
		LocalFree(message);
	}
}

HYSS_COM_DOTNET_API void hyss_com_wrap_dispatch(zval *z, IDispatch *disp,
		int codepage)
{
	hyss_com_dotnet_object *obj;

	obj = emalloc(sizeof(*obj));
	memset(obj, 0, sizeof(*obj));
	obj->code_page = codepage;
	obj->ce = hyss_com_variant_class_entry;
	obj->zo.ce = hyss_com_variant_class_entry;

	VariantInit(&obj->v);
	V_VT(&obj->v) = VT_DISPATCH;
	V_DISPATCH(&obj->v) = disp;

	IDispatch_AddRef(V_DISPATCH(&obj->v));
	IDispatch_GetTypeInfo(V_DISPATCH(&obj->v), 0, LANG_NEUTRAL, &obj->typeinfo);

	gear_object_std_init(&obj->zo, hyss_com_variant_class_entry);
	obj->zo.handlers = &hyss_com_object_handlers;
	ZVAL_OBJ(z, &obj->zo);
}

HYSS_COM_DOTNET_API void hyss_com_wrap_variant(zval *z, VARIANT *v,
		int codepage)
{
	hyss_com_dotnet_object *obj;

	obj = emalloc(sizeof(*obj));
	memset(obj, 0, sizeof(*obj));
	obj->code_page = codepage;
	obj->ce = hyss_com_variant_class_entry;
	obj->zo.ce = hyss_com_variant_class_entry;

	VariantInit(&obj->v);
	VariantCopyInd(&obj->v, v);
	obj->modified = 0;

	if ((V_VT(&obj->v) == VT_DISPATCH) && (V_DISPATCH(&obj->v) != NULL)) {
		IDispatch_GetTypeInfo(V_DISPATCH(&obj->v), 0, LANG_NEUTRAL, &obj->typeinfo);
	}

	gear_object_std_init(&obj->zo, hyss_com_variant_class_entry);
	obj->zo.handlers = &hyss_com_object_handlers;
	ZVAL_OBJ(z, &obj->zo);
}

/* this is a convenience function for fetching a particular
 * element from a (possibly multi-dimensional) safe array */
HYSS_COM_DOTNET_API int hyss_com_safearray_get_elem(VARIANT *array, VARIANT *dest, LONG dim1)
{
	UINT dims;
	LONG lbound, ubound;
	LONG indices[1];
	VARTYPE vt;

	if (!V_ISARRAY(array)) {
		return 0;
	}

	dims = SafeArrayGetDim(V_ARRAY(array));

	if (dims != 1) {
		hyss_error_docref(NULL, E_WARNING,
			   "Can only handle single dimension variant arrays (this array has %d)", dims);
		return 0;
	}

	if (FAILED(SafeArrayGetVartype(V_ARRAY(array), &vt)) || vt == VT_EMPTY) {
		vt = V_VT(array) & ~VT_ARRAY;
	}

	/* determine the bounds */
	SafeArrayGetLBound(V_ARRAY(array), 1, &lbound);
	SafeArrayGetUBound(V_ARRAY(array), 1, &ubound);

	/* check bounds */
	if (dim1 < lbound || dim1 > ubound) {
		hyss_com_throw_exception(DISP_E_BADINDEX, "index out of bounds");
		return 0;
	}

	/* now fetch that element */
	VariantInit(dest);

	indices[0] = dim1;

	if (vt == VT_VARIANT) {
		SafeArrayGetElement(V_ARRAY(array), indices, dest);
	} else {
		V_VT(dest) = vt;
		/* store the value into "lVal" member of the variant.
		 * This works because it is a union; since we know the variant
		 * type, we end up with a working variant */
		SafeArrayGetElement(V_ARRAY(array), indices, &dest->lVal);
	}

	return 1;
}
