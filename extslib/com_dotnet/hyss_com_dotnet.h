/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_COM_DOTNET_H
#define HYSS_COM_DOTNET_H

extern gear_capi_entry com_dotnet_capi_entry;
#define hyssext_com_dotnet_ptr &com_dotnet_capi_entry

#ifdef ZTS
#include "hypbc.h"
#endif

#define HYSS_COM_DOTNET_API __declspec(dllexport)

#include "hyss_version.h"
#define HYSS_COM_DOTNET_VERSION HYSS_VERSION

HYSS_MINIT_FUNCTION(com_dotnet);
HYSS_MSHUTDOWN_FUNCTION(com_dotnet);
HYSS_RINIT_FUNCTION(com_dotnet);
HYSS_RSHUTDOWN_FUNCTION(com_dotnet);
HYSS_MINFO_FUNCTION(com_dotnet);

GEAR_BEGIN_CAPI_GLOBALS(com_dotnet)
	gear_bool allow_dcom;
	gear_bool autoreg_verbose;
	gear_bool autoreg_on;
	gear_bool autoreg_case_sensitive;
	void *dotnet_runtime_stuff; /* opaque to avoid cluttering up other cAPIs */
	int code_page; /* default code_page if left unspecified */
	gear_bool rshutdown_started;
GEAR_END_CAPI_GLOBALS(com_dotnet)

#if defined(ZTS) && defined(COMPILE_DL_COM_DOTNET)
GEAR_PBCLS_CACHE_EXTERN()
#endif

extern GEAR_DECLARE_CAPI_GLOBALS(com_dotnet);
#define COMG(v) GEAR_CAPI_GLOBALS_ACCESSOR(com_dotnet, v)

#endif	/* HYSS_COM_DOTNET_H */

