/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_com_dotnet.h"
#include "hyss_com_dotnet_internal.h"


HYSS_COM_DOTNET_API OLECHAR *hyss_com_string_to_olestring(char *string, size_t string_len, int codepage)
{
	OLECHAR *olestring = NULL;
	DWORD flags = codepage == CP_UTF8 ? 0 : MB_PRECOMPOSED | MB_ERR_INVALID_CHARS;
	BOOL ok;

	if (string_len == -1) {
		/* determine required length for the buffer (includes NUL terminator) */
		string_len = MultiByteToWideChar(codepage, flags, string, -1, NULL, 0);
	} else {
		/* allow room for NUL terminator */
		string_len++;
	}

	if (string_len > 0) {
		olestring = (OLECHAR*)safe_emalloc(string_len, sizeof(OLECHAR), 0);
		/* XXX if that's a real multibyte string, olestring is obviously allocated excessively.
		This should be fixed by reallocating the olestring, but as emalloc is used, that doesn't
		matter much. */
		ok = MultiByteToWideChar(codepage, flags, string, (int)string_len, olestring, (int)string_len);
		if (ok > 0 && (size_t)ok < string_len) {
			olestring[ok] = '\0';
		}
	} else {
		ok = FALSE;
		olestring = (OLECHAR*)emalloc(sizeof(OLECHAR));
		*olestring = 0;
	}

	if (!ok) {
		char *msg = hyss_win32_error_to_msg(GetLastError());

		hyss_error_docref(NULL, E_WARNING,
			"Could not convert string to unicode: `%s'", msg);

		LocalFree(msg);
	}

	return olestring;
}

HYSS_COM_DOTNET_API char *hyss_com_olestring_to_string(OLECHAR *olestring, size_t *string_len, int codepage)
{
	char *string;
	uint32_t length = 0;
	BOOL ok;

	length = WideCharToMultiByte(codepage, 0, olestring, -1, NULL, 0, NULL, NULL);

	if (length) {
		string = (char*)safe_emalloc(length, sizeof(char), 0);
		length = WideCharToMultiByte(codepage, 0, olestring, -1, string, length, NULL, NULL);
		ok = length > 0;
	} else {
		string = (char*)emalloc(sizeof(char));
		*string = '\0';
		ok = FALSE;
		length = 0;
	}

	if (!ok) {
		char *msg = hyss_win32_error_to_msg(GetLastError());

		hyss_error_docref(NULL, E_WARNING,
			"Could not convert string from unicode: `%s'", msg);

		LocalFree(msg);
	}

	if (string_len) {
		*string_len = length-1;
	}

	return string;
}
