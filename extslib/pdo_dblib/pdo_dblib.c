/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "pdo/hyss_pdo.h"
#include "pdo/hyss_pdo_driver.h"
#include "hyss_pdo_dblib.h"
#include "hyss_pdo_dblib_int.h"
#include "gear_exceptions.h"

GEAR_DECLARE_CAPI_GLOBALS(dblib)
static HYSS_GINIT_FUNCTION(dblib);

static const gear_function_entry pdo_dblib_functions[] = {
	HYSS_FE_END
};

static const gear_capi_dep pdo_dblib_deps[] = {
	GEAR_CAPI_REQUIRED("pdo")
	GEAR_CAPI_END
};

#if PDO_DBLIB_IS_MSSQL
gear_capi_entry pdo_mssql_capi_entry = {
#else
gear_capi_entry pdo_dblib_capi_entry = {
#endif
	STANDARD_CAPI_HEADER_EX, NULL,
	pdo_dblib_deps,
#if PDO_DBLIB_IS_MSSQL
	"pdo_mssql",
#elif defined(HYSS_WIN32)
	"pdo_sybase",
#else
	"pdo_dblib",
#endif
	pdo_dblib_functions,
	HYSS_MINIT(pdo_dblib),
	HYSS_MSHUTDOWN(pdo_dblib),
	NULL,
	HYSS_RSHUTDOWN(pdo_dblib),
	HYSS_MINFO(pdo_dblib),
	HYSS_PDO_DBLIB_VERSION,
	HYSS_CAPI_GLOBALS(dblib),
	HYSS_GINIT(dblib),
	NULL,
	NULL,
	STANDARD_CAPI_PROPERTIES_EX
};

#if defined(COMPILE_DL_PDO_DBLIB) || defined(COMPILE_DL_PDO_MSSQL)
#if PDO_DBLIB_IS_MSSQL
GEAR_GET_CAPI(pdo_mssql)
#else
GEAR_GET_CAPI(pdo_dblib)
#endif
#endif

int pdo_dblib_error_handler(DBPROCESS *dbproc, int severity, int dberr,
	int oserr, char *dberrstr, char *oserrstr)
{
	pdo_dblib_err *einfo;
	char *state = "HY000";

	if(dbproc) {
		einfo = (pdo_dblib_err*)dbgetuserdata(dbproc);
		if (!einfo) einfo = &DBLIB_G(err);
	} else {
		einfo = &DBLIB_G(err);
	}

	einfo->severity = severity;
	einfo->oserr = oserr;
	einfo->dberr = dberr;

	if (einfo->oserrstr) {
		efree(einfo->oserrstr);
	}
	if (einfo->dberrstr) {
		efree(einfo->dberrstr);
	}
	if (oserrstr) {
		einfo->oserrstr = estrdup(oserrstr);
	} else {
		einfo->oserrstr = NULL;
	}
	if (dberrstr) {
		einfo->dberrstr = estrdup(dberrstr);
	} else {
		einfo->dberrstr = NULL;
	}

	switch (dberr) {
		case SYBESEOF:
		case SYBEFCON:	state = "01002"; break;
		case SYBEMEM:	state = "HY001"; break;
		case SYBEPWD:	state = "28000"; break;
	}
	strcpy(einfo->sqlstate, state);

	return INT_CANCEL;
}

int pdo_dblib_msg_handler(DBPROCESS *dbproc, DBINT msgno, int msgstate,
	int severity, char *msgtext, char *srvname, char *procname, DBUSMALLINT line)
{
	pdo_dblib_err *einfo;

	if (severity) {
		einfo = (pdo_dblib_err*)dbgetuserdata(dbproc);
		if (!einfo) {
			einfo = &DBLIB_G(err);
		}

		if (einfo->lastmsg) {
			efree(einfo->lastmsg);
		}

		einfo->lastmsg = estrdup(msgtext);
	}

	return 0;
}

void pdo_dblib_err_dtor(pdo_dblib_err *err)
{
	if (!err) {
		return;
	}

	if (err->dberrstr) {
		efree(err->dberrstr);
		err->dberrstr = NULL;
	}
	if (err->lastmsg) {
		efree(err->lastmsg);
		err->lastmsg = NULL;
	}
	if (err->oserrstr) {
		efree(err->oserrstr);
		err->oserrstr = NULL;
	}
}

static HYSS_GINIT_FUNCTION(dblib)
{
	memset(dblib_globals, 0, sizeof(*dblib_globals));
	dblib_globals->err.sqlstate = dblib_globals->sqlstate;
}

HYSS_RSHUTDOWN_FUNCTION(pdo_dblib)
{
	if (DBLIB_G(err).oserrstr) {
		efree(DBLIB_G(err).oserrstr);
		DBLIB_G(err).oserrstr = NULL;
	}
	if (DBLIB_G(err).dberrstr) {
		efree(DBLIB_G(err).dberrstr);
		DBLIB_G(err).dberrstr = NULL;
	}
	if (DBLIB_G(err).lastmsg) {
		efree(DBLIB_G(err).lastmsg);
		DBLIB_G(err).lastmsg = NULL;
	}
	return SUCCESS;
}

HYSS_MINIT_FUNCTION(pdo_dblib)
{
	REGISTER_PDO_CLASS_CONST_LONG("DBLIB_ATTR_CONNECTION_TIMEOUT", (long) PDO_DBLIB_ATTR_CONNECTION_TIMEOUT);
	REGISTER_PDO_CLASS_CONST_LONG("DBLIB_ATTR_QUERY_TIMEOUT", (long) PDO_DBLIB_ATTR_QUERY_TIMEOUT);
	REGISTER_PDO_CLASS_CONST_LONG("DBLIB_ATTR_STRINGIFY_UNIQUEIDENTIFIER", (long) PDO_DBLIB_ATTR_STRINGIFY_UNIQUEIDENTIFIER);
	REGISTER_PDO_CLASS_CONST_LONG("DBLIB_ATTR_VERSION", (long) PDO_DBLIB_ATTR_VERSION);
	REGISTER_PDO_CLASS_CONST_LONG("DBLIB_ATTR_TDS_VERSION", (long) PDO_DBLIB_ATTR_TDS_VERSION);
	REGISTER_PDO_CLASS_CONST_LONG("DBLIB_ATTR_SKIP_EMPTY_ROWSETS", (long) PDO_DBLIB_ATTR_SKIP_EMPTY_ROWSETS);
	REGISTER_PDO_CLASS_CONST_LONG("DBLIB_ATTR_DATETIME_CONVERT", (long) PDO_DBLIB_ATTR_DATETIME_CONVERT);

	if (FAIL == dbinit()) {
		return FAILURE;
	}

	if (FAILURE == hyss_pdo_register_driver(&pdo_dblib_driver)) {
		return FAILURE;
	}

#if !HYSS_DBLIB_IS_MSSQL
	dberrhandle((EHANDLEFUNC) pdo_dblib_error_handler);
	dbmsghandle((MHANDLEFUNC) pdo_dblib_msg_handler);
#endif

	return SUCCESS;
}

HYSS_MSHUTDOWN_FUNCTION(pdo_dblib)
{
	hyss_pdo_unregister_driver(&pdo_dblib_driver);
	dbexit();
	return SUCCESS;
}

HYSS_MINFO_FUNCTION(pdo_dblib)
{
	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "PDO Driver for "
#if PDO_DBLIB_IS_MSSQL
		"MSSQL"
#elif defined(HYSS_WIN32)
		"FreeTDS/Sybase/MSSQL"
#else
		"FreeTDS/Sybase"
#endif
		" DB-lib", "enabled");
	hyss_info_print_table_row(2, "Flavour", PDO_DBLIB_FLAVOUR);
	hyss_info_print_table_end();
}
