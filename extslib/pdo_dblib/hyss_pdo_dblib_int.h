/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PDO_DBLIB_INT_H
#define HYSS_PDO_DBLIB_INT_H

#ifndef PDO_DBLIB_FLAVOUR
# define PDO_DBLIB_FLAVOUR "Generic DB-lib"
#endif

#if HYSS_DBLIB_IS_MSSQL
# include <sqlfront.h>
# include <sqldb.h>

# define DBERRHANDLE(a, b)	dbprocerrhandle(a, b)
# define DBMSGHANDLE(a, b)	dbprocmsghandle(a, b)
# define EHANDLEFUNC		DBERRHANDLE_PROC
# define MHANDLEFUNC		DBMSGHANDLE_PROC
# define DBSETOPT(a, b, c)	dbsetopt(a, b, c)
# define SYBESMSG		SQLESMSG
# define SYBESEOF		SQLESEOF
# define SYBEFCON		SQLECONN		/* SQLEFCON does not exist in MS SQL Server. */
# define SYBEMEM		SQLEMEM
# define SYBEPWD		SQLEPWD

#else
# include <sybfront.h>
# include <sybdb.h>
# include <syberror.h>

/* alias some types */
# define SQLTEXT	SYBTEXT
# define SQLCHAR	SYBCHAR
# define SQLVARCHAR	SYBVARCHAR
# define SQLINT1	SYBINT1
# define SQLINT2	SYBINT2
# define SQLINT4	SYBINT4
# define SQLINT8	SYBINT8
# define SQLINTN	SYBINTN
# define SQLBIT		SYBBIT
# define SQLFLT4	SYBREAL
# define SQLFLT8	SYBFLT8
# define SQLFLTN	SYBFLTN
# define SQLDECIMAL	SYBDECIMAL
# define SQLNUMERIC	SYBNUMERIC
# define SQLDATETIME	SYBDATETIME
# define SQLDATETIM4	SYBDATETIME4
# define SQLDATETIMN	SYBDATETIMN
# ifdef SYBMSDATETIME2
# define SQLMSDATETIME2  SYBMSDATETIME2
# endif
# define SQLMONEY		SYBMONEY
# define SQLMONEY4		SYBMONEY4
# define SQLMONEYN		SYBMONEYN
# define SQLIMAGE		SYBIMAGE
# define SQLBINARY		SYBBINARY
# define SQLVARBINARY	SYBVARBINARY
# ifdef SYBUNIQUE
#  define SQLUNIQUE		SYBUNIQUE
#else
#  define SQLUNIQUE		36 /* FreeTDS Hack */
# endif

# define DBERRHANDLE(a, b)	dberrhandle(b)
# define DBMSGHANDLE(a, b)	dbmsghandle(b)
# define DBSETOPT(a, b, c)	dbsetopt(a, b, c, -1)
# define NO_MORE_RPC_RESULTS	3
# define dbfreelogin		dbloginfree
# define dbrpcexec			dbrpcsend

typedef short TDS_SHORT;
# ifndef HYSS_WIN32
typedef unsigned char *LPBYTE;
# endif
typedef float			DBFLT4;
#endif

/* hardcoded string length from FreeTDS
 * src/tds/convert.c:tds_convert_datetimeall()
 */
# define DATETIME_MAX_LEN   63

int pdo_dblib_error_handler(DBPROCESS *dbproc, int severity, int dberr,
	int oserr, char *dberrstr, char *oserrstr);

int pdo_dblib_msg_handler(DBPROCESS *dbproc, DBINT msgno, int msgstate,
	int severity, char *msgtext, char *srvname, char *procname, DBUSMALLINT line);

extern const pdo_driver_t pdo_dblib_driver;
extern const struct pdo_stmt_methods dblib_stmt_methods;

typedef struct {
	int severity;
	int oserr;
	int dberr;
	char *oserrstr;
	char *dberrstr;
	char *sqlstate;
	char *lastmsg;
} pdo_dblib_err;

void pdo_dblib_err_dtor(pdo_dblib_err *err);

typedef struct {
	LOGINREC	*login;
	DBPROCESS	*link;

	pdo_dblib_err err;
	unsigned assume_national_character_set_strings:1;
	unsigned stringify_uniqueidentifier:1;
	unsigned skip_empty_rowsets:1;
	unsigned datetime_convert:1;
} pdo_dblib_db_handle;

typedef struct {
	pdo_dblib_db_handle *H;
	pdo_dblib_err err;
	unsigned int computed_column_name_count;
} pdo_dblib_stmt;

typedef struct {
	const char* key;
	int value;
} pdo_dblib_keyval;


GEAR_BEGIN_CAPI_GLOBALS(dblib)
	pdo_dblib_err err;
	char sqlstate[6];
GEAR_END_CAPI_GLOBALS(dblib)

#ifdef ZTS
# define DBLIB_G(v) PBCG(dblib_globals_id, gear_dblib_globals *, v)
#else
# define DBLIB_G(v) (dblib_globals.v)
#endif

GEAR_EXTERN_CAPI_GLOBALS(dblib)

enum {
	PDO_DBLIB_ATTR_CONNECTION_TIMEOUT = PDO_ATTR_DRIVER_SPECIFIC,
	PDO_DBLIB_ATTR_QUERY_TIMEOUT,
	PDO_DBLIB_ATTR_STRINGIFY_UNIQUEIDENTIFIER,
	PDO_DBLIB_ATTR_VERSION,
	PDO_DBLIB_ATTR_TDS_VERSION,
	PDO_DBLIB_ATTR_SKIP_EMPTY_ROWSETS,
	PDO_DBLIB_ATTR_DATETIME_CONVERT,
};

#endif
