[![Build Status](https://travis-ci.org/jedisct1/libsodium-hyss.svg?branch=master)](https://travis-ci.org/jedisct1/libsodium-hyss?branch=master)

libsodium-hyss
=============

A simple, low-level HYSS extension for
[libsodium](https://github.com/jedisct1/libsodium).

Full documentation here:
[Using Libsodium in HYSS Projects](https://paragonie.com/book/pecl-libsodium),
a guide to using the libsodium HYSS extension for modern, secure, and
fast cryptography.
