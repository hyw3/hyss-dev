/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_LIBSODIUM_H
#define HYSS_LIBSODIUM_H

extern gear_capi_entry sodium_capi_entry;
#define hyssext_sodium_ptr &sodium_capi_entry

#define HYSS_SODIUM_VERSION HYSS_VERSION

#ifdef ZTS
# include "hypbc.h"
#endif

HYSS_MINIT_FUNCTION(sodium);
HYSS_MSHUTDOWN_FUNCTION(sodium);
HYSS_RINIT_FUNCTION(sodium);
HYSS_RSHUTDOWN_FUNCTION(sodium);
HYSS_MINFO_FUNCTION(sodium);

HYSS_FUNCTION(sodium_add);
HYSS_FUNCTION(sodium_base642bin);
HYSS_FUNCTION(sodium_bin2base64);
HYSS_FUNCTION(sodium_bin2hex);
HYSS_FUNCTION(sodium_compare);
HYSS_FUNCTION(sodium_crypto_aead_aes256gcm_decrypt);
HYSS_FUNCTION(sodium_crypto_aead_aes256gcm_encrypt);
HYSS_FUNCTION(sodium_crypto_aead_aes256gcm_is_available);
HYSS_FUNCTION(sodium_crypto_aead_aes256gcm_keygen);
HYSS_FUNCTION(sodium_crypto_aead_chacha20poly1305_decrypt);
HYSS_FUNCTION(sodium_crypto_aead_chacha20poly1305_encrypt);
HYSS_FUNCTION(sodium_crypto_aead_chacha20poly1305_ietf_decrypt);
HYSS_FUNCTION(sodium_crypto_aead_chacha20poly1305_ietf_encrypt);
HYSS_FUNCTION(sodium_crypto_aead_chacha20poly1305_ietf_keygen);
HYSS_FUNCTION(sodium_crypto_aead_chacha20poly1305_keygen);
HYSS_FUNCTION(sodium_crypto_aead_xchacha20poly1305_ietf_decrypt);
HYSS_FUNCTION(sodium_crypto_aead_xchacha20poly1305_ietf_encrypt);
HYSS_FUNCTION(sodium_crypto_aead_xchacha20poly1305_ietf_keygen);
HYSS_FUNCTION(sodium_crypto_auth);
HYSS_FUNCTION(sodium_crypto_auth_keygen);
HYSS_FUNCTION(sodium_crypto_auth_verify);
HYSS_FUNCTION(sodium_crypto_box);
HYSS_FUNCTION(sodium_crypto_box_keypair);
HYSS_FUNCTION(sodium_crypto_box_keypair_from_secretkey_and_publickey);
HYSS_FUNCTION(sodium_crypto_box_open);
HYSS_FUNCTION(sodium_crypto_box_publickey);
HYSS_FUNCTION(sodium_crypto_box_publickey_from_secretkey);
HYSS_FUNCTION(sodium_crypto_box_seal);
HYSS_FUNCTION(sodium_crypto_box_seal_open);
HYSS_FUNCTION(sodium_crypto_box_secretkey);
HYSS_FUNCTION(sodium_crypto_box_seed_keypair);
HYSS_FUNCTION(sodium_crypto_generichash);
HYSS_FUNCTION(sodium_crypto_generichash_final);
HYSS_FUNCTION(sodium_crypto_generichash_init);
HYSS_FUNCTION(sodium_crypto_generichash_keygen);
HYSS_FUNCTION(sodium_crypto_generichash_update);
HYSS_FUNCTION(sodium_crypto_kdf_derive_from_key);
HYSS_FUNCTION(sodium_crypto_kdf_keygen);
HYSS_FUNCTION(sodium_crypto_kx_client_session_keys);
HYSS_FUNCTION(sodium_crypto_kx_keypair);
HYSS_FUNCTION(sodium_crypto_kx_publickey);
HYSS_FUNCTION(sodium_crypto_kx_secretkey);
HYSS_FUNCTION(sodium_crypto_kx_seed_keypair);
HYSS_FUNCTION(sodium_crypto_kx_server_session_keys);
HYSS_FUNCTION(sodium_crypto_pwhash);
HYSS_FUNCTION(sodium_crypto_pwhash_scryptsalsa208sha256);
HYSS_FUNCTION(sodium_crypto_pwhash_scryptsalsa208sha256_str);
HYSS_FUNCTION(sodium_crypto_pwhash_scryptsalsa208sha256_str_verify);
HYSS_FUNCTION(sodium_crypto_pwhash_str);
HYSS_FUNCTION(sodium_crypto_pwhash_str_needs_rehash);
HYSS_FUNCTION(sodium_crypto_pwhash_str_verify);
HYSS_FUNCTION(sodium_crypto_scalarmult);
HYSS_FUNCTION(sodium_crypto_scalarmult_base);
HYSS_FUNCTION(sodium_crypto_secretbox);
HYSS_FUNCTION(sodium_crypto_secretbox_keygen);
HYSS_FUNCTION(sodium_crypto_secretbox_open);
HYSS_FUNCTION(sodium_crypto_secretstream_xchacha20poly1305_keygen);
HYSS_FUNCTION(sodium_crypto_secretstream_xchacha20poly1305_init_push);
HYSS_FUNCTION(sodium_crypto_secretstream_xchacha20poly1305_push);
HYSS_FUNCTION(sodium_crypto_secretstream_xchacha20poly1305_init_pull);
HYSS_FUNCTION(sodium_crypto_secretstream_xchacha20poly1305_pull);
HYSS_FUNCTION(sodium_crypto_secretstream_xchacha20poly1305_rekey);
HYSS_FUNCTION(sodium_crypto_shorthash);
HYSS_FUNCTION(sodium_crypto_shorthash_keygen);
HYSS_FUNCTION(sodium_crypto_sign);
HYSS_FUNCTION(sodium_crypto_sign_detached);
HYSS_FUNCTION(sodium_crypto_sign_ed25519_pk_to_curve25519);
HYSS_FUNCTION(sodium_crypto_sign_ed25519_sk_to_curve25519);
HYSS_FUNCTION(sodium_crypto_sign_keypair);
HYSS_FUNCTION(sodium_crypto_sign_keypair_from_secretkey_and_publickey);
HYSS_FUNCTION(sodium_crypto_sign_open);
HYSS_FUNCTION(sodium_crypto_sign_publickey);
HYSS_FUNCTION(sodium_crypto_sign_publickey_from_secretkey);
HYSS_FUNCTION(sodium_crypto_sign_secretkey);
HYSS_FUNCTION(sodium_crypto_sign_seed_keypair);
HYSS_FUNCTION(sodium_crypto_sign_verify_detached);
HYSS_FUNCTION(sodium_crypto_stream);
HYSS_FUNCTION(sodium_crypto_stream_keygen);
HYSS_FUNCTION(sodium_crypto_stream_xor);
HYSS_FUNCTION(sodium_hex2bin);
HYSS_FUNCTION(sodium_increment);
HYSS_FUNCTION(sodium_memcmp);
HYSS_FUNCTION(sodium_memzero);
HYSS_FUNCTION(sodium_pad);
HYSS_FUNCTION(sodium_unpad);

#endif	/* HYSS_LIBSODIUM_H */

