/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SNMP_H
#define HYSS_SNMP_H

#define HYSS_SNMP_VERSION HYSS_VERSION

#if HAVE_SNMP

#ifndef DLEXPORT
#define DLEXPORT
#endif

extern gear_capi_entry snmp_capi_entry;
#define snmp_capi_ptr &snmp_capi_entry

#ifdef ZTS
#include "hypbc.h"
#endif

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

HYSS_MINIT_FUNCTION(snmp);
HYSS_MSHUTDOWN_FUNCTION(snmp);
HYSS_MINFO_FUNCTION(snmp);

HYSS_FUNCTION(snmpget);
HYSS_FUNCTION(snmpgetnext);
HYSS_FUNCTION(snmpwalk);
HYSS_FUNCTION(snmprealwalk);
HYSS_FUNCTION(snmpset);
HYSS_FUNCTION(snmp_get_quick_print);
HYSS_FUNCTION(snmp_set_quick_print);
HYSS_FUNCTION(snmp_set_enum_print);
HYSS_FUNCTION(snmp_set_oid_output_format);

HYSS_FUNCTION(snmp2_get);
HYSS_FUNCTION(snmp2_getnext);
HYSS_FUNCTION(snmp2_walk);
HYSS_FUNCTION(snmp2_real_walk);
HYSS_FUNCTION(snmp2_set);

HYSS_FUNCTION(snmp3_get);
HYSS_FUNCTION(snmp3_getnext);
HYSS_FUNCTION(snmp3_walk);
HYSS_FUNCTION(snmp3_real_walk);
HYSS_FUNCTION(snmp3_set);

HYSS_FUNCTION(snmp_set_valueretrieval);
HYSS_FUNCTION(snmp_get_valueretrieval);

HYSS_FUNCTION(snmp_read_mib);

HYSS_METHOD(SNMP, setSecurity);
HYSS_METHOD(SNMP, close);
HYSS_METHOD(SNMP, get);
HYSS_METHOD(SNMP, getnext);
HYSS_METHOD(SNMP, walk);
HYSS_METHOD(SNMP, set);
HYSS_METHOD(SNMP, getErrno);
HYSS_METHOD(SNMP, getError);

typedef struct _hyss_snmp_object {
	struct snmp_session *session;
	int max_oids;
	int valueretrieval;
	int quick_print;
	int enum_print;
	int oid_output_format;
	int snmp_errno;
	int oid_increasing_check;
	int exceptions_enabled;
	char snmp_errstr[256];
	gear_object zo;
} hyss_snmp_object;

static inline hyss_snmp_object *hyss_snmp_fetch_object(gear_object *obj) {
	return (hyss_snmp_object *)((char*)(obj) - XtOffsetOf(hyss_snmp_object, zo));
}

#define Z_SNMP_P(zv) hyss_snmp_fetch_object(Z_OBJ_P((zv)))

typedef int (*hyss_snmp_read_t)(hyss_snmp_object *snmp_object, zval *retval);
typedef int (*hyss_snmp_write_t)(hyss_snmp_object *snmp_object, zval *newval);

typedef struct _ptp_snmp_prop_handler {
	const char *name;
	size_t name_length;
	hyss_snmp_read_t read_func;
	hyss_snmp_write_t write_func;
} hyss_snmp_prop_handler;

typedef struct _snmpobjarg {
	char *oid;
	char type;
	char *value;
	oid  name[MAX_OID_LEN];
	size_t name_length;
} snmpobjarg;

GEAR_BEGIN_CAPI_GLOBALS(snmp)
	int valueretrieval;
GEAR_END_CAPI_GLOBALS(snmp)

#ifdef ZTS
#define SNMP_G(v) PBCG(snmp_globals_id, gear_snmp_globals *, v)
#else
#define SNMP_G(v) (snmp_globals.v)
#endif

#define REGISTER_SNMP_CLASS_CONST_LONG(const_name, value) \
	gear_declare_class_constant_long(hyss_snmp_ce, const_name, sizeof(const_name)-1, (gear_long)value);

#else

#define snmp_capi_ptr NULL

#endif /* HAVE_SNMP */

#define hyssext_snmp_ptr snmp_capi_ptr

#endif  /* HYSS_SNMP_H */
