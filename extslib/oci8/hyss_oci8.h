/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_OCI8
# ifndef HYSS_OCI8_H
#  define HYSS_OCI8_H

#ifdef ZTS
# include "hypbc.h"
#endif

/*
 * The version of the OCI8 extension.
 */
#ifdef HYSS_OCI8_VERSION
/* The definition of HYSS_OCI8_VERSION changed in HYSS and building
 * this code with HYSS (e.g. when using OCI8 from PECL) will conflict.
 */
#undef HYSS_OCI8_VERSION
#endif
#define HYSS_OCI8_VERSION "2.2.0"

extern gear_capi_entry oci8_capi_entry;
#define hyssext_oci8_ptr &oci8_capi_entry
#define hyssext_oci8_11g_ptr &oci8_capi_entry
#define hyssext_oci8_12c_ptr &oci8_capi_entry

HYSS_MINIT_FUNCTION(oci);
HYSS_RINIT_FUNCTION(oci);
HYSS_MSHUTDOWN_FUNCTION(oci);
HYSS_RSHUTDOWN_FUNCTION(oci);
HYSS_MINFO_FUNCTION(oci);

# endif /* !HYSS_OCI8_H */
#else /* !HAVE_OCI8 */

# define oci8_capi_ptr NULL

#endif /* HAVE_OCI8 */

