/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

provider hyssoci {
	probe oci8__check__connection(void *connection, char *client_id, int is_open, long errcode, unsigned long server_status);
	probe oci8__connect__entry(char *username, char *dbname, char *charset, long session_mode, int persistent, int exclusive);
	probe oci8__connect__return(void *connection);
	probe oci8__connection__close(void *connection);
	probe oci8__error(int status, long errcode);
	probe oci8__execute__mode(void *connection, char *client_id, void *statement, unsigned int mode);
	probe oci8__sqltext(void *connection, char *client_id, void *statement, char *sql);

	probe oci8__connect__p__dtor__close(void *connection);
	probe oci8__connect__p__dtor__release(void *connection);
	probe oci8__connect__lookup(void *connection, int is_stub);
	probe oci8__connect__expiry(void *connection, int is_stub, long idle_expiry, long timestamp);
	probe oci8__connect__type(int persistent, int exclusive, void *connection, long num_persistent, long num_connections);
	probe oci8__sesspool__create(void *session_pool);
	probe oci8__sesspool__stats(unsigned long free, unsigned long busy, unsigned long open);
	probe oci8__sesspool__type(int type, void *session_pool);
};
