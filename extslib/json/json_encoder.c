/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "extslib/standard/html.h"
#include "gear_smart_str.h"
#include "hyss_json.h"
#include "hyss_json_encoder.h"
#include <gear_exceptions.h>

static const char digits[] = "0123456789abcdef";

static int hyss_json_escape_string(
		smart_str *buf,	const char *s, size_t len,
		int options, hyss_json_encoder *encoder);

static int hyss_json_determine_array_type(zval *val) /* {{{ */
{
	int i;
	HashTable *myht = Z_ARRVAL_P(val);

	i = myht ? gear_hash_num_elements(myht) : 0;
	if (i > 0) {
		gear_string *key;
		gear_ulong index, idx;

		if (HT_IS_PACKED(myht) && HT_IS_WITHOUT_HOLES(myht)) {
			return HYSS_JSON_OUTPUT_ARRAY;
		}

		idx = 0;
		GEAR_HASH_FOREACH_KEY(myht, index, key) {
			if (key) {
				return HYSS_JSON_OUTPUT_OBJECT;
			} else {
				if (index != idx) {
					return HYSS_JSON_OUTPUT_OBJECT;
				}
			}
			idx++;
		} GEAR_HASH_FOREACH_END();
	}

	return HYSS_JSON_OUTPUT_ARRAY;
}
/* }}} */

/* {{{ Pretty printing support functions */

static inline void hyss_json_pretty_print_char(smart_str *buf, int options, char c) /* {{{ */
{
	if (options & HYSS_JSON_PRETTY_PRINT) {
		smart_str_appendc(buf, c);
	}
}
/* }}} */

static inline void hyss_json_pretty_print_indent(smart_str *buf, int options, hyss_json_encoder *encoder) /* {{{ */
{
	int i;

	if (options & HYSS_JSON_PRETTY_PRINT) {
		for (i = 0; i < encoder->depth; ++i) {
			smart_str_appendl(buf, "    ", 4);
		}
	}
}
/* }}} */

/* }}} */

static inline int hyss_json_is_valid_double(double d) /* {{{ */
{
	return !gear_isinf(d) && !gear_isnan(d);
}
/* }}} */

static inline void hyss_json_encode_double(smart_str *buf, double d, int options) /* {{{ */
{
	size_t len;
	char num[HYSS_DOUBLE_MAX_LENGTH];

	hyss_gcvt(d, (int)PG(serialize_precision), '.', 'e', num);
	len = strlen(num);
	if (options & HYSS_JSON_PRESERVE_ZERO_FRACTION && strchr(num, '.') == NULL && len < HYSS_DOUBLE_MAX_LENGTH - 2) {
		num[len++] = '.';
		num[len++] = '0';
		num[len] = '\0';
	}
	smart_str_appendl(buf, num, len);
}
/* }}} */

#define HYSS_JSON_HASH_PROTECT_RECURSION(_tmp_ht) \
	do { \
		if (_tmp_ht && !(GC_FLAGS(_tmp_ht) & GC_IMMUTABLE)) { \
			GC_PROTECT_RECURSION(_tmp_ht); \
		} \
	} while (0)

#define HYSS_JSON_HASH_UNPROTECT_RECURSION(_tmp_ht) \
	do { \
		if (_tmp_ht && !(GC_FLAGS(_tmp_ht) & GC_IMMUTABLE)) { \
			GC_UNPROTECT_RECURSION(_tmp_ht); \
		} \
	} while (0)

static int hyss_json_encode_array(smart_str *buf, zval *val, int options, hyss_json_encoder *encoder) /* {{{ */
{
	int i, r, need_comma = 0;
	HashTable *myht;

	if (Z_TYPE_P(val) == IS_ARRAY) {
		myht = Z_ARRVAL_P(val);
		r = (options & HYSS_JSON_FORCE_OBJECT) ? HYSS_JSON_OUTPUT_OBJECT : hyss_json_determine_array_type(val);
	} else {
		myht = Z_OBJPROP_P(val);
		r = HYSS_JSON_OUTPUT_OBJECT;
	}

	if (myht && GC_IS_RECURSIVE(myht)) {
		encoder->error_code = HYSS_JSON_ERROR_RECURSION;
		smart_str_appendl(buf, "null", 4);
		return FAILURE;
	}

	HYSS_JSON_HASH_PROTECT_RECURSION(myht);

	if (r == HYSS_JSON_OUTPUT_ARRAY) {
		smart_str_appendc(buf, '[');
	} else {
		smart_str_appendc(buf, '{');
	}

	++encoder->depth;

	i = myht ? gear_hash_num_elements(myht) : 0;

	if (i > 0) {
		gear_string *key;
		zval *data;
		gear_ulong index;

		GEAR_HASH_FOREACH_KEY_VAL_IND(myht, index, key, data) {
			if (r == HYSS_JSON_OUTPUT_ARRAY) {
				if (need_comma) {
					smart_str_appendc(buf, ',');
				} else {
					need_comma = 1;
				}

				hyss_json_pretty_print_char(buf, options, '\n');
				hyss_json_pretty_print_indent(buf, options, encoder);
			} else if (r == HYSS_JSON_OUTPUT_OBJECT) {
				if (key) {
					if (ZSTR_VAL(key)[0] == '\0' && ZSTR_LEN(key) > 0 && Z_TYPE_P(val) == IS_OBJECT) {
						/* Skip protected and private members. */
						continue;
					}

					if (need_comma) {
						smart_str_appendc(buf, ',');
					} else {
						need_comma = 1;
					}

					hyss_json_pretty_print_char(buf, options, '\n');
					hyss_json_pretty_print_indent(buf, options, encoder);

					if (hyss_json_escape_string(buf, ZSTR_VAL(key), ZSTR_LEN(key),
								options & ~HYSS_JSON_NUMERIC_CHECK, encoder) == FAILURE &&
							(options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR) &&
							buf->s) {
						ZSTR_LEN(buf->s) -= 4;
						smart_str_appendl(buf, "\"\"", 2);
					}
				} else {
					if (need_comma) {
						smart_str_appendc(buf, ',');
					} else {
						need_comma = 1;
					}

					hyss_json_pretty_print_char(buf, options, '\n');
					hyss_json_pretty_print_indent(buf, options, encoder);

					smart_str_appendc(buf, '"');
					smart_str_append_long(buf, (gear_long) index);
					smart_str_appendc(buf, '"');
				}

				smart_str_appendc(buf, ':');
				hyss_json_pretty_print_char(buf, options, ' ');
			}

			if (hyss_json_encode_zval(buf, data, options, encoder) == FAILURE &&
					!(options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR)) {
				HYSS_JSON_HASH_UNPROTECT_RECURSION(myht);
				return FAILURE;
			}
		} GEAR_HASH_FOREACH_END();
	}

	HYSS_JSON_HASH_UNPROTECT_RECURSION(myht);

	if (encoder->depth > encoder->max_depth) {
		encoder->error_code = HYSS_JSON_ERROR_DEPTH;
		if (!(options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR)) {
			return FAILURE;
		}
	}
	--encoder->depth;

	/* Only keep closing bracket on same line for empty arrays/objects */
	if (need_comma) {
		hyss_json_pretty_print_char(buf, options, '\n');
		hyss_json_pretty_print_indent(buf, options, encoder);
	}

	if (r == HYSS_JSON_OUTPUT_ARRAY) {
		smart_str_appendc(buf, ']');
	} else {
		smart_str_appendc(buf, '}');
	}

	return SUCCESS;
}
/* }}} */

static int hyss_json_escape_string(
		smart_str *buf, const char *s, size_t len,
		int options, hyss_json_encoder *encoder) /* {{{ */
{
	int status;
	unsigned int us;
	size_t pos, checkpoint;
	char *dst;

	if (len == 0) {
		smart_str_appendl(buf, "\"\"", 2);
		return SUCCESS;
	}

	if (options & HYSS_JSON_NUMERIC_CHECK) {
		double d;
		int type;
		gear_long p;

		if ((type = is_numeric_string(s, len, &p, &d, 0)) != 0) {
			if (type == IS_LONG) {
				smart_str_append_long(buf, p);
				return SUCCESS;
			} else if (type == IS_DOUBLE && hyss_json_is_valid_double(d)) {
				hyss_json_encode_double(buf, d, options);
				return SUCCESS;
			}
		}

	}
	pos = 0;
	checkpoint = buf->s ? ZSTR_LEN(buf->s) : 0;

	/* pre-allocate for string length plus 2 quotes */
	smart_str_alloc(buf, len+2, 0);
	smart_str_appendc(buf, '"');

	do {
		us = (unsigned char)s[pos];
		if (UNEXPECTED(us >= 0x80)) {
			if (pos) {
				smart_str_appendl(buf, s, pos);
				s += pos;
				pos = 0;
			}
			us = hyss_next_utf8_char((unsigned char *)s, len, &pos, &status);
			len -= pos;

			/* check whether UTF8 character is correct */
			if (UNEXPECTED(status != SUCCESS)) {
				s += pos;
				pos = 0;
				if (options & HYSS_JSON_INVALID_UTF8_IGNORE) {
					/* ignore invalid UTF8 character */
					continue;
				} else if (options & HYSS_JSON_INVALID_UTF8_SUBSTITUTE) {
					/* Use Unicode character 'REPLACEMENT CHARACTER' (U+FFFD) */
					if (options & HYSS_JSON_UNESCAPED_UNICODE) {
						smart_str_appendl(buf, "\xef\xbf\xbd", 3);
					} else {
						smart_str_appendl(buf, "\\ufffd", 6);
					}
					continue;
				} else {
					ZSTR_LEN(buf->s) = checkpoint;
					encoder->error_code = HYSS_JSON_ERROR_UTF8;
					if (options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR) {
						smart_str_appendl(buf, "null", 4);
					}
					return FAILURE;
				}

			/* Escape U+2028/U+2029 line terminators, UNLESS both
			   JSON_UNESCAPED_UNICODE and
			   JSON_UNESCAPED_LINE_TERMINATORS were provided */
			} else if ((options & HYSS_JSON_UNESCAPED_UNICODE)
			    && ((options & HYSS_JSON_UNESCAPED_LINE_TERMINATORS)
					|| us < 0x2028 || us > 0x2029)) {
				smart_str_appendl(buf, s, pos);
				s += pos;
				pos = 0;
				continue;
			}
			/* From http://en.wikipedia.org/wiki/UTF16 */
			if (us >= 0x10000) {
				unsigned int next_us;

				us -= 0x10000;
				next_us = (unsigned short)((us & 0x3ff) | 0xdc00);
				us = (unsigned short)((us >> 10) | 0xd800);
				dst = smart_str_extend(buf, 6);
				dst[0] = '\\';
				dst[1] = 'u';
				dst[2] = digits[(us >> 12) & 0xf];
				dst[3] = digits[(us >> 8) & 0xf];
				dst[4] = digits[(us >> 4) & 0xf];
				dst[5] = digits[us & 0xf];
				us = next_us;
			}
			dst = smart_str_extend(buf, 6);
			dst[0] = '\\';
			dst[1] = 'u';
			dst[2] = digits[(us >> 12) & 0xf];
			dst[3] = digits[(us >> 8) & 0xf];
			dst[4] = digits[(us >> 4) & 0xf];
			dst[5] = digits[us & 0xf];
			s += pos;
			pos = 0;
		} else {
			static const uint32_t charmap[4] = {
				0xffffffff, 0x500080c4, 0x10000000, 0x00000000};

			len--;
			if (EXPECTED(!GEAR_BIT_TEST(charmap, us))) {
				pos++;
			} else {
				if (pos) {
					smart_str_appendl(buf, s, pos);
					s += pos;
					pos = 0;
				}
				s++;
				switch (us) {
					case '"':
						if (options & HYSS_JSON_HEX_QUOT) {
							smart_str_appendl(buf, "\\u0022", 6);
						} else {
							smart_str_appendl(buf, "\\\"", 2);
						}
						break;

					case '\\':
						smart_str_appendl(buf, "\\\\", 2);
						break;

					case '/':
						if (options & HYSS_JSON_UNESCAPED_SLASHES) {
							smart_str_appendc(buf, '/');
						} else {
							smart_str_appendl(buf, "\\/", 2);
						}
						break;

					case '\b':
						smart_str_appendl(buf, "\\b", 2);
						break;

					case '\f':
						smart_str_appendl(buf, "\\f", 2);
						break;

					case '\n':
						smart_str_appendl(buf, "\\n", 2);
						break;

					case '\r':
						smart_str_appendl(buf, "\\r", 2);
						break;

					case '\t':
						smart_str_appendl(buf, "\\t", 2);
						break;

					case '<':
						if (options & HYSS_JSON_HEX_TAG) {
							smart_str_appendl(buf, "\\u003C", 6);
						} else {
							smart_str_appendc(buf, '<');
						}
						break;

					case '>':
						if (options & HYSS_JSON_HEX_TAG) {
							smart_str_appendl(buf, "\\u003E", 6);
						} else {
							smart_str_appendc(buf, '>');
						}
						break;

					case '&':
						if (options & HYSS_JSON_HEX_AMP) {
							smart_str_appendl(buf, "\\u0026", 6);
						} else {
							smart_str_appendc(buf, '&');
						}
						break;

					case '\'':
						if (options & HYSS_JSON_HEX_APOS) {
							smart_str_appendl(buf, "\\u0027", 6);
						} else {
							smart_str_appendc(buf, '\'');
						}
						break;

					default:
						GEAR_ASSERT(us < ' ');
						dst = smart_str_extend(buf, 6);
						dst[0] = '\\';
						dst[1] = 'u';
						dst[2] = '0';
						dst[3] = '0';
						dst[4] = digits[(us >> 4) & 0xf];
						dst[5] = digits[us & 0xf];
						break;
				}
			}
		}
	} while (len);

	if (EXPECTED(pos)) {
		smart_str_appendl(buf, s, pos);
	}
	smart_str_appendc(buf, '"');

	return SUCCESS;
}
/* }}} */

static int hyss_json_encode_serializable_object(smart_str *buf, zval *val, int options, hyss_json_encoder *encoder) /* {{{ */
{
	gear_class_entry *ce = Z_OBJCE_P(val);
	HashTable* myht = Z_OBJPROP_P(val);
	zval retval, fname;
	int return_code;

	if (myht && GC_IS_RECURSIVE(myht)) {
		encoder->error_code = HYSS_JSON_ERROR_RECURSION;
		if (options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR) {
			smart_str_appendl(buf, "null", 4);
		}
		return FAILURE;
	}

	HYSS_JSON_HASH_PROTECT_RECURSION(myht);

	ZVAL_STRING(&fname, "jsonSerialize");

	if (FAILURE == call_user_function(EG(function_table), val, &fname, &retval, 0, NULL) || Z_TYPE(retval) == IS_UNDEF) {
		if (!EG(exception)) {
			gear_throw_exception_ex(NULL, 0, "Failed calling %s::jsonSerialize()", ZSTR_VAL(ce->name));
		}
		zval_ptr_dtor(&fname);

		if (options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR) {
			smart_str_appendl(buf, "null", 4);
		}
		HYSS_JSON_HASH_UNPROTECT_RECURSION(myht);
		return FAILURE;
	}

	if (EG(exception)) {
		/* Error already raised */
		zval_ptr_dtor(&retval);
		zval_ptr_dtor(&fname);

		if (options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR) {
			smart_str_appendl(buf, "null", 4);
		}
		HYSS_JSON_HASH_UNPROTECT_RECURSION(myht);
		return FAILURE;
	}

	if ((Z_TYPE(retval) == IS_OBJECT) &&
		(Z_OBJ(retval) == Z_OBJ_P(val))) {
		/* Handle the case where jsonSerialize does: return $this; by going straight to encode array */
		HYSS_JSON_HASH_UNPROTECT_RECURSION(myht);
		return_code = hyss_json_encode_array(buf, &retval, options, encoder);
	} else {
		/* All other types, encode as normal */
		return_code = hyss_json_encode_zval(buf, &retval, options, encoder);
		HYSS_JSON_HASH_UNPROTECT_RECURSION(myht);
	}

	zval_ptr_dtor(&retval);
	zval_ptr_dtor(&fname);

	return return_code;
}
/* }}} */

int hyss_json_encode_zval(smart_str *buf, zval *val, int options, hyss_json_encoder *encoder) /* {{{ */
{
again:
	switch (Z_TYPE_P(val))
	{
		case IS_NULL:
			smart_str_appendl(buf, "null", 4);
			break;

		case IS_TRUE:
			smart_str_appendl(buf, "true", 4);
			break;
		case IS_FALSE:
			smart_str_appendl(buf, "false", 5);
			break;

		case IS_LONG:
			smart_str_append_long(buf, Z_LVAL_P(val));
			break;

		case IS_DOUBLE:
			if (hyss_json_is_valid_double(Z_DVAL_P(val))) {
				hyss_json_encode_double(buf, Z_DVAL_P(val), options);
			} else {
				encoder->error_code = HYSS_JSON_ERROR_INF_OR_NAN;
				smart_str_appendc(buf, '0');
			}
			break;

		case IS_STRING:
			return hyss_json_escape_string(buf, Z_STRVAL_P(val), Z_STRLEN_P(val), options, encoder);

		case IS_OBJECT:
			if (instanceof_function(Z_OBJCE_P(val), hyss_json_serializable_ce)) {
				return hyss_json_encode_serializable_object(buf, val, options, encoder);
			}
			/* fallthrough -- Non-serializable object */
		case IS_ARRAY: {
			/* Avoid modifications (and potential freeing) of the array through a reference when a
			 * jsonSerialize() method is invoked. */
			zval zv;
			int res;
			ZVAL_COPY(&zv, val);
			res = hyss_json_encode_array(buf, &zv, options, encoder);
			zval_ptr_dtor_nogc(&zv);
			return res;
		}

		case IS_REFERENCE:
			val = Z_REFVAL_P(val);
			goto again;

		default:
			encoder->error_code = HYSS_JSON_ERROR_UNSUPPORTED_TYPE;
			if (options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR) {
				smart_str_appendl(buf, "null", 4);
			}
			return FAILURE;
	}

	return SUCCESS;
}
/* }}} */

