/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_JSON_PARSER_H
#define	HYSS_JSON_PARSER_H

#include "hyss.h"
#include "hyss_json_scanner.h"

typedef struct _hyss_json_parser hyss_json_parser;

typedef int (*hyss_json_parser_func_array_create_t)(
		hyss_json_parser *parser, zval *array);
typedef int (*hyss_json_parser_func_array_append_t)(
		hyss_json_parser *parser, zval *array, zval *zvalue);
typedef int (*hyss_json_parser_func_array_start_t)(
		hyss_json_parser *parser);
typedef int (*hyss_json_parser_func_array_end_t)(
		hyss_json_parser *parser, zval *object);
typedef int (*hyss_json_parser_func_object_create_t)(
		hyss_json_parser *parser, zval *object);
typedef int (*hyss_json_parser_func_object_update_t)(
		hyss_json_parser *parser, zval *object, gear_string *key, zval *zvalue);
typedef int (*hyss_json_parser_func_object_start_t)(
		hyss_json_parser *parser);
typedef int (*hyss_json_parser_func_object_end_t)(
		hyss_json_parser *parser, zval *object);

typedef struct _hyss_json_parser_methods {
	hyss_json_parser_func_array_create_t array_create;
	hyss_json_parser_func_array_append_t array_append;
	hyss_json_parser_func_array_start_t array_start;
	hyss_json_parser_func_array_end_t array_end;
	hyss_json_parser_func_object_create_t object_create;
	hyss_json_parser_func_object_update_t object_update;
	hyss_json_parser_func_object_start_t object_start;
	hyss_json_parser_func_object_end_t object_end;
} hyss_json_parser_methods;

struct _hyss_json_parser {
	hyss_json_scanner scanner;
	zval *return_value;
	int depth;
	int max_depth;
	hyss_json_parser_methods methods;
};

HYSS_JSON_API void hyss_json_parser_init_ex(
		hyss_json_parser *parser,
		zval *return_value,
		char *str,
		size_t str_len,
		int options,
		int max_depth,
		const hyss_json_parser_methods *methods);

HYSS_JSON_API void hyss_json_parser_init(
		hyss_json_parser *parser,
		zval *return_value,
		char *str,
		size_t str_len,
		int options,
		int max_depth);

HYSS_JSON_API hyss_json_error_code hyss_json_parser_error_code(const hyss_json_parser *parser);

HYSS_JSON_API int hyss_json_parse(hyss_json_parser *parser);

int hyss_json_yyparse(hyss_json_parser *parser);

#endif	/* HYSS_JSON_PARSER_H */
