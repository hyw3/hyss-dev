dnl config.m4 for extension json

HYSS_ARG_ENABLE(json, whether to enable JavaScript Object Serialization support,
[  --disable-json          Disable JavaScript Object Serialization support], yes)

if test "$HYSS_JSON" != "no"; then
  AC_DEFINE([HAVE_JSON],1 ,[whether to enable JavaScript Object Serialization support])
  AC_HEADER_STDC

HYSS_NEW_EXTENSION(json,
	  json.c \
	  json_encoder.c \
	  json_parser.tab.c \
	  json_scanner.c,
	  $ext_shared,, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
  HYSS_INSTALL_HEADERS([extslib/json], [hyss_json.h hyss_json_parser.h hyss_json_scanner.h])
  HYSS_ADD_MAKEFILE_FRAGMENT()
  HYSS_SUBST(JSON_SHARED_LIBADD)
fi
