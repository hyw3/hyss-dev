/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_JSON_H
#define HYSS_JSON_H

#define HYSS_JSON_VERSION "1.7.0"
#include "gear_smart_str_public.h"

extern gear_capi_entry json_capi_entry;
#define hyssext_json_ptr &json_capi_entry

#if defined(HYSS_WIN32) && defined(JSON_EXPORTS)
#define HYSS_JSON_API __declspec(dllexport)
#else
#define HYSS_JSON_API HYSSAPI
#endif

#ifdef ZTS
#include "hypbc.h"
#endif

extern HYSS_JSON_API gear_class_entry *hyss_json_serializable_ce;

/* error codes */
typedef enum {
	HYSS_JSON_ERROR_NONE = 0,
	HYSS_JSON_ERROR_DEPTH,
	HYSS_JSON_ERROR_STATE_MISMATCH,
	HYSS_JSON_ERROR_CTRL_CHAR,
	HYSS_JSON_ERROR_SYNTAX,
	HYSS_JSON_ERROR_UTF8,
	HYSS_JSON_ERROR_RECURSION,
	HYSS_JSON_ERROR_INF_OR_NAN,
	HYSS_JSON_ERROR_UNSUPPORTED_TYPE,
	HYSS_JSON_ERROR_INVALID_PROPERTY_NAME,
	HYSS_JSON_ERROR_UTF16
} hyss_json_error_code;

/* json_decode() options */
#define HYSS_JSON_OBJECT_AS_ARRAY            (1<<0)
#define HYSS_JSON_BIGINT_AS_STRING           (1<<1)

/* json_encode() options */
#define HYSS_JSON_HEX_TAG                    (1<<0)
#define HYSS_JSON_HEX_AMP                    (1<<1)
#define HYSS_JSON_HEX_APOS                   (1<<2)
#define HYSS_JSON_HEX_QUOT                   (1<<3)
#define HYSS_JSON_FORCE_OBJECT               (1<<4)
#define HYSS_JSON_NUMERIC_CHECK              (1<<5)
#define HYSS_JSON_UNESCAPED_SLASHES          (1<<6)
#define HYSS_JSON_PRETTY_PRINT               (1<<7)
#define HYSS_JSON_UNESCAPED_UNICODE          (1<<8)
#define HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR    (1<<9)
#define HYSS_JSON_PRESERVE_ZERO_FRACTION     (1<<10)
#define HYSS_JSON_UNESCAPED_LINE_TERMINATORS (1<<11)

/* json_decode() and json_encode() common options */
#define HYSS_JSON_INVALID_UTF8_IGNORE        (1<<20)
#define HYSS_JSON_INVALID_UTF8_SUBSTITUTE    (1<<21)
#define HYSS_JSON_THROW_ON_ERROR             (1<<22)

/* Internal flags */
#define HYSS_JSON_OUTPUT_ARRAY	0
#define HYSS_JSON_OUTPUT_OBJECT	1

/* default depth */
#define HYSS_JSON_PARSER_DEFAULT_DEPTH 512

GEAR_BEGIN_CAPI_GLOBALS(json)
	int encoder_depth;
	int encode_max_depth;
	hyss_json_error_code error_code;
GEAR_END_CAPI_GLOBALS(json)

HYSS_JSON_API GEAR_EXTERN_CAPI_GLOBALS(json)
#define JSON_G(v) GEAR_CAPI_GLOBALS_ACCESSOR(json, v)

#if defined(ZTS) && defined(COMPILE_DL_JSON)
GEAR_PBCLS_CACHE_EXTERN()
#endif

HYSS_JSON_API int hyss_json_encode_ex(smart_str *buf, zval *val, int options, gear_long depth);
HYSS_JSON_API int hyss_json_encode(smart_str *buf, zval *val, int options);
HYSS_JSON_API int hyss_json_decode_ex(zval *return_value, char *str, size_t str_len, gear_long options, gear_long depth);

static inline int hyss_json_decode(zval *return_value, char *str, int str_len, gear_bool assoc, gear_long depth)
{
	return hyss_json_decode_ex(return_value, str, str_len, assoc ? HYSS_JSON_OBJECT_AS_ARRAY : 0, depth);
}

#endif  /* HYSS_JSON_H */

