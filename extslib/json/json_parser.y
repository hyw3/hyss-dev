%code top {
/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_json.h"
#include "hyss_json_parser.h"

#define YYDEBUG 0

#if YYDEBUG
int json_yydebug = 1;
#endif

#ifdef _MSC_VER
#define YYMALLOC malloc
#define YYFREE free
#endif

#define HYSS_JSON_USE(uv) ((void) (uv))
#define HYSS_JSON_USE_1(uvr, uv1) HYSS_JSON_USE(uvr); HYSS_JSON_USE(uv1)
#define HYSS_JSON_USE_2(uvr, uv1, uv2) HYSS_JSON_USE(uvr); HYSS_JSON_USE(uv1); HYSS_JSON_USE(uv2)

#define HYSS_JSON_DEPTH_DEC --parser->depth
#define HYSS_JSON_DEPTH_INC \
	if (parser->max_depth && parser->depth >= parser->max_depth) { \
		parser->scanner.errcode = HYSS_JSON_ERROR_DEPTH; \
		YYERROR; \
	} \
	++parser->depth

}

%pure-parser
%name-prefix "hyss_json_yy"
%lex-param  { hyss_json_parser *parser  }
%parse-param { hyss_json_parser *parser }

%union {
	zval value;
	struct {
		gear_string *key;
		zval val;
	} pair;
}


%token <value> HYSS_JSON_T_NUL
%token <value> HYSS_JSON_T_TRUE
%token <value> HYSS_JSON_T_FALSE
%token <value> HYSS_JSON_T_INT
%token <value> HYSS_JSON_T_DOUBLE
%token <value> HYSS_JSON_T_STRING
%token <value> HYSS_JSON_T_ESTRING
%token <value> HYSS_JSON_T_EOI
%token <value> HYSS_JSON_T_ERROR

%type <value> start object key value array errlex
%type <value> members member elements element
%type <pair> pair

%destructor { zval_ptr_dtor_nogc(&$$); } <value>
%destructor { gear_string_release_ex($$.key, 0); zval_ptr_dtor_nogc(&$$.val); } <pair>

%code {
static int hyss_json_yylex(union YYSTYPE *value, hyss_json_parser *parser);
static void hyss_json_yyerror(hyss_json_parser *parser, char const *msg);

}

%% /* Rules */

start:
		value HYSS_JSON_T_EOI
			{
				ZVAL_COPY_VALUE(&$$, &$1);
				ZVAL_COPY_VALUE(parser->return_value, &$1);
				HYSS_JSON_USE($2); YYACCEPT;
			}
	|	value errlex
			{
				HYSS_JSON_USE_2($$, $1, $2);
			}
;

object:
		'{'
			{
				HYSS_JSON_DEPTH_INC;
				if (parser->methods.object_start && FAILURE == parser->methods.object_start(parser)) {
					YYERROR;
				}
			}
		members object_end
			{
				ZVAL_COPY_VALUE(&$$, &$3);
				HYSS_JSON_DEPTH_DEC;
				if (parser->methods.object_end && FAILURE == parser->methods.object_end(parser, &$$)) {
					YYERROR;
				}
			}
;

object_end:
		'}'
	|	']'
			{
				parser->scanner.errcode = HYSS_JSON_ERROR_STATE_MISMATCH;
				YYERROR;
			}
;

members:
		/* empty */
			{
				parser->methods.object_create(parser, &$$);
			}
	|	member
;

member:
		pair
			{
				parser->methods.object_create(parser, &$$);
				if (parser->methods.object_update(parser, &$$, $1.key, &$1.val) == FAILURE) {
					YYERROR;
				}
			}
	|	member ',' pair
			{
				if (parser->methods.object_update(parser, &$1, $3.key, &$3.val) == FAILURE) {
					YYERROR;
				}
				ZVAL_COPY_VALUE(&$$, &$1);
			}
	|	member errlex
			{
				HYSS_JSON_USE_2($$, $1, $2);
			}
;

pair:
		key ':' value
			{
				$$.key = Z_STR($1);
				ZVAL_COPY_VALUE(&$$.val, &$3);
			}
	|	key errlex
			{
				HYSS_JSON_USE_2($$, $1, $2);
			}
;

array:
		'['
			{
				HYSS_JSON_DEPTH_INC;
				if (parser->methods.array_start && FAILURE == parser->methods.array_start(parser)) {
					YYERROR;
				}
			}
		elements array_end
			{
				ZVAL_COPY_VALUE(&$$, &$3);
				HYSS_JSON_DEPTH_DEC;
				if (parser->methods.array_end && FAILURE == parser->methods.array_end(parser, &$$)) {
					YYERROR;
				}
			}
;

array_end:
		']'
	|	'}'
			{
				parser->scanner.errcode = HYSS_JSON_ERROR_STATE_MISMATCH;
				YYERROR;
			}
;

elements:
		/* empty */
			{
				parser->methods.array_create(parser, &$$);
			}
	|	element
;

element:
		value
			{
				parser->methods.array_create(parser, &$$);
				parser->methods.array_append(parser, &$$, &$1);
			}
	|	element ',' value
			{
				parser->methods.array_append(parser, &$1, &$3);
				ZVAL_COPY_VALUE(&$$, &$1);
			}
	|	element errlex
			{
				HYSS_JSON_USE_2($$, $1, $2);
			}
;

key:
		HYSS_JSON_T_STRING
	|	HYSS_JSON_T_ESTRING
;

value:
		object
	|	array
	|	HYSS_JSON_T_STRING
	|	HYSS_JSON_T_ESTRING
	|	HYSS_JSON_T_INT
	|	HYSS_JSON_T_DOUBLE
	|	HYSS_JSON_T_NUL
	|	HYSS_JSON_T_TRUE
	|	HYSS_JSON_T_FALSE
	|	errlex
;

errlex:
		HYSS_JSON_T_ERROR
			{
				HYSS_JSON_USE_1($$, $1);
				YYERROR;
			}
;

%% /* Functions */

static int hyss_json_parser_array_create(hyss_json_parser *parser, zval *array)
{
	array_init(array);
	return SUCCESS;
}

static int hyss_json_parser_array_append(hyss_json_parser *parser, zval *array, zval *zvalue)
{
	gear_hash_next_index_insert(Z_ARRVAL_P(array), zvalue);
	return SUCCESS;
}

static int hyss_json_parser_object_create(hyss_json_parser *parser, zval *object)
{
	if (parser->scanner.options & HYSS_JSON_OBJECT_AS_ARRAY) {
		array_init(object);
		return SUCCESS;
	} else {
		return object_init(object);
	}
}

static int hyss_json_parser_object_update(hyss_json_parser *parser, zval *object, gear_string *key, zval *zvalue)
{
	/* if JSON_OBJECT_AS_ARRAY is set */
	if (Z_TYPE_P(object) == IS_ARRAY) {
		gear_symtable_update(Z_ARRVAL_P(object), key, zvalue);
	} else {
		zval zkey;
		if (ZSTR_LEN(key) > 0 && ZSTR_VAL(key)[0] == '\0') {
			parser->scanner.errcode = HYSS_JSON_ERROR_INVALID_PROPERTY_NAME;
			gear_string_release_ex(key, 0);
			zval_ptr_dtor_nogc(zvalue);
			zval_ptr_dtor_nogc(object);
			return FAILURE;
		}
		ZVAL_NEW_STR(&zkey, key);
		gear_std_write_property(object, &zkey, zvalue, NULL);
		Z_TRY_DELREF_P(zvalue);
	}
	gear_string_release_ex(key, 0);

	return SUCCESS;
}

static int hyss_json_yylex(union YYSTYPE *value, hyss_json_parser *parser)
{
	int token = hyss_json_scan(&parser->scanner);
	value->value = parser->scanner.value;
	return token;
}

static void hyss_json_yyerror(hyss_json_parser *parser, char const *msg)
{
	if (!parser->scanner.errcode) {
		parser->scanner.errcode = HYSS_JSON_ERROR_SYNTAX;
	}
}

HYSS_JSON_API hyss_json_error_code hyss_json_parser_error_code(const hyss_json_parser *parser)
{
	return parser->scanner.errcode;
}

static const hyss_json_parser_methods default_parser_methods =
{
	hyss_json_parser_array_create,
	hyss_json_parser_array_append,
	NULL,
	NULL,
	hyss_json_parser_object_create,
	hyss_json_parser_object_update,
	NULL,
	NULL,
};

HYSS_JSON_API void hyss_json_parser_init_ex(hyss_json_parser *parser,
		zval *return_value,
		char *str,
		size_t str_len,
		int options,
		int max_depth,
		const hyss_json_parser_methods *parser_methods)
{
	memset(parser, 0, sizeof(hyss_json_parser));
	hyss_json_scanner_init(&parser->scanner, str, str_len, options);
	parser->depth = 1;
	parser->max_depth = max_depth;
	parser->return_value = return_value;
	memcpy(&parser->methods, parser_methods, sizeof(hyss_json_parser_methods));
}

HYSS_JSON_API void hyss_json_parser_init(hyss_json_parser *parser,
		zval *return_value,
		char *str,
		size_t str_len,
		int options,
		int max_depth)
{
	hyss_json_parser_init_ex(
			parser,
			return_value,
			str,
			str_len,
			options,
			max_depth,
			&default_parser_methods);
}

HYSS_JSON_API int hyss_json_parse(hyss_json_parser *parser)
{
	return hyss_json_yyparse(parser);
}
