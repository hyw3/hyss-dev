/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "extslib/standard/html.h"
#include "gear_smart_str.h"
#include "hyss_json.h"
#include "hyss_json_encoder.h"
#include "hyss_json_parser.h"
#include <gear_exceptions.h>

static HYSS_MINFO_FUNCTION(json);
static HYSS_FUNCTION(json_encode);
static HYSS_FUNCTION(json_decode);
static HYSS_FUNCTION(json_last_error);
static HYSS_FUNCTION(json_last_error_msg);

HYSS_JSON_API gear_class_entry *hyss_json_serializable_ce;
HYSS_JSON_API gear_class_entry *hyss_json_exception_ce;

HYSS_JSON_API GEAR_DECLARE_CAPI_GLOBALS(json)

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_json_encode, 0, 0, 1)
	GEAR_ARG_INFO(0, value)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, depth)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_json_decode, 0, 0, 1)
	GEAR_ARG_INFO(0, json)
	GEAR_ARG_INFO(0, assoc)
	GEAR_ARG_INFO(0, depth)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_json_last_error, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_json_last_error_msg, 0)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ json_functions[] */
static const gear_function_entry json_functions[] = {
	HYSS_FE(json_encode, arginfo_json_encode)
	HYSS_FE(json_decode, arginfo_json_decode)
	HYSS_FE(json_last_error, arginfo_json_last_error)
	HYSS_FE(json_last_error_msg, arginfo_json_last_error_msg)
	HYSS_FE_END
};
/* }}} */

/* {{{ JsonSerializable methods */
GEAR_BEGIN_ARG_INFO(json_serialize_arginfo, 0)
	/* No arguments */
GEAR_END_ARG_INFO();

static const gear_function_entry json_serializable_interface[] = {
	HYSS_ABSTRACT_ME(JsonSerializable, jsonSerialize, json_serialize_arginfo)
	HYSS_FE_END
};
/* }}} */

/* Register constant for options and errors */
#define HYSS_JSON_REGISTER_CONSTANT(_name, _value) \
	REGISTER_LONG_CONSTANT(_name,  _value, CONST_CS | CONST_PERSISTENT);

/* {{{ MINIT */
static HYSS_MINIT_FUNCTION(json)
{
	gear_class_entry ce;

	INIT_CLASS_ENTRY(ce, "JsonSerializable", json_serializable_interface);
	hyss_json_serializable_ce = gear_register_internal_interface(&ce);

	INIT_CLASS_ENTRY(ce, "JsonException", NULL);
	hyss_json_exception_ce = gear_register_internal_class_ex(&ce, gear_ce_exception);

	/* options for json_encode */
	HYSS_JSON_REGISTER_CONSTANT("JSON_HEX_TAG",  HYSS_JSON_HEX_TAG);
	HYSS_JSON_REGISTER_CONSTANT("JSON_HEX_AMP",  HYSS_JSON_HEX_AMP);
	HYSS_JSON_REGISTER_CONSTANT("JSON_HEX_APOS", HYSS_JSON_HEX_APOS);
	HYSS_JSON_REGISTER_CONSTANT("JSON_HEX_QUOT", HYSS_JSON_HEX_QUOT);
	HYSS_JSON_REGISTER_CONSTANT("JSON_FORCE_OBJECT", HYSS_JSON_FORCE_OBJECT);
	HYSS_JSON_REGISTER_CONSTANT("JSON_NUMERIC_CHECK", HYSS_JSON_NUMERIC_CHECK);
	HYSS_JSON_REGISTER_CONSTANT("JSON_UNESCAPED_SLASHES", HYSS_JSON_UNESCAPED_SLASHES);
	HYSS_JSON_REGISTER_CONSTANT("JSON_PRETTY_PRINT", HYSS_JSON_PRETTY_PRINT);
	HYSS_JSON_REGISTER_CONSTANT("JSON_UNESCAPED_UNICODE", HYSS_JSON_UNESCAPED_UNICODE);
	HYSS_JSON_REGISTER_CONSTANT("JSON_PARTIAL_OUTPUT_ON_ERROR", HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR);
	HYSS_JSON_REGISTER_CONSTANT("JSON_PRESERVE_ZERO_FRACTION", HYSS_JSON_PRESERVE_ZERO_FRACTION);
	HYSS_JSON_REGISTER_CONSTANT("JSON_UNESCAPED_LINE_TERMINATORS", HYSS_JSON_UNESCAPED_LINE_TERMINATORS);

	/* options for json_decode */
	HYSS_JSON_REGISTER_CONSTANT("JSON_OBJECT_AS_ARRAY", HYSS_JSON_OBJECT_AS_ARRAY);
	HYSS_JSON_REGISTER_CONSTANT("JSON_BIGINT_AS_STRING", HYSS_JSON_BIGINT_AS_STRING);

	/* common options for json_decode and json_encode */
	HYSS_JSON_REGISTER_CONSTANT("JSON_INVALID_UTF8_IGNORE", HYSS_JSON_INVALID_UTF8_IGNORE);
	HYSS_JSON_REGISTER_CONSTANT("JSON_INVALID_UTF8_SUBSTITUTE", HYSS_JSON_INVALID_UTF8_SUBSTITUTE);
	HYSS_JSON_REGISTER_CONSTANT("JSON_THROW_ON_ERROR", HYSS_JSON_THROW_ON_ERROR);

	/* json error constants */
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_NONE", HYSS_JSON_ERROR_NONE);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_DEPTH", HYSS_JSON_ERROR_DEPTH);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_STATE_MISMATCH", HYSS_JSON_ERROR_STATE_MISMATCH);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_CTRL_CHAR", HYSS_JSON_ERROR_CTRL_CHAR);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_SYNTAX", HYSS_JSON_ERROR_SYNTAX);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_UTF8", HYSS_JSON_ERROR_UTF8);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_RECURSION", HYSS_JSON_ERROR_RECURSION);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_INF_OR_NAN", HYSS_JSON_ERROR_INF_OR_NAN);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_UNSUPPORTED_TYPE", HYSS_JSON_ERROR_UNSUPPORTED_TYPE);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_INVALID_PROPERTY_NAME", HYSS_JSON_ERROR_INVALID_PROPERTY_NAME);
	HYSS_JSON_REGISTER_CONSTANT("JSON_ERROR_UTF16", HYSS_JSON_ERROR_UTF16);

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_GINIT_FUNCTION
*/
static HYSS_GINIT_FUNCTION(json)
{
#if defined(COMPILE_DL_JSON) && defined(ZTS)
	GEAR_PBCLS_CACHE_UPDATE();
#endif
	json_globals->encoder_depth = 0;
	json_globals->error_code = 0;
	json_globals->encode_max_depth = HYSS_JSON_PARSER_DEFAULT_DEPTH;
}
/* }}} */


/* {{{ json_capi_entry
 */
gear_capi_entry json_capi_entry = {
	STANDARD_CAPI_HEADER,
	"json",
	json_functions,
	HYSS_MINIT(json),
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(json),
	HYSS_JSON_VERSION,
	HYSS_CAPI_GLOBALS(json),
	HYSS_GINIT(json),
	NULL,
	NULL,
	STANDARD_CAPI_PROPERTIES_EX
};
/* }}} */

#ifdef COMPILE_DL_JSON
#ifdef ZTS
GEAR_PBCLS_CACHE_DEFINE()
#endif
GEAR_GET_CAPI(json)
#endif

/* {{{ HYSS_MINFO_FUNCTION
 */
static HYSS_MINFO_FUNCTION(json)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "json support", "enabled");
	hyss_info_print_table_row(2, "json version", HYSS_JSON_VERSION);
	hyss_info_print_table_end();
}
/* }}} */

HYSS_JSON_API int hyss_json_encode_ex(smart_str *buf, zval *val, int options, gear_long depth) /* {{{ */
{
	hyss_json_encoder encoder;
	int return_code;

	hyss_json_encode_init(&encoder);
	encoder.max_depth = depth;

	return_code = hyss_json_encode_zval(buf, val, options, &encoder);
	JSON_G(error_code) = encoder.error_code;

	return return_code;
}
/* }}} */

HYSS_JSON_API int hyss_json_encode(smart_str *buf, zval *val, int options) /* {{{ */
{
	return hyss_json_encode_ex(buf, val, options, JSON_G(encode_max_depth));
}
/* }}} */

static const char *hyss_json_get_error_msg(hyss_json_error_code error_code) /* {{{ */
{
	switch(error_code) {
		case HYSS_JSON_ERROR_NONE:
			return "No error";
		case HYSS_JSON_ERROR_DEPTH:
			return "Maximum stack depth exceeded";
		case HYSS_JSON_ERROR_STATE_MISMATCH:
			return "State mismatch (invalid or malformed JSON)";
		case HYSS_JSON_ERROR_CTRL_CHAR:
			return "Control character error, possibly incorrectly encoded";
		case HYSS_JSON_ERROR_SYNTAX:
			return "Syntax error";
		case HYSS_JSON_ERROR_UTF8:
			return "Malformed UTF-8 characters, possibly incorrectly encoded";
		case HYSS_JSON_ERROR_RECURSION:
			return "Recursion detected";
		case HYSS_JSON_ERROR_INF_OR_NAN:
			return "Inf and NaN cannot be JSON encoded";
		case HYSS_JSON_ERROR_UNSUPPORTED_TYPE:
			return "Type is not supported";
		case HYSS_JSON_ERROR_INVALID_PROPERTY_NAME:
			return "The decoded property name is invalid";
		case HYSS_JSON_ERROR_UTF16:
			return "Single unpaired UTF-16 surrogate in unicode escape";
		default:
			return "Unknown error";
	}
}
/* }}} */

HYSS_JSON_API int hyss_json_decode_ex(zval *return_value, char *str, size_t str_len, gear_long options, gear_long depth) /* {{{ */
{
	hyss_json_parser parser;

	hyss_json_parser_init(&parser, return_value, str, str_len, (int)options, (int)depth);

	if (hyss_json_yyparse(&parser)) {
		hyss_json_error_code error_code = hyss_json_parser_error_code(&parser);
		if (!(options & HYSS_JSON_THROW_ON_ERROR)) {
			JSON_G(error_code) = error_code;
		} else {
			gear_throw_exception(hyss_json_exception_ce, hyss_json_get_error_msg(error_code), error_code);
		}
		RETVAL_NULL();
		return FAILURE;
	}

	return SUCCESS;
}
/* }}} */

/* {{{ proto string json_encode(mixed data [, int options[, int depth]])
   Returns the JSON representation of a value */
static HYSS_FUNCTION(json_encode)
{
	zval *parameter;
	hyss_json_encoder encoder;
	smart_str buf = {0};
	gear_long options = 0;
	gear_long depth = HYSS_JSON_PARSER_DEFAULT_DEPTH;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_ZVAL(parameter)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(options)
		Z_PARAM_LONG(depth)
	GEAR_PARSE_PARAMETERS_END();

	hyss_json_encode_init(&encoder);
	encoder.max_depth = (int)depth;
	hyss_json_encode_zval(&buf, parameter, (int)options, &encoder);

	if (!(options & HYSS_JSON_THROW_ON_ERROR) || (options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR)) {
		JSON_G(error_code) = encoder.error_code;
		if (encoder.error_code != HYSS_JSON_ERROR_NONE && !(options & HYSS_JSON_PARTIAL_OUTPUT_ON_ERROR)) {
			smart_str_free(&buf);
			RETURN_FALSE;
		}
	} else {
		if (encoder.error_code != HYSS_JSON_ERROR_NONE) {
			smart_str_free(&buf);
			gear_throw_exception(hyss_json_exception_ce, hyss_json_get_error_msg(encoder.error_code), encoder.error_code);
			RETURN_FALSE;
		}
	}

	smart_str_0(&buf); /* copy? */
	if (buf.s) {
		RETURN_NEW_STR(buf.s);
	}
	RETURN_EMPTY_STRING();
}
/* }}} */

/* {{{ proto mixed json_decode(string json [, bool assoc [, int depth]])
   Decodes the JSON representation into a HYSS value */
static HYSS_FUNCTION(json_decode)
{
	char *str;
	size_t str_len;
	gear_bool assoc = 0; /* return JS objects as HYSS objects by default */
	gear_bool assoc_null = 1;
	gear_long depth = HYSS_JSON_PARSER_DEFAULT_DEPTH;
	gear_long options = 0;

	GEAR_PARSE_PARAMETERS_START(1, 4)
		Z_PARAM_STRING(str, str_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_BOOL_EX(assoc, assoc_null, 1, 0)
		Z_PARAM_LONG(depth)
		Z_PARAM_LONG(options)
	GEAR_PARSE_PARAMETERS_END();

	if (!(options & HYSS_JSON_THROW_ON_ERROR)) {
		JSON_G(error_code) = HYSS_JSON_ERROR_NONE;
	}

	if (!str_len) {
		if (!(options & HYSS_JSON_THROW_ON_ERROR)) {
			JSON_G(error_code) = HYSS_JSON_ERROR_SYNTAX;
		} else {
			gear_throw_exception(hyss_json_exception_ce, hyss_json_get_error_msg(HYSS_JSON_ERROR_SYNTAX), HYSS_JSON_ERROR_SYNTAX);
		}
		RETURN_NULL();
	}

	if (depth <= 0) {
		hyss_error_docref(NULL, E_WARNING, "Depth must be greater than zero");
		RETURN_NULL();
	}

	if (depth > INT_MAX) {
		hyss_error_docref(NULL, E_WARNING, "Depth must be lower than %d", INT_MAX);
		RETURN_NULL();
	}

	/* For BC reasons, the bool $assoc overrides the long $options bit for HYSS_JSON_OBJECT_AS_ARRAY */
	if (!assoc_null) {
		if (assoc) {
			options |=  HYSS_JSON_OBJECT_AS_ARRAY;
		} else {
			options &= ~HYSS_JSON_OBJECT_AS_ARRAY;
		}
	}

	hyss_json_decode_ex(return_value, str, str_len, options, depth);
}
/* }}} */

/* {{{ proto int json_last_error()
   Returns the error code of the last json_encode() or json_decode() call. */
static HYSS_FUNCTION(json_last_error)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_LONG(JSON_G(error_code));
}
/* }}} */

/* {{{ proto string json_last_error_msg()
   Returns the error string of the last json_encode() or json_decode() call. */
static HYSS_FUNCTION(json_last_error_msg)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_STRING(hyss_json_get_error_msg(JSON_G(error_code)));
}
/* }}} */

