/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_HYSS_JSON_YY_HOME_HABIBI_HYSS_DEV_EXTSLIB_JSON_JSON_PARSER_TAB_H_INCLUDED
# define YY_HYSS_JSON_YY_HOME_HABIBI_HYSS_DEV_EXTSLIB_JSON_JSON_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int hyss_json_yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    HYSS_JSON_T_NUL = 258,
    HYSS_JSON_T_TRUE = 259,
    HYSS_JSON_T_FALSE = 260,
    HYSS_JSON_T_INT = 261,
    HYSS_JSON_T_DOUBLE = 262,
    HYSS_JSON_T_STRING = 263,
    HYSS_JSON_T_ESTRING = 264,
    HYSS_JSON_T_EOI = 265,
    HYSS_JSON_T_ERROR = 266
  };
#endif
/* Tokens.  */
#define HYSS_JSON_T_NUL 258
#define HYSS_JSON_T_TRUE 259
#define HYSS_JSON_T_FALSE 260
#define HYSS_JSON_T_INT 261
#define HYSS_JSON_T_DOUBLE 262
#define HYSS_JSON_T_STRING 263
#define HYSS_JSON_T_ESTRING 264
#define HYSS_JSON_T_EOI 265
#define HYSS_JSON_T_ERROR 266

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{

	zval value;
	struct {
		gear_string *key;
		zval val;
	} pair;


};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int hyss_json_yyparse (hyss_json_parser *parser);

#endif /* !YY_HYSS_JSON_YY_HOME_HABIBI_HYSS_DEV_EXTSLIB_JSON_JSON_PARSER_TAB_H_INCLUDED  */
