/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_JSON_ENCODER_H
#define	HYSS_JSON_ENCODER_H

#include "hyss.h"
#include "gear_smart_str.h"

typedef struct _hyss_json_encoder hyss_json_encoder;

struct _hyss_json_encoder {
	int depth;
	int max_depth;
	hyss_json_error_code error_code;
};

static inline void hyss_json_encode_init(hyss_json_encoder *encoder)
{
	memset(encoder, 0, sizeof(hyss_json_encoder));
}

int hyss_json_encode_zval(smart_str *buf, zval *val, int options, hyss_json_encoder *encoder);

#endif	/* HYSS_JSON_ENCODER_H */
