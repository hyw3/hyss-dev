/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_JSON_SCANNER_H
#define	HYSS_JSON_SCANNER_H

#include "hyss.h"
#include "hyss_json.h"

typedef unsigned char hyss_json_ctype;

typedef struct _hyss_json_scanner {
	hyss_json_ctype *cursor;         /* cursor position */
	hyss_json_ctype *token;          /* token position */
	hyss_json_ctype *limit;          /* the last read character + 1 position */
	hyss_json_ctype *marker;         /* marker position for backtracking */
	hyss_json_ctype *ctxmarker;      /* marker position for context backtracking */
	hyss_json_ctype *str_start;      /* start position of the string */
	hyss_json_ctype *pstr;           /* string pointer for escapes conversion */
	zval value;                     /* value */
	int str_esc;                    /* number of extra characters for escaping */
	int state;                      /* condition state */
	int options;                    /* options */
	hyss_json_error_code errcode;    /* error type if there is an error */
	int utf8_invalid;               /* whether utf8 is invalid */
	int utf8_invalid_count;         /* number of extra character for invalid utf8 */
} hyss_json_scanner;


void hyss_json_scanner_init(hyss_json_scanner *scanner, char *str, size_t str_len, int options);
int hyss_json_scan(hyss_json_scanner *s);

#endif	/* HYSS_JSON_SCANNER_H */
