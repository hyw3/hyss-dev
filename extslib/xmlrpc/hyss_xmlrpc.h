/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
  This file is part of, or distributed with, libXMLRPC - a C library for
  xml-encoded function calls.

  Author: Dan Libby (dan@libby.com)
  Epinions.com may be contacted at feedback@epinions-inc.com
*/

/*
  Copyright 2001 Epinions, Inc.

  Subject to the following 3 conditions, Epinions, Inc.  permits you, free
  of charge, to (a) use, copy, distribute, modify, perform and display this
  software and associated documentation files (the "Software"), and (b)
  permit others to whom the Software is furnished to do so as well.

  1) The above copyright notice and this permission notice shall be included
  without modification in all copies or substantial portions of the
  Software.

  2) THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OR CONDITION OF
  ANY KIND, EXPRESS, IMPLIED OR STATUTORY, INCLUDING WITHOUT LIMITATION ANY
  IMPLIED WARRANTIES OF ACCURACY, MERCHANTABILITY, FITNESS FOR A PARTICULAR
  PURPOSE OR NONINFRINGEMENT.

  3) IN NO EVENT SHALL EPINIONS, INC. BE LIABLE FOR ANY DIRECT, INDIRECT,
  SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES OR LOST PROFITS ARISING OUT
  OF OR IN CONNECTION WITH THE SOFTWARE (HOWEVER ARISING, INCLUDING
  NEGLIGENCE), EVEN IF EPINIONS, INC.  IS AWARE OF THE POSSIBILITY OF SUCH
  DAMAGES.

*/

/* auto-generated portions of this file are also subject to the hyss license */

#ifndef _HYSS_XMLRPC_H
#define _HYSS_XMLRPC_H

#if 1 /* HAVE_XMLRPC */

extern gear_capi_entry xmlrpc_capi_entry;
#define hyssext_xmlrpc_ptr &xmlrpc_capi_entry

#include "hyss_version.h"
#define HYSS_XMLRPC_VERSION HYSS_VERSION

HYSS_MINIT_FUNCTION(xmlrpc);
HYSS_MINFO_FUNCTION(xmlrpc);

HYSS_FUNCTION(xmlrpc_encode);
HYSS_FUNCTION(xmlrpc_decode);
HYSS_FUNCTION(xmlrpc_decode_request);
HYSS_FUNCTION(xmlrpc_encode_request);
HYSS_FUNCTION(xmlrpc_get_type);
HYSS_FUNCTION(xmlrpc_set_type);
HYSS_FUNCTION(xmlrpc_is_fault);
HYSS_FUNCTION(xmlrpc_server_create);
HYSS_FUNCTION(xmlrpc_server_destroy);
HYSS_FUNCTION(xmlrpc_server_register_method);
HYSS_FUNCTION(xmlrpc_server_call_method);
HYSS_FUNCTION(xmlrpc_parse_method_descriptions);
HYSS_FUNCTION(xmlrpc_server_add_introspection_data);
HYSS_FUNCTION(xmlrpc_server_register_introspection_callback);

#else

#define hyssext_xmlrpc_ptr NULL

#endif

#endif	/* _HYSS_XMLRPC_H */

