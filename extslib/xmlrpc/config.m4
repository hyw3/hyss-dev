dnl config.m4 for extension xmlrpc

sinclude(extslib/xmlrpc/libxmlrpc/acinclude.m4)
sinclude(extslib/xmlrpc/libxmlrpc/xmlrpc.m4)
sinclude(libxmlrpc/acinclude.m4)
sinclude(libxmlrpc/xmlrpc.m4)

HYSS_ARG_WITH(xmlrpc, for XMLRPC-EPI support,
[  --with-xmlrpc[=DIR]       Include XMLRPC-EPI support])

if test -z "$HYSS_LIBXML_DIR"; then
  HYSS_ARG_WITH(libxml-dir, libxml2 install dir,
  [  --with-libxml-dir=DIR   XMLRPC-EPI: libxml2 install prefix], no, no)
fi

HYSS_ARG_WITH(libexpat-dir, libexpat dir for XMLRPC-EPI,
[  --with-libexpat-dir=DIR XMLRPC-EPI: libexpat dir for XMLRPC-EPI (deprecated)],no,no)

HYSS_ARG_WITH(iconv-dir, iconv dir for XMLRPC-EPI,
[  --with-iconv-dir=DIR    XMLRPC-EPI: iconv dir for XMLRPC-EPI],no,no)

if test "$HYSS_XMLRPC" != "no"; then

  HYSS_ADD_EXTENSION_DEP(xmlrpc, libxml)
  HYSS_SUBST(XMLRPC_SHARED_LIBADD)
  AC_DEFINE(HAVE_XMLRPC,1,[ ])

  dnl
  dnl Default to libxml2 if --with-libexpat-dir is not used
  dnl
  if test "$HYSS_LIBEXPAT_DIR" = "no"; then

    if test "$HYSS_LIBXML" = "no"; then
      AC_MSG_ERROR([XML-RPC extension requires LIBXML extension, add --enable-libxml])
    fi

    HYSS_SETUP_LIBXML(XMLRPC_SHARED_LIBADD, [
      if test "$HYSS_XML" = "no"; then
        HYSS_ADD_SOURCES(extslib/xml, compat.c)
        HYSS_ADD_BUILD_DIR(extslib/xml)
      fi
    ], [
      AC_MSG_ERROR([libxml2 not found. Use --with-libxml-dir=<DIR>])
    ])
  else
    testval=no
    for i in $HYSS_LIBEXPAT_DIR $XMLRPC_DIR /usr/local /usr; do
      if test -f $i/$HYSS_LIBDIR/libexpat.a || test -f $i/$HYSS_LIBDIR/libexpat.$SHLIB_SUFFIX_NAME; then
        AC_DEFINE(HAVE_LIBEXPAT,1,[ ])
        HYSS_ADD_LIBRARY_WITH_PATH(expat, $i/$HYSS_LIBDIR, XMLRPC_SHARED_LIBADD)
        HYSS_ADD_INCLUDE($i/include)
        testval=yes
        break
      fi
    done

    if test "$testval" = "no"; then
      AC_MSG_ERROR([XML-RPC support requires libexpat. Use --with-libexpat-dir=<DIR> (deprecated!)])
    fi
  fi

  dnl if iconv is shared or missing then we should build iconv ourselves
  if test "$HYSS_ICONV_SHARED" = "yes" || test "$HYSS_ICONV" = "no"; then

    if test "$HYSS_ICONV_DIR" != "no"; then
      HYSS_ICONV=$HYSS_ICONV_DIR
    fi

    if test -z "$HYSS_ICONV" || test "$HYSS_ICONV" = "no"; then
      HYSS_ICONV=yes
    fi

    HYSS_SETUP_ICONV(XMLRPC_SHARED_LIBADD, [], [
      AC_MSG_ERROR([iconv not found, in order to build xmlrpc you need the iconv library])
    ])
  fi
fi

if test "$HYSS_XMLRPC" = "yes"; then
  XMLRPC_CHECKS
  HYSS_NEW_EXTENSION(xmlrpc,xmlrpc-epi-hyss.c libxmlrpc/base64.c \
          libxmlrpc/simplestring.c libxmlrpc/xml_to_dandarpc.c \
          libxmlrpc/xmlrpc_introspection.c libxmlrpc/encodings.c \
          libxmlrpc/system_methods.c libxmlrpc/xml_to_xmlrpc.c \
          libxmlrpc/queue.c libxmlrpc/xml_element.c libxmlrpc/xmlrpc.c \
          libxmlrpc/xml_to_soap.c,$ext_shared,,
          -I@ext_srcdir@/libxmlrpc -DVERSION="0.50")
  HYSS_ADD_BUILD_DIR($ext_builddir/libxmlrpc)
  XMLRPC_CAPI_TYPE=builtin
  AC_DEFINE(HAVE_XMLRPC_BUNDLED, 1, [ ])

elif test "$HYSS_XMLRPC" != "no"; then

  if test -r $HYSS_XMLRPC/include/xmlrpc.h; then
    XMLRPC_DIR=$HYSS_XMLRPC/include
  elif test -r $HYSS_XMLRPC/include/xmlrpc-epi/xmlrpc.h; then
dnl some xmlrpc-epi header files have generic file names like
dnl queue.h or base64.h. Distributions have to create dir
dnl for xmlrpc-epi because of this.
    XMLRPC_DIR=$HYSS_XMLRPC/include/xmlrpc-epi
  else
    AC_MSG_CHECKING(for XMLRPC-EPI in default path)
    for i in /usr/local /usr; do
      if test -r $i/include/xmlrpc.h; then
        XMLRPC_DIR=$i/include
        AC_MSG_RESULT(found in $i)
        break
      fi
    done
  fi

  if test -z "$XMLRPC_DIR"; then
    AC_MSG_RESULT(not found)
    AC_MSG_ERROR(Please reinstall the XMLRPC-EPI distribution)
  fi

  HYSS_ADD_INCLUDE($XMLRPC_DIR)
  HYSS_ADD_LIBRARY_WITH_PATH(xmlrpc, $XMLRPC_DIR/$HYSS_LIBDIR, XMLRPC_SHARED_LIBADD)
  HYSS_NEW_EXTENSION(xmlrpc,xmlrpc-epi-hyss.c, $ext_shared)
  XMLRPC_CAPI_TYPE=external
fi
