/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define IS_EXT_CAPI

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>

#if HAVE_PSPELL

/* this will enforce compatibility in .12 version (broken after .11.2) */
#define USE_ORIGINAL_MANAGER_FUNCS

#include "hyss_pspell.h"
#include <pspell.h>
#include "extslib/standard/info.h"

#define PSPELL_FAST 1L
#define PSPELL_NORMAL 2L
#define PSPELL_BAD_SPELLERS 3L
#define PSPELL_SPEED_MASK_INTERNAL 3L
#define PSPELL_RUN_TOGETHER 8L

/* Largest ignored word can be 999 characters (this seems sane enough),
 * and it takes 3 bytes to represent that (see pspell_config_ignore)
 */
#define PSPELL_LARGEST_WORD 3

static HYSS_MINIT_FUNCTION(pspell);
static HYSS_MINFO_FUNCTION(pspell);
static HYSS_FUNCTION(pspell_new);
static HYSS_FUNCTION(pspell_new_personal);
static HYSS_FUNCTION(pspell_new_config);
static HYSS_FUNCTION(pspell_check);
static HYSS_FUNCTION(pspell_suggest);
static HYSS_FUNCTION(pspell_store_replacement);
static HYSS_FUNCTION(pspell_add_to_personal);
static HYSS_FUNCTION(pspell_add_to_session);
static HYSS_FUNCTION(pspell_clear_session);
static HYSS_FUNCTION(pspell_save_wordlist);
static HYSS_FUNCTION(pspell_config_create);
static HYSS_FUNCTION(pspell_config_runtogether);
static HYSS_FUNCTION(pspell_config_mode);
static HYSS_FUNCTION(pspell_config_ignore);
static HYSS_FUNCTION(pspell_config_personal);
static HYSS_FUNCTION(pspell_config_dict_dir);
static HYSS_FUNCTION(pspell_config_data_dir);
static HYSS_FUNCTION(pspell_config_repl);
static HYSS_FUNCTION(pspell_config_save_repl);

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_new, 0, 0, 1)
	GEAR_ARG_INFO(0, language)
	GEAR_ARG_INFO(0, spelling)
	GEAR_ARG_INFO(0, jargon)
	GEAR_ARG_INFO(0, encoding)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_new_personal, 0, 0, 2)
	GEAR_ARG_INFO(0, personal)
	GEAR_ARG_INFO(0, language)
	GEAR_ARG_INFO(0, spelling)
	GEAR_ARG_INFO(0, jargon)
	GEAR_ARG_INFO(0, encoding)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_new_config, 0, 0, 1)
	GEAR_ARG_INFO(0, config)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_check, 0, 0, 2)
	GEAR_ARG_INFO(0, pspell)
	GEAR_ARG_INFO(0, word)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_suggest, 0, 0, 2)
	GEAR_ARG_INFO(0, pspell)
	GEAR_ARG_INFO(0, word)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_store_replacement, 0, 0, 3)
	GEAR_ARG_INFO(0, pspell)
	GEAR_ARG_INFO(0, misspell)
	GEAR_ARG_INFO(0, correct)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_add_to_personal, 0, 0, 2)
	GEAR_ARG_INFO(0, pspell)
	GEAR_ARG_INFO(0, word)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_add_to_session, 0, 0, 2)
	GEAR_ARG_INFO(0, pspell)
	GEAR_ARG_INFO(0, word)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_clear_session, 0, 0, 1)
	GEAR_ARG_INFO(0, pspell)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_save_wordlist, 0, 0, 1)
	GEAR_ARG_INFO(0, pspell)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_create, 0, 0, 1)
	GEAR_ARG_INFO(0, language)
	GEAR_ARG_INFO(0, spelling)
	GEAR_ARG_INFO(0, jargon)
	GEAR_ARG_INFO(0, encoding)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_runtogether, 0, 0, 2)
	GEAR_ARG_INFO(0, conf)
	GEAR_ARG_INFO(0, runtogether)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_mode, 0, 0, 2)
	GEAR_ARG_INFO(0, conf)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_ignore, 0, 0, 2)
	GEAR_ARG_INFO(0, conf)
	GEAR_ARG_INFO(0, ignore)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_personal, 0, 0, 2)
	GEAR_ARG_INFO(0, conf)
	GEAR_ARG_INFO(0, personal)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_dict_dir, 0, 0, 2)
	GEAR_ARG_INFO(0, conf)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_data_dir, 0, 0, 2)
	GEAR_ARG_INFO(0, conf)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_repl, 0, 0, 2)
	GEAR_ARG_INFO(0, conf)
	GEAR_ARG_INFO(0, repl)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_pspell_config_save_repl, 0, 0, 2)
	GEAR_ARG_INFO(0, conf)
	GEAR_ARG_INFO(0, save)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ pspell_functions[]
 */
static const gear_function_entry pspell_functions[] = {
	HYSS_FE(pspell_new,					arginfo_pspell_new)
	HYSS_FE(pspell_new_personal,			arginfo_pspell_new_personal)
	HYSS_FE(pspell_new_config,			arginfo_pspell_new_config)
	HYSS_FE(pspell_check,				arginfo_pspell_check)
	HYSS_FE(pspell_suggest,				arginfo_pspell_suggest)
	HYSS_FE(pspell_store_replacement,	arginfo_pspell_store_replacement)
	HYSS_FE(pspell_add_to_personal,		arginfo_pspell_add_to_personal)
	HYSS_FE(pspell_add_to_session,		arginfo_pspell_add_to_session)
	HYSS_FE(pspell_clear_session,		arginfo_pspell_clear_session)
	HYSS_FE(pspell_save_wordlist,		arginfo_pspell_save_wordlist)
	HYSS_FE(pspell_config_create,		arginfo_pspell_config_create)
	HYSS_FE(pspell_config_runtogether,	arginfo_pspell_config_runtogether)
	HYSS_FE(pspell_config_mode,			arginfo_pspell_config_mode)
	HYSS_FE(pspell_config_ignore,		arginfo_pspell_config_ignore)
	HYSS_FE(pspell_config_personal,		arginfo_pspell_config_personal)
	HYSS_FE(pspell_config_dict_dir,		arginfo_pspell_config_dict_dir)
	HYSS_FE(pspell_config_data_dir,		arginfo_pspell_config_data_dir)
	HYSS_FE(pspell_config_repl,			arginfo_pspell_config_repl)
	HYSS_FE(pspell_config_save_repl,		arginfo_pspell_config_save_repl)
	HYSS_FE_END
};
/* }}} */

static int le_pspell, le_pspell_config;

gear_capi_entry pspell_capi_entry = {
    STANDARD_CAPI_HEADER,
	"pspell", pspell_functions, HYSS_MINIT(pspell), NULL, NULL, NULL, HYSS_MINFO(pspell), HYSS_PSPELL_VERSION, STANDARD_CAPI_PROPERTIES
};

#ifdef COMPILE_DL_PSPELL
GEAR_GET_CAPI(pspell)
#endif

static void hyss_pspell_close(gear_resource *rsrc)
{
	PspellManager *manager = (PspellManager *)rsrc->ptr;

	delete_pspell_manager(manager);
}

static void hyss_pspell_close_config(gear_resource *rsrc)
{
	PspellConfig *config = (PspellConfig *)rsrc->ptr;

	delete_pspell_config(config);
}

#define PSPELL_FETCH_CONFIG  do { \
	zval *res = gear_hash_index_find(&EG(regular_list), conf); \
	if (res == NULL || Z_RES_P(res)->type != le_pspell_config) { \
		hyss_error_docref(NULL, E_WARNING, GEAR_LONG_FMT " is not a PSPELL config index", conf); \
		RETURN_FALSE; \
	} \
	config = (PspellConfig *)Z_RES_P(res)->ptr; \
} while (0)

#define PSPELL_FETCH_MANAGER do { \
	zval *res = gear_hash_index_find(&EG(regular_list), scin); \
	if (res == NULL || Z_RES_P(res)->type != le_pspell) { \
		hyss_error_docref(NULL, E_WARNING, GEAR_LONG_FMT " is not a PSPELL result index", scin); \
		RETURN_FALSE; \
	} \
	manager = (PspellManager *)Z_RES_P(res)->ptr; \
} while (0);

/* {{{ HYSS_MINIT_FUNCTION
 */
static HYSS_MINIT_FUNCTION(pspell)
{
	REGISTER_LONG_CONSTANT("PSPELL_FAST", PSPELL_FAST, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("PSPELL_NORMAL", PSPELL_NORMAL, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("PSPELL_BAD_SPELLERS", PSPELL_BAD_SPELLERS, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("PSPELL_RUN_TOGETHER", PSPELL_RUN_TOGETHER, CONST_PERSISTENT | CONST_CS);
	le_pspell = gear_register_list_destructors_ex(hyss_pspell_close, NULL, "pspell", capi_number);
	le_pspell_config = gear_register_list_destructors_ex(hyss_pspell_close_config, NULL, "pspell config", capi_number);
	return SUCCESS;
}
/* }}} */

/* {{{ proto int pspell_new(string language [, string spelling [, string jargon [, string encoding [, int mode]]]])
   Load a dictionary */
static HYSS_FUNCTION(pspell_new)
{
	char *language, *spelling = NULL, *jargon = NULL, *encoding = NULL;
	size_t language_len, spelling_len = 0, jargon_len = 0, encoding_len = 0;
	gear_long mode = Z_L(0),  speed = Z_L(0);
	int argc = GEAR_NUM_ARGS();
	zval *ind;

#ifdef HYSS_WIN32
	TCHAR aspell_dir[200];
	TCHAR data_dir[220];
	TCHAR dict_dir[220];
	HKEY hkey;
	DWORD dwType,dwLen;
#endif

	PspellCanHaveError *ret;
	PspellManager *manager;
	PspellConfig *config;

	if (gear_parse_parameters(argc, "s|sssl", &language, &language_len, &spelling, &spelling_len,
		&jargon, &jargon_len, &encoding, &encoding_len, &mode) == FAILURE) {
		return;
	}

	config = new_pspell_config();

#ifdef HYSS_WIN32
	/* If aspell was installed using installer, we should have a key
	 * pointing to the location of the dictionaries
	 */
	if (0 == RegOpenKey(HKEY_LOCAL_MACHINE, "Software\\Aspell", &hkey)) {
		LONG result;
		dwLen = sizeof(aspell_dir) - 1;
		result = RegQueryValueEx(hkey, "", NULL, &dwType, (LPBYTE)&aspell_dir, &dwLen);
		RegCloseKey(hkey);
		if (result == ERROR_SUCCESS) {
			strlcpy(data_dir, aspell_dir, sizeof(data_dir));
			strlcat(data_dir, "\\data", sizeof(data_dir));
			strlcpy(dict_dir, aspell_dir, sizeof(dict_dir));
			strlcat(dict_dir, "\\dict", sizeof(dict_dir));

			pspell_config_replace(config, "data-dir", data_dir);
			pspell_config_replace(config, "dict-dir", dict_dir);
		}
	}
#endif

	pspell_config_replace(config, "language-tag", language);

	if (spelling_len) {
		pspell_config_replace(config, "spelling", spelling);
	}

	if (jargon_len) {
		pspell_config_replace(config, "jargon", jargon);
	}

	if (encoding_len) {
		pspell_config_replace(config, "encoding", encoding);
	}

	if (argc > 4) {
		speed = mode & PSPELL_SPEED_MASK_INTERNAL;

		/* First check what mode we want (how many suggestions) */
		if (speed == PSPELL_FAST) {
			pspell_config_replace(config, "sug-mode", "fast");
		} else if (speed == PSPELL_NORMAL) {
			pspell_config_replace(config, "sug-mode", "normal");
		} else if (speed == PSPELL_BAD_SPELLERS) {
			pspell_config_replace(config, "sug-mode", "bad-spellers");
		}

		/* Then we see if run-together words should be treated as valid components */
		if (mode & PSPELL_RUN_TOGETHER) {
			pspell_config_replace(config, "run-together", "true");
		}
	}

	ret = new_pspell_manager(config);
	delete_pspell_config(config);

	if (pspell_error_number(ret) != 0) {
		hyss_error_docref(NULL, E_WARNING, "PSPELL couldn't open the dictionary. reason: %s", pspell_error_message(ret));
		delete_pspell_can_have_error(ret);
		RETURN_FALSE;
	}

	manager = to_pspell_manager(ret);
	ind = gear_list_insert(manager, le_pspell);
	RETURN_LONG(Z_RES_HANDLE_P(ind));
}
/* }}} */

/* {{{ proto int pspell_new_personal(string personal, string language [, string spelling [, string jargon [, string encoding [, int mode]]]])
   Load a dictionary with a personal wordlist*/
static HYSS_FUNCTION(pspell_new_personal)
{
	char *personal, *language, *spelling = NULL, *jargon = NULL, *encoding = NULL;
	size_t personal_len, language_len, spelling_len = 0, jargon_len = 0, encoding_len = 0;
	gear_long mode = Z_L(0),  speed = Z_L(0);
	int argc = GEAR_NUM_ARGS();
	zval *ind;

#ifdef HYSS_WIN32
	TCHAR aspell_dir[200];
	TCHAR data_dir[220];
	TCHAR dict_dir[220];
	HKEY hkey;
	DWORD dwType,dwLen;
#endif

	PspellCanHaveError *ret;
	PspellManager *manager;
	PspellConfig *config;

	if (gear_parse_parameters(argc, "ps|sssl", &personal, &personal_len, &language, &language_len,
		&spelling, &spelling_len, &jargon, &jargon_len, &encoding, &encoding_len, &mode) == FAILURE) {
		return;
	}

	config = new_pspell_config();

#ifdef HYSS_WIN32
	/* If aspell was installed using installer, we should have a key
	 * pointing to the location of the dictionaries
	 */
	if (0 == RegOpenKey(HKEY_LOCAL_MACHINE, "Software\\Aspell", &hkey)) {
		LONG result;
		dwLen = sizeof(aspell_dir) - 1;
		result = RegQueryValueEx(hkey, "", NULL, &dwType, (LPBYTE)&aspell_dir, &dwLen);
		RegCloseKey(hkey);
		if (result == ERROR_SUCCESS) {
			strlcpy(data_dir, aspell_dir, sizeof(data_dir));
			strlcat(data_dir, "\\data", sizeof(data_dir));
			strlcpy(dict_dir, aspell_dir, sizeof(dict_dir));
			strlcat(dict_dir, "\\dict", sizeof(dict_dir));

			pspell_config_replace(config, "data-dir", data_dir);
			pspell_config_replace(config, "dict-dir", dict_dir);
		}
	}
#endif

	if (hyss_check_open_basedir(personal)) {
		delete_pspell_config(config);
		RETURN_FALSE;
	}

	pspell_config_replace(config, "personal", personal);
	pspell_config_replace(config, "save-repl", "false");

	pspell_config_replace(config, "language-tag", language);

	if (spelling_len) {
		pspell_config_replace(config, "spelling", spelling);
	}

	if (jargon_len) {
		pspell_config_replace(config, "jargon", jargon);
	}

	if (encoding_len) {
		pspell_config_replace(config, "encoding", encoding);
	}

	if (argc > 5) {
		speed = mode & PSPELL_SPEED_MASK_INTERNAL;

		/* First check what mode we want (how many suggestions) */
		if (speed == PSPELL_FAST) {
			pspell_config_replace(config, "sug-mode", "fast");
		} else if (speed == PSPELL_NORMAL) {
			pspell_config_replace(config, "sug-mode", "normal");
		} else if (speed == PSPELL_BAD_SPELLERS) {
			pspell_config_replace(config, "sug-mode", "bad-spellers");
		}

		/* Then we see if run-together words should be treated as valid components */
		if (mode & PSPELL_RUN_TOGETHER) {
			pspell_config_replace(config, "run-together", "true");
		}
	}

	ret = new_pspell_manager(config);
	delete_pspell_config(config);

	if (pspell_error_number(ret) != 0) {
		hyss_error_docref(NULL, E_WARNING, "PSPELL couldn't open the dictionary. reason: %s", pspell_error_message(ret));
		delete_pspell_can_have_error(ret);
		RETURN_FALSE;
	}

	manager = to_pspell_manager(ret);
	ind = gear_list_insert(manager, le_pspell);
	RETURN_LONG(Z_RES_HANDLE_P(ind));
}
/* }}} */

/* {{{ proto int pspell_new_config(int config)
   Load a dictionary based on the given config */
static HYSS_FUNCTION(pspell_new_config)
{
	gear_long conf;
	zval *ind;
	PspellCanHaveError *ret;
	PspellManager *manager;
	PspellConfig *config;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &conf) == FAILURE) {
		return;
	}

	PSPELL_FETCH_CONFIG;

	ret = new_pspell_manager(config);

	if (pspell_error_number(ret) != 0) {
		hyss_error_docref(NULL, E_WARNING, "PSPELL couldn't open the dictionary. reason: %s", pspell_error_message(ret));
		delete_pspell_can_have_error(ret);
		RETURN_FALSE;
	}

	manager = to_pspell_manager(ret);
	ind = gear_list_insert(manager, le_pspell);
	RETURN_LONG(Z_RES_HANDLE_P(ind));
}
/* }}} */

/* {{{ proto bool pspell_check(int pspell, string word)
   Returns true if word is valid */
static HYSS_FUNCTION(pspell_check)
{
	size_t word_len;
	gear_long scin;
	char *word;
	PspellManager *manager;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ls", &scin, &word, &word_len) == FAILURE) {
		return;
	}

	PSPELL_FETCH_MANAGER;

	if (pspell_manager_check(manager, word)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto array pspell_suggest(int pspell, string word)
   Returns array of suggestions */
static HYSS_FUNCTION(pspell_suggest)
{
	gear_long scin;
	char *word;
	size_t word_len;
	PspellManager *manager;
	const PspellWordList *wl;
	const char *sug;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ls", &scin, &word, &word_len) == FAILURE) {
		return;
	}

	PSPELL_FETCH_MANAGER;

	array_init(return_value);

	wl = pspell_manager_suggest(manager, word);
	if (wl) {
		PspellStringEmulation *els = pspell_word_list_elements(wl);
		while ((sug = pspell_string_emulation_next(els)) != 0) {
			add_next_index_string(return_value,(char *)sug);
		}
		delete_pspell_string_emulation(els);
	} else {
		hyss_error_docref(NULL, E_WARNING, "PSPELL had a problem. details: %s", pspell_manager_error_message(manager));
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool pspell_store_replacement(int pspell, string misspell, string correct)
   Notify the dictionary of a user-selected replacement */
static HYSS_FUNCTION(pspell_store_replacement)
{
	size_t miss_len, corr_len;
	gear_long scin;
	char *miss, *corr;
	PspellManager *manager;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lss", &scin, &miss, &miss_len, &corr, &corr_len) == FAILURE) {
		return;
	}

	PSPELL_FETCH_MANAGER;

	pspell_manager_store_replacement(manager, miss, corr);
	if (pspell_manager_error_number(manager) == 0) {
		RETURN_TRUE;
	} else {
		hyss_error_docref(NULL, E_WARNING, "pspell_store_replacement() gave error: %s", pspell_manager_error_message(manager));
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool pspell_add_to_personal(int pspell, string word)
   Adds a word to a personal list */
static HYSS_FUNCTION(pspell_add_to_personal)
{
	size_t word_len;
	gear_long scin;
	char *word;
	PspellManager *manager;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ls", &scin, &word, &word_len) == FAILURE) {
		return;
	}

	PSPELL_FETCH_MANAGER;

	/*If the word is empty, we have to return; otherwise we'll segfault! ouch!*/
	if (word_len == 0) {
		RETURN_FALSE;
	}

	pspell_manager_add_to_personal(manager, word);
	if (pspell_manager_error_number(manager) == 0) {
		RETURN_TRUE;
	} else {
		hyss_error_docref(NULL, E_WARNING, "pspell_add_to_personal() gave error: %s", pspell_manager_error_message(manager));
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool pspell_add_to_session(int pspell, string word)
   Adds a word to the current session */
static HYSS_FUNCTION(pspell_add_to_session)
{
	size_t word_len;
	gear_long scin;
	char *word;
	PspellManager *manager;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ls", &scin, &word, &word_len) == FAILURE) {
		return;
	}

	PSPELL_FETCH_MANAGER;

	/*If the word is empty, we have to return; otherwise we'll segfault! ouch!*/
	if (word_len == 0) {
		RETURN_FALSE;
	}

	pspell_manager_add_to_session(manager, word);
	if (pspell_manager_error_number(manager) == 0) {
		RETURN_TRUE;
	} else {
		hyss_error_docref(NULL, E_WARNING, "pspell_add_to_session() gave error: %s", pspell_manager_error_message(manager));
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool pspell_clear_session(int pspell)
   Clears the current session */
static HYSS_FUNCTION(pspell_clear_session)
{
	gear_long scin;
	PspellManager *manager;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &scin) == FAILURE) {
		return;
	}

	PSPELL_FETCH_MANAGER;

	pspell_manager_clear_session(manager);
	if (pspell_manager_error_number(manager) == 0) {
		RETURN_TRUE;
	} else {
		hyss_error_docref(NULL, E_WARNING, "pspell_clear_session() gave error: %s", pspell_manager_error_message(manager));
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool pspell_save_wordlist(int pspell)
   Saves the current (personal) wordlist */
static HYSS_FUNCTION(pspell_save_wordlist)
{
	gear_long scin;
	PspellManager *manager;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &scin) == FAILURE) {
		return;
	}

	PSPELL_FETCH_MANAGER;

	pspell_manager_save_all_word_lists(manager);

	if (pspell_manager_error_number(manager) == 0) {
		RETURN_TRUE;
	} else {
		hyss_error_docref(NULL, E_WARNING, "pspell_save_wordlist() gave error: %s", pspell_manager_error_message(manager));
		RETURN_FALSE;
	}

}
/* }}} */

/* {{{ proto int pspell_config_create(string language [, string spelling [, string jargon [, string encoding]]])
   Create a new config to be used later to create a manager */
static HYSS_FUNCTION(pspell_config_create)
{
	char *language, *spelling = NULL, *jargon = NULL, *encoding = NULL;
	size_t language_len, spelling_len = 0, jargon_len = 0, encoding_len = 0;
	zval *ind;
	PspellConfig *config;

#ifdef HYSS_WIN32
	TCHAR aspell_dir[200];
	TCHAR data_dir[220];
	TCHAR dict_dir[220];
	HKEY hkey;
	DWORD dwType,dwLen;
#endif

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s|sss", &language, &language_len, &spelling, &spelling_len,
		&jargon, &jargon_len, &encoding, &encoding_len) == FAILURE) {
		return;
	}

	config = new_pspell_config();

#ifdef HYSS_WIN32
    /* If aspell was installed using installer, we should have a key
     * pointing to the location of the dictionaries
     */
	if (0 == RegOpenKey(HKEY_LOCAL_MACHINE, "Software\\Aspell", &hkey)) {
		LONG result;
		dwLen = sizeof(aspell_dir) - 1;
		result = RegQueryValueEx(hkey, "", NULL, &dwType, (LPBYTE)&aspell_dir, &dwLen);
		RegCloseKey(hkey);
		if (result == ERROR_SUCCESS) {
			strlcpy(data_dir, aspell_dir, sizeof(data_dir));
			strlcat(data_dir, "\\data", sizeof(data_dir));
			strlcpy(dict_dir, aspell_dir, sizeof(dict_dir));
			strlcat(dict_dir, "\\dict", sizeof(dict_dir));

			pspell_config_replace(config, "data-dir", data_dir);
			pspell_config_replace(config, "dict-dir", dict_dir);
		}
	}
#endif

	pspell_config_replace(config, "language-tag", language);

 	if (spelling_len) {
		pspell_config_replace(config, "spelling", spelling);
	}

	if (jargon_len) {
		pspell_config_replace(config, "jargon", jargon);
	}

	if (encoding_len) {
		pspell_config_replace(config, "encoding", encoding);
	}

	/* By default I do not want to write anything anywhere because it'll try to write to $HOME
	which is not what we want */
	pspell_config_replace(config, "save-repl", "false");

	ind = gear_list_insert(config, le_pspell_config);
	RETURN_LONG(Z_RES_HANDLE_P(ind));
}
/* }}} */

/* {{{ proto bool pspell_config_runtogether(int conf, bool runtogether)
   Consider run-together words as valid components */
static HYSS_FUNCTION(pspell_config_runtogether)
{
	gear_long conf;
	gear_bool runtogether;
	PspellConfig *config;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lb", &conf, &runtogether) == FAILURE) {
		return;
	}

	PSPELL_FETCH_CONFIG;

	pspell_config_replace(config, "run-together", runtogether ? "true" : "false");

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool pspell_config_mode(int conf, int mode)
   Select mode for config (PSPELL_FAST, PSPELL_NORMAL or PSPELL_BAD_SPELLERS) */
static HYSS_FUNCTION(pspell_config_mode)
{
	gear_long conf, mode;
	PspellConfig *config;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ll", &conf, &mode) == FAILURE) {
		return;
	}

	PSPELL_FETCH_CONFIG;

	/* First check what mode we want (how many suggestions) */
	if (mode == PSPELL_FAST) {
		pspell_config_replace(config, "sug-mode", "fast");
	} else if (mode == PSPELL_NORMAL) {
		pspell_config_replace(config, "sug-mode", "normal");
	} else if (mode == PSPELL_BAD_SPELLERS) {
		pspell_config_replace(config, "sug-mode", "bad-spellers");
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool pspell_config_ignore(int conf, int ignore)
   Ignore words <= n chars */
static HYSS_FUNCTION(pspell_config_ignore)
{
	char ignore_str[MAX_LENGTH_OF_LONG + 1];
	gear_long conf, ignore = 0L;
	PspellConfig *config;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ll", &conf, &ignore) == FAILURE) {
		return;
	}

	PSPELL_FETCH_CONFIG;

	snprintf(ignore_str, sizeof(ignore_str), GEAR_LONG_FMT, ignore);

	pspell_config_replace(config, "ignore", ignore_str);
	RETURN_TRUE;
}
/* }}} */

static void pspell_config_path(INTERNAL_FUNCTION_PARAMETERS, char *option)
{
	gear_long conf;
	char *value;
	size_t value_len;
	PspellConfig *config;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lp", &conf, &value, &value_len) == FAILURE) {
		return;
	}

	PSPELL_FETCH_CONFIG;

	if (hyss_check_open_basedir(value)) {
		RETURN_FALSE;
	}

	pspell_config_replace(config, option, value);

	RETURN_TRUE;
}

/* {{{ proto bool pspell_config_personal(int conf, string personal)
   Use a personal dictionary for this config */
static HYSS_FUNCTION(pspell_config_personal)
{
	pspell_config_path(INTERNAL_FUNCTION_PARAM_PASSTHRU, "personal");
}
/* }}} */

/* {{{ proto bool pspell_config_dict_dir(int conf, string directory)
   location of the main word list */
static HYSS_FUNCTION(pspell_config_dict_dir)
{
	pspell_config_path(INTERNAL_FUNCTION_PARAM_PASSTHRU, "dict-dir");
}
/* }}} */

/* {{{ proto bool pspell_config_data_dir(int conf, string directory)
    location of language data files */
static HYSS_FUNCTION(pspell_config_data_dir)
{
	pspell_config_path(INTERNAL_FUNCTION_PARAM_PASSTHRU, "data-dir");
}
/* }}} */

/* {{{ proto bool pspell_config_repl(int conf, string repl)
   Use a personal dictionary with replacement pairs for this config */
static HYSS_FUNCTION(pspell_config_repl)
{
	gear_long conf;
	char *repl;
	size_t repl_len;
	PspellConfig *config;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lp", &conf, &repl, &repl_len) == FAILURE) {
		return;
	}

	PSPELL_FETCH_CONFIG;

	pspell_config_replace(config, "save-repl", "true");

	if (hyss_check_open_basedir(repl)) {
		RETURN_FALSE;
	}

	pspell_config_replace(config, "repl", repl);

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool pspell_config_save_repl(int conf, bool save)
   Save replacement pairs when personal list is saved for this config */
static HYSS_FUNCTION(pspell_config_save_repl)
{
	gear_long conf;
	gear_bool save;
	PspellConfig *config;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lb", &conf, &save) == FAILURE) {
		return;
	}

	PSPELL_FETCH_CONFIG;

	pspell_config_replace(config, "save-repl", save ? "true" : "false");

	RETURN_TRUE;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
static HYSS_MINFO_FUNCTION(pspell)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "PSpell Support", "enabled");
	hyss_info_print_table_end();
}
/* }}} */

#endif

