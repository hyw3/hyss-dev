/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PSPELL_H
#define _PSPELL_H
#if HAVE_PSPELL
extern gear_capi_entry pspell_capi_entry;
#define pspell_capi_ptr &pspell_capi_entry

#include "hyss_version.h"
#define HYSS_PSPELL_VERSION HYSS_VERSION

#else
#define pspell_capi_ptr NULL
#endif

#define hyssext_pspell_ptr pspell_capi_ptr

#endif /* _PSPELL_H */
