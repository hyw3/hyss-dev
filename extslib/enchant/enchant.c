/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include <enchant.h>
#include "hyss_enchant.h"

typedef EnchantBroker * EnchantBrokerPtr;
typedef struct _broker_struct enchant_broker;
typedef struct _dict_struct enchant_dict;

typedef enchant_broker * enchant_brokerPtr;
typedef enchant_dict * enchant_dictPtr;

typedef struct _broker_struct {
	EnchantBroker	*pbroker;
	enchant_dict	**dict;
	unsigned int	dictcnt;
	gear_resource	*rsrc;
} _enchant_broker;

typedef struct _dict_struct {
	unsigned int	id;
	EnchantDict		*pdict;
	enchant_broker	*pbroker;
	gear_resource	*rsrc;
} _enchant_dict;


/* True global resources - no need for thread safety here */
static int le_enchant_broker;
static int le_enchant_dict;

/* If you declare any globals in hyss_enchant.h uncomment this:*/
/*GEAR_DECLARE_CAPI_GLOBALS(enchant)*/

#define HYSS_ENCHANT_MYSPELL 1
#define HYSS_ENCHANT_ISPELL 2

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO(arginfo_enchant_broker_init, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_broker_free, 0, 0, 1)
	GEAR_ARG_INFO(0, broker)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_broker_set_dict_path, 0, 0, 3)
	GEAR_ARG_INFO(0, broker)
	GEAR_ARG_INFO(0, name)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_broker_get_dict_path, 0, 0, 2)
	GEAR_ARG_INFO(0, broker)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_broker_request_dict, 0, 0, 2)
	GEAR_ARG_INFO(0, broker)
	GEAR_ARG_INFO(0, tag)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_broker_request_pwl_dict, 0, 0, 2)
	GEAR_ARG_INFO(0, broker)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_broker_free_dict, 0, 0, 1)
	GEAR_ARG_INFO(0, dict)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_broker_set_ordering, 0, 0, 3)
	GEAR_ARG_INFO(0, broker)
	GEAR_ARG_INFO(0, tag)
	GEAR_ARG_INFO(0, ordering)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_dict_quick_check, 0, 0, 2)
	GEAR_ARG_INFO(0, dict)
	GEAR_ARG_INFO(0, word)
	GEAR_ARG_INFO(1, suggestions)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_dict_check, 0, 0, 2)
	GEAR_ARG_INFO(0, dict)
	GEAR_ARG_INFO(0, word)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_enchant_dict_store_replacement, 0, 0, 3)
	GEAR_ARG_INFO(0, dict)
	GEAR_ARG_INFO(0, mis)
	GEAR_ARG_INFO(0, cor)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ enchant_functions[]
 *
 * Every user visible function must have an entry in enchant_functions[].
 */
static const gear_function_entry enchant_functions[] = {
	HYSS_FE(enchant_broker_init, 			arginfo_enchant_broker_init)
	HYSS_FE(enchant_broker_free, 			arginfo_enchant_broker_free)
	HYSS_FE(enchant_broker_get_error, 		arginfo_enchant_broker_free)
	HYSS_FE(enchant_broker_set_dict_path,	arginfo_enchant_broker_set_dict_path)
	HYSS_FE(enchant_broker_get_dict_path,	arginfo_enchant_broker_get_dict_path)
	HYSS_FE(enchant_broker_list_dicts, 		arginfo_enchant_broker_free)
	HYSS_FE(enchant_broker_request_dict,		arginfo_enchant_broker_request_dict)
	HYSS_FE(enchant_broker_request_pwl_dict, arginfo_enchant_broker_request_pwl_dict)
	HYSS_FE(enchant_broker_free_dict, 		arginfo_enchant_broker_free_dict)
	HYSS_FE(enchant_broker_dict_exists, 		arginfo_enchant_broker_request_dict)
	HYSS_FE(enchant_broker_set_ordering, 	arginfo_enchant_broker_set_ordering)
	HYSS_FE(enchant_broker_describe, 		arginfo_enchant_broker_free)
	HYSS_FE(enchant_dict_check, 				arginfo_enchant_dict_check)
	HYSS_FE(enchant_dict_suggest, 			arginfo_enchant_dict_check)
	HYSS_FE(enchant_dict_add_to_personal, 	arginfo_enchant_dict_check)
	HYSS_FE(enchant_dict_add_to_session, 	arginfo_enchant_dict_check)
	HYSS_FE(enchant_dict_is_in_session, 		arginfo_enchant_dict_check)
	HYSS_FE(enchant_dict_store_replacement, 	arginfo_enchant_dict_store_replacement)
	HYSS_FE(enchant_dict_get_error, 			arginfo_enchant_broker_free_dict)
	HYSS_FE(enchant_dict_describe, 			arginfo_enchant_broker_free_dict)
	HYSS_FE(enchant_dict_quick_check, 		arginfo_enchant_dict_quick_check)
	HYSS_FE_END
};
/* }}} */

/* {{{ enchant_capi_entry
 */
gear_capi_entry enchant_capi_entry = {
	STANDARD_CAPI_HEADER,
	"enchant",
	enchant_functions,
	HYSS_MINIT(enchant),
	HYSS_MSHUTDOWN(enchant),
	NULL,	/* Replace with NULL if there's nothing to do at request start */
	NULL,	/* Replace with NULL if there's nothing to do at request end */
	HYSS_MINFO(enchant),
	HYSS_ENCHANT_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_ENCHANT
GEAR_GET_CAPI(enchant)
#endif

static void
enumerate_providers_fn (const char * const name,
                        const char * const desc,
                        const char * const file,
                        void * ud) /* {{{ */
{
	zval *zdesc = (zval *) ud;
	zval tmp_array;

	array_init(&tmp_array);

	add_assoc_string(&tmp_array, "name", (char *)name);
	add_assoc_string(&tmp_array, "desc", (char *)desc);
	add_assoc_string(&tmp_array, "file", (char *)file);

	if (Z_TYPE_P(zdesc)!=IS_ARRAY) {
		array_init(zdesc);
	}

	add_next_index_zval(zdesc, &tmp_array);
}
/* }}} */

static void
describe_dict_fn (const char * const lang,
                  const char * const name,
                  const char * const desc,
                  const char * const file,
                  void * ud) /* {{{ */
{
	zval *zdesc = (zval *) ud;
	array_init(zdesc);
	add_assoc_string(zdesc, "lang", (char *)lang);
	add_assoc_string(zdesc, "name", (char *)name);
	add_assoc_string(zdesc, "desc", (char *)desc);
	add_assoc_string(zdesc, "file", (char *)file);
}
/* }}} */

static void hyss_enchant_list_dicts_fn( const char * const lang_tag,
	   	const char * const provider_name, const char * const provider_desc,
		const char * const provider_file, void * ud) /* {{{ */
{
	zval *zdesc = (zval *) ud;
	zval tmp_array;

	array_init(&tmp_array);
	add_assoc_string(&tmp_array, "lang_tag", (char *)lang_tag);
	add_assoc_string(&tmp_array, "provider_name", (char *)provider_name);
	add_assoc_string(&tmp_array, "provider_desc", (char *)provider_desc);
	add_assoc_string(&tmp_array, "provider_file", (char *)provider_file);

	if (Z_TYPE_P(zdesc) != IS_ARRAY) {
		array_init(zdesc);
	}
	add_next_index_zval(zdesc, &tmp_array);

}
/* }}} */

static void hyss_enchant_broker_free(gear_resource *rsrc) /* {{{ */
{
	if (rsrc->ptr) {
		enchant_broker *broker = (enchant_broker *)rsrc->ptr;
		if (broker) {
			if (broker->pbroker) {
				if (broker->dictcnt && broker->dict) {
					if (broker->dict) {
						int total;
						total = broker->dictcnt-1;
						do {
							if (broker->dict[total]) {
								enchant_dict *pdict = broker->dict[total];
								broker->dict[total] = NULL;
								gear_list_free(pdict->rsrc);
								efree(pdict);
							}
							total--;
						} while (total>=0);
					}
					efree(broker->dict);
					broker->dict = NULL;
				}
				enchant_broker_free(broker->pbroker);
			}
			efree(broker);
		}
	}
}
/* }}} */

static void hyss_enchant_dict_free(gear_resource *rsrc) /* {{{ */

{
	if (rsrc->ptr) {
		enchant_dict *pdict = (enchant_dict *)rsrc->ptr;
		if (pdict) {
			enchant_broker *pbroker = pdict->pbroker;

			if (pdict->pdict && pbroker) {
				enchant_broker_free_dict(pbroker->pbroker, pdict->pdict);
			}

			pbroker->dict[pdict->id] = NULL;
			efree(pdict);
			gear_list_delete(pbroker->rsrc);
		}
	}
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(enchant)
{
	le_enchant_broker = gear_register_list_destructors_ex(hyss_enchant_broker_free, NULL, "enchant_broker", capi_number);
	le_enchant_dict = gear_register_list_destructors_ex(hyss_enchant_dict_free, NULL, "enchant_dict", capi_number);
	REGISTER_LONG_CONSTANT("ENCHANT_MYSPELL", HYSS_ENCHANT_MYSPELL, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("ENCHANT_ISPELL", HYSS_ENCHANT_ISPELL, CONST_CS | CONST_PERSISTENT);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION(enchant)
{
	return SUCCESS;
}
/* }}} */

static void __enumerate_providers_fn (const char * const name,
                        const char * const desc,
                        const char * const file,
                        void * ud) /* {{{ */
{
	hyss_info_print_table_row(3, name, desc, file);
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(enchant)
{
	EnchantBroker *pbroker;

	pbroker = enchant_broker_init();
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "enchant support", "enabled");
#ifdef ENCHANT_VERSION_STRING
	hyss_info_print_table_row(2, "Libenchant Version", ENCHANT_VERSION_STRING);
#elif defined(HAVE_ENCHANT_BROKER_SET_PARAM)
	hyss_info_print_table_row(2, "Libenchant Version", "1.5.0 or later");
#endif
	hyss_info_print_table_end();

	hyss_info_print_table_start();
	enchant_broker_describe(pbroker, __enumerate_providers_fn, NULL);
	hyss_info_print_table_end();
	enchant_broker_free(pbroker);
}
/* }}} */

#define HYSS_ENCHANT_GET_BROKER	\
	pbroker = (enchant_broker *)gear_fetch_resource(Z_RES_P(broker), "enchant_broker", le_enchant_broker); \
	if (!pbroker || !pbroker->pbroker) {	\
		hyss_error_docref(NULL, E_WARNING, "%s", "Resource broker invalid");	\
		RETURN_FALSE;	\
	}

#define HYSS_ENCHANT_GET_DICT	\
	pdict = (enchant_dict *)gear_fetch_resource(Z_RES_P(dict), "enchant_dict", le_enchant_dict); \
	if (!pdict || !pdict->pdict) {	\
		hyss_error_docref(NULL, E_WARNING, "%s", "Invalid dictionary resource.");	\
		RETURN_FALSE;	\
	}

/* {{{ proto resource enchant_broker_init()
   create a new broker object capable of requesting */
HYSS_FUNCTION(enchant_broker_init)
{
	enchant_broker *broker;
	EnchantBroker *pbroker;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	pbroker = enchant_broker_init();

	if (pbroker) {
		broker = (enchant_broker *) emalloc(sizeof(enchant_broker));
		broker->pbroker = pbroker;
		broker->dict = NULL;
		broker->dictcnt = 0;
		broker->rsrc = gear_register_resource(broker, le_enchant_broker);
		RETURN_RES(broker->rsrc);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto bool enchant_broker_free(resource broker)
   Destroys the broker object and its dictionnaries */
HYSS_FUNCTION(enchant_broker_free)
{
	zval *broker;
	enchant_broker *pbroker;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &broker) == FAILURE) {
		RETURN_FALSE;
	}
	HYSS_ENCHANT_GET_BROKER;

	gear_list_close(Z_RES_P(broker));
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto string enchant_broker_get_error(resource broker)
   Returns the last error of the broker */
HYSS_FUNCTION(enchant_broker_get_error)
{
	zval *broker;
	enchant_broker *pbroker;
	char *msg;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &broker) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	msg = enchant_broker_get_error(pbroker->pbroker);
	if (msg) {
		RETURN_STRING((char *)msg);
	}
	RETURN_FALSE;
}
/* }}} */

#if HAVE_ENCHANT_BROKER_SET_PARAM
/* {{{ proto bool enchant_broker_set_dict_path(resource broker, int dict_type, string value)
	Set the directory path for a given backend, works with ispell and myspell */
HYSS_FUNCTION(enchant_broker_set_dict_path)
{
	zval *broker;
	enchant_broker *pbroker;
	gear_long dict_type;
	char *value;
	size_t value_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rls", &broker, &dict_type, &value, &value_len) == FAILURE) {
		RETURN_FALSE;
	}

	if (!value_len) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	switch (dict_type) {
		case HYSS_ENCHANT_MYSPELL:
			HYSS_ENCHANT_GET_BROKER;
			enchant_broker_set_param(pbroker->pbroker, "enchant.myspell.dictionary.path", (const char *)value);
			RETURN_TRUE;
			break;

		case HYSS_ENCHANT_ISPELL:
			HYSS_ENCHANT_GET_BROKER;
			enchant_broker_set_param(pbroker->pbroker, "enchant.ispell.dictionary.path", (const char *)value);
			RETURN_TRUE;
			break;

		default:
			RETURN_FALSE;
	}
}
/* }}} */


/* {{{ proto string enchant_broker_get_dict_path(resource broker, int dict_type)
	Get the directory path for a given backend, works with ispell and myspell */
HYSS_FUNCTION(enchant_broker_get_dict_path)
{
	zval *broker;
	enchant_broker *pbroker;
	gear_long dict_type;
	char *value;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rl", &broker, &dict_type) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	switch (dict_type) {
		case HYSS_ENCHANT_MYSPELL:
			HYSS_ENCHANT_GET_BROKER;
			value = enchant_broker_get_param(pbroker->pbroker, "enchant.myspell.dictionary.path");
			break;

		case HYSS_ENCHANT_ISPELL:
			HYSS_ENCHANT_GET_BROKER;
			value = enchant_broker_get_param(pbroker->pbroker, "enchant.ispell.dictionary.path");
			break;

		default:
			RETURN_FALSE;
	}

	if (value == NULL) {
		hyss_error_docref(NULL, E_WARNING, "dict_path not set");
		RETURN_FALSE;
	}

	RETURN_STRING(value);
}
/* }}} */
#else
/* {{{ proto bool enchant_broker_set_dict_path(resource broker, int dict_type, string value)
	Set the directory path for a given backend, works with ispell and myspell */
HYSS_FUNCTION(enchant_broker_set_dict_path)
{
	RETURN_FALSE;
}
/* }}} */


/* {{{ proto string enchant_broker_get_dict_path(resource broker, int dict_type)
	Get the directory path for a given backend, works with ispell and myspell */
HYSS_FUNCTION(enchant_broker_get_dict_path)
{
	RETURN_FALSE;
}
/* }}} */
#endif

/* {{{ proto string enchant_broker_list_dicts(resource broker)
   Lists the dictionaries available for the given broker */
HYSS_FUNCTION(enchant_broker_list_dicts)
{
	zval *broker;
	enchant_broker *pbroker;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &broker) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	enchant_broker_list_dicts(pbroker->pbroker, hyss_enchant_list_dicts_fn, (void *)return_value);
}
/* }}} */

/* {{{ proto resource enchant_broker_request_dict(resource broker, string tag)
	create a new dictionary using tag, the non-empty language tag you wish to request
	a dictionary for ("en_US", "de_DE", ...) */
HYSS_FUNCTION(enchant_broker_request_dict)
{
	zval *broker;
	enchant_broker *pbroker;
	enchant_dict *dict;
	EnchantDict *d;
	char *tag;
	size_t taglen;
	int pos;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &broker, &tag, &taglen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	if (taglen == 0) {
		hyss_error_docref(NULL, E_WARNING, "Tag cannot be empty");
		RETURN_FALSE;
	}

	d = enchant_broker_request_dict(pbroker->pbroker, (const char *)tag);
	if (d) {
		pos = pbroker->dictcnt++;
		if (pbroker->dictcnt) {
			pbroker->dict = (enchant_dict **)erealloc(pbroker->dict, sizeof(enchant_dict *) * pbroker->dictcnt);
		} else {
			pbroker->dict = (enchant_dict **)emalloc(sizeof(enchant_dict *));
			pos = 0;
		}

		dict = pbroker->dict[pos] = (enchant_dict *)emalloc(sizeof(enchant_dict));
		dict->id = pos;
		dict->pbroker = pbroker;
		dict->pdict = d;
		pbroker->dict[pos] = dict;

		dict->rsrc = gear_register_resource(dict, le_enchant_dict);
		GC_ADDREF(pbroker->rsrc);
		RETURN_RES(dict->rsrc);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto resource enchant_broker_request_pwl_dict(resource broker, string filename)
   creates a dictionary using a PWL file. A PWL file is personal word file one word per line. It must exist before the call.*/
HYSS_FUNCTION(enchant_broker_request_pwl_dict)
{
	zval *broker;
	enchant_broker *pbroker;
	enchant_dict *dict;
	EnchantDict *d;
	char *pwl;
	size_t pwllen;
	int pos;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rp", &broker, &pwl, &pwllen) == FAILURE) {
		RETURN_FALSE;
	}

	if (hyss_check_open_basedir(pwl)) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	d = enchant_broker_request_pwl_dict(pbroker->pbroker, (const char *)pwl);
	if (d) {
		pos = pbroker->dictcnt++;
		if (pbroker->dictcnt) {
			pbroker->dict = (enchant_dict **)erealloc(pbroker->dict, sizeof(enchant_dict *) * pbroker->dictcnt);
		} else {
			pbroker->dict = (enchant_dict **)emalloc(sizeof(enchant_dict *));
			pos = 0;
		}

		dict = pbroker->dict[pos] = (enchant_dict *)emalloc(sizeof(enchant_dict));
		dict->id = pos;
		dict->pbroker = pbroker;
		dict->pdict = d;
		pbroker->dict[pos] = dict;

		dict->rsrc = gear_register_resource(dict, le_enchant_dict);
		GC_ADDREF(pbroker->rsrc);
		RETURN_RES(dict->rsrc);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto resource enchant_broker_free_dict(resource dict)
   Free the dictionary resource */
HYSS_FUNCTION(enchant_broker_free_dict)
{
	zval *dict;
	enchant_dict *pdict;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &dict) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	gear_list_close(Z_RES_P(dict));
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool enchant_broker_dict_exists(resource broker, string tag)
   Whether a dictionary exists or not. Using non-empty tag */
HYSS_FUNCTION(enchant_broker_dict_exists)
{
	zval *broker;
	char *tag;
	size_t taglen;
	enchant_broker * pbroker;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &broker, &tag, &taglen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	RETURN_BOOL(enchant_broker_dict_exists(pbroker->pbroker, tag));
}
/* }}} */

/* {{{ proto bool enchant_broker_set_ordering(resource broker, string tag, string ordering)
	Declares a preference of dictionaries to use for the language
	described/referred to by 'tag'. The ordering is a comma delimited
	list of provider names. As a special exception, the "*" tag can
	be used as a language tag to declare a default ordering for any
	language that does not explicitly declare an ordering. */

HYSS_FUNCTION(enchant_broker_set_ordering)
{
	zval *broker;
	char *pordering;
	size_t porderinglen;
	char *ptag;
	size_t ptaglen;
	enchant_broker * pbroker;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rss", &broker, &ptag, &ptaglen, &pordering, &porderinglen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	enchant_broker_set_ordering(pbroker->pbroker, ptag, pordering);
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto array enchant_broker_describe(resource broker)
	Enumerates the Enchant providers and tells you some rudimentary information about them. The same info is provided through hyssinfo() */
HYSS_FUNCTION(enchant_broker_describe)
{
	EnchantBrokerDescribeFn describetozval = enumerate_providers_fn;
	zval *broker;
	enchant_broker * pbroker;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &broker) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_BROKER;

	enchant_broker_describe(pbroker->pbroker, describetozval, (void *)return_value);
}
/* }}} */

/* {{{ proto bool enchant_dict_quick_check(resource dict, string word [, array &suggestions])
    If the word is correctly spelled return true, otherwise return false, if suggestions variable
    is provided, fill it with spelling alternatives. */
HYSS_FUNCTION(enchant_dict_quick_check)
{
	zval *dict, *sugg = NULL;
	char *word;
	size_t wordlen;
	enchant_dict *pdict;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs|z/", &dict, &word, &wordlen, &sugg) == FAILURE) {
		RETURN_FALSE;
	}

	if (sugg) {
		zval_ptr_dtor(sugg);
		array_init(sugg);
	}

	HYSS_ENCHANT_GET_DICT;

	if (enchant_dict_check(pdict->pdict, word, wordlen) > 0) {
		int n_sugg;
		size_t n_sugg_st;
		char **suggs;

		if (!sugg && GEAR_NUM_ARGS() == 2) {
			RETURN_FALSE;
		}

		suggs = enchant_dict_suggest(pdict->pdict, word, wordlen, &n_sugg_st);
		memcpy(&n_sugg, &n_sugg_st, sizeof(n_sugg));
		if (suggs && n_sugg) {
			int i;
			for (i = 0; i < n_sugg; i++) {
				add_next_index_string(sugg, suggs[i]);
			}
			enchant_dict_free_suggestions(pdict->pdict, suggs);
		}


		RETURN_FALSE;
	}
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool enchant_dict_check(resource dict, string word)
    If the word is correctly spelled return true, otherwise return false */
HYSS_FUNCTION(enchant_dict_check)
{
	zval *dict;
	char *word;
	size_t wordlen;
	enchant_dict *pdict;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &dict, &word, &wordlen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	RETURN_BOOL(!enchant_dict_check(pdict->pdict, word, wordlen));
}
/* }}} */

/* {{{ proto array enchant_dict_suggest(resource dict, string word)
    Will return a list of values if any of those pre-conditions are not met.*/
HYSS_FUNCTION(enchant_dict_suggest)
{
	zval *dict;
	char *word;
	size_t wordlen;
	char **suggs;
	enchant_dict *pdict;
	int n_sugg;
	size_t n_sugg_st;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &dict, &word, &wordlen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	suggs = enchant_dict_suggest(pdict->pdict, word, wordlen, &n_sugg_st);
	memcpy(&n_sugg, &n_sugg_st, sizeof(n_sugg));
	if (suggs && n_sugg) {
		int i;

		array_init(return_value);
		for (i = 0; i < n_sugg; i++) {
			add_next_index_string(return_value, suggs[i]);
		}

		enchant_dict_free_suggestions(pdict->pdict, suggs);
	}
}
/* }}} */

/* {{{ proto void enchant_dict_add_to_personal(resource dict, string word)
     add 'word' to personal word list */
HYSS_FUNCTION(enchant_dict_add_to_personal)
{
	zval *dict;
	char *word;
	size_t wordlen;
	enchant_dict *pdict;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &dict, &word, &wordlen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	enchant_dict_add_to_personal(pdict->pdict, word, wordlen);
}
/* }}} */

/* {{{ proto void enchant_dict_add_to_session(resource dict, string word)
   add 'word' to this spell-checking session */
HYSS_FUNCTION(enchant_dict_add_to_session)
{
	zval *dict;
	char *word;
	size_t wordlen;
	enchant_dict *pdict;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &dict, &word, &wordlen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	enchant_dict_add_to_session(pdict->pdict, word, wordlen);
}
/* }}} */

/* {{{ proto bool enchant_dict_is_in_session(resource dict, string word)
   whether or not 'word' exists in this spelling-session */
HYSS_FUNCTION(enchant_dict_is_in_session)
{
	zval *dict;
	char *word;
	size_t wordlen;
	enchant_dict *pdict;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &dict, &word, &wordlen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	RETURN_BOOL(enchant_dict_is_in_session(pdict->pdict, word, wordlen));
}
/* }}} */

/* {{{ proto void enchant_dict_store_replacement(resource dict, string mis, string cor)
	add a correction for 'mis' using 'cor'.
	Notes that you replaced @mis with @cor, so it's possibly more likely
	that future occurrences of @mis will be replaced with @cor. So it might
	bump @cor up in the suggestion list.*/
HYSS_FUNCTION(enchant_dict_store_replacement)
{
	zval *dict;
	char *mis, *cor;
	size_t mislen, corlen;

	enchant_dict *pdict;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rss", &dict, &mis, &mislen, &cor, &corlen) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	enchant_dict_store_replacement(pdict->pdict, mis, mislen, cor, corlen);
}
/* }}} */

/* {{{ proto string enchant_dict_get_error(resource dict)
   Returns the last error of the current spelling-session */
HYSS_FUNCTION(enchant_dict_get_error)
{
	zval *dict;
	enchant_dict *pdict;
	char *msg;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &dict) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	msg = enchant_dict_get_error(pdict->pdict);
	if (msg) {
		RETURN_STRING((char *)msg);
	}

	RETURN_FALSE;
}
/* }}} */

/* {{{ proto array enchant_dict_describe(resource dict)
   Describes an individual dictionary 'dict' */
HYSS_FUNCTION(enchant_dict_describe)
{
	zval *dict;
	enchant_dict *pdict;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &dict) == FAILURE) {
		RETURN_FALSE;
	}

	HYSS_ENCHANT_GET_DICT;

	enchant_dict_describe(pdict->pdict, describe_dict_fn, (void *)return_value);
}
/* }}} */

