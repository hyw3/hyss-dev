/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_ENCHANT_H
#define HYSS_ENCHANT_H

extern gear_capi_entry enchant_capi_entry;
#define hyssext_enchant_ptr &enchant_capi_entry

#define HYSS_ENCHANT_VERSION HYSS_VERSION

#ifdef HYSS_WIN32
#define HYSS_ENCHANT_API __declspec(dllexport)
#else
#define HYSS_ENCHANT_API
#endif

#ifdef ZTS
#include "hypbc.h"
#endif

HYSS_MINIT_FUNCTION(enchant);
HYSS_MSHUTDOWN_FUNCTION(enchant);
HYSS_MINFO_FUNCTION(enchant);

HYSS_FUNCTION(enchant_broker_init);
HYSS_FUNCTION(enchant_broker_free);
HYSS_FUNCTION(enchant_broker_get_error);
HYSS_FUNCTION(enchant_broker_set_dict_path);
HYSS_FUNCTION(enchant_broker_get_dict_path);
HYSS_FUNCTION(enchant_broker_list_dicts);
HYSS_FUNCTION(enchant_broker_request_dict);
HYSS_FUNCTION(enchant_broker_request_pwl_dict);
HYSS_FUNCTION(enchant_broker_free_dict);
HYSS_FUNCTION(enchant_broker_dict_exists);
HYSS_FUNCTION(enchant_broker_set_ordering);
HYSS_FUNCTION(enchant_broker_describe);

HYSS_FUNCTION(enchant_dict_check);
HYSS_FUNCTION(enchant_dict_suggest);
HYSS_FUNCTION(enchant_dict_add_to_personal);
HYSS_FUNCTION(enchant_dict_add_to_session);
HYSS_FUNCTION(enchant_dict_is_in_session);
HYSS_FUNCTION(enchant_dict_store_replacement);
HYSS_FUNCTION(enchant_dict_get_error);
HYSS_FUNCTION(enchant_dict_describe);
HYSS_FUNCTION(enchant_dict_quick_check);

#ifdef ZTS
#define ENCHANT_G(v) PBCG(enchant_globals_id, gear_enchant_globals *, v)
#else
#define ENCHANT_G(v) (enchant_globals.v)
#endif

#endif	/* HYSS_ENCHANT_H */

