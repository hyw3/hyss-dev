/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_tokenizer.h"

#include "gear.h"
#include "gear_exceptions.h"
#include "gear_language_scanner.h"
#include "gear_language_scanner_defs.h"
#include <gear_language_parser.h>

#define geartext   LANG_SCNG(yy_text)
#define gearleng   LANG_SCNG(yy_leng)
#define gearcursor LANG_SCNG(yy_cursor)
#define gearlimit  LANG_SCNG(yy_limit)

#define TOKEN_PARSE 				1

void tokenizer_token_get_all_register_constants(INIT_FUNC_ARGS) {
	REGISTER_LONG_CONSTANT("TOKEN_PARSE", TOKEN_PARSE, CONST_CS|CONST_PERSISTENT);
}

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_token_get_all, 0, 0, 1)
	GEAR_ARG_INFO(0, source)
	GEAR_ARG_INFO(0, flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_token_name, 0, 0, 1)
	GEAR_ARG_INFO(0, token)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ tokenizer_functions[]
 *
 * Every user visible function must have an entry in tokenizer_functions[].
 */
static const gear_function_entry tokenizer_functions[] = {
	HYSS_FE(token_get_all,	arginfo_token_get_all)
	HYSS_FE(token_name,		arginfo_token_name)
	HYSS_FE_END
};
/* }}} */

/* {{{ tokenizer_capi_entry
 */
gear_capi_entry tokenizer_capi_entry = {
	STANDARD_CAPI_HEADER,
	"tokenizer",
	tokenizer_functions,
	HYSS_MINIT(tokenizer),
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(tokenizer),
	HYSS_TOKENIZER_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_TOKENIZER
GEAR_GET_CAPI(tokenizer)
#endif

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(tokenizer)
{
	tokenizer_register_constants(INIT_FUNC_ARGS_PASSTHRU);
	tokenizer_token_get_all_register_constants(INIT_FUNC_ARGS_PASSTHRU);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(tokenizer)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "Tokenizer Support", "enabled");
	hyss_info_print_table_end();
}
/* }}} */

static void add_token(zval *return_value, int token_type,
		unsigned char *text, size_t leng, int lineno) {
	if (token_type >= 256) {
		zval keyword;
		array_init(&keyword);
		add_next_index_long(&keyword, token_type);
		add_next_index_stringl(&keyword, (char *) text, leng);
		add_next_index_long(&keyword, lineno);
		add_next_index_zval(return_value, &keyword);
	} else {
		if (leng == 1) {
			add_next_index_str(return_value, ZSTR_CHAR(text[0]));
		} else {
			add_next_index_stringl(return_value, (char *) text, leng);
		}
	}
}

static gear_bool tokenize(zval *return_value, gear_string *source)
{
	zval source_zval;
	gear_lex_state original_lex_state;
	zval token;
	int token_type;
	int token_line = 1;
	int need_tokens = -1; /* for __halt_compiler lexing. -1 = disabled */

	ZVAL_STR_COPY(&source_zval, source);
	gear_save_lexical_state(&original_lex_state);

	if (gear_prepare_string_for_scanning(&source_zval, "") == FAILURE) {
		gear_restore_lexical_state(&original_lex_state);
		return 0;
	}

	LANG_SCNG(yy_state) = yycINITIAL;
	array_init(return_value);

	while ((token_type = lex_scan(&token, NULL))) {
		add_token(return_value, token_type, geartext, gearleng, token_line);

		if (Z_TYPE(token) != IS_UNDEF) {
			zval_ptr_dtor_nogc(&token);
			ZVAL_UNDEF(&token);
		}

		/* after T_HALT_COMPILER collect the next three non-dropped tokens */
		if (need_tokens != -1) {
			if (token_type != T_WHITESPACE && token_type != T_OPEN_TAG
				&& token_type != T_COMMENT && token_type != T_DOC_COMMENT
				&& --need_tokens == 0
			) {
				/* fetch the rest into a T_INLINE_HTML */
				if (gearcursor != gearlimit) {
					add_token(return_value, T_INLINE_HTML,
						gearcursor, gearlimit - gearcursor, token_line);
				}
				break;
			}
		} else if (token_type == T_HALT_COMPILER) {
			need_tokens = 3;
		}

		if (CG(increment_lineno)) {
			CG(gear_lineno)++;
			CG(increment_lineno) = 0;
		}

		token_line = CG(gear_lineno);
	}

	zval_ptr_dtor_str(&source_zval);
	gear_restore_lexical_state(&original_lex_state);

	return 1;
}

void on_event(gear_hyss_scanner_event event, int token, int line, void *context)
{
	zval *token_stream = (zval *) context;
	HashTable *tokens_ht;
	zval *token_zv;

	switch (event) {
		case ON_TOKEN:
			{
				if (token == END) break;
				/* Special cases */
				if (token == ';' && LANG_SCNG(yy_leng) > 1) { /* !@ or !@\n or !@\r\n */
					token = T_CLOSE_TAG;
				} else if (token == T_ECHO && LANG_SCNG(yy_leng) == sizeof("@!=") - 1) {
					token = T_OPEN_TAG_WITH_ECHO;
				}
				add_token(token_stream, token, LANG_SCNG(yy_text), LANG_SCNG(yy_leng), line);
			}
			break;
		case ON_FEEDBACK:
			tokens_ht = Z_ARRVAL_P(token_stream);
			token_zv = gear_hash_index_find(tokens_ht, gear_hash_num_elements(tokens_ht) - 1);
			if (token_zv && Z_TYPE_P(token_zv) == IS_ARRAY) {
				ZVAL_LONG(gear_hash_index_find(Z_ARRVAL_P(token_zv), 0), token);
			}
			break;
		case ON_STOP:
			if (LANG_SCNG(yy_cursor) != LANG_SCNG(yy_limit)) {
				add_token(token_stream, T_INLINE_HTML, LANG_SCNG(yy_cursor),
					LANG_SCNG(yy_limit) - LANG_SCNG(yy_cursor), CG(gear_lineno));
			}
			break;
	}
}

static gear_bool tokenize_parse(zval *return_value, gear_string *source)
{
	zval source_zval;
	gear_lex_state original_lex_state;
	gear_bool original_in_compilation;
	gear_bool success;

	ZVAL_STR_COPY(&source_zval, source);

	original_in_compilation = CG(in_compilation);
	CG(in_compilation) = 1;
	gear_save_lexical_state(&original_lex_state);

	if ((success = (gear_prepare_string_for_scanning(&source_zval, "") == SUCCESS))) {
		zval token_stream;
		array_init(&token_stream);

		CG(ast) = NULL;
		CG(ast_arena) = gear_arena_create(1024 * 32);
		LANG_SCNG(yy_state) = yycINITIAL;
		LANG_SCNG(on_event) = on_event;
		LANG_SCNG(on_event_context) = &token_stream;

		if((success = (gearparse() == SUCCESS))) {
			ZVAL_COPY_VALUE(return_value, &token_stream);
		} else {
			zval_ptr_dtor(&token_stream);
		}

		gear_ast_destroy(CG(ast));
		gear_arena_destroy(CG(ast_arena));
	}

	/* restore compiler and scanner global states */
	gear_restore_lexical_state(&original_lex_state);
	CG(in_compilation) = original_in_compilation;

	zval_ptr_dtor_str(&source_zval);

	return success;
}

/* }}} */

/* {{{ proto array token_get_all(string source [, int flags])
 */
HYSS_FUNCTION(token_get_all)
{
	gear_string *source;
	gear_long flags = 0;
	gear_bool success;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(source)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(flags)
	GEAR_PARSE_PARAMETERS_END();

	if (flags & TOKEN_PARSE) {
		success = tokenize_parse(return_value, source);
	} else {
		success = tokenize(return_value, source);
		/* Normal token_get_all() should not throw. */
		gear_clear_exception();
	}

	if (!success) RETURN_FALSE;
}
/* }}} */

/* {{{ proto string token_name(int type)
 */
HYSS_FUNCTION(token_name)
{
	gear_long type;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(type)
	GEAR_PARSE_PARAMETERS_END();

	RETVAL_STRING(get_token_type_name(type));
}
/* }}} */

