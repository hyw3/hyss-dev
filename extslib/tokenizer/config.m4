dnl config.m4 for extension tokenizer

dnl Otherwise use enable:

HYSS_ARG_ENABLE(tokenizer, whether to enable tokenizer support,
[  --disable-tokenizer     Disable tokenizer support], yes)

if test "$HYSS_TOKENIZER" != "no"; then
  HYSS_NEW_EXTENSION(tokenizer, tokenizer.c tokenizer_data.c, $ext_shared)
  HYSS_ADD_MAKEFILE_FRAGMENT
fi
