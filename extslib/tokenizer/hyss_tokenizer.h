/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_TOKENIZER_H
#define HYSS_TOKENIZER_H

extern gear_capi_entry tokenizer_capi_entry;
#define hyssext_tokenizer_ptr &tokenizer_capi_entry

#include "hyss_version.h"
#define HYSS_TOKENIZER_VERSION HYSS_VERSION

#ifdef ZTS
#include "hypbc.h"
#endif

void tokenizer_register_constants(INIT_FUNC_ARGS);
char *get_token_type_name(int token_type);


HYSS_MINIT_FUNCTION(tokenizer);
HYSS_MINFO_FUNCTION(tokenizer);

HYSS_FUNCTION(token_get_all);
HYSS_FUNCTION(token_name);

#ifdef ZTS
#define TOKENIZER_G(v) PBCG(tokenizer_globals_id, gear_tokenizer_globals *, v)
#else
#define TOKENIZER_G(v) (tokenizer_globals.v)
#endif

#endif	/* HYSS_TOKENIZER_H */

