dnl config.m4 for extension ctype

HYSS_ARG_ENABLE(ctype, whether to enable ctype functions,
[  --disable-ctype         Disable ctype functions], yes)

if test "$HYSS_CTYPE" != "no"; then
  AC_DEFINE(HAVE_CTYPE, 1, [ ])
  HYSS_NEW_EXTENSION(ctype, ctype.c, $ext_shared)
fi
