/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "hyss_ctype.h"
#include "SAPI.h"
#include "extslib/standard/info.h"

#include <ctype.h>

#if HAVE_CTYPE

static HYSS_MINFO_FUNCTION(ctype);

static HYSS_FUNCTION(ctype_alnum);
static HYSS_FUNCTION(ctype_alpha);
static HYSS_FUNCTION(ctype_cntrl);
static HYSS_FUNCTION(ctype_digit);
static HYSS_FUNCTION(ctype_lower);
static HYSS_FUNCTION(ctype_graph);
static HYSS_FUNCTION(ctype_print);
static HYSS_FUNCTION(ctype_punct);
static HYSS_FUNCTION(ctype_space);
static HYSS_FUNCTION(ctype_upper);
static HYSS_FUNCTION(ctype_xdigit);

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO(arginfo_ctype_alnum, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_alpha, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_cntrl, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_digit, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_lower, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_graph, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_print, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_punct, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_space, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_upper, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ctype_xdigit, 0)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

/* }}} */

/* {{{ ctype_functions[]
 * Every user visible function must have an entry in ctype_functions[].
 */
static const gear_function_entry ctype_functions[] = {
	HYSS_FE(ctype_alnum,	arginfo_ctype_alnum)
	HYSS_FE(ctype_alpha,	arginfo_ctype_alpha)
	HYSS_FE(ctype_cntrl,	arginfo_ctype_cntrl)
	HYSS_FE(ctype_digit,	arginfo_ctype_digit)
	HYSS_FE(ctype_lower,	arginfo_ctype_lower)
	HYSS_FE(ctype_graph,	arginfo_ctype_graph)
	HYSS_FE(ctype_print,	arginfo_ctype_print)
	HYSS_FE(ctype_punct,	arginfo_ctype_punct)
	HYSS_FE(ctype_space,	arginfo_ctype_space)
	HYSS_FE(ctype_upper,	arginfo_ctype_upper)
	HYSS_FE(ctype_xdigit,	arginfo_ctype_xdigit)
	HYSS_FE_END
};
/* }}} */

/* {{{ ctype_capi_entry
 */
gear_capi_entry ctype_capi_entry = {
	STANDARD_CAPI_HEADER,
	"ctype",
	ctype_functions,
	NULL,
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(ctype),
    HYSS_CTYPE_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_CTYPE
GEAR_GET_CAPI(ctype)
#endif

/* {{{ HYSS_MINFO_FUNCTION
 */
static HYSS_MINFO_FUNCTION(ctype)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "ctype functions", "enabled");
	hyss_info_print_table_end();
}
/* }}} */

/* {{{ ctype
 */
#define CTYPE(iswhat, allow_digits, allow_minus) \
	zval *c; \
	GEAR_PARSE_PARAMETERS_START(1, 1); \
		Z_PARAM_ZVAL(c) \
	GEAR_PARSE_PARAMETERS_END(); \
	if (Z_TYPE_P(c) == IS_LONG) { \
		if (Z_LVAL_P(c) <= 255 && Z_LVAL_P(c) >= 0) { \
			RETURN_BOOL(iswhat((int)Z_LVAL_P(c))); \
		} else if (Z_LVAL_P(c) >= -128 && Z_LVAL_P(c) < 0) { \
			RETURN_BOOL(iswhat((int)Z_LVAL_P(c) + 256)); \
		} else if (Z_LVAL_P(c) >= 0) { \
			RETURN_BOOL(allow_digits); \
		} else { \
			RETURN_BOOL(allow_minus); \
		} \
	} else if (Z_TYPE_P(c) == IS_STRING) { \
		char *p = Z_STRVAL_P(c), *e = Z_STRVAL_P(c) + Z_STRLEN_P(c); \
		if (e == p) {	\
			RETURN_FALSE;	\
		}	\
		while (p < e) { \
			if(!iswhat((int)*(unsigned char *)(p++))) { \
				RETURN_FALSE; \
			} \
		} \
		RETURN_TRUE; \
	} else { \
		RETURN_FALSE; \
	} \

/* }}} */

/* {{{ proto bool ctype_alnum(mixed c)
   Checks for alphanumeric character(s) */
static HYSS_FUNCTION(ctype_alnum)
{
	CTYPE(isalnum, 1, 0);
}
/* }}} */

/* {{{ proto bool ctype_alpha(mixed c)
   Checks for alphabetic character(s) */
static HYSS_FUNCTION(ctype_alpha)
{
	CTYPE(isalpha, 0, 0);
}
/* }}} */

/* {{{ proto bool ctype_cntrl(mixed c)
   Checks for control character(s) */
static HYSS_FUNCTION(ctype_cntrl)
{
	CTYPE(iscntrl, 0, 0);
}
/* }}} */

/* {{{ proto bool ctype_digit(mixed c)
   Checks for numeric character(s) */
static HYSS_FUNCTION(ctype_digit)
{
	CTYPE(isdigit, 1, 0);
}
/* }}} */

/* {{{ proto bool ctype_lower(mixed c)
   Checks for lowercase character(s)  */
static HYSS_FUNCTION(ctype_lower)
{
	CTYPE(islower, 0, 0);
}
/* }}} */

/* {{{ proto bool ctype_graph(mixed c)
   Checks for any printable character(s) except space */
static HYSS_FUNCTION(ctype_graph)
{
	CTYPE(isgraph, 1, 1);
}
/* }}} */

/* {{{ proto bool ctype_print(mixed c)
   Checks for printable character(s) */
static HYSS_FUNCTION(ctype_print)
{
	CTYPE(isprint, 1, 1);
}
/* }}} */

/* {{{ proto bool ctype_punct(mixed c)
   Checks for any printable character which is not whitespace or an alphanumeric character */
static HYSS_FUNCTION(ctype_punct)
{
	CTYPE(ispunct, 0, 0);
}
/* }}} */

/* {{{ proto bool ctype_space(mixed c)
   Checks for whitespace character(s)*/
static HYSS_FUNCTION(ctype_space)
{
	CTYPE(isspace, 0, 0);
}
/* }}} */

/* {{{ proto bool ctype_upper(mixed c)
   Checks for uppercase character(s) */
static HYSS_FUNCTION(ctype_upper)
{
	CTYPE(isupper, 0, 0);
}
/* }}} */

/* {{{ proto bool ctype_xdigit(mixed c)
   Checks for character(s) representing a hexadecimal digit */
static HYSS_FUNCTION(ctype_xdigit)
{
	CTYPE(isxdigit, 1, 0);
}
/* }}} */

#endif	/* HAVE_CTYPE */

