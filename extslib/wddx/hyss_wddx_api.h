/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_WDDX_API_H
#define HYSS_WDDX_API_H

#include "gear_smart_str_public.h"

#define WDDX_ARRAY_S			"<array length='%d'>"
#define WDDX_ARRAY_E			"</array>"
#define WDDX_BINARY_S			"<binary>"
#define WDDX_BINARY_E			"</binary>"
#define WDDX_BOOLEAN_TRUE		"<boolean value='true'/>"
#define WDDX_BOOLEAN_FALSE		"<boolean value='false'/>"
#define WDDX_CHAR			"<char code='%02X'/>"
#define WDDX_COMMENT_S			"<comment>"
#define WDDX_COMMENT_E			"</comment>"
#define WDDX_DATA_S			"<data>"
#define WDDX_DATA_E			"</data>"
#define WDDX_HEADER			"<header/>"
#define WDDX_HEADER_S			"<header>"
#define WDDX_HEADER_E			"</header>"
#define WDDX_NULL			"<null/>"
#define WDDX_NUMBER			"<number>%s</number>"
#define WDDX_PACKET_S			"<wddxPacket version='1.0'>"
#define WDDX_PACKET_E			"</wddxPacket>"
#define WDDX_STRING_S			"<string>"
#define WDDX_STRING_E			"</string>"
#define WDDX_STRUCT_S			"<struct>"
#define WDDX_STRUCT_E			"</struct>"
#define WDDX_VAR_S			"<var name='%s'>"
#define WDDX_VAR_E			"</var>"

#define hyss_wddx_add_chunk(packet, str)	smart_str_appends(packet, str)
#define hyss_wddx_add_chunk_ex(packet, str, len)	smart_str_appendl(packet, str, len)
#define hyss_wddx_add_chunk_static(packet, str) smart_str_appendl(packet, str, sizeof(str)-1)

typedef smart_str wddx_packet;

wddx_packet* hyss_wddx_constructor(void);
void		 hyss_wddx_destructor(wddx_packet *packet);

void 		 hyss_wddx_packet_start(wddx_packet *packet, char *comment, size_t comment_len);
void 		 hyss_wddx_packet_end(wddx_packet *packet);

void 		 hyss_wddx_serialize_var(wddx_packet *packet, zval *var, gear_string *name);
int 		 hyss_wddx_deserialize_ex(const char *, size_t, zval *return_value);
#define hyss_wddx_gather(packet) estrndup(packet->c, packet->len)

#endif /* HYSS_WDDX_API_H */
