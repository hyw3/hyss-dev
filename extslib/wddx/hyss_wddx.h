/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_WDDX_H
#define HYSS_WDDX_H

#if HAVE_WDDX

extern gear_capi_entry wddx_capi_entry;
#define wddx_capi_ptr &wddx_capi_entry

#include "hyss_version.h"
#define HYSS_WDDX_VERSION HYSS_VERSION

HYSS_FUNCTION(wddx_serialize_value);
HYSS_FUNCTION(wddx_serialize_vars);
HYSS_FUNCTION(wddx_packet_start);
HYSS_FUNCTION(wddx_packet_end);
HYSS_FUNCTION(wddx_add_vars);
HYSS_FUNCTION(wddx_deserialize);

#else

#define wddx_capi_ptr NULL

#endif /* HAVE_WDDX */

#define hyssext_wddx_ptr wddx_capi_ptr

#endif /* !HYSS_WDDX_H */
