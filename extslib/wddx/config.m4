dnl config.m4 for extension wddx

HYSS_ARG_ENABLE(wddx,whether to enable WDDX support,
[  --enable-wddx           Enable WDDX support])

if test -z "$HYSS_LIBXML_DIR"; then
  HYSS_ARG_WITH(libxml-dir, libxml2 install dir,
  [  --with-libxml-dir=DIR   WDDX: libxml2 install prefix], no, no)
fi

HYSS_ARG_WITH(libexpat-dir, libexpat dir for WDDX,
[  --with-libexpat-dir=DIR WDDX: libexpat dir for XMLRPC-EPI (deprecated)],no,no)

if test "$HYSS_WDDX" != "no"; then

  dnl
  dnl Default to libxml2 if --with-libexpat-dir is not used
  dnl
  if test "$HYSS_LIBEXPAT_DIR" = "no"; then
    if test "$HYSS_LIBXML" = "no"; then
      AC_MSG_ERROR([WDDX extension requires LIBXML extension, add --enable-libxml])
    fi

    HYSS_SETUP_LIBXML(WDDX_SHARED_LIBADD, [
      if test "$HYSS_XML" = "no"; then
        HYSS_ADD_SOURCES(extslib/xml, compat.c)
        HYSS_ADD_BUILD_DIR(extslib/xml)
      fi
    ], [
      AC_MSG_ERROR([libxml2 not found. Use --with-libxml-dir=<DIR>])
    ])
  fi

  dnl
  dnl Check for expat only if --with-libexpat-dir is used.
  dnl
  if test "$HYSS_LIBEXPAT_DIR" != "no"; then
    for i in $HYSS_XML $HYSS_LIBEXPAT_DIR /usr /usr/local; do
      if test -f "$i/$HYSS_LIBDIR/libexpat.a" || test -f "$i/$HYSS_LIBDIR/libexpat.$SHLIB_SUFFIX_NAME"; then
        EXPAT_DIR=$i
        break
      fi
    done

    if test -z "$EXPAT_DIR"; then
      AC_MSG_ERROR([not found. Please reinstall the expat distribution.])
    fi

    HYSS_ADD_INCLUDE($EXPAT_DIR/include)
    HYSS_ADD_LIBRARY_WITH_PATH(expat, $EXPAT_DIR/$HYSS_LIBDIR, WDDX_SHARED_LIBADD)
    AC_DEFINE(HAVE_LIBEXPAT, 1, [ ])
  fi

  AC_DEFINE(HAVE_WDDX, 1, [ ])
  HYSS_NEW_EXTENSION(wddx, wddx.c, $ext_shared)
  HYSS_ADD_EXTENSION_DEP(wddx, libxml)
  HYSS_SUBST(XMLRPC_SHARED_LIBADD)
fi
