/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SQLITE_STRUCTS_H
#define HYSS_SQLITE_STRUCTS_H

#include <sqlite3.h>

/* for backwards compatibility reasons */
#ifndef SQLITE_OPEN_READONLY
#define SQLITE_OPEN_READONLY 0x00000001
#endif

#ifndef SQLITE_OPEN_READWRITE
#define SQLITE_OPEN_READWRITE 0x00000002
#endif

#ifndef SQLITE_OPEN_CREATE
#define SQLITE_OPEN_CREATE 0x00000004
#endif

/* Structure for SQLite Statement Parameter. */
struct hyss_sqlite3_bound_param  {
	gear_long param_number;
	gear_string *name;
	gear_long type;
	zval parameter;
};

struct hyss_sqlite3_fci {
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;
};

/* Structure for SQLite function. */
typedef struct _hyss_sqlite3_func {
	struct _hyss_sqlite3_func *next;

	const char *func_name;
	int argc;

	zval func, step, fini;
	struct hyss_sqlite3_fci afunc, astep, afini;
} hyss_sqlite3_func;

/* Structure for SQLite collation function */
typedef struct _hyss_sqlite3_collation {
	struct _hyss_sqlite3_collation *next;

	const char *collation_name;
	zval cmp_func;
	struct hyss_sqlite3_fci fci;
} hyss_sqlite3_collation;

/* Structure for SQLite Database object. */
typedef struct _hyss_sqlite3_db_object  {
	int initialised;
	sqlite3 *db;
	hyss_sqlite3_func *funcs;
	hyss_sqlite3_collation *collations;

	gear_bool exception;

	gear_llist free_list;
	gear_object zo;
} hyss_sqlite3_db_object;

static inline hyss_sqlite3_db_object *hyss_sqlite3_db_from_obj(gear_object *obj) {
	return (hyss_sqlite3_db_object*)((char*)(obj) - XtOffsetOf(hyss_sqlite3_db_object, zo));
}

#define Z_SQLITE3_DB_P(zv)  hyss_sqlite3_db_from_obj(Z_OBJ_P((zv)))

/* Structure for SQLite Database object. */
typedef struct _hyss_sqlite3_agg_context  {
	zval zval_context;
	gear_long row_count;
} hyss_sqlite3_agg_context;

typedef struct _hyss_sqlite3_stmt_object hyss_sqlite3_stmt;
typedef struct _hyss_sqlite3_result_object hyss_sqlite3_result;

/* sqlite3 objects to be destroyed */
typedef struct _hyss_sqlite3_free_list {
	zval stmt_obj_zval;
	hyss_sqlite3_stmt *stmt_obj;
} hyss_sqlite3_free_list;

/* Structure for SQLite Result object. */
struct _hyss_sqlite3_result_object  {
	hyss_sqlite3_db_object *db_obj;
	hyss_sqlite3_stmt *stmt_obj;
	zval stmt_obj_zval;

	int is_prepared_statement;
	int complete;
	gear_object zo;
};

static inline hyss_sqlite3_result *hyss_sqlite3_result_from_obj(gear_object *obj) {
	return (hyss_sqlite3_result*)((char*)(obj) - XtOffsetOf(hyss_sqlite3_result, zo));
}

#define Z_SQLITE3_RESULT_P(zv)  hyss_sqlite3_result_from_obj(Z_OBJ_P((zv)))

/* Structure for SQLite Statement object. */
struct _hyss_sqlite3_stmt_object  {
	sqlite3_stmt *stmt;
	hyss_sqlite3_db_object *db_obj;
	zval db_obj_zval;

	int initialised;

	/* Keep track of the zvals for bound parameters */
	HashTable *bound_params;
	gear_object zo;
};

static inline hyss_sqlite3_stmt *hyss_sqlite3_stmt_from_obj(gear_object *obj) {
	return (hyss_sqlite3_stmt*)((char*)(obj) - XtOffsetOf(hyss_sqlite3_stmt, zo));
}

#define Z_SQLITE3_STMT_P(zv)  hyss_sqlite3_stmt_from_obj(Z_OBJ_P((zv)))

#endif

