/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SQLITE3_H
#define HYSS_SQLITE3_H

#define HYSS_SQLITE3_VERSION	HYSS_VERSION

extern gear_capi_entry sqlite3_capi_entry;
#define hyssext_sqlite3_ptr &sqlite3_capi_entry

GEAR_BEGIN_CAPI_GLOBALS(sqlite3)
	char *extension_dir;
	int dbconfig_defensive;
GEAR_END_CAPI_GLOBALS(sqlite3)

#ifdef ZTS
# define SQLITE3G(v) PBCG(sqlite3_globals_id, gear_sqlite3_globals *, v)
# ifdef COMPILE_DL_SQLITE3
GEAR_PBCLS_CACHE_EXTERN()
# endif
#else
# define SQLITE3G(v) (sqlite3_globals.v)
#endif

#define HYSS_SQLITE3_ASSOC	1<<0
#define HYSS_SQLITE3_NUM		1<<1
#define HYSS_SQLITE3_BOTH	(HYSS_SQLITE3_ASSOC|HYSS_SQLITE3_NUM)

#endif
