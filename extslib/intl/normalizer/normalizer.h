/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NORMALIZER_NORMALIZER_H
#define NORMALIZER_NORMALIZER_H

#include <hyss.h>
#include <unicode/utypes.h>
#if U_ICU_VERSION_MAJOR_NUM < 56
#include <unicode/unorm.h>

#define NORMALIZER_NONE UNORM_NONE
#define NORMALIZER_FORM_D UNORM_NFD
#define NORMALIZER_NFD UNORM_NFD
#define NORMALIZER_FORM_KD UNORM_NFKD
#define NORMALIZER_NFKD UNORM_NFKD
#define NORMALIZER_FORM_C UNORM_NFC
#define NORMALIZER_NFC UNORM_NFC
#define NORMALIZER_FORM_KC UNORM_NFKC
#define NORMALIZER_NFKC UNORM_NFKC
#define NORMALIZER_DEFAULT UNORM_DEFAULT
#else
#include <unicode/unorm2.h>

#define NORMALIZER_NONE 0x2
#define NORMALIZER_FORM_D 0x4
#define NORMALIZER_NFD NORMALIZER_FORM_D
#define NORMALIZER_FORM_KD 0x8
#define NORMALIZER_NFKD NORMALIZER_FORM_KD
#define NORMALIZER_FORM_C 0x10
#define NORMALIZER_NFC NORMALIZER_FORM_C
#define NORMALIZER_FORM_KC 0x20
#define NORMALIZER_NFKC NORMALIZER_FORM_KC
#define NORMALIZER_FORM_KC_CF 0x30
#define NORMALIZER_NFKC_CF NORMALIZER_FORM_KC_CF
#define NORMALIZER_DEFAULT NORMALIZER_FORM_C
#endif

void normalizer_register_constants( INIT_FUNC_ARGS );

#endif // NORMALIZER_NORMALIZER_H
