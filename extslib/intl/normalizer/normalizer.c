/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "normalizer_class.h"
#include "normalizer.h"

#include <unicode/utypes.h>
#include <unicode/unorm.h>
#include <unicode/ustring.h>

/* {{{ normalizer_register_constants
 * Register constants common for the both (OO and procedural)
 * APIs.
 */
void normalizer_register_constants( INIT_FUNC_ARGS )
{
	if( !Normalizer_ce_ptr )
	{
		gear_error( E_ERROR, "Normalizer class not defined" );
		return;
	}

	#define NORMALIZER_EXPOSE_CONST(x) REGISTER_LONG_CONSTANT(#x, x, CONST_PERSISTENT | CONST_CS)
	#define NORMALIZER_EXPOSE_CLASS_CONST(x) gear_declare_class_constant_long( Normalizer_ce_ptr, GEAR_STRS( #x ) - 1, NORMALIZER_##x );
	#define NORMALIZER_EXPOSE_CUSTOM_CLASS_CONST(name, value) gear_declare_class_constant_long( Normalizer_ce_ptr, GEAR_STRS( name ) - 1, value );

	/* Normalization form constants */
	NORMALIZER_EXPOSE_CLASS_CONST( NONE );
	NORMALIZER_EXPOSE_CLASS_CONST( FORM_D );
	NORMALIZER_EXPOSE_CLASS_CONST( NFD );
	NORMALIZER_EXPOSE_CLASS_CONST( FORM_KD );
	NORMALIZER_EXPOSE_CLASS_CONST( NFKD );
	NORMALIZER_EXPOSE_CLASS_CONST( FORM_C );
	NORMALIZER_EXPOSE_CLASS_CONST( NFC );
	NORMALIZER_EXPOSE_CLASS_CONST( FORM_KC );
	NORMALIZER_EXPOSE_CLASS_CONST( NFKC );
#if U_ICU_VERSION_MAJOR_NUM >= 56
	NORMALIZER_EXPOSE_CLASS_CONST( FORM_KC_CF );
	NORMALIZER_EXPOSE_CLASS_CONST( NFKC_CF );
#endif

	#undef NORMALIZER_EXPOSE_CUSTOM_CLASS_CONST
	#undef NORMALIZER_EXPOSE_CLASS_CONST
	#undef NORMALIZER_EXPOSE_CONST
}
/* }}} */

