/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "normalizer_class.h"
#include "hyss_intl.h"
#include "normalizer_normalize.h"
#include "intl_error.h"

#include <unicode/unorm.h>

gear_class_entry *Normalizer_ce_ptr = NULL;

/*
 * 'Normalizer' class registration structures & functions
 */

/* {{{ Normalizer methods arguments info */

GEAR_BEGIN_ARG_INFO_EX( normalizer_args, 0, 0, 1 )
	GEAR_ARG_INFO( 0, input )
	GEAR_ARG_INFO( 0, form )
GEAR_END_ARG_INFO()

#if U_ICU_VERSION_MAJOR_NUM >= 56
GEAR_BEGIN_ARG_INFO_EX( decomposition_args, 0, 0, 1 )
	GEAR_ARG_INFO( 0, input )
GEAR_END_ARG_INFO();
#endif

/* }}} */

/* {{{ Normalizer_class_functions
 * Every 'Normalizer' class method has an entry in this table
 */

static const gear_function_entry Normalizer_class_functions[] = {
	GEAR_FENTRY( normalize, GEAR_FN( normalizer_normalize ), normalizer_args, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( isNormalized, GEAR_FN( normalizer_is_normalized ), normalizer_args, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
#if U_ICU_VERSION_MAJOR_NUM >= 56
	GEAR_FENTRY( getRawDecomposition, GEAR_FN( normalizer_get_raw_decomposition ), decomposition_args, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
#endif
	HYSS_FE_END
};
/* }}} */

/* {{{ normalizer_register_Normalizer_class
 * Initialize 'Normalizer' class
 */
void normalizer_register_Normalizer_class( void )
{
	gear_class_entry ce;

	/* Create and register 'Normalizer' class. */
	INIT_CLASS_ENTRY( ce, "Normalizer", Normalizer_class_functions );
	ce.create_object = NULL;
	Normalizer_ce_ptr = gear_register_internal_class( &ce );

	/* Declare 'Normalizer' class properties. */
	if( !Normalizer_ce_ptr )
	{
		gear_error( E_ERROR,
			"Normalizer: attempt to create properties "
			"on a non-registered class." );
		return;
	}
}
/* }}} */

