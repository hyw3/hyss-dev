/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RESOURCEBUNDLE_CLASS_H
#define RESOURCEBUNDLE_CLASS_H

#include <unicode/ures.h>

#include <gear.h>
#include "hyss.h"

#include "intl_error.h"

typedef struct {
	intl_error      error;

	UResourceBundle *me;
	UResourceBundle *child;
	gear_object     gear;
} ResourceBundle_object;

static inline ResourceBundle_object *hyss_intl_resourcebundle_fetch_object(gear_object *obj) {
	return (ResourceBundle_object *)((char*)(obj) - XtOffsetOf(ResourceBundle_object, gear));
}
#define Z_INTL_RESOURCEBUNDLE_P(zv) hyss_intl_resourcebundle_fetch_object(Z_OBJ_P(zv))

#define RESOURCEBUNDLE_METHOD_INIT_VARS		INTL_METHOD_INIT_VARS(ResourceBundle, rb)
#define RESOURCEBUNDLE_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_RESOURCEBUNDLE, rb)
#define RESOURCEBUNDLE_METHOD_FETCH_OBJECT							\
	INTL_METHOD_FETCH_OBJECT(INTL_RESOURCEBUNDLE, rb);					\
	if (RESOURCEBUNDLE_OBJECT(rb) == NULL) {						\
		intl_errors_set(&rb->error, U_ILLEGAL_ARGUMENT_ERROR,		\
				"Found unconstructed ResourceBundle", 0);	\
		RETURN_FALSE;												\
	}


#define RESOURCEBUNDLE_OBJECT(rb)			(rb)->me

void resourcebundle_register_class( void );
extern gear_class_entry *ResourceBundle_ce_ptr;

HYSS_FUNCTION( resourcebundle_create );
HYSS_FUNCTION( resourcebundle_get );
HYSS_FUNCTION( resourcebundle_count );
HYSS_FUNCTION( resourcebundle_locales );
HYSS_FUNCTION( resourcebundle_get_error_code );
HYSS_FUNCTION( resourcebundle_get_error_message );

#endif // #ifndef RESOURCEBUNDLE_CLASS_H
