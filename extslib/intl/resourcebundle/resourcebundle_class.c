/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicode/ures.h>
#include <unicode/uenum.h>

#include <gear.h>
#include <Gear/gear_exceptions.h>
#include <Gear/gear_interfaces.h>
#include <hyss.h>

#include "hyss_intl.h"
#include "intl_data.h"
#include "intl_common.h"

#include "resourcebundle/resourcebundle.h"
#include "resourcebundle/resourcebundle_iterator.h"
#include "resourcebundle/resourcebundle_class.h"

gear_class_entry *ResourceBundle_ce_ptr = NULL;

static gear_object_handlers ResourceBundle_object_handlers;

/* {{{ ResourceBundle_object_free */
static void ResourceBundle_object_free( gear_object *object )
{
	ResourceBundle_object *rb = hyss_intl_resourcebundle_fetch_object(object);

	// only free local errors
	intl_error_reset( INTL_DATA_ERROR_P(rb) );

	if (rb->me) {
		ures_close( rb->me );
	}
	if (rb->child) {
		ures_close( rb->child );
	}

	gear_object_std_dtor( &rb->gear );
}
/* }}} */

/* {{{ ResourceBundle_object_create */
static gear_object *ResourceBundle_object_create( gear_class_entry *ce )
{
	ResourceBundle_object *rb;

	rb = gear_object_alloc(sizeof(ResourceBundle_object), ce);

	gear_object_std_init( &rb->gear, ce );
	object_properties_init( &rb->gear, ce);

	intl_error_init( INTL_DATA_ERROR_P(rb) );
	rb->me = NULL;
	rb->child = NULL;

	rb->gear.handlers = &ResourceBundle_object_handlers;

	return &rb->gear;
}
/* }}} */

/* {{{ ResourceBundle_ctor */
static int resourcebundle_ctor(INTERNAL_FUNCTION_PARAMETERS, gear_bool is_constructor)
{
	const char *bundlename;
	size_t		bundlename_len = 0;
	const char *locale;
	size_t		locale_len = 0;
	gear_bool	fallback = 1;
	int         zpp_flags = is_constructor ? GEAR_PARSE_PARAMS_THROW : 0;

	zval                  *object = return_value;
	ResourceBundle_object *rb = Z_INTL_RESOURCEBUNDLE_P( object );

	intl_error_reset( NULL );

	if( gear_parse_parameters_ex( zpp_flags, GEAR_NUM_ARGS(), "s!s!|b",
		&locale, &locale_len, &bundlename, &bundlename_len, &fallback ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"resourcebundle_ctor: unable to parse input parameters", 0 );
		return FAILURE;
	}

	INTL_CHECK_LOCALE_LEN_OR_FAILURE(locale_len);

	if (locale == NULL) {
		locale = intl_locale_get_default();
	}

	if (bundlename_len >= MAXPATHLEN) {
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,	"Bundle name too long", 0 );
		zval_ptr_dtor(return_value);
		ZVAL_NULL(return_value);
		return FAILURE;
	}

	if (fallback) {
		rb->me = ures_open(bundlename, locale, &INTL_DATA_ERROR_CODE(rb));
	} else {
		rb->me = ures_openDirect(bundlename, locale, &INTL_DATA_ERROR_CODE(rb));
	}

	INTL_CTOR_CHECK_STATUS(rb, "resourcebundle_ctor: Cannot load libICU resource bundle");

	if (!fallback && (INTL_DATA_ERROR_CODE(rb) == U_USING_FALLBACK_WARNING ||
			INTL_DATA_ERROR_CODE(rb) == U_USING_DEFAULT_WARNING)) {
		char *pbuf;
		intl_errors_set_code(NULL, INTL_DATA_ERROR_CODE(rb));
		spprintf(&pbuf, 0, "resourcebundle_ctor: Cannot load libICU resource "
				"'%s' without fallback from %s to %s",
				bundlename ? bundlename : "(default data)", locale,
				ures_getLocaleByType(
					rb->me, ULOC_ACTUAL_LOCALE, &INTL_DATA_ERROR_CODE(rb)));
		intl_errors_set_custom_msg(INTL_DATA_ERROR_P(rb), pbuf, 1);
		efree(pbuf);
		return FAILURE;
	}

	return SUCCESS;
}
/* }}} */

/* {{{ arginfo_resourcebundle__construct */
GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle___construct, 0, 0, 2 )
	GEAR_ARG_INFO( 0, locale )
	GEAR_ARG_INFO( 0, bundlename )
	GEAR_ARG_INFO( 0, fallback )
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ proto ResourceBundle::__construct( string $locale [, string $bundlename [, bool $fallback = true ]] )
 * ResourceBundle object constructor
 */
HYSS_METHOD( ResourceBundle, __construct )
{
	gear_error_handling error_handling;

	gear_replace_error_handling(EH_THROW, IntlException_ce_ptr, &error_handling);
	return_value = getThis();
	if (resourcebundle_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1) == FAILURE) {
		if (!EG(exception)) {
			gear_throw_exception(IntlException_ce_ptr, "Constructor failed", 0);
		}
	}
	gear_restore_error_handling(&error_handling);
}
/* }}} */

/* {{{ proto ResourceBundle ResourceBundle::create( string $locale [, string $bundlename [, bool $fallback = true ]] )
proto ResourceBundle resourcebundle_create( string $locale [, string $bundlename [, bool $fallback = true ]] )
*/
HYSS_FUNCTION( resourcebundle_create )
{
	object_init_ex( return_value, ResourceBundle_ce_ptr );
	if (resourcebundle_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0) == FAILURE) {
		zval_ptr_dtor(return_value);
		RETURN_NULL();
	}
}
/* }}} */

/* {{{ resourcebundle_array_fetch */
static void resourcebundle_array_fetch(zval *object, zval *offset, zval *return_value, int fallback)
{
	int32_t     meindex = 0;
	char *      mekey = NULL;
    gear_bool    is_numeric = 0;
	char         *pbuf;
	ResourceBundle_object *rb;

	intl_error_reset( NULL );
	RESOURCEBUNDLE_METHOD_FETCH_OBJECT;

	if(Z_TYPE_P(offset) == IS_LONG) {
		is_numeric = 1;
		meindex = (int32_t)Z_LVAL_P(offset);
		rb->child = ures_getByIndex( rb->me, meindex, rb->child, &INTL_DATA_ERROR_CODE(rb) );
	} else if(Z_TYPE_P(offset) == IS_STRING) {
		mekey = Z_STRVAL_P(offset);
		rb->child = ures_getByKey(rb->me, mekey, rb->child, &INTL_DATA_ERROR_CODE(rb) );
	} else {
		intl_errors_set(INTL_DATA_ERROR_P(rb), U_ILLEGAL_ARGUMENT_ERROR,
			"resourcebundle_get: index should be integer or string", 0);
		RETURN_NULL();
	}

	intl_error_set_code( NULL, INTL_DATA_ERROR_CODE(rb) );
	if (U_FAILURE(INTL_DATA_ERROR_CODE(rb))) {
		if (is_numeric) {
			spprintf( &pbuf, 0, "Cannot load resource element %d", meindex );
		} else {
			spprintf( &pbuf, 0, "Cannot load resource element '%s'", mekey );
		}
		intl_errors_set_custom_msg( INTL_DATA_ERROR_P(rb), pbuf, 1 );
		efree(pbuf);
		RETURN_NULL();
	}

	if (!fallback && (INTL_DATA_ERROR_CODE(rb) == U_USING_FALLBACK_WARNING || INTL_DATA_ERROR_CODE(rb) == U_USING_DEFAULT_WARNING)) {
		UErrorCode icuerror;
		const char * locale = ures_getLocaleByType( rb->me, ULOC_ACTUAL_LOCALE, &icuerror );
		if (is_numeric) {
			spprintf( &pbuf, 0, "Cannot load element %d without fallback from to %s", meindex, locale );
		} else {
			spprintf( &pbuf, 0, "Cannot load element '%s' without fallback from to %s", mekey, locale );
		}
		intl_errors_set_custom_msg( INTL_DATA_ERROR_P(rb), pbuf, 1 );
		efree(pbuf);
		RETURN_NULL();
	}

	resourcebundle_extract_value( return_value, rb );
}
/* }}} */

/* {{{ resourcebundle_array_get */
zval *resourcebundle_array_get(zval *object, zval *offset, int type, zval *rv)
{
	if(offset == NULL) {
		hyss_error( E_ERROR, "Cannot apply [] to ResourceBundle object" );
	}
	ZVAL_NULL(rv);
	resourcebundle_array_fetch(object, offset, rv, 1);
	return rv;
}
/* }}} */

/* {{{ arginfo_resourcebundle_get */
GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_get, 0, 0, 1 )
	GEAR_ARG_INFO( 0, index )
	GEAR_ARG_INFO( 0, fallback )
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ proto mixed ResourceBundle::get( int|string $resindex [, bool $fallback = true ] )
 * proto mixed resourcebundle_get( ResourceBundle $rb, int|string $resindex [, bool $fallback = true ] )
 * Get resource identified by numerical index or key name.
 */
HYSS_FUNCTION( resourcebundle_get )
{
	gear_bool   fallback = 1;
	zval *		offset;
	zval *      object;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oz|b",	&object, ResourceBundle_ce_ptr, &offset, &fallback ) == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"resourcebundle_get: unable to parse input params", 0);
		RETURN_FALSE;
	}

	resourcebundle_array_fetch(object, offset, return_value, fallback);
}
/* }}} */

/* {{{ resourcebundle_array_count */
int resourcebundle_array_count(zval *object, gear_long *count)
{
	ResourceBundle_object *rb;
	RESOURCEBUNDLE_METHOD_FETCH_OBJECT_NO_CHECK;

	if (rb->me == NULL) {
		intl_errors_set(&rb->error, U_ILLEGAL_ARGUMENT_ERROR,
				"Found unconstructed ResourceBundle", 0);
		return 0;
	}

	*count = ures_getSize( rb->me );

	return SUCCESS;
}
/* }}} */

/* {{{ arginfo_resourcebundle_count */
GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_count, 0, 0, 0 )
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ proto int ResourceBundle::count()
 * proto int resourcebundle_count( ResourceBundle $bundle )
 * Get resources count
 */
HYSS_FUNCTION( resourcebundle_count )
{
	int32_t                len;
	RESOURCEBUNDLE_METHOD_INIT_VARS;

	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O", &object, ResourceBundle_ce_ptr ) == FAILURE ) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"resourcebundle_count: unable to parse input params", 0);
		RETURN_FALSE;
	}

	RESOURCEBUNDLE_METHOD_FETCH_OBJECT;

	len = ures_getSize( rb->me );
	RETURN_LONG( len );
}

/* {{{ arginfo_resourcebundle_getlocales */
GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_getlocales, 0, 0, 1 )
	GEAR_ARG_INFO( 0, bundlename )
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ proto array ResourceBundle::getLocales( string $bundlename )
 * proto array resourcebundle_locales( string $bundlename )
 * Get available locales from ResourceBundle name
 */
HYSS_FUNCTION( resourcebundle_locales )
{
	char * bundlename;
	size_t    bundlename_len = 0;
	const char * entry;
	int entry_len;
	UEnumeration *icuenum;
	UErrorCode   icuerror = U_ZERO_ERROR;

	intl_errors_reset( NULL );

	if( gear_parse_parameters(GEAR_NUM_ARGS(), "s", &bundlename, &bundlename_len ) == FAILURE )
	{
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"resourcebundle_locales: unable to parse input params", 0);
		RETURN_FALSE;
	}

	if (bundlename_len >= MAXPATHLEN) {
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,	"resourcebundle_locales: bundle name too long", 0 );
		RETURN_FALSE;
	}

	if(bundlename_len == 0) {
		// fetch default locales list
		bundlename = NULL;
	}

	icuenum = ures_openAvailableLocales( bundlename, &icuerror );
	INTL_CHECK_STATUS(icuerror, "Cannot fetch locales list");

	uenum_reset( icuenum, &icuerror );
	INTL_CHECK_STATUS(icuerror, "Cannot iterate locales list");

	array_init( return_value );
	while ((entry = uenum_next( icuenum, &entry_len, &icuerror ))) {
		add_next_index_stringl( return_value, (char *) entry, entry_len);
	}
	uenum_close( icuenum );
}
/* }}} */

/* {{{ arginfo_resourcebundle_get_error_code */
GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_get_error_code, 0, 0, 0 )
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ proto string ResourceBundle::getErrorCode( )
 * proto string resourcebundle_get_error_code( ResourceBundle $bundle )
 * Get text description for ResourceBundle's last error code.
 */
HYSS_FUNCTION( resourcebundle_get_error_code )
{
	RESOURCEBUNDLE_METHOD_INIT_VARS;

	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O",
		&object, ResourceBundle_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"resourcebundle_get_error_code: unable to parse input params", 0 );
		RETURN_FALSE;
	}

	rb = Z_INTL_RESOURCEBUNDLE_P( object );

	RETURN_LONG(INTL_DATA_ERROR_CODE(rb));
}
/* }}} */

/* {{{ arginfo_resourcebundle_get_error_message */
GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_get_error_message, 0, 0, 0 )
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ proto string ResourceBundle::getErrorMessage( )
 * proto string resourcebundle_get_error_message( ResourceBundle $bundle )
 * Get text description for ResourceBundle's last error.
 */
HYSS_FUNCTION( resourcebundle_get_error_message )
{
	gear_string* message = NULL;
	RESOURCEBUNDLE_METHOD_INIT_VARS;

	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O",
		&object, ResourceBundle_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"resourcebundle_get_error_message: unable to parse input params", 0 );
		RETURN_FALSE;
	}

	rb = Z_INTL_RESOURCEBUNDLE_P( object );
	message = intl_error_get_message(INTL_DATA_ERROR_P(rb));
	RETURN_STR(message);
}
/* }}} */

/* {{{ ResourceBundle_class_functions
 * Every 'ResourceBundle' class method has an entry in this table
 */
static const gear_function_entry ResourceBundle_class_functions[] = {
	HYSS_ME( ResourceBundle, __construct, arginfo_resourcebundle___construct, GEAR_ACC_PUBLIC|GEAR_ACC_CTOR )
	GEAR_NAMED_ME( create, GEAR_FN( resourcebundle_create ), arginfo_resourcebundle___construct, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_NAMED_ME( get, GEAR_FN(resourcebundle_get), arginfo_resourcebundle_get, GEAR_ACC_PUBLIC )
	GEAR_NAMED_ME( count, GEAR_FN(resourcebundle_count), arginfo_resourcebundle_count, GEAR_ACC_PUBLIC )
	GEAR_NAMED_ME( getLocales, GEAR_FN(resourcebundle_locales), arginfo_resourcebundle_getlocales, GEAR_ACC_PUBLIC | GEAR_ACC_STATIC )
	GEAR_NAMED_ME( getErrorCode, GEAR_FN(resourcebundle_get_error_code), arginfo_resourcebundle_get_error_code, GEAR_ACC_PUBLIC )
	GEAR_NAMED_ME( getErrorMessage, GEAR_FN(resourcebundle_get_error_message), arginfo_resourcebundle_get_error_message, GEAR_ACC_PUBLIC )
	HYSS_FE_END
};
/* }}} */

/* {{{ resourcebundle_register_class
 * Initialize 'ResourceBundle' class
 */
void resourcebundle_register_class( void )
{
	gear_class_entry ce;

	INIT_CLASS_ENTRY( ce, "ResourceBundle", ResourceBundle_class_functions );

	ce.create_object = ResourceBundle_object_create;
	ce.get_iterator = resourcebundle_get_iterator;

	ResourceBundle_ce_ptr = gear_register_internal_class( &ce );

	ResourceBundle_object_handlers = std_object_handlers;
	ResourceBundle_object_handlers.offset = XtOffsetOf(ResourceBundle_object, gear);
	ResourceBundle_object_handlers.clone_obj	  = NULL; /* ICU ResourceBundle has no clone implementation */
	ResourceBundle_object_handlers.free_obj = ResourceBundle_object_free;
	ResourceBundle_object_handlers.read_dimension = resourcebundle_array_get;
	ResourceBundle_object_handlers.count_elements = resourcebundle_array_count;

	/* gear_class_implements(ResourceBundle_ce_ptr, 1, gear_ce_traversable); */
        gear_class_implements(ResourceBundle_ce_ptr, 2, gear_ce_traversable, gear_ce_countable);
}
/* }}} */

