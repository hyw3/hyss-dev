/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyss.h>
#include <gear.h>
#include <gear_API.h>

#include "resourcebundle/resourcebundle.h"
#include "resourcebundle/resourcebundle_class.h"
#include "resourcebundle/resourcebundle_iterator.h"

/*
 * Although libicu offers iterator functions, they are not used here: libicu does iterate
 * irrespective of array indices. Those cannot be recreated afterwards. Arrays as well as tables
 * can however be accessed by numerical index, with table keys readable ex post.
 */

/* {{{ resourcebundle_iterator_read */
static void resourcebundle_iterator_read( ResourceBundle_iterator *iterator )
{
	UErrorCode icuerror = U_ZERO_ERROR;
	ResourceBundle_object *rb = iterator->subject;

	rb->child = ures_getByIndex( rb->me, iterator->i, rb->child, &icuerror );

	if (U_SUCCESS(icuerror)) {
		/* ATTN: key extraction must be the first thing to do... rb->child might be reset in read! */
		if (iterator->is_table) {
			iterator->currentkey = estrdup( ures_getKey( rb->child ) );
		}
		resourcebundle_extract_value( &iterator->current, rb );
	}
	else {
		// gear_throw_exception( spl_ce_OutOfRangeException, "Running past end of ResourceBundle", 0);
		ZVAL_UNDEF(&iterator->current);
	}
}
/* }}} */

/* {{{ resourcebundle_iterator_invalidate */
static void resourcebundle_iterator_invalidate( gear_object_iterator *iter )
{
	ResourceBundle_iterator *iterator = (ResourceBundle_iterator *) iter;

	if (!Z_ISUNDEF(iterator->current)) {
		zval_ptr_dtor( &iterator->current );
		ZVAL_UNDEF(&iterator->current);
	}
	if (iterator->currentkey) {
		efree( iterator->currentkey );
		iterator->currentkey = NULL;
	}
}
/* }}} */

/* {{{ resourcebundle_iterator_dtor */
static void resourcebundle_iterator_dtor( gear_object_iterator *iter )
{
	ResourceBundle_iterator *iterator = (ResourceBundle_iterator *) iter;
	zval                    *object = &iterator->intern.data;

	resourcebundle_iterator_invalidate( iter );

	zval_ptr_dtor(object);
}
/* }}} */

/* {{{ resourcebundle_iterator_has_more */
static int resourcebundle_iterator_has_more( gear_object_iterator *iter )
{
	ResourceBundle_iterator *iterator = (ResourceBundle_iterator *) iter;
	return (iterator->i < iterator->length) ? SUCCESS : FAILURE;
}
/* }}} */

/* {{{ resourcebundle_iterator_current */
static zval *resourcebundle_iterator_current( gear_object_iterator *iter )
{
	ResourceBundle_iterator *iterator = (ResourceBundle_iterator *) iter;
	if (Z_ISUNDEF(iterator->current)) {
		resourcebundle_iterator_read( iterator);
	}
	return &iterator->current;
}
/* }}} */

/* {{{ resourcebundle_iterator_key */
static void resourcebundle_iterator_key( gear_object_iterator *iter, zval *key )
{
	ResourceBundle_iterator *iterator = (ResourceBundle_iterator *) iter;

	if (Z_ISUNDEF(iterator->current)) {
		resourcebundle_iterator_read( iterator);
	}

	if (iterator->is_table) {
		ZVAL_STRING(key, iterator->currentkey);
	} else {
		ZVAL_LONG(key, iterator->i);
	}
}
/* }}} */

/* {{{ resourcebundle_iterator_step */
static void resourcebundle_iterator_step( gear_object_iterator *iter )
{
	ResourceBundle_iterator *iterator = (ResourceBundle_iterator *) iter;

	iterator->i++;
	resourcebundle_iterator_invalidate( iter );
}
/* }}} */

/* {{{ resourcebundle_iterator_has_reset */
static void resourcebundle_iterator_reset( gear_object_iterator *iter )
{
	ResourceBundle_iterator *iterator = (ResourceBundle_iterator *) iter;

	iterator->i = 0;
	resourcebundle_iterator_invalidate( iter );
}
/* }}} */

/* {{{ resourcebundle_iterator_funcs */
static const gear_object_iterator_funcs resourcebundle_iterator_funcs = {
	resourcebundle_iterator_dtor,
	resourcebundle_iterator_has_more,
	resourcebundle_iterator_current,
	resourcebundle_iterator_key,
	resourcebundle_iterator_step,
	resourcebundle_iterator_reset,
	resourcebundle_iterator_invalidate
};
/* }}} */

/* {{{ resourcebundle_get_iterator */
gear_object_iterator *resourcebundle_get_iterator( gear_class_entry *ce, zval *object, int byref )
{
	ResourceBundle_object   *rb = Z_INTL_RESOURCEBUNDLE_P(object );
	ResourceBundle_iterator *iterator = emalloc( sizeof( ResourceBundle_iterator ) );

	if (byref) {
	     hyss_error( E_ERROR, "ResourceBundle does not support writable iterators" );
	}

	gear_iterator_init(&iterator->intern);
	ZVAL_COPY(&iterator->intern.data, object);
	iterator->intern.funcs = &resourcebundle_iterator_funcs;

	iterator->subject = rb;

	/* The iterated rb can only be either URES_TABLE or URES_ARRAY
	 * All other types are returned as hyss primitives!
	 */
	iterator->is_table = (ures_getType( rb->me ) == URES_TABLE);
	iterator->length = ures_getSize( rb->me );

	ZVAL_UNDEF(&iterator->current);
	iterator->currentkey = NULL;
	iterator->i = 0;

	return (gear_object_iterator *) iterator;
}
/* }}} */

