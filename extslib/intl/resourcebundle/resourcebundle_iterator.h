/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RESOURCEBUNDLE_ITERATOR_H
#define RESOURCEBUNDLE_ITERATOR_H

#include <gear.h>

#include "resourcebundle/resourcebundle_class.h"

typedef struct {
	gear_object_iterator  intern;
	ResourceBundle_object *subject;
	gear_bool             is_table;
	gear_long             length;
	zval                  current;
	char                  *currentkey;
	gear_long             i;
} ResourceBundle_iterator;

gear_object_iterator *resourcebundle_get_iterator( gear_class_entry *ce, zval *object, int byref );

#endif // #ifndef RESOURCEBUNDLE_ITERATOR_H
