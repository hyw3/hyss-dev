/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicode/ures.h>

#include <gear.h>
#include <gear_API.h>

#include "intl_convert.h"
#include "intl_data.h"
#include "resourcebundle/resourcebundle_class.h"

/* {{{ ResourceBundle_extract_value */
void resourcebundle_extract_value( zval *return_value, ResourceBundle_object *source )
{
	UResType               restype;
	const UChar*           ufield;
	const uint8_t*         bfield;
	const int32_t*         vfield;
	int32_t                ilen;
	int                    i;
	gear_long              lfield;
	ResourceBundle_object* newrb;

	restype = ures_getType( source->child );
	switch (restype)
	{
		case URES_STRING:
			ufield = ures_getString( source->child, &ilen, &INTL_DATA_ERROR_CODE(source) );
			INTL_METHOD_CHECK_STATUS(source, "Failed to retrieve string value");
			INTL_METHOD_RETVAL_UTF8(source, (UChar *)ufield, ilen, 0);
			break;

		case URES_BINARY:
			bfield = ures_getBinary( source->child, &ilen, &INTL_DATA_ERROR_CODE(source) );
			INTL_METHOD_CHECK_STATUS(source, "Failed to retrieve binary value");
			ZVAL_STRINGL( return_value, (char *) bfield, ilen );
			break;

		case URES_INT:
			lfield = ures_getInt( source->child, &INTL_DATA_ERROR_CODE(source) );
			INTL_METHOD_CHECK_STATUS(source, "Failed to retrieve integer value");
			ZVAL_LONG( return_value, lfield );
			break;

		case URES_INT_VECTOR:
			vfield = ures_getIntVector( source->child, &ilen, &INTL_DATA_ERROR_CODE(source) );
			INTL_METHOD_CHECK_STATUS(source, "Failed to retrieve vector value");
			array_init( return_value );
			for (i=0; i<ilen; i++) {
				add_next_index_long( return_value, vfield[i] );
			}
			break;

		case URES_ARRAY:
		case URES_TABLE:
			object_init_ex( return_value, ResourceBundle_ce_ptr );
			newrb = Z_INTL_RESOURCEBUNDLE_P(return_value);
			newrb->me = source->child;
			source->child = NULL;
			intl_errors_reset(INTL_DATA_ERROR_P(source));
			break;

		default:
			intl_errors_set(INTL_DATA_ERROR_P(source), U_ILLEGAL_ARGUMENT_ERROR, "Unknown resource type", 0);
			RETURN_FALSE;
			break;
	}
}
/* }}} */

