/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTL_BREAKITERATOR_ITERATORS_H
#define INTL_BREAKITERATOR_ITERATORS_H

#include <unicode/umachine.h>

U_CDECL_BEGIN
#include <math.h>
#include <hyss.h>
U_CDECL_END

typedef enum {
	PARTS_ITERATOR_KEY_SEQUENTIAL,
	PARTS_ITERATOR_KEY_LEFT,
	PARTS_ITERATOR_KEY_RIGHT,
} parts_iter_key_type;

#ifdef __cplusplus
void IntlIterator_from_BreakIterator_parts(zval *break_iter_zv,
										   zval *object,
										   parts_iter_key_type key_type);
#endif

U_CFUNC gear_object_iterator *_breakiterator_get_iterator(
		gear_class_entry *ce, zval *object, int by_ref);
U_CFUNC void breakiterator_register_IntlPartsIterator_class(void);

#endif
