/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unicode/brkiter.h>
#include <unicode/rbbi.h>
#include "codepointiterator_internal.h"

#include "breakiterator_iterators.h"

#include <typeinfo>

extern "C" {
#define USE_BREAKITERATOR_POINTER 1
#include "breakiterator_class.h"
#include "breakiterator_methods.h"
#include "rulebasedbreakiterator_methods.h"
#include "codepointiterator_methods.h"
#include <gear_exceptions.h>
#include <gear_interfaces.h>
#include <assert.h>
}

using HYSS::CodePointBreakIterator;
using icu::RuleBasedBreakIterator;

/* {{{ Global variables */
gear_class_entry *BreakIterator_ce_ptr;
gear_class_entry *RuleBasedBreakIterator_ce_ptr;
gear_class_entry *CodePointBreakIterator_ce_ptr;
gear_object_handlers BreakIterator_handlers;
/* }}} */

U_CFUNC	void breakiterator_object_create(zval *object,
										 BreakIterator *biter, int brand_new)
{
	UClassID classId = biter->getDynamicClassID();
	gear_class_entry *ce;

	if (classId == RuleBasedBreakIterator::getStaticClassID()) {
		ce = RuleBasedBreakIterator_ce_ptr;
	} else if (classId == CodePointBreakIterator::getStaticClassID()) {
		ce = CodePointBreakIterator_ce_ptr;
	} else {
		ce = BreakIterator_ce_ptr;
	}

	if (brand_new) {
		object_init_ex(object, ce);
	}
	breakiterator_object_construct(object, biter);
}

U_CFUNC void breakiterator_object_construct(zval *object,
											BreakIterator *biter)
{
	BreakIterator_object *bio;

	BREAKITER_METHOD_FETCH_OBJECT_NO_CHECK; //populate to from object
	assert(bio->biter == NULL);
	bio->biter = biter;
}

/* {{{ compare handler for BreakIterator */
static int BreakIterator_compare_objects(zval *object1,
										 zval *object2)
{
	BreakIterator_object	*bio1,
							*bio2;

	bio1 = Z_INTL_BREAKITERATOR_P(object1);
	bio2 = Z_INTL_BREAKITERATOR_P(object2);

	if (bio1->biter == NULL || bio2->biter == NULL) {
		return bio1->biter == bio2->biter ? 0 : 1;
	}

	return *bio1->biter == *bio2->biter ? 0 : 1;
}
/* }}} */

/* {{{ clone handler for BreakIterator */
static gear_object *BreakIterator_clone_obj(zval *object)
{
	BreakIterator_object	*bio_orig,
							*bio_new;
	gear_object				*ret_val;

	bio_orig = Z_INTL_BREAKITERATOR_P(object);
	intl_errors_reset(INTL_DATA_ERROR_P(bio_orig));

	ret_val = BreakIterator_ce_ptr->create_object(Z_OBJCE_P(object));
	bio_new  = hyss_intl_breakiterator_fetch_object(ret_val);

	gear_objects_clone_members(&bio_new->zo, &bio_orig->zo);

	if (bio_orig->biter != NULL) {
		BreakIterator *new_biter;

		new_biter = bio_orig->biter->clone();
		if (!new_biter) {
			gear_string *err_msg;
			intl_errors_set_code(BREAKITER_ERROR_P(bio_orig),
				U_MEMORY_ALLOCATION_ERROR);
			intl_errors_set_custom_msg(BREAKITER_ERROR_P(bio_orig),
				"Could not clone BreakIterator", 0);
			err_msg = intl_error_get_message(BREAKITER_ERROR_P(bio_orig));
			gear_throw_exception(NULL, ZSTR_VAL(err_msg), 0);
			gear_string_free(err_msg);
		} else {
			bio_new->biter = new_biter;
			ZVAL_COPY(&bio_new->text, &bio_orig->text);
		}
	} else {
		gear_throw_exception(NULL, "Cannot clone unconstructed BreakIterator", 0);
	}

	return ret_val;
}
/* }}} */

/* {{{ get_debug_info handler for BreakIterator */
static HashTable *BreakIterator_get_debug_info(zval *object, int *is_temp)
{
	zval val;
	HashTable *debug_info;
	BreakIterator_object	*bio;
	const BreakIterator		*biter;

	*is_temp = 1;

	debug_info = gear_new_array(8);

	bio  = Z_INTL_BREAKITERATOR_P(object);
	biter = bio->biter;

	if (biter == NULL) {
		ZVAL_FALSE(&val);
		gear_hash_str_update(debug_info, "valid", sizeof("valid") - 1, &val);
		return debug_info;
	}
	ZVAL_TRUE(&val);
	gear_hash_str_update(debug_info, "valid", sizeof("valid") - 1, &val);

	if (Z_ISUNDEF(bio->text)) {
		ZVAL_NULL(&val);
		gear_hash_str_update(debug_info, "text", sizeof("text") - 1, &val);
	} else {
		Z_TRY_ADDREF(bio->text);
		gear_hash_str_update(debug_info, "text", sizeof("text") - 1, &bio->text);
	}

	ZVAL_STRING(&val, const_cast<char*>(typeid(*biter).name()));
	gear_hash_str_update(debug_info, "type", sizeof("type") - 1, &val);

	return debug_info;
}
/* }}} */

/* {{{ void breakiterator_object_init(BreakIterator_object* to)
 * Initialize internals of BreakIterator_object not specific to gear standard objects.
 */
static void breakiterator_object_init(BreakIterator_object *bio)
{
	intl_error_init(BREAKITER_ERROR_P(bio));
	bio->biter = NULL;
	ZVAL_UNDEF(&bio->text);
}
/* }}} */

/* {{{ BreakIterator_objects_free */
static void BreakIterator_objects_free(gear_object *object)
{
	BreakIterator_object* bio = hyss_intl_breakiterator_fetch_object(object);

	zval_ptr_dtor(&bio->text);
	if (bio->biter) {
		delete bio->biter;
		bio->biter = NULL;
	}
	intl_error_reset(BREAKITER_ERROR_P(bio));

	gear_object_std_dtor(&bio->zo);
}
/* }}} */

/* {{{ BreakIterator_object_create */
static gear_object *BreakIterator_object_create(gear_class_entry *ce)
{
	BreakIterator_object*	intern;

	intern = (BreakIterator_object*)ecalloc(1, sizeof(BreakIterator_object) + sizeof(zval) * (ce->default_properties_count - 1));

	gear_object_std_init(&intern->zo, ce);
    object_properties_init((gear_object*) intern, ce);
	breakiterator_object_init(intern);

	intern->zo.handlers = &BreakIterator_handlers;

	return &intern->zo;
}
/* }}} */

/* {{{ BreakIterator/RuleBasedBreakIterator methods arguments info */

GEAR_BEGIN_ARG_INFO_EX(ainfo_biter_void, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_biter_locale, 0, 0, 0)
	GEAR_ARG_INFO(0, locale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_biter_setText, 0, 0, 1)
	GEAR_ARG_INFO(0, text)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_biter_next, 0, 0, 0)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_biter_offset, 0, 0, 1)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_biter_get_locale, 0, 0, 1)
	GEAR_ARG_INFO(0, locale_type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_biter_getPartsIterator, 0, 0, 0)
	GEAR_ARG_INFO(0, key_type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_rbbi___construct, 0, 0, 1)
	GEAR_ARG_INFO(0, rules)
	GEAR_ARG_INFO(0, areCompiled)
GEAR_END_ARG_INFO()

/* }}} */

/* {{{ BreakIterator_class_functions
 * Every 'BreakIterator' class method has an entry in this table
 */
static const gear_function_entry BreakIterator_class_functions[] = {
	HYSS_ME(BreakIterator,					__construct,							ainfo_biter_void,					GEAR_ACC_PRIVATE)
	HYSS_ME_MAPPING(createWordInstance,		breakiter_create_word_instance,			ainfo_biter_locale,					GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(createLineInstance,		breakiter_create_line_instance,			ainfo_biter_locale,					GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(createCharacterInstance,	breakiter_create_character_instance,	ainfo_biter_locale,					GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(createSentenceInstance,	breakiter_create_sentence_instance,		ainfo_biter_locale,					GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(createTitleInstance,		breakiter_create_title_instance,		ainfo_biter_locale,					GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(createCodePointInstance,	breakiter_create_code_point_instance,	ainfo_biter_void,					GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getText,					breakiter_get_text,						ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(setText,					breakiter_set_text,						ainfo_biter_setText,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(first,					breakiter_first,						ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(last,					breakiter_last,							ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(previous,				breakiter_previous,						ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(next,					breakiter_next,							ainfo_biter_next,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(current,					breakiter_current,						ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(following,				breakiter_following,					ainfo_biter_offset,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(preceding,				breakiter_preceding,					ainfo_biter_offset,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(isBoundary,				breakiter_is_boundary,					ainfo_biter_offset,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getLocale,				breakiter_get_locale,					ainfo_biter_get_locale,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getPartsIterator,		breakiter_get_parts_iterator,			ainfo_biter_getPartsIterator,		GEAR_ACC_PUBLIC)

	HYSS_ME_MAPPING(getErrorCode,			breakiter_get_error_code,				ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getErrorMessage,			breakiter_get_error_message,			ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_FE_END
};
/* }}} */

/* {{{ RuleBasedBreakIterator_class_functions
 */
static const gear_function_entry RuleBasedBreakIterator_class_functions[] = {
	HYSS_ME(IntlRuleBasedBreakIterator,		__construct,							ainfo_rbbi___construct,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getRules,				rbbi_get_rules,							ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getRuleStatus,			rbbi_get_rule_status,					ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getRuleStatusVec,		rbbi_get_rule_status_vec,				ainfo_biter_void,					GEAR_ACC_PUBLIC)
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 48
	HYSS_ME_MAPPING(getBinaryRules,			rbbi_get_binary_rules,					ainfo_biter_void,					GEAR_ACC_PUBLIC)
#endif
	HYSS_FE_END
};
/* }}} */

/* {{{ CodePointBreakIterator_class_functions
 */
static const gear_function_entry CodePointBreakIterator_class_functions[] = {
	HYSS_ME_MAPPING(getLastCodePoint,		cpbi_get_last_code_point,				ainfo_biter_void,					GEAR_ACC_PUBLIC)
	HYSS_FE_END
};
/* }}} */


/* {{{ breakiterator_register_BreakIterator_class
 * Initialize 'BreakIterator' class
 */
U_CFUNC void breakiterator_register_BreakIterator_class(void)
{
	gear_class_entry ce;

	/* Create and register 'BreakIterator' class. */
	INIT_CLASS_ENTRY(ce, "IntlBreakIterator", BreakIterator_class_functions);
	ce.create_object = BreakIterator_object_create;
	ce.get_iterator = _breakiterator_get_iterator;
	BreakIterator_ce_ptr = gear_register_internal_class(&ce);

	memcpy(&BreakIterator_handlers, &std_object_handlers,
		sizeof BreakIterator_handlers);
	BreakIterator_handlers.offset = XtOffsetOf(BreakIterator_object, zo);
	BreakIterator_handlers.compare_objects = BreakIterator_compare_objects;
	BreakIterator_handlers.clone_obj = BreakIterator_clone_obj;
	BreakIterator_handlers.get_debug_info = BreakIterator_get_debug_info;
	BreakIterator_handlers.free_obj = BreakIterator_objects_free;

	gear_class_implements(BreakIterator_ce_ptr, 1,
			gear_ce_traversable);

	gear_declare_class_constant_long(BreakIterator_ce_ptr,
		"DONE", sizeof("DONE") - 1, BreakIterator::DONE );

	/* Declare constants that are defined in the C header */
#define BREAKITER_DECL_LONG_CONST(name) \
	gear_declare_class_constant_long(BreakIterator_ce_ptr, #name, \
		sizeof(#name) - 1, UBRK_ ## name)

	BREAKITER_DECL_LONG_CONST(WORD_NONE);
	BREAKITER_DECL_LONG_CONST(WORD_NONE_LIMIT);
	BREAKITER_DECL_LONG_CONST(WORD_NUMBER);
	BREAKITER_DECL_LONG_CONST(WORD_NUMBER_LIMIT);
	BREAKITER_DECL_LONG_CONST(WORD_LETTER);
	BREAKITER_DECL_LONG_CONST(WORD_LETTER_LIMIT);
	BREAKITER_DECL_LONG_CONST(WORD_KANA);
	BREAKITER_DECL_LONG_CONST(WORD_KANA_LIMIT);
	BREAKITER_DECL_LONG_CONST(WORD_IDEO);
	BREAKITER_DECL_LONG_CONST(WORD_IDEO_LIMIT);

	BREAKITER_DECL_LONG_CONST(LINE_SOFT);
	BREAKITER_DECL_LONG_CONST(LINE_SOFT_LIMIT);
	BREAKITER_DECL_LONG_CONST(LINE_HARD);
	BREAKITER_DECL_LONG_CONST(LINE_HARD_LIMIT);

	BREAKITER_DECL_LONG_CONST(SENTENCE_TERM);
	BREAKITER_DECL_LONG_CONST(SENTENCE_TERM_LIMIT);
	BREAKITER_DECL_LONG_CONST(SENTENCE_SEP);
	BREAKITER_DECL_LONG_CONST(SENTENCE_SEP_LIMIT);

#undef BREAKITER_DECL_LONG_CONST


	/* Create and register 'RuleBasedBreakIterator' class. */
	INIT_CLASS_ENTRY(ce, "IntlRuleBasedBreakIterator",
			RuleBasedBreakIterator_class_functions);
	RuleBasedBreakIterator_ce_ptr = gear_register_internal_class_ex(&ce,
			BreakIterator_ce_ptr);

	/* Create and register 'CodePointBreakIterator' class. */
	INIT_CLASS_ENTRY(ce, "IntlCodePointBreakIterator",
			CodePointBreakIterator_class_functions);
	CodePointBreakIterator_ce_ptr = gear_register_internal_class_ex(&ce,
			BreakIterator_ce_ptr);
}
/* }}} */
