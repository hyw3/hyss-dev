/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "codepointiterator_internal.h"

extern "C" {
#define USE_BREAKITERATOR_POINTER 1
#include "breakiterator_class.h"
}

using HYSS::CodePointBreakIterator;

static inline CodePointBreakIterator *fetch_cpbi(BreakIterator_object *bio) {
	return (CodePointBreakIterator*)bio->biter;
}

U_CFUNC HYSS_FUNCTION(cpbi_get_last_code_point)
{
	BREAKITER_METHOD_INIT_VARS;
	object = getThis();

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"cpbi_get_last_code_point: bad arguments", 0);
		RETURN_FALSE;
	}

	BREAKITER_METHOD_FETCH_OBJECT;

	RETURN_LONG(fetch_cpbi(bio)->getLastCodePoint());
}
