/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CODEPOINTITERATOR_INTERNAL_H
#define CODEPOINTITERATOR_INTERNAL_H

#include <unicode/brkiter.h>
#include <unicode/unistr.h>

using icu::BreakIterator;
using icu::CharacterIterator;
using icu::UnicodeString;

namespace HYSS {

	class CodePointBreakIterator : public BreakIterator {

	public:
		static UClassID getStaticClassID();

		CodePointBreakIterator();

		CodePointBreakIterator(const CodePointBreakIterator &other);

		CodePointBreakIterator& operator=(const CodePointBreakIterator& that);

		virtual ~CodePointBreakIterator();

		virtual UBool operator==(const BreakIterator& that) const;

		virtual CodePointBreakIterator* clone(void) const;

		virtual UClassID getDynamicClassID(void) const;

		virtual CharacterIterator& getText(void) const;

		virtual UText *getUText(UText *fillIn, UErrorCode &status) const;

		virtual void setText(const UnicodeString &text);

		virtual void setText(UText *text, UErrorCode &status);

		virtual void adoptText(CharacterIterator* it);

		virtual int32_t first(void);

		virtual int32_t last(void);

		virtual int32_t previous(void);

		virtual int32_t next(void);

		virtual int32_t current(void) const;

		virtual int32_t following(int32_t offset);

		virtual int32_t preceding(int32_t offset);

		virtual UBool isBoundary(int32_t offset);

		virtual int32_t next(int32_t n);

		virtual CodePointBreakIterator *createBufferClone(void *stackBuffer,
			int32_t &BufferSize,
			UErrorCode &status);

		virtual CodePointBreakIterator &refreshInputText(UText *input, UErrorCode &status);

		inline UChar32 getLastCodePoint()
		{
			return this->lastCodePoint;
		}

	private:
		UText *fText;
		UChar32 lastCodePoint;
		mutable CharacterIterator *fCharIter;

		inline void clearCurrentCharIter()
		{
			delete this->fCharIter;
			this->fCharIter = NULL;
			this->lastCodePoint = U_SENTINEL;
		}
	};
}

#endif
