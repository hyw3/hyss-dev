/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BREAKITERATOR_METHODS_H
#define BREAKITERATOR_METHODS_H

#include <hyss.h>

HYSS_METHOD(BreakIterator, __construct);

HYSS_FUNCTION(breakiter_create_word_instance);

HYSS_FUNCTION(breakiter_create_line_instance);

HYSS_FUNCTION(breakiter_create_character_instance);

HYSS_FUNCTION(breakiter_create_sentence_instance);

HYSS_FUNCTION(breakiter_create_title_instance);

HYSS_FUNCTION(breakiter_create_code_point_instance);

HYSS_FUNCTION(breakiter_get_text);

HYSS_FUNCTION(breakiter_set_text);

HYSS_FUNCTION(breakiter_first);

HYSS_FUNCTION(breakiter_last);

HYSS_FUNCTION(breakiter_previous);

HYSS_FUNCTION(breakiter_next);

HYSS_FUNCTION(breakiter_current);

HYSS_FUNCTION(breakiter_following);

HYSS_FUNCTION(breakiter_preceding);

HYSS_FUNCTION(breakiter_is_boundary);

HYSS_FUNCTION(breakiter_get_locale);

HYSS_FUNCTION(breakiter_get_parts_iterator);

HYSS_FUNCTION(breakiter_get_error_code);

HYSS_FUNCTION(breakiter_get_error_message);

#endif
