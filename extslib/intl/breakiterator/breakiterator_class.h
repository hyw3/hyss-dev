/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BREAKITERATOR_CLASS_H
#define BREAKITERATOR_CLASS_H

//redefinition of inline in HYSS headers causes problems, so include this before
#include <math.h>

#include <hyss.h>
#include "../intl_error.h"
#include "../intl_data.h"

#ifndef USE_BREAKITERATOR_POINTER
typedef void BreakIterator;
#else
using icu::BreakIterator;
#endif

typedef struct {
	// 	error handling
	intl_error  err;

	// ICU break iterator
	BreakIterator*	biter;

	// current text
	zval text;

	gear_object	zo;
} BreakIterator_object;

static inline BreakIterator_object *hyss_intl_breakiterator_fetch_object(gear_object *obj) {
	return (BreakIterator_object *)((char*)(obj) - XtOffsetOf(BreakIterator_object, zo));
}
#define Z_INTL_BREAKITERATOR_P(zv) hyss_intl_breakiterator_fetch_object(Z_OBJ_P(zv))

#define BREAKITER_ERROR(bio)		(bio)->err
#define BREAKITER_ERROR_P(bio)		&(BREAKITER_ERROR(bio))

#define BREAKITER_ERROR_CODE(bio)	INTL_ERROR_CODE(BREAKITER_ERROR(bio))
#define BREAKITER_ERROR_CODE_P(bio)	&(INTL_ERROR_CODE(BREAKITER_ERROR(bio)))

#define BREAKITER_METHOD_INIT_VARS		        INTL_METHOD_INIT_VARS(BreakIterator, bio)
#define BREAKITER_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_BREAKITERATOR, bio)
#define BREAKITER_METHOD_FETCH_OBJECT \
	BREAKITER_METHOD_FETCH_OBJECT_NO_CHECK; \
	if (bio->biter == NULL) \
	{ \
		intl_errors_set(&bio->err, U_ILLEGAL_ARGUMENT_ERROR, "Found unconstructed BreakIterator", 0); \
		RETURN_FALSE; \
	}

void breakiterator_object_create(zval *object, BreakIterator *break_iter, int brand_new);

void breakiterator_object_construct(zval *object, BreakIterator *break_iter);

void breakiterator_register_BreakIterator_class(void);

extern gear_class_entry *BreakIterator_ce_ptr,
						*RuleBasedBreakIterator_ce_ptr;

extern gear_object_handlers BreakIterator_handlers;

#endif /* #ifndef BREAKITERATOR_CLASS_H */
