/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include "hyss_intl.h"
#include "intl_error.h"
#include "collator/collator_class.h"
#include "collator/collator.h"
#include "collator/collator_attr.h"
#include "collator/collator_compare.h"
#include "collator/collator_sort.h"
#include "collator/collator_convert.h"
#include "collator/collator_locale.h"
#include "collator/collator_create.h"
#include "collator/collator_error.h"

#include "converter/converter.h"

#include "formatter/formatter.h"
#include "formatter/formatter_class.h"
#include "formatter/formatter_attr.h"
#include "formatter/formatter_format.h"
#include "formatter/formatter_main.h"
#include "formatter/formatter_parse.h"

#include "grapheme/grapheme.h"

#include "msgformat/msgformat.h"
#include "msgformat/msgformat_class.h"
#include "msgformat/msgformat_attr.h"
#include "msgformat/msgformat_format.h"
#include "msgformat/msgformat_parse.h"

#include "normalizer/normalizer.h"
#include "normalizer/normalizer_class.h"
#include "normalizer/normalizer_normalize.h"

#include "locale/locale.h"
#include "locale/locale_class.h"
#include "locale/locale_methods.h"

#include "dateformat/dateformat.h"
#include "dateformat/dateformat_class.h"
#include "dateformat/dateformat_attr.h"
#include "dateformat/dateformat_attrcpp.h"
#include "dateformat/dateformat_format.h"
#include "dateformat/dateformat_format_object.h"
#include "dateformat/dateformat_parse.h"
#include "dateformat/dateformat_data.h"

#include "resourcebundle/resourcebundle_class.h"

#include "transliterator/transliterator.h"
#include "transliterator/transliterator_class.h"
#include "transliterator/transliterator_methods.h"

#include "timezone/timezone_class.h"
#include "timezone/timezone_methods.h"

#include "calendar/calendar_class.h"
#include "calendar/calendar_methods.h"
#include "calendar/gregoriancalendar_methods.h"

#include "breakiterator/breakiterator_class.h"
#include "breakiterator/breakiterator_iterators.h"

#include "idn/idn.h"
#include "uchar/uchar.h"

#if U_ICU_VERSION_MAJOR_NUM * 1000 + U_ICU_VERSION_MINOR_NUM >= 4002
# include "spoofchecker/spoofchecker_class.h"
# include "spoofchecker/spoofchecker.h"
# include "spoofchecker/spoofchecker_create.h"
# include "spoofchecker/spoofchecker_main.h"
#endif

#include "msgformat/msgformat.h"
#include "common/common_error.h"
#include "common/common_enum.h"

#include <unicode/uloc.h>
#include <unicode/uclean.h>
#include <extslib/standard/info.h>

#include "hyss_ics.h"

/*
 * locale_get_default has a conflict since ICU also has
 * a function with the same  name
 * in fact ICU appends the version no. to it also
 * Hence the following undef for ICU version
 * Same true for the locale_set_default function
*/
#undef locale_get_default
#undef locale_set_default

GEAR_DECLARE_CAPI_GLOBALS( intl )

const char *intl_locale_get_default( void )
{
	if( INTL_G(default_locale) == NULL ) {
		return uloc_getDefault();
	}
	return INTL_G(default_locale);
}

/* {{{ Arguments info */
GEAR_BEGIN_ARG_INFO_EX(collator_static_0_args, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(collator_static_1_arg, 0, 0, 1)
	GEAR_ARG_INFO(0, arg1)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(collator_0_args, 0, 0, 1)
	GEAR_ARG_OBJ_INFO(0, object, Collator, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(collator_1_arg, 0, 0, 2)
	GEAR_ARG_OBJ_INFO(0, object, Collator, 0)
	GEAR_ARG_INFO(0, arg1)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(collator_2_args, 0, 0, 3)
	GEAR_ARG_OBJ_INFO(0, object, Collator, 0)
	GEAR_ARG_INFO(0, arg1)
	GEAR_ARG_INFO(0, arg2)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(collator_sort_args, 0, 0, 2)
	GEAR_ARG_OBJ_INFO(0, object, Collator, 0)
	GEAR_ARG_ARRAY_INFO(1, arr, 0)
	GEAR_ARG_INFO(0, sort_flags)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(collator_sort_with_sort_keys_args, 0, 0, 2)
	GEAR_ARG_OBJ_INFO(0, coll, Collator, 0)
	GEAR_ARG_ARRAY_INFO(1, arr, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(numfmt_parse_arginfo, 0, 0, 2)
	GEAR_ARG_INFO(0, formatter)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, type)
	GEAR_ARG_INFO(1, position)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(numfmt_parse_currency_arginfo, 0, 0, 3)
	GEAR_ARG_INFO(0, formatter)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(1, currency)
	GEAR_ARG_INFO(1, position)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_get_loc_in_loc_args, 0, GEAR_RETURN_VALUE, 1 )
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, in_locale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_filter_matches_args, 0, GEAR_RETURN_VALUE, 2 )
	GEAR_ARG_INFO(0, langtag)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, canonicalize)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_lookup_args, 0, GEAR_RETURN_VALUE, 2 )
	GEAR_ARG_INFO(0, langtag)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, canonicalize)
	GEAR_ARG_INFO(0, def)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(locale_0_args, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(locale_1_arg, 0, 0, 1)
	GEAR_ARG_INFO(0, arg1)
GEAR_END_ARG_INFO()

#define intl_0_args collator_static_0_args
#define intl_1_arg collator_static_1_arg

GEAR_BEGIN_ARG_INFO_EX(normalizer_args, 0, 0, 1)
	GEAR_ARG_INFO(0, input)
	GEAR_ARG_INFO(0, form)
GEAR_END_ARG_INFO()

#if U_ICU_VERSION_MAJOR_NUM >= 56
GEAR_BEGIN_ARG_INFO_EX(decomposition_args, 0, 0, 1)
	GEAR_ARG_INFO(0, input)
GEAR_END_ARG_INFO();
#endif

GEAR_BEGIN_ARG_INFO_EX(grapheme_1_arg, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(grapheme_search_args, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(grapheme_substr_args, 0, 0, 2)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, start)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(grapheme_strstr_args, 0, 0, 2)
	GEAR_ARG_INFO(0, haystack)
	GEAR_ARG_INFO(0, needle)
	GEAR_ARG_INFO(0, before_needle)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(grapheme_extract_args, 0, 0, 2)
	GEAR_ARG_INFO(0, arg1)
	GEAR_ARG_INFO(0, arg2)
	GEAR_ARG_INFO(0, arg3)
	GEAR_ARG_INFO(0, arg4)
	GEAR_ARG_INFO(1, arg5)  /* 1 = pass by reference */
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(datefmt_parse_args, 0, 0, 2)
	GEAR_ARG_INFO(0, formatter)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(1, position)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_create, 0, 0, 2)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, style)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_get_error_code, 0, 0, 1)
	GEAR_ARG_INFO(0, nf)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_format, 0, 0, 2)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, num)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_format_currency, 0, 0, 3)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, num)
	GEAR_ARG_INFO(0, currency)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_get_attribute, 0, 0, 2)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, attr)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_set_attribute, 0, 0, 3)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, attr)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_set_symbol, 0, 0, 3)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, attr)
	GEAR_ARG_INFO(0, symbol)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_set_pattern, 0, 0, 2)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_get_locale, 0, 0, 1)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_msgfmt_create, 0, 0, 2)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_msgfmt_get_error_code, 0, 0, 1)
	GEAR_ARG_INFO(0, nf)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_msgfmt_get_error_message, 0, 0, 1)
	GEAR_ARG_INFO(0, coll)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_msgfmt_format, 0, 0, 2)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_msgfmt_format_message, 0, 0, 3)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, pattern)
	GEAR_ARG_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_msgfmt_parse, 0, 0, 2)
	GEAR_ARG_INFO(0, nf)
	GEAR_ARG_INFO(0, source)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numfmt_parse_message, 0, 0, 3)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, pattern)
	GEAR_ARG_INFO(0, source)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_msgfmt_set_pattern, 0, 0, 2)
	GEAR_ARG_INFO(0, mf)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_msgfmt_get_locale, 0, 0, 1)
	GEAR_ARG_INFO(0, mf)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_datefmt_set_pattern, 0, 0, 2)
	GEAR_ARG_INFO(0, mf)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_datefmt_set_timezone, 0, 0, 2)
	GEAR_ARG_INFO(0, mf)
	GEAR_ARG_INFO(0, timezone)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_datefmt_set_calendar, 0, 0, 2)
	GEAR_ARG_INFO(0, mf)
	GEAR_ARG_INFO(0, calendar)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_datefmt_format, 0, 0, 0)
	GEAR_ARG_INFO(0, args)
	GEAR_ARG_INFO(0, array)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_datefmt_format_object, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, locale)
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO_EX(arginfo_datefmt_create, 0, 0, 3)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, date_type)
	GEAR_ARG_INFO(0, time_type)
	GEAR_ARG_INFO(0, timezone_str)
	GEAR_ARG_INFO(0, calendar)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_idn_to_ascii, 0, 0, 1)
	GEAR_ARG_INFO(0, domain)
	GEAR_ARG_INFO(0, option)
	GEAR_ARG_INFO(0, variant)
	GEAR_ARG_INFO(1, idn_info)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_create_proc, 0, 0, 2 )
	GEAR_ARG_INFO( 0, locale )
	GEAR_ARG_INFO( 0, bundlename )
	GEAR_ARG_INFO( 0, fallback )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_get_proc, 0, 0, 2 )
    GEAR_ARG_INFO( 0, bundle )
	GEAR_ARG_INFO( 0, index )
	GEAR_ARG_INFO( 0, fallback )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_count_proc, 0, 0, 1 )
  GEAR_ARG_INFO( 0, bundle )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_locales_proc, 0, 0, 1 )
	GEAR_ARG_INFO( 0, bundlename )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_get_error_code_proc, 0, 0, 1 )
  GEAR_ARG_INFO( 0, bundle )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_resourcebundle_get_error_message_proc, 0, 0, 1 )
  GEAR_ARG_INFO( 0, bundle )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_transliterator_void, 0, 0, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_transliterator_create, 0, 0, 1 )
	GEAR_ARG_INFO( 0, id )
	GEAR_ARG_INFO( 0, direction )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_transliterator_create_from_rules, 0, 0, 1 )
	GEAR_ARG_INFO( 0, rules )
	GEAR_ARG_INFO( 0, direction )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_transliterator_create_inverse, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, orig_trans, Transliterator, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_transliterator_transliterate, 0, 0, 2 )
	GEAR_ARG_INFO( 0, trans )
	GEAR_ARG_INFO( 0, subject )
	GEAR_ARG_INFO( 0, start )
	GEAR_ARG_INFO( 0, end )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_transliterator_error, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, trans, Transliterator, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_idarg_static, 0, 0, 1 )
	GEAR_ARG_INFO( 0, zoneId )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_from_date_time_zone, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, dateTimeZone, DateTimeZone, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_create_enumeration, 0, 0, 0 )
	GEAR_ARG_INFO( 0, countryOrRawOffset )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_create_time_zone_id_enumeration, 0, 0, 1 )
	GEAR_ARG_INFO( 0, zoneType )
	GEAR_ARG_INFO( 0, region )
	GEAR_ARG_INFO( 0, rawOffset )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_get_canonical_id, 0, 0, 1 )
	GEAR_ARG_INFO( 0, zoneId )
	GEAR_ARG_INFO( 1, isSystemID )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_get_equivalent_id, 0, 0, 2 )
	GEAR_ARG_INFO( 0, zoneId )
	GEAR_ARG_INFO( 0, index )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_get_offset, 0, 0, 5 )
	GEAR_ARG_OBJ_INFO( 0, timeZone, IntlTimeZone, 0 )
	GEAR_ARG_INFO( 0, date )
	GEAR_ARG_INFO( 0, local )
	GEAR_ARG_INFO( 1, rawOffset )
	GEAR_ARG_INFO( 1, dstOffset )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_has_same_rules, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, timeZone, IntlTimeZone, 0 )
	GEAR_ARG_OBJ_INFO( 0, otherTimeZone, IntlTimeZone, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_get_display_name, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, timeZone, IntlTimeZone, 0 )
	GEAR_ARG_INFO( 0, isDaylight )
	GEAR_ARG_INFO( 0, style )
	GEAR_ARG_INFO( 0, locale )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_only_tz, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, timeZone, IntlTimeZone, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( arginfo_tz_void, 0, 0, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_create_instance, 0, 0, 0 )
	GEAR_ARG_INFO( 0, timeZone )
	GEAR_ARG_INFO( 0, locale )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_only_cal, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_void, 0, 0, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_field, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, field )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_dow, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, dayOfWeek )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_other_cal, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_OBJ_INFO( 0, otherCalendar, IntlCalendar, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_date, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, date )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_date_optional, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, date )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_get_keyword_values_for_locale, 0, 0, 3)
	GEAR_ARG_INFO( 0, key )
	GEAR_ARG_INFO( 0, locale )
	GEAR_ARG_INFO( 0, commonlyUsed )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_add, 0, 0, 3 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, field )
	GEAR_ARG_INFO( 0, amount )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_set_time_zone, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, timeZone )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_set, 0, 0, 3 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, fieldOrYear )
	GEAR_ARG_INFO( 0, valueOrMonth )
	GEAR_ARG_INFO( 0, dayOfMonth )
	GEAR_ARG_INFO( 0, hour )
	GEAR_ARG_INFO( 0, minute )
	GEAR_ARG_INFO( 0, second )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_roll, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, field )
	GEAR_ARG_INFO( 0, amountOrUpOrDown )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_clear, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, field )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_field_difference, 0, 0, 3 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, when )
	GEAR_ARG_INFO( 0, field )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_get_locale, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, localeType )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_set_lenient, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, isLenient )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_set_minimal_days_in_first_week, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, numberOfDays )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_from_date_time, 0, 0, 1)
	GEAR_ARG_INFO(0, dateTime)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_cal_wall_time_option, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlCalendar, 0 )
	GEAR_ARG_INFO( 0, wallTimeOption )
GEAR_END_ARG_INFO()

/* Gregorian Calendar */
GEAR_BEGIN_ARG_INFO_EX( ainfo_gregcal_create_instance, 0, 0, 0 )
	GEAR_ARG_INFO(0, timeZoneOrYear)
	GEAR_ARG_INFO(0, localeOrMonth)
	GEAR_ARG_INFO(0, dayOfMonth)
	GEAR_ARG_INFO(0, hour)
	GEAR_ARG_INFO(0, minute)
	GEAR_ARG_INFO(0, second)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_gregcal_is_leap_year, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlGregorianCalendar, 0 )
	GEAR_ARG_INFO( 0, year )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_gregcal_only_gregcal, 0, 0, 1 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlGregorianCalendar, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( ainfo_gregcal_set_gregorian_change, 0, 0, 2 )
	GEAR_ARG_OBJ_INFO( 0, calendar, IntlGregorianCalendar, 0 )
	GEAR_ARG_INFO( 0, date )
GEAR_END_ARG_INFO()

/* }}} */

/* {{{ intl_functions
 *
 * Every user visible function must have an entry in intl_functions[].
 */
static const gear_function_entry intl_functions[] = {

	/* collator functions */
	HYSS_FE( collator_create, collator_static_1_arg )
	HYSS_FE( collator_compare, collator_2_args )
	HYSS_FE( collator_get_attribute, collator_1_arg )
	HYSS_FE( collator_set_attribute, collator_2_args )
	HYSS_FE( collator_get_strength, collator_0_args )
	HYSS_FE( collator_set_strength, collator_1_arg )
	HYSS_FE( collator_sort, collator_sort_args )
	HYSS_FE( collator_sort_with_sort_keys, collator_sort_with_sort_keys_args )
	HYSS_FE( collator_asort, collator_sort_args )
	HYSS_FE( collator_get_locale, collator_1_arg )
	HYSS_FE( collator_get_error_code, collator_0_args )
	HYSS_FE( collator_get_error_message, collator_0_args )
	HYSS_FE( collator_get_sort_key, collator_1_arg )

	/* formatter functions */
	HYSS_FE( numfmt_create, arginfo_numfmt_create )
	HYSS_FE( numfmt_format, arginfo_numfmt_format )
	HYSS_FE( numfmt_parse, numfmt_parse_arginfo )
	HYSS_FE( numfmt_format_currency, arginfo_numfmt_format_currency )
	HYSS_FE( numfmt_parse_currency, numfmt_parse_currency_arginfo )
	HYSS_FE( numfmt_set_attribute, arginfo_numfmt_set_attribute )
	HYSS_FE( numfmt_get_attribute, arginfo_numfmt_get_attribute )
	HYSS_FE( numfmt_set_text_attribute, arginfo_numfmt_set_attribute )
	HYSS_FE( numfmt_get_text_attribute, arginfo_numfmt_get_attribute )
	HYSS_FE( numfmt_set_symbol, arginfo_numfmt_set_symbol )
	HYSS_FE( numfmt_get_symbol, arginfo_numfmt_get_attribute )
	HYSS_FE( numfmt_set_pattern, arginfo_numfmt_set_pattern )
	HYSS_FE( numfmt_get_pattern, arginfo_numfmt_get_error_code )
	HYSS_FE( numfmt_get_locale, arginfo_numfmt_get_locale )
	HYSS_FE( numfmt_get_error_code, arginfo_numfmt_get_error_code )
	HYSS_FE( numfmt_get_error_message, arginfo_numfmt_get_error_code )

	/* normalizer functions */
	HYSS_FE( normalizer_normalize, normalizer_args )
	HYSS_FE( normalizer_is_normalized, normalizer_args )
#if U_ICU_VERSION_MAJOR_NUM >= 56
	HYSS_FE( normalizer_get_raw_decomposition, decomposition_args )
#endif

	/* Locale functions */
	HYSS_NAMED_FE( locale_get_default, zif_locale_get_default, locale_0_args )
	HYSS_NAMED_FE( locale_set_default, zif_locale_set_default, locale_1_arg )
	HYSS_FE( locale_get_primary_language, locale_1_arg )
	HYSS_FE( locale_get_script, locale_1_arg )
	HYSS_FE( locale_get_region, locale_1_arg )
	HYSS_FE( locale_get_keywords, locale_1_arg )
	HYSS_FE( locale_get_display_script, locale_get_loc_in_loc_args )
	HYSS_FE( locale_get_display_region, locale_get_loc_in_loc_args )
	HYSS_FE( locale_get_display_name, locale_get_loc_in_loc_args )
	HYSS_FE( locale_get_display_language, locale_get_loc_in_loc_args)
	HYSS_FE( locale_get_display_variant, locale_get_loc_in_loc_args )
	HYSS_FE( locale_compose, locale_1_arg )
	HYSS_FE( locale_parse, locale_1_arg )
	HYSS_FE( locale_get_all_variants, locale_1_arg )
	HYSS_FE( locale_filter_matches, locale_filter_matches_args )
	HYSS_FE( locale_canonicalize, locale_1_arg )
	HYSS_FE( locale_lookup, locale_lookup_args )
	HYSS_FE( locale_accept_from_http, locale_1_arg )

	/* MessageFormatter functions */
	HYSS_FE( msgfmt_create, arginfo_msgfmt_create )
	HYSS_FE( msgfmt_format, arginfo_msgfmt_format )
	HYSS_FE( msgfmt_format_message, arginfo_msgfmt_format_message )
	HYSS_FE( msgfmt_parse, arginfo_msgfmt_parse )
	HYSS_FE( msgfmt_parse_message, arginfo_numfmt_parse_message )
	HYSS_FE( msgfmt_set_pattern, arginfo_msgfmt_set_pattern )
	HYSS_FE( msgfmt_get_pattern, arginfo_msgfmt_get_locale )
	HYSS_FE( msgfmt_get_locale, arginfo_msgfmt_get_locale )
	HYSS_FE( msgfmt_get_error_code, arginfo_msgfmt_get_error_code )
	HYSS_FE( msgfmt_get_error_message, arginfo_msgfmt_get_error_message )

	/* IntlDateFormatter functions */
	HYSS_FE( datefmt_create, arginfo_datefmt_create )
	HYSS_FE( datefmt_get_datetype, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_get_timetype, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_get_calendar, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_get_calendar_object, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_set_calendar, arginfo_datefmt_set_calendar )
	HYSS_FE( datefmt_get_locale, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_get_timezone_id, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_get_timezone, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_set_timezone, arginfo_datefmt_set_timezone )
	HYSS_FE( datefmt_get_pattern, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_set_pattern, arginfo_datefmt_set_pattern )
	HYSS_FE( datefmt_is_lenient, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_set_lenient, arginfo_msgfmt_get_locale )
	HYSS_FE( datefmt_format, arginfo_datefmt_format )
	HYSS_FE( datefmt_format_object, arginfo_datefmt_format_object )
	HYSS_FE( datefmt_parse, datefmt_parse_args )
	HYSS_FE( datefmt_localtime , datefmt_parse_args )
	HYSS_FE( datefmt_get_error_code, arginfo_msgfmt_get_error_code )
	HYSS_FE( datefmt_get_error_message, arginfo_msgfmt_get_error_message )

	/* grapheme functions */
	HYSS_FE( grapheme_strlen, grapheme_1_arg )
	HYSS_FE( grapheme_strpos, grapheme_search_args )
	HYSS_FE( grapheme_stripos, grapheme_search_args )
	HYSS_FE( grapheme_strrpos, grapheme_search_args )
	HYSS_FE( grapheme_strripos, grapheme_search_args )
	HYSS_FE( grapheme_substr, grapheme_substr_args )
	HYSS_FE( grapheme_strstr, grapheme_strstr_args )
	HYSS_FE( grapheme_stristr, grapheme_strstr_args )
	HYSS_FE( grapheme_extract, grapheme_extract_args )

	/* IDN functions */
	HYSS_FE( idn_to_ascii, arginfo_idn_to_ascii)
	HYSS_FE( idn_to_utf8, arginfo_idn_to_ascii)

	/* ResourceBundle functions */
	HYSS_FE( resourcebundle_create, arginfo_resourcebundle_create_proc )
	HYSS_FE( resourcebundle_get, arginfo_resourcebundle_get_proc )
	HYSS_FE( resourcebundle_count, arginfo_resourcebundle_count_proc )
	HYSS_FE( resourcebundle_locales, arginfo_resourcebundle_locales_proc )
	HYSS_FE( resourcebundle_get_error_code, arginfo_resourcebundle_get_error_code_proc )
	HYSS_FE( resourcebundle_get_error_message, arginfo_resourcebundle_get_error_message_proc )

	/* Transliterator functions */
	HYSS_FE( transliterator_create, arginfo_transliterator_create )
	HYSS_FE( transliterator_create_from_rules, arginfo_transliterator_create_from_rules )
	HYSS_FE( transliterator_list_ids, arginfo_transliterator_void )
	HYSS_FE( transliterator_create_inverse, arginfo_transliterator_create_inverse)
	HYSS_FE( transliterator_transliterate, arginfo_transliterator_transliterate )
	HYSS_FE( transliterator_get_error_code, arginfo_transliterator_error )
	HYSS_FE( transliterator_get_error_message, arginfo_transliterator_error )

	/* TimeZone functions */
	HYSS_FE( intltz_create_time_zone, arginfo_tz_idarg_static )
	HYSS_FE( intltz_from_date_time_zone, arginfo_tz_from_date_time_zone )
	HYSS_FE( intltz_create_default, arginfo_tz_void )
	HYSS_FE( intltz_get_id, arginfo_tz_only_tz )
	HYSS_FE( intltz_get_gmt, arginfo_tz_void )
#if U_ICU_VERSION_MAJOR_NUM >= 49
	HYSS_FE( intltz_get_unknown, arginfo_tz_void )
#endif
	HYSS_FE( intltz_create_enumeration, arginfo_tz_create_enumeration )
	HYSS_FE( intltz_count_equivalent_ids, arginfo_tz_idarg_static )
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 48
	HYSS_FE( intltz_create_time_zone_id_enumeration, arginfo_tz_create_time_zone_id_enumeration )
#endif
	HYSS_FE( intltz_get_canonical_id, arginfo_tz_get_canonical_id )
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 48
	HYSS_FE( intltz_get_region, arginfo_tz_idarg_static )
#endif
	HYSS_FE( intltz_get_tz_data_version, arginfo_tz_void )
	HYSS_FE( intltz_get_equivalent_id, arginfo_tz_get_equivalent_id )
	HYSS_FE( intltz_use_daylight_time, arginfo_tz_only_tz )
	HYSS_FE( intltz_get_offset, arginfo_tz_get_offset )
	HYSS_FE( intltz_get_raw_offset, arginfo_tz_only_tz )
	HYSS_FE( intltz_has_same_rules, arginfo_tz_has_same_rules )
	HYSS_FE( intltz_get_display_name, arginfo_tz_get_display_name )
	HYSS_FE( intltz_get_dst_savings, arginfo_tz_only_tz )
	HYSS_FE( intltz_to_date_time_zone, arginfo_tz_only_tz )
	HYSS_FE( intltz_get_error_code, arginfo_tz_only_tz )
	HYSS_FE( intltz_get_error_message, arginfo_tz_only_tz )

	HYSS_FE( intlcal_create_instance, ainfo_cal_create_instance )
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 42
	HYSS_FE( intlcal_get_keyword_values_for_locale, ainfo_cal_get_keyword_values_for_locale )
#endif
	HYSS_FE( intlcal_get_now, ainfo_cal_void )
	HYSS_FE( intlcal_get_available_locales, ainfo_cal_void )
	HYSS_FE( intlcal_get, ainfo_cal_field )
	HYSS_FE( intlcal_get_time, ainfo_cal_only_cal )
	HYSS_FE( intlcal_set_time, ainfo_cal_date )
	HYSS_FE( intlcal_add, ainfo_cal_add )
	HYSS_FE( intlcal_set_time_zone, ainfo_cal_set_time_zone )
	HYSS_FE( intlcal_after, ainfo_cal_other_cal )
	HYSS_FE( intlcal_before, ainfo_cal_other_cal )
	HYSS_FE( intlcal_set, ainfo_cal_set )
	HYSS_FE( intlcal_roll, ainfo_cal_roll )
	HYSS_FE( intlcal_clear, ainfo_cal_clear )
	HYSS_FE( intlcal_field_difference, ainfo_cal_field_difference )
	HYSS_FE( intlcal_get_actual_maximum, ainfo_cal_field )
	HYSS_FE( intlcal_get_actual_minimum, ainfo_cal_field )
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 44
	HYSS_FE( intlcal_get_day_of_week_type, ainfo_cal_dow )
#endif
	HYSS_FE( intlcal_get_first_day_of_week, ainfo_cal_only_cal )
	HYSS_FE( intlcal_get_greatest_minimum, ainfo_cal_field )
	HYSS_FE( intlcal_get_least_maximum, ainfo_cal_field )
	HYSS_FE( intlcal_get_locale, ainfo_cal_get_locale )
	HYSS_FE( intlcal_get_maximum, ainfo_cal_field )
	HYSS_FE( intlcal_get_minimal_days_in_first_week, ainfo_cal_only_cal )
	HYSS_FE( intlcal_get_minimum, ainfo_cal_field )
	HYSS_FE( intlcal_get_time_zone, ainfo_cal_only_cal )
	HYSS_FE( intlcal_get_type, ainfo_cal_only_cal )
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 44
	HYSS_FE( intlcal_get_weekend_transition, ainfo_cal_dow )
#endif
	HYSS_FE( intlcal_in_daylight_time, ainfo_cal_only_cal )
	HYSS_FE( intlcal_is_equivalent_to, ainfo_cal_other_cal )
	HYSS_FE( intlcal_is_lenient, ainfo_cal_only_cal )
	HYSS_FE( intlcal_is_set, ainfo_cal_field )
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 44
	HYSS_FE( intlcal_is_weekend, ainfo_cal_date_optional )
#endif
	HYSS_FE( intlcal_set_first_day_of_week, ainfo_cal_dow )
	HYSS_FE( intlcal_set_lenient, ainfo_cal_set_lenient )
	HYSS_FE( intlcal_set_minimal_days_in_first_week, ainfo_cal_set_minimal_days_in_first_week )
	HYSS_FE( intlcal_equals, ainfo_cal_other_cal )
	HYSS_FE( intlcal_from_date_time, ainfo_cal_from_date_time )
	HYSS_FE( intlcal_to_date_time, ainfo_cal_only_cal )
#if U_ICU_VERSION_MAJOR_NUM >= 49
	HYSS_FE( intlcal_get_repeated_wall_time_option, ainfo_cal_only_cal )
	HYSS_FE( intlcal_get_skipped_wall_time_option, ainfo_cal_only_cal )
	HYSS_FE( intlcal_set_repeated_wall_time_option, ainfo_cal_wall_time_option )
	HYSS_FE( intlcal_set_skipped_wall_time_option, ainfo_cal_wall_time_option )
#endif
	HYSS_FE( intlcal_get_error_code, ainfo_cal_only_cal )
	HYSS_FE( intlcal_get_error_message, ainfo_cal_only_cal )

	HYSS_FE( intlgregcal_create_instance, ainfo_gregcal_create_instance )
	HYSS_FE( intlgregcal_set_gregorian_change, ainfo_gregcal_set_gregorian_change )
	HYSS_FE( intlgregcal_get_gregorian_change, ainfo_gregcal_only_gregcal )
	HYSS_FE( intlgregcal_is_leap_year, ainfo_gregcal_is_leap_year )

	/* common functions */
	HYSS_FE( intl_get_error_code, intl_0_args )
	HYSS_FE( intl_get_error_message, intl_0_args )
	HYSS_FE( intl_is_failure, intl_1_arg )
	HYSS_FE( intl_error_name, intl_1_arg )

	HYSS_FE_END
};
/* }}} */

/* {{{ ICS Settings */
HYSS_ICS_BEGIN()
    STD_HYSS_ICS_ENTRY(LOCALE_ICS_NAME, NULL, HYSS_ICS_ALL, OnUpdateStringUnempty, default_locale, gear_intl_globals, intl_globals)
    STD_HYSS_ICS_ENTRY("intl.error_level", "0", HYSS_ICS_ALL, OnUpdateLong, error_level, gear_intl_globals, intl_globals)
	STD_HYSS_ICS_ENTRY("intl.use_exceptions", "0", HYSS_ICS_ALL, OnUpdateBool, use_exceptions, gear_intl_globals, intl_globals)
HYSS_ICS_END()
/* }}} */

static HYSS_GINIT_FUNCTION(intl);

/* {{{ intl_capi_entry */
gear_capi_entry intl_capi_entry = {
	STANDARD_CAPI_HEADER,
	"intl",
	intl_functions,
	HYSS_MINIT( intl ),
	HYSS_MSHUTDOWN( intl ),
	HYSS_RINIT( intl ),
	HYSS_RSHUTDOWN( intl ),
	HYSS_MINFO( intl ),
	HYSS_INTL_VERSION,
	HYSS_CAPI_GLOBALS(intl),   /* globals descriptor */
	HYSS_GINIT(intl),            /* globals ctor */
	NULL,                       /* globals dtor */
	NULL,                       /* post deactivate */
	STANDARD_CAPI_PROPERTIES_EX
};
/* }}} */

#ifdef COMPILE_DL_INTL
#ifdef ZTS
GEAR_PBCLS_CACHE_DEFINE()
#endif
GEAR_GET_CAPI( intl )
#endif

/* {{{ intl_init_globals */
static HYSS_GINIT_FUNCTION(intl)
{
#if defined(COMPILE_DL_INTL) && defined(ZTS)
	GEAR_PBCLS_CACHE_UPDATE();
#endif
	memset( intl_globals, 0, sizeof(gear_intl_globals) );
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION( intl )
{
	/* For the default locale hyss.ics setting */
	REGISTER_ICS_ENTRIES();

	REGISTER_LONG_CONSTANT("INTL_MAX_LOCALE_LEN", INTL_MAX_LOCALE_LEN, CONST_PERSISTENT | CONST_CS);
	REGISTER_STRING_CONSTANT("INTL_ICU_VERSION", U_ICU_VERSION, CONST_PERSISTENT | CONST_CS);
#ifdef U_ICU_DATA_VERSION
	REGISTER_STRING_CONSTANT("INTL_ICU_DATA_VERSION", U_ICU_DATA_VERSION, CONST_PERSISTENT | CONST_CS);
#endif

	/* Register 'Collator' HYSS class */
	collator_register_Collator_class(  );

	/* Expose Collator constants to HYSS scripts */
	collator_register_constants( INIT_FUNC_ARGS_PASSTHRU );

	/* Register 'NumberFormatter' HYSS class */
	formatter_register_class(  );

	/* Expose NumberFormatter constants to HYSS scripts */
	formatter_register_constants( INIT_FUNC_ARGS_PASSTHRU );

	/* Register 'Normalizer' HYSS class */
	normalizer_register_Normalizer_class(  );

	/* Expose Normalizer constants to HYSS scripts */
	normalizer_register_constants( INIT_FUNC_ARGS_PASSTHRU );

	/* Register 'Locale' HYSS class */
	locale_register_Locale_class(  );

	/* Expose Locale constants to HYSS scripts */
	locale_register_constants( INIT_FUNC_ARGS_PASSTHRU );

	msgformat_register_class();

	grapheme_register_constants( INIT_FUNC_ARGS_PASSTHRU );

	/* Register 'DateFormat' HYSS class */
	dateformat_register_IntlDateFormatter_class(  );

	/* Expose DateFormat constants to HYSS scripts */
	dateformat_register_constants( INIT_FUNC_ARGS_PASSTHRU );

	/* Register 'ResourceBundle' HYSS class */
	resourcebundle_register_class( );

	/* Register 'Transliterator' HYSS class */
	transliterator_register_Transliterator_class(  );

	/* Register Transliterator constants */
	transliterator_register_constants( INIT_FUNC_ARGS_PASSTHRU );

	/* Register 'IntlTimeZone' HYSS class */
	timezone_register_IntlTimeZone_class(  );

	/* Register 'IntlCalendar' HYSS class */
	calendar_register_IntlCalendar_class(  );

	/* Expose ICU error codes to HYSS scripts. */
	intl_expose_icu_error_codes( INIT_FUNC_ARGS_PASSTHRU );

	/* Expose IDN constants to HYSS scripts. */
	idn_register_constants(INIT_FUNC_ARGS_PASSTHRU);

#if U_ICU_VERSION_MAJOR_NUM * 1000 + U_ICU_VERSION_MINOR_NUM >= 4002
	/* Register 'Spoofchecker' HYSS class */
	spoofchecker_register_Spoofchecker_class(  );

	/* Expose Spoofchecker constants to HYSS scripts */
	spoofchecker_register_constants( INIT_FUNC_ARGS_PASSTHRU );
#endif

	/* Register 'IntlException' HYSS class */
	intl_register_IntlException_class(  );

	/* Register 'IntlIterator' HYSS class */
	intl_register_IntlIterator_class(  );

	/* Register 'BreakIterator' class */
	breakiterator_register_BreakIterator_class(  );

	/* Register 'IntlPartsIterator' class */
	breakiterator_register_IntlPartsIterator_class(  );

	/* Global error handling. */
	intl_error_init( NULL );

	/* 'Converter' class for codepage conversions */
	hyss_converter_minit(INIT_FUNC_ARGS_PASSTHRU);

	/* IntlChar class */
	hyss_uchar_minit(INIT_FUNC_ARGS_PASSTHRU);

	return SUCCESS;
}
/* }}} */

#define EXPLICIT_CLEANUP_ENV_VAR "INTL_EXPLICIT_CLEANUP"

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION( intl )
{
	const char *cleanup;
    /* For the default locale hyss.ics setting */
    UNREGISTER_ICS_ENTRIES();

	cleanup = getenv(EXPLICIT_CLEANUP_ENV_VAR);
    if (cleanup != NULL && !(cleanup[0] == '0' && cleanup[1] == '\0')) {
		u_cleanup();
    }

    return SUCCESS;
}
/* }}} */

/* {{{ HYSS_RINIT_FUNCTION
 */
HYSS_RINIT_FUNCTION( intl )
{
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_RSHUTDOWN_FUNCTION
 */
HYSS_RSHUTDOWN_FUNCTION( intl )
{
	if(!Z_ISUNDEF(INTL_G(current_collator))) {
		ZVAL_UNDEF(&INTL_G(current_collator));
	}
	if (INTL_G(grapheme_iterator)) {
		grapheme_close_global_iterator(  );
		INTL_G(grapheme_iterator) = NULL;
	}

	intl_error_reset( NULL);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION( intl )
{
#if !UCONFIG_NO_FORMATTING
	UErrorCode status = U_ZERO_ERROR;
	const char *tzdata_ver = NULL;
#endif

	hyss_info_print_table_start();
	hyss_info_print_table_header( 2, "Internationalization support", "enabled" );
	hyss_info_print_table_row( 2, "ICU version", U_ICU_VERSION );
#ifdef U_ICU_DATA_VERSION
	hyss_info_print_table_row( 2, "ICU Data version", U_ICU_DATA_VERSION );
#endif
#if !UCONFIG_NO_FORMATTING
	tzdata_ver = ucal_getTZDataVersion(&status);
	if (U_ZERO_ERROR == status) {
		hyss_info_print_table_row( 2, "ICU TZData version", tzdata_ver);
	}
#endif
	hyss_info_print_table_row( 2, "ICU Unicode version", U_UNICODE_VERSION );
	hyss_info_print_table_end();

    /* For the default locale hyss.ics setting */
    DISPLAY_ICS_ENTRIES() ;
}
/* }}} */

