/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTL_COMMON_H
#define INTL_COMMON_H
/* Auxiliary macros */

BEGIN_EXTERN_C()
#include <hyss.h>
END_EXTERN_C()
#include <unicode/utypes.h>

#ifndef UBYTES
# define UBYTES(len) ((len) * sizeof(UChar))
#endif

#ifndef eumalloc
# define eumalloc(size)  (UChar*)safe_emalloc(size, sizeof(UChar), 0)
#endif

#ifndef eurealloc
# define eurealloc(ptr, size)  (UChar*)erealloc((ptr), size * sizeof(UChar))
#endif

#define USIZE(data) sizeof((data))/sizeof(UChar)
#define UCHARS(len) ((len) / sizeof(UChar))

#define INTL_Z_STRVAL_P(str) (UChar*) Z_STRVAL_P(str)
#define INTL_Z_STRLEN_P(str) UCHARS( Z_STRLEN_P(str) )

BEGIN_EXTERN_C()
extern gear_class_entry *IntlException_ce_ptr;
END_EXTERN_C()

#endif /* INTL_COMMON_H */
