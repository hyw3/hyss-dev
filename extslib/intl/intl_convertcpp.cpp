/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "intl_cppshims.h"

#include "intl_convertcpp.h"
#include <unicode/ustring.h>
extern "C" {
#include <hyss.h>
}

/* {{{ intl_stringFromChar */
int intl_stringFromChar(UnicodeString &ret, char *str, size_t str_len, UErrorCode *status)
{
	if(str_len > INT32_MAX) {
		*status = U_BUFFER_OVERFLOW_ERROR;
		ret.setToBogus();
		return FAILURE;
	}
	//the number of UTF-16 code units is not larger than that of UTF-8 code
	//units, + 1 for the terminator
	int32_t capacity = (int32_t)str_len + 1;

	//no check necessary -- if NULL will fail ahead
	UChar	*utf16 = ret.getBuffer(capacity);
	int32_t utf16_len = 0;
	*status = U_ZERO_ERROR;
	u_strFromUTF8WithSub(utf16, ret.getCapacity(), &utf16_len,
		str, str_len, U_SENTINEL /* no substitution */, NULL,
		status);
	ret.releaseBuffer(utf16_len);
	if (U_FAILURE(*status)) {
		ret.setToBogus();
		return FAILURE;
	}
	return SUCCESS;
}
/* }}} */

/* {{{ intl_charFromString
 * faster than doing intl_convert_utf16_to_utf8(
 *		from.getBuffer(), from.length(), &status),
 * but consumes more memory */
gear_string* intl_charFromString(const UnicodeString &from, UErrorCode *status)
{
	gear_string *u8res;

	if (from.isBogus()) {
		return NULL;
	}

	//the number of UTF-8 code units is not larger than that of UTF-16 code
	//units * 3
	int32_t capacity = from.length() * 3;

	if (from.isEmpty()) {
		return ZSTR_EMPTY_ALLOC();
	}

	u8res = gear_string_alloc(capacity, 0);

	const UChar *utf16buf = from.getBuffer();
	int32_t actual_len;
	u_strToUTF8WithSub(ZSTR_VAL(u8res), capacity, &actual_len, utf16buf, from.length(),
		U_SENTINEL, NULL, status);

	if (U_FAILURE(*status)) {
		gear_string_free(u8res);
		return NULL;
	}
	ZSTR_VAL(u8res)[actual_len] = '\0';
	ZSTR_LEN(u8res) = actual_len;

	return u8res;
}
/* }}} */
