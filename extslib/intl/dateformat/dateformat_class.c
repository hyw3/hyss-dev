/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicode/unum.h>

#include "dateformat_class.h"
#include "hyss_intl.h"
#include "dateformat_data.h"
#include "dateformat_format.h"
#include "dateformat_format_object.h"
#include "dateformat_parse.h"
#include "dateformat.h"
#include "dateformat_attr.h"
#include "dateformat_attrcpp.h"

#include <gear_exceptions.h>

gear_class_entry *IntlDateFormatter_ce_ptr = NULL;
static gear_object_handlers IntlDateFormatter_handlers;

/*
 * Auxiliary functions needed by objects of 'IntlDateFormatter' class
 */

/* {{{ IntlDateFormatter_objects_dtor */
static void IntlDateFormatter_object_dtor(gear_object *object )
{
	gear_objects_destroy_object( object );
}
/* }}} */

/* {{{ IntlDateFormatter_objects_free */
void IntlDateFormatter_object_free( gear_object *object )
{
	IntlDateFormatter_object* dfo = hyss_intl_dateformatter_fetch_object(object);

	gear_object_std_dtor( &dfo->zo );

	if (dfo->requested_locale) {
		efree( dfo->requested_locale );
	}

	dateformat_data_free( &dfo->datef_data );
}
/* }}} */

/* {{{ IntlDateFormatter_object_create */
gear_object *IntlDateFormatter_object_create(gear_class_entry *ce)
{
	IntlDateFormatter_object*     intern;

	intern = gear_object_alloc(sizeof(IntlDateFormatter_object), ce);
	dateformat_data_init( &intern->datef_data );
	gear_object_std_init( &intern->zo, ce );
	object_properties_init(&intern->zo, ce);
	intern->date_type			= 0;
	intern->time_type			= 0;
	intern->calendar			= -1;
	intern->requested_locale	= NULL;

	intern->zo.handlers = &IntlDateFormatter_handlers;

	return &intern->zo;
}
/* }}} */

/* {{{ IntlDateFormatter_object_clone */
gear_object *IntlDateFormatter_object_clone(zval *object)
{
	IntlDateFormatter_object *dfo, *new_dfo;
	gear_object *new_obj;

	DATE_FORMAT_METHOD_FETCH_OBJECT_NO_CHECK;

	new_obj = IntlDateFormatter_ce_ptr->create_object(Z_OBJCE_P(object));
	new_dfo = hyss_intl_dateformatter_fetch_object(new_obj);
	/* clone standard parts */
	gear_objects_clone_members(&new_dfo->zo, &dfo->zo);
	/* clone formatter object */
	if (dfo->datef_data.udatf != NULL) {
		DATE_FORMAT_OBJECT(new_dfo) = udat_clone(DATE_FORMAT_OBJECT(dfo),  &INTL_DATA_ERROR_CODE(dfo));
		if (U_FAILURE(INTL_DATA_ERROR_CODE(dfo))) {
			/* set up error in case error handler is interested */
			intl_errors_set(INTL_DATA_ERROR_P(dfo), INTL_DATA_ERROR_CODE(dfo),
					"Failed to clone IntlDateFormatter object", 0 );
			gear_throw_exception(NULL, "Failed to clone IntlDateFormatter object", 0);
		}
	} else {
		gear_throw_exception(NULL, "Cannot clone unconstructed IntlDateFormatter", 0);
	}
	return new_obj;
}
/* }}} */

/*
 * 'IntlDateFormatter' class registration structures & functions
 */

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(datefmt_parse_args, 0, 0, 1)
		GEAR_ARG_INFO(0, string)
		GEAR_ARG_INFO(1, position)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_intldateformatter_format, 0, 0, 0)
	GEAR_ARG_INFO(0, args)
	GEAR_ARG_INFO(0, array)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_intldateformatter_format_object, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, locale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_intldateformatter_getdatetype, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_intldateformatter_settimezoneid, 0, 0, 1)
	GEAR_ARG_INFO(0, zone)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_intldateformatter_setpattern, 0, 0, 1)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_intldateformatter_setlenient, 0, 0, 1)
	GEAR_ARG_INFO(0, lenient)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_intldateformatter_setcalendar, 0, 0, 1)
	GEAR_ARG_INFO(0, which)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_intldateformatter___construct, 0, 0, 3)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, datetype)
	GEAR_ARG_INFO(0, timetype)
	GEAR_ARG_INFO(0, timezone)
	GEAR_ARG_INFO(0, calendar)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ IntlDateFormatter_class_functions
 * Every 'IntlDateFormatter' class method has an entry in this table
 */
static const gear_function_entry IntlDateFormatter_class_functions[] = {
	HYSS_ME( IntlDateFormatter, __construct, arginfo_intldateformatter___construct, GEAR_ACC_PUBLIC|GEAR_ACC_CTOR )
	GEAR_FENTRY(  create, GEAR_FN( datefmt_create ), arginfo_intldateformatter___construct, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	HYSS_NAMED_FE( getDateType, GEAR_FN( datefmt_get_datetype ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( getTimeType, GEAR_FN( datefmt_get_timetype ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( getCalendar, GEAR_FN( datefmt_get_calendar ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( getCalendarObject, GEAR_FN( datefmt_get_calendar_object ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( setCalendar, GEAR_FN( datefmt_set_calendar ), arginfo_intldateformatter_setcalendar )
	HYSS_NAMED_FE( getTimeZoneId, GEAR_FN( datefmt_get_timezone_id ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( getTimeZone, GEAR_FN( datefmt_get_timezone ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( setTimeZone, GEAR_FN( datefmt_set_timezone ), arginfo_intldateformatter_settimezoneid )
	HYSS_NAMED_FE( setPattern, GEAR_FN( datefmt_set_pattern ), arginfo_intldateformatter_setpattern )
	HYSS_NAMED_FE( getPattern, GEAR_FN( datefmt_get_pattern ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( getLocale, GEAR_FN( datefmt_get_locale ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( setLenient, GEAR_FN( datefmt_set_lenient ), arginfo_intldateformatter_setlenient )
	HYSS_NAMED_FE( isLenient, GEAR_FN( datefmt_is_lenient ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( format, GEAR_FN( datefmt_format ), arginfo_intldateformatter_format )
	HYSS_ME_MAPPING( formatObject, datefmt_format_object, arginfo_intldateformatter_format_object, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_NAMED_FE( parse, GEAR_FN( datefmt_parse), datefmt_parse_args )
	HYSS_NAMED_FE( localtime, GEAR_FN( datefmt_localtime ), datefmt_parse_args )
	HYSS_NAMED_FE( getErrorCode, GEAR_FN( datefmt_get_error_code ), arginfo_intldateformatter_getdatetype )
	HYSS_NAMED_FE( getErrorMessage, GEAR_FN( datefmt_get_error_message ), arginfo_intldateformatter_getdatetype )
	HYSS_FE_END
};
/* }}} */

/* {{{ dateformat_register_class
 * Initialize 'IntlDateFormatter' class
 */
void dateformat_register_IntlDateFormatter_class( void )
{
	gear_class_entry ce;

	/* Create and register 'IntlDateFormatter' class. */
	INIT_CLASS_ENTRY( ce, "IntlDateFormatter", IntlDateFormatter_class_functions );
	ce.create_object = IntlDateFormatter_object_create;
	IntlDateFormatter_ce_ptr = gear_register_internal_class( &ce );

	memcpy(&IntlDateFormatter_handlers, &std_object_handlers,
		sizeof IntlDateFormatter_handlers);
	IntlDateFormatter_handlers.offset = XtOffsetOf(IntlDateFormatter_object, zo);
	IntlDateFormatter_handlers.clone_obj = IntlDateFormatter_object_clone;
	IntlDateFormatter_handlers.dtor_obj = IntlDateFormatter_object_dtor;
	IntlDateFormatter_handlers.free_obj = IntlDateFormatter_object_free;
}
/* }}} */
