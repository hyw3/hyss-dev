/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "dateformat_data.h"

/* {{{ void dateformat_data_init( dateformat_data* datef_data )
 * Initialize internals of dateformat_data.
 */
void dateformat_data_init( dateformat_data* datef_data )
{
	if( !datef_data )
		return;

	datef_data->udatf = NULL;
	intl_error_reset( &datef_data->error );
}
/* }}} */

/* {{{ void dateformat_data_free( dateformat_data* datef_data )
 * Clean up memory allocated for dateformat_data
 */
void dateformat_data_free( dateformat_data* datef_data )
{
	if( !datef_data )
		return;

	if( datef_data->udatf )
		udat_close( datef_data->udatf );

	datef_data->udatf = NULL;
	intl_error_reset( &datef_data->error );
}
/* }}} */

/* {{{ dateformat_data* dateformat_data_create()
 * Allocate memory for dateformat_data and initialize it with default values.
 */
dateformat_data* dateformat_data_create( void )
{
	dateformat_data* datef_data = ecalloc( 1, sizeof(dateformat_data) );

	dateformat_data_init( datef_data );

	return datef_data;
}
/* }}} */
