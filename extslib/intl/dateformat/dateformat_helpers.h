/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATEFORMAT_HELPERS_H
#define	DATEFORMAT_HELPERS_H

#ifndef __cplusplus
#error For C++ only
#endif

#include <unicode/calendar.h>
#include <unicode/datefmt.h>

extern "C" {
#include "../hyss_intl.h"
}

using icu::Locale;
using icu::Calendar;
using icu::DateFormat;

int datefmt_process_calendar_arg(zval* calendar_zv,
	Locale const& locale,
	const char *func_name,
	intl_error *err,
	Calendar*& cal,
	gear_long& cal_int_type,
	bool& calendar_owned);

#endif	/* DATEFORMAT_HELPERS_H */
