/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "../hyss_intl.h"
#include "dateformat_class.h"
#include "../intl_convert.h"
#include "dateformat_class.h"
#include "dateformat_attr.h"

#include <unicode/ustring.h>
#include <unicode/udat.h>

/* {{{ proto unicode IntlDateFormatter::getDateType( )
 * Get formatter datetype. }}} */
/* {{{ proto string datefmt_get_datetype( IntlDateFormatter $mf )
 * Get formatter datetype.
 */
HYSS_FUNCTION( datefmt_get_datetype )
{
	DATE_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O", &object, IntlDateFormatter_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"datefmt_get_datetype: unable to parse input params", 0 );
		RETURN_FALSE;
	}

	/* Fetch the object. */
	DATE_FORMAT_METHOD_FETCH_OBJECT;

	INTL_METHOD_CHECK_STATUS(dfo, "Error getting formatter datetype." );

	RETURN_LONG(dfo->date_type );
}
/* }}} */

/* {{{ proto unicode IntlDateFormatter::getTimeType( )
 * Get formatter timetype. }}} */
/* {{{ proto string datefmt_get_timetype( IntlDateFormatter $mf )
 * Get formatter timetype.
 */
HYSS_FUNCTION( datefmt_get_timetype )
{
	DATE_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O", &object, IntlDateFormatter_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"datefmt_get_timetype: unable to parse input params", 0 );
		RETURN_FALSE;
	}

	/* Fetch the object. */
	DATE_FORMAT_METHOD_FETCH_OBJECT;

	INTL_METHOD_CHECK_STATUS(dfo, "Error getting formatter timetype." );

	RETURN_LONG(dfo->time_type );
}
/* }}} */

/* {{{ proto string IntlDateFormatter::getPattern( )
 * Get formatter pattern. }}} */
/* {{{ proto string datefmt_get_pattern( IntlDateFormatter $mf )
 * Get formatter pattern.
 */
HYSS_FUNCTION( datefmt_get_pattern )
{
	UChar  value_buf[64];
	uint32_t    length = USIZE( value_buf );
	UChar* value  = value_buf;
	gear_bool   is_pattern_localized =FALSE;

	DATE_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O", &object, IntlDateFormatter_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"datefmt_get_pattern: unable to parse input params", 0 );
		RETURN_FALSE;
	}

	/* Fetch the object. */
	DATE_FORMAT_METHOD_FETCH_OBJECT;

	length = udat_toPattern(DATE_FORMAT_OBJECT(dfo), is_pattern_localized, value, length, &INTL_DATA_ERROR_CODE(dfo));
	if(INTL_DATA_ERROR_CODE(dfo) == U_BUFFER_OVERFLOW_ERROR && length >= USIZE( value_buf )) {
		++length; /* to avoid U_STRING_NOT_TERMINATED_WARNING */
		INTL_DATA_ERROR_CODE(dfo) = U_ZERO_ERROR;
		value = eumalloc(length);
		length = udat_toPattern(DATE_FORMAT_OBJECT(dfo), is_pattern_localized, value, length, &INTL_DATA_ERROR_CODE(dfo) );
		if(U_FAILURE(INTL_DATA_ERROR_CODE(dfo))) {
			efree(value);
			value = value_buf;
		}
	}
	INTL_METHOD_CHECK_STATUS(dfo, "Error getting formatter pattern" );

	INTL_METHOD_RETVAL_UTF8( dfo, value, length, ( value != value_buf ) );
}
/* }}} */

/* {{{ proto bool IntlDateFormatter::setPattern( string $pattern )
 * Set formatter pattern. }}} */
/* {{{ proto bool datefmt_set_pattern( IntlDateFormatter $mf, string $pattern )
 * Set formatter pattern.
 */
HYSS_FUNCTION( datefmt_set_pattern )
{
	char*       value = NULL;
	size_t      value_len = 0;
	int32_t     slength = 0;
	UChar*	    svalue  = NULL;
	gear_bool   is_pattern_localized =FALSE;


	DATE_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Os",
		&object, IntlDateFormatter_ce_ptr,  &value, &value_len ) == FAILURE )
	{
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"datefmt_set_pattern: unable to parse input params", 0);
		RETURN_FALSE;
	}

	DATE_FORMAT_METHOD_FETCH_OBJECT;

	/* Convert given pattern to UTF-16. */
	intl_convert_utf8_to_utf16(&svalue, &slength, value, value_len, &INTL_DATA_ERROR_CODE(dfo));
	INTL_METHOD_CHECK_STATUS(dfo, "Error converting pattern to UTF-16" );

	udat_applyPattern(DATE_FORMAT_OBJECT(dfo), (UBool)is_pattern_localized, svalue, slength);

	if (svalue) {
		efree(svalue);
	}
	INTL_METHOD_CHECK_STATUS(dfo, "Error setting symbol value");

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto string IntlDateFormatter::getLocale()
 * Get formatter locale. }}} */
/* {{{ proto string datefmt_get_locale(IntlDateFormatter $mf)
 * Get formatter locale.
 */
HYSS_FUNCTION( datefmt_get_locale )
{
	char *loc;
	gear_long  loc_type =ULOC_ACTUAL_LOCALE;

	DATE_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O|l",
		&object, IntlDateFormatter_ce_ptr,&loc_type) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"datefmt_get_locale: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	DATE_FORMAT_METHOD_FETCH_OBJECT;

	loc = (char *)udat_getLocaleByType(DATE_FORMAT_OBJECT(dfo), loc_type,&INTL_DATA_ERROR_CODE(dfo));
	INTL_METHOD_CHECK_STATUS(dfo, "Error getting locale");
	RETURN_STRING(loc);
}
/* }}} */

/* {{{ proto string IntlDateFormatter::isLenient()
 * Get formatter isLenient. }}} */
/* {{{ proto string datefmt_isLenient(IntlDateFormatter $mf)
 * Get formatter locale.
 */
HYSS_FUNCTION( datefmt_is_lenient )
{

	DATE_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O",
		&object, IntlDateFormatter_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"datefmt_is_lenient: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	DATE_FORMAT_METHOD_FETCH_OBJECT;

	RETVAL_BOOL(udat_isLenient(DATE_FORMAT_OBJECT(dfo)));
}
/* }}} */

/* {{{ proto string IntlDateFormatter::setLenient()
 * Set formatter lenient. }}} */
/* {{{ proto string datefmt_setLenient(IntlDateFormatter $mf)
 * Set formatter lenient.
 */
HYSS_FUNCTION( datefmt_set_lenient )
{
	gear_bool isLenient  = FALSE;

	DATE_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Ob",
	&object, IntlDateFormatter_ce_ptr,&isLenient ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"datefmt_set_lenient: unable to parse input params", 0 );
		RETURN_FALSE;
	}

	/* Fetch the object. */
	DATE_FORMAT_METHOD_FETCH_OBJECT;

	udat_setLenient(DATE_FORMAT_OBJECT(dfo), (UBool)isLenient );
}
/* }}} */
