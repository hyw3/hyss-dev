/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATE_FORMAT_CLASS_H
#define DATE_FORMAT_CLASS_H

#include <hyss.h>

#include "intl_common.h"
#include "intl_error.h"
#include "intl_data.h"
#include "dateformat_data.h"

typedef struct {
	dateformat_data	datef_data;
	int				date_type;
	int				time_type;
	int				calendar;
	char			*requested_locale;
	gear_object		zo;
} IntlDateFormatter_object;

static inline IntlDateFormatter_object *hyss_intl_dateformatter_fetch_object(gear_object *obj) {
	return (IntlDateFormatter_object *)((char*)(obj) - XtOffsetOf(IntlDateFormatter_object, zo));
}
#define Z_INTL_DATEFORMATTER_P(zv) hyss_intl_dateformatter_fetch_object(Z_OBJ_P(zv))

void dateformat_register_IntlDateFormatter_class( void );
extern gear_class_entry *IntlDateFormatter_ce_ptr;

/* Auxiliary macros */

#define DATE_FORMAT_METHOD_INIT_VARS	INTL_METHOD_INIT_VARS(IntlDateFormatter, dfo)
#define DATE_FORMAT_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_DATEFORMATTER, dfo)
#define DATE_FORMAT_METHOD_FETCH_OBJECT				\
		DATE_FORMAT_METHOD_FETCH_OBJECT_NO_CHECK;	\
	if (dfo->datef_data.udatf == NULL)				\
	{												\
		intl_errors_set(&dfo->datef_data.error, U_ILLEGAL_ARGUMENT_ERROR, "Found unconstructed IntlDateFormatter", 0); \
		RETURN_FALSE;								\
	}

#define DATE_FORMAT_OBJECT(dfo)		(dfo)->datef_data.udatf

#endif // #ifndef DATE_FORMAT_CLASS_H
