/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../intl_cppshims.h"

#include <unicode/calendar.h>
#include <unicode/gregocal.h>

#include "dateformat_helpers.h"

extern "C" {
#include "../hyss_intl.h"
#include <Gear/gear_operators.h>
#define USE_CALENDAR_POINTER 1
#include "../calendar/calendar_class.h"
}

using icu::GregorianCalendar;

int datefmt_process_calendar_arg(zval* calendar_zv,
								 Locale const& locale,
								 const char *func_name,
								 intl_error *err,
								 Calendar*& cal,
								 gear_long& cal_int_type,
								 bool& calendar_owned)
{
	char *msg;
	UErrorCode status = UErrorCode();

	if (calendar_zv == NULL || Z_TYPE_P(calendar_zv) == IS_NULL) {

		// default requested
		cal = new GregorianCalendar(locale, status);
		calendar_owned = true;

		cal_int_type = UCAL_GREGORIAN;

	} else if (Z_TYPE_P(calendar_zv) == IS_LONG) {

		gear_long v = Z_LVAL_P(calendar_zv);
		if (v != (gear_long)UCAL_TRADITIONAL && v != (gear_long)UCAL_GREGORIAN) {
			spprintf(&msg, 0, "%s: invalid value for calendar type; it must be "
					"one of IntlDateFormatter::TRADITIONAL (locale's default "
					"calendar) or IntlDateFormatter::GREGORIAN. "
					"Alternatively, it can be an IntlCalendar object",
					func_name);
			intl_errors_set(err, U_ILLEGAL_ARGUMENT_ERROR, msg, 1);
			efree(msg);
			return FAILURE;
		} else if (v == (gear_long)UCAL_TRADITIONAL) {
			cal = Calendar::createInstance(locale, status);
		} else { //UCAL_GREGORIAN
			cal = new GregorianCalendar(locale, status);
		}
		calendar_owned = true;

		cal_int_type = Z_LVAL_P(calendar_zv);

	} else if (Z_TYPE_P(calendar_zv) == IS_OBJECT &&
			instanceof_function_ex(Z_OBJCE_P(calendar_zv),
			Calendar_ce_ptr, 0)) {

		cal = calendar_fetch_native_calendar(calendar_zv);
		if (cal == NULL) {
			spprintf(&msg, 0, "%s: Found unconstructed IntlCalendar object",
					func_name);
			intl_errors_set(err, U_ILLEGAL_ARGUMENT_ERROR, msg, 1);
			efree(msg);
			return FAILURE;
		}
		calendar_owned = false;

		cal_int_type = -1;

	} else {
		spprintf(&msg, 0, "%s: Invalid calendar argument; should be an integer "
				"or an IntlCalendar instance", func_name);
		intl_errors_set(err, U_ILLEGAL_ARGUMENT_ERROR, msg, 1);
		efree(msg);
		return FAILURE;
	}

	if (cal == NULL && !U_FAILURE(status)) {
		status = U_MEMORY_ALLOCATION_ERROR;
	}
	if (U_FAILURE(status)) {
		spprintf(&msg, 0, "%s: Failure instantiating calendar", func_name);
		intl_errors_set(err, U_ILLEGAL_ARGUMENT_ERROR, msg, 1);
		efree(msg);
		return FAILURE;
	}

	return SUCCESS;
}
