/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATE_FORMATTER_H
#define DATE_FORMATTER_H

#include <hyss.h>

HYSS_FUNCTION( datefmt_create );
HYSS_FUNCTION( datefmt_get_error_code );
HYSS_FUNCTION( datefmt_get_error_message );
HYSS_METHOD( IntlDateFormatter, __construct );
void dateformat_register_constants( INIT_FUNC_ARGS );

/*
These are not necessary at this point of time
#define DATEF_GREGORIAN 1
#define DATEF_CUSTOMARY 2
#define DATEF_BUDDHIST 3
#define DATEF_JAPANESE_IMPERIAL 4
*/

#define CALENDAR_SEC "tm_sec"
#define CALENDAR_MIN "tm_min"
#define CALENDAR_HOUR "tm_hour"
#define CALENDAR_MDAY "tm_mday"
#define CALENDAR_MON "tm_mon"
#define CALENDAR_YEAR "tm_year"
#define CALENDAR_WDAY "tm_wday"
#define CALENDAR_YDAY "tm_yday"
#define CALENDAR_ISDST "tm_isdst"

#endif // DATE_FORMATTER_H
