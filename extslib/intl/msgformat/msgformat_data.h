/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MSG_FORMAT_DATA_H
#define MSG_FORMAT_DATA_H

#include <hyss.h>

#include "../intl_error.h"

#include <unicode/umsg.h>

typedef struct {
	// error hangling
	intl_error      error;

	// formatter handling
	UMessageFormat* umsgf;
	char*			orig_format;
	gear_ulong		orig_format_len;
	HashTable*		arg_types;
	int				tz_set; /* if we've already the time zone in sub-formats */
} msgformat_data;

msgformat_data* msgformat_data_create( void );
void msgformat_data_init( msgformat_data* mf_data );
void msgformat_data_free( msgformat_data* mf_data );

#ifdef MSG_FORMAT_QUOTE_APOS
int msgformat_fix_quotes(UChar **spattern, uint32_t *spattern_len, UErrorCode *ec);
#endif

#endif // MSG_FORMAT_DATA_H
