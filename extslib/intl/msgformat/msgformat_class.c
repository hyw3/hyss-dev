/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicode/unum.h>

#include "msgformat_class.h"
#include "hyss_intl.h"
#include "msgformat_data.h"
#include "msgformat_format.h"
#include "msgformat_parse.h"
#include "msgformat.h"
#include "msgformat_attr.h"

#include <gear_exceptions.h>

gear_class_entry *MessageFormatter_ce_ptr = NULL;
static gear_object_handlers MessageFormatter_handlers;

/*
 * Auxiliary functions needed by objects of 'MessageFormatter' class
 */

/* {{{ MessageFormatter_objects_free */
void MessageFormatter_object_free( gear_object *object )
{
	MessageFormatter_object* mfo = hyss_intl_messageformatter_fetch_object(object);

	gear_object_std_dtor( &mfo->zo );

	msgformat_data_free( &mfo->mf_data );
}
/* }}} */

/* {{{ MessageFormatter_object_create */
gear_object *MessageFormatter_object_create(gear_class_entry *ce)
{
	MessageFormatter_object*     intern;

	intern = gear_object_alloc(sizeof(MessageFormatter_object), ce);
	msgformat_data_init( &intern->mf_data );
	gear_object_std_init( &intern->zo, ce );
	object_properties_init(&intern->zo, ce);

	intern->zo.handlers = &MessageFormatter_handlers;

	return &intern->zo;
}
/* }}} */

/* {{{ MessageFormatter_object_clone */
gear_object *MessageFormatter_object_clone(zval *object)
{
	MessageFormatter_object *mfo, *new_mfo;
	gear_object *new_obj;

	MSG_FORMAT_METHOD_FETCH_OBJECT_NO_CHECK;
	new_obj = MessageFormatter_ce_ptr->create_object(Z_OBJCE_P(object));
	new_mfo = hyss_intl_messageformatter_fetch_object(new_obj);
	/* clone standard parts */
	gear_objects_clone_members(&new_mfo->zo, &mfo->zo);

	/* clone formatter object */
	if (MSG_FORMAT_OBJECT(mfo) != NULL) {
		MSG_FORMAT_OBJECT(new_mfo) = umsg_clone(MSG_FORMAT_OBJECT(mfo),
				&INTL_DATA_ERROR_CODE(mfo));

		if (U_FAILURE(INTL_DATA_ERROR_CODE(mfo))) {
			intl_errors_set(INTL_DATA_ERROR_P(mfo), INTL_DATA_ERROR_CODE(mfo),
					"Failed to clone MessageFormatter object", 0);
			gear_throw_exception_ex(NULL, 0, "Failed to clone MessageFormatter object");
		}
	} else {
		gear_throw_exception_ex(NULL, 0, "Cannot clone unconstructed MessageFormatter");
	}
	return new_obj;
}
/* }}} */

/*
 * 'MessageFormatter' class registration structures & functions
 */

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_messageformatter___construct, 0, 0, 2)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_messageformatter_geterrormessage, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_messageformatter_formatmessage, 0, 0, 3)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, pattern)
	GEAR_ARG_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_messageformatter_format, 0, 0, 1)
	GEAR_ARG_INFO(0, args)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_messageformatter_setpattern, 0, 0, 1)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_messageformatter_parse, 0, 0, 1)
	GEAR_ARG_INFO(0, source)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ MessageFormatter_class_functions
 * Every 'MessageFormatter' class method has an entry in this table
 */
static const gear_function_entry MessageFormatter_class_functions[] = {
	HYSS_ME( MessageFormatter, __construct, arginfo_messageformatter___construct, GEAR_ACC_PUBLIC|GEAR_ACC_CTOR )
	GEAR_FENTRY(  create, GEAR_FN( msgfmt_create ), arginfo_messageformatter___construct, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	HYSS_NAMED_FE( format, GEAR_FN( msgfmt_format ), arginfo_messageformatter_format )
	GEAR_FENTRY(  formatMessage, GEAR_FN( msgfmt_format_message ), arginfo_messageformatter_formatmessage, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	HYSS_NAMED_FE( parse, GEAR_FN( msgfmt_parse ), arginfo_messageformatter_parse )
	GEAR_FENTRY(  parseMessage, GEAR_FN( msgfmt_parse_message ), arginfo_messageformatter_formatmessage, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	HYSS_NAMED_FE( setPattern, GEAR_FN( msgfmt_set_pattern ), arginfo_messageformatter_setpattern )
	HYSS_NAMED_FE( getPattern, GEAR_FN( msgfmt_get_pattern ), arginfo_messageformatter_geterrormessage )
	HYSS_NAMED_FE( getLocale, GEAR_FN( msgfmt_get_locale ), arginfo_messageformatter_geterrormessage )
	HYSS_NAMED_FE( getErrorCode, GEAR_FN( msgfmt_get_error_code ), arginfo_messageformatter_geterrormessage )
	HYSS_NAMED_FE( getErrorMessage, GEAR_FN( msgfmt_get_error_message ), arginfo_messageformatter_geterrormessage )
	HYSS_FE_END
};
/* }}} */

/* {{{ msgformat_register_class
 * Initialize 'MessageFormatter' class
 */
void msgformat_register_class( void )
{
	gear_class_entry ce;

	/* Create and register 'MessageFormatter' class. */
	INIT_CLASS_ENTRY( ce, "MessageFormatter", MessageFormatter_class_functions );
	ce.create_object = MessageFormatter_object_create;
	MessageFormatter_ce_ptr = gear_register_internal_class( &ce );

	memcpy(&MessageFormatter_handlers, &std_object_handlers,
		sizeof MessageFormatter_handlers);
	MessageFormatter_handlers.offset = XtOffsetOf(MessageFormatter_object, zo);
	MessageFormatter_handlers.clone_obj = MessageFormatter_object_clone;
	MessageFormatter_handlers.free_obj = MessageFormatter_object_free;
}
/* }}} */

