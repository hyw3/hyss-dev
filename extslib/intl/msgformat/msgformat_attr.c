/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss_intl.h"
#include "msgformat_class.h"
#include "msgformat_attr.h"
#include "msgformat_data.h"
#include "intl_convert.h"

#include <unicode/ustring.h>

/* {{{ proto string MessageFormatter::getPattern( )
 * Get formatter pattern. }}} */
/* {{{ proto string msgfmt_get_pattern( MessageFormatter $mf )
 * Get formatter pattern.
 */
HYSS_FUNCTION( msgfmt_get_pattern )
{
	MSG_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O", &object, MessageFormatter_ce_ptr ) == FAILURE )
	{
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"msgfmt_get_pattern: unable to parse input params", 0 );
		RETURN_FALSE;
	}

	/* Fetch the object. */
	MSG_FORMAT_METHOD_FETCH_OBJECT;

	if(mfo->mf_data.orig_format) {
		RETURN_STRINGL(mfo->mf_data.orig_format, mfo->mf_data.orig_format_len);
	}

	RETURN_FALSE;
}
/* }}} */

/* {{{ proto bool MessageFormatter::setPattern( string $pattern )
 * Set formatter pattern. }}} */
/* {{{ proto bool msgfmt_set_pattern( MessageFormatter $mf, string $pattern )
 * Set formatter pattern.
 */
HYSS_FUNCTION( msgfmt_set_pattern )
{
	char*       value = NULL;
	size_t      value_len = 0;
	int32_t     spattern_len = 0;
	UChar*	    spattern  = NULL;
	MSG_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Os",
		&object, MessageFormatter_ce_ptr, &value, &value_len ) == FAILURE )
	{
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"msgfmt_set_pattern: unable to parse input params", 0);
		RETURN_FALSE;
	}

	MSG_FORMAT_METHOD_FETCH_OBJECT;

	/* Convert given pattern to UTF-16. */
	intl_convert_utf8_to_utf16(&spattern, &spattern_len, value, value_len, &INTL_DATA_ERROR_CODE(mfo));
	INTL_METHOD_CHECK_STATUS(mfo, "Error converting pattern to UTF-16" );

#ifdef MSG_FORMAT_QUOTE_APOS
	if(msgformat_fix_quotes(&spattern, &spattern_len, &INTL_DATA_ERROR_CODE(mfo)) != SUCCESS) {
		intl_error_set( NULL, U_INVALID_FORMAT_ERROR,
			"msgfmt_set_pattern: error converting pattern to quote-friendly format", 0 );
		RETURN_FALSE;
	}
#endif

	/* TODO: add parse error information */
	umsg_applyPattern(MSG_FORMAT_OBJECT(mfo), spattern, spattern_len, NULL, &INTL_DATA_ERROR_CODE(mfo));
	if (spattern) {
		efree(spattern);
	}
	INTL_METHOD_CHECK_STATUS(mfo, "Error setting symbol value");

	if(mfo->mf_data.orig_format) {
		efree(mfo->mf_data.orig_format);
	}
	mfo->mf_data.orig_format = estrndup(value, value_len);
	mfo->mf_data.orig_format_len = value_len;
	/* invalidate cached format types */
	if (mfo->mf_data.arg_types) {
		gear_hash_destroy(mfo->mf_data.arg_types);
		efree(mfo->mf_data.arg_types);
		mfo->mf_data.arg_types = NULL;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto string MessageFormatter::getLocale()
 * Get formatter locale. }}} */
/* {{{ proto string msgfmt_get_locale(MessageFormatter $mf)
 * Get formatter locale.
 */
HYSS_FUNCTION( msgfmt_get_locale )
{
	char *loc;
	MSG_FORMAT_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O",
		&object, MessageFormatter_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"msgfmt_get_locale: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	MSG_FORMAT_METHOD_FETCH_OBJECT;

	loc = (char *)umsg_getLocale(MSG_FORMAT_OBJECT(mfo));
	RETURN_STRING(loc);
}
/* }}} */

