/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MSG_FORMAT_HELPERS_H
#define MSG_FORMAT_HELPERS_H

int32_t umsg_format_arg_count(UMessageFormat *fmt);
void umsg_format_helper(MessageFormatter_object *mfo, HashTable *args,
						UChar **formatted, int *formatted_len);
void umsg_parse_helper(UMessageFormat *fmt, int *count, zval **args,
					   UChar *source, int source_len, UErrorCode *status);
#endif // MSG_FORMAT_HELPERS_H
