/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unicode/ustring.h>
#include "msgformat_data.h"

#include "msgformat_class.h"

/* {{{ void msgformat_data_init( msgformat_data* mf_data )
 * Initialize internals of msgformat_data.
 */
void msgformat_data_init( msgformat_data* mf_data )
{
	if( !mf_data )
		return;

	mf_data->umsgf			= NULL;
	mf_data->orig_format	= NULL;
	mf_data->arg_types		= NULL;
	mf_data->tz_set			= 0;
	intl_error_reset( &mf_data->error );
}
/* }}} */

/* {{{ void msgformat_data_free( msgformat_data* mf_data )
 * Clean up memory allocated for msgformat_data
 */
void msgformat_data_free(msgformat_data* mf_data)
{
	if (!mf_data)
		return;

	if (mf_data->umsgf)
		umsg_close(mf_data->umsgf);

	if (mf_data->orig_format) {
		efree(mf_data->orig_format);
		mf_data->orig_format = NULL;
	}

	if (mf_data->arg_types) {
		gear_hash_destroy(mf_data->arg_types);
		efree(mf_data->arg_types);
		mf_data->arg_types = NULL;
	}

	mf_data->umsgf = NULL;
	intl_error_reset(&mf_data->error);
}
/* }}} */

/* {{{ msgformat_data* msgformat_data_create()
 * Allocate memory for msgformat_data and initialize it with default values.
 */
msgformat_data* msgformat_data_create( void )
{
	msgformat_data* mf_data = ecalloc( 1, sizeof(msgformat_data) );

	msgformat_data_init( mf_data );

	return mf_data;
}
/* }}} */

#ifdef MSG_FORMAT_QUOTE_APOS
int msgformat_fix_quotes(UChar **spattern, uint32_t *spattern_len, UErrorCode *ec)
{
	if(*spattern && *spattern_len && u_strchr(*spattern, (UChar)'\'')) {
		UChar *npattern = safe_emalloc(sizeof(UChar)*2, *spattern_len, sizeof(UChar));
		uint32_t npattern_len;
		npattern_len = umsg_autoQuoteApostrophe(*spattern, *spattern_len, npattern, 2*(*spattern_len)+1, ec);
		efree(*spattern);
		if( U_FAILURE(*ec) )
		{
			return FAILURE;
		}
		npattern = erealloc(npattern, sizeof(UChar)*(npattern_len+1));
		*spattern = npattern;
		*spattern_len = npattern_len;
	}
	return SUCCESS;
}
#endif

