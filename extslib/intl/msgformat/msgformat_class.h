/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MSG_FORMAT_CLASS_H
#define MSG_FORMAT_CLASS_H

#include <hyss.h>

#include <unicode/uconfig.h>

#include "../intl_common.h"
#include "../intl_error.h"
#include "../intl_data.h"

#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM < 48
# define MSG_FORMAT_QUOTE_APOS 1
#endif

#include "msgformat_data.h"

typedef struct {
	msgformat_data  mf_data;
	gear_object     zo;
} MessageFormatter_object;


static inline MessageFormatter_object *hyss_intl_messageformatter_fetch_object(gear_object *obj) {
	return (MessageFormatter_object *)((char*)(obj) - XtOffsetOf(MessageFormatter_object, zo));
}
#define Z_INTL_MESSAGEFORMATTER_P(zv) hyss_intl_messageformatter_fetch_object(Z_OBJ_P(zv))

void msgformat_register_class( void );
extern gear_class_entry *MessageFormatter_ce_ptr;

/* Auxiliary macros */

#define MSG_FORMAT_METHOD_INIT_VARS		INTL_METHOD_INIT_VARS(MessageFormatter, mfo)
#define MSG_FORMAT_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_MESSAGEFORMATTER, mfo)
#define MSG_FORMAT_METHOD_FETCH_OBJECT									\
	MSG_FORMAT_METHOD_FETCH_OBJECT_NO_CHECK;							\
	if (MSG_FORMAT_OBJECT(mfo) == NULL)	{								\
		intl_errors_set(&mfo->mf_data.error, U_ILLEGAL_ARGUMENT_ERROR,	\
				"Found unconstructed MessageFormatter", 0);	\
		RETURN_FALSE;													\
	}

#define MSG_FORMAT_OBJECT(mfo)			(mfo)->mf_data.umsgf

#endif // #ifndef MSG_FORMAT_CLASS_H
