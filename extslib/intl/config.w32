// vim:ft=javascript

ARG_ENABLE("intl", "Enable internationalization support", "no");

if (HYSS_INTL != "no") {
	if (CHECK_LIB("icuuc.lib", "intl", HYSS_INTL) &&
					CHECK_HEADER_ADD_INCLUDE("unicode/utf.h", "CFLAGS_INTL")) {
		// always build as shared - gear_strtod.c/ICU type conflict
		EXTENSION("intl", "hyss_intl.c intl_convert.c intl_convertcpp.cpp intl_error.c ", true,
								"/I \"" + configure_capi_dirname + "\" /DGEAR_ENABLE_STATIC_PBCLS_CACHE=1");
		ADD_SOURCES(configure_capi_dirname + "/collator", "\
				collator.c \
				collator_attr.c \
				collator_class.c \
				collator_compare.c \
				collator_convert.c \
				collator_create.c \
				collator_error.c \
				collator_is_numeric.c \
				collator_locale.c \
				collator_sort.c \
				", "intl");
		ADD_SOURCES(configure_capi_dirname + "/common", "\
				common_error.c \
				common_enum.cpp \
				common_date.cpp \
				", "intl");
		ADD_SOURCES(configure_capi_dirname + "/converter", "\
				converter.c \
				", "intl");
		ADD_SOURCES(configure_capi_dirname + "/formatter", "\
				formatter.c \
				formatter_attr.c \
				formatter_class.c \
				formatter_data.c \
				formatter_format.c \
				formatter_main.c \
				formatter_parse.c \
				", "intl");
		ADD_SOURCES(configure_capi_dirname + "/locale", "\
				locale.c \
				locale_class.c \
				locale_methods.c \
				", "intl");
		ADD_SOURCES(configure_capi_dirname + "/msgformat", "\
				msgformat.c \
				msgformat_attr.c \
				msgformat_class.c \
				msgformat_data.c \
				msgformat_format.c \
				msgformat_helpers.cpp \
				msgformat_parse.c \
				", "intl");
		ADD_SOURCES(configure_capi_dirname + "/grapheme", "\
                                grapheme_string.c grapheme_util.c  \
                                ", "intl");
		ADD_SOURCES(configure_capi_dirname + "/normalizer", "\
				normalizer.c \
				normalizer_class.c \
				normalizer_normalize.c \
				", "intl");
		ADD_SOURCES(configure_capi_dirname + "/dateformat", "\
				dateformat.c \
				dateformat_class.c \
				dateformat_attr.c \
				dateformat_format.c \
				dateformat_format_object.cpp \
				dateformat_parse.c \
				dateformat_data.c \
				dateformat_attrcpp.cpp \
				dateformat_helpers.cpp \
				dateformat_create.cpp \
				", "intl");
		ADD_SOURCES(configure_capi_dirname + "/uchar", "\
				uchar.c",
				"intl");
		ADD_SOURCES(configure_capi_dirname + "/idn", "\
				idn.c",
				"intl");
		ADD_SOURCES(configure_capi_dirname + "/resourcebundle", "\
				resourcebundle.c \
				resourcebundle_class.c \
				resourcebundle_iterator.c",
				"intl");

		if (CHECK_HEADER_ADD_INCLUDE("unicode/uspoof.h", "CFLAGS_INTL")) {
			ADD_SOURCES(configure_capi_dirname + "/spoofchecker", "\
					spoofchecker.c \
					spoofchecker_class.c \
					spoofchecker_create.c \
					spoofchecker_main.c",
					"intl");
		}

		ADD_SOURCES(configure_capi_dirname + "/transliterator", "\
				transliterator.c \
				transliterator_class.c \
				transliterator_methods.c",
				"intl");

		ADD_SOURCES(configure_capi_dirname + "/timezone", "\
				timezone_class.cpp \
				timezone_methods.cpp",
				"intl");

		ADD_SOURCES(configure_capi_dirname + "/calendar", "\
				calendar_methods.cpp \
				gregoriancalendar_methods.cpp \
				calendar_class.cpp",
				"intl");

		ADD_SOURCES(configure_capi_dirname + "/breakiterator", "\
				breakiterator_class.cpp \
				breakiterator_methods.cpp \
				breakiterator_iterators.cpp \
				rulebasedbreakiterator_methods.cpp \
				codepointiterator_internal.cpp \
				codepointiterator_methods.cpp ",
				"intl");

		ADD_FLAG("LIBS_INTL", "icudt.lib icuin.lib icuio.lib");

		/* Compat for ICU before 58.1.*/
		if (CHECK_LIB("icule.lib", "intl", HYSS_INTL)) {
			ADD_FLAG("LIBS_INTL", "icule.lib");
		}
		if (CHECK_LIB("iculx.lib", "intl", HYSS_INTL)) {
			ADD_FLAG("LIBS_INTL", "iculx.lib");
		}

		ADD_FLAG("CFLAGS_INTL", "/EHsc /DUNISTR_FROM_CHAR_EXPLICIT=explicit /DUNISTR_FROM_STRING_EXPLICIT=explicit /DU_NO_DEFAULT_INCLUDE_UTF_HEADERS=1 /DU_HIDE_OBSOLETE_UTF_OLD_H=1");
		AC_DEFINE("HAVE_INTL", 1, "Internationalization support enabled");
	} else {
		WARNING("intl not enabled; libraries and/or headers not found");
	}
}
