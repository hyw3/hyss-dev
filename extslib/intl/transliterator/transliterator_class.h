/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRANSLITERATOR_CLASS_H
#define TRANSLITERATOR_CLASS_H

#include <hyss.h>

#include "intl_common.h"
#include "intl_error.h"

#include <unicode/utrans.h>

typedef struct {
	// 	error handling
	intl_error  err;

	// ICU transliterator
	UTransliterator* utrans;

	gear_object     zo;
} Transliterator_object;

static inline Transliterator_object *hyss_intl_transliterator_fetch_object(gear_object *obj) {
	return (Transliterator_object *)((char*)(obj) - XtOffsetOf(Transliterator_object, zo));
}
#define Z_INTL_TRANSLITERATOR_P(zv) hyss_intl_transliterator_fetch_object(Z_OBJ_P(zv))

#define TRANSLITERATOR_FORWARD UTRANS_FORWARD
#define TRANSLITERATOR_REVERSE UTRANS_REVERSE

#define TRANSLITERATOR_ERROR( co ) (co)->err
#define TRANSLITERATOR_ERROR_P( co ) &(TRANSLITERATOR_ERROR( co ))

#define TRANSLITERATOR_ERROR_CODE( co )   INTL_ERROR_CODE(TRANSLITERATOR_ERROR( co ))
#define TRANSLITERATOR_ERROR_CODE_P( co ) &(INTL_ERROR_CODE(TRANSLITERATOR_ERROR( co )))

#define TRANSLITERATOR_METHOD_INIT_VARS		         INTL_METHOD_INIT_VARS( Transliterator, to )
#define TRANSLITERATOR_METHOD_FETCH_OBJECT_NO_CHECK  INTL_METHOD_FETCH_OBJECT( INTL_TRANSLITERATOR, to )
#define TRANSLITERATOR_METHOD_FETCH_OBJECT\
	TRANSLITERATOR_METHOD_FETCH_OBJECT_NO_CHECK; \
	if( to->utrans == NULL ) \
	{ \
		intl_errors_set( &to->err, U_ILLEGAL_ARGUMENT_ERROR, "Found unconstructed transliterator", 0 ); \
		RETURN_FALSE; \
	}

int transliterator_object_construct( zval *object,
									 UTransliterator *utrans,
									 UErrorCode *status );

void transliterator_register_Transliterator_class( void );

extern gear_class_entry *Transliterator_ce_ptr;
extern gear_object_handlers Transliterator_handlers;

#endif /* #ifndef TRANSLITERATOR_CLASS_H */
