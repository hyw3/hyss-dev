/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRANSLITERATOR_METHODS_H
#define TRANSLITERATOR_METHODS_H

#include <hyss.h>

HYSS_FUNCTION( transliterator_create );

HYSS_FUNCTION( transliterator_create_from_rules );

HYSS_FUNCTION( transliterator_list_ids );

HYSS_FUNCTION( transliterator_create_inverse );

HYSS_FUNCTION( transliterator_transliterate );

HYSS_METHOD( Transliterator, __construct );

HYSS_FUNCTION( transliterator_get_error_code );

HYSS_FUNCTION( transliterator_get_error_message );

#endif /* #ifndef TRANSLITERATOR_METHODS_H */
