/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "transliterator_class.h"
#include "transliterator.h"
#include "intl_convert.h"

#include <unicode/ustring.h>

/* {{{ transliterator_register_constants
 * Register constants common for both (OO and procedural) APIs.
 */
void transliterator_register_constants( INIT_FUNC_ARGS )
{
	if( !Transliterator_ce_ptr )
	{
		gear_error( E_ERROR, "Transliterator class not defined" );
		return;
	}

	#define TRANSLITERATOR_EXPOSE_CONST( x ) REGISTER_LONG_CONSTANT( #x, x, CONST_PERSISTENT | CONST_CS )
	#define TRANSLITERATOR_EXPOSE_CLASS_CONST( x ) gear_declare_class_constant_long( Transliterator_ce_ptr, GEAR_STRS( #x ) - 1, TRANSLITERATOR_##x );
	#define TRANSLITERATOR_EXPOSE_CUSTOM_CLASS_CONST( name, value ) gear_declare_class_constant_long( Transliterator_ce_ptr, GEAR_STRS( name ) - 1, value );*/

	/* Normalization form constants */
	TRANSLITERATOR_EXPOSE_CLASS_CONST( FORWARD );
	TRANSLITERATOR_EXPOSE_CLASS_CONST( REVERSE );

	#undef NORMALIZER_EXPOSE_CUSTOM_CLASS_CONST
	#undef NORMALIZER_EXPOSE_CLASS_CONST
	#undef NORMALIZER_EXPOSE_CONST
}
/* }}} */

