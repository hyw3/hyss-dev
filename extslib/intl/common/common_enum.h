/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTL_COMMON_ENUM_H
#define INTL_COMMON_ENUM_H

#include <unicode/umachine.h>
#ifdef __cplusplus
#include <unicode/strenum.h>
extern "C" {
#include <math.h>
#endif
#include <hyss.h>
#include "../intl_error.h"
#include "../intl_data.h"
#ifdef __cplusplus
}
#endif

#define INTLITERATOR_ERROR(ii)						(ii)->err
#define INTLITERATOR_ERROR_P(ii)					&(INTLITERATOR_ERROR(ii))

#define INTLITERATOR_ERROR_CODE(ii)					INTL_ERROR_CODE(INTLITERATOR_ERROR(ii))
#define INTLITERATOR_ERROR_CODE_P(ii)				&(INTL_ERROR_CODE(INTLITERATOR_ERROR(ii)))

#define INTLITERATOR_METHOD_INIT_VARS				INTL_METHOD_INIT_VARS(IntlIterator, ii)
#define INTLITERATOR_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_ITERATOR, ii)
#define INTLITERATOR_METHOD_FETCH_OBJECT\
	object = getThis(); \
	INTLITERATOR_METHOD_FETCH_OBJECT_NO_CHECK; \
	if (ii->iterator == NULL) { \
		intl_errors_set(&ii->err, U_ILLEGAL_ARGUMENT_ERROR, "Found unconstructed IntlIterator", 0); \
		RETURN_FALSE; \
	}

typedef struct {
	intl_error				err;
	gear_object_iterator	*iterator;
	gear_object				zo;
} IntlIterator_object;


static inline IntlIterator_object *hyss_intl_iterator_fetch_object(gear_object *obj) {
	return (IntlIterator_object *)((char*)(obj) - XtOffsetOf(IntlIterator_object, zo));
}
#define Z_INTL_ITERATOR_P(zv) hyss_intl_iterator_fetch_object(Z_OBJ_P(zv))

typedef struct {
	gear_object_iterator	zoi;
	zval					current;
	zval					wrapping_obj;
	void					(*destroy_it)(gear_object_iterator	*iterator);
} zoi_with_current;

extern gear_class_entry *IntlIterator_ce_ptr;
extern gear_object_handlers IntlIterator_handlers;

U_CFUNC void zoi_with_current_dtor(gear_object_iterator *iter);
U_CFUNC int zoi_with_current_valid(gear_object_iterator *iter);
U_CFUNC zval *zoi_with_current_get_current_data(gear_object_iterator *iter);
U_CFUNC void zoi_with_current_invalidate_current(gear_object_iterator *iter);

#ifdef __cplusplus
using icu::StringEnumeration;
U_CFUNC void IntlIterator_from_StringEnumeration(StringEnumeration *se, zval *object);
#endif

U_CFUNC void intl_register_IntlIterator_class(void);

#endif // INTL_COMMON_ENUM_H
