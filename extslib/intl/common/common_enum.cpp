/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "../intl_cppshims.h"

// Fix build on Windows/old versions of ICU
#include <stdio.h>

#include "common_enum.h"

extern "C" {
#include <gear_interfaces.h>
#include <gear_exceptions.h>
}

gear_class_entry *IntlIterator_ce_ptr;
gear_object_handlers IntlIterator_handlers;

void zoi_with_current_dtor(gear_object_iterator *iter)
{
	zoi_with_current *zoiwc = (zoi_with_current*)iter;

	if (!Z_ISUNDEF(zoiwc->wrapping_obj)) {
		/* we have to copy the pointer because zoiwc->wrapping_obj may be
		 * changed midway the execution of zval_ptr_dtor() */
		zval *zwo = &zoiwc->wrapping_obj;

		/* object is still here, we can rely on it to call this again and
		 * destroy this object */
		zval_ptr_dtor(zwo);
	} else {
		/* Object not here anymore (we've been called by the object free handler)
		 * Note that the iterator wrapper objects (that also depend on this
		 * structure) call this function earlier, in the destruction phase, which
		 * precedes the object free phase. Therefore there's no risk on this
		 * function being called by the iterator wrapper destructor function and
		 * not finding the memory of this iterator allocated anymore. */
		iter->funcs->invalidate_current(iter);
		zoiwc->destroy_it(iter);
	}
}

U_CFUNC int zoi_with_current_valid(gear_object_iterator *iter)
{
	return Z_ISUNDEF(((zoi_with_current*)iter)->current)? FAILURE : SUCCESS;
}

U_CFUNC zval *zoi_with_current_get_current_data(gear_object_iterator *iter)
{
	return &((zoi_with_current*)iter)->current;
}

U_CFUNC void zoi_with_current_invalidate_current(gear_object_iterator *iter)
{
	zoi_with_current *zoi_iter = (zoi_with_current*)iter;
	if (!Z_ISUNDEF(zoi_iter->current)) {
		zval_ptr_dtor(&zoi_iter->current);
		ZVAL_UNDEF(&zoi_iter->current); //valid would return FAILURE now
	}
}

static void string_enum_current_move_forward(gear_object_iterator *iter)
{
	zoi_with_current *zoi_iter = (zoi_with_current*)iter;
	INTLITERATOR_METHOD_INIT_VARS;

	iter->funcs->invalidate_current(iter);

	object = &zoi_iter->wrapping_obj;
	INTLITERATOR_METHOD_FETCH_OBJECT_NO_CHECK;

	int32_t result_length;
	const char *result = ((StringEnumeration*)Z_PTR(iter->data))->next(
		&result_length, INTLITERATOR_ERROR_CODE(ii));

	intl_error_set_code(NULL, INTLITERATOR_ERROR_CODE(ii));
	if (U_FAILURE(INTLITERATOR_ERROR_CODE(ii))) {
		intl_errors_set_custom_msg(INTL_DATA_ERROR_P(ii),
			"Error fetching next iteration element", 0);
	} else if (result) {
		ZVAL_STRINGL(&zoi_iter->current, result, result_length);
	} //else we've reached the end of the enum, nothing more is required
}

static void string_enum_rewind(gear_object_iterator *iter)
{
	zoi_with_current *zoi_iter = (zoi_with_current*)iter;
	INTLITERATOR_METHOD_INIT_VARS;

	if (!Z_ISUNDEF(zoi_iter->current)) {
		iter->funcs->invalidate_current(iter);
	}

	object = &zoi_iter->wrapping_obj;
	INTLITERATOR_METHOD_FETCH_OBJECT_NO_CHECK;

	((StringEnumeration*)Z_PTR(iter->data))->reset(INTLITERATOR_ERROR_CODE(ii));

	intl_error_set_code(NULL, INTLITERATOR_ERROR_CODE(ii));
	if (U_FAILURE(INTLITERATOR_ERROR_CODE(ii))) {
		intl_errors_set_custom_msg(INTL_DATA_ERROR_P(ii),
			"Error resetting enumeration", 0);
	} else {
		iter->funcs->move_forward(iter);
	}
}

static void string_enum_destroy_it(gear_object_iterator *iter)
{
	delete (StringEnumeration*)Z_PTR(iter->data);
}

static const gear_object_iterator_funcs string_enum_object_iterator_funcs = {
	zoi_with_current_dtor,
	zoi_with_current_valid,
	zoi_with_current_get_current_data,
	NULL,
	string_enum_current_move_forward,
	string_enum_rewind,
	zoi_with_current_invalidate_current
};

U_CFUNC void IntlIterator_from_StringEnumeration(StringEnumeration *se, zval *object)
{
	IntlIterator_object *ii;
	object_init_ex(object, IntlIterator_ce_ptr);
	ii = Z_INTL_ITERATOR_P(object);
	ii->iterator = (gear_object_iterator*)emalloc(sizeof(zoi_with_current));
	gear_iterator_init(ii->iterator);
	ZVAL_PTR(&ii->iterator->data, se);
	ii->iterator->funcs = &string_enum_object_iterator_funcs;
	ii->iterator->index = 0;
	((zoi_with_current*)ii->iterator)->destroy_it = string_enum_destroy_it;
	ZVAL_COPY_VALUE(&((zoi_with_current*)ii->iterator)->wrapping_obj, object);
	ZVAL_UNDEF(&((zoi_with_current*)ii->iterator)->current);
}

static void IntlIterator_objects_free(gear_object *object)
{
	IntlIterator_object	*ii = hyss_intl_iterator_fetch_object(object);

	if (ii->iterator) {
		zval *wrapping_objp = &((zoi_with_current*)ii->iterator)->wrapping_obj;
		ZVAL_UNDEF(wrapping_objp);
		gear_iterator_dtor(ii->iterator);
	}
	intl_error_reset(INTLITERATOR_ERROR_P(ii));

	gear_object_std_dtor(&ii->zo);
}

static gear_object_iterator *IntlIterator_get_iterator(
	gear_class_entry *ce, zval *object, int by_ref)
{
	if (by_ref) {
		gear_throw_exception(NULL,
			"Iteration by reference is not supported", 0);
		return NULL;
	}

	IntlIterator_object *ii = Z_INTL_ITERATOR_P(object);

	if (ii->iterator == NULL) {
		gear_throw_exception(NULL,
			"The IntlIterator is not properly constructed", 0);
		return NULL;
	}

	GC_ADDREF(&ii->iterator->std);

	return ii->iterator;
}

static gear_object *IntlIterator_object_create(gear_class_entry *ce)
{
	IntlIterator_object	*intern;

	intern = (IntlIterator_object*)ecalloc(1, sizeof(IntlIterator_object) + sizeof(zval) * (ce->default_properties_count - 1));

	gear_object_std_init(&intern->zo, ce);
    object_properties_init(&intern->zo, ce);
	intl_error_init(INTLITERATOR_ERROR_P(intern));

	intern->iterator = NULL;

	intern->zo.handlers = &IntlIterator_handlers;

	return &intern->zo;
}

static HYSS_METHOD(IntlIterator, current)
{
	zval *data;
	INTLITERATOR_METHOD_INIT_VARS;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"IntlIterator::current: bad arguments", 0);
		return;
	}

	INTLITERATOR_METHOD_FETCH_OBJECT;
	data = ii->iterator->funcs->get_current_data(ii->iterator);
	if (data) {
		ZVAL_COPY_DEREF(return_value, data);
	}
}

static HYSS_METHOD(IntlIterator, key)
{
	INTLITERATOR_METHOD_INIT_VARS;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"IntlIterator::key: bad arguments", 0);
		return;
	}

	INTLITERATOR_METHOD_FETCH_OBJECT;

	if (ii->iterator->funcs->get_current_key) {
		ii->iterator->funcs->get_current_key(ii->iterator, return_value);
	} else {
		RETURN_LONG(ii->iterator->index);
	}
}

static HYSS_METHOD(IntlIterator, next)
{
	INTLITERATOR_METHOD_INIT_VARS;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"IntlIterator::next: bad arguments", 0);
		return;
	}

	INTLITERATOR_METHOD_FETCH_OBJECT;
	ii->iterator->funcs->move_forward(ii->iterator);
	/* foreach also advances the index after the last iteration,
	 * so I see no problem in incrementing the index here unconditionally */
	ii->iterator->index++;
}

static HYSS_METHOD(IntlIterator, rewind)
{
	INTLITERATOR_METHOD_INIT_VARS;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"IntlIterator::rewind: bad arguments", 0);
		return;
	}

	INTLITERATOR_METHOD_FETCH_OBJECT;
	if (ii->iterator->funcs->rewind) {
		ii->iterator->funcs->rewind(ii->iterator);
	} else {
		intl_errors_set(INTLITERATOR_ERROR_P(ii), U_UNSUPPORTED_ERROR,
			"IntlIterator::rewind: rewind not supported", 0);
	}
}

static HYSS_METHOD(IntlIterator, valid)
{
	INTLITERATOR_METHOD_INIT_VARS;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"IntlIterator::valid: bad arguments", 0);
		return;
	}

	INTLITERATOR_METHOD_FETCH_OBJECT;
	RETURN_BOOL(ii->iterator->funcs->valid(ii->iterator) == SUCCESS);
}

GEAR_BEGIN_ARG_INFO_EX(ainfo_se_void, 0, 0, 0)
GEAR_END_ARG_INFO()

static const gear_function_entry IntlIterator_class_functions[] = {
	HYSS_ME(IntlIterator,	current,	ainfo_se_void,			GEAR_ACC_PUBLIC)
	HYSS_ME(IntlIterator,	key,		ainfo_se_void,			GEAR_ACC_PUBLIC)
	HYSS_ME(IntlIterator,	next,		ainfo_se_void,			GEAR_ACC_PUBLIC)
	HYSS_ME(IntlIterator,	rewind,		ainfo_se_void,			GEAR_ACC_PUBLIC)
	HYSS_ME(IntlIterator,	valid,		ainfo_se_void,			GEAR_ACC_PUBLIC)
	HYSS_FE_END
};


/* {{{ intl_register_IntlIterator_class
 * Initialize 'IntlIterator' class
 */
U_CFUNC void intl_register_IntlIterator_class(void)
{
	gear_class_entry ce;

	/* Create and register 'IntlIterator' class. */
	INIT_CLASS_ENTRY(ce, "IntlIterator", IntlIterator_class_functions);
	ce.create_object = IntlIterator_object_create;
	IntlIterator_ce_ptr = gear_register_internal_class(&ce);
	IntlIterator_ce_ptr->get_iterator = IntlIterator_get_iterator;
	gear_class_implements(IntlIterator_ce_ptr, 1,
		gear_ce_iterator);

	memcpy(&IntlIterator_handlers, &std_object_handlers,
		sizeof IntlIterator_handlers);
	IntlIterator_handlers.offset = XtOffsetOf(IntlIterator_object, zo);
	IntlIterator_handlers.clone_obj = NULL;
	IntlIterator_handlers.dtor_obj = gear_objects_destroy_object;
	IntlIterator_handlers.free_obj = IntlIterator_objects_free;

}
