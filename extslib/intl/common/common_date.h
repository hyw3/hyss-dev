/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_DATE_H
#define	COMMON_DATE_H

#include <unicode/umachine.h>

U_CDECL_BEGIN
#include <hyss.h>
#include "../intl_error.h"
U_CDECL_END

#ifdef __cplusplus

#include <unicode/timezone.h>

using icu::TimeZone;

U_CFUNC TimeZone *timezone_convert_datetimezone(int type, void *object, int is_datetime, intl_error *outside_error, const char *func);
U_CFUNC int intl_datetime_decompose(zval *z, double *millis, TimeZone **tz,
		intl_error *err, const char *func);

#endif

U_CFUNC double intl_zval_to_millis(zval *z, intl_error *err, const char *func);

#endif	/* COMMON_DATE_H */
