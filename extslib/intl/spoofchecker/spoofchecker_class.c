/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spoofchecker_class.h"
#include "spoofchecker_main.h"
#include "spoofchecker_create.h"
#include "hyss_intl.h"
#include "intl_error.h"

#include <unicode/uspoof.h>

gear_class_entry *Spoofchecker_ce_ptr = NULL;
static gear_object_handlers Spoofchecker_handlers;

/*
 * Auxiliary functions needed by objects of 'Spoofchecker' class
 */

/* {{{ Spoofchecker_objects_free */
void Spoofchecker_objects_free(gear_object *object)
{
	Spoofchecker_object* co = hyss_intl_spoofchecker_fetch_object(object);

	gear_object_std_dtor(&co->zo);

	spoofchecker_object_destroy(co);
}
/* }}} */

/* {{{ Spoofchecker_object_create */
gear_object *Spoofchecker_object_create(gear_class_entry *ce)
{
	Spoofchecker_object*     intern;

	intern = gear_object_alloc(sizeof(Spoofchecker_object), ce);
	intl_error_init(SPOOFCHECKER_ERROR_P(intern));
	gear_object_std_init(&intern->zo, ce);
	object_properties_init(&intern->zo, ce);

	intern->zo.handlers = &Spoofchecker_handlers;

	return &intern->zo;
}
/* }}} */

/*
 * 'Spoofchecker' class registration structures & functions
 */

/* {{{ Spoofchecker methods arguments info */
GEAR_BEGIN_ARG_INFO_EX(spoofchecker_0_args, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(spoofchecker_set_checks, 0, 0, 1)
	GEAR_ARG_INFO(0, checks)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(spoofchecker_set_allowed_locales, 0, 0, 1)
	GEAR_ARG_INFO(0, locale_list)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(spoofchecker_is_suspicous, 0, 0, 1)
	GEAR_ARG_INFO(0, text)
	GEAR_ARG_INFO(1, error)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(spoofchecker_are_confusable, 0, 0, 2)
	GEAR_ARG_INFO(0, s1)
	GEAR_ARG_INFO(0, s2)
	GEAR_ARG_INFO(1, error)
GEAR_END_ARG_INFO()

#if U_ICU_VERSION_MAJOR_NUM >= 58
GEAR_BEGIN_ARG_INFO_EX(spoofchecker_set_restriction_level, 0, 0, 1)
	GEAR_ARG_INFO(0, level)
GEAR_END_ARG_INFO()
#endif

/* }}} */

/* {{{ Spoofchecker_class_functions
 * Every 'Spoofchecker' class method has an entry in this table
 */

static const gear_function_entry Spoofchecker_class_functions[] = {
	HYSS_ME(Spoofchecker, __construct, spoofchecker_0_args, GEAR_ACC_PUBLIC|GEAR_ACC_CTOR)
	HYSS_ME(Spoofchecker, isSuspicious, spoofchecker_is_suspicous, GEAR_ACC_PUBLIC)
	HYSS_ME(Spoofchecker, areConfusable, spoofchecker_are_confusable, GEAR_ACC_PUBLIC)
	HYSS_ME(Spoofchecker, setAllowedLocales, spoofchecker_set_allowed_locales, GEAR_ACC_PUBLIC)
	HYSS_ME(Spoofchecker, setChecks, spoofchecker_set_checks, GEAR_ACC_PUBLIC)
#if U_ICU_VERSION_MAJOR_NUM >= 58
	HYSS_ME(Spoofchecker, setRestrictionLevel, spoofchecker_set_restriction_level, GEAR_ACC_PUBLIC)
#endif
	HYSS_FE_END
};
/* }}} */

static gear_object *spoofchecker_clone_obj(zval *object) /* {{{ */
{
	gear_object *new_obj_val;
	Spoofchecker_object *sfo, *new_sfo;

    sfo = Z_INTL_SPOOFCHECKER_P(object);
    intl_error_reset(SPOOFCHECKER_ERROR_P(sfo));

	new_obj_val = Spoofchecker_ce_ptr->create_object(Z_OBJCE_P(object));
	new_sfo = hyss_intl_spoofchecker_fetch_object(new_obj_val);
	/* clone standard parts */
	gear_objects_clone_members(&new_sfo->zo, &sfo->zo);
	/* clone internal object */
	new_sfo->uspoof = uspoof_clone(sfo->uspoof, SPOOFCHECKER_ERROR_CODE_P(new_sfo));
	if(U_FAILURE(SPOOFCHECKER_ERROR_CODE(new_sfo))) {
		/* set up error in case error handler is interested */
		intl_error_set( NULL, SPOOFCHECKER_ERROR_CODE(new_sfo), "Failed to clone SpoofChecker object", 0 );
		Spoofchecker_objects_free(&new_sfo->zo); /* free new object */
		gear_error(E_ERROR, "Failed to clone SpoofChecker object");
	}
	return new_obj_val;
}
/* }}} */

/* {{{ spoofchecker_register_Spoofchecker_class
 * Initialize 'Spoofchecker' class
 */
void spoofchecker_register_Spoofchecker_class(void)
{
	gear_class_entry ce;

	/* Create and register 'Spoofchecker' class. */
	INIT_CLASS_ENTRY(ce, "Spoofchecker", Spoofchecker_class_functions);
	ce.create_object = Spoofchecker_object_create;
	Spoofchecker_ce_ptr = gear_register_internal_class(&ce);

	memcpy(&Spoofchecker_handlers, &std_object_handlers,
		sizeof Spoofchecker_handlers);
	Spoofchecker_handlers.offset = XtOffsetOf(Spoofchecker_object, zo);
	Spoofchecker_handlers.clone_obj = spoofchecker_clone_obj;
	Spoofchecker_handlers.free_obj = Spoofchecker_objects_free;
}
/* }}} */

/* {{{ void spoofchecker_object_init( Spoofchecker_object* co )
 * Initialize internals of Spoofchecker_object.
 * Must be called before any other call to 'spoofchecker_object_...' functions.
 */
void spoofchecker_object_init(Spoofchecker_object* co)
{
	if (!co) {
		return;
	}

	intl_error_init(SPOOFCHECKER_ERROR_P(co));
}
/* }}} */

/* {{{ void spoofchecker_object_destroy( Spoofchecker_object* co )
 * Clean up mem allocted by internals of Spoofchecker_object
 */
void spoofchecker_object_destroy(Spoofchecker_object* co)
{
	if (!co) {
		return;
	}

	if (co->uspoof) {
		uspoof_close(co->uspoof);
		co->uspoof = NULL;
	}

	intl_error_reset(SPOOFCHECKER_ERROR_P(co));
}
/* }}} */

