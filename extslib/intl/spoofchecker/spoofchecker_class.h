/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPOOFCHECKER_CLASS_H
#define SPOOFCHECKER_CLASS_H

#include <hyss.h>

#include "intl_common.h"
#include "spoofchecker_create.h"
#include "intl_error.h"
#include "intl_data.h"

#include <unicode/uspoof.h>

typedef struct {
	// error handling
	intl_error  err;

	// ICU Spoofchecker
	USpoofChecker*      uspoof;

	gear_object     zo;
} Spoofchecker_object;

static inline Spoofchecker_object *hyss_intl_spoofchecker_fetch_object(gear_object *obj) {
	    return (Spoofchecker_object *)((char*)(obj) - XtOffsetOf(Spoofchecker_object, zo));
}
#define Z_INTL_SPOOFCHECKER_P(zv) hyss_intl_spoofchecker_fetch_object((Z_OBJ_P(zv)))

#define SPOOFCHECKER_ERROR(co) (co)->err
#define SPOOFCHECKER_ERROR_P(co) &(SPOOFCHECKER_ERROR(co))

#define SPOOFCHECKER_ERROR_CODE(co)   INTL_ERROR_CODE(SPOOFCHECKER_ERROR(co))
#define SPOOFCHECKER_ERROR_CODE_P(co) &(INTL_ERROR_CODE(SPOOFCHECKER_ERROR(co)))

void spoofchecker_register_Spoofchecker_class(void);

void spoofchecker_object_init(Spoofchecker_object* co);
void spoofchecker_object_destroy(Spoofchecker_object* co);

extern gear_class_entry *Spoofchecker_ce_ptr;

/* Auxiliary macros */

#define SPOOFCHECKER_METHOD_INIT_VARS       \
    zval*             object  = getThis();   \
    Spoofchecker_object*  co  = NULL;   \
    intl_error_reset(NULL); \

#define SPOOFCHECKER_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_SPOOFCHECKER, co)
#define SPOOFCHECKER_METHOD_FETCH_OBJECT							\
	SPOOFCHECKER_METHOD_FETCH_OBJECT_NO_CHECK;						\
	if (co->uspoof == NULL)	{										\
		intl_errors_set(&co->err, U_ILLEGAL_ARGUMENT_ERROR,			\
				"Found unconstructed Spoofchecker", 0);	\
		RETURN_FALSE;												\
	}

// Macro to check return value of a ucol_* function call.
#define SPOOFCHECKER_CHECK_STATUS(co, msg)                                        \
    intl_error_set_code(NULL, SPOOFCHECKER_ERROR_CODE(co));           \
    if (U_FAILURE(SPOOFCHECKER_ERROR_CODE(co))) {                                  \
        intl_errors_set_custom_msg(SPOOFCHECKER_ERROR_P(co), msg, 0); \
        RETURN_FALSE;                                                           \
    }                                                                           \

#if U_ICU_VERSION_MAJOR_NUM >= 58
#define SPOOFCHECKER_DEFAULT_RESTRICTION_LEVEL USPOOF_HIGHLY_RESTRICTIVE
#endif

#endif // #ifndef SPOOFCHECKER_CLASS_H
