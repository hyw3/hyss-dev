/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss_intl.h"
#include "spoofchecker_class.h"

/* {{{ proto bool Spoofchecker::isSuspicious( string text[, int &error_code ] )
 * Checks if a given text contains any suspicious characters
 */
HYSS_METHOD(Spoofchecker, isSuspicious)
{
	int ret;
	char *text;
	size_t text_len;
	zval *error_code = NULL;
	SPOOFCHECKER_METHOD_INIT_VARS;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "s|z", &text, &text_len, &error_code)) {
		return;
	}

	SPOOFCHECKER_METHOD_FETCH_OBJECT;

	ret = uspoof_checkUTF8(co->uspoof, text, text_len, NULL, SPOOFCHECKER_ERROR_CODE_P(co));

	if (U_FAILURE(SPOOFCHECKER_ERROR_CODE(co))) {
		hyss_error_docref(NULL, E_WARNING, "(%d) %s", SPOOFCHECKER_ERROR_CODE(co), u_errorName(SPOOFCHECKER_ERROR_CODE(co)));
		RETURN_TRUE;
	}

	if (error_code) {
		zval_ptr_dtor(error_code);
		ZVAL_LONG(error_code, ret);
	}
	RETVAL_BOOL(ret != 0);
}
/* }}} */

/* {{{ proto bool Spoofchecker::areConfusable( string str1, string str2[, int &error_code ] )
 * Checks if a given text contains any confusable characters
 */
HYSS_METHOD(Spoofchecker, areConfusable)
{
	int ret;
	char *s1, *s2;
	size_t s1_len, s2_len;
	zval *error_code = NULL;
	SPOOFCHECKER_METHOD_INIT_VARS;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "ss|z", &s1, &s1_len,
										 &s2, &s2_len, &error_code)) {
		return;
	}

	SPOOFCHECKER_METHOD_FETCH_OBJECT;
	if(s1_len > INT32_MAX || s2_len > INT32_MAX) {
		SPOOFCHECKER_ERROR_CODE(co) = U_BUFFER_OVERFLOW_ERROR;
	} else {
		ret = uspoof_areConfusableUTF8(co->uspoof, s1, (int32_t)s1_len, s2, (int32_t)s2_len, SPOOFCHECKER_ERROR_CODE_P(co));
	}
	if (U_FAILURE(SPOOFCHECKER_ERROR_CODE(co))) {
		hyss_error_docref(NULL, E_WARNING, "(%d) %s", SPOOFCHECKER_ERROR_CODE(co), u_errorName(SPOOFCHECKER_ERROR_CODE(co)));
		RETURN_TRUE;
	}

	if (error_code) {
		zval_ptr_dtor(error_code);
		ZVAL_LONG(error_code, ret);
	}
	RETVAL_BOOL(ret != 0);
}
/* }}} */

/* {{{ proto void Spoofchecker::setAllowedLocales( string locales )
 * Locales to use when running checks
 */
HYSS_METHOD(Spoofchecker, setAllowedLocales)
{
	char *locales;
	size_t locales_len;
	SPOOFCHECKER_METHOD_INIT_VARS;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "s", &locales, &locales_len)) {
		return;
	}

	SPOOFCHECKER_METHOD_FETCH_OBJECT;

	uspoof_setAllowedLocales(co->uspoof, locales, SPOOFCHECKER_ERROR_CODE_P(co));

	if (U_FAILURE(SPOOFCHECKER_ERROR_CODE(co))) {
		hyss_error_docref(NULL, E_WARNING, "(%d) %s", SPOOFCHECKER_ERROR_CODE(co), u_errorName(SPOOFCHECKER_ERROR_CODE(co)));
		return;
	}
}
/* }}} */

/* {{{ proto void Spoofchecker::setChecks( int checks )
 * Set the checks to run
 */
HYSS_METHOD(Spoofchecker, setChecks)
{
	gear_long checks;
	SPOOFCHECKER_METHOD_INIT_VARS;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "l", &checks)) {
		return;
	}

	SPOOFCHECKER_METHOD_FETCH_OBJECT;

	uspoof_setChecks(co->uspoof, checks, SPOOFCHECKER_ERROR_CODE_P(co));

	if (U_FAILURE(SPOOFCHECKER_ERROR_CODE(co))) {
		hyss_error_docref(NULL, E_WARNING, "(%d) %s", SPOOFCHECKER_ERROR_CODE(co), u_errorName(SPOOFCHECKER_ERROR_CODE(co)));
	}
}
/* }}} */

#if U_ICU_VERSION_MAJOR_NUM >= 58
/* {{{ proto void Spoofchecker::setRestrictionLevel( int $restriction_level )
 * Set the loosest restriction level allowed for strings.
 */
HYSS_METHOD(Spoofchecker, setRestrictionLevel)
{
	gear_long level;
	SPOOFCHECKER_METHOD_INIT_VARS;

	if (FAILURE == gear_parse_parameters(GEAR_NUM_ARGS(), "l", &level)) {
		return;
	}

	SPOOFCHECKER_METHOD_FETCH_OBJECT;

	if (USPOOF_ASCII != level &&
			USPOOF_SINGLE_SCRIPT_RESTRICTIVE != level &&
			USPOOF_HIGHLY_RESTRICTIVE != level &&
			USPOOF_MODERATELY_RESTRICTIVE != level &&
			USPOOF_MINIMALLY_RESTRICTIVE != level &&
			USPOOF_UNRESTRICTIVE != level) {
		hyss_error_docref(NULL, E_WARNING, "Invalid restriction level value");
		return;
	}

	uspoof_setRestrictionLevel(co->uspoof, (URestrictionLevel)level);
}
/* }}} */
#endif

