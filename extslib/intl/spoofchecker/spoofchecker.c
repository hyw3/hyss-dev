/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "spoofchecker_class.h"
#include "spoofchecker.h"

#include <unicode/uspoof.h>


/* {{{ spoofchecker_register_constants
 * Register constants
 */
void spoofchecker_register_constants(INIT_FUNC_ARGS)
{
	if (!Spoofchecker_ce_ptr)
	{
		gear_error(E_ERROR, "Spoofchecker class not defined");
		return;
	}

	#define SPOOFCHECKER_EXPOSE_CLASS_CONST(x) gear_declare_class_constant_long(Spoofchecker_ce_ptr, GEAR_STRS( #x ) - 1, USPOOF_##x);

	SPOOFCHECKER_EXPOSE_CLASS_CONST(SINGLE_SCRIPT_CONFUSABLE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(MIXED_SCRIPT_CONFUSABLE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(WHOLE_SCRIPT_CONFUSABLE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(ANY_CASE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(SINGLE_SCRIPT)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(INVISIBLE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(CHAR_LIMIT)

#if U_ICU_VERSION_MAJOR_NUM >= 58
	SPOOFCHECKER_EXPOSE_CLASS_CONST(ASCII)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(HIGHLY_RESTRICTIVE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(MODERATELY_RESTRICTIVE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(MINIMALLY_RESTRICTIVE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(UNRESTRICTIVE)
	SPOOFCHECKER_EXPOSE_CLASS_CONST(SINGLE_SCRIPT_RESTRICTIVE)
#endif

	#undef SPOOFCHECKER_EXPOSE_CLASS_CONST
}
/* }}} */

