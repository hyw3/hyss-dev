/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "converter.h"
#include "gear_exceptions.h"

#include <unicode/utypes.h>
#if U_ICU_VERSION_MAJOR_NUM >= 49
#include <unicode/utf8.h>
#include <unicode/utf16.h>
#endif
#include <unicode/ucnv.h>
#include <unicode/ustring.h>

#include "../intl_error.h"
#include "../intl_common.h"

typedef struct _hyss_converter_object {
	UConverter *src, *dest;
	gear_fcall_info to_cb, from_cb;
	gear_fcall_info_cache to_cache, from_cache;
	intl_error error;
	gear_object obj;
} hyss_converter_object;


static inline hyss_converter_object *hyss_converter_fetch_object(gear_object *obj) {
	return (hyss_converter_object *)((char*)(obj) - XtOffsetOf(hyss_converter_object, obj));
}
#define Z_INTL_CONVERTER_P(zv) hyss_converter_fetch_object(Z_OBJ_P(zv))

static gear_class_entry     *hyss_converter_ce;
static gear_object_handlers  hyss_converter_object_handlers;

#define CONV_GET(pzv)  (Z_INTL_CONVERTER_P((pzv)))
#define THROW_UFAILURE(obj, fname, error) hyss_converter_throw_failure(obj, error, \
                                          fname "() returned error " GEAR_LONG_FMT ": %s", (gear_long)error, u_errorName(error))

/* {{{ hyss_converter_throw_failure */
static inline void hyss_converter_throw_failure(hyss_converter_object *objval, UErrorCode error, const char *format, ...) {
	intl_error *err = objval ? &(objval->error) : NULL;
	char message[1024];
	va_list vargs;

	va_start(vargs, format);
	vsnprintf(message, sizeof(message), format, vargs);
	va_end(vargs);

	intl_errors_set(err, error, message, 1);
}
/* }}} */

/* {{{ hyss_converter_default_callback */
static void hyss_converter_default_callback(zval *return_value, zval *zobj, gear_long reason, zval *error) {
	ZVAL_DEREF(error);
	zval_ptr_dtor(error);
	ZVAL_LONG(error, U_ZERO_ERROR);
	/* Basic functionality so children can call parent::toUCallback() */
	switch (reason) {
		case UCNV_UNASSIGNED:
		case UCNV_ILLEGAL:
		case UCNV_IRREGULAR:
		{
			hyss_converter_object *objval = (hyss_converter_object*)CONV_GET(zobj);
			char chars[127];
			int8_t chars_len = sizeof(chars);
			UErrorCode uerror = U_ZERO_ERROR;
            if(!objval->src) {
                hyss_converter_throw_failure(objval, U_INVALID_STATE_ERROR, "Source Converter has not been initialized yet");
				chars[0] = 0x1A;
				chars[1] = 0;
				chars_len = 1;
                ZVAL_LONG(error, U_INVALID_STATE_ERROR);
                RETVAL_STRINGL(chars, chars_len);
                return;
            }

			/* Yes, this is fairly wasteful at first glance,
			 * but considering that the alternative is to store
			 * what's sent into setSubstChars() and the fact
			 * that this is an extremely unlikely codepath
			 * I'd rather take the CPU hit here, than waste time
			 * storing a value I'm unlikely to use.
			 */
			ucnv_getSubstChars(objval->src, chars, &chars_len, &uerror);
			if (U_FAILURE(uerror)) {
				THROW_UFAILURE(objval, "ucnv_getSubstChars", uerror);
				chars[0] = 0x1A;
				chars[1] = 0;
				chars_len = 1;
            	ZVAL_LONG(error, uerror);
			}
			RETVAL_STRINGL(chars, chars_len);
		}
	}
}
/* }}} */

/* {{{ proto void UConverter::toUCallback(int $reason,
                                          string $source, string $codeUnits,
                                          int &$error) */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_toUCallback_arginfo, 0, GEAR_RETURN_VALUE, 4)
	GEAR_ARG_INFO(0, reason)
	GEAR_ARG_INFO(0, source)
	GEAR_ARG_INFO(0, codeUnits)
	GEAR_ARG_INFO(1, error)
GEAR_END_ARG_INFO();
static HYSS_METHOD(UConverter, toUCallback) {
	gear_long reason;
	zval *source, *codeUnits, *error;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lzzz",
		&reason, &source, &codeUnits, &error) == FAILURE) {
		return;
	}

	hyss_converter_default_callback(return_value, getThis(), reason, error);
}
/* }}} */

/* {{{ proto void UConverter::fromUCallback(int $reason,
                                            Array $source, int $codePoint,
                                            int &$error) */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_fromUCallback_arginfo, 0, GEAR_RETURN_VALUE, 4)
	GEAR_ARG_INFO(0, reason)
	GEAR_ARG_INFO(0, source)
	GEAR_ARG_INFO(0, codePoint)
	GEAR_ARG_INFO(1, error)
GEAR_END_ARG_INFO();
static HYSS_METHOD(UConverter, fromUCallback) {
	gear_long reason;
	zval *source, *codePoint, *error;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lzzz",
		&reason, &source, &codePoint, &error) == FAILURE) {
		return;
	}

	hyss_converter_default_callback(return_value, getThis(), reason, error);
}
/* }}} */

/* {{{ hyss_converter_check_limits */
static inline gear_bool hyss_converter_check_limits(hyss_converter_object *objval, gear_long available, gear_long needed) {
	if (available < needed) {
		hyss_converter_throw_failure(objval, U_BUFFER_OVERFLOW_ERROR, "Buffer overrun " GEAR_LONG_FMT " bytes needed, " GEAR_LONG_FMT " available", needed, available);
		return 0;
	}
	return 1;
}
/* }}} */

#define TARGET_CHECK(cnvargs, needed) hyss_converter_check_limits(objval, cnvargs->targetLimit - cnvargs->target, needed)

/* {{{ hyss_converter_append_toUnicode_target */
static void hyss_converter_append_toUnicode_target(zval *val, UConverterToUnicodeArgs *args, hyss_converter_object *objval) {
	switch (Z_TYPE_P(val)) {
		case IS_NULL:
			/* Code unit is being skipped */
			return;
		case IS_LONG:
		{
			gear_long lval = Z_LVAL_P(val);
			if ((lval < 0) || (lval > 0x10FFFF)) {
				hyss_converter_throw_failure(objval, U_ILLEGAL_ARGUMENT_ERROR, "Invalid codepoint U+%04lx", lval);
				return;
			}
			if (lval > 0xFFFF) {
				/* Supplemental planes U+010000 - U+10FFFF */
				if (TARGET_CHECK(args, 2)) {
					/* TODO: Find the ICU call which does this properly */
					*(args->target++) = (UChar)(((lval - 0x10000) >> 10)   | 0xD800);
					*(args->target++) = (UChar)(((lval - 0x10000) & 0x3FF) | 0xDC00);
				}
				return;
			}
			/* Non-suggogate BMP codepoint */
			if (TARGET_CHECK(args, 1)) {
				*(args->target++) = (UChar)lval;
			}
			return;
		}
		case IS_STRING:
		{
			const char *strval = Z_STRVAL_P(val);
			int i = 0, strlen = Z_STRLEN_P(val);

			while((i != strlen) && TARGET_CHECK(args, 1)) {
				UChar c;
				U8_NEXT(strval, i, strlen, c);
				*(args->target++) = c;
			}
			return;
		}
		case IS_ARRAY:
		{
			HashTable *ht = Z_ARRVAL_P(val);
			zval *tmpzval;

			GEAR_HASH_FOREACH_VAL(ht, tmpzval) {
				hyss_converter_append_toUnicode_target(tmpzval, args, objval);
			} GEAR_HASH_FOREACH_END();
			return;
		}
		default:
			hyss_converter_throw_failure(objval, U_ILLEGAL_ARGUMENT_ERROR,
                                                    "toUCallback() specified illegal type for substitution character");
	}
}
/* }}} */

/* {{{ hyss_converter_to_u_callback */
static void hyss_converter_to_u_callback(const void *context,
                                        UConverterToUnicodeArgs *args,
                                        const char *codeUnits, int32_t length,
                                        UConverterCallbackReason reason,
                                        UErrorCode *pErrorCode) {
	hyss_converter_object *objval = (hyss_converter_object*)context;
	zval retval;
	zval zargs[4];

	ZVAL_LONG(&zargs[0], reason);
	ZVAL_STRINGL(&zargs[1], args->source, args->sourceLimit - args->source);
	ZVAL_STRINGL(&zargs[2], codeUnits, length);
	ZVAL_LONG(&zargs[3], *pErrorCode);

	objval->to_cb.param_count    = 4;
	objval->to_cb.params = zargs;
	objval->to_cb.retval = &retval;
	objval->to_cb.no_separation  = 0;
	if (gear_call_function(&(objval->to_cb), &(objval->to_cache)) == FAILURE) {
		/* Unlikely */
		hyss_converter_throw_failure(objval, U_INTERNAL_PROGRAM_ERROR, "Unexpected failure calling toUCallback()");
	} else if (!Z_ISUNDEF(retval)) {
		hyss_converter_append_toUnicode_target(&retval, args, objval);
		zval_ptr_dtor(&retval);
	}

	if (Z_TYPE(zargs[3]) == IS_LONG) {
		*pErrorCode = Z_LVAL(zargs[3]);
	} else if (Z_ISREF(zargs[3]) && Z_TYPE_P(Z_REFVAL(zargs[3])) == IS_LONG) {
		*pErrorCode = Z_LVAL_P(Z_REFVAL(zargs[3]));
	}

	zval_ptr_dtor(&zargs[0]);
	zval_ptr_dtor(&zargs[1]);
	zval_ptr_dtor(&zargs[2]);
	zval_ptr_dtor(&zargs[3]);
}
/* }}} */

/* {{{ hyss_converter_append_fromUnicode_target */
static void hyss_converter_append_fromUnicode_target(zval *val, UConverterFromUnicodeArgs *args, hyss_converter_object *objval) {
	switch (Z_TYPE_P(val)) {
		case IS_NULL:
			/* Ignore */
			return;
		case IS_LONG:
			if (TARGET_CHECK(args, 1)) {
				*(args->target++) = Z_LVAL_P(val);
			}
			return;
		case IS_STRING:
		{
			size_t vallen = Z_STRLEN_P(val);
			if (TARGET_CHECK(args, vallen)) {
				memcpy(args->target, Z_STRVAL_P(val), vallen);
				args->target += vallen;
			}
			return;
		}
		case IS_ARRAY:
		{
			HashTable *ht = Z_ARRVAL_P(val);
			zval *tmpzval;
			GEAR_HASH_FOREACH_VAL(ht, tmpzval) {
				hyss_converter_append_fromUnicode_target(tmpzval, args, objval);
			} GEAR_HASH_FOREACH_END();
			return;
		}
		default:
			hyss_converter_throw_failure(objval, U_ILLEGAL_ARGUMENT_ERROR, "fromUCallback() specified illegal type for substitution character");
	}
}
/* }}} */

/* {{{ hyss_converter_from_u_callback */
static void hyss_converter_from_u_callback(const void *context,
                                          UConverterFromUnicodeArgs *args,
                                          const UChar *codeUnits, int32_t length, UChar32 codePoint,
                                          UConverterCallbackReason reason,
                                          UErrorCode *pErrorCode) {
	hyss_converter_object *objval = (hyss_converter_object*)context;
	zval retval;
	zval zargs[4];
	int i;

	ZVAL_LONG(&zargs[0], reason);
	array_init(&zargs[1]);
	i = 0;
	while (i < length) {
		UChar32 c;
		U16_NEXT(codeUnits, i, length, c);
		add_next_index_long(&zargs[1], c);
	}
	ZVAL_LONG(&zargs[2], codePoint);
	ZVAL_LONG(&zargs[3], *pErrorCode);

	objval->from_cb.param_count = 4;
	objval->from_cb.params = zargs;
	objval->from_cb.retval = &retval;
	objval->from_cb.no_separation  = 0;
	if (gear_call_function(&(objval->from_cb), &(objval->from_cache)) == FAILURE) {
		/* Unlikely */
		hyss_converter_throw_failure(objval, U_INTERNAL_PROGRAM_ERROR, "Unexpected failure calling fromUCallback()");
	} else if (!Z_ISUNDEF(retval)) {
		hyss_converter_append_fromUnicode_target(&retval, args, objval);
		zval_ptr_dtor(&retval);
	}

	if (Z_TYPE(zargs[3]) == IS_LONG) {
		*pErrorCode = Z_LVAL(zargs[3]);
	} else if (Z_ISREF(zargs[3]) && Z_TYPE_P(Z_REFVAL(zargs[3])) == IS_LONG) {
		*pErrorCode = Z_LVAL_P(Z_REFVAL(zargs[3]));
	}

	zval_ptr_dtor(&zargs[0]);
	zval_ptr_dtor(&zargs[1]);
	zval_ptr_dtor(&zargs[2]);
	zval_ptr_dtor(&zargs[3]);
}
/* }}} */

/* {{{ hyss_converter_set_callbacks */
static inline gear_bool hyss_converter_set_callbacks(hyss_converter_object *objval, UConverter *cnv) {
	gear_bool ret = 1;
	UErrorCode error = U_ZERO_ERROR;

	if (objval->obj.ce == hyss_converter_ce) {
		/* Short-circuit having to go through method calls and data marshalling
		 * when we're using default behavior
		 */
		return 1;
	}

	ucnv_setToUCallBack(cnv, (UConverterToUCallback)hyss_converter_to_u_callback, (const void*)objval,
                                 NULL, NULL, &error);
	if (U_FAILURE(error)) {
		THROW_UFAILURE(objval, "ucnv_setToUCallBack", error);
		ret = 0;
	}

	error = U_ZERO_ERROR;
	ucnv_setFromUCallBack(cnv, (UConverterFromUCallback)hyss_converter_from_u_callback, (const void*)objval,
                                    NULL, NULL, &error);
	if (U_FAILURE(error)) {
		THROW_UFAILURE(objval, "ucnv_setFromUCallBack", error);
		ret = 0;
	}
	return ret;
}
/* }}} */

/* {{{ hyss_converter_set_encoding */
static gear_bool hyss_converter_set_encoding(hyss_converter_object *objval,
                                            UConverter **pcnv,
                                            const char *enc, size_t enc_len
                                           ) {
	UErrorCode error = U_ZERO_ERROR;
	UConverter *cnv = ucnv_open(enc, &error);

	if (error == U_AMBIGUOUS_ALIAS_WARNING) {
		UErrorCode getname_error = U_ZERO_ERROR;
		const char *actual_encoding = ucnv_getName(cnv, &getname_error);
		if (U_FAILURE(getname_error)) {
			/* Should never happen */
			actual_encoding = "(unknown)";
		}
		hyss_error_docref(NULL, E_WARNING, "Ambiguous encoding specified, using %s", actual_encoding);
	} else if (U_FAILURE(error)) {
		if (objval) {
			THROW_UFAILURE(objval, "ucnv_open", error);
		} else {
			hyss_error_docref(NULL, E_WARNING, "Error setting encoding: %d - %s", (int)error, u_errorName(error));
		}
		return 0;
	}

	if (objval && !hyss_converter_set_callbacks(objval, cnv)) {
		return 0;
	}

	if (*pcnv) {
		ucnv_close(*pcnv);
	}
	*pcnv = cnv;
	return 1;
}
/* }}} */

/* {{{ hyss_converter_do_set_encoding */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_set_encoding_arginfo, 0, GEAR_RETURN_VALUE, 1)
	GEAR_ARG_INFO(0, encoding)
GEAR_END_ARG_INFO();
static void hyss_converter_do_set_encoding(UConverter **pcnv, INTERNAL_FUNCTION_PARAMETERS) {
	hyss_converter_object *objval = CONV_GET(getThis());
	char *enc;
	size_t enc_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &enc, &enc_len) == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR, "Bad arguments, "
				"expected one string argument", 0);
		RETURN_FALSE;
	}
	intl_errors_reset(&objval->error);

	RETURN_BOOL(hyss_converter_set_encoding(objval, pcnv, enc, enc_len));
}
/* }}} */

/* {{{ proto bool UConverter::setSourceEncoding(string encoding) */
static HYSS_METHOD(UConverter, setSourceEncoding) {
	hyss_converter_object *objval = CONV_GET(getThis());
	hyss_converter_do_set_encoding(&(objval->src), INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto bool UConverter::setDestinationEncoding(string encoding) */
static HYSS_METHOD(UConverter, setDestinationEncoding) {
	hyss_converter_object *objval = CONV_GET(getThis());
	hyss_converter_do_set_encoding(&(objval->dest), INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ hyss_converter_do_get_encoding */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_get_encoding_arginfo, 0, GEAR_RETURN_VALUE, 0)
GEAR_END_ARG_INFO();
static void hyss_converter_do_get_encoding(hyss_converter_object *objval, UConverter *cnv, INTERNAL_FUNCTION_PARAMETERS) {
	const char *name;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR, "Expected no arguments", 0);
		RETURN_FALSE;
	}

	intl_errors_reset(&objval->error);

	if (!cnv) {
		RETURN_NULL();
	}

	name = ucnv_getName(cnv, &objval->error.code);
	if (U_FAILURE(objval->error.code)) {
		THROW_UFAILURE(objval, "ucnv_getName()", objval->error.code);
		RETURN_FALSE;
	}

	RETURN_STRING(name);
}
/* }}} */

/* {{{ proto string UConverter::getSourceEncoding() */
static HYSS_METHOD(UConverter, getSourceEncoding) {
	hyss_converter_object *objval = CONV_GET(getThis());
	hyss_converter_do_get_encoding(objval, objval->src, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto string UConverter::getDestinationEncoding() */
static HYSS_METHOD(UConverter, getDestinationEncoding) {
        hyss_converter_object *objval = CONV_GET(getThis());
        hyss_converter_do_get_encoding(objval, objval->dest, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ hyss_converter_do_get_type */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_get_type_arginfo, 0, GEAR_RETURN_VALUE, 0)
GEAR_END_ARG_INFO();
static void hyss_converter_do_get_type(hyss_converter_object *objval, UConverter *cnv, INTERNAL_FUNCTION_PARAMETERS) {
	UConverterType t;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR, "Expected no arguments", 0);
		RETURN_FALSE;
	}
	intl_errors_reset(&objval->error);

	if (!cnv) {
		RETURN_NULL();
	}

	t = ucnv_getType(cnv);
	if (U_FAILURE(objval->error.code)) {
		THROW_UFAILURE(objval, "ucnv_getType", objval->error.code);
		RETURN_FALSE;
	}

	RETURN_LONG(t);
}
/* }}} */

/* {{{ proto int UConverter::getSourceType() */
static HYSS_METHOD(UConverter, getSourceType) {
	hyss_converter_object *objval = CONV_GET(getThis());
	hyss_converter_do_get_type(objval, objval->src, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto int UConverter::getDestinationType() */
static HYSS_METHOD(UConverter, getDestinationType) {
	hyss_converter_object *objval = CONV_GET(getThis());
	hyss_converter_do_get_type(objval, objval->dest, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ hyss_converter_resolve_callback */
static void hyss_converter_resolve_callback(zval *zobj,
                                           hyss_converter_object *objval,
                                           const char *callback_name,
                                           gear_fcall_info *finfo,
                                           gear_fcall_info_cache *fcache) {
	char *errstr = NULL;
	zval caller;

	array_init(&caller);
	Z_ADDREF_P(zobj);
	add_index_zval(&caller, 0, zobj);
	add_index_string(&caller, 1, callback_name);
	if (gear_fcall_info_init(&caller, 0, finfo, fcache, NULL, &errstr) == FAILURE) {
		hyss_converter_throw_failure(objval, U_INTERNAL_PROGRAM_ERROR, "Error setting converter callback: %s", errstr);
	}
	gear_array_destroy(Z_ARR(caller));
	ZVAL_UNDEF(&finfo->function_name);
	if (errstr) {
		efree(errstr);
	}
}
/* }}} */

/* {{{ proto UConverter::__construct([string dest = 'utf-8',[string src = 'utf-8']]) */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_arginfo, 0, GEAR_RETURN_VALUE, 0)
	GEAR_ARG_INFO(0, destination_encoding)
	GEAR_ARG_INFO(0, source_encoding)
GEAR_END_ARG_INFO();

static HYSS_METHOD(UConverter, __construct) {
	hyss_converter_object *objval = CONV_GET(getThis());
	char *src = "utf-8";
	size_t src_len = sizeof("utf-8") - 1;
	char *dest = src;
	size_t dest_len = src_len;

	intl_error_reset(NULL);

	if (gear_parse_parameters_throw(GEAR_NUM_ARGS(), "|s!s!", &dest, &dest_len, &src, &src_len) == FAILURE) {
		return;
	}

	hyss_converter_set_encoding(objval, &(objval->src),  src,  src_len );
	hyss_converter_set_encoding(objval, &(objval->dest), dest, dest_len);
	hyss_converter_resolve_callback(getThis(), objval, "toUCallback",   &(objval->to_cb),   &(objval->to_cache));
	hyss_converter_resolve_callback(getThis(), objval, "fromUCallback", &(objval->from_cb), &(objval->from_cache));
}
/* }}} */

/* {{{ proto bool UConverter::setSubstChars(string $chars) */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_setSubstChars_arginfo, 0, GEAR_RETURN_VALUE, 1)
	GEAR_ARG_INFO(0, chars)
GEAR_END_ARG_INFO();

static HYSS_METHOD(UConverter, setSubstChars) {
	hyss_converter_object *objval = CONV_GET(getThis());
	char *chars;
	size_t chars_len;
	int ret = 1;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &chars, &chars_len) == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::setSubstChars(): bad arguments", 0);
		RETURN_FALSE;
	}
	intl_errors_reset(&objval->error);

	if (objval->src) {
		UErrorCode error = U_ZERO_ERROR;
		ucnv_setSubstChars(objval->src, chars, chars_len, &error);
		if (U_FAILURE(error)) {
			THROW_UFAILURE(objval, "ucnv_setSubstChars", error);
			ret = 0;
		}
	} else {
		hyss_converter_throw_failure(objval, U_INVALID_STATE_ERROR, "Source Converter has not been initialized yet");
		ret = 0;
	}

	if (objval->dest) {
		UErrorCode error = U_ZERO_ERROR;
		ucnv_setSubstChars(objval->dest, chars, chars_len, &error);
		if (U_FAILURE(error)) {
			THROW_UFAILURE(objval, "ucnv_setSubstChars", error);
			ret = 0;
		}
	} else {
		hyss_converter_throw_failure(objval, U_INVALID_STATE_ERROR, "Destination Converter has not been initialized yet");
		ret = 0;
	}

	RETURN_BOOL(ret);
}
/* }}} */

/* {{{ proto string UConverter::getSubstChars() */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_getSubstChars_arginfo, 0, GEAR_RETURN_VALUE, 0)
GEAR_END_ARG_INFO();

static HYSS_METHOD(UConverter, getSubstChars) {
	hyss_converter_object *objval = CONV_GET(getThis());
	char chars[127];
	int8_t chars_len = sizeof(chars);
	UErrorCode error = U_ZERO_ERROR;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::getSubstChars(): expected no arguments", 0);
		RETURN_FALSE;
	}
	intl_errors_reset(&objval->error);

	if (!objval->src) {
		RETURN_NULL();
	}

	/* src and dest get the same subst chars set,
	 * so it doesn't really matter which one we read from
	 */
	ucnv_getSubstChars(objval->src, chars, &chars_len, &error);
	if (U_FAILURE(error)) {
		THROW_UFAILURE(objval, "ucnv_getSubstChars", error);
		RETURN_FALSE;
	}

	RETURN_STRINGL(chars, chars_len);
}
/* }}} */

/* {{{ hyss_converter_do_convert */
static gear_string* hyss_converter_do_convert(UConverter *dest_cnv,
                                             UConverter *src_cnv,  const char *src, int32_t src_len,
                                             hyss_converter_object *objval
                                            ) {
	UErrorCode	error = U_ZERO_ERROR;
	int32_t		temp_len, ret_len;
	gear_string	*ret;
	UChar		*temp;

	if (!src_cnv || !dest_cnv) {
		hyss_converter_throw_failure(objval, U_INVALID_STATE_ERROR,
		                            "Internal converters not initialized");
		return NULL;
	}

	/* Get necessary buffer size first */
	temp_len = 1 + ucnv_toUChars(src_cnv, NULL, 0, src, src_len, &error);
	if (U_FAILURE(error) && error != U_BUFFER_OVERFLOW_ERROR) {
		THROW_UFAILURE(objval, "ucnv_toUChars", error);
		return NULL;
	}
	temp = safe_emalloc(sizeof(UChar), temp_len, sizeof(UChar));

	/* Convert to intermediate UChar* array */
	error = U_ZERO_ERROR;
	temp_len = ucnv_toUChars(src_cnv, temp, temp_len, src, src_len, &error);
	if (U_FAILURE(error)) {
		THROW_UFAILURE(objval, "ucnv_toUChars", error);
		efree(temp);
		return NULL;
	}
	temp[temp_len] = 0;

	/* Get necessary output buffer size */
	ret_len = ucnv_fromUChars(dest_cnv, NULL, 0, temp, temp_len, &error);
	if (U_FAILURE(error) && error != U_BUFFER_OVERFLOW_ERROR) {
		THROW_UFAILURE(objval, "ucnv_fromUChars", error);
		efree(temp);
		return NULL;
	}

	ret = gear_string_alloc(ret_len, 0);

	/* Convert to final encoding */
	error = U_ZERO_ERROR;
	ZSTR_LEN(ret) = ucnv_fromUChars(dest_cnv, ZSTR_VAL(ret), ret_len+1, temp, temp_len, &error);
	efree(temp);
	if (U_FAILURE(error)) {
		THROW_UFAILURE(objval, "ucnv_fromUChars", error);
		gear_string_efree(ret);
		return NULL;
	}

	return ret;
}
/* }}} */

/* {{{ proto string UConverter::reasonText(int reason) */
#define UCNV_REASON_CASE(v) case (UCNV_ ## v) : RETURN_STRINGL( "REASON_" #v , sizeof( "REASON_" #v ) - 1);
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_reasontext_arginfo, 0, GEAR_RETURN_VALUE, 0)
	GEAR_ARG_INFO(0, reason)
GEAR_END_ARG_INFO();
static HYSS_METHOD(UConverter, reasonText) {
	gear_long reason;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &reason) == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::reasonText(): bad arguments", 0);
		RETURN_FALSE;
	}
	intl_error_reset(NULL);

	switch (reason) {
		UCNV_REASON_CASE(UNASSIGNED)
		UCNV_REASON_CASE(ILLEGAL)
		UCNV_REASON_CASE(IRREGULAR)
		UCNV_REASON_CASE(RESET)
		UCNV_REASON_CASE(CLOSE)
		UCNV_REASON_CASE(CLONE)
		default:
			hyss_error_docref(NULL, E_WARNING, "Unknown UConverterCallbackReason: " GEAR_LONG_FMT, reason);
			RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto string UConverter::convert(string str[, bool reverse]) */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_convert_arginfo, 0, GEAR_RETURN_VALUE, 1)
        GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, reverse)
GEAR_END_ARG_INFO();

static HYSS_METHOD(UConverter, convert) {
        hyss_converter_object *objval = CONV_GET(getThis());
	char *str;
	size_t str_len;
	gear_string *ret;
	gear_bool reverse = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s|b",
	                          &str, &str_len, &reverse) == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::convert(): bad arguments", 0);
		RETURN_FALSE;
	}
	intl_errors_reset(&objval->error);

	ret = hyss_converter_do_convert(reverse ? objval->src : objval->dest,
	                               reverse ? objval->dest : objval->src,
	                               str,   str_len,
	                               objval);
	if (ret) {
		RETURN_NEW_STR(ret);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto string UConverter::transcode(string $str, string $toEncoding, string $fromEncoding[, Array $options = array()]) */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_transcode_arginfo, 0, GEAR_RETURN_VALUE, 3)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, toEncoding)
	GEAR_ARG_INFO(0, fromEncoding)
	GEAR_ARG_ARRAY_INFO(0, options, 1)
GEAR_END_ARG_INFO();

static HYSS_METHOD(UConverter, transcode) {
	char *str, *src, *dest;
	size_t str_len, src_len, dest_len;
	zval *options = NULL;
	UConverter *src_cnv = NULL, *dest_cnv = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "sss|a!",
			&str, &str_len, &dest, &dest_len, &src, &src_len, &options) == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::transcode(): bad arguments", 0);
		RETURN_FALSE;
	}
	intl_error_reset(NULL);

	if (hyss_converter_set_encoding(NULL, &src_cnv,  src,  src_len) &&
	    hyss_converter_set_encoding(NULL, &dest_cnv, dest, dest_len)) {
	    gear_string *ret;
		UErrorCode error = U_ZERO_ERROR;

		if (options && gear_hash_num_elements(Z_ARRVAL_P(options))) {
			zval *tmpzval;

			if (U_SUCCESS(error) &&
				(tmpzval = gear_hash_str_find(Z_ARRVAL_P(options), "from_subst", sizeof("from_subst") - 1)) != NULL &&
				Z_TYPE_P(tmpzval) == IS_STRING) {
				error = U_ZERO_ERROR;
				ucnv_setSubstChars(src_cnv, Z_STRVAL_P(tmpzval), Z_STRLEN_P(tmpzval) & 0x7F, &error);
			}
			if (U_SUCCESS(error) &&
				(tmpzval = gear_hash_str_find(Z_ARRVAL_P(options), "to_subst", sizeof("to_subst") - 1)) != NULL &&
				Z_TYPE_P(tmpzval) == IS_STRING) {
				error = U_ZERO_ERROR;
				ucnv_setSubstChars(dest_cnv, Z_STRVAL_P(tmpzval), Z_STRLEN_P(tmpzval) & 0x7F, &error);
			}
		}

		if (U_SUCCESS(error) &&
			(ret = hyss_converter_do_convert(dest_cnv, src_cnv, str, str_len, NULL)) != NULL) {
			RETURN_NEW_STR(ret);
		}

		if (U_FAILURE(error)) {
			THROW_UFAILURE(NULL, "transcode", error);
			RETVAL_FALSE;
		}
	} else {
		RETVAL_FALSE;
	}

	if (src_cnv) {
		ucnv_close(src_cnv);
	}
	if (dest_cnv) {
		ucnv_close(dest_cnv);
	}
}
/* }}} */

/* {{{ proto int UConverter::getErrorCode() */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_geterrorcode_arginfo, 0, GEAR_RETURN_VALUE, 0)
GEAR_END_ARG_INFO();
static HYSS_METHOD(UConverter, getErrorCode) {
	hyss_converter_object *objval = CONV_GET(getThis());

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::getErrorCode(): expected no arguments", 0);
		RETURN_FALSE;
	}

	RETURN_LONG(intl_error_get_code(&(objval->error)));
}
/* }}} */

/* {{{ proto string UConverter::getErrorMessage() */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_geterrormsg_arginfo, 0, GEAR_RETURN_VALUE, 0)
GEAR_END_ARG_INFO();
static HYSS_METHOD(UConverter, getErrorMessage) {
	hyss_converter_object *objval = CONV_GET(getThis());
	gear_string *message = intl_error_get_message(&(objval->error));

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::getErrorMessage(): expected no arguments", 0);
		RETURN_FALSE;
	}

	if (message) {
		RETURN_STR(message);
	} else {
		RETURN_NULL();
	}
}
/* }}} */

/* {{{ proto array UConverter::getAvailable() */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_getavailable_arginfo, 0, GEAR_RETURN_VALUE, 0)
GEAR_END_ARG_INFO();
static HYSS_METHOD(UConverter, getAvailable) {
	int32_t i,
			count = ucnv_countAvailable();

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::getErrorMessage(): expected no arguments", 0);
		RETURN_FALSE;
	}
	intl_error_reset(NULL);

	array_init(return_value);
	for(i = 0; i < count; i++) {
		const char *name = ucnv_getAvailableName(i);
		add_next_index_string(return_value, name);
	}
}
/* }}} */

/* {{{ proto array UConverter::getAliases(string name) */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_getaliases_arginfo, 0, GEAR_RETURN_VALUE, 1)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO();
static HYSS_METHOD(UConverter, getAliases) {
	char *name;
	size_t name_len;
	UErrorCode error = U_ZERO_ERROR;
	uint16_t i, count;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &name, &name_len) == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::getAliases(): bad arguments", 0);
		RETURN_FALSE;
	}
	intl_error_reset(NULL);

	count = ucnv_countAliases(name, &error);
	if (U_FAILURE(error)) {
		THROW_UFAILURE(NULL, "ucnv_countAliases", error);
		RETURN_FALSE;
	}

	array_init(return_value);
	for(i = 0; i < count; i++) {
		const char *alias;

		error = U_ZERO_ERROR;
		alias = ucnv_getAlias(name, i, &error);
		if (U_FAILURE(error)) {
			THROW_UFAILURE(NULL, "ucnv_getAlias", error);
			gear_array_destroy(Z_ARR_P(return_value));
			RETURN_NULL();
		}
		add_next_index_string(return_value, alias);
	}
}
/* }}} */

/* {{{ proto array UConverter::getStandards() */
GEAR_BEGIN_ARG_INFO_EX(hyss_converter_getstandards_arginfo, 0, GEAR_RETURN_VALUE, 0)
GEAR_END_ARG_INFO();
static HYSS_METHOD(UConverter, getStandards) {
	uint16_t i, count;

	if (gear_parse_parameters_none() == FAILURE) {
		intl_error_set(NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"UConverter::getStandards(): expected no arguments", 0);
		RETURN_FALSE;
	}
	intl_error_reset(NULL);

	array_init(return_value);
	count = ucnv_countStandards();
	for(i = 0; i < count; i++) {
		UErrorCode error = U_ZERO_ERROR;
		const char *name = ucnv_getStandard(i, &error);
		if (U_FAILURE(error)) {
			THROW_UFAILURE(NULL, "ucnv_getStandard", error);
			gear_array_destroy(Z_ARR_P(return_value));
			RETURN_NULL();
		}
		add_next_index_string(return_value, name);
	}
}
/* }}} */

static const gear_function_entry hyss_converter_methods[] = {
	HYSS_ME(UConverter, __construct,            hyss_converter_arginfo,                   GEAR_ACC_PUBLIC | GEAR_ACC_CTOR)

	/* Encoding selection */
	HYSS_ME(UConverter, setSourceEncoding,      hyss_converter_set_encoding_arginfo,      GEAR_ACC_PUBLIC)
	HYSS_ME(UConverter, setDestinationEncoding, hyss_converter_set_encoding_arginfo,      GEAR_ACC_PUBLIC)
	HYSS_ME(UConverter, getSourceEncoding,      hyss_converter_get_encoding_arginfo,      GEAR_ACC_PUBLIC)
	HYSS_ME(UConverter, getDestinationEncoding, hyss_converter_get_encoding_arginfo,      GEAR_ACC_PUBLIC)

	/* Introspection for algorithmic converters */
	HYSS_ME(UConverter, getSourceType,          hyss_converter_get_type_arginfo,          GEAR_ACC_PUBLIC)
	HYSS_ME(UConverter, getDestinationType,     hyss_converter_get_type_arginfo,          GEAR_ACC_PUBLIC)

	/* Basic codeunit error handling */
	HYSS_ME(UConverter, getSubstChars,          hyss_converter_getSubstChars_arginfo,     GEAR_ACC_PUBLIC)
	HYSS_ME(UConverter, setSubstChars,          hyss_converter_setSubstChars_arginfo,     GEAR_ACC_PUBLIC)

	/* Default callback handlers */
	HYSS_ME(UConverter, toUCallback,            hyss_converter_toUCallback_arginfo,       GEAR_ACC_PUBLIC)
	HYSS_ME(UConverter, fromUCallback,          hyss_converter_fromUCallback_arginfo,     GEAR_ACC_PUBLIC)

	/* Core conversion workhorses */
	HYSS_ME(UConverter, convert,                hyss_converter_convert_arginfo,           GEAR_ACC_PUBLIC)
	HYSS_ME(UConverter, transcode,              hyss_converter_transcode_arginfo,         GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)

	/* Error inspection */
	HYSS_ME(UConverter, getErrorCode,           hyss_converter_geterrorcode_arginfo,      GEAR_ACC_PUBLIC)
	HYSS_ME(UConverter, getErrorMessage,        hyss_converter_geterrormsg_arginfo,       GEAR_ACC_PUBLIC)

	/* Ennumeration and lookup */
	HYSS_ME(UConverter, reasonText,             hyss_converter_reasontext_arginfo,        GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME(UConverter, getAvailable,           hyss_converter_getavailable_arginfo,      GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME(UConverter, getAliases,             hyss_converter_getaliases_arginfo,        GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME(UConverter, getStandards,           hyss_converter_getstandards_arginfo,      GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_FE_END
};

/* {{{ Converter create/clone/destroy */
static void hyss_converter_dtor_object(gear_object *obj) {
	hyss_converter_object *objval = hyss_converter_fetch_object(obj);

	if (objval->src) {
		ucnv_close(objval->src);
	}

	if (objval->dest) {
		ucnv_close(objval->dest);
	}

	intl_error_reset(&(objval->error));
}

static gear_object *hyss_converter_object_ctor(gear_class_entry *ce, hyss_converter_object **pobjval) {
	hyss_converter_object *objval;

	objval = gear_object_alloc(sizeof(hyss_converter_object), ce);

	gear_object_std_init(&objval->obj, ce);
	object_properties_init(&objval->obj, ce);
	intl_error_init(&(objval->error));

	objval->obj.handlers = &hyss_converter_object_handlers;
	*pobjval = objval;

	return &objval->obj;
}

static gear_object *hyss_converter_create_object(gear_class_entry *ce) {
	hyss_converter_object *objval = NULL;
	gear_object *retval = hyss_converter_object_ctor(ce, &objval);

	object_properties_init(&(objval->obj), ce);

	return retval;
}

static gear_object *hyss_converter_clone_object(zval *object) {
	hyss_converter_object *objval, *oldobj = Z_INTL_CONVERTER_P(object);
	gear_object *retval = hyss_converter_object_ctor(Z_OBJCE_P(object), &objval);
	UErrorCode error = U_ZERO_ERROR;

	intl_errors_reset(&oldobj->error);

	objval->src = ucnv_safeClone(oldobj->src, NULL, NULL, &error);
	if (U_SUCCESS(error)) {
		error = U_ZERO_ERROR;
		objval->dest = ucnv_safeClone(oldobj->dest, NULL, NULL, &error);
	}
	if (U_FAILURE(error)) {
		gear_string *err_msg;
		THROW_UFAILURE(oldobj, "ucnv_safeClone", error);

		err_msg = intl_error_get_message(&oldobj->error);
		gear_throw_exception(NULL, ZSTR_VAL(err_msg), 0);
		gear_string_release_ex(err_msg, 0);

		return retval;
	}

	/* Update contexts for converter error handlers */
	hyss_converter_set_callbacks(objval, objval->src );
	hyss_converter_set_callbacks(objval, objval->dest);

	gear_objects_clone_members(&(objval->obj), &(oldobj->obj));

	/* Newly cloned object deliberately does not inherit error state from original object */

	return retval;
}
/* }}} */

#define CONV_REASON_CONST(v) gear_declare_class_constant_long(hyss_converter_ce, "REASON_" #v, sizeof("REASON_" #v) - 1, UCNV_ ## v)
#define CONV_TYPE_CONST(v)   gear_declare_class_constant_long(hyss_converter_ce, #v ,          sizeof(#v) - 1,           UCNV_ ## v)

/* {{{ hyss_converter_minit */
int hyss_converter_minit(INIT_FUNC_ARGS) {
	gear_class_entry ce;

	INIT_CLASS_ENTRY(ce, "UConverter", hyss_converter_methods);
	hyss_converter_ce = gear_register_internal_class(&ce);
	hyss_converter_ce->create_object = hyss_converter_create_object;
	memcpy(&hyss_converter_object_handlers, &std_object_handlers, sizeof(gear_object_handlers));
	hyss_converter_object_handlers.offset = XtOffsetOf(hyss_converter_object, obj);
	hyss_converter_object_handlers.clone_obj = hyss_converter_clone_object;
	hyss_converter_object_handlers.dtor_obj = hyss_converter_dtor_object;

	/* enum UConverterCallbackReason */
	CONV_REASON_CONST(UNASSIGNED);
	CONV_REASON_CONST(ILLEGAL);
	CONV_REASON_CONST(IRREGULAR);
	CONV_REASON_CONST(RESET);
	CONV_REASON_CONST(CLOSE);
	CONV_REASON_CONST(CLONE);

	/* enum UConverterType */
	CONV_TYPE_CONST(UNSUPPORTED_CONVERTER);
	CONV_TYPE_CONST(SBCS);
	CONV_TYPE_CONST(DBCS);
	CONV_TYPE_CONST(MBCS);
	CONV_TYPE_CONST(LATIN_1);
	CONV_TYPE_CONST(UTF8);
	CONV_TYPE_CONST(UTF16_BigEndian);
	CONV_TYPE_CONST(UTF16_LittleEndian);
	CONV_TYPE_CONST(UTF32_BigEndian);
	CONV_TYPE_CONST(UTF32_LittleEndian);
	CONV_TYPE_CONST(EBCDIC_STATEFUL);
	CONV_TYPE_CONST(ISO_2022);
	CONV_TYPE_CONST(LMBCS_1);
	CONV_TYPE_CONST(LMBCS_2);
	CONV_TYPE_CONST(LMBCS_3);
	CONV_TYPE_CONST(LMBCS_4);
	CONV_TYPE_CONST(LMBCS_5);
	CONV_TYPE_CONST(LMBCS_6);
	CONV_TYPE_CONST(LMBCS_8);
	CONV_TYPE_CONST(LMBCS_11);
	CONV_TYPE_CONST(LMBCS_16);
	CONV_TYPE_CONST(LMBCS_17);
	CONV_TYPE_CONST(LMBCS_18);
	CONV_TYPE_CONST(LMBCS_19);
	CONV_TYPE_CONST(LMBCS_LAST);
	CONV_TYPE_CONST(HZ);
	CONV_TYPE_CONST(SCSU);
	CONV_TYPE_CONST(ISCII);
	CONV_TYPE_CONST(US_ASCII);
	CONV_TYPE_CONST(UTF7);
	CONV_TYPE_CONST(BOCU1);
	CONV_TYPE_CONST(UTF16);
	CONV_TYPE_CONST(UTF32);
	CONV_TYPE_CONST(CESU8);
	CONV_TYPE_CONST(IMAP_MAILBOX);

	return SUCCESS;
}
/* }}} */

