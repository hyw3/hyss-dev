/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss_intl.h"
#include "collator_class.h"
#include "collator_compare.h"
#include "intl_convert.h"

/* {{{ proto int Collator::compare( string $str1, string $str2 )
 * Compare two strings. }}} */
/* {{{ proto int collator_compare( Collator $coll, string $str1, string $str2 )
 * Compare two strings.
 */
HYSS_FUNCTION( collator_compare )
{
	char*            str1      = NULL;
	char*            str2      = NULL;
	size_t              str1_len  = 0;
	size_t              str2_len  = 0;

	UChar*           ustr1     = NULL;
	UChar*           ustr2     = NULL;
	int              ustr1_len = 0;
	int              ustr2_len = 0;

	UCollationResult result;

	COLLATOR_METHOD_INIT_VARS

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Oss",
		&object, Collator_ce_ptr, &str1, &str1_len, &str2, &str2_len ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			 "collator_compare: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	COLLATOR_METHOD_FETCH_OBJECT;

	if (!co || !co->ucoll) {
		intl_error_set_code( NULL, COLLATOR_ERROR_CODE( co ) );
		intl_errors_set_custom_msg( COLLATOR_ERROR_P( co ),
			"Object not initialized", 0 );
		gear_throw_error(NULL, "Object not initialized");

		RETURN_FALSE;
	}

	/*
	 * Compare given strings (converting them to UTF-16 first).
	 */

	/* First convert the strings to UTF-16. */
	intl_convert_utf8_to_utf16(
		&ustr1, &ustr1_len, str1, str1_len, COLLATOR_ERROR_CODE_P( co ) );
	if( U_FAILURE( COLLATOR_ERROR_CODE( co ) ) )
	{
		/* Set global error code. */
		intl_error_set_code( NULL, COLLATOR_ERROR_CODE( co ) );

		/* Set error messages. */
		intl_errors_set_custom_msg( COLLATOR_ERROR_P( co ),
			"Error converting first argument to UTF-16", 0 );
		if (ustr1) {
			efree( ustr1 );
		}
		RETURN_FALSE;
	}

	intl_convert_utf8_to_utf16(
		&ustr2, &ustr2_len, str2, str2_len, COLLATOR_ERROR_CODE_P( co ) );
	if( U_FAILURE( COLLATOR_ERROR_CODE( co ) ) )
	{
		/* Set global error code. */
		intl_error_set_code( NULL, COLLATOR_ERROR_CODE( co ) );

		/* Set error messages. */
		intl_errors_set_custom_msg( COLLATOR_ERROR_P( co ),
			"Error converting second argument to UTF-16", 0 );
		if (ustr1) {
			efree( ustr1 );
		}
		if (ustr2) {
			efree( ustr2 );
		}
		RETURN_FALSE;
	}

	/* Then compare them. */
	result = ucol_strcoll(
		co->ucoll,
		ustr1, ustr1_len,
		ustr2, ustr2_len );

	if( ustr1 )
		efree( ustr1 );
	if( ustr2 )
		efree( ustr2 );

	/* Return result of the comparison. */
	RETURN_LONG( result );
}
/* }}} */

