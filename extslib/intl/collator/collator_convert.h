/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COLLATOR_CONVERT_H
#define COLLATOR_CONVERT_H

#include <hyss.h>
#include <unicode/utypes.h>

void collator_convert_hash_from_utf8_to_utf16( HashTable* hash, UErrorCode* status );
void collator_convert_hash_from_utf16_to_utf8( HashTable* hash, UErrorCode* status );

zval* collator_convert_zstr_utf16_to_utf8( zval* utf16_zval, zval *rv );
zval* collator_convert_zstr_utf8_to_utf16( zval* utf8_zval, zval *rv );

zval* collator_normalize_sort_argument( zval* arg, zval *rv );
zval* collator_convert_object_to_string( zval* obj, zval *rv );
zval* collator_convert_string_to_number( zval* arg, zval *rv );
zval* collator_convert_string_to_number_if_possible( zval* str, zval *rv );
zval* collator_convert_string_to_double( zval* str, zval *rv );

zval* collator_make_printable_zval( zval* arg, zval *rv );

#endif // COLLATOR_CONVERT_H
