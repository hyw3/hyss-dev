/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss_intl.h"
#include "collator_class.h"
#include "collator_convert.h"
#include "collator_attr.h"

#include <unicode/ustring.h>

/* {{{ proto int Collator::getAttribute( int $attr )
 * Get collation attribute value. }}} */
/* {{{ proto int collator_get_attribute( Collator $coll, int $attr )
 * Get collation attribute value.
 */
HYSS_FUNCTION( collator_get_attribute )
{
	gear_long attribute, value;

	COLLATOR_METHOD_INIT_VARS

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Ol",
		&object, Collator_ce_ptr, &attribute ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"collator_get_attribute: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	COLLATOR_METHOD_FETCH_OBJECT;

	value = ucol_getAttribute( co->ucoll, attribute, COLLATOR_ERROR_CODE_P( co ) );
	COLLATOR_CHECK_STATUS( co, "Error getting attribute value" );

	RETURN_LONG( value );
}
/* }}} */

/* {{{ proto bool Collator::getAttribute( int $attr )
 * Get collation attribute value. }}} */
/* {{{ proto bool collator_set_attribute( Collator $coll, int $attr, int $val )
 * Set collation attribute.
 */
HYSS_FUNCTION( collator_set_attribute )
{
	gear_long attribute, value;
	COLLATOR_METHOD_INIT_VARS


	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Oll",
		&object, Collator_ce_ptr, &attribute, &value ) == FAILURE)
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			 "collator_set_attribute: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	COLLATOR_METHOD_FETCH_OBJECT;

	/* Set new value for the given attribute. */
	ucol_setAttribute( co->ucoll, attribute, value, COLLATOR_ERROR_CODE_P( co ) );
	COLLATOR_CHECK_STATUS( co, "Error setting attribute value" );

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int Collator::getStrength()
 * Returns the current collation strength. }}} */
/* {{{ proto int collator_get_strength(Collator coll)
 * Returns the current collation strength.
 */
HYSS_FUNCTION( collator_get_strength )
{
	COLLATOR_METHOD_INIT_VARS

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O",
		&object, Collator_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			 "collator_get_strength: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	COLLATOR_METHOD_FETCH_OBJECT;

	/* Get current strength and return it. */
	RETURN_LONG( ucol_getStrength( co->ucoll ) );
}
/* }}} */

/* {{{ proto bool Collator::setStrength(int strength)
 * Set the collation strength. }}} */
/* {{{ proto bool collator_set_strength(Collator coll, int strength)
 * Set the collation strength.
 */
HYSS_FUNCTION( collator_set_strength )
{
	gear_long strength;

	COLLATOR_METHOD_INIT_VARS

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Ol",
		&object, Collator_ce_ptr, &strength ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			 "collator_set_strength: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	COLLATOR_METHOD_FETCH_OBJECT;

	/* Set given strength. */
	ucol_setStrength( co->ucoll, strength );

	RETURN_TRUE;
}
/* }}} */

