/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COLLATOR_SORT_H
#define COLLATOR_SORT_H

#include <hyss.h>

typedef int (*collator_compare_func_t)( zval *result, zval *op1, zval *op2 );

HYSS_FUNCTION( collator_sort );
HYSS_FUNCTION( collator_sort_with_sort_keys );
HYSS_FUNCTION( collator_get_sort_key );
HYSS_FUNCTION( collator_asort );

#endif // COLLATOR_SORT_H
