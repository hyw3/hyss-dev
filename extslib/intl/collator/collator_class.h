/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COLLATOR_CLASS_H
#define COLLATOR_CLASS_H

#include <hyss.h>

#include "../intl_common.h"
#include "../intl_error.h"
#include "../intl_data.h"

#include <unicode/ucol.h>

typedef struct {
	// error handling
	intl_error  err;

	// ICU collator
	UCollator*      ucoll;

	gear_object     zo;

} Collator_object;

#define COLLATOR_ERROR(co) (co)->err
#define COLLATOR_ERROR_P(co) &(COLLATOR_ERROR(co))

#define COLLATOR_ERROR_CODE(co)   INTL_ERROR_CODE(COLLATOR_ERROR(co))
#define COLLATOR_ERROR_CODE_P(co) &(INTL_ERROR_CODE(COLLATOR_ERROR(co)))

static inline Collator_object *hyss_intl_collator_fetch_object(gear_object *obj) {
	return (Collator_object *)((char*)(obj) - XtOffsetOf(Collator_object, zo));
}
#define Z_INTL_COLLATOR_P(zv) hyss_intl_collator_fetch_object(Z_OBJ_P(zv))

void collator_register_Collator_class( void );
void collator_object_init( Collator_object* co );
void collator_object_destroy( Collator_object* co );

extern gear_class_entry *Collator_ce_ptr;

/* Auxiliary macros */

#define COLLATOR_METHOD_INIT_VARS       \
    zval*             object  = NULL;   \
    Collator_object*  co      = NULL;   \
    intl_error_reset( NULL ); \

#define COLLATOR_METHOD_FETCH_OBJECT	INTL_METHOD_FETCH_OBJECT(INTL_COLLATOR, co)

// Macro to check return value of a ucol_* function call.
#define COLLATOR_CHECK_STATUS( co, msg )                                        \
    intl_error_set_code( NULL, COLLATOR_ERROR_CODE( co ) );           \
    if( U_FAILURE( COLLATOR_ERROR_CODE( co ) ) )                                \
    {                                                                           \
        intl_errors_set_custom_msg( COLLATOR_ERROR_P( co ), msg, 0 ); \
        RETURN_FALSE;                                                           \
    }                                                                           \

#endif // #ifndef COLLATOR_CLASS_H
