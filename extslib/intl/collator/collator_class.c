/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "collator_class.h"
#include "hyss_intl.h"
#include "collator_attr.h"
#include "collator_compare.h"
#include "collator_sort.h"
#include "collator_convert.h"
#include "collator_locale.h"
#include "collator_create.h"
#include "collator_error.h"
#include "intl_error.h"

#include <unicode/ucol.h>

gear_class_entry *Collator_ce_ptr = NULL;
static gear_object_handlers Collator_handlers;

/*
 * Auxiliary functions needed by objects of 'Collator' class
 */

/* {{{ Collator_objects_free */
void Collator_objects_free(gear_object *object )
{
	Collator_object* co = hyss_intl_collator_fetch_object(object);

	gear_object_std_dtor(&co->zo );

	collator_object_destroy(co );
}
/* }}} */

/* {{{ Collator_object_create */
gear_object *Collator_object_create(gear_class_entry *ce )
{
	Collator_object *intern = gear_object_alloc(sizeof(Collator_object), ce);
	intl_error_init(COLLATOR_ERROR_P(intern));
	gear_object_std_init(&intern->zo, ce );
	object_properties_init(&intern->zo, ce);

	intern->zo.handlers = &Collator_handlers;

	return &intern->zo;
}
/* }}} */

/*
 * 'Collator' class registration structures & functions
 */

/* {{{ Collator methods arguments info */
/* NOTE: modifying 'collator_XX_args' do not forget to
       modify approptiate 'collator_XX_args' for
       the procedural API.
*/
GEAR_BEGIN_ARG_INFO_EX( collator_0_args, 0, 0, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( collator_1_arg, 0, 0, 1 )
	GEAR_ARG_INFO( 0, arg1 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( collator_2_args, 0, 0, 2 )
	GEAR_ARG_INFO( 0, arg1 )
	GEAR_ARG_INFO( 0, arg2 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( collator_sort_args, 0, 0, 1 )
	GEAR_ARG_ARRAY_INFO( 1, arr, 0 )
	GEAR_ARG_INFO( 0, flags )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( collator_sort_with_sort_keys_args, 0, 0, 1 )
	GEAR_ARG_ARRAY_INFO( 1, arr, 0 )
GEAR_END_ARG_INFO()

/* }}} */

/* {{{ Collator_class_functions
 * Every 'Collator' class method has an entry in this table
 */

static const gear_function_entry Collator_class_functions[] = {
	HYSS_ME( Collator, __construct, collator_1_arg, GEAR_ACC_PUBLIC|GEAR_ACC_CTOR )
	GEAR_FENTRY( create, GEAR_FN( collator_create ), collator_1_arg, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	HYSS_NAMED_FE( compare, GEAR_FN( collator_compare ), collator_2_args )
	HYSS_NAMED_FE( sort, GEAR_FN( collator_sort ), collator_sort_args )
	HYSS_NAMED_FE( sortWithSortKeys, GEAR_FN( collator_sort_with_sort_keys ), collator_sort_with_sort_keys_args )
	HYSS_NAMED_FE( asort, GEAR_FN( collator_asort ), collator_sort_args )
	HYSS_NAMED_FE( getAttribute, GEAR_FN( collator_get_attribute ), collator_1_arg )
	HYSS_NAMED_FE( setAttribute, GEAR_FN( collator_set_attribute ), collator_2_args )
	HYSS_NAMED_FE( getStrength, GEAR_FN( collator_get_strength ), collator_0_args )
	HYSS_NAMED_FE( setStrength, GEAR_FN( collator_set_strength ), collator_1_arg )
	HYSS_NAMED_FE( getLocale, GEAR_FN( collator_get_locale ), collator_1_arg )
	HYSS_NAMED_FE( getErrorCode, GEAR_FN( collator_get_error_code ), collator_0_args )
	HYSS_NAMED_FE( getErrorMessage, GEAR_FN( collator_get_error_message ), collator_0_args )
	HYSS_NAMED_FE( getSortKey, GEAR_FN( collator_get_sort_key ), collator_1_arg )
	HYSS_FE_END
};
/* }}} */

/* {{{ collator_register_Collator_class
 * Initialize 'Collator' class
 */
void collator_register_Collator_class( void )
{
	gear_class_entry ce;

	/* Create and register 'Collator' class. */
	INIT_CLASS_ENTRY( ce, "Collator", Collator_class_functions );
	ce.create_object = Collator_object_create;
	Collator_ce_ptr = gear_register_internal_class( &ce );

	memcpy(&Collator_handlers, &std_object_handlers,
		sizeof Collator_handlers);
	/* Collator has no usable clone semantics - ucol_cloneBinary/ucol_openBinary require binary buffer
	   for which we don't have the place to keep */
	Collator_handlers.offset = XtOffsetOf(Collator_object, zo);
	Collator_handlers.clone_obj = NULL;
	Collator_handlers.free_obj = Collator_objects_free;

	/* Declare 'Collator' class properties. */
	if( !Collator_ce_ptr )
	{
		gear_error( E_ERROR,
			"Collator: attempt to create properties "
			"on a non-registered class." );
		return;
	}
}
/* }}} */

/* {{{ void collator_object_init( Collator_object* co )
 * Initialize internals of Collator_object.
 * Must be called before any other call to 'collator_object_...' functions.
 */
void collator_object_init( Collator_object* co )
{
	if( !co )
		return;

	intl_error_init( COLLATOR_ERROR_P( co ) );
}
/* }}} */

/* {{{ void collator_object_destroy( Collator_object* co )
 * Clean up mem allocted by internals of Collator_object
 */
void collator_object_destroy( Collator_object* co )
{
	if( !co )
		return;

	if( co->ucoll )
	{
		ucol_close( co->ucoll );
		co->ucoll = NULL;
	}

	intl_error_reset( COLLATOR_ERROR_P( co ) );
}
/* }}} */

