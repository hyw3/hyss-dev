/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COLLATOR_IS_NUMERIC_H
#define COLLATOR_IS_NUMERIC_H

#include <hyss.h>
#include <unicode/uchar.h>

gear_uchar collator_is_numeric( UChar *str, int32_t length, gear_long *lval, double *dval, int allow_errors );

#endif // COLLATOR_IS_NUMERIC_H
