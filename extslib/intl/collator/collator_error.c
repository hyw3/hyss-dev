/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss_intl.h"
#include "collator_class.h"
#include "collator_error.h"

/* {{{ proto int Collator::getErrorCode( Collator $coll )
 * Get collator's last error code. }}} */
/* {{{ proto int collator_get_error_code( Collator $coll )
 * Get collator's last error code.
 */
HYSS_FUNCTION( collator_get_error_code )
{
	COLLATOR_METHOD_INIT_VARS

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O",
		&object, Collator_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"collator_get_error_code: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object (without resetting its last error code). */
	co = Z_INTL_COLLATOR_P(object);
	if( co == NULL )
		RETURN_FALSE;

	/* Return collator's last error code. */
	RETURN_LONG( COLLATOR_ERROR_CODE( co ) );
}
/* }}} */

/* {{{ proto string Collator::getErrorMessage( Collator $coll )
 * Get text description for collator's last error code. }}} */
/* {{{ proto string collator_get_error_message( Collator $coll )
 * Get text description for collator's last error code.
 */
HYSS_FUNCTION( collator_get_error_message )
{
	gear_string* message = NULL;

	COLLATOR_METHOD_INIT_VARS

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "O",
		&object, Collator_ce_ptr ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"collator_get_error_message: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object (without resetting its last error code). */
	co = Z_INTL_COLLATOR_P( object );
	if( co == NULL )
		RETURN_FALSE;

	/* Return last error message. */
	message = intl_error_get_message( COLLATOR_ERROR_P( co ) );
	RETURN_STR(message);
}
/* }}} */

