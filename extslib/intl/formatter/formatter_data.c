/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "formatter_data.h"

/* {{{ void formatter_data_init( formatter_data* nf_data )
 * Initialize internals of formatter_data.
 */
void formatter_data_init( formatter_data* nf_data )
{
	if( !nf_data )
		return;

	nf_data->unum                = NULL;
	intl_error_reset( &nf_data->error );
}
/* }}} */

/* {{{ void formatter_data_free( formatter_data* nf_data )
 * Clean up mem allocted by internals of formatter_data
 */
void formatter_data_free( formatter_data* nf_data )
{
	if( !nf_data )
		return;

	if( nf_data->unum )
		unum_close( nf_data->unum );

	nf_data->unum = NULL;
	intl_error_reset( &nf_data->error );
}
/* }}} */

/* {{{ formatter_data* formatter_data_create()
 * Alloc mem for formatter_data and initialize it with default values.
 */
formatter_data* formatter_data_create( void )
{
	formatter_data* nf_data = ecalloc( 1, sizeof(formatter_data) );

	formatter_data_init( nf_data );

	return nf_data;
}
/* }}} */

