/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss_intl.h"

#include <unicode/ustring.h>
#include <locale.h>

#include "formatter_class.h"
#include "formatter_format.h"
#include "formatter_parse.h"
#include "intl_convert.h"

#define ICU_LOCALE_BUG 1

/* {{{ proto mixed NumberFormatter::parse( string $str[, int $type, int &$position ])
 * Parse a number. }}} */
/* {{{ proto mixed numfmt_parse( NumberFormatter $nf, string $str[, int $type, int &$position ])
 * Parse a number.
 */
HYSS_FUNCTION( numfmt_parse )
{
	gear_long type = FORMAT_TYPE_DOUBLE;
	UChar* sstr = NULL;
	int32_t sstr_len = 0;
	char* str = NULL;
	size_t str_len;
	int32_t val32, position = 0;
	int64_t val64;
	double val_double;
	int32_t* position_p = NULL;
	zval *zposition = NULL;
	char *oldlocale;
	FORMATTER_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Os|lz/!",
		&object, NumberFormatter_ce_ptr,  &str, &str_len, &type, &zposition ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"number_parse: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	FORMATTER_METHOD_FETCH_OBJECT;

	/* Convert given string to UTF-16. */
	intl_convert_utf8_to_utf16(&sstr, &sstr_len, str, str_len, &INTL_DATA_ERROR_CODE(nfo));
	INTL_METHOD_CHECK_STATUS( nfo, "String conversion to UTF-16 failed" );

	if(zposition) {
		ZVAL_DEREF(zposition);
		position = (int32_t)zval_get_long( zposition );
		position_p = &position;
	}

#if ICU_LOCALE_BUG && defined(LC_NUMERIC)
	/* need to copy here since setlocale may change it later */
	oldlocale = estrdup(setlocale(LC_NUMERIC, NULL));
	setlocale(LC_NUMERIC, "C");
#endif

	switch(type) {
		case FORMAT_TYPE_INT32:
			val32 = unum_parse(FORMATTER_OBJECT(nfo), sstr, sstr_len, position_p, &INTL_DATA_ERROR_CODE(nfo));
			RETVAL_LONG(val32);
			break;
		case FORMAT_TYPE_INT64:
			val64 = unum_parseInt64(FORMATTER_OBJECT(nfo), sstr, sstr_len, position_p, &INTL_DATA_ERROR_CODE(nfo));
			if(val64 > GEAR_LONG_MAX || val64 < GEAR_LONG_MIN) {
				RETVAL_DOUBLE(val64);
			} else {
				RETVAL_LONG((gear_long)val64);
			}
			break;
		case FORMAT_TYPE_DOUBLE:
			val_double = unum_parseDouble(FORMATTER_OBJECT(nfo), sstr, sstr_len, position_p, &INTL_DATA_ERROR_CODE(nfo));
			RETVAL_DOUBLE(val_double);
			break;
		default:
			hyss_error_docref(NULL, E_WARNING, "Unsupported format type " GEAR_LONG_FMT, type);
			RETVAL_FALSE;
			break;
	}
#if ICU_LOCALE_BUG && defined(LC_NUMERIC)
	setlocale(LC_NUMERIC, oldlocale);
	efree(oldlocale);
#endif
	if(zposition) {
		zval_ptr_dtor(zposition);
		ZVAL_LONG(zposition, position);
	}

	if (sstr) {
		efree(sstr);
	}

	INTL_METHOD_CHECK_STATUS( nfo, "Number parsing failed" );
}
/* }}} */

/* {{{ proto float NumberFormatter::parseCurrency( string $str, string &$currency[, int &$position] )
 * Parse a number as currency. }}} */
/* {{{ proto float numfmt_parse_currency( NumberFormatter $nf, string $str, string &$currency[, int &$position] )
 * Parse a number as currency.
 */
HYSS_FUNCTION( numfmt_parse_currency )
{
	double number;
	UChar currency[5] = {0};
	UChar* sstr = NULL;
	int32_t sstr_len = 0;
	gear_string *u8str;
	char *str;
	size_t str_len;
	int32_t* position_p = NULL;
	int32_t position = 0;
	zval *zcurrency, *zposition = NULL;
	FORMATTER_METHOD_INIT_VARS;

	/* Parse parameters. */
	if( gear_parse_method_parameters( GEAR_NUM_ARGS(), getThis(), "Osz/|z/!",
		&object, NumberFormatter_ce_ptr,  &str, &str_len, &zcurrency, &zposition ) == FAILURE )
	{
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,
			"number_parse_currency: unable to parse input params", 0 );

		RETURN_FALSE;
	}

	/* Fetch the object. */
	FORMATTER_METHOD_FETCH_OBJECT;

	/* Convert given string to UTF-16. */
	intl_convert_utf8_to_utf16(&sstr, &sstr_len, str, str_len, &INTL_DATA_ERROR_CODE(nfo));
	INTL_METHOD_CHECK_STATUS( nfo, "String conversion to UTF-16 failed" );

	if(zposition) {
		ZVAL_DEREF(zposition);
		position = (int32_t)zval_get_long( zposition );
		position_p = &position;
	}

	number = unum_parseDoubleCurrency(FORMATTER_OBJECT(nfo), sstr, sstr_len, position_p, currency, &INTL_DATA_ERROR_CODE(nfo));
	if(zposition) {
		zval_ptr_dtor(zposition);
		ZVAL_LONG(zposition, position);
	}
	if (sstr) {
		efree(sstr);
	}
	INTL_METHOD_CHECK_STATUS( nfo, "Number parsing failed" );

	/* Convert parsed currency to UTF-8 and pass it back to caller. */
	u8str = intl_convert_utf16_to_utf8(currency, u_strlen(currency), &INTL_DATA_ERROR_CODE(nfo));
	INTL_METHOD_CHECK_STATUS( nfo, "Currency conversion to UTF-8 failed" );
	zval_ptr_dtor( zcurrency );
	ZVAL_NEW_STR(zcurrency, u8str);

	RETVAL_DOUBLE( number );
}
/* }}} */

