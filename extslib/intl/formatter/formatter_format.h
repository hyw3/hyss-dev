/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FORMATTER_FORMAT_H
#define FORMATTER_FORMAT_H

#include <hyss.h>

HYSS_FUNCTION( numfmt_format );
HYSS_FUNCTION( numfmt_format_currency );

#define FORMAT_TYPE_DEFAULT	0
#define FORMAT_TYPE_INT32	1
#define FORMAT_TYPE_INT64	2
#define FORMAT_TYPE_DOUBLE	3
#define FORMAT_TYPE_CURRENCY	4

#endif // FORMATTER_FORMAT_H
