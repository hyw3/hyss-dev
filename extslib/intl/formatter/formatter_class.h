/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FORMATTER_CLASS_H
#define FORMATTER_CLASS_H

#include <hyss.h>

#include "intl_common.h"
#include "intl_error.h"
#include "intl_data.h"
#include "formatter_data.h"

typedef struct {
	formatter_data  nf_data;
	gear_object     zo;
} NumberFormatter_object;

static inline NumberFormatter_object *hyss_intl_number_format_fetch_object(gear_object *obj) {
	return (NumberFormatter_object *)((char*)(obj) - XtOffsetOf(NumberFormatter_object, zo));
}
#define Z_INTL_NUMBERFORMATTER_P(zv) hyss_intl_number_format_fetch_object(Z_OBJ_P(zv))

void formatter_register_class( void );
extern gear_class_entry *NumberFormatter_ce_ptr;

/* Auxiliary macros */

#define FORMATTER_METHOD_INIT_VARS				INTL_METHOD_INIT_VARS(NumberFormatter, nfo)
#define FORMATTER_OBJECT(nfo)					(nfo)->nf_data.unum
#define FORMATTER_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_NUMBERFORMATTER, nfo)
#define FORMATTER_METHOD_FETCH_OBJECT \
	FORMATTER_METHOD_FETCH_OBJECT_NO_CHECK; \
	if (FORMATTER_OBJECT(nfo) == NULL) \
	{ \
		intl_errors_set(&nfo->nf_data.error, U_ILLEGAL_ARGUMENT_ERROR, \
				"Found unconstructed NumberFormatter", 0); \
		RETURN_FALSE; \
	}


#endif // #ifndef FORMATTER_CLASS_H
