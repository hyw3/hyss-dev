/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicode/unum.h>

#include "formatter_class.h"
#include "hyss_intl.h"
#include "formatter_data.h"
#include "formatter_format.h"
#include "formatter_parse.h"
#include "formatter_main.h"
#include "formatter_attr.h"

#include <gear_exceptions.h>

gear_class_entry *NumberFormatter_ce_ptr = NULL;
static gear_object_handlers NumberFormatter_handlers;

/*
 * Auxiliary functions needed by objects of 'NumberFormatter' class
 */

/* {{{ NumberFormatter_objects_free */
void NumberFormatter_object_free( gear_object *object )
{
	NumberFormatter_object* nfo = hyss_intl_number_format_fetch_object(object);

	gear_object_std_dtor( &nfo->zo );

	formatter_data_free( &nfo->nf_data );
}
/* }}} */

/* {{{ NumberFormatter_object_create */
gear_object *NumberFormatter_object_create(gear_class_entry *ce)
{
	NumberFormatter_object*     intern;

	intern = gear_object_alloc(sizeof(NumberFormatter_object), ce);
	formatter_data_init( &intern->nf_data );
	gear_object_std_init( &intern->zo, ce );
	object_properties_init(&intern->zo, ce);

	intern->zo.handlers = &NumberFormatter_handlers;

	return &intern->zo;
}
/* }}} */

/* {{{ NumberFormatter_object_clone */
gear_object *NumberFormatter_object_clone(zval *object)
{
	NumberFormatter_object *nfo, *new_nfo;
	gear_object *new_obj;

	FORMATTER_METHOD_FETCH_OBJECT_NO_CHECK;
	new_obj = NumberFormatter_ce_ptr->create_object(Z_OBJCE_P(object));
	new_nfo = hyss_intl_number_format_fetch_object(new_obj);
	/* clone standard parts */
	gear_objects_clone_members(&new_nfo->zo, &nfo->zo);
	/* clone formatter object. It may fail, the destruction code must handle this case */
	if (FORMATTER_OBJECT(nfo) != NULL) {
		FORMATTER_OBJECT(new_nfo) = unum_clone(FORMATTER_OBJECT(nfo),
				&INTL_DATA_ERROR_CODE(nfo));
		if (U_FAILURE(INTL_DATA_ERROR_CODE(nfo))) {
			/* set up error in case error handler is interested */
			intl_errors_set(INTL_DATA_ERROR_P(nfo), INTL_DATA_ERROR_CODE(nfo),
					"Failed to clone NumberFormatter object", 0);
			gear_throw_exception(NULL, "Failed to clone NumberFormatter object", 0);
		}
	} else {
		gear_throw_exception(NULL, "Cannot clone unconstructed NumberFormatter", 0);
	}
	return new_obj;
}
/* }}} */

/*
 * 'NumberFormatter' class registration structures & functions
 */

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(number_parse_arginfo, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, type)
	GEAR_ARG_INFO(1, position)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(number_parse_currency_arginfo, 0, 0, 2)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(1, currency)
	GEAR_ARG_INFO(1, position)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numberformatter_getattribute, 0, 0, 1)
	GEAR_ARG_INFO(0, attr)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numberformatter_setattribute, 0, 0, 2)
	GEAR_ARG_INFO(0, attr)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numberformatter_setsymbol, 0, 0, 2)
	GEAR_ARG_INFO(0, attr)
	GEAR_ARG_INFO(0, symbol)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_numberformatter_getpattern, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numberformatter_setpattern, 0, 0, 1)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numberformatter_getlocale, 0, 0, 0)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numberformatter___construct, 0, 0, 2)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, style)
	GEAR_ARG_INFO(0, pattern)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numberformatter_formatcurrency, 0, 0, 2)
	GEAR_ARG_INFO(0, num)
	GEAR_ARG_INFO(0, currency)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_numberformatter_format, 0, 0, 1)
	GEAR_ARG_INFO(0, num)
	GEAR_ARG_INFO(0, type)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ NumberFormatter_class_functions
 * Every 'NumberFormatter' class method has an entry in this table
 */
static const gear_function_entry NumberFormatter_class_functions[] = {
	HYSS_ME( NumberFormatter, __construct, arginfo_numberformatter___construct, GEAR_ACC_PUBLIC|GEAR_ACC_CTOR )
	GEAR_FENTRY( create, GEAR_FN( numfmt_create ), arginfo_numberformatter___construct, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	HYSS_NAMED_FE( format, GEAR_FN( numfmt_format ), arginfo_numberformatter_format )
	HYSS_NAMED_FE( parse, GEAR_FN( numfmt_parse ), number_parse_arginfo )
	HYSS_NAMED_FE( formatCurrency, GEAR_FN( numfmt_format_currency ), arginfo_numberformatter_formatcurrency )
	HYSS_NAMED_FE( parseCurrency, GEAR_FN( numfmt_parse_currency ), number_parse_currency_arginfo )
	HYSS_NAMED_FE( setAttribute, GEAR_FN( numfmt_set_attribute ), arginfo_numberformatter_setattribute )
	HYSS_NAMED_FE( getAttribute, GEAR_FN( numfmt_get_attribute ), arginfo_numberformatter_getattribute )
	HYSS_NAMED_FE( setTextAttribute, GEAR_FN( numfmt_set_text_attribute ), arginfo_numberformatter_setattribute )
	HYSS_NAMED_FE( getTextAttribute, GEAR_FN( numfmt_get_text_attribute ), arginfo_numberformatter_getattribute )
	HYSS_NAMED_FE( setSymbol, GEAR_FN( numfmt_set_symbol ), arginfo_numberformatter_setsymbol )
	HYSS_NAMED_FE( getSymbol, GEAR_FN( numfmt_get_symbol ), arginfo_numberformatter_getattribute )
	HYSS_NAMED_FE( setPattern, GEAR_FN( numfmt_set_pattern ), arginfo_numberformatter_setpattern )
	HYSS_NAMED_FE( getPattern, GEAR_FN( numfmt_get_pattern ), arginfo_numberformatter_getpattern )
	HYSS_NAMED_FE( getLocale, GEAR_FN( numfmt_get_locale ), arginfo_numberformatter_getlocale )
	HYSS_NAMED_FE( getErrorCode, GEAR_FN( numfmt_get_error_code ), arginfo_numberformatter_getpattern )
	HYSS_NAMED_FE( getErrorMessage, GEAR_FN( numfmt_get_error_message ), arginfo_numberformatter_getpattern )
	HYSS_FE_END
};
/* }}} */

/* {{{ formatter_register_class
 * Initialize 'NumberFormatter' class
 */
void formatter_register_class( void )
{
	gear_class_entry ce;

	/* Create and register 'NumberFormatter' class. */
	INIT_CLASS_ENTRY( ce, "NumberFormatter", NumberFormatter_class_functions );
	ce.create_object = NumberFormatter_object_create;
	NumberFormatter_ce_ptr = gear_register_internal_class( &ce );

	memcpy(&NumberFormatter_handlers, &std_object_handlers,
		sizeof(NumberFormatter_handlers));
	NumberFormatter_handlers.offset = XtOffsetOf(NumberFormatter_object, zo);
	NumberFormatter_handlers.clone_obj = NumberFormatter_object_clone;
	NumberFormatter_handlers.free_obj = NumberFormatter_object_free;
}
/* }}} */

