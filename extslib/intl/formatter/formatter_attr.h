/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FORMATTER_ATTR_H
#define FORMATTER_ATTR_H

#include <hyss.h>

HYSS_FUNCTION( numfmt_set_attribute );
HYSS_FUNCTION( numfmt_get_attribute );
HYSS_FUNCTION( numfmt_set_text_attribute );
HYSS_FUNCTION( numfmt_get_text_attribute );
HYSS_FUNCTION( numfmt_set_symbol );
HYSS_FUNCTION( numfmt_get_symbol );
HYSS_FUNCTION( numfmt_set_pattern );
HYSS_FUNCTION( numfmt_get_pattern );
HYSS_FUNCTION( numfmt_get_locale );

#endif // FORMATTER_ATTR_H
