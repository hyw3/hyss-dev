/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "../intl_cppshims.h"

#include <unicode/calendar.h>
#include <unicode/gregocal.h>

extern "C" {
#define USE_TIMEZONE_POINTER 1
#include "../timezone/timezone_class.h"
#define USE_CALENDAR_POINTER 1
#include "calendar_class.h"
#include "calendar_methods.h"
#include "gregoriancalendar_methods.h"
#include <gear_exceptions.h>
#include <assert.h>
}

using icu::GregorianCalendar;
using icu::Locale;

/* {{{ Global variables */
gear_class_entry *Calendar_ce_ptr;
gear_class_entry *GregorianCalendar_ce_ptr;
gear_object_handlers Calendar_handlers;
/* }}} */

U_CFUNC	void calendar_object_create(zval *object,
									Calendar *calendar)
{
	UClassID classId = calendar->getDynamicClassID();
	gear_class_entry *ce;

	//if (dynamic_cast<GregorianCalendar*>(calendar) != NULL) {
	if (classId == GregorianCalendar::getStaticClassID()) {
		ce = GregorianCalendar_ce_ptr;
	} else {
		ce = Calendar_ce_ptr;
	}

	object_init_ex(object, ce);
	calendar_object_construct(object, calendar);
}

U_CFUNC Calendar *calendar_fetch_native_calendar(zval *object)
{
	Calendar_object *co = Z_INTL_CALENDAR_P(object);

	return co->ucal;
}

U_CFUNC void calendar_object_construct(zval *object,
									   Calendar *calendar)
{
	Calendar_object *co;

	CALENDAR_METHOD_FETCH_OBJECT_NO_CHECK; //populate to from object
	assert(co->ucal == NULL);
	co->ucal = (Calendar*)calendar;
}

/* {{{ clone handler for Calendar */
static gear_object *Calendar_clone_obj(zval *object)
{
	Calendar_object		*co_orig,
						*co_new;
	gear_object 	    *ret_val;
	intl_error_reset(NULL);

	co_orig = Z_INTL_CALENDAR_P(object);
	intl_error_reset(INTL_DATA_ERROR_P(co_orig));

	ret_val = Calendar_ce_ptr->create_object(Z_OBJCE_P(object));
	co_new  = hyss_intl_calendar_fetch_object(ret_val);

	gear_objects_clone_members(&co_new->zo, &co_orig->zo);

	if (co_orig->ucal != NULL) {
		Calendar	*newCalendar;

		newCalendar = co_orig->ucal->clone();
		if (!newCalendar) {
			gear_string *err_msg;
			intl_errors_set_code(CALENDAR_ERROR_P(co_orig),
				U_MEMORY_ALLOCATION_ERROR);
			intl_errors_set_custom_msg(CALENDAR_ERROR_P(co_orig),
				"Could not clone IntlCalendar", 0);
			err_msg = intl_error_get_message(CALENDAR_ERROR_P(co_orig));
			gear_throw_exception(NULL, ZSTR_VAL(err_msg), 0);
			gear_string_free(err_msg);
		} else {
			co_new->ucal = newCalendar;
		}
	} else {
		gear_throw_exception(NULL, "Cannot clone unconstructed IntlCalendar", 0);
	}

	return ret_val;
}
/* }}} */

static const struct {
	UCalendarDateFields field;
	const char			*name;
} debug_info_fields[] = {
	{UCAL_ERA,					"era"},
	{UCAL_YEAR,					"year"},
	{UCAL_MONTH,				"month"},
	{UCAL_WEEK_OF_YEAR,			"week of year"},
	{UCAL_WEEK_OF_MONTH,		"week of month"},
	{UCAL_DAY_OF_YEAR,			"day of year"},
	{UCAL_DAY_OF_MONTH,			"day of month"},
	{UCAL_DAY_OF_WEEK,			"day of week"},
	{UCAL_DAY_OF_WEEK_IN_MONTH,	"day of week in month"},
	{UCAL_AM_PM,				"AM/PM"},
	{UCAL_HOUR,					"hour"},
	{UCAL_HOUR_OF_DAY,			"hour of day"},
	{UCAL_MINUTE,				"minute"},
	{UCAL_SECOND,				"second"},
	{UCAL_MILLISECOND,			"millisecond"},
	{UCAL_ZONE_OFFSET,			"zone offset"},
	{UCAL_DST_OFFSET,			"DST offset"},
	{UCAL_YEAR_WOY,				"year for week of year"},
	{UCAL_DOW_LOCAL,			"localized day of week"},
	{UCAL_EXTENDED_YEAR,		"extended year"},
	{UCAL_JULIAN_DAY,			"julian day"},
	{UCAL_MILLISECONDS_IN_DAY,	"milliseconds in day"},
	{UCAL_IS_LEAP_MONTH,		"is leap month"},
};

/* {{{ get_debug_info handler for Calendar */
static HashTable *Calendar_get_debug_info(zval *object, int *is_temp)
{
	zval			zv,
					zfields;
	Calendar_object	*co;
	const Calendar	*cal;
	HashTable		*debug_info;

	*is_temp = 1;

	debug_info = gear_new_array(8);

	co  = Z_INTL_CALENDAR_P(object);
	cal = co->ucal;

	if (cal == NULL) {
		ZVAL_FALSE(&zv);
		gear_hash_str_update(debug_info, "valid", sizeof("valid") - 1, &zv);
		return debug_info;
	}
	ZVAL_TRUE(&zv);
	gear_hash_str_update(debug_info, "valid", sizeof("valid") - 1, &zv);

	ZVAL_STRING(&zv, const_cast<char*>(cal->getType()));
	gear_hash_str_update(debug_info, "type", sizeof("type") - 1, &zv);
	{
		zval		   ztz,
					   ztz_debug;
		int			   is_tmp;
		HashTable	   *debug_info_tz;

		timezone_object_construct(&cal->getTimeZone(), &ztz , 0);
		debug_info_tz = Z_OBJ_HANDLER(ztz, get_debug_info)(&ztz, &is_tmp);
		assert(is_tmp == 1);

		array_init(&ztz_debug);
		gear_hash_copy(Z_ARRVAL(ztz_debug), debug_info_tz, zval_add_ref);
		gear_hash_destroy(debug_info_tz);
		FREE_HASHTABLE(debug_info_tz);

		gear_hash_str_update(debug_info, "timeZone", sizeof("timeZone") - 1, &ztz_debug);
	}

	{
		UErrorCode	uec		= U_ZERO_ERROR;
		Locale		locale	= cal->getLocale(ULOC_VALID_LOCALE, uec);
		if (U_SUCCESS(uec)) {
			ZVAL_STRING(&zv, const_cast<char*>(locale.getName()));
			gear_hash_str_update(debug_info, "locale", sizeof("locale") - 1, &zv);
		} else {
			ZVAL_STRING(&zv, const_cast<char*>(u_errorName(uec)));
			gear_hash_str_update(debug_info, "locale", sizeof("locale") - 1, &zv);
		}
	}

	array_init_size(&zfields, UCAL_FIELD_COUNT);

	for (int i = 0;
			 i < sizeof(debug_info_fields) / sizeof(*debug_info_fields);
			 i++) {
		UErrorCode	uec		= U_ZERO_ERROR;
		const char	*name	= debug_info_fields[i].name;
		int32_t		res		= cal->get(debug_info_fields[i].field, uec);
		if (U_SUCCESS(uec)) {
			add_assoc_long(&zfields, name, (gear_long)res);
		} else {
			add_assoc_string(&zfields, name, const_cast<char*>(u_errorName(uec)));
		}
	}

	gear_hash_str_update(debug_info, "fields", sizeof("fields") - 1, &zfields);

	return debug_info;
}
/* }}} */

/* {{{ void calendar_object_init(Calendar_object* to)
 * Initialize internals of Calendar_object not specific to gear standard objects.
 */
static void calendar_object_init(Calendar_object *co)
{
	intl_error_init(CALENDAR_ERROR_P(co));
	co->ucal = NULL;
}
/* }}} */

/* {{{ Calendar_objects_free */
static void Calendar_objects_free(gear_object *object)
{
	Calendar_object* co = hyss_intl_calendar_fetch_object(object);

	if (co->ucal) {
		delete co->ucal;
		co->ucal = NULL;
	}
	intl_error_reset(CALENDAR_ERROR_P(co));

	gear_object_std_dtor(&co->zo);
}
/* }}} */

/* {{{ Calendar_object_create */
static gear_object *Calendar_object_create(gear_class_entry *ce)
{
	Calendar_object*	intern;

	intern = (Calendar_object*)ecalloc(1, sizeof(Calendar_object) + sizeof(zval) * (ce->default_properties_count - 1));

	gear_object_std_init(&intern->zo, ce);
    object_properties_init(&intern->zo, ce);
	calendar_object_init(intern);


	intern->zo.handlers = &Calendar_handlers;

	return &intern->zo;
}
/* }}} */

/* {{{ Calendar methods arguments info */

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_void, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_field, 0, 0, 1)
	GEAR_ARG_INFO(0, field)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_dow, 0, 0, 1)
	GEAR_ARG_INFO(0, dayOfWeek)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_other_cal, 0, 0, 1)
	GEAR_ARG_OBJ_INFO(0, calendar, IntlCalendar, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_date, 0, 0, 1)
	GEAR_ARG_INFO(0, date)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_date_optional, 0, 0, 0)
	GEAR_ARG_INFO(0, date)
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_createInstance, 0, 0, 0)
	GEAR_ARG_INFO(0, timeZone)
	GEAR_ARG_INFO(0, locale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_get_keyword_values_for_locale, 0, 0, 3)
	GEAR_ARG_INFO(0, key)
	GEAR_ARG_INFO(0, locale)
	GEAR_ARG_INFO(0, commonlyUsed)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_add, 0, 0, 2)
	GEAR_ARG_INFO(0, field)
	GEAR_ARG_INFO(0, amount)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_setTimeZone, 0, 0, 1)
	GEAR_ARG_INFO(0, timeZone)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_set, 0, 0, 2)
	GEAR_ARG_INFO(0, fieldOrYear)
	GEAR_ARG_INFO(0, valueOrMonth)
	GEAR_ARG_INFO(0, dayOfMonth)
	GEAR_ARG_INFO(0, hour)
	GEAR_ARG_INFO(0, minute)
	GEAR_ARG_INFO(0, second)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_roll, 0, 0, 2)
	GEAR_ARG_INFO(0, field)
	GEAR_ARG_INFO(0, amountOrUpOrDown)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_clear, 0, 0, 0)
	GEAR_ARG_INFO(0, field)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_field_difference, 0, 0, 2)
	GEAR_ARG_INFO(0, when)
	GEAR_ARG_INFO(0, field)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_get_locale, 0, 0, 1)
	GEAR_ARG_INFO(0, localeType)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_setLenient, 0, 0, 1)
	GEAR_ARG_INFO(0, isLenient)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_set_minimal_days_in_first_week, 0, 0, 1)
	GEAR_ARG_INFO(0, numberOfDays)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_from_date_time, 0, 0, 1)
	GEAR_ARG_INFO(0, dateTime)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_cal_wall_time_option, 0, 0, 1)
	GEAR_ARG_INFO(0, wallTimeOption)
GEAR_END_ARG_INFO()

/* Gregorian Calendar */
GEAR_BEGIN_ARG_INFO_EX(ainfo_gregcal___construct, 0, 0, 0)
	GEAR_ARG_INFO(0, timeZoneOrYear)
	GEAR_ARG_INFO(0, localeOrMonth)
	GEAR_ARG_INFO(0, dayOfMonth)
	GEAR_ARG_INFO(0, hour)
	GEAR_ARG_INFO(0, minute)
	GEAR_ARG_INFO(0, second)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_gregcal_isLeapYear, 0, 0, 1)
	GEAR_ARG_INFO(0, year)
GEAR_END_ARG_INFO()

/* }}} */

/* {{{ Calendar_class_functions
 * Every 'IntlCalendar' class method has an entry in this table
 */
static const gear_function_entry Calendar_class_functions[] = {
	HYSS_ME(IntlCalendar,				__construct,				ainfo_cal_void,						GEAR_ACC_PRIVATE)
	HYSS_ME_MAPPING(createInstance,		intlcal_create_instance,	ainfo_cal_createInstance,			GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 42
	HYSS_ME_MAPPING(getKeywordValuesForLocale, intlcal_get_keyword_values_for_locale, ainfo_cal_get_keyword_values_for_locale, GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
#endif
	HYSS_ME_MAPPING(getNow,				intlcal_get_now,			ainfo_cal_void,						GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getAvailableLocales,	intlcal_get_available_locales, ainfo_cal_void,					GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(get,					intlcal_get,				ainfo_cal_field,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getTime,				intlcal_get_time,			ainfo_cal_void,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(setTime,				intlcal_set_time,			ainfo_cal_date,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(add,					intlcal_add,				ainfo_cal_add,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(setTimeZone,			intlcal_set_time_zone,		ainfo_cal_setTimeZone,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(after,				intlcal_after,				ainfo_cal_other_cal,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(before,				intlcal_before,				ainfo_cal_other_cal,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(set,					intlcal_set,				ainfo_cal_set,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(roll,				intlcal_roll,				ainfo_cal_roll,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(clear,				intlcal_clear,				ainfo_cal_clear,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(fieldDifference,		intlcal_field_difference,	ainfo_cal_field_difference,			GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getActualMaximum,	intlcal_get_actual_maximum,	ainfo_cal_field,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getActualMinimum,	intlcal_get_actual_minimum,	ainfo_cal_field,					GEAR_ACC_PUBLIC)
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 44
	HYSS_ME_MAPPING(getDayOfWeekType,	intlcal_get_day_of_week_type, ainfo_cal_dow,					GEAR_ACC_PUBLIC)
#endif
	HYSS_ME_MAPPING(getFirstDayOfWeek,	intlcal_get_first_day_of_week, ainfo_cal_void,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getGreatestMinimum,	intlcal_get_greatest_minimum, ainfo_cal_field,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getLeastMaximum,		intlcal_get_least_maximum,	ainfo_cal_field,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getLocale,			intlcal_get_locale,			ainfo_cal_get_locale,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getMaximum,			intlcal_get_maximum,		ainfo_cal_field,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getMinimalDaysInFirstWeek, intlcal_get_minimal_days_in_first_week, ainfo_cal_void,	GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getMinimum,			intlcal_get_minimum,		ainfo_cal_field,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getTimeZone,			intlcal_get_time_zone,		ainfo_cal_void,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getType,				intlcal_get_type,			ainfo_cal_void,						GEAR_ACC_PUBLIC)
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 44
	HYSS_ME_MAPPING(getWeekendTransition,intlcal_get_weekend_transition, ainfo_cal_dow,					GEAR_ACC_PUBLIC)
#endif
	HYSS_ME_MAPPING(inDaylightTime,		intlcal_in_daylight_time,	ainfo_cal_void,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(isEquivalentTo,		intlcal_is_equivalent_to,	ainfo_cal_other_cal,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(isLenient,			intlcal_is_lenient,			ainfo_cal_void,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(isSet,				intlcal_is_set,				ainfo_cal_field,					GEAR_ACC_PUBLIC)
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 44
	HYSS_ME_MAPPING(isWeekend,			intlcal_is_weekend,			ainfo_cal_date_optional,			GEAR_ACC_PUBLIC)
#endif
	HYSS_ME_MAPPING(setFirstDayOfWeek,	intlcal_set_first_day_of_week, ainfo_cal_dow,					GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(setLenient,			intlcal_set_lenient,		ainfo_cal_setLenient,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(setMinimalDaysInFirstWeek,intlcal_set_minimal_days_in_first_week,ainfo_cal_set_minimal_days_in_first_week,GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(equals,				intlcal_equals,				ainfo_cal_other_cal,				GEAR_ACC_PUBLIC)
#if U_ICU_VERSION_MAJOR_NUM >= 49
	HYSS_ME_MAPPING(getRepeatedWallTimeOption,intlcal_get_repeated_wall_time_option,ainfo_cal_void,		GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getSkippedWallTimeOption,intlcal_get_skipped_wall_time_option,ainfo_cal_void,		GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(setRepeatedWallTimeOption,intlcal_set_repeated_wall_time_option,ainfo_cal_wall_time_option,GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(setSkippedWallTimeOption,intlcal_set_skipped_wall_time_option,ainfo_cal_wall_time_option,GEAR_ACC_PUBLIC)
#endif
	HYSS_ME_MAPPING(fromDateTime,		intlcal_from_date_time,		ainfo_cal_from_date_time,			GEAR_ACC_STATIC | GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(toDateTime,			intlcal_to_date_time,		ainfo_cal_void,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getErrorCode,		intlcal_get_error_code,		ainfo_cal_void,						GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getErrorMessage,		intlcal_get_error_message,	ainfo_cal_void,						GEAR_ACC_PUBLIC)
	HYSS_FE_END
};
/* }}} */

/* {{{ GregorianCalendar_class_functions
 */
static const gear_function_entry GregorianCalendar_class_functions[] = {
	HYSS_ME(IntlGregorianCalendar,		__construct,				ainfo_gregcal___construct,			GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(setGregorianChange,	intlgregcal_set_gregorian_change, ainfo_cal_date,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getGregorianChange,	intlgregcal_get_gregorian_change, ainfo_cal_void,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(isLeapYear,			intlgregcal_is_leap_year,	ainfo_gregcal_isLeapYear,			GEAR_ACC_PUBLIC)
	HYSS_FE_END
};
/* }}} */


/* {{{ calendar_register_IntlCalendar_class
 * Initialize 'IntlCalendar' class
 */
void calendar_register_IntlCalendar_class(void)
{
	gear_class_entry ce;

	/* Create and register 'IntlCalendar' class. */
	INIT_CLASS_ENTRY(ce, "IntlCalendar", Calendar_class_functions);
	ce.create_object = Calendar_object_create;
	Calendar_ce_ptr = gear_register_internal_class(&ce);

	memcpy( &Calendar_handlers, &std_object_handlers,
		sizeof Calendar_handlers);
	Calendar_handlers.offset = XtOffsetOf(Calendar_object, zo);
	Calendar_handlers.clone_obj = Calendar_clone_obj;
	Calendar_handlers.get_debug_info = Calendar_get_debug_info;
	Calendar_handlers.free_obj = Calendar_objects_free;

	/* Declare 'IntlCalendar' class constants */
#define CALENDAR_DECL_LONG_CONST(name, val) \
	gear_declare_class_constant_long(Calendar_ce_ptr, name, sizeof(name) - 1, \
		val)

	CALENDAR_DECL_LONG_CONST("FIELD_ERA",					UCAL_ERA);
	CALENDAR_DECL_LONG_CONST("FIELD_YEAR",					UCAL_YEAR);
	CALENDAR_DECL_LONG_CONST("FIELD_MONTH",					UCAL_MONTH);
	CALENDAR_DECL_LONG_CONST("FIELD_WEEK_OF_YEAR",			UCAL_WEEK_OF_YEAR);
	CALENDAR_DECL_LONG_CONST("FIELD_WEEK_OF_MONTH",			UCAL_WEEK_OF_MONTH);
	CALENDAR_DECL_LONG_CONST("FIELD_DATE",					UCAL_DATE);
	CALENDAR_DECL_LONG_CONST("FIELD_DAY_OF_YEAR",			UCAL_DAY_OF_YEAR);
	CALENDAR_DECL_LONG_CONST("FIELD_DAY_OF_WEEK",			UCAL_DAY_OF_WEEK);
	CALENDAR_DECL_LONG_CONST("FIELD_DAY_OF_WEEK_IN_MONTH",	UCAL_DAY_OF_WEEK_IN_MONTH);
	CALENDAR_DECL_LONG_CONST("FIELD_AM_PM",					UCAL_AM_PM);
	CALENDAR_DECL_LONG_CONST("FIELD_HOUR",					UCAL_HOUR);
	CALENDAR_DECL_LONG_CONST("FIELD_HOUR_OF_DAY",			UCAL_HOUR_OF_DAY);
	CALENDAR_DECL_LONG_CONST("FIELD_MINUTE",				UCAL_MINUTE);
	CALENDAR_DECL_LONG_CONST("FIELD_SECOND",				UCAL_SECOND);
	CALENDAR_DECL_LONG_CONST("FIELD_MILLISECOND",			UCAL_MILLISECOND);
	CALENDAR_DECL_LONG_CONST("FIELD_ZONE_OFFSET",			UCAL_ZONE_OFFSET);
	CALENDAR_DECL_LONG_CONST("FIELD_DST_OFFSET",			UCAL_DST_OFFSET);
	CALENDAR_DECL_LONG_CONST("FIELD_YEAR_WOY",				UCAL_YEAR_WOY);
	CALENDAR_DECL_LONG_CONST("FIELD_DOW_LOCAL",				UCAL_DOW_LOCAL);
	CALENDAR_DECL_LONG_CONST("FIELD_EXTENDED_YEAR",			UCAL_EXTENDED_YEAR);
	CALENDAR_DECL_LONG_CONST("FIELD_JULIAN_DAY",			UCAL_JULIAN_DAY);
	CALENDAR_DECL_LONG_CONST("FIELD_MILLISECONDS_IN_DAY",	UCAL_MILLISECONDS_IN_DAY);
	CALENDAR_DECL_LONG_CONST("FIELD_IS_LEAP_MONTH",			UCAL_IS_LEAP_MONTH);
	CALENDAR_DECL_LONG_CONST("FIELD_FIELD_COUNT",			UCAL_FIELD_COUNT);
	CALENDAR_DECL_LONG_CONST("FIELD_DAY_OF_MONTH",			UCAL_DAY_OF_MONTH);

	CALENDAR_DECL_LONG_CONST("DOW_SUNDAY",					UCAL_SUNDAY);
	CALENDAR_DECL_LONG_CONST("DOW_MONDAY",					UCAL_MONDAY);
	CALENDAR_DECL_LONG_CONST("DOW_TUESDAY",					UCAL_TUESDAY);
	CALENDAR_DECL_LONG_CONST("DOW_WEDNESDAY",				UCAL_WEDNESDAY);
	CALENDAR_DECL_LONG_CONST("DOW_THURSDAY",				UCAL_THURSDAY);
	CALENDAR_DECL_LONG_CONST("DOW_FRIDAY",					UCAL_FRIDAY);
	CALENDAR_DECL_LONG_CONST("DOW_SATURDAY",				UCAL_SATURDAY);

#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 44
	CALENDAR_DECL_LONG_CONST("DOW_TYPE_WEEKDAY",			UCAL_WEEKDAY);
	CALENDAR_DECL_LONG_CONST("DOW_TYPE_WEEKEND",			UCAL_WEEKEND);
	CALENDAR_DECL_LONG_CONST("DOW_TYPE_WEEKEND_OFFSET",		UCAL_WEEKEND_ONSET);
	CALENDAR_DECL_LONG_CONST("DOW_TYPE_WEEKEND_CEASE",		UCAL_WEEKEND_CEASE);
#endif

#if U_ICU_VERSION_MAJOR_NUM >= 49
	CALENDAR_DECL_LONG_CONST("WALLTIME_FIRST",				UCAL_WALLTIME_FIRST);
	CALENDAR_DECL_LONG_CONST("WALLTIME_LAST",				UCAL_WALLTIME_LAST);
	CALENDAR_DECL_LONG_CONST("WALLTIME_NEXT_VALID",			UCAL_WALLTIME_NEXT_VALID);
#endif

	/* Create and register 'IntlGregorianCalendar' class. */
	INIT_CLASS_ENTRY(ce, "IntlGregorianCalendar", GregorianCalendar_class_functions);
	GregorianCalendar_ce_ptr = gear_register_internal_class_ex(&ce,
		Calendar_ce_ptr);
}
/* }}} */
