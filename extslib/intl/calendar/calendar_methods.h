/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALENDAR_METHODS_H
#define CALENDAR_METHODS_H

#include <hyss.h>

HYSS_METHOD(IntlCalendar, __construct);

HYSS_FUNCTION(intlcal_create_instance);

HYSS_FUNCTION(intlcal_get_keyword_values_for_locale);

HYSS_FUNCTION(intlcal_get_now);

HYSS_FUNCTION(intlcal_get_available_locales);

HYSS_FUNCTION(intlcal_get);

HYSS_FUNCTION(intlcal_get_time);

HYSS_FUNCTION(intlcal_set_time);

HYSS_FUNCTION(intlcal_add);

HYSS_FUNCTION(intlcal_set_time_zone);

HYSS_FUNCTION(intlcal_after);

HYSS_FUNCTION(intlcal_before);

HYSS_FUNCTION(intlcal_set);

HYSS_FUNCTION(intlcal_roll);

HYSS_FUNCTION(intlcal_clear);

HYSS_FUNCTION(intlcal_field_difference);

HYSS_FUNCTION(intlcal_get_actual_maximum);

HYSS_FUNCTION(intlcal_get_actual_minimum);

HYSS_FUNCTION(intlcal_get_day_of_week_type);

HYSS_FUNCTION(intlcal_get_first_day_of_week);

HYSS_FUNCTION(intlcal_get_greatest_minimum);

HYSS_FUNCTION(intlcal_get_least_maximum);

HYSS_FUNCTION(intlcal_get_locale);

HYSS_FUNCTION(intlcal_get_maximum);

HYSS_FUNCTION(intlcal_get_minimal_days_in_first_week);

HYSS_FUNCTION(intlcal_get_minimum);

HYSS_FUNCTION(intlcal_get_time_zone);

HYSS_FUNCTION(intlcal_get_type);

HYSS_FUNCTION(intlcal_get_weekend_transition);

HYSS_FUNCTION(intlcal_in_daylight_time);

HYSS_FUNCTION(intlcal_is_equivalent_to);

HYSS_FUNCTION(intlcal_is_lenient);

HYSS_FUNCTION(intlcal_is_set);

HYSS_FUNCTION(intlcal_is_weekend);

HYSS_FUNCTION(intlcal_set_first_day_of_week);

HYSS_FUNCTION(intlcal_set_lenient);

HYSS_FUNCTION(intlcal_set_minimal_days_in_first_week);

HYSS_FUNCTION(intlcal_equals);

HYSS_FUNCTION(intlcal_get_repeated_wall_time_option);

HYSS_FUNCTION(intlcal_get_skipped_wall_time_option);

HYSS_FUNCTION(intlcal_set_repeated_wall_time_option);

HYSS_FUNCTION(intlcal_set_skipped_wall_time_option);

HYSS_FUNCTION(intlcal_from_date_time);

HYSS_FUNCTION(intlcal_to_date_time);

HYSS_FUNCTION(intlcal_get_error_code);

HYSS_FUNCTION(intlcal_get_error_message);

#endif /* #ifndef CALENDAR_METHODS_H */
