/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALENDAR_CLASS_H
#define CALENDAR_CLASS_H

//redefinition of inline in HYSS headers causes problems, so include this before
#include <math.h>

#include <hyss.h>
#include "intl_error.h"
#include "intl_data.h"

#ifndef USE_CALENDAR_POINTER
typedef void Calendar;
#else
using icu::Calendar;
#endif

typedef struct {
	// 	error handling
	intl_error  err;

	// ICU calendar
	Calendar*	ucal;

	gear_object	zo;
} Calendar_object;

static inline Calendar_object *hyss_intl_calendar_fetch_object(gear_object *obj) {
	return (Calendar_object *)((char*)(obj) - XtOffsetOf(Calendar_object, zo));
}
#define Z_INTL_CALENDAR_P(zv) hyss_intl_calendar_fetch_object(Z_OBJ_P(zv))

#define CALENDAR_ERROR(co)		(co)->err
#define CALENDAR_ERROR_P(co)	&(CALENDAR_ERROR(co))

#define CALENDAR_ERROR_CODE(co)		INTL_ERROR_CODE(CALENDAR_ERROR(co))
#define CALENDAR_ERROR_CODE_P(co)	&(INTL_ERROR_CODE(CALENDAR_ERROR(co)))

#define CALENDAR_METHOD_INIT_VARS		        INTL_METHOD_INIT_VARS(Calendar, co)
#define CALENDAR_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_CALENDAR, co)
#define CALENDAR_METHOD_FETCH_OBJECT \
	CALENDAR_METHOD_FETCH_OBJECT_NO_CHECK; \
	if (co->ucal == NULL) \
	{ \
		intl_errors_set(&co->err, U_ILLEGAL_ARGUMENT_ERROR, "Found unconstructed IntlCalendar", 0); \
		RETURN_FALSE; \
	}

void calendar_object_create(zval *object, Calendar *calendar);

Calendar *calendar_fetch_native_calendar(zval *object);

void calendar_object_construct(zval *object, Calendar *calendar);

void calendar_register_IntlCalendar_class(void);

extern gear_class_entry *Calendar_ce_ptr,
						*GregorianCalendar_ce_ptr;

extern gear_object_handlers Calendar_handlers;

#endif /* #ifndef CALENDAR_CLASS_H */
