/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GREORIANCALENDAR_METHODS_H
#define GREORIANCALENDAR_METHODS_H

#include <hyss.h>

HYSS_FUNCTION(intlgregcal_create_instance);

HYSS_METHOD(IntlGregorianCalendar, __construct);

HYSS_FUNCTION(intlgregcal_set_gregorian_change);

HYSS_FUNCTION(intlgregcal_get_gregorian_change);

HYSS_FUNCTION(intlgregcal_is_leap_year);

#endif
