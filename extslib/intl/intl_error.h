/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTL_ERROR_H
#define INTL_ERROR_H

#include <unicode/utypes.h>
#include <unicode/parseerr.h>
#include <gear_smart_str.h>

#define INTL_ERROR_CODE(e) (e).code

typedef struct _intl_error {
	UErrorCode      code;
	int             free_custom_error_message;
	char*           custom_error_message;
} intl_error;

intl_error* intl_error_create( void );
void        intl_error_init( intl_error* err );
void        intl_error_reset( intl_error* err );
void        intl_error_set_code( intl_error* err, UErrorCode err_code );
void        intl_error_set_custom_msg( intl_error* err, const char* msg, int copyMsg );
void        intl_error_set( intl_error* err, UErrorCode code, const char* msg, int copyMsg );
UErrorCode  intl_error_get_code( intl_error* err );
gear_string* intl_error_get_message( intl_error* err );

// Wrappers to synchonize object's and global error structures.
void        intl_errors_reset( intl_error* err );
void        intl_errors_set_custom_msg( intl_error* err, const char* msg, int copyMsg );
void        intl_errors_set_code( intl_error* err, UErrorCode err_code );
void        intl_errors_set( intl_error* err, UErrorCode code, const char* msg, int copyMsg );

// Other error helpers
smart_str	intl_parse_error_to_string( UParseError* pe );

// exported to be called on extension MINIT
void		intl_register_IntlException_class( void );

#endif // INTL_ERROR_H
