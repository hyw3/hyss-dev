/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicode/uloc.h>
#include "hyss_intl.h"
#include "intl_error.h"
#include "locale_class.h"
#include "locale_methods.h"
#include "locale.h"

gear_class_entry *Locale_ce_ptr = NULL;

/*
 * 'Locale' class registration structures & functions
 */

/* {{{ Locale methods arguments info */
/*
 *  NOTE: when modifying 'locale_XX_args' do not forget to modify
 *        approptiate 'locale_XX_args' for the procedural API!
 */

GEAR_BEGIN_ARG_INFO_EX( locale_0_args, 0, 0, 0 )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_arg_locale, 0, 0, 1 )
	GEAR_ARG_INFO( 0, locale )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_accept_from_http_args, 0, 0, 1 )
	GEAR_ARG_INFO( 0, header )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_compose_args, 0, 0, 1 )
	GEAR_ARG_INFO( 0, subtags )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_2_args, 0, 0, 1 )
	GEAR_ARG_INFO( 0, locale )
	GEAR_ARG_INFO( 0, in_locale )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_filter_matches_args, 0, 0, 2 )
        GEAR_ARG_INFO( 0, langtag )
        GEAR_ARG_INFO( 0, locale )
        GEAR_ARG_INFO( 0, canonicalize )
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX( locale_lookup_args, 0, 0, 2 )
        GEAR_ARG_INFO( 0, langtag )
        GEAR_ARG_INFO( 0, locale )
        GEAR_ARG_INFO( 0, canonicalize )
        GEAR_ARG_INFO( 0, default )
GEAR_END_ARG_INFO()

/* }}} */

/* {{{ Locale_class_functions
 * Every 'Locale' class method has an entry in this table
 */

static const gear_function_entry Locale_class_functions[] = {
	GEAR_FENTRY( getDefault, zif_locale_get_default , locale_0_args , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC  )
	GEAR_FENTRY( setDefault, zif_locale_set_default , locale_arg_locale , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getPrimaryLanguage, GEAR_FN( locale_get_primary_language ), locale_arg_locale , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getScript, GEAR_FN( locale_get_script ), locale_arg_locale , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getRegion, GEAR_FN( locale_get_region ), locale_arg_locale , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getKeywords, GEAR_FN( locale_get_keywords ), locale_arg_locale , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getDisplayScript, GEAR_FN( locale_get_display_script ), locale_2_args , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getDisplayRegion, GEAR_FN( locale_get_display_region ), locale_2_args , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getDisplayName, GEAR_FN( locale_get_display_name ), locale_2_args , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getDisplayLanguage, GEAR_FN( locale_get_display_language ), locale_2_args , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getDisplayVariant, GEAR_FN( locale_get_display_variant ), locale_2_args , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( composeLocale, GEAR_FN( locale_compose ), locale_compose_args , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( parseLocale, GEAR_FN( locale_parse ), locale_arg_locale , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( getAllVariants, GEAR_FN( locale_get_all_variants ), locale_arg_locale , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( filterMatches, GEAR_FN( locale_filter_matches ), locale_filter_matches_args, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( lookup, GEAR_FN( locale_lookup ), locale_lookup_args, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( canonicalize, GEAR_FN( locale_canonicalize ), locale_arg_locale , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	GEAR_FENTRY( acceptFromHttp, GEAR_FN( locale_accept_from_http ), locale_accept_from_http_args , GEAR_ACC_PUBLIC|GEAR_ACC_STATIC )
	HYSS_FE_END
};
/* }}} */

/* {{{ locale_register_Locale_class
 * Initialize 'Locale' class
 */
void locale_register_Locale_class( void )
{
	gear_class_entry ce;

	/* Create and register 'Locale' class. */
	INIT_CLASS_ENTRY( ce, "Locale", Locale_class_functions );
	ce.create_object = NULL;
	Locale_ce_ptr = gear_register_internal_class( &ce );

	/* Declare 'Locale' class properties. */
	if( !Locale_ce_ptr )
	{
		gear_error( E_ERROR,
			"Locale: Failed to register Locale class.");
		return;
	}
}
/* }}} */

