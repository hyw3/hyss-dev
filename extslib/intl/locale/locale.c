/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "locale_class.h"
#include "locale.h"

#include <unicode/utypes.h>
#include <unicode/uloc.h>
#include <unicode/ustring.h>

/* {{{ locale_register_constants
 * Register constants common for the both (OO and procedural)
 * APIs.
 */
void locale_register_constants( INIT_FUNC_ARGS )
{
	if( !Locale_ce_ptr )
	{
		gear_error( E_ERROR, "Locale class not defined" );
		return;
	}

	#define LOCALE_EXPOSE_CONST(x) REGISTER_LONG_CONSTANT(#x, x, CONST_PERSISTENT | CONST_CS)
	#define LOCALE_EXPOSE_CLASS_CONST(x) gear_declare_class_constant_long( Locale_ce_ptr, GEAR_STRS( #x ) - 1, ULOC_##x );
	#define LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR(name, value) gear_declare_class_constant_string( Locale_ce_ptr, GEAR_STRS( name ) - 1, value );

	LOCALE_EXPOSE_CLASS_CONST( ACTUAL_LOCALE );
	LOCALE_EXPOSE_CLASS_CONST( VALID_LOCALE );

	gear_declare_class_constant_null(Locale_ce_ptr, GEAR_STRS("DEFAULT_LOCALE") - 1);

	LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR( "LANG_TAG", LOC_LANG_TAG);
	LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR( "EXTLANG_TAG", LOC_EXTLANG_TAG);
	LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR( "SCRIPT_TAG", LOC_SCRIPT_TAG);
	LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR( "REGION_TAG", LOC_REGION_TAG);
	LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR( "VARIANT_TAG",LOC_VARIANT_TAG);
	LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR( "GRANDFATHERED_LANG_TAG",LOC_GRANDFATHERED_LANG_TAG);
	LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR( "PRIVATE_TAG",LOC_PRIVATE_TAG);

	#undef LOCALE_EXPOSE_CUSTOM_CLASS_CONST_STR
	#undef LOCALE_EXPOSE_CLASS_CONST
	#undef LOCALE_EXPOSE_CONST
}
/* }}} */

