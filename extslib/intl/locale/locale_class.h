/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOCALE_CLASS_H
#define LOCALE_CLASS_H

#include <hyss.h>

#include "intl_common.h"
#include "intl_error.h"

#include <unicode/uloc.h>

typedef struct {
	gear_object     zo;

	// ICU locale
	char*      		uloc1;

} Locale_object;


void locale_register_Locale_class( void );

extern gear_class_entry *Locale_ce_ptr;

/* Auxiliary macros */

#define LOCALE_METHOD_INIT_VARS       \
    zval*             	object  = NULL;   \
    intl_error_reset( NULL ); \

#endif // #ifndef LOCALE_CLASS_H
