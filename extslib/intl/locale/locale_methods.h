/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOCALE_METHODS_H
#define LOCALE_METHODS_H

#include <hyss.h>

HYSS_FUNCTION( locale_get_primary_language );
HYSS_FUNCTION( locale_get_script );
HYSS_FUNCTION( locale_get_region );
HYSS_FUNCTION( locale_get_all_variants);

HYSS_NAMED_FUNCTION( zif_locale_get_default );
HYSS_NAMED_FUNCTION( zif_locale_set_default );

HYSS_FUNCTION( locale_get_display_name );
HYSS_FUNCTION( locale_get_display_language );
HYSS_FUNCTION( locale_get_display_script );
HYSS_FUNCTION( locale_get_display_region );
HYSS_FUNCTION( locale_get_display_variant );

HYSS_FUNCTION( locale_get_keywords );
HYSS_FUNCTION( locale_canonicalize);

HYSS_FUNCTION( locale_compose);
HYSS_FUNCTION( locale_parse);

HYSS_FUNCTION( locale_filter_matches);
HYSS_FUNCTION( locale_lookup);
HYSS_FUNCTION( locale_canonicalize);
HYSS_FUNCTION( locale_accept_from_http);

#endif // LOCALE_METHODS_H
