/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HYSS_INTL_H
#define HYSS_INTL_H

#include <hyss.h>

/* Even if we're included from C++, don't introduce C++ definitions
 * because we were included with extern "C". The effect would be that
 * when the headers defined any method, they would do so with C linkage */
#undef U_SHOW_CPLUSPLUS_API
#define U_SHOW_CPLUSPLUS_API 0
#include "collator/collator_sort.h"
#include <unicode/ubrk.h>
#include "intl_error.h"
#include "Gear/gear_exceptions.h"

extern gear_capi_entry intl_capi_entry;
#define hyssext_intl_ptr &intl_capi_entry

#ifdef HYSS_WIN32
#define HYSS_INTL_API __declspec(dllexport)
#else
#define HYSS_INTL_API
#endif

#ifdef ZTS
#include "hypbc.h"
#endif

GEAR_BEGIN_CAPI_GLOBALS(intl)
	zval current_collator;
	char* default_locale;
	collator_compare_func_t compare_func;
	UBreakIterator* grapheme_iterator;
	intl_error g_error;
	gear_long error_level;
	gear_bool use_exceptions;
GEAR_END_CAPI_GLOBALS(intl)

#if defined(ZTS) && defined(COMPILE_DL_INTL)
GEAR_PBCLS_CACHE_EXTERN()
#endif

GEAR_EXTERN_CAPI_GLOBALS(intl)
/* Macro to access request-wide global variables. */
#define INTL_G(v) GEAR_CAPI_GLOBALS_ACCESSOR(intl, v)

HYSS_MINIT_FUNCTION(intl);
HYSS_MSHUTDOWN_FUNCTION(intl);
HYSS_RINIT_FUNCTION(intl);
HYSS_RSHUTDOWN_FUNCTION(intl);
HYSS_MINFO_FUNCTION(intl);

const char *intl_locale_get_default( void );

#define HYSS_INTL_VERSION HYSS_VERSION

#endif  /* HYSS_INTL_H */

