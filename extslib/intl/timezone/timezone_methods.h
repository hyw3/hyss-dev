/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TIMEZONE_METHODS_H
#define TIMEZONE_METHODS_H

#include <hyss.h>

HYSS_METHOD(IntlTimeZone, __construct);

HYSS_FUNCTION(intltz_create_time_zone);

HYSS_FUNCTION(intltz_from_date_time_zone);

HYSS_FUNCTION(intltz_create_default);

HYSS_FUNCTION(intltz_get_id);

HYSS_FUNCTION(intltz_get_gmt);

HYSS_FUNCTION(intltz_get_unknown);

HYSS_FUNCTION(intltz_create_enumeration);

HYSS_FUNCTION(intltz_count_equivalent_ids);

HYSS_FUNCTION(intltz_create_time_zone_id_enumeration);

HYSS_FUNCTION(intltz_get_canonical_id);

HYSS_FUNCTION(intltz_get_region);

HYSS_FUNCTION(intltz_get_tz_data_version);

HYSS_FUNCTION(intltz_get_equivalent_id);

HYSS_FUNCTION(intltz_use_daylight_time);

HYSS_FUNCTION(intltz_get_offset);

HYSS_FUNCTION(intltz_get_raw_offset);

HYSS_FUNCTION(intltz_has_same_rules);

HYSS_FUNCTION(intltz_get_display_name);

HYSS_FUNCTION(intltz_get_dst_savings);

HYSS_FUNCTION(intltz_to_date_time_zone);

HYSS_FUNCTION(intltz_get_error_code);

HYSS_FUNCTION(intltz_get_error_message);

#if U_ICU_VERSION_MAJOR_NUM >= 52
HYSS_FUNCTION(intltz_get_windows_id);
HYSS_FUNCTION(intltz_get_id_for_windows_id);
#endif

#endif /* #ifndef TIMEZONE_METHODS_H */
