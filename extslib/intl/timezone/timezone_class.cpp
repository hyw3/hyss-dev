/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "../intl_cppshims.h"

#include <unicode/timezone.h>
#include <unicode/calendar.h>
#include "../intl_convertcpp.h"

#include "../common/common_date.h"

extern "C" {
#include "../intl_convert.h"
#define USE_TIMEZONE_POINTER 1
#include "timezone_class.h"
#include "timezone_methods.h"
#include <gear_exceptions.h>
#include <gear_interfaces.h>
#include <extslib/date/hyss_date.h>
}

using icu::Calendar;

/* {{{ Global variables */
U_CDECL_BEGIN
gear_class_entry *TimeZone_ce_ptr = NULL;
gear_object_handlers TimeZone_handlers;
U_CDECL_END
/* }}} */

/* {{{ timezone_object_construct */
U_CFUNC void timezone_object_construct(const TimeZone *zone, zval *object, int owned)
{
	TimeZone_object	*to;

	object_init_ex(object, TimeZone_ce_ptr);
	TIMEZONE_METHOD_FETCH_OBJECT_NO_CHECK; /* fetch gear object from zval "object" into "to" */
	to->utimezone = zone;
	to->should_delete = owned;
}
/* }}} */

/* {{{ timezone_convert_to_datetimezone
 *	   Convert from TimeZone to DateTimeZone object */
U_CFUNC zval *timezone_convert_to_datetimezone(const TimeZone *timeZone,
											   intl_error *outside_error,
											   const char *func, zval *ret)
{
	UnicodeString		id;
	char				*message = NULL;
	hyss_timezone_obj	*tzobj;
	zval				arg;

	timeZone->getID(id);
	if (id.isBogus()) {
		spprintf(&message, 0, "%s: could not obtain TimeZone id", func);
		intl_errors_set(outside_error, U_ILLEGAL_ARGUMENT_ERROR,
			message, 1);
		goto error;
	}

	object_init_ex(ret, hyss_date_get_timezone_ce());
	tzobj = Z_HYSSTIMEZONE_P(ret);

	if (id.compare(0, 3, UnicodeString("GMT", sizeof("GMT")-1, US_INV)) == 0) {
		/* The DateTimeZone constructor doesn't support offset time zones,
		 * so we must mess with DateTimeZone structure ourselves */
		tzobj->initialized	  = 1;
		tzobj->type			  = TIMELIB_ZONETYPE_OFFSET;
		//convert offset from milliseconds to seconds
		tzobj->tzi.utc_offset = timeZone->getRawOffset() / 1000;
	} else {
		gear_string *u8str;
		/* Call the constructor! */
		u8str = intl_charFromString(id, &INTL_ERROR_CODE(*outside_error));
		if (!u8str) {
			spprintf(&message, 0, "%s: could not convert id to UTF-8", func);
			intl_errors_set(outside_error, INTL_ERROR_CODE(*outside_error),
				message, 1);
			goto error;
		}
		ZVAL_STR(&arg, u8str);
		gear_call_method_with_1_params(ret, NULL, &Z_OBJCE_P(ret)->constructor, "__construct", NULL, &arg);
		if (EG(exception)) {
			spprintf(&message, 0,
				"%s: DateTimeZone constructor threw exception", func);
			intl_errors_set(outside_error, U_ILLEGAL_ARGUMENT_ERROR,
				message, 1);
			gear_object_store_ctor_failed(Z_OBJ_P(ret));
			zval_ptr_dtor(&arg);
			goto error;
		}
		zval_ptr_dtor(&arg);
	}

	if (0) {
error:
		if (ret) {
			zval_ptr_dtor(ret);
		}
		ret = NULL;
	}

	if (message) {
		efree(message);
	}
	return ret;
}
/* }}} */

/* {{{ timezone_process_timezone_argument
 * TimeZone argument processor. outside_error may be NULL (for static functions/constructors) */
U_CFUNC TimeZone *timezone_process_timezone_argument(zval *zv_timezone,
													 intl_error *outside_error,
													 const char *func)
{
	zval		local_zv_tz;
	char		*message = NULL;
	TimeZone	*timeZone;

	if (zv_timezone == NULL || Z_TYPE_P(zv_timezone) == IS_NULL) {
		timelib_tzinfo *tzinfo = get_timezone_info();
		ZVAL_STRING(&local_zv_tz, tzinfo->name);
		zv_timezone = &local_zv_tz;
	} else {
		ZVAL_NULL(&local_zv_tz);
	}

	if (Z_TYPE_P(zv_timezone) == IS_OBJECT &&
			instanceof_function(Z_OBJCE_P(zv_timezone), TimeZone_ce_ptr)) {
		TimeZone_object *to = Z_INTL_TIMEZONE_P(zv_timezone);
		if (to->utimezone == NULL) {
			spprintf(&message, 0, "%s: passed IntlTimeZone is not "
				"properly constructed", func);
			if (message) {
				intl_errors_set(outside_error, U_ILLEGAL_ARGUMENT_ERROR, message, 1);
				efree(message);
			}
			zval_ptr_dtor_str(&local_zv_tz);
			return NULL;
		}
		timeZone = to->utimezone->clone();
		if (timeZone == NULL) {
			spprintf(&message, 0, "%s: could not clone TimeZone", func);
			if (message) {
				intl_errors_set(outside_error, U_MEMORY_ALLOCATION_ERROR, message, 1);
				efree(message);
			}
			zval_ptr_dtor_str(&local_zv_tz);
			return NULL;
		}
	} else if (Z_TYPE_P(zv_timezone) == IS_OBJECT &&
			instanceof_function(Z_OBJCE_P(zv_timezone), hyss_date_get_timezone_ce())) {

		hyss_timezone_obj *tzobj = Z_HYSSTIMEZONE_P(zv_timezone);

		zval_ptr_dtor_str(&local_zv_tz);
		return timezone_convert_datetimezone(tzobj->type, tzobj, 0,
			outside_error, func);
	} else {
		UnicodeString	id,
						gottenId;
		UErrorCode		status = U_ZERO_ERROR; /* outside_error may be NULL */
		convert_to_string_ex(zv_timezone);
		if (intl_stringFromChar(id, Z_STRVAL_P(zv_timezone), Z_STRLEN_P(zv_timezone),
				&status) == FAILURE) {
			spprintf(&message, 0, "%s: Time zone identifier given is not a "
				"valid UTF-8 string", func);
			if (message) {
				intl_errors_set(outside_error, status, message, 1);
				efree(message);
			}
			zval_ptr_dtor_str(&local_zv_tz);
			return NULL;
		}
		timeZone = TimeZone::createTimeZone(id);
		if (timeZone == NULL) {
			spprintf(&message, 0, "%s: could not create time zone", func);
			if (message) {
				intl_errors_set(outside_error, U_MEMORY_ALLOCATION_ERROR, message, 1);
				efree(message);
			}
			zval_ptr_dtor_str(&local_zv_tz);
			return NULL;
		}
		if (timeZone->getID(gottenId) != id) {
			spprintf(&message, 0, "%s: no such time zone: '%s'",
				func, Z_STRVAL_P(zv_timezone));
			if (message) {
				intl_errors_set(outside_error, U_ILLEGAL_ARGUMENT_ERROR, message, 1);
				efree(message);
			}
			zval_ptr_dtor_str(&local_zv_tz);
			delete timeZone;
			return NULL;
		}
	}

	zval_ptr_dtor_str(&local_zv_tz);

	return timeZone;
}
/* }}} */

/* {{{ clone handler for TimeZone */
static gear_object *TimeZone_clone_obj(zval *object)
{
	TimeZone_object		*to_orig,
						*to_new;
	gear_object			*ret_val;
	intl_error_reset(NULL);

	to_orig = Z_INTL_TIMEZONE_P(object);
	intl_error_reset(TIMEZONE_ERROR_P(to_orig));

	ret_val = TimeZone_ce_ptr->create_object(Z_OBJCE_P(object));
	to_new  = hyss_intl_timezone_fetch_object(ret_val);

	gear_objects_clone_members(&to_new->zo, &to_orig->zo);

	if (to_orig->utimezone != NULL) {
		TimeZone	*newTimeZone;

		newTimeZone = to_orig->utimezone->clone();
		to_new->should_delete = 1;
		if (!newTimeZone) {
			gear_string *err_msg;
			intl_errors_set_code(TIMEZONE_ERROR_P(to_orig),
				U_MEMORY_ALLOCATION_ERROR);
			intl_errors_set_custom_msg(TIMEZONE_ERROR_P(to_orig),
				"Could not clone IntlTimeZone", 0);
			err_msg = intl_error_get_message(TIMEZONE_ERROR_P(to_orig));
			gear_throw_exception(NULL, ZSTR_VAL(err_msg), 0);
			gear_string_free(err_msg);
		} else {
			to_new->utimezone = newTimeZone;
		}
	} else {
		gear_throw_exception(NULL, "Cannot clone unconstructed IntlTimeZone", 0);
	}

	return ret_val;
}
/* }}} */

/* {{{ compare_objects handler for TimeZone
 *     Can't be used for >, >=, <, <= comparisons */
static int TimeZone_compare_objects(zval *object1, zval *object2)
{
	TimeZone_object		*to1,
						*to2;
	to1 = Z_INTL_TIMEZONE_P(object1);
	to2 = Z_INTL_TIMEZONE_P(object2);

	if (to1->utimezone == NULL || to2->utimezone == NULL) {
		gear_throw_exception(NULL, "Comparison with at least one unconstructed "
				"IntlTimeZone operand", 0);
		/* intentionally not returning */
	} else {
		if (*to1->utimezone == *to2->utimezone) {
			return 0;
		}
	}

	return 1;
}
/* }}} */

/* {{{ get_debug_info handler for TimeZone */
static HashTable *TimeZone_get_debug_info(zval *object, int *is_temp)
{
	zval			zv;
	TimeZone_object	*to;
	const TimeZone	*tz;
	UnicodeString	ustr;
	gear_string     *u8str;
	HashTable 		*debug_info;
	UErrorCode		uec = U_ZERO_ERROR;

	*is_temp = 1;

	debug_info = gear_new_array(8);

	to = Z_INTL_TIMEZONE_P(object);
	tz = to->utimezone;

	if (tz == NULL) {
		ZVAL_FALSE(&zv);
		gear_hash_str_update(debug_info, "valid", sizeof("valid") - 1, &zv);
		return debug_info;
	}

	ZVAL_TRUE(&zv);
	gear_hash_str_update(debug_info, "valid", sizeof("valid") - 1, &zv);

	tz->getID(ustr);
	u8str = intl_convert_utf16_to_utf8(
		ustr.getBuffer(), ustr.length(), &uec);
	if (!u8str) {
		return debug_info;
	}
	ZVAL_NEW_STR(&zv, u8str);
	gear_hash_str_update(debug_info, "id", sizeof("id") - 1, &zv);

	int32_t rawOffset, dstOffset;
	UDate now = Calendar::getNow();
	tz->getOffset(now, FALSE, rawOffset, dstOffset, uec);
	if (U_FAILURE(uec)) {
		return debug_info;
	}

	ZVAL_LONG(&zv, (gear_long)rawOffset);
	gear_hash_str_update(debug_info,"rawOffset", sizeof("rawOffset") - 1, &zv);
	ZVAL_LONG(&zv, (gear_long)(rawOffset + dstOffset));
	gear_hash_str_update(debug_info,"currentOffset", sizeof("currentOffset") - 1, &zv);

	return debug_info;
}
/* }}} */

/* {{{ void TimeZone_object_init(TimeZone_object* to)
 * Initialize internals of TImeZone_object not specific to gear standard objects.
 */
static void TimeZone_object_init(TimeZone_object *to)
{
	intl_error_init(TIMEZONE_ERROR_P(to));
	to->utimezone = NULL;
	to->should_delete = 0;
}
/* }}} */

/* {{{ TimeZone_objects_dtor */
static void TimeZone_objects_dtor(gear_object *object)
{
	gear_objects_destroy_object(object);
}
/* }}} */

/* {{{ TimeZone_objects_free */
static void TimeZone_objects_free(gear_object *object)
{
	TimeZone_object* to = hyss_intl_timezone_fetch_object(object);

	if (to->utimezone && to->should_delete) {
		delete to->utimezone;
		to->utimezone = NULL;
	}
	intl_error_reset(TIMEZONE_ERROR_P(to));

	gear_object_std_dtor(&to->zo);
}
/* }}} */

/* {{{ TimeZone_object_create */
static gear_object *TimeZone_object_create(gear_class_entry *ce)
{
	TimeZone_object*	intern;

	intern = (TimeZone_object*)ecalloc(1, sizeof(TimeZone_object) + sizeof(zval) * (ce->default_properties_count - 1));

	gear_object_std_init(&intern->zo, ce);
    object_properties_init(&intern->zo, ce);
	TimeZone_object_init(intern);

	intern->zo.handlers = &TimeZone_handlers;

	return &intern->zo;
}
/* }}} */

/* {{{ TimeZone methods arguments info */

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_idarg, 0, 0, 1)
	GEAR_ARG_INFO(0, zoneId)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_fromDateTimeZone, 0, 0, 1)
	GEAR_ARG_OBJ_INFO(0, otherTimeZone, IntlTimeZone, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_createEnumeration, 0, 0, 0)
	GEAR_ARG_INFO(0, countryOrRawOffset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_countEquivalentIDs, 0, 0, 1)
	GEAR_ARG_INFO(0, zoneId)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_createTimeZoneIDEnumeration, 0, 0, 1)
	GEAR_ARG_INFO(0, zoneType)
	GEAR_ARG_INFO(0, region)
	GEAR_ARG_INFO(0, rawOffset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_getCanonicalID, 0, 0, 1)
	GEAR_ARG_INFO(0, zoneId)
	GEAR_ARG_INFO(1, isSystemID)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_getEquivalentID, 0, 0, 2)
	GEAR_ARG_INFO(0, zoneId)
	GEAR_ARG_INFO(0, index)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_getOffset, 0, 0, 4)
	GEAR_ARG_INFO(0, date)
	GEAR_ARG_INFO(0, local)
	GEAR_ARG_INFO(1, rawOffset)
	GEAR_ARG_INFO(1, dstOffset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_hasSameRules, 0, 0, 1)
	GEAR_ARG_OBJ_INFO(0, otherTimeZone, IntlTimeZone, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_getDisplayName, 0, 0, 0)
	GEAR_ARG_INFO(0, isDaylight)
	GEAR_ARG_INFO(0, style)
	GEAR_ARG_INFO(0, locale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_void, 0, 0, 0)
GEAR_END_ARG_INFO()

#if U_ICU_VERSION_MAJOR_NUM >= 52
GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_getWindowsID, 0, GEAR_RETURN_VALUE, 1)
	GEAR_ARG_INFO(0, timezone)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(ainfo_tz_getIDForWindowsID, 0, GEAR_RETURN_VALUE, 1)
	GEAR_ARG_INFO(0, timezone)
	GEAR_ARG_INFO(0, region)
GEAR_END_ARG_INFO()
#endif

/* }}} */

/* {{{ TimeZone_class_functions
 * Every 'IntlTimeZone' class method has an entry in this table
 */
static const gear_function_entry TimeZone_class_functions[] = {
	HYSS_ME(IntlTimeZone,				__construct,					ainfo_tz_void,				GEAR_ACC_PRIVATE)
	HYSS_ME_MAPPING(createTimeZone,		intltz_create_time_zone,		ainfo_tz_idarg,				GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(fromDateTimeZone,	intltz_from_date_time_zone,		ainfo_tz_idarg,				GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(createDefault,		intltz_create_default,			ainfo_tz_void,				GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(getGMT,				intltz_get_gmt,					ainfo_tz_void,				GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
#if U_ICU_VERSION_MAJOR_NUM >= 49
	HYSS_ME_MAPPING(getUnknown,			intltz_get_unknown,				ainfo_tz_void,				GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
#endif
	HYSS_ME_MAPPING(createEnumeration,	intltz_create_enumeration,		ainfo_tz_createEnumeration,	GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(countEquivalentIDs,	intltz_count_equivalent_ids,	ainfo_tz_idarg,				GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 48
	HYSS_ME_MAPPING(createTimeZoneIDEnumeration, intltz_create_time_zone_id_enumeration, ainfo_tz_createTimeZoneIDEnumeration, GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
#endif
	HYSS_ME_MAPPING(getCanonicalID,		intltz_get_canonical_id,		ainfo_tz_getCanonicalID,	GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 48
	HYSS_ME_MAPPING(getRegion,			intltz_get_region,				ainfo_tz_idarg,				GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
#endif
	HYSS_ME_MAPPING(getTZDataVersion,	intltz_get_tz_data_version,		ainfo_tz_void,				GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(getEquivalentID,		intltz_get_equivalent_id,		ainfo_tz_getEquivalentID,	GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)

	HYSS_ME_MAPPING(getID,				intltz_get_id,					ainfo_tz_void,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(useDaylightTime,		intltz_use_daylight_time,		ainfo_tz_void,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getOffset,			intltz_get_offset,				ainfo_tz_getOffset,			GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getRawOffset,		intltz_get_raw_offset,			ainfo_tz_void,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(hasSameRules,		intltz_has_same_rules,			ainfo_tz_hasSameRules,		GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getDisplayName,		intltz_get_display_name,		ainfo_tz_getDisplayName,	GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getDSTSavings,		intltz_get_dst_savings,			ainfo_tz_void,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(toDateTimeZone,		intltz_to_date_time_zone,		ainfo_tz_void,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getErrorCode,		intltz_get_error_code,			ainfo_tz_void,				GEAR_ACC_PUBLIC)
	HYSS_ME_MAPPING(getErrorMessage,		intltz_get_error_message,		ainfo_tz_void,				GEAR_ACC_PUBLIC)
#if U_ICU_VERSION_MAJOR_NUM >= 52
	HYSS_ME_MAPPING(getWindowsID,		intltz_get_windows_id,			ainfo_tz_getWindowsID,		GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(getIDForWindowsID,	intltz_get_id_for_windows_id,		ainfo_tz_getIDForWindowsID,	GEAR_ACC_PUBLIC | GEAR_ACC_STATIC)
#endif
	HYSS_FE_END
};
/* }}} */

/* {{{ timezone_register_IntlTimeZone_class
 * Initialize 'IntlTimeZone' class
 */
U_CFUNC void timezone_register_IntlTimeZone_class(void)
{
	gear_class_entry ce;

	/* Create and register 'IntlTimeZone' class. */
	INIT_CLASS_ENTRY(ce, "IntlTimeZone", TimeZone_class_functions);
	ce.create_object = TimeZone_object_create;
	TimeZone_ce_ptr = gear_register_internal_class(&ce);
	if (!TimeZone_ce_ptr) {
		//can't happen now without bigger problems before
		hyss_error_docref0(NULL, E_ERROR,
			"IntlTimeZone: class registration has failed.");
		return;
	}

	memcpy(&TimeZone_handlers, &std_object_handlers,
		sizeof TimeZone_handlers);
	TimeZone_handlers.offset = XtOffsetOf(TimeZone_object, zo);
	TimeZone_handlers.clone_obj = TimeZone_clone_obj;
	TimeZone_handlers.compare_objects = TimeZone_compare_objects;
	TimeZone_handlers.get_debug_info = TimeZone_get_debug_info;
	TimeZone_handlers.dtor_obj = TimeZone_objects_dtor;
	TimeZone_handlers.free_obj = TimeZone_objects_free;


	/* Declare 'IntlTimeZone' class constants */
#define TIMEZONE_DECL_LONG_CONST(name, val) \
	gear_declare_class_constant_long(TimeZone_ce_ptr, name, sizeof(name) - 1, \
		val)

	TIMEZONE_DECL_LONG_CONST("DISPLAY_SHORT", TimeZone::SHORT);
	TIMEZONE_DECL_LONG_CONST("DISPLAY_LONG", TimeZone::LONG);

#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 44
	TIMEZONE_DECL_LONG_CONST("DISPLAY_SHORT_GENERIC", TimeZone::SHORT_GENERIC);
	TIMEZONE_DECL_LONG_CONST("DISPLAY_LONG_GENERIC", TimeZone::LONG_GENERIC);
	TIMEZONE_DECL_LONG_CONST("DISPLAY_SHORT_GMT", TimeZone::SHORT_GMT);
	TIMEZONE_DECL_LONG_CONST("DISPLAY_LONG_GMT", TimeZone::LONG_GMT);
	TIMEZONE_DECL_LONG_CONST("DISPLAY_SHORT_COMMONLY_USED", TimeZone::SHORT_COMMONLY_USED);
	TIMEZONE_DECL_LONG_CONST("DISPLAY_GENERIC_LOCATION", TimeZone::GENERIC_LOCATION);
#endif

#if U_ICU_VERSION_MAJOR_NUM * 10 + U_ICU_VERSION_MINOR_NUM >= 48
	TIMEZONE_DECL_LONG_CONST("TYPE_ANY", UCAL_ZONE_TYPE_ANY);
	TIMEZONE_DECL_LONG_CONST("TYPE_CANONICAL", UCAL_ZONE_TYPE_CANONICAL);
	TIMEZONE_DECL_LONG_CONST("TYPE_CANONICAL_LOCATION", UCAL_ZONE_TYPE_CANONICAL_LOCATION);
#endif

	/* Declare 'IntlTimeZone' class properties */

}
/* }}} */
