/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TIMEZONE_CLASS_H
#define TIMEZONE_CLASS_H

//redefinition of inline in HYSS headers causes problems, so include this before
#include <math.h>

//fixes the build on windows for old versions of ICU
#include <stdio.h>

#include <hyss.h>
#include "intl_error.h"
#include "intl_data.h"

#ifndef USE_TIMEZONE_POINTER
typedef void TimeZone;
#else
using icu::TimeZone;
#endif

typedef struct {
	// 	error handling
	intl_error		err;

	// ICU TimeZone
	const TimeZone	*utimezone;

	//whether to delete the timezone on object free
	gear_bool		should_delete;

	gear_object		zo;
} TimeZone_object;

static inline TimeZone_object *hyss_intl_timezone_fetch_object(gear_object *obj) {
	return (TimeZone_object *)((char*)(obj) - XtOffsetOf(TimeZone_object, zo));
}
#define Z_INTL_TIMEZONE_P(zv) hyss_intl_timezone_fetch_object(Z_OBJ_P(zv))

#define TIMEZONE_ERROR(to)						(to)->err
#define TIMEZONE_ERROR_P(to)					&(TIMEZONE_ERROR(to))

#define TIMEZONE_ERROR_CODE(co)					INTL_ERROR_CODE(TIMEZONE_ERROR(to))
#define TIMEZONE_ERROR_CODE_P(co)				&(INTL_ERROR_CODE(TIMEZONE_ERROR(to)))

#define TIMEZONE_METHOD_INIT_VARS				INTL_METHOD_INIT_VARS(TimeZone, to)
#define TIMEZONE_METHOD_FETCH_OBJECT_NO_CHECK	INTL_METHOD_FETCH_OBJECT(INTL_TIMEZONE, to)
#define TIMEZONE_METHOD_FETCH_OBJECT\
	TIMEZONE_METHOD_FETCH_OBJECT_NO_CHECK; \
	if (to->utimezone == NULL) { \
		intl_errors_set(&to->err, U_ILLEGAL_ARGUMENT_ERROR, "Found unconstructed IntlTimeZone", 0); \
		RETURN_FALSE; \
	}

zval *timezone_convert_to_datetimezone(const TimeZone *timeZone, intl_error *outside_error, const char *func, zval *ret);
TimeZone *timezone_process_timezone_argument(zval *zv_timezone, intl_error *error, const char *func);

void timezone_object_construct(const TimeZone *zone, zval *object, int owned);

void timezone_register_IntlTimeZone_class(void);

extern gear_class_entry *TimeZone_ce_ptr;
extern gear_object_handlers TimeZone_handlers;

#endif /* #ifndef TIMEZONE_CLASS_H */
