/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTL_DATA_H
#define INTL_DATA_H

#include <unicode/utypes.h>

#include "intl_error.h"

/* Mock object to generalize error handling in sub-cAPIs.
   Sub-cAPI data structures should always have error as first element
   for this to work!
*/
typedef struct _intl_data {
	intl_error		error;
	gear_object		zo;
} intl_object;

#define INTL_METHOD_INIT_VARS(oclass, obj)		\
	zval*             object  = NULL;			\
	oclass##_object*  obj     = NULL;			\
	intl_error_reset( NULL );

#define INTL_DATA_ERROR(obj)				(((intl_object *)(obj))->error)
#define INTL_DATA_ERROR_P(obj)				(&(INTL_DATA_ERROR((obj))))
#define INTL_DATA_ERROR_CODE(obj)			INTL_ERROR_CODE(INTL_DATA_ERROR((obj)))

#define INTL_METHOD_FETCH_OBJECT(oclass, obj)									\
	obj = Z_##oclass##_P( object );												\
    intl_error_reset( INTL_DATA_ERROR_P(obj) );						\

/* Check status by error code, if error return false */
#define INTL_CHECK_STATUS(err, msg)											\
    intl_error_set_code( NULL, (err) );							\
    if( U_FAILURE((err)) )													\
    {																		\
        intl_error_set_custom_msg( NULL, msg, 0 );				\
        RETURN_FALSE;														\
    }

/* Check status by error code, if error return null */
#define INTL_CHECK_STATUS_OR_NULL(err, msg)                     \
    intl_error_set_code( NULL, (err) );             \
    if( U_FAILURE((err)) )                          \
    {                                   \
        intl_error_set_custom_msg( NULL, msg, 0 );        \
        RETURN_NULL();                           \
    }


/* Check status in object, if error return false */
#define INTL_METHOD_CHECK_STATUS(obj, msg)											\
    intl_error_set_code( NULL, INTL_DATA_ERROR_CODE((obj)) );				\
    if( U_FAILURE( INTL_DATA_ERROR_CODE((obj)) ) )									\
    {																				\
        intl_errors_set_custom_msg( INTL_DATA_ERROR_P((obj)), msg, 0 );	\
        RETURN_FALSE;										\
    }

/* Check status in object, if error return null */
#define INTL_METHOD_CHECK_STATUS_OR_NULL(obj, msg)									\
    intl_error_set_code( NULL, INTL_DATA_ERROR_CODE((obj)) );						\
    if( U_FAILURE( INTL_DATA_ERROR_CODE((obj)) ) )									\
    {																				\
        intl_errors_set_custom_msg( INTL_DATA_ERROR_P((obj)), msg, 0 );				\
        zval_ptr_dtor(return_value);												\
        RETURN_NULL();																\
    }

/* Check status in object, if error return FAILURE */
#define INTL_CTOR_CHECK_STATUS(obj, msg)											\
    intl_error_set_code( NULL, INTL_DATA_ERROR_CODE((obj)) );						\
    if( U_FAILURE( INTL_DATA_ERROR_CODE((obj)) ) )									\
    {																				\
        intl_errors_set_custom_msg( INTL_DATA_ERROR_P((obj)), msg, 0 );				\
        return FAILURE;																\
    }

#define INTL_METHOD_RETVAL_UTF8(obj, ustring, ulen, free_it)									\
{																								\
	gear_string *u8str;																			\
	u8str = intl_convert_utf16_to_utf8(ustring, ulen, &INTL_DATA_ERROR_CODE((obj)));			\
	if((free_it)) {																				\
		efree(ustring);																			\
	}																							\
	INTL_METHOD_CHECK_STATUS((obj), "Error converting value to UTF-8");							\
	RETVAL_NEW_STR(u8str);																		\
}

#define INTL_MAX_LOCALE_LEN (ULOC_FULLNAME_CAPACITY-1)

#define INTL_CHECK_LOCALE_LEN(locale_len)												\
	if((locale_len) > INTL_MAX_LOCALE_LEN) {											\
		char *_msg; \
		spprintf(&_msg, 0, "Locale string too long, should be no longer than %d characters", INTL_MAX_LOCALE_LEN);			\
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,	_msg, 1); 								\
		efree(_msg); \
		RETURN_NULL();																	\
	}

#define INTL_CHECK_LOCALE_LEN_OR_FAILURE(locale_len)									\
	if((locale_len) > INTL_MAX_LOCALE_LEN) {											\
		char *_msg; \
		spprintf(&_msg, 0, "Locale string too long, should be no longer than %d characters", INTL_MAX_LOCALE_LEN);			\
		intl_error_set( NULL, U_ILLEGAL_ARGUMENT_ERROR,	_msg, 1); 								\
		efree(_msg); \
		return FAILURE;																	\
	}

#endif // INTL_DATA_H
