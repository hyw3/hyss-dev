/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GRAPHEME_GRAPHEME_H
#define GRAPHEME_GRAPHEME_H

#include <hyss.h>
#include <unicode/utypes.h>

HYSS_FUNCTION(grapheme_strlen);
HYSS_FUNCTION(grapheme_strpos);
HYSS_FUNCTION(grapheme_stripos);
HYSS_FUNCTION(grapheme_strrpos);
HYSS_FUNCTION(grapheme_strripos);
HYSS_FUNCTION(grapheme_substr);
HYSS_FUNCTION(grapheme_strstr);
HYSS_FUNCTION(grapheme_stristr);
HYSS_FUNCTION(grapheme_extract);

void grapheme_register_constants( INIT_FUNC_ARGS );
void grapheme_close_global_iterator( void );

#endif // GRAPHEME_GRAPHEME_H
