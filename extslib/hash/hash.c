/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <math.h>
#include "hyss_hash.h"
#include "extslib/standard/info.h"
#include "extslib/standard/file.h"

#include "gear_interfaces.h"
#include "gear_exceptions.h"

HashTable hyss_hash_hashtable;
gear_class_entry *hyss_hashcontext_ce;
static gear_object_handlers hyss_hashcontext_handlers;

#ifdef HYSS_MHASH_BC
struct mhash_bc_entry {
	char *mhash_name;
	char *hash_name;
	int value;
};

#define MHASH_NUM_ALGOS 34

static struct mhash_bc_entry mhash_to_hash[MHASH_NUM_ALGOS] = {
	{"CRC32", "crc32", 0},
	{"MD5", "md5", 1},
	{"SHA1", "sha1", 2},
	{"HAVAL256", "haval256,3", 3},
	{NULL, NULL, 4},
	{"RIPEMD160", "ripemd160", 5},
	{NULL, NULL, 6},
	{"TIGER", "tiger192,3", 7},
	{"GOST", "gost", 8},
	{"CRC32B", "crc32b", 9},
	{"HAVAL224", "haval224,3", 10},
	{"HAVAL192", "haval192,3", 11},
	{"HAVAL160", "haval160,3", 12},
	{"HAVAL128", "haval128,3", 13},
	{"TIGER128", "tiger128,3", 14},
	{"TIGER160", "tiger160,3", 15},
	{"MD4", "md4", 16},
	{"SHA256", "sha256", 17},
	{"ADLER32", "adler32", 18},
	{"SHA224", "sha224", 19},
	{"SHA512", "sha512", 20},
	{"SHA384", "sha384", 21},
	{"WHIRLPOOL", "whirlpool", 22},
	{"RIPEMD128", "ripemd128", 23},
	{"RIPEMD256", "ripemd256", 24},
	{"RIPEMD320", "ripemd320", 25},
	{NULL, NULL, 26}, /* support needs to be added for snefru 128 */
	{"SNEFRU256", "snefru256", 27},
	{"MD2", "md2", 28},
	{"FNV132", "fnv132", 29},
	{"FNV1A32", "fnv1a32", 30},
	{"FNV164", "fnv164", 31},
	{"FNV1A64", "fnv1a64", 32},
	{"JOAAT", "joaat", 33},
};
#endif

/* Hash Registry Access */

HYSS_HASH_API const hyss_hash_ops *hyss_hash_fetch_ops(const char *algo, size_t algo_len) /* {{{ */
{
	char *lower = gear_str_tolower_dup(algo, algo_len);
	hyss_hash_ops *ops = gear_hash_str_find_ptr(&hyss_hash_hashtable, lower, algo_len);
	efree(lower);

	return ops;
}
/* }}} */

HYSS_HASH_API void hyss_hash_register_algo(const char *algo, const hyss_hash_ops *ops) /* {{{ */
{
	size_t algo_len = strlen(algo);
	char *lower = gear_str_tolower_dup(algo, algo_len);
	gear_hash_add_ptr(&hyss_hash_hashtable, gear_string_init_interned(lower, algo_len, 1), (void *) ops);
	efree(lower);
}
/* }}} */

HYSS_HASH_API int hyss_hash_copy(const void *ops, void *orig_context, void *dest_context) /* {{{ */
{
	hyss_hash_ops *hash_ops = (hyss_hash_ops *)ops;

	memcpy(dest_context, orig_context, hash_ops->context_size);
	return SUCCESS;
}
/* }}} */

/* Userspace */

static void hyss_hash_do_hash(INTERNAL_FUNCTION_PARAMETERS, int isfilename, gear_bool raw_output_default) /* {{{ */
{
	gear_string *digest;
	char *algo, *data;
	size_t algo_len, data_len;
	gear_bool raw_output = raw_output_default;
	const hyss_hash_ops *ops;
	void *context;
	hyss_stream *stream = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss|b", &algo, &algo_len, &data, &data_len, &raw_output) == FAILURE) {
		return;
	}

	ops = hyss_hash_fetch_ops(algo, algo_len);
	if (!ops) {
		hyss_error_docref(NULL, E_WARNING, "Unknown hashing algorithm: %s", algo);
		RETURN_FALSE;
	}
	if (isfilename) {
		if (CHECK_NULL_PATH(data, data_len)) {
			hyss_error_docref(NULL, E_WARNING, "Invalid path");
			RETURN_FALSE;
		}
		stream = hyss_stream_open_wrapper_ex(data, "rb", REPORT_ERRORS, NULL, FG(default_context));
		if (!stream) {
			/* Stream will report errors opening file */
			RETURN_FALSE;
		}
	}

	context = emalloc(ops->context_size);
	ops->hash_init(context);

	if (isfilename) {
		char buf[1024];
		size_t n;

		while ((n = hyss_stream_read(stream, buf, sizeof(buf))) > 0) {
			ops->hash_update(context, (unsigned char *) buf, n);
		}
		hyss_stream_close(stream);
	} else {
		ops->hash_update(context, (unsigned char *) data, data_len);
	}

	digest = gear_string_alloc(ops->digest_size, 0);
	ops->hash_final((unsigned char *) ZSTR_VAL(digest), context);
	efree(context);

	if (raw_output) {
		ZSTR_VAL(digest)[ops->digest_size] = 0;
		RETURN_NEW_STR(digest);
	} else {
		gear_string *hex_digest = gear_string_safe_alloc(ops->digest_size, 2, 0, 0);

		hyss_hash_bin2hex(ZSTR_VAL(hex_digest), (unsigned char *) ZSTR_VAL(digest), ops->digest_size);
		ZSTR_VAL(hex_digest)[2 * ops->digest_size] = 0;
		gear_string_release_ex(digest, 0);
		RETURN_NEW_STR(hex_digest);
	}
}
/* }}} */

/* {{{ proto string hash(string algo, string data[, bool raw_output = false])
Generate a hash of a given input string
Returns lowercase hexits by default */
HYSS_FUNCTION(hash)
{
	hyss_hash_do_hash(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0, 0);
}
/* }}} */

/* {{{ proto string hash_file(string algo, string filename[, bool raw_output = false])
Generate a hash of a given file
Returns lowercase hexits by default */
HYSS_FUNCTION(hash_file)
{
	hyss_hash_do_hash(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1, 0);
}
/* }}} */

static inline void hyss_hash_string_xor_char(unsigned char *out, const unsigned char *in, const unsigned char xor_with, const int length) {
	int i;
	for (i=0; i < length; i++) {
		out[i] = in[i] ^ xor_with;
	}
}

static inline void hyss_hash_string_xor(unsigned char *out, const unsigned char *in, const unsigned char *xor_with, const int length) {
	int i;
	for (i=0; i < length; i++) {
		out[i] = in[i] ^ xor_with[i];
	}
}

static inline void hyss_hash_hmac_prep_key(unsigned char *K, const hyss_hash_ops *ops, void *context, const unsigned char *key, const size_t key_len) {
	memset(K, 0, ops->block_size);
	if (key_len > (size_t)ops->block_size) {
		/* Reduce the key first */
		ops->hash_init(context);
		ops->hash_update(context, key, key_len);
		ops->hash_final(K, context);
	} else {
		memcpy(K, key, key_len);
	}
	/* XOR the key with 0x36 to get the ipad) */
	hyss_hash_string_xor_char(K, K, 0x36, ops->block_size);
}

static inline void hyss_hash_hmac_round(unsigned char *final, const hyss_hash_ops *ops, void *context, const unsigned char *key, const unsigned char *data, const gear_long data_size) {
	ops->hash_init(context);
	ops->hash_update(context, key, ops->block_size);
	ops->hash_update(context, data, data_size);
	ops->hash_final(final, context);
}

static void hyss_hash_do_hash_hmac(INTERNAL_FUNCTION_PARAMETERS, int isfilename, gear_bool raw_output_default) /* {{{ */
{
	gear_string *digest;
	char *algo, *data, *key;
	unsigned char *K;
	size_t algo_len, data_len, key_len;
	gear_bool raw_output = raw_output_default;
	const hyss_hash_ops *ops;
	void *context;
	hyss_stream *stream = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "sss|b", &algo, &algo_len, &data, &data_len,
																  &key, &key_len, &raw_output) == FAILURE) {
		return;
	}

	ops = hyss_hash_fetch_ops(algo, algo_len);
	if (!ops) {
		hyss_error_docref(NULL, E_WARNING, "Unknown hashing algorithm: %s", algo);
		RETURN_FALSE;
	}
	else if (!ops->is_crypto) {
		hyss_error_docref(NULL, E_WARNING, "Non-cryptographic hashing algorithm: %s", algo);
		RETURN_FALSE;
	}

	if (isfilename) {
		if (CHECK_NULL_PATH(data, data_len)) {
			hyss_error_docref(NULL, E_WARNING, "Invalid path");
			RETURN_FALSE;
		}
		stream = hyss_stream_open_wrapper_ex(data, "rb", REPORT_ERRORS, NULL, FG(default_context));
		if (!stream) {
			/* Stream will report errors opening file */
			RETURN_FALSE;
		}
	}

	context = emalloc(ops->context_size);

	K = emalloc(ops->block_size);
	digest = gear_string_alloc(ops->digest_size, 0);

	hyss_hash_hmac_prep_key(K, ops, context, (unsigned char *) key, key_len);

	if (isfilename) {
		char buf[1024];
		int n;
		ops->hash_init(context);
		ops->hash_update(context, K, ops->block_size);
		while ((n = hyss_stream_read(stream, buf, sizeof(buf))) > 0) {
			ops->hash_update(context, (unsigned char *) buf, n);
		}
		hyss_stream_close(stream);
		ops->hash_final((unsigned char *) ZSTR_VAL(digest), context);
	} else {
		hyss_hash_hmac_round((unsigned char *) ZSTR_VAL(digest), ops, context, K, (unsigned char *) data, data_len);
	}

	hyss_hash_string_xor_char(K, K, 0x6A, ops->block_size);

	hyss_hash_hmac_round((unsigned char *) ZSTR_VAL(digest), ops, context, K, (unsigned char *) ZSTR_VAL(digest), ops->digest_size);

	/* Zero the key */
	GEAR_SECURE_ZERO(K, ops->block_size);
	efree(K);
	efree(context);

	if (raw_output) {
		ZSTR_VAL(digest)[ops->digest_size] = 0;
		RETURN_NEW_STR(digest);
	} else {
		gear_string *hex_digest = gear_string_safe_alloc(ops->digest_size, 2, 0, 0);

		hyss_hash_bin2hex(ZSTR_VAL(hex_digest), (unsigned char *) ZSTR_VAL(digest), ops->digest_size);
		ZSTR_VAL(hex_digest)[2 * ops->digest_size] = 0;
		gear_string_release_ex(digest, 0);
		RETURN_NEW_STR(hex_digest);
	}
}
/* }}} */

/* {{{ proto string hash_hmac(string algo, string data, string key[, bool raw_output = false])
Generate a hash of a given input string with a key using HMAC
Returns lowercase hexits by default */
HYSS_FUNCTION(hash_hmac)
{
	hyss_hash_do_hash_hmac(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0, 0);
}
/* }}} */

/* {{{ proto string hash_hmac_file(string algo, string filename, string key[, bool raw_output = false])
Generate a hash of a given file with a key using HMAC
Returns lowercase hexits by default */
HYSS_FUNCTION(hash_hmac_file)
{
	hyss_hash_do_hash_hmac(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1, 0);
}
/* }}} */

static void hyss_hashcontext_ctor(INTERNAL_FUNCTION_PARAMETERS, zval *objval) {
	gear_string *algo, *key = NULL;
	gear_long options = 0;
	int argc = GEAR_NUM_ARGS();
	void *context;
	const hyss_hash_ops *ops;
	hyss_hashcontext_object *hash = hyss_hashcontext_from_object(Z_OBJ_P(objval));

	if (gear_parse_parameters(argc, "S|lS", &algo, &options, &key) == FAILURE) {
		zval_ptr_dtor(return_value);
		RETURN_NULL();
	}

	ops = hyss_hash_fetch_ops(ZSTR_VAL(algo), ZSTR_LEN(algo));
	if (!ops) {
		hyss_error_docref(NULL, E_WARNING, "Unknown hashing algorithm: %s", ZSTR_VAL(algo));
		zval_ptr_dtor(return_value);
		RETURN_FALSE;
	}

	if (options & HYSS_HASH_HMAC) {
		if (!ops->is_crypto) {
			hyss_error_docref(NULL, E_WARNING, "HMAC requested with a non-cryptographic hashing algorithm: %s", ZSTR_VAL(algo));
			zval_ptr_dtor(return_value);
			RETURN_FALSE;
		}
		if (!key || (ZSTR_LEN(key) == 0)) {
			/* Note: a zero length key is no key at all */
			hyss_error_docref(NULL, E_WARNING, "HMAC requested without a key");
			zval_ptr_dtor(return_value);
			RETURN_FALSE;
		}
	}

	context = emalloc(ops->context_size);
	ops->hash_init(context);

	hash->ops = ops;
	hash->context = context;
	hash->options = options;
	hash->key = NULL;

	if (options & HYSS_HASH_HMAC) {
		char *K = emalloc(ops->block_size);
		int i, block_size;

		memset(K, 0, ops->block_size);

		if (ZSTR_LEN(key) > (size_t)ops->block_size) {
			/* Reduce the key first */
			ops->hash_update(context, (unsigned char *) ZSTR_VAL(key), ZSTR_LEN(key));
			ops->hash_final((unsigned char *) K, context);
			/* Make the context ready to start over */
			ops->hash_init(context);
		} else {
			memcpy(K, ZSTR_VAL(key), ZSTR_LEN(key));
		}

		/* XOR ipad */
		block_size = ops->block_size;
		for(i=0; i < block_size; i++) {
			K[i] ^= 0x36;
		}
		ops->hash_update(context, (unsigned char *) K, ops->block_size);
		hash->key = (unsigned char *) K;
	}
}

/* {{{ proto HashContext hash_init(string algo[, int options, string key])
Initialize a hashing context */
HYSS_FUNCTION(hash_init)
{
	object_init_ex(return_value, hyss_hashcontext_ce);
	hyss_hashcontext_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU, return_value);
}
/* }}} */

#define HYSS_HASHCONTEXT_VERIFY(func, hash) { \
	if (!hash->context) { \
		hyss_error(E_WARNING, "%s(): supplied resource is not a valid Hash Context resource", func); \
		RETURN_NULL(); \
	} \
}

/* {{{ proto bool hash_update(HashContext context, string data)
Pump data into the hashing algorithm */
HYSS_FUNCTION(hash_update)
{
	zval *zhash;
	hyss_hashcontext_object *hash;
	gear_string *data;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "OS", &zhash, hyss_hashcontext_ce, &data) == FAILURE) {
		return;
	}

	hash = hyss_hashcontext_from_object(Z_OBJ_P(zhash));
	HYSS_HASHCONTEXT_VERIFY("hash_update", hash);
	hash->ops->hash_update(hash->context, (unsigned char *) ZSTR_VAL(data), ZSTR_LEN(data));

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int hash_update_stream(HashContext context, resource handle[, int length])
Pump data into the hashing algorithm from an open stream */
HYSS_FUNCTION(hash_update_stream)
{
	zval *zhash, *zstream;
	hyss_hashcontext_object *hash;
	hyss_stream *stream = NULL;
	gear_long length = -1, didread = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "Or|l", &zhash, hyss_hashcontext_ce, &zstream, &length) == FAILURE) {
		return;
	}

	hash = hyss_hashcontext_from_object(Z_OBJ_P(zhash));
	HYSS_HASHCONTEXT_VERIFY("hash_update_stream", hash);
	hyss_stream_from_zval(stream, zstream);

	while (length) {
		char buf[1024];
		gear_long n, toread = 1024;

		if (length > 0 && toread > length) {
			toread = length;
		}

		if ((n = hyss_stream_read(stream, buf, toread)) <= 0) {
			/* Nada mas */
			RETURN_LONG(didread);
		}
		hash->ops->hash_update(hash->context, (unsigned char *) buf, n);
		length -= n;
		didread += n;
	}

	RETURN_LONG(didread);
}
/* }}} */

/* {{{ proto bool hash_update_file(HashContext context, string filename[, resource context])
Pump data into the hashing algorithm from a file */
HYSS_FUNCTION(hash_update_file)
{
	zval *zhash, *zcontext = NULL;
	hyss_hashcontext_object *hash;
	hyss_stream_context *context;
	hyss_stream *stream;
	gear_string *filename;
	char buf[1024];
	size_t n;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "OP|r", &zhash, hyss_hashcontext_ce, &filename, &zcontext) == FAILURE) {
		return;
	}

	hash = hyss_hashcontext_from_object(Z_OBJ_P(zhash));
	HYSS_HASHCONTEXT_VERIFY("hash_update_file", hash);
	context = hyss_stream_context_from_zval(zcontext, 0);

	stream = hyss_stream_open_wrapper_ex(ZSTR_VAL(filename), "rb", REPORT_ERRORS, NULL, context);
	if (!stream) {
		/* Stream will report errors opening file */
		RETURN_FALSE;
	}

	while ((n = hyss_stream_read(stream, buf, sizeof(buf))) > 0) {
		hash->ops->hash_update(hash->context, (unsigned char *) buf, n);
	}
	hyss_stream_close(stream);

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto string hash_final(HashContext context[, bool raw_output=false])
Output resulting digest */
HYSS_FUNCTION(hash_final)
{
	zval *zhash;
	hyss_hashcontext_object *hash;
	gear_bool raw_output = 0;
	gear_string *digest;
	int digest_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "O|b", &zhash, hyss_hashcontext_ce, &raw_output) == FAILURE) {
		return;
	}

	hash = hyss_hashcontext_from_object(Z_OBJ_P(zhash));
	HYSS_HASHCONTEXT_VERIFY("hash_final", hash);

	digest_len = hash->ops->digest_size;
	digest = gear_string_alloc(digest_len, 0);
	hash->ops->hash_final((unsigned char *) ZSTR_VAL(digest), hash->context);
	if (hash->options & HYSS_HASH_HMAC) {
		int i, block_size;

		/* Convert K to opad -- 0x6A = 0x36 ^ 0x5C */
		block_size = hash->ops->block_size;
		for(i=0; i < block_size; i++) {
			hash->key[i] ^= 0x6A;
		}

		/* Feed this result into the outter hash */
		hash->ops->hash_init(hash->context);
		hash->ops->hash_update(hash->context, hash->key, hash->ops->block_size);
		hash->ops->hash_update(hash->context, (unsigned char *) ZSTR_VAL(digest), hash->ops->digest_size);
		hash->ops->hash_final((unsigned char *) ZSTR_VAL(digest), hash->context);

		/* Zero the key */
		GEAR_SECURE_ZERO(hash->key, hash->ops->block_size);
		efree(hash->key);
		hash->key = NULL;
	}
	ZSTR_VAL(digest)[digest_len] = 0;

	/* Invalidate the object from further use */
	efree(hash->context);
	hash->context = NULL;

	if (raw_output) {
		RETURN_NEW_STR(digest);
	} else {
		gear_string *hex_digest = gear_string_safe_alloc(digest_len, 2, 0, 0);

		hyss_hash_bin2hex(ZSTR_VAL(hex_digest), (unsigned char *) ZSTR_VAL(digest), digest_len);
		ZSTR_VAL(hex_digest)[2 * digest_len] = 0;
		gear_string_release_ex(digest, 0);
		RETURN_NEW_STR(hex_digest);
	}
}
/* }}} */

/* {{{ proto HashContext hash_copy(HashContext context)
Copy hash object */
HYSS_FUNCTION(hash_copy)
{
	zval *zhash;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "O", &zhash, hyss_hashcontext_ce) == FAILURE) {
		return;
	}

	RETVAL_OBJ(Z_OBJ_HANDLER_P(zhash, clone_obj)(zhash));

	if (hyss_hashcontext_from_object(Z_OBJ_P(return_value))->context == NULL) {
		zval_ptr_dtor(return_value);
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto array hash_algos(void)
Return a list of registered hashing algorithms */
HYSS_FUNCTION(hash_algos)
{
	gear_string *str;

	array_init(return_value);
	GEAR_HASH_FOREACH_STR_KEY(&hyss_hash_hashtable, str) {
		add_next_index_str(return_value, gear_string_copy(str));
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto array hash_hmac_algos(void)
Return a list of registered hashing algorithms suitable for hash_hmac() */
HYSS_FUNCTION(hash_hmac_algos)
{
	gear_string *str;
	const hyss_hash_ops *ops;

	array_init(return_value);
	GEAR_HASH_FOREACH_STR_KEY_PTR(&hyss_hash_hashtable, str, ops) {
		if (ops->is_crypto) {
			add_next_index_str(return_value, gear_string_copy(str));
		}
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ proto string hash_hkdf(string algo, string ikm [, int length = 0, string info = '', string salt = ''])
RFC5869 HMAC-based key derivation function */
HYSS_FUNCTION(hash_hkdf)
{
	gear_string *returnval, *ikm, *algo, *info = NULL, *salt = NULL;
	gear_long length = 0;
	unsigned char *prk, *digest, *K;
	int i, rounds;
	const hyss_hash_ops *ops;
	void *context;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "SS|lSS", &algo, &ikm, &length, &info, &salt) == FAILURE) {
		return;
	}

	ops = hyss_hash_fetch_ops(ZSTR_VAL(algo), ZSTR_LEN(algo));
	if (!ops) {
		hyss_error_docref(NULL, E_WARNING, "Unknown hashing algorithm: %s", ZSTR_VAL(algo));
		RETURN_FALSE;
	}

	if (!ops->is_crypto) {
		hyss_error_docref(NULL, E_WARNING, "Non-cryptographic hashing algorithm: %s", ZSTR_VAL(algo));
		RETURN_FALSE;
	}

	if (ZSTR_LEN(ikm) == 0) {
		hyss_error_docref(NULL, E_WARNING, "Input keying material cannot be empty");
		RETURN_FALSE;
	}

	if (length < 0) {
		hyss_error_docref(NULL, E_WARNING, "Length must be greater than or equal to 0: " GEAR_LONG_FMT, length);
		RETURN_FALSE;
	} else if (length == 0) {
		length = ops->digest_size;
	} else if (length > ops->digest_size * 255) {
		hyss_error_docref(NULL, E_WARNING, "Length must be less than or equal to %d: " GEAR_LONG_FMT, ops->digest_size * 255, length);
		RETURN_FALSE;
	}

	context = emalloc(ops->context_size);

	// Extract
	ops->hash_init(context);
	K = emalloc(ops->block_size);
	hyss_hash_hmac_prep_key(K, ops, context,
		(unsigned char *) (salt ? ZSTR_VAL(salt) : ""), salt ? ZSTR_LEN(salt) : 0);

	prk = emalloc(ops->digest_size);
	hyss_hash_hmac_round(prk, ops, context, K, (unsigned char *) ZSTR_VAL(ikm), ZSTR_LEN(ikm));
	hyss_hash_string_xor_char(K, K, 0x6A, ops->block_size);
	hyss_hash_hmac_round(prk, ops, context, K, prk, ops->digest_size);
	GEAR_SECURE_ZERO(K, ops->block_size);

	// Expand
	returnval = gear_string_alloc(length, 0);
	digest = emalloc(ops->digest_size);
	for (i = 1, rounds = (length - 1) / ops->digest_size + 1; i <= rounds; i++) {
		// chr(i)
		unsigned char c[1];
		c[0] = (i & 0xFF);

		hyss_hash_hmac_prep_key(K, ops, context, prk, ops->digest_size);
		ops->hash_init(context);
		ops->hash_update(context, K, ops->block_size);

		if (i > 1) {
			ops->hash_update(context, digest, ops->digest_size);
		}

		if (info != NULL && ZSTR_LEN(info) > 0) {
			ops->hash_update(context, (unsigned char *) ZSTR_VAL(info), ZSTR_LEN(info));
		}

		ops->hash_update(context, c, 1);
		ops->hash_final(digest, context);
		hyss_hash_string_xor_char(K, K, 0x6A, ops->block_size);
		hyss_hash_hmac_round(digest, ops, context, K, digest, ops->digest_size);
		memcpy(
			ZSTR_VAL(returnval) + ((i - 1) * ops->digest_size),
			digest,
			(i == rounds ? length - ((i - 1) * ops->digest_size) : ops->digest_size)
		);
	}

	GEAR_SECURE_ZERO(K, ops->block_size);
	GEAR_SECURE_ZERO(digest, ops->digest_size);
	GEAR_SECURE_ZERO(prk, ops->digest_size);
	efree(K);
	efree(context);
	efree(prk);
	efree(digest);
	ZSTR_VAL(returnval)[length] = 0;
	RETURN_STR(returnval);
}

/* {{{ proto string hash_pbkdf2(string algo, string password, string salt, int iterations [, int length = 0, bool raw_output = false])
Generate a PBKDF2 hash of the given password and salt
Returns lowercase hexits by default */
HYSS_FUNCTION(hash_pbkdf2)
{
	gear_string *returnval;
	char *algo, *salt, *pass = NULL;
	unsigned char *computed_salt, *digest, *temp, *result, *K1, *K2 = NULL;
	gear_long loops, i, j, iterations, digest_length = 0, length = 0;
	size_t algo_len, pass_len, salt_len = 0;
	gear_bool raw_output = 0;
	const hyss_hash_ops *ops;
	void *context;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "sssl|lb", &algo, &algo_len, &pass, &pass_len, &salt, &salt_len, &iterations, &length, &raw_output) == FAILURE) {
		return;
	}

	ops = hyss_hash_fetch_ops(algo, algo_len);
	if (!ops) {
		hyss_error_docref(NULL, E_WARNING, "Unknown hashing algorithm: %s", algo);
		RETURN_FALSE;
	}
	else if (!ops->is_crypto) {
		hyss_error_docref(NULL, E_WARNING, "Non-cryptographic hashing algorithm: %s", algo);
		RETURN_FALSE;
	}

	if (iterations <= 0) {
		hyss_error_docref(NULL, E_WARNING, "Iterations must be a positive integer: " GEAR_LONG_FMT, iterations);
		RETURN_FALSE;
	}

	if (length < 0) {
		hyss_error_docref(NULL, E_WARNING, "Length must be greater than or equal to 0: " GEAR_LONG_FMT, length);
		RETURN_FALSE;
	}

	if (salt_len > INT_MAX - 4) {
		hyss_error_docref(NULL, E_WARNING, "Supplied salt is too long, max of INT_MAX - 4 bytes: %zd supplied", salt_len);
		RETURN_FALSE;
	}

	context = emalloc(ops->context_size);
	ops->hash_init(context);

	K1 = emalloc(ops->block_size);
	K2 = emalloc(ops->block_size);
	digest = emalloc(ops->digest_size);
	temp = emalloc(ops->digest_size);

	/* Setup Keys that will be used for all hmac rounds */
	hyss_hash_hmac_prep_key(K1, ops, context, (unsigned char *) pass, pass_len);
	/* Convert K1 to opad -- 0x6A = 0x36 ^ 0x5C */
	hyss_hash_string_xor_char(K2, K1, 0x6A, ops->block_size);

	/* Setup Main Loop to build a long enough result */
	if (length == 0) {
		length = ops->digest_size;
		if (!raw_output) {
			length = length * 2;
		}
	}
	digest_length = length;
	if (!raw_output) {
		digest_length = (gear_long) ceil((float) length / 2.0);
	}

	loops = (gear_long) ceil((float) digest_length / (float) ops->digest_size);

	result = safe_emalloc(loops, ops->digest_size, 0);

	computed_salt = safe_emalloc(salt_len, 1, 4);
	memcpy(computed_salt, (unsigned char *) salt, salt_len);

	for (i = 1; i <= loops; i++) {
		/* digest = hash_hmac(salt + pack('N', i), password) { */

		/* pack("N", i) */
		computed_salt[salt_len] = (unsigned char) (i >> 24);
		computed_salt[salt_len + 1] = (unsigned char) ((i & 0xFF0000) >> 16);
		computed_salt[salt_len + 2] = (unsigned char) ((i & 0xFF00) >> 8);
		computed_salt[salt_len + 3] = (unsigned char) (i & 0xFF);

		hyss_hash_hmac_round(digest, ops, context, K1, computed_salt, (gear_long) salt_len + 4);
		hyss_hash_hmac_round(digest, ops, context, K2, digest, ops->digest_size);
		/* } */

		/* temp = digest */
		memcpy(temp, digest, ops->digest_size);

		/*
		 * Note that the loop starting at 1 is intentional, since we've already done
		 * the first round of the algorithm.
		 */
		for (j = 1; j < iterations; j++) {
			/* digest = hash_hmac(digest, password) { */
			hyss_hash_hmac_round(digest, ops, context, K1, digest, ops->digest_size);
			hyss_hash_hmac_round(digest, ops, context, K2, digest, ops->digest_size);
			/* } */
			/* temp ^= digest */
			hyss_hash_string_xor(temp, temp, digest, ops->digest_size);
		}
		/* result += temp */
		memcpy(result + ((i - 1) * ops->digest_size), temp, ops->digest_size);
	}
	/* Zero potentially sensitive variables */
	GEAR_SECURE_ZERO(K1, ops->block_size);
	GEAR_SECURE_ZERO(K2, ops->block_size);
	GEAR_SECURE_ZERO(computed_salt, salt_len + 4);
	efree(K1);
	efree(K2);
	efree(computed_salt);
	efree(context);
	efree(digest);
	efree(temp);

	returnval = gear_string_alloc(length, 0);
	if (raw_output) {
		memcpy(ZSTR_VAL(returnval), result, length);
	} else {
		hyss_hash_bin2hex(ZSTR_VAL(returnval), result, digest_length);
	}
	ZSTR_VAL(returnval)[length] = 0;
	efree(result);
	RETURN_NEW_STR(returnval);
}
/* }}} */

/* {{{ proto bool hash_equals(string known_string, string user_string)
   Compares two strings using the same time whether they're equal or not.
   A difference in length will leak */
HYSS_FUNCTION(hash_equals)
{
	zval *known_zval, *user_zval;
	char *known_str, *user_str;
	int result = 0;
	size_t j;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "zz", &known_zval, &user_zval) == FAILURE) {
		return;
	}

	/* We only allow comparing string to prevent unexpected results. */
	if (Z_TYPE_P(known_zval) != IS_STRING) {
		hyss_error_docref(NULL, E_WARNING, "Expected known_string to be a string, %s given", gear_zval_type_name(known_zval));
		RETURN_FALSE;
	}

	if (Z_TYPE_P(user_zval) != IS_STRING) {
		hyss_error_docref(NULL, E_WARNING, "Expected user_string to be a string, %s given", gear_zval_type_name(user_zval));
		RETURN_FALSE;
	}

	if (Z_STRLEN_P(known_zval) != Z_STRLEN_P(user_zval)) {
		RETURN_FALSE;
	}

	known_str = Z_STRVAL_P(known_zval);
	user_str = Z_STRVAL_P(user_zval);

	/* This is security sensitive code. Do not optimize this for speed. */
	for (j = 0; j < Z_STRLEN_P(known_zval); j++) {
		result |= known_str[j] ^ user_str[j];
	}

	RETURN_BOOL(0 == result);
}
/* }}} */

/* {{{ proto HashContext::__construct() */
static HYSS_METHOD(HashContext, __construct) {
	/* Normally unreachable as private/final */
	gear_throw_exception(gear_ce_error, "Illegal call to private/final constructor", 0);
}
/* }}} */

static const gear_function_entry hyss_hashcontext_methods[] = {
	HYSS_ME(HashContext, __construct, NULL, GEAR_ACC_PRIVATE | GEAR_ACC_CTOR)
	HYSS_FE_END
};

/* cAPI Housekeeping */

#define HYSS_HASH_HAVAL_REGISTER(p,b)	hyss_hash_register_algo("haval" #b "," #p , &hyss_hash_##p##haval##b##_ops);

#ifdef HYSS_MHASH_BC

#if 0
/* See #69823, we should not insert cAPI into capi_registry while doing startup */

HYSS_MINFO_FUNCTION(mhash)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "MHASH support", "Enabled");
	hyss_info_print_table_row(2, "MHASH API Version", "Emulated Support");
	hyss_info_print_table_end();
}

gear_capi_entry mhash_capi_entry = {
	STANDARD_CAPI_HEADER,
	"mhash",
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(mhash),
	HYSS_MHASH_VERSION,
	STANDARD_CAPI_PROPERTIES,
};
#endif

static void mhash_init(INIT_FUNC_ARGS)
{
	char buf[128];
	int len;
	int algo_number = 0;

	for (algo_number = 0; algo_number < MHASH_NUM_ALGOS; algo_number++) {
		struct mhash_bc_entry algorithm = mhash_to_hash[algo_number];
		if (algorithm.mhash_name == NULL) {
			continue;
		}

		len = slprintf(buf, 127, "MHASH_%s", algorithm.mhash_name);
		gear_register_long_constant(buf, len, algorithm.value, CONST_CS | CONST_PERSISTENT, capi_number);
	}

	/* TODO: this cause #69823 gear_register_internal_capi(&mhash_capi_entry); */
}

/* {{{ proto string mhash(int hash, string data [, string key])
   Hash data with hash */
HYSS_FUNCTION(mhash)
{
	zval *z_algorithm;
	gear_long algorithm;

	if (gear_parse_parameters(1, "z", &z_algorithm) == FAILURE) {
		return;
	}

	algorithm = zval_get_long(z_algorithm);

	/* need to convert the first parameter from int constant to string algorithm name */
	if (algorithm >= 0 && algorithm < MHASH_NUM_ALGOS) {
		struct mhash_bc_entry algorithm_lookup = mhash_to_hash[algorithm];
		if (algorithm_lookup.hash_name) {
			ZVAL_STRING(z_algorithm, algorithm_lookup.hash_name);
		}
	}

	if (GEAR_NUM_ARGS() == 3) {
		hyss_hash_do_hash_hmac(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0, 1);
	} else if (GEAR_NUM_ARGS() == 2) {
		hyss_hash_do_hash(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0, 1);
	} else {
		WRONG_PARAM_COUNT;
	}
}
/* }}} */

/* {{{ proto string mhash_get_hash_name(int hash)
   Gets the name of hash */
HYSS_FUNCTION(mhash_get_hash_name)
{
	gear_long algorithm;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &algorithm) == FAILURE) {
		return;
	}

	if (algorithm >= 0 && algorithm  < MHASH_NUM_ALGOS) {
		struct mhash_bc_entry algorithm_lookup = mhash_to_hash[algorithm];
		if (algorithm_lookup.mhash_name) {
			RETURN_STRING(algorithm_lookup.mhash_name);
		}
	}
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto int mhash_count(void)
   Gets the number of available hashes */
HYSS_FUNCTION(mhash_count)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	RETURN_LONG(MHASH_NUM_ALGOS - 1);
}
/* }}} */

/* {{{ proto int mhash_get_block_size(int hash)
   Gets the block size of hash */
HYSS_FUNCTION(mhash_get_block_size)
{
	gear_long algorithm;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &algorithm) == FAILURE) {
		return;
	}
	RETVAL_FALSE;

	if (algorithm >= 0 && algorithm  < MHASH_NUM_ALGOS) {
		struct mhash_bc_entry algorithm_lookup = mhash_to_hash[algorithm];
		if (algorithm_lookup.mhash_name) {
			const hyss_hash_ops *ops = hyss_hash_fetch_ops(algorithm_lookup.hash_name, strlen(algorithm_lookup.hash_name));
			if (ops) {
				RETVAL_LONG(ops->digest_size);
			}
		}
	}
}
/* }}} */

#define SALT_SIZE 8

/* {{{ proto string mhash_keygen_s2k(int hash, string input_password, string salt, int bytes)
   Generates a key using hash functions */
HYSS_FUNCTION(mhash_keygen_s2k)
{
	gear_long algorithm, l_bytes;
	int bytes;
	char *password, *salt;
	size_t password_len, salt_len;
	char padded_salt[SALT_SIZE];

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lssl", &algorithm, &password, &password_len, &salt, &salt_len, &l_bytes) == FAILURE) {
		return;
	}

	bytes = (int)l_bytes;
	if (bytes <= 0){
		hyss_error_docref(NULL, E_WARNING, "the byte parameter must be greater than 0");
		RETURN_FALSE;
	}

	salt_len = MIN(salt_len, SALT_SIZE);

	memcpy(padded_salt, salt, salt_len);
	if (salt_len < SALT_SIZE) {
		memset(padded_salt + salt_len, 0, SALT_SIZE - salt_len);
	}
	salt_len = SALT_SIZE;

	RETVAL_FALSE;
	if (algorithm >= 0 && algorithm < MHASH_NUM_ALGOS) {
		struct mhash_bc_entry algorithm_lookup = mhash_to_hash[algorithm];
		if (algorithm_lookup.mhash_name) {
			const hyss_hash_ops *ops = hyss_hash_fetch_ops(algorithm_lookup.hash_name, strlen(algorithm_lookup.hash_name));
			if (ops) {
				unsigned char null = '\0';
				void *context;
				char *key, *digest;
				int i = 0, j = 0;
				int block_size = ops->digest_size;
				int times = bytes / block_size;
				if (bytes % block_size  != 0) times++;

				context = emalloc(ops->context_size);
				ops->hash_init(context);

				key = ecalloc(1, times * block_size);
				digest = emalloc(ops->digest_size + 1);

				for (i = 0; i < times; i++) {
					ops->hash_init(context);

					for (j=0;j<i;j++) {
						ops->hash_update(context, &null, 1);
					}
					ops->hash_update(context, (unsigned char *)padded_salt, salt_len);
					ops->hash_update(context, (unsigned char *)password, password_len);
					ops->hash_final((unsigned char *)digest, context);
					memcpy( &key[i*block_size], digest, block_size);
				}

				RETVAL_STRINGL(key, bytes);
				GEAR_SECURE_ZERO(key, bytes);
				efree(digest);
				efree(context);
				efree(key);
			}
		}
	}
}
/* }}} */

#endif

/* ----------------------------------------------------------------------- */

/* {{{ hyss_hashcontext_create */
static gear_object* hyss_hashcontext_create(gear_class_entry *ce) {
	hyss_hashcontext_object *objval = gear_object_alloc(sizeof(hyss_hashcontext_object), ce);
	gear_object *zobj = &objval->std;

	gear_object_std_init(zobj, ce);
	object_properties_init(zobj, ce);
	zobj->handlers = &hyss_hashcontext_handlers;

	return zobj;
}
/* }}} */

/* {{{ hyss_hashcontext_dtor */
static void hyss_hashcontext_dtor(gear_object *obj) {
	hyss_hashcontext_object *hash = hyss_hashcontext_from_object(obj);

	/* Just in case the algo has internally allocated resources */
	if (hash->context) {
		unsigned char *dummy = emalloc(hash->ops->digest_size);
		hash->ops->hash_final(dummy, hash->context);
		efree(dummy);
		efree(hash->context);
		hash->context = NULL;
	}

	if (hash->key) {
		GEAR_SECURE_ZERO(hash->key, hash->ops->block_size);
		efree(hash->key);
		hash->key = NULL;
	}
}
/* }}} */

/* {{{ hyss_hashcontext_clone */
static gear_object *hyss_hashcontext_clone(zval *pzv) {
	hyss_hashcontext_object *oldobj = hyss_hashcontext_from_object(Z_OBJ_P(pzv));
	gear_object *znew = hyss_hashcontext_create(Z_OBJCE_P(pzv));
	hyss_hashcontext_object *newobj = hyss_hashcontext_from_object(znew);

	gear_objects_clone_members(znew, Z_OBJ_P(pzv));

	newobj->ops = oldobj->ops;
	newobj->options = oldobj->options;
	newobj->context = emalloc(newobj->ops->context_size);
	newobj->ops->hash_init(newobj->context);

	if (SUCCESS != newobj->ops->hash_copy(newobj->ops, oldobj->context, newobj->context)) {
		efree(newobj->context);
		newobj->context = NULL;
		return znew;
	}

	newobj->key = ecalloc(1, newobj->ops->block_size);
	if (oldobj->key) {
		memcpy(newobj->key, oldobj->key, newobj->ops->block_size);
	}

	return znew;
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(hash)
{
	gear_class_entry ce;

	gear_hash_init(&hyss_hash_hashtable, 35, NULL, NULL, 1);

	hyss_hash_register_algo("md2",			&hyss_hash_md2_ops);
	hyss_hash_register_algo("md4",			&hyss_hash_md4_ops);
	hyss_hash_register_algo("md5",			&hyss_hash_md5_ops);
	hyss_hash_register_algo("sha1",			&hyss_hash_sha1_ops);
	hyss_hash_register_algo("sha224",		&hyss_hash_sha224_ops);
	hyss_hash_register_algo("sha256",		&hyss_hash_sha256_ops);
	hyss_hash_register_algo("sha384",		&hyss_hash_sha384_ops);
	hyss_hash_register_algo("sha512/224",            &hyss_hash_sha512_224_ops);
	hyss_hash_register_algo("sha512/256",            &hyss_hash_sha512_256_ops);
	hyss_hash_register_algo("sha512",		&hyss_hash_sha512_ops);
	hyss_hash_register_algo("sha3-224",		&hyss_hash_sha3_224_ops);
	hyss_hash_register_algo("sha3-256",		&hyss_hash_sha3_256_ops);
	hyss_hash_register_algo("sha3-384",		&hyss_hash_sha3_384_ops);
	hyss_hash_register_algo("sha3-512",		&hyss_hash_sha3_512_ops);
	hyss_hash_register_algo("ripemd128",		&hyss_hash_ripemd128_ops);
	hyss_hash_register_algo("ripemd160",		&hyss_hash_ripemd160_ops);
	hyss_hash_register_algo("ripemd256",		&hyss_hash_ripemd256_ops);
	hyss_hash_register_algo("ripemd320",		&hyss_hash_ripemd320_ops);
	hyss_hash_register_algo("whirlpool",		&hyss_hash_whirlpool_ops);
	hyss_hash_register_algo("tiger128,3",	&hyss_hash_3tiger128_ops);
	hyss_hash_register_algo("tiger160,3",	&hyss_hash_3tiger160_ops);
	hyss_hash_register_algo("tiger192,3",	&hyss_hash_3tiger192_ops);
	hyss_hash_register_algo("tiger128,4",	&hyss_hash_4tiger128_ops);
	hyss_hash_register_algo("tiger160,4",	&hyss_hash_4tiger160_ops);
	hyss_hash_register_algo("tiger192,4",	&hyss_hash_4tiger192_ops);
	hyss_hash_register_algo("snefru",		&hyss_hash_snefru_ops);
	hyss_hash_register_algo("snefru256",		&hyss_hash_snefru_ops);
	hyss_hash_register_algo("gost",			&hyss_hash_gost_ops);
	hyss_hash_register_algo("gost-crypto",		&hyss_hash_gost_crypto_ops);
	hyss_hash_register_algo("adler32",		&hyss_hash_adler32_ops);
	hyss_hash_register_algo("crc32",			&hyss_hash_crc32_ops);
	hyss_hash_register_algo("crc32b",		&hyss_hash_crc32b_ops);
	hyss_hash_register_algo("fnv132",		&hyss_hash_fnv132_ops);
	hyss_hash_register_algo("fnv1a32",		&hyss_hash_fnv1a32_ops);
	hyss_hash_register_algo("fnv164",		&hyss_hash_fnv164_ops);
	hyss_hash_register_algo("fnv1a64",		&hyss_hash_fnv1a64_ops);
	hyss_hash_register_algo("joaat",			&hyss_hash_joaat_ops);

	HYSS_HASH_HAVAL_REGISTER(3,128);
	HYSS_HASH_HAVAL_REGISTER(3,160);
	HYSS_HASH_HAVAL_REGISTER(3,192);
	HYSS_HASH_HAVAL_REGISTER(3,224);
	HYSS_HASH_HAVAL_REGISTER(3,256);

	HYSS_HASH_HAVAL_REGISTER(4,128);
	HYSS_HASH_HAVAL_REGISTER(4,160);
	HYSS_HASH_HAVAL_REGISTER(4,192);
	HYSS_HASH_HAVAL_REGISTER(4,224);
	HYSS_HASH_HAVAL_REGISTER(4,256);

	HYSS_HASH_HAVAL_REGISTER(5,128);
	HYSS_HASH_HAVAL_REGISTER(5,160);
	HYSS_HASH_HAVAL_REGISTER(5,192);
	HYSS_HASH_HAVAL_REGISTER(5,224);
	HYSS_HASH_HAVAL_REGISTER(5,256);

	REGISTER_LONG_CONSTANT("HASH_HMAC",		HYSS_HASH_HMAC,	CONST_CS | CONST_PERSISTENT);

	INIT_CLASS_ENTRY(ce, "HashContext", hyss_hashcontext_methods);
	hyss_hashcontext_ce = gear_register_internal_class(&ce);
	hyss_hashcontext_ce->ce_flags |= GEAR_ACC_FINAL;
	hyss_hashcontext_ce->create_object = hyss_hashcontext_create;
	hyss_hashcontext_ce->serialize = gear_class_serialize_deny;
	hyss_hashcontext_ce->unserialize = gear_class_unserialize_deny;

	memcpy(&hyss_hashcontext_handlers, &std_object_handlers,
	       sizeof(gear_object_handlers));
	hyss_hashcontext_handlers.offset = XtOffsetOf(hyss_hashcontext_object, std);
	hyss_hashcontext_handlers.dtor_obj = hyss_hashcontext_dtor;
	hyss_hashcontext_handlers.clone_obj = hyss_hashcontext_clone;

#ifdef HYSS_MHASH_BC
	mhash_init(INIT_FUNC_ARGS_PASSTHRU);
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION(hash)
{
	gear_hash_destroy(&hyss_hash_hashtable);

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(hash)
{
	char buffer[2048];
	gear_string *str;
	char *s = buffer, *e = s + sizeof(buffer);

	GEAR_HASH_FOREACH_STR_KEY(&hyss_hash_hashtable, str) {
		s += slprintf(s, e - s, "%s ", ZSTR_VAL(str));
	} GEAR_HASH_FOREACH_END();
	*s = 0;

	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "hash support", "enabled");
	hyss_info_print_table_row(2, "Hashing Engines", buffer);
	hyss_info_print_table_end();

#ifdef HYSS_MHASH_BC
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "MHASH support", "Enabled");
	hyss_info_print_table_row(2, "MHASH API Version", "Emulated Support");
	hyss_info_print_table_end();
#endif

}
/* }}} */

/* {{{ arginfo */
#ifdef HYSS_HASH_MD5_NOT_IN_CORE
GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_md5, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_md5_file, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()
#endif

#ifdef HYSS_HASH_SHA1_NOT_IN_CORE
GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_sha1, 0, 0, 1)
	GEAR_ARG_INFO(0, str)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_sha1_file, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash, 0, 0, 2)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, data)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_file, 0, 0, 2)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_hmac, 0, 0, 3)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, data)
	GEAR_ARG_INFO(0, key)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_hmac_file, 0, 0, 3)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, key)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_init, 0, 0, 1)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, options)
	GEAR_ARG_INFO(0, key)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hash_update, 0)
	GEAR_ARG_INFO(0, context)
	GEAR_ARG_INFO(0, data)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_update_stream, 0, 0, 2)
	GEAR_ARG_INFO(0, context)
	GEAR_ARG_INFO(0, handle)
	GEAR_ARG_INFO(0, length)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_update_file, 0, 0, 2)
	GEAR_ARG_INFO(0, context)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, stream_context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_final, 0, 0, 1)
	GEAR_ARG_INFO(0, context)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hash_copy, 0)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hash_algos, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_pbkdf2, 0, 0, 4)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, password)
	GEAR_ARG_INFO(0, salt)
	GEAR_ARG_INFO(0, iterations)
	GEAR_ARG_INFO(0, length)
	GEAR_ARG_INFO(0, raw_output)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_hash_equals, 0)
	GEAR_ARG_INFO(0, known_string)
	GEAR_ARG_INFO(0, user_string)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_hash_hkdf, 0, 0, 2)
	GEAR_ARG_INFO(0, ikm)
	GEAR_ARG_INFO(0, algo)
	GEAR_ARG_INFO(0, length)
	GEAR_ARG_INFO(0, string)
	GEAR_ARG_INFO(0, salt)
GEAR_END_ARG_INFO()

/* BC Land */
#ifdef HYSS_MHASH_BC
GEAR_BEGIN_ARG_INFO(arginfo_mhash_get_block_size, 0)
	GEAR_ARG_INFO(0, hash)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_mhash_get_hash_name, 0)
	GEAR_ARG_INFO(0, hash)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_mhash_keygen_s2k, 0)
	GEAR_ARG_INFO(0, hash)
	GEAR_ARG_INFO(0, input_password)
	GEAR_ARG_INFO(0, salt)
	GEAR_ARG_INFO(0, bytes)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_mhash_count, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mhash, 0, 0, 2)
	GEAR_ARG_INFO(0, hash)
	GEAR_ARG_INFO(0, data)
	GEAR_ARG_INFO(0, key)
GEAR_END_ARG_INFO()
#endif

/* }}} */

/* {{{ hash_functions[]
 */
static const gear_function_entry hash_functions[] = {
	HYSS_FE(hash,									arginfo_hash)
	HYSS_FE(hash_file,								arginfo_hash_file)

	HYSS_FE(hash_hmac,								arginfo_hash_hmac)
	HYSS_FE(hash_hmac_file,							arginfo_hash_hmac_file)

	HYSS_FE(hash_init,								arginfo_hash_init)
	HYSS_FE(hash_update,								arginfo_hash_update)
	HYSS_FE(hash_update_stream,						arginfo_hash_update_stream)
	HYSS_FE(hash_update_file,						arginfo_hash_update_file)
	HYSS_FE(hash_final,								arginfo_hash_final)
	HYSS_FE(hash_copy,								arginfo_hash_copy)

	HYSS_FE(hash_algos,								arginfo_hash_algos)
	HYSS_FE(hash_hmac_algos,							arginfo_hash_algos)
	HYSS_FE(hash_pbkdf2,								arginfo_hash_pbkdf2)
	HYSS_FE(hash_equals,								arginfo_hash_equals)
	HYSS_FE(hash_hkdf,								arginfo_hash_hkdf)

	/* BC Land */
#ifdef HYSS_HASH_MD5_NOT_IN_CORE
	HYSS_NAMED_FE(md5, hyss_if_md5,					arginfo_hash_md5)
	HYSS_NAMED_FE(md5_file, hyss_if_md5_file,			arginfo_hash_md5_file)
#endif /* HYSS_HASH_MD5_NOT_IN_CORE */

#ifdef HYSS_HASH_SHA1_NOT_IN_CORE
	HYSS_NAMED_FE(sha1, hyss_if_sha1,					arginfo_hash_sha1)
	HYSS_NAMED_FE(sha1_file, hyss_if_sha1_file,		arginfo_hash_sha1_file)
#endif /* HYSS_HASH_SHA1_NOT_IN_CORE */

#ifdef HYSS_MHASH_BC
	HYSS_FE(mhash_keygen_s2k, arginfo_mhash_keygen_s2k)
	HYSS_FE(mhash_get_block_size, arginfo_mhash_get_block_size)
	HYSS_FE(mhash_get_hash_name, arginfo_mhash_get_hash_name)
	HYSS_FE(mhash_count, arginfo_mhash_count)
	HYSS_FE(mhash, arginfo_mhash)
#endif

	HYSS_FE_END
};
/* }}} */

/* {{{ hash_capi_entry
 */
gear_capi_entry hash_capi_entry = {
	STANDARD_CAPI_HEADER,
	HYSS_HASH_EXTNAME,
	hash_functions,
	HYSS_MINIT(hash),
	HYSS_MSHUTDOWN(hash),
	NULL, /* RINIT */
	NULL, /* RSHUTDOWN */
	HYSS_MINFO(hash),
	HYSS_HASH_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_HASH
GEAR_GET_CAPI(hash)
#endif

