/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_WHIRLPOOL_H
#define HYSS_HASH_WHIRLPOOL_H

/* WHIRLPOOL context */
typedef struct {
	uint64_t state[8];
	unsigned char bitlength[32];
	struct {
		int pos;
		int bits;
		unsigned char data[64];
	} buffer;
} HYSS_WHIRLPOOL_CTX;

HYSS_HASH_API void HYSS_WHIRLPOOLInit(HYSS_WHIRLPOOL_CTX *);
HYSS_HASH_API void HYSS_WHIRLPOOLUpdate(HYSS_WHIRLPOOL_CTX *, const unsigned char *, size_t);
HYSS_HASH_API void HYSS_WHIRLPOOLFinal(unsigned char[64], HYSS_WHIRLPOOL_CTX *);

#endif

