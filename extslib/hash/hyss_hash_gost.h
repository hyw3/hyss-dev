/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_GOST_H
#define HYSS_HASH_GOST_H

#include "extslib/standard/basic_functions.h"

/* GOST context */
typedef struct {
	uint32_t state[16];
	uint32_t count[2];
	unsigned char length;
	unsigned char buffer[32];
	const uint32_t (*tables)[4][256];
} HYSS_GOST_CTX;

HYSS_HASH_API void HYSS_GOSTInit(HYSS_GOST_CTX *);
HYSS_HASH_API void HYSS_GOSTUpdate(HYSS_GOST_CTX *, const unsigned char *, size_t);
HYSS_HASH_API void HYSS_GOSTFinal(unsigned char[64], HYSS_GOST_CTX *);

#endif

