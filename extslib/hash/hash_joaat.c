/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Implements Jenkins's one-at-a-time hashing algorithm as presented on
 * http://www.burtleburtle.net/bob/hash/doobs.html.
 */

#include "hyss_hash.h"
#include "hyss_hash_joaat.h"

const hyss_hash_ops hyss_hash_joaat_ops = {
	(hyss_hash_init_func_t) HYSS_JOAATInit,
	(hyss_hash_update_func_t) HYSS_JOAATUpdate,
	(hyss_hash_final_func_t) HYSS_JOAATFinal,
	(hyss_hash_copy_func_t) hyss_hash_copy,
	4,
	4,
	sizeof(HYSS_JOAAT_CTX),
	0
};

HYSS_HASH_API void HYSS_JOAATInit(HYSS_JOAAT_CTX *context)
{
	context->state = 0;
}

HYSS_HASH_API void HYSS_JOAATUpdate(HYSS_JOAAT_CTX *context, const unsigned char *input, unsigned int inputLen)
{
	context->state = joaat_buf((void *)input, inputLen, context->state);
}

HYSS_HASH_API void HYSS_JOAATFinal(unsigned char digest[4], HYSS_JOAAT_CTX * context)
{
#ifdef WORDS_BIGENDIAN
	memcpy(digest, &context->state, 4);
#else
	int i = 0;
	unsigned char *c = (unsigned char *) &context->state;

	for (i = 0; i < 4; i++) {
		digest[i] = c[3 - i];
	}
#endif
    context->state = 0;
}

/*
 * joaat_buf - perform a Jenkins's one-at-a-time hash on a buffer
 *
 * input:
 *  buf - start of buffer to hash
 *  len - length of buffer in octets
 *
 * returns:
 *  32 bit hash as a static hash type
 */
static uint32_t
joaat_buf(void *buf, size_t len, uint32_t hval)
{
    size_t i;
    unsigned char *input = (unsigned char *)buf;

    for (i = 0; i < len; i++) {
        hval += input[i];
        hval += (hval << 10);
        hval ^= (hval >> 6);
    }

    hval += (hval << 3);
    hval ^= (hval >> 11);
    hval += (hval << 15);

    return hval;
}

