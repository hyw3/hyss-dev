/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*  Based on the public domain algorithm found at
	http://www.isthe.com/chongo/tech/comp/fnv/index.html */

#include "hyss_hash.h"
#include "hyss_hash_fnv.h"

const hyss_hash_ops hyss_hash_fnv132_ops = {
	(hyss_hash_init_func_t) HYSS_FNV132Init,
	(hyss_hash_update_func_t) HYSS_FNV132Update,
	(hyss_hash_final_func_t) HYSS_FNV132Final,
	(hyss_hash_copy_func_t) hyss_hash_copy,
	4,
	4,
	sizeof(HYSS_FNV132_CTX),
	0
};

const hyss_hash_ops hyss_hash_fnv1a32_ops = {
	(hyss_hash_init_func_t) HYSS_FNV132Init,
	(hyss_hash_update_func_t) HYSS_FNV1a32Update,
 	(hyss_hash_final_func_t) HYSS_FNV132Final,
	(hyss_hash_copy_func_t) hyss_hash_copy,
	4,
	4,
	sizeof(HYSS_FNV132_CTX),
	0
};

const hyss_hash_ops hyss_hash_fnv164_ops = {
	(hyss_hash_init_func_t) HYSS_FNV164Init,
	(hyss_hash_update_func_t) HYSS_FNV164Update,
	(hyss_hash_final_func_t) HYSS_FNV164Final,
	(hyss_hash_copy_func_t) hyss_hash_copy,
	8,
	4,
	sizeof(HYSS_FNV164_CTX),
	0
};

const hyss_hash_ops hyss_hash_fnv1a64_ops = {
	(hyss_hash_init_func_t) HYSS_FNV164Init,
	(hyss_hash_update_func_t) HYSS_FNV1a64Update,
 	(hyss_hash_final_func_t) HYSS_FNV164Final,
	(hyss_hash_copy_func_t) hyss_hash_copy,
	8,
	4,
	sizeof(HYSS_FNV164_CTX),
	0
};

/* {{{ HYSS_FNV132Init
 * 32-bit FNV-1 hash initialisation
 */
HYSS_HASH_API void HYSS_FNV132Init(HYSS_FNV132_CTX *context)
{
	context->state = HYSS_FNV1_32_INIT;
}
/* }}} */

HYSS_HASH_API void HYSS_FNV132Update(HYSS_FNV132_CTX *context, const unsigned char *input,
		unsigned int inputLen)
{
	context->state = fnv_32_buf((void *)input, inputLen, context->state, 0);
}

HYSS_HASH_API void HYSS_FNV1a32Update(HYSS_FNV132_CTX *context, const unsigned char *input,
		unsigned int inputLen)
{
	context->state = fnv_32_buf((void *)input, inputLen, context->state, 1);
}

HYSS_HASH_API void HYSS_FNV132Final(unsigned char digest[4], HYSS_FNV132_CTX * context)
{
#ifdef WORDS_BIGENDIAN
	memcpy(digest, &context->state, 4);
#else
	int i = 0;
	unsigned char *c = (unsigned char *) &context->state;

	for (i = 0; i < 4; i++) {
		digest[i] = c[3 - i];
	}
#endif
}

/* {{{ HYSS_FNV164Init
 * 64-bit FNV-1 hash initialisation
 */
HYSS_HASH_API void HYSS_FNV164Init(HYSS_FNV164_CTX *context)
{
	context->state = HYSS_FNV1_64_INIT;
}
/* }}} */

HYSS_HASH_API void HYSS_FNV164Update(HYSS_FNV164_CTX *context, const unsigned char *input,
		unsigned int inputLen)
{
	context->state = fnv_64_buf((void *)input, inputLen, context->state, 0);
}

HYSS_HASH_API void HYSS_FNV1a64Update(HYSS_FNV164_CTX *context, const unsigned char *input,
		unsigned int inputLen)
{
	context->state = fnv_64_buf((void *)input, inputLen, context->state, 1);
}

HYSS_HASH_API void HYSS_FNV164Final(unsigned char digest[8], HYSS_FNV164_CTX * context)
{
#ifdef WORDS_BIGENDIAN
	memcpy(digest, &context->state, 8);
#else
	int i = 0;
	unsigned char *c = (unsigned char *) &context->state;

	for (i = 0; i < 8; i++) {
		digest[i] = c[7 - i];
	}
#endif
}


/*
 * fnv_32_buf - perform a 32 bit Fowler/Noll/Vo hash on a buffer
 *
 * input:
 *  buf - start of buffer to hash
 *  len - length of buffer in octets
 *  hval	- previous hash value or 0 if first call
 *  alternate - if > 0 use the alternate version
 *
 * returns:
 *  32 bit hash as a static hash type
 */
static uint32_t
fnv_32_buf(void *buf, size_t len, uint32_t hval, int alternate)
{
	unsigned char *bp = (unsigned char *)buf;   /* start of buffer */
	unsigned char *be = bp + len;	   /* beyond end of buffer */

	/*
	 * FNV-1 hash each octet in the buffer
	 */
	if (alternate == 0) {
		while (bp < be) {
			/* multiply by the 32 bit FNV magic prime mod 2^32 */
			hval *= HYSS_FNV_32_PRIME;

			/* xor the bottom with the current octet */
			hval ^= (uint32_t)*bp++;
		}
	} else {
		while (bp < be) {
			/* xor the bottom with the current octet */
			hval ^= (uint32_t)*bp++;

			/* multiply by the 32 bit FNV magic prime mod 2^32 */
			hval *= HYSS_FNV_32_PRIME;
		}
	}

	/* return our new hash value */
	return hval;
}

/*
 * fnv_64_buf - perform a 64 bit Fowler/Noll/Vo hash on a buffer
 *
 * input:
 *  buf - start of buffer to hash
 *  len - length of buffer in octets
 *  hval	- previous hash value or 0 if first call
 *  alternate - if > 0 use the alternate version
 *
 * returns:
 *  64 bit hash as a static hash type
 */
static uint64_t
fnv_64_buf(void *buf, size_t len, uint64_t hval, int alternate)
{
	unsigned char *bp = (unsigned char *)buf;   /* start of buffer */
	unsigned char *be = bp + len;	   /* beyond end of buffer */

	/*
	 * FNV-1 hash each octet of the buffer
	 */

	if (alternate == 0) {
		while (bp < be) {
			/* multiply by the 64 bit FNV magic prime mod 2^64 */
			hval *= HYSS_FNV_64_PRIME;

			/* xor the bottom with the current octet */
			hval ^= (uint64_t)*bp++;
		}
	 } else {
		while (bp < be) {
			/* xor the bottom with the current octet */
			hval ^= (uint64_t)*bp++;

			/* multiply by the 64 bit FNV magic prime mod 2^64 */
			hval *= HYSS_FNV_64_PRIME;
		 }
	}

	/* return our new hash value */
	return hval;
}

