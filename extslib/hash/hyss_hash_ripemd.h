/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_RIPEMD_H
#define HYSS_HASH_RIPEMD_H
#include "extslib/standard/basic_functions.h"

/* RIPEMD context. */
typedef struct {
	uint32_t state[4];		/* state (ABCD) */
	uint32_t count[2];		/* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];	/* input buffer */
} HYSS_RIPEMD128_CTX;

typedef struct {
	uint32_t state[5];		/* state (ABCD) */
	uint32_t count[2];		/* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];	/* input buffer */
} HYSS_RIPEMD160_CTX;

typedef struct {
	uint32_t state[8];		/* state (ABCD) */
	uint32_t count[2];		/* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];	/* input buffer */
} HYSS_RIPEMD256_CTX;

typedef struct {
	uint32_t state[10];		/* state (ABCD) */
	uint32_t count[2];		/* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];	/* input buffer */
} HYSS_RIPEMD320_CTX;

HYSS_HASH_API void HYSS_RIPEMD128Init(HYSS_RIPEMD128_CTX *);
HYSS_HASH_API void HYSS_RIPEMD128Update(HYSS_RIPEMD128_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_RIPEMD128Final(unsigned char[16], HYSS_RIPEMD128_CTX *);

HYSS_HASH_API void HYSS_RIPEMD160Init(HYSS_RIPEMD160_CTX *);
HYSS_HASH_API void HYSS_RIPEMD160Update(HYSS_RIPEMD160_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_RIPEMD160Final(unsigned char[20], HYSS_RIPEMD160_CTX *);

HYSS_HASH_API void HYSS_RIPEMD256Init(HYSS_RIPEMD256_CTX *);
HYSS_HASH_API void HYSS_RIPEMD256Update(HYSS_RIPEMD256_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_RIPEMD256Final(unsigned char[32], HYSS_RIPEMD256_CTX *);

HYSS_HASH_API void HYSS_RIPEMD320Init(HYSS_RIPEMD320_CTX *);
HYSS_HASH_API void HYSS_RIPEMD320Update(HYSS_RIPEMD320_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_RIPEMD320Final(unsigned char[40], HYSS_RIPEMD320_CTX *);

#endif /* HYSS_HASH_RIPEMD_H */
