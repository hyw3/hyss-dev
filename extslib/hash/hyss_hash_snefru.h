/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_SNEFRU_H
#define HYSS_HASH_SNEFRU_H

/* SNEFRU-2.5a with 8 passes and 256 bit hash output
 * AKA "Xerox Secure Hash Function"
 */

#include "extslib/standard/basic_functions.h"

/* SNEFRU context */
typedef struct {
	uint32_t state[16];
	uint32_t count[2];
	unsigned char length;
	unsigned char buffer[32];
} HYSS_SNEFRU_CTX;

HYSS_HASH_API void HYSS_SNEFRUInit(HYSS_SNEFRU_CTX *);
HYSS_HASH_API void HYSS_SNEFRUUpdate(HYSS_SNEFRU_CTX *, const unsigned char *, size_t);
HYSS_HASH_API void HYSS_SNEFRUFinal(unsigned char[32], HYSS_SNEFRU_CTX *);

#endif

