/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_SHA_H
#define HYSS_HASH_SHA_H

/* When SHA is removed from Core,
	the extslib/standard/sha1.c file can be removed
	and the extslib/standard/sha1.h file can be reduced to:
		#define HYSS_HASH_SHA1_NOT_IN_CORE
		#include "extslib/hash/hyss_hash_sha.h"
	Don't forget to remove sha1() and sha1_file() from basic_functions.c
 */
#include "extslib/standard/sha1.h"
#include "extslib/standard/basic_functions.h"

#ifdef HYSS_HASH_SHA1_NOT_IN_CORE

/* SHA1 context. */
typedef struct {
	uint32_t state[5];		/* state (ABCD) */
	uint32_t count[2];		/* number of bits, modulo 2^64 */
	unsigned char buffer[64];	/* input buffer */
} HYSS_SHA1_CTX;

HYSS_HASH_API void HYSS_SHA1Init(HYSS_SHA1_CTX *);
HYSS_HASH_API void HYSS_SHA1Update(HYSS_SHA1_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_SHA1Final(unsigned char[20], HYSS_SHA1_CTX *);

HYSS_FUNCTION(sha1);
HYSS_FUNCTION(sha1_file);

#endif /* HYSS_HASH_SHA1_NOT_IN_CORE */

/* SHA224 context. */
typedef struct {
	uint32_t state[8];		/* state */
	uint32_t count[2];		/* number of bits, modulo 2^64 */
	unsigned char buffer[64];	/* input buffer */
} HYSS_SHA224_CTX;

HYSS_HASH_API void HYSS_SHA224Init(HYSS_SHA224_CTX *);
HYSS_HASH_API void HYSS_SHA224Update(HYSS_SHA224_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_SHA224Final(unsigned char[28], HYSS_SHA224_CTX *);

/* SHA256 context. */
typedef struct {
	uint32_t state[8];		/* state */
	uint32_t count[2];		/* number of bits, modulo 2^64 */
	unsigned char buffer[64];	/* input buffer */
} HYSS_SHA256_CTX;

HYSS_HASH_API void HYSS_SHA256Init(HYSS_SHA256_CTX *);
HYSS_HASH_API void HYSS_SHA256Update(HYSS_SHA256_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_SHA256Final(unsigned char[32], HYSS_SHA256_CTX *);

/* SHA384 context */
typedef struct {
	uint64_t state[8];	/* state */
	uint64_t count[2];	/* number of bits, modulo 2^128 */
	unsigned char buffer[128];	/* input buffer */
} HYSS_SHA384_CTX;

HYSS_HASH_API void HYSS_SHA384Init(HYSS_SHA384_CTX *);
HYSS_HASH_API void HYSS_SHA384Update(HYSS_SHA384_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_SHA384Final(unsigned char[48], HYSS_SHA384_CTX *);

/* SHA512 context */
typedef struct {
	uint64_t state[8];	/* state */
	uint64_t count[2];	/* number of bits, modulo 2^128 */
	unsigned char buffer[128];	/* input buffer */
} HYSS_SHA512_CTX;

HYSS_HASH_API void HYSS_SHA512Init(HYSS_SHA512_CTX *);
HYSS_HASH_API void HYSS_SHA512Update(HYSS_SHA512_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_SHA512Final(unsigned char[64], HYSS_SHA512_CTX *);

HYSS_HASH_API void HYSS_SHA512_256Init(HYSS_SHA512_CTX *);
#define HYSS_SHA512_256Update HYSS_SHA512Update
HYSS_HASH_API void HYSS_SHA512_256Final(unsigned char[32], HYSS_SHA512_CTX *);

HYSS_HASH_API void HYSS_SHA512_224Init(HYSS_SHA512_CTX *);
#define HYSS_SHA512_224Update HYSS_SHA512Update
HYSS_HASH_API void HYSS_SHA512_224Final(unsigned char[28], HYSS_SHA512_CTX *);

#endif /* HYSS_HASH_SHA_H */
