/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_HAVAL_H
#define HYSS_HASH_HAVAL_H

#include "extslib/standard/basic_functions.h"
/* HAVAL context. */
typedef struct {
	uint32_t state[8];
	uint32_t count[2];
	unsigned char buffer[128];

	char passes;
	short output;
	void (*Transform)(uint32_t state[8], const unsigned char block[128]);
} HYSS_HAVAL_CTX;

#define HYSS_HASH_HAVAL_INIT_DECL(p,b)	HYSS_HASH_API void HYSS_##p##HAVAL##b##Init(HYSS_HAVAL_CTX *); \
										HYSS_HASH_API void HYSS_HAVAL##b##Final(unsigned char*, HYSS_HAVAL_CTX *);

HYSS_HASH_API void HYSS_HAVALUpdate(HYSS_HAVAL_CTX *, const unsigned char *, unsigned int);

HYSS_HASH_HAVAL_INIT_DECL(3,128)
HYSS_HASH_HAVAL_INIT_DECL(3,160)
HYSS_HASH_HAVAL_INIT_DECL(3,192)
HYSS_HASH_HAVAL_INIT_DECL(3,224)
HYSS_HASH_HAVAL_INIT_DECL(3,256)

HYSS_HASH_HAVAL_INIT_DECL(4,128)
HYSS_HASH_HAVAL_INIT_DECL(4,160)
HYSS_HASH_HAVAL_INIT_DECL(4,192)
HYSS_HASH_HAVAL_INIT_DECL(4,224)
HYSS_HASH_HAVAL_INIT_DECL(4,256)

HYSS_HASH_HAVAL_INIT_DECL(5,128)
HYSS_HASH_HAVAL_INIT_DECL(5,160)
HYSS_HASH_HAVAL_INIT_DECL(5,192)
HYSS_HASH_HAVAL_INIT_DECL(5,224)
HYSS_HASH_HAVAL_INIT_DECL(5,256)

#endif
