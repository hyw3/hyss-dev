/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss_hash.h"
#include "hyss_hash_adler32.h"

HYSS_HASH_API void HYSS_ADLER32Init(HYSS_ADLER32_CTX *context)
{
	context->state = 1;
}

HYSS_HASH_API void HYSS_ADLER32Update(HYSS_ADLER32_CTX *context, const unsigned char *input, size_t len)
{
	uint32_t i, s[2];

	s[0] = context->state & 0xffff;
	s[1] = (context->state >> 16) & 0xffff;
	for (i = 0; i < len; ++i) {
		s[0] += input[i];
		s[1] += s[0];
		if (s[1]>=0x7fffffff)
		{
			s[0] = s[0] % 65521;
			s[1] = s[1] % 65521;
		}
	}
	s[0] = s[0] % 65521;
	s[1] = s[1] % 65521;
	context->state = s[0] + (s[1] << 16);
}

HYSS_HASH_API void HYSS_ADLER32Final(unsigned char digest[4], HYSS_ADLER32_CTX *context)
{
	digest[0] = (unsigned char) ((context->state >> 24) & 0xff);
	digest[1] = (unsigned char) ((context->state >> 16) & 0xff);
	digest[2] = (unsigned char) ((context->state >> 8) & 0xff);
	digest[3] = (unsigned char) (context->state & 0xff);
	context->state = 0;
}

HYSS_HASH_API int HYSS_ADLER32Copy(const hyss_hash_ops *ops, HYSS_ADLER32_CTX *orig_context, HYSS_ADLER32_CTX *copy_context)
{
	copy_context->state = orig_context->state;
	return SUCCESS;
}

const hyss_hash_ops hyss_hash_adler32_ops = {
	(hyss_hash_init_func_t) HYSS_ADLER32Init,
	(hyss_hash_update_func_t) HYSS_ADLER32Update,
	(hyss_hash_final_func_t) HYSS_ADLER32Final,
	(hyss_hash_copy_func_t) HYSS_ADLER32Copy,
	4, /* what to say here? */
	4,
	sizeof(HYSS_ADLER32_CTX),
	0
};

