/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss_hash.h"
#include "hyss_hash_crc32.h"
#include "hyss_hash_crc32_tables.h"

HYSS_HASH_API void HYSS_CRC32Init(HYSS_CRC32_CTX *context)
{
	context->state = ~0;
}

HYSS_HASH_API void HYSS_CRC32Update(HYSS_CRC32_CTX *context, const unsigned char *input, size_t len)
{
	size_t i;

	for (i = 0; i < len; ++i) {
		context->state = (context->state << 8) ^ crc32_table[(context->state >> 24) ^ (input[i] & 0xff)];
	}
}

HYSS_HASH_API void HYSS_CRC32BUpdate(HYSS_CRC32_CTX *context, const unsigned char *input, size_t len)
{
	size_t i;

	for (i = 0; i < len; ++i) {
		context->state = (context->state >> 8) ^ crc32b_table[(context->state ^ input[i]) & 0xff];
	}
}

HYSS_HASH_API void HYSS_CRC32Final(unsigned char digest[4], HYSS_CRC32_CTX *context)
{
	context->state=~context->state;
	digest[3] = (unsigned char) ((context->state >> 24) & 0xff);
	digest[2] = (unsigned char) ((context->state >> 16) & 0xff);
	digest[1] = (unsigned char) ((context->state >> 8) & 0xff);
	digest[0] = (unsigned char) (context->state & 0xff);
	context->state = 0;
}

HYSS_HASH_API void HYSS_CRC32BFinal(unsigned char digest[4], HYSS_CRC32_CTX *context)
{
	context->state=~context->state;
	digest[0] = (unsigned char) ((context->state >> 24) & 0xff);
	digest[1] = (unsigned char) ((context->state >> 16) & 0xff);
	digest[2] = (unsigned char) ((context->state >> 8) & 0xff);
	digest[3] = (unsigned char) (context->state & 0xff);
	context->state = 0;
}

HYSS_HASH_API int HYSS_CRC32Copy(const hyss_hash_ops *ops, HYSS_CRC32_CTX *orig_context, HYSS_CRC32_CTX *copy_context)
{
	copy_context->state = orig_context->state;
	return SUCCESS;
}

const hyss_hash_ops hyss_hash_crc32_ops = {
	(hyss_hash_init_func_t) HYSS_CRC32Init,
	(hyss_hash_update_func_t) HYSS_CRC32Update,
	(hyss_hash_final_func_t) HYSS_CRC32Final,
	(hyss_hash_copy_func_t) HYSS_CRC32Copy,
	4, /* what to say here? */
	4,
	sizeof(HYSS_CRC32_CTX),
	0
};

const hyss_hash_ops hyss_hash_crc32b_ops = {
	(hyss_hash_init_func_t) HYSS_CRC32Init,
	(hyss_hash_update_func_t) HYSS_CRC32BUpdate,
	(hyss_hash_final_func_t) HYSS_CRC32BFinal,
	(hyss_hash_copy_func_t) HYSS_CRC32Copy,
	4, /* what to say here? */
	4,
	sizeof(HYSS_CRC32_CTX),
	0
};

