/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_TIGER_H
#define HYSS_HASH_TIGER_H

/* TIGER context */
typedef struct {
	uint64_t state[3];
	uint64_t passed;
	unsigned char buffer[64];
	unsigned int passes:1;
	unsigned int length:7;
} HYSS_TIGER_CTX;

HYSS_HASH_API void HYSS_3TIGERInit(HYSS_TIGER_CTX *context);
HYSS_HASH_API void HYSS_4TIGERInit(HYSS_TIGER_CTX *context);
HYSS_HASH_API void HYSS_TIGERUpdate(HYSS_TIGER_CTX *context, const unsigned char *input, size_t len);
HYSS_HASH_API void HYSS_TIGER128Final(unsigned char digest[16], HYSS_TIGER_CTX *context);
HYSS_HASH_API void HYSS_TIGER160Final(unsigned char digest[20], HYSS_TIGER_CTX *context);
HYSS_HASH_API void HYSS_TIGER192Final(unsigned char digest[24], HYSS_TIGER_CTX *context);

#endif

