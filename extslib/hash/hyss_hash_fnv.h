/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_FNV_H
#define HYSS_HASH_FNV_H

#define HYSS_FNV1_32_INIT ((uint32_t)0x811c9dc5)
#define HYSS_FNV1_32A_INIT HYSS_FNV1_32_INIT

#define HYSS_FNV_32_PRIME ((uint32_t)0x01000193)

#define HYSS_FNV1_64_INIT ((uint64_t)0xcbf29ce484222325ULL)
#define HYSS_FNV1A_64_INIT FNV1_64_INIT

#define HYSS_FNV_64_PRIME ((uint64_t)0x100000001b3ULL)


/*
 * hash types
 */
enum hyss_fnv_type {
	HYSS_FNV_NONE  = 0,	/* invalid FNV hash type */
	HYSS_FNV0_32   = 1,	/* FNV-0 32 bit hash */
	HYSS_FNV1_32   = 2,	/* FNV-1 32 bit hash */
	HYSS_FNV1a_32  = 3,	/* FNV-1a 32 bit hash */
	HYSS_FNV0_64   = 4,	/* FNV-0 64 bit hash */
	HYSS_FNV1_64   = 5,	/* FNV-1 64 bit hash */
	HYSS_FNV1a_64  = 6,	/* FNV-1a 64 bit hash */
};

typedef struct {
	uint32_t state;
} HYSS_FNV132_CTX;

typedef struct {
	uint64_t state;
} HYSS_FNV164_CTX;


HYSS_HASH_API void HYSS_FNV132Init(HYSS_FNV132_CTX *context);
HYSS_HASH_API void HYSS_FNV132Update(HYSS_FNV132_CTX *context, const unsigned char *input, unsigned int inputLen);
HYSS_HASH_API void HYSS_FNV1a32Update(HYSS_FNV132_CTX *context, const unsigned char *input, unsigned int inputLen);
HYSS_HASH_API void HYSS_FNV132Final(unsigned char digest[16], HYSS_FNV132_CTX * context);

HYSS_HASH_API void HYSS_FNV164Init(HYSS_FNV164_CTX *context);
HYSS_HASH_API void HYSS_FNV164Update(HYSS_FNV164_CTX *context, const unsigned char *input, unsigned int inputLen);
HYSS_HASH_API void HYSS_FNV1a64Update(HYSS_FNV164_CTX *context, const unsigned char *input, unsigned int inputLen);
HYSS_HASH_API void HYSS_FNV164Final(unsigned char digest[16], HYSS_FNV164_CTX * context);

static uint32_t fnv_32_buf(void *buf, size_t len, uint32_t hval, int alternate);
static uint64_t fnv_64_buf(void *buf, size_t len, uint64_t hval, int alternate);

#endif

