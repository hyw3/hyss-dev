/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_MD_H
#define HYSS_HASH_MD_H

/* When SHA is removed from Core,
    the extslib/standard/sha1.c file can be removed
    and the extslib/standard/sha1.h file can be reduced to:
        #define HYSS_HASH_SHA1_NOT_IN_CORE
        #include "extslib/hash/hyss_hash_sha.h"
	Don't forget to remove md5() and md5_file() entries from basic_functions.c
 */

#include "extslib/standard/md5.h"

#ifdef HYSS_HASH_MD5_NOT_IN_CORE
/* MD5.H - header file for MD5C.C
 */

/* Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
   rights reserved.

   License to copy and use this software is granted provided that it
   is identified as the "RSA Data Security, Inc. MD5 Message-Digest
   Algorithm" in all material mentioning or referencing this software
   or this function.

   License is also granted to make and use derivative works provided
   that such works are identified as "derived from the RSA Data
   Security, Inc. MD5 Message-Digest Algorithm" in all material
   mentioning or referencing the derived work.

   RSA Data Security, Inc. makes no representations concerning either
   the merchantability of this software or the suitability of this
   software for any particular purpose. It is provided "as is"
   without express or implied warranty of any kind.

   These notices must be retained in any copies of any part of this
   documentation and/or software.
 */

/* MD5 context. */
typedef struct {
	uint32_t state[4];		/* state (ABCD) */
	uint32_t count[2];		/* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];	/* input buffer */
} HYSS_MD5_CTX;

HYSS_HASH_API void make_digest(char *md5str, unsigned char *digest);
HYSS_HASH_API void HYSS_MD5Init(HYSS_MD5_CTX *);
HYSS_HASH_API void HYSS_MD5Update(HYSS_MD5_CTX *, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_MD5Final(unsigned char[16], HYSS_MD5_CTX *);

HYSS_NAMED_FUNCTION(hyss_if_md5);
HYSS_NAMED_FUNCTION(hyss_if_md5_file);
#endif /* HYSS_HASH_MD5_NOT_IN_CORE */

/* MD4 context */
typedef struct {
	uint32_t state[4];
	uint32_t count[2];
	unsigned char buffer[64];
} HYSS_MD4_CTX;

HYSS_HASH_API void HYSS_MD4Init(HYSS_MD4_CTX *);
HYSS_HASH_API void HYSS_MD4Update(HYSS_MD4_CTX *context, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_MD4Final(unsigned char[16], HYSS_MD4_CTX *);

/* MD2 context */
typedef struct {
	unsigned char state[48];
	unsigned char checksum[16];
	unsigned char buffer[16];
	char in_buffer;
} HYSS_MD2_CTX;

HYSS_HASH_API void HYSS_MD2Init(HYSS_MD2_CTX *context);
HYSS_HASH_API void HYSS_MD2Update(HYSS_MD2_CTX *context, const unsigned char *, unsigned int);
HYSS_HASH_API void HYSS_MD2Final(unsigned char[16], HYSS_MD2_CTX *);

#endif
