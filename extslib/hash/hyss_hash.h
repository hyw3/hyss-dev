/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_H
#define HYSS_HASH_H

#include "hyss.h"

#define HYSS_HASH_EXTNAME	"hash"
#define HYSS_HASH_VERSION	HYSS_VERSION
#define HYSS_MHASH_VERSION	HYSS_VERSION

#define HYSS_HASH_HMAC		0x0001

#define L64 INT64_C

typedef void (*hyss_hash_init_func_t)(void *context);
typedef void (*hyss_hash_update_func_t)(void *context, const unsigned char *buf, unsigned int count);
typedef void (*hyss_hash_final_func_t)(unsigned char *digest, void *context);
typedef int  (*hyss_hash_copy_func_t)(const void *ops, void *orig_context, void *dest_context);

typedef struct _hyss_hash_ops {
	hyss_hash_init_func_t hash_init;
	hyss_hash_update_func_t hash_update;
	hyss_hash_final_func_t hash_final;
	hyss_hash_copy_func_t hash_copy;

	int digest_size;
	int block_size;
	int context_size;
	unsigned is_crypto: 1;
} hyss_hash_ops;

typedef struct _hyss_hashcontext_object {
	const hyss_hash_ops *ops;
	void *context;

	gear_long options;
	unsigned char *key;

	gear_object std;
} hyss_hashcontext_object;

static inline hyss_hashcontext_object *hyss_hashcontext_from_object(gear_object *obj) {
	return ((hyss_hashcontext_object*)(obj + 1)) - 1;
}

extern const hyss_hash_ops hyss_hash_md2_ops;
extern const hyss_hash_ops hyss_hash_md4_ops;
extern const hyss_hash_ops hyss_hash_md5_ops;
extern const hyss_hash_ops hyss_hash_sha1_ops;
extern const hyss_hash_ops hyss_hash_sha224_ops;
extern const hyss_hash_ops hyss_hash_sha256_ops;
extern const hyss_hash_ops hyss_hash_sha384_ops;
extern const hyss_hash_ops hyss_hash_sha512_ops;
extern const hyss_hash_ops hyss_hash_sha512_256_ops;
extern const hyss_hash_ops hyss_hash_sha512_224_ops;
extern const hyss_hash_ops hyss_hash_sha3_224_ops;
extern const hyss_hash_ops hyss_hash_sha3_256_ops;
extern const hyss_hash_ops hyss_hash_sha3_384_ops;
extern const hyss_hash_ops hyss_hash_sha3_512_ops;
extern const hyss_hash_ops hyss_hash_ripemd128_ops;
extern const hyss_hash_ops hyss_hash_ripemd160_ops;
extern const hyss_hash_ops hyss_hash_ripemd256_ops;
extern const hyss_hash_ops hyss_hash_ripemd320_ops;
extern const hyss_hash_ops hyss_hash_whirlpool_ops;
extern const hyss_hash_ops hyss_hash_3tiger128_ops;
extern const hyss_hash_ops hyss_hash_3tiger160_ops;
extern const hyss_hash_ops hyss_hash_3tiger192_ops;
extern const hyss_hash_ops hyss_hash_4tiger128_ops;
extern const hyss_hash_ops hyss_hash_4tiger160_ops;
extern const hyss_hash_ops hyss_hash_4tiger192_ops;
extern const hyss_hash_ops hyss_hash_snefru_ops;
extern const hyss_hash_ops hyss_hash_gost_ops;
extern const hyss_hash_ops hyss_hash_gost_crypto_ops;
extern const hyss_hash_ops hyss_hash_adler32_ops;
extern const hyss_hash_ops hyss_hash_crc32_ops;
extern const hyss_hash_ops hyss_hash_crc32b_ops;
extern const hyss_hash_ops hyss_hash_fnv132_ops;
extern const hyss_hash_ops hyss_hash_fnv1a32_ops;
extern const hyss_hash_ops hyss_hash_fnv164_ops;
extern const hyss_hash_ops hyss_hash_fnv1a64_ops;
extern const hyss_hash_ops hyss_hash_joaat_ops;

#define HYSS_HASH_HAVAL_OPS(p,b)	extern const hyss_hash_ops hyss_hash_##p##haval##b##_ops;

HYSS_HASH_HAVAL_OPS(3,128)
HYSS_HASH_HAVAL_OPS(3,160)
HYSS_HASH_HAVAL_OPS(3,192)
HYSS_HASH_HAVAL_OPS(3,224)
HYSS_HASH_HAVAL_OPS(3,256)

HYSS_HASH_HAVAL_OPS(4,128)
HYSS_HASH_HAVAL_OPS(4,160)
HYSS_HASH_HAVAL_OPS(4,192)
HYSS_HASH_HAVAL_OPS(4,224)
HYSS_HASH_HAVAL_OPS(4,256)

HYSS_HASH_HAVAL_OPS(5,128)
HYSS_HASH_HAVAL_OPS(5,160)
HYSS_HASH_HAVAL_OPS(5,192)
HYSS_HASH_HAVAL_OPS(5,224)
HYSS_HASH_HAVAL_OPS(5,256)

extern gear_capi_entry hash_capi_entry;
#define hyssext_hash_ptr &hash_capi_entry

#ifdef HYSS_WIN32
#	define HYSS_HASH_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define HYSS_HASH_API __attribute__ ((visibility("default")))
#else
#	define HYSS_HASH_API
#endif

#ifdef ZTS
#include "hypbc.h"
#endif

HYSS_FUNCTION(hash);
HYSS_FUNCTION(hash_file);
HYSS_FUNCTION(hash_hkdf);
HYSS_FUNCTION(hash_hmac);
HYSS_FUNCTION(hash_hmac_file);
HYSS_FUNCTION(hash_init);
HYSS_FUNCTION(hash_update);
HYSS_FUNCTION(hash_update_stream);
HYSS_FUNCTION(hash_update_file);
HYSS_FUNCTION(hash_final);
HYSS_FUNCTION(hash_algos);
HYSS_FUNCTION(hash_pbkdf2);
HYSS_FUNCTION(hash_equals);

extern HYSS_HASH_API gear_class_entry *hyss_hashcontext_ce;
HYSS_HASH_API const hyss_hash_ops *hyss_hash_fetch_ops(const char *algo, size_t algo_len);
HYSS_HASH_API void hyss_hash_register_algo(const char *algo, const hyss_hash_ops *ops);
HYSS_HASH_API int hyss_hash_copy(const void *ops, void *orig_context, void *dest_context);

static inline void hyss_hash_bin2hex(char *out, const unsigned char *in, int in_len)
{
	static const char hexits[17] = "0123456789abcdef";
	int i;

	for(i = 0; i < in_len; i++) {
		out[i * 2]       = hexits[in[i] >> 4];
		out[(i * 2) + 1] = hexits[in[i] &  0x0F];
	}
}

#endif	/* HYSS_HASH_H */

