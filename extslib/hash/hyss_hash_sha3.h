/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_SHA3_H
#define HYSS_HASH_SHA3_H

#include "hyss.h"

typedef struct {
#ifdef HAVE_SLOW_HASH3
	unsigned char state[200]; // 5 * 5 * sizeof(uint64)
	uint32_t pos;
#else
	void *hashinstance;
#endif
} HYSS_SHA3_CTX;

typedef HYSS_SHA3_CTX HYSS_SHA3_224_CTX;
typedef HYSS_SHA3_CTX HYSS_SHA3_256_CTX;
typedef HYSS_SHA3_CTX HYSS_SHA3_384_CTX;
typedef HYSS_SHA3_CTX HYSS_SHA3_512_CTX;

HYSS_HASH_API void HYSS_SHA3224Init(HYSS_SHA3_224_CTX*);
HYSS_HASH_API void HYSS_SHA3224Update(HYSS_SHA3_224_CTX*, const unsigned char*, unsigned int);
HYSS_HASH_API void HYSS_SAH3224Final(unsigned char[32], HYSS_SHA3_224_CTX*);

HYSS_HASH_API void HYSS_SHA3256Init(HYSS_SHA3_256_CTX*);
HYSS_HASH_API void HYSS_SHA3256Update(HYSS_SHA3_256_CTX*, const unsigned char*, unsigned int);
HYSS_HASH_API void HYSS_SAH3256Final(unsigned char[32], HYSS_SHA3_256_CTX*);

HYSS_HASH_API void HYSS_SHA3384Init(HYSS_SHA3_384_CTX*);
HYSS_HASH_API void HYSS_SHA3384Update(HYSS_SHA3_384_CTX*, const unsigned char*, unsigned int);
HYSS_HASH_API void HYSS_SAH3384Final(unsigned char[32], HYSS_SHA3_384_CTX*);

HYSS_HASH_API void HYSS_SHA3512Init(HYSS_SHA3_512_CTX*);
HYSS_HASH_API void HYSS_SHA3512Update(HYSS_SHA3_512_CTX*, const unsigned char*, unsigned int);
HYSS_HASH_API void HYSS_SAH3512Final(unsigned char[32], HYSS_SHA3_512_CTX*);

#endif

