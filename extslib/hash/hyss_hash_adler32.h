/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_ADLER32_H
#define HYSS_HASH_ADLER32_H

#include "extslib/standard/basic_functions.h"

typedef struct {
	uint32_t state;
} HYSS_ADLER32_CTX;

HYSS_HASH_API void HYSS_ADLER32Init(HYSS_ADLER32_CTX *context);
HYSS_HASH_API void HYSS_ADLER32Update(HYSS_ADLER32_CTX *context, const unsigned char *input, size_t len);
HYSS_HASH_API void HYSS_ADLER32Final(unsigned char digest[4], HYSS_ADLER32_CTX *context);
HYSS_HASH_API int HYSS_ADLER32Copy(const hyss_hash_ops *ops, HYSS_ADLER32_CTX *orig_context, HYSS_ADLER32_CTX *copy_context);

#endif

