/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_HASH_JOAAT_H
#define HYSS_HASH_JOAAT_H

typedef struct {
	uint32_t state;
} HYSS_JOAAT_CTX;

HYSS_HASH_API void HYSS_JOAATInit(HYSS_JOAAT_CTX *context);
HYSS_HASH_API void HYSS_JOAATUpdate(HYSS_JOAAT_CTX *context, const unsigned char *input, unsigned int inputLen);
HYSS_HASH_API void HYSS_JOAATFinal(unsigned char digest[16], HYSS_JOAAT_CTX * context);

static uint32_t joaat_buf(void *buf, size_t len, uint32_t hval);

#endif

