/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "pdo/hyss_pdo.h"
#include "pdo/hyss_pdo_driver.h"
#include "hyss_pdo_pgsql.h"
#include "hyss_pdo_pgsql_int.h"

#ifdef HAVE_PG_CONFIG_H
#undef PACKAGE_BUGREPORT
#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION
#include <pg_config.h>
#endif

/* {{{ pdo_pgsql_functions[] */
static const gear_function_entry pdo_pgsql_functions[] = {
	HYSS_FE_END
};
/* }}} */

/* {{{ pdo_sqlite_deps
 */
static const gear_capi_dep pdo_pgsql_deps[] = {
	GEAR_CAPI_REQUIRED("pdo")
	GEAR_CAPI_END
};
/* }}} */

/* {{{ pdo_pgsql_capi_entry */
gear_capi_entry pdo_pgsql_capi_entry = {
	STANDARD_CAPI_HEADER_EX, NULL,
	pdo_pgsql_deps,
	"pdo_pgsql",
	pdo_pgsql_functions,
	HYSS_MINIT(pdo_pgsql),
	HYSS_MSHUTDOWN(pdo_pgsql),
	NULL,
	NULL,
	HYSS_MINFO(pdo_pgsql),
	HYSS_PDO_PGSQL_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_PDO_PGSQL
GEAR_GET_CAPI(pdo_pgsql)
#endif

/* true global environment */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(pdo_pgsql)
{
	REGISTER_PDO_CLASS_CONST_LONG("PGSQL_ATTR_DISABLE_PREPARES", PDO_PGSQL_ATTR_DISABLE_PREPARES);
	REGISTER_PDO_CLASS_CONST_LONG("PGSQL_TRANSACTION_IDLE", (gear_long)PGSQL_TRANSACTION_IDLE);
	REGISTER_PDO_CLASS_CONST_LONG("PGSQL_TRANSACTION_ACTIVE", (gear_long)PGSQL_TRANSACTION_ACTIVE);
	REGISTER_PDO_CLASS_CONST_LONG("PGSQL_TRANSACTION_INTRANS", (gear_long)PGSQL_TRANSACTION_INTRANS);
	REGISTER_PDO_CLASS_CONST_LONG("PGSQL_TRANSACTION_INERROR", (gear_long)PGSQL_TRANSACTION_INERROR);
	REGISTER_PDO_CLASS_CONST_LONG("PGSQL_TRANSACTION_UNKNOWN", (gear_long)PGSQL_TRANSACTION_UNKNOWN);

	hyss_pdo_register_driver(&pdo_pgsql_driver);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION(pdo_pgsql)
{
	hyss_pdo_unregister_driver(&pdo_pgsql_driver);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(pdo_pgsql)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "PDO Driver for PostgreSQL", "enabled");
#ifdef HAVE_PG_CONFIG_H
	hyss_info_print_table_row(2, "PostgreSQL(libpq) Version", PG_VERSION);
#endif
	hyss_info_print_table_end();
}
/* }}} */

