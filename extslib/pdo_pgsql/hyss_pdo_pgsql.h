/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PDO_PGSQL_H
#define HYSS_PDO_PGSQL_H

#include <libpq-fe.h>

extern gear_capi_entry pdo_pgsql_capi_entry;
#define hyssext_pdo_pgsql_ptr &pdo_pgsql_capi_entry

#include "hyss_version.h"
#define HYSS_PDO_PGSQL_VERSION HYSS_VERSION

#ifdef ZTS
#include "hypbc.h"
#endif

HYSS_MINIT_FUNCTION(pdo_pgsql);
HYSS_MSHUTDOWN_FUNCTION(pdo_pgsql);
HYSS_MINFO_FUNCTION(pdo_pgsql);

#endif	/* HYSS_PDO_PGSQL_H */

