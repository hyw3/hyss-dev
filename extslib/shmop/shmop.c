/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "hyss_shmop.h"
# ifndef HYSS_WIN32
# include <sys/ipc.h>
# include <sys/shm.h>
#else
#include "pbc_win32.h"
#endif


#if HAVE_SHMOP

#include "extslib/standard/info.h"

#ifdef ZTS
int shmop_globals_id;
#else
hyss_shmop_globals shmop_globals;
#endif

int shm_type;

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_shmop_open, 0, 0, 4)
	GEAR_ARG_INFO(0, key)
	GEAR_ARG_INFO(0, flags)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, size)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shmop_read, 0, 0, 3)
	GEAR_ARG_INFO(0, shmid)
	GEAR_ARG_INFO(0, start)
	GEAR_ARG_INFO(0, count)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shmop_close, 0, 0, 1)
	GEAR_ARG_INFO(0, shmid)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shmop_size, 0, 0, 1)
	GEAR_ARG_INFO(0, shmid)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shmop_write, 0, 0, 3)
	GEAR_ARG_INFO(0, shmid)
	GEAR_ARG_INFO(0, data)
	GEAR_ARG_INFO(0, offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shmop_delete, 0, 0, 1)
	GEAR_ARG_INFO(0, shmid)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ shmop_functions[]
 */
static const gear_function_entry shmop_functions[] = {
	HYSS_FE(shmop_open, 		arginfo_shmop_open)
	HYSS_FE(shmop_read, 		arginfo_shmop_read)
	HYSS_FE(shmop_close, 	arginfo_shmop_close)
	HYSS_FE(shmop_size, 		arginfo_shmop_size)
	HYSS_FE(shmop_write, 	arginfo_shmop_write)
	HYSS_FE(shmop_delete, 	arginfo_shmop_delete)
	HYSS_FE_END
};
/* }}} */

/* {{{ shmop_capi_entry
 */
gear_capi_entry shmop_capi_entry = {
	STANDARD_CAPI_HEADER,
	"shmop",
	shmop_functions,
	HYSS_MINIT(shmop),
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(shmop),
	HYSS_SHMOP_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_SHMOP
GEAR_GET_CAPI(shmop)
#endif

/* {{{ rsclean
 */
static void rsclean(gear_resource *rsrc)
{
	struct hyss_shmop *shmop = (struct hyss_shmop *)rsrc->ptr;

	shmdt(shmop->addr);
	efree(shmop);
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(shmop)
{
	shm_type = gear_register_list_destructors_ex(rsclean, NULL, "shmop", capi_number);

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(shmop)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "shmop support", "enabled");
	hyss_info_print_table_end();
}
/* }}} */

/* {{{ proto resource shmop_open(int key, string flags, int mode, int size)
   gets and attaches a shared memory segment */
HYSS_FUNCTION(shmop_open)
{
	gear_long key, mode, size;
	struct hyss_shmop *shmop;
	struct shmid_ds shm;
	char *flags;
	size_t flags_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "lsll", &key, &flags, &flags_len, &mode, &size) == FAILURE) {
		return;
	}

	if (flags_len != 1) {
		hyss_error_docref(NULL, E_WARNING, "%s is not a valid flag", flags);
		RETURN_FALSE;
	}

	shmop = emalloc(sizeof(struct hyss_shmop));
	memset(shmop, 0, sizeof(struct hyss_shmop));

	shmop->key = key;
	shmop->shmflg |= mode;

	switch (flags[0])
	{
		case 'a':
			shmop->shmatflg |= SHM_RDONLY;
			break;
		case 'c':
			shmop->shmflg |= IPC_CREAT;
			shmop->size = size;
			break;
		case 'n':
			shmop->shmflg |= (IPC_CREAT | IPC_EXCL);
			shmop->size = size;
			break;
		case 'w':
			/* noop
				shm segment is being opened for read & write
				will fail if segment does not exist
			*/
			break;
		default:
			hyss_error_docref(NULL, E_WARNING, "invalid access mode");
			goto err;
	}

	if (shmop->shmflg & IPC_CREAT && shmop->size < 1) {
		hyss_error_docref(NULL, E_WARNING, "Shared memory segment size must be greater than zero");
		goto err;
	}

	shmop->shmid = shmget(shmop->key, shmop->size, shmop->shmflg);
	if (shmop->shmid == -1) {
		hyss_error_docref(NULL, E_WARNING, "unable to attach or create shared memory segment '%s'", strerror(errno));
		goto err;
	}

	if (shmctl(shmop->shmid, IPC_STAT, &shm)) {
		/* please do not add coverage here: the segment would be leaked and impossible to delete via hyss */
		hyss_error_docref(NULL, E_WARNING, "unable to get shared memory segment information '%s'", strerror(errno));
		goto err;
	}

	shmop->addr = shmat(shmop->shmid, 0, shmop->shmatflg);
	if (shmop->addr == (char*) -1) {
		hyss_error_docref(NULL, E_WARNING, "unable to attach to shared memory segment '%s'", strerror(errno));
		goto err;
	}

	shmop->size = shm.shm_segsz;

	RETURN_RES(gear_register_resource(shmop, shm_type));
err:
	efree(shmop);
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto string shmop_read(resource shmid, int start, int count)
   reads from a shm segment */
HYSS_FUNCTION(shmop_read)
{
	zval *shmid;
	gear_long start, count;
	struct hyss_shmop *shmop;
	char *startaddr;
	int bytes;
	gear_string *return_string;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rll", &shmid, &start, &count) == FAILURE) {
		return;
	}

	if ((shmop = (struct hyss_shmop *)gear_fetch_resource(Z_RES_P(shmid), "shmop", shm_type)) == NULL) {
		RETURN_FALSE;
	}

	if (start < 0 || start > shmop->size) {
		hyss_error_docref(NULL, E_WARNING, "start is out of range");
		RETURN_FALSE;
	}

	if (count < 0 || start > (INT_MAX - count) || start + count > shmop->size) {
		hyss_error_docref(NULL, E_WARNING, "count is out of range");
		RETURN_FALSE;
	}

	startaddr = shmop->addr + start;
	bytes = count ? count : shmop->size - start;

	return_string = gear_string_init(startaddr, bytes, 0);

	RETURN_NEW_STR(return_string);
}
/* }}} */

/* {{{ proto void shmop_close(resource shmid)
   closes a shared memory segment */
HYSS_FUNCTION(shmop_close)
{
	zval *shmid;
	struct hyss_shmop *shmop;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &shmid) == FAILURE) {
		return;
	}


	if ((shmop = (struct hyss_shmop *)gear_fetch_resource(Z_RES_P(shmid), "shmop", shm_type)) == NULL) {
		RETURN_FALSE;
	}

	gear_list_close(Z_RES_P(shmid));
}
/* }}} */

/* {{{ proto int shmop_size(resource shmid)
   returns the shm size */
HYSS_FUNCTION(shmop_size)
{
	zval *shmid;
	struct hyss_shmop *shmop;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &shmid) == FAILURE) {
		return;
	}

	if ((shmop = (struct hyss_shmop *)gear_fetch_resource(Z_RES_P(shmid), "shmop", shm_type)) == NULL) {
		RETURN_FALSE;
	}

	RETURN_LONG(shmop->size);
}
/* }}} */

/* {{{ proto int shmop_write(resource shmid, string data, int offset)
   writes to a shared memory segment */
HYSS_FUNCTION(shmop_write)
{
	struct hyss_shmop *shmop;
	gear_long writesize;
	gear_long offset;
	gear_string *data;
	zval *shmid;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rSl", &shmid, &data, &offset) == FAILURE) {
		return;
	}

	if ((shmop = (struct hyss_shmop *)gear_fetch_resource(Z_RES_P(shmid), "shmop", shm_type)) == NULL) {
		RETURN_FALSE;
	}

	if ((shmop->shmatflg & SHM_RDONLY) == SHM_RDONLY) {
		hyss_error_docref(NULL, E_WARNING, "trying to write to a read only segment");
		RETURN_FALSE;
	}

	if (offset < 0 || offset > shmop->size) {
		hyss_error_docref(NULL, E_WARNING, "offset out of range");
		RETURN_FALSE;
	}

	writesize = ((gear_long)ZSTR_LEN(data) < shmop->size - offset) ? (gear_long)ZSTR_LEN(data) : shmop->size - offset;
	memcpy(shmop->addr + offset, ZSTR_VAL(data), writesize);

	RETURN_LONG(writesize);
}
/* }}} */

/* {{{ proto bool shmop_delete(resource shmid)
   mark segment for deletion */
HYSS_FUNCTION(shmop_delete)
{
	zval *shmid;
	struct hyss_shmop *shmop;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &shmid) == FAILURE) {
		return;
	}

	if ((shmop = (struct hyss_shmop *)gear_fetch_resource(Z_RES_P(shmid), "shmop", shm_type)) == NULL) {
		RETURN_FALSE;
	}

	if (shmctl(shmop->shmid, IPC_RMID, NULL)) {
		hyss_error_docref(NULL, E_WARNING, "can't mark segment for deletion (are you the owner?)");
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

#endif	/* HAVE_SHMOP */

