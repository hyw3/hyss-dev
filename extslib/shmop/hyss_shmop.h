/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SHMOP_H
#define HYSS_SHMOP_H

#if HAVE_SHMOP

extern gear_capi_entry shmop_capi_entry;
#define hyssext_shmop_ptr &shmop_capi_entry

#include "hyss_version.h"
#define HYSS_SHMOP_VERSION HYSS_VERSION

HYSS_MINIT_FUNCTION(shmop);
HYSS_MINFO_FUNCTION(shmop);

HYSS_FUNCTION(shmop_open);
HYSS_FUNCTION(shmop_read);
HYSS_FUNCTION(shmop_close);
HYSS_FUNCTION(shmop_size);
HYSS_FUNCTION(shmop_write);
HYSS_FUNCTION(shmop_delete);

#ifdef HYSS_WIN32
# include "win32/ipc.h"
#endif

struct hyss_shmop
{
	int shmid;
	key_t key;
	int shmflg;
	int shmatflg;
	char *addr;
	gear_long size;
};

typedef struct {
	int le_shmop;
} hyss_shmop_globals;

#ifdef ZTS
#define SHMOPG(v) PBCG(shmop_globals_id, hyss_shmop_globals *, v)
#else
#define SHMOPG(v) (shmop_globals.v)
#endif

#else

#define hyssext_shmop_ptr NULL

#endif

#endif	/* HYSS_SHMOP_H */


