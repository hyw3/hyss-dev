dnl config.m4 for extension shmop

HYSS_ARG_ENABLE(shmop, whether to enable shmop support,
[  --enable-shmop          Enable shmop support])

if test "$HYSS_SHMOP" != "no"; then
  AC_DEFINE(HAVE_SHMOP, 1, [ ])
  HYSS_NEW_EXTENSION(shmop, shmop.c, $ext_shared)
fi
