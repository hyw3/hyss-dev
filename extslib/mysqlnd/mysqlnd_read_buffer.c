/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "mysqlnd.h"
#include "mysqlnd_debug.h"
#include "mysqlnd_read_buffer.h"


/* {{{ mysqlnd_read_buffer_is_empty */
static gear_bool
mysqlnd_read_buffer_is_empty(const MYSQLND_READ_BUFFER * const buffer)
{
	return buffer->len? FALSE:TRUE;
}
/* }}} */


/* {{{ mysqlnd_read_buffer_read */
static void
mysqlnd_read_buffer_read(MYSQLND_READ_BUFFER * buffer, const size_t count, gear_uchar * dest)
{
	if (buffer->len >= count) {
		memcpy(dest, buffer->data + buffer->offset, count);
		buffer->offset += count;
		buffer->len -= count;
	}
}
/* }}} */


/* {{{ mysqlnd_read_buffer_bytes_left */
static size_t
mysqlnd_read_buffer_bytes_left(const MYSQLND_READ_BUFFER * const buffer)
{
	return buffer->len;
}
/* }}} */


/* {{{ mysqlnd_read_buffer_free */
static void
mysqlnd_read_buffer_free(MYSQLND_READ_BUFFER ** buffer)
{
	DBG_ENTER("mysqlnd_read_buffer_free");
	if (*buffer) {
		mnd_efree((*buffer)->data);
		mnd_efree(*buffer);
		*buffer = NULL;
	}
	DBG_VOID_RETURN;
}
/* }}} */


/* {{{ mysqlnd_create_read_buffer */
HYSSAPI MYSQLND_READ_BUFFER *
mysqlnd_create_read_buffer(const size_t count)
{
	MYSQLND_READ_BUFFER * ret = mnd_emalloc(sizeof(MYSQLND_READ_BUFFER));
	DBG_ENTER("mysqlnd_create_read_buffer");
	ret->is_empty = mysqlnd_read_buffer_is_empty;
	ret->read = mysqlnd_read_buffer_read;
	ret->bytes_left = mysqlnd_read_buffer_bytes_left;
	ret->free_buffer = mysqlnd_read_buffer_free;
	ret->data = mnd_emalloc(count);
	ret->size = ret->len = count;
	ret->offset = 0;
	DBG_RETURN(ret);
}
/* }}} */

