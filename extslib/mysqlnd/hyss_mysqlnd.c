/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "mysqlnd.h"
#include "mysqlnd_priv.h"
#include "mysqlnd_debug.h"
#include "mysqlnd_statistics.h"
#include "mysqlnd_reverse_api.h"
#include "extslib/standard/info.h"
#include "gear_smart_str.h"

/* {{{ mysqlnd_functions[]
 *
 * Every user visible function must have an entry in mysqlnd_functions[].
 */
static gear_function_entry mysqlnd_functions[] = {
	HYSS_FE_END
};
/* }}} */


/* {{{ mysqlnd_minfo_print_hash */
HYSSAPI void
mysqlnd_minfo_print_hash(zval *values)
{
	zval *values_entry;
	gear_string	*string_key;

	GEAR_HASH_FOREACH_STR_KEY_VAL(Z_ARRVAL_P(values), string_key, values_entry) {
		convert_to_string(values_entry);
		hyss_info_print_table_row(2, ZSTR_VAL(string_key), Z_STRVAL_P(values_entry));
	} GEAR_HASH_FOREACH_END();
}
/* }}} */


/* {{{ mysqlnd_minfo_dump_plugin_stats */
static int
mysqlnd_minfo_dump_plugin_stats(zval *el, void * argument)
{
	struct st_mysqlnd_plugin_header * plugin_header = (struct st_mysqlnd_plugin_header *)Z_PTR_P(el);
	if (plugin_header->plugin_stats.values) {
		char buf[64];
		zval values;
		snprintf(buf, sizeof(buf), "%s statistics", plugin_header->plugin_name);

		mysqlnd_fill_stats_hash(plugin_header->plugin_stats.values, plugin_header->plugin_stats.names, &values GEAR_FILE_LINE_CC);

		hyss_info_print_table_start();
		hyss_info_print_table_header(2, buf, "");
		mysqlnd_minfo_print_hash(&values);
		hyss_info_print_table_end();
		gear_array_destroy(Z_ARR(values));
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */


/* {{{ mysqlnd_minfo_dump_loaded_plugins */
static int
mysqlnd_minfo_dump_loaded_plugins(zval *el, void * buf)
{
	smart_str * buffer = (smart_str *) buf;
	struct st_mysqlnd_plugin_header * plugin_header = (struct st_mysqlnd_plugin_header *)Z_PTR_P(el);
	if (plugin_header->plugin_name) {
		if (buffer->s) {
			smart_str_appendc(buffer, ',');
		}
		smart_str_appends(buffer, plugin_header->plugin_name);
	}
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */


/* {{{ mysqlnd_minfo_dump_api_plugins */
static void
mysqlnd_minfo_dump_api_plugins(smart_str * buffer)
{
	HashTable *ht = mysqlnd_reverse_api_get_api_list();
	MYSQLND_REVERSE_API *ext;

	GEAR_HASH_FOREACH_PTR(ht, ext) {
		if (buffer->s) {
			smart_str_appendc(buffer, ',');
		}
		smart_str_appends(buffer, ext->cAPI->name);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */


/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(mysqlnd)
{
	char buf[32];

	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "mysqlnd", "enabled");
	hyss_info_print_table_row(2, "Version", mysqlnd_get_client_info());
	hyss_info_print_table_row(2, "Compression",
#ifdef MYSQLND_COMPRESSION_ENABLED
								"supported");
#else
								"not supported");
#endif
	hyss_info_print_table_row(2, "core SSL",
#ifdef MYSQLND_SSL_SUPPORTED
								"supported");
#else
								"not supported");
#endif
	hyss_info_print_table_row(2, "extended SSL",
#ifdef MYSQLND_HAVE_SSL
								"supported");
#else
								"not supported");
#endif
	snprintf(buf, sizeof(buf), GEAR_LONG_FMT, MYSQLND_G(net_cmd_buffer_size));
	hyss_info_print_table_row(2, "Command buffer size", buf);
	snprintf(buf, sizeof(buf), GEAR_LONG_FMT, MYSQLND_G(net_read_buffer_size));
	hyss_info_print_table_row(2, "Read buffer size", buf);
	snprintf(buf, sizeof(buf), GEAR_LONG_FMT, MYSQLND_G(net_read_timeout));
	hyss_info_print_table_row(2, "Read timeout", buf);
	hyss_info_print_table_row(2, "Collecting statistics", MYSQLND_G(collect_statistics)? "Yes":"No");
	hyss_info_print_table_row(2, "Collecting memory statistics", MYSQLND_G(collect_memory_statistics)? "Yes":"No");

	hyss_info_print_table_row(2, "Tracing", MYSQLND_G(debug)? MYSQLND_G(debug):"n/a");

	/* loaded plugins */
	{
		smart_str tmp_str = {0};
		mysqlnd_plugin_apply_with_argument(mysqlnd_minfo_dump_loaded_plugins, &tmp_str);
		smart_str_0(&tmp_str);
		hyss_info_print_table_row(2, "Loaded plugins", tmp_str.s? ZSTR_VAL(tmp_str.s) : "");
		smart_str_free(&tmp_str);

		mysqlnd_minfo_dump_api_plugins(&tmp_str);
		smart_str_0(&tmp_str);
		hyss_info_print_table_row(2, "API Extensions", tmp_str.s? ZSTR_VAL(tmp_str.s) : "");
		smart_str_free(&tmp_str);
	}

	hyss_info_print_table_end();


	/* Print client stats */
	mysqlnd_plugin_apply_with_argument(mysqlnd_minfo_dump_plugin_stats, NULL);
}
/* }}} */


HYSSAPI GEAR_DECLARE_CAPI_GLOBALS(mysqlnd)


/* {{{ HYSS_GINIT_FUNCTION
 */
static HYSS_GINIT_FUNCTION(mysqlnd)
{
#if defined(COMPILE_DL_MYSQLND) && defined(ZTS)
	GEAR_PBCLS_CACHE_UPDATE();
#endif
	mysqlnd_globals->collect_statistics = TRUE;
	mysqlnd_globals->collect_memory_statistics = FALSE;
	mysqlnd_globals->debug = NULL;	/* The actual string */
	mysqlnd_globals->dbg = NULL;	/* The DBG object*/
	mysqlnd_globals->trace_alloc_settings = NULL;
	mysqlnd_globals->trace_alloc = NULL;
	mysqlnd_globals->net_cmd_buffer_size = MYSQLND_NET_CMD_BUFFER_MIN_SIZE;
	mysqlnd_globals->net_read_buffer_size = 32768;
	mysqlnd_globals->net_read_timeout = 31536000;
	mysqlnd_globals->log_mask = 0;
	mysqlnd_globals->mempool_default_size = 16000;
	mysqlnd_globals->debug_emalloc_fail_threshold = -1;
	mysqlnd_globals->debug_ecalloc_fail_threshold = -1;
	mysqlnd_globals->debug_erealloc_fail_threshold = -1;
	mysqlnd_globals->debug_malloc_fail_threshold = -1;
	mysqlnd_globals->debug_calloc_fail_threshold = -1;
	mysqlnd_globals->debug_realloc_fail_threshold = -1;
	mysqlnd_globals->sha256_server_public_key = NULL;
	mysqlnd_globals->fetch_data_copy = FALSE;
}
/* }}} */


/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateNetCmdBufferSize)
{
	gear_long long_value;

	GEAR_ATOL(long_value, ZSTR_VAL(new_value));
	if (long_value < MYSQLND_NET_CMD_BUFFER_MIN_SIZE) {
		return FAILURE;
	}
	MYSQLND_G(net_cmd_buffer_size) = long_value;

	return SUCCESS;
}
/* }}} */


/* {{{ HYSS_ICS_BEGIN
*/
HYSS_ICS_BEGIN()
	STD_HYSS_ICS_BOOLEAN("mysqlnd.collect_statistics",	"1", 	HYSS_ICS_ALL,	OnUpdateBool,	collect_statistics, gear_mysqlnd_globals, mysqlnd_globals)
	STD_HYSS_ICS_BOOLEAN("mysqlnd.collect_memory_statistics","0",HYSS_ICS_SYSTEM, OnUpdateBool,	collect_memory_statistics, gear_mysqlnd_globals, mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.debug",					NULL, 	HYSS_ICS_SYSTEM, OnUpdateString,	debug, gear_mysqlnd_globals, mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.trace_alloc",			NULL, 	HYSS_ICS_SYSTEM, OnUpdateString,	trace_alloc_settings, gear_mysqlnd_globals, mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.net_cmd_buffer_size",	MYSQLND_NET_CMD_BUFFER_MIN_SIZE_STR,	HYSS_ICS_ALL,	OnUpdateNetCmdBufferSize,	net_cmd_buffer_size,	gear_mysqlnd_globals,		mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.net_read_buffer_size",	"32768",HYSS_ICS_ALL,	OnUpdateLong,	net_read_buffer_size,	gear_mysqlnd_globals,		mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.net_read_timeout",		"86400",HYSS_ICS_ALL,	OnUpdateLong,	net_read_timeout, gear_mysqlnd_globals, mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.log_mask",				"0", 	HYSS_ICS_ALL,	OnUpdateLong,	log_mask, gear_mysqlnd_globals, mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.mempool_default_size","16000",   HYSS_ICS_ALL,	OnUpdateLong,	mempool_default_size,	gear_mysqlnd_globals,		mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.sha256_server_public_key",NULL, 	HYSS_ICS_PERDIR, OnUpdateString,	sha256_server_public_key, gear_mysqlnd_globals, mysqlnd_globals)
	STD_HYSS_ICS_BOOLEAN("mysqlnd.fetch_data_copy",	"0", 		HYSS_ICS_ALL,	OnUpdateBool,	fetch_data_copy, gear_mysqlnd_globals, mysqlnd_globals)
#if HYSS_DEBUG
	STD_HYSS_ICS_ENTRY("mysqlnd.debug_emalloc_fail_threshold","-1",   HYSS_ICS_SYSTEM,	OnUpdateLong,	debug_emalloc_fail_threshold,	gear_mysqlnd_globals,		mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.debug_ecalloc_fail_threshold","-1",   HYSS_ICS_SYSTEM,	OnUpdateLong,	debug_ecalloc_fail_threshold,	gear_mysqlnd_globals,		mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.debug_erealloc_fail_threshold","-1",   HYSS_ICS_SYSTEM,	OnUpdateLong,	debug_erealloc_fail_threshold,	gear_mysqlnd_globals,		mysqlnd_globals)

	STD_HYSS_ICS_ENTRY("mysqlnd.debug_malloc_fail_threshold","-1",   HYSS_ICS_SYSTEM,	OnUpdateLong,	debug_malloc_fail_threshold,	gear_mysqlnd_globals,		mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.debug_calloc_fail_threshold","-1",   HYSS_ICS_SYSTEM,	OnUpdateLong,	debug_calloc_fail_threshold,	gear_mysqlnd_globals,		mysqlnd_globals)
	STD_HYSS_ICS_ENTRY("mysqlnd.debug_realloc_fail_threshold","-1",   HYSS_ICS_SYSTEM,	OnUpdateLong,	debug_realloc_fail_threshold,	gear_mysqlnd_globals,		mysqlnd_globals)
#endif
HYSS_ICS_END()
/* }}} */


/* {{{ HYSS_MINIT_FUNCTION
 */
static HYSS_MINIT_FUNCTION(mysqlnd)
{
	REGISTER_ICS_ENTRIES();

	mysqlnd_library_init();
	return SUCCESS;
}
/* }}} */


/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
static HYSS_MSHUTDOWN_FUNCTION(mysqlnd)
{
	mysqlnd_library_end();

	UNREGISTER_ICS_ENTRIES();
	return SUCCESS;
}
/* }}} */


#if HYSS_DEBUG
/* {{{ HYSS_RINIT_FUNCTION
 */
static HYSS_RINIT_FUNCTION(mysqlnd)
{
	if (MYSQLND_G(debug)) {
		struct st_mysqlnd_plugin_trace_log * trace_log_plugin = mysqlnd_plugin_find("debug_trace");
		MYSQLND_G(dbg) = NULL;
		if (trace_log_plugin) {
			MYSQLND_DEBUG * dbg = trace_log_plugin->methods.trace_instance_init(mysqlnd_debug_std_no_trace_funcs);
			MYSQLND_DEBUG * trace_alloc = trace_log_plugin->methods.trace_instance_init(NULL);
			if (!dbg || !trace_alloc) {
				return FAILURE;
			}
			dbg->m->set_mode(dbg, MYSQLND_G(debug));
			trace_alloc->m->set_mode(trace_alloc, MYSQLND_G(trace_alloc_settings));
			MYSQLND_G(dbg) = dbg;
			MYSQLND_G(trace_alloc) = trace_alloc;
		}
	}
	return SUCCESS;
}
/* }}} */
#endif


#if HYSS_DEBUG
/* {{{ HYSS_RSHUTDOWN_FUNCTION
 */
static HYSS_RSHUTDOWN_FUNCTION(mysqlnd)
{
	MYSQLND_DEBUG * dbg = MYSQLND_G(dbg);
	MYSQLND_DEBUG * trace_alloc = MYSQLND_G(trace_alloc);
	DBG_ENTER("RSHUTDOWN");
	if (dbg) {
		dbg->m->close(dbg);
		dbg->m->free_handle(dbg);
		MYSQLND_G(dbg) = NULL;
	}
	if (trace_alloc) {
		trace_alloc->m->close(trace_alloc);
		trace_alloc->m->free_handle(trace_alloc);
		MYSQLND_G(trace_alloc) = NULL;
	}
	return SUCCESS;
}
/* }}} */
#endif


static const gear_capi_dep mysqlnd_deps[] = {
	GEAR_CAPI_REQUIRED("standard")
	GEAR_CAPI_END
};

/* {{{ mysqlnd_capi_entry
 */
gear_capi_entry mysqlnd_capi_entry = {
	STANDARD_CAPI_HEADER_EX,
	NULL,
	mysqlnd_deps,
	"mysqlnd",
	mysqlnd_functions,
	HYSS_MINIT(mysqlnd),
	HYSS_MSHUTDOWN(mysqlnd),
#if HYSS_DEBUG
	HYSS_RINIT(mysqlnd),
#else
	NULL,
#endif
#if HYSS_DEBUG
	HYSS_RSHUTDOWN(mysqlnd),
#else
	NULL,
#endif
	HYSS_MINFO(mysqlnd),
	HYSS_MYSQLND_VERSION,
	HYSS_CAPI_GLOBALS(mysqlnd),
	HYSS_GINIT(mysqlnd),
	NULL,
	NULL,
	STANDARD_CAPI_PROPERTIES_EX
};
/* }}} */

/* {{{ COMPILE_DL_MYSQLND */
#ifdef COMPILE_DL_MYSQLND
#ifdef ZTS
GEAR_PBCLS_CACHE_DEFINE()
#endif
GEAR_GET_CAPI(mysqlnd)
#endif
/* }}} */

