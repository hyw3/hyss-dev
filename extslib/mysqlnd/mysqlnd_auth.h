/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MYSQLND_AUTH_H
#define MYSQLND_AUTH_H
enum_func_status
mysqlnd_auth_handshake(MYSQLND_CONN_DATA * conn,
						const char * const user,
						const char * const passwd,
						const size_t passwd_len,
						const char * const db,
						const size_t db_len,
						const MYSQLND_SESSION_OPTIONS * const session_options,
						gear_ulong mysql_flags,
						unsigned int server_charset_no,
						gear_bool use_full_blown_auth_packet,
						const char * const auth_protocol,
						const gear_uchar * const auth_plugin_data,
						const size_t auth_plugin_data_len,
						char ** switch_to_auth_protocol,
						size_t * switch_to_auth_protocol_len,
						gear_uchar ** switch_to_auth_protocol_data,
						size_t * switch_to_auth_protocol_data_len
						);

enum_func_status
mysqlnd_auth_handshake(MYSQLND_CONN_DATA * conn,
						const char * const user,
						const char * const passwd,
						const size_t passwd_len,
						const char * const db,
						const size_t db_len,
						const MYSQLND_SESSION_OPTIONS * const session_options,
						gear_ulong mysql_flags,
						unsigned int server_charset_no,
						gear_bool use_full_blown_auth_packet,
						const char * const auth_protocol,
						const gear_uchar * const auth_plugin_data,
						const size_t auth_plugin_data_len,
						char ** switch_to_auth_protocol,
						size_t * switch_to_auth_protocol_len,
						gear_uchar ** switch_to_auth_protocol_data,
						size_t * switch_to_auth_protocol_data_len
						);

enum_func_status
mysqlnd_auth_change_user(MYSQLND_CONN_DATA * const conn,
								const char * const user,
								const size_t user_len,
								const char * const passwd,
								const size_t passwd_len,
								const char * const db,
								const size_t db_len,
								const gear_bool silent,
								gear_bool use_full_blown_auth_packet,
								const char * const auth_protocol,
								gear_uchar * auth_plugin_data,
								size_t auth_plugin_data_len,
								char ** switch_to_auth_protocol,
								size_t * switch_to_auth_protocol_len,
								gear_uchar ** switch_to_auth_protocol_data,
								size_t * switch_to_auth_protocol_data_len
								);


enum_func_status
mysqlnd_connect_run_authentication(
			MYSQLND_CONN_DATA * conn,
			const char * const user,
			const char * const passwd,
			const char * const db,
			size_t db_len,
			size_t passwd_len,
			MYSQLND_STRING authentication_plugin_data,
			const char * const authentication_protocol,
			const unsigned int charset_no,
			size_t server_capabilities,
			const MYSQLND_SESSION_OPTIONS * const session_options,
			gear_ulong mysql_flags
			);

enum_func_status
mysqlnd_run_authentication(
			MYSQLND_CONN_DATA * conn,
			const char * const user,
			const char * const passwd,
			const size_t passwd_len,
			const char * const db,
			const size_t db_len,
			const MYSQLND_STRING auth_plugin_data,
			const char * const auth_protocol,
			unsigned int charset_no,
			const MYSQLND_SESSION_OPTIONS * const session_options,
			gear_ulong mysql_flags,
			gear_bool silent,
			gear_bool is_change_user
			);

HYSSAPI void hyss_mysqlnd_scramble(gear_uchar * const buffer, const gear_uchar * const scramble, const gear_uchar * const pass, const size_t pass_len);

#endif /* MYSQLND_AUTH_H */

