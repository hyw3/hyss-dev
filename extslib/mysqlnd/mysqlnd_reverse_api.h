/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MYSQLND_REVERSE_API_H
#define MYSQLND_REVERSE_API_H
typedef struct st_mysqlnd_reverse_api
{
	gear_capi_entry * cAPI;
	MYSQLND *(*conversion_cb)(zval * zv);
} MYSQLND_REVERSE_API;


HYSSAPI void mysqlnd_reverse_api_init(void);
HYSSAPI void mysqlnd_reverse_api_end(void);

HYSSAPI HashTable * mysqlnd_reverse_api_get_api_list(void);

HYSSAPI void mysqlnd_reverse_api_register_api(const MYSQLND_REVERSE_API * apiext);
HYSSAPI MYSQLND * zval_to_mysqlnd(zval * zv, const unsigned int client_api_capabilities, unsigned int * save_client_api_capabilities);

#endif	/* MYSQLND_REVERSE_API_H */

