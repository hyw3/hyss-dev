/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "mysqlnd.h"
#include "mysqlnd_block_alloc.h"
#include "mysqlnd_debug.h"
#include "mysqlnd_priv.h"


/* {{{ mysqlnd_arena_create */
static gear_always_inline gear_arena* mysqlnd_arena_create(size_t size)
{
	gear_arena *arena = (gear_arena*)mnd_emalloc(size);

	arena->ptr = (char*) arena + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena));
	arena->end = (char*) arena + size;
	arena->prev = NULL;
	return arena;
}
/* }}} */

/* {{{ mysqlnd_arena_destroy */
static gear_always_inline void mysqlnd_arena_destroy(gear_arena *arena)
{
	do {
		gear_arena *prev = arena->prev;
		mnd_efree(arena);
		arena = prev;
	} while (arena);
}
/* }}} */

/* {{{ mysqlnd_arena_alloc */
static gear_always_inline void* mysqlnd_arena_alloc(gear_arena **arena_ptr, size_t size)
{
	gear_arena *arena = *arena_ptr;
	char *ptr = arena->ptr;

	size = GEAR_MM_ALIGNED_SIZE(size);

	if (EXPECTED(size <= (size_t)(arena->end - ptr))) {
		arena->ptr = ptr + size;
	} else {
		size_t arena_size =
			UNEXPECTED((size + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena))) > (size_t)(arena->end - (char*) arena)) ?
				(size + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena))) :
				(size_t)(arena->end - (char*) arena);
		gear_arena *new_arena = (gear_arena*)mnd_emalloc(arena_size);

		ptr = (char*) new_arena + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena));
		new_arena->ptr = (char*) new_arena + GEAR_MM_ALIGNED_SIZE(sizeof(gear_arena)) + size;
		new_arena->end = (char*) new_arena + arena_size;
		new_arena->prev = arena;
		*arena_ptr = new_arena;
	}

	return (void*) ptr;
}
/* }}} */

static gear_always_inline void* mysqlnd_arena_checkpoint(gear_arena *arena)
{
	return arena->ptr;
}

static gear_always_inline void mysqlnd_arena_release(gear_arena **arena_ptr, void *checkpoint)
{
	gear_arena *arena = *arena_ptr;

	while (UNEXPECTED((char*)checkpoint > arena->end) ||
	       UNEXPECTED((char*)checkpoint <= (char*)arena)) {
		gear_arena *prev = arena->prev;
		mnd_efree(arena);
		*arena_ptr = arena = prev;
	}
	GEAR_ASSERT((char*)checkpoint > (char*)arena && (char*)checkpoint <= arena->end);
	arena->ptr = (char*)checkpoint;
}

/* {{{ mysqlnd_mempool_free_chunk */
static void
mysqlnd_mempool_free_chunk(MYSQLND_MEMORY_POOL * pool, void * ptr)
{
	DBG_ENTER("mysqlnd_mempool_free_chunk");
	/* Try to back-off and guess if this is the last block allocated */
	if (ptr == pool->last) {
		/*
			This was the last allocation. Lucky us, we can free
			a bit of memory from the pool. Next time we will return from the same ptr.
		*/
		pool->arena->ptr = (char*)ptr;
		pool->last = NULL;
	}
	DBG_VOID_RETURN;
}
/* }}} */


/* {{{ mysqlnd_mempool_resize_chunk */
static void *
mysqlnd_mempool_resize_chunk(MYSQLND_MEMORY_POOL * pool, void * ptr, size_t old_size, size_t size)
{
	DBG_ENTER("mysqlnd_mempool_resize_chunk");

	/* Try to back-off and guess if this is the last block allocated */
	if (ptr == pool->last
	  && (GEAR_MM_ALIGNED_SIZE(size) <= ((char*)pool->arena->end - (char*)ptr))) {
		/*
			This was the last allocation. Lucky us, we can free
			a bit of memory from the pool. Next time we will return from the same ptr.
		*/
		pool->arena->ptr = (char*)ptr + GEAR_MM_ALIGNED_SIZE(size);
	} else {
		void *new_ptr = mysqlnd_arena_alloc(&pool->arena, size);
		memcpy(new_ptr, ptr, MIN(old_size, size));
		pool->last = ptr = new_ptr;
	}
	DBG_RETURN(ptr);
}
/* }}} */


/* {{{ mysqlnd_mempool_get_chunk */
static void *
mysqlnd_mempool_get_chunk(MYSQLND_MEMORY_POOL * pool, size_t size)
{
	void *ptr = NULL;
	DBG_ENTER("mysqlnd_mempool_get_chunk");

	ptr = mysqlnd_arena_alloc(&pool->arena, size);
	pool->last = ptr;

	DBG_RETURN(ptr);
}
/* }}} */


/* {{{ mysqlnd_mempool_create */
HYSSAPI MYSQLND_MEMORY_POOL *
mysqlnd_mempool_create(size_t arena_size)
{
	gear_arena * arena;
	MYSQLND_MEMORY_POOL * ret;

	DBG_ENTER("mysqlnd_mempool_create");
	arena = mysqlnd_arena_create(MAX(arena_size, sizeof(gear_arena)));
	ret = mysqlnd_arena_alloc(&arena, sizeof(MYSQLND_MEMORY_POOL));
	ret->arena = arena;
	ret->last = NULL;
	ret->checkpoint = NULL;
	ret->get_chunk = mysqlnd_mempool_get_chunk;
	ret->free_chunk = mysqlnd_mempool_free_chunk;
	ret->resize_chunk = mysqlnd_mempool_resize_chunk;
	DBG_RETURN(ret);
}
/* }}} */


/* {{{ mysqlnd_mempool_destroy */
HYSSAPI void
mysqlnd_mempool_destroy(MYSQLND_MEMORY_POOL * pool)
{
	DBG_ENTER("mysqlnd_mempool_destroy");
	/* mnd_free will reference LOCK_access and might crash, depending on the caller...*/
	mysqlnd_arena_destroy(pool->arena);
	DBG_VOID_RETURN;
}
/* }}} */

/* {{{ mysqlnd_mempool_save_state */
HYSSAPI void
mysqlnd_mempool_save_state(MYSQLND_MEMORY_POOL * pool)
{
	DBG_ENTER("mysqlnd_mempool_save_state");
	pool->checkpoint = mysqlnd_arena_checkpoint(pool->arena);
	DBG_VOID_RETURN;
}
/* }}} */

/* {{{ mysqlnd_mempool_restore_state */
HYSSAPI void
mysqlnd_mempool_restore_state(MYSQLND_MEMORY_POOL * pool)
{
	DBG_ENTER("mysqlnd_mempool_restore_state");
	if (pool->checkpoint) {
		mysqlnd_arena_release(&pool->arena, pool->checkpoint);
		pool->last = NULL;
		pool->checkpoint = NULL;
	}
	DBG_VOID_RETURN;
}
/* }}} */

