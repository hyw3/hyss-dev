/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MYSQLND_PLUGIN_H
#define MYSQLND_PLUGIN_H


void mysqlnd_plugin_subsystem_init(void);
void mysqlnd_plugin_subsystem_end(void);

void mysqlnd_register_builtin_authentication_plugins(void);

void mysqlnd_example_plugin_register(void);

#endif	/* MYSQLND_PLUGIN_H */

