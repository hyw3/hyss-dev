/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MYSQLND_CONNECTION_H
#define MYSQLND_CONNECTION_H

HYSSAPI extern const char * const mysqlnd_out_of_sync;
HYSSAPI extern const char * const mysqlnd_server_gone;
HYSSAPI extern const char * const mysqlnd_out_of_memory;


void mysqlnd_upsert_status_init(MYSQLND_UPSERT_STATUS * const upsert_status);

#define UPSERT_STATUS_RESET(status)							(status)->m->reset((status))

#define UPSERT_STATUS_GET_SERVER_STATUS(status)				(status)->server_status
#define UPSERT_STATUS_SET_SERVER_STATUS(status, server_st)	(status)->server_status = (server_st)

#define UPSERT_STATUS_GET_WARNINGS(status)					(status)->warning_count
#define UPSERT_STATUS_SET_WARNINGS(status, warnings)		(status)->warning_count = (warnings)

#define UPSERT_STATUS_GET_AFFECTED_ROWS(status)				(status)->affected_rows
#define UPSERT_STATUS_SET_AFFECTED_ROWS(status, rows)		(status)->affected_rows = (rows)
#define UPSERT_STATUS_SET_AFFECTED_ROWS_TO_ERROR(status)	(status)->m->set_affected_rows_to_error((status))

#define UPSERT_STATUS_GET_LAST_INSERT_ID(status)			(status)->last_insert_id
#define UPSERT_STATUS_SET_LAST_INSERT_ID(status, id)		(status)->last_insert_id = (id)


/* Error handling */
#define SET_NEW_MESSAGE(buf, buf_len, message, len) \
	{\
		if ((buf)) { \
			mnd_efree((buf)); \
		} \
		if ((message)) { \
			(buf) = mnd_pestrndup((message), (len), 0); \
		} else { \
			(buf) = NULL; \
		} \
		(buf_len) = (len); \
	}

#define SET_EMPTY_MESSAGE(buf, buf_len) \
	{\
		if ((buf)) { \
			mnd_efree((buf)); \
			(buf) = NULL; \
		} \
		(buf_len) = 0; \
	}


HYSSAPI enum_func_status mysqlnd_error_info_init(MYSQLND_ERROR_INFO * const info, const gear_bool persistent);
HYSSAPI void	mysqlnd_error_info_free_contents(MYSQLND_ERROR_INFO * const info);

#define GET_CONNECTION_STATE(state_struct)		(state_struct)->m->get((state_struct))
#define SET_CONNECTION_STATE(state_struct, s)	(state_struct)->m->set((state_struct), (s))

HYSSAPI void mysqlnd_connection_state_init(struct st_mysqlnd_connection_state * const state);

#endif /* MYSQLND_CONNECTION_H */

