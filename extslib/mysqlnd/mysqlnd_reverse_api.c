/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "mysqlnd.h"
#include "mysqlnd_priv.h"
#include "mysqlnd_debug.h"
#include "mysqlnd_reverse_api.h"


static HashTable mysqlnd_api_ext_ht;


/* {{{ mysqlnd_reverse_api_init */
HYSSAPI void
mysqlnd_reverse_api_init(void)
{
	gear_hash_init(&mysqlnd_api_ext_ht, 3, NULL, NULL, 1);
}
/* }}} */


/* {{{ mysqlnd_reverse_api_end */
HYSSAPI void
mysqlnd_reverse_api_end(void)
{
	gear_hash_destroy(&mysqlnd_api_ext_ht);
}
/* }}} */


/* {{{ myslqnd_get_api_extensions */
HYSSAPI HashTable *
mysqlnd_reverse_api_get_api_list(void)
{
	return &mysqlnd_api_ext_ht;
}
/* }}} */


/* {{{ mysqlnd_reverse_api_register_api */
HYSSAPI void
mysqlnd_reverse_api_register_api(const MYSQLND_REVERSE_API * apiext)
{
	gear_hash_str_add_ptr(&mysqlnd_api_ext_ht, apiext->cAPI->name, strlen(apiext->cAPI->name), (void*)apiext);
}
/* }}} */


/* {{{ zval_to_mysqlnd */
HYSSAPI MYSQLND *
zval_to_mysqlnd(zval * zv, const unsigned int client_api_capabilities, unsigned int * save_client_api_capabilities)
{
	MYSQLND * retval;
#ifdef OLD_CODE
	MYSQLND_REVERSE_API * elem;
	GEAR_HASH_FOREACH_PTR(&mysqlnd_api_ext_ht, elem) {
		if (elem->conversion_cb) {
			retval = elem->conversion_cb(zv);
			if (retval) {
				if (retval->data) {
					*save_client_api_capabilities = retval->data->m->negotiate_client_api_capabilities(retval->data, client_api_capabilities);
				}
				return retval;
			}
		}
	} GEAR_HASH_FOREACH_END();
#else
	MYSQLND_REVERSE_API * api;
	GEAR_HASH_FOREACH_PTR(&mysqlnd_api_ext_ht, api) {
		if (api && api->conversion_cb) {
			retval = api->conversion_cb(zv);
			if (retval) {
				if (retval->data) {
					*save_client_api_capabilities = retval->data->m->negotiate_client_api_capabilities(retval->data, client_api_capabilities);
				}
				return retval;
			}
		}
	} GEAR_HASH_FOREACH_END();
#endif
	return NULL;
}
/* }}} */

