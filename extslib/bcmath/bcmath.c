/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if HAVE_BCMATH

#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "hyss_bcmath.h"
#include "libbcmath/src/bcmath.h"

GEAR_DECLARE_CAPI_GLOBALS(bcmath)
static HYSS_GINIT_FUNCTION(bcmath);
static HYSS_GSHUTDOWN_FUNCTION(bcmath);

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_bcadd, 0, 0, 2)
	GEAR_ARG_INFO(0, left_operand)
	GEAR_ARG_INFO(0, right_operand)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bcsub, 0, 0, 2)
	GEAR_ARG_INFO(0, left_operand)
	GEAR_ARG_INFO(0, right_operand)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bcmul, 0, 0, 2)
	GEAR_ARG_INFO(0, left_operand)
	GEAR_ARG_INFO(0, right_operand)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bcdiv, 0, 0, 2)
	GEAR_ARG_INFO(0, left_operand)
	GEAR_ARG_INFO(0, right_operand)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bcmod, 0, 0, 2)
	GEAR_ARG_INFO(0, left_operand)
	GEAR_ARG_INFO(0, right_operand)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bcpowmod, 0, 0, 3)
	GEAR_ARG_INFO(0, x)
	GEAR_ARG_INFO(0, y)
	GEAR_ARG_INFO(0, mod)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bcpow, 0, 0, 2)
	GEAR_ARG_INFO(0, x)
	GEAR_ARG_INFO(0, y)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bcsqrt, 0, 0, 1)
	GEAR_ARG_INFO(0, operand)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bccomp, 0, 0, 2)
	GEAR_ARG_INFO(0, left_operand)
	GEAR_ARG_INFO(0, right_operand)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_bcscale, 0, 0, 0)
	GEAR_ARG_INFO(0, scale)
GEAR_END_ARG_INFO()

/* }}} */

static const gear_function_entry bcmath_functions[] = {
	HYSS_FE(bcadd,									arginfo_bcadd)
	HYSS_FE(bcsub,									arginfo_bcsub)
	HYSS_FE(bcmul,									arginfo_bcmul)
	HYSS_FE(bcdiv,									arginfo_bcdiv)
	HYSS_FE(bcmod,									arginfo_bcmod)
	HYSS_FE(bcpow,									arginfo_bcpow)
	HYSS_FE(bcsqrt,									arginfo_bcsqrt)
	HYSS_FE(bcscale,								arginfo_bcscale)
	HYSS_FE(bccomp,									arginfo_bccomp)
	HYSS_FE(bcpowmod,								arginfo_bcpowmod)
	HYSS_FE_END
};

gear_capi_entry bcmath_capi_entry = {
	STANDARD_CAPI_HEADER,
	"bcmath",
	bcmath_functions,
	HYSS_MINIT(bcmath),
	HYSS_MSHUTDOWN(bcmath),
	NULL,
	NULL,
	HYSS_MINFO(bcmath),
	HYSS_BCMATH_VERSION,
	HYSS_CAPI_GLOBALS(bcmath),
	HYSS_GINIT(bcmath),
    HYSS_GSHUTDOWN(bcmath),
	NULL,
	STANDARD_CAPI_PROPERTIES_EX
};

#ifdef COMPILE_DL_BCMATH
#ifdef ZTS
GEAR_PBCLS_CACHE_DEFINE()
#endif
GEAR_GET_CAPI(bcmath)
#endif

/* {{{ HYSS_ICS */
HYSS_ICS_BEGIN()
	STD_HYSS_ICS_ENTRY("bcmath.scale", "0", HYSS_ICS_ALL, OnUpdateLongGEZero, bc_precision, gear_bcmath_globals, bcmath_globals)
HYSS_ICS_END()
/* }}} */

/* {{{ HYSS_GINIT_FUNCTION
 */
static HYSS_GINIT_FUNCTION(bcmath)
{
#if defined(COMPILE_DL_BCMATH) && defined(ZTS)
	GEAR_PBCLS_CACHE_UPDATE();
#endif
	bcmath_globals->bc_precision = 0;
	bc_init_numbers();
}
/* }}} */

/* {{{ HYSS_GSHUTDOWN_FUNCTION
 */
static HYSS_GSHUTDOWN_FUNCTION(bcmath)
{
	_bc_free_num_ex(&bcmath_globals->_zero_, 1);
	_bc_free_num_ex(&bcmath_globals->_one_, 1);
	_bc_free_num_ex(&bcmath_globals->_two_, 1);
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(bcmath)
{
	REGISTER_ICS_ENTRIES();

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION(bcmath)
{
	UNREGISTER_ICS_ENTRIES();

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(bcmath)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "BCMath support", "enabled");
	hyss_info_print_table_end();
	DISPLAY_ICS_ENTRIES();
}
/* }}} */

/* {{{ hyss_str2num
   Convert to bc_num detecting scale */
static void hyss_str2num(bc_num *num, char *str)
{
	char *p;

	if (!(p = strchr(str, '.'))) {
		bc_str2num(num, str, 0);
		return;
	}

	bc_str2num(num, str, strlen(p+1));
}
/* }}} */

/* {{{ proto string bcadd(string left_operand, string right_operand [, int scale])
   Returns the sum of two arbitrary precision numbers */
HYSS_FUNCTION(bcadd)
{
	gear_string *left, *right;
	gear_long scale_param = 0;
	bc_num first, second, result;
	int scale = (int)BCG(bc_precision);

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(left)
		Z_PARAM_STR(right)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale_param)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 3) {
		scale = (int) (scale_param < 0 ? 0 : scale_param);
	}

	bc_init_num(&first);
	bc_init_num(&second);
	bc_init_num(&result);
	hyss_str2num(&first, ZSTR_VAL(left));
	hyss_str2num(&second, ZSTR_VAL(right));
	bc_add (first, second, &result, scale);

	RETVAL_STR(bc_num2str_ex(result, scale));
	bc_free_num(&first);
	bc_free_num(&second);
	bc_free_num(&result);
	return;
}
/* }}} */

/* {{{ proto string bcsub(string left_operand, string right_operand [, int scale])
   Returns the difference between two arbitrary precision numbers */
HYSS_FUNCTION(bcsub)
{
	gear_string *left, *right;
	gear_long scale_param = 0;
	bc_num first, second, result;
	int scale = (int)BCG(bc_precision);

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(left)
		Z_PARAM_STR(right)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale_param)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 3) {
		scale = (int) ((int)scale_param < 0 ? 0 : scale_param);
	}

	bc_init_num(&first);
	bc_init_num(&second);
	bc_init_num(&result);
	hyss_str2num(&first, ZSTR_VAL(left));
	hyss_str2num(&second, ZSTR_VAL(right));
	bc_sub (first, second, &result, scale);

	RETVAL_STR(bc_num2str_ex(result, scale));
	bc_free_num(&first);
	bc_free_num(&second);
	bc_free_num(&result);
	return;
}
/* }}} */

/* {{{ proto string bcmul(string left_operand, string right_operand [, int scale])
   Returns the multiplication of two arbitrary precision numbers */
HYSS_FUNCTION(bcmul)
{
	gear_string *left, *right;
	gear_long scale_param = 0;
	bc_num first, second, result;
	int scale = (int)BCG(bc_precision);

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(left)
		Z_PARAM_STR(right)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale_param)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 3) {
		scale = (int) ((int)scale_param < 0 ? 0 : scale_param);
	}

	bc_init_num(&first);
	bc_init_num(&second);
	bc_init_num(&result);
	hyss_str2num(&first, ZSTR_VAL(left));
	hyss_str2num(&second, ZSTR_VAL(right));
	bc_multiply (first, second, &result, scale);

	RETVAL_STR(bc_num2str_ex(result, scale));
	bc_free_num(&first);
	bc_free_num(&second);
	bc_free_num(&result);
	return;
}
/* }}} */

/* {{{ proto string bcdiv(string left_operand, string right_operand [, int scale])
   Returns the quotient of two arbitrary precision numbers (division) */
HYSS_FUNCTION(bcdiv)
{
	gear_string *left, *right;
	gear_long scale_param = 0;
	bc_num first, second, result;
	int scale = (int)BCG(bc_precision);

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(left)
		Z_PARAM_STR(right)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale_param)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 3) {
		scale = (int) ((int)scale_param < 0 ? 0 : scale_param);
	}

	bc_init_num(&first);
	bc_init_num(&second);
	bc_init_num(&result);
	hyss_str2num(&first, ZSTR_VAL(left));
	hyss_str2num(&second, ZSTR_VAL(right));

	switch (bc_divide(first, second, &result, scale)) {
		case 0: /* OK */
			RETVAL_STR(bc_num2str_ex(result, scale));
			break;
		case -1: /* division by zero */
			hyss_error_docref(NULL, E_WARNING, "Division by zero");
			break;
	}

	bc_free_num(&first);
	bc_free_num(&second);
	bc_free_num(&result);
	return;
}
/* }}} */

/* {{{ proto string bcmod(string left_operand, string right_operand [, int scale])
   Returns the modulus of the two arbitrary precision operands */
HYSS_FUNCTION(bcmod)
{
	gear_string *left, *right;
	gear_long scale_param = 0;
	bc_num first, second, result;
	int scale = (int)BCG(bc_precision);

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(left)
		Z_PARAM_STR(right)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale_param)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 3) {
		scale = (int) ((int)scale_param < 0 ? 0 : scale_param);
	}

	bc_init_num(&first);
	bc_init_num(&second);
	bc_init_num(&result);
	hyss_str2num(&first, ZSTR_VAL(left));
	hyss_str2num(&second, ZSTR_VAL(right));

	switch (bc_modulo(first, second, &result, scale)) {
		case 0:
			RETVAL_STR(bc_num2str_ex(result, scale));
			break;
		case -1:
			hyss_error_docref(NULL, E_WARNING, "Division by zero");
			break;
	}

	bc_free_num(&first);
	bc_free_num(&second);
	bc_free_num(&result);
	return;
}
/* }}} */

/* {{{ proto string bcpowmod(string x, string y, string mod [, int scale])
   Returns the value of an arbitrary precision number raised to the power of another reduced by a modulous */
HYSS_FUNCTION(bcpowmod)
{
	gear_string *left, *right, *modulous;
	bc_num first, second, mod, result;
	gear_long scale = BCG(bc_precision);
	int scale_int;

	GEAR_PARSE_PARAMETERS_START(3, 4)
		Z_PARAM_STR(left)
		Z_PARAM_STR(right)
		Z_PARAM_STR(modulous)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale)
	GEAR_PARSE_PARAMETERS_END();

	bc_init_num(&first);
	bc_init_num(&second);
	bc_init_num(&mod);
	bc_init_num(&result);
	hyss_str2num(&first, ZSTR_VAL(left));
	hyss_str2num(&second, ZSTR_VAL(right));
	hyss_str2num(&mod, ZSTR_VAL(modulous));

	scale_int = (int) ((int)scale < 0 ? 0 : scale);

	if (bc_raisemod(first, second, mod, &result, scale_int) != -1) {
		RETVAL_STR(bc_num2str_ex(result, scale_int));
	} else {
		RETVAL_FALSE;
	}

	bc_free_num(&first);
	bc_free_num(&second);
	bc_free_num(&mod);
	bc_free_num(&result);
	return;
}
/* }}} */

/* {{{ proto string bcpow(string x, string y [, int scale])
   Returns the value of an arbitrary precision number raised to the power of another */
HYSS_FUNCTION(bcpow)
{
	gear_string *left, *right;
	gear_long scale_param = 0;
	bc_num first, second, result;
	int scale = (int)BCG(bc_precision);

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(left)
		Z_PARAM_STR(right)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale_param)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 3) {
		scale = (int) ((int)scale_param < 0 ? 0 : scale_param);
	}

	bc_init_num(&first);
	bc_init_num(&second);
	bc_init_num(&result);
	hyss_str2num(&first, ZSTR_VAL(left));
	hyss_str2num(&second, ZSTR_VAL(right));
	bc_raise (first, second, &result, scale);

	RETVAL_STR(bc_num2str_ex(result, scale));
	bc_free_num(&first);
	bc_free_num(&second);
	bc_free_num(&result);
	return;
}
/* }}} */

/* {{{ proto string bcsqrt(string operand [, int scale])
   Returns the square root of an arbitray precision number */
HYSS_FUNCTION(bcsqrt)
{
	gear_string *left;
	gear_long scale_param = 0;
	bc_num result;
	int scale = (int)BCG(bc_precision);

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(left)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale_param)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 2) {
		scale = (int) ((int)scale_param < 0 ? 0 : scale_param);
	}

	bc_init_num(&result);
	hyss_str2num(&result, ZSTR_VAL(left));

	if (bc_sqrt (&result, scale) != 0) {
		RETVAL_STR(bc_num2str_ex(result, scale));
	} else {
		hyss_error_docref(NULL, E_WARNING, "Square root of negative number");
	}

	bc_free_num(&result);
	return;
}
/* }}} */

/* {{{ proto int bccomp(string left_operand, string right_operand [, int scale])
   Compares two arbitrary precision numbers */
HYSS_FUNCTION(bccomp)
{
	gear_string *left, *right;
	gear_long scale_param = 0;
	bc_num first, second;
	int scale = (int)BCG(bc_precision);

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(left)
		Z_PARAM_STR(right)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(scale_param)
	GEAR_PARSE_PARAMETERS_END();

	if (GEAR_NUM_ARGS() == 3) {
		scale = (int) ((int)scale_param < 0 ? 0 : scale_param);
	}

	bc_init_num(&first);
	bc_init_num(&second);

	bc_str2num(&first, ZSTR_VAL(left), scale);
	bc_str2num(&second, ZSTR_VAL(right), scale);
	RETVAL_LONG(bc_compare(first, second));

	bc_free_num(&first);
	bc_free_num(&second);
	return;
}
/* }}} */

/* {{{ proto int bcscale([int scale])
   Sets default scale parameter for all bc math functions */
HYSS_FUNCTION(bcscale)
{
	gear_long old_scale, new_scale;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(new_scale)
	GEAR_PARSE_PARAMETERS_END();

	old_scale = BCG(bc_precision);

	if (GEAR_NUM_ARGS() == 1) {
		BCG(bc_precision) = ((int)new_scale < 0) ? 0 : new_scale;
	}

	RETURN_LONG(old_scale);
}
/* }}} */


#endif

