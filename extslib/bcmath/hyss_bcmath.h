/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_BCMATH_H
#define HYSS_BCMATH_H

#include "libbcmath/src/bcmath.h"

extern gear_capi_entry bcmath_capi_entry;
#define hyssext_bcmath_ptr &bcmath_capi_entry

#include "hyss_version.h"
#define HYSS_BCMATH_VERSION HYSS_VERSION

HYSS_MINIT_FUNCTION(bcmath);
HYSS_MSHUTDOWN_FUNCTION(bcmath);
HYSS_MINFO_FUNCTION(bcmath);

HYSS_FUNCTION(bcadd);
HYSS_FUNCTION(bcsub);
HYSS_FUNCTION(bcmul);
HYSS_FUNCTION(bcdiv);
HYSS_FUNCTION(bcmod);
HYSS_FUNCTION(bcpow);
HYSS_FUNCTION(bcsqrt);
HYSS_FUNCTION(bccomp);
HYSS_FUNCTION(bcscale);
HYSS_FUNCTION(bcpowmod);

GEAR_BEGIN_CAPI_GLOBALS(bcmath)
	bc_num _zero_;
	bc_num _one_;
	bc_num _two_;
	gear_long bc_precision;
GEAR_END_CAPI_GLOBALS(bcmath)

#if defined(ZTS) && defined(COMPILE_DL_BCMATH)
GEAR_PBCLS_CACHE_EXTERN()
#endif

GEAR_EXTERN_CAPI_GLOBALS(bcmath)
#define BCG(v) GEAR_CAPI_GLOBALS_ACCESSOR(bcmath, v)

#endif /* HYSS_BCMATH_H */
