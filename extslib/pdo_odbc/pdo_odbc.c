/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "pdo/hyss_pdo.h"
#include "pdo/hyss_pdo_driver.h"
#include "hyss_pdo_odbc.h"
#include "hyss_pdo_odbc_int.h"

/* {{{ pdo_odbc_functions[] */
static const gear_function_entry pdo_odbc_functions[] = {
	HYSS_FE_END
};
/* }}} */

/* {{{ pdo_odbc_deps[] */
static const gear_capi_dep pdo_odbc_deps[] = {
	GEAR_CAPI_REQUIRED("pdo")
	GEAR_CAPI_END
};
/* }}} */

/* {{{ pdo_odbc_capi_entry */
gear_capi_entry pdo_odbc_capi_entry = {
	STANDARD_CAPI_HEADER_EX, NULL,
	pdo_odbc_deps,
	"PDO_ODBC",
	pdo_odbc_functions,
	HYSS_MINIT(pdo_odbc),
	HYSS_MSHUTDOWN(pdo_odbc),
	NULL,
	NULL,
	HYSS_MINFO(pdo_odbc),
	HYSS_PDO_ODBC_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_PDO_ODBC
GEAR_GET_CAPI(pdo_odbc)
#endif

#ifdef SQL_ATTR_CONNECTION_POOLING
gear_ulong pdo_odbc_pool_on = SQL_CP_OFF;
gear_ulong pdo_odbc_pool_mode = SQL_CP_ONE_PER_HENV;
#endif

#if defined(DB2CLI_VER) && !defined(HYSS_WIN32)
HYSS_ICS_BEGIN()
	HYSS_ICS_ENTRY("pdo_odbc.db2_instance_name", NULL, HYSS_ICS_SYSTEM, NULL)
HYSS_ICS_END()

#endif

/* {{{ HYSS_MINIT_FUNCTION */
HYSS_MINIT_FUNCTION(pdo_odbc)
{
#ifdef SQL_ATTR_CONNECTION_POOLING
	char *pooling_val = NULL;
#endif

	if (FAILURE == hyss_pdo_register_driver(&pdo_odbc_driver)) {
		return FAILURE;
	}

#if defined(DB2CLI_VER) && !defined(HYSS_WIN32)
	REGISTER_ICS_ENTRIES();
	{
		char *instance = ICS_STR("pdo_odbc.db2_instance_name");
		if (instance) {
			char *env = malloc(sizeof("DB2INSTANCE=") + strlen(instance));

			hyss_error_docref(NULL, E_DEPRECATED, "The pdo_odbc.db2_instance_name ics directive is deprecated and will be removed in the future");

			if (!env) {
				return FAILURE;
			}
			strcpy(env, "DB2INSTANCE=");
			strcat(env, instance);
			putenv(env);
			/* after this point, we can't free env without breaking the environment */
		}
	}
#endif

#ifdef SQL_ATTR_CONNECTION_POOLING
	/* ugh, we don't really like .ics stuff in PDO, but since ODBC connection
	 * pooling is process wide, we can't set it from within the scope of a
	 * request without affecting others, which goes against our isolated request
	 * policy.  So, we use cfg_get_string here to check it this once.
	 * */
	if (FAILURE == cfg_get_string("pdo_odbc.connection_pooling", &pooling_val) || pooling_val == NULL) {
		pooling_val = "strict";
	}
	if (strcasecmp(pooling_val, "strict") == 0 || strcmp(pooling_val, "1") == 0) {
		pdo_odbc_pool_on = SQL_CP_ONE_PER_HENV;
		pdo_odbc_pool_mode = SQL_CP_STRICT_MATCH;
	} else if (strcasecmp(pooling_val, "relaxed") == 0) {
		pdo_odbc_pool_on = SQL_CP_ONE_PER_HENV;
		pdo_odbc_pool_mode = SQL_CP_RELAXED_MATCH;
	} else if (*pooling_val == '\0' || strcasecmp(pooling_val, "off") == 0) {
		pdo_odbc_pool_on = SQL_CP_OFF;
	} else {
		hyss_error_docref(NULL, E_CORE_ERROR, "Error in pdo_odbc.connection_pooling configuration.  Value MUST be one of 'strict', 'relaxed' or 'off'");
		return FAILURE;
	}

	if (pdo_odbc_pool_on != SQL_CP_OFF) {
		SQLSetEnvAttr(SQL_NULL_HANDLE, SQL_ATTR_CONNECTION_POOLING, (void*)pdo_odbc_pool_on, 0);
	}
#endif

	REGISTER_PDO_CLASS_CONST_LONG("ODBC_ATTR_USE_CURSOR_LIBRARY", PDO_ODBC_ATTR_USE_CURSOR_LIBRARY);
	REGISTER_PDO_CLASS_CONST_LONG("ODBC_ATTR_ASSUME_UTF8", PDO_ODBC_ATTR_ASSUME_UTF8);
	REGISTER_PDO_CLASS_CONST_LONG("ODBC_SQL_USE_IF_NEEDED", SQL_CUR_USE_IF_NEEDED);
	REGISTER_PDO_CLASS_CONST_LONG("ODBC_SQL_USE_DRIVER", SQL_CUR_USE_DRIVER);
	REGISTER_PDO_CLASS_CONST_LONG("ODBC_SQL_USE_ODBC", SQL_CUR_USE_ODBC);

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION
 */
HYSS_MSHUTDOWN_FUNCTION(pdo_odbc)
{
#if defined(DB2CLI_VER) && !defined(HYSS_WIN32)
	UNREGISTER_ICS_ENTRIES();
#endif
	hyss_pdo_unregister_driver(&pdo_odbc_driver);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(pdo_odbc)
{
	hyss_info_print_table_start();
	hyss_info_print_table_header(2, "PDO Driver for ODBC (" PDO_ODBC_TYPE ")" , "enabled");
#ifdef SQL_ATTR_CONNECTION_POOLING
	hyss_info_print_table_row(2, "ODBC Connection Pooling",	pdo_odbc_pool_on == SQL_CP_OFF ?
			"Disabled" : (pdo_odbc_pool_mode == SQL_CP_STRICT_MATCH ? "Enabled, strict matching" : "Enabled, relaxed matching"));
#else
	hyss_info_print_table_row(2, "ODBC Connection Pooling", "Not supported in this build");
#endif
	hyss_info_print_table_end();

#if defined(DB2CLI_VER) && !defined(HYSS_WIN32)
	DISPLAY_ICS_ENTRIES();
#endif
}
/* }}} */
