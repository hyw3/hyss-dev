/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef	_INCLUDED_FTP_H
#define	_INCLUDED_FTP_H

#if HAVE_FTP

extern gear_capi_entry hyss_ftp_capi_entry;
#define hyss_ftp_capi_ptr &hyss_ftp_capi_entry

#include "hyss_version.h"
#define HYSS_FTP_VERSION HYSS_VERSION

#define HYSS_FTP_OPT_TIMEOUT_SEC	0
#define HYSS_FTP_OPT_AUTOSEEK	1
#define HYSS_FTP_OPT_USEPASVADDRESS	2
#define HYSS_FTP_AUTORESUME		-1

HYSS_MINIT_FUNCTION(ftp);
HYSS_MINFO_FUNCTION(ftp);

HYSS_FUNCTION(ftp_connect);
#ifdef HAVE_FTP_SSL
HYSS_FUNCTION(ftp_ssl_connect);
#endif
HYSS_FUNCTION(ftp_login);
HYSS_FUNCTION(ftp_pwd);
HYSS_FUNCTION(ftp_cdup);
HYSS_FUNCTION(ftp_chdir);
HYSS_FUNCTION(ftp_exec);
HYSS_FUNCTION(ftp_raw);
HYSS_FUNCTION(ftp_mkdir);
HYSS_FUNCTION(ftp_rmdir);
HYSS_FUNCTION(ftp_chmod);
HYSS_FUNCTION(ftp_alloc);
HYSS_FUNCTION(ftp_nlist);
HYSS_FUNCTION(ftp_rawlist);
HYSS_FUNCTION(ftp_mlsd);
HYSS_FUNCTION(ftp_systype);
HYSS_FUNCTION(ftp_pasv);
HYSS_FUNCTION(ftp_get);
HYSS_FUNCTION(ftp_fget);
HYSS_FUNCTION(ftp_put);
HYSS_FUNCTION(ftp_append);
HYSS_FUNCTION(ftp_fput);
HYSS_FUNCTION(ftp_size);
HYSS_FUNCTION(ftp_mdtm);
HYSS_FUNCTION(ftp_rename);
HYSS_FUNCTION(ftp_delete);
HYSS_FUNCTION(ftp_site);
HYSS_FUNCTION(ftp_close);
HYSS_FUNCTION(ftp_set_option);
HYSS_FUNCTION(ftp_get_option);
HYSS_FUNCTION(ftp_nb_get);
HYSS_FUNCTION(ftp_nb_fget);
HYSS_FUNCTION(ftp_nb_put);
HYSS_FUNCTION(ftp_nb_fput);
HYSS_FUNCTION(ftp_nb_continue);

#define hyssext_ftp_ptr hyss_ftp_capi_ptr

#else
#define hyss_ftp_capi_ptr NULL
#endif	/* HAVE_FTP */

#endif
