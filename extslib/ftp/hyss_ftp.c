/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#ifdef HAVE_FTP_SSL
# include <openssl/ssl.h>
#endif

#if HAVE_FTP

#include "extslib/standard/info.h"
#include "extslib/standard/file.h"

#include "hyss_ftp.h"
#include "ftp.h"

static int le_ftpbuf;
#define le_ftpbuf_name "FTP Buffer"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_connect, 0, 0, 1)
	GEAR_ARG_INFO(0, host)
	GEAR_ARG_INFO(0, port)
	GEAR_ARG_INFO(0, timeout)
GEAR_END_ARG_INFO()

#ifdef HAVE_FTP_SSL
GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_ssl_connect, 0, 0, 1)
	GEAR_ARG_INFO(0, host)
	GEAR_ARG_INFO(0, port)
	GEAR_ARG_INFO(0, timeout)
GEAR_END_ARG_INFO()
#endif

GEAR_BEGIN_ARG_INFO(arginfo_ftp_login, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, username)
	GEAR_ARG_INFO(0, password)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_pwd, 0)
	GEAR_ARG_INFO(0, ftp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_cdup, 0)
	GEAR_ARG_INFO(0, ftp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_chdir, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_exec, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, command)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_raw, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, command)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_mkdir, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_rmdir, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_chmod, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_alloc, 0, 0, 2)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, size)
	GEAR_ARG_INFO(1, response)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_nlist, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_rawlist, 0, 0, 2)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, directory)
	GEAR_ARG_INFO(0, recursive)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_mlsd, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, directory)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_systype, 0)
	GEAR_ARG_INFO(0, ftp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_fget, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, resumepos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_nb_fget, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, resumepos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_pasv, 0, 0, 2)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, pasv)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_get, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, local_file)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, resume_pos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_nb_get, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, local_file)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, resume_pos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_nb_continue, 0)
	GEAR_ARG_INFO(0, ftp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_fput, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, startpos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_nb_fput, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, fp)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, startpos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_put, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, local_file)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, startpos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_append, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, local_file)
	GEAR_ARG_INFO(0, mode)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_ftp_nb_put, 0, 0, 3)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, remote_file)
	GEAR_ARG_INFO(0, local_file)
	GEAR_ARG_INFO(0, mode)
	GEAR_ARG_INFO(0, startpos)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_size, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_mdtm, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, filename)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_rename, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, src)
	GEAR_ARG_INFO(0, dest)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_delete, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, file)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_site, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, cmd)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_close, 0)
	GEAR_ARG_INFO(0, ftp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_set_option, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, option)
	GEAR_ARG_INFO(0, value)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_ftp_get_option, 0)
	GEAR_ARG_INFO(0, ftp)
	GEAR_ARG_INFO(0, option)
GEAR_END_ARG_INFO()

/* }}} */

static const gear_function_entry hyss_ftp_functions[] = {
	HYSS_FE(ftp_connect,			arginfo_ftp_connect)
#ifdef HAVE_FTP_SSL
	HYSS_FE(ftp_ssl_connect,		arginfo_ftp_ssl_connect)
#endif
	HYSS_FE(ftp_login,			arginfo_ftp_login)
	HYSS_FE(ftp_pwd,				arginfo_ftp_pwd)
	HYSS_FE(ftp_cdup,			arginfo_ftp_cdup)
	HYSS_FE(ftp_chdir,			arginfo_ftp_chdir)
	HYSS_FE(ftp_exec,			arginfo_ftp_exec)
	HYSS_FE(ftp_raw,				arginfo_ftp_raw)
	HYSS_FE(ftp_mkdir,			arginfo_ftp_mkdir)
	HYSS_FE(ftp_rmdir,			arginfo_ftp_rmdir)
	HYSS_FE(ftp_chmod,			arginfo_ftp_chmod)
	HYSS_FE(ftp_alloc,			arginfo_ftp_alloc)
	HYSS_FE(ftp_nlist,			arginfo_ftp_nlist)
	HYSS_FE(ftp_rawlist,			arginfo_ftp_rawlist)
	HYSS_FE(ftp_mlsd,			arginfo_ftp_mlsd)
	HYSS_FE(ftp_systype,			arginfo_ftp_systype)
	HYSS_FE(ftp_pasv,			arginfo_ftp_pasv)
	HYSS_FE(ftp_get,				arginfo_ftp_get)
	HYSS_FE(ftp_fget,			arginfo_ftp_fget)
	HYSS_FE(ftp_put,				arginfo_ftp_put)
	HYSS_FE(ftp_append,			arginfo_ftp_append)
	HYSS_FE(ftp_fput,			arginfo_ftp_fput)
	HYSS_FE(ftp_size,			arginfo_ftp_size)
	HYSS_FE(ftp_mdtm,			arginfo_ftp_mdtm)
	HYSS_FE(ftp_rename,			arginfo_ftp_rename)
	HYSS_FE(ftp_delete,			arginfo_ftp_delete)
	HYSS_FE(ftp_site,			arginfo_ftp_site)
	HYSS_FE(ftp_close,			arginfo_ftp_close)
	HYSS_FE(ftp_set_option,		arginfo_ftp_set_option)
	HYSS_FE(ftp_get_option,		arginfo_ftp_get_option)
	HYSS_FE(ftp_nb_fget,			arginfo_ftp_nb_fget)
	HYSS_FE(ftp_nb_get,			arginfo_ftp_nb_get)
	HYSS_FE(ftp_nb_continue,		arginfo_ftp_nb_continue)
	HYSS_FE(ftp_nb_put,			arginfo_ftp_nb_put)
	HYSS_FE(ftp_nb_fput,			arginfo_ftp_nb_fput)
	HYSS_FALIAS(ftp_quit, ftp_close, arginfo_ftp_close)
	HYSS_FE_END
};

gear_capi_entry hyss_ftp_capi_entry = {
	STANDARD_CAPI_HEADER_EX,
	NULL,
	NULL,
	"ftp",
	hyss_ftp_functions,
	HYSS_MINIT(ftp),
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(ftp),
	HYSS_FTP_VERSION,
	STANDARD_CAPI_PROPERTIES
};

#if COMPILE_DL_FTP
GEAR_GET_CAPI(hyss_ftp)
#endif

static void ftp_destructor_ftpbuf(gear_resource *rsrc)
{
	ftpbuf_t *ftp = (ftpbuf_t *)rsrc->ptr;

	ftp_close(ftp);
}

HYSS_MINIT_FUNCTION(ftp)
{
#ifdef HAVE_FTP_SSL
	SSL_library_init();
	OpenSSL_add_all_ciphers();
	OpenSSL_add_all_digests();
	OpenSSL_add_all_algorithms();

	SSL_load_error_strings();
#endif

	le_ftpbuf = gear_register_list_destructors_ex(ftp_destructor_ftpbuf, NULL, le_ftpbuf_name, capi_number);
	REGISTER_LONG_CONSTANT("FTP_ASCII",  FTPTYPE_ASCII, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_TEXT",   FTPTYPE_ASCII, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_BINARY", FTPTYPE_IMAGE, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_IMAGE",  FTPTYPE_IMAGE, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_AUTORESUME", HYSS_FTP_AUTORESUME, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_TIMEOUT_SEC", HYSS_FTP_OPT_TIMEOUT_SEC, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_AUTOSEEK", HYSS_FTP_OPT_AUTOSEEK, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_USEPASVADDRESS", HYSS_FTP_OPT_USEPASVADDRESS, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_FAILED", HYSS_FTP_FAILED, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_FINISHED", HYSS_FTP_FINISHED, CONST_PERSISTENT | CONST_CS);
	REGISTER_LONG_CONSTANT("FTP_MOREDATA", HYSS_FTP_MOREDATA, CONST_PERSISTENT | CONST_CS);
	return SUCCESS;
}

HYSS_MINFO_FUNCTION(ftp)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "FTP support", "enabled");
#ifdef HAVE_FTP_SSL
	hyss_info_print_table_row(2, "FTPS support", "enabled");
#else
	hyss_info_print_table_row(2, "FTPS support", "disabled");
#endif
	hyss_info_print_table_end();
}

#define	XTYPE(xtype, mode)	{ \
								if (mode != FTPTYPE_ASCII && mode != FTPTYPE_IMAGE) { \
									hyss_error_docref(NULL, E_WARNING, "Mode must be FTP_ASCII or FTP_BINARY"); \
									RETURN_FALSE; \
								} \
								xtype = mode; \
							}


/* {{{ proto resource ftp_connect(string host [, int port [, int timeout]])
   Opens a FTP stream */
HYSS_FUNCTION(ftp_connect)
{
	ftpbuf_t	*ftp;
	char		*host;
	size_t		host_len;
	gear_long 		port = 0;
	gear_long		timeout_sec = FTP_DEFAULT_TIMEOUT;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s|ll", &host, &host_len, &port, &timeout_sec) == FAILURE) {
		return;
	}

	if (timeout_sec <= 0) {
		hyss_error_docref(NULL, E_WARNING, "Timeout has to be greater than 0");
		RETURN_FALSE;
	}

	/* connect */
	if (!(ftp = ftp_open(host, (short)port, timeout_sec))) {
		RETURN_FALSE;
	}

	/* autoseek for resuming */
	ftp->autoseek = FTP_DEFAULT_AUTOSEEK;
	ftp->usepasvaddress = FTP_DEFAULT_USEPASVADDRESS;
#ifdef HAVE_FTP_SSL
	/* disable ssl */
	ftp->use_ssl = 0;
#endif

	RETURN_RES(gear_register_resource(ftp, le_ftpbuf));
}
/* }}} */

#ifdef HAVE_FTP_SSL
/* {{{ proto resource ftp_ssl_connect(string host [, int port [, int timeout]])
   Opens a FTP-SSL stream */
HYSS_FUNCTION(ftp_ssl_connect)
{
	ftpbuf_t	*ftp;
	char		*host;
	size_t		host_len;
	gear_long		port = 0;
	gear_long		timeout_sec = FTP_DEFAULT_TIMEOUT;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s|ll", &host, &host_len, &port, &timeout_sec) == FAILURE) {
		return;
	}

	if (timeout_sec <= 0) {
		hyss_error_docref(NULL, E_WARNING, "Timeout has to be greater than 0");
		RETURN_FALSE;
	}

	/* connect */
	if (!(ftp = ftp_open(host, (short)port, timeout_sec))) {
		RETURN_FALSE;
	}

	/* autoseek for resuming */
	ftp->autoseek = FTP_DEFAULT_AUTOSEEK;
	ftp->usepasvaddress = FTP_DEFAULT_USEPASVADDRESS;
	/* enable ssl */
	ftp->use_ssl = 1;

	RETURN_RES(gear_register_resource(ftp, le_ftpbuf));
}
/* }}} */
#endif

/* {{{ proto bool ftp_login(resource stream, string username, string password)
   Logs into the FTP server */
HYSS_FUNCTION(ftp_login)
{
	zval 		*z_ftp;
	ftpbuf_t	*ftp;
	char *user, *pass;
	size_t user_len, pass_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rss", &z_ftp, &user, &user_len, &pass, &pass_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* log in */
	if (!ftp_login(ftp, user, user_len, pass, pass_len)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto string ftp_pwd(resource stream)
   Returns the present working directory */
HYSS_FUNCTION(ftp_pwd)
{
	zval 		*z_ftp;
	ftpbuf_t	*ftp;
	const char	*pwd;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &z_ftp) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	if (!(pwd = ftp_pwd(ftp))) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_STRING((char*) pwd);
}
/* }}} */

/* {{{ proto bool ftp_cdup(resource stream)
   Changes to the parent directory */
HYSS_FUNCTION(ftp_cdup)
{
	zval 		*z_ftp;
	ftpbuf_t	*ftp;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &z_ftp) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	if (!ftp_cdup(ftp)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool ftp_chdir(resource stream, string directory)
   Changes directories */
HYSS_FUNCTION(ftp_chdir)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*dir;
	size_t			dir_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &z_ftp, &dir, &dir_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* change directories */
	if (!ftp_chdir(ftp, dir, dir_len)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool ftp_exec(resource stream, string command)
   Requests execution of a program on the FTP server */
HYSS_FUNCTION(ftp_exec)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*cmd;
	size_t			cmd_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &z_ftp, &cmd, &cmd_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* execute serverside command */
	if (!ftp_exec(ftp, cmd, cmd_len)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto array ftp_raw(resource stream, string command)
   Sends a literal command to the FTP server */
HYSS_FUNCTION(ftp_raw)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*cmd;
	size_t			cmd_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &z_ftp, &cmd, &cmd_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* execute arbitrary ftp command */
	ftp_raw(ftp, cmd, cmd_len, return_value);
}
/* }}} */

/* {{{ proto string ftp_mkdir(resource stream, string directory)
   Creates a directory and returns the absolute path for the new directory or false on error */
HYSS_FUNCTION(ftp_mkdir)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*dir;
	gear_string *tmp;
	size_t		dir_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &z_ftp, &dir, &dir_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* create directory */
	if (NULL == (tmp = ftp_mkdir(ftp, dir, dir_len))) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_STR(tmp);
}
/* }}} */

/* {{{ proto bool ftp_rmdir(resource stream, string directory)
   Removes a directory */
HYSS_FUNCTION(ftp_rmdir)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*dir;
	size_t		dir_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &z_ftp, &dir, &dir_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* remove directorie */
	if (!ftp_rmdir(ftp, dir, dir_len)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int ftp_chmod(resource stream, int mode, string filename)
   Sets permissions on a file */
HYSS_FUNCTION(ftp_chmod)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*filename;
	size_t		filename_len;
	gear_long		mode;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rlp", &z_ftp, &mode, &filename, &filename_len) == FAILURE) {
		RETURN_FALSE;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	if (!ftp_chmod(ftp, mode, filename, filename_len)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_LONG(mode);
}
/* }}} */

/* {{{ proto bool ftp_alloc(resource stream, int size[, &response])
   Attempt to allocate space on the remote FTP server */
HYSS_FUNCTION(ftp_alloc)
{
	zval		*z_ftp, *zresponse = NULL;
	ftpbuf_t	*ftp;
	gear_long		size, ret;
	gear_string	*response = NULL;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rl|z/", &z_ftp, &size, &zresponse) == FAILURE) {
		RETURN_FALSE;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	ret = ftp_alloc(ftp, size, zresponse ? &response : NULL);
	if (response) {
		zval_ptr_dtor(zresponse);
		ZVAL_STR(zresponse, response);
	}

	if (!ret) {
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto array ftp_nlist(resource stream, string directory)
   Returns an array of filenames in the given directory */
HYSS_FUNCTION(ftp_nlist)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		**nlist, **ptr, *dir;
	size_t		dir_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rp", &z_ftp, &dir, &dir_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* get list of files */
	if (NULL == (nlist = ftp_nlist(ftp, dir, dir_len))) {
		RETURN_FALSE;
	}

	array_init(return_value);
	for (ptr = nlist; *ptr; ptr++) {
		add_next_index_string(return_value, *ptr);
	}
	efree(nlist);
}
/* }}} */

/* {{{ proto array ftp_rawlist(resource stream, string directory [, bool recursive])
   Returns a detailed listing of a directory as an array of output lines */
HYSS_FUNCTION(ftp_rawlist)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		**llist, **ptr, *dir;
	size_t		dir_len;
	gear_bool	recursive = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs|b", &z_ftp, &dir, &dir_len, &recursive) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* get raw directory listing */
	if (NULL == (llist = ftp_list(ftp, dir, dir_len, recursive))) {
		RETURN_FALSE;
	}

	array_init(return_value);
	for (ptr = llist; *ptr; ptr++) {
		add_next_index_string(return_value, *ptr);
	}
	efree(llist);
}
/* }}} */

/* {{{ proto array ftp_mlsd(resource stream, string directory)
   Returns a detailed listing of a directory as an array of parsed output lines */
HYSS_FUNCTION(ftp_mlsd)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		**llist, **ptr, *dir;
	size_t		dir_len;
	zval		entry;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &z_ftp, &dir, &dir_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* get raw directory listing */
	if (NULL == (llist = ftp_mlsd(ftp, dir, dir_len))) {
		RETURN_FALSE;
	}

	array_init(return_value);
	for (ptr = llist; *ptr; ptr++) {
		array_init(&entry);
		if (ftp_mlsd_parse_line(Z_ARRVAL_P(&entry), *ptr) == SUCCESS) {
			gear_hash_next_index_insert(Z_ARRVAL_P(return_value), &entry);
		} else {
			zval_ptr_dtor(&entry);
		}
	}

	efree(llist);
}
/* }}} */

/* {{{ proto string ftp_systype(resource stream)
   Returns the system type identifier */
HYSS_FUNCTION(ftp_systype)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	const char	*syst;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &z_ftp) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	if (NULL == (syst = ftp_syst(ftp))) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_STRING((char*) syst);
}
/* }}} */

/* {{{ proto bool ftp_fget(resource stream, resource fp, string remote_file, [, int mode [, int resumepos]])
   Retrieves a file from the FTP server and writes it to an open file */
HYSS_FUNCTION(ftp_fget)
{
	zval		*z_ftp, *z_file;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	hyss_stream	*stream;
	char		*file;
	size_t		file_len;
	gear_long		mode=FTPTYPE_IMAGE, resumepos=0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rrs|ll", &z_ftp, &z_file, &file, &file_len, &mode, &resumepos) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	hyss_stream_from_res(stream, Z_RES_P(z_file));
	XTYPE(xtype, mode);

	/* ignore autoresume if autoseek is switched off */
	if (!ftp->autoseek && resumepos == HYSS_FTP_AUTORESUME) {
		resumepos = 0;
	}

	if (ftp->autoseek && resumepos) {
		/* if autoresume is wanted seek to end */
		if (resumepos == HYSS_FTP_AUTORESUME) {
			hyss_stream_seek(stream, 0, SEEK_END);
			resumepos = hyss_stream_tell(stream);
		} else {
			hyss_stream_seek(stream, resumepos, SEEK_SET);
		}
	}

	if (!ftp_get(ftp, stream, file, file_len, xtype, resumepos)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int ftp_nb_fget(resource stream, resource fp, string remote_file [, int mode [, int resumepos]])
   Retrieves a file from the FTP server asynchronly and writes it to an open file */
HYSS_FUNCTION(ftp_nb_fget)
{
	zval		*z_ftp, *z_file;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	hyss_stream	*stream;
	char		*file;
	size_t		file_len;
	gear_long		mode=FTPTYPE_IMAGE, resumepos=0, ret;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rrs|ll", &z_ftp, &z_file, &file, &file_len, &mode, &resumepos) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	hyss_stream_from_res(stream, Z_RES_P(z_file));
	XTYPE(xtype, mode);

	/* ignore autoresume if autoseek is switched off */
	if (!ftp->autoseek && resumepos == HYSS_FTP_AUTORESUME) {
		resumepos = 0;
	}

	if (ftp->autoseek && resumepos) {
		/* if autoresume is wanted seek to end */
		if (resumepos == HYSS_FTP_AUTORESUME) {
			hyss_stream_seek(stream, 0, SEEK_END);
			resumepos = hyss_stream_tell(stream);
		} else {
			hyss_stream_seek(stream, resumepos, SEEK_SET);
		}
	}

	/* configuration */
	ftp->direction = 0;   /* recv */
	ftp->closestream = 0; /* do not close */

	if ((ret = ftp_nb_get(ftp, stream, file, file_len, xtype, resumepos)) == HYSS_FTP_FAILED) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_LONG(ret);
	}

	RETURN_LONG(ret);
}
/* }}} */

/* {{{ proto bool ftp_pasv(resource stream, bool pasv)
   Turns passive mode on or off */
HYSS_FUNCTION(ftp_pasv)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	gear_bool	pasv;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rb", &z_ftp, &pasv) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	if (!ftp_pasv(ftp, pasv ? 1 : 0)) {
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool ftp_get(resource stream, string local_file, string remote_file [, int mode [, int resume_pos]])
   Retrieves a file from the FTP server and writes it to a local file */
HYSS_FUNCTION(ftp_get)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	hyss_stream	*outstream;
	char		*local, *remote;
	size_t		local_len, remote_len;
	gear_long		mode=FTPTYPE_IMAGE, resumepos=0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rpp|ll", &z_ftp, &local, &local_len, &remote, &remote_len, &mode, &resumepos) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	XTYPE(xtype, mode);

	/* ignore autoresume if autoseek is switched off */
	if (!ftp->autoseek && resumepos == HYSS_FTP_AUTORESUME) {
		resumepos = 0;
	}

#ifdef HYSS_WIN32
	mode = FTPTYPE_IMAGE;
#endif

	if (ftp->autoseek && resumepos) {
		outstream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "rt+" : "rb+", REPORT_ERRORS, NULL);
		if (outstream == NULL) {
			outstream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "wt" : "wb", REPORT_ERRORS, NULL);
		}
		if (outstream != NULL) {
			/* if autoresume is wanted seek to end */
			if (resumepos == HYSS_FTP_AUTORESUME) {
				hyss_stream_seek(outstream, 0, SEEK_END);
				resumepos = hyss_stream_tell(outstream);
			} else {
				hyss_stream_seek(outstream, resumepos, SEEK_SET);
			}
		}
	} else {
		outstream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "wt" : "wb", REPORT_ERRORS, NULL);
	}

	if (outstream == NULL)	{
		hyss_error_docref(NULL, E_WARNING, "Error opening %s", local);
		RETURN_FALSE;
	}

	if (!ftp_get(ftp, outstream, remote, remote_len, xtype, resumepos)) {
		hyss_stream_close(outstream);
		VCWD_UNLINK(local);
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	hyss_stream_close(outstream);
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int ftp_nb_get(resource stream, string local_file, string remote_file, [, int mode [, int resume_pos]])
   Retrieves a file from the FTP server nbhronly and writes it to a local file */
HYSS_FUNCTION(ftp_nb_get)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	hyss_stream	*outstream;
	char		*local, *remote;
	size_t		local_len, remote_len;
	int ret;
	gear_long		mode=FTPTYPE_IMAGE, resumepos=0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rss|ll", &z_ftp, &local, &local_len, &remote, &remote_len, &mode, &resumepos) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	XTYPE(xtype, mode);

	/* ignore autoresume if autoseek is switched off */
	if (!ftp->autoseek && resumepos == HYSS_FTP_AUTORESUME) {
		resumepos = 0;
	}
#ifdef HYSS_WIN32
	mode = FTPTYPE_IMAGE;
#endif
	if (ftp->autoseek && resumepos) {
		outstream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "rt+" : "rb+", REPORT_ERRORS, NULL);
		if (outstream == NULL) {
			outstream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "wt" : "wb", REPORT_ERRORS, NULL);
		}
		if (outstream != NULL) {
			/* if autoresume is wanted seek to end */
			if (resumepos == HYSS_FTP_AUTORESUME) {
				hyss_stream_seek(outstream, 0, SEEK_END);
				resumepos = hyss_stream_tell(outstream);
			} else {
				hyss_stream_seek(outstream, resumepos, SEEK_SET);
			}
		}
	} else {
		outstream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "wt" : "wb", REPORT_ERRORS, NULL);
	}

	if (outstream == NULL)	{
		hyss_error_docref(NULL, E_WARNING, "Error opening %s", local);
		RETURN_FALSE;
	}

	/* configuration */
	ftp->direction = 0;   /* recv */
	ftp->closestream = 1; /* do close */

	if ((ret = ftp_nb_get(ftp, outstream, remote, remote_len, xtype, resumepos)) == HYSS_FTP_FAILED) {
		hyss_stream_close(outstream);
		ftp->stream = NULL;
		VCWD_UNLINK(local);
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_LONG(HYSS_FTP_FAILED);
	}

	if (ret == HYSS_FTP_FINISHED){
		hyss_stream_close(outstream);
		ftp->stream = NULL;
	}

	RETURN_LONG(ret);
}
/* }}} */

/* {{{ proto int ftp_nb_continue(resource stream)
   Continues retrieving/sending a file nbronously */
HYSS_FUNCTION(ftp_nb_continue)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	gear_long		ret;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &z_ftp) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	if (!ftp->nb) {
		hyss_error_docref(NULL, E_WARNING, "no nbronous transfer to continue.");
		RETURN_LONG(HYSS_FTP_FAILED);
	}

	if (ftp->direction) {
		ret=ftp_nb_continue_write(ftp);
	} else {
		ret=ftp_nb_continue_read(ftp);
	}

	if (ret != HYSS_FTP_MOREDATA && ftp->closestream) {
		hyss_stream_close(ftp->stream);
		ftp->stream = NULL;
	}

	if (ret == HYSS_FTP_FAILED) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
	}

	RETURN_LONG(ret);
}
/* }}} */

/* {{{ proto bool ftp_fput(resource stream, string remote_file, resource fp [, int mode [, int startpos]])
   Stores a file from an open file to the FTP server */
HYSS_FUNCTION(ftp_fput)
{
	zval		*z_ftp, *z_file;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	size_t		remote_len;
	gear_long		mode=FTPTYPE_IMAGE, startpos=0;
	hyss_stream	*stream;
	char		*remote;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rsr|ll", &z_ftp, &remote, &remote_len, &z_file, &mode, &startpos) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	hyss_stream_from_zval(stream, z_file);
	XTYPE(xtype, mode);

	/* ignore autoresume if autoseek is switched off */
	if (!ftp->autoseek && startpos == HYSS_FTP_AUTORESUME) {
		startpos = 0;
	}

	if (ftp->autoseek && startpos) {
		/* if autoresume is wanted ask for remote size */
		if (startpos == HYSS_FTP_AUTORESUME) {
			startpos = ftp_size(ftp, remote, remote_len);
			if (startpos < 0) {
				startpos = 0;
			}
		}
		if (startpos) {
			hyss_stream_seek(stream, startpos, SEEK_SET);
		}
	}

	if (!ftp_put(ftp, remote, remote_len, stream, xtype, startpos)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int ftp_nb_fput(resource stream, string remote_file, resource fp [, int mode [, int startpos]])
   Stores a file from an open file to the FTP server nbronly */
HYSS_FUNCTION(ftp_nb_fput)
{
	zval		*z_ftp, *z_file;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	size_t		remote_len;
	int             ret;
	gear_long	mode=FTPTYPE_IMAGE, startpos=0;
	hyss_stream	*stream;
	char		*remote;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rsr|ll", &z_ftp, &remote, &remote_len, &z_file, &mode, &startpos) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	hyss_stream_from_res(stream, Z_RES_P(z_file));
	XTYPE(xtype, mode);

	/* ignore autoresume if autoseek is switched off */
	if (!ftp->autoseek && startpos == HYSS_FTP_AUTORESUME) {
		startpos = 0;
	}

	if (ftp->autoseek && startpos) {
		/* if autoresume is wanted ask for remote size */
		if (startpos == HYSS_FTP_AUTORESUME) {
			startpos = ftp_size(ftp, remote, remote_len);
			if (startpos < 0) {
				startpos = 0;
			}
		}
		if (startpos) {
			hyss_stream_seek(stream, startpos, SEEK_SET);
		}
	}

	/* configuration */
	ftp->direction = 1;   /* send */
	ftp->closestream = 0; /* do not close */

	if (((ret = ftp_nb_put(ftp, remote, remote_len, stream, xtype, startpos)) == HYSS_FTP_FAILED)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_LONG(ret);
	}

	RETURN_LONG(ret);
}
/* }}} */


/* {{{ proto bool ftp_put(resource stream, string remote_file, string local_file [, int mode [, int startpos]])
   Stores a file on the FTP server */
HYSS_FUNCTION(ftp_put)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	char		*remote, *local;
	size_t		remote_len, local_len;
	gear_long		mode=FTPTYPE_IMAGE, startpos=0;
	hyss_stream 	*instream;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rpp|ll", &z_ftp, &remote, &remote_len, &local, &local_len, &mode, &startpos) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	XTYPE(xtype, mode);

	if (!(instream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "rt" : "rb", REPORT_ERRORS, NULL))) {
		RETURN_FALSE;
	}

	/* ignore autoresume if autoseek is switched off */
	if (!ftp->autoseek && startpos == HYSS_FTP_AUTORESUME) {
		startpos = 0;
	}

	if (ftp->autoseek && startpos) {
		/* if autoresume is wanted ask for remote size */
		if (startpos == HYSS_FTP_AUTORESUME) {
			startpos = ftp_size(ftp, remote, remote_len);
			if (startpos < 0) {
				startpos = 0;
			}
		}
		if (startpos) {
			hyss_stream_seek(instream, startpos, SEEK_SET);
		}
	}

	if (!ftp_put(ftp, remote, remote_len, instream, xtype, startpos)) {
		hyss_stream_close(instream);
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}
	hyss_stream_close(instream);

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool ftp_append(resource stream, string remote_file, string local_file [, int mode])
   Append content of a file a another file on the FTP server */
HYSS_FUNCTION(ftp_append)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	char		*remote, *local;
	size_t		remote_len, local_len;
	gear_long		mode=FTPTYPE_IMAGE;
	hyss_stream 	*instream;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rpp|l", &z_ftp, &remote, &remote_len, &local, &local_len, &mode) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	XTYPE(xtype, mode);

	if (!(instream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "rt" : "rb", REPORT_ERRORS, NULL))) {
		RETURN_FALSE;
	}

	if (!ftp_append(ftp, remote, remote_len, instream, xtype)) {
		hyss_stream_close(instream);
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}
	hyss_stream_close(instream);

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto int ftp_nb_put(resource stream, string remote_file, string local_file [, int mode [, int startpos]])
   Stores a file on the FTP server */
HYSS_FUNCTION(ftp_nb_put)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	ftptype_t	xtype;
	char		*remote, *local;
	size_t		remote_len, local_len;
	gear_long		mode=FTPTYPE_IMAGE, startpos=0, ret;
	hyss_stream 	*instream;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rpp|ll", &z_ftp, &remote, &remote_len, &local, &local_len, &mode, &startpos) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}
	XTYPE(xtype, mode);

	if (!(instream = hyss_stream_open_wrapper(local, mode == FTPTYPE_ASCII ? "rt" : "rb", REPORT_ERRORS, NULL))) {
		RETURN_FALSE;
	}

	/* ignore autoresume if autoseek is switched off */
	if (!ftp->autoseek && startpos == HYSS_FTP_AUTORESUME) {
		startpos = 0;
	}

	if (ftp->autoseek && startpos) {
		/* if autoresume is wanted ask for remote size */
		if (startpos == HYSS_FTP_AUTORESUME) {
			startpos = ftp_size(ftp, remote, remote_len);
			if (startpos < 0) {
				startpos = 0;
			}
		}
		if (startpos) {
			hyss_stream_seek(instream, startpos, SEEK_SET);
		}
	}

	/* configuration */
	ftp->direction = 1;   /* send */
	ftp->closestream = 1; /* do close */

	ret = ftp_nb_put(ftp, remote, remote_len, instream, xtype, startpos);

	if (ret != HYSS_FTP_MOREDATA) {
		hyss_stream_close(instream);
		ftp->stream = NULL;
	}

	if (ret == HYSS_FTP_FAILED) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
	}

	RETURN_LONG(ret);
}
/* }}} */

/* {{{ proto int ftp_size(resource stream, string filename)
   Returns the size of the file, or -1 on error */
HYSS_FUNCTION(ftp_size)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*file;
	size_t		file_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rp", &z_ftp, &file, &file_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* get file size */
	RETURN_LONG(ftp_size(ftp, file, file_len));
}
/* }}} */

/* {{{ proto int ftp_mdtm(resource stream, string filename)
   Returns the last modification time of the file, or -1 on error */
HYSS_FUNCTION(ftp_mdtm)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*file;
	size_t		file_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rp", &z_ftp, &file, &file_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* get file mod time */
	RETURN_LONG(ftp_mdtm(ftp, file, file_len));
}
/* }}} */

/* {{{ proto bool ftp_rename(resource stream, string src, string dest)
   Renames the given file to a new path */
HYSS_FUNCTION(ftp_rename)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*src, *dest;
	size_t		src_len, dest_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rss", &z_ftp, &src, &src_len, &dest, &dest_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* rename the file */
	if (!ftp_rename(ftp, src, src_len, dest, dest_len)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool ftp_delete(resource stream, string file)
   Deletes a file */
HYSS_FUNCTION(ftp_delete)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*file;
	size_t		file_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &z_ftp, &file, &file_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* delete the file */
	if (!ftp_delete(ftp, file, file_len)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool ftp_site(resource stream, string cmd)
   Sends a SITE command to the server */
HYSS_FUNCTION(ftp_site)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;
	char		*cmd;
	size_t		cmd_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rs", &z_ftp, &cmd, &cmd_len) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	/* send the site command */
	if (!ftp_site(ftp, cmd, cmd_len)) {
		hyss_error_docref(NULL, E_WARNING, "%s", ftp->inbuf);
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool ftp_close(resource stream)
   Closes the FTP stream */
HYSS_FUNCTION(ftp_close)
{
	zval		*z_ftp;
	ftpbuf_t	*ftp;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "r", &z_ftp) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	ftp_quit(ftp);

	RETURN_BOOL(gear_list_close(Z_RES_P(z_ftp)) == SUCCESS);
}
/* }}} */

/* {{{ proto bool ftp_set_option(resource stream, int option, mixed value)
   Sets an FTP option */
HYSS_FUNCTION(ftp_set_option)
{
	zval		*z_ftp, *z_value;
	gear_long		option;
	ftpbuf_t	*ftp;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rlz", &z_ftp, &option, &z_value) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	switch (option) {
		case HYSS_FTP_OPT_TIMEOUT_SEC:
			if (Z_TYPE_P(z_value) != IS_LONG) {
				hyss_error_docref(NULL, E_WARNING, "Option TIMEOUT_SEC expects value of type int, %s given",
					gear_zval_type_name(z_value));
				RETURN_FALSE;
			}
			if (Z_LVAL_P(z_value) <= 0) {
				hyss_error_docref(NULL, E_WARNING, "Timeout has to be greater than 0");
				RETURN_FALSE;
			}
			ftp->timeout_sec = Z_LVAL_P(z_value);
			RETURN_TRUE;
			break;
		case HYSS_FTP_OPT_AUTOSEEK:
			if (Z_TYPE_P(z_value) != IS_TRUE && Z_TYPE_P(z_value) != IS_FALSE) {
				hyss_error_docref(NULL, E_WARNING, "Option AUTOSEEK expects value of type bool, %s given",
					gear_zval_type_name(z_value));
				RETURN_FALSE;
			}
			ftp->autoseek = Z_TYPE_P(z_value) == IS_TRUE ? 1 : 0;
			RETURN_TRUE;
			break;
		case HYSS_FTP_OPT_USEPASVADDRESS:
			if (Z_TYPE_P(z_value) != IS_TRUE && Z_TYPE_P(z_value) != IS_FALSE) {
				hyss_error_docref(NULL, E_WARNING, "Option USEPASVADDRESS expects value of type bool, %s given",
					gear_zval_type_name(z_value));
				RETURN_FALSE;
			}
			ftp->usepasvaddress = Z_TYPE_P(z_value) == IS_TRUE ? 1 : 0;
			RETURN_TRUE;
			break;
		default:
			hyss_error_docref(NULL, E_WARNING, "Unknown option '" GEAR_LONG_FMT "'", option);
			RETURN_FALSE;
			break;
	}
}
/* }}} */

/* {{{ proto mixed ftp_get_option(resource stream, int option)
   Gets an FTP option */
HYSS_FUNCTION(ftp_get_option)
{
	zval		*z_ftp;
	gear_long		option;
	ftpbuf_t	*ftp;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "rl", &z_ftp, &option) == FAILURE) {
		return;
	}

	if ((ftp = (ftpbuf_t *)gear_fetch_resource(Z_RES_P(z_ftp), le_ftpbuf_name, le_ftpbuf)) == NULL) {
		RETURN_FALSE;
	}

	switch (option) {
		case HYSS_FTP_OPT_TIMEOUT_SEC:
			RETURN_LONG(ftp->timeout_sec);
			break;
		case HYSS_FTP_OPT_AUTOSEEK:
			RETURN_BOOL(ftp->autoseek);
			break;
		case HYSS_FTP_OPT_USEPASVADDRESS:
			RETURN_BOOL(ftp->usepasvaddress);
			break;
		default:
			hyss_error_docref(NULL, E_WARNING, "Unknown option '" GEAR_LONG_FMT "'", option);
			RETURN_FALSE;
			break;
	}
}
/* }}} */

#endif /* HAVE_FTP */

