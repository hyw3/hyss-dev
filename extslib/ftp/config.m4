dnl config.m4 for extension ftp

HYSS_ARG_ENABLE(ftp,whether to enable FTP support,
[  --enable-ftp            Enable FTP support])

HYSS_ARG_WITH(openssl-dir,OpenSSL dir for FTP,
[  --with-openssl-dir[=DIR]  FTP: openssl install prefix], no, no)

if test "$HYSS_FTP" = "yes"; then
  AC_DEFINE(HAVE_FTP,1,[Whether you want FTP support])
  HYSS_NEW_EXTENSION(ftp, hyss_ftp.c ftp.c, $ext_shared)

  dnl Empty variable means 'no'
  test -z "$HYSS_OPENSSL" && HYSS_OPENSSL=no

  if test "$HYSS_OPENSSL" != "no" || test "$HYSS_OPENSSL_DIR" != "no"; then
    HYSS_SETUP_OPENSSL(FTP_SHARED_LIBADD)
    HYSS_SUBST(FTP_SHARED_LIBADD)
    AC_DEFINE(HAVE_FTP_SSL,1,[Whether FTP over SSL is supported])
  fi
fi
