/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define GEAR_INCLUDE_FULL_WINDOWS_HEADERS

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if HAVE_CURL

#include "hyss_curl.h"

#include <curl/curl.h>

#define SAVE_CURLSH_ERROR(__handle, __err) (__handle)->err.no = (int) __err;

/* {{{ proto void curl_share_init()
   Initialize a share curl handle */
HYSS_FUNCTION(curl_share_init)
{
	hyss_curlsh *sh;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	sh = ecalloc(1, sizeof(hyss_curlsh));

	sh->share = curl_share_init();

	RETURN_RES(gear_register_resource(sh, le_curl_share_handle));
}
/* }}} */

/* {{{ proto void curl_share_close(resource sh)
   Close a set of cURL handles */
HYSS_FUNCTION(curl_share_close)
{
	zval *z_sh;
	hyss_curlsh *sh;

	GEAR_PARSE_PARAMETERS_START(1,1)
		Z_PARAM_RESOURCE(z_sh)
	GEAR_PARSE_PARAMETERS_END();

	if ((sh = (hyss_curlsh *)gear_fetch_resource(Z_RES_P(z_sh), le_curl_share_handle_name, le_curl_share_handle)) == NULL) {
		RETURN_FALSE;
	}

	gear_list_close(Z_RES_P(z_sh));
}
/* }}} */

static int _hyss_curl_share_setopt(hyss_curlsh *sh, gear_long option, zval *zvalue, zval *return_value) /* {{{ */
{
	CURLSHcode error = CURLSHE_OK;

	switch (option) {
		case CURLSHOPT_SHARE:
		case CURLSHOPT_UNSHARE:
			error = curl_share_setopt(sh->share, option, zval_get_long(zvalue));
			break;

		default:
			hyss_error_docref(NULL, E_WARNING, "Invalid curl share configuration option");
			error = CURLSHE_BAD_OPTION;
			break;
	}

	SAVE_CURLSH_ERROR(sh, error);
	if (error != CURLSHE_OK) {
		return 1;
	} else {
		return 0;
	}
}
/* }}} */

/* {{{ proto bool curl_share_setopt(resource sh, int option, mixed value)
      Set an option for a cURL transfer */
HYSS_FUNCTION(curl_share_setopt)
{
	zval       *zid, *zvalue;
	gear_long        options;
	hyss_curlsh *sh;

	GEAR_PARSE_PARAMETERS_START(3,3)
		Z_PARAM_RESOURCE(zid)
		Z_PARAM_LONG(options)
		Z_PARAM_ZVAL(zvalue)
	GEAR_PARSE_PARAMETERS_END();

	if ((sh = (hyss_curlsh *)gear_fetch_resource(Z_RES_P(zid), le_curl_share_handle_name, le_curl_share_handle)) == NULL) {
		RETURN_FALSE;
	}

	if (!_hyss_curl_share_setopt(sh, options, zvalue, return_value)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

void _hyss_curl_share_close(gear_resource *rsrc) /* {{{ */
{
	hyss_curlsh *sh = (hyss_curlsh *)rsrc->ptr;
	if (sh) {
		curl_share_cleanup(sh->share);
		efree(sh);
		rsrc->ptr = NULL;
	}
}
/* }}} */

/* {{{ proto int curl_share_errno(resource mh)
         Return an integer containing the last share curl error number */
HYSS_FUNCTION(curl_share_errno)
{
	zval        *z_sh;
	hyss_curlsh  *sh;

	GEAR_PARSE_PARAMETERS_START(1,1)
		Z_PARAM_RESOURCE(z_sh)
	GEAR_PARSE_PARAMETERS_END();

	if ((sh = (hyss_curlsh *)gear_fetch_resource(Z_RES_P(z_sh), le_curl_share_handle_name, le_curl_share_handle)) == NULL) {
		RETURN_FALSE;
	}

	RETURN_LONG(sh->err.no);
}
/* }}} */


/* {{{ proto bool curl_share_strerror(int code)
         return string describing error code */
HYSS_FUNCTION(curl_share_strerror)
{
	gear_long code;
	const char *str;

	GEAR_PARSE_PARAMETERS_START(1,1)
		Z_PARAM_LONG(code)
	GEAR_PARSE_PARAMETERS_END();

	str = curl_share_strerror(code);
	if (str) {
		RETURN_STRING(str);
	} else {
		RETURN_NULL();
	}
}
/* }}} */

#endif

