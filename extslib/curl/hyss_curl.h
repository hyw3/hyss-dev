/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HYSS_CURL_H
#define _HYSS_CURL_H

#include "hyss.h"
#include "gear_smart_str.h"

#ifdef COMPILE_DL_CURL
#undef HAVE_CURL
#define HAVE_CURL 1
#endif

#if HAVE_CURL

#define HYSS_CURL_DEBUG 0

#ifdef HYSS_WIN32
# define HYSS_CURL_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
# define HYSS_CURL_API __attribute__ ((visibility("default")))
#else
# define HYSS_CURL_API
#endif

#include "hyss_version.h"
#define HYSS_CURL_VERSION HYSS_VERSION

#include <curl/curl.h>
#include <curl/multi.h>

extern gear_capi_entry curl_capi_entry;
#define curl_capi_ptr &curl_capi_entry

#define CURLOPT_RETURNTRANSFER 19913
#define CURLOPT_BINARYTRANSFER 19914 /* For Backward compatibility */
#define HYSS_CURL_STDOUT 0
#define HYSS_CURL_FILE   1
#define HYSS_CURL_USER   2
#define HYSS_CURL_DIRECT 3
#define HYSS_CURL_RETURN 4
#define HYSS_CURL_IGNORE 7

extern int  le_curl;
#define le_curl_name "cURL handle"
extern int  le_curl_multi_handle;
#define le_curl_multi_handle_name "cURL Multi Handle"
extern int  le_curl_share_handle;
#define le_curl_share_handle_name "cURL Share Handle"
//extern int  le_curl_pushheaders;
//#define le_curl_pushheaders "cURL Push Headers"

HYSS_MINIT_FUNCTION(curl);
HYSS_MSHUTDOWN_FUNCTION(curl);
HYSS_MINFO_FUNCTION(curl);

HYSS_FUNCTION(curl_close);
HYSS_FUNCTION(curl_copy_handle);
HYSS_FUNCTION(curl_errno);
HYSS_FUNCTION(curl_error);
HYSS_FUNCTION(curl_exec);
HYSS_FUNCTION(curl_getinfo);
HYSS_FUNCTION(curl_init);
HYSS_FUNCTION(curl_setopt);
HYSS_FUNCTION(curl_setopt_array);
HYSS_FUNCTION(curl_version);

HYSS_FUNCTION(curl_multi_add_handle);
HYSS_FUNCTION(curl_multi_close);
HYSS_FUNCTION(curl_multi_exec);
HYSS_FUNCTION(curl_multi_getcontent);
HYSS_FUNCTION(curl_multi_info_read);
HYSS_FUNCTION(curl_multi_init);
HYSS_FUNCTION(curl_multi_remove_handle);
HYSS_FUNCTION(curl_multi_select);
HYSS_FUNCTION(curl_multi_errno);

HYSS_FUNCTION(curl_share_close);
HYSS_FUNCTION(curl_share_init);
HYSS_FUNCTION(curl_share_setopt);
HYSS_FUNCTION(curl_share_errno);

HYSS_FUNCTION(curl_strerror);
HYSS_FUNCTION(curl_multi_strerror);
HYSS_FUNCTION(curl_share_strerror);

HYSS_FUNCTION(curl_reset);

#if LIBCURL_VERSION_NUM >= 0x070f04 /* 7.15.4 */
HYSS_FUNCTION(curl_escape);
HYSS_FUNCTION(curl_unescape);

HYSS_FUNCTION(curl_multi_setopt);
#endif

#if LIBCURL_VERSION_NUM >= 0x071200 /* 7.18.0 */
HYSS_FUNCTION(curl_pause);
#endif

HYSS_FUNCTION(curl_file_create);


void _hyss_curl_multi_close(gear_resource *);
void _hyss_curl_share_close(gear_resource *);

typedef struct {
	zval                  func_name;
	gear_fcall_info_cache fci_cache;
	FILE                 *fp;
	smart_str             buf;
	int                   method;
	zval					stream;
} hyss_curl_write;

typedef struct {
	zval                  func_name;
	gear_fcall_info_cache fci_cache;
	FILE                 *fp;
	gear_resource        *res;
	int                   method;
	zval                  stream;
} hyss_curl_read;

typedef struct {
	zval                  func_name;
	gear_fcall_info_cache fci_cache;
	int                   method;
} hyss_curl_progress, hyss_curl_fnmatch, hyss_curlm_server_push;

typedef struct {
	hyss_curl_write    *write;
	hyss_curl_write    *write_header;
	hyss_curl_read     *read;
	zval               std_err;
	hyss_curl_progress *progress;
#if LIBCURL_VERSION_NUM >= 0x071500 /* Available since 7.21.0 */
	hyss_curl_fnmatch  *fnmatch;
#endif
} hyss_curl_handlers;

struct _hyss_curl_error  {
	char str[CURL_ERROR_SIZE + 1];
	int  no;
};

struct _hyss_curl_send_headers {
	gear_string *str;
};

struct _hyss_curl_free {
	gear_llist str;
	gear_llist post;
	HashTable *slist;
};

typedef struct {
	CURL                         *cp;
	hyss_curl_handlers            *handlers;
	gear_resource                *res;
	struct _hyss_curl_free        *to_free;
	struct _hyss_curl_send_headers header;
	struct _hyss_curl_error        err;
	gear_bool                     in_callback;
	uint32_t*                     clone;
} hyss_curl;

#define CURLOPT_SAFE_UPLOAD -1

typedef struct {
	hyss_curlm_server_push	*server_push;
} hyss_curlm_handlers;

typedef struct {
	int         still_running;
	CURLM      *multi;
	gear_llist  easyh;
	hyss_curlm_handlers	*handlers;
	struct {
		int no;
	} err;
} hyss_curlm;

typedef struct {
	CURLSH                   *share;
	struct {
		int no;
	} err;
} hyss_curlsh;

hyss_curl *alloc_curl_handle();
void _hyss_curl_cleanup_handle(hyss_curl *);
void _hyss_curl_multi_cleanup_list(void *data);
void _hyss_curl_verify_handlers(hyss_curl *ch, int reporterror);
void _hyss_setup_easy_copy_handlers(hyss_curl *ch, hyss_curl *source);

void curlfile_register_class(void);
HYSS_CURL_API extern gear_class_entry *curl_CURLFile_class;

#else
#define curl_capi_ptr NULL
#endif /* HAVE_CURL */
#define hyssext_curl_ptr curl_capi_ptr
#endif  /* _HYSS_CURL_H */
