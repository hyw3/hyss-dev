/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "hyss.h"
#include "Gear/gear_exceptions.h"
#include "hyss_curl.h"
#if HAVE_CURL

HYSS_CURL_API gear_class_entry *curl_CURLFile_class;

static void curlfile_ctor(INTERNAL_FUNCTION_PARAMETERS)
{
	gear_string *fname, *mime = NULL, *postname = NULL;
	zval *cf = return_value;

	GEAR_PARSE_PARAMETERS_START(1,3)
		Z_PARAM_PATH_STR(fname)
		Z_PARAM_OPTIONAL
		Z_PARAM_STR(mime)
		Z_PARAM_STR(postname)
	GEAR_PARSE_PARAMETERS_END();

	gear_update_property_string(curl_CURLFile_class, cf, "name", sizeof("name")-1, ZSTR_VAL(fname));

	if (mime) {
		gear_update_property_string(curl_CURLFile_class, cf, "mime", sizeof("mime")-1, ZSTR_VAL(mime));
	}

	if (postname) {
		gear_update_property_string(curl_CURLFile_class, cf, "postname", sizeof("postname")-1, ZSTR_VAL(postname));
	}
}

/* {{{ proto CURLFile::__construct(string $name, [string $mimetype [, string $postfilename]])
   Create the CURLFile object */
GEAR_METHOD(CURLFile, __construct)
{
	return_value = getThis();
	curlfile_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto CURLFile curl_file_create(string $name, [string $mimetype [, string $postfilename]])
   Create the CURLFile object */
HYSS_FUNCTION(curl_file_create)
{
    object_init_ex( return_value, curl_CURLFile_class );
    curlfile_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

static void curlfile_get_property(char *name, size_t name_len, INTERNAL_FUNCTION_PARAMETERS)
{
	zval *res, rv;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}
	res = gear_read_property(curl_CURLFile_class, getThis(), name, name_len, 1, &rv);
	ZVAL_COPY_DEREF(return_value, res);
}

static void curlfile_set_property(char *name, size_t name_len, INTERNAL_FUNCTION_PARAMETERS)
{
	gear_string *arg;

	GEAR_PARSE_PARAMETERS_START(1,1)
		Z_PARAM_STR(arg)
	GEAR_PARSE_PARAMETERS_END();

	gear_update_property_string(curl_CURLFile_class, getThis(), name, name_len, ZSTR_VAL(arg));
}

/* {{{ proto string CURLFile::getFilename()
   Get file name */
GEAR_METHOD(CURLFile, getFilename)
{
	curlfile_get_property("name", sizeof("name")-1, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto string CURLFile::getMimeType()
   Get MIME type */
GEAR_METHOD(CURLFile, getMimeType)
{
	curlfile_get_property("mime", sizeof("mime")-1, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto string CURLFile::getPostFilename()
   Get file name for POST */
GEAR_METHOD(CURLFile, getPostFilename)
{
	curlfile_get_property("postname", sizeof("postname")-1, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto void CURLFile::setMimeType(string $mime)
   Set MIME type */
GEAR_METHOD(CURLFile, setMimeType)
{
	curlfile_set_property("mime", sizeof("mime")-1, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto void CURLFile::setPostFilename(string $name)
   Set file name for POST */
GEAR_METHOD(CURLFile, setPostFilename)
{
	curlfile_set_property("postname", sizeof("postname")-1, INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */

/* {{{ proto CURLFile::__wakeup()
   Unserialization handler */
GEAR_METHOD(CURLFile, __wakeup)
{
	gear_unset_property(curl_CURLFile_class, getThis(), "name", sizeof("name")-1);
	gear_update_property_string(curl_CURLFile_class, getThis(), "name", sizeof("name")-1, "");
	gear_throw_exception(NULL, "Unserialization of CURLFile instances is not allowed", 0);
}
/* }}} */

GEAR_BEGIN_ARG_INFO_EX(arginfo_curlfile_create, 0, 0, 1)
	GEAR_ARG_INFO(0, filename)
	GEAR_ARG_INFO(0, mimetype)
	GEAR_ARG_INFO(0, postname)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_curlfile_name, 0)
	GEAR_ARG_INFO(0, name)
GEAR_END_ARG_INFO()


static const gear_function_entry curlfile_funcs[] = {
	HYSS_ME(CURLFile,			__construct,        arginfo_curlfile_create, GEAR_ACC_CTOR|GEAR_ACC_PUBLIC)
	HYSS_ME(CURLFile,			getFilename,        NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(CURLFile,			getMimeType,        NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(CURLFile,			setMimeType,        arginfo_curlfile_name, GEAR_ACC_PUBLIC)
	HYSS_ME(CURLFile,			getPostFilename,    NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(CURLFile,			setPostFilename,    arginfo_curlfile_name, GEAR_ACC_PUBLIC)
	HYSS_ME(CURLFile,            __wakeup,           NULL, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};

void curlfile_register_class(void)
{
	gear_class_entry ce;
	INIT_CLASS_ENTRY( ce, "CURLFile", curlfile_funcs );
	curl_CURLFile_class = gear_register_internal_class(&ce);
	gear_declare_property_string(curl_CURLFile_class, "name", sizeof("name")-1, "", GEAR_ACC_PUBLIC);
	gear_declare_property_string(curl_CURLFile_class, "mime", sizeof("mime")-1, "", GEAR_ACC_PUBLIC);
	gear_declare_property_string(curl_CURLFile_class, "postname", sizeof("postname")-1, "", GEAR_ACC_PUBLIC);
}

#endif
