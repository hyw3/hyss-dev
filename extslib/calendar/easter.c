/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_calendar.h"
#include "sdncal.h"
#include <time.h>

static void _cal_easter(INTERNAL_FUNCTION_PARAMETERS, gear_long gm)
{

	/* based on code by Simon Kershaw, <webmaster@ely.anglican.org> */

	struct tm te;
	gear_long year, golden, solar, lunar, pfm, dom, tmp, easter, result;
	gear_long method = CAL_EASTER_DEFAULT;

	/* Default to the current year if year parameter is not given */
	{
		time_t a;
		struct tm b, *res;
		time(&a);
		res = hyss_localtime_r(&a, &b);
		if (!res) {
			year = 1900;
		} else {
			year = 1900 + b.tm_year;
		}
	}

	if (gear_parse_parameters(GEAR_NUM_ARGS(),
		"|ll", &year, &method) == FAILURE) {
			return;
	}

	if (gm && (year<1970 || year>2037)) {			/* out of range for timestamps */
		hyss_error_docref(NULL, E_WARNING, "This function is only valid for years between 1970 and 2037 inclusive");
		RETURN_FALSE;
	}

	golden = (year % 19) + 1;				/* the Golden number */

	if ((year <= 1582 && method != CAL_EASTER_ALWAYS_GREGORIAN) ||
	    (year >= 1583 && year <= 1752 && method != CAL_EASTER_ROMAN && method != CAL_EASTER_ALWAYS_GREGORIAN) ||
	     method == CAL_EASTER_ALWAYS_JULIAN) {		/* JULIAN CALENDAR */

		dom = (year + (year/4) + 5) % 7;		/* the "Dominical number" - finding a Sunday */
		if (dom < 0) {
			dom += 7;
		}

		pfm = (3 - (11*golden) - 7) % 30;		/* uncorrected date of the Paschal full moon */
		if (pfm < 0) {
			pfm += 30;
		}
	} else {						/* GREGORIAN CALENDAR */
		dom = (year + (year/4) - (year/100) + (year/400)) % 7;	/* the "Domincal number" */
		if (dom < 0) {
			dom += 7;
		}

		solar = (year-1600)/100 - (year-1600)/400;	/* the solar and lunar corrections */
		lunar = (((year-1400) / 100) * 8) / 25;

		pfm = (3 - (11*golden) + solar - lunar) % 30;	/* uncorrected date of the Paschal full moon */
		if (pfm < 0) {
			pfm += 30;
		}
	}

	if ((pfm == 29) || (pfm == 28 && golden > 11)) {	/* corrected date of the Paschal full moon */
		pfm--;						/* - days after 21st March                 */
	}

	tmp = (4-pfm-dom) % 7;
	if (tmp < 0) {
		tmp += 7;
	}

	easter = pfm + tmp + 1;	    			/* Easter as the number of days after 21st March */

	if (gm) {					/* return a timestamp */
		te.tm_isdst = -1;
		te.tm_year = year-1900;
		te.tm_sec = 0;
		te.tm_min = 0;
		te.tm_hour = 0;

		if (easter < 11) {
			te.tm_mon = 2;			/* March */
			te.tm_mday = easter+21;
		} else {
			te.tm_mon = 3;			/* April */
			te.tm_mday = easter-10;
		}
	    result = mktime(&te);
	} else {				 	/* return the days after March 21 */
	    result = easter;
	}
    ZVAL_LONG(return_value, result);
}

/* {{{ proto int easter_date([int year])
   Return the timestamp of midnight on Easter of a given year (defaults to current year) */
HYSS_FUNCTION(easter_date)
{
	_cal_easter(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto int easter_days([int year, [int method]])
   Return the number of days after March 21 that Easter falls on for a given year (defaults to current year) */
HYSS_FUNCTION(easter_days)
{
	_cal_easter(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

