/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_CALENDAR_H
#define HYSS_CALENDAR_H

extern gear_capi_entry calendar_capi_entry;
#define	calendar_capi_ptr &calendar_capi_entry

#include "hyss_version.h"
#define HYSS_CALENDAR_VERSION HYSS_VERSION

/* Functions */

HYSS_MINIT_FUNCTION(calendar);
HYSS_MINFO_FUNCTION(calendar);

HYSS_FUNCTION(jdtogregorian);
HYSS_FUNCTION(gregoriantojd);
HYSS_FUNCTION(jdtojulian);
HYSS_FUNCTION(juliantojd);
HYSS_FUNCTION(jdtojewish);
HYSS_FUNCTION(jewishtojd);
HYSS_FUNCTION(jdtofrench);
HYSS_FUNCTION(frenchtojd);
HYSS_FUNCTION(jddayofweek);
HYSS_FUNCTION(jdmonthname);
HYSS_FUNCTION(easter_days);
HYSS_FUNCTION(easter_date);
HYSS_FUNCTION(unixtojd);
HYSS_FUNCTION(jdtounix);
HYSS_FUNCTION(cal_from_jd);
HYSS_FUNCTION(cal_to_jd);
HYSS_FUNCTION(cal_days_in_month);
HYSS_FUNCTION(cal_info);

#define hyssext_calendar_ptr calendar_capi_ptr

/*
 * Specifying the easter calculation method
 *
 * DEFAULT is Anglican, ie. use Julian calendar before 1753
 * and Gregorian after that. With ROMAN, the cutoff year is 1582.
 * ALWAYS_GREGORIAN and ALWAYS_JULIAN force the calendar
 * regardless of date.
 *
 */

#define CAL_EASTER_DEFAULT		0
#define CAL_EASTER_ROMAN		1
#define CAL_EASTER_ALWAYS_GREGORIAN	2
#define CAL_EASTER_ALWAYS_JULIAN	3

#endif
