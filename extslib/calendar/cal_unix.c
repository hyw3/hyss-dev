/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_calendar.h"
#include "sdncal.h"
#include <time.h>

/* {{{ proto int unixtojd([int timestamp])
   Convert UNIX timestamp to Julian Day */
HYSS_FUNCTION(unixtojd)
{
	time_t ts = 0;
	struct tm *ta, tmbuf;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|l", &ts) == FAILURE) {
		return;
	}

	if (!ts) {
		ts = time(NULL);
	} else if (ts < 0) {
		RETURN_FALSE;
	}

	if (!(ta = hyss_localtime_r(&ts, &tmbuf))) {
		RETURN_FALSE;
	}

	RETURN_LONG(GregorianToSdn(ta->tm_year+1900, ta->tm_mon+1, ta->tm_mday));
}
/* }}} */

/* {{{ proto int jdtounix(int jday)
   Convert Julian Day to UNIX timestamp */
HYSS_FUNCTION(jdtounix)
{
	gear_long uday;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &uday) == FAILURE) {
		return;
	}
	uday -= 2440588 /* J.D. of 1.1.1970 */;

	if (uday < 0 || uday > 24755) { /* before beginning of unix epoch or behind end of unix epoch */
		RETURN_FALSE;
	}

	RETURN_LONG(uday * 24 * 3600);
}
/* }}} */

