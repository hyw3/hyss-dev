dnl config.m4 for extension sysvshm

HYSS_ARG_ENABLE(sysvshm,whether to enable System V shared memory support,
[  --enable-sysvshm        Enable the System V shared memory support])

if test "$HYSS_SYSVSHM" != "no"; then
  AC_DEFINE(HAVE_SYSVSHM, 1, [ ])
  HYSS_NEW_EXTENSION(sysvshm, sysvshm.c, $ext_shared)
fi
