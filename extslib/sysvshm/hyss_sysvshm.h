/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SYSVSHM_H
#define HYSS_SYSVSHM_H

#if HAVE_SYSVSHM

extern gear_capi_entry sysvshm_capi_entry;
#define sysvshm_capi_ptr &sysvshm_capi_entry

#include "hyss_version.h"
#define HYSS_SYSVSHM_VERSION HYSS_VERSION

#include <sys/types.h>

#ifdef HYSS_WIN32
# include <hypbc/pbc_win32.h>
# include "win32/ipc.h"
# ifndef THREAD_LS
#  define THREAD_LS
# endif
#else
# include <sys/ipc.h>
# include <sys/shm.h>
#endif

#define HYSS_SHM_RSRC_NAME "sysvshm"

typedef struct {
	int le_shm;
	gear_long init_mem;
} sysvshm_capi;

typedef struct {
	gear_long key;
	gear_long length;
	gear_long next;
	char mem;
} sysvshm_chunk;

typedef struct {
	char magic[8];
	gear_long start;
	gear_long end;
	gear_long free;
	gear_long total;
} sysvshm_chunk_head;

typedef struct {
	key_t key;               /* key set by user */
	gear_long id;                 /* returned by shmget */
	sysvshm_chunk_head *ptr; /* memory address of shared memory */
} sysvshm_shm;

HYSS_MINIT_FUNCTION(sysvshm);
HYSS_MINFO_FUNCTION(sysvshm);
HYSS_FUNCTION(shm_attach);
HYSS_FUNCTION(shm_detach);
HYSS_FUNCTION(shm_remove);
HYSS_FUNCTION(shm_put_var);
HYSS_FUNCTION(shm_get_var);
HYSS_FUNCTION(shm_has_var);
HYSS_FUNCTION(shm_remove_var);

extern sysvshm_capi hyss_sysvshm;

#else

#define sysvshm_capi_ptr NULL

#endif

#define hyssext_sysvshm_ptr sysvshm_capi_ptr

#endif /* HYSS_SYSVSHM_H */
