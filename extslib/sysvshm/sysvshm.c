/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This has been built and tested on Linux 2.2.14
 *
 * This has been built and tested on Solaris 2.6.
 * It may not compile or execute correctly on other systems.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if HAVE_SYSVSHM

#include <errno.h>

#include "hyss_sysvshm.h"
#include "extslib/standard/info.h"
#include "extslib/standard/hyss_var.h"
#include "gear_smart_str.h"
#include "hyss_ics.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_shm_attach, 0, 0, 1)
	GEAR_ARG_INFO(0, key)
	GEAR_ARG_INFO(0, memsize)
	GEAR_ARG_INFO(0, perm)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shm_detach, 0, 0, 1)
	GEAR_ARG_INFO(0, shm_identifier)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shm_has_var, 0, 0, 2)
	GEAR_ARG_INFO(0, id)
	GEAR_ARG_INFO(0, variable_key)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shm_remove, 0, 0, 1)
	GEAR_ARG_INFO(0, shm_identifier)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shm_put_var, 0, 0, 3)
	GEAR_ARG_INFO(0, shm_identifier)
	GEAR_ARG_INFO(0, variable_key)
	GEAR_ARG_INFO(0, variable)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shm_get_var, 0, 0, 2)
	GEAR_ARG_INFO(0, id)
	GEAR_ARG_INFO(0, variable_key)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_shm_remove_var, 0, 0, 2)
	GEAR_ARG_INFO(0, id)
	GEAR_ARG_INFO(0, variable_key)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ sysvshm_functions[]
 */
static const gear_function_entry sysvshm_functions[] = {
	HYSS_FE(shm_attach,		arginfo_shm_attach)
	HYSS_FE(shm_remove,		arginfo_shm_detach)
	HYSS_FE(shm_detach, 		arginfo_shm_remove)
	HYSS_FE(shm_put_var,		arginfo_shm_put_var)
	HYSS_FE(shm_has_var,		arginfo_shm_has_var)
	HYSS_FE(shm_get_var,		arginfo_shm_get_var)
	HYSS_FE(shm_remove_var,	arginfo_shm_remove_var)
	HYSS_FE_END
};
/* }}} */

/* {{{ sysvshm_capi_entry
 */
gear_capi_entry sysvshm_capi_entry = {
	STANDARD_CAPI_HEADER,
	"sysvshm",
	sysvshm_functions,
	HYSS_MINIT(sysvshm),
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(sysvshm),
	HYSS_SYSVSHM_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_SYSVSHM
GEAR_GET_CAPI(sysvshm)
#endif

#undef shm_ptr					/* undefine AIX-specific macro */

#define SHM_FETCH_RESOURCE(shm_ptr, z_ptr) do { \
	if ((shm_ptr = (sysvshm_shm *)gear_fetch_resource(Z_RES_P(z_ptr), HYSS_SHM_RSRC_NAME, hyss_sysvshm.le_shm)) == NULL) { \
		RETURN_FALSE; \
	} \
} while (0)

THREAD_LS sysvshm_capi hyss_sysvshm;

static int hyss_put_shm_data(sysvshm_chunk_head *ptr, gear_long key, const char *data, gear_long len);
static gear_long hyss_check_shm_data(sysvshm_chunk_head *ptr, gear_long key);
static int hyss_remove_shm_data(sysvshm_chunk_head *ptr, gear_long shm_varpos);

/* {{{ hyss_release_sysvshm
 */
static void hyss_release_sysvshm(gear_resource *rsrc)
{
	sysvshm_shm *shm_ptr = (sysvshm_shm *) rsrc->ptr;
	shmdt((void *) shm_ptr->ptr);
	efree(shm_ptr);
}
/* }}} */

/* {{{ HYSS_MINIT_FUNCTION
 */
HYSS_MINIT_FUNCTION(sysvshm)
{
	hyss_sysvshm.le_shm = gear_register_list_destructors_ex(hyss_release_sysvshm, NULL, HYSS_SHM_RSRC_NAME, capi_number);

	if (cfg_get_long("sysvshm.init_mem", &hyss_sysvshm.init_mem) == FAILURE) {
		hyss_sysvshm.init_mem=10000;
	}
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION
 */
HYSS_MINFO_FUNCTION(sysvshm)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "sysvshm support", "enabled");
	hyss_info_print_table_end();
}
/* }}} */

/* {{{ proto int shm_attach(int key [, int memsize [, int perm]])
   Creates or open a shared memory segment */
HYSS_FUNCTION(shm_attach)
{
	sysvshm_shm *shm_list_ptr;
	char *shm_ptr;
	sysvshm_chunk_head *chunk_ptr;
	gear_long shm_key, shm_id, shm_size = hyss_sysvshm.init_mem, shm_flag = 0666;

	if (SUCCESS != gear_parse_parameters(GEAR_NUM_ARGS(), "l|ll", &shm_key, &shm_size, &shm_flag)) {
		return;
	}

	if (shm_size < 1) {
		hyss_error_docref(NULL, E_WARNING, "Segment size must be greater than zero");
		RETURN_FALSE;
  	}

	shm_list_ptr = (sysvshm_shm *) emalloc(sizeof(sysvshm_shm));

	/* get the id from a specified key or create new shared memory */
	if ((shm_id = shmget(shm_key, 0, 0)) < 0) {
		if (shm_size < (gear_long)sizeof(sysvshm_chunk_head)) {
			hyss_error_docref(NULL, E_WARNING, "failed for key 0x" GEAR_XLONG_FMT ": memorysize too small", shm_key);
			efree(shm_list_ptr);
			RETURN_FALSE;
		}
		if ((shm_id = shmget(shm_key, shm_size, shm_flag | IPC_CREAT | IPC_EXCL)) < 0) {
			hyss_error_docref(NULL, E_WARNING, "failed for key 0x" GEAR_XLONG_FMT ": %s", shm_key, strerror(errno));
			efree(shm_list_ptr);
			RETURN_FALSE;
		}
	}

	if ((shm_ptr = shmat(shm_id, NULL, 0)) == (void *) -1) {
		hyss_error_docref(NULL, E_WARNING, "failed for key 0x" GEAR_XLONG_FMT ": %s", shm_key, strerror(errno));
		efree(shm_list_ptr);
		RETURN_FALSE;
	}

	/* check if shm is already initialized */
	chunk_ptr = (sysvshm_chunk_head *) shm_ptr;
	if (strcmp((char*) &(chunk_ptr->magic), "HYSS_SM") != 0) {
		strcpy((char*) &(chunk_ptr->magic), "HYSS_SM");
		chunk_ptr->start = sizeof(sysvshm_chunk_head);
		chunk_ptr->end = chunk_ptr->start;
		chunk_ptr->total = shm_size;
		chunk_ptr->free = shm_size-chunk_ptr->end;
	}

	shm_list_ptr->key = shm_key;
	shm_list_ptr->id = shm_id;
	shm_list_ptr->ptr = chunk_ptr;

	RETURN_RES(gear_register_resource(shm_list_ptr, hyss_sysvshm.le_shm));
}
/* }}} */

/* {{{ proto bool shm_detach(resource shm_identifier)
   Disconnects from shared memory segment */
HYSS_FUNCTION(shm_detach)
{
	zval *shm_id;
	sysvshm_shm *shm_list_ptr;

	if (SUCCESS != gear_parse_parameters(GEAR_NUM_ARGS(), "r", &shm_id)) {
		return;
	}
	SHM_FETCH_RESOURCE(shm_list_ptr, shm_id);
	RETURN_BOOL(SUCCESS == gear_list_close(Z_RES_P(shm_id)));
}
/* }}} */

/* {{{ proto bool shm_remove(resource shm_identifier)
   Removes shared memory from Unix systems */
HYSS_FUNCTION(shm_remove)
{
	zval *shm_id;
	sysvshm_shm *shm_list_ptr;

	if (SUCCESS != gear_parse_parameters(GEAR_NUM_ARGS(), "r", &shm_id)) {
		return;
	}
	SHM_FETCH_RESOURCE(shm_list_ptr, shm_id);

	if (shmctl(shm_list_ptr->id, IPC_RMID, NULL) < 0) {
		hyss_error_docref(NULL, E_WARNING, "failed for key 0x%x, id " GEAR_LONG_FMT ": %s", shm_list_ptr->key, Z_LVAL_P(shm_id), strerror(errno));
		RETURN_FALSE;
	}

	RETURN_TRUE;
}
/* }}} */

/* {{{ proto bool shm_put_var(resource shm_identifier, int variable_key, mixed variable)
   Inserts or updates a variable in shared memory */
HYSS_FUNCTION(shm_put_var)
{
	zval *shm_id, *arg_var;
	int ret;
	gear_long shm_key;
	sysvshm_shm *shm_list_ptr;
	smart_str shm_var = {0};
	hyss_serialize_data_t var_hash;

	if (SUCCESS != gear_parse_parameters(GEAR_NUM_ARGS(), "rlz", &shm_id, &shm_key, &arg_var)) {
		return;
	}

	/* setup string-variable and serialize */
	HYSS_VAR_SERIALIZE_INIT(var_hash);
	hyss_var_serialize(&shm_var, arg_var, &var_hash);
	HYSS_VAR_SERIALIZE_DESTROY(var_hash);

	shm_list_ptr = gear_fetch_resource(Z_RES_P(shm_id), HYSS_SHM_RSRC_NAME, hyss_sysvshm.le_shm);
	if (!shm_list_ptr) {
		smart_str_free(&shm_var);
		RETURN_FALSE;
	}

	/* insert serialized variable into shared memory */
	ret = hyss_put_shm_data(shm_list_ptr->ptr, shm_key, shm_var.s? ZSTR_VAL(shm_var.s) : NULL, shm_var.s? ZSTR_LEN(shm_var.s) : 0);

	/* free string */
	smart_str_free(&shm_var);

	if (ret == -1) {
		hyss_error_docref(NULL, E_WARNING, "not enough shared memory left");
		RETURN_FALSE;
	}
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto mixed shm_get_var(resource id, int variable_key)
   Returns a variable from shared memory */
HYSS_FUNCTION(shm_get_var)
{
	zval *shm_id;
	gear_long shm_key;
	sysvshm_shm *shm_list_ptr;
	char *shm_data;
	gear_long shm_varpos;
	sysvshm_chunk *shm_var;
	hyss_unserialize_data_t var_hash;

	if (SUCCESS != gear_parse_parameters(GEAR_NUM_ARGS(), "rl", &shm_id, &shm_key)) {
		return;
	}
	SHM_FETCH_RESOURCE(shm_list_ptr, shm_id);

	/* setup string-variable and serialize */
	/* get serialized variable from shared memory */
	shm_varpos = hyss_check_shm_data((shm_list_ptr->ptr), shm_key);

	if (shm_varpos < 0) {
		hyss_error_docref(NULL, E_WARNING, "variable key " GEAR_LONG_FMT " doesn't exist", shm_key);
		RETURN_FALSE;
	}
	shm_var = (sysvshm_chunk*) ((char *)shm_list_ptr->ptr + shm_varpos);
	shm_data = &shm_var->mem;

	HYSS_VAR_UNSERIALIZE_INIT(var_hash);
	if (hyss_var_unserialize(return_value, (const unsigned char **) &shm_data, (unsigned char *) shm_data + shm_var->length, &var_hash) != 1) {
		hyss_error_docref(NULL, E_WARNING, "variable data in shared memory is corrupted");
		RETVAL_FALSE;
	}
	HYSS_VAR_UNSERIALIZE_DESTROY(var_hash);
}
/* }}} */

/* {{{ proto bool shm_has_var(resource id, int variable_key)
	Checks whether a specific entry exists */
HYSS_FUNCTION(shm_has_var)
{
	zval *shm_id;
	gear_long shm_key;
	sysvshm_shm *shm_list_ptr;

	if (SUCCESS != gear_parse_parameters(GEAR_NUM_ARGS(), "rl", &shm_id, &shm_key)) {
		return;
	}
	SHM_FETCH_RESOURCE(shm_list_ptr, shm_id);
	RETURN_BOOL(hyss_check_shm_data(shm_list_ptr->ptr, shm_key) >= 0);
}
/* }}} */

/* {{{ proto bool shm_remove_var(resource id, int variable_key)
   Removes variable from shared memory */
HYSS_FUNCTION(shm_remove_var)
{
	zval *shm_id;
	gear_long shm_key, shm_varpos;
	sysvshm_shm *shm_list_ptr;

	if (SUCCESS != gear_parse_parameters(GEAR_NUM_ARGS(), "rl", &shm_id, &shm_key)) {
		return;
	}
	SHM_FETCH_RESOURCE(shm_list_ptr, shm_id);

	shm_varpos = hyss_check_shm_data((shm_list_ptr->ptr), shm_key);

	if (shm_varpos < 0) {
		hyss_error_docref(NULL, E_WARNING, "variable key " GEAR_LONG_FMT " doesn't exist", shm_key);
		RETURN_FALSE;
	}
	hyss_remove_shm_data((shm_list_ptr->ptr), shm_varpos);
	RETURN_TRUE;
}
/* }}} */

/* {{{ hyss_put_shm_data
 * inserts an ascii-string into shared memory */
static int hyss_put_shm_data(sysvshm_chunk_head *ptr, gear_long key, const char *data, gear_long len)
{
	sysvshm_chunk *shm_var;
	gear_long total_size;
	gear_long shm_varpos;

	total_size = ((gear_long) (len + sizeof(sysvshm_chunk) - 1) / sizeof(gear_long)) * sizeof(gear_long) + sizeof(gear_long); /* gear_long alligment */

	if ((shm_varpos = hyss_check_shm_data(ptr, key)) > 0) {
		hyss_remove_shm_data(ptr, shm_varpos);
	}

	if (ptr->free < total_size) {
		return -1; /* not enough memory */
	}

	shm_var = (sysvshm_chunk *) ((char *) ptr + ptr->end);
	shm_var->key = key;
	shm_var->length = len;
	shm_var->next = total_size;
	memcpy(&(shm_var->mem), data, len);
	ptr->end += total_size;
	ptr->free -= total_size;
	return 0;
}
/* }}} */

/* {{{ hyss_check_shm_data
 */
static gear_long hyss_check_shm_data(sysvshm_chunk_head *ptr, gear_long key)
{
	gear_long pos;
	sysvshm_chunk *shm_var;

	pos = ptr->start;

	for (;;) {
		if (pos >= ptr->end) {
			return -1;
		}
		shm_var = (sysvshm_chunk*) ((char *) ptr + pos);
		if (shm_var->key == key) {
			return pos;
		}
		pos += shm_var->next;

		if (shm_var->next <= 0 || pos < ptr->start) {
			return -1;
		}
	}
	return -1;
}
/* }}} */

/* {{{ hyss_remove_shm_data
 */
static int hyss_remove_shm_data(sysvshm_chunk_head *ptr, gear_long shm_varpos)
{
	sysvshm_chunk *chunk_ptr, *next_chunk_ptr;
	gear_long memcpy_len;

	chunk_ptr = (sysvshm_chunk *) ((char *) ptr + shm_varpos);
	next_chunk_ptr = (sysvshm_chunk *) ((char *) ptr + shm_varpos + chunk_ptr->next);

	memcpy_len = ptr->end-shm_varpos - chunk_ptr->next;
	ptr->free += chunk_ptr->next;
	ptr->end -= chunk_ptr->next;
	if (memcpy_len > 0) {
		memmove(chunk_ptr, next_chunk_ptr, memcpy_len);
	}
	return 0;
}
/* }}} */

#endif /* HAVE_SYSVSHM */

