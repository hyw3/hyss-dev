/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_GETTEXT_H
#define HYSS_GETTEXT_H

#if HAVE_LIBINTL

extern gear_capi_entry hyss_gettext_capi_entry;
#define gettext_capi_ptr &hyss_gettext_capi_entry

#include "hyss_version.h"
#define HYSS_GETTEXT_VERSION HYSS_VERSION

HYSS_MINFO_FUNCTION(hyss_gettext);

HYSS_NAMED_FUNCTION(zif_textdomain);
HYSS_NAMED_FUNCTION(zif_gettext);
HYSS_NAMED_FUNCTION(zif_dgettext);
HYSS_NAMED_FUNCTION(zif_dcgettext);
HYSS_NAMED_FUNCTION(zif_bindtextdomain);
#if HAVE_NGETTEXT
HYSS_NAMED_FUNCTION(zif_ngettext);
#endif
#if HAVE_DNGETTEXT
HYSS_NAMED_FUNCTION(zif_dngettext);
#endif
#if HAVE_DCNGETTEXT
HYSS_NAMED_FUNCTION(zif_dcngettext);
#endif
#if HAVE_BIND_TEXTDOMAIN_CODESET
HYSS_NAMED_FUNCTION(zif_bind_textdomain_codeset);
#endif

#else
#define gettext_capi_ptr NULL
#endif /* HAVE_LIBINTL */

#define hyssext_gettext_ptr gettext_capi_ptr

#endif /* HYSS_GETTEXT_H */
