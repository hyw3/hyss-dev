/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"

#if HAVE_LIBINTL

#include <stdio.h>
#include "extslib/standard/info.h"
#include "hyss_gettext.h"

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO(arginfo_textdomain, 0)
	GEAR_ARG_INFO(0, domain)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_gettext, 0)
	GEAR_ARG_INFO(0, msgid)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_dgettext, 0)
	GEAR_ARG_INFO(0, domain_name)
	GEAR_ARG_INFO(0, msgid)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_dcgettext, 0)
	GEAR_ARG_INFO(0, domain_name)
	GEAR_ARG_INFO(0, msgid)
	GEAR_ARG_INFO(0, category)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_bindtextdomain, 0)
	GEAR_ARG_INFO(0, domain_name)
	GEAR_ARG_INFO(0, dir)
GEAR_END_ARG_INFO()

#if HAVE_NGETTEXT
GEAR_BEGIN_ARG_INFO(arginfo_ngettext, 0)
	GEAR_ARG_INFO(0, msgid1)
	GEAR_ARG_INFO(0, msgid2)
	GEAR_ARG_INFO(0, count)
GEAR_END_ARG_INFO()
#endif

#if HAVE_DNGETTEXT
GEAR_BEGIN_ARG_INFO(arginfo_dngettext, 0)
	GEAR_ARG_INFO(0, domain)
	GEAR_ARG_INFO(0, msgid1)
	GEAR_ARG_INFO(0, msgid2)
	GEAR_ARG_INFO(0, count)
GEAR_END_ARG_INFO()
#endif

#if HAVE_DCNGETTEXT
GEAR_BEGIN_ARG_INFO(arginfo_dcngettext, 0)
	GEAR_ARG_INFO(0, domain)
	GEAR_ARG_INFO(0, msgid1)
	GEAR_ARG_INFO(0, msgid2)
	GEAR_ARG_INFO(0, count)
	GEAR_ARG_INFO(0, category)
GEAR_END_ARG_INFO()
#endif

#if HAVE_BIND_TEXTDOMAIN_CODESET
GEAR_BEGIN_ARG_INFO(arginfo_bind_textdomain_codeset, 0)
	GEAR_ARG_INFO(0, domain)
	GEAR_ARG_INFO(0, codeset)
GEAR_END_ARG_INFO()
#endif
/* }}} */

/* {{{ hyss_gettext_functions[]
 */
static const gear_function_entry hyss_gettext_functions[] = {
	HYSS_NAMED_FE(textdomain,		zif_textdomain,		arginfo_textdomain)
	HYSS_NAMED_FE(gettext,			zif_gettext,		arginfo_gettext)
	/* Alias for gettext() */
	HYSS_NAMED_FE(_,					zif_gettext,		arginfo_gettext)
	HYSS_NAMED_FE(dgettext,			zif_dgettext,		arginfo_dgettext)
	HYSS_NAMED_FE(dcgettext,			zif_dcgettext,		arginfo_dcgettext)
	HYSS_NAMED_FE(bindtextdomain,	zif_bindtextdomain,	arginfo_bindtextdomain)
#if HAVE_NGETTEXT
	HYSS_NAMED_FE(ngettext,			zif_ngettext,		arginfo_ngettext)
#endif
#if HAVE_DNGETTEXT
	HYSS_NAMED_FE(dngettext,			zif_dngettext,		arginfo_dngettext)
#endif
#if HAVE_DCNGETTEXT
	HYSS_NAMED_FE(dcngettext,		zif_dcngettext,		arginfo_dcngettext)
#endif
#if HAVE_BIND_TEXTDOMAIN_CODESET
	HYSS_NAMED_FE(bind_textdomain_codeset,	zif_bind_textdomain_codeset,	arginfo_bind_textdomain_codeset)
#endif
    HYSS_FE_END
};
/* }}} */

#include <libintl.h>

gear_capi_entry hyss_gettext_capi_entry = {
	STANDARD_CAPI_HEADER,
	"gettext",
	hyss_gettext_functions,
	NULL,
	NULL,
	NULL,
	NULL,
	HYSS_MINFO(hyss_gettext),
	HYSS_GETTEXT_VERSION,
	STANDARD_CAPI_PROPERTIES
};

#ifdef COMPILE_DL_GETTEXT
GEAR_GET_CAPI(hyss_gettext)
#endif

#define HYSS_GETTEXT_MAX_DOMAIN_LENGTH 1024
#define HYSS_GETTEXT_MAX_MSGID_LENGTH 4096

#define HYSS_GETTEXT_DOMAIN_LENGTH_CHECK(domain_len) \
	if (UNEXPECTED(domain_len > HYSS_GETTEXT_MAX_DOMAIN_LENGTH)) { \
		hyss_error_docref(NULL, E_WARNING, "domain passed too long"); \
		RETURN_FALSE; \
	}

#define HYSS_GETTEXT_LENGTH_CHECK(check_name, check_len) \
	if (UNEXPECTED(check_len > HYSS_GETTEXT_MAX_MSGID_LENGTH)) { \
		hyss_error_docref(NULL, E_WARNING, "%s passed too long", check_name); \
		RETURN_FALSE; \
	}

HYSS_MINFO_FUNCTION(hyss_gettext)
{
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "GetText Support", "enabled");
	hyss_info_print_table_end();
}

/* {{{ proto string textdomain(string domain)
   Set the textdomain to "domain". Returns the current domain */
HYSS_NAMED_FUNCTION(zif_textdomain)
{
	char *domain = NULL, *domain_name, *retval;
	size_t domain_len = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s!", &domain, &domain_len) == FAILURE) {
		return;
	}

	HYSS_GETTEXT_DOMAIN_LENGTH_CHECK(domain_len)

	if (domain != NULL && strcmp(domain, "") && strcmp(domain, "0")) {
		domain_name = domain;
	} else {
		domain_name = NULL;
	}

	retval = textdomain(domain_name);

	RETURN_STRING(retval);
}
/* }}} */

/* {{{ proto string gettext(string msgid)
   Return the translation of msgid for the current domain, or msgid unaltered if a translation does not exist */
HYSS_NAMED_FUNCTION(zif_gettext)
{
	char *msgstr;
	gear_string *msgid;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(msgid)
	GEAR_PARSE_PARAMETERS_END();

	HYSS_GETTEXT_LENGTH_CHECK("msgid", ZSTR_LEN(msgid))
	msgstr = gettext(ZSTR_VAL(msgid));

	if (msgstr != ZSTR_VAL(msgid)) {
		RETURN_STRING(msgstr);
	} else {
		RETURN_STR_COPY(msgid);
	}
}
/* }}} */

/* {{{ proto string dgettext(string domain_name, string msgid)
   Return the translation of msgid for domain_name, or msgid unaltered if a translation does not exist */
HYSS_NAMED_FUNCTION(zif_dgettext)
{
	char *msgstr;
	gear_string *domain, *msgid;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "SS", &domain, &msgid) == FAILURE)	{
		return;
	}

	HYSS_GETTEXT_DOMAIN_LENGTH_CHECK(ZSTR_LEN(domain))
	HYSS_GETTEXT_LENGTH_CHECK("msgid", ZSTR_LEN(msgid))

	msgstr = dgettext(ZSTR_VAL(domain), ZSTR_VAL(msgid));

	if (msgstr != ZSTR_VAL(msgid)) {
		RETURN_STRING(msgstr);
	} else {
		RETURN_STR_COPY(msgid);
	}
}
/* }}} */

/* {{{ proto string dcgettext(string domain_name, string msgid, int category)
   Return the translation of msgid for domain_name and category, or msgid unaltered if a translation does not exist */
HYSS_NAMED_FUNCTION(zif_dcgettext)
{
	char *msgstr;
	gear_string *domain, *msgid;
	gear_long category;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "SSl", &domain, &msgid, &category) == FAILURE) {
		return;
	}

	HYSS_GETTEXT_DOMAIN_LENGTH_CHECK(ZSTR_LEN(domain))
	HYSS_GETTEXT_LENGTH_CHECK("msgid", ZSTR_LEN(msgid))

	msgstr = dcgettext(ZSTR_VAL(domain), ZSTR_VAL(msgid), category);

	if (msgstr != ZSTR_VAL(msgid)) {
		RETURN_STRING(msgstr);
	} else {
		RETURN_STR_COPY(msgid);
	}
}
/* }}} */

/* {{{ proto string bindtextdomain(string domain_name, string dir)
   Bind to the text domain domain_name, looking for translations in dir. Returns the current domain */
HYSS_NAMED_FUNCTION(zif_bindtextdomain)
{
	char *domain, *dir;
	size_t domain_len, dir_len;
	char *retval, dir_name[MAXPATHLEN];

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss", &domain, &domain_len, &dir, &dir_len) == FAILURE) {
		return;
	}

	HYSS_GETTEXT_DOMAIN_LENGTH_CHECK(domain_len)

	if (domain[0] == '\0') {
		hyss_error(E_WARNING, "The first parameter of bindtextdomain must not be empty");
		RETURN_FALSE;
	}

	if (dir[0] != '\0' && strcmp(dir, "0")) {
		if (!VCWD_REALPATH(dir, dir_name)) {
			RETURN_FALSE;
		}
	} else if (!VCWD_GETCWD(dir_name, MAXPATHLEN)) {
		RETURN_FALSE;
	}

	retval = bindtextdomain(domain, dir_name);

	RETURN_STRING(retval);
}
/* }}} */

#if HAVE_NGETTEXT
/* {{{ proto string ngettext(string MSGID1, string MSGID2, int N)
   Plural version of gettext() */
HYSS_NAMED_FUNCTION(zif_ngettext)
{
	char *msgid1, *msgid2, *msgstr;
	size_t msgid1_len, msgid2_len;
	gear_long count;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ssl", &msgid1, &msgid1_len, &msgid2, &msgid2_len, &count) == FAILURE) {
		return;
	}

	HYSS_GETTEXT_LENGTH_CHECK("msgid1", msgid1_len)
	HYSS_GETTEXT_LENGTH_CHECK("msgid2", msgid2_len)

	msgstr = ngettext(msgid1, msgid2, count);

	GEAR_ASSERT(msgstr);
	RETURN_STRING(msgstr);
}
/* }}} */
#endif

#if HAVE_DNGETTEXT
/* {{{ proto string dngettext(string domain, string msgid1, string msgid2, int count)
   Plural version of dgettext() */
HYSS_NAMED_FUNCTION(zif_dngettext)
{
	char *domain, *msgid1, *msgid2, *msgstr = NULL;
	size_t domain_len, msgid1_len, msgid2_len;
	gear_long count;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "sssl", &domain, &domain_len,
		&msgid1, &msgid1_len, &msgid2, &msgid2_len, &count) == FAILURE) {
		return;
	}

	HYSS_GETTEXT_DOMAIN_LENGTH_CHECK(domain_len)
	HYSS_GETTEXT_LENGTH_CHECK("msgid1", msgid1_len)
	HYSS_GETTEXT_LENGTH_CHECK("msgid2", msgid2_len)

	msgstr = dngettext(domain, msgid1, msgid2, count);

	GEAR_ASSERT(msgstr);
	RETURN_STRING(msgstr);
}
/* }}} */
#endif

#if HAVE_DCNGETTEXT
/* {{{ proto string dcngettext(string domain, string msgid1, string msgid2, int n, int category)
   Plural version of dcgettext() */
HYSS_NAMED_FUNCTION(zif_dcngettext)
{
	char *domain, *msgid1, *msgid2, *msgstr = NULL;
	size_t domain_len, msgid1_len, msgid2_len;
	gear_long count, category;

	RETVAL_FALSE;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "sssll", &domain, &domain_len,
		&msgid1, &msgid1_len, &msgid2, &msgid2_len, &count, &category) == FAILURE) {
		return;
	}

	HYSS_GETTEXT_DOMAIN_LENGTH_CHECK(domain_len)
	HYSS_GETTEXT_LENGTH_CHECK("msgid1", msgid1_len)
	HYSS_GETTEXT_LENGTH_CHECK("msgid2", msgid2_len)

	msgstr = dcngettext(domain, msgid1, msgid2, count, category);

	GEAR_ASSERT(msgstr);
	RETURN_STRING(msgstr);
}
/* }}} */
#endif

#if HAVE_BIND_TEXTDOMAIN_CODESET

/* {{{ proto string bind_textdomain_codeset(string domain, string codeset)
   Specify the character encoding in which the messages from the DOMAIN message catalog will be returned. */
HYSS_NAMED_FUNCTION(zif_bind_textdomain_codeset)
{
	char *domain, *codeset, *retval = NULL;
	size_t domain_len, codeset_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss", &domain, &domain_len, &codeset, &codeset_len) == FAILURE) {
		return;
	}

	HYSS_GETTEXT_DOMAIN_LENGTH_CHECK(domain_len)

	retval = bind_textdomain_codeset(domain, codeset);

	if (!retval) {
		RETURN_FALSE;
	}
	RETURN_STRING(retval);
}
/* }}} */
#endif


#endif /* HAVE_LIBINTL */

