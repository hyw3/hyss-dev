/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_ZIP_H
#define HYSS_ZIP_H

extern gear_capi_entry zip_capi_entry;
#define hyssext_zip_ptr &zip_capi_entry

#ifdef ZTS
#include "hypbc.h"
#endif

#if defined(HAVE_LIBZIP)
#include <zip.h>
#else
#include "lib/zip.h"
#endif

#ifndef ZIP_OVERWRITE
#define ZIP_OVERWRITE ZIP_TRUNCATE
#endif

#define HYSS_ZIP_VERSION "1.15.4"

#define ZIP_OPENBASEDIR_CHECKPATH(filename) hyss_check_open_basedir(filename)

typedef struct _ze_zip_rsrc {
	struct zip *za;
	int index_current;
	int num_files;
} zip_rsrc;

typedef zip_rsrc * zip_rsrc_ptr;

typedef struct _ze_zip_read_rsrc {
	struct zip_file *zf;
	struct zip_stat sb;
} zip_read_rsrc;

#define ZIPARCHIVE_ME(name, arg_info, flags) {#name, c_ziparchive_ ##name, arg_info,(uint32_t) (sizeof(arg_info)/sizeof(struct _gear_arg_info)-1), flags },
#define ZIPARCHIVE_METHOD(name)	GEAR_NAMED_FUNCTION(c_ziparchive_ ##name)

/* Extends gear object */
typedef struct _ze_zip_object {
	struct zip *za;
	char **buffers;
	HashTable *prop_handler;
	char *filename;
	int filename_len;
	int buffers_cnt;
	gear_object zo;
} ze_zip_object;

static inline ze_zip_object *hyss_zip_fetch_object(gear_object *obj) {
	return (ze_zip_object *)((char*)(obj) - XtOffsetOf(ze_zip_object, zo));
}

#define Z_ZIP_P(zv) hyss_zip_fetch_object(Z_OBJ_P((zv)))

hyss_stream *hyss_stream_zip_opener(hyss_stream_wrapper *wrapper, const char *path, const char *mode, int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC);
hyss_stream *hyss_stream_zip_open(const char *filename, const char *path, const char *mode STREAMS_DC);

extern const hyss_stream_wrapper hyss_stream_zip_wrapper;

#endif	/* HYSS_ZIP_H */

