/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hypbc.h"

#ifdef ZTS

#include <stdio.h>

#if HAVE_STDARG_H
#include <stdarg.h>
#endif

typedef struct _pbc_tls_entry pbc_tls_entry;

#if defined(PBC_WIN32)
/* PBCLS_CACHE_DEFINE; is already done in Gear, this is being always compiled statically. */
#endif

struct _pbc_tls_entry {
	void **storage;
	int count;
	THREAD_T thread_id;
	pbc_tls_entry *next;
};


typedef struct {
	size_t size;
	ts_allocate_ctor ctor;
	ts_allocate_dtor dtor;
	int done;
} pbc_resource_type;


/* The memory manager table */
static pbc_tls_entry	**pbc_tls_table=NULL;
static int				pbc_tls_table_size;
static ts_rsrc_id		id_count;

/* The resource sizes table */
static pbc_resource_type	*resource_types_table=NULL;
static int					resource_types_table_size;


static MUTEX_T tsmm_mutex;	/* thread-safe memory manager mutex */

/* New thread handlers */
static pbc_thread_begin_func_t pbc_new_thread_begin_handler = NULL;
static pbc_thread_end_func_t pbc_new_thread_end_handler = NULL;
static pbc_shutdown_func_t pbc_shutdown_handler = NULL;

/* Debug support */
int pbc_error(int level, const char *format, ...);

/* Read a resource from a thread's resource storage */
static int pbc_error_level;
static FILE *pbc_error_file;

#if PBC_DEBUG
#define PBC_ERROR(args) pbc_error args
#define PBC_SAFE_RETURN_RSRC(array, offset, range)																		\
	{																													\
		int unshuffled_offset = PBC_UNSHUFFLE_RSRC_ID(offset);															\
																														\
		if (offset==0) {																								\
			return &array;																								\
		} else if ((unshuffled_offset)>=0 && (unshuffled_offset)<(range)) {												\
			PBC_ERROR((PBC_ERROR_LEVEL_INFO, "Successfully fetched resource id %d for thread id %ld - 0x%0.8X",		\
						unshuffled_offset, (long) thread_resources->thread_id, array[unshuffled_offset]));				\
			return array[unshuffled_offset];																			\
		} else {																										\
			PBC_ERROR((PBC_ERROR_LEVEL_ERROR, "Resource id %d is out of range (%d..%d)",								\
						unshuffled_offset, PBC_SHUFFLE_RSRC_ID(0), PBC_SHUFFLE_RSRC_ID(thread_resources->count-1)));	\
			return NULL;																								\
		}																												\
	}
#else
#define PBC_ERROR(args)
#define PBC_SAFE_RETURN_RSRC(array, offset, range)		\
	if (offset==0) {									\
		return &array;									\
	} else {											\
		return array[PBC_UNSHUFFLE_RSRC_ID(offset)];	\
	}
#endif

#if defined(GNUPTH)
static pth_key_t tls_key;
# define pbc_tls_set(what)		pth_key_setdata(tls_key, (void*)(what))
# define pbc_tls_get()			pth_key_getdata(tls_key)

#elif defined(PTHREADS)
/* Thread local storage */
static pthread_key_t tls_key;
# define pbc_tls_set(what)		pthread_setspecific(tls_key, (void*)(what))
# define pbc_tls_get()			pthread_getspecific(tls_key)

#elif defined(PBC_ST)
static int tls_key;
# define pbc_tls_set(what)		st_thread_setspecific(tls_key, (void*)(what))
# define pbc_tls_get()			st_thread_getspecific(tls_key)

#elif defined(PBC_WIN32)
static DWORD tls_key;
# define pbc_tls_set(what)		TlsSetValue(tls_key, (void*)(what))
# define pbc_tls_get()			TlsGetValue(tls_key)

#else
# define pbc_tls_set(what)
# define pbc_tls_get()			NULL
# warning pbc_set_interpreter_context is probably broken on this platform
#endif

PBC_TLS uint8_t in_main_thread = 0;

/* Startup hypbc (call once for the entire process) */
PBC_API int pbc_startup(int expected_threads, int expected_resources, int debug_level, char *debug_filename)
{/*{{{*/
#if defined(GNUPTH)
	pth_init();
	pth_key_create(&tls_key, 0);
#elif defined(PTHREADS)
	pthread_key_create( &tls_key, 0 );
#elif defined(PBC_ST)
	st_init();
	st_key_create(&tls_key, 0);
#elif defined(PBC_WIN32)
	tls_key = TlsAlloc();
#endif

	/* ensure singleton */
	in_main_thread = 1;

	pbc_error_file = stderr;
	pbc_error_set(debug_level, debug_filename);
	pbc_tls_table_size = expected_threads;

	pbc_tls_table = (pbc_tls_entry **) calloc(pbc_tls_table_size, sizeof(pbc_tls_entry *));
	if (!pbc_tls_table) {
		PBC_ERROR((PBC_ERROR_LEVEL_ERROR, "Unable to allocate TLS table"));
		return 0;
	}
	id_count=0;

	resource_types_table_size = expected_resources;
	resource_types_table = (pbc_resource_type *) calloc(resource_types_table_size, sizeof(pbc_resource_type));
	if (!resource_types_table) {
		PBC_ERROR((PBC_ERROR_LEVEL_ERROR, "Unable to allocate resource types table"));
		free(pbc_tls_table);
		pbc_tls_table = NULL;
		return 0;
	}

	tsmm_mutex = pbc_mutex_alloc();

	PBC_ERROR((PBC_ERROR_LEVEL_CORE, "Started up hypbc, %d expected threads, %d expected resources", expected_threads, expected_resources));
	return 1;
}/*}}}*/


/* Shutdown hypbc (call once for the entire process) */
PBC_API void pbc_shutdown(void)
{/*{{{*/
	int i;

	if (!in_main_thread) {
		/* ensure singleton */
		return;
	}

	if (pbc_tls_table) {
		for (i=0; i<pbc_tls_table_size; i++) {
			pbc_tls_entry *p = pbc_tls_table[i], *next_p;

			while (p) {
				int j;

				next_p = p->next;
				for (j=0; j<p->count; j++) {
					if (p->storage[j]) {
						if (resource_types_table && !resource_types_table[j].done && resource_types_table[j].dtor) {
							resource_types_table[j].dtor(p->storage[j]);
						}
						free(p->storage[j]);
					}
				}
				free(p->storage);
				free(p);
				p = next_p;
			}
		}
		free(pbc_tls_table);
		pbc_tls_table = NULL;
	}
	if (resource_types_table) {
		free(resource_types_table);
		resource_types_table=NULL;
	}
	pbc_mutex_free(tsmm_mutex);
	tsmm_mutex = NULL;
	PBC_ERROR((PBC_ERROR_LEVEL_CORE, "Shutdown hypbc"));
	if (pbc_error_file!=stderr) {
		fclose(pbc_error_file);
	}
#if defined(GNUPTH)
	pth_kill();
#elif defined(PTHREADS)
	pthread_setspecific(tls_key, 0);
	pthread_key_delete(tls_key);
#elif defined(PBC_WIN32)
	TlsFree(tls_key);
#endif
	if (pbc_shutdown_handler) {
		pbc_shutdown_handler();
	}
	pbc_new_thread_begin_handler = NULL;
	pbc_new_thread_end_handler = NULL;
	pbc_shutdown_handler = NULL;
}/*}}}*/


/* allocates a new thread-safe-resource id */
PBC_API ts_rsrc_id ts_allocate_id(ts_rsrc_id *rsrc_id, size_t size, ts_allocate_ctor ctor, ts_allocate_dtor dtor)
{/*{{{*/
	int i;

	PBC_ERROR((PBC_ERROR_LEVEL_CORE, "Obtaining a new resource id, %d bytes", size));

	pbc_mutex_lock(tsmm_mutex);

	/* obtain a resource id */
	*rsrc_id = PBC_SHUFFLE_RSRC_ID(id_count++);
	PBC_ERROR((PBC_ERROR_LEVEL_CORE, "Obtained resource id %d", *rsrc_id));

	/* store the new resource type in the resource sizes table */
	if (resource_types_table_size < id_count) {
		pbc_resource_type *_tmp;
		_tmp = (pbc_resource_type *) realloc(resource_types_table, sizeof(pbc_resource_type)*id_count);
		if (!_tmp) {
			pbc_mutex_unlock(tsmm_mutex);
			PBC_ERROR((PBC_ERROR_LEVEL_ERROR, "Unable to allocate storage for resource"));
			*rsrc_id = 0;
			return 0;
		}
		resource_types_table = _tmp;
		resource_types_table_size = id_count;
	}
	resource_types_table[PBC_UNSHUFFLE_RSRC_ID(*rsrc_id)].size = size;
	resource_types_table[PBC_UNSHUFFLE_RSRC_ID(*rsrc_id)].ctor = ctor;
	resource_types_table[PBC_UNSHUFFLE_RSRC_ID(*rsrc_id)].dtor = dtor;
	resource_types_table[PBC_UNSHUFFLE_RSRC_ID(*rsrc_id)].done = 0;

	/* enlarge the arrays for the already active threads */
	for (i=0; i<pbc_tls_table_size; i++) {
		pbc_tls_entry *p = pbc_tls_table[i];

		while (p) {
			if (p->count < id_count) {
				int j;

				p->storage = (void *) realloc(p->storage, sizeof(void *)*id_count);
				for (j=p->count; j<id_count; j++) {
					p->storage[j] = (void *) malloc(resource_types_table[j].size);
					if (resource_types_table[j].ctor) {
						resource_types_table[j].ctor(p->storage[j]);
					}
				}
				p->count = id_count;
			}
			p = p->next;
		}
	}
	pbc_mutex_unlock(tsmm_mutex);

	PBC_ERROR((PBC_ERROR_LEVEL_CORE, "Successfully allocated new resource id %d", *rsrc_id));
	return *rsrc_id;
}/*}}}*/


static void allocate_new_resource(pbc_tls_entry **thread_resources_ptr, THREAD_T thread_id)
{/*{{{*/
	int i;

	PBC_ERROR((PBC_ERROR_LEVEL_CORE, "Creating data structures for thread %x", thread_id));
	(*thread_resources_ptr) = (pbc_tls_entry *) malloc(sizeof(pbc_tls_entry));
	(*thread_resources_ptr)->storage = NULL;
	if (id_count > 0) {
		(*thread_resources_ptr)->storage = (void **) malloc(sizeof(void *)*id_count);
	}
	(*thread_resources_ptr)->count = id_count;
	(*thread_resources_ptr)->thread_id = thread_id;
	(*thread_resources_ptr)->next = NULL;

	/* Set thread local storage to this new thread resources structure */
	pbc_tls_set(*thread_resources_ptr);

	if (pbc_new_thread_begin_handler) {
		pbc_new_thread_begin_handler(thread_id);
	}
	for (i=0; i<id_count; i++) {
		if (resource_types_table[i].done) {
			(*thread_resources_ptr)->storage[i] = NULL;
		} else
		{
			(*thread_resources_ptr)->storage[i] = (void *) malloc(resource_types_table[i].size);
			if (resource_types_table[i].ctor) {
				resource_types_table[i].ctor((*thread_resources_ptr)->storage[i]);
			}
		}
	}

	if (pbc_new_thread_end_handler) {
		pbc_new_thread_end_handler(thread_id);
	}

	pbc_mutex_unlock(tsmm_mutex);
}/*}}}*/


/* fetches the requested resource for the current thread */
PBC_API void *ts_resource_ex(ts_rsrc_id id, THREAD_T *th_id)
{/*{{{*/
	THREAD_T thread_id;
	int hash_value;
	pbc_tls_entry *thread_resources;

	if (!th_id) {
		/* Fast path for looking up the resources for the current
		 * thread. Its used by just about every call to
		 * ts_resource_ex(). This avoids the need for a mutex lock
		 * and our hashtable lookup.
		 */
		thread_resources = pbc_tls_get();

		if (thread_resources) {
			PBC_ERROR((PBC_ERROR_LEVEL_INFO, "Fetching resource id %d for current thread %d", id, (long) thread_resources->thread_id));
			/* Read a specific resource from the thread's resources.
			 * This is called outside of a mutex, so have to be aware about external
			 * changes to the structure as we read it.
			 */
			PBC_SAFE_RETURN_RSRC(thread_resources->storage, id, thread_resources->count);
		}
		thread_id = pbc_thread_id();
	} else {
		thread_id = *th_id;
	}

	PBC_ERROR((PBC_ERROR_LEVEL_INFO, "Fetching resource id %d for thread %ld", id, (long) thread_id));
	pbc_mutex_lock(tsmm_mutex);

	hash_value = THREAD_HASH_OF(thread_id, pbc_tls_table_size);
	thread_resources = pbc_tls_table[hash_value];

	if (!thread_resources) {
		allocate_new_resource(&pbc_tls_table[hash_value], thread_id);
		return ts_resource_ex(id, &thread_id);
	} else {
		 do {
			if (thread_resources->thread_id == thread_id) {
				break;
			}
			if (thread_resources->next) {
				thread_resources = thread_resources->next;
			} else {
				allocate_new_resource(&thread_resources->next, thread_id);
				return ts_resource_ex(id, &thread_id);
				/*
				 * thread_resources = thread_resources->next;
				 * break;
				 */
			}
		 } while (thread_resources);
	}
	pbc_mutex_unlock(tsmm_mutex);
	/* Read a specific resource from the thread's resources.
	 * This is called outside of a mutex, so have to be aware about external
	 * changes to the structure as we read it.
	 */
	PBC_SAFE_RETURN_RSRC(thread_resources->storage, id, thread_resources->count);
}/*}}}*/

/* frees an interpreter context.  You are responsible for making sure that
 * it is not linked into the hypbc hash, and not marked as the current interpreter */
void pbc_free_interpreter_context(void *context)
{/*{{{*/
	pbc_tls_entry *next, *thread_resources = (pbc_tls_entry*)context;
	int i;

	while (thread_resources) {
		next = thread_resources->next;

		for (i=0; i<thread_resources->count; i++) {
			if (resource_types_table[i].dtor) {
				resource_types_table[i].dtor(thread_resources->storage[i]);
			}
		}
		for (i=0; i<thread_resources->count; i++) {
			free(thread_resources->storage[i]);
		}
		free(thread_resources->storage);
		free(thread_resources);
		thread_resources = next;
	}
}/*}}}*/

void *pbc_set_interpreter_context(void *new_ctx)
{/*{{{*/
	pbc_tls_entry *current;

	current = pbc_tls_get();

	/* TODO: unlink current from the global linked list, and replace it
	 * it with the new context, protected by mutex where/if appropriate */

	/* Set thread local storage to this new thread resources structure */
	pbc_tls_set(new_ctx);

	/* return old context, so caller can restore it when they're done */
	return current;
}/*}}}*/


/* allocates a new interpreter context */
void *pbc_new_interpreter_context(void)
{/*{{{*/
	pbc_tls_entry *new_ctx, *current;
	THREAD_T thread_id;

	thread_id = pbc_thread_id();
	pbc_mutex_lock(tsmm_mutex);

	current = pbc_tls_get();

	allocate_new_resource(&new_ctx, thread_id);

	/* switch back to the context that was in use prior to our creation
	 * of the new one */
	return pbc_set_interpreter_context(current);
}/*}}}*/


/* frees all resources allocated for the current thread */
void ts_free_thread(void)
{/*{{{*/
	pbc_tls_entry *thread_resources;
	int i;
	THREAD_T thread_id = pbc_thread_id();
	int hash_value;
	pbc_tls_entry *last=NULL;

	pbc_mutex_lock(tsmm_mutex);
	hash_value = THREAD_HASH_OF(thread_id, pbc_tls_table_size);
	thread_resources = pbc_tls_table[hash_value];

	while (thread_resources) {
		if (thread_resources->thread_id == thread_id) {
			for (i=0; i<thread_resources->count; i++) {
				if (resource_types_table[i].dtor) {
					resource_types_table[i].dtor(thread_resources->storage[i]);
				}
			}
			for (i=0; i<thread_resources->count; i++) {
				free(thread_resources->storage[i]);
			}
			free(thread_resources->storage);
			if (last) {
				last->next = thread_resources->next;
			} else {
				pbc_tls_table[hash_value] = thread_resources->next;
			}
			pbc_tls_set(0);
			free(thread_resources);
			break;
		}
		if (thread_resources->next) {
			last = thread_resources;
		}
		thread_resources = thread_resources->next;
	}
	pbc_mutex_unlock(tsmm_mutex);
}/*}}}*/


/* frees all resources allocated for all threads except current */
void ts_free_worker_threads(void)
{/*{{{*/
	pbc_tls_entry *thread_resources;
	int i;
	THREAD_T thread_id = pbc_thread_id();
	int hash_value;
	pbc_tls_entry *last=NULL;

	pbc_mutex_lock(tsmm_mutex);
	hash_value = THREAD_HASH_OF(thread_id, pbc_tls_table_size);
	thread_resources = pbc_tls_table[hash_value];

	while (thread_resources) {
		if (thread_resources->thread_id != thread_id) {
			for (i=0; i<thread_resources->count; i++) {
				if (resource_types_table[i].dtor) {
					resource_types_table[i].dtor(thread_resources->storage[i]);
				}
			}
			for (i=0; i<thread_resources->count; i++) {
				free(thread_resources->storage[i]);
			}
			free(thread_resources->storage);
			if (last) {
				last->next = thread_resources->next;
			} else {
				pbc_tls_table[hash_value] = thread_resources->next;
			}
			free(thread_resources);
			if (last) {
				thread_resources = last->next;
			} else {
				thread_resources = pbc_tls_table[hash_value];
			}
		} else {
			if (thread_resources->next) {
				last = thread_resources;
			}
			thread_resources = thread_resources->next;
		}
	}
	pbc_mutex_unlock(tsmm_mutex);
}/*}}}*/


/* deallocates all occurrences of a given id */
void ts_free_id(ts_rsrc_id id)
{/*{{{*/
	int i;
	int j = PBC_UNSHUFFLE_RSRC_ID(id);

	pbc_mutex_lock(tsmm_mutex);

	PBC_ERROR((PBC_ERROR_LEVEL_CORE, "Freeing resource id %d", id));

	if (pbc_tls_table) {
		for (i=0; i<pbc_tls_table_size; i++) {
			pbc_tls_entry *p = pbc_tls_table[i];

			while (p) {
				if (p->count > j && p->storage[j]) {
					if (resource_types_table && resource_types_table[j].dtor) {
						resource_types_table[j].dtor(p->storage[j]);
					}
					free(p->storage[j]);
					p->storage[j] = NULL;
				}
				p = p->next;
			}
		}
	}
	resource_types_table[j].done = 1;

	pbc_mutex_unlock(tsmm_mutex);

	PBC_ERROR((PBC_ERROR_LEVEL_CORE, "Successfully freed resource id %d", id));
}/*}}}*/




/*
 * Utility Functions
 */

/* Obtain the current thread id */
PBC_API THREAD_T pbc_thread_id(void)
{/*{{{*/
#ifdef PBC_WIN32
	return GetCurrentThreadId();
#elif defined(GNUPTH)
	return pth_self();
#elif defined(PTHREADS)
	return pthread_self();
#elif defined(PBC_ST)
	return st_thread_self();
#endif
}/*}}}*/


/* Allocate a mutex */
PBC_API MUTEX_T pbc_mutex_alloc(void)
{/*{{{*/
	MUTEX_T mutexp;
#ifdef PBC_WIN32
	mutexp = malloc(sizeof(CRITICAL_SECTION));
	InitializeCriticalSection(mutexp);
#elif defined(GNUPTH)
	mutexp = (MUTEX_T) malloc(sizeof(*mutexp));
	pth_mutex_init(mutexp);
#elif defined(PTHREADS)
	mutexp = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(mutexp,NULL);
#elif defined(PBC_ST)
	mutexp = st_mutex_new();
#endif
#ifdef THR_DEBUG
	printf("Mutex created thread: %d\n",mythreadid());
#endif
	return( mutexp );
}/*}}}*/


/* Free a mutex */
PBC_API void pbc_mutex_free(MUTEX_T mutexp)
{/*{{{*/
	if (mutexp) {
#ifdef PBC_WIN32
		DeleteCriticalSection(mutexp);
		free(mutexp);
#elif defined(GNUPTH)
		free(mutexp);
#elif defined(PTHREADS)
		pthread_mutex_destroy(mutexp);
		free(mutexp);
#elif defined(PBC_ST)
		st_mutex_destroy(mutexp);
#endif
	}
#ifdef THR_DEBUG
	printf("Mutex freed thread: %d\n",mythreadid());
#endif
}/*}}}*/


/*
  Lock a mutex.
  A return value of 0 indicates success
*/
PBC_API int pbc_mutex_lock(MUTEX_T mutexp)
{/*{{{*/
	PBC_ERROR((PBC_ERROR_LEVEL_INFO, "Mutex locked thread: %ld", pbc_thread_id()));
#ifdef PBC_WIN32
	EnterCriticalSection(mutexp);
	return 0;
#elif defined(GNUPTH)
	if (pth_mutex_acquire(mutexp, 0, NULL)) {
		return 0;
	}
	return -1;
#elif defined(PTHREADS)
	return pthread_mutex_lock(mutexp);
#elif defined(PBC_ST)
	return st_mutex_lock(mutexp);
#endif
}/*}}}*/


/*
  Unlock a mutex.
  A return value of 0 indicates success
*/
PBC_API int pbc_mutex_unlock(MUTEX_T mutexp)
{/*{{{*/
	PBC_ERROR((PBC_ERROR_LEVEL_INFO, "Mutex unlocked thread: %ld", pbc_thread_id()));
#ifdef PBC_WIN32
	LeaveCriticalSection(mutexp);
	return 0;
#elif defined(GNUPTH)
	if (pth_mutex_release(mutexp)) {
		return 0;
	}
	return -1;
#elif defined(PTHREADS)
	return pthread_mutex_unlock(mutexp);
#elif defined(PBC_ST)
	return st_mutex_unlock(mutexp);
#endif
}/*}}}*/

/*
  Changes the signal mask of the calling thread
*/
#ifdef HAVE_SIGPROCMASK
PBC_API int pbc_sigmask(int how, const sigset_t *set, sigset_t *oldset)
{/*{{{*/
	PBC_ERROR((PBC_ERROR_LEVEL_INFO, "Changed sigmask in thread: %ld", pbc_thread_id()));
	/* TODO: add support for other APIs */
#ifdef PTHREADS
	return pthread_sigmask(how, set, oldset);
#else
	return sigprocmask(how, set, oldset);
#endif
}/*}}}*/
#endif


PBC_API void *pbc_set_new_thread_begin_handler(pbc_thread_begin_func_t new_thread_begin_handler)
{/*{{{*/
	void *retval = (void *) pbc_new_thread_begin_handler;

	pbc_new_thread_begin_handler = new_thread_begin_handler;
	return retval;
}/*}}}*/


PBC_API void *pbc_set_new_thread_end_handler(pbc_thread_end_func_t new_thread_end_handler)
{/*{{{*/
	void *retval = (void *) pbc_new_thread_end_handler;

	pbc_new_thread_end_handler = new_thread_end_handler;
	return retval;
}/*}}}*/


PBC_API void *pbc_set_shutdown_handler(pbc_shutdown_func_t shutdown_handler)
{/*{{{*/
	void *retval = (void *) pbc_shutdown_handler;

	pbc_shutdown_handler = shutdown_handler;
	return retval;
}/*}}}*/


/*
 * Debug support
 */

#if PBC_DEBUG
int pbc_error(int level, const char *format, ...)
{/*{{{*/
	if (level<=pbc_error_level) {
		va_list args;
		int size;

		fprintf(pbc_error_file, "hypbc:  ");
		va_start(args, format);
		size = vfprintf(pbc_error_file, format, args);
		va_end(args);
		fprintf(pbc_error_file, "\n");
		fflush(pbc_error_file);
		return size;
	} else {
		return 0;
	}
}/*}}}*/
#endif


void pbc_error_set(int level, char *debug_filename)
{/*{{{*/
	pbc_error_level = level;

#if PBC_DEBUG
	if (pbc_error_file!=stderr) { /* close files opened earlier */
		fclose(pbc_error_file);
	}

	if (debug_filename) {
		pbc_error_file = fopen(debug_filename, "w");
		if (!pbc_error_file) {
			pbc_error_file = stderr;
		}
	} else {
		pbc_error_file = stderr;
	}
#endif
}/*}}}*/

PBC_API void *pbc_get_ls_cache(void)
{/*{{{*/
	return pbc_tls_get();
}/*}}}*/

PBC_API uint8_t pbc_is_main_thread(void)
{/*{{{*/
	return in_main_thread;
}/*}}}*/

PBC_API const char *pbc_api_name(void)
{/*{{{*/
#if defined(GNUPTH)
	return "GNU Pth";
#elif defined(PTHREADS)
	return "POSIX Threads";
#elif defined(PBC_ST)
	return "State Threads";
#elif defined(PBC_WIN32)
	return "Windows Threads";
#else
	return "Unknown";
#endif
}/*}}}*/

#endif /* ZTS */

