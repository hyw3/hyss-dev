/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PBC_H
#define PBC_H

#if !defined(__CYGWIN__) && defined(WIN32)
# define PBC_WIN32
# include "pbc_config.w32.h"
#else
# include <pbc_config.h>
#endif

#include "main/hyss_stdint.h"

#ifdef PBC_WIN32
#	ifdef PBC_EXPORTS
#		define PBC_API __declspec(dllexport)
#	else
#		define PBC_API __declspec(dllimport)
#	endif
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PBC_API __attribute__ ((visibility("default")))
#else
#	define PBC_API
#endif

typedef intptr_t pbc_intptr_t;
typedef uintptr_t pbc_uintptr_t;

/* Only compile multi-threading functions if we're in ZTS mode */
#ifdef ZTS

#ifdef PBC_WIN32
# ifndef PBC_INCLUDE_FULL_WINDOWS_HEADERS
#  define WIN32_LEAN_AND_MEAN
# endif
# include <windows.h>
# include <shellapi.h>
#elif defined(GNUPTH)
# include <pth.h>
#elif defined(PTHREADS)
# include <pthread.h>
#elif defined(PBC_ST)
# include <st.h>
#elif defined(BETHREADS)
#include <kernel/OS.h>
#include <TLS.h>
#endif

typedef int ts_rsrc_id;

/* Define THREAD_T and MUTEX_T */
#ifdef PBC_WIN32
# define THREAD_T DWORD
# define MUTEX_T CRITICAL_SECTION *
#elif defined(GNUPTH)
# define THREAD_T pth_t
# define MUTEX_T pth_mutex_t *
#elif defined(PTHREADS)
# define THREAD_T pthread_t
# define MUTEX_T pthread_mutex_t *
#elif defined(PBC_ST)
# define THREAD_T st_thread_t
# define MUTEX_T st_mutex_t
#endif

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

typedef void (*ts_allocate_ctor)(void *);
typedef void (*ts_allocate_dtor)(void *);

#define THREAD_HASH_OF(thr,ts)  (unsigned long)thr%(unsigned long)ts

#ifdef __cplusplus
extern "C" {
#endif

/* startup/shutdown */
PBC_API int pbc_startup(int expected_threads, int expected_resources, int debug_level, char *debug_filename);
PBC_API void pbc_shutdown(void);

/* allocates a new thread-safe-resource id */
PBC_API ts_rsrc_id ts_allocate_id(ts_rsrc_id *rsrc_id, size_t size, ts_allocate_ctor ctor, ts_allocate_dtor dtor);

/* fetches the requested resource for the current thread */
PBC_API void *ts_resource_ex(ts_rsrc_id id, THREAD_T *th_id);
#define ts_resource(id)			ts_resource_ex(id, NULL)

/* frees all resources allocated for the current thread */
PBC_API void ts_free_thread(void);

/* frees all resources allocated for all threads except current */
void ts_free_worker_threads(void);

/* deallocates all occurrences of a given id */
PBC_API void ts_free_id(ts_rsrc_id id);


/* Debug support */
#define PBC_ERROR_LEVEL_ERROR	1
#define PBC_ERROR_LEVEL_CORE	2
#define PBC_ERROR_LEVEL_INFO	3

typedef void (*pbc_thread_begin_func_t)(THREAD_T thread_id);
typedef void (*pbc_thread_end_func_t)(THREAD_T thread_id);
typedef void (*pbc_shutdown_func_t)(void);


PBC_API int pbc_error(int level, const char *format, ...);
PBC_API void pbc_error_set(int level, char *debug_filename);

/* utility functions */
PBC_API THREAD_T pbc_thread_id(void);
PBC_API MUTEX_T pbc_mutex_alloc(void);
PBC_API void pbc_mutex_free(MUTEX_T mutexp);
PBC_API int pbc_mutex_lock(MUTEX_T mutexp);
PBC_API int pbc_mutex_unlock(MUTEX_T mutexp);
#ifdef HAVE_SIGPROCMASK
PBC_API int pbc_sigmask(int how, const sigset_t *set, sigset_t *oldset);
#endif

PBC_API void *pbc_set_new_thread_begin_handler(pbc_thread_begin_func_t new_thread_begin_handler);
PBC_API void *pbc_set_new_thread_end_handler(pbc_thread_end_func_t new_thread_end_handler);
PBC_API void *pbc_set_shutdown_handler(pbc_shutdown_func_t shutdown_handler);

/* these 3 APIs should only be used by people that fully understand the threading model
 * used by HYSS/Gear and the selected SAPI. */
PBC_API void *pbc_new_interpreter_context(void);
PBC_API void *pbc_set_interpreter_context(void *new_ctx);
PBC_API void pbc_free_interpreter_context(void *context);

PBC_API void *pbc_get_ls_cache(void);
PBC_API uint8_t pbc_is_main_thread(void);
PBC_API const char *pbc_api_name(void);

#if defined(__cplusplus) && __cplusplus > 199711L
# define PBC_TLS thread_local
#else
# ifdef PBC_WIN32
#  define PBC_TLS __declspec(thread)
# else
#  define PBC_TLS __thread
# endif
#endif

#define PBC_SHUFFLE_RSRC_ID(rsrc_id)		((rsrc_id)+1)
#define PBC_UNSHUFFLE_RSRC_ID(rsrc_id)		((rsrc_id)-1)

#define PBCLS_FETCH_FROM_CTX(ctx)	void ***pbc_ls = (void ***) ctx
#define PBCLS_SET_CTX(ctx)		ctx = (void ***) pbc_get_ls_cache()
#define PBCG(id, type, element)	(PBCG_BULK(id, type)->element)
#define PBCG_BULK(id, type)	((type) (*((void ***) pbc_get_ls_cache()))[PBC_UNSHUFFLE_RSRC_ID(id)])

#define PBCG_STATIC(id, type, element)	(PBCG_BULK_STATIC(id, type)->element)
#define PBCG_BULK_STATIC(id, type)	((type) (*((void ***) PBCLS_CACHE))[PBC_UNSHUFFLE_RSRC_ID(id)])
#define PBCLS_CACHE_EXTERN() extern PBC_TLS void *PBCLS_CACHE;
#define PBCLS_CACHE_DEFINE() PBC_TLS void *PBCLS_CACHE = NULL;
#if GEAR_DEBUG
#define PBCLS_CACHE_UPDATE() PBCLS_CACHE = pbc_get_ls_cache()
#define PBCLS_CACHE_RESET()
#else
#define PBCLS_CACHE_UPDATE() if (!PBCLS_CACHE) PBCLS_CACHE = pbc_get_ls_cache()
#define PBCLS_CACHE_RESET()  PBCLS_CACHE = NULL
#endif
#define PBCLS_CACHE _pbc_ls_cache

/* BC only */
#define PBCLS_D void
#define PBCLS_DC
#define PBCLS_C
#define PBCLS_CC
#define PBCLS_FETCH()

#ifdef __cplusplus
}
#endif

#else /* non ZTS */

#define PBCLS_FETCH()
#define PBCLS_FETCH_FROM_CTX(ctx)
#define PBCLS_SET_CTX(ctx)

#define PBCG_STATIC(id, type, element)
#define PBCLS_CACHE_EXTERN()
#define PBCLS_CACHE_DEFINE()
#define PBCLS_CACHE_UPDATE()
#define PBCLS_CACHE

#define PBC_TLS

/* BC only */
#define PBCLS_D	void
#define PBCLS_DC
#define PBCLS_C
#define PBCLS_CC

#endif /* ZTS */

#endif /* PBC_H */


