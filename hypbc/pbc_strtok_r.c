/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "pbc_config_common.h"
#include "pbc_strtok_r.h"

static inline int in_character_class(char ch, const char *delim)
{/*{{{*/
	while (*delim) {
		if (*delim == ch) {
			return 1;
		}
		delim++;
	}
	return 0;
}/*}}}*/

PBC_API char *pbc_strtok_r(char *s, const char *delim, char **last)
{/*{{{*/
	char *token;

	if (s == NULL) {
		s = *last;
	}

	while (*s && in_character_class(*s, delim)) {
		s++;
	}
	if (!*s) {
		return NULL;
	}

	token = s;

	while (*s && !in_character_class(*s, delim)) {
		s++;
	}
	if (!*s) {
		*last = s;
	} else {
		*s = '\0';
		*last = s + 1;
	}
	return token;
}/*}}}*/

#if 0

main()
{
	char foo[] = "/foo/bar//\\barbara";
	char *last;
	char *token;

	token = pbc_strtok_r(foo, "/\\", &last);
	while (token) {
		printf ("Token = '%s'\n", token);
		token = pbc_strtok_r(NULL, "/\\", &last);
	}

	return 0;
}

#endif

