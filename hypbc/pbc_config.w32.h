/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PBC_CONFIG_W32_H
#define PBC_CONFIG_W32_H

#include <../main/config.w32.h>
#include "Gear/gear_config.w32.h"

#define HAVE_UTIME 1
#define HAVE_ALLOCA 1

#include <malloc.h>
#include <stdlib.h>
#include <crtdbg.h>

#endif

