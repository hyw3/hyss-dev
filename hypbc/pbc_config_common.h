/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PBC_CONFIG_COMMON_H
#define PBC_CONFIG_COMMON_H

#ifndef __CYGWIN__
# ifdef _WIN32
#  define PBC_WIN32
# endif
#endif

#ifdef PBC_WIN32
# include "pbc_config.w32.h"
#else
# include <pbc_config.h>
# include <sys/param.h>
#endif

#if HAVE_ALLOCA_H && !defined(_ALLOCA_H)
#  include <alloca.h>
#endif

/* AIX requires this to be the first thing in the file.  */
#ifndef __GNUC__
# ifndef HAVE_ALLOCA_H
#  ifdef _AIX
#pragma alloca
#  else
#   ifndef alloca /* predefined by HP cc +Olibcalls */
char *alloca ();
#   endif
#  endif
# endif
#endif

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#if HAVE_LIMITS_H
#include <limits.h>
#endif

#ifndef MAXPATHLEN
# if _WIN32
#  include "win32/ioutil.h"
#  define MAXPATHLEN HYSS_WIN32_IOUTIL_MAXPATHLEN
# elif PATH_MAX
#  define MAXPATHLEN PATH_MAX
# elif defined(MAX_PATH)
#  define MAXPATHLEN MAX_PATH
# else
#  define MAXPATHLEN 256
# endif
#endif

#if (HAVE_ALLOCA || (defined (__GNUC__) && __GNUC__ >= 2))
# define PBC_ALLOCA_MAX_SIZE 4096
# define PBC_ALLOCA_FLAG(name) \
	int name;
# define pbc_do_alloca_ex(size, limit, use_heap) \
	((use_heap = ((size) > (limit))) ? malloc(size) : alloca(size))
# define pbc_do_alloca(size, use_heap) \
	pbc_do_alloca_ex(size, PBC_ALLOCA_MAX_SIZE, use_heap)
# define pbc_free_alloca(p, use_heap) \
	do { if (use_heap) free(p); } while (0)
#else
# define PBC_ALLOCA_FLAG(name)
# define pbc_do_alloca(p, use_heap)	malloc(p)
# define pbc_free_alloca(p, use_heap)	free(p)
#endif

#endif /* PBC_CONFIG_COMMON_H */

