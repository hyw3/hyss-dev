/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PBC_WIN32_H
#define PBC_WIN32_H

#include "hypbc.h"
#include <windows.h>
#if HAVE_UTIME
# include <sys/utime.h>
#endif
#include "win32/ipc.h"

struct ipc_perm {
	key_t		key;
	unsigned short	uid;
	unsigned short	gid;
	unsigned short	cuid;
	unsigned short	cgid;
	unsigned short	mode;
	unsigned short	seq;
};

struct shmid_ds {
	struct	ipc_perm	shm_perm;
	size_t			shm_segsz;
	time_t			shm_atime;
	time_t			shm_dtime;
	time_t			shm_ctime;
	unsigned short	shm_cpid;
	unsigned short	shm_lpid;
	short			shm_nattch;
};

typedef struct {
	FILE	*stream;
	HANDLE	prochnd;
} process_pair;

typedef struct {
	void	*addr;
	HANDLE	info;
	HANDLE	segment;
	struct	shmid_ds	*descriptor;
} shm_pair;

typedef struct {
	process_pair	*process;
	shm_pair		*shm;
	int				process_size;
	int				shm_size;
	char			*comspec;
	HANDLE impersonation_token;
	PSID			impersonation_token_sid;
} pbc_win32_globals;

#ifdef ZTS
# define TWG(v) PBCG_STATIC(win32_globals_id, pbc_win32_globals *, v)
PBCLS_CACHE_EXTERN()
#else
# define TWG(v) (win32_globals.v)
#endif

#define IPC_PRIVATE	0
#define IPC_CREAT	00001000
#define IPC_EXCL	00002000
#define IPC_NOWAIT	00004000

#define IPC_RMID	0
#define IPC_SET		1
#define IPC_STAT	2
#define IPC_INFO	3

#define SHM_R		PAGE_READONLY
#define SHM_W		PAGE_READWRITE

#define	SHM_RDONLY	FILE_MAP_READ
#define	SHM_RND		FILE_MAP_WRITE
#define	SHM_REMAP	FILE_MAP_COPY

char * pbc_win32_get_path_sid_key(const char *pathname, size_t pathname_len, size_t *key_len);

PBC_API void pbc_win32_startup(void);
PBC_API void pbc_win32_shutdown(void);

PBC_API FILE *popen_ex(const char *command, const char *type, const char *cwd, char *env);
PBC_API FILE *popen(const char *command, const char *type);
PBC_API int pclose(FILE *stream);
PBC_API int pbc_win32_access(const char *pathname, int mode);
PBC_API int win32_utime(const char *filename, struct utimbuf *buf);

PBC_API int shmget(key_t key, size_t size, int flags);
PBC_API void *shmat(int key, const void *shmaddr, int flags);
PBC_API int shmdt(const void *shmaddr);
PBC_API int shmctl(int key, int cmd, struct shmid_ds *buf);
#endif

