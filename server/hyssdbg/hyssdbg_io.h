/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_IO_H
#define HYSSDBG_IO_H

#include "hyssdbg.h"

/* Older versions of glibc <= 2.3.0 and <= OS X 10.5 do not have this constant defined */
#ifndef AI_NUMERICSERV
#define AI_NUMERICSERV 0
#endif

HYSSDBG_API int hyssdbg_consume_stdin_line(char *buf);

HYSSDBG_API int hyssdbg_consume_bytes(int sock, char *ptr, int len, int tmo);
HYSSDBG_API int hyssdbg_send_bytes(int sock, const char *ptr, int len);
HYSSDBG_API int hyssdbg_mixed_read(int sock, char *ptr, int len, int tmo);
HYSSDBG_API int hyssdbg_mixed_write(int sock, const char *ptr, int len);

HYSSDBG_API int hyssdbg_create_listenable_socket(const char *addr, unsigned short port, struct addrinfo *res);
HYSSDBG_API int hyssdbg_open_socket(const char *interface, unsigned short port);
HYSSDBG_API void hyssdbg_close_socket(int sock);

#endif /* HYSSDBG_IO_H */
