/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_INFO_H
#define HYSSDBG_INFO_H

#include "hyssdbg_cmd.h"

#define HYSSDBG_INFO(name) HYSSDBG_COMMAND(info_##name)

HYSSDBG_INFO(files);
HYSSDBG_INFO(break);
HYSSDBG_INFO(classes);
HYSSDBG_INFO(funcs);
HYSSDBG_INFO(error);
HYSSDBG_INFO(constants);
HYSSDBG_INFO(vars);
HYSSDBG_INFO(globals);
HYSSDBG_INFO(literal);
HYSSDBG_INFO(memory);

extern const hyssdbg_command_t hyssdbg_info_commands[];

#endif /* HYSSDBG_INFO_H */
