/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#ifndef _WIN32
#	include <sys/mman.h>
#	include <unistd.h>
#endif
#include <fcntl.h>
#include "hyssdbg.h"
#include "hyssdbg_list.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_prompt.h"
#include "hyss_streams.h"
#include "gear_exceptions.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

#define HYSSDBG_LIST_COMMAND_D(f, h, a, m, l, s, flags) \
	HYSSDBG_COMMAND_D_EXP(f, h, a, m, l, s, &hyssdbg_prompt_commands[12], flags)

const hyssdbg_command_t hyssdbg_list_commands[] = {
	HYSSDBG_LIST_COMMAND_D(lines,     "lists the specified lines",    'l', list_lines,  NULL, "l", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_LIST_COMMAND_D(class,     "lists the specified class",    'c', list_class,  NULL, "s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_LIST_COMMAND_D(method,    "lists the specified method",   'm', list_method, NULL, "m", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_LIST_COMMAND_D(func,      "lists the specified function", 'f', list_func,   NULL, "s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_END_COMMAND
};

HYSSDBG_LIST(lines) /* {{{ */
{
	if (!HYSSDBG_G(exec) && !gear_is_executing()) {
		hyssdbg_error("inactive", "type=\"execution\"", "Not executing, and execution context not set");
		return SUCCESS;
	}

	switch (param->type) {
		case NUMERIC_PARAM: {
			const char *char_file = hyssdbg_current_file();
			gear_string *file = gear_string_init(char_file, strlen(char_file), 0);
			hyssdbg_list_file(file, param->num < 0 ? 1 - param->num : param->num, (param->num < 0 ? param->num : 0) + gear_get_executed_lineno(), 0);
			efree(file);
		} break;

		case FILE_PARAM: {
			gear_string *file;
			char resolved_path_buf[MAXPATHLEN];
			const char *abspath = param->file.name;
			if (VCWD_REALPATH(abspath, resolved_path_buf)) {
				abspath = resolved_path_buf;
			}
			file = gear_string_init(abspath, strlen(abspath), 0);
			hyssdbg_list_file(file, param->file.line, 0, 0);
			gear_string_release(file);
		} break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_LIST(func) /* {{{ */
{
	hyssdbg_list_function_byname(param->str, param->len);

	return SUCCESS;
} /* }}} */

HYSSDBG_LIST(method) /* {{{ */
{
	gear_class_entry *ce;

	if (hyssdbg_safe_class_lookup(param->method.class, strlen(param->method.class), &ce) == SUCCESS) {
		gear_function *function;
		char *lcname = gear_str_tolower_dup(param->method.name, strlen(param->method.name));

		if ((function = gear_hash_str_find_ptr(&ce->function_table, lcname, strlen(lcname)))) {
			hyssdbg_list_function(function);
		} else {
			hyssdbg_error("list", "type=\"notfound\" method=\"%s::%s\"", "Could not find %s::%s", param->method.class, param->method.name);
		}

		efree(lcname);
	} else {
		hyssdbg_error("list", "type=\"notfound\" class=\"%s\"", "Could not find the class %s", param->method.class);
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_LIST(class) /* {{{ */
{
	gear_class_entry *ce;

	if (hyssdbg_safe_class_lookup(param->str, param->len, &ce) == SUCCESS) {
		if (ce->type == GEAR_USER_CLASS) {
			if (ce->info.user.filename) {
				hyssdbg_list_file(ce->info.user.filename, ce->info.user.line_end - ce->info.user.line_start + 1, ce->info.user.line_start, 0);
			} else {
				hyssdbg_error("list", "type=\"nosource\" class=\"%s\"", "The source of the requested class (%s) cannot be found", ZSTR_VAL(ce->name));
			}
		} else {
			hyssdbg_error("list", "type=\"internalclass\" class=\"%s\"", "The class requested (%s) is not user defined", ZSTR_VAL(ce->name));
		}
	} else {
		hyssdbg_error("list", "type=\"notfound\" class=\"%s\"", "The requested class (%s) could not be found", param->str);
	}

	return SUCCESS;
} /* }}} */

void hyssdbg_list_file(gear_string *filename, uint32_t count, int offset, uint32_t highlight) /* {{{ */
{
	uint32_t line, lastline;
	hyssdbg_file_source *data;

	if (!(data = gear_hash_find_ptr(&HYSSDBG_G(file_sources), filename))) {
		hyssdbg_error("list", "type=\"unknownfile\"", "Could not find information about included file...");
		return;
	}

	if (offset < 0) {
		count += offset;
		offset = 0;
	}

	lastline = offset + count;

	if (lastline > data->lines) {
		lastline = data->lines;
	}

	hyssdbg_xml("<list %r file=\"%s\">", ZSTR_VAL(filename));

	for (line = offset; line < lastline;) {
		uint32_t linestart = data->line[line++];
		uint32_t linelen = data->line[line] - linestart;
		char *buffer = data->buf + linestart;

		if (!highlight) {
			hyssdbg_write("line", "line=\"%u\" code=\"%.*s\"", " %05u: %.*s", line, linelen, buffer);
		} else {
			if (highlight != line) {
				hyssdbg_write("line", "line=\"%u\" code=\"%.*s\"", " %05u: %.*s", line, linelen, buffer);
			} else {
				hyssdbg_write("line", "line=\"%u\" code=\"%.*s\" current=\"current\"", ">%05u: %.*s", line, linelen, buffer);
			}
		}

		if (*(buffer + linelen - 1) != '\n' || !linelen) {
			hyssdbg_out("\n");
		}
	}

	hyssdbg_xml("</list>");
} /* }}} */

void hyssdbg_list_function(const gear_function *fbc) /* {{{ */
{
	const gear_op_array *ops;

	if (fbc->type != GEAR_USER_FUNCTION) {
		hyssdbg_error("list", "type=\"internalfunction\" function=\"%s\"", "The function requested (%s) is not user defined", ZSTR_VAL(fbc->common.function_name));
		return;
	}

	ops = (gear_op_array *) fbc;

	hyssdbg_list_file(ops->filename, ops->line_end - ops->line_start + 1, ops->line_start, 0);
} /* }}} */

void hyssdbg_list_function_byname(const char *str, size_t len) /* {{{ */
{
	HashTable *func_table = EG(function_table);
	gear_function* fbc;
	char *func_name = (char*) str;
	size_t func_name_len = len;

	/* search active scope if begins with period */
	if (func_name[0] == '.') {
		gear_class_entry *scope = gear_get_executed_scope();
		if (scope) {
			func_name++;
			func_name_len--;

			func_table = &scope->function_table;
		} else {
			hyssdbg_error("inactive", "type=\"noclasses\"", "No active class");
			return;
		}
	} else if (!EG(function_table)) {
		hyssdbg_error("inactive", "type=\"function_table\"", "No function table loaded");
		return;
	} else {
		func_table = EG(function_table);
	}

	/* use lowercase names, case insensitive */
	func_name = gear_str_tolower_dup(func_name, func_name_len);

	hyssdbg_try_access {
		if ((fbc = gear_hash_str_find_ptr(func_table, func_name, func_name_len))) {
			hyssdbg_list_function(fbc);
		} else {
			hyssdbg_error("list", "type=\"nofunction\" function=\"%s\"", "Function %s not found", func_name);
		}
	} hyssdbg_catch_access {
		hyssdbg_error("signalsegv", "function=\"%s\"", "Could not list function %s, invalid data source", func_name);
	} hyssdbg_end_try_access();

	efree(func_name);
} /* }}} */

/* Note: do not free the original file handler, let original compile_file() or caller do that. Caller may rely on its value to check success */
gear_op_array *hyssdbg_compile_file(gear_file_handle *file, int type) {
	hyssdbg_file_source data, *dataptr;
	gear_op_array *ret;
	uint32_t line;
	char *bufptr, *endptr;
	int size;

	ret = HYSSDBG_G(compile_file)(file, type);
	if (ret == NULL) {
		return ret;
	}

	if (file->type == GEAR_HANDLE_MAPPED) {
		data.len = file->handle.stream.mmap.len;
		data.buf = emalloc(data.len + 1);
		memcpy(data.buf, file->handle.stream.mmap.buf, data.len);
	} else {
		if (file->type == GEAR_HANDLE_FILENAME) {
			gear_stream_open(file->filename, file);
		}

		size = file->handle.stream.fsizer(file->handle.stream.handle);
		data.buf = emalloc(size + 1);
		data.len = file->handle.stream.reader(file->handle.stream.handle, data.buf, size);
	}

	memset(data.buf + data.len, 0, 1);

	data.line[0] = 0;
	*(dataptr = emalloc(sizeof(hyssdbg_file_source) + sizeof(uint32_t) * data.len)) = data;

	for (line = 0, bufptr = data.buf - 1, endptr = data.buf + data.len; ++bufptr < endptr;) {
		if (*bufptr == '\n') {
			dataptr->line[++line] = (uint32_t)(bufptr - data.buf) + 1;
		}
	}

	dataptr->lines = ++line;
	dataptr = erealloc(dataptr, sizeof(hyssdbg_file_source) + sizeof(uint32_t) * line);
	dataptr->line[line] = endptr - data.buf;

	gear_hash_del(&HYSSDBG_G(file_sources), ret->filename);
	gear_hash_add_ptr(&HYSSDBG_G(file_sources), ret->filename, dataptr);
	hyssdbg_resolve_pending_file_break(ZSTR_VAL(ret->filename));

	return ret;
}

gear_op_array *hyssdbg_init_compile_file(gear_file_handle *file, int type) {
	char *filename = (char *)(file->opened_path ? ZSTR_VAL(file->opened_path) : file->filename);
	char resolved_path_buf[MAXPATHLEN];
	gear_op_array *op_array;
	hyssdbg_file_source *dataptr;

	if (VCWD_REALPATH(filename, resolved_path_buf)) {
		filename = resolved_path_buf;

		if (file->opened_path) {
			gear_string_release(file->opened_path);
			file->opened_path = gear_string_init(filename, strlen(filename), 0);
		} else {
			if (file->free_filename) {
				efree((char *) file->filename);
			}
			file->free_filename = 0;
			file->filename = filename;
		}
	}

	op_array = HYSSDBG_G(init_compile_file)(file, type);

	if (op_array == NULL) {
		return NULL;
	}

	dataptr = gear_hash_find_ptr(&HYSSDBG_G(file_sources), op_array->filename);
	GEAR_ASSERT(dataptr != NULL);

	dataptr->op_array = *op_array;
	if (dataptr->op_array.refcount) {
		++*dataptr->op_array.refcount;
	}

	return op_array;
}

gear_op_array *hyssdbg_compile_string(zval *source_string, char *filename) {
	gear_string *fake_name;
	gear_op_array *op_array;
	hyssdbg_file_source *dataptr;
	uint32_t line;
	char *bufptr, *endptr;

	if (HYSSDBG_G(flags) & HYSSDBG_IN_EVAL) {
		return HYSSDBG_G(compile_string)(source_string, filename);
	}

	dataptr = emalloc(sizeof(hyssdbg_file_source) + sizeof(uint32_t) * Z_STRLEN_P(source_string));
	dataptr->buf = estrndup(Z_STRVAL_P(source_string), Z_STRLEN_P(source_string));
	dataptr->len = Z_STRLEN_P(source_string);
	dataptr->line[0] = 0;
	for (line = 0, bufptr = dataptr->buf - 1, endptr = dataptr->buf + dataptr->len; ++bufptr < endptr;) {
		if (*bufptr == '\n') {
			dataptr->line[++line] = (uint32_t)(bufptr - dataptr->buf) + 1;
		}
	}
	dataptr->lines = ++line;
	dataptr->line[line] = endptr - dataptr->buf;

	op_array = HYSSDBG_G(compile_string)(source_string, filename);

	if (op_array == NULL) {
		efree(dataptr->buf);
		efree(dataptr);
		return NULL;
	}

	fake_name = strpprintf(0, "%s%c%p", filename, 0, op_array->opcodes);

	dataptr = erealloc(dataptr, sizeof(hyssdbg_file_source) + sizeof(uint32_t) * line);
	gear_hash_add_ptr(&HYSSDBG_G(file_sources), fake_name, dataptr);

	gear_string_release(fake_name);

	dataptr->op_array = *op_array;
	if (dataptr->op_array.refcount) {
		++*dataptr->op_array.refcount;
	}

	return op_array;
}

void hyssdbg_init_list(void) {
	HYSSDBG_G(compile_file) = gear_compile_file;
	HYSSDBG_G(compile_string) = gear_compile_string;
	gear_compile_file = hyssdbg_compile_file;
	gear_compile_string = hyssdbg_compile_string;
}

void hyssdbg_list_update(void) {
	HYSSDBG_G(init_compile_file) = gear_compile_file;
	gear_compile_file = hyssdbg_init_compile_file;
}
