The interactive HYSS debugger
=============================

Implemented as a SAPI cAPI, hyssdbg can exert complete control over the environment
without impacting the functionality or performance of your code.

hyssdbg aims to be a lightweight, powerful, easy to use debugging platform for HYSS

Features
========

 - Stepthrough Debugging
 - Flexible Breakpoints (Class Method, Function, File:Line, Address, Opcode)
 - Easy Access to HYSS with built-in eval()
 - Easy Access to Currently Executing Code
 - Userland API
 - SAPI Agnostic - Easily Integrated
 - HYSS Configuration File Support
 - JIT Super Globals - Set Your Own!!
 - Optional readline Support - Comfortable Terminal Operation
 - Remote Debugging Support - Bundled Java GUI
 - Easy Operation - See Help :)

Planned
=======

 - Improve Everything :)

Installation
============

To install **hyssdbg**, you must compile the source against your HYSS
installation sources, and enable the SAPI with the configure command.


Command Line Options
====================

The following switches are implemented (just like cli SAPI):

 - -n ignore hyss ics
 - -c search for hyss ics in path
 - -z load gear extension
 - -d define hyss ics entry

The following switches change the default behaviour of hyssdbg:

 - -v disables quietness
 - -s enabled stepping
 - -e sets execution context
 - -b boring - disables use of colour on the console
 - -I ignore .hyssdbginit (default init file)
 - -i override .hyssgdbinit location (implies -I)
 - -O set oplog output file
 - -q do not print banner on startup
 - -r jump straight to run
 - -E enable step through eval()
 - -l listen ports for remote mode
 - -a listen address for remote mode
 - -S override SAPI name

**Note:** Passing -rr will cause hyssdbg to quit after execution,
rather than returning to the console.

