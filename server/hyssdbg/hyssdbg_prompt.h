/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_PROMPT_H
#define HYSSDBG_PROMPT_H

/* {{{ */
void hyssdbg_string_init(char *buffer);
void hyssdbg_init(char *init_file, size_t init_file_len, gear_bool use_default);
void hyssdbg_try_file_init(char *init_file, size_t init_file_len, gear_bool free_init);
int hyssdbg_interactive(gear_bool allow_async_unsafe, char *input);
int hyssdbg_compile(void);
int hyssdbg_compile_stdin(gear_string *code);
void hyssdbg_force_interruption(void);
/* }}} */

/* {{{ hyssdbg command handlers */
HYSSDBG_COMMAND(exec);
HYSSDBG_COMMAND(stdin);
HYSSDBG_COMMAND(step);
HYSSDBG_COMMAND(continue);
HYSSDBG_COMMAND(run);
HYSSDBG_COMMAND(ev);
HYSSDBG_COMMAND(until);
HYSSDBG_COMMAND(finish);
HYSSDBG_COMMAND(leave);
HYSSDBG_COMMAND(frame);
HYSSDBG_COMMAND(print);
HYSSDBG_COMMAND(break);
HYSSDBG_COMMAND(back);
HYSSDBG_COMMAND(list);
HYSSDBG_COMMAND(info);
HYSSDBG_COMMAND(clean);
HYSSDBG_COMMAND(clear);
HYSSDBG_COMMAND(help);
HYSSDBG_COMMAND(sh);
HYSSDBG_COMMAND(dl);
HYSSDBG_COMMAND(generator);
HYSSDBG_COMMAND(set);
HYSSDBG_COMMAND(source);
HYSSDBG_COMMAND(export);
HYSSDBG_COMMAND(register);
HYSSDBG_COMMAND(quit);
HYSSDBG_COMMAND(watch);
HYSSDBG_COMMAND(next);
HYSSDBG_COMMAND(eol);
HYSSDBG_COMMAND(wait); /* }}} */

/* {{{ prompt commands */
extern const hyssdbg_command_t hyssdbg_prompt_commands[]; /* }}} */

void hyssdbg_execute_ex(gear_execute_data *execute_data);

#endif /* HYSSDBG_PROMPT_H */
