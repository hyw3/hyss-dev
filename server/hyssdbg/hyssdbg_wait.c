/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg_wait.h"
#include "hyssdbg_prompt.h"
#include "extslib/standard/hyss_var.h"
#include "extslib/standard/basic_functions.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

static void hyssdbg_rebuild_http_globals_array(int type, const char *name) {
	zval *zvp;
	if (Z_TYPE(PG(http_globals)[type]) != IS_UNDEF) {
		zval_ptr_dtor_nogc(&PG(http_globals)[type]);
	}
	if ((zvp = gear_hash_str_find(&EG(symbol_table), name, strlen(name)))) {
		Z_ADDREF_P(zvp);
		PG(http_globals)[type] = *zvp;
	}
}


static int hyssdbg_dearm_autoglobals(gear_auto_global *auto_global) {
	if (ZSTR_LEN(auto_global->name) != sizeof("GLOBALS") - 1 || memcmp(ZSTR_VAL(auto_global->name), "GLOBALS", sizeof("GLOBALS") - 1)) {
		auto_global->armed = 0;
	}

	return GEAR_HASH_APPLY_KEEP;
}

typedef struct {
	HashTable *ht[2];
	HashPosition pos[2];
} hyssdbg_intersect_ptr;

static int hyssdbg_array_data_compare(const void *a, const void *b) {
	Bucket *f, *s;
	int result;
	zval *first, *second;

	f = *((Bucket **) a);
	s = *((Bucket **) b);

	first = &f->val;
	second = &s->val;

	result = string_compare_function(first, second);

	if (result < 0) {
		return -1;
	} else if (result > 0) {
		return 1;
	}

	return 0;
}

static void hyssdbg_array_intersect_init(hyssdbg_intersect_ptr *info, HashTable *ht1, HashTable *ht2) {
	info->ht[0] = ht1;
	info->ht[1] = ht2;

	gear_hash_sort(info->ht[0], (compare_func_t) hyssdbg_array_data_compare, 0);
	gear_hash_sort(info->ht[1], (compare_func_t) hyssdbg_array_data_compare, 0);

	gear_hash_internal_pointer_reset_ex(info->ht[0], &info->pos[0]);
	gear_hash_internal_pointer_reset_ex(info->ht[1], &info->pos[1]);
}

/* -1 => first array, 0 => both arrays equal, 1 => second array */
static int hyssdbg_array_intersect(hyssdbg_intersect_ptr *info, zval **ptr) {
	int ret;
	zval *zvp[2];
	int invalid = !info->ht[0] + !info->ht[1];

	if (invalid > 0) {
		invalid = !info->ht[0];

		if (!(*ptr = gear_hash_get_current_data_ex(info->ht[invalid], &info->pos[invalid]))) {
			return 0;
		}

		gear_hash_move_forward_ex(info->ht[invalid], &info->pos[invalid]);

		return invalid ? 1 : -1;
	}

	if (!(zvp[0] = gear_hash_get_current_data_ex(info->ht[0], &info->pos[0]))) {
		info->ht[0] = NULL;
		return hyssdbg_array_intersect(info, ptr);
	}
	if (!(zvp[1] = gear_hash_get_current_data_ex(info->ht[1], &info->pos[1]))) {
		info->ht[1] = NULL;
		return hyssdbg_array_intersect(info, ptr);
	}

	ret = gear_binary_zval_strcmp(zvp[0], zvp[1]);

	if (ret <= 0) {
		*ptr = zvp[0];
		gear_hash_move_forward_ex(info->ht[0], &info->pos[0]);
	}
	if (ret >= 0) {
		*ptr = zvp[1];
		gear_hash_move_forward_ex(info->ht[1], &info->pos[1]);
	}

	return ret;
}

void hyssdbg_webdata_decompress(char *msg, int len) {
	zval *free_zv = NULL;
	zval zv, *zvp;
	HashTable *ht;
	hyss_unserialize_data_t var_hash;

	HYSS_VAR_UNSERIALIZE_INIT(var_hash);
	if (!hyss_var_unserialize(&zv, (const unsigned char **) &msg, (unsigned char *) msg + len, &var_hash)) {
		HYSS_VAR_UNSERIALIZE_DESTROY(var_hash);
		hyssdbg_error("wait", "type=\"invaliddata\" import=\"fail\"", "Malformed serialized was sent to this socket, arborting");
		return;
	}
	HYSS_VAR_UNSERIALIZE_DESTROY(var_hash);

	ht = Z_ARRVAL(zv);

	/* Reapply symbol table */
	if ((zvp = gear_hash_str_find(ht, GEAR_STRL("GLOBALS"))) && Z_TYPE_P(zvp) == IS_ARRAY) {
		{
			zval *srv;
			if ((srv = gear_hash_str_find(Z_ARRVAL_P(zvp), GEAR_STRL("_SERVER"))) && Z_TYPE_P(srv) == IS_ARRAY) {
				zval *script;
				if ((script = gear_hash_str_find(Z_ARRVAL_P(srv), GEAR_STRL("SCRIPT_FILENAME"))) && Z_TYPE_P(script) == IS_STRING) {
					hyssdbg_param_t param;
					param.str = Z_STRVAL_P(script);
					HYSSDBG_COMMAND_HANDLER(exec)(&param);
				}
			}
		}

		PG(auto_globals_jit) = 0;
		gear_hash_apply(CG(auto_globals), (apply_func_t) hyssdbg_dearm_autoglobals);

		gear_hash_clean(&EG(symbol_table));
		EG(symbol_table) = *Z_ARR_P(zvp);

		/* Rebuild cookies, env vars etc. from GLOBALS (PG(http_globals)) */
		hyssdbg_rebuild_http_globals_array(TRACK_VARS_POST, "_POST");
		hyssdbg_rebuild_http_globals_array(TRACK_VARS_GET, "_GET");
		hyssdbg_rebuild_http_globals_array(TRACK_VARS_COOKIE, "_COOKIE");
		hyssdbg_rebuild_http_globals_array(TRACK_VARS_SERVER, "_SERVER");
		hyssdbg_rebuild_http_globals_array(TRACK_VARS_ENV, "_ENV");
		hyssdbg_rebuild_http_globals_array(TRACK_VARS_FILES, "_FILES");

		Z_ADDREF_P(zvp);
		free_zv = zvp;
	}

	if ((zvp = gear_hash_str_find(ht, GEAR_STRL("input"))) && Z_TYPE_P(zvp) == IS_STRING) {
		if (SG(request_info).request_body) {
			hyss_stream_close(SG(request_info).request_body);
		}
		SG(request_info).request_body = hyss_stream_temp_create_ex(TEMP_STREAM_DEFAULT, SAPI_POST_BLOCK_SIZE, PG(upload_tmp_dir));
		hyss_stream_truncate_set_size(SG(request_info).request_body, 0);
		hyss_stream_write(SG(request_info).request_body, Z_STRVAL_P(zvp), Z_STRLEN_P(zvp));
	}

	if ((zvp = gear_hash_str_find(ht, GEAR_STRL("cwd"))) && Z_TYPE_P(zvp) == IS_STRING) {
		if (VCWD_CHDIR(Z_STRVAL_P(zvp)) == SUCCESS) {
			if (BG(CurrentStatFile) && !IS_ABSOLUTE_PATH(BG(CurrentStatFile), strlen(BG(CurrentStatFile)))) {
				efree(BG(CurrentStatFile));
				BG(CurrentStatFile) = NULL;
			}
			if (BG(CurrentLStatFile) && !IS_ABSOLUTE_PATH(BG(CurrentLStatFile), strlen(BG(CurrentLStatFile)))) {
				efree(BG(CurrentLStatFile));
				BG(CurrentLStatFile) = NULL;
			}
		}
	}

	if ((zvp = gear_hash_str_find(ht, GEAR_STRL("sapi_name"))) && (Z_TYPE_P(zvp) == IS_STRING || Z_TYPE_P(zvp) == IS_NULL)) {
		if (HYSSDBG_G(sapi_name_ptr)) {
			free(HYSSDBG_G(sapi_name_ptr));
		}
		if (Z_TYPE_P(zvp) == IS_STRING) {
			HYSSDBG_G(sapi_name_ptr) = sapi_capi.name = strdup(Z_STRVAL_P(zvp));
		} else {
			HYSSDBG_G(sapi_name_ptr) = sapi_capi.name = NULL;
		}
	}

	if ((zvp = gear_hash_str_find(ht, GEAR_STRL("cAPIs"))) && Z_TYPE_P(zvp) == IS_ARRAY) {
		hyssdbg_intersect_ptr pos;
		zval *cAPI;
		gear_capi_entry *mod;
		HashTable zv_registry;

		/* intersect cAPIs, unregister cAPIs loaded "too much", announce not yet registered cAPIs (hyssdbg_notice) */

		gear_hash_init(&zv_registry, gear_hash_num_elements(&capi_registry), 0, ZVAL_PTR_DTOR, 0);
		GEAR_HASH_FOREACH_PTR(&capi_registry, mod) {
			if (mod->name) {
				zval value;
				ZVAL_NEW_STR(&value, gear_string_init(mod->name, strlen(mod->name), 0));
				gear_hash_next_index_insert(&zv_registry, &value);
			}
		} GEAR_HASH_FOREACH_END();

		hyssdbg_array_intersect_init(&pos, &zv_registry, Z_ARRVAL_P(zvp));
		do {
			int mode = hyssdbg_array_intersect(&pos, &cAPI);
			if (mode < 0) {
				// loaded cAPI, but not needed
				if (strcmp(HYSSDBG_NAME, Z_STRVAL_P(cAPI))) {
					gear_hash_del(&capi_registry, Z_STR_P(cAPI));
				}
			} else if (mode > 0) {
				// not loaded cAPI
				if (!sapi_capi.name || strcmp(sapi_capi.name, Z_STRVAL_P(cAPI))) {
					hyssdbg_notice("wait", "missingcAPI=\"%.*s\"", "The cAPI %.*s isn't present in " HYSSDBG_NAME ", you still can load via dl /path/to/cAPI/%.*s.so", (int) Z_STRLEN_P(cAPI), Z_STRVAL_P(cAPI), (int) Z_STRLEN_P(cAPI), Z_STRVAL_P(cAPI));
				}
			}
		} while (cAPI);

		gear_hash_clean(&zv_registry);
	}

	if ((zvp = gear_hash_str_find(ht, GEAR_STRL("extensions"))) && Z_TYPE_P(zvp) == IS_ARRAY) {
		gear_extension *extension;
		gear_llist_position pos;
		zval *name = NULL;
		gear_string *strkey;

		extension = (gear_extension *) gear_llist_get_first_ex(&gear_extensions, &pos);
		while (extension) {
			extension = (gear_extension *) gear_llist_get_next_ex(&gear_extensions, &pos);
			if (extension == NULL){
				break;
			}

			GEAR_HASH_FOREACH_STR_KEY_PTR(Z_ARRVAL_P(zvp), strkey, name) {
				if (Z_TYPE_P(name) == IS_STRING && !gear_binary_strcmp(extension->name, strlen(extension->name), Z_STRVAL_P(name), Z_STRLEN_P(name))) {
					break;
				}
				name = NULL;
			} GEAR_HASH_FOREACH_END();

			if (name) {
				/* sigh, breaking the encapsulation, there aren't any functions manipulating the llist at the place of the gear_llist_position */
				gear_llist_element *elm = pos;
				if (elm->prev) {
					elm->prev->next = elm->next;
				} else {
					gear_extensions.head = elm->next;
				}
				if (elm->next) {
					elm->next->prev = elm->prev;
				} else {
					gear_extensions.tail = elm->prev;
				}
#if GEAR_EXTENSIONS_SUPPORT
				if (extension->shutdown) {
					extension->shutdown(extension);
				}
#endif
				if (gear_extensions.dtor) {
					gear_extensions.dtor(elm->data);
				}
				pefree(elm, gear_extensions.persistent);
				gear_extensions.count--;
			} else {
				gear_hash_del(Z_ARRVAL_P(zvp), strkey);
			}
		}

		GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(zvp), name) {
			if (Z_TYPE_P(name) == IS_STRING) {
				hyssdbg_notice("wait", "missingextension=\"%.*s\"", "The Gear extension %.*s isn't present in " HYSSDBG_NAME ", you still can load via dl /path/to/extension.so", (int) Z_STRLEN_P(name), Z_STRVAL_P(name));
			}
		} GEAR_HASH_FOREACH_END();
	}

	gear_ics_deactivate();

	if ((zvp = gear_hash_str_find(ht, GEAR_STRL("systemini"))) && Z_TYPE_P(zvp) == IS_ARRAY) {
		zval *ics_entry;
		gear_ics_entry *original_ics;
		gear_string *key;

		GEAR_HASH_FOREACH_STR_KEY_VAL(Z_ARRVAL_P(zvp), key, ics_entry) {
			if (key && Z_TYPE_P(ics_entry) == IS_STRING) {
				if ((original_ics = gear_hash_find_ptr(EG(ics_directives), key))) {
					if (!original_ics->on_modify || original_ics->on_modify(original_ics, Z_STR_P(ics_entry), original_ics->mh_arg1, original_ics->mh_arg2, original_ics->mh_arg3, GEAR_ICS_STAGE_ACTIVATE) == SUCCESS) {
						if (original_ics->modified && original_ics->orig_value != original_ics->value) {
							efree(original_ics->value);
						}
						original_ics->value = Z_STR_P(ics_entry);
						Z_ADDREF_P(ics_entry); /* don't free the string */
					}
				}
			}
		} GEAR_HASH_FOREACH_END();
	}

	if ((zvp = gear_hash_str_find(ht, GEAR_STRL("userini"))) && Z_TYPE_P(zvp) == IS_ARRAY) {
		zval *ics_entry;
		gear_string *key;

		GEAR_HASH_FOREACH_STR_KEY_VAL(Z_ARRVAL_P(zvp), key, ics_entry) {
			if (key && Z_TYPE_P(ics_entry) == IS_STRING) {
				gear_alter_ics_entry_ex(key, Z_STR_P(ics_entry), GEAR_ICS_PERDIR, GEAR_ICS_STAGE_HTACCESS, 1);
			}
		} GEAR_HASH_FOREACH_END();
	}

	zval_ptr_dtor(&zv);
	if (free_zv) {
		/* separate freeing to not dtor the symtable too, just the container zval... */
		efree(free_zv);
	}

	/* Reapply raw input */
	/* ??? */
}

HYSSDBG_COMMAND(wait) /* {{{ */
{
#ifndef HYSS_WIN32
	struct sockaddr_un local, remote;
	int rlen, sr, sl;
	unlink(HYSSDBG_G(socket_path));
	if (HYSSDBG_G(socket_server_fd) == -1) {
		int len;
		HYSSDBG_G(socket_server_fd) = sl = socket(AF_UNIX, SOCK_STREAM, 0);
		if (sl == -1) {
			hyssdbg_error("wait", "type=\"nosocket\" import=\"fail\"", "Unable to open a socket to UNIX domain socket at %s defined by hyssdbg.path ics setting", HYSSDBG_G(socket_path));
			return FAILURE;
		}

		local.sun_family = AF_UNIX;
		if (strlcpy(local.sun_path, HYSSDBG_G(socket_path), sizeof(local.sun_path)) > sizeof(local.sun_path)) {
			hyssdbg_error("wait", "type=\"nosocket\" import=\"fail\"", "Socket at %s defined by hyssdbg.path ics setting is too long", HYSSDBG_G(socket_path));
			return FAILURE;
		}
		len = strlen(local.sun_path) + sizeof(local.sun_family);
		if (bind(sl, (struct sockaddr *)&local, len) == -1) {
			hyssdbg_error("wait", "type=\"nosocket\" import=\"fail\"", "Unable to connect to UNIX domain socket at %s defined by hyssdbg.path ics setting", HYSSDBG_G(socket_path));
			return FAILURE;
		}

		chmod(HYSSDBG_G(socket_path), 0666);

		listen(sl, 2);
	} else {
		sl = HYSSDBG_G(socket_server_fd);
	}

	rlen = sizeof(remote);
	sr = accept(sl, (struct sockaddr *) &remote, (socklen_t *) &rlen);
	if (sr == -1) {
		hyssdbg_error("wait", "type=\"nosocket\" import=\"fail\"", "Unable to create a connection to UNIX domain socket at %s defined by hyssdbg.path ics setting", HYSSDBG_G(socket_path));
		close(HYSSDBG_G(socket_server_fd));
		return FAILURE;
	}

	char msglen[5];
	int recvd = 4;

	do {
		recvd -= recv(sr, &(msglen[4 - recvd]), recvd, 0);
	} while (recvd > 0);

	recvd = *(size_t *) msglen;
	char *data = emalloc(recvd);

	do {
		recvd -= recv(sr, &(data[(*(int *) msglen) - recvd]), recvd, 0);
	} while (recvd > 0);

	hyssdbg_webdata_decompress(data, *(int *) msglen);

	if (HYSSDBG_G(socket_fd) != -1) {
		close(HYSSDBG_G(socket_fd));
	}
	HYSSDBG_G(socket_fd) = sr;

	efree(data);

	hyssdbg_notice("wait", "import=\"success\"", "Successfully imported request data, stopped before executing");
#endif

	return SUCCESS;
} /* }}} */
