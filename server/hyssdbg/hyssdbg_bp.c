/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "gear_hash.h"
#include "hyssdbg.h"
#include "hyssdbg_bp.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_opcode.h"
#include "gear_globals.h"
#include "extslib/standard/hyss_string.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

/* {{{ private api functions */
static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_file(gear_op_array*);
static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_symbol(gear_function*);
static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_method(gear_op_array*);
static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_opline(hyssdbg_opline_ptr_t);
static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_opcode(gear_uchar);
static inline hyssdbg_breakbase_t *hyssdbg_find_conditional_breakpoint(gear_execute_data *execute_data); /* }}} */

/*
* Note:
*	A break point must always set the correct id and type
*	A set breakpoint function must always map new points
*/
static inline void _hyssdbg_break_mapping(int id, HashTable *table) /* {{{ */
{
	gear_hash_index_update_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP], id, table);
}
/* }}} */

#define HYSSDBG_BREAK_MAPPING(id, table) _hyssdbg_break_mapping(id, table)
#define HYSSDBG_BREAK_UNMAPPING(id) \
	gear_hash_index_del(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP], (id))

#define HYSSDBG_BREAK_INIT(b, t) do {\
	memset(&b, 0, sizeof(b)); \
	b.id = HYSSDBG_G(bp_count)++; \
	b.type = t; \
	b.disabled = 0;\
	b.hits = 0; \
} while(0)

static void hyssdbg_file_breaks_dtor(zval *data) /* {{{ */
{
	hyssdbg_breakfile_t *bp = (hyssdbg_breakfile_t*) Z_PTR_P(data);

	efree((char*)bp->filename);
	efree(bp);
} /* }}} */

static void hyssdbg_class_breaks_dtor(zval *data) /* {{{ */
{
	hyssdbg_breakmethod_t *bp = (hyssdbg_breakmethod_t *) Z_PTR_P(data);

	efree((char*)bp->class_name);
	efree((char*)bp->func_name);
	efree(bp);
} /* }}} */

static void hyssdbg_opline_class_breaks_dtor(zval *data) /* {{{ */
{
	gear_hash_destroy(Z_ARRVAL_P(data));
	efree(Z_ARRVAL_P(data));
} /* }}} */

static void hyssdbg_opline_breaks_dtor(zval *data) /* {{{ */
{
	hyssdbg_breakopline_t *bp = (hyssdbg_breakopline_t *) Z_PTR_P(data);

	if (bp->class_name) {
		efree((char*)bp->class_name);
	}
	if (bp->func_name) {
		efree((char*)bp->func_name);
	}
	efree(bp);
} /* }}} */

HYSSDBG_API void hyssdbg_reset_breakpoints(void) /* {{{ */
{
	HashTable *table;

	GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP], table) {
		hyssdbg_breakbase_t *brake;

		GEAR_HASH_FOREACH_PTR(table, brake) {
			brake->hits = 0;
		} GEAR_HASH_FOREACH_END();
	} GEAR_HASH_FOREACH_END();
} /* }}} */

HYSSDBG_API void hyssdbg_export_breakpoints(FILE *handle) /* {{{ */
{
	char *string;
	hyssdbg_export_breakpoints_to_string(&string);
	fputs(string, handle);
}
/* }}} */

HYSSDBG_API void hyssdbg_export_breakpoints_to_string(char **str) /* {{{ */
{
	HashTable *table;
	gear_ulong id = 0L;

	*str = "";

	if (gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP])) {
		hyssdbg_notice("exportbreakpoint", "count=\"%d\"", "Exporting %d breakpoints", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP]));

		/* this only looks like magic, it isn't */
		GEAR_HASH_FOREACH_NUM_KEY_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP], id, table) {
			hyssdbg_breakbase_t *brake;

			GEAR_HASH_FOREACH_PTR(table, brake) {
				if (brake->id == id) {
					char *new_str = NULL;

					switch (brake->type) {
						case HYSSDBG_BREAK_FILE: {
							gear_string *filename = hyss_addcslashes_str(((hyssdbg_breakfile_t*)brake)->filename, strlen(((hyssdbg_breakfile_t*)brake)->filename), "\\\"\n", 3);
							hyssdbg_asprintf(&new_str,
								"%sbreak \"%s\":%lu\n", *str,
								ZSTR_VAL(filename),
								((hyssdbg_breakfile_t*)brake)->line);
							gear_string_release(filename);
						} break;

						case HYSSDBG_BREAK_SYM: {
							hyssdbg_asprintf(&new_str,
								"%sbreak %s\n", *str,
								((hyssdbg_breaksymbol_t*)brake)->symbol);
						} break;

						case HYSSDBG_BREAK_METHOD: {
							hyssdbg_asprintf(&new_str,
								"%sbreak %s::%s\n", *str,
								((hyssdbg_breakmethod_t*)brake)->class_name,
								((hyssdbg_breakmethod_t*)brake)->func_name);
						} break;

						case HYSSDBG_BREAK_METHOD_OPLINE: {
							hyssdbg_asprintf(&new_str,
								"%sbreak %s::%s#%llu\n", *str,
								((hyssdbg_breakopline_t*)brake)->class_name,
								((hyssdbg_breakopline_t*)brake)->func_name,
								((hyssdbg_breakopline_t*)brake)->opline_num);
						} break;

						case HYSSDBG_BREAK_FUNCTION_OPLINE: {
							hyssdbg_asprintf(&new_str,
								"%sbreak %s#%llu\n", *str,
								((hyssdbg_breakopline_t*)brake)->func_name,
								((hyssdbg_breakopline_t*)brake)->opline_num);
						} break;

						case HYSSDBG_BREAK_FILE_OPLINE: {
							gear_string *filename = hyss_addcslashes_str(((hyssdbg_breakopline_t*)brake)->class_name, strlen(((hyssdbg_breakopline_t*)brake)->class_name), "\\\"\n", 3);
							hyssdbg_asprintf(&new_str,
								"%sbreak \"%s\":#%llu\n", *str,
								ZSTR_VAL(filename),
								((hyssdbg_breakopline_t*)brake)->opline_num);
							gear_string_release(filename);
						} break;

						case HYSSDBG_BREAK_OPCODE: {
							hyssdbg_asprintf(&new_str,
								"%sbreak %s\n", *str,
								((hyssdbg_breakop_t*)brake)->name);
						} break;

						case HYSSDBG_BREAK_COND: {
							hyssdbg_breakcond_t *conditional = (hyssdbg_breakcond_t*) brake;

							if (conditional->paramed) {
								switch (conditional->param.type) {
		                            case NUMERIC_FUNCTION_PARAM:
		                                hyssdbg_asprintf(&new_str,
		                                    "%sbreak at %s#%ld if %s\n",
		                                    *str, conditional->param.str, conditional->param.num, conditional->code);
		                            break;
		                            
		                            case NUMERIC_METHOD_PARAM:
		                                hyssdbg_asprintf(&new_str,
		                                    "%sbreak at %s::%s#%ld if %s\n",
		                                    *str, conditional->param.method.class, conditional->param.method.name, conditional->param.num, conditional->code);
		                            break;
		                            
		                            case ADDR_PARAM:
		                                hyssdbg_asprintf(&new_str,
		                                    "%sbreak at 0X%lx if %s\n",
		                                    *str, conditional->param.addr, conditional->code);
		                            break;
		                            
									case STR_PARAM:
										hyssdbg_asprintf(&new_str,
											"%sbreak at %s if %s\n", *str, conditional->param.str, conditional->code);
									break;

									case METHOD_PARAM:
										hyssdbg_asprintf(&new_str,
											"%sbreak at %s::%s if %s\n", *str,
											conditional->param.method.class, conditional->param.method.name,
											conditional->code);
									break;

									case FILE_PARAM: {
										gear_string *filename = hyss_addcslashes_str(conditional->param.file.name, strlen(conditional->param.file.name), "\\\"\n", 3);
										hyssdbg_asprintf(&new_str,
											"%sbreak at \"%s\":%lu if %s\n", *str,
											ZSTR_VAL(filename), conditional->param.file.line,
											conditional->code);
										gear_string_release(filename);
									} break;

									default: { /* do nothing */ } break;
								}
							} else {
								hyssdbg_asprintf(&new_str, "%sbreak if %s\n", str, conditional->code);
							}
						} break;

						default: continue;
					}

					if ((*str)[0]) {
						efree(*str);
					}
					*str = new_str;
				}
			} GEAR_HASH_FOREACH_END();
		} GEAR_HASH_FOREACH_END();
	}

	if ((*str) && !(*str)[0]) {
		*str = NULL;
	}
} /* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_file(const char *path, size_t path_len, long line_num) /* {{{ */
{
	hyss_stream_statbuf ssb;
	char realpath[MAXPATHLEN];
	const char *original_path = path;
	gear_bool pending = 0;
	gear_string *path_str;

	HashTable *broken, *file_breaks = &HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE];
	hyssdbg_breakfile_t new_break;

	if (!path_len) {
		if (VCWD_REALPATH(path, realpath)) {
			path = realpath;
		}
	}
	path_len = strlen(path);

	hyssdbg_debug("file path: %s, resolved path: %s, was compiled: %d\n", original_path, path, gear_hash_str_exists(&HYSSDBG_G(file_sources), path, path_len));

	if (!gear_hash_str_exists(&HYSSDBG_G(file_sources), path, path_len)) {
		if (hyss_stream_stat_path(path, &ssb) == FAILURE) {
			if (original_path[0] == '/') {
				hyssdbg_error("breakpoint", "type=\"nofile\" add=\"fail\" file=\"%s\"", "Cannot stat %s, it does not exist", original_path);
				return;
			}

			file_breaks = &HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING];
			path = original_path;
			path_len = strlen(path);
			pending = 1;
		} else if (!(ssb.sb.st_mode & (S_IFREG|S_IFLNK))) {
			hyssdbg_error("breakpoint", "type=\"notregular\" add=\"fail\" file=\"%s\"", "Cannot set breakpoint in %s, it is not a regular file", path);
			return;
		} else {
			hyssdbg_debug("File exists, but not compiled\n");
		}
	}

	path_str = gear_string_init(path, path_len, 0);

	if (!(broken = gear_hash_find_ptr(file_breaks, path_str))) {
		HashTable breaks;
		gear_hash_init(&breaks, 8, NULL, hyssdbg_file_breaks_dtor, 0);

		broken = gear_hash_add_mem(file_breaks, path_str, &breaks, sizeof(HashTable));
	}

	if (!gear_hash_index_exists(broken, line_num)) {
		HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_FILE);
		new_break.filename = estrndup(path, path_len);
		new_break.line = line_num;

		gear_hash_index_update_mem(broken, line_num, &new_break, sizeof(hyssdbg_breakfile_t));

		HYSSDBG_BREAK_MAPPING(new_break.id, broken);

		if (pending) {
			gear_string *file;
			GEAR_HASH_FOREACH_STR_KEY(&HYSSDBG_G(file_sources), file) {
				HashTable *fileht;

				hyssdbg_debug("Compare against loaded %s\n", file);

				if (!(pending = ((fileht = hyssdbg_resolve_pending_file_break_ex(ZSTR_VAL(file), ZSTR_LEN(file), path_str, broken)) == NULL))) {
					new_break = *(hyssdbg_breakfile_t *) gear_hash_index_find_ptr(fileht, line_num);
					break;
				}
			} GEAR_HASH_FOREACH_END();
		}

		if (pending) {
			HYSSDBG_G(flags) |= HYSSDBG_HAS_PENDING_FILE_BP;

			hyssdbg_notice("breakpoint", "add=\"success\" id=\"%d\" file=\"%s\" line=\"%ld\" pending=\"pending\"", "Pending breakpoint #%d added at %s:%ld", new_break.id, new_break.filename, new_break.line);
		} else {
			HYSSDBG_G(flags) |= HYSSDBG_HAS_FILE_BP;

			hyssdbg_notice("breakpoint", "add=\"success\" id=\"%d\" file=\"%s\" line=\"%ld\"", "Breakpoint #%d added at %s:%ld", new_break.id, new_break.filename, new_break.line);
		}
	} else {
		hyssdbg_error("breakpoint", "type=\"exists\" add=\"fail\" file=\"%s\" line=\"%ld\"", "Breakpoint at %s:%ld exists", path, line_num);
	}

	gear_string_release(path_str);
} /* }}} */

HYSSDBG_API HashTable *hyssdbg_resolve_pending_file_break_ex(const char *file, uint32_t filelen, gear_string *cur, HashTable *fileht) /* {{{ */
{
	hyssdbg_debug("file: %s, filelen: %u, cur: %s, curlen %u, pos: %c, memcmp: %d\n", file, filelen, ZSTR_VAL(cur), ZSTR_LEN(cur), filelen > ZSTR_LEN(cur) ? file[filelen - ZSTR_LEN(cur) - 1] : '?', filelen > ZSTR_LEN(cur) ? memcmp(file + filelen - ZSTR_LEN(cur), ZSTR_VAL(cur), ZSTR_LEN(cur)) : 0);

#ifdef _WIN32
# define WIN32_PATH_CHECK file[filelen - ZSTR_LEN(cur) - 1] == '\\'
#else
# define WIN32_PATH_CHECK 0
#endif

	if (((ZSTR_LEN(cur) < filelen && (file[filelen - ZSTR_LEN(cur) - 1] == '/' || WIN32_PATH_CHECK)) || filelen == ZSTR_LEN(cur)) && !memcmp(file + filelen - ZSTR_LEN(cur), ZSTR_VAL(cur), ZSTR_LEN(cur))) {
		hyssdbg_breakfile_t *brake, new_brake;
		HashTable *master;

		HYSSDBG_G(flags) |= HYSSDBG_HAS_FILE_BP;

		if (!(master = gear_hash_str_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE], file, filelen))) {
			HashTable new_ht;
			gear_hash_init(&new_ht, 8, NULL, hyssdbg_file_breaks_dtor, 0);
			master = gear_hash_str_add_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE], file, filelen, &new_ht, sizeof(HashTable));
		}

		GEAR_HASH_FOREACH_PTR(fileht, brake) {
			new_brake = *brake;
			new_brake.filename = estrndup(file, filelen);
			HYSSDBG_BREAK_UNMAPPING(brake->id);

			if (gear_hash_index_add_mem(master, brake->line, &new_brake, sizeof(hyssdbg_breakfile_t))) {
				HYSSDBG_BREAK_MAPPING(brake->id, master);
			}
		} GEAR_HASH_FOREACH_END();

		gear_hash_del(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING], cur);

		if (!gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING])) {
			HYSSDBG_G(flags) &= ~HYSSDBG_HAS_PENDING_FILE_BP;
		}

		hyssdbg_debug("compiled file: %s, cur bp file: %s\n", file, cur);

		return master;
	}

	return NULL;
} /* }}} */

HYSSDBG_API void hyssdbg_resolve_pending_file_break(const char *file) /* {{{ */
{
	HashTable *fileht;
	uint32_t filelen = strlen(file);
	gear_string *cur;

	hyssdbg_debug("was compiled: %s\n", file);

	GEAR_HASH_FOREACH_STR_KEY_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING], cur, fileht) {
		hyssdbg_debug("check bp: %s\n", cur);

		hyssdbg_resolve_pending_file_break_ex(file, filelen, cur, fileht);
	} GEAR_HASH_FOREACH_END();
} /* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_symbol(const char *name, size_t name_len) /* {{{ */
{
	char *lcname;

	if (*name == '\\') {
		name++;
		name_len--;
	}

	lcname = gear_str_tolower_dup(name, name_len);

	if (!gear_hash_str_exists(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM], name, name_len)) {
		hyssdbg_breaksymbol_t new_break;

		HYSSDBG_G(flags) |= HYSSDBG_HAS_SYM_BP;

		HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_SYM);
		new_break.symbol = estrndup(name, name_len);

		gear_hash_str_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM], lcname, name_len, &new_break, sizeof(hyssdbg_breaksymbol_t));

		hyssdbg_notice("breakpoint", "add=\"success\" id=\"%d\" function=\"%s\"", "Breakpoint #%d added at %s", new_break.id, new_break.symbol);

		HYSSDBG_BREAK_MAPPING(new_break.id, &HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM]);
	} else {
		hyssdbg_error("breakpoint", "type=\"exists\" add=\"fail\" function=\"%s\"", "Breakpoint exists at %s", name);
	}

	efree(lcname);
} /* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_method(const char *class_name, const char *func_name) /* {{{ */
{
	HashTable class_breaks, *class_table;
	size_t class_len = strlen(class_name);
	size_t func_len = strlen(func_name);
	char *func_lcname, *class_lcname;

	if (*class_name == '\\') {
		class_name++;
		class_len--;
	}

	func_lcname = gear_str_tolower_dup(func_name, func_len);
	class_lcname = gear_str_tolower_dup(class_name, class_len);

	if (!(class_table = gear_hash_str_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD], class_lcname, class_len))) {
		gear_hash_init(&class_breaks, 8, NULL, hyssdbg_class_breaks_dtor, 0);
		class_table = gear_hash_str_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD], class_lcname, class_len, &class_breaks, sizeof(HashTable));
	}

	if (!gear_hash_str_exists(class_table, func_lcname, func_len)) {
		hyssdbg_breakmethod_t new_break;

		HYSSDBG_G(flags) |= HYSSDBG_HAS_METHOD_BP;

		HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_METHOD);
		new_break.class_name = estrndup(class_name, class_len);
		new_break.class_len = class_len;
		new_break.func_name = estrndup(func_name, func_len);
		new_break.func_len = func_len;

		gear_hash_str_update_mem(class_table, func_lcname, func_len, &new_break, sizeof(hyssdbg_breakmethod_t));

		hyssdbg_notice("breakpoint", "add=\"success\" id=\"%d\" method=\"%s::%s\"", "Breakpoint #%d added at %s::%s", new_break.id, class_name, func_name);

		HYSSDBG_BREAK_MAPPING(new_break.id, class_table);
	} else {
		hyssdbg_error("breakpoint", "type=\"exists\" add=\"fail\" method=\"%s::%s\"", "Breakpoint exists at %s::%s", class_name, func_name);
	}

	efree(func_lcname);
	efree(class_lcname);
} /* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_opline(gear_ulong opline) /* {{{ */
{
	if (!gear_hash_index_exists(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], opline)) {
		hyssdbg_breakline_t new_break;

		HYSSDBG_G(flags) |= HYSSDBG_HAS_OPLINE_BP;

		HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_OPLINE);
		new_break.name = NULL;
		new_break.opline = opline;
		new_break.base = NULL;

		gear_hash_index_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], opline, &new_break, sizeof(hyssdbg_breakline_t));

		hyssdbg_notice("breakpoint", "add=\"success\" id=\"%d\" opline=\"%#lx\"", "Breakpoint #%d added at %#lx", new_break.id, new_break.opline);
		HYSSDBG_BREAK_MAPPING(new_break.id, &HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]);
	} else {
		hyssdbg_error("breakpoint", "type=\"exists\" add=\"fail\" opline=\"%#lx\"", "Breakpoint exists at %#lx", opline);
	}
} /* }}} */

HYSSDBG_API int hyssdbg_resolve_op_array_break(hyssdbg_breakopline_t *brake, gear_op_array *op_array) /* {{{ */
{
	hyssdbg_breakline_t opline_break;
	if (op_array->last <= brake->opline_num) {
		if (brake->class_name == NULL) {
			hyssdbg_error("breakpoint", "type=\"maxoplines\" add=\"fail\" maxoplinenum=\"%d\" function=\"%s\" usedoplinenum=\"%ld\"", "There are only %d oplines in function %s (breaking at opline %ld impossible)", op_array->last, brake->func_name, brake->opline_num);
		} else if (brake->func_name == NULL) {
			hyssdbg_error("breakpoint", "type=\"maxoplines\" add=\"fail\" maxoplinenum=\"%d\" file=\"%s\" usedoplinenum=\"%ld\"", "There are only %d oplines in file %s (breaking at opline %ld impossible)", op_array->last, brake->class_name, brake->opline_num);
		} else {
			hyssdbg_error("breakpoint", "type=\"maxoplines\" add=\"fail\" maxoplinenum=\"%d\" method=\"%s::%s\" usedoplinenum=\"%ld\"", "There are only %d oplines in method %s::%s (breaking at opline %ld impossible)", op_array->last, brake->class_name, brake->func_name, brake->opline_num);
		}

		return FAILURE;
	}

	opline_break.disabled = 0;
	opline_break.hits = 0;
	opline_break.id = brake->id;
	opline_break.opline = brake->opline = (gear_ulong)(op_array->opcodes + brake->opline_num);
	opline_break.name = NULL;
	opline_break.base = brake;
	if (op_array->scope) {
		opline_break.type = HYSSDBG_BREAK_METHOD_OPLINE;
	} else if (op_array->function_name) {
		opline_break.type = HYSSDBG_BREAK_FUNCTION_OPLINE;
	} else {
		opline_break.type = HYSSDBG_BREAK_FILE_OPLINE;
	}

	HYSSDBG_G(flags) |= HYSSDBG_HAS_OPLINE_BP;

	gear_hash_index_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], opline_break.opline, &opline_break, sizeof(hyssdbg_breakline_t));

	return SUCCESS;
} /* }}} */

HYSSDBG_API void hyssdbg_resolve_op_array_breaks(gear_op_array *op_array) /* {{{ */
{
	HashTable *func_table = &HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE];
	HashTable *oplines_table;
	hyssdbg_breakopline_t *brake;

	if (op_array->scope != NULL && !(func_table = gear_hash_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE], op_array->scope->name))) {
		return;
	}

	if (op_array->function_name == NULL) {
		if (!(oplines_table = gear_hash_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE], op_array->filename))) {
			return;
		}
	} else if (!op_array->function_name || !(oplines_table = gear_hash_find_ptr(func_table, op_array->function_name))) {
		return;
	}

	GEAR_HASH_FOREACH_PTR(oplines_table, brake) {
		if (hyssdbg_resolve_op_array_break(brake, op_array) == SUCCESS) {
			hyssdbg_breakline_t *opline_break;

			gear_hash_internal_pointer_end(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]);
			opline_break = gear_hash_get_current_data_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]);

			hyssdbg_notice("breakpoint", "add=\"success\" id=\"%d\" symbol=\"%s\" num=\"%ld\" opline=\"%#lx\"", "Breakpoint #%d resolved at %s%s%s#%ld (opline %#lx)",
				opline_break->id,
				brake->class_name ? brake->class_name : "",
				brake->class_name && brake->func_name ? "::" : "",
				brake->func_name ? brake->func_name : "",
				brake->opline_num,
				opline_break->opline);
		}
	} GEAR_HASH_FOREACH_END();
} /* }}} */

HYSSDBG_API int hyssdbg_resolve_opline_break(hyssdbg_breakopline_t *new_break) /* {{{ */
{
	HashTable *func_table = EG(function_table);
	gear_function *func;

	if (new_break->func_name == NULL) {
		if (EG(current_execute_data) == NULL) {
			if (HYSSDBG_G(ops) != NULL && !memcmp(HYSSDBG_G(ops)->filename, new_break->class_name, new_break->class_len)) {
				if (hyssdbg_resolve_op_array_break(new_break, HYSSDBG_G(ops)) == SUCCESS) {
					return SUCCESS;
				} else {
					return 2;
				}
			}
			return FAILURE;
		} else {
			gear_execute_data *execute_data = EG(current_execute_data);
			do {
				if (GEAR_USER_CODE(execute_data->func->common.type)) {
					gear_op_array *op_array = &execute_data->func->op_array;
					if (op_array->function_name == NULL && op_array->scope == NULL && new_break->class_len == ZSTR_LEN(op_array->filename) && !memcmp(ZSTR_VAL(op_array->filename), new_break->class_name, new_break->class_len)) {
						if (hyssdbg_resolve_op_array_break(new_break, op_array) == SUCCESS) {
							return SUCCESS;
						} else {
							return 2;
						}
					}
				}
			} while ((execute_data = execute_data->prev_execute_data) != NULL);
			return FAILURE;
		}
	}

	if (new_break->class_name != NULL) {
		gear_class_entry *ce;
		if (!(ce = gear_hash_str_find_ptr(EG(class_table), gear_str_tolower_dup(new_break->class_name, new_break->class_len), new_break->class_len))) {
			return FAILURE;
		}
		func_table = &ce->function_table;
	}

	if (!(func = gear_hash_str_find_ptr(func_table, gear_str_tolower_dup(new_break->func_name, new_break->func_len), new_break->func_len))) {
		if (new_break->class_name != NULL && new_break->func_name != NULL) {
			hyssdbg_error("breakpoint", "type=\"nomethod\" method=\"%s::%s\"", "Method %s doesn't exist in class %s", new_break->func_name, new_break->class_name);
			return 2;
		}
		return FAILURE;
	}

	if (func->type != GEAR_USER_FUNCTION) {
		if (new_break->class_name == NULL) {
			hyssdbg_error("breakpoint", "type=\"internalfunction\" function=\"%s\"", "%s is not a user defined function, no oplines exist", new_break->func_name);
		} else {
			hyssdbg_error("breakpoint", "type=\"internalfunction\" method=\"%s::%s\"", "%s::%s is not a user defined method, no oplines exist", new_break->class_name, new_break->func_name);
		}
		return 2;
	}

	if (hyssdbg_resolve_op_array_break(new_break, &func->op_array) == FAILURE) {
		return 2;
	}

	return SUCCESS;
} /* }}} */

/* TODO ... method/function oplines need to be normalized (leading backslash, lowercase) and file oplines need to be resolved properly */

HYSSDBG_API void hyssdbg_set_breakpoint_method_opline(const char *class, const char *method, gear_ulong opline) /* {{{ */
{
	hyssdbg_breakopline_t new_break;
	HashTable class_breaks, *class_table;
	HashTable method_breaks, *method_table;

	HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_METHOD_OPLINE);
	new_break.func_len = strlen(method);
	new_break.func_name = estrndup(method, new_break.func_len);
	new_break.class_len = strlen(class);
	new_break.class_name = estrndup(class, new_break.class_len);
	new_break.opline_num = opline;
	new_break.opline = 0;

	switch (hyssdbg_resolve_opline_break(&new_break)) {
		case FAILURE:
			hyssdbg_notice("breakpoint", "pending=\"pending\" id=\"%d\" method=\"%::%s\" num=\"%ld\"", "Pending breakpoint #%d at %s::%s#%ld", new_break.id, new_break.class_name, new_break.func_name, opline);
			break;

		case SUCCESS:
			hyssdbg_notice("breakpoint", "id=\"%d\" method=\"%::%s\" num=\"%ld\"", "Breakpoint #%d added at %s::%s#%ld", new_break.id, new_break.class_name, new_break.func_name, opline);
			break;

		case 2:
			return;
	}

	if (!(class_table = gear_hash_str_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE], new_break.class_name, new_break.class_len))) {
		gear_hash_init(&class_breaks, 8, NULL, hyssdbg_opline_class_breaks_dtor, 0);
		class_table = gear_hash_str_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE], new_break.class_name, new_break.class_len, &class_breaks, sizeof(HashTable));
	}

	if (!(method_table = gear_hash_str_find_ptr(class_table, new_break.func_name, new_break.func_len))) {
		gear_hash_init(&method_breaks, 8, NULL, hyssdbg_opline_breaks_dtor, 0);
		method_table = gear_hash_str_update_mem(class_table, new_break.func_name, new_break.func_len, &method_breaks, sizeof(HashTable));
	}

	if (gear_hash_index_exists(method_table, opline)) {
		hyssdbg_error("breakpoint", "type=\"exists\" method=\"%s\" num=\"%ld\"", "Breakpoint already exists for %s::%s#%ld", new_break.class_name, new_break.func_name, opline);
		efree((char*)new_break.func_name);
		efree((char*)new_break.class_name);
		HYSSDBG_G(bp_count)--;
		return;
	}

	HYSSDBG_G(flags) |= HYSSDBG_HAS_METHOD_OPLINE_BP;

	HYSSDBG_BREAK_MAPPING(new_break.id, method_table);

	gear_hash_index_update_mem(method_table, opline, &new_break, sizeof(hyssdbg_breakopline_t));
}
/* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_function_opline(const char *function, gear_ulong opline) /* {{{ */
{
	hyssdbg_breakopline_t new_break;
	HashTable func_breaks, *func_table;

	HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_FUNCTION_OPLINE);
	new_break.func_len = strlen(function);
	new_break.func_name = estrndup(function, new_break.func_len);
	new_break.class_len = 0;
	new_break.class_name = NULL;
	new_break.opline_num = opline;
	new_break.opline = 0;

	switch (hyssdbg_resolve_opline_break(&new_break)) {
		case FAILURE:
			hyssdbg_notice("breakpoint", "pending=\"pending\" id=\"%d\" function=\"%s\" num=\"%ld\"", "Pending breakpoint #%d at %s#%ld", new_break.id, new_break.func_name, opline);
			break;

		case SUCCESS:
			hyssdbg_notice("breakpoint", "id=\"%d\" function=\"%s\" num=\"%ld\"", "Breakpoint #%d added at %s#%ld", new_break.id, new_break.func_name, opline);
			break;

		case 2:
			return;
	}

	if (!(func_table = gear_hash_str_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE], new_break.func_name, new_break.func_len))) {
		gear_hash_init(&func_breaks, 8, NULL, hyssdbg_opline_breaks_dtor, 0);
		func_table = gear_hash_str_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE], new_break.func_name, new_break.func_len, &func_breaks, sizeof(HashTable));
	}

	if (gear_hash_index_exists(func_table, opline)) {
		hyssdbg_error("breakpoint", "type=\"exists\" function=\"%s\" num=\"%ld\"", "Breakpoint already exists for %s#%ld", new_break.func_name, opline);
		efree((char*)new_break.func_name);
		HYSSDBG_G(bp_count)--;
		return;
	}

	HYSSDBG_BREAK_MAPPING(new_break.id, func_table);

	HYSSDBG_G(flags) |= HYSSDBG_HAS_FUNCTION_OPLINE_BP;

	gear_hash_index_update_mem(func_table, opline, &new_break, sizeof(hyssdbg_breakopline_t));
}
/* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_file_opline(const char *file, gear_ulong opline) /* {{{ */
{
	hyssdbg_breakopline_t new_break;
	HashTable file_breaks, *file_table;

	HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_FILE_OPLINE);
	new_break.func_len = 0;
	new_break.func_name = NULL;
	new_break.class_len = strlen(file);
	new_break.class_name = estrndup(file, new_break.class_len);
	new_break.opline_num = opline;
	new_break.opline = 0;

	switch (hyssdbg_resolve_opline_break(&new_break)) {
		case FAILURE:
			hyssdbg_notice("breakpoint", "pending=\"pending\" id=\"%d\" file=\"%s\" num=\"%ld\"", "Pending breakpoint #%d at %s:%ld", new_break.id, new_break.class_name, opline);
			break;

		case SUCCESS:
			hyssdbg_notice("breakpoint", "id=\"%d\" file=\"%s\" num=\"%ld\"", "Breakpoint #%d added at %s:%ld", new_break.id, new_break.class_name, opline);
			break;

		case 2:
			return;
	}

	if (!(file_table = gear_hash_str_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE], new_break.class_name, new_break.class_len))) {
		gear_hash_init(&file_breaks, 8, NULL, hyssdbg_opline_breaks_dtor, 0);
		file_table = gear_hash_str_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE], new_break.class_name, new_break.class_len, &file_breaks, sizeof(HashTable));
	}

	if (gear_hash_index_exists(file_table, opline)) {
		hyssdbg_error("breakpoint", "type=\"exists\" file=\"%s\" num=\"%d\"", "Breakpoint already exists for %s:%ld", new_break.class_name, opline);
		efree((char*)new_break.class_name);
		HYSSDBG_G(bp_count)--;
		return;
	}

	HYSSDBG_BREAK_MAPPING(new_break.id, file_table);

	HYSSDBG_G(flags) |= HYSSDBG_HAS_FILE_OPLINE_BP;

	gear_hash_index_update_mem(file_table, opline, &new_break, sizeof(hyssdbg_breakopline_t));
}
/* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_opcode(const char *name, size_t name_len) /* {{{ */
{
	hyssdbg_breakop_t new_break;
	gear_ulong hash = gear_hash_func(name, name_len);

	if (gear_hash_index_exists(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPCODE], hash)) {
		hyssdbg_error("breakpoint", "type=\"exists\" opcode=\"%s\"", "Breakpoint exists for %s", name);
		return;
	}

	HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_OPCODE);
	new_break.hash = hash;
	new_break.name = estrndup(name, name_len);

	gear_hash_index_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPCODE], hash, &new_break, sizeof(hyssdbg_breakop_t));

	HYSSDBG_G(flags) |= HYSSDBG_HAS_OPCODE_BP;

	hyssdbg_notice("breakpoint", "id=\"%d\" opcode=\"%s\"", "Breakpoint #%d added at %s", new_break.id, name);
	HYSSDBG_BREAK_MAPPING(new_break.id, &HYSSDBG_G(bp)[HYSSDBG_BREAK_OPCODE]);
} /* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_opline_ex(hyssdbg_opline_ptr_t opline) /* {{{ */
{
	if (!gear_hash_index_exists(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], (gear_ulong) opline)) {
		hyssdbg_breakline_t new_break;

		HYSSDBG_G(flags) |= HYSSDBG_HAS_OPLINE_BP;

		HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_OPLINE);
		new_break.opline = (gear_ulong) opline;
		new_break.base = NULL;

		gear_hash_index_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], (gear_ulong) opline, &new_break, sizeof(hyssdbg_breakline_t));

		hyssdbg_notice("breakpoint", "id=\"%d\" opline=\"%#lx\"", "Breakpoint #%d added at %#lx", new_break.id, new_break.opline);
		HYSSDBG_BREAK_MAPPING(new_break.id, &HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]);
	} else {
		hyssdbg_error("breakpoint", "type=\"exists\" opline=\"%#lx\"", "Breakpoint exists for opline %#lx", (gear_ulong) opline);
	}
} /* }}} */

static inline void hyssdbg_create_conditional_break(hyssdbg_breakcond_t *brake, const hyssdbg_param_t *param, const char *expr, size_t expr_len, gear_ulong hash) /* {{{ */
{
	hyssdbg_breakcond_t new_break;
	uint32_t cops = CG(compiler_options);
	zval pv;

	switch (param->type) {
	    case STR_PARAM:
		case NUMERIC_FUNCTION_PARAM:
		case METHOD_PARAM:
		case NUMERIC_METHOD_PARAM:
		case FILE_PARAM:
		case ADDR_PARAM:
		    /* do nothing */
		break;
		
		default:
			hyssdbg_error("eval", "type=\"invalidparameter\"", "Invalid parameter type for conditional breakpoint");
			return;
	}

	HYSSDBG_BREAK_INIT(new_break, HYSSDBG_BREAK_COND);
	new_break.hash = hash;

	if (param) {
		new_break.paramed = 1;
		hyssdbg_copy_param(
			param, &new_break.param);
	    if (new_break.param.type == FILE_PARAM ||
	        new_break.param.type == NUMERIC_FILE_PARAM) {
	        char realpath[MAXPATHLEN];
	        
	        if (VCWD_REALPATH(new_break.param.file.name, realpath)) {
	            efree(new_break.param.file.name);
	            
	            new_break.param.file.name = estrdup(realpath);
	        } else {
	            hyssdbg_error("eval", "type=\"invalidparameter\"", "Invalid file for conditional break %s", new_break.param.file.name);
	            hyssdbg_clear_param(&new_break.param);
	            return;
	        }
	    }
	} else {
		new_break.paramed = 0;
	}

	cops = CG(compiler_options);

	CG(compiler_options) = GEAR_COMPILE_DEFAULT_FOR_EVAL;

	new_break.code = estrndup(expr, expr_len);
	new_break.code_len = expr_len;

	Z_STR(pv) = gear_string_alloc(expr_len + sizeof("return ;") - 1, 0);
	memcpy(Z_STRVAL(pv), "return ", sizeof("return ") - 1);
	memcpy(Z_STRVAL(pv) + sizeof("return ") - 1, expr, expr_len);
	Z_STRVAL(pv)[Z_STRLEN(pv) - 1] = ';';
	Z_STRVAL(pv)[Z_STRLEN(pv)] = '\0';
	Z_TYPE_INFO(pv) = IS_STRING;

	new_break.ops = gear_compile_string(&pv, "Conditional Breakpoint Code");

	zval_ptr_dtor_str(&pv);

	if (new_break.ops) {
		brake = gear_hash_index_update_mem(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND], hash, &new_break, sizeof(hyssdbg_breakcond_t));

		hyssdbg_notice("breakpoint", "id=\"%d\" expression=\"%s\" ptr=\"%p\"", "Conditional breakpoint #%d added %s/%p", brake->id, brake->code, brake->ops);

		HYSSDBG_G(flags) |= HYSSDBG_HAS_COND_BP;
		HYSSDBG_BREAK_MAPPING(new_break.id, &HYSSDBG_G(bp)[HYSSDBG_BREAK_COND]);
	} else {
		 hyssdbg_error("compile", "expression=\"%s\"", "Failed to compile code for expression %s", expr);
		 efree((char*)new_break.code);
		 HYSSDBG_G(bp_count)--;
	}

	CG(compiler_options) = cops;
} /* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_expression(const char *expr, size_t expr_len) /* {{{ */
{
	gear_ulong expr_hash = gear_inline_hash_func(expr, expr_len);
	hyssdbg_breakcond_t new_break;

	if (!gear_hash_index_exists(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND], expr_hash)) {
		hyssdbg_create_conditional_break(
			&new_break, NULL, expr, expr_len, expr_hash);
	} else {
		hyssdbg_error("breakpoint", "type=\"exists\" expression=\"%s\"", "Conditional break %s exists", expr);
	}
} /* }}} */

HYSSDBG_API void hyssdbg_set_breakpoint_at(const hyssdbg_param_t *param) /* {{{ */
{
	hyssdbg_breakcond_t new_break;
	hyssdbg_param_t *condition;
	gear_ulong hash = 0L;

	if (param->next) {
		condition = param->next;
		hash = gear_inline_hash_func(condition->str, condition->len);

		if (!gear_hash_index_exists(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND], hash)) {
			hyssdbg_create_conditional_break(&new_break, param, condition->str, condition->len, hash);
		} else {
			hyssdbg_notice("breakpoint", "type=\"exists\" arg=\"%s\"", "Conditional break %s exists at the specified location", condition->str);
		}
	}

} /* }}} */

static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_file(gear_op_array *op_array) /* {{{ */
{
	HashTable *breaks;
	hyssdbg_breakbase_t *brake;

#if 0
	hyssdbg_debug("Op at: %.*s %d\n", ZSTR_LEN(op_array->filename), ZSTR_VAL(op_array->filename), (*EG(opline_ptr))->lineno);
#endif

	/* NOTE: realpath resolution should have happened at compile time - no reason to do it here again */
	if (!(breaks = gear_hash_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE], op_array->filename))) {
		return NULL;
	}

	if (EG(current_execute_data) && (brake = gear_hash_index_find_ptr(breaks, EG(current_execute_data)->opline->lineno))) {
		return brake;
	}

	return NULL;
} /* }}} */

static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_symbol(gear_function *fbc) /* {{{ */
{
	gear_op_array *ops;

	if (fbc->type != GEAR_USER_FUNCTION) {
		return NULL;
	}

	ops = (gear_op_array *) fbc;

	if (ops->scope) {
		/* find method breaks here */
		return hyssdbg_find_breakpoint_method(ops);
	}

	if (ops->function_name) {
		hyssdbg_breakbase_t *brake;
		gear_string *fname = gear_string_tolower(ops->function_name);

		brake = gear_hash_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM], fname);

		gear_string_release(fname);
		return brake;
	} else {
		return gear_hash_str_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM], GEAR_STRL("main"));
	}
} /* }}} */

static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_method(gear_op_array *ops) /* {{{ */
{
	HashTable *class_table;
	hyssdbg_breakbase_t *brake = NULL;
	gear_string *class_lcname = gear_string_tolower(ops->scope->name);

	if ((class_table = gear_hash_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD], class_lcname))) {
		gear_string *lcname = gear_string_tolower(ops->function_name);

		brake = gear_hash_find_ptr(class_table, lcname);

		gear_string_release(lcname);
	}

	gear_string_release(class_lcname);
	return brake;
} /* }}} */

static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_opline(hyssdbg_opline_ptr_t opline) /* {{{ */
{
	hyssdbg_breakline_t *brake;

	if ((brake = gear_hash_index_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], (gear_ulong) opline)) && brake->base) {
		return (hyssdbg_breakbase_t *)brake->base;
	}

	return (hyssdbg_breakbase_t *) brake;
} /* }}} */

static inline hyssdbg_breakbase_t *hyssdbg_find_breakpoint_opcode(gear_uchar opcode) /* {{{ */
{
	const char *opname = gear_get_opcode_name(opcode);

	if (!opname) {
		return NULL;
	}

	return gear_hash_index_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPCODE], gear_hash_func(opname, strlen(opname)));
} /* }}} */

static inline gear_bool hyssdbg_find_breakpoint_param(hyssdbg_param_t *param, gear_execute_data *execute_data) /* {{{ */
{
	gear_function *function = execute_data->func;

	switch (param->type) {
		case NUMERIC_FUNCTION_PARAM:
		case STR_PARAM: {
			/* function breakpoint */

			if (function->type != GEAR_USER_FUNCTION) {
				return 0;
			}

			{
				const char *str = NULL;
				size_t len = 0L;
				gear_op_array *ops = (gear_op_array*)function;
				str = ops->function_name ? ZSTR_VAL(ops->function_name) : "main";
				len = ops->function_name ? ZSTR_LEN(ops->function_name) : strlen(str);

				if (len == param->len && memcmp(param->str, str, len) == SUCCESS) {
					return param->type == STR_PARAM || execute_data->opline - ops->opcodes == param->num;
				}
			}
		} break;

		case FILE_PARAM: {
			if (param->file.line == gear_get_executed_lineno()) {
				const char *str = gear_get_executed_filename();
				size_t lengths[2] = {strlen(param->file.name), strlen(str)};

				if (lengths[0] == lengths[1]) {
					return (memcmp(
						param->file.name, str, lengths[0]) == SUCCESS);
				}
			}
		} break;

		case NUMERIC_METHOD_PARAM:
		case METHOD_PARAM: {
			if (function->type != GEAR_USER_FUNCTION) {
				return 0;
			}

			{
				gear_op_array *ops = (gear_op_array*) function;

				if (ops->scope) {
					size_t lengths[2] = { strlen(param->method.class), ZSTR_LEN(ops->scope->name) };
					if (lengths[0] == lengths[1] && memcmp(param->method.class, ops->scope->name, lengths[0]) == SUCCESS) {
						lengths[0] = strlen(param->method.name);
						lengths[1] = ZSTR_LEN(ops->function_name);

						if (lengths[0] == lengths[1] && memcmp(param->method.name, ops->function_name, lengths[0]) == SUCCESS) {
							return param->type == METHOD_PARAM || (execute_data->opline - ops->opcodes) == param->num;
						}
					}
				}
			}
		} break;

		case ADDR_PARAM: {
			return ((gear_ulong)(hyssdbg_opline_ptr_t)execute_data->opline == param->addr);
		} break;

		default: {
			/* do nothing */
		} break;
	}
	return 0;
} /* }}} */

static inline hyssdbg_breakbase_t *hyssdbg_find_conditional_breakpoint(gear_execute_data *execute_data) /* {{{ */
{
	hyssdbg_breakcond_t *bp;
	int breakpoint = FAILURE;

	GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND], bp) {
		zval retval;
		const gear_op *orig_opline = EG(current_execute_data)->opline;
		gear_function *orig_func = EG(current_execute_data)->func;
		zval *orig_retval = EG(current_execute_data)->return_value;

		if (((hyssdbg_breakbase_t*)bp)->disabled) {
			continue;
		}

		if (bp->paramed) {
			if (!hyssdbg_find_breakpoint_param(&bp->param, execute_data)) {
				continue;
			}
		}

		EG(no_extensions) = 1;

		gear_rebuild_symbol_table();

		gear_try {
			HYSSDBG_G(flags) |= HYSSDBG_IN_COND_BP;
			gear_execute(bp->ops, &retval);
			if (gear_is_true(&retval)) {
				breakpoint = SUCCESS;
			}
 		} gear_end_try();

		EG(no_extensions) = 1;
		EG(current_execute_data)->opline = orig_opline;
		EG(current_execute_data)->func = orig_func;
		EG(current_execute_data)->return_value = orig_retval;
		HYSSDBG_G(flags) &= ~HYSSDBG_IN_COND_BP;

		if (breakpoint == SUCCESS) {
			break;
		}
	} GEAR_HASH_FOREACH_END();

	return (breakpoint == SUCCESS) ? ((hyssdbg_breakbase_t *) bp) : NULL;
} /* }}} */

HYSSDBG_API hyssdbg_breakbase_t *hyssdbg_find_breakpoint(gear_execute_data *execute_data) /* {{{ */
{
	hyssdbg_breakbase_t *base = NULL;

	if (!(HYSSDBG_G(flags) & HYSSDBG_IS_BP_ENABLED)) {
		return NULL;
	}

	/* conditions cannot be executed by eval()'d code */
	if (!(HYSSDBG_G(flags) & HYSSDBG_IN_EVAL) &&
		(HYSSDBG_G(flags) & HYSSDBG_HAS_COND_BP) &&
		(base = hyssdbg_find_conditional_breakpoint(execute_data))) {
		goto result;
	}

	if ((HYSSDBG_G(flags) & HYSSDBG_HAS_FILE_BP) && (base = hyssdbg_find_breakpoint_file(&execute_data->func->op_array))) {
		goto result;
	}

	if (HYSSDBG_G(flags) & (HYSSDBG_HAS_METHOD_BP|HYSSDBG_HAS_SYM_BP)) {
		gear_op_array *op_array = &execute_data->func->op_array;
		/* check we are at the beginning of the stack, but after argument RECV */
		if (execute_data->opline == op_array->opcodes + op_array->num_args + !!(op_array->fn_flags & GEAR_ACC_VARIADIC)) {
			if ((base = hyssdbg_find_breakpoint_symbol(execute_data->func))) {
				goto result;
			}
		}
	}

	if ((HYSSDBG_G(flags) & HYSSDBG_HAS_OPLINE_BP) && (base = hyssdbg_find_breakpoint_opline((hyssdbg_opline_ptr_t) execute_data->opline))) {
		goto result;
	}

	if ((HYSSDBG_G(flags) & HYSSDBG_HAS_OPCODE_BP) && (base = hyssdbg_find_breakpoint_opcode(execute_data->opline->opcode))) {
		goto result;
	}

	return NULL;

result:
	/* we return nothing for disable breakpoints */
	if (base->disabled) {
		return NULL;
	}

	return base;
} /* }}} */

HYSSDBG_API void hyssdbg_delete_breakpoint(gear_ulong num) /* {{{ */
{
	HashTable *table;
	hyssdbg_breakbase_t *brake;
	gear_string *strkey;
	gear_ulong numkey;

	if ((brake = hyssdbg_find_breakbase_ex(num, &table, &numkey, &strkey))) {
		int type = brake->type;
		char *name = NULL;
		size_t name_len = 0L;

		switch (type) {
			case HYSSDBG_BREAK_FILE:
			case HYSSDBG_BREAK_METHOD:
				if (gear_hash_num_elements(table) == 1) {
					name = estrdup(brake->name);
					name_len = strlen(name);
					if (gear_hash_num_elements(&HYSSDBG_G(bp)[type]) == 1) {
						HYSSDBG_G(flags) &= ~(1<<(brake->type+1));
					}
				}
			break;

			default: {
				if (gear_hash_num_elements(table) == 1) {
					HYSSDBG_G(flags) &= ~(1<<(brake->type+1));
				}
			}
		}

		switch (type) {
			case HYSSDBG_BREAK_FILE_OPLINE:
			case HYSSDBG_BREAK_FUNCTION_OPLINE:
			case HYSSDBG_BREAK_METHOD_OPLINE:
				if (gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]) == 1) {
					HYSSDBG_G(flags) &= HYSSDBG_HAS_OPLINE_BP;
				}
				gear_hash_index_del(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], ((hyssdbg_breakopline_t *) brake)->opline);
		}

		if (strkey) {
			gear_hash_del(table, strkey);
		} else {
			gear_hash_index_del(table, numkey);
		}

		switch (type) {
			case HYSSDBG_BREAK_FILE:
			case HYSSDBG_BREAK_METHOD:
				if (name) {
					gear_hash_str_del(&HYSSDBG_G(bp)[type], name, name_len);
					efree(name);
				}
			break;
		}

		hyssdbg_notice("breakpoint", "deleted=\"success\" id=\"%ld\"", "Deleted breakpoint #%ld", num);
		HYSSDBG_BREAK_UNMAPPING(num);
	} else {
		hyssdbg_error("breakpoint", "type=\"nobreakpoint\" deleted=\"fail\" id=\"%ld\"", "Failed to find breakpoint #%ld", num);
	}
} /* }}} */

HYSSDBG_API void hyssdbg_clear_breakpoints(void) /* {{{ */
{
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPCODE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP]);

	HYSSDBG_G(flags) &= ~HYSSDBG_BP_MASK;

	HYSSDBG_G(bp_count) = 0;
} /* }}} */

HYSSDBG_API void hyssdbg_hit_breakpoint(hyssdbg_breakbase_t *brake, gear_bool output) /* {{{ */
{
	brake->hits++;

	if (output) {
		hyssdbg_print_breakpoint(brake);
	}
} /* }}} */

HYSSDBG_API void hyssdbg_print_breakpoint(hyssdbg_breakbase_t *brake) /* {{{ */
{
	if (!brake)
		goto unknown;

	switch (brake->type) {
		case HYSSDBG_BREAK_FILE: {
			hyssdbg_notice("breakpoint", "id=\"%d\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Breakpoint #%d at %s:%ld, hits: %lu",
				((hyssdbg_breakfile_t*)brake)->id,
				((hyssdbg_breakfile_t*)brake)->filename,
				((hyssdbg_breakfile_t*)brake)->line,
				((hyssdbg_breakfile_t*)brake)->hits);
		} break;

		case HYSSDBG_BREAK_SYM: {
			hyssdbg_notice("breakpoint", "id=\"%d\" function=\"%s\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Breakpoint #%d in %s() at %s:%u, hits: %lu",
				((hyssdbg_breaksymbol_t*)brake)->id,
				((hyssdbg_breaksymbol_t*)brake)->symbol,
				gear_get_executed_filename(),
				gear_get_executed_lineno(),
				((hyssdbg_breakfile_t*)brake)->hits);
		} break;

		case HYSSDBG_BREAK_OPLINE: {
			hyssdbg_notice("breakpoint", "id=\"%d\" opline=\"%#lx\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Breakpoint #%d in %#lx at %s:%u, hits: %lu",
				((hyssdbg_breakline_t*)brake)->id,
				((hyssdbg_breakline_t*)brake)->opline,
				gear_get_executed_filename(),
				gear_get_executed_lineno(),
				((hyssdbg_breakline_t*)brake)->hits);
		} break;

		case HYSSDBG_BREAK_METHOD_OPLINE: {
			 hyssdbg_notice("breakpoint", "id=\"%d\" method=\"%s::%s\" num=\"%lu\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Breakpoint #%d in %s::%s()#%lu at %s:%u, hits: %lu",
				((hyssdbg_breakopline_t*)brake)->id,
				((hyssdbg_breakopline_t*)brake)->class_name,
				((hyssdbg_breakopline_t*)brake)->func_name,
				((hyssdbg_breakopline_t*)brake)->opline_num,
				gear_get_executed_filename(),
				gear_get_executed_lineno(),
				((hyssdbg_breakopline_t*)brake)->hits);
		} break;

		case HYSSDBG_BREAK_FUNCTION_OPLINE: {
			 hyssdbg_notice("breakpoint", "id=\"%d\" num=\"%lu\" function=\"%s\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Breakpoint #%d in %s()#%lu at %s:%u, hits: %lu",
				((hyssdbg_breakopline_t*)brake)->id,
				((hyssdbg_breakopline_t*)brake)->func_name,
				((hyssdbg_breakopline_t*)brake)->opline_num,
				gear_get_executed_filename(),
				gear_get_executed_lineno(),
				((hyssdbg_breakopline_t*)brake)->hits);
		} break;

		case HYSSDBG_BREAK_FILE_OPLINE: {
			 hyssdbg_notice("breakpoint", "id=\"%d\" num=\"%lu\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Breakpoint #%d in #%lu at %s:%u, hits: %lu",
				((hyssdbg_breakopline_t*)brake)->id,
				((hyssdbg_breakopline_t*)brake)->opline_num,
				gear_get_executed_filename(),
				gear_get_executed_lineno(),
				((hyssdbg_breakopline_t*)brake)->hits);
		} break;

		case HYSSDBG_BREAK_OPCODE: {
			 hyssdbg_notice("breakpoint", "id=\"%d\" opcode=\"%s\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Breakpoint #%d in %s at %s:%u, hits: %lu",
				((hyssdbg_breakop_t*)brake)->id,
				((hyssdbg_breakop_t*)brake)->name,
				gear_get_executed_filename(),
				gear_get_executed_lineno(),
				((hyssdbg_breakop_t*)brake)->hits);
		} break;

		case HYSSDBG_BREAK_METHOD: {
			 hyssdbg_notice("breakpoint", "id=\"%d\" method=\"%s::%s\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Breakpoint #%d in %s::%s() at %s:%u, hits: %lu",
				((hyssdbg_breakmethod_t*)brake)->id,
				((hyssdbg_breakmethod_t*)brake)->class_name,
				((hyssdbg_breakmethod_t*)brake)->func_name,
				gear_get_executed_filename(),
				gear_get_executed_lineno(),
				((hyssdbg_breakmethod_t*)brake)->hits);
		} break;

		case HYSSDBG_BREAK_COND: {
			if (((hyssdbg_breakcond_t*)brake)->paramed) {
				char *param;
				hyssdbg_notice("breakpoint", "id=\"%d\" location=\"%s\" eval=\"%s\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Conditional breakpoint #%d: at %s if %s at %s:%u, hits: %lu",
					((hyssdbg_breakcond_t*)brake)->id,
					hyssdbg_param_tostring(&((hyssdbg_breakcond_t*)brake)->param, &param),
					((hyssdbg_breakcond_t*)brake)->code,
					gear_get_executed_filename(),
					gear_get_executed_lineno(),
					((hyssdbg_breakcond_t*)brake)->hits);
				if (param)
					free(param);
			} else {
				hyssdbg_notice("breakpoint", "id=\"%d\" eval=\"%s\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Conditional breakpoint #%d: on %s == true at %s:%u, hits: %lu",
					((hyssdbg_breakcond_t*)brake)->id,
					((hyssdbg_breakcond_t*)brake)->code,
					gear_get_executed_filename(),
					gear_get_executed_lineno(),
					((hyssdbg_breakcond_t*)brake)->hits);
			}

		} break;

		default: {
unknown:
			hyssdbg_notice("breakpoint", "id=\"\" file=\"%s\" line=\"%ld\" hits=\"%lu\"", "Unknown breakpoint at %s:%u",
				gear_get_executed_filename(),
				gear_get_executed_lineno());
		}
	}
} /* }}} */

HYSSDBG_API void hyssdbg_enable_breakpoint(gear_ulong id) /* {{{ */
{
	hyssdbg_breakbase_t *brake = hyssdbg_find_breakbase(id);

	if (brake) {
		brake->disabled = 0;
	}
} /* }}} */

HYSSDBG_API void hyssdbg_disable_breakpoint(gear_ulong id) /* {{{ */
{
	hyssdbg_breakbase_t *brake = hyssdbg_find_breakbase(id);

	if (brake) {
		brake->disabled = 1;
	}
} /* }}} */

HYSSDBG_API void hyssdbg_enable_breakpoints(void) /* {{{ */
{
	HYSSDBG_G(flags) |= HYSSDBG_IS_BP_ENABLED;
} /* }}} */

HYSSDBG_API void hyssdbg_disable_breakpoints(void) { /* {{{ */
	HYSSDBG_G(flags) &= ~HYSSDBG_IS_BP_ENABLED;
} /* }}} */

HYSSDBG_API hyssdbg_breakbase_t *hyssdbg_find_breakbase(gear_ulong id) /* {{{ */
{
	HashTable *table;
	gear_string *strkey;
	gear_ulong numkey;

	return hyssdbg_find_breakbase_ex(id, &table, &numkey, &strkey);
} /* }}} */

HYSSDBG_API hyssdbg_breakbase_t *hyssdbg_find_breakbase_ex(gear_ulong id, HashTable **table, gear_ulong *numkey, gear_string **strkey) /* {{{ */
{
	if ((*table = gear_hash_index_find_ptr(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP], id))) {
		hyssdbg_breakbase_t *brake;

		GEAR_HASH_FOREACH_KEY_PTR(*table, *numkey, *strkey, brake) {
			if (brake->id == id) {
				return brake;
			}
		} GEAR_HASH_FOREACH_END();
	}

	return NULL;
} /* }}} */

HYSSDBG_API void hyssdbg_print_breakpoints(gear_ulong type) /* {{{ */
{
	hyssdbg_xml("<breakpoints %r>");

	switch (type) {
		case HYSSDBG_BREAK_SYM: if ((HYSSDBG_G(flags) & HYSSDBG_HAS_SYM_BP)) {
			hyssdbg_breaksymbol_t *brake;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("Function Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM], brake) {
				hyssdbg_writeln("function", "id=\"%d\" name=\"%s\" disabled=\"%s\"", "#%d\t\t%s%s",
					brake->id, brake->symbol,
					((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
			} GEAR_HASH_FOREACH_END();
		} break;

		case HYSSDBG_BREAK_METHOD: if ((HYSSDBG_G(flags) & HYSSDBG_HAS_METHOD_BP)) {
			HashTable *class_table;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("Method Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD], class_table) {
				hyssdbg_breakmethod_t *brake;

				GEAR_HASH_FOREACH_PTR(class_table, brake) {
					hyssdbg_writeln("method", "id=\"%d\" name=\"%s::%s\" disabled=\"%s\"", "#%d\t\t%s::%s%s",
						brake->id, brake->class_name, brake->func_name,
						((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
				} GEAR_HASH_FOREACH_END();
			} GEAR_HASH_FOREACH_END();
		} break;

		case HYSSDBG_BREAK_FILE: if ((HYSSDBG_G(flags) & HYSSDBG_HAS_FILE_BP)) {
			HashTable *points;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("File Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE], points) {
				hyssdbg_breakfile_t *brake;

				GEAR_HASH_FOREACH_PTR(points, brake) {
					hyssdbg_writeln("file", "id=\"%d\" name=\"%s\" line=\"%lu\" disabled=\"%s\"", "#%d\t\t%s:%lu%s",
						brake->id, brake->filename, brake->line,
						((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
				} GEAR_HASH_FOREACH_END();
			} GEAR_HASH_FOREACH_END();
		}  if ((HYSSDBG_G(flags) & HYSSDBG_HAS_PENDING_FILE_BP)) {
			HashTable *points;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("Pending File Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING], points) {
				hyssdbg_breakfile_t *brake;

				GEAR_HASH_FOREACH_PTR(points, brake) {
					hyssdbg_writeln("file", "id=\"%d\" name=\"%s\" line=\"%lu\" disabled=\"%s\" pending=\"pending\"", "#%d\t\t%s:%lu%s",
						brake->id, brake->filename, brake->line,
						((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
				} GEAR_HASH_FOREACH_END();
			} GEAR_HASH_FOREACH_END();
		} break;

		case HYSSDBG_BREAK_OPLINE: if ((HYSSDBG_G(flags) & HYSSDBG_HAS_OPLINE_BP)) {
			hyssdbg_breakline_t *brake;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("Opline Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], brake) {
				const char *type;
				switch (brake->type) {
					case HYSSDBG_BREAK_METHOD_OPLINE:
						type = "method";
						goto print_opline;
					case HYSSDBG_BREAK_FUNCTION_OPLINE:
						type = "function";
						goto print_opline;
					case HYSSDBG_BREAK_FILE_OPLINE:
						type = "method";

					print_opline: {
						if (brake->type == HYSSDBG_BREAK_METHOD_OPLINE) {
							type = "method";
						} else if (brake->type == HYSSDBG_BREAK_FUNCTION_OPLINE) {
							type = "function";
						} else if (brake->type == HYSSDBG_BREAK_FILE_OPLINE) {
							type = "file";
						}

						hyssdbg_writeln("opline", "id=\"%d\" num=\"%#lx\" type=\"%s\" disabled=\"%s\"", "#%d\t\t%#lx\t\t(%s breakpoint)%s",
							brake->id, brake->opline, type,
							((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
					} break;

					default:
						hyssdbg_writeln("opline", "id=\"%d\" num=\"%#lx\" disabled=\"%s\"", "#%d\t\t%#lx%s",
							brake->id, brake->opline,
							((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
						break;
				}
			} GEAR_HASH_FOREACH_END();
		} break;

		case HYSSDBG_BREAK_METHOD_OPLINE: if ((HYSSDBG_G(flags) & HYSSDBG_HAS_METHOD_OPLINE_BP)) {
			HashTable *class_table, *method_table;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("Method opline Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE], class_table) {
				GEAR_HASH_FOREACH_PTR(class_table, method_table) {
					hyssdbg_breakopline_t *brake;

					GEAR_HASH_FOREACH_PTR(method_table, brake) {
						hyssdbg_writeln("methodopline", "id=\"%d\" name=\"%s::%s\" num=\"%ld\" disabled=\"%s\"", "#%d\t\t%s::%s opline %ld%s",
							brake->id, brake->class_name, brake->func_name, brake->opline_num,
							((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
					} GEAR_HASH_FOREACH_END();
				} GEAR_HASH_FOREACH_END();
			} GEAR_HASH_FOREACH_END();
		} break;

		case HYSSDBG_BREAK_FUNCTION_OPLINE: if ((HYSSDBG_G(flags) & HYSSDBG_HAS_FUNCTION_OPLINE_BP)) {
			HashTable *function_table;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("Function opline Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE], function_table) {
				hyssdbg_breakopline_t *brake;

				GEAR_HASH_FOREACH_PTR(function_table, brake) {
					hyssdbg_writeln("functionopline", "id=\"%d\" name=\"%s\" num=\"%ld\" disabled=\"%s\"", "#%d\t\t%s opline %ld%s",
						brake->id, brake->func_name, brake->opline_num,
						((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
				} GEAR_HASH_FOREACH_END();
			} GEAR_HASH_FOREACH_END();
		} break;

		case HYSSDBG_BREAK_FILE_OPLINE: if ((HYSSDBG_G(flags) & HYSSDBG_HAS_FILE_OPLINE_BP)) {
			HashTable *file_table;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("File opline Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE], file_table) {
				hyssdbg_breakopline_t *brake;

				GEAR_HASH_FOREACH_PTR(file_table, brake) {
					hyssdbg_writeln("fileopline", "id=\"%d\" name=\"%s\" num=\"%ld\" disabled=\"%s\"", "#%d\t\t%s opline %ld%s",
						brake->id, brake->class_name, brake->opline_num,
						((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
				} GEAR_HASH_FOREACH_END();
			} GEAR_HASH_FOREACH_END();
		} break;

		case HYSSDBG_BREAK_COND: if ((HYSSDBG_G(flags) & HYSSDBG_HAS_COND_BP)) {
			hyssdbg_breakcond_t *brake;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("Conditional Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND], brake) {
				if (brake->paramed) {
					switch (brake->param.type) {
						case STR_PARAM:
							hyssdbg_writeln("evalfunction", "id=\"%d\" name=\"%s\" eval=\"%s\" disabled=\"%s\"", "#%d\t\tat %s if %s%s",
				 				brake->id, brake->param.str, brake->code,
				 				((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
						break;

						case NUMERIC_FUNCTION_PARAM:
							hyssdbg_writeln("evalfunctionopline", "id=\"%d\" name=\"%s\" num=\"%ld\" eval=\"%s\" disabled=\"%s\"", "#%d\t\tat %s#%ld if %s%s",
				 				brake->id, brake->param.str, brake->param.num, brake->code,
				 				((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
						break;

						case METHOD_PARAM:
							hyssdbg_writeln("evalmethod", "id=\"%d\" name=\"%s::%s\" eval=\"%s\" disabled=\"%s\"", "#%d\t\tat %s::%s if %s%s",
				 				brake->id, brake->param.method.class, brake->param.method.name, brake->code,
				 				((hyssdbg_breakbase_t*)brake)->disabled ? " [disabled]" : "");
						break;

						case NUMERIC_METHOD_PARAM:
							hyssdbg_writeln("evalmethodopline", "id=\"%d\" name=\"%s::%s\" num=\"%d\" eval=\"%s\" disabled=\"%s\"", "#%d\t\tat %s::%s#%ld if %s%s",
				 				brake->id, brake->param.method.class, brake->param.method.name, brake->param.num, brake->code,
				 				((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
						break;

						case FILE_PARAM:
							hyssdbg_writeln("evalfile", "id=\"%d\" name=\"%s\" line=\"%d\" eval=\"%s\" disabled=\"%s\"", "#%d\t\tat %s:%lu if %s%s",
				 				brake->id, brake->param.file.name, brake->param.file.line, brake->code,
				 				((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
						break;

						case ADDR_PARAM:
							hyssdbg_writeln("evalopline", "id=\"%d\" opline=\"%#lx\" eval=\"%s\" disabled=\"%s\"", "#%d\t\tat #%lx if %s%s",
				 				brake->id, brake->param.addr, brake->code,
				 				((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
						break;

						default:
							hyssdbg_error("eval", "type=\"invalidparameter\"", "Invalid parameter type for conditional breakpoint");
						return;
					}
				} else {
					hyssdbg_writeln("eval", "id=\"%d\" eval=\"%s\" disabled=\"%s\"", "#%d\t\tif %s%s",
				 		brake->id, brake->code,
				 		((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
				}
			} GEAR_HASH_FOREACH_END();
		} break;

		case HYSSDBG_BREAK_OPCODE: if (HYSSDBG_G(flags) & HYSSDBG_HAS_OPCODE_BP) {
			hyssdbg_breakop_t *brake;

			hyssdbg_out(SEPARATE "\n");
			hyssdbg_out("Opcode Breakpoints:\n");
			GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPCODE], brake) {
				hyssdbg_writeln("opcode", "id=\"%d\" name=\"%s\" disabled=\"%s\"", "#%d\t\t%s%s",
					brake->id, brake->name,
					((hyssdbg_breakbase_t *) brake)->disabled ? " [disabled]" : "");
			} GEAR_HASH_FOREACH_END();
		} break;
	}

	hyssdbg_xml("</breakpoints>");
} /* }}} */
