dnl config.m4 for sapi hyssdbg

HYSS_ARG_ENABLE(hyssdbg, for hyssdbg support,
[  --enable-hyssdbg         Build hyssdbg], yes, yes)

HYSS_ARG_ENABLE(hyssdbg-webhelper, for hyssdbg web SAPI support,
[  --enable-hyssdbg-webhelper
                          Build hyssdbg web SAPI support], no)

HYSS_ARG_ENABLE(hyssdbg-debug, for hyssdbg debug build,
[  --enable-hyssdbg-debug   Build hyssdbg in debug mode], no, no)

HYSS_ARG_ENABLE(hyssdbg-readline, for hyssdbg readline support,
[  --enable-hyssdbg-readline   Enable readline support in hyssdbg (depends on static extslib/readline)], no, no)

if test "$BUILD_HYSSDBG" = "" && test "$HYSS_HYSSDBG" != "no"; then
  AC_HEADER_TIOCGWINSZ
  AC_DEFINE(HAVE_HYSSDBG, 1, [ ])

  if test "$HYSS_HYSSDBG_DEBUG" != "no"; then
    AC_DEFINE(HYSSDBG_DEBUG, 1, [ ])
  else
    AC_DEFINE(HYSSDBG_DEBUG, 0, [ ])
  fi

  HYSS_HYSSDBG_CFLAGS="-D_GNU_SOURCE -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1"
  HYSS_HYSSDBG_FILES="hyssdbg.c hyssdbg_parser.c hyssdbg_lexer.c hyssdbg_prompt.c hyssdbg_help.c hyssdbg_break.c hyssdbg_print.c hyssdbg_bp.c hyssdbg_opcode.c hyssdbg_list.c hyssdbg_utils.c hyssdbg_info.c hyssdbg_cmd.c hyssdbg_set.c hyssdbg_frame.c hyssdbg_watch.c hyssdbg_btree.c hyssdbg_sigsafe.c hyssdbg_wait.c hyssdbg_io.c hyssdbg_eol.c hyssdbg_out.c"

  AC_MSG_CHECKING([for hyssdbg and readline integration])
  if test "$HYSS_HYSSDBG_READLINE" = "yes"; then
    if test "$HYSS_READLINE" != "no" -o  "$HYSS_LIBEDIT" != "no"; then
  	  AC_DEFINE(HAVE_HYSSDBG_READLINE, 1, [ ])
  	  HYSSDBG_EXTRA_LIBS="$HYSS_READLINE_LIBS"
  	  AC_MSG_RESULT([ok])
  	else
  	  AC_MSG_RESULT([readline is not available])
    fi
  else
    AC_MSG_RESULT([disabled])
  fi

  HYSS_SUBST(HYSS_HYSSDBG_CFLAGS)
  HYSS_SUBST(HYSS_HYSSDBG_FILES)
  HYSS_SUBST(HYSSDBG_EXTRA_LIBS)

  HYSS_ADD_MAKEFILE_FRAGMENT([$abs_srcdir/server/hyssdbg/Makefile.frag], [$abs_srcdir/server/hyssdbg], [$abs_builddir/server/hyssdbg])
  HYSS_SELECT_SAPI(hyssdbg, program, $HYSS_HYSSDBG_FILES, $HYSS_HYSSDBG_CFLAGS, [$(SAPI_HYSSDBG_PATH)])

  BUILD_BINARY="server/hyssdbg/hyssdbg"
  BUILD_SHARED="server/hyssdbg/libhyssdbg.la"

  BUILD_HYSSDBG="\$(LIBTOOL) --mode=link \
        \$(CC) -export-dynamic \$(CFLAGS_CLEAN) \$(EXTRA_CFLAGS) \$(EXTRA_LDFLAGS_PROGRAM) \$(LDFLAGS) \$(HYSS_RPATHS) \
                \$(HYSS_GLOBAL_OBJS) \
                \$(HYSS_BINARY_OBJS) \
                \$(HYSS_HYSSDBG_OBJS) \
                \$(EXTRA_LIBS) \
                \$(HYSSDBG_EXTRA_LIBS) \
                \$(GEAR_EXTRA_LIBS) \
                \$(HYSS_FRAMEWORKS) \
         -o \$(BUILD_BINARY)"

  BUILD_HYSSDBG_SHARED="\$(LIBTOOL) --mode=link \
        \$(CC) -shared -Wl,-soname,libhyssdbg.so -export-dynamic \$(CFLAGS_CLEAN) \$(EXTRA_CFLAGS) \$(EXTRA_LDFLAGS_PROGRAM) \$(LDFLAGS) \$(HYSS_RPATHS) \
                \$(HYSS_GLOBAL_OBJS) \
                \$(HYSS_BINARY_OBJS) \
                \$(HYSS_HYSSDBG_OBJS) \
                \$(EXTRA_LIBS) \
                \$(HYSSDBG_EXTRA_LIBS) \
                \$(GEAR_EXTRA_LIBS) \
                \-DHYSSDBG_SHARED \
         -o \$(BUILD_SHARED)"

  HYSS_SUBST(BUILD_BINARY)
  HYSS_SUBST(BUILD_SHARED)
  HYSS_SUBST(BUILD_HYSSDBG)
  HYSS_SUBST(BUILD_HYSSDBG_SHARED)

  HYSS_OUTPUT(server/hyssdbg/hyssdbg.1)
fi

if test "$HYSS_HYSSDBG_WEBHELPER" != "no"; then
  HYSS_NEW_EXTENSION(hyssdbg_webhelper, hyssdbg_rinit_hook.c hyssdbg_webdata_transfer.c, $ext_shared)
fi

dnl ## Local Variables:
dnl ## tab-width: 4
dnl ## End:
