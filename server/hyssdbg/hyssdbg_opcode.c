/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg.h"
#include "gear_vm_opcodes.h"
#include "gear_compile.h"
#include "hyssdbg_opcode.h"
#include "hyssdbg_utils.h"
#include "extslib/standard/hyss_string.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

static inline const char *hyssdbg_decode_opcode(gear_uchar opcode) /* {{{ */
{
	const char *ret = gear_get_opcode_name(opcode);
	if (ret) {
		return ret + 5; /* Skip GEAR_ prefix */
	}
	return "UNKNOWN";
} /* }}} */

static inline char *hyssdbg_decode_op(
		gear_op_array *ops, const gear_op *opline, const znode_op *op, uint32_t type) /* {{{ */
{
	char *decode = NULL;

	switch (type) {
		case IS_CV: {
			gear_string *var = ops->vars[EX_VAR_TO_NUM(op->var)];
			spprintf(&decode, 0, "$%.*s%c",
				ZSTR_LEN(var) <= 19 ? (int) ZSTR_LEN(var) : 18,
				ZSTR_VAL(var), ZSTR_LEN(var) <= 19 ? 0 : '+');
		} break;

		case IS_VAR:
			spprintf(&decode, 0, "@%u", EX_VAR_TO_NUM(op->var) - ops->last_var);
		break;
		case IS_TMP_VAR:
			spprintf(&decode, 0, "~%u", EX_VAR_TO_NUM(op->var) - ops->last_var);
		break;
		case IS_CONST: {
			zval *literal = RT_CONSTANT(opline, *op);
			decode = hyssdbg_short_zval_print(literal, 20);
		} break;
	}
	return decode;
} /* }}} */

char *hyssdbg_decode_input_op(
		gear_op_array *ops, const gear_op *opline, znode_op op, gear_uchar op_type,
		uint32_t flags) {
	char *result = NULL;
	if (op_type != IS_UNUSED) {
		result = hyssdbg_decode_op(ops, opline, &op, op_type);
	} else if (GEAR_VM_OP_JMP_ADDR == (flags & GEAR_VM_OP_MASK)) {
		spprintf(&result, 0, "J%td", OP_JMP_ADDR(opline, op) - ops->opcodes);
	} else if (GEAR_VM_OP_NUM == (flags & GEAR_VM_OP_MASK)) {
		spprintf(&result, 0, "%" PRIu32, op.num);
	} else if (GEAR_VM_OP_TRY_CATCH == (flags & GEAR_VM_OP_MASK)) {
		if (op.num != (uint32_t)-1) {
			spprintf(&result, 0, "try-catch(%" PRIu32 ")", op.num);
		}
	} else if (GEAR_VM_OP_THIS == (flags & GEAR_VM_OP_MASK)) {
		result = estrdup("THIS");
	} else if (GEAR_VM_OP_NEXT == (flags & GEAR_VM_OP_MASK)) {
		result = estrdup("NEXT");
	} else if (GEAR_VM_OP_CLASS_FETCH == (flags & GEAR_VM_OP_MASK)) {
		//gear_dump_class_fetch_type(op.num);
	} else if (GEAR_VM_OP_CONSTRUCTOR == (flags & GEAR_VM_OP_MASK)) {
		result = estrdup("CONSTRUCTOR");
	}
	return result;
}

char *hyssdbg_decode_opline(gear_op_array *ops, gear_op *opline) /*{{{ */
{
	const char *opcode_name = hyssdbg_decode_opcode(opline->opcode);
	uint32_t flags = gear_get_opcode_flags(opline->opcode);
	char *result, *decode[4] = {NULL, NULL, NULL, NULL};

	/* OpcodeName */
	if (opline->extended_value) {
		spprintf(&decode[0], 0, "%s<%" PRIi32 ">", opcode_name, opline->extended_value);
	}

	/* OP1 */
	decode[1] = hyssdbg_decode_input_op(
		ops, opline, opline->op1, opline->op1_type, GEAR_VM_OP1_FLAGS(flags));

	/* OP2 */
	decode[2] = hyssdbg_decode_input_op(
		ops, opline, opline->op2, opline->op2_type, GEAR_VM_OP2_FLAGS(flags));

	/* RESULT */
	switch (opline->opcode) {
	case GEAR_CATCH:
		if (opline->extended_value & GEAR_LAST_CATCH) {
			if (decode[2]) {
				efree(decode[2]);
				decode[2] = NULL;
			}
		}
		decode[3] = hyssdbg_decode_op(ops, opline, &opline->result, opline->result_type);
		break;
	default:
		decode[3] = hyssdbg_decode_op(ops, opline, &opline->result, opline->result_type);
		break;
	}

	spprintf(&result, 0,
		"%-23s %-20s %-20s %-20s",
		decode[0] ? decode[0] : opcode_name,
		decode[1] ? decode[1] : "",
		decode[2] ? decode[2] : "",
		decode[3] ? decode[3] : "");

	if (decode[0])
		efree(decode[0]);
	if (decode[1])
		efree(decode[1]);
	if (decode[2])
		efree(decode[2]);
	if (decode[3])
		efree(decode[3]);

	return result;
} /* }}} */

void hyssdbg_print_opline_ex(gear_execute_data *execute_data, gear_bool ignore_flags) /* {{{ */
{
	/* force out a line while stepping so the user knows what is happening */
	if (ignore_flags ||
		(!(HYSSDBG_G(flags) & HYSSDBG_IS_QUIET) ||
		(HYSSDBG_G(flags) & HYSSDBG_IS_STEPPING) ||
		(HYSSDBG_G(oplog)))) {

		gear_op *opline = (gear_op *) execute_data->opline;
		char *decode = hyssdbg_decode_opline(&execute_data->func->op_array, opline);

		if (ignore_flags || (!(HYSSDBG_G(flags) & HYSSDBG_IS_QUIET) || (HYSSDBG_G(flags) & HYSSDBG_IS_STEPPING))) {
			/* output line info */
			hyssdbg_notice("opline", "line=\"%u\" opline=\"%p\" op=\"%s\" file=\"%s\"", "L%-5u %16p %s %s",
			   opline->lineno,
			   opline,
			   decode,
			   execute_data->func->op_array.filename ? ZSTR_VAL(execute_data->func->op_array.filename) : "unknown");
		}

		if (!ignore_flags && HYSSDBG_G(oplog)) {
			hyssdbg_log_ex(fileno(HYSSDBG_G(oplog)), "L%-5u %16p %s %s\n",
				opline->lineno,
				opline,
				decode,
				execute_data->func->op_array.filename ? ZSTR_VAL(execute_data->func->op_array.filename) : "unknown");
		}

		efree(decode);
	}

	if (HYSSDBG_G(oplog_list)) {
		hyssdbg_oplog_entry *cur = gear_arena_alloc(&HYSSDBG_G(oplog_arena), sizeof(hyssdbg_oplog_entry));
		gear_op_array *op_array = &execute_data->func->op_array;
		cur->op = (gear_op *) execute_data->opline;
		cur->opcodes = op_array->opcodes;
		cur->filename = op_array->filename;
		cur->scope = op_array->scope;
		cur->function_name = op_array->function_name;
		cur->next = NULL;
		HYSSDBG_G(oplog_cur)->next = cur;
		HYSSDBG_G(oplog_cur) = cur;
	}
} /* }}} */

void hyssdbg_print_opline(gear_execute_data *execute_data, gear_bool ignore_flags) /* {{{ */
{
	hyssdbg_print_opline_ex(execute_data, ignore_flags);
} /* }}} */
