/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_OUT_H
#define HYSSDBG_OUT_H

/**
 * Error/notice/formatting helpers
 */
enum {
	P_ERROR  = 1,
	P_NOTICE,
	P_WRITELN,
	P_WRITE,
	P_STDOUT,
	P_STDERR,
	P_LOG
};

/* hyssdbg uses lots of custom format specifiers, so we disable format checks by default. */
#if defined(HYSSDBG_CHECK_FORMAT_STRINGS)
# define HYSSDBG_ATTRIBUTE_FORMAT(type, idx, first) HYSS_ATTRIBUTE_FORMAT(type, idx, first)
#else
# define HYSSDBG_ATTRIBUTE_FORMAT(type, idx, first)
#endif

HYSSDBG_API int hyssdbg_print(int severity, int fd, const char *tag, const char *xmlfmt, const char *strfmt, ...) HYSSDBG_ATTRIBUTE_FORMAT(printf, 5, 6);
HYSSDBG_API int hyssdbg_xml_internal(int fd, const char *fmt, ...) HYSSDBG_ATTRIBUTE_FORMAT(printf, 2, 3);
HYSSDBG_API int hyssdbg_log_internal(int fd, const char *fmt, ...) HYSSDBG_ATTRIBUTE_FORMAT(printf, 2, 3);
HYSSDBG_API int hyssdbg_out_internal(int fd, const char *fmt, ...) HYSSDBG_ATTRIBUTE_FORMAT(printf, 2, 3);
HYSSDBG_API int hyssdbg_rlog_internal(int fd, const char *fmt, ...) HYSSDBG_ATTRIBUTE_FORMAT(printf, 2, 3);

#define hyssdbg_error(tag, xmlfmt, strfmt, ...)              hyssdbg_print(P_ERROR  , HYSSDBG_G(io)[HYSSDBG_STDOUT].fd, tag,  xmlfmt, strfmt, ##__VA_ARGS__)
#define hyssdbg_notice(tag, xmlfmt, strfmt, ...)             hyssdbg_print(P_NOTICE , HYSSDBG_G(io)[HYSSDBG_STDOUT].fd, tag,  xmlfmt, strfmt, ##__VA_ARGS__)
#define hyssdbg_writeln(tag, xmlfmt, strfmt, ...)            hyssdbg_print(P_WRITELN, HYSSDBG_G(io)[HYSSDBG_STDOUT].fd, tag,  xmlfmt, strfmt, ##__VA_ARGS__)
#define hyssdbg_write(tag, xmlfmt, strfmt, ...)              hyssdbg_print(P_WRITE  , HYSSDBG_G(io)[HYSSDBG_STDOUT].fd, tag,  xmlfmt, strfmt, ##__VA_ARGS__)
#define hyssdbg_script(type, fmt, ...)                       hyssdbg_print(type     , HYSSDBG_G(io)[HYSSDBG_STDOUT].fd, NULL, NULL,   fmt,    ##__VA_ARGS__)
#define hyssdbg_log(fmt, ...) hyssdbg_log_internal(HYSSDBG_G(io)[HYSSDBG_STDOUT].fd, fmt, ##__VA_ARGS__)
#define hyssdbg_xml(fmt, ...) hyssdbg_xml_internal(HYSSDBG_G(io)[HYSSDBG_STDOUT].fd, fmt, ##__VA_ARGS__)
#define hyssdbg_out(fmt, ...) hyssdbg_out_internal(HYSSDBG_G(io)[HYSSDBG_STDOUT].fd, fmt, ##__VA_ARGS__)

#define hyssdbg_error_ex(out, tag, xmlfmt, strfmt, ...)      hyssdbg_print(P_ERROR  , out, tag,  xmlfmt, strfmt, ##__VA_ARGS__)
#define hyssdbg_notice_ex(out, tag, xmlfmt, strfmt, ...)     hyssdbg_print(P_NOTICE , out, tag,  xmlfmt, strfmt, ##__VA_ARGS__)
#define hyssdbg_writeln_ex(out, tag, xmlfmt, strfmt, ...)    hyssdbg_print(P_WRITELN, out, tag,  xmlfmt, strfmt, ##__VA_ARGS__)
#define hyssdbg_write_ex(out, tag, xmlfmt, strfmt, ...)      hyssdbg_print(P_WRITE  , out, tag,  xmlfmt, strfmt, ##__VA_ARGS__)
#define hyssdbg_script_ex(out, type, fmt, ...)               hyssdbg_print(type     , out, NULL, NULL,   fmt,    ##__VA_ARGS__)
#define hyssdbg_log_ex(out, fmt, ...) hyssdbg_log_internal(out, fmt, ##__VA_ARGS__)
#define hyssdbg_xml_ex(out, fmt, ...) hyssdbg_xml_internal(out, fmt, ##__VA_ARGS__)
#define hyssdbg_out_ex(out, fmt, ...) hyssdbg_out_internal(out, fmt, ##__VA_ARGS__)

#define hyssdbg_rlog(fd, fmt, ...) hyssdbg_rlog_internal(fd, fmt, ##__VA_ARGS__)

#define hyssdbg_xml_asprintf(buf, ...) _hyssdbg_xml_asprintf(buf, ##__VA_ARGS__)
HYSSDBG_API int _hyssdbg_xml_asprintf(char **buf, const char *format, gear_bool escape_xml, ...);

#define hyssdbg_asprintf(buf, ...) _hyssdbg_asprintf(buf, ##__VA_ARGS__)
HYSSDBG_API int _hyssdbg_asprintf(char **buf, const char *format, ...);


#if HYSSDBG_DEBUG
#	define hyssdbg_debug(fmt, ...) hyssdbg_log_ex(HYSSDBG_G(io)[HYSSDBG_STDERR].fd, fmt, ##__VA_ARGS__)
#else
#	define hyssdbg_debug(fmt, ...)
#endif

HYSSDBG_API void hyssdbg_free_err_buf(void);
HYSSDBG_API void hyssdbg_activate_err_buf(gear_bool active);
HYSSDBG_API int hyssdbg_output_err_buf(const char *tag, const char *xmlfmt, const char *strfmt, ...);


/* {{{ For separation */
#define SEPARATE "------------------------------------------------" /* }}} */

#endif /* HYSSDBG_OUT_H */
