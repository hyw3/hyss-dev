/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_SIGIO_WIN32_H
#define HYSSDBG_SIGIO_WIN32_H

#include "hyssdbg.h"
#include "hyssdbg_prompt.h"
#include "hyssdbg_io.h"

struct win32_sigio_watcher_data {
	uint64_t *flags;
	int fd;
};

void
sigio_watcher_start(void);

void
sigio_watcher_stop(void);

#endif /* HYSSDBG_SIGIO_WIN32_H */
