/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_EOL_H
#define HYSSDBG_EOL_H

#include "hyssdbg.h"

struct hyssdbg_eol_rep {
	char *name;
	char *rep;
	int id;
};

enum {
	HYSSDBG_EOL_CRLF, /* DOS */
	/*HYSSDBG_EOL_LFCR,*/ /* for Risc OS? */
	HYSSDBG_EOL_LF, /* UNIX */
	HYSSDBG_EOL_CR /* MAC */
};

int hyssdbg_eol_global_update(char *name);

char *hyssdbg_eol_name(int id);

char *hyssdbg_eol_rep(int id);

void hyssdbg_eol_convert(char **str, int *len);

#endif /* HYSSDBG_EOL_H */
