/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_BTREE_H
#define HYSSDBG_BTREE_H

#include "gear.h"

typedef struct {
	gear_ulong idx;
	void *ptr;
} hyssdbg_btree_result;

typedef union _hyssdbg_btree_branch hyssdbg_btree_branch;
union _hyssdbg_btree_branch {
	hyssdbg_btree_branch *branches[2];
	hyssdbg_btree_result result;
};

typedef struct {
	gear_ulong count;
	gear_ulong depth;
	gear_bool persistent;
	hyssdbg_btree_branch *branch;
} hyssdbg_btree;

typedef struct {
	hyssdbg_btree *tree;
	gear_ulong cur;
	gear_ulong end;
} hyssdbg_btree_position;

void hyssdbg_btree_init(hyssdbg_btree *tree, gear_ulong depth);
void hyssdbg_btree_clean(hyssdbg_btree *tree);
hyssdbg_btree_result *hyssdbg_btree_find(hyssdbg_btree *tree, gear_ulong idx);
hyssdbg_btree_result *hyssdbg_btree_find_closest(hyssdbg_btree *tree, gear_ulong idx);
hyssdbg_btree_position hyssdbg_btree_find_between(hyssdbg_btree *tree, gear_ulong lower_idx, gear_ulong higher_idx);
hyssdbg_btree_result *hyssdbg_btree_next(hyssdbg_btree_position *pos);
int hyssdbg_btree_delete(hyssdbg_btree *tree, gear_ulong idx);

#define HYSSDBG_BTREE_INSERT 1
#define HYSSDBG_BTREE_UPDATE 2
#define HYSSDBG_BTREE_OVERWRITE (HYSSDBG_BTREE_INSERT | HYSSDBG_BTREE_UPDATE)

int hyssdbg_btree_insert_or_update(hyssdbg_btree *tree, gear_ulong idx, void *ptr, int flags);
#define hyssdbg_btree_insert(tree, idx, ptr) hyssdbg_btree_insert_or_update(tree, idx, ptr, HYSSDBG_BTREE_INSERT)
#define hyssdbg_btree_update(tree, idx, ptr) hyssdbg_btree_insert_or_update(tree, idx, ptr, HYSSDBG_BTREE_UPDATE)
#define hyssdbg_btree_overwrite(tree, idx, ptr) hyssdbg_btree_insert_or_update(tree, idx, ptr, HYSSDBG_BTREE_OWERWRITE)


/* debugging functions */
void hyssdbg_btree_branch_dump(hyssdbg_btree_branch *branch, gear_ulong depth);
void hyssdbg_btree_dump(hyssdbg_btree *tree);

#endif
