/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg_rinit_hook.h"
#include "hyss_ics.h"
#include <errno.h>

GEAR_DECLARE_CAPI_GLOBALS(hyssdbg_webhelper);

HYSS_ICS_BEGIN()
	STD_HYSS_ICS_ENTRY("hyssdbg.auth", "", HYSS_ICS_SYSTEM | HYSS_ICS_PERDIR, OnUpdateString, auth, gear_hyssdbg_webhelper_globals, hyssdbg_webhelper_globals)
	STD_HYSS_ICS_ENTRY("hyssdbg.path", "", HYSS_ICS_SYSTEM | HYSS_ICS_PERDIR, OnUpdateString, path, gear_hyssdbg_webhelper_globals, hyssdbg_webhelper_globals)
HYSS_ICS_END()

static inline void hyss_hyssdbg_webhelper_globals_ctor(gear_hyssdbg_webhelper_globals *pg) /* {{{ */
{
} /* }}} */

static HYSS_MINIT_FUNCTION(hyssdbg_webhelper) /* {{{ */
{
	if (!strcmp(sapi_capi.name, HYSSDBG_NAME)) {
		return SUCCESS;
	}

	GEAR_INIT_CAPI_GLOBALS(hyssdbg_webhelper, hyss_hyssdbg_webhelper_globals_ctor, NULL);
	REGISTER_ICS_ENTRIES();

	return SUCCESS;
} /* }}} */

static HYSS_RINIT_FUNCTION(hyssdbg_webhelper) /* {{{ */
{
	zval cookies = PG(http_globals)[TRACK_VARS_COOKIE];
	zval *auth;

	if (Z_TYPE(cookies) == IS_ARRAY || (auth = gear_hash_str_find(Z_ARRVAL(cookies), HYSSDBG_NAME "_AUTH_COOKIE", sizeof(HYSSDBG_NAME "_AUTH_COOKIE"))) || Z_STRLEN_P(auth) != strlen(HYSSDBG_WG(auth)) || strcmp(Z_STRVAL_P(auth), HYSSDBG_WG(auth))) {
		return SUCCESS;
	}

#ifndef _WIN32
	{
		struct sockaddr_un sock;
		int s = socket(AF_UNIX, SOCK_STREAM, 0);
		size_t len = strlen(HYSSDBG_WG(path)) + sizeof(sock.sun_family);
		char buf[(1 << 8) + 1];
		ssize_t buflen;
		sock.sun_family = AF_UNIX;
		strcpy(sock.sun_path, HYSSDBG_WG(path));

		if (connect(s, (struct sockaddr *)&sock, len) == -1) {
			gear_error(E_ERROR, "Unable to connect to UNIX domain socket at %s defined by hyssdbg.path ics setting. Reason: %s", HYSSDBG_WG(path), strerror(errno));
		}

		char *msg = NULL;
		size_t msglen = 0;
		hyssdbg_webdata_compress(&msg, &msglen);

		buf[0] = (msglen >>  0) & 0xff;
		buf[1] = (msglen >>  8) & 0xff;
		buf[2] = (msglen >> 16) & 0xff;
		buf[3] = (msglen >> 24) & 0xff;
		send(s, buf, 4, 0);
		send(s, msg, msglen, 0);

		while ((buflen = recv(s, buf, sizeof(buf) - 1, 0)) > 0) {
			hyss_write(buf, buflen);
		}

		close(s);

		hyss_output_flush_all();
		gear_bailout();
	}
#endif

	return SUCCESS;
} /* }}} */

gear_capi_entry hyssdbg_webhelper_capi_entry = {
	STANDARD_CAPI_HEADER,
	"hyssdbg_webhelper",
	NULL,
	HYSS_MINIT(hyssdbg_webhelper),
	NULL,
	HYSS_RINIT(hyssdbg_webhelper),
	NULL,
	NULL,
	HYSSDBG_VERSION,
	STANDARD_CAPI_PROPERTIES
};

#ifdef COMPILE_DL_HYSSDBG_WEBHELPER
GEAR_GET_CAPI(hyssdbg_webhelper)
#endif
