/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg_btree.h"
#include "hyssdbg.h"

#define CHOOSE_BRANCH(n) \
	branch = branch->branches[!!(n)];

#ifdef _Win32
# undef pemalloc
# undef pefree
# define pemalloc(size, persistent) malloc(size)
# define pefree(ptr, persistent) free(ptr)
#endif

/* depth in bits */
void hyssdbg_btree_init(hyssdbg_btree *tree, gear_ulong depth) {
	tree->depth = depth;
	tree->branch = NULL;
	tree->persistent = 0;
	tree->count = 0;
}

hyssdbg_btree_result *hyssdbg_btree_find(hyssdbg_btree *tree, gear_ulong idx) {
	hyssdbg_btree_branch *branch = tree->branch;
	int i = tree->depth - 1;

	if (branch == NULL) {
		return NULL;
	}

	do {
		if ((idx >> i) % 2 == 1) {
		 	if (branch->branches[1]) {
				CHOOSE_BRANCH(1);
			} else {
				return NULL;
			}
		} else {
			if (branch->branches[0]) {
				CHOOSE_BRANCH(0);
			} else {
				return NULL;
			}
		}
	} while (i--);

	return &branch->result;
}

hyssdbg_btree_result *hyssdbg_btree_find_closest(hyssdbg_btree *tree, gear_ulong idx) {
	hyssdbg_btree_branch *branch = tree->branch;
	int i = tree->depth - 1, last_superior_i = -1;

	if (branch == NULL) {
		return NULL;
	}

	/* find nearest watchpoint */
	do {
		if ((idx >> i) % 2 == 0) {
			if (branch->branches[0]) {
				CHOOSE_BRANCH(0);
			/* an impossible branch was found if: */
			} else {
				/* there's no lower branch than idx */
				if (last_superior_i == -1) {
					/* failure */
					return NULL;
				}
				/* reset state */
				branch = tree->branch;
				i = tree->depth - 1;
				/* follow branch according to bits in idx until the last lower branch before the impossible branch */
				do {
					CHOOSE_BRANCH((idx >> i) % 2 == 1 && branch->branches[1]);
				} while (--i > last_superior_i);
				/* use now the lower branch of which we can be sure that it contains only branches lower than idx */
				CHOOSE_BRANCH(0);
				/* and choose the highest possible branch in the branch containing only branches lower than idx */
				while (i--) {
					CHOOSE_BRANCH(branch->branches[1]);
				}
				break;
			}
		/* follow branch according to bits in idx until having found an impossible branch */
		} else {
			if (branch->branches[1]) {
				if (branch->branches[0]) {
					last_superior_i = i;
				}
				CHOOSE_BRANCH(1);
			} else {
				CHOOSE_BRANCH(0);
				while (i--) {
					CHOOSE_BRANCH(branch->branches[1]);
				}
				break;
			}
		}
	} while (i--);

	return &branch->result;
}

hyssdbg_btree_position hyssdbg_btree_find_between(hyssdbg_btree *tree, gear_ulong lower_idx, gear_ulong higher_idx) {
	hyssdbg_btree_position pos;

	pos.tree = tree;
	pos.end = lower_idx;
	pos.cur = higher_idx;

	return pos;
}

hyssdbg_btree_result *hyssdbg_btree_next(hyssdbg_btree_position *pos) {
	hyssdbg_btree_result *result = hyssdbg_btree_find_closest(pos->tree, pos->cur);

	if (result == NULL || result->idx < pos->end) {
		return NULL;
	}

	pos->cur = result->idx - 1;

	return result;
}

int hyssdbg_btree_insert_or_update(hyssdbg_btree *tree, gear_ulong idx, void *ptr, int flags) {
	int i = tree->depth - 1;
	hyssdbg_btree_branch **branch = &tree->branch;

	do {
		if (*branch == NULL) {
			break;
		}
		branch = &(*branch)->branches[(idx >> i) % 2];
	} while (i--);

	if (*branch == NULL) {
		if (!(flags & HYSSDBG_BTREE_INSERT)) {
			return FAILURE;
		}

		{
			hyssdbg_btree_branch *memory = *branch = pemalloc((i + 2) * sizeof(hyssdbg_btree_branch), tree->persistent);
			do {
				(*branch)->branches[!((idx >> i) % 2)] = NULL;
				branch = &(*branch)->branches[(idx >> i) % 2];
				*branch = ++memory;
			} while (i--);
			tree->count++;
		}
	} else if (!(flags & HYSSDBG_BTREE_UPDATE)) {
		return FAILURE;
	}

	(*branch)->result.idx = idx;
	(*branch)->result.ptr = ptr;

	return SUCCESS;
}

int hyssdbg_btree_delete(hyssdbg_btree *tree, gear_ulong idx) {
	int i = tree->depth;
	hyssdbg_btree_branch *branch = tree->branch;
	int i_last_dual_branch = -1, last_dual_branch_branch;
	hyssdbg_btree_branch *last_dual_branch = NULL;

	goto check_branch_existence;
	do {
		if (branch->branches[0] && branch->branches[1]) {
			last_dual_branch = branch;
			i_last_dual_branch = i;
			last_dual_branch_branch = (idx >> i) % 2;
		}
		branch = branch->branches[(idx >> i) % 2];

check_branch_existence:
		if (branch == NULL) {
			return FAILURE;
		}
	} while (i--);

	tree->count--;

	if (i_last_dual_branch == -1) {
		pefree(tree->branch, tree->persistent);
		tree->branch = NULL;
	} else {
		if (last_dual_branch->branches[last_dual_branch_branch] == last_dual_branch + 1) {
			hyssdbg_btree_branch *original_branch = last_dual_branch->branches[!last_dual_branch_branch];

			memcpy(last_dual_branch + 1, last_dual_branch->branches[!last_dual_branch_branch], (i_last_dual_branch + 1) * sizeof(hyssdbg_btree_branch));
			pefree(last_dual_branch->branches[!last_dual_branch_branch], tree->persistent);
			last_dual_branch->branches[!last_dual_branch_branch] = last_dual_branch + 1;

			branch = last_dual_branch->branches[!last_dual_branch_branch];
			for (i = i_last_dual_branch; i--;) {
				branch = (branch->branches[branch->branches[1] == ++original_branch] = last_dual_branch + i_last_dual_branch - i + 1);
			}
		} else {
			pefree(last_dual_branch->branches[last_dual_branch_branch], tree->persistent);
		}

		last_dual_branch->branches[last_dual_branch_branch] = NULL;
	}

	return SUCCESS;
}

void hyssdbg_btree_clean_recursive(hyssdbg_btree_branch *branch, gear_ulong depth, gear_bool persistent) {
	hyssdbg_btree_branch *start = branch;
	while (depth--) {
		gear_bool use_branch = branch + 1 == branch->branches[0];
		if (branch->branches[use_branch]) {
			hyssdbg_btree_clean_recursive(branch->branches[use_branch], depth, persistent);
		}
	}

	pefree(start, persistent);
}

void hyssdbg_btree_clean(hyssdbg_btree *tree) {
	if (tree->branch) {
		hyssdbg_btree_clean_recursive(tree->branch, tree->depth, tree->persistent);
		tree->branch = NULL;
		tree->count = 0;
	}
}

void hyssdbg_btree_branch_dump(hyssdbg_btree_branch *branch, gear_ulong depth) {
	if (branch) {
		if (depth--) {
			hyssdbg_btree_branch_dump(branch->branches[0], depth);
			hyssdbg_btree_branch_dump(branch->branches[1], depth);
		} else {
			fprintf(stderr, "%p: %p\n", (void *) branch->result.idx, branch->result.ptr);
		}
	}
}

void hyssdbg_btree_dump(hyssdbg_btree *tree) {
	hyssdbg_btree_branch_dump(tree->branch, tree->depth);
}
