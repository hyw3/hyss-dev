/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"

#include "hyss.h"
#include "hyssdbg.h"
#include "hyssdbg_opcode.h"
#include "hyssdbg_utils.h"
#include "extslib/standard/hyss_string.h"

/* FASYNC under Solaris */
#ifdef HAVE_SYS_FILE_H
# include <sys/file.h>
#endif

#ifdef HAVE_SYS_IOCTL_H
# include "sys/ioctl.h"
# ifndef GWINSZ_IN_SYS_IOCTL
#  include <termios.h>
# endif
#endif

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

/* {{{ color structures */
const static hyssdbg_color_t colors[] = {
	HYSSDBG_COLOR_D("none",             "0;0"),

	HYSSDBG_COLOR_D("white",            "0;64"),
	HYSSDBG_COLOR_D("white-bold",       "1;64"),
	HYSSDBG_COLOR_D("white-underline",  "4;64"),
	HYSSDBG_COLOR_D("red",              "0;31"),
	HYSSDBG_COLOR_D("red-bold",         "1;31"),
	HYSSDBG_COLOR_D("red-underline",    "4;31"),
	HYSSDBG_COLOR_D("green",            "0;32"),
	HYSSDBG_COLOR_D("green-bold",       "1;32"),
	HYSSDBG_COLOR_D("green-underline",  "4;32"),
	HYSSDBG_COLOR_D("yellow",           "0;33"),
	HYSSDBG_COLOR_D("yellow-bold",      "1;33"),
	HYSSDBG_COLOR_D("yellow-underline", "4;33"),
	HYSSDBG_COLOR_D("blue",             "0;34"),
	HYSSDBG_COLOR_D("blue-bold",        "1;34"),
	HYSSDBG_COLOR_D("blue-underline",   "4;34"),
	HYSSDBG_COLOR_D("purple",           "0;35"),
	HYSSDBG_COLOR_D("purple-bold",      "1;35"),
	HYSSDBG_COLOR_D("purple-underline", "4;35"),
	HYSSDBG_COLOR_D("cyan",             "0;36"),
	HYSSDBG_COLOR_D("cyan-bold",        "1;36"),
	HYSSDBG_COLOR_D("cyan-underline",   "4;36"),
	HYSSDBG_COLOR_D("black",            "0;30"),
	HYSSDBG_COLOR_D("black-bold",       "1;30"),
	HYSSDBG_COLOR_D("black-underline",  "4;30"),
	HYSSDBG_COLOR_END
}; /* }}} */

/* {{{ */
const static hyssdbg_element_t elements[] = {
	HYSSDBG_ELEMENT_D("prompt", HYSSDBG_COLOR_PROMPT),
	HYSSDBG_ELEMENT_D("error", HYSSDBG_COLOR_ERROR),
	HYSSDBG_ELEMENT_D("notice", HYSSDBG_COLOR_NOTICE),
	HYSSDBG_ELEMENT_END
}; /* }}} */

HYSSDBG_API int hyssdbg_is_numeric(const char *str) /* {{{ */
{
	if (!str)
		return 0;

	for (; *str; str++) {
		if (isspace(*str) || *str == '-') {
			continue;
		}
		return isdigit(*str);
	}
	return 0;
} /* }}} */

HYSSDBG_API int hyssdbg_is_empty(const char *str) /* {{{ */
{
	if (!str)
		return 1;

	for (; *str; str++) {
		if (isspace(*str)) {
			continue;
		}
		return 0;
	}
	return 1;
} /* }}} */

HYSSDBG_API int hyssdbg_is_addr(const char *str) /* {{{ */
{
	return str[0] && str[1] && memcmp(str, "0x", 2) == 0;
} /* }}} */

HYSSDBG_API int hyssdbg_is_class_method(const char *str, size_t len, char **class, char **method) /* {{{ */
{
	char *sep = NULL;

	if (strstr(str, "#") != NULL)
		return 0;

	if (strstr(str, " ") != NULL)
		return 0;

	sep = strstr(str, "::");

	if (!sep || sep == str || sep+2 == str+len-1) {
		return 0;
	}

	if (class != NULL) {

		if (str[0] == '\\') {
			str++;
			len--;
		}

		*class = estrndup(str, sep - str);
		(*class)[sep - str] = 0;
	}

	if (method != NULL) {
		*method = estrndup(sep+2, str + len - (sep + 2));
	}

	return 1;
} /* }}} */

HYSSDBG_API char *hyssdbg_resolve_path(const char *path) /* {{{ */
{
	char resolved_name[MAXPATHLEN];

	if (expand_filepath(path, resolved_name) == NULL) {
		return NULL;
	}

	return estrdup(resolved_name);
} /* }}} */

HYSSDBG_API const char *hyssdbg_current_file(void) /* {{{ */
{
	const char *file = gear_get_executed_filename();

	if (memcmp(file, "[no active file]", sizeof("[no active file]")) == 0) {
		return HYSSDBG_G(exec);
	}

	return file;
} /* }}} */

HYSSDBG_API const gear_function *hyssdbg_get_function(const char *fname, const char *cname) /* {{{ */
{
	gear_function *func = NULL;
	gear_string *lfname = gear_string_init(fname, strlen(fname), 0);
	gear_string *tmp = gear_string_tolower(lfname);
	gear_string_release(lfname);
	lfname = tmp;

	if (cname) {
		gear_class_entry *ce;
		gear_string *lcname = gear_string_init(cname, strlen(cname), 0);
		tmp = gear_string_tolower(lcname);
		gear_string_release(lcname);
		lcname = tmp;
		ce = gear_lookup_class(lcname);

		gear_string_release(lcname);

		if (ce) {
			func = gear_hash_find_ptr(&ce->function_table, lfname);
		}
	} else {
		func = gear_hash_find_ptr(EG(function_table), lfname);
	}

	gear_string_release(lfname);
	return func;
} /* }}} */

HYSSDBG_API char *hyssdbg_trim(const char *str, size_t len, size_t *new_len) /* {{{ */
{
	const char *p = str;
	char *new = NULL;

	while (p && isspace(*p)) {
		++p;
		--len;
	}

	while (*p && isspace(*(p + len -1))) {
		--len;
	}

	if (len == 0) {
		new = estrndup("", sizeof(""));
		*new_len = 0;
	} else {
		new = estrndup(p, len);
		*(new + len) = '\0';

		if (new_len) {
			*new_len = len;
		}
	}

	return new;

} /* }}} */

HYSSDBG_API const hyssdbg_color_t *hyssdbg_get_color(const char *name, size_t name_length) /* {{{ */
{
	const hyssdbg_color_t *color = colors;

	while (color && color->name) {
		if (name_length == color->name_length &&
			memcmp(name, color->name, name_length) == SUCCESS) {
			hyssdbg_debug("hyssdbg_get_color(%s, %lu): %s", name, name_length, color->code);
			return color;
		}
		++color;
	}

	hyssdbg_debug("hyssdbg_get_color(%s, %lu): failed", name, name_length);

	return NULL;
} /* }}} */

HYSSDBG_API void hyssdbg_set_color(int element, const hyssdbg_color_t *color) /* {{{ */
{
	HYSSDBG_G(colors)[element] = color;
} /* }}} */

HYSSDBG_API void hyssdbg_set_color_ex(int element, const char *name, size_t name_length) /* {{{ */
{
	const hyssdbg_color_t *color = hyssdbg_get_color(name, name_length);

	if (color) {
		hyssdbg_set_color(element, color);
	} else HYSSDBG_G(colors)[element] = colors;
} /* }}} */

HYSSDBG_API const hyssdbg_color_t* hyssdbg_get_colors(void) /* {{{ */
{
	return colors;
} /* }}} */

HYSSDBG_API int hyssdbg_get_element(const char *name, size_t len) {
	const hyssdbg_element_t *element = elements;

	while (element && element->name) {
		if (len == element->name_length) {
			if (strncasecmp(name, element->name, len) == SUCCESS) {
				return element->id;
			}
		}
		element++;
	}

	return HYSSDBG_COLOR_INVALID;
}

HYSSDBG_API void hyssdbg_set_prompt(const char *prompt) /* {{{ */
{
	/* free formatted prompt */
	if (HYSSDBG_G(prompt)[1]) {
		free(HYSSDBG_G(prompt)[1]);
		HYSSDBG_G(prompt)[1] = NULL;
	}
	/* free old prompt */
	if (HYSSDBG_G(prompt)[0]) {
		free(HYSSDBG_G(prompt)[0]);
		HYSSDBG_G(prompt)[0] = NULL;
	}

	/* copy new prompt */
	HYSSDBG_G(prompt)[0] = strdup(prompt);
} /* }}} */

HYSSDBG_API const char *hyssdbg_get_prompt(void) /* {{{ */
{
	/* find cached prompt */
	if (HYSSDBG_G(prompt)[1]) {
		return HYSSDBG_G(prompt)[1];
	}

	/* create cached prompt */
#ifndef HAVE_LIBEDIT
	/* TODO: libedit doesn't seems to support coloured prompt */
	if ((HYSSDBG_G(flags) & HYSSDBG_IS_COLOURED)) {
		GEAR_IGNORE_VALUE(asprintf(&HYSSDBG_G(prompt)[1], "\033[%sm%s\033[0m ",
			HYSSDBG_G(colors)[HYSSDBG_COLOR_PROMPT]->code,
			HYSSDBG_G(prompt)[0]));
	} else
#endif
	{
		GEAR_IGNORE_VALUE(asprintf(&HYSSDBG_G(prompt)[1], "%s ", HYSSDBG_G(prompt)[0]));
	}

	return HYSSDBG_G(prompt)[1];
} /* }}} */

int hyssdbg_rebuild_symtable(void) {
	if (!EG(current_execute_data) || !EG(current_execute_data)->func) {
		hyssdbg_error("inactive", "type=\"op_array\"", "No active op array!");
		return FAILURE;
	}

	if (!gear_rebuild_symbol_table()) {
		hyssdbg_error("inactive", "type=\"symbol_table\"", "No active symbol table!");
		return FAILURE;
	}

	return SUCCESS;
}

HYSSDBG_API int hyssdbg_get_terminal_width(void) /* {{{ */
{
	int columns;
#ifdef _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
#elif defined(HAVE_SYS_IOCTL_H) && defined(TIOCGWINSZ)
	struct winsize w;

	columns = ioctl(fileno(stdout), TIOCGWINSZ, &w) == 0 ? w.ws_col : 80;
#else
	columns = 80;
#endif
	return columns;
} /* }}} */

HYSSDBG_API int hyssdbg_get_terminal_height(void) /* {{{ */
{
	int lines;
#ifdef _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	lines = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
#elif defined(HAVE_SYS_IOCTL_H) && defined(TIOCGWINSZ)
	struct winsize w;

	lines = ioctl(fileno(stdout), TIOCGWINSZ, &w) == 0 ? w.ws_row : 40;
#else
	lines = 40;
#endif
	return lines;
} /* }}} */

HYSSDBG_API void hyssdbg_set_async_io(int fd) {
#if !defined(_WIN32) && defined(FASYNC)
	int flags;
	fcntl(STDIN_FILENO, F_SETOWN, getpid());
	flags = fcntl(STDIN_FILENO, F_GETFL);
	fcntl(STDIN_FILENO, F_SETFL, flags | FASYNC);
#endif
}

int hyssdbg_safe_class_lookup(const char *name, int name_length, gear_class_entry **ce) {
	if (HYSSDBG_G(flags) & HYSSDBG_IN_SIGNAL_HANDLER) {
		char *lc_name, *lc_free;
		int lc_length;

		if (name == NULL || !name_length) {
			return FAILURE;
		}

		lc_free = lc_name = emalloc(name_length + 1);
		gear_str_tolower_copy(lc_name, name, name_length);
		lc_length = name_length + 1;

		if (lc_name[0] == '\\') {
			lc_name += 1;
			lc_length -= 1;
		}

		hyssdbg_try_access {
			*ce = gear_hash_str_find_ptr(EG(class_table), lc_name, lc_length);
		} hyssdbg_catch_access {
			hyssdbg_error("signalsegv", "class=\"%.*s\"", "Could not fetch class %.*s, invalid data source", name_length, name);
		} hyssdbg_end_try_access();

		efree(lc_free);
	} else {
		gear_string *str_name = gear_string_init(name, name_length, 0);
		*ce = gear_lookup_class(str_name);
		efree(str_name);
	}

	return *ce ? SUCCESS : FAILURE;
}

char *hyssdbg_get_property_key(char *key) {
	if (*key != 0) {
		return key;
	}
	return strchr(key + 1, 0) + 1;
}

static int hyssdbg_parse_variable_arg_wrapper(char *name, size_t len, char *keyname, size_t keylen, HashTable *parent, zval *zv, hyssdbg_parse_var_func callback) {
	return callback(name, len, keyname, keylen, parent, zv);
}

HYSSDBG_API int hyssdbg_parse_variable(char *input, size_t len, HashTable *parent, size_t i, hyssdbg_parse_var_func callback, gear_bool silent) {
	return hyssdbg_parse_variable_with_arg(input, len, parent, i, (hyssdbg_parse_var_with_arg_func) hyssdbg_parse_variable_arg_wrapper, NULL, silent, callback);
}

HYSSDBG_API int hyssdbg_parse_variable_with_arg(char *input, size_t len, HashTable *parent, size_t i, hyssdbg_parse_var_with_arg_func callback, hyssdbg_parse_var_with_arg_func step_cb, gear_bool silent, void *arg) {
	int ret = FAILURE;
	gear_bool new_index = 1;
	char *last_index;
	size_t index_len = 0;
	zval *zv;

	if (len < 2 || *input != '$') {
		goto error;
	}

	while (i++ < len) {
		if (i == len) {
			new_index = 1;
		} else {
			switch (input[i]) {
				case '[':
					new_index = 1;
					break;
				case ']':
					break;
				case '>':
					if (last_index[index_len - 1] == '-') {
						new_index = 1;
						index_len--;
					}
					break;

				default:
					if (new_index) {
						last_index = input + i;
						new_index = 0;
					}
					if (input[i - 1] == ']') {
						goto error;
					}
					index_len++;
			}
		}

		if (new_index && index_len == 0) {
			gear_ulong numkey;
			gear_string *strkey;
			GEAR_HASH_FOREACH_KEY_PTR(parent, numkey, strkey, zv) {
				while (Z_TYPE_P(zv) == IS_INDIRECT) {
					zv = Z_INDIRECT_P(zv);
				}

				if (i == len || (i == len - 1 && input[len - 1] == ']')) {
					char *key, *propkey;
					size_t namelen, keylen;
					char *name;
					char *keyname = estrndup(last_index, index_len);
					if (strkey) {
						key = ZSTR_VAL(strkey);
						keylen = ZSTR_LEN(strkey);
					} else {
						keylen = spprintf(&key, 0, GEAR_ULONG_FMT, numkey);
					}
					propkey = hyssdbg_get_property_key(key);
					name = emalloc(i + keylen + 2);
					namelen = sprintf(name, "%.*s%.*s%s", (int) i, input, (int) (keylen - (propkey - key)), propkey, input[len - 1] == ']'?"]":"");
					if (!strkey) {
						efree(key);
					}

					ret = callback(name, namelen, keyname, index_len, parent, zv, arg) == SUCCESS || ret == SUCCESS?SUCCESS:FAILURE;
				} else retry_ref: if (Z_TYPE_P(zv) == IS_OBJECT) {
					if (step_cb) {
						char *name = estrndup(input, i);
						char *keyname = estrndup(last_index, index_len);

						ret = step_cb(name, i, keyname, index_len, parent, zv, arg) == SUCCESS || ret == SUCCESS?SUCCESS:FAILURE;
					}

					hyssdbg_parse_variable_with_arg(input, len, Z_OBJPROP_P(zv), i, callback, step_cb, silent, arg);
				} else if (Z_TYPE_P(zv) == IS_ARRAY) {
					if (step_cb) {
						char *name = estrndup(input, i);
						char *keyname = estrndup(last_index, index_len);

						ret = step_cb(name, i, keyname, index_len, parent, zv, arg) == SUCCESS || ret == SUCCESS?SUCCESS:FAILURE;
					}

					hyssdbg_parse_variable_with_arg(input, len, Z_ARRVAL_P(zv), i, callback, step_cb, silent, arg);
				} else if (Z_ISREF_P(zv)) {
					if (step_cb) {
						char *name = estrndup(input, i);
						char *keyname = estrndup(last_index, index_len);

						ret = step_cb(name, i, keyname, index_len, parent, zv, arg) == SUCCESS || ret == SUCCESS?SUCCESS:FAILURE;
					}

					ZVAL_DEREF(zv);
					goto retry_ref;
				} else {
					/* Ignore silently */
				}
			} GEAR_HASH_FOREACH_END();
			return ret;
		} else if (new_index) {
			char last_chr = last_index[index_len];
			last_index[index_len] = 0;
			if (!(zv = gear_symtable_str_find(parent, last_index, index_len))) {
				if (!silent) {
					hyssdbg_error("variable", "type=\"undefined\" variable=\"%.*s\"", "%.*s is undefined", (int) input[i] == ']' ? i + 1 : i, input);
				}
				return FAILURE;
			}
			while (Z_TYPE_P(zv) == IS_INDIRECT) {
				zv = Z_INDIRECT_P(zv);
			}

			last_index[index_len] = last_chr;
			if (i == len) {
				char *name = estrndup(input, i);
				char *keyname = estrndup(last_index, index_len);

				ret = callback(name, i, keyname, index_len, parent, zv, arg) == SUCCESS || ret == SUCCESS?SUCCESS:FAILURE;
			} else retry_ref_end: if (Z_TYPE_P(zv) == IS_OBJECT) {
				if (step_cb) {
					char *name = estrndup(input, i);
					char *keyname = estrndup(last_index, index_len);

					ret = step_cb(name, i, keyname, index_len, parent, zv, arg) == SUCCESS || ret == SUCCESS?SUCCESS:FAILURE;
				}

				parent = Z_OBJPROP_P(zv);
			} else if (Z_TYPE_P(zv) == IS_ARRAY) {
				if (step_cb) {
					char *name = estrndup(input, i);
					char *keyname = estrndup(last_index, index_len);

					ret = step_cb(name, i, keyname, index_len, parent, zv, arg) == SUCCESS || ret == SUCCESS?SUCCESS:FAILURE;
				}

				parent = Z_ARRVAL_P(zv);
			} else if (Z_ISREF_P(zv)) {
				if (step_cb) {
					char *name = estrndup(input, i);
					char *keyname = estrndup(last_index, index_len);

					ret = step_cb(name, i, keyname, index_len, parent, zv, arg) == SUCCESS || ret == SUCCESS?SUCCESS:FAILURE;
				}

				ZVAL_DEREF(zv);
				goto retry_ref_end;
			} else {
				hyssdbg_error("variable", "type=\"notiterable\" variable=\"%.*s\"", "%.*s is nor an array nor an object", (int) (input[i] == '>' ? i - 1 : i), input);
				return FAILURE;
			}
			index_len = 0;
		}
	}

	return ret;
	error:
		hyssdbg_error("variable", "type=\"invalidinput\"", "Malformed input");
		return FAILURE;
}

int hyssdbg_is_auto_global(char *name, int len) {
	return gear_is_auto_global_str(name, len);
}

static int hyssdbg_xml_array_element_dump(zval *zv, gear_string *key, gear_ulong num) {
	hyssdbg_xml("<element");

	hyssdbg_try_access {
		if (key) { /* string key */
			hyssdbg_xml(" name=\"%.*s\"", (int) ZSTR_LEN(key), ZSTR_VAL(key));
		} else { /* numeric key */
			hyssdbg_xml(" name=\"%ld\"", num);
		}
	} hyssdbg_catch_access {
		hyssdbg_xml(" severity=\"error\" ></element>");
		return 0;
	} hyssdbg_end_try_access();

	hyssdbg_xml(">");

	hyssdbg_xml_var_dump(zv);

	hyssdbg_xml("</element>");

	return 0;
}

static int hyssdbg_xml_object_property_dump(zval *zv, gear_string *key, gear_ulong num) {
	hyssdbg_xml("<property");

	hyssdbg_try_access {
		if (key) { /* string key */
			const char *prop_name, *class_name;
			int unmangle = gear_unmangle_property_name(key, &class_name, &prop_name);

			if (class_name && unmangle == SUCCESS) {
				hyssdbg_xml(" name=\"%s\"", prop_name);
				if (class_name[0] == '*') {
					hyssdbg_xml(" protection=\"protected\"");
				} else {
					hyssdbg_xml(" class=\"%s\" protection=\"private\"", class_name);
				}
			} else {
				hyssdbg_xml(" name=\"%.*s\" protection=\"public\"", (int) ZSTR_LEN(key), ZSTR_VAL(key));
			}
		} else { /* numeric key */
			hyssdbg_xml(" name=\"%ld\" protection=\"public\"", num);
		}
	} hyssdbg_catch_access {
		hyssdbg_xml(" severity=\"error\" ></property>");
		return 0;
	} hyssdbg_end_try_access();

	hyssdbg_xml(">");

	hyssdbg_xml_var_dump(zv);

	hyssdbg_xml("</property>");

	return 0;
}

#define COMMON (is_ref ? "&" : "")

HYSSDBG_API void hyssdbg_xml_var_dump(zval *zv) {
	HashTable *myht;
	gear_string *class_name, *key;
	gear_ulong num;
	zval *val;
	int (*element_dump_func)(zval *zv, gear_string *key, gear_ulong num);
	gear_bool is_ref = 0;

	int is_temp;

	hyssdbg_try_access {
		is_ref = Z_ISREF_P(zv) && GC_REFCOUNT(Z_COUNTED_P(zv)) > 1;
		ZVAL_DEREF(zv);

		switch (Z_TYPE_P(zv)) {
			case IS_TRUE:
				hyssdbg_xml("<bool refstatus=\"%s\" value=\"true\" />", COMMON);
				break;
			case IS_FALSE:
				hyssdbg_xml("<bool refstatus=\"%s\" value=\"false\" />", COMMON);
				break;
			case IS_NULL:
				hyssdbg_xml("<null refstatus=\"%s\" />", COMMON);
				break;
			case IS_LONG:
				hyssdbg_xml("<int refstatus=\"%s\" value=\"" GEAR_LONG_FMT "\" />", COMMON, Z_LVAL_P(zv));
				break;
			case IS_DOUBLE:
				hyssdbg_xml("<float refstatus=\"%s\" value=\"%.*G\" />", COMMON, (int) EG(precision), Z_DVAL_P(zv));
				break;
			case IS_STRING:
				hyssdbg_xml("<string refstatus=\"%s\" length=\"%zd\" value=\"%.*s\" />", COMMON, Z_STRLEN_P(zv), (int) Z_STRLEN_P(zv), Z_STRVAL_P(zv));
				break;
			case IS_ARRAY:
				myht = Z_ARRVAL_P(zv);
				if (!(GC_FLAGS(myht) & GC_IMMUTABLE)) {
					if (GC_IS_RECURSIVE(myht)) {
						hyssdbg_xml("<recursion />");
						break;
					}
					GC_PROTECT_RECURSION(myht);
				}
				hyssdbg_xml("<array refstatus=\"%s\" num=\"%d\">", COMMON, gear_hash_num_elements(myht));
				element_dump_func = hyssdbg_xml_array_element_dump;
				is_temp = 0;
				goto head_done;
			case IS_OBJECT:
				myht = Z_OBJDEBUG_P(zv, is_temp);
				if (myht && GC_IS_RECURSIVE(myht)) {
					hyssdbg_xml("<recursion />");
					break;
				}

				class_name = Z_OBJ_HANDLER_P(zv, get_class_name)(Z_OBJ_P(zv));
				hyssdbg_xml("<object refstatus=\"%s\" class=\"%.*s\" id=\"%d\" num=\"%d\">", COMMON, (int) ZSTR_LEN(class_name), ZSTR_VAL(class_name), Z_OBJ_HANDLE_P(zv), myht ? gear_hash_num_elements(myht) : 0);
				gear_string_release(class_name);

				element_dump_func = hyssdbg_xml_object_property_dump;
head_done:
				if (myht) {
					GEAR_HASH_FOREACH_KEY_VAL_IND(myht, num, key, val) {
						element_dump_func(val, key, num);
					} GEAR_HASH_FOREACH_END();
					gear_hash_apply_with_arguments(myht, (apply_func_args_t) element_dump_func, 0);
					GC_UNPROTECT_RECURSION(myht);
					if (is_temp) {
						gear_hash_destroy(myht);
						efree(myht);
					}
				}
				if (Z_TYPE_P(zv) == IS_ARRAY) {
					hyssdbg_xml("</array>");
				} else {
					hyssdbg_xml("</object>");
				}
				break;
			case IS_RESOURCE: {
				const char *type_name = gear_rsrc_list_get_rsrc_type(Z_RES_P(zv));
				hyssdbg_xml("<resource refstatus=\"%s\" id=\"%pd\" type=\"%s\" />", COMMON, Z_RES_P(zv)->handle, type_name ? type_name : "unknown");
				break;
			}
			default:
				break;
		}
	} hyssdbg_end_try_access();
}

HYSSDBG_API gear_bool hyssdbg_check_caught_ex(gear_execute_data *execute_data, gear_object *exception) {
	const gear_op *op;
	gear_op *cur;
	uint32_t op_num, i;
	gear_op_array *op_array = &execute_data->func->op_array;

	if (execute_data->opline >= EG(exception_op) && execute_data->opline < EG(exception_op) + 3) {
		op = EG(opline_before_exception);
	} else {
		op = execute_data->opline;
	}

	op_num = op - op_array->opcodes;

	for (i = 0; i < op_array->last_try_catch && op_array->try_catch_array[i].try_op <= op_num; i++) {
		uint32_t catch = op_array->try_catch_array[i].catch_op, finally = op_array->try_catch_array[i].finally_op;
		if (op_num <= catch || op_num <= finally) {
			if (finally) {
				return 1;
			}

			cur = &op_array->opcodes[catch];
			while (1) {
				gear_class_entry *ce;

				if (!(ce = CACHED_PTR(cur->extended_value & ~GEAR_LAST_CATCH))) {
					ce = gear_fetch_class_by_name(Z_STR_P(RT_CONSTANT(cur, cur->op1)), RT_CONSTANT(cur, cur->op1) + 1, GEAR_FETCH_CLASS_NO_AUTOLOAD);
					CACHE_PTR(cur->extended_value & ~GEAR_LAST_CATCH, ce);
				}

				if (ce == exception->ce || (ce && instanceof_function(exception->ce, ce))) {
					return 1;
				}

				if (cur->extended_value & GEAR_LAST_CATCH) {
					return 0;
				}

				cur = OP_JMP_ADDR(cur, cur->op2);
			}

			return 0;
		}
	}

	return op->opcode == GEAR_CATCH;
}

char *hyssdbg_short_zval_print(zval *zv, int maxlen) /* {{{ */
{
	char *decode = NULL;

	switch (Z_TYPE_P(zv)) {
		case IS_UNDEF:
			decode = estrdup("");
			break;
		case IS_NULL:
			decode = estrdup("null");
			break;
		case IS_FALSE:
			decode = estrdup("false");
			break;
		case IS_TRUE:
			decode = estrdup("true");
			break;
		case IS_LONG:
			spprintf(&decode, 0, GEAR_LONG_FMT, Z_LVAL_P(zv));
			break;
		case IS_DOUBLE:
			spprintf(&decode, 0, "%.*G", 14, Z_DVAL_P(zv));

			/* Make sure it looks like a float */
			if (gear_finite(Z_DVAL_P(zv)) && !strchr(decode, '.')) {
				size_t len = strlen(decode);
				char *decode2 = emalloc(len + strlen(".0") + 1);
				memcpy(decode2, decode, len);
				decode2[len] = '.';
				decode2[len+1] = '0';
				decode2[len+2] = '\0';
				efree(decode);
				decode = decode2;
			}
			break;
		case IS_STRING: {
			int i;
			gear_string *str = hyss_addcslashes(Z_STR_P(zv), "\\\"\n\t\0", 5);
			for (i = 0; i < ZSTR_LEN(str); i++) {
				if (ZSTR_VAL(str)[i] < 32) {
					ZSTR_VAL(str)[i] = ' ';
				}
			}
			spprintf(&decode, 0, "\"%.*s\"%c",
				ZSTR_LEN(str) <= maxlen - 2 ? (int) ZSTR_LEN(str) : (maxlen - 3),
				ZSTR_VAL(str), ZSTR_LEN(str) <= maxlen - 2 ? 0 : '+');
			gear_string_release(str);
			} break;
		case IS_RESOURCE:
			spprintf(&decode, 0, "Rsrc #%d", Z_RES_HANDLE_P(zv));
			break;
		case IS_ARRAY:
			spprintf(&decode, 0, "array(%d)", gear_hash_num_elements(Z_ARR_P(zv)));
			break;
		case IS_OBJECT: {
			gear_string *str = Z_OBJCE_P(zv)->name;
			spprintf(&decode, 0, "%.*s%c",
				ZSTR_LEN(str) <= maxlen ? (int) ZSTR_LEN(str) : maxlen - 1,
				ZSTR_VAL(str), ZSTR_LEN(str) <= maxlen ? 0 : '+');
			break;
		}
		case IS_CONSTANT_AST: {
			gear_ast *ast = Z_ASTVAL_P(zv);

			if (ast->kind == GEAR_AST_CONSTANT
			 || ast->kind == GEAR_AST_CONSTANT_CLASS) {
				decode = estrdup("<constant>");
			} else {
				decode = estrdup("<ast>");
			}
			break;
		}
		default:
			spprintf(&decode, 0, "unknown type: %d", Z_TYPE_P(zv));
			break;
	}

	return decode;
} /* }}} */
