/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg.h"
#include "hyssdbg_print.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_opcode.h"
#include "hyssdbg_prompt.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

#define HYSSDBG_PRINT_COMMAND_D(f, h, a, m, l, s, flags) \
	HYSSDBG_COMMAND_D_EXP(f, h, a, m, l, s, &hyssdbg_prompt_commands[8], flags)

const hyssdbg_command_t hyssdbg_print_commands[] = {
	HYSSDBG_PRINT_COMMAND_D(exec,       "print out the instructions in the main execution context", 'e', print_exec,   NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_PRINT_COMMAND_D(opline,     "print out the instruction in the current opline",          'o', print_opline, NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_PRINT_COMMAND_D(class,      "print out the instructions in the specified class",        'c', print_class,  NULL, "s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_PRINT_COMMAND_D(method,     "print out the instructions in the specified method",       'm', print_method, NULL, "m", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_PRINT_COMMAND_D(func,       "print out the instructions in the specified function",     'f', print_func,   NULL, "s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_PRINT_COMMAND_D(stack,      "print out the instructions in the current stack",          's', print_stack,  NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_END_COMMAND
};

HYSSDBG_PRINT(opline) /* {{{ */
{
	if (HYSSDBG_G(in_execution) && EG(current_execute_data)) {
		hyssdbg_print_opline(hyssdbg_user_execute_data(EG(current_execute_data)), 1);
	} else {
		hyssdbg_error("inactive", "type=\"execution\"", "Not Executing!");
	}

	return SUCCESS;
} /* }}} */

static inline void hyssdbg_print_function_helper(gear_function *method) /* {{{ */
{
	switch (method->type) {
		case GEAR_USER_FUNCTION: {
			gear_op_array* op_array = &(method->op_array);

			if (op_array) {
				gear_op *opline = &(op_array->opcodes[0]);
				uint32_t opcode = 0,
				end = op_array->last-1;

				if (method->common.scope) {
					hyssdbg_writeln("printoplineinfo", "type=\"User\" startline=\"%d\" endline=\"%d\" method=\"%s::%s\" file=\"%s\" opline=\"%p\"", "L%d-%d %s::%s() %s - %p + %d ops",
						op_array->line_start,
						op_array->line_end,
						ZSTR_VAL(method->common.scope->name),
						ZSTR_VAL(method->common.function_name),
						op_array->filename ? ZSTR_VAL(op_array->filename) : "unknown",
						opline,
						op_array->last);
				} else {
					hyssdbg_writeln("printoplineinfo", "type=\"User\" startline=\"%d\" endline=\"%d\" function=\"%s\" file=\"%s\" opline=\"%p\"", "L%d-%d %s() %s - %p + %d ops",
						op_array->line_start,
						op_array->line_end,
						method->common.function_name ? ZSTR_VAL(method->common.function_name) : "{main}",
						op_array->filename ? ZSTR_VAL(op_array->filename) : "unknown",
						opline,
						op_array->last);
				}

				do {
					char *decode = hyssdbg_decode_opline(op_array, opline);
					hyssdbg_writeln("print", "line=\"%u\" opnum=\"%u\" op=\"%s\"", " L%-4u #%-5u %s",
						opline->lineno,
						opcode,
						decode);
					efree(decode);
					opline++;
				} while (opcode++ < end);
			}
		} break;

		default: {
			if (method->common.scope) {
				hyssdbg_writeln("printoplineinfo", "type=\"Internal\" method=\"%s::%s\"", "\tInternal %s::%s()", ZSTR_VAL(method->common.scope->name), ZSTR_VAL(method->common.function_name));
			} else {
				hyssdbg_writeln("printoplineinfo", "type=\"Internal\" function=\"%s\"", "\tInternal %s()", ZSTR_VAL(method->common.function_name));
			}
		}
	}
} /* }}} */

HYSSDBG_PRINT(exec) /* {{{ */
{
	if (HYSSDBG_G(exec)) {
		if (!HYSSDBG_G(ops) && !(HYSSDBG_G(flags) & HYSSDBG_IN_SIGNAL_HANDLER)) {
			hyssdbg_compile();
		}

		if (HYSSDBG_G(ops)) {
			hyssdbg_notice("printinfo", "file=\"%s\" num=\"%d\"", "Context %s (%d ops)", HYSSDBG_G(exec), HYSSDBG_G(ops)->last);

			hyssdbg_print_function_helper((gear_function*) HYSSDBG_G(ops));
		}
	} else {
		hyssdbg_error("inactive", "type=\"nocontext\"", "No execution context set");
	}

return SUCCESS;
} /* }}} */

HYSSDBG_PRINT(stack) /* {{{ */
{
	if (HYSSDBG_G(in_execution) && EG(current_execute_data)) {
		gear_op_array *ops = &hyssdbg_user_execute_data(EG(current_execute_data))->func->op_array;
		if (ops->function_name) {
			if (ops->scope) {
				hyssdbg_notice("printinfo", "method=\"%s::%s\" num=\"%d\"", "Stack in %s::%s() (%d ops)", ZSTR_VAL(ops->scope->name), ZSTR_VAL(ops->function_name), ops->last);
			} else {
				hyssdbg_notice("printinfo", "function=\"%s\" num=\"%d\"", "Stack in %s() (%d ops)", ZSTR_VAL(ops->function_name), ops->last);
			}
		} else {
			if (ops->filename) {
				hyssdbg_notice("printinfo", "file=\"%s\" num=\"%d\"", "Stack in %s (%d ops)", ZSTR_VAL(ops->filename), ops->last);
			} else {
				hyssdbg_notice("printinfo", "opline=\"%p\" num=\"%d\"", "Stack @ %p (%d ops)", ops, ops->last);
			}
		}
		hyssdbg_print_function_helper((gear_function*) ops);
	} else {
		hyssdbg_error("inactive", "type=\"execution\"", "Not Executing!");
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_PRINT(class) /* {{{ */
{
	gear_class_entry *ce;

	if (hyssdbg_safe_class_lookup(param->str, param->len, &ce) == SUCCESS) {
		hyssdbg_notice("printinfo", "type=\"%s\" flag=\"%s\" class=\"%s\" num=\"%d\"", "%s %s: %s (%d methods)",
			(ce->type == GEAR_USER_CLASS) ?
				"User" : "Internal",
			(ce->ce_flags & GEAR_ACC_INTERFACE) ?
				"Interface" :
				(ce->ce_flags & GEAR_ACC_ABSTRACT) ?
					"Abstract Class" :
					"Class",
			ZSTR_VAL(ce->name),
			gear_hash_num_elements(&ce->function_table));

		hyssdbg_xml("<printmethods %r>");

		if (gear_hash_num_elements(&ce->function_table)) {
			gear_function *method;

			GEAR_HASH_FOREACH_PTR(&ce->function_table, method) {
				hyssdbg_print_function_helper(method);
			} GEAR_HASH_FOREACH_END();
		}

		hyssdbg_xml("</printmethods>");
	} else {
		hyssdbg_error("print", "type=\"noclass\" class=\"%s\"", "The class %s could not be found", param->str);
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_PRINT(method) /* {{{ */
{
	gear_class_entry *ce;

	if (hyssdbg_safe_class_lookup(param->method.class, strlen(param->method.class), &ce) == SUCCESS) {
		gear_function *fbc;
		gear_string *lcname = gear_string_alloc(strlen(param->method.name), 0);
		gear_str_tolower_copy(ZSTR_VAL(lcname), param->method.name, ZSTR_LEN(lcname));

		if ((fbc = gear_hash_find_ptr(&ce->function_table, lcname))) {
			hyssdbg_notice("printinfo", "type=\"%s\" flags=\"Method\" symbol=\"%s\" num=\"%d\"", "%s Method %s (%d ops)",
				(fbc->type == GEAR_USER_FUNCTION) ? "User" : "Internal",
				ZSTR_VAL(fbc->common.function_name),
				(fbc->type == GEAR_USER_FUNCTION) ? fbc->op_array.last : 0);

			hyssdbg_print_function_helper(fbc);
		} else {
			hyssdbg_error("print", "type=\"nomethod\" method=\"%s::%s\"", "The method %s::%s could not be found", param->method.class, param->method.name);
		}

		gear_string_release(lcname);
	} else {
		hyssdbg_error("print", "type=\"noclass\" class=\"%s\"", "The class %s could not be found", param->method.class);
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_PRINT(func) /* {{{ */
{
	HashTable *func_table = EG(function_table);
	gear_function* fbc;
	const char *func_name = param->str;
	size_t func_name_len = param->len;
	gear_string *lcname;
	/* search active scope if begins with period */
	if (func_name[0] == '.') {
		gear_class_entry *scope = gear_get_executed_scope();

		if (scope) {
			func_name++;
			func_name_len--;

			func_table = &scope->function_table;
		} else {
			hyssdbg_error("inactive", "type=\"noclasses\"", "No active class");
			return SUCCESS;
		}
	} else if (!EG(function_table)) {
		hyssdbg_error("inactive", "type=\"function_table\"", "No function table loaded");
		return SUCCESS;
	} else {
		func_table = EG(function_table);
	}

	lcname = gear_string_alloc(func_name_len, 0);
	gear_str_tolower_copy(ZSTR_VAL(lcname), func_name, ZSTR_LEN(lcname));

	hyssdbg_try_access {
		if ((fbc = gear_hash_find_ptr(func_table, lcname))) {
			hyssdbg_notice("printinfo", "type=\"%s\" flags=\"%s\" symbol=\"%s\" num=\"%d\"", "%s %s %s (%d ops)",
				(fbc->type == GEAR_USER_FUNCTION) ? "User" : "Internal",
				(fbc->common.scope) ? "Method" : "Function",
				ZSTR_VAL(fbc->common.function_name),
				(fbc->type == GEAR_USER_FUNCTION) ? fbc->op_array.last : 0);

			hyssdbg_print_function_helper(fbc);
		} else {
			hyssdbg_error("print", "type=\"nofunction\" function=\"%s\"", "The function %s could not be found", func_name);
		}
	} hyssdbg_catch_access {
		hyssdbg_error("signalsegv", "function=\"%.*s\"", "Couldn't fetch function %.*s, invalid data source", (int) func_name_len, func_name);
	} hyssdbg_end_try_access();

	efree(lcname);

	return SUCCESS;
} /* }}} */

void hyssdbg_print_opcodes_main() {
	hyssdbg_out("function name: (null)\n");
	hyssdbg_print_function_helper((gear_function *) HYSSDBG_G(ops));
}

void hyssdbg_print_opcodes_function(const char *function, size_t len) {
	gear_function *func = gear_hash_str_find_ptr(EG(function_table), function, len);

	if (!func) {
		gear_string *rt_name;
		GEAR_HASH_FOREACH_STR_KEY_PTR(EG(class_table), rt_name, func) {
			if (func->type == GEAR_USER_FUNCTION && *rt_name->val == '\0') {
				if (func->op_array.function_name->len == len && !gear_binary_strcasecmp(function, len, func->op_array.function_name->val, func->op_array.function_name->len)) {
					hyssdbg_print_opcodes_function(rt_name->val, rt_name->len);
				}
			}
		} GEAR_HASH_FOREACH_END();

		return;
	}

	hyssdbg_out("function name: %.*s\n", (int) ZSTR_LEN(func->op_array.function_name), ZSTR_VAL(func->op_array.function_name));
	hyssdbg_print_function_helper(func);
}

static void hyssdbg_print_opcodes_method_ce(gear_class_entry *ce, const char *function) {
	gear_function *func;

	if (ce->type != GEAR_USER_CLASS) {
		hyssdbg_out("function name: %s::%s (internal)\n", ce->name->val, function);
		return;
	}

	if (!(func = gear_hash_str_find_ptr(&ce->function_table, function, strlen(function)))) {
		return;
	}

	hyssdbg_out("function name: %s::%s\n", ce->name->val, function);
	hyssdbg_print_function_helper(func);
}

void hyssdbg_print_opcodes_method(const char *class, const char *function) {
	gear_class_entry *ce;

	if (hyssdbg_safe_class_lookup(class, strlen(class), &ce) != SUCCESS) {
		gear_string *rt_name;
		GEAR_HASH_FOREACH_STR_KEY_PTR(EG(class_table), rt_name, ce) {
			if (ce->type == GEAR_USER_CLASS && *rt_name->val == '\0') {
				if (ce->name->len == strlen(class) && !gear_binary_strcasecmp(class, strlen(class), ce->name->val, ce->name->len)) {
					hyssdbg_print_opcodes_method_ce(ce, function);
				}
			}
		} GEAR_HASH_FOREACH_END();

		return;
	}

	hyssdbg_print_opcodes_method_ce(ce, function);
}

static void hyssdbg_print_opcodes_ce(gear_class_entry *ce) {
	gear_function *method;
	gear_string *method_name;
	gear_bool first = 1;

	hyssdbg_out("%s %s: %s\n",
		(ce->type == GEAR_USER_CLASS) ?
			"user" : "internal",
		(ce->ce_flags & GEAR_ACC_INTERFACE) ?
			"interface" :
			(ce->ce_flags & GEAR_ACC_ABSTRACT) ?
				"abstract Class" :
				"class",
		ZSTR_VAL(ce->name));

	if (ce->type != GEAR_USER_CLASS) {
		return;
	}

	hyssdbg_out("%d methods: ", gear_hash_num_elements(&ce->function_table));
	GEAR_HASH_FOREACH_PTR(&ce->function_table, method) {
		if (first) {
			first = 0;
		} else {
			hyssdbg_out(", ");
		}
		hyssdbg_out("%s", ZSTR_VAL(method->common.function_name));
	} GEAR_HASH_FOREACH_END();
	if (first) {
		hyssdbg_out("-");
	}
	hyssdbg_out("\n");

	GEAR_HASH_FOREACH_STR_KEY_PTR(&ce->function_table, method_name, method) {
		hyssdbg_out("\nfunction name: %s\n", ZSTR_VAL(method_name));
		hyssdbg_print_function_helper(method);
	} GEAR_HASH_FOREACH_END();
}

void hyssdbg_print_opcodes_class(const char *class) {
	gear_class_entry *ce;

	if (hyssdbg_safe_class_lookup(class, strlen(class), &ce) != SUCCESS) {
		gear_string *rt_name;
		GEAR_HASH_FOREACH_STR_KEY_PTR(EG(class_table), rt_name, ce) {
			if (ce->type == GEAR_USER_CLASS && *rt_name->val == '\0') {
				if (ce->name->len == strlen(class) && !gear_binary_strcasecmp(class, strlen(class), ce->name->val, ce->name->len)) {
					hyssdbg_print_opcodes_ce(ce);
				}
			}
		} GEAR_HASH_FOREACH_END();

		return;
	}

	hyssdbg_print_opcodes_ce(ce);
}

HYSSDBG_API void hyssdbg_print_opcodes(char *function)
{
	if (function == NULL) {
		hyssdbg_print_opcodes_main();
	} else if (function[0] == '*' && function[1] == 0) {
		/* all */
		gear_string *name;
		gear_function *func;
		gear_class_entry *ce;

		hyssdbg_print_opcodes_main();

		GEAR_HASH_FOREACH_STR_KEY_PTR(EG(function_table), name, func) {
			if (func->type == GEAR_USER_FUNCTION) {
				hyssdbg_out("\n");
				hyssdbg_print_opcodes_function(ZSTR_VAL(name), ZSTR_LEN(name));
			}
		} GEAR_HASH_FOREACH_END();

		GEAR_HASH_FOREACH_PTR(EG(class_table), ce) {
			if (ce->type == GEAR_USER_CLASS) {
				hyssdbg_out("\n\n");
				hyssdbg_print_opcodes_ce(ce);
			}
		} GEAR_HASH_FOREACH_END();
	} else {
		function = gear_str_tolower_dup(function, strlen(function));

		if (strstr(function, "::") == NULL) {
			hyssdbg_print_opcodes_function(function, strlen(function));
		} else {
			char *method_name, *class_name = strtok(function, "::");
			if ((method_name = strtok(NULL, "::")) == NULL) {
				hyssdbg_print_opcodes_class(class_name);
			} else {
				hyssdbg_print_opcodes_method(class_name, method_name);
			}
		}

		efree(function);
	}
}
