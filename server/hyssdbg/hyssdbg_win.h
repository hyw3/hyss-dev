/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_WIN_H
#define HYSSDBG_WIN_H

#include "winbase.h"
#include "windows.h"
#include "excpt.h"

#define PROT_READ 1
#define PROT_WRITE 2

int mprotect(void *addr, size_t size, int protection);

void hyssdbg_win_set_mm_heap(gear_mm_heap *heap);

int hyssdbg_exception_handler_win32(EXCEPTION_POINTERS *xp);

#endif
