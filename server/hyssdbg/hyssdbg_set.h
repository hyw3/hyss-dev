/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_SET_H
#define HYSSDBG_SET_H

#include "hyssdbg_cmd.h"

#define HYSSDBG_SET(name) HYSSDBG_COMMAND(set_##name)

HYSSDBG_SET(prompt);
#ifndef _WIN32
HYSSDBG_SET(color);
HYSSDBG_SET(colors);
#endif
HYSSDBG_SET(oplog);
HYSSDBG_SET(break);
HYSSDBG_SET(breaks);
HYSSDBG_SET(quiet);
HYSSDBG_SET(stepping);
HYSSDBG_SET(refcount);
HYSSDBG_SET(pagination);
HYSSDBG_SET(lines);

extern const hyssdbg_command_t hyssdbg_set_commands[];

#endif /* HYSSDBG_SET_H */
