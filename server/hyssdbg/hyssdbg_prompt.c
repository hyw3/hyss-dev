/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include "gear.h"
#include "gear_compile.h"
#include "gear_exceptions.h"
#include "gear_vm.h"
#include "gear_generators.h"
#include "gear_interfaces.h"
#include "gear_smart_str.h"
#include "hyssdbg.h"
#include "hyssdbg_io.h"

#include "hyssdbg_help.h"
#include "hyssdbg_print.h"
#include "hyssdbg_info.h"
#include "hyssdbg_break.h"
#include "hyssdbg_opcode.h"
#include "hyssdbg_list.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_prompt.h"
#include "hyssdbg_cmd.h"
#include "hyssdbg_set.h"
#include "hyssdbg_frame.h"
#include "hyssdbg_lexer.h"
#include "hyssdbg_parser.h"
#include "hyssdbg_wait.h"
#include "hyssdbg_eol.h"

#if GEAR_VM_KIND != GEAR_VM_KIND_CALL && GEAR_VM_KIND != GEAR_VM_KIND_HYBRID
#error "hyssdbg can only be built with CALL gear vm kind"
#endif

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)
extern int hyssdbg_startup_run;

#ifdef HAVE_LIBDL
#ifdef HYSS_WIN32
#include "win32/param.h"
#include "win32/winutil.h"
#define GET_DL_ERROR()  hyss_win_err()
#else
#include <sys/param.h>
#define GET_DL_ERROR()  DL_ERROR()
#endif
#endif

/* {{{ command declarations */
const hyssdbg_command_t hyssdbg_prompt_commands[] = {
	HYSSDBG_COMMAND_D(exec,      "set execution context",                    'e', NULL, "s", 0),
	HYSSDBG_COMMAND_D(stdin,     "read script from stdin",                    0 , NULL, "s", 0),
	HYSSDBG_COMMAND_D(step,      "step through execution",                   's', NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(continue,  "continue execution",                       'c', NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(run,       "attempt execution",                        'r', NULL, "|s", 0),
	HYSSDBG_COMMAND_D(ev,        "evaluate some code",                        0 , NULL, "i", HYSSDBG_ASYNC_SAFE), /* restricted ASYNC_SAFE */
	HYSSDBG_COMMAND_D(until,     "continue past the current line",           'u', NULL, 0, 0),
	HYSSDBG_COMMAND_D(finish,    "continue past the end of the stack",       'F', NULL, 0, 0),
	HYSSDBG_COMMAND_D(leave,     "continue until the end of the stack",      'L', NULL, 0, 0),
	HYSSDBG_COMMAND_D(generator, "inspect or switch to a generator",         'g', NULL, "|n", 0),
	HYSSDBG_COMMAND_D(print,     "print something",                          'p', hyssdbg_print_commands, "|*c", 0),
	HYSSDBG_COMMAND_D(break,     "set breakpoint",                           'b', hyssdbg_break_commands, "|*c", 0),
	HYSSDBG_COMMAND_D(back,      "show trace",                               't', NULL, "|n", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(frame,     "switch to a frame",                        'f', NULL, "|n", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(list,      "lists some code",                          'l', hyssdbg_list_commands,  "*", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(info,      "displays some informations",               'i', hyssdbg_info_commands, "|s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(clean,     "clean the execution environment",          'X', NULL, 0, 0),
	HYSSDBG_COMMAND_D(clear,     "clear breakpoints",                        'C', NULL, 0, 0),
	HYSSDBG_COMMAND_D(help,      "show help menu",                           'h', hyssdbg_help_commands, "|s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(set,       "set hyssdbg configuration",                 'S', hyssdbg_set_commands,   "s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(register,  "register a function",                      'R', NULL, "s", 0),
	HYSSDBG_COMMAND_D(source,    "execute a hyssdbginit",                     '<', NULL, "s", 0),
	HYSSDBG_COMMAND_D(export,    "export breaks to a .hyssdbginit script",    '>', NULL, "s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(sh,   	    "shell a command",                           0 , NULL, "i", 0),
	HYSSDBG_COMMAND_D(quit,      "exit hyssdbg",                              'q', NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(wait,      "wait for other process",                   'W', NULL, 0, 0),
	HYSSDBG_COMMAND_D(watch,     "set watchpoint",                           'w', hyssdbg_watch_commands, "|ss", 0),
	HYSSDBG_COMMAND_D(next,      "step over next line",                      'n', NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_COMMAND_D(eol,       "set EOL",                                  'E', NULL, "|s", 0),
	HYSSDBG_END_COMMAND
}; /* }}} */

static inline int hyssdbg_call_register(hyssdbg_param_t *stack) /* {{{ */
{
	hyssdbg_param_t *name = NULL;

	if (stack->type == STACK_PARAM) {
		char *lc_name;

		name = stack->next;

		if (!name || name->type != STR_PARAM) {
			return FAILURE;
		}

		lc_name = gear_str_tolower_dup(name->str, name->len);

		if (gear_hash_str_exists(&HYSSDBG_G(registered), lc_name, name->len)) {
			zval fretval;
			gear_fcall_info fci;

			memset(&fci, 0, sizeof(gear_fcall_info));

			ZVAL_STRINGL(&fci.function_name, lc_name, name->len);
			fci.size = sizeof(gear_fcall_info);
			//???fci.symbol_table = gear_rebuild_symbol_table();
			fci.object = NULL;
			fci.retval = &fretval;
			fci.no_separation = 1;

			if (name->next) {
				zval params;
				hyssdbg_param_t *next = name->next;

				array_init(&params);

				while (next) {
					char *buffered = NULL;

					switch (next->type) {
						case OP_PARAM:
						case COND_PARAM:
						case STR_PARAM:
							add_next_index_stringl(&params, next->str, next->len);
						break;

						case NUMERIC_PARAM:
							add_next_index_long(&params, next->num);
						break;

						case METHOD_PARAM:
							spprintf(&buffered, 0, "%s::%s", next->method.class, next->method.name);
							add_next_index_string(&params, buffered);
						break;

						case NUMERIC_METHOD_PARAM:
							spprintf(&buffered, 0, "%s::%s#%ld", next->method.class, next->method.name, next->num);
							add_next_index_string(&params, buffered);
						break;

						case NUMERIC_FUNCTION_PARAM:
							spprintf(&buffered, 0, "%s#%ld", next->str, next->num);
							add_next_index_string(&params, buffered);
						break;

						case FILE_PARAM:
							spprintf(&buffered, 0, "%s:%ld", next->file.name, next->file.line);
							add_next_index_string(&params, buffered);
						break;

						case NUMERIC_FILE_PARAM:
							spprintf(&buffered, 0, "%s:#%ld", next->file.name, next->file.line);
							add_next_index_string(&params, buffered);
						break;

						default: {
							/* not yet */
						}
					}

					next = next->next;
				}

				gear_fcall_info_args(&fci, &params);
			} else {
				fci.params = NULL;
				fci.param_count = 0;
			}

			hyssdbg_activate_err_buf(0);
			hyssdbg_free_err_buf();

			hyssdbg_debug("created %d params from arguments", fci.param_count);

			if (gear_call_function(&fci, NULL) == SUCCESS) {
				gear_print_zval_r(&fretval, 0);
				hyssdbg_out("\n");
				zval_ptr_dtor(&fretval);
			}

			zval_ptr_dtor_str(&fci.function_name);
			efree(lc_name);

			return SUCCESS;
		}

		efree(lc_name);
	}

	return FAILURE;
} /* }}} */

struct hyssdbg_init_state {
	int line;
	gear_bool in_code;
	char *code;
	size_t code_len;
	const char *init_file;
};

static void hyssdbg_line_init(char *cmd, struct hyssdbg_init_state *state) {
	size_t cmd_len = strlen(cmd);

	state->line++;

	while (cmd_len > 0L && isspace(cmd[cmd_len-1])) {
		cmd_len--;
	}

	cmd[cmd_len] = '\0';

	if (*cmd && cmd_len > 0L && cmd[0] != '#') {
		if (cmd_len == 2) {
			if (memcmp(cmd, "<:", sizeof("<:")-1) == SUCCESS) {
				state->in_code = 1;
				return;
			} else {
				if (memcmp(cmd, ":>", sizeof(":>")-1) == SUCCESS) {
					state->in_code = 0;
					state->code[state->code_len] = '\0';
					gear_eval_stringl(state->code, state->code_len, NULL, "hyssdbginit code");
					free(state->code);
					state->code = NULL;
					return;
				}
			}
		}

		if (state->in_code) {
			if (state->code == NULL) {
				state->code = malloc(cmd_len + 1);
			} else {
				state->code = realloc(state->code, state->code_len + cmd_len + 1);
			}

			if (state->code) {
				memcpy(&state->code[state->code_len], cmd, cmd_len);
				state->code_len += cmd_len;
			}

			return;
		}

		gear_try {
			char *input = hyssdbg_read_input(cmd);
			hyssdbg_param_t stack;

			hyssdbg_init_param(&stack, STACK_PARAM);

			hyssdbg_activate_err_buf(1);

			if (hyssdbg_do_parse(&stack, input) <= 0) {
				switch (hyssdbg_stack_execute(&stack, 1 /* allow_async_unsafe == 1 */)) {
					case FAILURE:
						hyssdbg_activate_err_buf(0);
						if (hyssdbg_call_register(&stack) == FAILURE) {
							if (state->init_file) {
								hyssdbg_output_err_buf("initfailure", "%b file=\"%s\" line=\"%d\" input=\"%s\"", "Unrecognized command in %s:%d: %s, %b!", state->init_file, state->line, input);
							} else {
								hyssdbg_output_err_buf("initfailure", "%b line=\"%d\" input=\"%s\"", "Unrecognized command on line %d: %s, %b!", state->line, input);
							}
						}
					break;
				}
			}

			hyssdbg_activate_err_buf(0);
			hyssdbg_free_err_buf();

			hyssdbg_stack_free(&stack);
			hyssdbg_destroy_input(&input);
		} gear_catch {
			HYSSDBG_G(flags) &= ~(HYSSDBG_IS_RUNNING | HYSSDBG_IS_CLEANING);
			if (HYSSDBG_G(flags) & HYSSDBG_IS_QUITTING) {
				gear_bailout();
			}
		} gear_end_try();
	}

}

void hyssdbg_string_init(char *buffer) {
	struct hyssdbg_init_state state = {0};
	char *str = strtok(buffer, "\n");

	while (str) {
		hyssdbg_line_init(str, &state);

		str = strtok(NULL, "\n");
	}

	if (state.code) {
		free(state.code);
	}
}

void hyssdbg_try_file_init(char *init_file, size_t init_file_len, gear_bool free_init) /* {{{ */
{
	gear_stat_t sb;

	if (init_file && VCWD_STAT(init_file, &sb) != -1) {
		FILE *fp = fopen(init_file, "r");
		if (fp) {
			char cmd[HYSSDBG_MAX_CMD];
			struct hyssdbg_init_state state = {0};

			state.init_file = init_file;

			while (fgets(cmd, HYSSDBG_MAX_CMD, fp) != NULL) {
				hyssdbg_line_init(cmd, &state);
			}

			if (state.code) {
				free(state.code);
			}

			fclose(fp);
		} else {
			hyssdbg_error("initfailure", "type=\"openfile\" file=\"%s\"", "Failed to open %s for initialization", init_file);
		}

		if (free_init) {
			free(init_file);
		}
	}
} /* }}} */

void hyssdbg_init(char *init_file, size_t init_file_len, gear_bool use_default) /* {{{ */
{
	if (init_file) {
		hyssdbg_try_file_init(init_file, init_file_len, 1);
	} else if (use_default) {
		char *scan_dir = getenv("HYSS_ICS_SCAN_DIR");
		char *sys_ics;
		int i;

		GEAR_IGNORE_VALUE(asprintf(&sys_ics, "%s/" HYSSDBG_INIT_FILENAME, HYSS_CONFIG_FILE_PATH));
		hyssdbg_try_file_init(sys_ics, strlen(sys_ics), 0);
		free(sys_ics);

		if (!scan_dir) {
			scan_dir = HYSS_CONFIG_FILE_SCAN_DIR;
		}
		while (*scan_dir != 0) {
			i = 0;
			while (scan_dir[i] != ':') {
				if (scan_dir[i++] == 0) {
					i = -1;
					break;
				}
			}
			if (i != -1) {
				scan_dir[i] = 0;
			}

			GEAR_IGNORE_VALUE(asprintf(&init_file, "%s/%s", scan_dir, HYSSDBG_INIT_FILENAME));
			hyssdbg_try_file_init(init_file, strlen(init_file), 1);
			if (i == -1) {
				break;
			}
			scan_dir += i + 1;
		}

		hyssdbg_try_file_init(HYSSDBG_STRL(HYSSDBG_INIT_FILENAME), 0);
	}
}
/* }}} */

void hyssdbg_clean(gear_bool full, gear_bool resubmit) /* {{{ */
{
	/* this is implicitly required */
	if (HYSSDBG_G(ops)) {
		destroy_op_array(HYSSDBG_G(ops));
		efree(HYSSDBG_G(ops));
		HYSSDBG_G(ops) = NULL;
	}

	if (!resubmit && HYSSDBG_G(cur_command)) {
		free(HYSSDBG_G(cur_command));
		HYSSDBG_G(cur_command) = NULL;
	}

	if (full) {
		HYSSDBG_G(flags) |= HYSSDBG_IS_CLEANING;
	}
} /* }}} */

HYSSDBG_COMMAND(exec) /* {{{ */
{
	gear_stat_t sb;

	if (VCWD_STAT(param->str, &sb) != FAILURE) {
		if (sb.st_mode & (S_IFREG|S_IFLNK)) {
			char *res = hyssdbg_resolve_path(param->str);
			size_t res_len = strlen(res);

			if ((res_len != HYSSDBG_G(exec_len)) || (memcmp(res, HYSSDBG_G(exec), res_len) != SUCCESS)) {
				if (HYSSDBG_G(in_execution)) {
					if (hyssdbg_ask_user_permission("Do you really want to stop execution to set a new execution context?") == FAILURE) {
						return FAILURE;
					}
				}

				if (HYSSDBG_G(exec)) {
					hyssdbg_notice("exec", "type=\"unset\" context=\"%s\"", "Unsetting old execution context: %s", HYSSDBG_G(exec));
					efree(HYSSDBG_G(exec));
					HYSSDBG_G(exec) = NULL;
					HYSSDBG_G(exec_len) = 0L;
				}

				if (HYSSDBG_G(ops)) {
					hyssdbg_notice("exec", "type=\"unsetops\"", "Destroying compiled opcodes");
					hyssdbg_clean(0, 0);
				}

				HYSSDBG_G(exec) = res;
				HYSSDBG_G(exec_len) = res_len;

				VCWD_CHDIR_FILE(res);

				*SG(request_info).argv = HYSSDBG_G(exec);
				hyss_build_argv(NULL, &PG(http_globals)[TRACK_VARS_SERVER]);

				hyssdbg_notice("exec", "type=\"set\" context=\"%s\"", "Set execution context: %s", HYSSDBG_G(exec));

				if (HYSSDBG_G(in_execution)) {
					hyssdbg_clean(1, 0);
					return SUCCESS;
				}

				hyssdbg_compile();
			} else {
				hyssdbg_notice("exec", "type=\"unchanged\"", "Execution context not changed");
			}
		} else {
			hyssdbg_error("exec", "type=\"invalid\" context=\"%s\"", "Cannot use %s as execution context, not a valid file or symlink", param->str);
		}
	} else {
		hyssdbg_error("exec", "type=\"notfound\" context=\"%s\"", "Cannot stat %s, ensure the file exists", param->str);
	}
	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(stdin)
{
	smart_str code = {0};
	char *buf;
	char *sep = param->str;
	int seplen = param->len;
	int bytes = 0;

	smart_str_appends(&code, "!@");

	do {
		HYSSDBG_G(input_buflen) += bytes;
		if (HYSSDBG_G(input_buflen) <= 0) {
			continue;
		}

		if (sep && seplen) {
			char *nl = buf = HYSSDBG_G(input_buffer);
			do {
				if (buf == nl + seplen) {
					if (!memcmp(sep, nl, seplen) && (*buf == '\n' || (*buf == '\r' && buf[1] == '\n'))) {
						smart_str_appendl(&code, HYSSDBG_G(input_buffer), nl - HYSSDBG_G(input_buffer));
						memmove(HYSSDBG_G(input_buffer), ++buf, --HYSSDBG_G(input_buflen));
						goto exec_code;
					}
				}
				if (*buf == '\n') {
					nl = buf + 1;
				}
				buf++;
			} while (--HYSSDBG_G(input_buflen));
			if (buf != nl && buf <= nl + seplen) {
				smart_str_appendl(&code, HYSSDBG_G(input_buffer), nl - HYSSDBG_G(input_buffer));
				HYSSDBG_G(input_buflen) = buf - nl;
				memmove(HYSSDBG_G(input_buffer), nl, HYSSDBG_G(input_buflen));
			} else {
				HYSSDBG_G(input_buflen) = 0;
				smart_str_appendl(&code, HYSSDBG_G(input_buffer), buf - HYSSDBG_G(input_buffer));
			}
		} else {
			smart_str_appendl(&code, HYSSDBG_G(input_buffer), HYSSDBG_G(input_buflen));
			HYSSDBG_G(input_buflen) = 0;
		}
	} while ((bytes = hyssdbg_mixed_read(HYSSDBG_G(io)[HYSSDBG_STDIN].fd, HYSSDBG_G(input_buffer) + HYSSDBG_G(input_buflen), HYSSDBG_MAX_CMD - HYSSDBG_G(input_buflen), -1)) > 0);

	if (bytes < 0) {
		HYSSDBG_G(flags) |= HYSSDBG_IS_QUITTING | HYSSDBG_IS_DISCONNECTED;
		gear_bailout();
	}

exec_code:
	smart_str_0(&code);

	if (hyssdbg_compile_stdin(code.s) == FAILURE) {
		gear_exception_error(EG(exception), E_ERROR);
		gear_bailout();
	}

	return SUCCESS;
} /* }}} */

int hyssdbg_compile_stdin(gear_string *code) {
	zval zv;

	ZVAL_STR(&zv, code);

	HYSSDBG_G(ops) = gear_compile_string(&zv, "Standard input code");

	gear_string_release(code);

	if (EG(exception)) {
		return FAILURE;
	}

	if (HYSSDBG_G(exec)) {
		efree(HYSSDBG_G(exec));
	}
	HYSSDBG_G(exec) = estrdup("Standard input code");
	HYSSDBG_G(exec_len) = sizeof("Standard input code") - 1;
	{ /* remove leading !@ from source */
		int i;
		/* remove trailing data after zero byte, used for avoiding conflicts in eval()'ed code snippets */
		gear_string *source_path = strpprintf(0, "Standard input code%c%p", 0, HYSSDBG_G(ops)->opcodes);
		hyssdbg_file_source *data = gear_hash_find_ptr(&HYSSDBG_G(file_sources), source_path);
		dtor_func_t dtor = HYSSDBG_G(file_sources).pDestructor;
		HYSSDBG_G(file_sources).pDestructor = NULL;
		gear_hash_del(&HYSSDBG_G(file_sources), source_path);
		HYSSDBG_G(file_sources).pDestructor = dtor;
		gear_hash_str_update_ptr(&HYSSDBG_G(file_sources), "Standard input code", sizeof("Standard input code")-1, data);
		gear_string_release(source_path);

		for (i = 1; i <= data->lines; i++) {
			data->line[i] -= 2;
		}
		data->len -= 2;
		memmove(data->buf, data->buf + 2, data->len);
	}

	hyssdbg_notice("compile", "context=\"Standard input code\"", "Successful compilation of stdin input");

	return SUCCESS;
}

int hyssdbg_compile(void) /* {{{ */
{
	gear_file_handle fh;
	char *buf;
	char *start_line = NULL;
	size_t len;
	size_t start_line_len;
	int i;

	if (!HYSSDBG_G(exec)) {
		hyssdbg_error("inactive", "type=\"nocontext\"", "No execution context");
		return FAILURE;
	}

	if (hyss_stream_open_for_gear_ex(HYSSDBG_G(exec), &fh, USE_PATH|STREAM_OPEN_FOR_INCLUDE) == SUCCESS && gear_stream_fixup(&fh, &buf, &len) == SUCCESS) {
		/* Skip #! line */
		if (len >= 3 && buf[0] == '#' && buf[1] == '!') {
			char *end = buf + len;
			do {
				switch (fh.handle.stream.mmap.buf++[0]) {
					case '\r':
						if (fh.handle.stream.mmap.buf[0] == '\n') {
							fh.handle.stream.mmap.buf++;
						}
					case '\n':
						CG(start_lineno) = 2;
						start_line_len = fh.handle.stream.mmap.buf - buf;
						start_line = emalloc(start_line_len);
						memcpy(start_line, buf, start_line_len);
						fh.handle.stream.mmap.len -= start_line_len;
						end = fh.handle.stream.mmap.buf;
				}
			} while (fh.handle.stream.mmap.buf + 1 < end);
		}

		HYSSDBG_G(ops) = gear_compile_file(&fh, GEAR_INCLUDE);

		/* prepend shebang line to file_source */
		if (start_line) {
			hyssdbg_file_source *data = gear_hash_find_ptr(&HYSSDBG_G(file_sources), HYSSDBG_G(ops)->filename);

			dtor_func_t dtor = HYSSDBG_G(file_sources).pDestructor;
			HYSSDBG_G(file_sources).pDestructor = NULL;
			gear_hash_del(&HYSSDBG_G(file_sources), HYSSDBG_G(ops)->filename);
			HYSSDBG_G(file_sources).pDestructor = dtor;

			data = erealloc(data, sizeof(hyssdbg_file_source) + sizeof(uint32_t) * ++data->lines);
			memmove(data->line + 1, data->line, sizeof(uint32_t) * data->lines);
			data->line[0] = 0;
			data->buf = erealloc(data->buf, data->len + start_line_len);
			memmove(data->buf + start_line_len, data->buf, data->len);
			memcpy(data->buf, start_line, start_line_len);
			efree(start_line);
			data->len += start_line_len;
			for (i = 1; i <= data->lines; i++) {
				data->line[i] += start_line_len;
			}
			gear_hash_update_ptr(&HYSSDBG_G(file_sources), HYSSDBG_G(ops)->filename, data);
		}

		fh.handle.stream.mmap.buf = buf;
		fh.handle.stream.mmap.len = len;
		gear_destroy_file_handle(&fh);
		if (EG(exception)) {
			gear_exception_error(EG(exception), E_ERROR);
			gear_bailout();
		}

		hyssdbg_notice("compile", "context=\"%s\"", "Successful compilation of %s", HYSSDBG_G(exec));

		return SUCCESS;
	} else {
		hyssdbg_error("compile", "type=\"openfailure\" context=\"%s\"", "Could not open file %s", HYSSDBG_G(exec));
	}

	return FAILURE;
} /* }}} */

HYSSDBG_COMMAND(step) /* {{{ */
{
	if (HYSSDBG_G(in_execution)) {
		HYSSDBG_G(flags) |= HYSSDBG_IS_STEPPING;
	}

	return HYSSDBG_NEXT;
} /* }}} */

HYSSDBG_COMMAND(continue) /* {{{ */
{
	return HYSSDBG_NEXT;
} /* }}} */

int hyssdbg_skip_line_helper() /* {{{ */ {
	gear_execute_data *ex = hyssdbg_user_execute_data(EG(current_execute_data));
	const gear_op_array *op_array = &ex->func->op_array;
	const gear_op *opline = op_array->opcodes;

	HYSSDBG_G(flags) |= HYSSDBG_IN_UNTIL;
	HYSSDBG_G(seek_ex) = ex;
	do {
		if (opline->lineno != ex->opline->lineno
		 || opline->opcode == GEAR_RETURN
		 || opline->opcode == GEAR_FAST_RET
		 || opline->opcode == GEAR_GENERATOR_RETURN
		 || opline->opcode == GEAR_EXIT
		 || opline->opcode == GEAR_YIELD
		 || opline->opcode == GEAR_YIELD_FROM
		) {
			gear_hash_index_update_ptr(&HYSSDBG_G(seek), (gear_ulong) opline, (void *) opline);
		}
	} while (++opline < op_array->opcodes + op_array->last);

	return HYSSDBG_UNTIL;
}
/* }}} */

HYSSDBG_COMMAND(until) /* {{{ */
{
	if (!HYSSDBG_G(in_execution)) {
		hyssdbg_error("inactive", "type=\"noexec\"", "Not executing");
		return SUCCESS;
	}

	return hyssdbg_skip_line_helper();
} /* }}} */

HYSSDBG_COMMAND(next) /* {{{ */
{
	if (!HYSSDBG_G(in_execution)) {
		hyssdbg_error("inactive", "type=\"noexec\"", "Not executing");
		return SUCCESS;
	}

	HYSSDBG_G(flags) |= HYSSDBG_IS_STEPPING;
	return hyssdbg_skip_line_helper();
} /* }}} */

static void hyssdbg_seek_to_end(void) /* {{{ */ {
	gear_execute_data *ex = hyssdbg_user_execute_data(EG(current_execute_data));
	const gear_op_array *op_array = &ex->func->op_array;
	const gear_op *opline = op_array->opcodes;

	HYSSDBG_G(seek_ex) = ex;
	do {
		switch (opline->opcode) {
			case GEAR_RETURN:
			case GEAR_FAST_RET:
			case GEAR_GENERATOR_RETURN:
			case GEAR_EXIT:
			case GEAR_YIELD:
			case GEAR_YIELD_FROM:
				gear_hash_index_update_ptr(&HYSSDBG_G(seek), (gear_ulong) opline, (void *) opline);
		}
	} while (++opline < op_array->opcodes + op_array->last);
}
/* }}} */

HYSSDBG_COMMAND(finish) /* {{{ */
{
	if (!HYSSDBG_G(in_execution)) {
		hyssdbg_error("inactive", "type=\"noexec\"", "Not executing");
		return SUCCESS;
	}

	hyssdbg_seek_to_end();
	if (gear_hash_index_exists(&HYSSDBG_G(seek), (gear_ulong) hyssdbg_user_execute_data(EG(current_execute_data))->opline)) {
		gear_hash_clean(&HYSSDBG_G(seek));
	} else {
		HYSSDBG_G(flags) |= HYSSDBG_IN_FINISH;
	}

	return HYSSDBG_FINISH;
} /* }}} */

HYSSDBG_COMMAND(leave) /* {{{ */
{
	if (!HYSSDBG_G(in_execution)) {
		hyssdbg_error("inactive", "type=\"noexec\"", "Not executing");
		return SUCCESS;
	}

	hyssdbg_seek_to_end();
	if (gear_hash_index_exists(&HYSSDBG_G(seek), (gear_ulong) hyssdbg_user_execute_data(EG(current_execute_data))->opline)) {
		gear_hash_clean(&HYSSDBG_G(seek));
		hyssdbg_notice("leave", "type=\"end\"", "Already at the end of the function");
		return SUCCESS;
	} else {
		HYSSDBG_G(flags) |= HYSSDBG_IN_LEAVE;
		return HYSSDBG_LEAVE;
	}
} /* }}} */

HYSSDBG_COMMAND(frame) /* {{{ */
{
	if (!param) {
		hyssdbg_notice("frame", "id=\"%d\"", "Currently in frame #%d", HYSSDBG_G(frame).num);
	} else {
		hyssdbg_switch_frame(param->num);
	}

	return SUCCESS;
} /* }}} */

static inline void hyssdbg_handle_exception(void) /* {{{ */
{
	gear_object *ex = EG(exception);
	gear_string *msg, *file;
	gear_long line;
	zval zv, rv, tmp;

	EG(exception) = NULL;

	ZVAL_OBJ(&zv, ex);
	gear_call_method_with_0_params(&zv, ex->ce, &ex->ce->__tostring, "__tostring", &tmp);
	file = zval_get_string(gear_read_property(gear_get_exception_base(&zv), &zv, GEAR_STRL("file"), 1, &rv));
	line = zval_get_long(gear_read_property(gear_get_exception_base(&zv), &zv, GEAR_STRL("line"), 1, &rv));

	if (EG(exception)) {
		EG(exception) = NULL;
		msg = ZSTR_EMPTY_ALLOC();
	} else {
		gear_update_property_string(gear_get_exception_base(&zv), &zv, GEAR_STRL("string"), Z_STRVAL(tmp));
		zval_ptr_dtor(&tmp);
		msg = zval_get_string(gear_read_property(gear_get_exception_base(&zv), &zv, GEAR_STRL("string"), 1, &rv));
	}

	hyssdbg_error("exception", "name=\"%s\" file=\"%s\" line=\"" GEAR_LONG_FMT "\"", "Uncaught %s in %s on line " GEAR_LONG_FMT, ZSTR_VAL(ex->ce->name), ZSTR_VAL(file), line);
	gear_string_release(file);
	hyssdbg_writeln("exceptionmsg", "msg=\"%s\"", "%s", ZSTR_VAL(msg));
	gear_string_release(msg);

	if (EG(prev_exception)) {
		OBJ_RELEASE(EG(prev_exception));
		EG(prev_exception) = 0;
	}
	OBJ_RELEASE(ex);
	EG(opline_before_exception) = NULL;

	EG(exit_status) = 255;
} /* }}} */

HYSSDBG_COMMAND(run) /* {{{ */
{
	if (HYSSDBG_G(ops) || HYSSDBG_G(exec)) {
		gear_execute_data *ex = EG(current_execute_data);
		gear_bool restore = 1;

		if (HYSSDBG_G(in_execution)) {
			if (hyssdbg_ask_user_permission("Do you really want to restart execution?") == SUCCESS) {
				hyssdbg_startup_run++;
				hyssdbg_clean(1, 1);
			}
			return SUCCESS;
		}

		if (!HYSSDBG_G(ops)) {
			if (hyssdbg_compile() == FAILURE) {
				hyssdbg_error("compile", "type=\"compilefailure\" context=\"%s\"", "Failed to compile %s, cannot run", HYSSDBG_G(exec));
				goto out;
			}
		}

		if (param && param->type != EMPTY_PARAM && param->len != 0) {
			char **argv = emalloc(5 * sizeof(char *));
			char *end = param->str + param->len, *p = param->str;
			char last_byte;
			int argc = 0;
			int i;

			while (*end == '\r' || *end == '\n') *(end--) = 0;
			last_byte = end[1];
			end[1] = 0;

			while (*p == ' ') p++;
			while (*p) {
				char sep = ' ';
				char *buf = emalloc(end - p + 1), *q = buf;

				if (*p == '<') {
					/* use as STDIN */
					do p++; while (*p == ' ');

					if (*p == '\'' || *p == '"') {
						sep = *(p++);
					}
					while (*p && *p != sep) {
						if (*p == '\\' && (p[1] == sep || p[1] == '\\')) {
							p++;
						}
						*(q++) = *(p++);
					}
					*(q++) = 0;
					if (*p) {
						do p++; while (*p == ' ');
					}

					if (*p) {
						hyssdbg_error("cmd", "", "Invalid run command, cannot put further arguments after stdin");
						goto free_cmd;
					}

					HYSSDBG_G(stdin_file) = fopen(buf, "r");
					if (HYSSDBG_G(stdin_file) == NULL) {
						hyssdbg_error("stdin", "path=\"%s\"", "Could not open '%s' for reading from stdin", buf);
						goto free_cmd;
					}
					efree(buf);
					hyssdbg_register_file_handles();
					break;
				}

				if (argc >= 4 && argc == (argc & -argc)) {
					argv = erealloc(argv, (argc * 2 + 1) * sizeof(char *));
				}

				if (*p == '\'' || *p == '"') {
					sep = *(p++);
				}
				if (*p == '\\' && (p[1] == '<' || p[1] == '\'' || p[1] == '"')) {
					p++;
				}
				while (*p && *p != sep) {
					if (*p == '\\' && (p[1] == sep || p[1] == '\\' || (p[1] == '#' && sep == ' '))) {
						p++;
					}
					*(q++) = *(p++);
				}
				if (!*p && sep != ' ') {
					hyssdbg_error("cmd", "", "Invalid run command, unterminated escape sequence");
free_cmd:
					efree(buf);
					for (i = 0; i < argc; i++) {
						efree(argv[i]);
					}
					efree(argv);
					end[1] = last_byte;
					return SUCCESS;
				}

				*(q++) = 0;
				argv[++argc] = erealloc(buf, q - buf);

				if (*p) {
					do p++; while (*p == ' ');
				}
			}
			end[1] = last_byte;

			argv[0] = SG(request_info).argv[0];
			for (i = SG(request_info).argc; --i;) {
				efree(SG(request_info).argv[i]);
			}
			efree(SG(request_info).argv);
			SG(request_info).argv = erealloc(argv, ++argc * sizeof(char *));
			SG(request_info).argc = argc;

			hyss_build_argv(NULL, &PG(http_globals)[TRACK_VARS_SERVER]);
		}

		/* clean up from last execution */
		if (ex && (GEAR_CALL_INFO(ex) & GEAR_CALL_HAS_SYMBOL_TABLE)) {
			gear_hash_clean(ex->symbol_table);
		} else {
			gear_rebuild_symbol_table();
		}
		HYSSDBG_G(handled_exception) = NULL;

		/* clean seek state */
		HYSSDBG_G(flags) &= ~HYSSDBG_SEEK_MASK;
		gear_hash_clean(&HYSSDBG_G(seek));

		/* reset hit counters */
		hyssdbg_reset_breakpoints();

		gear_try {
			HYSSDBG_G(flags) ^= HYSSDBG_IS_INTERACTIVE;
			HYSSDBG_G(flags) |= HYSSDBG_IS_RUNNING;
			gear_execute(HYSSDBG_G(ops), &HYSSDBG_G(retval));
			HYSSDBG_G(flags) ^= HYSSDBG_IS_INTERACTIVE;
		} gear_catch {
			HYSSDBG_G(in_execution) = 0;

			if (!(HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING)) {
				restore = 0;
			} else {
				gear_bailout();
			}
		} gear_end_try();

		if (HYSSDBG_G(socket_fd) != -1) {
			close(HYSSDBG_G(socket_fd));
			HYSSDBG_G(socket_fd) = -1;
		}

		if (restore) {
			gear_exception_restore();
			gear_try {
				gear_try_exception_handler();
				HYSSDBG_G(in_execution) = 1;
			} gear_catch {
				HYSSDBG_G(in_execution) = 0;

				if (HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING) {
					gear_bailout();
				}
			} gear_end_try();

			if (EG(exception)) {
				hyssdbg_handle_exception();
			}
		}

		HYSSDBG_G(flags) &= ~HYSSDBG_IS_RUNNING;

		hyssdbg_clean(1, 0);
	} else {
		hyssdbg_error("inactive", "type=\"nocontext\"", "Nothing to execute!");
	}

out:
	HYSSDBG_FRAME(num) = 0;
	return SUCCESS;
} /* }}} */

int hyssdbg_output_ev_variable(char *name, size_t len, char *keyname, size_t keylen, HashTable *parent, zval *zv) /* {{{ */ {
	hyssdbg_notice("eval", "variable=\"%.*s\"", "Printing variable %.*s", (int) len, name);
	hyssdbg_xml("<eval %r>");
	gear_print_zval_r(zv, 0);
	hyssdbg_xml("</eval>");
	hyssdbg_out("\n");

	efree(name);
	efree(keyname);

	return SUCCESS;
}
/* }}} */

HYSSDBG_COMMAND(ev) /* {{{ */
{
	gear_bool stepping = ((HYSSDBG_G(flags) & HYSSDBG_IS_STEPPING) == HYSSDBG_IS_STEPPING);
	zval retval;

	gear_execute_data *original_execute_data = EG(current_execute_data);
	gear_vm_stack original_stack = EG(vm_stack);
	gear_object *ex = NULL;

	HYSSDBG_OUTPUT_BACKUP();

	original_stack->top = EG(vm_stack_top);

	if (HYSSDBG_G(flags) & HYSSDBG_IN_SIGNAL_HANDLER) {
		hyssdbg_try_access {
			hyssdbg_parse_variable(param->str, param->len, &EG(symbol_table), 0, hyssdbg_output_ev_variable, 0);
		} hyssdbg_catch_access {
			hyssdbg_error("signalsegv", "", "Could not fetch data, invalid data source");
		} hyssdbg_end_try_access();

		HYSSDBG_OUTPUT_BACKUP_RESTORE();
		return SUCCESS;
	}

	if (!(HYSSDBG_G(flags) & HYSSDBG_IS_STEPONEVAL)) {
		HYSSDBG_G(flags) &= ~HYSSDBG_IS_STEPPING;
	}

	/* disable stepping while eval() in progress */
	HYSSDBG_G(flags) |= HYSSDBG_IN_EVAL;
	gear_try {
		if (gear_eval_stringl(param->str, param->len, &retval, "eval()'d code") == SUCCESS) {
			if (EG(exception)) {
				ex = EG(exception);
				gear_exception_error(EG(exception), E_ERROR);
			} else {
				hyssdbg_xml("<eval %r>");
				if (HYSSDBG_G(flags) & HYSSDBG_WRITE_XML) {
					zval *zvp = &retval;
					hyssdbg_xml_var_dump(zvp);
				}
				gear_print_zval_r(&retval, 0);
				hyssdbg_xml("</eval>");
				hyssdbg_out("\n");
				zval_ptr_dtor(&retval);
			}
		}
	} gear_catch {
		HYSSDBG_G(unclean_eval) = 1;
		if (ex) {
			OBJ_RELEASE(ex);
		}
		EG(current_execute_data) = original_execute_data;
		EG(vm_stack_top) = original_stack->top;
		EG(vm_stack_end) = original_stack->end;
		EG(vm_stack) = original_stack;
		EG(exit_status) = 0;
	} gear_end_try();

	HYSSDBG_G(flags) &= ~HYSSDBG_IN_EVAL;

	/* switch stepping back on */
	if (stepping && !(HYSSDBG_G(flags) & HYSSDBG_IS_STEPONEVAL)) {
		HYSSDBG_G(flags) |= HYSSDBG_IS_STEPPING;
	}

	CG(unclean_shutdown) = 0;

	HYSSDBG_OUTPUT_BACKUP_RESTORE();

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(back) /* {{{ */
{
	if (!HYSSDBG_G(in_execution)) {
		hyssdbg_error("inactive", "type=\"noexec\"", "Not executing!");
		return SUCCESS;
	}

	if (!param) {
		hyssdbg_dump_backtrace(0);
	} else {
		hyssdbg_dump_backtrace(param->num);
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(generator) /* {{{ */
{
	int i;

	if (!HYSSDBG_G(in_execution)) {
		hyssdbg_error("inactive", "type=\"noexec\"", "Not executing!");
		return SUCCESS;
	}

	if (param) {
		i = param->num;
		gear_object **obj = EG(objects_store).object_buckets + i;
		if (i < EG(objects_store).top && *obj && IS_OBJ_VALID(*obj) && (*obj)->ce == gear_ce_generator) {
			gear_generator *gen = (gear_generator *) *obj;
			if (gen->execute_data) {
				if (gear_generator_get_current(gen)->flags & GEAR_GENERATOR_CURRENTLY_RUNNING) {
					hyssdbg_error("generator", "type=\"running\"", "Generator currently running");
				} else {
					hyssdbg_open_generator_frame(gen);
				}
			} else {
				hyssdbg_error("generator", "type=\"closed\"", "Generator already closed");
			}
		} else {
			hyssdbg_error("invalidarg", "", "Invalid object handle");
		}
	} else {
		for (i = 0; i < EG(objects_store).top; i++) {
			gear_object *obj = EG(objects_store).object_buckets[i];
			if (obj && IS_OBJ_VALID(obj) && obj->ce == gear_ce_generator) {
				gear_generator *gen = (gear_generator *) obj, *current = gear_generator_get_current(gen);
				if (gen->execute_data) {
					gear_string *s = hyssdbg_compile_stackframe(gen->execute_data);
					hyssdbg_out("#%d: %.*s", i, (int) ZSTR_LEN(s), ZSTR_VAL(s));
					gear_string_release(s);
					if (gen != current) {
						if (gen->node.parent != current) {
							hyssdbg_out(" with direct parent #%d and", gen->node.parent->std.handle);
						}
						hyssdbg_out(" executing #%d currently", current->std.handle);
					}
					hyssdbg_out("\n");
				}
			}
		}
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(print) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		return hyssdbg_do_print_stack(param);
	} else switch (param->type) {
		case STR_PARAM:
			return hyssdbg_do_print_func(param);
		case METHOD_PARAM:
			return hyssdbg_do_print_method(param);
		default:
			hyssdbg_error("print", "type=\"invalidarg\"", "Invalid arguments to print, expected nothing, function name or method name");
			return SUCCESS;
	}
} /* }}} */

HYSSDBG_COMMAND(info) /* {{{ */
{
	hyssdbg_out("Execution Context Information\n\n");
	hyssdbg_xml("<printinfo %r>");
#ifdef HAVE_HYSSDBG_READLINE
# ifdef HAVE_LIBREADLINE
	 hyssdbg_writeln("info", "readline=\"yes\"", "Readline   yes");
# else
	 hyssdbg_writeln("info", "readline=\"no\"", "Readline   no");
# endif
# ifdef HAVE_LIBEDIT
	 hyssdbg_writeln("info", "libedit=\"yes\"", "Libedit    yes");
# else
	 hyssdbg_writeln("info", "libedit=\"no\"", "Libedit    no");
# endif
#else
     hyssdbg_writeln("info", "readline=\"unavailable\"", "Readline   unavailable");
#endif

	hyssdbg_writeln("info", "context=\"%s\"", "Exec       %s", HYSSDBG_G(exec) ? HYSSDBG_G(exec) : "none");
	hyssdbg_writeln("info", "compiled=\"%s\"", "Compiled   %s", HYSSDBG_G(ops) ? "yes" : "no");
	hyssdbg_writeln("info", "stepping=\"%s\"", "Stepping   %s", (HYSSDBG_G(flags) & HYSSDBG_IS_STEPPING) ? "on" : "off");
	hyssdbg_writeln("info", "quiet=\"%s\"", "Quietness  %s", (HYSSDBG_G(flags) & HYSSDBG_IS_QUIET) ? "on" : "off");
	hyssdbg_writeln("info", "oplog=\"%s\"", "Oplog      %s", HYSSDBG_G(oplog) ? "on" : "off");

	if (HYSSDBG_G(ops)) {
		hyssdbg_writeln("info", "ops=\"%d\"", "Opcodes    %d", HYSSDBG_G(ops)->last);
		hyssdbg_writeln("info", "vars=\"%d\"", "Variables  %d", HYSSDBG_G(ops)->last_var ? HYSSDBG_G(ops)->last_var - 1 : 0);
	}

	hyssdbg_writeln("info", "executing=\"%d\"", "Executing  %s", HYSSDBG_G(in_execution) ? "yes" : "no");
	if (HYSSDBG_G(in_execution)) {
		hyssdbg_writeln("info", "vmret=\"%d\"", "VM Return  %d", HYSSDBG_G(vmret));
	}

	hyssdbg_writeln("info", "classes=\"%d\"", "Classes    %d", gear_hash_num_elements(EG(class_table)));
	hyssdbg_writeln("info", "functions=\"%d\"", "Functions  %d", gear_hash_num_elements(EG(function_table)));
	hyssdbg_writeln("info", "constants=\"%d\"", "Constants  %d", gear_hash_num_elements(EG(gear_constants)));
	hyssdbg_writeln("info", "includes=\"%d\"", "Included   %d", gear_hash_num_elements(&EG(included_files)));
	hyssdbg_xml("</printinfo>");

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(set) /* {{{ */
{
	hyssdbg_error("set", "type=\"toofewargs\" expected=\"1\"", "No set command selected!");

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(break) /* {{{ */
{
	if (!param) {
		if (HYSSDBG_G(exec)) {
			hyssdbg_set_breakpoint_file(
				gear_get_executed_filename(),
				strlen(gear_get_executed_filename()),
				gear_get_executed_lineno());
		} else {
			hyssdbg_error("inactive", "type=\"noexec\"", "Execution context not set!");
		}
	} else switch (param->type) {
		case ADDR_PARAM:
			hyssdbg_set_breakpoint_opline(param->addr);
			break;
		case NUMERIC_PARAM:
			if (HYSSDBG_G(exec)) {
				hyssdbg_set_breakpoint_file(hyssdbg_current_file(), strlen(hyssdbg_current_file()), param->num);
			} else {
				hyssdbg_error("inactive", "type=\"noexec\"", "Execution context not set!");
			}
			break;
		case METHOD_PARAM:
			hyssdbg_set_breakpoint_method(param->method.class, param->method.name);
			break;
		case NUMERIC_METHOD_PARAM:
			hyssdbg_set_breakpoint_method_opline(param->method.class, param->method.name, param->num);
			break;
		case NUMERIC_FUNCTION_PARAM:
			hyssdbg_set_breakpoint_function_opline(param->str, param->num);
			break;
		case FILE_PARAM:
			hyssdbg_set_breakpoint_file(param->file.name, 0, param->file.line);
			break;
		case NUMERIC_FILE_PARAM:
			hyssdbg_set_breakpoint_file_opline(param->file.name, param->file.line);
			break;
		case COND_PARAM:
			hyssdbg_set_breakpoint_expression(param->str, param->len);
			break;
		case STR_PARAM:
			hyssdbg_set_breakpoint_symbol(param->str, param->len);
			break;
		case OP_PARAM:
			hyssdbg_set_breakpoint_opcode(param->str, param->len);
			break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(sh) /* {{{ */
{
	FILE *fd = NULL;
	if ((fd=VCWD_POPEN((char*)param->str, "w"))) {
		/* TODO: do something perhaps ?? do we want input ?? */
		pclose(fd);
	} else {
		hyssdbg_error("sh", "type=\"failure\" smd=\"%s\"", "Failed to execute %s", param->str);
	}

	return SUCCESS;
} /* }}} */

static int add_capi_info(gear_capi_entry *cAPI) /* {{{ */ {
	hyssdbg_write("cAPI", "name=\"%s\"", "%s\n", cAPI->name);
	return 0;
}
/* }}} */

static int add_gearext_info(gear_extension *ext) /* {{{ */ {
	hyssdbg_write("extension", "name=\"%s\"", "%s\n", ext->name);
	return 0;
}
/* }}} */

#ifdef HAVE_LIBDL
HYSSDBG_API const char *hyssdbg_load_capi_or_extension(char **path, char **name) /* {{{ */ {
	DL_HANDLE handle;
	char *extension_dir;

	extension_dir = ICS_STR("extension_dir");

	if (strchr(*path, '/') != NULL || strchr(*path, DEFAULT_SLASH) != NULL) {
		/* path is fine */
	} else if (extension_dir && extension_dir[0]) {
		char *libpath;
		int extension_dir_len = strlen(extension_dir);
		if (IS_SLASH(extension_dir[extension_dir_len-1])) {
			spprintf(&libpath, 0, "%s%s", extension_dir, *path); /* SAFE */
		} else {
			spprintf(&libpath, 0, "%s%c%s", extension_dir, DEFAULT_SLASH, *path); /* SAFE */
		}
		efree(*path);
		*path = libpath;
	} else {
		hyssdbg_error("dl", "type=\"relpath\"", "Not a full path given or extension_dir ics setting is not set");

		return NULL;
	}

	handle = DL_LOAD(*path);

	if (!handle) {
#ifdef HYSS_WIN32
		char *err = GET_DL_ERROR();
		if (err && err[0]) {
			hyssdbg_error("dl", "type=\"unknown\"", "%s", err);
			LocalFree(err);
		} else {
			hyssdbg_error("dl", "type=\"unknown\"", "Unknown reason");
		}
#else
		hyssdbg_error("dl", "type=\"unknown\"", "%s", GET_DL_ERROR());
#endif
		return NULL;
	}

#if GEAR_EXTENSIONS_SUPPORT
	do {
		gear_extension *new_extension;
		gear_extension_version_info *extension_version_info;

		extension_version_info = (gear_extension_version_info *) DL_FETCH_SYMBOL(handle, "extension_version_info");
		if (!extension_version_info) {
			extension_version_info = (gear_extension_version_info *) DL_FETCH_SYMBOL(handle, "_extension_version_info");
		}
		new_extension = (gear_extension *) DL_FETCH_SYMBOL(handle, "gear_extension_entry");
		if (!new_extension) {
			new_extension = (gear_extension *) DL_FETCH_SYMBOL(handle, "_gear_extension_entry");
		}
		if (!extension_version_info || !new_extension) {
			break;
		}
		if (extension_version_info->gear_extension_api_no != GEAR_EXTENSION_API_NO &&(!new_extension->api_no_check || new_extension->api_no_check(GEAR_EXTENSION_API_NO) != SUCCESS)) {
			hyssdbg_error("dl", "type=\"wrongapi\" extension=\"%s\" apineeded=\"%d\" apiinstalled=\"%d\"", "%s requires Gear Engine API version %d, which does not match the installed Gear Engine API version %d", new_extension->name, extension_version_info->gear_extension_api_no, GEAR_EXTENSION_API_NO);

			goto quit;
		} else if (strcmp(GEAR_EXTENSION_BUILD_ID, extension_version_info->build_id) && (!new_extension->build_id_check || new_extension->build_id_check(GEAR_EXTENSION_BUILD_ID) != SUCCESS)) {
			hyssdbg_error("dl", "type=\"wrongbuild\" extension=\"%s\" buildneeded=\"%s\" buildinstalled=\"%s\"", "%s was built with configuration %s, whereas running engine is %s", new_extension->name, extension_version_info->build_id, GEAR_EXTENSION_BUILD_ID);

			goto quit;
		}

		*name = new_extension->name;

		gear_register_extension(new_extension, handle);

		if (new_extension->startup) {
			if (new_extension->startup(new_extension) != SUCCESS) {
				hyssdbg_error("dl", "type=\"startupfailure\" extension=\"%s\"", "Unable to startup Gear extension %s", new_extension->name);

				goto quit;
			}
			gear_append_version_info(new_extension);
		}

		return "Gear extension";
	} while (0);
#endif

	do {
		gear_capi_entry *capi_entry;
		gear_capi_entry *(*get_capi)(void);

		get_capi = (gear_capi_entry *(*)(void)) DL_FETCH_SYMBOL(handle, "get_capi");
		if (!get_capi) {
			get_capi = (gear_capi_entry *(*)(void)) DL_FETCH_SYMBOL(handle, "_get_capi");
		}

		if (!get_capi) {
			break;
		}

		capi_entry = get_capi();
		*name = (char *) capi_entry->name;

		if (strcmp(GEAR_EXTENSION_BUILD_ID, capi_entry->build_id)) {
			hyssdbg_error("dl", "type=\"wrongbuild\" cAPI=\"%s\" buildneeded=\"%s\" buildinstalled=\"%s\"",  "%s was built with configuration %s, whereas running engine is %s", capi_entry->name, capi_entry->build_id, GEAR_EXTENSION_BUILD_ID);

			goto quit;
		}

		capi_entry->type = CAPI_PERSISTENT;
		capi_entry->capi_number = gear_next_free_capi();
		capi_entry->handle = handle;

		if ((capi_entry = gear_register_capi_ex(capi_entry)) == NULL) {
			hyssdbg_error("dl", "type=\"registerfailure\" cAPI=\"%s\"", "Unable to register cAPI %s", capi_entry->name);

			goto quit;
		}

		if (gear_startup_capi_ex(capi_entry) == FAILURE) {
			hyssdbg_error("dl", "type=\"startupfailure\" cAPI=\"%s\"", "Unable to startup cAPI %s", capi_entry->name);

			goto quit;
		}

		if (capi_entry->request_startup_func) {
			if (capi_entry->request_startup_func(CAPI_PERSISTENT, capi_entry->capi_number) == FAILURE) {
				hyssdbg_error("dl", "type=\"initfailure\" cAPI=\"%s\"", "Unable to initialize cAPI %s", capi_entry->name);

				goto quit;
			}
		}

		return "cAPI";
	} while (0);

	hyssdbg_error("dl", "type=\"nohyssso\"", "This shared object is nor a Gear extension nor a cAPI");

quit:
	DL_UNLOAD(handle);
	return NULL;
}
/* }}} */
#endif

HYSSDBG_COMMAND(dl) /* {{{ */
{
	const char *type;
	char *name, *path;

	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_notice("dl", "extensiontype=\"Gear extension\"", "Gear extensions");
		gear_llist_apply(&gear_extensions, (llist_apply_func_t) add_gearext_info);
		hyssdbg_out("\n");
		hyssdbg_notice("dl", "extensiontype=\"cAPI\"", "cAPIs");
		gear_hash_apply(&capi_registry, (apply_func_t) add_capi_info);
	} else switch (param->type) {
		case STR_PARAM:
#ifdef HAVE_LIBDL
			path = estrndup(param->str, param->len);

			hyssdbg_activate_err_buf(1);
			if ((type = hyssdbg_load_capi_or_extension(&path, &name)) == NULL) {
				hyssdbg_error("dl", "path=\"%s\" %b", "Could not load %s, not found or invalid gear extension / cAPI: %b", path);
				efree(name);
			} else {
				hyssdbg_notice("dl", "extensiontype=\"%s\" name=\"%s\" path=\"%s\"", "Successfully loaded the %s %s at path %s", type, name, path);
			}
			hyssdbg_activate_err_buf(0);
			hyssdbg_free_err_buf();
			efree(path);
#else
			hyssdbg_error("dl", "type=\"unsupported\" path=\"%.*s\"", "Cannot dynamically load %.*s - dynamic cAPIs are not supported", (int) param->len, param->str);
#endif
			break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(source) /* {{{ */
{
	gear_stat_t sb;

	if (VCWD_STAT(param->str, &sb) != -1) {
		hyssdbg_try_file_init(param->str, param->len, 0);
	} else {
		hyssdbg_error("source", "type=\"notfound\" file=\"%s\"", "Failed to stat %s, file does not exist", param->str);
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(export) /* {{{ */
{
	FILE *handle = VCWD_FOPEN(param->str, "w+");

	if (handle) {
		hyssdbg_export_breakpoints(handle);
		fclose(handle);
	} else {
		hyssdbg_error("export", "type=\"openfailure\" file=\"%s\"", "Failed to open or create %s, check path and permissions", param->str);
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(register) /* {{{ */
{
	gear_function *function;
	char *lcname = gear_str_tolower_dup(param->str, param->len);
	size_t lcname_len = strlen(lcname);

	if (!gear_hash_str_exists(&HYSSDBG_G(registered), lcname, lcname_len)) {
		if ((function = gear_hash_str_find_ptr(EG(function_table), lcname, lcname_len))) {
			gear_hash_str_update_ptr(&HYSSDBG_G(registered), lcname, lcname_len, function);
			function_add_ref(function);

			hyssdbg_notice("register", "function=\"%s\"", "Registered %s", lcname);
		} else {
			hyssdbg_error("register", "type=\"notfound\" function=\"%s\"", "The requested function (%s) could not be found", param->str);
		}
	} else {
		hyssdbg_error("register", "type=\"inuse\" function=\"%s\"", "The requested name (%s) is already in use", lcname);
	}

	efree(lcname);
	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(quit) /* {{{ */
{
	HYSSDBG_G(flags) |= HYSSDBG_IS_QUITTING;
	HYSSDBG_G(flags) &= ~HYSSDBG_IS_CLEANING;

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(clean) /* {{{ */
{
	if (HYSSDBG_G(in_execution)) {
		if (hyssdbg_ask_user_permission("Do you really want to clean your current environment?") == FAILURE) {
			return SUCCESS;
		}
	}

	hyssdbg_out("Cleaning Execution Environment\n");
	hyssdbg_xml("<cleaninfo %r>");

	hyssdbg_writeln("clean", "classes=\"%d\"", "Classes    %d", gear_hash_num_elements(EG(class_table)));
	hyssdbg_writeln("clean", "functions=\"%d\"", "Functions  %d", gear_hash_num_elements(EG(function_table)));
	hyssdbg_writeln("clean", "constants=\"%d\"", "Constants  %d", gear_hash_num_elements(EG(gear_constants)));
	hyssdbg_writeln("clean", "includes=\"%d\"", "Includes   %d", gear_hash_num_elements(&EG(included_files)));

	hyssdbg_clean(1, 0);

	hyssdbg_xml("</cleaninfo>");

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(clear) /* {{{ */
{
	hyssdbg_out("Clearing Breakpoints\n");
	hyssdbg_xml("<clearinfo %r>");

	hyssdbg_writeln("clear", "files=\"%d\"", "File              %d", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE]));
	hyssdbg_writeln("clear", "functions=\"%d\"", "Functions         %d", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM]));
	hyssdbg_writeln("clear", "methods=\"%d\"", "Methods           %d", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD]));
	hyssdbg_writeln("clear", "oplines=\"%d\"", "Oplines           %d", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]));
	hyssdbg_writeln("clear", "fileoplines=\"%d\"", "File oplines      %d", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE]));
	hyssdbg_writeln("clear", "functionoplines=\"%d\"", "Function oplines  %d", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE]));
	hyssdbg_writeln("clear", "methodoplines=\"%d\"", "Method oplines    %d", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE]));
	hyssdbg_writeln("clear", "eval=\"%d\"", "Conditionals      %d", gear_hash_num_elements(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND]));

	hyssdbg_clear_breakpoints();

	hyssdbg_xml("</clearinfo>");

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(list) /* {{{ */
{
	if (!param) {
		return HYSSDBG_LIST_HANDLER(lines)(HYSSDBG_COMMAND_ARGS);
	} else switch (param->type) {
		case NUMERIC_PARAM:
			return HYSSDBG_LIST_HANDLER(lines)(HYSSDBG_COMMAND_ARGS);

		case FILE_PARAM:
			return HYSSDBG_LIST_HANDLER(lines)(HYSSDBG_COMMAND_ARGS);

		case STR_PARAM:
			hyssdbg_list_function_byname(param->str, param->len);
			break;

		case METHOD_PARAM:
			return HYSSDBG_LIST_HANDLER(method)(HYSSDBG_COMMAND_ARGS);

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_COMMAND(watch) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_list_watchpoints();
	} else switch (param->type) {
		case STR_PARAM:
			hyssdbg_create_var_watchpoint(param->str, param->len);
			break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

int hyssdbg_interactive(gear_bool allow_async_unsafe, char *input) /* {{{ */
{
	int ret = SUCCESS;
	hyssdbg_param_t stack;

	HYSSDBG_G(flags) |= HYSSDBG_IS_INTERACTIVE;

	while (ret == SUCCESS || ret == FAILURE) {
		if (HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING) {
			gear_bailout();
		}

		if (!input && !(input = hyssdbg_read_input(NULL))) {
			break;
		}


		hyssdbg_init_param(&stack, STACK_PARAM);

		if (hyssdbg_do_parse(&stack, input) <= 0) {
			hyssdbg_activate_err_buf(1);

#ifdef HYSS_WIN32
#define PARA ((hyssdbg_param_t *)stack.next)->type
			if (HYSSDBG_G(flags) & HYSSDBG_IS_REMOTE && (RUN_PARAM == PARA || EVAL_PARAM == PARA)) {
				sigio_watcher_start();
			}
#endif
			switch (ret = hyssdbg_stack_execute(&stack, allow_async_unsafe)) {
				case FAILURE:
					if (!(HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING)) {
						if (!allow_async_unsafe || hyssdbg_call_register(&stack) == FAILURE) {
							hyssdbg_output_err_buf(NULL, "%b", "%b");
						}
					}
				break;

				case HYSSDBG_LEAVE:
				case HYSSDBG_FINISH:
				case HYSSDBG_UNTIL:
				case HYSSDBG_NEXT: {
					hyssdbg_activate_err_buf(0);
					hyssdbg_free_err_buf();
					if (!HYSSDBG_G(in_execution) && !(HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING)) {
						hyssdbg_error("command", "type=\"noexec\"", "Not running");
					}
					break;
				}
			}

			hyssdbg_activate_err_buf(0);
			hyssdbg_free_err_buf();
#ifdef HYSS_WIN32
			if (HYSSDBG_G(flags) & HYSSDBG_IS_REMOTE && (RUN_PARAM == PARA || EVAL_PARAM == PARA)) {
				sigio_watcher_stop();
			}
#undef PARA
#endif
		}

		hyssdbg_stack_free(&stack);
		hyssdbg_destroy_input(&input);
		HYSSDBG_G(req_id) = 0;
		input = NULL;
	}

	if (input) {
		hyssdbg_stack_free(&stack);
		hyssdbg_destroy_input(&input);
		HYSSDBG_G(req_id) = 0;
	}

	if (HYSSDBG_G(in_execution)) {
		hyssdbg_restore_frame();
	}

	HYSSDBG_G(flags) &= ~HYSSDBG_IS_INTERACTIVE;

	hyssdbg_print_changed_zvals();

	return ret;
} /* }}} */

/* code may behave weirdly if EG(exception) is set; thus backup it */
#define DO_INTERACTIVE(allow_async_unsafe) do { \
	const gear_op *backup_opline; \
	const gear_op *before_ex; \
	if (exception) { \
		if (EG(current_execute_data) && EG(current_execute_data)->func && GEAR_USER_CODE(EG(current_execute_data)->func->common.type)) { \
			backup_opline = EG(current_execute_data)->opline; \
		} \
		before_ex = EG(opline_before_exception); \
		GC_ADDREF(exception); \
		gear_clear_exception(); \
	} \
	if (!(HYSSDBG_G(flags) & HYSSDBG_IN_EVAL)) { \
		const char *file_char = gear_get_executed_filename(); \
		gear_string *file = gear_string_init(file_char, strlen(file_char), 0); \
		hyssdbg_list_file(file, 3, gear_get_executed_lineno()-1, gear_get_executed_lineno()); \
		efree(file); \
	} \
	\
	switch (hyssdbg_interactive(allow_async_unsafe, NULL)) { \
		zval zv; \
		case HYSSDBG_LEAVE: \
		case HYSSDBG_FINISH: \
		case HYSSDBG_UNTIL: \
		case HYSSDBG_NEXT: \
			if (exception) { \
				if (EG(current_execute_data) && EG(current_execute_data)->func && GEAR_USER_CODE(EG(current_execute_data)->func->common.type) \
				 && (backup_opline->opcode == GEAR_HANDLE_EXCEPTION || backup_opline->opcode == GEAR_CATCH)) { \
					EG(current_execute_data)->opline = backup_opline; \
					EG(exception) = exception; \
				} else { \
					Z_OBJ(zv) = exception; \
					gear_throw_exception_internal(&zv); \
				} \
				EG(opline_before_exception) = before_ex; \
			} \
			/* fallthrough */ \
		default: \
			goto next; \
	} \
} while (0)

void hyssdbg_execute_ex(gear_execute_data *execute_data) /* {{{ */
{
	gear_bool original_in_execution = HYSSDBG_G(in_execution);

	if ((HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING) && !(HYSSDBG_G(flags) & HYSSDBG_IS_RUNNING)) {
		gear_bailout();
	}

	HYSSDBG_G(in_execution) = 1;

	while (1) {
		gear_object *exception = EG(exception);

		if ((HYSSDBG_G(flags) & HYSSDBG_BP_RESOLVE_MASK)) {
			/* resolve nth opline breakpoints */
			hyssdbg_resolve_op_array_breaks(&execute_data->func->op_array);
		}

#ifdef GEAR_WIN32
		if (EG(timed_out)) {
			gear_timeout(0);
		}
#endif

		if (HYSSDBG_G(flags) & HYSSDBG_PREVENT_INTERACTIVE) {
			hyssdbg_print_opline_ex(execute_data, 0);
			goto next;
		}

		/* check for uncaught exceptions */
		if (exception && HYSSDBG_G(handled_exception) != exception && !(HYSSDBG_G(flags) & HYSSDBG_IN_EVAL)) {
			gear_execute_data *prev_ex = execute_data;
			zval zv, rv;
			gear_string *file, *msg;
			gear_long line;

			do {
				prev_ex = gear_generator_check_placeholder_frame(prev_ex);
				/* assuming that no internal functions will silently swallow exceptions ... */
				if (!prev_ex->func || !GEAR_USER_CODE(prev_ex->func->common.type)) {
					continue;
				}

				if (hyssdbg_check_caught_ex(prev_ex, exception)) {
					goto ex_is_caught;
				}
			} while ((prev_ex = prev_ex->prev_execute_data));

			HYSSDBG_G(handled_exception) = exception;

			ZVAL_OBJ(&zv, exception);
			file = zval_get_string(gear_read_property(gear_get_exception_base(&zv), &zv, GEAR_STRL("file"), 1, &rv));
			line = zval_get_long(gear_read_property(gear_get_exception_base(&zv), &zv, GEAR_STRL("line"), 1, &rv));
			msg = zval_get_string(gear_read_property(gear_get_exception_base(&zv), &zv, GEAR_STRL("message"), 1, &rv));

			hyssdbg_error("exception",
				"name=\"%s\" file=\"%s\" line=\"" GEAR_LONG_FMT "\"",
				"Uncaught %s in %s on line " GEAR_LONG_FMT ": %.*s",
				ZSTR_VAL(exception->ce->name), ZSTR_VAL(file), line,
				ZSTR_LEN(msg) < 80 ? (int) ZSTR_LEN(msg) : 80, ZSTR_VAL(msg));
			gear_string_release(msg);
			gear_string_release(file);

			DO_INTERACTIVE(1);
		}
ex_is_caught:

		/* allow conditional breakpoints and initialization to access the vm uninterrupted */
		if (HYSSDBG_G(flags) & (HYSSDBG_IN_COND_BP | HYSSDBG_IS_INITIALIZING)) {
			/* skip possible breakpoints */
			goto next;
		}

		/* not while in conditionals */
		hyssdbg_print_opline_ex(execute_data, 0);

		/* perform seek operation */
		if ((HYSSDBG_G(flags) & HYSSDBG_SEEK_MASK) && !(HYSSDBG_G(flags) & HYSSDBG_IN_EVAL)) {
			/* current address */
			gear_ulong address = (gear_ulong) execute_data->opline;

			if (HYSSDBG_G(seek_ex) != execute_data) {
				if (HYSSDBG_G(flags) & HYSSDBG_IS_STEPPING) {
					goto stepping;
				}
				goto next;
			}

#define INDEX_EXISTS_CHECK (gear_hash_index_exists(&HYSSDBG_G(seek), address) || (exception && hyssdbg_check_caught_ex(execute_data, exception) == 0))

			/* run to next line */
			if (HYSSDBG_G(flags) & HYSSDBG_IN_UNTIL) {
				if (INDEX_EXISTS_CHECK) {
					HYSSDBG_G(flags) &= ~HYSSDBG_IN_UNTIL;
					gear_hash_clean(&HYSSDBG_G(seek));
				} else {
					/* skip possible breakpoints */
					goto next;
				}
			}

			/* run to finish */
			if (HYSSDBG_G(flags) & HYSSDBG_IN_FINISH) {
				if (INDEX_EXISTS_CHECK) {
					HYSSDBG_G(flags) &= ~HYSSDBG_IN_FINISH;
					gear_hash_clean(&HYSSDBG_G(seek));
				}
				/* skip possible breakpoints */
				goto next;
			}

			/* break for leave */
			if (HYSSDBG_G(flags) & HYSSDBG_IN_LEAVE) {
				if (INDEX_EXISTS_CHECK) {
					HYSSDBG_G(flags) &= ~HYSSDBG_IN_LEAVE;
					gear_hash_clean(&HYSSDBG_G(seek));
					hyssdbg_notice("breakpoint", "id=\"leave\" file=\"%s\" line=\"%u\"", "Breaking for leave at %s:%u",
						gear_get_executed_filename(),
						gear_get_executed_lineno()
					);
					DO_INTERACTIVE(1);
				} else {
					/* skip possible breakpoints */
					goto next;
				}
			}
		}

		if (HYSSDBG_G(flags) & HYSSDBG_IS_STEPPING && (HYSSDBG_G(flags) & HYSSDBG_STEP_OPCODE || execute_data->opline->lineno != HYSSDBG_G(last_line))) {
stepping:
			HYSSDBG_G(flags) &= ~HYSSDBG_IS_STEPPING;
			DO_INTERACTIVE(1);
		}

		/* check if some watchpoint was hit */
		{
			if (hyssdbg_print_changed_zvals() == SUCCESS) {
				DO_INTERACTIVE(1);
			}
		}

		/* search for breakpoints */
		{
			hyssdbg_breakbase_t *brake;

			if ((HYSSDBG_G(flags) & HYSSDBG_BP_MASK)
			    && (brake = hyssdbg_find_breakpoint(execute_data))
			    && (brake->type != HYSSDBG_BREAK_FILE || execute_data->opline->lineno != HYSSDBG_G(last_line))) {
				hyssdbg_hit_breakpoint(brake, 1);
				DO_INTERACTIVE(1);
			}
		}

		if (HYSSDBG_G(flags) & HYSSDBG_IS_SIGNALED) {
			HYSSDBG_G(flags) &= ~HYSSDBG_IS_SIGNALED;

			hyssdbg_out("\n");
			hyssdbg_notice("signal", "type=\"SIGINT\"", "Program received signal SIGINT");
			DO_INTERACTIVE(1);
		}

next:

		HYSSDBG_G(last_line) = execute_data->opline->lineno;

		/* stupid hack to make gear_do_fcall_common_helper return GEAR_VM_ENTER() instead of recursively calling gear_execute() and eventually segfaulting */
		if ((execute_data->opline->opcode == GEAR_DO_FCALL ||
		     execute_data->opline->opcode == GEAR_DO_UCALL ||
		     execute_data->opline->opcode == GEAR_DO_FCALL_BY_NAME) &&
		     execute_data->call->func->type == GEAR_USER_FUNCTION) {
			gear_execute_ex = execute_ex;
		}
		HYSSDBG_G(vmret) = gear_vm_call_opcode_handler(execute_data);
		gear_execute_ex = hyssdbg_execute_ex;

		if (HYSSDBG_G(vmret) != 0) {
			if (HYSSDBG_G(vmret) < 0) {
				HYSSDBG_G(in_execution) = original_in_execution;
				return;
			} else {
				execute_data = EG(current_execute_data);
			}
		}
	}
	gear_error_noreturn(E_ERROR, "Arrived at end of main loop which shouldn't happen");
} /* }}} */

/* only if *not* interactive and while executing */
void hyssdbg_force_interruption(void) /* {{{ */ {
	gear_object *exception = EG(exception);
	gear_execute_data *data = EG(current_execute_data); /* should be always readable if not NULL */

	HYSSDBG_G(flags) |= HYSSDBG_IN_SIGNAL_HANDLER;

	if (data) {
		if (data->func) {
			if (GEAR_USER_CODE(data->func->type)) {
				hyssdbg_notice("hardinterrupt", "opline=\"%p\" num=\"%lu\" file=\"%s\" line=\"%u\"", "Current opline: %p (op #%lu) in %s:%u", data->opline, (data->opline - data->func->op_array.opcodes) / sizeof(data->opline), data->func->op_array.filename->val, data->opline->lineno);
			} else if (data->func->internal_function.function_name) {
				hyssdbg_notice("hardinterrupt", "func=\"%s\"", "Current opline: in internal function %s", data->func->internal_function.function_name->val);
			} else {
				hyssdbg_notice("hardinterrupt", "", "Current opline: executing internal code");
			}
		} else {
			hyssdbg_notice("hardinterrupt", "opline=\"%p\"", "Current opline: %p (op_array information unavailable)", data->opline);
		}
	} else {
		hyssdbg_notice("hardinterrupt", "", "No information available about executing context");
	}

	DO_INTERACTIVE(0);

next:
	HYSSDBG_G(flags) &= ~HYSSDBG_IN_SIGNAL_HANDLER;

	if (HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING) {
		gear_bailout();
	}
}
/* }}} */

HYSSDBG_COMMAND(eol) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_notice("eol", "argument required", "argument required");
	} else switch (param->type) {
		case STR_PARAM:
			if (FAILURE == hyssdbg_eol_global_update(param->str)) {
				hyssdbg_notice("eol", "unknown EOL name '%s', give crlf, lf, cr", "unknown EOL name '%s', give  crlf, lf, cr", param->str);
			}
			break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */
