/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_HELP_H
#define HYSSDBG_HELP_H

#include "hypbc.h"
#include "hyssdbg.h"
#include "hyssdbg_cmd.h"

#define HYSSDBG_HELP(name) HYSSDBG_COMMAND(help_##name)

/**
 * Helper Forward Declarations
 */
HYSSDBG_HELP(aliases);

extern const hyssdbg_command_t hyssdbg_help_commands[];

#define hyssdbg_help_header() \
	hyssdbg_notice("version", "version=\"%s\"", "Welcome to hyssdbg, the interactive HYSS debugger, v%s", HYSSDBG_VERSION);
#define hyssdbg_help_footer() \
	hyssdbg_notice("issues", "url=\"%s\"", "Please report bugs to <%s>", HYSSDBG_ISSUES);

typedef struct _hyssdbg_help_text_t {
	char *key;
	char *text;
} hyssdbg_help_text_t;

extern hyssdbg_help_text_t hyssdbg_help_text[];

extern void hyssdbg_do_help_cmd(char *type);
#endif /* HYSSDBG_HELP_H */
