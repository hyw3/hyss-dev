/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_H
#define HYSSDBG_H

#ifdef HYSS_WIN32
# define HYSSDBG_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
# define HYSSDBG_API __attribute__ ((visibility("default")))
#else
# define HYSSDBG_API
#endif

#ifndef HYSS_WIN32
#	include <stdint.h>
#	include <stddef.h>
#else
#	include "main/hyss_stdint.h"
#endif
#include "hyss.h"
#include "hyss_globals.h"
#include "hyss_variables.h"
#include "hyss_getopt.h"
#include "gear_builtin_functions.h"
#include "gear_extensions.h"
#include "gear_capis.h"
#include "gear_globals.h"
#include "gear_ics_scanner.h"
#include "gear_stream.h"
#include "gear_signal.h"
#if !defined(_WIN32) && !defined(GEAR_SIGNALS) && defined(HAVE_SIGNAL_H)
#	include <signal.h>
#elif defined(HYSS_WIN32)
#	include "win32/signal.h"
#endif
#include "SAPI.h"
#include <fcntl.h>
#include <sys/types.h>
#if defined(_WIN32) && !defined(__MINGW32__)
#	include <windows.h>
#	include "config.w32.h"
#	undef  strcasecmp
#	undef  strncasecmp
#	define strcasecmp _stricmp
#	define strncasecmp _strnicmp
#else
#	include "hyss_config.h"
#endif
#ifndef O_BINARY
#	define O_BINARY 0
#endif
#include "hyss_main.h"

#ifdef ZTS
# include "hypbc.h"
#endif

#undef gear_hash_str_add
#ifdef HYSS_WIN32
#define gear_hash_str_add(...) \
	gear_hash_str_add(__VA_ARGS__)
#else
#define gear_hash_str_add_tmp(ht, key, len, pData) \
	gear_hash_str_add(ht, key, len, pData)
#define gear_hash_str_add(...) gear_hash_str_add_tmp(__VA_ARGS__)
#endif

#ifdef HAVE_HYSSDBG_READLINE
# ifdef HAVE_LIBREADLINE
#	 include <readline/readline.h>
#	 include <readline/history.h>
# endif
# ifdef HAVE_LIBEDIT
#	 include <editline/readline.h>
# endif
#endif

/* {{{ remote console headers */
#ifndef _WIN32
#	include <sys/socket.h>
#	include <sys/un.h>
#	include <sys/select.h>
#	include <sys/types.h>
#	include <netdb.h>
#endif /* }}} */

/* {{{ strings */
#define HYSSDBG_NAME "hyssdbg"
#define HYSSDBG_AUTHORS "Felipe Pena, Joe Watkins and Bob Weinand" /* Ordered by last name */
#define HYSSDBG_ISSUES "http://hyss.hyang.org/bugs/report.hyss"
#define HYSSDBG_VERSION "0.5.0"
#define HYSSDBG_INIT_FILENAME ".hyssdbginit"
#define HYSSDBG_DEFAULT_PROMPT "prompt>"
/* }}} */

/* Hey, apple. One shouldn't define *functions* from the standard C library as marcos. */
#ifdef memcpy
#define memcpy_tmp(...) memcpy(__VA_ARGS__)
#undef memcpy
#define memcpy(...) memcpy_tmp(__VA_ARGS__)
#endif

#if !defined(HYSSDBG_WEBDATA_TRANSFER_H) && !defined(HYSSDBG_WEBHELPER_H)

#ifdef ZTS
# define HYSSDBG_G(v) PBCG(hyssdbg_globals_id, gear_hyssdbg_globals *, v)
#else
# define HYSSDBG_G(v) (hyssdbg_globals.v)
#endif

#include "hyssdbg_sigsafe.h"
#include "hyssdbg_out.h"
#include "hyssdbg_lexer.h"
#include "hyssdbg_cmd.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_btree.h"
#include "hyssdbg_watch.h"
#include "hyssdbg_bp.h"
#include "hyssdbg_opcode.h"
#ifdef HYSS_WIN32
# include "hyssdbg_sigio_win32.h"
#endif

int hyssdbg_do_parse(hyssdbg_param_t *stack, char *input);

#define HYSSDBG_NEXT   2
#define HYSSDBG_UNTIL  3
#define HYSSDBG_FINISH 4
#define HYSSDBG_LEAVE  5

/*
 BEGIN: DO NOT CHANGE DO NOT CHANGE DO NOT CHANGE
*/

/* {{{ flags */
#define HYSSDBG_HAS_FILE_BP            (1ULL<<1)
#define HYSSDBG_HAS_PENDING_FILE_BP    (1ULL<<2)
#define HYSSDBG_HAS_SYM_BP             (1ULL<<3)
#define HYSSDBG_HAS_OPLINE_BP          (1ULL<<4)
#define HYSSDBG_HAS_METHOD_BP          (1ULL<<5)
#define HYSSDBG_HAS_COND_BP            (1ULL<<6)
#define HYSSDBG_HAS_OPCODE_BP          (1ULL<<7)
#define HYSSDBG_HAS_FUNCTION_OPLINE_BP (1ULL<<8)
#define HYSSDBG_HAS_METHOD_OPLINE_BP   (1ULL<<9)
#define HYSSDBG_HAS_FILE_OPLINE_BP     (1ULL<<10) /* }}} */

/*
 END: DO NOT CHANGE DO NOT CHANGE DO NOT CHANGE
*/

#define HYSSDBG_IN_COND_BP             (1ULL<<11)
#define HYSSDBG_IN_EVAL                (1ULL<<12)

#define HYSSDBG_IS_STEPPING            (1ULL<<13)
#define HYSSDBG_STEP_OPCODE            (1ULL<<14)
#define HYSSDBG_IS_QUIET               (1ULL<<15)
#define HYSSDBG_IS_QUITTING            (1ULL<<16)
#define HYSSDBG_IS_COLOURED            (1ULL<<17)
#define HYSSDBG_IS_CLEANING            (1ULL<<18)
#define HYSSDBG_IS_RUNNING             (1ULL<<19)

#define HYSSDBG_IN_UNTIL               (1ULL<<20)
#define HYSSDBG_IN_FINISH              (1ULL<<21)
#define HYSSDBG_IN_LEAVE               (1ULL<<22)

#define HYSSDBG_IS_REGISTERED          (1ULL<<23)
#define HYSSDBG_IS_STEPONEVAL          (1ULL<<24)
#define HYSSDBG_IS_INITIALIZING        (1ULL<<25)
#define HYSSDBG_IS_SIGNALED            (1ULL<<26)
#define HYSSDBG_IS_INTERACTIVE         (1ULL<<27)
#define HYSSDBG_PREVENT_INTERACTIVE    (1ULL<<28)
#define HYSSDBG_IS_BP_ENABLED          (1ULL<<29)
#define HYSSDBG_IS_REMOTE              (1ULL<<30)
#define HYSSDBG_IS_DISCONNECTED        (1ULL<<31)
#define HYSSDBG_WRITE_XML              (1ULL<<32)

#define HYSSDBG_SHOW_REFCOUNTS         (1ULL<<33)

#define HYSSDBG_IN_SIGNAL_HANDLER      (1ULL<<34)

#define HYSSDBG_DISCARD_OUTPUT         (1ULL<<35)

#define HYSSDBG_HAS_PAGINATION         (1ULL<<36)

#define HYSSDBG_SEEK_MASK              (HYSSDBG_IN_UNTIL | HYSSDBG_IN_FINISH | HYSSDBG_IN_LEAVE)
#define HYSSDBG_BP_RESOLVE_MASK	      (HYSSDBG_HAS_FUNCTION_OPLINE_BP | HYSSDBG_HAS_METHOD_OPLINE_BP | HYSSDBG_HAS_FILE_OPLINE_BP)
#define HYSSDBG_BP_MASK                (HYSSDBG_HAS_FILE_BP | HYSSDBG_HAS_SYM_BP | HYSSDBG_HAS_METHOD_BP | HYSSDBG_HAS_OPLINE_BP | HYSSDBG_HAS_COND_BP | HYSSDBG_HAS_OPCODE_BP | HYSSDBG_HAS_FUNCTION_OPLINE_BP | HYSSDBG_HAS_METHOD_OPLINE_BP | HYSSDBG_HAS_FILE_OPLINE_BP)
#define HYSSDBG_IS_STOPPING            (HYSSDBG_IS_QUITTING | HYSSDBG_IS_CLEANING)

#define HYSSDBG_PRESERVE_FLAGS_MASK    (HYSSDBG_SHOW_REFCOUNTS | HYSSDBG_IS_STEPONEVAL | HYSSDBG_IS_BP_ENABLED | HYSSDBG_STEP_OPCODE | HYSSDBG_IS_QUIET | HYSSDBG_IS_COLOURED | HYSSDBG_IS_REMOTE | HYSSDBG_WRITE_XML | HYSSDBG_IS_DISCONNECTED | HYSSDBG_HAS_PAGINATION)

#ifndef _WIN32
#	define HYSSDBG_DEFAULT_FLAGS (HYSSDBG_IS_QUIET | HYSSDBG_IS_COLOURED | HYSSDBG_IS_BP_ENABLED | HYSSDBG_HAS_PAGINATION)
#else
#	define HYSSDBG_DEFAULT_FLAGS (HYSSDBG_IS_QUIET | HYSSDBG_IS_BP_ENABLED | HYSSDBG_HAS_PAGINATION)
#endif /* }}} */

/* {{{ output descriptors */
#define HYSSDBG_STDIN 			0
#define HYSSDBG_STDOUT			1
#define HYSSDBG_STDERR			2
#define HYSSDBG_IO_FDS 			3 /* }}} */

#define hyssdbg_try_access \
	{                                                            \
		JMP_BUF *__orig_bailout = HYSSDBG_G(sigsegv_bailout); \
		JMP_BUF __bailout;                                   \
                                                                     \
		HYSSDBG_G(sigsegv_bailout) = &__bailout;              \
		if (SETJMP(__bailout) == 0) {
#define hyssdbg_catch_access \
		} else {                                             \
			HYSSDBG_G(sigsegv_bailout) = __orig_bailout;
#define hyssdbg_end_try_access() \
		}                                                    \
			HYSSDBG_G(sigsegv_bailout) = __orig_bailout;  \
	}


void hyssdbg_register_file_handles(void);

/* {{{ structs */
GEAR_BEGIN_CAPI_GLOBALS(hyssdbg)
	HashTable bp[HYSSDBG_BREAK_TABLES];           /* break points */
	HashTable registered;                        /* registered */
	HashTable seek;                              /* seek oplines */
	gear_execute_data *seek_ex;                  /* call frame of oplines to seek to */
	gear_object *handled_exception;              /* last handled exception (prevent multiple handling of same exception) */
	hyssdbg_frame_t frame;                        /* frame */
	uint32_t last_line;                          /* last executed line */

	char *cur_command;                           /* current command */
	hyssdbg_lexer_data lexer;                     /* lexer data */
	hyssdbg_param_t *parser_stack;                /* param stack during lexer / parser phase */

#ifndef _WIN32
	struct sigaction old_sigsegv_signal;         /* segv signal handler */
#endif
	hyssdbg_btree watchpoint_tree;                /* tree with watchpoints */
	hyssdbg_btree watch_HashTables;               /* tree with original dtors of watchpoints */
	HashTable watch_elements;                    /* user defined watch elements */
	HashTable watch_collisions;                  /* collision table to check if multiple watches share the same recursive watchpoint */
	HashTable watch_recreation;                  /* watch elements pending recreation of their respective watchpoints */
	HashTable watch_free;                        /* pointers to watch for being freed */
	HashTable *watchlist_mem;                    /* triggered watchpoints */
	HashTable *watchlist_mem_backup;             /* triggered watchpoints backup table while iterating over it */
	gear_bool watchpoint_hit;                    /* a watchpoint was hit */
	void (*original_free_function)(void *);      /* the original AG(mm_heap)->_free function */
	hyssdbg_watch_element *watch_tmp;             /* temporary pointer for a watch element */

	char *exec;                                  /* file to execute */
	size_t exec_len;                             /* size of exec */
	gear_op_array *ops;                 	     /* op_array */
	zval retval;                                 /* return value */
	int bp_count;                                /* breakpoint count */
	int vmret;                                   /* return from last opcode handler execution */
	gear_bool in_execution;                      /* in execution? */
	gear_bool unclean_eval;                      /* do not check for memory leaks when we needed to bail out during eval */

	gear_op_array *(*compile_file)(gear_file_handle *file_handle, int type);
	gear_op_array *(*init_compile_file)(gear_file_handle *file_handle, int type);
	gear_op_array *(*compile_string)(zval *source_string, char *filename);
	HashTable file_sources;

	FILE *oplog;                                 /* opline log */
	gear_arena *oplog_arena;                     /* arena for storing oplog */
	hyssdbg_oplog_list *oplog_list;               /* list of oplog starts */
	hyssdbg_oplog_entry *oplog_cur;               /* current oplog entry */

	struct {
		FILE *ptr;
		int fd;
	} io[HYSSDBG_IO_FDS];                         /* io */
	int eol;                                     /* type of line ending to use */
	size_t (*hyss_stdiop_write)(hyss_stream *, const char *, size_t);
	int in_script_xml;                           /* in <stream> output mode */
	struct {
		gear_bool active;
		int type;
		int fd;
		char *tag;
		char *msg;
		int msglen;
		char *xml;
		int xmllen;
	} err_buf;                                   /* error buffer */
	gear_ulong req_id;                           /* "request id" to keep track of commands */

	char *prompt[2];                             /* prompt */
	const hyssdbg_color_t *colors[HYSSDBG_COLORS]; /* colors */
	char *buffer;                                /* buffer */
	gear_bool last_was_newline;                  /* check if we don't need to output a newline upon next hyssdbg_error or hyssdbg_notice */

	FILE *stdin_file;                            /* FILE pointer to stdin source file */
	const hyss_stream_wrapper *orig_url_wrap_hyss;

	char input_buffer[HYSSDBG_MAX_CMD];           /* stdin input buffer */
	int input_buflen;                            /* length of stdin input buffer */
	hyssdbg_signal_safe_mem sigsafe_mem;          /* memory to use in async safe environment (only once!) */

	JMP_BUF *sigsegv_bailout;                    /* bailout address for accesibility probing */

	uint64_t flags;                              /* hyssdbg flags */

	char *socket_path;                           /* hyssdbg.path ics setting */
	char *sapi_name_ptr;                         /* store sapi name to free it if necessary to not leak memory */
	int socket_fd;                               /* file descriptor to socket (wait command) (-1 if unused) */
	int socket_server_fd;                        /* file descriptor to master socket (wait command) (-1 if unused) */
#ifdef HYSS_WIN32
	HANDLE sigio_watcher_thread;                 /* sigio watcher thread handle */
	struct win32_sigio_watcher_data swd;
#endif
	long lines;                                  /* max number of lines to display */
GEAR_END_CAPI_GLOBALS(hyssdbg) /* }}} */

#endif

#endif /* HYSSDBG_H */
