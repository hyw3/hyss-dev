/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg.h"
#include "hyssdbg_prompt.h"
#include "hyssdbg_bp.h"
#include "hyssdbg_break.h"
#include "hyssdbg_list.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_set.h"
#include "hyssdbg_io.h"
#include "gear_alloc.h"
#include "hyssdbg_eol.h"
#include "hyssdbg_print.h"
#include "hyssdbg_help.h"

#include "extslib/standard/basic_functions.h"

/* {{{ remote console headers */
#ifndef _WIN32
#	include <sys/socket.h>
#	include <sys/select.h>
#	include <sys/time.h>
#	include <sys/types.h>
#	if HAVE_POLL_H
#		include <poll.h>
#	elif HAVE_SYS_POLL_H
#		include <sys/poll.h>
#	endif
#	include <netinet/in.h>
#	include <unistd.h>
#	include <arpa/inet.h>
#endif /* }}} */

#if defined(HYSS_WIN32) && defined(HAVE_OPENSSL)
# include "openssl/applink.c"
#endif

#if defined(HYSS_WIN32) && defined(ZTS)
GEAR_PBCLS_CACHE_DEFINE()
#endif

GEAR_DECLARE_CAPI_GLOBALS(hyssdbg);
int hyssdbg_startup_run = 0;

static HYSS_ICS_MH(OnUpdateEol)
{
	if (!new_value) {
		return FAILURE;
	}

	return hyssdbg_eol_global_update(ZSTR_VAL(new_value));
}

HYSS_ICS_BEGIN()
	STD_HYSS_ICS_ENTRY("hyssdbg.path", "", HYSS_ICS_SYSTEM | HYSS_ICS_PERDIR, OnUpdateString, socket_path, gear_hyssdbg_globals, hyssdbg_globals)
	STD_HYSS_ICS_ENTRY("hyssdbg.eol", "2", HYSS_ICS_ALL, OnUpdateEol, socket_path, gear_hyssdbg_globals, hyssdbg_globals)
HYSS_ICS_END()

static gear_bool hyssdbg_booted = 0;
static gear_bool hyssdbg_fully_started = 0;
gear_bool use_mm_wrappers = 1;

static void hyss_hyssdbg_destroy_bp_file(zval *brake) /* {{{ */
{
	gear_hash_destroy(Z_ARRVAL_P(brake));
	efree(Z_ARRVAL_P(brake));
} /* }}} */

static void hyss_hyssdbg_destroy_bp_symbol(zval *brake) /* {{{ */
{
	efree((char *) ((hyssdbg_breaksymbol_t *) Z_PTR_P(brake))->symbol);
	efree(Z_PTR_P(brake));
} /* }}} */

static void hyss_hyssdbg_destroy_bp_opcode(zval *brake) /* {{{ */
{
	efree((char *) ((hyssdbg_breakop_t *) Z_PTR_P(brake))->name);
	efree(Z_PTR_P(brake));
} /* }}} */

static void hyss_hyssdbg_destroy_bp_opline(zval *brake) /* {{{ */
{
	efree(Z_PTR_P(brake));
} /* }}} */

static void hyss_hyssdbg_destroy_bp_methods(zval *brake) /* {{{ */
{
	gear_hash_destroy(Z_ARRVAL_P(brake));
	efree(Z_ARRVAL_P(brake));
} /* }}} */

static void hyss_hyssdbg_destroy_bp_condition(zval *data) /* {{{ */
{
	hyssdbg_breakcond_t *brake = (hyssdbg_breakcond_t *) Z_PTR_P(data);

	if (brake->ops) {
		destroy_op_array(brake->ops);
		efree(brake->ops);
	}
	efree((char*) brake->code);
	efree(brake);
} /* }}} */

static void hyss_hyssdbg_destroy_registered(zval *data) /* {{{ */
{
	gear_function_dtor(data);
} /* }}} */

static void hyss_hyssdbg_destroy_file_source(zval *data) /* {{{ */
{
	hyssdbg_file_source *source = (hyssdbg_file_source *) Z_PTR_P(data);
	destroy_op_array(&source->op_array);
	if (source->buf) {
		efree(source->buf);
	}
	efree(source);
} /* }}} */

static inline void hyss_hyssdbg_globals_ctor(gear_hyssdbg_globals *pg) /* {{{ */
{
	pg->prompt[0] = NULL;
	pg->prompt[1] = NULL;

	pg->colors[0] = NULL;
	pg->colors[1] = NULL;
	pg->colors[2] = NULL;

	pg->lines = hyssdbg_get_terminal_height();
	pg->exec = NULL;
	pg->exec_len = 0;
	pg->buffer = NULL;
	pg->last_was_newline = 1;
	pg->ops = NULL;
	pg->vmret = 0;
	pg->in_execution = 0;
	pg->bp_count = 0;
	pg->flags = HYSSDBG_DEFAULT_FLAGS;
	pg->oplog = NULL;
	memset(pg->io, 0, sizeof(pg->io));
	pg->frame.num = 0;
	pg->sapi_name_ptr = NULL;
	pg->socket_fd = -1;
	pg->socket_server_fd = -1;
	pg->unclean_eval = 0;

	pg->req_id = 0;
	pg->err_buf.active = 0;
	pg->err_buf.type = 0;

	pg->input_buflen = 0;
	pg->sigsafe_mem.mem = NULL;
	pg->sigsegv_bailout = NULL;

	pg->oplog_list = NULL;

#ifdef HYSS_WIN32
	pg->sigio_watcher_thread = INVALID_HANDLE_VALUE;
	memset(&pg->swd, 0, sizeof(struct win32_sigio_watcher_data));
#endif

	pg->eol = HYSSDBG_EOL_LF;

	pg->stdin_file = NULL;

	pg->cur_command = NULL;
} /* }}} */

static HYSS_MINIT_FUNCTION(hyssdbg) /* {{{ */
{
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE], 8, NULL, hyss_hyssdbg_destroy_bp_file, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING], 8, NULL, hyss_hyssdbg_destroy_bp_file, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM], 8, NULL, hyss_hyssdbg_destroy_bp_symbol, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE], 8, NULL, hyss_hyssdbg_destroy_bp_methods, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE], 8, NULL, hyss_hyssdbg_destroy_bp_methods, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE], 8, NULL, hyss_hyssdbg_destroy_bp_methods, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE], 8, NULL, hyss_hyssdbg_destroy_bp_opline, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPCODE], 8, NULL, hyss_hyssdbg_destroy_bp_opcode, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD], 8, NULL, hyss_hyssdbg_destroy_bp_methods, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND], 8, NULL, hyss_hyssdbg_destroy_bp_condition, 0);
	gear_hash_init(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP], 8, NULL, NULL, 0);

	gear_hash_init(&HYSSDBG_G(seek), 8, NULL, NULL, 0);
	gear_hash_init(&HYSSDBG_G(registered), 8, NULL, hyss_hyssdbg_destroy_registered, 0);

	gear_hash_init(&HYSSDBG_G(file_sources), 0, NULL, hyss_hyssdbg_destroy_file_source, 0);
	hyssdbg_setup_watchpoints();

	REGISTER_ICS_ENTRIES();

	gear_execute_ex = hyssdbg_execute_ex;

	REGISTER_STRINGL_CONSTANT("HYSSDBG_VERSION", HYSSDBG_VERSION, sizeof(HYSSDBG_VERSION)-1, CONST_CS|CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("HYSSDBG_COLOR_PROMPT", HYSSDBG_COLOR_PROMPT, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSSDBG_COLOR_NOTICE", HYSSDBG_COLOR_NOTICE, CONST_CS|CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("HYSSDBG_COLOR_ERROR",  HYSSDBG_COLOR_ERROR, CONST_CS|CONST_PERSISTENT);

	return SUCCESS;
} /* }}} */

static HYSS_MSHUTDOWN_FUNCTION(hyssdbg) /* {{{ */
{
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPCODE]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND]);
	gear_hash_destroy(&HYSSDBG_G(bp)[HYSSDBG_BREAK_MAP]);
	gear_hash_destroy(&HYSSDBG_G(seek));
	gear_hash_destroy(&HYSSDBG_G(registered));
	hyssdbg_destroy_watchpoints();

	if (!(HYSSDBG_G(flags) & HYSSDBG_IS_QUITTING)) {
		hyssdbg_notice("stop", "type=\"normal\"", "Script ended normally");
	}

	/* hack to restore mm_heap->use_custom_heap in order to receive memory leak info */
	if (use_mm_wrappers) {
		/* ASSUMING that mm_heap->use_custom_heap is the first element of the struct ... */
		*(int *) gear_mm_get_heap() = 0;
	}

	if (HYSSDBG_G(buffer)) {
		free(HYSSDBG_G(buffer));
		HYSSDBG_G(buffer) = NULL;
	}

	if (HYSSDBG_G(exec)) {
		efree(HYSSDBG_G(exec));
		HYSSDBG_G(exec) = NULL;
	}

	if (HYSSDBG_G(oplog)) {
		fclose(HYSSDBG_G(oplog));
		HYSSDBG_G(oplog) = NULL;
	}

	if (HYSSDBG_G(ops)) {
		destroy_op_array(HYSSDBG_G(ops));
		efree(HYSSDBG_G(ops));
		HYSSDBG_G(ops) = NULL;
	}

	if (HYSSDBG_G(oplog_list)) {
		hyssdbg_oplog_list *cur = HYSSDBG_G(oplog_list);
		do {
			hyssdbg_oplog_list *prev = cur->prev;
			efree(cur);
			cur = prev;
		} while (cur != NULL);

		gear_arena_destroy(HYSSDBG_G(oplog_arena));
		HYSSDBG_G(oplog_list) = NULL;
	}

	fflush(stdout);
	if (SG(request_info).argv0) {
		free(SG(request_info).argv0);
		SG(request_info).argv0 = NULL;
	}

	return SUCCESS;
}
/* }}} */

static HYSS_RINIT_FUNCTION(hyssdbg) /* {{{ */
{
	/* deactivate symbol table caching to have these properly destroyed upon stack leaving (especially important for watchpoints) */
	EG(symtable_cache_limit) = EG(symtable_cache) - 1;

	return SUCCESS;
} /* }}} */

static HYSS_RSHUTDOWN_FUNCTION(hyssdbg) /* {{{ */
{
	if (HYSSDBG_G(stdin_file)) {
		fclose(HYSSDBG_G(stdin_file));
		HYSSDBG_G(stdin_file) = NULL;
	}

	return SUCCESS;
} /* }}} */

/* {{{ proto mixed hyssdbg_exec(string context)
	Attempt to set the execution context for hyssdbg
	If the execution context was set previously it is returned
	If the execution context was not set previously boolean true is returned
	If the request to set the context fails, boolean false is returned, and an E_WARNING raised */
static HYSS_FUNCTION(hyssdbg_exec)
{
	gear_string *exec;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "S", &exec) == FAILURE) {
		return;
	}

	{
		gear_stat_t sb;
		gear_bool result = 1;

		if (VCWD_STAT(ZSTR_VAL(exec), &sb) != FAILURE) {
			if (sb.st_mode & (S_IFREG|S_IFLNK)) {
				if (HYSSDBG_G(exec)) {
					ZVAL_STRINGL(return_value, HYSSDBG_G(exec), HYSSDBG_G(exec_len));
					efree(HYSSDBG_G(exec));
					result = 0;
				}

				HYSSDBG_G(exec) = estrndup(ZSTR_VAL(exec), ZSTR_LEN(exec));
				HYSSDBG_G(exec_len) = ZSTR_LEN(exec);

				if (result) {
					ZVAL_TRUE(return_value);
				}
			} else {
				gear_error(E_WARNING, "Failed to set execution context (%s), not a regular file or symlink", ZSTR_VAL(exec));
				ZVAL_FALSE(return_value);
			}
		} else {
			gear_error(E_WARNING, "Failed to set execution context (%s) the file does not exist", ZSTR_VAL(exec));

			ZVAL_FALSE(return_value);
		}
	}
} /* }}} */

/* {{{ proto void hyssdbg_break()
    instructs hyssdbg to insert a breakpoint at the next opcode */
static HYSS_FUNCTION(hyssdbg_break_next)
{
	gear_execute_data *ex = EG(current_execute_data);

	while (ex && ex->func && !GEAR_USER_CODE(ex->func->type)) {
		ex = ex->prev_execute_data;
	}

	if (gear_parse_parameters_none() == FAILURE || !ex) {
		return;
	}

	hyssdbg_set_breakpoint_opline_ex((hyssdbg_opline_ptr_t) ex->opline + 1);
} /* }}} */

/* {{{ proto void hyssdbg_break_file(string file, int line) */
static HYSS_FUNCTION(hyssdbg_break_file)
{
	char *file;
	size_t flen;
	gear_long line;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "sl", &file, &flen, &line) == FAILURE) {
		return;
	}

	hyssdbg_set_breakpoint_file(file, 0, line);
} /* }}} */

/* {{{ proto void hyssdbg_break_method(string class, string method) */
static HYSS_FUNCTION(hyssdbg_break_method)
{
	char *class = NULL, *method = NULL;
	size_t clen = 0, mlen = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ss", &class, &clen, &method, &mlen) == FAILURE) {
		return;
	}

	hyssdbg_set_breakpoint_method(class, method);
} /* }}} */

/* {{{ proto void hyssdbg_break_function(string function) */
static HYSS_FUNCTION(hyssdbg_break_function)
{
	char    *function = NULL;
	size_t   function_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &function, &function_len) == FAILURE) {
		return;
	}

	hyssdbg_set_breakpoint_symbol(function, function_len);
} /* }}} */

/* {{{ proto void hyssdbg_clear(void)
   instructs hyssdbg to clear breakpoints */
static HYSS_FUNCTION(hyssdbg_clear)
{
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_PENDING]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_SYM]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FUNCTION_OPLINE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD_OPLINE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_FILE_OPLINE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_OPLINE]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_METHOD]);
	gear_hash_clean(&HYSSDBG_G(bp)[HYSSDBG_BREAK_COND]);
} /* }}} */

/* {{{ proto void hyssdbg_color(int element, string color) */
static HYSS_FUNCTION(hyssdbg_color)
{
	gear_long element;
	char *color;
	size_t color_len;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "ls", &element, &color, &color_len) == FAILURE) {
		return;
	}

	switch (element) {
		case HYSSDBG_COLOR_NOTICE:
		case HYSSDBG_COLOR_ERROR:
		case HYSSDBG_COLOR_PROMPT:
			hyssdbg_set_color_ex(element, color, color_len);
		break;

		default: gear_error(E_ERROR, "hyssdbg detected an incorrect color constant");
	}
} /* }}} */

/* {{{ proto void hyssdbg_prompt(string prompt) */
static HYSS_FUNCTION(hyssdbg_prompt)
{
	char *prompt = NULL;
	size_t prompt_len = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &prompt, &prompt_len) == FAILURE) {
		return;
	}

	hyssdbg_set_prompt(prompt);
} /* }}} */

/* {{{ proto void hyssdbg_start_oplog() */
static HYSS_FUNCTION(hyssdbg_start_oplog)
{
	hyssdbg_oplog_list *prev;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	prev = HYSSDBG_G(oplog_list);

	if (!prev) {
		HYSSDBG_G(oplog_arena) = gear_arena_create(64 * 1024);

		HYSSDBG_G(oplog_cur) = ((hyssdbg_oplog_entry *) gear_arena_alloc(&HYSSDBG_G(oplog_arena), sizeof(hyssdbg_oplog_entry))) + 1;
		HYSSDBG_G(oplog_cur)->next = NULL;
	}

	HYSSDBG_G(oplog_list) = emalloc(sizeof(hyssdbg_oplog_list));
	HYSSDBG_G(oplog_list)->prev = prev;
	HYSSDBG_G(oplog_list)->start = HYSSDBG_G(oplog_cur);
}

static gear_always_inline gear_bool hyssdbg_is_ignored_opcode(gear_uchar opcode) {
	return
	    opcode == GEAR_NOP || opcode == GEAR_OP_DATA || opcode == GEAR_FE_FREE || opcode == GEAR_FREE || opcode == GEAR_ASSERT_CHECK || opcode == GEAR_VERIFY_RETURN_TYPE
	 || opcode == GEAR_DECLARE_CONST || opcode == GEAR_DECLARE_CLASS || opcode == GEAR_DECLARE_INHERITED_CLASS || opcode == GEAR_DECLARE_FUNCTION
	 || opcode == GEAR_DECLARE_INHERITED_CLASS_DELAYED || opcode == GEAR_VERIFY_ABSTRACT_CLASS || opcode == GEAR_ADD_TRAIT || opcode == GEAR_BIND_TRAITS
	 || opcode == GEAR_DECLARE_ANON_CLASS || opcode == GEAR_DECLARE_ANON_INHERITED_CLASS || opcode == GEAR_FAST_RET || opcode == GEAR_TICKS
	 || opcode == GEAR_EXT_STMT || opcode == GEAR_EXT_FCALL_BEGIN || opcode == GEAR_EXT_FCALL_END || opcode == GEAR_EXT_NOP || opcode == GEAR_BIND_GLOBAL
	;
}

static void hyssdbg_oplog_fill_executable(gear_op_array *op_array, HashTable *insert_ht, gear_bool by_opcode) {
	/* ignore RECV_* opcodes */
	gear_op *cur = op_array->opcodes + op_array->num_args + !!(op_array->fn_flags & GEAR_ACC_VARIADIC);
	gear_op *end = op_array->opcodes + op_array->last;

	gear_long insert_idx;
	zval zero;
	ZVAL_LONG(&zero, 0);

	/* ignore autogenerated return (well, not too precise with finally branches, but that's okay) */
	if (op_array->last >= 1 && (((end - 1)->opcode == GEAR_RETURN || (end - 1)->opcode == GEAR_RETURN_BY_REF || (end - 1)->opcode == GEAR_GENERATOR_RETURN)
	 && ((op_array->last > 1 && ((end - 2)->opcode == GEAR_RETURN || (end - 2)->opcode == GEAR_RETURN_BY_REF || (end - 2)->opcode == GEAR_GENERATOR_RETURN || (end - 2)->opcode == GEAR_THROW))
	  || op_array->function_name == NULL || (end - 1)->extended_value == -1))) {
		end--;
	}

	for (; cur < end; cur++) {
		gear_uchar opcode = cur->opcode;
		if (hyssdbg_is_ignored_opcode(opcode)) {
			continue;
		}

		if (by_opcode) {
			insert_idx = cur - op_array->opcodes;
		} else {
			insert_idx = cur->lineno;
		}

		if (opcode == GEAR_NEW && cur[1].opcode == GEAR_DO_FCALL) {
			cur++;
		}

		gear_hash_index_update(insert_ht, insert_idx, &zero);
	}
}

static inline HashTable* hyssdbg_add_empty_array(HashTable *ht, gear_string *name) {
	zval *ht_zv = gear_hash_find(ht, name);
	if (!ht_zv) {
		zval zv;
		array_init(&zv);
		ht_zv = gear_hash_add_new(ht, name, &zv);
	}
	return Z_ARR_P(ht_zv);
}

/* {{{ proto void hyssdbg_get_executable() */
static HYSS_FUNCTION(hyssdbg_get_executable)
{
	HashTable *options = NULL;
	zval *option_buffer;
	gear_bool by_function = 0;
	gear_bool by_opcode = 0;
	HashTable *insert_ht;

	gear_function *func;
	gear_class_entry *ce;
	gear_string *name;
	HashTable *files = &HYSSDBG_G(file_sources);
	HashTable files_tmp;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|H", &options) == FAILURE) {
		return;
	}

	if (options && (option_buffer = gear_hash_str_find(options, GEAR_STRL("functions")))) {
		by_function = gear_is_true(option_buffer);
	}

	if (options && (option_buffer = gear_hash_str_find(options, GEAR_STRL("opcodes")))) {
		if (by_function) {
			by_opcode = gear_is_true(option_buffer);
		}
	}

	if (options && (option_buffer = gear_hash_str_find(options, GEAR_STRL("files")))) {
		ZVAL_DEREF(option_buffer);
		if (Z_TYPE_P(option_buffer) == IS_ARRAY && gear_hash_num_elements(Z_ARR_P(option_buffer)) > 0) {
			zval *filename;

			files = &files_tmp;
			gear_hash_init(files, 0, NULL, NULL, 0);

			GEAR_HASH_FOREACH_VAL(Z_ARR_P(option_buffer), filename) {
				gear_hash_add_empty_element(files, zval_get_string(filename));
			} GEAR_HASH_FOREACH_END();
		} else {
			GC_ADDREF(files);
		}
	} else {
		GC_ADDREF(files);
	}

	array_init(return_value);

	GEAR_HASH_FOREACH_STR_KEY_PTR(EG(function_table), name, func) {
		if (func->type == GEAR_USER_FUNCTION) {
			if (gear_hash_exists(files, func->op_array.filename)) {
				insert_ht = hyssdbg_add_empty_array(Z_ARR_P(return_value), func->op_array.filename);

				if (by_function) {
					insert_ht = hyssdbg_add_empty_array(insert_ht, name);
				}

				hyssdbg_oplog_fill_executable(&func->op_array, insert_ht, by_opcode);
			}
		}
	} GEAR_HASH_FOREACH_END();

	GEAR_HASH_FOREACH_STR_KEY_PTR(EG(class_table), name, ce) {
		if (ce->type == GEAR_USER_CLASS) {
			if (gear_hash_exists(files, ce->info.user.filename)) {
				GEAR_HASH_FOREACH_PTR(&ce->function_table, func) {
					if (func->type == GEAR_USER_FUNCTION && gear_hash_exists(files, func->op_array.filename)) {
						insert_ht = hyssdbg_add_empty_array(Z_ARR_P(return_value), func->op_array.filename);

						if (by_function) {
							gear_string *fn_name = strpprintf(ZSTR_LEN(name) + ZSTR_LEN(func->op_array.function_name) + 2, "%.*s::%.*s", (int) ZSTR_LEN(name), ZSTR_VAL(name), (int) ZSTR_LEN(func->op_array.function_name), ZSTR_VAL(func->op_array.function_name));
							insert_ht = hyssdbg_add_empty_array(insert_ht, fn_name);
							gear_string_release(fn_name);
						}

						hyssdbg_oplog_fill_executable(&func->op_array, insert_ht, by_opcode);
					}
				} GEAR_HASH_FOREACH_END();
			}
		}
	} GEAR_HASH_FOREACH_END();

	GEAR_HASH_FOREACH_STR_KEY(files, name) {
		hyssdbg_file_source *source = gear_hash_find_ptr(&HYSSDBG_G(file_sources), name);
		if (source) {
			hyssdbg_oplog_fill_executable(
				&source->op_array,
				hyssdbg_add_empty_array(Z_ARR_P(return_value), source->op_array.filename),
				by_opcode);
		}
	} GEAR_HASH_FOREACH_END();

	if (!GC_DELREF(files)) {
		gear_hash_destroy(files);
	}
}

/* {{{ proto void hyssdbg_end_oplog() */
static HYSS_FUNCTION(hyssdbg_end_oplog)
{
	hyssdbg_oplog_entry *cur;
	hyssdbg_oplog_list *prev;

	HashTable *options = NULL;
	zval *option_buffer;
	gear_bool by_function = 0;
	gear_bool by_opcode = 0;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "|H", &options) == FAILURE) {
		return;
	}

	if (!HYSSDBG_G(oplog_list)) {
		gear_error(E_WARNING, "Can not end an oplog without starting it");
		return;
	}

	cur = HYSSDBG_G(oplog_list)->start;
	prev = HYSSDBG_G(oplog_list)->prev;

	efree(HYSSDBG_G(oplog_list));
	HYSSDBG_G(oplog_list) = prev;

	if (options && (option_buffer = gear_hash_str_find(options, GEAR_STRL("functions")))) {
		by_function = gear_is_true(option_buffer);
	}

	if (options && (option_buffer = gear_hash_str_find(options, GEAR_STRL("opcodes")))) {
		if (by_function) {
			by_opcode = gear_is_true(option_buffer);
		}
	}

	array_init(return_value);

	{
		gear_string *last_file = NULL;
		HashTable *file_ht;
		gear_string *last_function = (void *)~(uintptr_t)0;
		gear_class_entry *last_scope = NULL;

		HashTable *insert_ht;
		gear_long insert_idx;

		do {
			zval zero;
			ZVAL_LONG(&zero, 0);

			if (cur->filename != last_file) {
				last_file = cur->filename;
				file_ht = insert_ht = hyssdbg_add_empty_array(Z_ARR_P(return_value), last_file);
			}

			if (by_function) {
				if (cur->function_name == NULL) {
					if (last_function != NULL) {
						insert_ht = file_ht;
					}
					last_function = NULL;
				} else if (cur->function_name != last_function || cur->scope != last_scope) {
					gear_string *fn_name;
					last_function = cur->function_name;
					last_scope = cur->scope;
					if (last_scope == NULL) {
						fn_name = gear_string_copy(last_function);
					} else {
						fn_name = strpprintf(ZSTR_LEN(last_function) + ZSTR_LEN(last_scope->name) + 2, "%.*s::%.*s", (int) ZSTR_LEN(last_scope->name), ZSTR_VAL(last_scope->name), (int) ZSTR_LEN(last_function), ZSTR_VAL(last_function));
					}
					insert_ht = hyssdbg_add_empty_array(Z_ARR_P(return_value), fn_name);
					gear_string_release(fn_name);
				}
			}

			if (by_opcode) {
				insert_idx = cur->op - cur->opcodes;
			} else {
				if (hyssdbg_is_ignored_opcode(cur->op->opcode)) {
					continue;
				}

				insert_idx = cur->op->lineno;
			}

			{
				zval *num = gear_hash_index_find(insert_ht, insert_idx);
				if (!num) {
					num = gear_hash_index_add_new(insert_ht, insert_idx, &zero);
				}
				Z_LVAL_P(num)++;
			}

		} while ((cur = cur->next));
	}

	if (!prev) {
		gear_arena_destroy(HYSSDBG_G(oplog_arena));
	}
}

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_break_next_arginfo, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_break_file_arginfo, 0, 0, 2)
	GEAR_ARG_INFO(0, file)
	GEAR_ARG_INFO(0, line)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_break_method_arginfo, 0, 0, 2)
	GEAR_ARG_INFO(0, class)
	GEAR_ARG_INFO(0, method)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_break_function_arginfo, 0, 0, 1)
	GEAR_ARG_INFO(0, function)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_color_arginfo, 0, 0, 2)
	GEAR_ARG_INFO(0, element)
	GEAR_ARG_INFO(0, color)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_prompt_arginfo, 0, 0, 1)
	GEAR_ARG_INFO(0, string)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_exec_arginfo, 0, 0, 1)
	GEAR_ARG_INFO(0, context)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_clear_arginfo, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_start_oplog_arginfo, 0, 0, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_end_oplog_arginfo, 0, 0, 0)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(hyssdbg_get_executable_arginfo, 0, 0, 0)
	GEAR_ARG_INFO(0, options)
GEAR_END_ARG_INFO()

static const gear_function_entry hyssdbg_user_functions[] = {
	HYSS_FE(hyssdbg_clear, hyssdbg_clear_arginfo)
	HYSS_FE(hyssdbg_break_next, hyssdbg_break_next_arginfo)
	HYSS_FE(hyssdbg_break_file, hyssdbg_break_file_arginfo)
	HYSS_FE(hyssdbg_break_method, hyssdbg_break_method_arginfo)
	HYSS_FE(hyssdbg_break_function, hyssdbg_break_function_arginfo)
	HYSS_FE(hyssdbg_exec,  hyssdbg_exec_arginfo)
	HYSS_FE(hyssdbg_color, hyssdbg_color_arginfo)
	HYSS_FE(hyssdbg_prompt, hyssdbg_prompt_arginfo)
	HYSS_FE(hyssdbg_start_oplog, hyssdbg_start_oplog_arginfo)
	HYSS_FE(hyssdbg_end_oplog, hyssdbg_end_oplog_arginfo)
	HYSS_FE(hyssdbg_get_executable, hyssdbg_get_executable_arginfo)
#ifdef  HYSS_FE_END
	HYSS_FE_END
#else
	{NULL,NULL,NULL}
#endif
};

static gear_capi_entry sapi_hyssdbg_capi_entry = {
	STANDARD_CAPI_HEADER,
	HYSSDBG_NAME,
	hyssdbg_user_functions,
	HYSS_MINIT(hyssdbg),
	HYSS_MSHUTDOWN(hyssdbg),
	HYSS_RINIT(hyssdbg),
	HYSS_RSHUTDOWN(hyssdbg),
	NULL,
	HYSSDBG_VERSION,
	STANDARD_CAPI_PROPERTIES
};

static inline int hyss_sapi_hyssdbg_capi_startup(sapi_capi_struct *cAPI) /* {{{ */
{
	if (hyss_capi_startup(cAPI, &sapi_hyssdbg_capi_entry, 1) == FAILURE) {
		return FAILURE;
	}

	hyssdbg_booted = 1;

	return SUCCESS;
} /* }}} */

static char* hyss_sapi_hyssdbg_read_cookies(void) /* {{{ */
{
	return NULL;
} /* }}} */

static int hyss_sapi_hyssdbg_header_handler(sapi_header_struct *h, sapi_header_op_enum op, sapi_headers_struct *s) /* {{{ */
{
	return 0;
}
/* }}} */

static int hyss_sapi_hyssdbg_send_headers(sapi_headers_struct *sapi_headers) /* {{{ */
{
	/* We do nothing here, this function is needed to prevent that the fallback
	 * header handling is called. */
	return SAPI_HEADER_SENT_SUCCESSFULLY;
}
/* }}} */

static void hyss_sapi_hyssdbg_send_header(sapi_header_struct *sapi_header, void *server_context) /* {{{ */
{
}
/* }}} */

static void hyss_sapi_hyssdbg_log_message(char *message, int syslog_type_int) /* {{{ */
{
	/*
	* We must not request hypbc before being booted
	*/
	if (hyssdbg_booted) {
		if (HYSSDBG_G(flags) & HYSSDBG_IN_EVAL) {
			hyssdbg_error("eval", "msg=\"%s\"", "%s", message);
			return;
		}

		hyssdbg_error("hyss", "msg=\"%s\"", "%s", message);

		if (HYSSDBG_G(flags) & HYSSDBG_PREVENT_INTERACTIVE) {
			return;
		}

		switch (PG(last_error_type)) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_PARSE:
			case E_RECOVERABLE_ERROR: {
				const char *file_char = gear_get_executed_filename();
				gear_string *file = gear_string_init(file_char, strlen(file_char), 0);
				hyssdbg_list_file(file, 3, gear_get_executed_lineno() - 1, gear_get_executed_lineno());
				gear_string_release(file);

				if (!hyssdbg_fully_started) {
					return;
				}

				do {
					switch (hyssdbg_interactive(1, NULL)) {
						case HYSSDBG_LEAVE:
						case HYSSDBG_FINISH:
						case HYSSDBG_UNTIL:
						case HYSSDBG_NEXT:
							return;
					}
				} while (!(HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING));
			}
		}
	} else {
		fprintf(stdout, "%s\n", message);
	}
}
/* }}} */

static int hyss_sapi_hyssdbg_activate(void) /* {{{ */
{
	return SUCCESS;
}

static int hyss_sapi_hyssdbg_deactivate(void) /* {{{ */
{
	return SUCCESS;
}

static void hyss_sapi_hyssdbg_register_vars(zval *track_vars_array) /* {{{ */
{
	size_t len;
	char  *docroot = "";

	/* In hyssdbg mode, we consider the environment to be a part of the server variables
	*/
	hyss_import_environment_variables(track_vars_array);

	if (HYSSDBG_G(exec)) {
		len = HYSSDBG_G(exec_len);
		if (sapi_capi.input_filter(PARSE_SERVER, "HYSS_SELF", &HYSSDBG_G(exec), HYSSDBG_G(exec_len), &len)) {
			hyss_register_variable("HYSS_SELF", HYSSDBG_G(exec), track_vars_array);
		}
		if (sapi_capi.input_filter(PARSE_SERVER, "SCRIPT_NAME", &HYSSDBG_G(exec), HYSSDBG_G(exec_len), &len)) {
			hyss_register_variable("SCRIPT_NAME", HYSSDBG_G(exec), track_vars_array);
		}

		if (sapi_capi.input_filter(PARSE_SERVER, "SCRIPT_FILENAME", &HYSSDBG_G(exec), HYSSDBG_G(exec_len), &len)) {
			hyss_register_variable("SCRIPT_FILENAME", HYSSDBG_G(exec), track_vars_array);
		}
		if (sapi_capi.input_filter(PARSE_SERVER, "PATH_TRANSLATED", &HYSSDBG_G(exec), HYSSDBG_G(exec_len), &len)) {
			hyss_register_variable("PATH_TRANSLATED", HYSSDBG_G(exec), track_vars_array);
		}
	}

	/* any old docroot will do */
	len = 0;
	if (sapi_capi.input_filter(PARSE_SERVER, "DOCUMENT_ROOT", &docroot, len, &len)) {
		hyss_register_variable("DOCUMENT_ROOT", docroot, track_vars_array);
	}
}
/* }}} */

static inline size_t hyss_sapi_hyssdbg_ub_write(const char *message, size_t length) /* {{{ */
{
	if (HYSSDBG_G(socket_fd) != -1 && !(HYSSDBG_G(flags) & HYSSDBG_IS_INTERACTIVE)) {
		send(HYSSDBG_G(socket_fd), message, length, 0);
	}
	return hyssdbg_script(P_STDOUT, "%.*s", (int) length, message);
} /* }}} */

/* beginning of struct, see main/streams/plain_wrapper.c line 111 */
typedef struct {
	FILE *file;
	int fd;
} hyss_stdio_stream_data;

static size_t hyssdbg_stdiop_write(hyss_stream *stream, const char *buf, size_t count) {
	hyss_stdio_stream_data *data = (hyss_stdio_stream_data*)stream->abstract;

	while (data->fd >= 0) {
		struct stat stat[3];
		memset(stat, 0, sizeof(stat));
		if (((fstat(fileno(stderr), &stat[2]) < 0) & (fstat(fileno(stdout), &stat[0]) < 0)) | (fstat(data->fd, &stat[1]) < 0)) {
			break;
		}

		if (stat[0].st_dev == stat[1].st_dev && stat[0].st_ino == stat[1].st_ino) {
			hyssdbg_script(P_STDOUT, "%.*s", (int) count, buf);
			return count;
		}
		if (stat[2].st_dev == stat[1].st_dev && stat[2].st_ino == stat[1].st_ino) {
			hyssdbg_script_ex(HYSSDBG_G(io)[HYSSDBG_STDERR].fd, P_STDERR, "%.*s", (int) count, buf);
			return count;
		}
		break;
	}

	return HYSSDBG_G(hyss_stdiop_write)(stream, buf, count);
}

static inline void hyss_sapi_hyssdbg_flush(void *context)  /* {{{ */
{
	if (!hyssdbg_active_sigsafe_mem()) {
		fflush(HYSSDBG_G(io)[HYSSDBG_STDOUT].ptr);
	}
} /* }}} */

/* copied from server/cli/hyss_cli.c cli_register_file_handles */
void hyssdbg_register_file_handles(void) /* {{{ */
{
	zval zin, zout, zerr;
	hyss_stream *s_in, *s_out, *s_err;
	hyss_stream_context *sc_in=NULL, *sc_out=NULL, *sc_err=NULL;
	gear_constant ic, oc, ec;

	s_in  = hyss_stream_open_wrapper_ex("hyss://stdin",  "rb", 0, NULL, sc_in);
	s_out = hyss_stream_open_wrapper_ex("hyss://stdout", "wb", 0, NULL, sc_out);
	s_err = hyss_stream_open_wrapper_ex("hyss://stderr", "wb", 0, NULL, sc_err);

	if (s_in==NULL || s_out==NULL || s_err==NULL) {
		if (s_in) hyss_stream_close(s_in);
		if (s_out) hyss_stream_close(s_out);
		if (s_err) hyss_stream_close(s_err);
		return;
	}

#if HYSS_DEBUG
	/* do not close stdout and stderr */
	s_out->flags |= HYSS_STREAM_FLAG_NO_CLOSE;
	s_err->flags |= HYSS_STREAM_FLAG_NO_CLOSE;
#endif

	hyss_stream_to_zval(s_in,  &zin);
	hyss_stream_to_zval(s_out, &zout);
	hyss_stream_to_zval(s_err, &zerr);

	ic.value = zin;
	GEAR_CONSTANT_SET_FLAGS(&ic, CONST_CS, 0);
	ic.name = gear_string_init(GEAR_STRL("STDIN"), 0);
	gear_hash_del(EG(gear_constants), ic.name);
	gear_register_constant(&ic);

	oc.value = zout;
	GEAR_CONSTANT_SET_FLAGS(&oc, CONST_CS, 0);
	oc.name = gear_string_init(GEAR_STRL("STDOUT"), 0);
	gear_hash_del(EG(gear_constants), oc.name);
	gear_register_constant(&oc);

	ec.value = zerr;
	GEAR_CONSTANT_SET_FLAGS(&ec, CONST_CS, 0);
	ec.name = gear_string_init(GEAR_STRL("STDERR"), 0);
	gear_hash_del(EG(gear_constants), ec.name);
	gear_register_constant(&ec);
}
/* }}} */

/* {{{ sapi_capi_struct hyssdbg_sapi_capi
*/
static sapi_capi_struct hyssdbg_sapi_capi = {
	"hyssdbg",                       /* name */
	"hyssdbg",                       /* pretty name */

	hyss_sapi_hyssdbg_capi_startup, /* startup */
	hyss_capi_shutdown_wrapper,    /* shutdown */

	hyss_sapi_hyssdbg_activate,       /* activate */
	hyss_sapi_hyssdbg_deactivate,     /* deactivate */

	hyss_sapi_hyssdbg_ub_write,       /* unbuffered write */
	hyss_sapi_hyssdbg_flush,          /* flush */
	NULL,                           /* get uid */
	NULL,                           /* getenv */

	hyss_error,                      /* error handler */

	hyss_sapi_hyssdbg_header_handler, /* header handler */
	hyss_sapi_hyssdbg_send_headers,   /* send headers handler */
	hyss_sapi_hyssdbg_send_header,    /* send header handler */

	NULL,                           /* read POST data */
	hyss_sapi_hyssdbg_read_cookies,   /* read Cookies */

	hyss_sapi_hyssdbg_register_vars,  /* register server variables */
	hyss_sapi_hyssdbg_log_message,    /* Log message */
	NULL,                           /* Get request time */
	NULL,                           /* Child terminate */
	STANDARD_SAPI_CAPI_PROPERTIES
};
/* }}} */

const opt_struct OPTIONS[] = { /* {{{ */
	{'c', 1, "ics path override"},
	{'d', 1, "define ics entry on command line"},
	{'n', 0, "no hyss.ics"},
	{'z', 1, "load gear_extension"},
	/* hyssdbg options */
	{'q', 0, "no banner"},
	{'v', 0, "disable quietness"},
	{'b', 0, "boring colours"},
	{'i', 1, "specify init"},
	{'I', 0, "ignore init"},
	{'O', 1, "opline log"},
	{'r', 0, "run"},
	{'e', 0, "generate ext_stmt opcodes"},
	{'E', 0, "step-through-eval"},
	{'s', 1, "script from stdin"},
	{'S', 1, "sapi-name"},
#ifndef _WIN32
	{'l', 1, "listen"},
	{'a', 1, "address-or-any"},
#endif
	{'x', 0, "xml output"},
	{'p', 2, "show opcodes"},
	{'h', 0, "help"},
	{'V', 0, "version"},
	{'-', 0, NULL}
}; /* }}} */

const char hyssdbg_ics_hardcoded[] =
"html_errors=Off\n"
"register_argc_argv=On\n"
"implicit_flush=On\n"
"display_errors=Off\n"
"log_errors=On\n"
"max_execution_time=0\n"
"max_input_time=-1\n"
"error_log=\n"
"output_buffering=off\n\0";

/* overwriteable ics defaults must be set in hyssdbg_ics_defaults() */
#define ICS_DEFAULT(name, value) \
	ZVAL_NEW_STR(&tmp, gear_string_init(value, sizeof(value) - 1, 1)); \
	gear_hash_str_update(configuration_hash, name, sizeof(name) - 1, &tmp);

void hyssdbg_ics_defaults(HashTable *configuration_hash) /* {{{ */
{
	zval tmp;
	ICS_DEFAULT("report_gear_debug", "0");
} /* }}} */

static void hyssdbg_welcome(gear_bool cleaning) /* {{{ */
{
	/* print blurb */
	if (!cleaning) {
		hyssdbg_xml("<intros>");
		hyssdbg_notice("intro", "version=\"%s\"", "Welcome to hyssdbg, the interactive HYSS debugger, v%s", HYSSDBG_VERSION);
		hyssdbg_writeln("intro", "help=\"help\"", "To get help using hyssdbg type \"help\" and press enter");
		hyssdbg_notice("intro", "report=\"%s\"", "Please report bugs to <%s>", HYSSDBG_ISSUES);
		hyssdbg_xml("</intros>");
	} else if (hyssdbg_startup_run == 0) {
		if (!(HYSSDBG_G(flags) & HYSSDBG_WRITE_XML)) {
			hyssdbg_notice(NULL, NULL, "Clean Execution Environment");
		}

		hyssdbg_write("cleaninfo", "classes=\"%d\" functions=\"%d\" constants=\"%d\" includes=\"%d\"",
			"Classes              %d\n"
			"Functions            %d\n"
			"Constants            %d\n"
			"Includes             %d\n",
			gear_hash_num_elements(EG(class_table)),
			gear_hash_num_elements(EG(function_table)),
			gear_hash_num_elements(EG(gear_constants)),
			gear_hash_num_elements(&EG(included_files)));
	}
} /* }}} */

static inline void hyssdbg_sigint_handler(int signo) /* {{{ */
{

	if (HYSSDBG_G(flags) & HYSSDBG_IS_INTERACTIVE) {
		/* we quit remote consoles on recv SIGINT */
		if (HYSSDBG_G(flags) & HYSSDBG_IS_REMOTE) {
			HYSSDBG_G(flags) |= HYSSDBG_IS_STOPPING;
			gear_bailout();
		}
	} else {
		/* set signalled only when not interactive */
		if (HYSSDBG_G(flags) & HYSSDBG_IS_SIGNALED) {
			char mem[HYSSDBG_SIGSAFE_MEM_SIZE + 1];

			hyssdbg_set_sigsafe_mem(mem);
			gear_try {
				hyssdbg_force_interruption();
			} gear_end_try()
			hyssdbg_clear_sigsafe_mem();

			HYSSDBG_G(flags) &= ~HYSSDBG_IS_SIGNALED;

			if (HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING) {
				gear_bailout();
			}
		} else {
			HYSSDBG_G(flags) |= HYSSDBG_IS_SIGNALED;
			if (HYSSDBG_G(flags) & HYSSDBG_PREVENT_INTERACTIVE) {
				HYSSDBG_G(flags) |= HYSSDBG_HAS_PAGINATION;
				HYSSDBG_G(flags) &= ~HYSSDBG_PREVENT_INTERACTIVE;
			}
		}
	}
} /* }}} */

static void hyssdbg_remote_close(int socket, FILE *stream) {
	if (socket >= 0) {
		hyssdbg_close_socket(socket);
	}

	if (stream) {
		fclose(stream);
	}
}

/* don't inline this, want to debug it easily, will inline when done */
static int hyssdbg_remote_init(const char* address, unsigned short port, int server, int *socket, FILE **stream) {
	hyssdbg_remote_close(*socket, *stream);

	if (server < 0) {
		hyssdbg_rlog(fileno(stderr), "Initializing connection on %s:%u failed", address, port);

		return FAILURE;
	}

	hyssdbg_rlog(fileno(stderr), "accepting connections on %s:%u", address, port);
	{
		struct sockaddr_storage address;
		socklen_t size = sizeof(address);
		char buffer[20] = {0};
		/* XXX error checks */
		memset(&address, 0, size);
		*socket = accept(server, (struct sockaddr *) &address, &size);
		inet_ntop(AF_INET, &(((struct sockaddr_in *)&address)->sin_addr), buffer, sizeof(buffer));

		hyssdbg_rlog(fileno(stderr), "connection established from %s", buffer);
	}

#ifndef _WIN32
	dup2(*socket, fileno(stdout));
	dup2(*socket, fileno(stdin));

	setbuf(stdout, NULL);

	*stream = fdopen(*socket, "r+");

	hyssdbg_set_async_io(*socket);
#endif
	return SUCCESS;
}

#ifndef _WIN32
/* This function *strictly* assumes that SIGIO is *only* used on the remote connection stream */
void hyssdbg_sigio_handler(int sig, siginfo_t *info, void *context) /* {{{ */
{
	int flags;
	size_t newlen;
	size_t i/*, last_nl*/;

//	if (!(info->si_band & POLLIN)) {
//		return; /* Not interested in writeablility etc., just interested in incoming data */
//	}

	/* only non-blocking reading, avoid non-blocking writing */
	flags = fcntl(HYSSDBG_G(io)[HYSSDBG_STDIN].fd, F_GETFL, 0);
	fcntl(HYSSDBG_G(io)[HYSSDBG_STDIN].fd, F_SETFL, flags | O_NONBLOCK);

	do {
		char mem[HYSSDBG_SIGSAFE_MEM_SIZE + 1];
		size_t off = 0;

		if ((newlen = recv(HYSSDBG_G(io)[HYSSDBG_STDIN].fd, mem, HYSSDBG_SIGSAFE_MEM_SIZE, MSG_PEEK)) == (size_t) -1) {
			break;
		}
		for (i = 0; i < newlen; i++) {
			switch (mem[off + i]) {
				case '\x03': /* ^C char */
					if (HYSSDBG_G(flags) & HYSSDBG_IS_INTERACTIVE) {
						break; /* or quit ??? */
					}
					if (HYSSDBG_G(flags) & HYSSDBG_IS_SIGNALED) {
						hyssdbg_set_sigsafe_mem(mem);
						gear_try {
							hyssdbg_force_interruption();
						} gear_end_try();
						hyssdbg_clear_sigsafe_mem();

						HYSSDBG_G(flags) &= ~HYSSDBG_IS_SIGNALED;

						if (HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING) {
							gear_bailout();
						}
					} else if (!(HYSSDBG_G(flags) & HYSSDBG_IS_INTERACTIVE)) {
						HYSSDBG_G(flags) |= HYSSDBG_IS_SIGNALED;
					}
					break;
/*				case '\n':
					gear_llist_add_element(HYSSDBG_G(stdin), strndup()
					last_nl = HYSSDBG_G(stdin_buf).len + i;
					break;
*/			}
		}
		off += i;
	} while (0);


	fcntl(HYSSDBG_G(io)[HYSSDBG_STDIN].fd, F_SETFL, flags);
} /* }}} */

void hyssdbg_signal_handler(int sig, siginfo_t *info, void *context) /* {{{ */
{
	int is_handled = FAILURE;

	switch (sig) {
		case SIGBUS:
		case SIGSEGV:
			is_handled = hyssdbg_watchpoint_segfault_handler(info, context);
			if (is_handled == FAILURE) {
				if (HYSSDBG_G(sigsegv_bailout)) {
					LONGJMP(*HYSSDBG_G(sigsegv_bailout), FAILURE);
				}
				gear_sigaction(sig, &HYSSDBG_G(old_sigsegv_signal), NULL);
			}
			break;
	}

} /* }}} */
#endif

void hyssdbg_sighup_handler(int sig) /* {{{ */
{
	exit(0);
} /* }}} */

void *hyssdbg_malloc_wrapper(size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) /* {{{ */
{
	return _gear_mm_alloc(gear_mm_get_heap(), size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
} /* }}} */

void hyssdbg_free_wrapper(void *p GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) /* {{{ */
{
	gear_mm_heap *heap = gear_mm_get_heap();
	if (UNEXPECTED(heap == p)) {
		/* TODO: heap maybe allocated by mmap(gear_mm_init) or malloc(USE_GEAR_ALLOC=0)
		 * let's prevent it from segfault for now
		 */
	} else {
		hyssdbg_watch_efree(p);
		_gear_mm_free(heap, p GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
	}
} /* }}} */

void *hyssdbg_realloc_wrapper(void *ptr, size_t size GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC) /* {{{ */
{
	return _gear_mm_realloc(gear_mm_get_heap(), ptr, size GEAR_FILE_LINE_RELAY_CC GEAR_FILE_LINE_ORIG_RELAY_CC);
} /* }}} */

hyss_stream *hyssdbg_stream_url_wrap_hyss(hyss_stream_wrapper *wrapper, const char *path, const char *mode, int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC) /* {{{ */
{
	if (!strncasecmp(path, "hyss://", 6)) {
		path += 6;
	}

	if (!strncasecmp(path, "stdin", 6) && HYSSDBG_G(stdin_file)) {
		hyss_stream *stream = hyss_stream_fopen_from_fd(dup(fileno(HYSSDBG_G(stdin_file))), "r", NULL);
#ifdef HYSS_WIN32
		zval *blocking_pipes = hyss_stream_context_get_option(context, "pipe", "blocking");
		if (blocking_pipes) {
			convert_to_long(blocking_pipes);
			hyss_stream_set_option(stream, HYSS_STREAM_OPTION_PIPE_BLOCKING, Z_LVAL_P(blocking_pipes), NULL);
		}
#endif
		return stream;
	}

	return HYSSDBG_G(orig_url_wrap_hyss)->wops->stream_opener(wrapper, path, mode, options, opened_path, context STREAMS_CC);
} /* }}} */

int main(int argc, char **argv) /* {{{ */
{
	sapi_capi_struct *hyssdbg = &hyssdbg_sapi_capi;
	char *sapi_name;
	char *ics_entries;
	int   ics_entries_len;
	char **gear_extensions = NULL;
	gear_ulong gear_extensions_len = 0L;
	gear_bool ics_ignore;
	char *ics_override;
	char *exec = NULL;
	char *first_command = NULL;
	char *init_file;
	size_t init_file_len;
	gear_bool init_file_default;
	char *oplog_file;
	size_t oplog_file_len;
	uint64_t flags;
	char *hyss_optarg;
	int hyss_optind, opt, show_banner = 1;
	long cleaning = -1;
	volatile gear_bool quit_immediately = 0; /* somehow some gcc release builds will play a bit around with order in combination with setjmp..., hence volatile */
	gear_bool remote = 0;
	gear_hyssdbg_globals *settings = NULL;
	char *bp_tmp = NULL;
	char *address;
	int listen = -1;
	int server = -1;
	int socket = -1;
	FILE* stream = NULL;
	char *print_opline_func;
	gear_bool ext_stmt = 0;
	gear_bool is_exit;
	int exit_status;
	char *read_from_stdin = NULL;
	gear_string *backup_hyssdbg_compile = NULL;
	gear_bool show_help = 0, show_version = 0;
	void* (*_malloc)(size_t);
	void (*_free)(void*);
	void* (*_realloc)(void*, size_t);
	hyss_stream_wrapper wrapper;
	hyss_stream_wrapper_ops wops;


#ifndef _WIN32
	struct sigaction sigio_struct;
	struct sigaction signal_struct;
	signal_struct.sa_sigaction = hyssdbg_signal_handler;
	signal_struct.sa_flags = SA_SIGINFO | SA_NODEFER;
	sigemptyset(&signal_struct.sa_mask);
	sigio_struct.sa_sigaction = hyssdbg_sigio_handler;
	sigio_struct.sa_flags = SA_SIGINFO;
	sigemptyset(&sigio_struct.sa_mask);

	address = strdup("127.0.0.1");
#endif

#ifdef HYSS_WIN32
	_fmode = _O_BINARY;                 /* sets default for file streams to binary */
	setmode(_fileno(stdin), O_BINARY);  /* make the stdio mode be binary */
	setmode(_fileno(stdout), O_BINARY); /* make the stdio mode be binary */
	setmode(_fileno(stderr), O_BINARY); /* make the stdio mode be binary */
#endif

hyssdbg_main:
#ifdef ZTS
	pbc_startup(1, 1, 0, NULL);
	(void)ts_resource(0);
	GEAR_PBCLS_CACHE_UPDATE();
#endif

	gear_signal_startup();

	ics_entries = NULL;
	ics_entries_len = 0;
	ics_ignore = 0;
	ics_override = NULL;
	gear_extensions = NULL;
	gear_extensions_len = 0L;
	init_file = NULL;
	init_file_len = 0;
	init_file_default = 1;
	oplog_file = NULL;
	oplog_file_len = 0;
	flags = HYSSDBG_DEFAULT_FLAGS;
	is_exit = 0;
	hyss_optarg = NULL;
	hyss_optind = 1;
	opt = 0;
	sapi_name = NULL;
	exit_status = 0;
	if (settings) {
		exec = settings->exec;
	}

	while ((opt = hyss_getopt(argc, argv, OPTIONS, &hyss_optarg, &hyss_optind, 0, 2)) != -1) {
		switch (opt) {
			case 'r':
				if (settings == NULL) {
					hyssdbg_startup_run++;
				}
				break;
			case 'n':
				ics_ignore = 1;
				break;
			case 'c':
				if (ics_override) {
					free(ics_override);
				}
				ics_override = strdup(hyss_optarg);
				break;
			case 'd': {
				int len = strlen(hyss_optarg);
				char *val;

				if ((val = strchr(hyss_optarg, '='))) {
				  val++;
				  if (!isalnum(*val) && *val != '"' && *val != '\'' && *val != '\0') {
					  ics_entries = realloc(ics_entries, ics_entries_len + len + sizeof("\"\"\n\0"));
					  memcpy(ics_entries + ics_entries_len, hyss_optarg, (val - hyss_optarg));
					  ics_entries_len += (val - hyss_optarg);
					  memcpy(ics_entries + ics_entries_len, "\"", 1);
					  ics_entries_len++;
					  memcpy(ics_entries + ics_entries_len, val, len - (val - hyss_optarg));
					  ics_entries_len += len - (val - hyss_optarg);
					  memcpy(ics_entries + ics_entries_len, "\"\n\0", sizeof("\"\n\0"));
					  ics_entries_len += sizeof("\n\0\"") - 2;
				  } else {
					  ics_entries = realloc(ics_entries, ics_entries_len + len + sizeof("\n\0"));
					  memcpy(ics_entries + ics_entries_len, hyss_optarg, len);
					  memcpy(ics_entries + ics_entries_len + len, "\n\0", sizeof("\n\0"));
					  ics_entries_len += len + sizeof("\n\0") - 2;
				  }
				} else {
				  ics_entries = realloc(ics_entries, ics_entries_len + len + sizeof("=1\n\0"));
				  memcpy(ics_entries + ics_entries_len, hyss_optarg, len);
				  memcpy(ics_entries + ics_entries_len + len, "=1\n\0", sizeof("=1\n\0"));
				  ics_entries_len += len + sizeof("=1\n\0") - 2;
				}
			} break;

			case 'z':
				gear_extensions_len++;
				if (gear_extensions) {
					gear_extensions = realloc(gear_extensions, sizeof(char*) * gear_extensions_len);
				} else gear_extensions = malloc(sizeof(char*) * gear_extensions_len);
				gear_extensions[gear_extensions_len-1] = strdup(hyss_optarg);
			break;

			/* begin hyssdbg options */

			case 's': { /* read script from stdin */
				if (settings == NULL) {
					read_from_stdin = strdup(hyss_optarg);
				}
			} break;

			case 'S': { /* set SAPI name */
				sapi_name = strdup(hyss_optarg);
			} break;

			case 'I': { /* ignore .hyssdbginit */
				init_file_default = 0;
			} break;

			case 'i': { /* set init file */
				if (init_file) {
					free(init_file);
					init_file = NULL;
				}

				init_file_len = strlen(hyss_optarg);
				if (init_file_len) {
					init_file = strdup(hyss_optarg);
				}
			} break;

			case 'O': { /* set oplog output */
				oplog_file_len = strlen(hyss_optarg);
				if (oplog_file_len) {
					oplog_file = strdup(hyss_optarg);
				}
			} break;

			case 'v': /* set quietness off */
				flags &= ~HYSSDBG_IS_QUIET;
			break;

			case 'e':
				ext_stmt = 1;
			break;

			case 'E': /* stepping through eval on */
				flags |= HYSSDBG_IS_STEPONEVAL;
			break;

			case 'b': /* set colours off */
				flags &= ~HYSSDBG_IS_COLOURED;
			break;

			case 'q': /* hide banner */
				show_banner = 0;
			break;

#ifndef _WIN32
			/* if you pass a listen port, we will read and write on listen port */
			case 'l': /* set listen ports */
				if (sscanf(hyss_optarg, "%d", &listen) != 1) {
					listen = 8000;
				}
			break;

			case 'a': { /* set bind address */
				free(address);
				if (!hyss_optarg) {
					address = strdup("*");
				} else address = strdup(hyss_optarg);
			} break;
#endif

			case 'x':
				flags |= HYSSDBG_WRITE_XML;
			break;


			case 'p': {
				print_opline_func = hyss_optarg;
				show_banner = 0;
				settings = (void *) 0x1;
			} break;

			case 'h': {
				show_help = 1;
			} break;

			case 'V': {
				show_version = 1;
			} break;
		}

		hyss_optarg = NULL;
	}

	quit_immediately = hyssdbg_startup_run > 1;

	/* set exec if present on command line */
	if (!read_from_stdin && argc > hyss_optind) {
		if (!exec && strlen(argv[hyss_optind])) {
			exec = strdup(argv[hyss_optind]);
		}
		hyss_optind++;
	}

	if (sapi_name) {
		hyssdbg->name = sapi_name;
	}

	hyssdbg->ics_defaults = hyssdbg_ics_defaults;
	hyssdbg->hyssinfo_as_text = 1;
	hyssdbg->hyss_ics_ignore_cwd = 1;

	sapi_startup(hyssdbg);

	hyssdbg->executable_location = argv[0];
	hyssdbg->hyssinfo_as_text = 1;
	hyssdbg->hyss_ics_ignore = ics_ignore;
	hyssdbg->hyss_ics_path_override = ics_override;

	if (ics_entries) {
		ics_entries = realloc(ics_entries, ics_entries_len + sizeof(hyssdbg_ics_hardcoded));
		memmove(ics_entries + sizeof(hyssdbg_ics_hardcoded) - 2, ics_entries, ics_entries_len + 1);
		memcpy(ics_entries, hyssdbg_ics_hardcoded, sizeof(hyssdbg_ics_hardcoded) - 2);
	} else {
		ics_entries = malloc(sizeof(hyssdbg_ics_hardcoded));
		memcpy(ics_entries, hyssdbg_ics_hardcoded, sizeof(hyssdbg_ics_hardcoded));
	}
	ics_entries_len += sizeof(hyssdbg_ics_hardcoded) - 2;

	if (gear_extensions_len) {
		gear_ulong gear_extension = 0L;

		while (gear_extension < gear_extensions_len) {
			const char *ze = gear_extensions[gear_extension];
			size_t ze_len = strlen(ze);

			ics_entries = realloc(
				ics_entries, ics_entries_len + (ze_len + (sizeof("gear_extension=\n"))));
			memcpy(&ics_entries[ics_entries_len], "gear_extension=", (sizeof("gear_extension=\n")-1));
			ics_entries_len += (sizeof("gear_extension=")-1);
			memcpy(&ics_entries[ics_entries_len], ze, ze_len);
			ics_entries_len += ze_len;
			memcpy(&ics_entries[ics_entries_len], "\n", (sizeof("\n") - 1));

			free(gear_extensions[gear_extension]);
			gear_extension++;
		}

		free(gear_extensions);
	}

	hyssdbg->ics_entries = ics_entries;

	GEAR_INIT_CAPI_GLOBALS(hyssdbg, hyss_hyssdbg_globals_ctor, NULL);

	/* set default colors */
	hyssdbg_set_color_ex(HYSSDBG_COLOR_PROMPT,  HYSSDBG_STRL("white-bold"));
	hyssdbg_set_color_ex(HYSSDBG_COLOR_ERROR,   HYSSDBG_STRL("red-bold"));
	hyssdbg_set_color_ex(HYSSDBG_COLOR_NOTICE,  HYSSDBG_STRL("green"));

	/* set default prompt */
	hyssdbg_set_prompt(HYSSDBG_DEFAULT_PROMPT);

	if (settings > (gear_hyssdbg_globals *) 0x2) {
#ifdef ZTS
		*((gear_hyssdbg_globals *) (*((void ***) PBCLS_CACHE))[PBC_UNSHUFFLE_RSRC_ID(hyssdbg_globals_id)]) = *settings;
#else
		hyssdbg_globals = *settings;
#endif
		free(settings);
	}

	/* set flags from command line */
	HYSSDBG_G(flags) = flags;

	if (hyssdbg->startup(hyssdbg) == SUCCESS) {
		gear_mm_heap *mm_heap;
#ifdef _WIN32
    EXCEPTION_POINTERS *xp;
    __try {
#endif

		if (show_version || show_help) {
			/* It ain't gonna proceed to real execution anyway,
				but the correct descriptor is needed already. */
			HYSSDBG_G(io)[HYSSDBG_STDOUT].ptr = stdout;
			HYSSDBG_G(io)[HYSSDBG_STDOUT].fd = fileno(stdout);
			if (show_help) {
				hyssdbg_do_help_cmd(exec);
			} else if (show_version) {
				hyssdbg_out(
					"hyssdbg %s (built: %s %s)\nHYSS %s, Copyright (c) 2017-2019 The Hyang Language Foundation\n%s",
					HYSSDBG_VERSION,
					__DATE__,
					__TIME__,
					HYSS_VERSION,
					get_gear_version()
				);
			}
			sapi_deactivate();
			sapi_shutdown();
			if (ics_entries) {
				free(ics_entries);
			}
			if (ics_override) {
				free(ics_override);
			}
			if (exec) {
				free(exec);
			}
			if (oplog_file) {
				free(oplog_file);
			}
			if (init_file) {
				free(init_file);
			}
			goto free_and_return;
		}

		gear_try {
			gear_signal_activate();
		} gear_end_try();

		/* setup remote server if necessary */
		if (cleaning <= 0 && listen > 0) {
			server = hyssdbg_open_socket(address, listen);
			if (-1 > server || hyssdbg_remote_init(address, listen, server, &socket, &stream) == FAILURE) {
				exit(0);
			}

#ifndef _WIN32
			gear_sigaction(SIGIO, &sigio_struct, NULL);
#endif

			/* set remote flag to stop service shutting down upon quit */
			remote = 1;
#ifndef _WIN32
		} else {

			gear_signal(SIGHUP, hyssdbg_sighup_handler);
#endif
		}

		mm_heap = gear_mm_get_heap();
		gear_mm_get_custom_handlers(mm_heap, &_malloc, &_free, &_realloc);

		use_mm_wrappers = !_malloc && !_realloc && !_free;

		HYSSDBG_G(original_free_function) = _free;
		_free = hyssdbg_watch_efree;

		if (use_mm_wrappers) {
#if GEAR_DEBUG
			gear_mm_set_custom_debug_handlers(mm_heap, hyssdbg_malloc_wrapper, hyssdbg_free_wrapper, hyssdbg_realloc_wrapper);
#else
			gear_mm_set_custom_handlers(mm_heap, hyssdbg_malloc_wrapper, hyssdbg_free_wrapper, hyssdbg_realloc_wrapper);
#endif
		} else {
			gear_mm_set_custom_handlers(mm_heap, _malloc, _free, _realloc);
		}

		_free = HYSSDBG_G(original_free_function);


		hyssdbg_init_list();

		HYSSDBG_G(sapi_name_ptr) = sapi_name;

		if (exec) { /* set execution context */
			HYSSDBG_G(exec) = hyssdbg_resolve_path(exec);
			HYSSDBG_G(exec_len) = HYSSDBG_G(exec) ? strlen(HYSSDBG_G(exec)) : 0;

			free(exec);
			exec = NULL;
		}

		hyss_output_activate();
		hyss_output_deactivate();

		if (SG(sapi_headers).mimetype) {
			efree(SG(sapi_headers).mimetype);
			SG(sapi_headers).mimetype = NULL;
		}

		hyss_output_activate();

		{
			int i;

			SG(request_info).argc = argc - hyss_optind + 1;
			SG(request_info).argv = emalloc(SG(request_info).argc * sizeof(char *));
			for (i = SG(request_info).argc; --i;) {
				SG(request_info).argv[i] = estrdup(argv[hyss_optind - 1 + i]);
			}
			SG(request_info).argv[0] = HYSSDBG_G(exec) ? estrdup(HYSSDBG_G(exec)) : estrdup("");
		}

		if (hyss_request_startup() == FAILURE) {
			PUTS("Could not startup");
#ifndef _WIN32
			if (address) {
				free(address);
			}
#endif
			return 1;
		}

#ifndef _WIN32
		gear_try { gear_sigaction(SIGSEGV, &signal_struct, &HYSSDBG_G(old_sigsegv_signal)); } gear_end_try();
		gear_try { gear_sigaction(SIGBUS, &signal_struct, &HYSSDBG_G(old_sigsegv_signal)); } gear_end_try();
#endif

		/* do not install sigint handlers for remote consoles */
		/* sending SIGINT then provides a decent way of shutting down the server */
#ifndef _WIN32
		if (listen < 0) {
#endif
			gear_try { gear_signal(SIGINT, hyssdbg_sigint_handler); } gear_end_try();
#ifndef _WIN32
		}

		/* setup io here */
		if (remote) {
			HYSSDBG_G(flags) |= HYSSDBG_IS_REMOTE;
			gear_signal(SIGPIPE, SIG_IGN);
		}
		HYSSDBG_G(io)[HYSSDBG_STDIN].ptr = stdin;
		HYSSDBG_G(io)[HYSSDBG_STDIN].fd = fileno(stdin);
		HYSSDBG_G(io)[HYSSDBG_STDOUT].ptr = stdout;
		HYSSDBG_G(io)[HYSSDBG_STDOUT].fd = fileno(stdout);
#else
		/* XXX this is a complete mess here with FILE/fd/SOCKET,
			we should let only one to survive probably. Need
			a clean separation whether it's a remote or local
			prompt. And what is supposed to go as user interaction,
			error log, etc. */
		if (remote) {
			HYSSDBG_G(io)[HYSSDBG_STDIN].ptr = stdin;
			HYSSDBG_G(io)[HYSSDBG_STDIN].fd = socket;
			HYSSDBG_G(io)[HYSSDBG_STDOUT].ptr = stdout;
			HYSSDBG_G(io)[HYSSDBG_STDOUT].fd = socket;
		} else {
			HYSSDBG_G(io)[HYSSDBG_STDIN].ptr = stdin;
			HYSSDBG_G(io)[HYSSDBG_STDIN].fd = fileno(stdin);
			HYSSDBG_G(io)[HYSSDBG_STDOUT].ptr = stdout;
			HYSSDBG_G(io)[HYSSDBG_STDOUT].fd = fileno(stdout);
		}
#endif
		HYSSDBG_G(io)[HYSSDBG_STDERR].ptr = stderr;
		HYSSDBG_G(io)[HYSSDBG_STDERR].fd = fileno(stderr);

#ifndef _WIN32
		HYSSDBG_G(hyss_stdiop_write) = hyss_stream_stdio_ops.write;
		hyss_stream_stdio_ops.write = hyssdbg_stdiop_write;
#endif

		if (oplog_file) { /* open oplog */
			HYSSDBG_G(oplog) = fopen(oplog_file, "w+");
			if (!HYSSDBG_G(oplog)) {
				hyssdbg_error("oplog", "path=\"%s\"", "Failed to open oplog %s", oplog_file);
			}
			free(oplog_file);
			oplog_file = NULL;
		}

		{
			zval *zv = gear_hash_str_find(hyss_stream_get_url_stream_wrappers_hash(), GEAR_STRL("hyss"));
			hyss_stream_wrapper *tmp_wrapper = Z_PTR_P(zv);
			HYSSDBG_G(orig_url_wrap_hyss) = tmp_wrapper;
			memcpy(&wrapper, tmp_wrapper, sizeof(wrapper));
			memcpy(&wops, tmp_wrapper->wops, sizeof(wops));
			wops.stream_opener = hyssdbg_stream_url_wrap_hyss;
			wrapper.wops = (const hyss_stream_wrapper_ops*)&wops;
			Z_PTR_P(zv) = &wrapper;
		}

		/* Make stdin, stdout and stderr accessible from HYSS scripts */
		hyssdbg_register_file_handles();

		hyssdbg_list_update();

		if (show_banner && cleaning < 2) {
			/* print blurb */
			hyssdbg_welcome(cleaning == 1);
		}

		cleaning = -1;

		if (ext_stmt) {
			CG(compiler_options) |= GEAR_COMPILE_EXTENDED_INFO;
		}

		/* initialize from file */
		HYSSDBG_G(flags) |= HYSSDBG_IS_INITIALIZING;
		gear_try {
			hyssdbg_init(init_file, init_file_len, init_file_default);
		} gear_end_try();
		HYSSDBG_G(flags) &= ~HYSSDBG_IS_INITIALIZING;

		/* quit if init says so */
		if (HYSSDBG_G(flags) & HYSSDBG_IS_QUITTING) {
			goto hyssdbg_out;
		}

		/* auto compile */
		if (read_from_stdin) {
			if (!read_from_stdin[0]) {
				if (!quit_immediately) {
					hyssdbg_error("error", "", "Impossible to not specify a stdin delimiter without -rr");
					HYSSDBG_G(flags) |= HYSSDBG_IS_QUITTING;
					goto hyssdbg_out;
				}
			}
			if (show_banner || read_from_stdin[0]) {
				hyssdbg_notice("stdin", "delimiter=\"%s\"", "Reading input from stdin; put '%s' followed by a newline on an own line after code to end input", read_from_stdin);
			}

			if (hyssdbg_startup_run > 0) {
				HYSSDBG_G(flags) |= HYSSDBG_DISCARD_OUTPUT;
			}

			gear_try {
				hyssdbg_param_t cmd;
				cmd.str = read_from_stdin;
				cmd.len = strlen(read_from_stdin);
				HYSSDBG_COMMAND_HANDLER(stdin)(&cmd);
			} gear_end_try();

			HYSSDBG_G(flags) &= ~HYSSDBG_DISCARD_OUTPUT;
		} else if (HYSSDBG_G(exec)) {
			if (settings || hyssdbg_startup_run > 0) {
				HYSSDBG_G(flags) |= HYSSDBG_DISCARD_OUTPUT;
			}

			gear_try {
				if (backup_hyssdbg_compile) {
					hyssdbg_compile_stdin(backup_hyssdbg_compile);
				} else {
					hyssdbg_compile();
				}
			} gear_end_try();
			backup_hyssdbg_compile = NULL;

			HYSSDBG_G(flags) &= ~HYSSDBG_DISCARD_OUTPUT;
		}

		if (bp_tmp) {
			HYSSDBG_G(flags) |= HYSSDBG_DISCARD_OUTPUT | HYSSDBG_IS_INITIALIZING;
			hyssdbg_string_init(bp_tmp);
			free(bp_tmp);
			bp_tmp = NULL;
			HYSSDBG_G(flags) &= ~HYSSDBG_DISCARD_OUTPUT & ~HYSSDBG_IS_INITIALIZING;
		}

		if (settings == (void *) 0x1) {
			if (HYSSDBG_G(ops)) {
				hyssdbg_print_opcodes(print_opline_func);
			} else {
				gear_quiet_write(HYSSDBG_G(io)[HYSSDBG_STDERR].fd, GEAR_STRL("No opcodes could be compiled | No file specified or compilation failed?\n"));
			}
			goto hyssdbg_out;
		}

		PG(during_request_startup) = 0;

		hyssdbg_fully_started = 1;

/* #ifndef for making compiler shutting up */
#ifndef _WIN32
hyssdbg_interact:
#endif
		/* hyssdbg main() */
		do {
			gear_try {
				if (hyssdbg_startup_run) {
					hyssdbg_startup_run = 0;
					if (quit_immediately) {
						HYSSDBG_G(flags) = (HYSSDBG_G(flags) & ~HYSSDBG_HAS_PAGINATION) | HYSSDBG_IS_INTERACTIVE | HYSSDBG_PREVENT_INTERACTIVE;
					} else {
						HYSSDBG_G(flags) |= HYSSDBG_IS_INTERACTIVE;
					}
					gear_try {
						if (first_command) {
							hyssdbg_interactive(1, estrdup(first_command));
						} else {
							HYSSDBG_COMMAND_HANDLER(run)(NULL);
						}
					} gear_end_try();
					if (quit_immediately) {
						/* if -r is on the command line more than once just quit */
						EG(bailout) = __orig_bailout; /* reset gear_try */
						exit_status = EG(exit_status);
						break;
					}
				}

				CG(unclean_shutdown) = 0;
				hyssdbg_interactive(1, NULL);
			} gear_catch {
				if ((HYSSDBG_G(flags) & HYSSDBG_IS_CLEANING)) {
					char *bp_tmp_str;
					HYSSDBG_G(flags) |= HYSSDBG_DISCARD_OUTPUT;
					hyssdbg_export_breakpoints_to_string(&bp_tmp_str);
					HYSSDBG_G(flags) &= ~HYSSDBG_DISCARD_OUTPUT;
					if (bp_tmp_str) {
						bp_tmp = strdup(bp_tmp_str);
						efree(bp_tmp_str);
					}
					cleaning = 1;
				} else {
					cleaning = 0;
				}

#ifndef _WIN32
				if (!cleaning) {
					/* remote client disconnected */
					if ((HYSSDBG_G(flags) & HYSSDBG_IS_DISCONNECTED)) {

						if (HYSSDBG_G(flags) & HYSSDBG_IS_REMOTE) {
							/* renegociate connections */
							hyssdbg_remote_init(address, listen, server, &socket, &stream);

							/* set streams */
							if (stream) {
								HYSSDBG_G(flags) &= ~HYSSDBG_IS_QUITTING;
							}

							/* this must be forced */
							CG(unclean_shutdown) = 0;
						} else {
							/* local consoles cannot disconnect, ignore EOF */
							HYSSDBG_G(flags) &= ~HYSSDBG_IS_DISCONNECTED;
						}
					}
				}
#endif
			} gear_end_try();
		} while (!(HYSSDBG_G(flags) & HYSSDBG_IS_STOPPING));


#ifndef _WIN32
hyssdbg_out:
		if ((HYSSDBG_G(flags) & HYSSDBG_IS_DISCONNECTED)) {
			HYSSDBG_G(flags) &= ~HYSSDBG_IS_DISCONNECTED;
			goto hyssdbg_interact;
		}
#endif

#ifdef _WIN32
	} __except(hyssdbg_exception_handler_win32(xp = GetExceptionInformation())) {
		hyssdbg_error("segfault", "", "Access violation (Segmentation fault) encountered\ntrying to abort cleanly...");
	}
hyssdbg_out:
#endif

		if (first_command) {
			free(first_command);
			first_command = NULL;
		}

		if (cleaning <= 0) {
			HYSSDBG_G(flags) &= ~HYSSDBG_IS_CLEANING;
			cleaning = -1;
		}

		{
			int i;
			/* free argv */
			for (i = SG(request_info).argc; i--;) {
				efree(SG(request_info).argv[i]);
			}
			efree(SG(request_info).argv);
		}

		if (ics_entries) {
			free(ics_entries);
		}

		if (ics_override) {
			free(ics_override);
		}

		/* In case we aborted during script execution, we may not reset CG(unclean_shutdown) */
		if (!(HYSSDBG_G(flags) & HYSSDBG_IS_RUNNING)) {
			is_exit = !HYSSDBG_G(in_execution);
			CG(unclean_shutdown) = is_exit || HYSSDBG_G(unclean_eval);
		}

		if ((HYSSDBG_G(flags) & (HYSSDBG_IS_CLEANING | HYSSDBG_IS_RUNNING)) == HYSSDBG_IS_CLEANING) {
			hyss_free_shutdown_functions();
			gear_objects_store_mark_destructed(&EG(objects_store));
		}

		gear_try {
			hyss_request_shutdown(NULL);
		} gear_end_try();

		if (HYSSDBG_G(exec) && strcmp("Standard input code", HYSSDBG_G(exec)) == SUCCESS) { /* i.e. execution context has been read from stdin - back it up */
			hyssdbg_file_source *data = gear_hash_str_find_ptr(&HYSSDBG_G(file_sources), HYSSDBG_G(exec), HYSSDBG_G(exec_len));
			backup_hyssdbg_compile = gear_string_alloc(data->len + 2, 1);
			GC_MAKE_PERSISTENT_LOCAL(backup_hyssdbg_compile);
			sprintf(ZSTR_VAL(backup_hyssdbg_compile), "!@%.*s", (int) data->len, data->buf);
		}

		/* backup globals when cleaning */
		if ((cleaning > 0 || remote) && !quit_immediately) {
			settings = calloc(1, sizeof(gear_hyssdbg_globals));

			hyss_hyssdbg_globals_ctor(settings);

			if (HYSSDBG_G(exec)) {
				settings->exec = gear_strndup(HYSSDBG_G(exec), HYSSDBG_G(exec_len));
				settings->exec_len = HYSSDBG_G(exec_len);
			}
			settings->oplog = HYSSDBG_G(oplog);
			settings->prompt[0] = HYSSDBG_G(prompt)[0];
			settings->prompt[1] = HYSSDBG_G(prompt)[1];
			memcpy(settings->colors, HYSSDBG_G(colors), sizeof(settings->colors));
			settings->eol = HYSSDBG_G(eol);
			settings->input_buflen = HYSSDBG_G(input_buflen);
			memcpy(settings->input_buffer, HYSSDBG_G(input_buffer), settings->input_buflen);
			settings->flags = HYSSDBG_G(flags) & HYSSDBG_PRESERVE_FLAGS_MASK;
			first_command = HYSSDBG_G(cur_command);
		} else {
			if (HYSSDBG_G(prompt)[0]) {
				free(HYSSDBG_G(prompt)[0]);
			}
			if (HYSSDBG_G(prompt)[1]) {
				free(HYSSDBG_G(prompt)[1]);
			}
			if (HYSSDBG_G(cur_command)) {
				free(HYSSDBG_G(cur_command));
			}
		}

		if (exit_status == 0) {
			exit_status = EG(exit_status);
		}

		hyss_output_deactivate();

		if (!(HYSSDBG_G(flags) & HYSSDBG_IS_QUITTING)) {
			HYSSDBG_G(flags) |= HYSSDBG_IS_QUITTING;
			if (HYSSDBG_G(in_execution) || is_exit) {
				if (!quit_immediately && !hyssdbg_startup_run) {
					HYSSDBG_G(flags) -= HYSSDBG_IS_QUITTING;
					cleaning++;
				}
			}
		}

		{
			zval *zv = gear_hash_str_find(hyss_stream_get_url_stream_wrappers_hash(), GEAR_STRL("hyss"));
			Z_PTR_P(zv) = (void*)HYSSDBG_G(orig_url_wrap_hyss);
		}

		gear_hash_destroy(&HYSSDBG_G(file_sources));

		gear_try {
			hyss_capi_shutdown();
		} gear_end_try();

#ifndef _WIN32
		/* force override (no gear_signals) to prevent crashes due to signal recursion in SIGSEGV/SIGBUS handlers */
		signal(SIGSEGV, SIG_DFL);
		signal(SIGBUS, SIG_DFL);

		/* reset it... else we risk a stack overflow upon next run (when clean'ing) */
		hyss_stream_stdio_ops.write = HYSSDBG_G(hyss_stdiop_write);
#endif
	}

	sapi_shutdown();

	if (sapi_name) {
		free(sapi_name);
	}

free_and_return:
	if (read_from_stdin) {
		free(read_from_stdin);
		read_from_stdin = NULL;
	}

#ifdef ZTS
	/* reset to original handlers - otherwise HYSSDBG_G() in hyssdbg_watch_efree will be segfaulty (with e.g. USE_GEAR_ALLOC=0) */
	if (!use_mm_wrappers) {
		gear_mm_set_custom_handlers(gear_mm_get_heap(), _malloc, _free, _realloc);
	}

	ts_free_id(hyssdbg_globals_id);

	pbc_shutdown();
#endif

	if ((cleaning > 0 || remote) && !quit_immediately) {
		/* reset internal hyss_getopt state */
		hyss_getopt(-1, argv, OPTIONS, NULL, &hyss_optind, 0, 0);

		goto hyssdbg_main;
	}

	if (backup_hyssdbg_compile) {
		gear_string_free(backup_hyssdbg_compile);
	}

#ifndef _WIN32
	if (address) {
		free(address);
	}
#endif

	/* usually 0; just for -rr */
	return exit_status;
} /* }}} */
