hyssdbg: $(BUILD_BINARY)

hyssdbg-shared: $(BUILD_SHARED)

$(BUILD_SHARED): $(HYSS_GLOBAL_OBJS) $(HYSS_BINARY_OBJS) $(HYSS_HYSSDBG_OBJS)
	$(BUILD_HYSSDBG_SHARED)

$(BUILD_BINARY): $(HYSS_GLOBAL_OBJS) $(HYSS_BINARY_OBJS) $(HYSS_HYSSDBG_OBJS)
	$(BUILD_HYSSDBG)

%.c: %.y
%.c: %.l

$(builddir)/hyssdbg_lexer.lo: $(srcdir)/hyssdbg_parser.h

$(srcdir)/hyssdbg_lexer.c: $(srcdir)/hyssdbg_lexer.l
	@(cd $(top_srcdir); $(RE2C) $(RE2C_FLAGS) --no-generation-date -cbdFo server/hyssdbg/hyssdbg_lexer.c server/hyssdbg/hyssdbg_lexer.l)

$(srcdir)/hyssdbg_parser.h: $(srcdir)/hyssdbg_parser.c
$(srcdir)/hyssdbg_parser.c: $(srcdir)/hyssdbg_parser.y
	@$(YACC) -p hyssdbg_ -v -d $(srcdir)/hyssdbg_parser.y -o $@

install-hyssdbg: $(BUILD_BINARY)
	@echo "Installing hyssdbg binary:         $(INSTALL_ROOT)$(bindir)/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(bindir)
	@$(mkinstalldirs) $(INSTALL_ROOT)$(localstatedir)/log
	@$(mkinstalldirs) $(INSTALL_ROOT)$(localstatedir)/run
	@$(INSTALL) -m 0755 $(BUILD_BINARY) $(INSTALL_ROOT)$(bindir)/$(program_prefix)hyssdbg$(program_suffix)$(EXEEXT)
	@echo "Installing hyssdbg man page:       $(INSTALL_ROOT)$(mandir)/man1/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(mandir)/man1
	@$(INSTALL_DATA) server/hyssdbg/hyssdbg.1 $(INSTALL_ROOT)$(mandir)/man1/$(program_prefix)hyssdbg$(program_suffix).1

clean-hyssdbg:
	@echo "Cleaning hyssdbg object files ..."
	find server/hyssdbg/ -name *.lo -o -name *.o | xargs rm -f

test-hyssdbg:
	@echo "Running hyssdbg tests ..."
	@$(top_builddir)/server/cli/hyss server/hyssdbg/tests/run-tests.hyss --hyssdbg server/hyssdbg/hyssdbg

.PHONY: clean-hyssdbg test-hyssdbg
