.TH @program_prefix@hyssdbg 1 "2020" "The Hyang Language Foundation" "Scripting Language"
.SH NAME
@program_prefix@hyssdbg \- The interactive HYSS debugger
.SH SYNOPSIS
.B @program_prefix@hyssdbg
[options]
[\fIfile\fP]
[\fIargs...\fP]
.SH DESCRIPTION
.B hyssdbg
is a lightweight, powerful, easy to use debugging platform for HYSS.
.SH OPTIONS
.TP 15
.B \-c \fIpath\fB|\fIfile\fR
Look for
.B hyss.ics
file in the directory
.IR path
or use the specified
.IR file
.TP
.B \-d \fIfoo\fP[=\fIbar\fP]
Define ICS entry
.IR foo
with value
.IR bar
.TP
.B \-n
No
.B hyss.ics
file will be used
.TP
.B \-z \fIfile\fR
Load Gear extension
.IR file
.TP
.BR \-q
Do not print banner on startup
.TP
.B \-v
Enable oplog output
.TP
.B \-b
Disables use of color on the console
.TP
.B \-i \fIpath\fB|\fIfile\fR
Override .hyssgdbinit location (implies -I)
.TP
.B \-I
Ignore .hyssdbginit (default init file)
.TP
.B \-O \fIfile\fR
Set oplog output to
.IR file
.TP
.B \-r
Jump straight to run
.TP
.B -e
Generate extended information for debugger/profiler
.TP
.B \-E
Enable step through eval()
.TP
.B \-s \fIdelimiter\fP
Read code to execute from stdin with an optional
.IR delimiter
.TP
.B \-S \fIsapi_name\fP
Override SAPI name
.TP
.B \-l \fIport\fP
Setup remote console port
.TP
.B \-a \fIaddress\fP
Setup remote console bind address
.TP
.B \-x
Enable XML output
.TP
.B \-p \fIopcode\fP
Output opcodes and quit
.TP
.B \-h
Print the help overview
.TP
.B \-V
Version number
.TP
.IR args.\|.\|.
Arguments passed to script. Use
.B '\-\-'
.IR args
when first argument starts with
.B '\-'
or script is read from stdin
.SH NOTES
Passing
.B \-rr
will cause
.B hyssdbg
to quit after execution, rather than returning to the console
.SH FILES
.TP 15
.B hyss.ics
The standard configuration file
.TP
.B .hyssdbginit
The init file
.SH SEE ALSO
The online manual can be found at
.PD 0
.P
.B http://hyss.hyang.org/manual/book.hyssdbg.hyss
.PD 1
.SH BUGS
You can view the list of known bugs or report any new bug you
found at
.PD 0
.P
.B http://hyss.hyang.org/bugs/
.PD 1
.SH AUTHORS
Written by M. Sindu Natama.
.P
A List of active developers can be found at
.PD 0
.P
.B http://hyss.hyang.org/credits.hyss
.PD 1
.P
And last but not least HYSS was developed with the help of a huge amount of
contributors all around the world.
.SH VERSION INFORMATION
This manpage describes \fBhyssdbg\fP, for HYSS version @HYSS_VERSION@.
.SH COPYRIGHT
Copyright \(co 2019\-2020 The Hyang Language Foundation
.LP
This source file is subject to the GNU GPL version 3 license,
that is bundled with this package in the file LICENSE, and is
available through the world-wide-web at the following url:
.PD 0
.P
.B http://hyss.hyang.org/license.hyss
.PD 1
.P
If you did not receive a copy of the license and are unable to
obtain it through the world-wide-web, please send a note to
.B hyangcontrib@gmail.com
so we can mail you a copy immediately.
