/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyssdbg.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_info.h"
#include "hyssdbg_bp.h"
#include "hyssdbg_prompt.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

#define HYSSDBG_INFO_COMMAND_D(f, h, a, m, l, s, flags) \
	HYSSDBG_COMMAND_D_EXP(f, h, a, m, l, s, &hyssdbg_prompt_commands[13], flags)

const hyssdbg_command_t hyssdbg_info_commands[] = {
	HYSSDBG_INFO_COMMAND_D(break,     "show breakpoints",              'b', info_break,     NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(files,     "show included files",           'F', info_files,     NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(classes,   "show loaded classes",           'c', info_classes,   NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(funcs,     "show loaded classes",           'f', info_funcs,     NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(error,     "show last error",               'e', info_error,     NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(constants, "show user defined constants",   'd', info_constants, NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(vars,      "show active variables",         'v', info_vars,      NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(globals,   "show superglobals",             'g', info_globals,   NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(literal,   "show active literal constants", 'l', info_literal,   NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_INFO_COMMAND_D(memory,    "show memory manager stats",     'm', info_memory,    NULL, 0, HYSSDBG_ASYNC_SAFE),
	HYSSDBG_END_COMMAND
};

HYSSDBG_INFO(break) /* {{{ */
{
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_FILE);
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_SYM);
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_METHOD);
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_OPLINE);
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_FILE_OPLINE);
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_FUNCTION_OPLINE);
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_METHOD_OPLINE);
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_COND);
	hyssdbg_print_breakpoints(HYSSDBG_BREAK_OPCODE);

	return SUCCESS;
} /* }}} */

HYSSDBG_INFO(files) /* {{{ */
{
	gear_string *fname;

	hyssdbg_try_access {
		hyssdbg_notice("includedfilecount", "num=\"%d\"", "Included files: %d", gear_hash_num_elements(&EG(included_files)));
	} hyssdbg_catch_access {
		hyssdbg_error("signalsegv", "", "Could not fetch included file count, invalid data source");
		return SUCCESS;
	} hyssdbg_end_try_access();

	hyssdbg_try_access {
		GEAR_HASH_FOREACH_STR_KEY(&EG(included_files), fname) {
			hyssdbg_writeln("includedfile", "name=\"%s\"", "File: %s", ZSTR_VAL(fname));
		} GEAR_HASH_FOREACH_END();
	} hyssdbg_catch_access {
		hyssdbg_error("signalsegv", "", "Could not fetch file name, invalid data source, aborting included file listing");
	} hyssdbg_end_try_access();

	return SUCCESS;
} /* }}} */

HYSSDBG_INFO(error) /* {{{ */
{
	if (PG(last_error_message)) {
		hyssdbg_try_access {
			hyssdbg_writeln("lasterror", "error=\"%s\" file=\"%s\" line=\"%d\"", "Last error: %s at %s line %d", PG(last_error_message), PG(last_error_file), PG(last_error_lineno));
		} hyssdbg_catch_access {
			hyssdbg_notice("lasterror", "error=\"\"", "No error found!");
		} hyssdbg_end_try_access();
	} else {
		hyssdbg_notice("lasterror", "error=\"\"", "No error found!");
	}
	return SUCCESS;
} /* }}} */

HYSSDBG_INFO(constants) /* {{{ */
{
	HashTable consts;
	gear_constant *data;

	gear_hash_init(&consts, 8, NULL, NULL, 0);

	if (EG(gear_constants)) {
		hyssdbg_try_access {
			GEAR_HASH_FOREACH_PTR(EG(gear_constants), data) {
				if (GEAR_CONSTANT_CAPI_NUMBER(data) == HYSS_USER_CONSTANT) {
					gear_hash_update_ptr(&consts, data->name, data);
				}
			} GEAR_HASH_FOREACH_END();
		} hyssdbg_catch_access {
			hyssdbg_error("signalsegv", "", "Cannot fetch all the constants, invalid data source");
		} hyssdbg_end_try_access();
	}

	hyssdbg_notice("constantinfo", "num=\"%d\"", "User-defined constants (%d)", gear_hash_num_elements(&consts));

	if (gear_hash_num_elements(&consts)) {
		hyssdbg_out("Address            Refs    Type      Constant\n");
		GEAR_HASH_FOREACH_PTR(&consts, data) {

#define VARIABLEINFO(attrs, msg, ...) \
	hyssdbg_writeln("constant", \
		"address=\"%p\" refcount=\"%d\" type=\"%s\" name=\"%.*s\" " attrs, \
		"%-18p %-7d %-9s %.*s" msg, &data->value, \
		Z_REFCOUNTED(data->value) ? Z_REFCOUNT(data->value) : 1, \
		gear_zval_type_name(&data->value), \
		(int) ZSTR_LEN(data->name), ZSTR_VAL(data->name), ##__VA_ARGS__)

			switch (Z_TYPE(data->value)) {
				case IS_STRING:
					hyssdbg_try_access {
						VARIABLEINFO("length=\"%zd\" value=\"%.*s\"", "\nstring (%zd) \"%.*s%s\"", Z_STRLEN(data->value), Z_STRLEN(data->value) < 255 ? (int) Z_STRLEN(data->value) : 255, Z_STRVAL(data->value), Z_STRLEN(data->value) > 255 ? "..." : "");
					} hyssdbg_catch_access {
						VARIABLEINFO("", "");
					} hyssdbg_end_try_access();
					break;
				case IS_TRUE:
					VARIABLEINFO("value=\"true\"", "\nbool (true)");
					break;
				case IS_FALSE:
					VARIABLEINFO("value=\"false\"", "\nbool (false)");
					break;
				case IS_LONG:
					VARIABLEINFO("value=\"%ld\"", "\nint (%ld)", Z_LVAL(data->value));
					break;
				case IS_DOUBLE:
					VARIABLEINFO("value=\"%lf\"", "\ndouble (%lf)", Z_DVAL(data->value));
					break;
				default:
					VARIABLEINFO("", "");

#undef VARIABLEINFO
			}
		} GEAR_HASH_FOREACH_END();
	}

	return SUCCESS;
} /* }}} */

static int hyssdbg_arm_auto_global(zval *ptrzv) {
	gear_auto_global *auto_global = Z_PTR_P(ptrzv);

	if (auto_global->armed) {
		if (HYSSDBG_G(flags) & HYSSDBG_IN_SIGNAL_HANDLER) {
			hyssdbg_notice("variableinfo", "unreachable=\"%.*s\"", "Cannot show information about superglobal variable %.*s", (int) ZSTR_LEN(auto_global->name), ZSTR_VAL(auto_global->name));
		} else {
			auto_global->armed = auto_global->auto_global_callback(auto_global->name);
		}
	}

	return 0;
}

static int hyssdbg_print_symbols(gear_bool show_globals) {
	HashTable vars;
	gear_array *symtable;
	gear_string *var;
	zval *data;

	if (!EG(current_execute_data) || !EG(current_execute_data)->func) {
		hyssdbg_error("inactive", "type=\"op_array\"", "No active op array!");
		return SUCCESS;
	}

	if (show_globals) {
		/* that array should only be manipulated during init, so safe for async access during execution */
		gear_hash_apply(CG(auto_globals), (apply_func_t) hyssdbg_arm_auto_global);
		symtable = &EG(symbol_table);
	} else if (!(symtable = gear_rebuild_symbol_table())) {
		hyssdbg_error("inactive", "type=\"symbol_table\"", "No active symbol table!");
		return SUCCESS;
	}

	gear_hash_init(&vars, 8, NULL, NULL, 0);

	hyssdbg_try_access {
		GEAR_HASH_FOREACH_STR_KEY_VAL(symtable, var, data) {
			if (gear_is_auto_global(var) ^ !show_globals) {
				gear_hash_update(&vars, var, data);
			}
		} GEAR_HASH_FOREACH_END();
	} hyssdbg_catch_access {
		hyssdbg_error("signalsegv", "", "Cannot fetch all data from the symbol table, invalid data source");
	} hyssdbg_end_try_access();

	if (show_globals) {
		hyssdbg_notice("variableinfo", "num=\"%d\"", "Superglobal variables (%d)", gear_hash_num_elements(&vars));
	} else {
		gear_op_array *ops = &EG(current_execute_data)->func->op_array;

		if (ops->function_name) {
			if (ops->scope) {
				hyssdbg_notice("variableinfo", "method=\"%s::%s\" num=\"%d\"", "Variables in %s::%s() (%d)", ops->scope->name->val, ops->function_name->val, gear_hash_num_elements(&vars));
			} else {
				hyssdbg_notice("variableinfo", "function=\"%s\" num=\"%d\"", "Variables in %s() (%d)", ZSTR_VAL(ops->function_name), gear_hash_num_elements(&vars));
			}
		} else {
			if (ops->filename) {
				hyssdbg_notice("variableinfo", "file=\"%s\" num=\"%d\"", "Variables in %s (%d)", ZSTR_VAL(ops->filename), gear_hash_num_elements(&vars));
			} else {
				hyssdbg_notice("variableinfo", "opline=\"%p\" num=\"%d\"", "Variables @ %p (%d)", ops, gear_hash_num_elements(&vars));
			}
		}
	}

	if (gear_hash_num_elements(&vars)) {
		hyssdbg_out("Address            Refs    Type      Variable\n");
		GEAR_HASH_FOREACH_STR_KEY_VAL(&vars, var, data) {
			hyssdbg_try_access {
				const char *isref = "";
#define VARIABLEINFO(attrs, msg, ...) \
	hyssdbg_writeln("variable", \
		"address=\"%p\" refcount=\"%d\" type=\"%s\" refstatus=\"%s\" name=\"%.*s\" " attrs, \
		"%-18p %-7d %-9s %s$%.*s" msg, data, Z_REFCOUNTED_P(data) ? Z_REFCOUNT_P(data) : 1, gear_zval_type_name(data), isref, (int) ZSTR_LEN(var), ZSTR_VAL(var), ##__VA_ARGS__)
retry_switch:
				switch (Z_TYPE_P(data)) {
					case IS_RESOURCE:
						hyssdbg_try_access {
							const char *type = gear_rsrc_list_get_rsrc_type(Z_RES_P(data));
							VARIABLEINFO("type=\"%s\"", "\n|-------(typeof)------> (%s)\n", type ? type : "unknown");
						} hyssdbg_catch_access {
							VARIABLEINFO("type=\"unknown\"", "\n|-------(typeof)------> (unknown)\n");
						} hyssdbg_end_try_access();
						break;
					case IS_OBJECT:
						hyssdbg_try_access {
							VARIABLEINFO("instanceof=\"%s\"", "\n|-----(instanceof)----> (%s)\n", ZSTR_VAL(Z_OBJCE_P(data)->name));
						} hyssdbg_catch_access {
							VARIABLEINFO("instanceof=\"%s\"", "\n|-----(instanceof)----> (unknown)\n");
						} hyssdbg_end_try_access();
						break;
					case IS_STRING:
						hyssdbg_try_access {
							VARIABLEINFO("length=\"%zd\" value=\"%.*s\"", "\nstring (%zd) \"%.*s%s\"", Z_STRLEN_P(data), Z_STRLEN_P(data) < 255 ? (int) Z_STRLEN_P(data) : 255, Z_STRVAL_P(data), Z_STRLEN_P(data) > 255 ? "..." : "");
						} hyssdbg_catch_access {
							VARIABLEINFO("", "");
						} hyssdbg_end_try_access();
						break;
					case IS_TRUE:
						VARIABLEINFO("value=\"true\"", "\nbool (true)");
						break;
					case IS_FALSE:
						VARIABLEINFO("value=\"false\"", "\nbool (false)");
						break;
					case IS_LONG:
						VARIABLEINFO("value=\"%ld\"", "\nint (%ld)", Z_LVAL_P(data));
						break;
					case IS_DOUBLE:
						VARIABLEINFO("value=\"%lf\"", "\ndouble (%lf)", Z_DVAL_P(data));
						break;
					case IS_REFERENCE:
						isref = "&";
						data = Z_REFVAL_P(data);
						goto retry_switch;
					case IS_INDIRECT:
						data = Z_INDIRECT_P(data);
						goto retry_switch;
					default:
						VARIABLEINFO("", "");
				}

#undef VARIABLEINFO
			} hyssdbg_catch_access {
				hyssdbg_writeln("variable", "address=\"%p\" name=\"%s\"", "%p\tn/a\tn/a\t$%s", data, ZSTR_VAL(var));
			} hyssdbg_end_try_access();
		} GEAR_HASH_FOREACH_END();
	}

	gear_hash_destroy(&vars);

	return SUCCESS;
} /* }}} */

HYSSDBG_INFO(vars) /* {{{ */
{
	return hyssdbg_print_symbols(0);
}

HYSSDBG_INFO(globals) /* {{{ */
{
	return hyssdbg_print_symbols(1);
}

HYSSDBG_INFO(literal) /* {{{ */
{
	/* literals are assumed to not be manipulated during executing of their op_array and as such async safe */
	gear_bool in_executor = HYSSDBG_G(in_execution) && EG(current_execute_data) && EG(current_execute_data)->func;
	if (in_executor || HYSSDBG_G(ops)) {
		gear_op_array *ops = in_executor ? &EG(current_execute_data)->func->op_array : HYSSDBG_G(ops);
		int literal = 0, count = ops->last_literal - 1;

		if (ops->function_name) {
			if (ops->scope) {
				hyssdbg_notice("literalinfo", "method=\"%s::%s\" num=\"%d\"", "Literal Constants in %s::%s() (%d)", ops->scope->name->val, ops->function_name->val, count);
			} else {
				hyssdbg_notice("literalinfo", "function=\"%s\" num=\"%d\"", "Literal Constants in %s() (%d)", ops->function_name->val, count);
			}
		} else {
			if (ops->filename) {
				hyssdbg_notice("literalinfo", "file=\"%s\" num=\"%d\"", "Literal Constants in %s (%d)", ZSTR_VAL(ops->filename), count);
			} else {
				hyssdbg_notice("literalinfo", "opline=\"%p\" num=\"%d\"", "Literal Constants @ %p (%d)", ops, count);
			}
		}

		while (literal < ops->last_literal) {
			if (Z_TYPE(ops->literals[literal]) != IS_NULL) {
				hyssdbg_write("literal", "id=\"%u\"", "|-------- C%u -------> [", literal);
				gear_print_zval(&ops->literals[literal], 0);
				hyssdbg_out("]\n");
			}
			literal++;
		}
	} else {
		hyssdbg_error("inactive", "type=\"execution\"", "Not executing!");
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_INFO(memory) /* {{{ */
{
	size_t used, real, peak_used, peak_real;
	gear_mm_heap *heap;
	gear_bool is_mm;

	if (HYSSDBG_G(flags) & HYSSDBG_IN_SIGNAL_HANDLER) {
		heap = gear_mm_set_heap(hyssdbg_original_heap_sigsafe_mem());
	}
	if ((is_mm = is_gear_mm())) {
		used = gear_memory_usage(0);
		real = gear_memory_usage(1);
		peak_used = gear_memory_peak_usage(0);
		peak_real = gear_memory_peak_usage(1);
	}
	if (HYSSDBG_G(flags) & HYSSDBG_IN_SIGNAL_HANDLER) {
		gear_mm_set_heap(heap);
	}

	if (is_mm) {
		hyssdbg_notice("meminfo", "", "Memory Manager Information");
		hyssdbg_notice("current", "", "Current");
		hyssdbg_writeln("used", "mem=\"%.3f\"", "|-------> Used:\t%.3f kB", (float) (used / 1024));
		hyssdbg_writeln("real", "mem=\"%.3f\"", "|-------> Real:\t%.3f kB", (float) (real / 1024));
		hyssdbg_notice("peak", "", "Peak");
		hyssdbg_writeln("used", "mem=\"%.3f\"", "|-------> Used:\t%.3f kB", (float) (peak_used / 1024));
		hyssdbg_writeln("real", "mem=\"%.3f\"", "|-------> Real:\t%.3f kB", (float) (peak_real / 1024));
	} else {
		hyssdbg_error("inactive", "type=\"memory_manager\"", "Memory Manager Disabled!");
	}
	return SUCCESS;
} /* }}} */

static inline void hyssdbg_print_class_name(gear_class_entry *ce) /* {{{ */
{
	const char *visibility = ce->type == GEAR_USER_CLASS ? "User" : "Internal";
	const char *type = (ce->ce_flags & GEAR_ACC_INTERFACE) ? "Interface" : (ce->ce_flags & GEAR_ACC_ABSTRACT) ? "Abstract Class" : "Class";

	hyssdbg_writeln("class", "type=\"%s\" flags=\"%s\" name=\"%.*s\" methodcount=\"%d\"", "%s %s %.*s (%d)", visibility, type, (int) ZSTR_LEN(ce->name), ZSTR_VAL(ce->name), gear_hash_num_elements(&ce->function_table));
} /* }}} */

HYSSDBG_INFO(classes) /* {{{ */
{
	gear_class_entry *ce;
	HashTable classes;

	gear_hash_init(&classes, 8, NULL, NULL, 0);

	hyssdbg_try_access {
		GEAR_HASH_FOREACH_PTR(EG(class_table), ce) {
			if (ce->type == GEAR_USER_CLASS) {
				gear_hash_next_index_insert_ptr(&classes, ce);
			}
		} GEAR_HASH_FOREACH_END();
	} hyssdbg_catch_access {
		hyssdbg_notice("signalsegv", "", "Not all classes could be fetched, possibly invalid data source");
	} hyssdbg_end_try_access();

	hyssdbg_notice("classinfo", "num=\"%d\"", "User Classes (%d)", gear_hash_num_elements(&classes));

	/* once added, assume that classes are stable... until shutdown. */
	GEAR_HASH_FOREACH_PTR(&classes, ce) {
		hyssdbg_print_class_name(ce);

		if (ce->parent) {
			gear_class_entry *pce;
			hyssdbg_xml("<parents %r>");
			pce = ce->parent;
			do {
				hyssdbg_out("|-------- ");
				hyssdbg_print_class_name(pce);
			} while ((pce = pce->parent));
			hyssdbg_xml("</parents>");
		}

		if (ce->info.user.filename) {
			hyssdbg_writeln("classsource", "file=\"%s\" line=\"%u\"", "|---- in %s on line %u", ZSTR_VAL(ce->info.user.filename), ce->info.user.line_start);
		} else {
			hyssdbg_writeln("classsource", "", "|---- no source code");
		}
	} GEAR_HASH_FOREACH_END();

	gear_hash_destroy(&classes);

	return SUCCESS;
} /* }}} */

HYSSDBG_INFO(funcs) /* {{{ */
{
	gear_function *zf;
	HashTable functions;

	gear_hash_init(&functions, 8, NULL, NULL, 0);

	hyssdbg_try_access {
		GEAR_HASH_FOREACH_PTR(EG(function_table), zf) {
			if (zf->type == GEAR_USER_FUNCTION) {
				gear_hash_next_index_insert_ptr(&functions, zf);
			}
		} GEAR_HASH_FOREACH_END();
	} hyssdbg_catch_access {
		hyssdbg_notice("signalsegv", "", "Not all functions could be fetched, possibly invalid data source");
	} hyssdbg_end_try_access();

	hyssdbg_notice("functioninfo", "num=\"%d\"", "User Functions (%d)", gear_hash_num_elements(&functions));

	GEAR_HASH_FOREACH_PTR(&functions, zf) {
		gear_op_array *op_array = &zf->op_array;

		hyssdbg_write("function", "name=\"%s\"", "|-------- %s", op_array->function_name ? ZSTR_VAL(op_array->function_name) : "{main}");

		if (op_array->filename) {
			hyssdbg_writeln("functionsource", "file=\"%s\" line=\"%d\"", " in %s on line %d", ZSTR_VAL(op_array->filename), op_array->line_start);
		} else {
			hyssdbg_writeln("functionsource", "", " (no source code)");
		}
	} GEAR_HASH_FOREACH_END();

	gear_hash_destroy(&functions);

	return SUCCESS;
} /* }}} */
