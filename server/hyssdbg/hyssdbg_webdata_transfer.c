/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg_webdata_transfer.h"
#include "extslib/standard/hyss_var.h"

static int hyssdbg_is_auto_global(char *name, int len) {
	int ret;
	gear_string *str = gear_string_init(name, len, 0);
	ret = gear_is_auto_global(str);
	gear_string_free(str);
	return ret;
}

HYSSDBG_API void hyssdbg_webdata_compress(char **msg, size_t *len) {
	zval array;
	HashTable *ht;
	zval zv[9] = {{{0}}};

	array_init(&array);
	ht = Z_ARRVAL(array);

	/* fetch superglobals */
	{
		hyssdbg_is_auto_global(GEAR_STRL("GLOBALS"));
		/* might be JIT */
		hyssdbg_is_auto_global(GEAR_STRL("_ENV"));
		hyssdbg_is_auto_global(GEAR_STRL("_SERVER"));
		hyssdbg_is_auto_global(GEAR_STRL("_REQUEST"));
		array_init(&zv[1]);
		gear_hash_copy(Z_ARRVAL(zv[1]), &EG(symbol_table), NULL);
		Z_ARRVAL(zv[1])->pDestructor = NULL; /* we're operating on a copy! Don't double free zvals */
		gear_hash_str_del(Z_ARRVAL(zv[1]), GEAR_STRL("GLOBALS")); /* do not use the reference to itself in json */
		gear_hash_str_add(ht, GEAR_STRL("GLOBALS"), &zv[1]);
	}

	/* save hyss://input */
	{
		hyss_stream *stream;
		gear_string *str;

		stream = hyss_stream_temp_create_ex(TEMP_STREAM_DEFAULT, SAPI_POST_BLOCK_SIZE, PG(upload_tmp_dir));
		if ((str = hyss_stream_copy_to_mem(stream, HYSS_STREAM_COPY_ALL, 0))) {
			ZVAL_STR(&zv[2], str);
		} else {
			ZVAL_EMPTY_STRING(&zv[2]);
		}
		Z_SET_REFCOUNT(zv[2], 1);
		gear_hash_str_add(ht, GEAR_STRL("input"), &zv[2]);
	}

	/* change sapi name */
	{
		if (sapi_capi.name) {
			ZVAL_STRING(&zv[6], sapi_capi.name);
		} else {
			Z_TYPE_INFO(zv[6]) = IS_NULL;
		}
		gear_hash_str_add(ht, GEAR_STRL("sapi_name"), &zv[6]);
		Z_SET_REFCOUNT(zv[6], 1);
	}

	/* handle cAPIs / extensions */
	{
		gear_capi_entry *cAPI;
		gear_extension *extension;
		gear_llist_position pos;

		array_init(&zv[7]);
		GEAR_HASH_FOREACH_PTR(&capi_registry, cAPI) {
			zval *value = ecalloc(sizeof(zval), 1);
			ZVAL_STRING(value, cAPI->name);
			gear_hash_next_index_insert(Z_ARRVAL(zv[7]), value);
		} GEAR_HASH_FOREACH_END();
		gear_hash_str_add(ht, GEAR_STRL("cAPIs"), &zv[7]);

		array_init(&zv[8]);
		extension = (gear_extension *) gear_llist_get_first_ex(&gear_extensions, &pos);
		while (extension) {
			zval *value = ecalloc(sizeof(zval), 1);
			ZVAL_STRING(value, extension->name);
			gear_hash_next_index_insert(Z_ARRVAL(zv[8]), value);
			extension = (gear_extension *) gear_llist_get_next_ex(&gear_extensions, &pos);
		}
		gear_hash_str_add(ht, GEAR_STRL("extensions"), &zv[8]);
	}

	/* switch cwd */
	if (SG(options) & SAPI_OPTION_NO_CHDIR) {
		char *ret = NULL;
		char path[MAXPATHLEN];

#if HAVE_GETCWD
		ret = VCWD_GETCWD(path, MAXPATHLEN);
#elif HAVE_GETWD
		ret = VCWD_GETWD(path);
#endif
		if (ret) {
			ZVAL_STRING(&zv[5], path);
			Z_SET_REFCOUNT(zv[5], 1);
			gear_hash_str_add(ht, GEAR_STRL("cwd"), &zv[5]);
		}
	}

	/* get system ics entries */
	{
		gear_ics_entry *ics_entry;

		array_init(&zv[3]);
		GEAR_HASH_FOREACH_PTR(EG(ics_directives), ics_entry) {
			zval *value = ecalloc(sizeof(zval), 1);
			if (ics_entry->modified) {
				if (!ics_entry->orig_value) {
					efree(value);
					continue;
				}
				ZVAL_STR(value, ics_entry->orig_value);
			} else {
				if (!ics_entry->value) {
					efree(value);
					continue;
				}
				ZVAL_STR(value, ics_entry->value);
			}
			gear_hash_add(Z_ARRVAL(zv[3]), ics_entry->name, value);
		} GEAR_HASH_FOREACH_END();
		gear_hash_str_add(ht, GEAR_STRL("systemini"), &zv[3]);
	}

	/* get perdir ics entries */
	if (EG(modified_ics_directives)) {
		gear_ics_entry *ics_entry;

		array_init(&zv[4]);
		GEAR_HASH_FOREACH_PTR(EG(ics_directives), ics_entry) {
			zval *value = ecalloc(sizeof(zval), 1);
			if (!ics_entry->value) {
				efree(value);
				continue;
			}
			ZVAL_STR(value, ics_entry->value);
			gear_hash_add(Z_ARRVAL(zv[4]), ics_entry->name, value);
		} GEAR_HASH_FOREACH_END();
		gear_hash_str_add(ht, GEAR_STRL("userini"), &zv[4]);
	}

	/* encode data */
	{
		hyss_serialize_data_t var_hash;
		smart_str buf = {0};

		HYSS_VAR_SERIALIZE_INIT(var_hash);
		hyss_var_serialize(&buf, &array, &var_hash);
		HYSS_VAR_SERIALIZE_DESTROY(var_hash);
		*msg = ZSTR_VAL(buf.s);
		*len = ZSTR_LEN(buf.s);
	}

	gear_array_destroy(Z_ARR(array));
}
