/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg.h"
#include "hyssdbg_cmd.h"
#include "hyssdbg_set.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_bp.h"
#include "hyssdbg_prompt.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

#define HYSSDBG_SET_COMMAND_D(f, h, a, m, l, s, flags) \
	HYSSDBG_COMMAND_D_EXP(f, h, a, m, l, s, &hyssdbg_prompt_commands[17], flags)

const hyssdbg_command_t hyssdbg_set_commands[] = {
	HYSSDBG_SET_COMMAND_D(prompt,       "usage: set prompt [<string>]",            'p', set_prompt,       NULL, "|s", 0),
	HYSSDBG_SET_COMMAND_D(pagination,   "usage: set pagination [<on|off>]",        'P', set_pagination,   NULL, "|b", HYSSDBG_ASYNC_SAFE),
#ifndef _WIN32
	HYSSDBG_SET_COMMAND_D(color,        "usage: set color  <element> <color>",     'c', set_color,        NULL, "ss", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_SET_COMMAND_D(colors,       "usage: set colors [<on|off>]",            'C', set_colors,       NULL, "|b", HYSSDBG_ASYNC_SAFE),
#endif
	HYSSDBG_SET_COMMAND_D(oplog,        "usage: set oplog  [<output>]",            'O', set_oplog,        NULL, "|s", 0),
	HYSSDBG_SET_COMMAND_D(break,        "usage: set break id [<on|off>]",          'b', set_break,        NULL, "l|b", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_SET_COMMAND_D(breaks,       "usage: set breaks [<on|off>]",            'B', set_breaks,       NULL, "|b", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_SET_COMMAND_D(quiet,        "usage: set quiet [<on|off>]",             'q', set_quiet,        NULL, "|b", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_SET_COMMAND_D(stepping,     "usage: set stepping [<line|op>]",         's', set_stepping,     NULL, "|s", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_SET_COMMAND_D(refcount,     "usage: set refcount [<on|off>]",          'r', set_refcount,     NULL, "|b", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_SET_COMMAND_D(lines,        "usage: set lines [<number>]",             'l', set_lines,        NULL, "|l", HYSSDBG_ASYNC_SAFE),
	HYSSDBG_END_COMMAND
};

HYSSDBG_SET(prompt) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_writeln("setprompt", "str=\"%s\"", "Current prompt: %s", hyssdbg_get_prompt());
	} else {
		hyssdbg_set_prompt(param->str);
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_SET(pagination) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_writeln("setpagination", "active=\"%s\"", "Pagination %s", HYSSDBG_G(flags) & HYSSDBG_HAS_PAGINATION ? "on" : "off");
	} else switch (param->type) {
		case NUMERIC_PARAM: {
			if (param->num) {
				HYSSDBG_G(flags) |= HYSSDBG_HAS_PAGINATION;
			} else {
				HYSSDBG_G(flags) &= ~HYSSDBG_HAS_PAGINATION;
			}
		} break;

		default:
			hyssdbg_error("setpagination", "type=\"wrongargs\"", "set pagination used incorrectly: set pagination <on|off>");
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_SET(lines) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_writeln("setlines", "active=\"%s\"", "Lines %ld", HYSSDBG_G(lines));
	} else switch (param->type) {
		case NUMERIC_PARAM: {
			HYSSDBG_G(lines) = param->num;
		} break;

		default:
			hyssdbg_error("setlines", "type=\"wrongargs\"", "set lines used incorrectly: set lines <number>");
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_SET(break) /* {{{ */
{
	switch (param->type) {
		case NUMERIC_PARAM: {
			if (param->next) {
				if (param->next->num) {
					hyssdbg_enable_breakpoint(param->num);
				} else {
					hyssdbg_disable_breakpoint(param->num);
				}
			} else {
				hyssdbg_breakbase_t *brake = hyssdbg_find_breakbase(param->num);
				if (brake) {
					hyssdbg_writeln("setbreak", "id=\"%ld\" active=\"%s\"", "Breakpoint #%ld %s", param->num, brake->disabled ? "off" : "on");
				} else {
					hyssdbg_error("setbreak", "type=\"nobreak\" id=\"%ld\"", "Failed to find breakpoint #%ld", param->num);
				}
			}
		} break;

		default:
			hyssdbg_error("setbreak", "type=\"wrongargs\"", "set break used incorrectly: set break [id] <on|off>");
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_SET(breaks) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_writeln("setbreaks", "active=\"%s\"", "Breakpoints %s",HYSSDBG_G(flags) & HYSSDBG_IS_BP_ENABLED ? "on" : "off");
	} else switch (param->type) {
		case NUMERIC_PARAM: {
			if (param->num) {
				hyssdbg_enable_breakpoints();
			} else {
				hyssdbg_disable_breakpoints();
			}
		} break;

		default:
			hyssdbg_error("setbreaks", "type=\"wrongargs\"", "set breaks used incorrectly: set breaks <on|off>");
	}

	return SUCCESS;
} /* }}} */

#ifndef _WIN32
HYSSDBG_SET(color) /* {{{ */
{
	const hyssdbg_color_t *color = hyssdbg_get_color(param->next->str, param->next->len);

	if (!color) {
		hyssdbg_error("setcolor", "type=\"nocolor\"", "Failed to find the requested color (%s)", param->next->str);
		return SUCCESS;
	}

	switch (hyssdbg_get_element(param->str, param->len)) {
		case HYSSDBG_COLOR_PROMPT:
			hyssdbg_notice("setcolor", "type=\"prompt\" color=\"%s\" code=\"%s\"", "setting prompt color to %s (%s)", color->name, color->code);
			if (HYSSDBG_G(prompt)[1]) {
				free(HYSSDBG_G(prompt)[1]);
				HYSSDBG_G(prompt)[1]=NULL;
			}
			hyssdbg_set_color(HYSSDBG_COLOR_PROMPT, color);
		break;

		case HYSSDBG_COLOR_ERROR:
			hyssdbg_notice("setcolor", "type=\"error\" color=\"%s\" code=\"%s\"", "setting error color to %s (%s)", color->name, color->code);
			hyssdbg_set_color(HYSSDBG_COLOR_ERROR, color);
		break;

		case HYSSDBG_COLOR_NOTICE:
			hyssdbg_notice("setcolor", "type=\"notice\" color=\"%s\" code=\"%s\"", "setting notice color to %s (%s)", color->name, color->code);
			hyssdbg_set_color(HYSSDBG_COLOR_NOTICE, color);
		break;

		default:
			hyssdbg_error("setcolor", "type=\"invalidtype\"", "Failed to find the requested element (%s)", param->str);
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_SET(colors) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_writeln("setcolors", "active=\"%s\"", "Colors %s", HYSSDBG_G(flags) & HYSSDBG_IS_COLOURED ? "on" : "off");
	} else switch (param->type) {
		case NUMERIC_PARAM: {
			if (param->num) {
				HYSSDBG_G(flags) |= HYSSDBG_IS_COLOURED;
			} else {
				HYSSDBG_G(flags) &= ~HYSSDBG_IS_COLOURED;
			}
		} break;

		default:
			hyssdbg_error("setcolors", "type=\"wrongargs\"", "set colors used incorrectly: set colors <on|off>");
	}

	return SUCCESS;
} /* }}} */
#endif

HYSSDBG_SET(oplog) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_notice("setoplog", "active=\"%s\"", "Oplog %s", HYSSDBG_G(oplog) ? "on" : "off");
	} else switch (param->type) {
		case STR_PARAM: {
			/* open oplog */
			FILE *old = HYSSDBG_G(oplog);

			HYSSDBG_G(oplog) = fopen(param->str, "w+");
			if (!HYSSDBG_G(oplog)) {
				hyssdbg_error("setoplog", "type=\"openfailure\" file=\"%s\"", "Failed to open %s for oplog", param->str);
				HYSSDBG_G(oplog) = old;
			} else {
				if (old) {
					hyssdbg_notice("setoplog", "type=\"closingold\"", "Closing previously open oplog");
					fclose(old);
				}

				hyssdbg_notice("setoplog", "file=\"%s\"", "Successfully opened oplog %s", param->str);
			}
		} break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_SET(quiet) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_writeln("setquiet", "active=\"%s\"", "Quietness %s", HYSSDBG_G(flags) & HYSSDBG_IS_QUIET ? "on" : "off");
	} else switch (param->type) {
		case NUMERIC_PARAM: {
			if (param->num) {
				HYSSDBG_G(flags) |= HYSSDBG_IS_QUIET;
			} else {
				HYSSDBG_G(flags) &= ~HYSSDBG_IS_QUIET;
			}
		} break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_SET(stepping) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_writeln("setstepping", "type=\"%s\"", "Stepping %s", HYSSDBG_G(flags) & HYSSDBG_STEP_OPCODE ? "opcode" : "line");
	} else switch (param->type) {
		case STR_PARAM: {
			if (param->len == sizeof("opcode") - 1 && !memcmp(param->str, "opcode", sizeof("opcode"))) {
				HYSSDBG_G(flags) |= HYSSDBG_STEP_OPCODE;
			} else if (param->len == sizeof("line") - 1 && !memcmp(param->str, "line", sizeof("line"))) {
				HYSSDBG_G(flags) &= ~HYSSDBG_STEP_OPCODE;
			} else {
				hyssdbg_error("setstepping", "type=\"wrongargs\"", "usage set stepping [<opcode|line>]");
			}
		} break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_SET(refcount) /* {{{ */
{
	if (!param || param->type == EMPTY_PARAM) {
		hyssdbg_writeln("setrefcount", "active=\"%s\"", "Showing refcounts %s", HYSSDBG_G(flags) & HYSSDBG_IS_QUIET ? "on" : "off");
	} else switch (param->type) {
		case NUMERIC_PARAM: {
			if (param->num) {
				HYSSDBG_G(flags) |= HYSSDBG_SHOW_REFCOUNTS;
			} else {
				HYSSDBG_G(flags) &= ~HYSSDBG_SHOW_REFCOUNTS;
			}
		} break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */
