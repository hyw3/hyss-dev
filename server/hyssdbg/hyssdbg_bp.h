/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_BP_H
#define HYSSDBG_BP_H

/* {{{ defines */
#define HYSSDBG_BREAK_FILE            0
#define HYSSDBG_BREAK_FILE_PENDING    1
#define HYSSDBG_BREAK_SYM             2
#define HYSSDBG_BREAK_OPLINE          3
#define HYSSDBG_BREAK_METHOD          4
#define HYSSDBG_BREAK_COND            5
#define HYSSDBG_BREAK_OPCODE          6
#define HYSSDBG_BREAK_FUNCTION_OPLINE 7
#define HYSSDBG_BREAK_METHOD_OPLINE   8
#define HYSSDBG_BREAK_FILE_OPLINE     9
#define HYSSDBG_BREAK_MAP             10
#define HYSSDBG_BREAK_TABLES          11 /* }}} */

/* {{{ */
typedef struct _gear_op *hyssdbg_opline_ptr_t; /* }}} */

/* {{{ breakpoint base structure */
#define hyssdbg_breakbase(name) \
	int         id; \
	gear_uchar  type; \
	gear_ulong  hits; \
	gear_bool   disabled; \
	const char *name /* }}} */

/* {{{ breakpoint base */
typedef struct _hyssdbg_breakbase_t {
	hyssdbg_breakbase(name);
} hyssdbg_breakbase_t; /* }}} */

/**
 * Breakpoint file-based representation
 */
typedef struct _hyssdbg_breakfile_t {
	hyssdbg_breakbase(filename);
	long        line;
} hyssdbg_breakfile_t;

/**
 * Breakpoint symbol-based representation
 */
typedef struct _hyssdbg_breaksymbol_t {
	hyssdbg_breakbase(symbol);
} hyssdbg_breaksymbol_t;

/**
 * Breakpoint method based representation
 */
typedef struct _hyssdbg_breakmethod_t {
	hyssdbg_breakbase(class_name);
	size_t      class_len;
	const char *func_name;
	size_t      func_len;
} hyssdbg_breakmethod_t;

/**
 * Breakpoint opline num based representation
 */
typedef struct _hyssdbg_breakopline_t {
	hyssdbg_breakbase(func_name);
	size_t      func_len;
	const char *class_name;
	size_t      class_len;
	gear_ulong  opline_num;
	gear_ulong  opline;
} hyssdbg_breakopline_t;

/**
 * Breakpoint opline based representation
 */
typedef struct _hyssdbg_breakline_t {
	hyssdbg_breakbase(name);
	gear_ulong opline;
	hyssdbg_breakopline_t *base;
} hyssdbg_breakline_t;

/**
 * Breakpoint opcode based representation
 */
typedef struct _hyssdbg_breakop_t {
	hyssdbg_breakbase(name);
	gear_ulong hash;
} hyssdbg_breakop_t;

/**
 * Breakpoint condition based representation
 */
typedef struct _hyssdbg_breakcond_t {
	hyssdbg_breakbase(code);
	size_t          code_len;
	gear_bool       paramed;
	hyssdbg_param_t  param;
	gear_ulong      hash;
	gear_op_array  *ops;
} hyssdbg_breakcond_t;

/* {{{ Resolving breaks API */
HYSSDBG_API void hyssdbg_resolve_op_array_breaks(gear_op_array *op_array);
HYSSDBG_API int hyssdbg_resolve_op_array_break(hyssdbg_breakopline_t *brake, gear_op_array *op_array);
HYSSDBG_API int hyssdbg_resolve_opline_break(hyssdbg_breakopline_t *new_break);
HYSSDBG_API HashTable *hyssdbg_resolve_pending_file_break_ex(const char *file, uint32_t filelen, gear_string *cur, HashTable *fileht);
HYSSDBG_API void hyssdbg_resolve_pending_file_break(const char *file); /* }}} */

/* {{{ Breakpoint Creation API */
HYSSDBG_API void hyssdbg_set_breakpoint_file(const char* filename, size_t path_len, long lineno);
HYSSDBG_API void hyssdbg_set_breakpoint_symbol(const char* func_name, size_t func_name_len);
HYSSDBG_API void hyssdbg_set_breakpoint_method(const char* class_name, const char* func_name);
HYSSDBG_API void hyssdbg_set_breakpoint_opcode(const char* opname, size_t opname_len);
HYSSDBG_API void hyssdbg_set_breakpoint_opline(gear_ulong opline);
HYSSDBG_API void hyssdbg_set_breakpoint_opline_ex(hyssdbg_opline_ptr_t opline);
HYSSDBG_API void hyssdbg_set_breakpoint_method_opline(const char *class, const char *method, gear_ulong opline);
HYSSDBG_API void hyssdbg_set_breakpoint_function_opline(const char *function, gear_ulong opline);
HYSSDBG_API void hyssdbg_set_breakpoint_file_opline(const char *file, gear_ulong opline);
HYSSDBG_API void hyssdbg_set_breakpoint_expression(const char* expression, size_t expression_len);
HYSSDBG_API void hyssdbg_set_breakpoint_at(const hyssdbg_param_t *param); /* }}} */

/* {{{ Breakpoint Detection API */
HYSSDBG_API hyssdbg_breakbase_t* hyssdbg_find_breakpoint(gear_execute_data*); /* }}} */

/* {{{ Misc Breakpoint API */
HYSSDBG_API void hyssdbg_hit_breakpoint(hyssdbg_breakbase_t* brake, gear_bool output);
HYSSDBG_API void hyssdbg_print_breakpoints(gear_ulong type);
HYSSDBG_API void hyssdbg_print_breakpoint(hyssdbg_breakbase_t* brake);
HYSSDBG_API void hyssdbg_reset_breakpoints(void);
HYSSDBG_API void hyssdbg_clear_breakpoints(void);
HYSSDBG_API void hyssdbg_delete_breakpoint(gear_ulong num);
HYSSDBG_API void hyssdbg_enable_breakpoints(void);
HYSSDBG_API void hyssdbg_enable_breakpoint(gear_ulong id);
HYSSDBG_API void hyssdbg_disable_breakpoint(gear_ulong id);
HYSSDBG_API void hyssdbg_disable_breakpoints(void); /* }}} */

/* {{{ Breakbase API */
HYSSDBG_API hyssdbg_breakbase_t *hyssdbg_find_breakbase(gear_ulong id);
HYSSDBG_API hyssdbg_breakbase_t *hyssdbg_find_breakbase_ex(gear_ulong id, HashTable **table, gear_ulong *numkey, gear_string **strkey); /* }}} */

/* {{{ Breakpoint Exportation API */
HYSSDBG_API void hyssdbg_export_breakpoints(FILE *handle);
HYSSDBG_API void hyssdbg_export_breakpoints_to_string(char **str); /* }}} */

#endif /* HYSSDBG_BP_H */
