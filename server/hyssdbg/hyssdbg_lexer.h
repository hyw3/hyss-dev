/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_LEXER_H
#define HYSSDBG_LEXER_H

#include "hyssdbg_cmd.h"

typedef struct {
        unsigned int len;
        unsigned char *text;
        unsigned char *cursor;
        unsigned char *marker;
        unsigned char *ctxmarker;
        int state;
} hyssdbg_lexer_data;

#define yyparse hyssdbg_parse
#define yylex hyssdbg_lex

void hyssdbg_init_lexer (hyssdbg_param_t *stack, char *input);

int hyssdbg_lex (hyssdbg_param_t* yylval);

#endif
