/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg_sigsafe.h"
#include "hyssdbg.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

#define STR(x) #x
#define EXP_STR(x) STR(x)

static void* gear_mm_mem_alloc(gear_mm_storage *storage, size_t size, size_t alignment) {

	if (EXPECTED(size <= HYSSDBG_SIGSAFE_MEM_SIZE && !HYSSDBG_G(sigsafe_mem).allocated)) {
		HYSSDBG_G(sigsafe_mem).allocated = 1;
		return (void *) (((size_t) HYSSDBG_G(sigsafe_mem).mem & ~(alignment - 1)) + alignment);
	}

	gear_quiet_write(HYSSDBG_G(io)[HYSSDBG_STDERR].fd, GEAR_STRL("Tried to allocate more than " EXP_STR(HYSSDBG_SIGSAFE_MEM_SIZE) " bytes from stack memory in signal handler ... bailing out of signal handler\n"));

	if (*EG(bailout)) {
		LONGJMP(*EG(bailout), FAILURE);
	}

	gear_quiet_write(HYSSDBG_G(io)[HYSSDBG_STDERR].fd, GEAR_STRL("Bailed out without a bailout address in signal handler!\n"));

	return NULL;
}

static void gear_mm_mem_free(gear_mm_storage *storage, void *ptr, size_t size) {
}

void hyssdbg_set_sigsafe_mem(char *buffer) {
	hyssdbg_signal_safe_mem *mem = &HYSSDBG_G(sigsafe_mem);
	const gear_mm_handlers hyssdbg_handlers = {
		gear_mm_mem_alloc,
		gear_mm_mem_free,
		NULL,
		NULL,
	};

	mem->mem = buffer;
	mem->allocated = 0;

	mem->heap = gear_mm_startup_ex(&hyssdbg_handlers, NULL, 0);

	mem->old_heap = gear_mm_set_heap(mem->heap);
}

gear_mm_heap *hyssdbg_original_heap_sigsafe_mem(void) {
	return HYSSDBG_G(sigsafe_mem).old_heap;
}

void hyssdbg_clear_sigsafe_mem(void) {
	gear_mm_set_heap(hyssdbg_original_heap_sigsafe_mem());
	HYSSDBG_G(sigsafe_mem).mem = NULL;
}

gear_bool hyssdbg_active_sigsafe_mem(void) {
	return !!HYSSDBG_G(sigsafe_mem).mem;
}
