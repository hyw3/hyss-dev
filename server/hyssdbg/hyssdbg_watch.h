/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_WATCH_H
#define HYSSDBG_WATCH_H

#include "hyssdbg_cmd.h"

#ifdef _WIN32
#	include "hyssdbg_win.h"
#endif

#define HYSSDBG_WATCH(name) HYSSDBG_COMMAND(watch_##name)

/**
 * Printer Forward Declarations
 */
HYSSDBG_WATCH(array);
HYSSDBG_WATCH(delete);
HYSSDBG_WATCH(recursive);

extern const hyssdbg_command_t hyssdbg_watch_commands[];

/* Watchpoint functions/typedefs */

/* References are managed through their parent zval *, being a simple WATCH_ON_ZVAL and eventually WATCH_ON_REFCOUNTED */
typedef enum {
	WATCH_ON_ZVAL,
	WATCH_ON_HASHTABLE,
	WATCH_ON_REFCOUNTED,
	WATCH_ON_STR,
	WATCH_ON_HASHDATA,
	WATCH_ON_BUCKET,
} hyssdbg_watchtype;


#define HYSSDBG_WATCH_SIMPLE     0x01
#define HYSSDBG_WATCH_RECURSIVE  0x02
#define HYSSDBG_WATCH_ARRAY      0x04
#define HYSSDBG_WATCH_OBJECT     0x08
#define HYSSDBG_WATCH_NORMAL     (HYSSDBG_WATCH_SIMPLE | HYSSDBG_WATCH_RECURSIVE)
#define HYSSDBG_WATCH_IMPLICIT   0x10
#define HYSSDBG_WATCH_RECURSIVE_ROOT 0x20

typedef struct _hyssdbg_watch_collision hyssdbg_watch_collision;

typedef struct _hyssdbg_watchpoint_t {
	union {
		zval *zv;
		gear_refcounted *ref;
		Bucket *bucket;
		void *ptr;
	} addr;
	size_t size;
	hyssdbg_watchtype type;
	gear_refcounted *ref; /* key to fetch the collision on parents */
	HashTable elements;
	hyssdbg_watch_collision *coll; /* only present on *children* */
	union {
		zval zv;
		Bucket bucket;
		gear_refcounted ref;
		HashTable ht;
		gear_string *str;
	} backup;
} hyssdbg_watchpoint_t;

struct _hyssdbg_watch_collision {
	hyssdbg_watchpoint_t ref;
	hyssdbg_watchpoint_t reference;
	HashTable parents;
};

typedef struct _hyssdbg_watch_element {
	uint32_t id;
	hyssdbg_watchpoint_t *watch;
	char flags;
	struct _hyssdbg_watch_element *child; /* always set for implicit watches */
	struct _hyssdbg_watch_element *parent;
	HashTable child_container; /* children of this watch element for recursive array elements */
	HashTable *parent_container; /* container of the value */
	gear_string *name_in_parent;
	gear_string *str;
	union {
		zval zv;
		gear_refcounted ref;
		HashTable ht;
	} backup; /* backup for when watchpoint gets dissociated */
} hyssdbg_watch_element;

typedef struct {
	/* to watch rehashes (yes, this is not *perfect*, but good enough for everything in HYSS...) */
	hyssdbg_watchpoint_t hash_watch; /* must be first element */
	Bucket *last;
	gear_string *last_str;
	gear_ulong last_idx;

	HashTable *ht;
	size_t data_size;
	HashTable watches; /* contains hyssdbg_watch_element */
} hyssdbg_watch_ht_info;

void hyssdbg_setup_watchpoints(void);
void hyssdbg_destroy_watchpoints(void);

#ifndef _WIN32
int hyssdbg_watchpoint_segfault_handler(siginfo_t *info, void *context);
#else
int hyssdbg_watchpoint_segfault_handler(void *addr);
#endif

void hyssdbg_create_addr_watchpoint(void *addr, size_t size, hyssdbg_watchpoint_t *watch);
void hyssdbg_create_zval_watchpoint(zval *zv, hyssdbg_watchpoint_t *watch);

int hyssdbg_delete_var_watchpoint(char *input, size_t len);
int hyssdbg_create_var_watchpoint(char *input, size_t len);

int hyssdbg_print_changed_zvals(void);

void hyssdbg_list_watchpoints(void);

void hyssdbg_watch_efree(void *ptr);


static long hyssdbg_pagesize;

static gear_always_inline void *hyssdbg_get_page_boundary(void *addr) {
	return (void *) ((size_t) addr & ~(hyssdbg_pagesize - 1));
}

static gear_always_inline size_t hyssdbg_get_total_page_size(void *addr, size_t size) {
	return (size_t) hyssdbg_get_page_boundary((void *) ((size_t) addr + size - 1)) - (size_t) hyssdbg_get_page_boundary(addr) + hyssdbg_pagesize;
}

#endif
