/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_UTILS_H
#define HYSSDBG_UTILS_H

/**
 * Input scan functions
 */
HYSSDBG_API int hyssdbg_is_numeric(const char*);
HYSSDBG_API int hyssdbg_is_empty(const char*);
HYSSDBG_API int hyssdbg_is_addr(const char*);
HYSSDBG_API int hyssdbg_is_class_method(const char*, size_t, char**, char**);
HYSSDBG_API const char *hyssdbg_current_file(void);
HYSSDBG_API char *hyssdbg_resolve_path(const char*);
HYSSDBG_API char *hyssdbg_trim(const char*, size_t, size_t*);
HYSSDBG_API const gear_function *hyssdbg_get_function(const char*, const char*);

/* {{{ Color Management */
#define HYSSDBG_COLOR_LEN 12
#define HYSSDBG_COLOR_D(color, code) \
	{color, sizeof(color)-1, code}
#define HYSSDBG_COLOR_END \
	{NULL, 0L, {0}}
#define HYSSDBG_ELEMENT_LEN 3
#define HYSSDBG_ELEMENT_D(name, id) \
	{name, sizeof(name)-1, id}
#define HYSSDBG_ELEMENT_END \
	{NULL, 0L, 0}

#define HYSSDBG_COLOR_INVALID	-1
#define HYSSDBG_COLOR_PROMPT 	 0
#define HYSSDBG_COLOR_ERROR		 1
#define HYSSDBG_COLOR_NOTICE		 2
#define HYSSDBG_COLORS			 3

typedef struct _hyssdbg_color_t {
	char       *name;
	size_t      name_length;
	const char  code[HYSSDBG_COLOR_LEN];
} hyssdbg_color_t;

typedef struct _hyssdbg_element_t {
	char		*name;
	size_t		name_length;
	int			id;
} hyssdbg_element_t;

HYSSDBG_API const hyssdbg_color_t *hyssdbg_get_color(const char *name, size_t name_length);
HYSSDBG_API void hyssdbg_set_color(int element, const hyssdbg_color_t *color);
HYSSDBG_API void hyssdbg_set_color_ex(int element, const char *name, size_t name_length);
HYSSDBG_API const hyssdbg_color_t *hyssdbg_get_colors(void);
HYSSDBG_API int hyssdbg_get_element(const char *name, size_t len); /* }}} */

/* {{{ Prompt Management */
HYSSDBG_API void hyssdbg_set_prompt(const char*);
HYSSDBG_API const char *hyssdbg_get_prompt(void); /* }}} */

/* {{{ Console size */
HYSSDBG_API int hyssdbg_get_terminal_width(void);
HYSSDBG_API int hyssdbg_get_terminal_height(void); /* }}} */

HYSSDBG_API void hyssdbg_set_async_io(int fd);

int hyssdbg_rebuild_symtable(void);

int hyssdbg_safe_class_lookup(const char *name, int name_length, gear_class_entry **ce);

char *hyssdbg_get_property_key(char *key);

typedef int (*hyssdbg_parse_var_func)(char *name, size_t len, char *keyname, size_t keylen, HashTable *parent, zval *zv);
typedef int (*hyssdbg_parse_var_with_arg_func)(char *name, size_t len, char *keyname, size_t keylen, HashTable *parent, zval *zv, void *arg);

HYSSDBG_API int hyssdbg_parse_variable(char *input, size_t len, HashTable *parent, size_t i, hyssdbg_parse_var_func callback, gear_bool silent);
HYSSDBG_API int hyssdbg_parse_variable_with_arg(char *input, size_t len, HashTable *parent, size_t i, hyssdbg_parse_var_with_arg_func callback, hyssdbg_parse_var_with_arg_func step_cb, gear_bool silent, void *arg);

int hyssdbg_is_auto_global(char *name, int len);

HYSSDBG_API void hyssdbg_xml_var_dump(zval *zv);

char *hyssdbg_short_zval_print(zval *zv, int maxlen);

HYSSDBG_API gear_bool hyssdbg_check_caught_ex(gear_execute_data *ex, gear_object *exception);

static gear_always_inline gear_execute_data *hyssdbg_user_execute_data(gear_execute_data *ex) {
	while (!ex->func || !GEAR_USER_CODE(ex->func->common.type)) {
		ex = ex->prev_execute_data;
		GEAR_ASSERT(ex);
	}
	return ex;
}

#ifdef ZTS
#define HYSSDBG_OUTPUT_BACKUP_DEFINES() \
	gear_output_globals *output_globals_ptr; \
	gear_output_globals original_output_globals; \
	output_globals_ptr = (gear_output_globals *) (*((void ***) pbc_get_ls_cache()))[PBC_UNSHUFFLE_RSRC_ID(output_globals_id)];
#else
#define HYSSDBG_OUTPUT_BACKUP_DEFINES() \
	gear_output_globals *output_globals_ptr; \
	gear_output_globals original_output_globals; \
	output_globals_ptr = &output_globals;
#endif

#define HYSSDBG_OUTPUT_BACKUP_SWAP() \
	original_output_globals = *output_globals_ptr; \
	memset(output_globals_ptr, 0, sizeof(gear_output_globals)); \
	hyss_output_activate();

#define HYSSDBG_OUTPUT_BACKUP() \
	HYSSDBG_OUTPUT_BACKUP_DEFINES() \
	HYSSDBG_OUTPUT_BACKUP_SWAP()

#define HYSSDBG_OUTPUT_BACKUP_RESTORE() \
	hyss_output_deactivate(); \
	*output_globals_ptr = original_output_globals;

#endif /* HYSSDBG_UTILS_H */
