/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_FRAME_H
#define HYSSDBG_FRAME_H

#include "hypbc.h"

gear_string *hyssdbg_compile_stackframe(gear_execute_data *);
void hyssdbg_restore_frame(void);
void hyssdbg_switch_frame(int);
void hyssdbg_dump_backtrace(size_t);
void hyssdbg_open_generator_frame(gear_generator *);

#endif /* HYSSDBG_FRAME_H */
