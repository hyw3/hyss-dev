/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_WEBHELPER_H
#define HYSSDBG_WEBHELPER_H

#include "hyssdbg_webdata_transfer.h"

extern gear_capi_entry hyssdbg_webhelper_capi_entry;
#define hyssext_hyssdbg_webhelper_ptr &hyssdbg_webhelper_capi_entry

#ifdef ZTS
# define HYSSDBG_WG(v) PBCG(hyssdbg_webhelper_globals_id, gear_hyssdbg_webhelper_globals *, v)
#else
# define HYSSDBG_WG(v) (hyssdbg_webhelper_globals.v)
#endif

/* {{{ structs */
GEAR_BEGIN_CAPI_GLOBALS(hyssdbg_webhelper)
	char *auth;
	char *path;
GEAR_END_CAPI_GLOBALS(hyssdbg_webhelper) /* }}} */

#endif /* HYSSDBG_WEBHELPER_H */
