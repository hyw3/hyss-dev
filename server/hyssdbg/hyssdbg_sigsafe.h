/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_SIGSAFE_H
#define HYSSDBG_SIGSAFE_H

#define HYSSDBG_SIGSAFE_MEM_SIZE (GEAR_MM_CHUNK_SIZE * 2)

#include "gear.h"

typedef struct {
	char *mem;
	gear_bool allocated;
	gear_mm_heap *heap;
	gear_mm_heap *old_heap;
} hyssdbg_signal_safe_mem;

#include "hyssdbg.h"

gear_bool hyssdbg_active_sigsafe_mem(void);

void hyssdbg_set_sigsafe_mem(char *mem);
void hyssdbg_clear_sigsafe_mem(void);

gear_mm_heap *hyssdbg_original_heap_sigsafe_mem(void);

#endif
