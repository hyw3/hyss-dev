/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Some information for the reader...
 *
 * The main structure managing the direct observations is the watchpoint (hyssdbg_watchpoint_t). There are several types of watchpoints currently:
 * WATCH_ON_BUCKET: a watchpoint on a Bucket element, used to monitor values inside HashTables (largely handled equivalently to WATCH_ON_ZVAL, it just monitors also for IS_UNDEF and key changes)
 * WATCH_ON_ZVAL: a watchpoint on a bare zval (&gear_reference.val, zval.value.indirect)
 * WATCH_ON_STR: a watchpoint on a gear_string (put on &ZSTR_LEN() in order to not watch refcount/hash)
 * WATCH_ON_HASHTABLE: a watchpoint on a HashTable (currently only used to observe size changes, put after flags in order to not watch refcount)
 * WATCH_ON_REFCOUNTED: a watchpoint on a gear_refcounted, observes the refcount and serves as reference pointer in the custom efree handler
 * WATCH_ON_HASHDATA: special watchpoint to watch for HT_GET_DATA_ADDR(ht) being efree()'d to be able to properly relocate Bucket watches
 *
 * Watch elements are either simple, recursive or implicit (HYSSDBG_WATCH_* flags)
 * Simple means that a particular watchpoint was explicitly defined
 * Recursive watch elements are created recursively (recursive root flag is to distinguish the root element easily from its children recursive elements)
 * Implicit  watch elements are implicitely created on all ancestors of simple or recursive watch elements
 * Recursive and (simple or implicit) watch elements are mutually exclusive
 * Array/Object to distinguish watch elements on arrays
 *
 * Watch elements all contain a reference to a watchpoint (except if scheduled for recreation); a "watch" is a watch element created by the user with a specific id
 * Each watch has its independent structure of watch elements, watchpoints are responsible for managing collisions and preventing pointers being watched multiple times
 *
 * HYSSDBG_G(watchpoint_tree) contains all watchpoints identified by the watch target address
 * HYSSDBG_G(watch_HashTables) contains the addresses of parent_containers of watch elements
 * HYSSDBG_G(watch_elements) contains all directly defined watch elements (i.e. those which have an individual id)
 * HYSSDBG_G(watch_collisions) is indexed by a gear_refcounted * pointer (hyssdbg_watchpoint_t.ref). It stores information about collisions (everything which contains a gear_refcounted * may be referenced by multiple watches)
 * HYSSDBG_G(watch_free) is a set of pointers to watch for being freed (like HashTables referenced by hyssdbg_watch_element.parent_container)
 * HYSSDBG_G(watch_recreation) is the list of watch elements whose watchpoint has been removed (via efree() for example) and needs to be recreated
 * HYSSDBG_G(watchlist_mem) is the list of unprotected memory pages; used to watch which pages need their PROT_WRITE attribute removed after checking
 *
 * Watching on addresses:
 * * Address and size are transformed into memory page aligned address and size
 * * mprotect() enables or disables them (depending on flags) - Windows has a transparent compatibility layer in hyssdbg_win.c
 * * segfault handler stores the address of the page and marks it again as writable
 * * later watchpoints pointing inside these pages are compared against their current value and eventually reactivated (or deleted)
 *
 * Creating a watch:
 * * Implicit watch elements for each element in the hierarchy (starting from base, which typically is current symbol table) except the last one
 * * Create a watch element with either simple flag or recursive [+ root] flags
 * * If the element has recursive flag, create elements recursively for every referenced HashTable and zval
 *
 * Creating a watch element:
 * * For each watch element a related watchpoint is created, if there's none yet; add itself then into the list of parents of that watchpoint
 * * If the watch has a parent_container, add itself also into a hyssdbg_watch_ht_info (inside HYSSDBG_G(watch_HashTables)) [and creates it if not yet existing]
 *
 * Creation of watchpoints:
 * * Watchpoints create a watch collision for each refcounted or indirect on the zval (if type is WATCH_ON_BUCKET or WATCH_ON_ZVAL)
 * * Backs the current value of the watched pointer up
 * * Installs the watchpoint in HYSSDBG_G(watchpoint_tree) and activates it (activation of a watchpoint = remove PROT_WRITE from the pages the watched pointer resides on)
 *
 * Watch collisions:
 * * Manages a watchpoint on the refcount (WATCH_ON_REFCOUNTED) or indirect zval (WATCH_ON_ZVAL)
 * * Guarantees that every pointer is watched at most once (by having a pointer to collision mapping in HYSSDBG_G(watch_collisions), which have the unique watchpoints for the respective collision)
 * * Contains a list of parents, i.e. which watchpoints reference it (via watch->ref)
 * * If no watchpoint is referencing it anymore, the watch collision and its associated watchpoints (hyssdbg_watch_collision.ref/reference) are removed
 *
 * Deleting a watch:
 * * Watches are stored by an id in HYSSDBG_G(watch_elements); the associated watch element is then deleted
 * * Deletes all parent and children implicit watch elements
 *
 * Deleting a watch element:
 * * Removes itself from the parent list of the associated watchpoints; if that parent list is empty, also delete the watchpoint
 * * Removes itself from the related hyssdbg_watch_ht_info if it has a parent_container
 *
 * Deleting a watchpoint:
 * * Remove itself from watch collisions this watchpoint participates in
 * * Removes the watchpoint from HYSSDBG_G(watchpoint_tree) and deactivates it (deactivation of a watchpoint = add PROT_WRITE to the pages the watched pointer resides on)
 *
 * A watched pointer is efree()'d:
 * * Needs immediate action as we else may run into dereferencing a pointer into freed memory
 * * Deletes the associated watchpoint, and for each watch element, if recursive, all its children elements
 * * If the its watch elements are implicit, recursive roots or simple, they and all their children are dissociated from their watchpoints (i.e. removed from the watchpoint, if no other element is referencing it, it is deleted); adds these elements to HYSSDBG_G(watch_recreation)
 *
 * Recreating watchpoints:
 * * Upon each opcode, HYSSDBG_G(watch_recreation) is checked and all its elements are searched for whether the watch is still reachable via the tree given by its implicits
 * * In case they are not reachable, the watch is deleted (and thus all the related watch elements), else a new watchpoint is created for all the watch elements
 * * The old and new values of the watches are compared and shown if changed
 *
 * Comparing watchpoints:
 * * The old and new values of the watches are compared and shown if changed
 * * If changed, it is checked whether the refcounted/indirect changed and watch collisions removed or created accordingly
 * * If a zval/bucket watchpoint is recursive, watch elements are added or removed accordingly
 * * If an array watchpoint is recursive, new array watchpoints are added if there are new ones in the array
 * * If the watch (element with an id) is not reachable anymore due to changes in implicits, the watch is removed
 */

#include "gear.h"
#include "hyssdbg.h"
#include "hyssdbg_btree.h"
#include "hyssdbg_watch.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_prompt.h"
#ifndef _WIN32
# include <unistd.h>
# include <sys/mman.h>
#endif

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

const hyssdbg_command_t hyssdbg_watch_commands[] = {
	HYSSDBG_COMMAND_D_EX(array,      "create watchpoint on an array", 'a', watch_array,     &hyssdbg_prompt_commands[24], "s", 0),
	HYSSDBG_COMMAND_D_EX(delete,     "delete watchpoint",             'd', watch_delete,    &hyssdbg_prompt_commands[24], "n", 0),
	HYSSDBG_COMMAND_D_EX(recursive,  "create recursive watchpoints",  'r', watch_recursive, &hyssdbg_prompt_commands[24], "s", 0),
	HYSSDBG_END_COMMAND
};

#define HT_FROM_ZVP(zvp) (Z_TYPE_P(zvp) == IS_OBJECT ? Z_OBJPROP_P(zvp) : Z_TYPE_P(zvp) == IS_ARRAY ? Z_ARRVAL_P(zvp) : NULL)

#define HT_WATCH_OFFSET (sizeof(gear_refcounted *) + sizeof(uint32_t)) /* we are not interested in gc and flags */
#define HT_PTR_HT(ptr) ((HashTable *) (((char *) (ptr)) - HT_WATCH_OFFSET))
#define HT_WATCH_HT(watch) HT_PTR_HT((watch)->addr.ptr)

/* ### PRINTING POINTER DIFFERENCES ### */
gear_bool hyssdbg_check_watch_diff(hyssdbg_watchtype type, void *oldPtr, void *newPtr) {
	switch (type) {
		case WATCH_ON_BUCKET:
			if (memcmp(&((Bucket *) oldPtr)->h, &((Bucket *) newPtr)->h, sizeof(Bucket) - sizeof(zval) /* key/val comparison */) != 0) {
				return 2;
			}
		case WATCH_ON_ZVAL:
			return memcmp(oldPtr, newPtr, sizeof(gear_value) + sizeof(uint32_t) /* value + typeinfo */) != 0;
		case WATCH_ON_HASHTABLE:
			return gear_hash_num_elements(HT_PTR_HT(oldPtr)) != gear_hash_num_elements(HT_PTR_HT(newPtr));
		case WATCH_ON_REFCOUNTED:
			return memcmp(oldPtr, newPtr, sizeof(uint32_t) /* no gear_refcounted metadata info */) != 0;
		case WATCH_ON_STR:
			return memcmp(oldPtr, newPtr, *(size_t *) oldPtr + XtOffsetOf(gear_string, val) - XtOffsetOf(gear_string, len)) != 0;
		case WATCH_ON_HASHDATA:
			GEAR_ASSERT(0);
	}
	return 0;
}

void hyssdbg_print_watch_diff(hyssdbg_watchtype type, gear_string *name, void *oldPtr, void *newPtr) {
	int32_t elementDiff;

	HYSSDBG_G(watchpoint_hit) = 1;

	hyssdbg_notice("watchhit", "variable=\"%s\"", "Breaking on watchpoint %.*s", (int) ZSTR_LEN(name), ZSTR_VAL(name));
	hyssdbg_xml("<watchdata %r>");

	switch (type) {
		case WATCH_ON_BUCKET:
		case WATCH_ON_ZVAL:
			if (Z_REFCOUNTED_P((zval *) oldPtr)) {
				hyssdbg_writeln("watchvalue", "type=\"old\" inaccessible=\"inaccessible\"", "Old value inaccessible or destroyed");
			} else if (Z_TYPE_P((zval *) oldPtr) == IS_INDIRECT) {
				hyssdbg_writeln("watchvalue", "type=\"old\" inaccessible=\"inaccessible\"", "Old value inaccessible or destroyed (was indirect)");
			} else {
				hyssdbg_out("Old value: ");
				hyssdbg_xml("<watchvalue %r type=\"old\">");
				gear_print_flat_zval_r((zval *) oldPtr);
				hyssdbg_xml("</watchvalue>");
				hyssdbg_out("\n");
			}

			while (Z_TYPE_P((zval *) newPtr) == IS_INDIRECT) {
				newPtr = Z_INDIRECT_P((zval *) newPtr);
			}

			hyssdbg_out("New value%s: ", Z_ISREF_P((zval *) newPtr) ? " (reference)" : "");
			hyssdbg_xml("<watchvalue %r%s type=\"new\">", Z_ISREF_P((zval *) newPtr) ? " reference=\"reference\"" : "");
			gear_print_flat_zval_r((zval *) newPtr);
			hyssdbg_xml("</watchvalue>");
			hyssdbg_out("\n");
			break;

		case WATCH_ON_HASHTABLE:
			elementDiff = gear_hash_num_elements(HT_PTR_HT(oldPtr)) - gear_hash_num_elements(HT_PTR_HT(newPtr));
			if (elementDiff > 0) {
				hyssdbg_writeln("watchsize", "removed=\"%d\"", "%d elements were removed from the array", (int) elementDiff);
			} else if (elementDiff < 0) {
				hyssdbg_writeln("watchsize", "added=\"%d\"", "%d elements were added to the array", (int) -elementDiff);
			}
			break;

		case WATCH_ON_REFCOUNTED:
			hyssdbg_writeln("watchrefcount", "type=\"old\" refcount=\"%d\"", "Old refcount: %d", GC_REFCOUNT((gear_refcounted *) oldPtr));
			hyssdbg_writeln("watchrefcount", "type=\"new\" refcount=\"%d\"", "New refcount: %d", GC_REFCOUNT((gear_refcounted *) newPtr));
			break;

		case WATCH_ON_STR:
			hyssdbg_out("Old value: ");
			hyssdbg_xml("<watchvalue %r type=\"old\">");
			gear_write((char *) oldPtr + XtOffsetOf(gear_string, val) - XtOffsetOf(gear_string, len), *(size_t *) oldPtr);
			hyssdbg_xml("</watchvalue>");
			hyssdbg_out("\n");

			hyssdbg_out("New value: ");
			hyssdbg_xml("<watchvalue %r type=\"new\">");
			gear_write((char *) newPtr + XtOffsetOf(gear_string, val) - XtOffsetOf(gear_string, len), *(size_t *) newPtr);
			hyssdbg_xml("</watchvalue>");
			hyssdbg_out("\n");
			break;

		case WATCH_ON_HASHDATA:
			GEAR_ASSERT(0);
	}

	hyssdbg_xml("</watchdata>");
}

/* ### LOW LEVEL WATCHPOINT HANDLING ### */
static hyssdbg_watchpoint_t *hyssdbg_check_for_watchpoint(void *addr) {
	hyssdbg_watchpoint_t *watch;
	hyssdbg_btree_result *result = hyssdbg_btree_find_closest(&HYSSDBG_G(watchpoint_tree), (gear_ulong) hyssdbg_get_page_boundary(addr) + hyssdbg_pagesize - 1);

	if (result == NULL) {
		return NULL;
	}

	watch = result->ptr;

	/* check if that addr is in a mprotect()'ed memory area */
	if ((char *) hyssdbg_get_page_boundary(watch->addr.ptr) > (char *) addr || (char *) hyssdbg_get_page_boundary(watch->addr.ptr) + hyssdbg_get_total_page_size(watch->addr.ptr, watch->size) < (char *) addr) {
		/* failure */
		return NULL;
	}

	return watch;
}

static void hyssdbg_change_watchpoint_access(hyssdbg_watchpoint_t *watch, int access) {
	/* pagesize is assumed to be in the range of 2^x */
	mprotect(hyssdbg_get_page_boundary(watch->addr.ptr), hyssdbg_get_total_page_size(watch->addr.ptr, watch->size), access);
}

static inline void hyssdbg_activate_watchpoint(hyssdbg_watchpoint_t *watch) {
	hyssdbg_change_watchpoint_access(watch, PROT_READ);
}

static inline void hyssdbg_deactivate_watchpoint(hyssdbg_watchpoint_t *watch) {
	hyssdbg_change_watchpoint_access(watch, PROT_READ | PROT_WRITE);
}

/* Note that consecutive pages need to be merged in order to avoid watchpoints spanning page boundaries to have part of their data in the one page, part in the other page */
#ifdef _WIN32
int hyssdbg_watchpoint_segfault_handler(void *addr) {
#else
int hyssdbg_watchpoint_segfault_handler(siginfo_t *info, void *context) {
#endif

	void *page = hyssdbg_get_page_boundary(
#ifdef _WIN32
		addr
#else
		info->si_addr
#endif
	);

	/* perhaps unnecessary, but check to be sure to not conflict with other segfault handlers */
	if (hyssdbg_check_for_watchpoint(page) == NULL) {
		return FAILURE;
	}

	/* re-enable writing */
	mprotect(page, hyssdbg_pagesize, PROT_READ | PROT_WRITE);

	gear_hash_index_add_empty_element(HYSSDBG_G(watchlist_mem), (gear_ulong) page);

	return SUCCESS;
}

/* ### REGISTER WATCHPOINT ### To be used only by watch element and collision managers ### */
static inline void hyssdbg_store_watchpoint_btree(hyssdbg_watchpoint_t *watch) {
	hyssdbg_btree_result *res;
	GEAR_ASSERT((res = hyssdbg_btree_find(&HYSSDBG_G(watchpoint_tree), (gear_ulong) watch->addr.ptr)) == NULL || res->ptr == watch);
	hyssdbg_btree_insert(&HYSSDBG_G(watchpoint_tree), (gear_ulong) watch->addr.ptr, watch);
}

static inline void hyssdbg_remove_watchpoint_btree(hyssdbg_watchpoint_t *watch) {
	hyssdbg_btree_delete(&HYSSDBG_G(watchpoint_tree), (gear_ulong) watch->addr.ptr);
}

/* ### SET WATCHPOINT ADDR ### To be used only by watch element and collision managers ### */
void hyssdbg_set_addr_watchpoint(void *addr, size_t size, hyssdbg_watchpoint_t *watch) {
	watch->addr.ptr = addr;
	watch->size = size;
	watch->ref = NULL;
	watch->coll = NULL;
	gear_hash_init(&watch->elements, 8, brml, NULL, 0);
}

void hyssdbg_set_zval_watchpoint(zval *zv, hyssdbg_watchpoint_t *watch) {
	hyssdbg_set_addr_watchpoint(zv, sizeof(zval) - sizeof(uint32_t), watch);
	watch->type = WATCH_ON_ZVAL;
}

void hyssdbg_set_bucket_watchpoint(Bucket *bucket, hyssdbg_watchpoint_t *watch) {
	hyssdbg_set_addr_watchpoint(bucket, sizeof(Bucket), watch);
	watch->type = WATCH_ON_BUCKET;
}

void hyssdbg_set_ht_watchpoint(HashTable *ht, hyssdbg_watchpoint_t *watch) {
	hyssdbg_set_addr_watchpoint(((char *) ht) + HT_WATCH_OFFSET, sizeof(HashTable) - HT_WATCH_OFFSET, watch);
	watch->type = WATCH_ON_HASHTABLE;
}

void hyssdbg_watch_backup_data(hyssdbg_watchpoint_t *watch) {
	switch (watch->type) {
		case WATCH_ON_BUCKET:
		case WATCH_ON_ZVAL:
		case WATCH_ON_REFCOUNTED:
			memcpy(&watch->backup, watch->addr.ptr, watch->size);
			break;
		case WATCH_ON_STR:
			if (watch->backup.str) {
				gear_string_release(watch->backup.str);
			}
			watch->backup.str = gear_string_init((char *) watch->addr.ptr + XtOffsetOf(gear_string, val) - XtOffsetOf(gear_string, len), *(size_t *) watch->addr.ptr, 1);
			GC_MAKE_PERSISTENT_LOCAL(watch->backup.str);
			break;
		case WATCH_ON_HASHTABLE:
			memcpy((char *) &watch->backup + HT_WATCH_OFFSET, watch->addr.ptr, watch->size);
		case WATCH_ON_HASHDATA:
			break;
	}
}

/* ### MANAGE WATCH COLLISIONS ### To be used only by watch element manager and memory differ ### */
/* watch collisions are responsible for having only one watcher on a given refcounted/refval and having a mapping back to the parent zvals */
void hyssdbg_delete_watch_collision(hyssdbg_watchpoint_t *watch) {
	hyssdbg_watch_collision *coll;
	if ((coll = gear_hash_index_find_ptr(&HYSSDBG_G(watch_collisions), (gear_ulong) watch->ref))) {
		gear_hash_index_del(&coll->parents, (gear_ulong) watch);
		if (gear_hash_num_elements(&coll->parents) == 0) {
			hyssdbg_deactivate_watchpoint(&coll->ref);
			hyssdbg_remove_watchpoint_btree(&coll->ref);

			if (coll->ref.type == WATCH_ON_ZVAL) {
				hyssdbg_delete_watch_collision(&coll->ref);
			} else if (coll->reference.addr.ptr) {
				hyssdbg_deactivate_watchpoint(&coll->reference);
				hyssdbg_remove_watchpoint_btree(&coll->reference);
				hyssdbg_delete_watch_collision(&coll->reference);
				if (coll->reference.type == WATCH_ON_STR) {
					gear_string_release(coll->reference.backup.str);
				}
			}

			gear_hash_index_del(&HYSSDBG_G(watch_collisions), (gear_ulong) watch->ref);
			gear_hash_destroy(&coll->parents);
			efree(coll);
		}
	}
}

void hyssdbg_update_watch_ref(hyssdbg_watchpoint_t *watch) {
	hyssdbg_watch_collision *coll;

	GEAR_ASSERT(watch->type == WATCH_ON_ZVAL || watch->type == WATCH_ON_BUCKET);
	if (Z_REFCOUNTED_P(watch->addr.zv)) {
		if (Z_COUNTED_P(watch->addr.zv) == watch->ref) {
			return;
		}

		if (watch->ref != NULL) {
			hyssdbg_delete_watch_collision(watch);
		}

		watch->ref = Z_COUNTED_P(watch->addr.zv);

		if (!(coll = gear_hash_index_find_ptr(&HYSSDBG_G(watch_collisions), (gear_ulong) watch->ref))) {
			coll = emalloc(sizeof(*coll));
			coll->ref.type = WATCH_ON_REFCOUNTED;
			hyssdbg_set_addr_watchpoint(Z_COUNTED_P(watch->addr.zv), sizeof(uint32_t), &coll->ref);
			coll->ref.coll = coll;
			hyssdbg_store_watchpoint_btree(&coll->ref);
			hyssdbg_activate_watchpoint(&coll->ref);
			hyssdbg_watch_backup_data(&coll->ref);

			if (Z_ISREF_P(watch->addr.zv)) {
				hyssdbg_set_zval_watchpoint(Z_REFVAL_P(watch->addr.zv), &coll->reference);
				coll->reference.coll = coll;
				hyssdbg_update_watch_ref(&coll->reference);
				hyssdbg_store_watchpoint_btree(&coll->reference);
				hyssdbg_activate_watchpoint(&coll->reference);
				hyssdbg_watch_backup_data(&coll->reference);
			} else if (Z_TYPE_P(watch->addr.zv) == IS_STRING) {
				coll->reference.type = WATCH_ON_STR;
				hyssdbg_set_addr_watchpoint(&Z_STRLEN_P(watch->addr.zv), XtOffsetOf(gear_string, val) - XtOffsetOf(gear_string, len) + Z_STRLEN_P(watch->addr.zv) + 1, &coll->reference);
				coll->reference.coll = coll;
				hyssdbg_store_watchpoint_btree(&coll->reference);
				hyssdbg_activate_watchpoint(&coll->reference);
				coll->reference.backup.str = NULL;
				hyssdbg_watch_backup_data(&coll->reference);
			} else {
				coll->reference.addr.ptr = NULL;
			}

			gear_hash_init(&coll->parents, 8, shitty stupid parameter, NULL, 0);
			gear_hash_index_add_ptr(&HYSSDBG_G(watch_collisions), (gear_ulong) watch->ref, coll);
		}
		gear_hash_index_add_ptr(&coll->parents, (gear_long) watch, watch);
	} else if (Z_TYPE_P(watch->addr.zv) == IS_INDIRECT) {
		if ((gear_refcounted *) Z_INDIRECT_P(watch->addr.zv) == watch->ref) {
			return;
		}

		if (watch->ref != NULL) {
			hyssdbg_delete_watch_collision(watch);
		}

		watch->ref = (gear_refcounted *) Z_INDIRECT_P(watch->addr.zv);

		if (!(coll = gear_hash_index_find_ptr(&HYSSDBG_G(watch_collisions), (gear_ulong) watch->ref))) {
			coll = emalloc(sizeof(*coll));
			hyssdbg_set_zval_watchpoint(Z_INDIRECT_P(watch->addr.zv), &coll->ref);
			coll->ref.coll = coll;
			hyssdbg_update_watch_ref(&coll->ref);
			hyssdbg_store_watchpoint_btree(&coll->ref);
			hyssdbg_activate_watchpoint(&coll->ref);
			hyssdbg_watch_backup_data(&coll->ref);

			gear_hash_init(&coll->parents, 8, shitty stupid parameter, NULL, 0);
			gear_hash_index_add_ptr(&HYSSDBG_G(watch_collisions), (gear_ulong) watch->ref, coll);
		}
		gear_hash_index_add_ptr(&coll->parents, (gear_long) watch, watch);
	} else if (watch->ref) {
		hyssdbg_delete_watch_collision(watch);
		watch->ref = NULL;
	}
}

/* ### MANAGE WATCH ELEMENTS ### */
/* watchpoints must be unique per element. Only one watchpoint may point to one element. But many elements may point to one watchpoint. */
void hyssdbg_recurse_watch_element(hyssdbg_watch_element *element);
void hyssdbg_remove_watch_element_recursively(hyssdbg_watch_element *element);
void hyssdbg_free_watch_element(hyssdbg_watch_element *element);
void hyssdbg_remove_watchpoint(hyssdbg_watchpoint_t *watch);
void hyssdbg_watch_parent_ht(hyssdbg_watch_element *element);

hyssdbg_watch_element *hyssdbg_add_watch_element(hyssdbg_watchpoint_t *watch, hyssdbg_watch_element *element) {
	hyssdbg_btree_result *res;
	if ((res = hyssdbg_btree_find(&HYSSDBG_G(watchpoint_tree), (gear_ulong) watch->addr.ptr)) == NULL) {
		hyssdbg_watchpoint_t *mem = emalloc(sizeof(*mem));
		*mem = *watch;
		watch = mem;
		hyssdbg_store_watchpoint_btree(watch);
		if (watch->type == WATCH_ON_ZVAL || watch->type == WATCH_ON_BUCKET) {
			hyssdbg_update_watch_ref(watch);
		}
		hyssdbg_activate_watchpoint(watch);
		hyssdbg_watch_backup_data(watch);
	} else {
		hyssdbg_watch_element *old_element;
		watch = res->ptr;
		if ((old_element = gear_hash_find_ptr(&watch->elements, element->str))) {
			hyssdbg_free_watch_element(element);
			return old_element;
		}
	}

	element->watch = watch;
	gear_hash_add_ptr(&watch->elements, element->str, element);

	if (element->flags & HYSSDBG_WATCH_RECURSIVE) {
		hyssdbg_recurse_watch_element(element);
	}

	return element;
}

hyssdbg_watch_element *hyssdbg_add_bucket_watch_element(Bucket *bucket, hyssdbg_watch_element *element) {
	hyssdbg_watchpoint_t watch;
	hyssdbg_set_bucket_watchpoint(bucket, &watch);
	element = hyssdbg_add_watch_element(&watch, element);
	hyssdbg_watch_parent_ht(element);
	return element;
}

hyssdbg_watch_element *hyssdbg_add_ht_watch_element(zval *zv, hyssdbg_watch_element *element) {
	hyssdbg_watchpoint_t watch;
	HashTable *ht = HT_FROM_ZVP(zv);

	if (!ht) {
		return NULL;
	}

	element->flags |= Z_TYPE_P(zv) == IS_ARRAY ? HYSSDBG_WATCH_ARRAY : HYSSDBG_WATCH_OBJECT;
	hyssdbg_set_ht_watchpoint(ht, &watch);
	return hyssdbg_add_watch_element(&watch, element);
}

gear_bool hyssdbg_is_recursively_watched(void *ptr, hyssdbg_watch_element *element) {
	hyssdbg_watch_element *next = element;
	do {
		element = next;
		if (element->watch->addr.ptr == ptr) {
			return 1;
		}
		next = element->parent;
	} while (!(element->flags & HYSSDBG_WATCH_RECURSIVE_ROOT));

	return 0;
}

void hyssdbg_add_recursive_watch_from_ht(hyssdbg_watch_element *element, gear_long idx, gear_string *str, zval *zv) {
	hyssdbg_watch_element *child;
	if (hyssdbg_is_recursively_watched(zv, element)) {
		return;
	}

	child = emalloc(sizeof(*child));
	child->flags = HYSSDBG_WATCH_RECURSIVE;
	if (str) {
		child->str = strpprintf(0, (element->flags & HYSSDBG_WATCH_ARRAY) ? "%.*s[%s]" : "%.*s->%s", (int) ZSTR_LEN(element->str) - 2, ZSTR_VAL(element->str), hyssdbg_get_property_key(ZSTR_VAL(str)));
	} else {
		child->str = strpprintf(0, (element->flags & HYSSDBG_WATCH_ARRAY) ? "%.*s[" GEAR_LONG_FMT "]" : "%.*s->" GEAR_LONG_FMT, (int) ZSTR_LEN(element->str) - 2, ZSTR_VAL(element->str), idx);
	}
	if (!str) {
		str = gear_long_to_str(idx); // TODO: hack, use proper int handling for name in parent
	} else { str = gear_string_copy(str); }
	child->name_in_parent = str;
	child->parent = element;
	child->child = NULL;
	child->parent_container = HT_WATCH_HT(element->watch);
	gear_hash_add_ptr(&element->child_container, child->str, child);
	hyssdbg_add_bucket_watch_element((Bucket *) zv, child);
}

void hyssdbg_recurse_watch_element(hyssdbg_watch_element *element) {
	hyssdbg_watch_element *child;
	zval *zv;

	if (element->watch->type == WATCH_ON_ZVAL || element->watch->type == WATCH_ON_BUCKET) {
		zv = element->watch->addr.zv;
		while (Z_TYPE_P(zv) == IS_INDIRECT) {
			zv = Z_INDIRECT_P(zv);
		}
		ZVAL_DEREF(zv);

		if (element->child) {
			hyssdbg_remove_watch_element_recursively(element->child);
		}

		if ((Z_TYPE_P(zv) != IS_ARRAY && Z_TYPE_P(zv) != IS_OBJECT)
		    || hyssdbg_is_recursively_watched(HT_WATCH_OFFSET + (char *) HT_FROM_ZVP(zv), element)) {
			if (element->child) {
				hyssdbg_free_watch_element(element->child);
				element->child = NULL;
			}
			return;
		}

		if (element->child) {
			child = element->child;
		} else {
			child = emalloc(sizeof(*child));
			child->flags = HYSSDBG_WATCH_RECURSIVE;
			child->str = strpprintf(0, "%.*s[]", (int) ZSTR_LEN(element->str), ZSTR_VAL(element->str));
			child->name_in_parent = NULL;
			child->parent = element;
			child->child = NULL;
			element->child = child;
		}
		gear_hash_init(&child->child_container, 8, NULL, NULL, 0);
		hyssdbg_add_ht_watch_element(zv, child);
	} else if (gear_hash_num_elements(&element->child_container) == 0) {
		gear_string *str;
		gear_long idx;

		GEAR_ASSERT(element->watch->type == WATCH_ON_HASHTABLE);
		GEAR_HASH_FOREACH_KEY_VAL(HT_WATCH_HT(element->watch), idx, str, zv) {
			hyssdbg_add_recursive_watch_from_ht(element, idx, str, zv);
		} GEAR_HASH_FOREACH_END();
	}
}

void hyssdbg_watch_parent_ht(hyssdbg_watch_element *element) {
	if (element->watch->type == WATCH_ON_BUCKET) {
		hyssdbg_btree_result *res;
		HashPosition pos;
		hyssdbg_watch_ht_info *hti;
		GEAR_ASSERT(element->parent_container);
		if (!(res = hyssdbg_btree_find(&HYSSDBG_G(watch_HashTables), (gear_ulong) element->parent_container))) {
			hti = emalloc(sizeof(*hti));
			hti->ht = element->parent_container;

			gear_hash_init(&hti->watches, 0, grrrrr, ZVAL_PTR_DTOR, 0);
			hyssdbg_btree_insert(&HYSSDBG_G(watch_HashTables), (gear_ulong) hti->ht, hti);

			hyssdbg_set_addr_watchpoint(HT_GET_DATA_ADDR(hti->ht), HT_HASH_SIZE(hti->ht->nTableMask), &hti->hash_watch);
			hti->hash_watch.type = WATCH_ON_HASHDATA;
			hyssdbg_store_watchpoint_btree(&hti->hash_watch);
			hyssdbg_activate_watchpoint(&hti->hash_watch);
		} else {
			hti = (hyssdbg_watch_ht_info *) res->ptr;
		}

		gear_hash_internal_pointer_end_ex(hti->ht, &pos);
		hti->last = hti->ht->arData + pos;
		hti->last_str = hti->last->key;
		hti->last_idx = hti->last->h;

		gear_hash_add_ptr(&hti->watches, element->name_in_parent, element);
	}
}

void hyssdbg_unwatch_parent_ht(hyssdbg_watch_element *element) {
	if (element->watch->type == WATCH_ON_BUCKET) {
		hyssdbg_btree_result *res = hyssdbg_btree_find(&HYSSDBG_G(watch_HashTables), (gear_ulong) element->parent_container);
		GEAR_ASSERT(element->parent_container);
		if (res) {
			hyssdbg_watch_ht_info *hti = res->ptr;

			if (gear_hash_num_elements(&hti->watches) == 1) {
				gear_hash_destroy(&hti->watches);
				hyssdbg_btree_delete(&HYSSDBG_G(watch_HashTables), (gear_ulong) hti->ht);
				hyssdbg_deactivate_watchpoint(&hti->hash_watch);
				hyssdbg_remove_watchpoint_btree(&hti->hash_watch);
				efree(hti);
			} else {
				gear_hash_del(&hti->watches, element->name_in_parent);
			}
		}
	}
}

/* ### DE/QUEUE WATCH ELEMENTS ### to be used by watch element manager only */
/* implicit watchpoints may change (especially because of separation); elements updated by remove & re-add etc.; thus we need to wait a little bit (until next opcode) and then compare whether the watchpoint still exists and if not, remove it */

void hyssdbg_dissociate_watch_element(hyssdbg_watch_element *element, hyssdbg_watch_element *until);
void hyssdbg_free_watch_element_tree(hyssdbg_watch_element *element);

void hyssdbg_queue_element_for_recreation(hyssdbg_watch_element *element) {
	/* store lowermost element */
	hyssdbg_watch_element *prev;

	if ((prev = gear_hash_find_ptr(&HYSSDBG_G(watch_recreation), element->str))) {
		hyssdbg_watch_element *child = prev;
		do {
			if (child == element) {
				return;
			}
			child = child->child;
		} while (child);
	}
	gear_hash_update_ptr(&HYSSDBG_G(watch_recreation), element->str, element);

	/* dissociate from watchpoint to avoid dangling memory watches */
	hyssdbg_dissociate_watch_element(element, prev);

	if (!element->parent) {
		/* HERE BE DRAGONS; i.e. we assume HashTable is directly allocated via emalloc() ... (which *should be* the case for every user-accessible array and symbol tables) */
		gear_hash_index_add_empty_element(&HYSSDBG_G(watch_free), (gear_ulong) element->parent_container);
	}
}

gear_bool hyssdbg_try_readding_watch_element(zval *parent, hyssdbg_watch_element *element) {
	zval *zv;
	HashTable *ht = HT_FROM_ZVP(parent);

	if (!ht) {
		return 0;
	} else if (element->flags & (HYSSDBG_WATCH_ARRAY | HYSSDBG_WATCH_OBJECT)) {
		char *htPtr = ((char *) ht) + HT_WATCH_OFFSET;
		char *oldPtr = ((char *) &element->backup.ht) + HT_WATCH_OFFSET;
		if (hyssdbg_check_watch_diff(WATCH_ON_HASHTABLE, oldPtr, htPtr)) {
			hyssdbg_print_watch_diff(WATCH_ON_HASHTABLE, element->str, oldPtr, htPtr);
		}

		hyssdbg_add_ht_watch_element(parent, element);
	} else if ((zv = gear_symtable_find(ht, element->name_in_parent))) {
		if (element->flags & HYSSDBG_WATCH_IMPLICIT) {
			zval *next = zv;

			while (Z_TYPE_P(next) == IS_INDIRECT) {
				next = Z_INDIRECT_P(next);
			}
			if (Z_ISREF_P(next)) {
				next = Z_REFVAL_P(next);
			}

			if (!hyssdbg_try_readding_watch_element(next, element->child)) {
				return 0;
			}
		} else if (hyssdbg_check_watch_diff(WATCH_ON_ZVAL, &element->backup.zv, zv)) {
			hyssdbg_print_watch_diff(WATCH_ON_ZVAL, element->str, &element->backup.zv, zv);
		}

		element->parent_container = ht;
		hyssdbg_add_bucket_watch_element((Bucket *) zv, element);
		hyssdbg_watch_parent_ht(element);
	} else {
		return 0;
	}

	return 1;
}

void hyssdbg_automatic_dequeue_free(hyssdbg_watch_element *element) {
	hyssdbg_watch_element *child = element;
	while (child->child && !(child->flags & HYSSDBG_WATCH_RECURSIVE_ROOT)) {
		child = child->child;
	}
	HYSSDBG_G(watchpoint_hit) = 1;
	hyssdbg_notice("watchdelete", "variable=\"%.*s\" recursive=\"%s\"", "%.*s has been removed, removing watchpoint%s", (int) ZSTR_LEN(child->str), ZSTR_VAL(child->str), (child->flags & HYSSDBG_WATCH_RECURSIVE_ROOT) ? " recursively" : "");
	gear_hash_index_del(&HYSSDBG_G(watch_elements), child->id);
	hyssdbg_free_watch_element_tree(element);
}

void hyssdbg_dequeue_elements_for_recreation() {
	hyssdbg_watch_element *element;

	GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(watch_recreation), element) {
		GEAR_ASSERT(element->flags & (HYSSDBG_WATCH_IMPLICIT | HYSSDBG_WATCH_RECURSIVE_ROOT | HYSSDBG_WATCH_SIMPLE));
		if (element->parent || gear_hash_index_find(&HYSSDBG_G(watch_free), (gear_ulong) element->parent_container)) {
			zval _zv, *zv = &_zv;
			if (element->parent) {
				GEAR_ASSERT(element->parent->watch->type == WATCH_ON_ZVAL || element->parent->watch->type == WATCH_ON_BUCKET);
				zv = element->parent->watch->addr.zv;
				while (Z_TYPE_P(zv) == IS_INDIRECT) {
					zv = Z_INDIRECT_P(zv);
				}
				ZVAL_DEREF(zv);
			} else {
				ZVAL_ARR(zv, element->parent_container);
			}
			if (!hyssdbg_try_readding_watch_element(zv, element)) {
				hyssdbg_automatic_dequeue_free(element);
			}
		} else {
			hyssdbg_automatic_dequeue_free(element);
		}
	} GEAR_HASH_FOREACH_END();

	gear_hash_clean(&HYSSDBG_G(watch_recreation));
	gear_hash_clean(&HYSSDBG_G(watch_free));
}

/* ### WATCH ELEMENT DELETION ### only use hyssdbg_remove_watch_element from the exterior */
void hyssdbg_clean_watch_element(hyssdbg_watch_element *element);

void hyssdbg_free_watch_element(hyssdbg_watch_element *element) {
	gear_string_release(element->str);
	if (element->name_in_parent) {
		gear_string_release(element->name_in_parent);
	}
	efree(element);
}

/* note: does *not* free the passed element, only clean */
void hyssdbg_remove_watch_element_recursively(hyssdbg_watch_element *element) {
	if (element->child) {
		hyssdbg_remove_watch_element_recursively(element->child);
		hyssdbg_free_watch_element(element->child);
		element->child = NULL;
	} else if (element->flags & (HYSSDBG_WATCH_ARRAY | HYSSDBG_WATCH_OBJECT)) {
		hyssdbg_watch_element *child;
		GEAR_HASH_FOREACH_PTR(&element->child_container, child) {
			hyssdbg_remove_watch_element_recursively(child);
			hyssdbg_free_watch_element(child);
		} GEAR_HASH_FOREACH_END();
		gear_hash_destroy(&element->child_container);
	}

	hyssdbg_clean_watch_element(element);
}

/* remove single watch (i.e. manual unset) or implicit removed */
void hyssdbg_remove_watch_element(hyssdbg_watch_element *element) {
	hyssdbg_watch_element *parent = element->parent, *child = element->child;
	while (parent) {
		hyssdbg_watch_element *cur = parent;
		parent = parent->parent;
		hyssdbg_clean_watch_element(cur);
		hyssdbg_free_watch_element(cur);
	}
	while (child) {
		hyssdbg_watch_element *cur = child;
		child = child->child;
		if (cur->flags & HYSSDBG_WATCH_RECURSIVE_ROOT) {
			hyssdbg_remove_watch_element_recursively(cur);
			child = NULL;
		} else {
			hyssdbg_clean_watch_element(cur);
		}
		hyssdbg_free_watch_element(cur);
	}
	if (element->flags & HYSSDBG_WATCH_RECURSIVE_ROOT) {
		hyssdbg_remove_watch_element_recursively(element);
	} else {
		hyssdbg_clean_watch_element(element);
	}
	gear_hash_index_del(&HYSSDBG_G(watch_elements), element->id);
	hyssdbg_free_watch_element(element);
}

void hyssdbg_backup_watch_element(hyssdbg_watch_element *element) {
	memcpy(&element->backup, &element->watch->backup, /* element->watch->size */ sizeof(element->backup));
}

/* until argument to prevent double remove of children elements */
void hyssdbg_dissociate_watch_element(hyssdbg_watch_element *element, hyssdbg_watch_element *until) {
	hyssdbg_watch_element *child = element;
	GEAR_ASSERT((element->flags & (HYSSDBG_WATCH_RECURSIVE_ROOT | HYSSDBG_WATCH_RECURSIVE)) != HYSSDBG_WATCH_RECURSIVE);

	if (element->flags & HYSSDBG_WATCH_RECURSIVE_ROOT) {
		hyssdbg_backup_watch_element(element);
		hyssdbg_remove_watch_element_recursively(element);
		return;
	}

	while (child->child != until) {
		child = child->child;
		if (child->flags & HYSSDBG_WATCH_RECURSIVE_ROOT) {
			hyssdbg_backup_watch_element(child);
			hyssdbg_remove_watch_element_recursively(child);
			child->child = NULL;
			break;
		}
		if (child->child == NULL || (child->flags & HYSSDBG_WATCH_RECURSIVE_ROOT)) {
			hyssdbg_backup_watch_element(child);
		}
		hyssdbg_clean_watch_element(child);
	}
	/* element needs to be removed last! */
	if (element->child == NULL) {
		hyssdbg_backup_watch_element(element);
	}
	hyssdbg_clean_watch_element(element);
}

/* unlike hyssdbg_remove_watch_element this *only* frees and does not clean up element + children! Only use after previous cleanup (e.g. hyssdbg_dissociate_watch_element) */
void hyssdbg_free_watch_element_tree(hyssdbg_watch_element *element) {
	hyssdbg_watch_element *parent = element->parent, *child = element->child;
	while (parent) {
		hyssdbg_watch_element *cur = parent;
		parent = parent->parent;
		hyssdbg_clean_watch_element(cur);
		hyssdbg_free_watch_element(cur);
	}
	while (child) {
		hyssdbg_watch_element *cur = child;
		child = child->child;
		hyssdbg_free_watch_element(cur);
	}
	hyssdbg_free_watch_element(element);
}

void hyssdbg_update_watch_element_watch(hyssdbg_watch_element *element) {
	if (element->flags & HYSSDBG_WATCH_IMPLICIT) {
		hyssdbg_watch_element *child = element->child;
		while (child->flags & HYSSDBG_WATCH_IMPLICIT) {
			child = child->child;
		}

		GEAR_ASSERT(element->watch->type == WATCH_ON_ZVAL || element->watch->type == WATCH_ON_BUCKET);
		hyssdbg_queue_element_for_recreation(element);
	} else if (element->flags & (HYSSDBG_WATCH_RECURSIVE_ROOT | HYSSDBG_WATCH_SIMPLE)) {
		hyssdbg_queue_element_for_recreation(element);
	} else if (element->flags & HYSSDBG_WATCH_RECURSIVE) {
		hyssdbg_remove_watch_element_recursively(element);
		if (element->parent->flags & (HYSSDBG_WATCH_OBJECT | HYSSDBG_WATCH_ARRAY)) {
			gear_hash_del(&element->parent->child_container, element->str);
		} else {
			element->parent->child = NULL;
		}
		hyssdbg_free_watch_element(element);
	}
}

void hyssdbg_update_watch_collision_elements(hyssdbg_watchpoint_t *watch) {
	hyssdbg_watchpoint_t *parent;
	hyssdbg_watch_element *element;

	GEAR_HASH_FOREACH_PTR(&watch->coll->parents, parent) {
		if (parent->coll) {
			hyssdbg_update_watch_collision_elements(parent);
		} else {
			GEAR_HASH_FOREACH_PTR(&parent->elements, element) {
				hyssdbg_update_watch_element_watch(element);
			} GEAR_HASH_FOREACH_END();
		}
	} GEAR_HASH_FOREACH_END();
}

void hyssdbg_remove_watchpoint(hyssdbg_watchpoint_t *watch) {
	hyssdbg_watch_element *element;

	hyssdbg_deactivate_watchpoint(watch);
	hyssdbg_remove_watchpoint_btree(watch);
	hyssdbg_delete_watch_collision(watch);

	if (watch->coll) {
		hyssdbg_update_watch_collision_elements(watch);
		return;
	}

	watch->elements.nNumOfElements++; /* dirty hack to avoid double free */
	GEAR_HASH_FOREACH_PTR(&watch->elements, element) {
		hyssdbg_update_watch_element_watch(element);
	} GEAR_HASH_FOREACH_END();
	gear_hash_destroy(&watch->elements);

	efree(watch);
}

void hyssdbg_clean_watch_element(hyssdbg_watch_element *element) {
	HashTable *elements = &element->watch->elements;
	hyssdbg_unwatch_parent_ht(element);
	gear_hash_del(elements, element->str);
	if (gear_hash_num_elements(elements) == 0) {
		hyssdbg_remove_watchpoint(element->watch);
	}
}

/* TODO: compile a name of all hit watchpoints (ids ??) */
gear_string *hyssdbg_watchpoint_change_collision_name(hyssdbg_watchpoint_t *watch) {
	hyssdbg_watchpoint_t *parent;
	hyssdbg_watch_element *element;
	gear_string *name = NULL;
	if (watch->coll) {
		GEAR_HASH_FOREACH_PTR(&watch->coll->parents, parent) {
			if (name) {
				gear_string_release(name);
			}
			name = hyssdbg_watchpoint_change_collision_name(parent);
		} GEAR_HASH_FOREACH_END();
		return name;
	}
	GEAR_HASH_FOREACH_PTR(&watch->elements, element) {
		if (element->flags & HYSSDBG_WATCH_IMPLICIT) {
			if ((watch->type == WATCH_ON_ZVAL || watch->type == WATCH_ON_BUCKET) && Z_TYPE(watch->backup.zv) > IS_STRING) {
				hyssdbg_update_watch_element_watch(element->child);
			}
			continue;
		}
		name = element->str;
	} GEAR_HASH_FOREACH_END();

	return name ? gear_string_copy(name) : NULL;
}

/* ### WATCHING FOR CHANGES ### */
/* TODO: enforce order: first parents, then children, in order to avoid false positives */
void hyssdbg_check_watchpoint(hyssdbg_watchpoint_t *watch) {
	gear_string *name = NULL;
	void *comparePtr;

	if (watch->type == WATCH_ON_HASHTABLE) {
		hyssdbg_watch_element *element;
		gear_string *str;
		gear_long idx;
		zval *zv;
		GEAR_HASH_FOREACH_PTR(&watch->elements, element) {
			if (element->flags & HYSSDBG_WATCH_RECURSIVE) {
				hyssdbg_btree_result *res = hyssdbg_btree_find(&HYSSDBG_G(watch_HashTables), (gear_ulong) HT_WATCH_HT(watch));
				hyssdbg_watch_ht_info *hti = res ? res->ptr : NULL;

				GEAR_HASH_REVERSE_FOREACH_KEY_VAL(HT_WATCH_HT(watch), idx, str, zv) {
					if (!str) {
						str = gear_long_to_str(idx); // TODO: hack, use proper int handling for name in parent
					} else {
						str = gear_string_copy(str);
					}
					if (hti && gear_hash_find(&hti->watches, str)) {
						gear_string_release(str);
						break;
					}
					GEAR_HASH_FOREACH_PTR(&watch->elements, element) {
						if (element->flags & HYSSDBG_WATCH_RECURSIVE) {
							hyssdbg_add_recursive_watch_from_ht(element, idx, str, zv);
						}
					} GEAR_HASH_FOREACH_END();
					hyssdbg_notice("watchadd", "element=\"%.*s\"", "Element %.*s has been added to watchpoint", (int) ZSTR_LEN(str), ZSTR_VAL(str));
					gear_string_release(str);
					HYSSDBG_G(watchpoint_hit) = 1;
				} GEAR_HASH_FOREACH_END();

				break;
			}
		} GEAR_HASH_FOREACH_END();
	}
	if (watch->type == WATCH_ON_HASHDATA) {
		return;
	}

	switch (watch->type) {
		case WATCH_ON_STR:
			comparePtr = &ZSTR_LEN(watch->backup.str);
			break;
		case WATCH_ON_HASHTABLE:
			comparePtr = (char *) &watch->backup.ht + HT_WATCH_OFFSET;
			break;
		default:
			comparePtr = &watch->backup;
	}
	if (!hyssdbg_check_watch_diff(watch->type, comparePtr, watch->addr.ptr)) {
		return;
	}
	if (watch->type == WATCH_ON_REFCOUNTED && !(HYSSDBG_G(flags) & HYSSDBG_SHOW_REFCOUNTS)) {
		hyssdbg_watch_backup_data(watch);
		return;
	}
	if (watch->type == WATCH_ON_BUCKET) {
		if (watch->backup.bucket.key != watch->addr.bucket->key || (watch->backup.bucket.key != NULL && watch->backup.bucket.h != watch->addr.bucket->h)) {
			hyssdbg_watch_element *element;
			zval *new;

			GEAR_HASH_FOREACH_PTR(&watch->elements, element) {
				break;
			} GEAR_HASH_FOREACH_END();

			new = gear_symtable_find(element->parent_container, element->name_in_parent);

			if (!new) {
				/* dequeuing will take care of appropriate notification about removal */
				hyssdbg_remove_watchpoint(watch);
				return;
			}

			hyssdbg_deactivate_watchpoint(watch);
			hyssdbg_remove_watchpoint_btree(watch);
			watch->addr.zv = new;
			hyssdbg_store_watchpoint_btree(watch);
			hyssdbg_activate_watchpoint(watch);

			if (!hyssdbg_check_watch_diff(WATCH_ON_ZVAL, &watch->backup.bucket.val, watch->addr.ptr)) {
				hyssdbg_watch_backup_data(watch);
				return;
			}
		} else if (Z_TYPE_P(watch->addr.zv) == IS_UNDEF) {
			/* dequeuing will take care of appropriate notification about removal */
			hyssdbg_remove_watchpoint(watch);
			return;
		}
	}

	name = hyssdbg_watchpoint_change_collision_name(watch);

	if (name) {
		hyssdbg_print_watch_diff(watch->type, name, comparePtr, watch->addr.ptr);
		gear_string_release(name);
	}

	if (watch->type == WATCH_ON_ZVAL || watch->type == WATCH_ON_BUCKET) {
		hyssdbg_watch_element *element;
		hyssdbg_update_watch_ref(watch);
		GEAR_HASH_FOREACH_PTR(&watch->elements, element) {
			if (element->flags & HYSSDBG_WATCH_RECURSIVE) {
				hyssdbg_recurse_watch_element(element);
			}
		} GEAR_HASH_FOREACH_END();
	}

	hyssdbg_watch_backup_data(watch);
}

void hyssdbg_reenable_memory_watches(void) {
	gear_ulong page;
	hyssdbg_btree_result *res;
	hyssdbg_watchpoint_t *watch;

	GEAR_HASH_FOREACH_NUM_KEY(HYSSDBG_G(watchlist_mem), page) {
		/* Disble writing again if there are any watchers on that page */
		res = hyssdbg_btree_find_closest(&HYSSDBG_G(watchpoint_tree), page + hyssdbg_pagesize - 1);
		if (res) {
			watch = res->ptr;
			if ((char *) page < (char *) watch->addr.ptr + watch->size) {
				mprotect((void *) page, hyssdbg_pagesize, PROT_READ);
			}
		}
	} GEAR_HASH_FOREACH_END();
	gear_hash_clean(HYSSDBG_G(watchlist_mem));
}

int hyssdbg_print_changed_zvals(void) {
	int ret;
	gear_ulong page;
	hyssdbg_watchpoint_t *watch;
	hyssdbg_btree_result *res;
	HashTable *mem_list = NULL;

	if (gear_hash_num_elements(&HYSSDBG_G(watch_elements)) == 0) {
		return FAILURE;
	}

	if (gear_hash_num_elements(HYSSDBG_G(watchlist_mem)) > 0) {
		/* we must not add elements to the hashtable while iterating over it (resize => read into freed memory) */
		mem_list = HYSSDBG_G(watchlist_mem);
		HYSSDBG_G(watchlist_mem) = HYSSDBG_G(watchlist_mem_backup);

		GEAR_HASH_FOREACH_NUM_KEY(mem_list, page) {
			hyssdbg_btree_position pos = hyssdbg_btree_find_between(&HYSSDBG_G(watchpoint_tree), page, page + hyssdbg_pagesize);

			while ((res = hyssdbg_btree_next(&pos))) {
				watch = res->ptr;
				hyssdbg_check_watchpoint(watch);
			}
			if ((res = hyssdbg_btree_find_closest(&HYSSDBG_G(watchpoint_tree), page - 1))) {
				watch = res->ptr;
				if ((char *) page < (char *) watch->addr.ptr + watch->size) {
					hyssdbg_check_watchpoint(watch);
				}
			}
		} GEAR_HASH_FOREACH_END();
	}

	hyssdbg_dequeue_elements_for_recreation();

	hyssdbg_reenable_memory_watches();

	if (mem_list) {
		HYSSDBG_G(watchlist_mem) = mem_list;
		hyssdbg_reenable_memory_watches();
	}

	ret = HYSSDBG_G(watchpoint_hit) ? SUCCESS : FAILURE;
	HYSSDBG_G(watchpoint_hit) = 0;

	return ret;
}

void hyssdbg_watch_efree(void *ptr) {
	hyssdbg_btree_result *result;

	/* only do expensive checks if there are any watches at all */
	if (gear_hash_num_elements(&HYSSDBG_G(watch_elements))) {
		if ((result = hyssdbg_btree_find(&HYSSDBG_G(watchpoint_tree), (gear_ulong) ptr))) {
			hyssdbg_watchpoint_t *watch = result->ptr;
			if (watch->type != WATCH_ON_HASHDATA) {
				hyssdbg_remove_watchpoint(watch);
			} else {
				/* remove all linked watchpoints, they will be dissociated from their elements */
				hyssdbg_watch_element *element;
				hyssdbg_watch_ht_info *hti = (hyssdbg_watch_ht_info *) watch;

				GEAR_HASH_FOREACH_PTR(&hti->watches, element) {
					gear_ulong num = gear_hash_num_elements(&hti->watches);
					hyssdbg_remove_watchpoint(element->watch);
					if (num == 1) { /* prevent access into freed memory */
						break;
					}
				} GEAR_HASH_FOREACH_END();
			}
		}

		/* special case watchpoints as they aren't on ptr but on ptr + HT_WATCH_OFFSET */
		if ((result = hyssdbg_btree_find(&HYSSDBG_G(watchpoint_tree), HT_WATCH_OFFSET + (gear_ulong) ptr))) {
			hyssdbg_watchpoint_t *watch = result->ptr;
			if (watch->type == WATCH_ON_HASHTABLE) {
				hyssdbg_remove_watchpoint(watch);
			}
		}

		gear_hash_index_del(&HYSSDBG_G(watch_free), (gear_ulong) ptr);
	}

	if (HYSSDBG_G(original_free_function)) {
		HYSSDBG_G(original_free_function)(ptr);
	}
}

/* ### USER API ### */
void hyssdbg_list_watchpoints(void) {
	hyssdbg_watch_element *element;

	hyssdbg_xml("<watchlist %r>");

	GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(watch_elements), element) {
		hyssdbg_writeln("watchvariable", "variable=\"%.*s\" on=\"%s\" type=\"%s\"", "%.*s (%s, %s)", (int) ZSTR_LEN(element->str), ZSTR_VAL(element->str), (element->flags & (HYSSDBG_WATCH_ARRAY|HYSSDBG_WATCH_OBJECT)) ? "array" : "variable", (element->flags & HYSSDBG_WATCH_RECURSIVE) ? "recursive" : "simple");
	} GEAR_HASH_FOREACH_END();

	hyssdbg_xml("</watchlist>");
}

static int hyssdbg_create_simple_watchpoint(zval *zv, hyssdbg_watch_element *element) {
	element->flags = HYSSDBG_WATCH_SIMPLE;
	hyssdbg_add_bucket_watch_element((Bucket *) zv, element);
	return SUCCESS;
}

static int hyssdbg_create_array_watchpoint(zval *zv, hyssdbg_watch_element *element) {
	hyssdbg_watch_element *new;
	gear_string *str;
	zval *orig_zv = zv;

	ZVAL_DEREF(zv);
	if (Z_TYPE_P(zv) != IS_ARRAY && Z_TYPE_P(zv) != IS_OBJECT) {
		return FAILURE;
	}

	new = ecalloc(1, sizeof(hyssdbg_watch_element));

	str = strpprintf(0, "%.*s[]", (int) ZSTR_LEN(element->str), ZSTR_VAL(element->str));
	gear_string_release(element->str);
	element->str = str;
	element->flags = HYSSDBG_WATCH_IMPLICIT;
	hyssdbg_add_bucket_watch_element((Bucket *) orig_zv, element);
	element->child = new;

	new->flags = HYSSDBG_WATCH_SIMPLE;
	new->str = gear_string_copy(str);
	new->parent = element;
	hyssdbg_add_ht_watch_element(zv, new);
	return SUCCESS;
}

static int hyssdbg_create_recursive_watchpoint(zval *zv, hyssdbg_watch_element *element) {
	element->flags = HYSSDBG_WATCH_RECURSIVE | HYSSDBG_WATCH_RECURSIVE_ROOT;
	element->child = NULL;
	hyssdbg_add_bucket_watch_element((Bucket *) zv, element);
	return SUCCESS;
}

typedef struct { int (*callback)(zval *zv, hyssdbg_watch_element *); gear_string *str; } hyssdbg_watch_parse_struct;

static int hyssdbg_watchpoint_parse_wrapper(char *name, size_t namelen, char *key, size_t keylen, HashTable *parent, zval *zv, hyssdbg_watch_parse_struct *info) {
	int ret;
	hyssdbg_watch_element *element = ecalloc(1, sizeof(hyssdbg_watch_element));
	element->str = gear_string_init(name, namelen, 0);
	element->name_in_parent = gear_string_init(key, keylen, 0);
	element->parent_container = parent;
	element->parent = HYSSDBG_G(watch_tmp);
	element->child = NULL;

	ret = info->callback(zv, element);

	efree(name);
	efree(key);

	if (ret != SUCCESS) {
		hyssdbg_remove_watch_element(element);
	} else {
		if (HYSSDBG_G(watch_tmp)) {
			HYSSDBG_G(watch_tmp)->child = element;
		}

		if (element->child) {
			element = element->child;
		}
		element->id = HYSSDBG_G(watch_elements).nNextFreeElement;
		gear_hash_index_add_ptr(&HYSSDBG_G(watch_elements), element->id, element);

		hyssdbg_notice("watchadd", "index=\"%d\" variable=\"%.*s\"", "Added%s watchpoint #%d for %.*s", (element->flags & HYSSDBG_WATCH_RECURSIVE_ROOT) ? " recursive" : "", element->id, (int) ZSTR_LEN(element->str), ZSTR_VAL(element->str));
	}

	HYSSDBG_G(watch_tmp) = NULL;

	return ret;
}

HYSSDBG_API int hyssdbg_watchpoint_parse_input(char *input, size_t len, HashTable *parent, size_t i, hyssdbg_watch_parse_struct *info, gear_bool silent) {
	return hyssdbg_parse_variable_with_arg(input, len, parent, i, (hyssdbg_parse_var_with_arg_func) hyssdbg_watchpoint_parse_wrapper, NULL, 0, info);
}

static int hyssdbg_watchpoint_parse_step(char *name, size_t namelen, char *key, size_t keylen, HashTable *parent, zval *zv, hyssdbg_watch_parse_struct *info) {
	hyssdbg_watch_element *element;

	/* do not install watch elements for references */
	if (HYSSDBG_G(watch_tmp) && Z_ISREF_P(HYSSDBG_G(watch_tmp)->watch->addr.zv) && Z_REFVAL_P(HYSSDBG_G(watch_tmp)->watch->addr.zv) == zv) {
		efree(name);
		efree(key);
		return SUCCESS;
	}

	element = ecalloc(1, sizeof(hyssdbg_watch_element));
	element->flags = HYSSDBG_WATCH_IMPLICIT;
	element->str = gear_string_copy(info->str);
	element->name_in_parent = gear_string_init(key, keylen, 0);
	element->parent_container = parent;
	element->parent = HYSSDBG_G(watch_tmp);
	element = hyssdbg_add_bucket_watch_element((Bucket *) zv, element);

	efree(name);
	efree(key);

	if (HYSSDBG_G(watch_tmp)) {
		HYSSDBG_G(watch_tmp)->child = element;
	}
	HYSSDBG_G(watch_tmp) = element;

	return SUCCESS;
}

static int hyssdbg_watchpoint_parse_symtables(char *input, size_t len, int (*callback)(zval *, hyssdbg_watch_element *)) {
	gear_class_entry *scope = gear_get_executed_scope();
	hyssdbg_watch_parse_struct info;
	int ret;

	if (scope && len >= 5 && !memcmp("$this", input, 5)) {
		gear_hash_str_add(EG(current_execute_data)->symbol_table, GEAR_STRL("this"), &EG(current_execute_data)->This);
	}

	if (callback == hyssdbg_create_array_watchpoint) {
		info.str = strpprintf(0, "%.*s[]", (int) len, input);
	} else {
		info.str = gear_string_init(input, len, 0);
	}
	info.callback = callback;

	if (hyssdbg_is_auto_global(input, len) && hyssdbg_watchpoint_parse_input(input, len, &EG(symbol_table), 0, &info, 1) != FAILURE) {
		gear_string_release(info.str);
		return SUCCESS;
	}

	ret = hyssdbg_parse_variable_with_arg(input, len, EG(current_execute_data)->symbol_table, 0, (hyssdbg_parse_var_with_arg_func) hyssdbg_watchpoint_parse_wrapper, (hyssdbg_parse_var_with_arg_func) hyssdbg_watchpoint_parse_step, 0, &info);

	gear_string_release(info.str);
	return ret;
}

HYSSDBG_WATCH(delete) /* {{{ */
{
	hyssdbg_watch_element *element;
	switch (param->type) {
		case NUMERIC_PARAM:
			if ((element = gear_hash_index_find_ptr(&HYSSDBG_G(watch_elements), param->num))) {
				hyssdbg_remove_watch_element(element);
				hyssdbg_notice("watchdelete", "variable=\"%.*s\"", "Removed watchpoint %d", (int) param->num);
			} else {
				hyssdbg_error("watchdelete", "type=\"nowatch\"", "Nothing was deleted, no corresponding watchpoint found");
			}
			break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

int hyssdbg_create_var_watchpoint(char *input, size_t len) {
	if (hyssdbg_rebuild_symtable() == FAILURE) {
		return FAILURE;
	}

	return hyssdbg_watchpoint_parse_symtables(input, len, hyssdbg_create_simple_watchpoint);
}

HYSSDBG_WATCH(recursive) /* {{{ */
{
	if (hyssdbg_rebuild_symtable() == FAILURE) {
		return SUCCESS;
	}

	switch (param->type) {
		case STR_PARAM:
			hyssdbg_watchpoint_parse_symtables(param->str, param->len, hyssdbg_create_recursive_watchpoint);
			break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */

HYSSDBG_WATCH(array) /* {{{ */
{
	if (hyssdbg_rebuild_symtable() == FAILURE) {
		return SUCCESS;
	}

	switch (param->type) {
		case STR_PARAM:
			hyssdbg_watchpoint_parse_symtables(param->str, param->len, hyssdbg_create_array_watchpoint);
			break;

		hyssdbg_default_switch_case();
	}

	return SUCCESS;
} /* }}} */


void hyssdbg_setup_watchpoints(void) {
#if _SC_PAGE_SIZE
	hyssdbg_pagesize = sysconf(_SC_PAGE_SIZE);
#elif _SC_PAGESIZE
	hyssdbg_pagesize = sysconf(_SC_PAGESIZE);
#elif _SC_NUTC_OS_PAGESIZE
	hyssdbg_pagesize = sysconf(_SC_NUTC_OS_PAGESIZE);
#else
	hyssdbg_pagesize = 4096; /* common pagesize */
#endif

	hyssdbg_btree_init(&HYSSDBG_G(watchpoint_tree), sizeof(void *) * 8);
	hyssdbg_btree_init(&HYSSDBG_G(watch_HashTables), sizeof(void *) * 8);
	gear_hash_init(&HYSSDBG_G(watch_elements), 8, NULL, NULL, 0);
	gear_hash_init(&HYSSDBG_G(watch_collisions), 8, NULL, NULL, 0);
	gear_hash_init(&HYSSDBG_G(watch_recreation), 8, NULL, NULL, 0);
	gear_hash_init(&HYSSDBG_G(watch_free), 8, NULL, NULL, 0);

	/* put these on a separate page, to avoid conflicts with other memory */
	HYSSDBG_G(watchlist_mem) = malloc(hyssdbg_pagesize > sizeof(HashTable) ? hyssdbg_pagesize : sizeof(HashTable));
	gear_hash_init(HYSSDBG_G(watchlist_mem), hyssdbg_pagesize / (sizeof(Bucket) + sizeof(uint32_t)), NULL, NULL, 1);
	HYSSDBG_G(watchlist_mem_backup) = malloc(hyssdbg_pagesize > sizeof(HashTable) ? hyssdbg_pagesize : sizeof(HashTable));
	gear_hash_init(HYSSDBG_G(watchlist_mem_backup), hyssdbg_pagesize / (sizeof(Bucket) + sizeof(uint32_t)), NULL, NULL, 1);
}

void hyssdbg_destroy_watchpoints(void) {
	hyssdbg_watch_element *element;
	hyssdbg_btree_position pos;
	hyssdbg_btree_result *res;

	/* unconditionally free all remaining elements to avoid memory leaks */
	GEAR_HASH_FOREACH_PTR(&HYSSDBG_G(watch_recreation), element) {
		hyssdbg_automatic_dequeue_free(element);
	} GEAR_HASH_FOREACH_END();

	/* upon fatal errors etc. (i.e. CG(unclean_shutdown) == 1), some watchpoints may still be active. Ensure memory is not watched anymore for next run. Do not care about memory freeing here, shutdown is unclean and near anyway. */
	pos = hyssdbg_btree_find_between(&HYSSDBG_G(watchpoint_tree), 0, -1);
	while ((res = hyssdbg_btree_next(&pos))) {
		hyssdbg_deactivate_watchpoint(res->ptr);
	}

	gear_hash_destroy(&HYSSDBG_G(watch_elements)); HYSSDBG_G(watch_elements).nNumOfElements = 0; /* hyssdbg_watch_efree() is checking against this arrays size */
	gear_hash_destroy(&HYSSDBG_G(watch_recreation));
	gear_hash_destroy(&HYSSDBG_G(watch_free));
	gear_hash_destroy(&HYSSDBG_G(watch_collisions));
	gear_hash_destroy(HYSSDBG_G(watchlist_mem));
	free(HYSSDBG_G(watchlist_mem));
	gear_hash_destroy(HYSSDBG_G(watchlist_mem_backup));
	free(HYSSDBG_G(watchlist_mem_backup));
}
