/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gear.h"
#include "hyssdbg.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_frame.h"
#include "hyssdbg_list.h"
#include "gear_smart_str.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

static inline void hyssdbg_append_individual_arg(smart_str *s, uint32_t i, gear_function *func, zval *arg) {
	const gear_arg_info *arginfo = func->common.arg_info;
	char *arg_name = NULL;

	if (i) {
		smart_str_appends(s, ", ");
	}
	if (i < func->common.num_args) {
		if (arginfo) {
			if (func->type == GEAR_INTERNAL_FUNCTION) {
				arg_name = (char *) ((gear_internal_arg_info *) &arginfo[i])->name;
			} else {
				arg_name = ZSTR_VAL(arginfo[i].name);
			}
		}
		smart_str_appends(s, arg_name ? arg_name : "?");
		smart_str_appendc(s, '=');
	}
	{
		char *arg_print = hyssdbg_short_zval_print(arg, 40);
		smart_str_appends(s, arg_print);
		efree(arg_print);
	}
}

gear_string *hyssdbg_compile_stackframe(gear_execute_data *ex) {
	smart_str s = {0};
	gear_op_array *op_array = &ex->func->op_array;
	uint32_t i = 0, first_extra_arg = op_array->num_args, num_args = GEAR_CALL_NUM_ARGS(ex);
	zval *p = GEAR_CALL_ARG(ex, 1);

	if (op_array->scope) {
		smart_str_append(&s, op_array->scope->name);
		smart_str_appends(&s, "::");
	}
	smart_str_append(&s, op_array->function_name);
	smart_str_appendc(&s, '(');
	if (GEAR_CALL_NUM_ARGS(ex) > first_extra_arg) {
		while (i < first_extra_arg) {
			hyssdbg_append_individual_arg(&s, i, ex->func, p);
			p++;
			i++;
		}
		p = GEAR_CALL_VAR_NUM(ex, op_array->last_var + op_array->T);
	}
	while (i < num_args) {
		hyssdbg_append_individual_arg(&s, i, ex->func, p);
		p++;
		i++;
	}
	smart_str_appendc(&s, ')');

	if (ex->func->type == GEAR_USER_FUNCTION) {
		smart_str_appends(&s, " at ");
		smart_str_append(&s, op_array->filename);
		smart_str_appendc(&s, ':');
		smart_str_append_unsigned(&s, ex->opline->lineno);
	} else {
		smart_str_appends(&s, " [internal function]");
	}

	return s.s;
}

void hyssdbg_print_cur_frame_info() {
	const char *file_chr = gear_get_executed_filename();
	gear_string *file = gear_string_init(file_chr, strlen(file_chr), 0);

	hyssdbg_list_file(file, 3, gear_get_executed_lineno() - 1, gear_get_executed_lineno());
	efree(file);
}

void hyssdbg_restore_frame(void) /* {{{ */
{
	if (HYSSDBG_FRAME(num) == 0) {
		return;
	}

	if (HYSSDBG_FRAME(generator)) {
		if (HYSSDBG_FRAME(generator)->execute_data->call) {
			HYSSDBG_FRAME(generator)->frozen_call_stack = gear_generator_freeze_call_stack(HYSSDBG_FRAME(generator)->execute_data);
		}
		HYSSDBG_FRAME(generator) = NULL;
	}

	HYSSDBG_FRAME(num) = 0;

	/* move things back */
	EG(current_execute_data) = HYSSDBG_FRAME(execute_data);
} /* }}} */

void hyssdbg_switch_frame(int frame) /* {{{ */
{
	gear_execute_data *execute_data = HYSSDBG_FRAME(num) ? HYSSDBG_FRAME(execute_data) : EG(current_execute_data);
	int i = 0;

	if (HYSSDBG_FRAME(num) == frame) {
		hyssdbg_notice("frame", "id=\"%d\"", "Already in frame #%d", frame);
		return;
	}

	hyssdbg_try_access {
		while (execute_data) {
			if (i++ == frame) {
				break;
			}

			do {
				execute_data = execute_data->prev_execute_data;
			} while (execute_data && execute_data->opline == NULL);
		}
	} hyssdbg_catch_access {
		hyssdbg_error("signalsegv", "", "Couldn't switch frames, invalid data source");
		return;
	} hyssdbg_end_try_access();

	if (execute_data == NULL) {
		hyssdbg_error("frame", "type=\"maxnum\" id=\"%d\"", "No frame #%d", frame);
		return;
	}

	hyssdbg_restore_frame();

	if (frame > 0) {
		HYSSDBG_FRAME(num) = frame;

		/* backup things and jump back */
		HYSSDBG_FRAME(execute_data) = EG(current_execute_data);
		EG(current_execute_data) = execute_data;
	}

	hyssdbg_try_access {
		gear_string *s = hyssdbg_compile_stackframe(EG(current_execute_data));
		hyssdbg_notice("frame", "id=\"%d\" frameinfo=\"%.*s\"", "Switched to frame #%d: %.*s", frame, (int) ZSTR_LEN(s), ZSTR_VAL(s));
		gear_string_release(s);
	} hyssdbg_catch_access {
		hyssdbg_notice("frame", "id=\"%d\"", "Switched to frame #%d", frame);
	} hyssdbg_end_try_access();

	hyssdbg_print_cur_frame_info();
} /* }}} */

static void hyssdbg_dump_prototype(zval *tmp) /* {{{ */
{
	zval *funcname, *class, class_zv, *type, *args, *argstmp;

	funcname = gear_hash_str_find(Z_ARRVAL_P(tmp), GEAR_STRL("function"));

	if ((class = gear_hash_str_find(Z_ARRVAL_P(tmp), GEAR_STRL("object")))) {
		ZVAL_NEW_STR(&class_zv, Z_OBJCE_P(class)->name);
		class = &class_zv;
	} else {
		class = gear_hash_str_find(Z_ARRVAL_P(tmp), GEAR_STRL("class"));
	}

	if (class) {
		type = gear_hash_str_find(Z_ARRVAL_P(tmp), GEAR_STRL("type"));
	}

	args = gear_hash_str_find(Z_ARRVAL_P(tmp), GEAR_STRL("args"));

	hyssdbg_xml(" symbol=\"%s%s%s\"", class ? Z_STRVAL_P(class) : "", class ? Z_STRVAL_P(type) : "", Z_STRVAL_P(funcname));

	if (args) {
		hyssdbg_xml(">");
	} else {
		hyssdbg_xml(" />");
	}

	hyssdbg_out("%s%s%s(", class ? Z_STRVAL_P(class) : "", class ? Z_STRVAL_P(type) : "", Z_STRVAL_P(funcname));

	if (args) {
		const gear_function *func = NULL;
		const gear_arg_info *arginfo = NULL;
		gear_bool is_variadic = 0;
		int j = 0, m;

		hyssdbg_try_access {
			/* assuming no autoloader call is necessary, class should have been loaded if it's in backtrace ... */
			if ((func = hyssdbg_get_function(Z_STRVAL_P(funcname), class ? Z_STRVAL_P(class) : NULL))) {
				arginfo = func->common.arg_info;
			}
		} hyssdbg_end_try_access();

		m = func ? func->common.num_args : 0;

		GEAR_HASH_FOREACH_VAL(Z_ARRVAL_P(args), argstmp) {
			if (j) {
				hyssdbg_out(", ");
			}
			hyssdbg_xml("<arg %r");
			if (m && j < m) {
				char *arg_name = NULL;

				if (arginfo) {
					if (func->type == GEAR_INTERNAL_FUNCTION) {
						arg_name = (char *)((gear_internal_arg_info *)&arginfo[j])->name;
					} else {
						arg_name = ZSTR_VAL(arginfo[j].name);
					}
				}

				if (!is_variadic) {
					is_variadic = arginfo ? arginfo[j].is_variadic : 0;
				}

				hyssdbg_xml(" variadic=\"%s\" name=\"%s\">", is_variadic ? "variadic" : "", arg_name ? arg_name : "");
				hyssdbg_out("%s=%s", arg_name ? arg_name : "?", is_variadic ? "[": "");

			} else {
				hyssdbg_xml(">");
			}
			++j;

			{
				char *arg_print = hyssdbg_short_zval_print(argstmp, 40);
				hyss_printf("%s", arg_print);
				efree(arg_print);
			}

			hyssdbg_xml("</arg>");
		} GEAR_HASH_FOREACH_END();

		if (is_variadic) {
			hyssdbg_out("]");
		}
		hyssdbg_xml("</frame>");
	}
	hyssdbg_out(")");
}

void hyssdbg_dump_backtrace(size_t num) /* {{{ */
{
	HashPosition position;
	zval zbacktrace;
	zval *tmp;
	zval startline, startfile;
	const char *startfilename;
	zval *file = &startfile, *line = &startline;
	int i = 0, limit = num;

	HYSSDBG_OUTPUT_BACKUP();

	if (limit < 0) {
		hyssdbg_error("backtrace", "type=\"minnum\"", "Invalid backtrace size %d", limit);

		HYSSDBG_OUTPUT_BACKUP_RESTORE();
		return;
	}

	hyssdbg_try_access {
		gear_fetch_debug_backtrace(&zbacktrace, 0, 0, limit);
	} hyssdbg_catch_access {
		hyssdbg_error("signalsegv", "", "Couldn't fetch backtrace, invalid data source");
		return;
	} hyssdbg_end_try_access();

	hyssdbg_xml("<backtrace %r>");

	Z_LVAL(startline) = gear_get_executed_lineno();
	startfilename = gear_get_executed_filename();
	Z_STR(startfile) = gear_string_init(startfilename, strlen(startfilename), 0);

	gear_hash_internal_pointer_reset_ex(Z_ARRVAL(zbacktrace), &position);
	tmp = gear_hash_get_current_data_ex(Z_ARRVAL(zbacktrace), &position);
	while ((tmp = gear_hash_get_current_data_ex(Z_ARRVAL(zbacktrace), &position))) {
		if (file) { /* userland */
			hyssdbg_out("frame #%d: ", i);
			hyssdbg_xml("<frame %r id=\"%d\" file=\"%s\" line=\"" GEAR_LONG_FMT "\"", i, Z_STRVAL_P(file), Z_LVAL_P(line));
			hyssdbg_dump_prototype(tmp);
			hyssdbg_out(" at %s:%ld\n", Z_STRVAL_P(file), Z_LVAL_P(line));
			i++;
		} else {
			hyssdbg_out(" => ");
			hyssdbg_xml("<frame %r id=\"%d\" internal=\"internal\"", i);
			hyssdbg_dump_prototype(tmp);
			hyssdbg_out(" (internal function)\n");
		}

		file = gear_hash_str_find(Z_ARRVAL_P(tmp), GEAR_STRL("file"));
		line = gear_hash_str_find(Z_ARRVAL_P(tmp), GEAR_STRL("line"));
		gear_hash_move_forward_ex(Z_ARRVAL(zbacktrace), &position);
	}

	hyssdbg_writeln("frame", "id=\"%d\" symbol=\"{main}\" file=\"%s\" line=\"%d\"", "frame #%d: {main} at %s:%ld", i, Z_STRVAL_P(file), Z_LVAL_P(line));
	hyssdbg_xml("</backtrace>");

	zval_ptr_dtor_nogc(&zbacktrace);
	gear_string_release(Z_STR(startfile));

	HYSSDBG_OUTPUT_BACKUP_RESTORE();
} /* }}} */

void hyssdbg_open_generator_frame(gear_generator *gen) {
	gear_string *s;

	if (EG(current_execute_data) == gen->execute_data) {
		return;
	}

	hyssdbg_restore_frame();

	HYSSDBG_FRAME(num) = -1;
	HYSSDBG_FRAME(generator) = gen;

	EG(current_execute_data) = gen->execute_data;
	if (gen->frozen_call_stack) {
		gear_generator_restore_call_stack(gen);
	}
	gen->execute_data->prev_execute_data = NULL;

	s = hyssdbg_compile_stackframe(EG(current_execute_data));
	hyssdbg_notice("frame", "handle=\"%d\" frameinfo=\"%.*s\"", "Switched to generator with handle #%d: %.*s", gen->std.handle, (int) ZSTR_LEN(s), ZSTR_VAL(s));
	gear_string_release(s);
	hyssdbg_print_cur_frame_info();
}
