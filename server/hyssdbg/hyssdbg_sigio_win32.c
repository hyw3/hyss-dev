/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <signal.h>

#include "hyssdbg.h"
#include "hyssdbg_sigio_win32.h"


GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)


VOID
SigIoWatcherThread(VOID *p)
{
	gear_uchar sig;
	struct win32_sigio_watcher_data *swd = (struct win32_sigio_watcher_data *)p;

top:
	(void)hyssdbg_consume_bytes(swd->fd, &sig, 1, -1);


	if (3 == sig) {
		/* XXX completely not sure it is done right here */
		if (*swd->flags & HYSSDBG_IS_INTERACTIVE) {
			if (raise(sig)) {
				goto top;
			}
		}
		if (*swd->flags & HYSSDBG_IS_SIGNALED) {
			hyssdbg_set_sigsafe_mem(&sig);
			gear_try {
				hyssdbg_force_interruption();
			} gear_end_try();
			hyssdbg_clear_sigsafe_mem();
			goto end;
		}
		if (!(*swd->flags & HYSSDBG_IS_INTERACTIVE)) {
			*swd->flags |= HYSSDBG_IS_SIGNALED;
		}
end:
		/* XXX set signaled flag to the caller thread, question is - whether it's needed */
		ExitThread(sig);
	} else {
		goto top;
	}
}


/* Start this only for the time of the run or eval command,
for so long that the main thread is busy serving some debug
session. */
void
sigio_watcher_start(void)
{

	HYSSDBG_G(swd).fd = HYSSDBG_G(io)[HYSSDBG_STDIN].fd;
#ifdef ZTS
	HYSSDBG_G(swd).flags = &HYSSDBG_G(flags);
#endif

	HYSSDBG_G(sigio_watcher_thread) = CreateThread(
		NULL,
		0,
		(LPTHREAD_START_ROUTINE)SigIoWatcherThread,
		&HYSSDBG_G(swd),
		0,
		NULL);
}

void
sigio_watcher_stop(void)
{
	DWORD waited;

	if (INVALID_HANDLE_VALUE == HYSSDBG_G(sigio_watcher_thread)) {
		/* it probably did bail out already */
		return;
	}

	waited = WaitForSingleObject(HYSSDBG_G(sigio_watcher_thread), 300);

	if (WAIT_OBJECT_0 != waited) {
		if (!CancelSynchronousIo(HYSSDBG_G(sigio_watcher_thread))) {
			/* error out */
		}

		if (!TerminateThread(HYSSDBG_G(sigio_watcher_thread), 0)) {
			/* error out */
		}
	}

	HYSSDBG_G(swd).fd = -1;
	HYSSDBG_G(sigio_watcher_thread) = INVALID_HANDLE_VALUE;
}
