/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_OPCODE_H
#define HYSSDBG_OPCODE_H

#include "gear_types.h"

char *hyssdbg_decode_opline(gear_op_array *ops, gear_op *op);
void hyssdbg_print_opline(gear_execute_data *execute_data, gear_bool ignore_flags);
void hyssdbg_print_opline_ex(gear_execute_data *execute_data, gear_bool ignore_flags);

typedef struct _hyssdbg_oplog_entry hyssdbg_oplog_entry;
struct _hyssdbg_oplog_entry {
	hyssdbg_oplog_entry *next;
	gear_string *function_name;
	gear_class_entry *scope;
	gear_string *filename;
	gear_op *opcodes;
	gear_op *op;
};

typedef struct _hyssdbg_oplog_list hyssdbg_oplog_list;
struct _hyssdbg_oplog_list {
	hyssdbg_oplog_list *prev;
	hyssdbg_oplog_entry *start;
};

#endif /* HYSSDBG_OPCODE_H */
