/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "hyssdbg.h"
#include "hyssdbg_eol.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

#define EOL_LIST_LEN 4
struct hyssdbg_eol_rep hyssdbg_eol_list[EOL_LIST_LEN] = {
	{"CRLF", "\r\n", HYSSDBG_EOL_CRLF},
/*	{"LFCR", "\n\r", HYSSDBG_EOL_LFCR},*/
	{"LF", "\n", HYSSDBG_EOL_LF},
	{"CR", "\r", HYSSDBG_EOL_CR},
};

int hyssdbg_eol_global_update(char *name)
{

	if (0 == memcmp(name, "CRLF", 4) || 0 == memcmp(name, "crlf", 4) || 0 == memcmp(name, "DOS", 3) || 0 == memcmp(name, "dos", 3)) {
		HYSSDBG_G(eol) = HYSSDBG_EOL_CRLF;
	} else if (0 == memcmp(name, "LF", 2) || 0 == memcmp(name, "lf", 2) || 0 == memcmp(name, "UNIX", 4) || 0 == memcmp(name, "unix", 4)) {
		HYSSDBG_G(eol) = HYSSDBG_EOL_LF;
	} else if (0 == memcmp(name, "CR", 2) || 0 == memcmp(name, "cr", 2) || 0 == memcmp(name, "MAC", 3) || 0 == memcmp(name, "mac", 3)) {
		HYSSDBG_G(eol) = HYSSDBG_EOL_CR;
	} else {
		return FAILURE;
	}

	return SUCCESS;
}

char *hyssdbg_eol_name(int id)
{
	size_t i = 0;

	while (i < EOL_LIST_LEN) {

		if (id == hyssdbg_eol_list[i].id) {
			return hyssdbg_eol_list[i].name;
		}

		i++;
	}

	return NULL;
}

char *hyssdbg_eol_rep(int id)
{
	size_t i = 0;

	while (i < EOL_LIST_LEN) {

		if (id == hyssdbg_eol_list[i].id) {
			return hyssdbg_eol_list[i].rep;
		}

		i++;
	}

	return NULL;
}


/* Inspired by https://ccrma.stanford.edu/~craig/utility/flip/flip.cpp */
void hyssdbg_eol_convert(char **str, int *len)
{
	char *in = *str, *out ;
	int in_len = *len, out_len, cursor, i;
	char last, cur;

	if ((HYSSDBG_G(flags) & HYSSDBG_IS_REMOTE) != HYSSDBG_IS_REMOTE) {
		return;
	}

	out_len = *len;
	if (HYSSDBG_EOL_CRLF == HYSSDBG_G(eol)) { /* XXX add LFCR case if it's gonna be needed */
		/* depending on the source EOL the out str will have all CR/LF duplicated */
		for (i = 0; i < in_len; i++) {
			if (0x0a == in[i] || 0x0d == in[i]) {
				out_len++;
			}
		}
		out = (char *)emalloc(out_len);

		last = cur = in[0];
		i = cursor = 0;
		for (; i < in_len;) {
			if (0x0a == cur && last != 0x0d) {
				out[cursor] = 0x0d;
				cursor++;
				out[cursor] = cur;
			} else if(0x0d == cur) {
				if (i + 1 < in_len && 0x0a != in[i+1]) {
					out[cursor] = cur;
					cursor++;
					out[cursor] = 0x0a;
					last = 0x0a;
				} else {
					out[cursor] = 0x0d;
					last = 0x0d;
				}
			} else {
				out[cursor] = cur;
				last = cur;
			}

			i++;
			cursor++;
			cur = in[i];
		}

	} else if (HYSSDBG_EOL_LF == HYSSDBG_G(eol) || HYSSDBG_EOL_CR == HYSSDBG_G(eol)) {
		char want, kick;

		if (HYSSDBG_EOL_LF == HYSSDBG_G(eol)) {
			want = 0x0a;
			kick = 0x0d;
		} else {
			want = 0x0d;
			kick = 0x0a;
		}

		/* We gonna have a smaller or equally long string, estimation is almost neglecting */
		out = (char *)emalloc(out_len);

		last = cur = in[0];
		i = cursor = 0;
		for (; cursor < in_len;) {
			if (kick == cur) {
				out[cursor] = want;
			} else if (want == cur) {
				if (kick != last) {
					out[cursor] = want;
				}
			} else {
				out[cursor] = cur;
			}

			last = cur;
			cursor++;
			cur = in[cursor];
		}
	} else {
		return;
	}

	efree(*str);
	*str = erealloc(out, cursor);
	*len = cursor;
	in = NULL;
}
