/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyssdbg.h"
#include "hyssdbg_print.h"
#include "hyssdbg_utils.h"
#include "hyssdbg_opcode.h"
#include "hyssdbg_break.h"
#include "hyssdbg_bp.h"
#include "hyssdbg_prompt.h"

GEAR_EXTERN_CAPI_GLOBALS(hyssdbg)

#define HYSSDBG_BREAK_COMMAND_D(f, h, a, m, l, s, flags) \
	HYSSDBG_COMMAND_D_EXP(f, h, a, m, l, s, &hyssdbg_prompt_commands[9], flags)

/**
 * Commands
 */
const hyssdbg_command_t hyssdbg_break_commands[] = {
	HYSSDBG_BREAK_COMMAND_D(at,         "specify breakpoint by location and condition",           '@', break_at,      NULL, "*c", 0),
	HYSSDBG_BREAK_COMMAND_D(del,        "delete breakpoint by identifier number",                 '~', break_del,     NULL, "n",  0),
	HYSSDBG_END_COMMAND
};

HYSSDBG_BREAK(at) /* {{{ */
{
	hyssdbg_set_breakpoint_at(param);

	return SUCCESS;
} /* }}} */

HYSSDBG_BREAK(del) /* {{{ */
{
	hyssdbg_delete_breakpoint(param->num);

	return SUCCESS;
} /* }}} */
