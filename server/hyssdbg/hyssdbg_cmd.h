/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_CMD_H
#define HYSSDBG_CMD_H

#include "hypbc.h"
#include "gear_generators.h"

/* {{{ Command and Parameter */
enum {
	NO_ARG = 0,
	REQUIRED_ARG,
	OPTIONAL_ARG
};

typedef enum {
	EMPTY_PARAM = 0,
	ADDR_PARAM,
	FILE_PARAM,
	NUMERIC_FILE_PARAM,
	METHOD_PARAM,
	STR_PARAM,
	NUMERIC_PARAM,
	NUMERIC_FUNCTION_PARAM,
	NUMERIC_METHOD_PARAM,
	STACK_PARAM,
	EVAL_PARAM,
	SHELL_PARAM,
	COND_PARAM,
	OP_PARAM,
	ORIG_PARAM,
	RUN_PARAM
} hyssdbg_param_type;

typedef struct _hyssdbg_param hyssdbg_param_t;
struct _hyssdbg_param {
	hyssdbg_param_type type;
	long num;
	gear_ulong addr;
	struct {
		char *name;
		long line;
	} file;
	struct {
		char *class;
		char *name;
	} method;
	char *str;
	size_t len;
	hyssdbg_param_t *next;
	hyssdbg_param_t *top;
};

#define hyssdbg_init_param(v, t) do{ \
	(v)->type = (t); \
	(v)->addr = 0; \
	(v)->num = 0; \
	(v)->file.name = NULL; \
	(v)->file.line = 0; \
	(v)->method.class = NULL; \
	(v)->method.name = NULL; \
	(v)->str = NULL; \
	(v)->len = 0; \
	(v)->next = NULL; \
	(v)->top = NULL; \
} while(0)

#ifndef YYSTYPE
#define YYSTYPE hyssdbg_param_t
#endif

#define HYSSDBG_ASYNC_SAFE 1

typedef int (*hyssdbg_command_handler_t)(const hyssdbg_param_t*);

typedef struct _hyssdbg_command_t hyssdbg_command_t;
struct _hyssdbg_command_t {
	const char *name;                   /* Command name */
	size_t name_len;                    /* Command name length */
	const char *tip;                    /* Menu tip */
	size_t tip_len;                     /* Menu tip length */
	char alias;                         /* Alias */
	hyssdbg_command_handler_t handler;   /* Command handler */
	const hyssdbg_command_t *subs;       /* Sub Commands */
	char *args;                         /* Argument Spec */
	const hyssdbg_command_t *parent;     /* Parent Command */
	gear_bool flags;                    /* General flags */
};
/* }}} */

/* {{{ misc */
#define HYSSDBG_STRL(s) s, sizeof(s)-1
#define HYSSDBG_MAX_CMD 500
#define HYSSDBG_FRAME(v) (HYSSDBG_G(frame).v)
#define HYSSDBG_EX(v) (EG(current_execute_data)->v)

typedef struct {
	int num;
	gear_generator *generator;
	gear_execute_data *execute_data;
} hyssdbg_frame_t;
/* }}} */

/*
* Workflow:
* 1) the lexer/parser creates a stack of commands and arguments from input
* 2) the commands at the top of the stack are resolved sensibly using aliases, abbreviations and case insensitive matching
* 3) the remaining arguments in the stack are verified (optionally) against the handlers declared argument specification
* 4) the handler is called passing the top of the stack as the only parameter
* 5) the stack is destroyed upon return from the handler
*/

/*
* Input Management
*/
HYSSDBG_API char* hyssdbg_read_input(char *buffered);
HYSSDBG_API void hyssdbg_destroy_input(char**);
HYSSDBG_API int hyssdbg_ask_user_permission(const char *question);

/**
 * Stack Management
 */
HYSSDBG_API void hyssdbg_stack_push(hyssdbg_param_t *stack, hyssdbg_param_t *param);
HYSSDBG_API void hyssdbg_stack_separate(hyssdbg_param_t *param);
HYSSDBG_API const hyssdbg_command_t *hyssdbg_stack_resolve(const hyssdbg_command_t *commands, const hyssdbg_command_t *parent, hyssdbg_param_t **top);
HYSSDBG_API int hyssdbg_stack_verify(const hyssdbg_command_t *command, hyssdbg_param_t **stack);
HYSSDBG_API int hyssdbg_stack_execute(hyssdbg_param_t *stack, gear_bool allow_async_unsafe);
HYSSDBG_API void hyssdbg_stack_free(hyssdbg_param_t *stack);

/*
* Parameter Management
*/
HYSSDBG_API void hyssdbg_clear_param(hyssdbg_param_t*);
HYSSDBG_API void hyssdbg_copy_param(const hyssdbg_param_t*, hyssdbg_param_t*);
HYSSDBG_API gear_bool hyssdbg_match_param(const hyssdbg_param_t *, const hyssdbg_param_t *);
HYSSDBG_API gear_ulong hyssdbg_hash_param(const hyssdbg_param_t *);
HYSSDBG_API const char* hyssdbg_get_param_type(const hyssdbg_param_t*);
HYSSDBG_API char* hyssdbg_param_tostring(const hyssdbg_param_t *param, char **pointer);
HYSSDBG_API void hyssdbg_param_debug(const hyssdbg_param_t *param, const char *msg);

/**
 * Command Declarators
 */
#define HYSSDBG_COMMAND_HANDLER(name) hyssdbg_do_##name

#define HYSSDBG_COMMAND_D_EXP(name, tip, alias, handler, children, args, parent, flags) \
	{HYSSDBG_STRL(#name), tip, sizeof(tip)-1, alias, hyssdbg_do_##handler, children, args, parent, flags}

#define HYSSDBG_COMMAND_D_EX(name, tip, alias, handler, children, args, flags) \
	{HYSSDBG_STRL(#name), tip, sizeof(tip)-1, alias, hyssdbg_do_##handler, children, args, NULL, flags}

#define HYSSDBG_COMMAND_D(name, tip, alias, children, args, flags) \
	{HYSSDBG_STRL(#name), tip, sizeof(tip)-1, alias, hyssdbg_do_##name, children, args, NULL, flags}

#define HYSSDBG_COMMAND(name) int hyssdbg_do_##name(const hyssdbg_param_t *param)

#define HYSSDBG_COMMAND_ARGS param

#define HYSSDBG_END_COMMAND {NULL, 0, NULL, 0, '\0', NULL, NULL, NULL, NULL, 0}

/*
* Default Switch Case
*/
#define hyssdbg_default_switch_case() \
	default: \
		hyssdbg_error("command", "type=\"wrongarg\" got=\"%s\"", "Unsupported parameter type (%s) for command", hyssdbg_get_param_type(param)); \
	break

#endif /* HYSSDBG_CMD_H */
