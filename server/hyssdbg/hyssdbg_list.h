/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_LIST_H
#define HYSSDBG_LIST_H

#include "hypbc.h"
#include "hyssdbg_cmd.h"

#define HYSSDBG_LIST(name)         HYSSDBG_COMMAND(list_##name)
#define HYSSDBG_LIST_HANDLER(name) HYSSDBG_COMMAND_HANDLER(list_##name)

HYSSDBG_LIST(lines);
HYSSDBG_LIST(class);
HYSSDBG_LIST(method);
HYSSDBG_LIST(func);

void hyssdbg_list_function_byname(const char *, size_t);
void hyssdbg_list_function(const gear_function *);
void hyssdbg_list_file(gear_string *, uint32_t, int, uint32_t);

extern const hyssdbg_command_t hyssdbg_list_commands[];

void hyssdbg_init_list(void);
void hyssdbg_list_update(void);

typedef struct {
	char *buf;
	size_t len;
#if HAVE_MMAP
	void *map;
#endif
	gear_op_array op_array;
	uint32_t lines;
	uint32_t line[1];
} hyssdbg_file_source;

#endif /* HYSSDBG_LIST_H */
