/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSSDBG_PRINT_H
#define HYSSDBG_PRINT_H

#include "hyssdbg_cmd.h"

#define HYSSDBG_PRINT(name) HYSSDBG_COMMAND(print_##name)

/**
 * Printer Forward Declarations
 */
HYSSDBG_PRINT(exec);
HYSSDBG_PRINT(opline);
HYSSDBG_PRINT(class);
HYSSDBG_PRINT(method);
HYSSDBG_PRINT(func);
HYSSDBG_PRINT(stack);

HYSSDBG_API void hyssdbg_print_opcodes(char *function);

extern const hyssdbg_command_t hyssdbg_print_commands[];

#endif /* HYSSDBG_PRINT_H */
