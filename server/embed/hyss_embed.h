/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HYSS_EMBED_H_
#define _HYSS_EMBED_H_

#include <main/hyss.h>
#include <main/SAPI.h>
#include <main/hyss_main.h>
#include <main/hyss_variables.h>
#include <main/hyss_ics.h>
#include <gear_ics.h>

#define HYSS_EMBED_START_BLOCK(x,y) { \
    hyss_embed_init(x, y); \
    gear_first_try {

#define HYSS_EMBED_END_BLOCK() \
  } gear_catch { \
    /* int exit_status = EG(exit_status); */ \
  } gear_end_try(); \
  hyss_embed_shutdown(); \
}

#ifndef HYSS_WIN32
    #define EMBED_SAPI_API SAPI_API
#else
    #define EMBED_SAPI_API
#endif

#ifdef ZTS
GEAR_PBCLS_CACHE_EXTERN()
#endif

BEGIN_EXTERN_C()
EMBED_SAPI_API int hyss_embed_init(int argc, char **argv);
EMBED_SAPI_API void hyss_embed_shutdown(void);
extern EMBED_SAPI_API sapi_capi_struct hyss_embed_capi;
END_EXTERN_C()


#endif /* _HYSS_EMBED_H_ */
