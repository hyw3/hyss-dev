/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss_embed.h"
#include "extslib/standard/hyss_standard.h"

#ifdef HYSS_WIN32
#include <io.h>
#include <fcntl.h>
#endif

const char HARDCODED_ICS[] =
	"html_errors=0\n"
	"register_argc_argv=1\n"
	"implicit_flush=1\n"
	"output_buffering=0\n"
	"max_execution_time=0\n"
	"max_input_time=-1\n\0";

#if defined(HYSS_WIN32) && defined(ZTS)
GEAR_PBCLS_CACHE_DEFINE()
#endif

static char* hyss_embed_read_cookies(void)
{
	return NULL;
}

static int hyss_embed_deactivate(void)
{
	fflush(stdout);
	return SUCCESS;
}

static inline size_t hyss_embed_single_write(const char *str, size_t str_length)
{
#ifdef HYSS_WRITE_STDOUT
	gear_long ret;

	ret = write(STDOUT_FILENO, str, str_length);
	if (ret <= 0) return 0;
	return ret;
#else
	size_t ret;

	ret = fwrite(str, 1, MIN(str_length, 16384), stdout);
	return ret;
#endif
}


static size_t hyss_embed_ub_write(const char *str, size_t str_length)
{
	const char *ptr = str;
	size_t remaining = str_length;
	size_t ret;

	while (remaining > 0) {
		ret = hyss_embed_single_write(ptr, remaining);
		if (!ret) {
			hyss_handle_aborted_connection();
		}
		ptr += ret;
		remaining -= ret;
	}

	return str_length;
}

static void hyss_embed_flush(void *server_context)
{
	if (fflush(stdout)==EOF) {
		hyss_handle_aborted_connection();
	}
}

static void hyss_embed_send_header(sapi_header_struct *sapi_header, void *server_context)
{
}

static void hyss_embed_log_message(char *message, int syslog_type_int)
{
	fprintf (stderr, "%s\n", message);
}

static void hyss_embed_register_variables(zval *track_vars_array)
{
	hyss_import_environment_variables(track_vars_array);
}

static int hyss_embed_startup(sapi_capi_struct *sapi_capi)
{
	if (hyss_capi_startup(sapi_capi, NULL, 0)==FAILURE) {
		return FAILURE;
	}
	return SUCCESS;
}

EMBED_SAPI_API sapi_capi_struct hyss_embed_capi = {
	"embed",                       /* name */
	"HYSS Embedded Library",        /* pretty name */

	hyss_embed_startup,              /* startup */
	hyss_capi_shutdown_wrapper,   /* shutdown */

	NULL,                          /* activate */
	hyss_embed_deactivate,           /* deactivate */

	hyss_embed_ub_write,             /* unbuffered write */
	hyss_embed_flush,                /* flush */
	NULL,                          /* get uid */
	NULL,                          /* getenv */

	hyss_error,                     /* error handler */

	NULL,                          /* header handler */
	NULL,                          /* send headers handler */
	hyss_embed_send_header,          /* send header handler */

	NULL,                          /* read POST data */
	hyss_embed_read_cookies,         /* read Cookies */

	hyss_embed_register_variables,   /* register server variables */
	hyss_embed_log_message,          /* Log message */
	NULL,							/* Get request time */
	NULL,							/* Child terminate */

	STANDARD_SAPI_CAPI_PROPERTIES
};
/* }}} */

/* {{{ arginfo extslib/standard/dl.c */
GEAR_BEGIN_ARG_INFO(arginfo_dl, 0)
	GEAR_ARG_INFO(0, extension_filename)
GEAR_END_ARG_INFO()
/* }}} */

static const gear_function_entry additional_functions[] = {
	GEAR_FE(dl, arginfo_dl)
	{NULL, NULL, NULL}
};

EMBED_SAPI_API int hyss_embed_init(int argc, char **argv)
{
	gear_llist global_vars;

#ifdef HAVE_SIGNAL_H
#if defined(SIGPIPE) && defined(SIG_IGN)
	signal(SIGPIPE, SIG_IGN); /* ignore SIGPIPE in standalone mode so
								 that sockets created via fsockopen()
								 don't kill HYSS if the remote site
								 closes it.  in clhy|clhyext mode clhy
								 does that for us!  thies@thieso.net
								 20000419 */
#endif
#endif

#ifdef ZTS
  pbc_startup(1, 1, 0, NULL);
  (void)ts_resource(0);
  GEAR_PBCLS_CACHE_UPDATE();
#endif

	gear_signal_startup();

  sapi_startup(&hyss_embed_capi);

#ifdef HYSS_WIN32
  _fmode = _O_BINARY;			/*sets default for file streams to binary */
  setmode(_fileno(stdin), O_BINARY);		/* make the stdio mode be binary */
  setmode(_fileno(stdout), O_BINARY);		/* make the stdio mode be binary */
  setmode(_fileno(stderr), O_BINARY);		/* make the stdio mode be binary */
#endif

  hyss_embed_capi.ics_entries = malloc(sizeof(HARDCODED_ICS));
  memcpy(hyss_embed_capi.ics_entries, HARDCODED_ICS, sizeof(HARDCODED_ICS));

  hyss_embed_capi.additional_functions = additional_functions;

  if (argv) {
	hyss_embed_capi.executable_location = argv[0];
  }

  if (hyss_embed_capi.startup(&hyss_embed_capi)==FAILURE) {
	  return FAILURE;
  }

  gear_llist_init(&global_vars, sizeof(char *), NULL, 0);

  /* Set some Embedded HYSS defaults */
  SG(options) |= SAPI_OPTION_NO_CHDIR;
  SG(request_info).argc=argc;
  SG(request_info).argv=argv;

  if (hyss_request_startup()==FAILURE) {
	  hyss_capi_shutdown();
	  return FAILURE;
  }

  SG(headers_sent) = 1;
  SG(request_info).no_headers = 1;
  hyss_register_variable("HYSS_SELF", "-", NULL);

  return SUCCESS;
}

EMBED_SAPI_API void hyss_embed_shutdown(void)
{
	hyss_request_shutdown((void *) 0);
	hyss_capi_shutdown();
	sapi_shutdown();
#ifdef ZTS
    pbc_shutdown();
	PBCLS_CACHE_RESET();
#endif
	if (hyss_embed_capi.ics_entries) {
		free(hyss_embed_capi.ics_entries);
		hyss_embed_capi.ics_entries = NULL;
	}
}

