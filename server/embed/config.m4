dnl config.m4 for sapi embed

HYSS_ARG_ENABLE(embed,,
[  --enable-embed[=TYPE]     EXPERIMENTAL: Enable building of embedded SAPI library
                          TYPE is either 'shared' or 'static'. [TYPE=shared]], no, no)

AC_MSG_CHECKING([for embedded SAPI library support])

if test "$HYSS_EMBED" != "no"; then
  case "$HYSS_EMBED" in
    yes|shared)
      HYSS_EMBED_TYPE=shared
      INSTALL_IT="\$(mkinstalldirs) \$(INSTALL_ROOT)\$(prefix)/lib; \$(INSTALL) -m 0755 $SAPI_SHARED \$(INSTALL_ROOT)\$(prefix)/lib"
      ;;
    static)
      HYSS_EMBED_TYPE=static
      INSTALL_IT="\$(mkinstalldirs) \$(INSTALL_ROOT)\$(prefix)/lib; \$(INSTALL) -m 0644 $SAPI_STATIC \$(INSTALL_ROOT)\$(prefix)/lib"
      ;;
    *)
      HYSS_EMBED_TYPE=no
      ;;
  esac
  if test "$HYSS_EMBED_TYPE" != "no"; then
    HYSS_SELECT_SAPI(embed, $HYSS_EMBED_TYPE, hyss_embed.c, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1)
    HYSS_INSTALL_HEADERS([server/embed/hyss_embed.h])
  fi
  AC_MSG_RESULT([$HYSS_EMBED_TYPE])
else
  AC_MSG_RESULT(no)
fi
