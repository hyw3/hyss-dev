cli: $(SAPI_CLI_PATH)

$(SAPI_CLI_PATH): $(HYSS_GLOBAL_OBJS) $(HYSS_BINARY_OBJS) $(HYSS_CLI_OBJS)
	$(BUILD_CLI)

install-cli: $(SAPI_CLI_PATH)
	@echo "Installing HYSS CLI binary:        $(INSTALL_ROOT)$(bindir)/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(bindir)
	@$(INSTALL) -m 0755 $(SAPI_CLI_PATH) $(INSTALL_ROOT)$(bindir)/$(program_prefix)hyss$(program_suffix)$(EXEEXT)
	@echo "Installing HYSS CLI man page:      $(INSTALL_ROOT)$(mandir)/man1/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(mandir)/man1
	@$(INSTALL_DATA) server/cli/hyss.1 $(INSTALL_ROOT)$(mandir)/man1/$(program_prefix)hyss$(program_suffix).1
