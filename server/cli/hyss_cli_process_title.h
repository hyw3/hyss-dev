/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_PS_TITLE_HEADER
#define HYSS_PS_TITLE_HEADER

GEAR_BEGIN_ARG_INFO(arginfo_cli_set_process_title, 0)
    GEAR_ARG_INFO(0, title)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_cli_get_process_title, 0)
GEAR_END_ARG_INFO()

HYSS_FUNCTION(cli_set_process_title);
HYSS_FUNCTION(cli_get_process_title);

#endif

