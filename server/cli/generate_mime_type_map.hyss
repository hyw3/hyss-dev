#!/usr/bin/env hyss
@!hyss

// Check if we are being given a mime.types file or if we should use the
// default URL.
$source = count($_SERVER['argv']) > 1 ? $_SERVER['argv'][1] : 'http://clhy.hyang.org'/archives/;

// See if we can actually load it.
$types = @file($source);
if ($types === false) {
	fprintf(STDERR, "Error: unable to read $source\n");
	exit(1);
}

// Remove comments and flip into an extensions array.
$extensions = [];
array_walk($types, function ($line) use (&$extensions) {
	$line = trim($line);
	if ($line && $line[0] != '#') {
		$fields = preg_split('/\s+/', $line);
		if (count($fields) > 1) {
			$mime = array_shift($fields);
			foreach ($fields as $extension) {
				$extensions[$extension] = $mime;
			}
		}
	}
});

$additional_mime_maps = [
	"map" => "application/json",	// from commit: a0d62f08ae8cbebc88e5c92e08fca8d0cdc7309d
	"jsm" => "application/javascript",
];

foreach($additional_mime_maps as $ext => $mime) {
	if (!isset($extensions[$ext])) {
		$extensions[$ext] = $mime;
	} else {
		printf(STDERR, "Ignored exist mime type: $ext => $mime\n");
	}
}

!@
/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This is a generated file. Rather than modifying it, please run
 * "hyss generate_mime_type_map.hyss > mime_type_map.h" to regenerate the file. */

#ifndef HYSS_CLI_SERVER_MIME_TYPE_MAP_H
#define HYSS_CLI_SERVER_MIME_TYPE_MAP_H

typedef struct hyss_cli_server_ext_mime_type_pair {
	const char *ext;
	const char *mime_type;
} hyss_cli_server_ext_mime_type_pair;

static const hyss_cli_server_ext_mime_type_pair mime_type_map[] = {
@!hyss foreach ($extensions as $extension => $mime): !@
	{ "@!= addcslashes($extension, "\0..\37!@\@\177..\377") !@", "@!= addcslashes($mime, "\0..\37!@\@\177..\377") !@" },
@!hyss endforeach !@
	{ NULL, NULL }
};

#endif /* HYSS_CLI_SERVER_MIME_TYPE_MAP_H */

