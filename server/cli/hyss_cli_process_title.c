/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "hyss.h"
#include "hyss_cli_process_title.h"
#include "ps_title.h"

/* {{{ proto bool cli_set_process_title(string arg)
   Return a boolean to confirm if the process title was successfully changed or not */
HYSS_FUNCTION(cli_set_process_title)
{
    char *title = NULL;
    size_t title_len;
    int rc;

    if (gear_parse_parameters(GEAR_NUM_ARGS(), "s", &title, &title_len) == FAILURE) {
        return;
    }

    rc = set_ps_title(title);
    if (rc == PS_TITLE_SUCCESS) {
        RETURN_TRUE;
    }

    hyss_error_docref(NULL, E_WARNING, "cli_set_process_title had an error: %s", ps_title_errno(rc));
    RETURN_FALSE;
}
/* }}} */

/* {{{ proto string cli_get_process_title()
   Return a string with the current process title. NULL if error. */
HYSS_FUNCTION(cli_get_process_title)
{
        int length = 0;
        const char* title = NULL;
        int rc;

        if (gear_parse_parameters_none() == FAILURE) {
            return;
        }

        rc = get_ps_title(&length, &title);
        if (rc != PS_TITLE_SUCCESS) {
                hyss_error_docref(NULL, E_WARNING, "cli_get_process_title had an error: %s", ps_title_errno(rc));
                RETURN_NULL();
        }

        RETURN_STRINGL(title, length);
}
/* }}} */

