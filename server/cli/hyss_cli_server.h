/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_CLI_SERVER_H
#define HYSS_CLI_SERVER_H

#include "SAPI.h"

extern const gear_function_entry server_additional_functions[];
extern sapi_capi_struct cli_server_sapi_capi;
extern int do_cli_server(int argc, char **argv);

GEAR_BEGIN_CAPI_GLOBALS(cli_server)
	short color;
GEAR_END_CAPI_GLOBALS(cli_server)

#ifdef ZTS
#define CLI_SERVER_G(v) GEAR_PBCG(cli_server_globals_id, gear_cli_server_globals *, v)
GEAR_PBCLS_CACHE_EXTERN()
#else
#define CLI_SERVER_G(v) (cli_server_globals.v)
#endif

#endif /* HYSS_CLI_SERVER_H */

