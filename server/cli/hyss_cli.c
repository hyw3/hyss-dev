/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_globals.h"
#include "hyss_variables.h"
#include "gear_hash.h"
#include "gear_capis.h"
#include "gear_interfaces.h"

#include "extslib/reflection/hyss_reflection.h"

#include "SAPI.h"

#include <stdio.h>
#include "hyss.h"
#ifdef HYSS_WIN32
#include "win32/time.h"
#include "win32/signal.h"
#include "win32/console.h"
#include <process.h>
#include <shellapi.h>
#endif
#if HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#if HAVE_SIGNAL_H
#include <signal.h>
#endif
#if HAVE_SETLOCALE
#include <locale.h>
#endif
#include "gear.h"
#include "gear_extensions.h"
#include "hyss_ics.h"
#include "hyss_globals.h"
#include "hyss_main.h"
#include "fopen_wrappers.h"
#include "extslib/standard/hyss_standard.h"
#include "cli.h"
#ifdef HYSS_WIN32
#include <io.h>
#include <fcntl.h>
#include "win32/hyss_registry.h"
#endif

#if HAVE_SIGNAL_H
#include <signal.h>
#endif

#ifdef __riscos__
#include <unixlib/local.h>
#endif

#include "gear_compile.h"
#include "gear_execute.h"
#include "gear_highlight.h"
#include "gear_exceptions.h"

#include "hyss_getopt.h"

#ifndef HYSS_CLI_WIN32_NO_CONSOLE
#include "hyss_cli_server.h"
#endif

#include "ps_title.h"
#include "hyss_cli_process_title.h"

#ifndef HYSS_WIN32
# define hyss_select(m, r, w, e, t)	select(m, r, w, e, t)
#else
# include "win32/select.h"
#endif

#if defined(HYSS_WIN32) && defined(HAVE_OPENSSL)
# include "openssl/applink.c"
#endif

HYSSAPI extern char *hyss_ics_opened_path;
HYSSAPI extern char *hyss_ics_scanned_path;
HYSSAPI extern char *hyss_ics_scanned_files;

#if defined(HYSS_WIN32)
#if defined(ZTS)
GEAR_PBCLS_CACHE_DEFINE()
#endif
static DWORD orig_cp = 0;
#endif

#ifndef O_BINARY
#define O_BINARY 0
#endif

#define HYSS_MODE_STANDARD      1
#define HYSS_MODE_HIGHLIGHT     2
#define HYSS_MODE_LINT          4
#define HYSS_MODE_STRIP         5
#define HYSS_MODE_CLI_DIRECT    6
#define HYSS_MODE_PROCESS_STDIN 7
#define HYSS_MODE_REFLECTION_FUNCTION    8
#define HYSS_MODE_REFLECTION_CLASS       9
#define HYSS_MODE_REFLECTION_EXTENSION   10
#define HYSS_MODE_REFLECTION_EXT_INFO    11
#define HYSS_MODE_REFLECTION_GEAR_EXTENSION 12
#define HYSS_MODE_SHOW_ICS_CONFIG        13

cli_shell_callbacks_t cli_shell_callbacks = { NULL, NULL, NULL };
HYSS_CLI_API cli_shell_callbacks_t *hyss_cli_get_shell_callbacks()
{
	return &cli_shell_callbacks;
}

const char HARDCODED_ICS[] =
	"html_errors=0\n"
	"register_argc_argv=1\n"
	"implicit_flush=1\n"
	"output_buffering=0\n"
	"max_execution_time=0\n"
	"max_input_time=-1\n\0";


const opt_struct OPTIONS[] = {
	{'a', 0, "interactive"},
	{'B', 1, "process-begin"},
	{'C', 0, "no-chdir"}, /* for compatibility with CGI (do not chdir to script directory) */
	{'c', 1, "hyss-ics"},
	{'d', 1, "define"},
	{'E', 1, "process-end"},
	{'e', 0, "profile-info"},
	{'F', 1, "process-file"},
	{'f', 1, "file"},
	{'h', 0, "help"},
	{'i', 0, "info"},
	{'l', 0, "syntax-check"},
	{'m', 0, "cAPIs"},
	{'n', 0, "no-hyss-ics"},
	{'q', 0, "no-header"}, /* for compatibility with CGI (do not generate HTTP headers) */
	{'R', 1, "process-code"},
	{'H', 0, "hide-args"},
	{'r', 1, "run"},
	{'s', 0, "syntax-highlight"},
	{'s', 0, "syntax-highlighting"},
	{'S', 1, "server"},
	{'t', 1, "docroot"},
	{'w', 0, "strip"},
	{'?', 0, "usage"},/* help alias (both '?' and 'usage') */
	{'v', 0, "version"},
	{'z', 1, "gear-extension"},
	{10,  1, "rf"},
	{10,  1, "rfunction"},
	{11,  1, "rc"},
	{11,  1, "rclass"},
	{12,  1, "re"},
	{12,  1, "rextension"},
	{13,  1, "rz"},
	{13,  1, "rgearextension"},
	{14,  1, "ri"},
	{14,  1, "rextinfo"},
	{15,  0, "ics"},
	{'-', 0, NULL} /* end of args */
};

static int print_capi_info(zval *element) /* {{{ */
{
	gear_capi_entry *cAPI = (gear_capi_entry*)Z_PTR_P(element);
	hyss_printf("%s\n", cAPI->name);
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

static int capi_name_cmp(const void *a, const void *b) /* {{{ */
{
	Bucket *f = (Bucket *) a;
	Bucket *s = (Bucket *) b;

	return strcasecmp(((gear_capi_entry *)Z_PTR(f->val))->name,
				  ((gear_capi_entry *)Z_PTR(s->val))->name);
}
/* }}} */

static void print_capis(void) /* {{{ */
{
	HashTable sorted_registry;

	gear_hash_init(&sorted_registry, 50, NULL, NULL, 0);
	gear_hash_copy(&sorted_registry, &capi_registry, NULL);
	gear_hash_sort(&sorted_registry, capi_name_cmp, 0);
	gear_hash_apply(&sorted_registry, print_capi_info);
	gear_hash_destroy(&sorted_registry);
}
/* }}} */

static int print_extension_info(gear_extension *ext, void *arg) /* {{{ */
{
	hyss_printf("%s\n", ext->name);
	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

static int extension_name_cmp(const gear_llist_element **f, const gear_llist_element **s) /* {{{ */
{
	gear_extension *fe = (gear_extension*)(*f)->data;
	gear_extension *se = (gear_extension*)(*s)->data;
	return strcmp(fe->name, se->name);
}
/* }}} */

static void print_extensions(void) /* {{{ */
{
	gear_llist sorted_exts;

	gear_llist_copy(&sorted_exts, &gear_extensions);
	sorted_exts.dtor = NULL;
	gear_llist_sort(&sorted_exts, extension_name_cmp);
	gear_llist_apply(&sorted_exts, (llist_apply_func_t) print_extension_info);
	gear_llist_destroy(&sorted_exts);
}
/* }}} */

#ifndef STDOUT_FILENO
#define STDOUT_FILENO 1
#endif
#ifndef STDERR_FILENO
#define STDERR_FILENO 2
#endif

static inline int sapi_cli_select(hyss_socket_t fd)
{
	fd_set wfd, dfd;
	struct timeval tv;
	int ret;

	FD_ZERO(&wfd);
	FD_ZERO(&dfd);

	HYSS_SAFE_FD_SET(fd, &wfd);

	tv.tv_sec = (long)FG(default_socket_timeout);
	tv.tv_usec = 0;

	ret = hyss_select(fd+1, &dfd, &wfd, &dfd, &tv);

	return ret != -1;
}

HYSS_CLI_API ssize_t sapi_cli_single_write(const char *str, size_t str_length) /* {{{ */
{
	ssize_t ret;

	if (cli_shell_callbacks.cli_shell_write) {
		cli_shell_callbacks.cli_shell_write(str, str_length);
	}

#ifdef HYSS_WRITE_STDOUT
	do {
		ret = write(STDOUT_FILENO, str, str_length);
	} while (ret <= 0 && errno == EAGAIN && sapi_cli_select(STDOUT_FILENO));
#else
	ret = fwrite(str, 1, MIN(str_length, 16384), stdout);
#endif
	return ret;
}
/* }}} */

static size_t sapi_cli_ub_write(const char *str, size_t str_length) /* {{{ */
{
	const char *ptr = str;
	size_t remaining = str_length;
	ssize_t ret;

	if (!str_length) {
		return 0;
	}

	if (cli_shell_callbacks.cli_shell_ub_write) {
		size_t ub_wrote;
		ub_wrote = cli_shell_callbacks.cli_shell_ub_write(str, str_length);
		if (ub_wrote != (size_t) -1) {
			return ub_wrote;
		}
	}

	while (remaining > 0)
	{
		ret = sapi_cli_single_write(ptr, remaining);
		if (ret < 0) {
#ifndef HYSS_CLI_WIN32_NO_CONSOLE
			EG(exit_status) = 255;
			hyss_handle_aborted_connection();
#endif
			break;
		}
		ptr += ret;
		remaining -= ret;
	}

	return (ptr - str);
}
/* }}} */

static void sapi_cli_flush(void *server_context) /* {{{ */
{
	/* Ignore EBADF here, it's caused by the fact that STDIN/STDOUT/STDERR streams
	 * are/could be closed before fflush() is called.
	 */
	if (fflush(stdout)==EOF && errno!=EBADF) {
#ifndef HYSS_CLI_WIN32_NO_CONSOLE
		hyss_handle_aborted_connection();
#endif
	}
}
/* }}} */

static char *hyss_self = "";
static char *script_filename = "";

static void sapi_cli_register_variables(zval *track_vars_array) /* {{{ */
{
	size_t len;
	char   *docroot = "";

	/* In CGI mode, we consider the environment to be a part of the server
	 * variables
	 */
	hyss_import_environment_variables(track_vars_array);

	/* Build the special-case HYSS_SELF variable for the CLI version */
	len = strlen(hyss_self);
	if (sapi_capi.input_filter(PARSE_SERVER, "HYSS_SELF", &hyss_self, len, &len)) {
		hyss_register_variable("HYSS_SELF", hyss_self, track_vars_array);
	}
	if (sapi_capi.input_filter(PARSE_SERVER, "SCRIPT_NAME", &hyss_self, len, &len)) {
		hyss_register_variable("SCRIPT_NAME", hyss_self, track_vars_array);
	}
	/* filenames are empty for stdin */
	len = strlen(script_filename);
	if (sapi_capi.input_filter(PARSE_SERVER, "SCRIPT_FILENAME", &script_filename, len, &len)) {
		hyss_register_variable("SCRIPT_FILENAME", script_filename, track_vars_array);
	}
	if (sapi_capi.input_filter(PARSE_SERVER, "PATH_TRANSLATED", &script_filename, len, &len)) {
		hyss_register_variable("PATH_TRANSLATED", script_filename, track_vars_array);
	}
	/* just make it available */
	len = 0U;
	if (sapi_capi.input_filter(PARSE_SERVER, "DOCUMENT_ROOT", &docroot, len, &len)) {
		hyss_register_variable("DOCUMENT_ROOT", docroot, track_vars_array);
	}
}
/* }}} */

static void sapi_cli_log_message(char *message, int syslog_type_int) /* {{{ */
{
	fprintf(stderr, "%s\n", message);
#ifdef HYSS_WIN32
	fflush(stderr);
#endif
}
/* }}} */

static int sapi_cli_deactivate(void) /* {{{ */
{
	fflush(stdout);
	if(SG(request_info).argv0) {
		free(SG(request_info).argv0);
		SG(request_info).argv0 = NULL;
	}
	return SUCCESS;
}
/* }}} */

static char* sapi_cli_read_cookies(void) /* {{{ */
{
	return NULL;
}
/* }}} */

static int sapi_cli_header_handler(sapi_header_struct *h, sapi_header_op_enum op, sapi_headers_struct *s) /* {{{ */
{
	return 0;
}
/* }}} */

static int sapi_cli_send_headers(sapi_headers_struct *sapi_headers) /* {{{ */
{
	/* We do nothing here, this function is needed to prevent that the fallback
	 * header handling is called. */
	return SAPI_HEADER_SENT_SUCCESSFULLY;
}
/* }}} */

static void sapi_cli_send_header(sapi_header_struct *sapi_header, void *server_context) /* {{{ */
{
}
/* }}} */

static int hyss_cli_startup(sapi_capi_struct *sapi_capi) /* {{{ */
{
	if (hyss_capi_startup(sapi_capi, NULL, 0)==FAILURE) {
		return FAILURE;
	}
	return SUCCESS;
}
/* }}} */

/* {{{ sapi_cli_ics_defaults */

/* overwriteable ics defaults must be set in sapi_cli_ics_defaults() */
#define ICS_DEFAULT(name,value)\
	ZVAL_NEW_STR(&tmp, gear_string_init(value, sizeof(value)-1, 1));\
	gear_hash_str_update(configuration_hash, name, sizeof(name)-1, &tmp);\

static void sapi_cli_ics_defaults(HashTable *configuration_hash)
{
	zval tmp;
	ICS_DEFAULT("report_gear_debug", "0");
	ICS_DEFAULT("display_errors", "1");
}
/* }}} */

/* {{{ sapi_capi_struct cli_sapi_capi
 */
static sapi_capi_struct cli_sapi_capi = {
	"cli",							/* name */
	"Command Line Interface",    	/* pretty name */

	hyss_cli_startup,				/* startup */
	hyss_capi_shutdown_wrapper,	/* shutdown */

	NULL,							/* activate */
	sapi_cli_deactivate,			/* deactivate */

	sapi_cli_ub_write,		    	/* unbuffered write */
	sapi_cli_flush,				    /* flush */
	NULL,							/* get uid */
	NULL,							/* getenv */

	hyss_error,						/* error handler */

	sapi_cli_header_handler,		/* header handler */
	sapi_cli_send_headers,			/* send headers handler */
	sapi_cli_send_header,			/* send header handler */

	NULL,				            /* read POST data */
	sapi_cli_read_cookies,          /* read Cookies */

	sapi_cli_register_variables,	/* register server variables */
	sapi_cli_log_message,			/* Log message */
	NULL,							/* Get request time */
	NULL,							/* Child terminate */

	STANDARD_SAPI_CAPI_PROPERTIES
};
/* }}} */

/* {{{ arginfo extslib/standard/dl.c */
GEAR_BEGIN_ARG_INFO(arginfo_dl, 0)
	GEAR_ARG_INFO(0, extension_filename)
GEAR_END_ARG_INFO()
/* }}} */

static const gear_function_entry additional_functions[] = {
	GEAR_FE(dl, arginfo_dl)
	HYSS_FE(cli_set_process_title,        arginfo_cli_set_process_title)
	HYSS_FE(cli_get_process_title,        arginfo_cli_get_process_title)
	HYSS_FE_END
};

/* {{{ hyss_cli_usage
 */
static void hyss_cli_usage(char *argv0)
{
	char *prog;

	prog = strrchr(argv0, '/');
	if (prog) {
		prog++;
	} else {
		prog = "hyss";
	}

	printf( "Usage: %s [options] [-f] <file> [--] [args...]\n"
				"   %s [options] -r <code> [--] [args...]\n"
				"   %s [options] [-B <begin_code>] -R <code> [-E <end_code>] [--] [args...]\n"
				"   %s [options] [-B <begin_code>] -F <file> [-E <end_code>] [--] [args...]\n"
				"   %s [options] -S <addr>:<port> [-t docroot] [router]\n"
				"   %s [options] -- [args...]\n"
				"   %s [options] -a\n"
				"\n"
#if (HAVE_LIBREADLINE || HAVE_LIBEDIT) && !defined(COMPILE_DL_READLINE)
				"  -a               Run as interactive shell\n"
#else
				"  -a               Run interactively\n"
#endif
				"  -c <path>|<file> Look for hyss.ics file in this directory\n"
				"  -n               No configuration (ics) files will be used\n"
				"  -d foo[=bar]     Define ICS entry foo with value 'bar'\n"
				"  -e               Generate extended information for debugger/profiler\n"
				"  -f <file>        Parse and execute <file>.\n"
				"  -h               This help\n"
				"  -i               HYSS information\n"
				"  -l               Syntax check only (lint)\n"
				"  -m               Show compiled in cAPIs\n"
				"  -r <code>        Run HYSS <code> without using script tags @!..!@\n"
				"  -B <begin_code>  Run HYSS <begin_code> before processing input lines\n"
				"  -R <code>        Run HYSS <code> for every input line\n"
				"  -F <file>        Parse and execute <file> for every input line\n"
				"  -E <end_code>    Run HYSS <end_code> after processing all input lines\n"
				"  -H               Hide any passed arguments from external tools.\n"
				"  -S <addr>:<port> Run with built-in web server.\n"
				"  -t <docroot>     Specify document root <docroot> for built-in web server.\n"
				"  -s               Output HTML syntax highlighted source.\n"
				"  -v               Version number\n"
				"  -w               Output source with stripped comments and whitespace.\n"
				"  -z <file>        Load Gear extension <file>.\n"
				"\n"
				"  args...          Arguments passed to script. Use -- args when first argument\n"
				"                   starts with - or script is read from stdin\n"
				"\n"
				"  --ics            Show configuration file names\n"
				"\n"
				"  --rf <name>      Show information about function <name>.\n"
				"  --rc <name>      Show information about class <name>.\n"
				"  --re <name>      Show information about extension <name>.\n"
				"  --rz <name>      Show information about Gear extension <name>.\n"
				"  --ri <name>      Show configuration for extension <name>.\n"
				"\n"
				, prog, prog, prog, prog, prog, prog, prog);
}
/* }}} */

static hyss_stream *s_in_process = NULL;

static void cli_register_file_handles(void) /* {{{ */
{
	hyss_stream *s_in, *s_out, *s_err;
	hyss_stream_context *sc_in=NULL, *sc_out=NULL, *sc_err=NULL;
	gear_constant ic, oc, ec;

	s_in  = hyss_stream_open_wrapper_ex("hyss://stdin",  "rb", 0, NULL, sc_in);
	s_out = hyss_stream_open_wrapper_ex("hyss://stdout", "wb", 0, NULL, sc_out);
	s_err = hyss_stream_open_wrapper_ex("hyss://stderr", "wb", 0, NULL, sc_err);

	if (s_in==NULL || s_out==NULL || s_err==NULL) {
		if (s_in) hyss_stream_close(s_in);
		if (s_out) hyss_stream_close(s_out);
		if (s_err) hyss_stream_close(s_err);
		return;
	}

#if HYSS_DEBUG
	/* do not close stdout and stderr */
	s_out->flags |= HYSS_STREAM_FLAG_NO_CLOSE;
	s_err->flags |= HYSS_STREAM_FLAG_NO_CLOSE;
#endif

	s_in_process = s_in;

	hyss_stream_to_zval(s_in,  &ic.value);
	hyss_stream_to_zval(s_out, &oc.value);
	hyss_stream_to_zval(s_err, &ec.value);

	GEAR_CONSTANT_SET_FLAGS(&ic, CONST_CS, 0);
	ic.name = gear_string_init_interned("STDIN", sizeof("STDIN")-1, 0);
	gear_register_constant(&ic);

	GEAR_CONSTANT_SET_FLAGS(&oc, CONST_CS, 0);
	oc.name = gear_string_init_interned("STDOUT", sizeof("STDOUT")-1, 0);
	gear_register_constant(&oc);

	GEAR_CONSTANT_SET_FLAGS(&ec, CONST_CS, 0);
	ec.name = gear_string_init_interned("STDERR", sizeof("STDERR")-1, 0);
	gear_register_constant(&ec);
}
/* }}} */

static const char *param_mode_conflict = "Either execute direct code, process stdin or use a file.\n";

/* {{{ cli_seek_file_begin
 */
static int cli_seek_file_begin(gear_file_handle *file_handle, char *script_file, int *lineno)
{
	int c;

	*lineno = 1;

	file_handle->type = GEAR_HANDLE_FP;
	file_handle->opened_path = NULL;
	file_handle->free_filename = 0;
	if (!(file_handle->handle.fp = VCWD_FOPEN(script_file, "rb"))) {
		hyss_printf("Could not open input file: %s\n", script_file);
		return FAILURE;
	}
	file_handle->filename = script_file;

	/* #!hyss support */
	c = fgetc(file_handle->handle.fp);
	if (c == '#' && (c = fgetc(file_handle->handle.fp)) == '!') {
		while (c != '\n' && c != '\r' && c != EOF) {
			c = fgetc(file_handle->handle.fp);	/* skip to end of line */
		}
		/* handle situations where line is terminated by \r\n */
		if (c == '\r') {
			if (fgetc(file_handle->handle.fp) != '\n') {
				gear_long pos = gear_ftell(file_handle->handle.fp);
				gear_fseek(file_handle->handle.fp, pos - 1, SEEK_SET);
			}
		}
		*lineno = 2;
	} else {
		rewind(file_handle->handle.fp);
	}

	return SUCCESS;
}
/* }}} */

/*{{{ hyss_cli_win32_ctrl_handler */
#if defined(HYSS_WIN32)
BOOL WINAPI hyss_cli_win32_ctrl_handler(DWORD sig)
{
	(void)hyss_win32_cp_cli_do_restore(orig_cp);

	return FALSE;
}
#endif
/*}}}*/

static int do_cli(int argc, char **argv) /* {{{ */
{
	int c;
	gear_file_handle file_handle;
	int behavior = HYSS_MODE_STANDARD;
	char *reflection_what = NULL;
	volatile int request_started = 0;
	volatile int exit_status = 0;
	char *hyss_optarg = NULL, *orig_optarg = NULL;
	int hyss_optind = 1, orig_optind = 1;
	char *exec_direct=NULL, *exec_run=NULL, *exec_begin=NULL, *exec_end=NULL;
	char *arg_free=NULL, **arg_excp=&arg_free;
	char *script_file=NULL, *translated_path = NULL;
	int interactive=0;
	int lineno = 0;
	const char *param_error=NULL;
	int hide_argv = 0;

	gear_try {

		CG(in_compilation) = 0; /* not initialized but needed for several options */

		while ((c = hyss_getopt(argc, argv, OPTIONS, &hyss_optarg, &hyss_optind, 0, 2)) != -1) {
			switch (c) {

			case 'i': /* hyss info & quit */
				if (hyss_request_startup()==FAILURE) {
					goto err;
				}
				request_started = 1;
				hyss_print_info(0xFFFFFFFF);
				hyss_output_end_all();
				exit_status = (c == '?' && argc > 1 && !strchr(argv[1],  c));
				goto out;

			case 'v': /* show hyss version & quit */
				hyss_printf("HYSS %s (%s) (built: %s %s) ( %s)\nCopyright (c) 2019-2020 The Hyang Language Foundation\n%s",
					HYSS_VERSION, cli_sapi_capi.name, __DATE__, __TIME__,
#if ZTS
					"ZTS "
#else
					"NTS "
#endif
#ifdef COMPILER
					COMPILER
					" "
#endif
#ifdef ARCHITECTURE
					ARCHITECTURE
					" "
#endif
#if GEAR_DEBUG
					"DEBUG "
#endif
#ifdef HAVE_GCOV
					"GCOV "
#endif
					,
					get_gear_version()
				);
				sapi_deactivate();
				goto out;

			case 'm': /* list compiled in cAPIs */
				if (hyss_request_startup()==FAILURE) {
					goto err;
				}
				request_started = 1;
				hyss_printf("[HYSS cAPIs]\n");
				print_capis();
				hyss_printf("\n[Gear cAPIs]\n");
				print_extensions();
				hyss_printf("\n");
				hyss_output_end_all();
				exit_status=0;
				goto out;

			default:
				break;
			}
		}

		/* Set some CLI defaults */
		SG(options) |= SAPI_OPTION_NO_CHDIR;

		hyss_optind = orig_optind;
		hyss_optarg = orig_optarg;
		while ((c = hyss_getopt(argc, argv, OPTIONS, &hyss_optarg, &hyss_optind, 0, 2)) != -1) {
			switch (c) {

			case 'a':	/* interactive mode */
				if (!interactive) {
					if (behavior != HYSS_MODE_STANDARD) {
						param_error = param_mode_conflict;
						break;
					}

					interactive=1;
				}
				break;

			case 'C': /* don't chdir to the script directory */
				/* This is default so NOP */
				break;

			case 'F':
				if (behavior == HYSS_MODE_PROCESS_STDIN) {
					if (exec_run || script_file) {
						param_error = "You can use -R or -F only once.\n";
						break;
					}
				} else if (behavior != HYSS_MODE_STANDARD) {
					param_error = param_mode_conflict;
					break;
				}
				behavior=HYSS_MODE_PROCESS_STDIN;
				script_file = hyss_optarg;
				break;

			case 'f': /* parse file */
				if (behavior == HYSS_MODE_CLI_DIRECT || behavior == HYSS_MODE_PROCESS_STDIN) {
					param_error = param_mode_conflict;
					break;
				} else if (script_file) {
					param_error = "You can use -f only once.\n";
					break;
				}
				script_file = hyss_optarg;
				break;

			case 'l': /* syntax check mode */
				if (behavior != HYSS_MODE_STANDARD) {
					break;
				}
				behavior=HYSS_MODE_LINT;
				break;

			case 'q': /* do not generate HTTP headers */
				/* This is default so NOP */
				break;

			case 'r': /* run code from command line */
				if (behavior == HYSS_MODE_CLI_DIRECT) {
					if (exec_direct || script_file) {
						param_error = "You can use -r only once.\n";
						break;
					}
				} else if (behavior != HYSS_MODE_STANDARD || interactive) {
					param_error = param_mode_conflict;
					break;
				}
				behavior=HYSS_MODE_CLI_DIRECT;
				exec_direct=hyss_optarg;
				break;

			case 'R':
				if (behavior == HYSS_MODE_PROCESS_STDIN) {
					if (exec_run || script_file) {
						param_error = "You can use -R or -F only once.\n";
						break;
					}
				} else if (behavior != HYSS_MODE_STANDARD) {
					param_error = param_mode_conflict;
					break;
				}
				behavior=HYSS_MODE_PROCESS_STDIN;
				exec_run=hyss_optarg;
				break;

			case 'B':
				if (behavior == HYSS_MODE_PROCESS_STDIN) {
					if (exec_begin) {
						param_error = "You can use -B only once.\n";
						break;
					}
				} else if (behavior != HYSS_MODE_STANDARD || interactive) {
					param_error = param_mode_conflict;
					break;
				}
				behavior=HYSS_MODE_PROCESS_STDIN;
				exec_begin=hyss_optarg;
				break;

			case 'E':
				if (behavior == HYSS_MODE_PROCESS_STDIN) {
					if (exec_end) {
						param_error = "You can use -E only once.\n";
						break;
					}
				} else if (behavior != HYSS_MODE_STANDARD || interactive) {
					param_error = param_mode_conflict;
					break;
				}
				behavior=HYSS_MODE_PROCESS_STDIN;
				exec_end=hyss_optarg;
				break;

			case 's': /* generate highlighted HTML from source */
				if (behavior == HYSS_MODE_CLI_DIRECT || behavior == HYSS_MODE_PROCESS_STDIN) {
					param_error = "Source highlighting only works for files.\n";
					break;
				}
				behavior=HYSS_MODE_HIGHLIGHT;
				break;

			case 'w':
				if (behavior == HYSS_MODE_CLI_DIRECT || behavior == HYSS_MODE_PROCESS_STDIN) {
					param_error = "Source stripping only works for files.\n";
					break;
				}
				behavior=HYSS_MODE_STRIP;
				break;

			case 'z': /* load extension file */
				gear_load_extension(hyss_optarg);
				break;
			case 'H':
				hide_argv = 1;
				break;
			case 10:
				behavior=HYSS_MODE_REFLECTION_FUNCTION;
				reflection_what = hyss_optarg;
				break;
			case 11:
				behavior=HYSS_MODE_REFLECTION_CLASS;
				reflection_what = hyss_optarg;
				break;
			case 12:
				behavior=HYSS_MODE_REFLECTION_EXTENSION;
				reflection_what = hyss_optarg;
				break;
			case 13:
				behavior=HYSS_MODE_REFLECTION_GEAR_EXTENSION;
				reflection_what = hyss_optarg;
				break;
			case 14:
				behavior=HYSS_MODE_REFLECTION_EXT_INFO;
				reflection_what = hyss_optarg;
				break;
			case 15:
				behavior = HYSS_MODE_SHOW_ICS_CONFIG;
				break;
			default:
				break;
			}
		}

		if (param_error) {
			PUTS(param_error);
			exit_status=1;
			goto err;
		}

#if defined(HYSS_WIN32) && !defined(HYSS_CLI_WIN32_NO_CONSOLE) && (HAVE_LIBREADLINE || HAVE_LIBEDIT) && !defined(COMPILE_DL_READLINE)
		if (!interactive) {
		/* The -a option was not passed. If there is no file, it could
		 	still make sense to run interactively. The presence of a file
			is essential to mitigate buggy console info. */
			interactive = hyss_win32_console_is_own() &&
				!(script_file ||
					argc > hyss_optind && behavior!=HYSS_MODE_CLI_DIRECT &&
					behavior!=HYSS_MODE_PROCESS_STDIN &&
					strcmp(argv[hyss_optind-1],"--")
				);
		}
#endif

		if (interactive) {
#if (HAVE_LIBREADLINE || HAVE_LIBEDIT) && !defined(COMPILE_DL_READLINE)
			printf("Interactive shell\n\n");
#else
			printf("Interactive mode enabled\n\n");
#endif
			fflush(stdout);
		}

		/* only set script_file if not set already and not in direct mode and not at end of parameter list */
		if (argc > hyss_optind
		  && !script_file
		  && behavior!=HYSS_MODE_CLI_DIRECT
		  && behavior!=HYSS_MODE_PROCESS_STDIN
		  && strcmp(argv[hyss_optind-1],"--"))
		{
			script_file=argv[hyss_optind];
			hyss_optind++;
		}
		if (script_file) {
			if (cli_seek_file_begin(&file_handle, script_file, &lineno) != SUCCESS) {
				goto err;
			} else {
				char real_path[MAXPATHLEN];
				if (VCWD_REALPATH(script_file, real_path)) {
					translated_path = strdup(real_path);
				}
				script_filename = script_file;
			}
		} else {
			/* We could handle HYSS_MODE_PROCESS_STDIN in a different manner  */
			/* here but this would make things only more complicated. And it */
			/* is consitent with the way -R works where the stdin file handle*/
			/* is also accessible. */
			file_handle.filename = "Standard input code";
			file_handle.handle.fp = stdin;
		}
		file_handle.type = GEAR_HANDLE_FP;
		file_handle.opened_path = NULL;
		file_handle.free_filename = 0;
		hyss_self = (char*)file_handle.filename;

		/* before registering argv to cAPI exchange the *new* argv[0] */
		/* we can achieve this without allocating more memory */
		SG(request_info).argc=argc-hyss_optind+1;
		arg_excp = argv+hyss_optind-1;
		arg_free = argv[hyss_optind-1];
		SG(request_info).path_translated = translated_path? translated_path: (char*)file_handle.filename;
		argv[hyss_optind-1] = (char*)file_handle.filename;
		SG(request_info).argv=argv+hyss_optind-1;

		if (hyss_request_startup()==FAILURE) {
			*arg_excp = arg_free;
			fclose(file_handle.handle.fp);
			PUTS("Could not startup.\n");
			goto err;
		}
		request_started = 1;
		CG(start_lineno) = lineno;
		*arg_excp = arg_free; /* reconstuct argv */

		if (hide_argv) {
			int i;
			for (i = 1; i < argc; i++) {
				memset(argv[i], 0, strlen(argv[i]));
			}
		}

		gear_is_auto_global_str(GEAR_STRL("_SERVER"));

		PG(during_request_startup) = 0;
		switch (behavior) {
		case HYSS_MODE_STANDARD:
			if (strcmp(file_handle.filename, "Standard input code")) {
				cli_register_file_handles();
			}

			if (interactive && cli_shell_callbacks.cli_shell_run) {
				exit_status = cli_shell_callbacks.cli_shell_run();
			} else {
				hyss_execute_script(&file_handle);
				exit_status = EG(exit_status);
			}
			break;
		case HYSS_MODE_LINT:
			exit_status = hyss_lint_script(&file_handle);
			if (exit_status==SUCCESS) {
				gear_printf("No syntax errors detected in %s\n", file_handle.filename);
			} else {
				gear_printf("Errors parsing %s\n", file_handle.filename);
			}
			break;
		case HYSS_MODE_STRIP:
			if (open_file_for_scanning(&file_handle)==SUCCESS) {
				gear_strip();
			}
			goto out;
			break;
		case HYSS_MODE_HIGHLIGHT:
			{
				gear_syntax_highlighter_ics syntax_highlighter_ics;

				if (open_file_for_scanning(&file_handle)==SUCCESS) {
					hyss_get_highlight_struct(&syntax_highlighter_ics);
					gear_highlight(&syntax_highlighter_ics);
				}
				goto out;
			}
			break;
		case HYSS_MODE_CLI_DIRECT:
			cli_register_file_handles();
			if (gear_eval_string_ex(exec_direct, NULL, "Command line code", 1) == FAILURE) {
				exit_status=254;
			}
			break;

		case HYSS_MODE_PROCESS_STDIN:
			{
				char *input;
				size_t len, index = 0;
				zval argn, argi;

				cli_register_file_handles();

				if (exec_begin && gear_eval_string_ex(exec_begin, NULL, "Command line begin code", 1) == FAILURE) {
					exit_status=254;
				}
				while (exit_status == SUCCESS && (input=hyss_stream_gets(s_in_process, NULL, 0)) != NULL) {
					len = strlen(input);
					while (len > 0 && len-- && (input[len]=='\n' || input[len]=='\r')) {
						input[len] = '\0';
					}
					ZVAL_STRINGL(&argn, input, len + 1);
					gear_hash_str_update(&EG(symbol_table), "argn", sizeof("argn")-1, &argn);
					ZVAL_LONG(&argi, ++index);
					gear_hash_str_update(&EG(symbol_table), "argi", sizeof("argi")-1, &argi);
					if (exec_run) {
						if (gear_eval_string_ex(exec_run, NULL, "Command line run code", 1) == FAILURE) {
							exit_status=254;
						}
					} else {
						if (script_file) {
							if (cli_seek_file_begin(&file_handle, script_file, &lineno) != SUCCESS) {
								exit_status = 1;
							} else {
								CG(start_lineno) = lineno;
								hyss_execute_script(&file_handle);
								exit_status = EG(exit_status);
							}
						}
					}
					efree(input);
				}
				if (exec_end && gear_eval_string_ex(exec_end, NULL, "Command line end code", 1) == FAILURE) {
					exit_status=254;
				}

				break;
			}

			case HYSS_MODE_REFLECTION_FUNCTION:
			case HYSS_MODE_REFLECTION_CLASS:
			case HYSS_MODE_REFLECTION_EXTENSION:
			case HYSS_MODE_REFLECTION_GEAR_EXTENSION:
				{
					gear_class_entry *pce = NULL;
					zval arg, ref;
					gear_execute_data execute_data;

					switch (behavior) {
						default:
							break;
						case HYSS_MODE_REFLECTION_FUNCTION:
							if (strstr(reflection_what, "::")) {
								pce = reflection_method_ptr;
							} else {
								pce = reflection_function_ptr;
							}
							break;
						case HYSS_MODE_REFLECTION_CLASS:
							pce = reflection_class_ptr;
							break;
						case HYSS_MODE_REFLECTION_EXTENSION:
							pce = reflection_extension_ptr;
							break;
						case HYSS_MODE_REFLECTION_GEAR_EXTENSION:
							pce = reflection_gear_extension_ptr;
							break;
					}

					ZVAL_STRING(&arg, reflection_what);
					object_init_ex(&ref, pce);

					memset(&execute_data, 0, sizeof(gear_execute_data));
					EG(current_execute_data) = &execute_data;
					gear_call_method_with_1_params(&ref, pce, &pce->constructor, "__construct", NULL, &arg);

					if (EG(exception)) {
						zval tmp, *msg, rv;

						ZVAL_OBJ(&tmp, EG(exception));
						msg = gear_read_property(gear_ce_exception, &tmp, "message", sizeof("message")-1, 0, &rv);
						gear_printf("Exception: %s\n", Z_STRVAL_P(msg));
						zval_ptr_dtor(&tmp);
						EG(exception) = NULL;
					} else {
						gear_call_method_with_1_params(NULL, reflection_ptr, NULL, "export", NULL, &ref);
					}
					zval_ptr_dtor(&ref);
					zval_ptr_dtor(&arg);

					break;
				}
			case HYSS_MODE_REFLECTION_EXT_INFO:
				{
					size_t len = strlen(reflection_what);
					char *lcname = gear_str_tolower_dup(reflection_what, len);
					gear_capi_entry *cAPI;

					if ((cAPI = gear_hash_str_find_ptr(&capi_registry, lcname, len)) == NULL) {
						if (!strcmp(reflection_what, "main")) {
							display_ics_entries(NULL);
						} else {
							gear_printf("Extension '%s' not present.\n", reflection_what);
							exit_status = 1;
						}
					} else {
						hyss_info_print_capi(cAPI);
					}

					efree(lcname);
					break;
				}

			case HYSS_MODE_SHOW_ICS_CONFIG:
				{
					gear_printf("Configuration File (hyss.ics) Path: %s\n", HYSS_CONFIG_FILE_PATH);
					gear_printf("Loaded Configuration File:         %s\n", hyss_ics_opened_path ? hyss_ics_opened_path : "(none)");
					gear_printf("Scan for additional .ics files in: %s\n", hyss_ics_scanned_path  ? hyss_ics_scanned_path : "(none)");
					gear_printf("Additional .ics files parsed:      %s\n", hyss_ics_scanned_files ? hyss_ics_scanned_files : "(none)");
					break;
				}
		}
	} gear_end_try();

out:
	if (request_started) {
		hyss_request_shutdown((void *) 0);
	}
	if (translated_path) {
		free(translated_path);
	}
	if (exit_status == 0) {
		exit_status = EG(exit_status);
	}
	return exit_status;
err:
	sapi_deactivate();
	gear_ics_deactivate();
	exit_status = 1;
	goto out;
}
/* }}} */

/* {{{ main
 */
#ifdef HYSS_CLI_WIN32_NO_CONSOLE
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
#else
int main(int argc, char *argv[])
#endif
{
#if defined(HYSS_WIN32)
# ifdef HYSS_CLI_WIN32_NO_CONSOLE
	int argc = __argc;
	char **argv = __argv;
# endif
	int num_args;
	wchar_t **argv_wide;
	char **argv_save = argv;
	BOOL using_wide_argv = 0;
#endif

	int c;
	int exit_status = SUCCESS;
	int capi_started = 0, sapi_started = 0;
	char *hyss_optarg = NULL;
	int hyss_optind = 1, use_extended_info = 0;
	char *ics_path_override = NULL;
	char *ics_entries = NULL;
	size_t ics_entries_len = 0;
	int ics_ignore = 0;
	sapi_capi_struct *sapi_capi = &cli_sapi_capi;

	/*
	 * Do not move this initialization. It needs to happen before argv is used
	 * in any way.
	 */
	argv = save_ps_args(argc, argv);

#if defined(HYSS_WIN32) && !defined(HYSS_CLI_WIN32_NO_CONSOLE)
	hyss_win32_console_fileno_set_vt100(STDOUT_FILENO, TRUE);
	hyss_win32_console_fileno_set_vt100(STDERR_FILENO, TRUE);
#endif

	cli_sapi_capi.additional_functions = additional_functions;

#if defined(HYSS_WIN32) && defined(_DEBUG) && defined(HYSS_WIN32_DEBUG_HEAP)
	{
		int tmp_flag;
		_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDERR);
		_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);
		_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
		_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
		tmp_flag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
		tmp_flag |= _CRTDBG_DELAY_FREE_MEM_DF;
		tmp_flag |= _CRTDBG_LEAK_CHECK_DF;

		_CrtSetDbgFlag(tmp_flag);
	}
#endif

#ifdef HAVE_SIGNAL_H
#if defined(SIGPIPE) && defined(SIG_IGN)
	signal(SIGPIPE, SIG_IGN); /* ignore SIGPIPE in standalone mode so
								that sockets created via fsockopen()
								don't kill HYSS if the remote site
								closes it.  in clhy|clhyext mode clhy
								does that for us!  thies@thieso.net
								20000419 */
#endif
#endif


#ifdef ZTS
	pbc_startup(1, 1, 0, NULL);
	(void)ts_resource(0);
	GEAR_PBCLS_CACHE_UPDATE();
#endif

	gear_signal_startup();

#ifdef HYSS_WIN32
	_fmode = _O_BINARY;			/*sets default for file streams to binary */
	setmode(_fileno(stdin), O_BINARY);		/* make the stdio mode be binary */
	setmode(_fileno(stdout), O_BINARY);		/* make the stdio mode be binary */
	setmode(_fileno(stderr), O_BINARY);		/* make the stdio mode be binary */
#endif

	while ((c = hyss_getopt(argc, argv, OPTIONS, &hyss_optarg, &hyss_optind, 0, 2))!=-1) {
		switch (c) {
			case 'c':
				if (ics_path_override) {
					free(ics_path_override);
				}
 				ics_path_override = strdup(hyss_optarg);
				break;
			case 'n':
				ics_ignore = 1;
				break;
			case 'd': {
				/* define ics entries on command line */
				size_t len = strlen(hyss_optarg);
				char *val;

				if ((val = strchr(hyss_optarg, '='))) {
					val++;
					if (!isalnum(*val) && *val != '"' && *val != '\'' && *val != '\0') {
						ics_entries = realloc(ics_entries, ics_entries_len + len + sizeof("\"\"\n\0"));
						memcpy(ics_entries + ics_entries_len, hyss_optarg, (val - hyss_optarg));
						ics_entries_len += (val - hyss_optarg);
						memcpy(ics_entries + ics_entries_len, "\"", 1);
						ics_entries_len++;
						memcpy(ics_entries + ics_entries_len, val, len - (val - hyss_optarg));
						ics_entries_len += len - (val - hyss_optarg);
						memcpy(ics_entries + ics_entries_len, "\"\n\0", sizeof("\"\n\0"));
						ics_entries_len += sizeof("\n\0\"") - 2;
					} else {
						ics_entries = realloc(ics_entries, ics_entries_len + len + sizeof("\n\0"));
						memcpy(ics_entries + ics_entries_len, hyss_optarg, len);
						memcpy(ics_entries + ics_entries_len + len, "\n\0", sizeof("\n\0"));
						ics_entries_len += len + sizeof("\n\0") - 2;
					}
				} else {
					ics_entries = realloc(ics_entries, ics_entries_len + len + sizeof("=1\n\0"));
					memcpy(ics_entries + ics_entries_len, hyss_optarg, len);
					memcpy(ics_entries + ics_entries_len + len, "=1\n\0", sizeof("=1\n\0"));
					ics_entries_len += len + sizeof("=1\n\0") - 2;
				}
				break;
			}
#ifndef HYSS_CLI_WIN32_NO_CONSOLE
			case 'S':
				sapi_capi = &cli_server_sapi_capi;
				cli_server_sapi_capi.additional_functions = server_additional_functions;
				break;
#endif
			case 'h': /* help & quit */
			case '?':
				hyss_cli_usage(argv[0]);
				goto out;
			case 'i': case 'v': case 'm':
				sapi_capi = &cli_sapi_capi;
				goto exit_loop;
			case 'e': /* enable extended info output */
				use_extended_info = 1;
				break;
		}
	}
exit_loop:

	sapi_capi->ics_defaults = sapi_cli_ics_defaults;
	sapi_capi->hyss_ics_path_override = ics_path_override;
	sapi_capi->hyssinfo_as_text = 1;
	sapi_capi->hyss_ics_ignore_cwd = 1;
	sapi_startup(sapi_capi);
	sapi_started = 1;

	sapi_capi->hyss_ics_ignore = ics_ignore;

	sapi_capi->executable_location = argv[0];

	if (sapi_capi == &cli_sapi_capi) {
		if (ics_entries) {
			ics_entries = realloc(ics_entries, ics_entries_len + sizeof(HARDCODED_ICS));
			memmove(ics_entries + sizeof(HARDCODED_ICS) - 2, ics_entries, ics_entries_len + 1);
			memcpy(ics_entries, HARDCODED_ICS, sizeof(HARDCODED_ICS) - 2);
		} else {
			ics_entries = malloc(sizeof(HARDCODED_ICS));
			memcpy(ics_entries, HARDCODED_ICS, sizeof(HARDCODED_ICS));
		}
		ics_entries_len += sizeof(HARDCODED_ICS) - 2;
	}

	sapi_capi->ics_entries = ics_entries;

	/* startup after we get the above ics override se we get things right */
	if (sapi_capi->startup(sapi_capi) == FAILURE) {
		/* there is no way to see if we must call gear_ics_deactivate()
		 * since we cannot check if EG(ics_directives) has been initialised
		 * because the executor's constructor does not set initialize it.
		 * Apart from that there seems no need for gear_ics_deactivate() yet.
		 * So we goto out_err.*/
		exit_status = 1;
		goto out;
	}
	capi_started = 1;

#if defined(HYSS_WIN32)
	hyss_win32_cp_cli_setup();
	orig_cp = (hyss_win32_cp_get_orig())->id;
	/* Ignore the delivered argv and argc, read from W API. This place
		might be too late though, but this is the earliest place ATW
		we can access the internal charset information from HYSS. */
	argv_wide = CommandLineToArgvW(GetCommandLineW(), &num_args);
	HYSS_WIN32_CP_W_TO_ANY_ARRAY(argv_wide, num_args, argv, argc)
	using_wide_argv = 1;

	SetConsoleCtrlHandler(hyss_cli_win32_ctrl_handler, TRUE);
#endif

	/* -e option */
	if (use_extended_info) {
		CG(compiler_options) |= GEAR_COMPILE_EXTENDED_INFO;
	}

	gear_first_try {
#ifndef HYSS_CLI_WIN32_NO_CONSOLE
		if (sapi_capi == &cli_sapi_capi) {
#endif
			exit_status = do_cli(argc, argv);
#ifndef HYSS_CLI_WIN32_NO_CONSOLE
		} else {
			exit_status = do_cli_server(argc, argv);
		}
#endif
	} gear_end_try();
out:
	if (ics_path_override) {
		free(ics_path_override);
	}
	if (ics_entries) {
		free(ics_entries);
	}
	if (capi_started) {
		hyss_capi_shutdown();
	}
	if (sapi_started) {
		sapi_shutdown();
	}
#ifdef ZTS
	pbc_shutdown();
#endif

#if defined(HYSS_WIN32)
	(void)hyss_win32_cp_cli_restore();

	if (using_wide_argv) {
		HYSS_WIN32_CP_FREE_ARRAY(argv, argc);
		LocalFree(argv_wide);
	}
	argv = argv_save;
#endif
	/*
	 * Do not move this de-initialization. It needs to happen right before
	 * exiting.
	 */
	cleanup_ps_args(argv);
	exit(exit_status);
}
/* }}} */

