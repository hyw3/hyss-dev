/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef	PS_TITLE_HEADER
#define	PS_TITLE_HEADER

#define PS_TITLE_SUCCESS 0
#define PS_TITLE_NOT_AVAILABLE 1
#define PS_TITLE_NOT_INITIALIZED 2
#define PS_TITLE_BUFFER_NOT_AVAILABLE 3
#define PS_TITLE_WINDOWS_ERROR 4

extern char** save_ps_args(int argc, char** argv);

extern int set_ps_title(const char* new_str);

extern int get_ps_title(int* displen, const char** string);

extern const char* ps_title_errno(int rc);

extern int is_ps_title_available();

extern void cleanup_ps_args(char **argv);

#endif // PS_TITLE_HEADER
