dnl config.m4 for sapi cli

HYSS_ARG_ENABLE(cli,,
[  --disable-cli           Disable building CLI version of HYSS
                          (this forces --without-hexa)], yes, no)

AC_CHECK_FUNCS(setproctitle)

AC_CHECK_HEADERS([sys/pstat.h])

AC_CACHE_CHECK([for PS_STRINGS], [cli_cv_var_PS_STRINGS],
[AC_LINK_IFELSE([AC_LANG_PROGRAM([[#include <machine/vmparam.h>
#include <sys/exec.h>
]],
[[PS_STRINGS->ps_nargvstr = 1;
PS_STRINGS->ps_argvstr = "foo";]])],
[cli_cv_var_PS_STRINGS=yes],
[cli_cv_var_PS_STRINGS=no])])
if test "$cli_cv_var_PS_STRINGS" = yes ; then
  AC_DEFINE([HAVE_PS_STRINGS], [], [Define to 1 if the PS_STRINGS thing exists.])
fi

AC_MSG_CHECKING(for CLI build)
if test "$HYSS_CLI" != "no"; then
  HYSS_ADD_MAKEFILE_FRAGMENT($abs_srcdir/server/cli/Makefile.frag)

  dnl Set filename
  SAPI_CLI_PATH=server/cli/hyss

  dnl Select SAPI
  HYSS_SELECT_SAPI(cli, program, hyss_cli.c hyss_http_parser.c hyss_cli_server.c ps_title.c hyss_cli_process_title.c, -DGEAR_ENABLE_STATIC_PBCLS_CACHE=1, '$(SAPI_CLI_PATH)')

  case $host_alias in
  *aix*)
    if test "$hyss_sapi_capi" = "shared"; then
      BUILD_CLI="echo '\#! .' > hyss.sym && echo >>hyss.sym && nm -BCpg \`echo \$(HYSS_GLOBAL_OBJS) \$(HYSS_BINARY_OBJS) \$(HYSS_CLI_OBJS) | sed 's/\([A-Za-z0-9_]*\)\.lo/.libs\/\1.o/g'\` | \$(AWK) '{ if (((\$\$2 == \"T\") || (\$\$2 == \"D\") || (\$\$2 == \"B\")) && (substr(\$\$3,1,1) != \".\")) { print \$\$3 } }' | sort -u >> hyss.sym && \$(LIBTOOL) --mode=link \$(CC) -export-dynamic \$(CFLAGS_CLEAN) \$(EXTRA_CFLAGS) \$(EXTRA_LDFLAGS_PROGRAM) \$(LDFLAGS) -Wl,-brtl -Wl,-bE:hyss.sym \$(HYSS_RPATHS) \$(HYSS_GLOBAL_OBJS) \$(HYSS_BINARY_OBJS) \$(HYSS_CLI_OBJS) \$(EXTRA_LIBS) \$(GEAR_EXTRA_LIBS) -o \$(SAPI_CLI_PATH)"
    else
      BUILD_CLI="echo '\#! .' > hyss.sym && echo >>hyss.sym && nm -BCpg \`echo \$(HYSS_GLOBAL_OBJS) \$(HYSS_BINARY_OBJS) \$(HYSS_CLI_OBJS) | sed 's/\([A-Za-z0-9_]*\)\.lo/\1.o/g'\` | \$(AWK) '{ if (((\$\$2 == \"T\") || (\$\$2 == \"D\") || (\$\$2 == \"B\")) && (substr(\$\$3,1,1) != \".\")) { print \$\$3 } }' | sort -u >> hyss.sym && \$(LIBTOOL) --mode=link \$(CC) -export-dynamic \$(CFLAGS_CLEAN) \$(EXTRA_CFLAGS) \$(EXTRA_LDFLAGS_PROGRAM) \$(LDFLAGS) -Wl,-brtl -Wl,-bE:hyss.sym \$(HYSS_RPATHS) \$(HYSS_GLOBAL_OBJS) \$(HYSS_BINARY_OBJS) \$(HYSS_CLI_OBJS) \$(EXTRA_LIBS) \$(GEAR_EXTRA_LIBS) -o \$(SAPI_CLI_PATH)"
    fi
    ;;
  *darwin*)
    BUILD_CLI="\$(CC) \$(CFLAGS_CLEAN) \$(EXTRA_CFLAGS) \$(EXTRA_LDFLAGS_PROGRAM) \$(LDFLAGS) \$(NATIVE_RPATHS) \$(HYSS_GLOBAL_OBJS:.lo=.o) \$(HYSS_BINARY_OBJS:.lo=.o) \$(HYSS_CLI_OBJS:.lo=.o) \$(HYSS_FRAMEWORKS) \$(EXTRA_LIBS) \$(GEAR_EXTRA_LIBS) -o \$(SAPI_CLI_PATH)"
    ;;
  *)
    BUILD_CLI="\$(LIBTOOL) --mode=link \$(CC) -export-dynamic \$(CFLAGS_CLEAN) \$(EXTRA_CFLAGS) \$(EXTRA_LDFLAGS_PROGRAM) \$(LDFLAGS) \$(HYSS_RPATHS) \$(HYSS_GLOBAL_OBJS) \$(HYSS_BINARY_OBJS) \$(HYSS_CLI_OBJS) \$(EXTRA_LIBS) \$(GEAR_EXTRA_LIBS) -o \$(SAPI_CLI_PATH)"
    ;;
  esac

  dnl Set executable for tests
  HYSS_EXECUTABLE="\$(top_builddir)/\$(SAPI_CLI_PATH)"
  HYSS_SUBST(HYSS_EXECUTABLE)

  dnl Expose to Makefile
  HYSS_SUBST(SAPI_CLI_PATH)
  HYSS_SUBST(BUILD_CLI)

  HYSS_OUTPUT(server/cli/hyss.1)

  HYSS_INSTALL_HEADERS([server/cli/cli.h])
fi
AC_MSG_RESULT($HYSS_CLI)
