/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <assert.h>

#ifdef HYSS_WIN32
# include <process.h>
# include <io.h>
# include "win32/time.h"
# include "win32/signal.h"
# include "win32/hyss_registry.h"
# include <sys/timeb.h>
#else
# include "hyss_config.h"
#endif

#ifdef __riscos__
#include <unixlib/local.h>
#endif


#if HAVE_TIME_H
#include <time.h>
#endif
#if HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#if HAVE_SIGNAL_H
#include <signal.h>
#endif
#if HAVE_SETLOCALE
#include <locale.h>
#endif
#if HAVE_DLFCN_H
#include <dlfcn.h>
#endif

#include "SAPI.h"
#include "hyss.h"
#include "hyss_ics.h"
#include "hyss_main.h"
#include "hyss_globals.h"
#include "hyss_variables.h"
#include "gear_hash.h"
#include "gear_capis.h"
#include "fopen_wrappers.h"
#include "http_status_codes.h"

#include "gear_compile.h"
#include "gear_execute.h"
#include "gear_highlight.h"
#include "gear_exceptions.h"

#include "hyss_getopt.h"

#ifndef HYSS_WIN32
# define hyss_select(m, r, w, e, t)	select(m, r, w, e, t)
# define SOCK_EINVAL EINVAL
# define SOCK_EAGAIN EAGAIN
# define SOCK_EINTR EINTR
# define SOCK_EADDRINUSE EADDRINUSE
#else
# include "win32/select.h"
# define SOCK_EINVAL WSAEINVAL
# define SOCK_EAGAIN WSAEWOULDBLOCK
# define SOCK_EINTR WSAEINTR
# define SOCK_EADDRINUSE WSAEADDRINUSE
#endif

#include "extslib/standard/file.h" /* for hyss_set_sock_blocking() :-( */
#include "gear_smart_str.h"
#include "extslib/standard/html.h"
#include "extslib/standard/url.h" /* for hyss_raw_url_decode() */
#include "extslib/standard/hyss_string.h" /* for hyss_dirname() */
#include "extslib/date/hyss_date.h" /* for hyss_format_date() */
#include "hyss_network.h"

#include "hyss_http_parser.h"
#include "hyss_cli_server.h"
#include "mime_type_map.h"

#include "hyss_cli_process_title.h"

#define OUTPUT_NOT_CHECKED -1
#define OUTPUT_IS_TTY 1
#define OUTPUT_NOT_TTY 0

typedef struct hyss_cli_server_poller {
	fd_set rfds, wfds;
	struct {
		fd_set rfds, wfds;
	} active;
	hyss_socket_t max_fd;
} hyss_cli_server_poller;

typedef struct hyss_cli_server_request {
	enum hyss_http_method request_method;
	int protocol_version;
	char *request_uri;
	size_t request_uri_len;
	char *vpath;
	size_t vpath_len;
	char *path_translated;
	size_t path_translated_len;
	char *path_info;
	size_t path_info_len;
	char *query_string;
	size_t query_string_len;
	HashTable headers;
	HashTable headers_original_case;
	char *content;
	size_t content_len;
	const char *ext;
	size_t ext_len;
	gear_stat_t sb;
} hyss_cli_server_request;

typedef struct hyss_cli_server_chunk {
	struct hyss_cli_server_chunk *next;
	enum hyss_cli_server_chunk_type {
		HYSS_CLI_SERVER_CHUNK_HEAP,
		HYSS_CLI_SERVER_CHUNK_IMMORTAL
	} type;
	union {
		struct { void *block; char *p; size_t len; } heap;
		struct { const char *p; size_t len; } immortal;
	} data;
} hyss_cli_server_chunk;

typedef struct hyss_cli_server_buffer {
	hyss_cli_server_chunk *first;
	hyss_cli_server_chunk *last;
} hyss_cli_server_buffer;

typedef struct hyss_cli_server_content_sender {
	hyss_cli_server_buffer buffer;
} hyss_cli_server_content_sender;

typedef struct hyss_cli_server_client {
	struct hyss_cli_server *server;
	hyss_socket_t sock;
	struct sockaddr *addr;
	socklen_t addr_len;
	char *addr_str;
	size_t addr_str_len;
	hyss_http_parser parser;
	unsigned int request_read:1;
	char *current_header_name;
	size_t current_header_name_len;
	unsigned int current_header_name_allocated:1;
	char *current_header_value;
	size_t current_header_value_len;
	enum { HEADER_NONE=0, HEADER_FIELD, HEADER_VALUE } last_header_element;
	size_t post_read_offset;
	hyss_cli_server_request request;
	unsigned int content_sender_initialized:1;
	hyss_cli_server_content_sender content_sender;
	int file_fd;
} hyss_cli_server_client;

typedef struct hyss_cli_server {
	hyss_socket_t server_sock;
	hyss_cli_server_poller poller;
	int is_running;
	char *host;
	int port;
	int address_family;
	char *document_root;
	size_t document_root_len;
	char *router;
	size_t router_len;
	socklen_t socklen;
	HashTable clients;
	HashTable extension_mime_types;
} hyss_cli_server;

typedef struct hyss_cli_server_http_response_status_code_pair {
	int code;
	const char *str;
} hyss_cli_server_http_response_status_code_pair;

static hyss_cli_server_http_response_status_code_pair template_map[] = {
	{ 400, "<h1>%s</h1><p>Your browser sent a request that this server could not understand.</p>" },
	{ 404, "<h1>%s</h1><p>The requested resource <code class=\"url\">%s</code> was not found on this server.</p>" },
	{ 500, "<h1>%s</h1><p>The server is temporarily unavailable.</p>" },
	{ 501, "<h1>%s</h1><p>Request method not supported.</p>" }
};

#if HAVE_UNISTD_H
static int hyss_cli_output_is_tty = OUTPUT_NOT_CHECKED;
#endif

static const char hyss_cli_server_request_error_unexpected_eof[] = "Unexpected EOF";

static size_t hyss_cli_server_client_send_through(hyss_cli_server_client *client, const char *str, size_t str_len);
static hyss_cli_server_chunk *hyss_cli_server_chunk_heap_new_self_contained(size_t len);
static void hyss_cli_server_buffer_append(hyss_cli_server_buffer *buffer, hyss_cli_server_chunk *chunk);
static void hyss_cli_server_logf(const char *format, ...);
static void hyss_cli_server_log_response(hyss_cli_server_client *client, int status, const char *message);

GEAR_DECLARE_CAPI_GLOBALS(cli_server);

/* {{{ static char hyss_cli_server_css[]
 * copied from extslib/standard/info.c
 */
static const char hyss_cli_server_css[] = "<style>\n" \
										"body { background-color: #fcfcfc; color: #333333; margin: 0; padding:0; }\n" \
										"h1 { font-size: 1.5em; font-weight: normal; background-color: #9999cc; min-height:2em; line-height:2em; border-bottom: 1px inset black; margin: 0; }\n" \
										"h1, p { padding-left: 10px; }\n" \
										"code.url { background-color: #eeeeee; font-family:monospace; padding:0 2px;}\n" \
										"</style>\n";
/* }}} */

#ifdef HYSS_WIN32
int hyss_cli_server_get_system_time(char *buf) {
	struct _timeb system_time;
	errno_t err;

	if (buf == NULL) {
		return -1;
	}

	_ftime(&system_time);
	err = ctime_s(buf, 52, &(system_time.time) );
	if (err) {
		return -1;
	}
	return 0;
}
#else
int hyss_cli_server_get_system_time(char *buf) {
	struct timeval tv;
	struct tm tm;

	gettimeofday(&tv, NULL);

	/* TODO: should be checked for NULL tm/return vaue */
	hyss_localtime_r(&tv.tv_sec, &tm);
	hyss_asctime_r(&tm, buf);
	return 0;
}
#endif

static void char_ptr_dtor_p(zval *zv) /* {{{ */
{
	pefree(Z_PTR_P(zv), 1);
} /* }}} */

static char *get_last_error() /* {{{ */
{
	return pestrdup(strerror(errno), 1);
} /* }}} */

static int status_comp(const void *a, const void *b) /* {{{ */
{
	const http_response_status_code_pair *pa = (const http_response_status_code_pair *) a;
	const http_response_status_code_pair *pb = (const http_response_status_code_pair *) b;

	if (pa->code < pb->code) {
		return -1;
	} else if (pa->code > pb->code) {
		return 1;
	}

	return 0;
} /* }}} */

static const char *get_status_string(int code) /* {{{ */
{
	http_response_status_code_pair needle = {code, NULL},
		*result = NULL;

	result = bsearch(&needle, http_status_map, http_status_map_len, sizeof(needle), status_comp);

	if (result) {
		return result->str;
	}

	/* Returning NULL would require complicating append_http_status_line() to
	 * not segfault in that case, so let's just return a placeholder, since RFC
	 * 2616 requires a reason phrase. This is basically what a lot of other Web
	 * servers do in this case anyway. */
	return "Unknown Status Code";
} /* }}} */

static const char *get_template_string(int code) /* {{{ */
{
	size_t e = (sizeof(template_map) / sizeof(hyss_cli_server_http_response_status_code_pair));
	size_t s = 0;

	while (e != s) {
		size_t c = MIN((e + s + 1) / 2, e - 1);
		int d = template_map[c].code;
		if (d > code) {
			e = c;
		} else if (d < code) {
			s = c;
		} else {
			return template_map[c].str;
		}
	}
	return NULL;
} /* }}} */

static void append_http_status_line(smart_str *buffer, int protocol_version, int response_code, int persistent) /* {{{ */
{
	if (!response_code) {
		response_code = 200;
	}
	smart_str_appendl_ex(buffer, "HTTP", 4, persistent);
	smart_str_appendc_ex(buffer, '/', persistent);
	smart_str_append_long_ex(buffer, protocol_version / 100, persistent);
	smart_str_appendc_ex(buffer, '.', persistent);
	smart_str_append_long_ex(buffer, protocol_version % 100, persistent);
	smart_str_appendc_ex(buffer, ' ', persistent);
	smart_str_append_long_ex(buffer, response_code, persistent);
	smart_str_appendc_ex(buffer, ' ', persistent);
	smart_str_appends_ex(buffer, get_status_string(response_code), persistent);
	smart_str_appendl_ex(buffer, "\r\n", 2, persistent);
} /* }}} */

static void append_essential_headers(smart_str* buffer, hyss_cli_server_client *client, int persistent) /* {{{ */
{
	char *val;
	struct timeval tv = {0};

	if (NULL != (val = gear_hash_str_find_ptr(&client->request.headers, "host", sizeof("host")-1))) {
		smart_str_appends_ex(buffer, "Host: ", persistent);
		smart_str_appends_ex(buffer, val, persistent);
		smart_str_appends_ex(buffer, "\r\n", persistent);
	}

	if (!gettimeofday(&tv, NULL)) {
		gear_string *dt = hyss_format_date("D, d M Y H:i:s", sizeof("D, d M Y H:i:s") - 1, tv.tv_sec, 0);
		smart_str_appends_ex(buffer, "Date: ", persistent);
		smart_str_appends_ex(buffer, dt->val, persistent);
		smart_str_appends_ex(buffer, " GMT\r\n", persistent);
		gear_string_release_ex(dt, 0);
	}

	smart_str_appendl_ex(buffer, "Connection: close\r\n", sizeof("Connection: close\r\n") - 1, persistent);
} /* }}} */

static const char *get_mime_type(const hyss_cli_server *server, const char *ext, size_t ext_len) /* {{{ */
{
	return (const char*)gear_hash_str_find_ptr(&server->extension_mime_types, ext, ext_len);
} /* }}} */

HYSS_FUNCTION(clhy_request_headers) /* {{{ */
{
	hyss_cli_server_client *client;
	HashTable *headers;
	gear_string *key;
	char *value;
	zval tmp;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	client = SG(server_context);
	headers = &client->request.headers_original_case;

	array_init_size(return_value, gear_hash_num_elements(headers));

	GEAR_HASH_FOREACH_STR_KEY_PTR(headers, key, value) {
		ZVAL_STRING(&tmp, value);
		gear_symtable_update(Z_ARRVAL_P(return_value), key, &tmp);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

static void add_response_header(sapi_header_struct *h, zval *return_value) /* {{{ */
{
	char *s, *p;
	ptrdiff_t  len;
	ALLOCA_FLAG(use_heap)

	if (h->header_len > 0) {
		p = strchr(h->header, ':');
		len = p - h->header;
		if (p && (len > 0)) {
			while (len > 0 && (h->header[len-1] == ' ' || h->header[len-1] == '\t')) {
				len--;
			}
			if (len) {
				s = do_alloca(len + 1, use_heap);
				memcpy(s, h->header, len);
				s[len] = 0;
				do {
					p++;
				} while (*p == ' ' || *p == '\t');
				add_assoc_stringl_ex(return_value, s, (uint32_t)len, p, h->header_len - (p - h->header));
				free_alloca(s, use_heap);
			}
		}
	}
}
/* }}} */

HYSS_FUNCTION(clhy_response_headers) /* {{{ */
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	array_init(return_value);
	gear_llist_apply_with_argument(&SG(sapi_headers).headers, (llist_apply_with_arg_func_t)add_response_header, return_value);
}
/* }}} */

/* {{{ cli_server cAPI
 */

static void cli_server_init_globals(gear_cli_server_globals *cg)
{
	cg->color = 0;
}

HYSS_ICS_BEGIN()
	STD_HYSS_ICS_BOOLEAN("cli_server.color", "0", HYSS_ICS_ALL, OnUpdateBool, color, gear_cli_server_globals, cli_server_globals)
HYSS_ICS_END()

static HYSS_MINIT_FUNCTION(cli_server)
{
	GEAR_INIT_CAPI_GLOBALS(cli_server, cli_server_init_globals, NULL);
	REGISTER_ICS_ENTRIES();
	return SUCCESS;
}

static HYSS_MSHUTDOWN_FUNCTION(cli_server)
{
	UNREGISTER_ICS_ENTRIES();
	return SUCCESS;
}

static HYSS_MINFO_FUNCTION(cli_server)
{
	DISPLAY_ICS_ENTRIES();
}

gear_capi_entry cli_server_capi_entry = {
	STANDARD_CAPI_HEADER,
	"cli_server",
	NULL,
	HYSS_MINIT(cli_server),
	HYSS_MSHUTDOWN(cli_server),
	NULL,
	NULL,
	HYSS_MINFO(cli_server),
	HYSS_VERSION,
	STANDARD_CAPI_PROPERTIES
};
/* }}} */

GEAR_BEGIN_ARG_INFO(arginfo_no_args, 0)
GEAR_END_ARG_INFO()

const gear_function_entry server_additional_functions[] = {
	HYSS_FE(cli_set_process_title,        arginfo_cli_set_process_title)
	HYSS_FE(cli_get_process_title,        arginfo_cli_get_process_title)
	HYSS_FE(clhy_request_headers, arginfo_no_args)
	HYSS_FE(clhy_response_headers, arginfo_no_args)
	HYSS_FALIAS(getallheaders, clhy_request_headers, arginfo_no_args)
	HYSS_FE_END
};

static int sapi_cli_server_startup(sapi_capi_struct *sapi_capi) /* {{{ */
{
	if (hyss_capi_startup(sapi_capi, &cli_server_capi_entry, 1) == FAILURE) {
		return FAILURE;
	}
	return SUCCESS;
} /* }}} */

static size_t sapi_cli_server_ub_write(const char *str, size_t str_length) /* {{{ */
{
	hyss_cli_server_client *client = SG(server_context);
	if (!client) {
		return 0;
	}
	return hyss_cli_server_client_send_through(client, str, str_length);
} /* }}} */

static void sapi_cli_server_flush(void *server_context) /* {{{ */
{
	hyss_cli_server_client *client = server_context;

	if (!client) {
		return;
	}

	if (!GEAR_VALID_SOCKET(client->sock)) {
		hyss_handle_aborted_connection();
		return;
	}

	if (!SG(headers_sent)) {
		sapi_send_headers();
		SG(headers_sent) = 1;
	}
} /* }}} */

static int sapi_cli_server_discard_headers(sapi_headers_struct *sapi_headers) /* {{{ */{
	return SAPI_HEADER_SENT_SUCCESSFULLY;
}
/* }}} */

static int sapi_cli_server_send_headers(sapi_headers_struct *sapi_headers) /* {{{ */
{
	hyss_cli_server_client *client = SG(server_context);
	smart_str buffer = { 0 };
	sapi_header_struct *h;
	gear_llist_position pos;

	if (client == NULL || SG(request_info).no_headers) {
		return SAPI_HEADER_SENT_SUCCESSFULLY;
	}

	if (SG(sapi_headers).http_status_line) {
		smart_str_appends(&buffer, SG(sapi_headers).http_status_line);
		smart_str_appendl(&buffer, "\r\n", 2);
	} else {
		append_http_status_line(&buffer, client->request.protocol_version, SG(sapi_headers).http_response_code, 0);
	}

	append_essential_headers(&buffer, client, 0);

	h = (sapi_header_struct*)gear_llist_get_first_ex(&sapi_headers->headers, &pos);
	while (h) {
		if (h->header_len) {
			smart_str_appendl(&buffer, h->header, h->header_len);
			smart_str_appendl(&buffer, "\r\n", 2);
		}
		h = (sapi_header_struct*)gear_llist_get_next_ex(&sapi_headers->headers, &pos);
	}
	smart_str_appendl(&buffer, "\r\n", 2);

	hyss_cli_server_client_send_through(client, ZSTR_VAL(buffer.s), ZSTR_LEN(buffer.s));

	smart_str_free(&buffer);
	return SAPI_HEADER_SENT_SUCCESSFULLY;
}
/* }}} */

static char *sapi_cli_server_read_cookies(void) /* {{{ */
{
	hyss_cli_server_client *client = SG(server_context);
	char *val;
	if (NULL == (val = gear_hash_str_find_ptr(&client->request.headers, "cookie", sizeof("cookie")-1))) {
		return NULL;
	}
	return val;
} /* }}} */

static size_t sapi_cli_server_read_post(char *buf, size_t count_bytes) /* {{{ */
{
	hyss_cli_server_client *client = SG(server_context);
	if (client->request.content) {
		size_t content_len = client->request.content_len;
		size_t nbytes_copied = MIN(client->post_read_offset + count_bytes, content_len) - client->post_read_offset;
		memmove(buf, client->request.content + client->post_read_offset, nbytes_copied);
		client->post_read_offset += nbytes_copied;
		return nbytes_copied;
	}
	return 0;
} /* }}} */

static void sapi_cli_server_register_variable(zval *track_vars_array, const char *key, const char *val) /* {{{ */
{
	char *new_val = (char *)val;
	size_t new_val_len;

	if (NULL == val) {
		return;
	}

	if (sapi_capi.input_filter(PARSE_SERVER, (char*)key, &new_val, strlen(val), &new_val_len)) {
		hyss_register_variable_safe((char *)key, new_val, new_val_len, track_vars_array);
	}
} /* }}} */

static int sapi_cli_server_register_entry_cb(char **entry, int num_args, va_list args, gear_hash_key *hash_key) /* {{{ */ {
	zval *track_vars_array = va_arg(args, zval *);
	if (hash_key->key) {
		char *real_key, *key;
		uint32_t i;
		key = estrndup(ZSTR_VAL(hash_key->key), ZSTR_LEN(hash_key->key));
		for(i=0; i<ZSTR_LEN(hash_key->key); i++) {
			if (key[i] == '-') {
				key[i] = '_';
			} else {
				key[i] = toupper(key[i]);
			}
		}
		spprintf(&real_key, 0, "%s_%s", "HTTP", key);
		if (strcmp(key, "CONTENT_TYPE") == 0 || strcmp(key, "CONTENT_LENGTH") == 0) {
			sapi_cli_server_register_variable(track_vars_array, key, *entry);
		}
		sapi_cli_server_register_variable(track_vars_array, real_key, *entry);
		efree(key);
		efree(real_key);
	}

	return GEAR_HASH_APPLY_KEEP;
}
/* }}} */

static void sapi_cli_server_register_variables(zval *track_vars_array) /* {{{ */
{
	hyss_cli_server_client *client = SG(server_context);
	sapi_cli_server_register_variable(track_vars_array, "DOCUMENT_ROOT", client->server->document_root);
	{
		char *tmp;
		if ((tmp = strrchr(client->addr_str, ':'))) {
			char addr[64], port[8];
			const char *addr_start = client->addr_str, *addr_end = tmp;
			if (addr_start[0] == '[') addr_start++;
			if (addr_end[-1] == ']') addr_end--;

			strncpy(port, tmp + 1, 8);
			port[7] = '\0';
			strncpy(addr, addr_start, addr_end - addr_start);
			addr[addr_end - addr_start] = '\0';
			sapi_cli_server_register_variable(track_vars_array, "REMOTE_ADDR", addr);
			sapi_cli_server_register_variable(track_vars_array, "REMOTE_PORT", port);
		} else {
			sapi_cli_server_register_variable(track_vars_array, "REMOTE_ADDR", client->addr_str);
		}
	}
	{
		char *tmp;
		spprintf(&tmp, 0, "HYSS %s Development Server", HYSS_VERSION);
		sapi_cli_server_register_variable(track_vars_array, "SERVER_SOFTWARE", tmp);
		efree(tmp);
	}
	{
		char *tmp;
		spprintf(&tmp, 0, "HTTP/%d.%d", client->request.protocol_version / 100, client->request.protocol_version % 100);
		sapi_cli_server_register_variable(track_vars_array, "SERVER_PROTOCOL", tmp);
		efree(tmp);
	}
	sapi_cli_server_register_variable(track_vars_array, "SERVER_NAME", client->server->host);
	{
		char *tmp;
		spprintf(&tmp, 0, "%i",  client->server->port);
		sapi_cli_server_register_variable(track_vars_array, "SERVER_PORT", tmp);
		efree(tmp);
	}

	sapi_cli_server_register_variable(track_vars_array, "REQUEST_URI", client->request.request_uri);
	sapi_cli_server_register_variable(track_vars_array, "REQUEST_METHOD", SG(request_info).request_method);
	sapi_cli_server_register_variable(track_vars_array, "SCRIPT_NAME", client->request.vpath);
	if (SG(request_info).path_translated) {
		sapi_cli_server_register_variable(track_vars_array, "SCRIPT_FILENAME", SG(request_info).path_translated);
	} else if (client->server->router) {
		sapi_cli_server_register_variable(track_vars_array, "SCRIPT_FILENAME", client->server->router);
	}
	if (client->request.path_info) {
		sapi_cli_server_register_variable(track_vars_array, "PATH_INFO", client->request.path_info);
	}
	if (client->request.path_info_len) {
		char *tmp;
		spprintf(&tmp, 0, "%s%s", client->request.vpath, client->request.path_info);
		sapi_cli_server_register_variable(track_vars_array, "HYSS_SELF", tmp);
		efree(tmp);
	} else {
		sapi_cli_server_register_variable(track_vars_array, "HYSS_SELF", client->request.vpath);
	}
	if (client->request.query_string) {
		sapi_cli_server_register_variable(track_vars_array, "QUERY_STRING", client->request.query_string);
	}
	gear_hash_apply_with_arguments(&client->request.headers, (apply_func_args_t)sapi_cli_server_register_entry_cb, 1, track_vars_array);
} /* }}} */

static void sapi_cli_server_log_message(char *msg, int syslog_type_int) /* {{{ */
{
	char buf[52];

	if (hyss_cli_server_get_system_time(buf) != 0) {
		memmove(buf, "unknown time, can't be fetched", sizeof("unknown time, can't be fetched"));
	} else {
		size_t l = strlen(buf);
		if (l > 0) {
			buf[l - 1] = '\0';
		} else {
			memmove(buf, "unknown", sizeof("unknown"));
		}
	}
	fprintf(stderr, "[%s] %s\n", buf, msg);
} /* }}} */

/* {{{ sapi_capi_struct cli_server_sapi_capi
 */
sapi_capi_struct cli_server_sapi_capi = {
	"cli-server",							/* name */
	"Built-in HTTP server",		/* pretty name */

	sapi_cli_server_startup,				/* startup */
	hyss_capi_shutdown_wrapper,	/* shutdown */

	NULL,							/* activate */
	NULL,							/* deactivate */

	sapi_cli_server_ub_write,		/* unbuffered write */
	sapi_cli_server_flush,			/* flush */
	NULL,							/* get uid */
	NULL,							/* getenv */

	hyss_error,						/* error handler */

	NULL,							/* header handler */
	sapi_cli_server_send_headers,	/* send headers handler */
	NULL,							/* send header handler */

	sapi_cli_server_read_post,		/* read POST data */
	sapi_cli_server_read_cookies,	/* read Cookies */

	sapi_cli_server_register_variables,	/* register server variables */
	sapi_cli_server_log_message,	/* Log message */
	NULL,							/* Get request time */
	NULL,							/* Child terminate */

	STANDARD_SAPI_CAPI_PROPERTIES
}; /* }}} */

static int hyss_cli_server_poller_ctor(hyss_cli_server_poller *poller) /* {{{ */
{
	FD_ZERO(&poller->rfds);
	FD_ZERO(&poller->wfds);
	poller->max_fd = -1;
	return SUCCESS;
} /* }}} */

static void hyss_cli_server_poller_add(hyss_cli_server_poller *poller, int mode, hyss_socket_t fd) /* {{{ */
{
	if (mode & POLLIN) {
		HYSS_SAFE_FD_SET(fd, &poller->rfds);
	}
	if (mode & POLLOUT) {
		HYSS_SAFE_FD_SET(fd, &poller->wfds);
	}
	if (fd > poller->max_fd) {
		poller->max_fd = fd;
	}
} /* }}} */

static void hyss_cli_server_poller_remove(hyss_cli_server_poller *poller, int mode, hyss_socket_t fd) /* {{{ */
{
	if (mode & POLLIN) {
		HYSS_SAFE_FD_CLR(fd, &poller->rfds);
	}
	if (mode & POLLOUT) {
		HYSS_SAFE_FD_CLR(fd, &poller->wfds);
	}
#ifndef HYSS_WIN32
	if (fd == poller->max_fd) {
		while (fd > 0) {
			fd--;
			if (HYSS_SAFE_FD_ISSET(fd, &poller->rfds) || HYSS_SAFE_FD_ISSET(fd, &poller->wfds)) {
				break;
			}
		}
		poller->max_fd = fd;
	}
#endif
} /* }}} */

static int hyss_cli_server_poller_poll(hyss_cli_server_poller *poller, struct timeval *tv) /* {{{ */
{
	memmove(&poller->active.rfds, &poller->rfds, sizeof(poller->rfds));
	memmove(&poller->active.wfds, &poller->wfds, sizeof(poller->wfds));
	return hyss_select(poller->max_fd + 1, &poller->active.rfds, &poller->active.wfds, NULL, tv);
} /* }}} */

static int hyss_cli_server_poller_iter_on_active(hyss_cli_server_poller *poller, void *opaque, int(*callback)(void *, hyss_socket_t fd, int events)) /* {{{ */
{
	int retval = SUCCESS;
#ifdef HYSS_WIN32
	struct socket_entry {
		SOCKET fd;
		int events;
	} entries[FD_SETSIZE * 2];
	size_t i;
	struct socket_entry *n = entries, *m;

	for (i = 0; i < poller->active.rfds.fd_count; i++) {
		n->events = POLLIN;
		n->fd = poller->active.rfds.fd_array[i];
		n++;
	}

	m = n;
	for (i = 0; i < poller->active.wfds.fd_count; i++) {
		struct socket_entry *e;
		SOCKET fd = poller->active.wfds.fd_array[i];
		for (e = entries; e < m; e++) {
			if (e->fd == fd) {
				e->events |= POLLOUT;
			}
		}
		if (e == m) {
			assert(n < entries + FD_SETSIZE * 2);
			n->events = POLLOUT;
			n->fd = fd;
			n++;
		}
	}

	{
		struct socket_entry *e = entries;
		for (; e < n; e++) {
			if (SUCCESS != callback(opaque, e->fd, e->events)) {
				retval = FAILURE;
			}
		}
	}

#else
	hyss_socket_t fd;
	const hyss_socket_t max_fd = poller->max_fd;

	for (fd=0 ; fd<=max_fd ; fd++)  {
		if (HYSS_SAFE_FD_ISSET(fd, &poller->active.rfds)) {
                if (SUCCESS != callback(opaque, fd, POLLIN)) {
                    retval = FAILURE;
                }
		}
		if (HYSS_SAFE_FD_ISSET(fd, &poller->active.wfds)) {
                if (SUCCESS != callback(opaque, fd, POLLOUT)) {
                    retval = FAILURE;
                }
		}
	}
#endif
	return retval;
} /* }}} */

static size_t hyss_cli_server_chunk_size(const hyss_cli_server_chunk *chunk) /* {{{ */
{
	switch (chunk->type) {
	case HYSS_CLI_SERVER_CHUNK_HEAP:
		return chunk->data.heap.len;
	case HYSS_CLI_SERVER_CHUNK_IMMORTAL:
		return chunk->data.immortal.len;
	}
	return 0;
} /* }}} */

static void hyss_cli_server_chunk_dtor(hyss_cli_server_chunk *chunk) /* {{{ */
{
	switch (chunk->type) {
	case HYSS_CLI_SERVER_CHUNK_HEAP:
		if (chunk->data.heap.block != chunk) {
			pefree(chunk->data.heap.block, 1);
		}
		break;
	case HYSS_CLI_SERVER_CHUNK_IMMORTAL:
		break;
	}
} /* }}} */

static void hyss_cli_server_buffer_dtor(hyss_cli_server_buffer *buffer) /* {{{ */
{
	hyss_cli_server_chunk *chunk, *next;
	for (chunk = buffer->first; chunk; chunk = next) {
		next = chunk->next;
		hyss_cli_server_chunk_dtor(chunk);
		pefree(chunk, 1);
	}
} /* }}} */

static void hyss_cli_server_buffer_ctor(hyss_cli_server_buffer *buffer) /* {{{ */
{
	buffer->first = NULL;
	buffer->last = NULL;
} /* }}} */

static void hyss_cli_server_buffer_append(hyss_cli_server_buffer *buffer, hyss_cli_server_chunk *chunk) /* {{{ */
{
	hyss_cli_server_chunk *last;
	for (last = chunk; last->next; last = last->next);
	if (!buffer->last) {
		buffer->first = chunk;
	} else {
		buffer->last->next = chunk;
	}
	buffer->last = last;
} /* }}} */

static void hyss_cli_server_buffer_prepend(hyss_cli_server_buffer *buffer, hyss_cli_server_chunk *chunk) /* {{{ */
{
	hyss_cli_server_chunk *last;
	for (last = chunk; last->next; last = last->next);
	last->next = buffer->first;
	if (!buffer->last) {
		buffer->last = last;
	}
	buffer->first = chunk;
} /* }}} */

static size_t hyss_cli_server_buffer_size(const hyss_cli_server_buffer *buffer) /* {{{ */
{
	hyss_cli_server_chunk *chunk;
	size_t retval = 0;
	for (chunk = buffer->first; chunk; chunk = chunk->next) {
		retval += hyss_cli_server_chunk_size(chunk);
	}
	return retval;
} /* }}} */

static hyss_cli_server_chunk *hyss_cli_server_chunk_immortal_new(const char *buf, size_t len) /* {{{ */
{
	hyss_cli_server_chunk *chunk = pemalloc(sizeof(hyss_cli_server_chunk), 1);

	chunk->type = HYSS_CLI_SERVER_CHUNK_IMMORTAL;
	chunk->next = NULL;
	chunk->data.immortal.p = buf;
	chunk->data.immortal.len = len;
	return chunk;
} /* }}} */

static hyss_cli_server_chunk *hyss_cli_server_chunk_heap_new(void *block, char *buf, size_t len) /* {{{ */
{
	hyss_cli_server_chunk *chunk = pemalloc(sizeof(hyss_cli_server_chunk), 1);

	chunk->type = HYSS_CLI_SERVER_CHUNK_HEAP;
	chunk->next = NULL;
	chunk->data.heap.block = block;
	chunk->data.heap.p = buf;
	chunk->data.heap.len = len;
	return chunk;
} /* }}} */

static hyss_cli_server_chunk *hyss_cli_server_chunk_heap_new_self_contained(size_t len) /* {{{ */
{
	hyss_cli_server_chunk *chunk = pemalloc(sizeof(hyss_cli_server_chunk) + len, 1);

	chunk->type = HYSS_CLI_SERVER_CHUNK_HEAP;
	chunk->next = NULL;
	chunk->data.heap.block = chunk;
	chunk->data.heap.p = (char *)(chunk + 1);
	chunk->data.heap.len = len;
	return chunk;
} /* }}} */

static void hyss_cli_server_content_sender_dtor(hyss_cli_server_content_sender *sender) /* {{{ */
{
	hyss_cli_server_buffer_dtor(&sender->buffer);
} /* }}} */

static void hyss_cli_server_content_sender_ctor(hyss_cli_server_content_sender *sender) /* {{{ */
{
	hyss_cli_server_buffer_ctor(&sender->buffer);
} /* }}} */

static int hyss_cli_server_content_sender_send(hyss_cli_server_content_sender *sender, hyss_socket_t fd, size_t *nbytes_sent_total) /* {{{ */
{
	hyss_cli_server_chunk *chunk, *next;
	size_t _nbytes_sent_total = 0;

	for (chunk = sender->buffer.first; chunk; chunk = next) {
#ifdef HYSS_WIN32
		int nbytes_sent;
#else
		ssize_t nbytes_sent;
#endif
		next = chunk->next;

		switch (chunk->type) {
		case HYSS_CLI_SERVER_CHUNK_HEAP:
#ifdef HYSS_WIN32
			nbytes_sent = send(fd, chunk->data.heap.p, (int)chunk->data.heap.len, 0);
#else
			nbytes_sent = send(fd, chunk->data.heap.p, chunk->data.heap.len, 0);
#endif
			if (nbytes_sent < 0) {
				*nbytes_sent_total = _nbytes_sent_total;
				return hyss_socket_errno();
#ifdef HYSS_WIN32
			} else if (nbytes_sent == chunk->data.heap.len) {
#else
			} else if (nbytes_sent == (ssize_t)chunk->data.heap.len) {
#endif
				hyss_cli_server_chunk_dtor(chunk);
				pefree(chunk, 1);
				sender->buffer.first = next;
				if (!next) {
					sender->buffer.last = NULL;
				}
			} else {
				chunk->data.heap.p += nbytes_sent;
				chunk->data.heap.len -= nbytes_sent;
			}
			_nbytes_sent_total += nbytes_sent;
			break;

		case HYSS_CLI_SERVER_CHUNK_IMMORTAL:
#ifdef HYSS_WIN32
			nbytes_sent = send(fd, chunk->data.immortal.p, (int)chunk->data.immortal.len, 0);
#else
			nbytes_sent = send(fd, chunk->data.immortal.p, chunk->data.immortal.len, 0);
#endif
			if (nbytes_sent < 0) {
				*nbytes_sent_total = _nbytes_sent_total;
				return hyss_socket_errno();
#ifdef HYSS_WIN32
			} else if (nbytes_sent == chunk->data.immortal.len) {
#else
			} else if (nbytes_sent == (ssize_t)chunk->data.immortal.len) {
#endif
				hyss_cli_server_chunk_dtor(chunk);
				pefree(chunk, 1);
				sender->buffer.first = next;
				if (!next) {
					sender->buffer.last = NULL;
				}
			} else {
				chunk->data.immortal.p += nbytes_sent;
				chunk->data.immortal.len -= nbytes_sent;
			}
			_nbytes_sent_total += nbytes_sent;
			break;
		}
	}
	*nbytes_sent_total = _nbytes_sent_total;
	return 0;
} /* }}} */

static int hyss_cli_server_content_sender_pull(hyss_cli_server_content_sender *sender, int fd, size_t *nbytes_read) /* {{{ */
{
#ifdef HYSS_WIN32
	int _nbytes_read;
#else
	ssize_t _nbytes_read;
#endif
	hyss_cli_server_chunk *chunk = hyss_cli_server_chunk_heap_new_self_contained(131072);

#ifdef HYSS_WIN32
	_nbytes_read = read(fd, chunk->data.heap.p, (unsigned int)chunk->data.heap.len);
#else
	_nbytes_read = read(fd, chunk->data.heap.p, chunk->data.heap.len);
#endif
	if (_nbytes_read < 0) {
		char *errstr = get_last_error();
		hyss_cli_server_logf("%s", errstr);
		pefree(errstr, 1);
		hyss_cli_server_chunk_dtor(chunk);
		pefree(chunk, 1);
		return 1;
	}
	chunk->data.heap.len = _nbytes_read;
	hyss_cli_server_buffer_append(&sender->buffer, chunk);
	*nbytes_read = _nbytes_read;
	return 0;
} /* }}} */

#if HAVE_UNISTD_H
static int hyss_cli_is_output_tty() /* {{{ */
{
	if (hyss_cli_output_is_tty == OUTPUT_NOT_CHECKED) {
		hyss_cli_output_is_tty = isatty(STDOUT_FILENO);
	}
	return hyss_cli_output_is_tty;
} /* }}} */
#endif

static void hyss_cli_server_log_response(hyss_cli_server_client *client, int status, const char *message) /* {{{ */
{
	int color = 0, effective_status = status;
	char *basic_buf, *message_buf = "", *error_buf = "";
	gear_bool append_error_message = 0;

	if (PG(last_error_message)) {
		switch (PG(last_error_type)) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_PARSE:
				if (status == 200) {
					/* the status code isn't changed by a fatal error, so fake it */
					effective_status = 500;
				}

				append_error_message = 1;
				break;
		}
	}

#if HAVE_UNISTD_H
	if (CLI_SERVER_G(color) && hyss_cli_is_output_tty() == OUTPUT_IS_TTY) {
		if (effective_status >= 500) {
			/* server error: red */
			color = 1;
		} else if (effective_status >= 400) {
			/* client error: yellow */
			color = 3;
		} else if (effective_status >= 200) {
			/* success: green */
			color = 2;
		}
	}
#endif

	/* basic */
	spprintf(&basic_buf, 0, "%s [%d]: %s", client->addr_str, status, client->request.request_uri);
	if (!basic_buf) {
		return;
	}

	/* message */
	if (message) {
		spprintf(&message_buf, 0, " - %s", message);
		if (!message_buf) {
			efree(basic_buf);
			return;
		}
	}

	/* error */
	if (append_error_message) {
		spprintf(&error_buf, 0, " - %s in %s on line %d", PG(last_error_message), PG(last_error_file), PG(last_error_lineno));
		if (!error_buf) {
			efree(basic_buf);
			if (message) {
				efree(message_buf);
			}
			return;
		}
	}

	if (color) {
		hyss_cli_server_logf("\x1b[3%dm%s%s%s\x1b[0m", color, basic_buf, message_buf, error_buf);
	} else {
		hyss_cli_server_logf("%s%s%s", basic_buf, message_buf, error_buf);
	}

	efree(basic_buf);
	if (message) {
		efree(message_buf);
	}
	if (append_error_message) {
		efree(error_buf);
	}
} /* }}} */

static void hyss_cli_server_logf(const char *format, ...) /* {{{ */
{
	char *buf = NULL;
	va_list ap;

	va_start(ap, format);
	vspprintf(&buf, 0, format, ap);
	va_end(ap);

	if (!buf) {
		return;
	}

	if (sapi_capi.log_message) {
		sapi_capi.log_message(buf, -1);
	}

	efree(buf);
} /* }}} */

static hyss_socket_t hyss_network_listen_socket(const char *host, int *port, int socktype, int *af, socklen_t *socklen, gear_string **errstr) /* {{{ */
{
	hyss_socket_t retval = SOCK_ERR;
	int err = 0;
	struct sockaddr *sa = NULL, **p, **sal;

	int num_addrs = hyss_network_getaddresses(host, socktype, &sal, errstr);
	if (num_addrs == 0) {
		return -1;
	}
	for (p = sal; *p; p++) {
		if (sa) {
			pefree(sa, 1);
			sa = NULL;
		}

		retval = socket((*p)->sa_family, socktype, 0);
		if (retval == SOCK_ERR) {
			continue;
		}

		switch ((*p)->sa_family) {
#if HAVE_GETADDRINFO && HAVE_IPV6
		case AF_INET6:
			sa = pemalloc(sizeof(struct sockaddr_in6), 1);
			*(struct sockaddr_in6 *)sa = *(struct sockaddr_in6 *)*p;
			((struct sockaddr_in6 *)sa)->sin6_port = htons(*port);
			*socklen = sizeof(struct sockaddr_in6);
			break;
#endif
		case AF_INET:
			sa = pemalloc(sizeof(struct sockaddr_in), 1);
			*(struct sockaddr_in *)sa = *(struct sockaddr_in *)*p;
			((struct sockaddr_in *)sa)->sin_port = htons(*port);
			*socklen = sizeof(struct sockaddr_in);
			break;
		default:
			/* Unknown family */
			*socklen = 0;
			closesocket(retval);
			continue;
		}

#ifdef SO_REUSEADDR
		{
			int val = 1;
			setsockopt(retval, SOL_SOCKET, SO_REUSEADDR, (char*)&val, sizeof(val));
		}
#endif

		if (bind(retval, sa, *socklen) == SOCK_CONN_ERR) {
			err = hyss_socket_errno();
			if (err == SOCK_EINVAL || err == SOCK_EADDRINUSE) {
				goto out;
			}
			closesocket(retval);
			retval = SOCK_ERR;
			continue;
		}
		err = 0;

		*af = sa->sa_family;
		if (*port == 0) {
			if (getsockname(retval, sa, socklen)) {
				err = hyss_socket_errno();
				goto out;
			}
			switch (sa->sa_family) {
#if HAVE_GETADDRINFO && HAVE_IPV6
			case AF_INET6:
				*port = ntohs(((struct sockaddr_in6 *)sa)->sin6_port);
				break;
#endif
			case AF_INET:
				*port = ntohs(((struct sockaddr_in *)sa)->sin_port);
				break;
			}
		}

		break;
	}

	if (retval == SOCK_ERR) {
		goto out;
	}

	if (listen(retval, SOMAXCONN)) {
		err = hyss_socket_errno();
		goto out;
	}

out:
	if (sa) {
		pefree(sa, 1);
	}
	if (sal) {
		hyss_network_freeaddresses(sal);
	}
	if (err) {
		if (GEAR_VALID_SOCKET(retval)) {
			closesocket(retval);
		}
		if (errstr) {
			*errstr = hyss_socket_error_str(err);
		}
		return SOCK_ERR;
	}
	return retval;
} /* }}} */

static int hyss_cli_server_request_ctor(hyss_cli_server_request *req) /* {{{ */
{
#ifdef ZTS
GEAR_PBCLS_CACHE_UPDATE();
#endif
	req->protocol_version = 0;
	req->request_uri = NULL;
	req->request_uri_len = 0;
	req->vpath = NULL;
	req->vpath_len = 0;
	req->path_translated = NULL;
	req->path_translated_len = 0;
	req->path_info = NULL;
	req->path_info_len = 0;
	req->query_string = NULL;
	req->query_string_len = 0;
	gear_hash_init(&req->headers, 0, NULL, char_ptr_dtor_p, 1);
	gear_hash_init(&req->headers_original_case, 0, NULL, NULL, 1);
	req->content = NULL;
	req->content_len = 0;
	req->ext = NULL;
	req->ext_len = 0;
	return SUCCESS;
} /* }}} */

static void hyss_cli_server_request_dtor(hyss_cli_server_request *req) /* {{{ */
{
	if (req->request_uri) {
		pefree(req->request_uri, 1);
	}
	if (req->vpath) {
		pefree(req->vpath, 1);
	}
	if (req->path_translated) {
		pefree(req->path_translated, 1);
	}
	if (req->path_info) {
		pefree(req->path_info, 1);
	}
	if (req->query_string) {
		pefree(req->query_string, 1);
	}
	gear_hash_destroy(&req->headers);
	gear_hash_destroy(&req->headers_original_case);
	if (req->content) {
		pefree(req->content, 1);
	}
} /* }}} */

static void hyss_cli_server_request_translate_vpath(hyss_cli_server_request *request, const char *document_root, size_t document_root_len) /* {{{ */
{
	gear_stat_t sb;
	static const char *index_files[] = { "index.hyss", "index.html", NULL };
	char *buf = safe_pemalloc(1, request->vpath_len, 1 + document_root_len + 1 + sizeof("index.html"), 1);
	char *p = buf, *prev_path = NULL, *q, *vpath;
	size_t prev_path_len = 0;
	int  is_static_file = 0;

	memmove(p, document_root, document_root_len);
	p += document_root_len;
	vpath = p;
	if (request->vpath_len > 0 && request->vpath[0] != '/') {
		*p++ = DEFAULT_SLASH;
	}
	q = request->vpath + request->vpath_len;
	while (q > request->vpath) {
		if (*q-- == '.') {
			is_static_file = 1;
			break;
		}
	}
	memmove(p, request->vpath, request->vpath_len);
#ifdef HYSS_WIN32
	q = p + request->vpath_len;
	do {
		if (*q == '/') {
			*q = '\\';
		}
	} while (q-- > p);
#endif
	p += request->vpath_len;
	*p = '\0';
	q = p;
	while (q > buf) {
		if (!hyss_sys_stat(buf, &sb)) {
			if (sb.st_mode & S_IFDIR) {
				const char **file = index_files;
				if (q[-1] != DEFAULT_SLASH) {
					*q++ = DEFAULT_SLASH;
				}
				while (*file) {
					size_t l = strlen(*file);
					memmove(q, *file, l + 1);
					if (!hyss_sys_stat(buf, &sb) && (sb.st_mode & S_IFREG)) {
						q += l;
						break;
					}
					file++;
				}
				if (!*file || is_static_file) {
					if (prev_path) {
						pefree(prev_path, 1);
					}
					pefree(buf, 1);
					return;
				}
			}
			break; /* regular file */
		}
		if (prev_path) {
			pefree(prev_path, 1);
			*q = DEFAULT_SLASH;
		}
		while (q > buf && *(--q) != DEFAULT_SLASH);
		prev_path_len = p - q;
		prev_path = pestrndup(q, prev_path_len, 1);
		*q = '\0';
	}
	if (prev_path) {
		request->path_info_len = prev_path_len;
#ifdef HYSS_WIN32
		while (prev_path_len--) {
			if (prev_path[prev_path_len] == '\\') {
				prev_path[prev_path_len] = '/';
			}
		}
#endif
		request->path_info = prev_path;
		pefree(request->vpath, 1);
		request->vpath = pestrndup(vpath, q - vpath, 1);
		request->vpath_len = q - vpath;
		request->path_translated = buf;
		request->path_translated_len = q - buf;
	} else {
		pefree(request->vpath, 1);
		request->vpath = pestrndup(vpath, q - vpath, 1);
		request->vpath_len = q - vpath;
		request->path_translated = buf;
		request->path_translated_len = q - buf;
	}
#ifdef HYSS_WIN32
	{
		uint32_t i = 0;
		for (;i<request->vpath_len;i++) {
			if (request->vpath[i] == '\\') {
				request->vpath[i] = '/';
			}
		}
	}
#endif
	request->sb = sb;
} /* }}} */

static void normalize_vpath(char **retval, size_t *retval_len, const char *vpath, size_t vpath_len, int persistent) /* {{{ */
{
	char *decoded_vpath = NULL;
	char *decoded_vpath_end;
	char *p;

	*retval = NULL;

	decoded_vpath = pestrndup(vpath, vpath_len, persistent);
	if (!decoded_vpath) {
		return;
	}

	decoded_vpath_end = decoded_vpath + hyss_raw_url_decode(decoded_vpath, (int)vpath_len);

#ifdef HYSS_WIN32
	{
		char *p = decoded_vpath;

		do {
			if (*p == '\\') {
				*p = '/';
			}
		} while (*p++);
	}
#endif

	p = decoded_vpath;

	if (p < decoded_vpath_end && *p == '/') {
		char *n = p;
		while (n < decoded_vpath_end && *n == '/') n++;
		memmove(++p, n, decoded_vpath_end - n);
		decoded_vpath_end -= n - p;
	}

	while (p < decoded_vpath_end) {
		char *n = p;
		while (n < decoded_vpath_end && *n != '/') n++;
		if (n - p == 2 && p[0] == '.' && p[1] == '.') {
			if (p > decoded_vpath) {
				--p;
				for (;;) {
					if (p == decoded_vpath) {
						if (*p == '/') {
							p++;
						}
						break;
					}
					if (*(--p) == '/') {
						p++;
						break;
					}
				}
			}
			while (n < decoded_vpath_end && *n == '/') n++;
			memmove(p, n, decoded_vpath_end - n);
			decoded_vpath_end -= n - p;
		} else if (n - p == 1 && p[0] == '.') {
			while (n < decoded_vpath_end && *n == '/') n++;
			memmove(p, n, decoded_vpath_end - n);
			decoded_vpath_end -= n - p;
		} else {
			if (n < decoded_vpath_end) {
				char *nn = n;
				while (nn < decoded_vpath_end && *nn == '/') nn++;
				p = n + 1;
				memmove(p, nn, decoded_vpath_end - nn);
				decoded_vpath_end -= nn - p;
			} else {
				p = n;
			}
		}
	}

	*decoded_vpath_end = '\0';
	*retval = decoded_vpath;
	*retval_len = decoded_vpath_end - decoded_vpath;
} /* }}} */

/* {{{ hyss_cli_server_client_read_request */
static int hyss_cli_server_client_read_request_on_message_begin(hyss_http_parser *parser)
{
	return 0;
}

static int hyss_cli_server_client_read_request_on_path(hyss_http_parser *parser, const char *at, size_t length)
{
	hyss_cli_server_client *client = parser->data;
	{
		char *vpath;
		size_t vpath_len;
		normalize_vpath(&vpath, &vpath_len, at, length, 1);
		client->request.vpath = vpath;
		client->request.vpath_len = vpath_len;
	}
	return 0;
}

static int hyss_cli_server_client_read_request_on_query_string(hyss_http_parser *parser, const char *at, size_t length)
{
	hyss_cli_server_client *client = parser->data;
	client->request.query_string = pestrndup(at, length, 1);
	client->request.query_string_len = length;
	return 0;
}

static int hyss_cli_server_client_read_request_on_url(hyss_http_parser *parser, const char *at, size_t length)
{
	hyss_cli_server_client *client = parser->data;
	client->request.request_method = parser->method;
	client->request.request_uri = pestrndup(at, length, 1);
	client->request.request_uri_len = length;
	return 0;
}

static int hyss_cli_server_client_read_request_on_fragment(hyss_http_parser *parser, const char *at, size_t length)
{
	return 0;
}

static void hyss_cli_server_client_save_header(hyss_cli_server_client *client)
{
	/* strip off the colon */
	gear_string *orig_header_name = gear_string_init(client->current_header_name, client->current_header_name_len, 1);
	gear_string *lc_header_name = gear_string_alloc(client->current_header_name_len, 1);
	gear_str_tolower_copy(ZSTR_VAL(lc_header_name), client->current_header_name, client->current_header_name_len);
	GC_MAKE_PERSISTENT_LOCAL(orig_header_name);
	GC_MAKE_PERSISTENT_LOCAL(lc_header_name);
	gear_hash_add_ptr(&client->request.headers, lc_header_name, client->current_header_value);
	gear_hash_add_ptr(&client->request.headers_original_case, orig_header_name, client->current_header_value);
	gear_string_release_ex(lc_header_name, 1);
	gear_string_release_ex(orig_header_name, 1);

	if (client->current_header_name_allocated) {
		pefree(client->current_header_name, 1);
		client->current_header_name_allocated = 0;
	}
	client->current_header_name = NULL;
	client->current_header_name_len = 0;
	client->current_header_value = NULL;
	client->current_header_value_len = 0;
}

static int hyss_cli_server_client_read_request_on_header_field(hyss_http_parser *parser, const char *at, size_t length)
{
	hyss_cli_server_client *client = parser->data;
	switch (client->last_header_element) {
	case HEADER_VALUE:
		hyss_cli_server_client_save_header(client);
		/* break missing intentionally */
	case HEADER_NONE:
		client->current_header_name = (char *)at;
		client->current_header_name_len = length;
		break;
	case HEADER_FIELD:
		if (client->current_header_name_allocated) {
			size_t new_length = client->current_header_name_len + length;
			client->current_header_name = perealloc(client->current_header_name, new_length + 1, 1);
			memcpy(client->current_header_name + client->current_header_name_len, at, length);
			client->current_header_name[new_length] = '\0';
			client->current_header_name_len = new_length;
		} else {
			size_t new_length = client->current_header_name_len + length;
			char* field = pemalloc(new_length + 1, 1);
			memcpy(field, client->current_header_name, client->current_header_name_len);
			memcpy(field + client->current_header_name_len, at, length);
			field[new_length] = '\0';
			client->current_header_name = field;
			client->current_header_name_len = new_length;
			client->current_header_name_allocated = 1;
		}
		break;
	}

	client->last_header_element = HEADER_FIELD;
	return 0;
}

static int hyss_cli_server_client_read_request_on_header_value(hyss_http_parser *parser, const char *at, size_t length)
{
	hyss_cli_server_client *client = parser->data;
	switch (client->last_header_element) {
	case HEADER_FIELD:
		client->current_header_value = pestrndup(at, length, 1);
		client->current_header_value_len = length;
		break;
	case HEADER_VALUE:
		{
			size_t new_length = client->current_header_value_len + length;
			client->current_header_value = perealloc(client->current_header_value, new_length + 1, 1);
			memcpy(client->current_header_value + client->current_header_value_len, at, length);
			client->current_header_value[new_length] = '\0';
			client->current_header_value_len = new_length;
		}
		break;
	case HEADER_NONE:
		// can't happen
		assert(0);
		break;
	}
	client->last_header_element = HEADER_VALUE;
	return 0;
}

static int hyss_cli_server_client_read_request_on_headers_complete(hyss_http_parser *parser)
{
	hyss_cli_server_client *client = parser->data;
	switch (client->last_header_element) {
	case HEADER_NONE:
		break;
	case HEADER_FIELD:
		client->current_header_value = pemalloc(1, 1);
		*client->current_header_value = '\0';
		client->current_header_value_len = 0;
		/* break missing intentionally */
	case HEADER_VALUE:
		hyss_cli_server_client_save_header(client);
		break;
	}
	client->last_header_element = HEADER_NONE;
	return 0;
}

static int hyss_cli_server_client_read_request_on_body(hyss_http_parser *parser, const char *at, size_t length)
{
	hyss_cli_server_client *client = parser->data;
	if (!client->request.content) {
		client->request.content = pemalloc(parser->content_length, 1);
		client->request.content_len = 0;
	}
	client->request.content = perealloc(client->request.content, client->request.content_len + length, 1);
	memmove(client->request.content + client->request.content_len, at, length);
	client->request.content_len += length;
	return 0;
}

static int hyss_cli_server_client_read_request_on_message_complete(hyss_http_parser *parser)
{
	hyss_cli_server_client *client = parser->data;
	client->request.protocol_version = parser->http_major * 100 + parser->http_minor;
	hyss_cli_server_request_translate_vpath(&client->request, client->server->document_root, client->server->document_root_len);
	{
		const char *vpath = client->request.vpath, *end = vpath + client->request.vpath_len, *p = end;
		client->request.ext = end;
		client->request.ext_len = 0;
		while (p > vpath) {
			--p;
			if (*p == '.') {
				++p;
				client->request.ext = p;
				client->request.ext_len = end - p;
				break;
			}
		}
	}
	client->request_read = 1;
	return 0;
}

static int hyss_cli_server_client_read_request(hyss_cli_server_client *client, char **errstr)
{
	char buf[16384];
	static const hyss_http_parser_settings settings = {
		hyss_cli_server_client_read_request_on_message_begin,
		hyss_cli_server_client_read_request_on_path,
		hyss_cli_server_client_read_request_on_query_string,
		hyss_cli_server_client_read_request_on_url,
		hyss_cli_server_client_read_request_on_fragment,
		hyss_cli_server_client_read_request_on_header_field,
		hyss_cli_server_client_read_request_on_header_value,
		hyss_cli_server_client_read_request_on_headers_complete,
		hyss_cli_server_client_read_request_on_body,
		hyss_cli_server_client_read_request_on_message_complete
	};
	size_t nbytes_consumed;
	int nbytes_read;
	if (client->request_read) {
		return 1;
	}
	nbytes_read = recv(client->sock, buf, sizeof(buf) - 1, 0);
	if (nbytes_read < 0) {
		int err = hyss_socket_errno();
		if (err == SOCK_EAGAIN) {
			return 0;
		}
		*errstr = hyss_socket_strerror(err, NULL, 0);
		return -1;
	} else if (nbytes_read == 0) {
		*errstr = estrdup(hyss_cli_server_request_error_unexpected_eof);
		return -1;
	}
	client->parser.data = client;
	nbytes_consumed = hyss_http_parser_execute(&client->parser, &settings, buf, nbytes_read);
	if (nbytes_consumed != (size_t)nbytes_read) {
		if (buf[0] & 0x80 /* SSLv2 */ || buf[0] == 0x16 /* SSLv3/TLSv1 */) {
			*errstr = estrdup("Unsupported SSL request");
		} else {
			*errstr = estrdup("Malformed HTTP request");
		}
		return -1;
	}
	if (client->current_header_name) {
		char *header_name = safe_pemalloc(client->current_header_name_len, 1, 1, 1);
		memmove(header_name, client->current_header_name, client->current_header_name_len);
		client->current_header_name = header_name;
		client->current_header_name_allocated = 1;
	}
	return client->request_read ? 1: 0;
}
/* }}} */

static size_t hyss_cli_server_client_send_through(hyss_cli_server_client *client, const char *str, size_t str_len) /* {{{ */
{
	struct timeval tv = { 10, 0 };
#ifdef HYSS_WIN32
	int nbytes_left = (int)str_len;
#else
	ssize_t nbytes_left = (ssize_t)str_len;
#endif
	do {
#ifdef HYSS_WIN32
		int nbytes_sent;
#else
		ssize_t nbytes_sent;
#endif

		nbytes_sent = send(client->sock, str + str_len - nbytes_left, nbytes_left, 0);
		if (nbytes_sent < 0) {
			int err = hyss_socket_errno();
			if (err == SOCK_EAGAIN) {
				int nfds = hyss_pollfd_for(client->sock, POLLOUT, &tv);
				if (nfds > 0) {
					continue;
				} else if (nfds < 0) {
					/* error */
					hyss_handle_aborted_connection();
					return nbytes_left;
				} else {
					/* timeout */
					hyss_handle_aborted_connection();
					return nbytes_left;
				}
			} else {
				hyss_handle_aborted_connection();
				return nbytes_left;
			}
		}
		nbytes_left -= nbytes_sent;
	} while (nbytes_left > 0);

	return str_len;
} /* }}} */

static void hyss_cli_server_client_populate_request_info(const hyss_cli_server_client *client, sapi_request_info *request_info) /* {{{ */
{
	char *val;

	request_info->request_method = hyss_http_method_str(client->request.request_method);
	request_info->proto_num = client->request.protocol_version;
	request_info->request_uri = client->request.request_uri;
	request_info->path_translated = client->request.path_translated;
	request_info->query_string = client->request.query_string;
	request_info->content_length = client->request.content_len;
	request_info->auth_user = request_info->auth_password = request_info->auth_digest = NULL;
	if (NULL != (val = gear_hash_str_find_ptr(&client->request.headers, "content-type", sizeof("content-type")-1))) {
		request_info->content_type = val;
	}
} /* }}} */

static void destroy_request_info(sapi_request_info *request_info) /* {{{ */
{
} /* }}} */

static int hyss_cli_server_client_ctor(hyss_cli_server_client *client, hyss_cli_server *server, hyss_socket_t client_sock, struct sockaddr *addr, socklen_t addr_len) /* {{{ */
{
	client->server = server;
	client->sock = client_sock;
	client->addr = addr;
	client->addr_len = addr_len;
	{
		gear_string *addr_str = 0;

		hyss_network_populate_name_from_sockaddr(addr, addr_len, &addr_str, NULL, 0);
		client->addr_str = pestrndup(ZSTR_VAL(addr_str), ZSTR_LEN(addr_str), 1);
		client->addr_str_len = ZSTR_LEN(addr_str);
		gear_string_release_ex(addr_str, 0);
	}
	hyss_http_parser_init(&client->parser, HYSS_HTTP_REQUEST);
	client->request_read = 0;

	client->last_header_element = HEADER_NONE;
	client->current_header_name = NULL;
	client->current_header_name_len = 0;
	client->current_header_name_allocated = 0;
	client->current_header_value = NULL;
	client->current_header_value_len = 0;

	client->post_read_offset = 0;
	if (FAILURE == hyss_cli_server_request_ctor(&client->request)) {
		return FAILURE;
	}
	client->content_sender_initialized = 0;
	client->file_fd = -1;
	return SUCCESS;
} /* }}} */

static void hyss_cli_server_client_dtor(hyss_cli_server_client *client) /* {{{ */
{
	hyss_cli_server_request_dtor(&client->request);
	if (client->file_fd >= 0) {
		close(client->file_fd);
		client->file_fd = -1;
	}
	pefree(client->addr, 1);
	pefree(client->addr_str, 1);
	if (client->content_sender_initialized) {
		hyss_cli_server_content_sender_dtor(&client->content_sender);
	}
} /* }}} */

static void hyss_cli_server_close_connection(hyss_cli_server *server, hyss_cli_server_client *client) /* {{{ */
{
#if HYSS_DEBUG
	hyss_cli_server_logf("%s Closing", client->addr_str);
#endif
	gear_hash_index_del(&server->clients, client->sock);
} /* }}} */

static int hyss_cli_server_send_error_page(hyss_cli_server *server, hyss_cli_server_client *client, int status) /* {{{ */
{
	gear_string *escaped_request_uri = NULL;
	const char *status_string = get_status_string(status);
	const char *content_template = get_template_string(status);
	char *errstr = get_last_error();
	assert(status_string && content_template);

	hyss_cli_server_content_sender_ctor(&client->content_sender);
	client->content_sender_initialized = 1;

	escaped_request_uri = hyss_escape_html_entities_ex((unsigned char *)client->request.request_uri, client->request.request_uri_len, 0, ENT_QUOTES, NULL, 0);

	{
		static const char prologue_template[] = "<!doctype html><html><head><title>%d %s</title>";
		hyss_cli_server_chunk *chunk = hyss_cli_server_chunk_heap_new_self_contained(strlen(prologue_template) + 3 + strlen(status_string) + 1);
		if (!chunk) {
			goto fail;
		}
		snprintf(chunk->data.heap.p, chunk->data.heap.len, prologue_template, status, status_string);
		chunk->data.heap.len = strlen(chunk->data.heap.p);
		hyss_cli_server_buffer_append(&client->content_sender.buffer, chunk);
	}
	{
		hyss_cli_server_chunk *chunk = hyss_cli_server_chunk_immortal_new(hyss_cli_server_css, sizeof(hyss_cli_server_css) - 1);
		if (!chunk) {
			goto fail;
		}
		hyss_cli_server_buffer_append(&client->content_sender.buffer, chunk);
	}
	{
		static const char template[] = "</head><body>";
		hyss_cli_server_chunk *chunk = hyss_cli_server_chunk_immortal_new(template, sizeof(template) - 1);
		if (!chunk) {
			goto fail;
		}
		hyss_cli_server_buffer_append(&client->content_sender.buffer, chunk);
	}
	{
		hyss_cli_server_chunk *chunk = hyss_cli_server_chunk_heap_new_self_contained(strlen(content_template) + ZSTR_LEN(escaped_request_uri) + 3 + strlen(status_string) + 1);
		if (!chunk) {
			goto fail;
		}
		snprintf(chunk->data.heap.p, chunk->data.heap.len, content_template, status_string, ZSTR_VAL(escaped_request_uri));
		chunk->data.heap.len = strlen(chunk->data.heap.p);
		hyss_cli_server_buffer_append(&client->content_sender.buffer, chunk);
	}
	{
		static const char epilogue_template[] = "</body></html>";
		hyss_cli_server_chunk *chunk = hyss_cli_server_chunk_immortal_new(epilogue_template, sizeof(epilogue_template) - 1);
		if (!chunk) {
			goto fail;
		}
		hyss_cli_server_buffer_append(&client->content_sender.buffer, chunk);
	}

	{
		hyss_cli_server_chunk *chunk;
		smart_str buffer = { 0 };
		append_http_status_line(&buffer, client->request.protocol_version, status, 1);
		if (!buffer.s) {
			/* out of memory */
			goto fail;
		}
		append_essential_headers(&buffer, client, 1);
		smart_str_appends_ex(&buffer, "Content-Type: text/html; charset=UTF-8\r\n", 1);
		smart_str_appends_ex(&buffer, "Content-Length: ", 1);
		smart_str_append_unsigned_ex(&buffer, hyss_cli_server_buffer_size(&client->content_sender.buffer), 1);
		smart_str_appendl_ex(&buffer, "\r\n", 2, 1);
		smart_str_appendl_ex(&buffer, "\r\n", 2, 1);

		chunk = hyss_cli_server_chunk_heap_new(buffer.s, ZSTR_VAL(buffer.s), ZSTR_LEN(buffer.s));
		if (!chunk) {
			smart_str_free_ex(&buffer, 1);
			goto fail;
		}
		hyss_cli_server_buffer_prepend(&client->content_sender.buffer, chunk);
	}

	hyss_cli_server_log_response(client, status, errstr ? errstr : "?");
	hyss_cli_server_poller_add(&server->poller, POLLOUT, client->sock);
	if (errstr) {
		pefree(errstr, 1);
	}
	gear_string_free(escaped_request_uri);
	return SUCCESS;

fail:
	if (errstr) {
		pefree(errstr, 1);
	}
	gear_string_free(escaped_request_uri);
	return FAILURE;
} /* }}} */

static int hyss_cli_server_dispatch_script(hyss_cli_server *server, hyss_cli_server_client *client) /* {{{ */
{
	if (strlen(client->request.path_translated) != client->request.path_translated_len) {
		/* can't handle paths that contain nul bytes */
		return hyss_cli_server_send_error_page(server, client, 400);
	}
	{
		gear_file_handle zfd;
		zfd.type = GEAR_HANDLE_FILENAME;
		zfd.filename = SG(request_info).path_translated;
		zfd.handle.fp = NULL;
		zfd.free_filename = 0;
		zfd.opened_path = NULL;
		gear_try {
			hyss_execute_script(&zfd);
		} gear_end_try();
	}

	hyss_cli_server_log_response(client, SG(sapi_headers).http_response_code, NULL);
	return SUCCESS;
} /* }}} */

static int hyss_cli_server_begin_send_static(hyss_cli_server *server, hyss_cli_server_client *client) /* {{{ */
{
	int fd;
	int status = 200;

	if (client->request.path_translated && strlen(client->request.path_translated) != client->request.path_translated_len) {
		/* can't handle paths that contain nul bytes */
		return hyss_cli_server_send_error_page(server, client, 400);
	}

#ifdef HYSS_WIN32
	/* The win32 namespace will cut off trailing dots and spaces. Since the
	   VCWD functionality isn't used here, a sophisticated functionality
	   would have to be reimplemented to know ahead there are no files
	   with invalid names there. The simplest is just to forbid invalid
	   filenames, which is done here. */
	if (client->request.path_translated &&
		('.' == client->request.path_translated[client->request.path_translated_len-1] ||
		 ' ' == client->request.path_translated[client->request.path_translated_len-1])) {
		return hyss_cli_server_send_error_page(server, client, 500);
	}

	fd = client->request.path_translated ? hyss_win32_ioutil_open(client->request.path_translated, O_RDONLY): -1;
#else
	fd = client->request.path_translated ? open(client->request.path_translated, O_RDONLY): -1;
#endif
	if (fd < 0) {
		return hyss_cli_server_send_error_page(server, client, 404);
	}

	hyss_cli_server_content_sender_ctor(&client->content_sender);
	client->content_sender_initialized = 1;
	client->file_fd = fd;

	{
		hyss_cli_server_chunk *chunk;
		smart_str buffer = { 0 };
		const char *mime_type = get_mime_type(server, client->request.ext, client->request.ext_len);

		append_http_status_line(&buffer, client->request.protocol_version, status, 1);
		if (!buffer.s) {
			/* out of memory */
			hyss_cli_server_log_response(client, 500, NULL);
			return FAILURE;
		}
		append_essential_headers(&buffer, client, 1);
		if (mime_type) {
			smart_str_appendl_ex(&buffer, "Content-Type: ", sizeof("Content-Type: ") - 1, 1);
			smart_str_appends_ex(&buffer, mime_type, 1);
			if (strncmp(mime_type, "text/", 5) == 0) {
				smart_str_appends_ex(&buffer, "; charset=UTF-8", 1);
			}
			smart_str_appendl_ex(&buffer, "\r\n", 2, 1);
		}
		smart_str_appends_ex(&buffer, "Content-Length: ", 1);
		smart_str_append_unsigned_ex(&buffer, client->request.sb.st_size, 1);
		smart_str_appendl_ex(&buffer, "\r\n", 2, 1);
		smart_str_appendl_ex(&buffer, "\r\n", 2, 1);
		chunk = hyss_cli_server_chunk_heap_new(buffer.s, ZSTR_VAL(buffer.s), ZSTR_LEN(buffer.s));
		if (!chunk) {
			smart_str_free_ex(&buffer, 1);
			hyss_cli_server_log_response(client, 500, NULL);
			return FAILURE;
		}
		hyss_cli_server_buffer_append(&client->content_sender.buffer, chunk);
	}
	hyss_cli_server_log_response(client, 200, NULL);
	hyss_cli_server_poller_add(&server->poller, POLLOUT, client->sock);
	return SUCCESS;
}
/* }}} */

static int hyss_cli_server_request_startup(hyss_cli_server *server, hyss_cli_server_client *client) { /* {{{ */
	char *auth;
	hyss_cli_server_client_populate_request_info(client, &SG(request_info));
	if (NULL != (auth = gear_hash_str_find_ptr(&client->request.headers, "authorization", sizeof("authorization")-1))) {
		hyss_handle_auth_data(auth);
	}
	SG(sapi_headers).http_response_code = 200;
	if (FAILURE == hyss_request_startup()) {
		/* should never be happen */
		destroy_request_info(&SG(request_info));
		return FAILURE;
	}
	PG(during_request_startup) = 0;

	return SUCCESS;
}
/* }}} */

static int hyss_cli_server_request_shutdown(hyss_cli_server *server, hyss_cli_server_client *client) { /* {{{ */
	hyss_request_shutdown(0);
	hyss_cli_server_close_connection(server, client);
	destroy_request_info(&SG(request_info));
	SG(server_context) = NULL;
	SG(rfc1867_uploaded_files) = NULL;
	return SUCCESS;
}
/* }}} */

static int hyss_cli_server_dispatch_router(hyss_cli_server *server, hyss_cli_server_client *client) /* {{{ */
{
	int decline = 0;
	gear_file_handle zfd;
	char *old_cwd;

	ALLOCA_FLAG(use_heap)
	old_cwd = do_alloca(MAXPATHLEN, use_heap);
	old_cwd[0] = '\0';
	hyss_ignore_value(VCWD_GETCWD(old_cwd, MAXPATHLEN - 1));

	zfd.type = GEAR_HANDLE_FILENAME;
	zfd.filename = server->router;
	zfd.handle.fp = NULL;
	zfd.free_filename = 0;
	zfd.opened_path = NULL;

	gear_try {
		zval retval;

		ZVAL_UNDEF(&retval);
		if (SUCCESS == gear_execute_scripts(GEAR_REQUIRE, &retval, 1, &zfd)) {
			if (Z_TYPE(retval) != IS_UNDEF) {
				decline = Z_TYPE(retval) == IS_FALSE;
				zval_ptr_dtor(&retval);
			}
		} else {
			decline = 1;
		}
	} gear_end_try();

	if (old_cwd[0] != '\0') {
		hyss_ignore_value(VCWD_CHDIR(old_cwd));
	}

	free_alloca(old_cwd, use_heap);

	return decline;
}
/* }}} */

static int hyss_cli_server_dispatch(hyss_cli_server *server, hyss_cli_server_client *client) /* {{{ */
{
	int is_static_file  = 0;

	SG(server_context) = client;
	if (client->request.ext_len != 4 || memcmp(client->request.ext, "hyss", 4) || !client->request.path_translated) {
		is_static_file = 1;
	}

	if (server->router || !is_static_file) {
		if (FAILURE == hyss_cli_server_request_startup(server, client)) {
			SG(server_context) = NULL;
			hyss_cli_server_close_connection(server, client);
			destroy_request_info(&SG(request_info));
			return SUCCESS;
		}
	}

	if (server->router) {
		if (!hyss_cli_server_dispatch_router(server, client)) {
			hyss_cli_server_request_shutdown(server, client);
			return SUCCESS;
		}
	}

	if (!is_static_file) {
		if (SUCCESS == hyss_cli_server_dispatch_script(server, client)
				|| SUCCESS != hyss_cli_server_send_error_page(server, client, 500)) {
			if (SG(sapi_headers).http_response_code == 304) {
				SG(sapi_headers).send_default_content_type = 0;
			}
			hyss_cli_server_request_shutdown(server, client);
			return SUCCESS;
		}
	} else {
		if (server->router) {
			static int (*send_header_func)(sapi_headers_struct *);
			send_header_func = sapi_capi.send_headers;
			/* do not generate default content type header */
		    SG(sapi_headers).send_default_content_type = 0;
			/* we don't want headers to be sent */
			sapi_capi.send_headers = sapi_cli_server_discard_headers;
			hyss_request_shutdown(0);
			sapi_capi.send_headers = send_header_func;
		    SG(sapi_headers).send_default_content_type = 1;
			SG(rfc1867_uploaded_files) = NULL;
		}
		if (SUCCESS != hyss_cli_server_begin_send_static(server, client)) {
			hyss_cli_server_close_connection(server, client);
		}
		SG(server_context) = NULL;
		return SUCCESS;
	}

	SG(server_context) = NULL;
	destroy_request_info(&SG(request_info));
	return SUCCESS;
}
/* }}} */

static int hyss_cli_server_mime_type_ctor(hyss_cli_server *server, const hyss_cli_server_ext_mime_type_pair *mime_type_map) /* {{{ */
{
	const hyss_cli_server_ext_mime_type_pair *pair;

	gear_hash_init(&server->extension_mime_types, 0, NULL, NULL, 1);

	for (pair = mime_type_map; pair->ext; pair++) {
		size_t ext_len = strlen(pair->ext);
		gear_hash_str_add_ptr(&server->extension_mime_types, pair->ext, ext_len, (void*)pair->mime_type);
	}

	return SUCCESS;
} /* }}} */

static void hyss_cli_server_dtor(hyss_cli_server *server) /* {{{ */
{
	gear_hash_destroy(&server->clients);
	gear_hash_destroy(&server->extension_mime_types);
	if (GEAR_VALID_SOCKET(server->server_sock)) {
		closesocket(server->server_sock);
	}
	if (server->host) {
		pefree(server->host, 1);
	}
	if (server->document_root) {
		pefree(server->document_root, 1);
	}
	if (server->router) {
		pefree(server->router, 1);
	}
} /* }}} */

static void hyss_cli_server_client_dtor_wrapper(zval *zv) /* {{{ */
{
	hyss_cli_server_client *p = Z_PTR_P(zv);

	closesocket(p->sock);
	hyss_cli_server_poller_remove(&p->server->poller, POLLIN | POLLOUT, p->sock);
	hyss_cli_server_client_dtor(p);
	pefree(p, 1);
} /* }}} */

static int hyss_cli_server_ctor(hyss_cli_server *server, const char *addr, const char *document_root, const char *router) /* {{{ */
{
	int retval = SUCCESS;
	char *host = NULL;
	gear_string *errstr = NULL;
	char *_document_root = NULL;
	char *_router = NULL;
	int err = 0;
	int port = 3000;
	hyss_socket_t server_sock = SOCK_ERR;
	char *p = NULL;

	if (addr[0] == '[') {
		host = pestrdup(addr + 1, 1);
		if (!host) {
			return FAILURE;
		}
		p = strchr(host, ']');
		if (p) {
			*p++ = '\0';
			if (*p == ':') {
				port = strtol(p + 1, &p, 10);
				if (port <= 0 || port > 65535) {
					p = NULL;
				}
			} else if (*p != '\0') {
				p = NULL;
			}
		}
	} else {
		host = pestrdup(addr, 1);
		if (!host) {
			return FAILURE;
		}
		p = strchr(host, ':');
		if (p) {
			*p++ = '\0';
			port = strtol(p, &p, 10);
			if (port <= 0 || port > 65535) {
				p = NULL;
			}
		}
	}
	if (!p) {
		fprintf(stderr, "Invalid address: %s\n", addr);
		retval = FAILURE;
		goto out;
	}

	server_sock = hyss_network_listen_socket(host, &port, SOCK_STREAM, &server->address_family, &server->socklen, &errstr);
	if (server_sock == SOCK_ERR) {
		hyss_cli_server_logf("Failed to listen on %s:%d (reason: %s)", host, port, errstr ? ZSTR_VAL(errstr) : "?");
		if (errstr) {
			gear_string_release_ex(errstr, 0);
		}
		retval = FAILURE;
		goto out;
	}
	server->server_sock = server_sock;

	err = hyss_cli_server_poller_ctor(&server->poller);
	if (SUCCESS != err) {
		goto out;
	}

	hyss_cli_server_poller_add(&server->poller, POLLIN, server_sock);

	server->host = host;
	server->port = port;

	gear_hash_init(&server->clients, 0, NULL, hyss_cli_server_client_dtor_wrapper, 1);

	{
		size_t document_root_len = strlen(document_root);
		_document_root = pestrndup(document_root, document_root_len, 1);
		if (!_document_root) {
			retval = FAILURE;
			goto out;
		}
		server->document_root = _document_root;
		server->document_root_len = document_root_len;
	}

	if (router) {
		size_t router_len = strlen(router);
		_router = pestrndup(router, router_len, 1);
		if (!_router) {
			retval = FAILURE;
			goto out;
		}
		server->router = _router;
		server->router_len = router_len;
	} else {
		server->router = NULL;
		server->router_len = 0;
	}

	if (hyss_cli_server_mime_type_ctor(server, mime_type_map) == FAILURE) {
		retval = FAILURE;
		goto out;
	}

	server->is_running = 1;
out:
	if (retval != SUCCESS) {
		if (host) {
			pefree(host, 1);
		}
		if (_document_root) {
			pefree(_document_root, 1);
		}
		if (_router) {
			pefree(_router, 1);
		}
		if (server_sock > -1) {
			closesocket(server_sock);
		}
	}
	return retval;
} /* }}} */

static int hyss_cli_server_recv_event_read_request(hyss_cli_server *server, hyss_cli_server_client *client) /* {{{ */
{
	char *errstr = NULL;
	int status = hyss_cli_server_client_read_request(client, &errstr);
	if (status < 0) {
		if (strcmp(errstr, hyss_cli_server_request_error_unexpected_eof) == 0 && client->parser.state == s_start_req) {
#if HYSS_DEBUG
			hyss_cli_server_logf("%s Closed without sending a request; it was probably just an unused speculative preconnection", client->addr_str);
#endif
		} else {
			hyss_cli_server_logf("%s Invalid request (%s)", client->addr_str, errstr);
		}
		efree(errstr);
		hyss_cli_server_close_connection(server, client);
		return FAILURE;
	} else if (status == 1 && client->request.request_method == HYSS_HTTP_NOT_IMPLEMENTED) {
		return hyss_cli_server_send_error_page(server, client, 501);
	} else if (status == 1) {
		hyss_cli_server_poller_remove(&server->poller, POLLIN, client->sock);
		hyss_cli_server_dispatch(server, client);
	} else {
		hyss_cli_server_poller_add(&server->poller, POLLIN, client->sock);
	}

	return SUCCESS;
} /* }}} */

static int hyss_cli_server_send_event(hyss_cli_server *server, hyss_cli_server_client *client) /* {{{ */
{
	if (client->content_sender_initialized) {
		if (client->file_fd >= 0 && !client->content_sender.buffer.first) {
			size_t nbytes_read;
			if (hyss_cli_server_content_sender_pull(&client->content_sender, client->file_fd, &nbytes_read)) {
				hyss_cli_server_close_connection(server, client);
				return FAILURE;
			}
			if (nbytes_read == 0) {
				close(client->file_fd);
				client->file_fd = -1;
			}
		}
		{
			size_t nbytes_sent;
			int err = hyss_cli_server_content_sender_send(&client->content_sender, client->sock, &nbytes_sent);
			if (err && err != SOCK_EAGAIN) {
				hyss_cli_server_close_connection(server, client);
				return FAILURE;
			}
		}
		if (!client->content_sender.buffer.first && client->file_fd < 0) {
			hyss_cli_server_close_connection(server, client);
		}
	}
	return SUCCESS;
}
/* }}} */

typedef struct hyss_cli_server_do_event_for_each_fd_callback_params {
	hyss_cli_server *server;
	int(*rhandler)(hyss_cli_server*, hyss_cli_server_client*);
	int(*whandler)(hyss_cli_server*, hyss_cli_server_client*);
} hyss_cli_server_do_event_for_each_fd_callback_params;

static int hyss_cli_server_do_event_for_each_fd_callback(void *_params, hyss_socket_t fd, int event) /* {{{ */
{
	hyss_cli_server_do_event_for_each_fd_callback_params *params = _params;
	hyss_cli_server *server = params->server;
	if (server->server_sock == fd) {
		hyss_cli_server_client *client = NULL;
		hyss_socket_t client_sock;
		socklen_t socklen = server->socklen;
		struct sockaddr *sa = pemalloc(server->socklen, 1);
		client_sock = accept(server->server_sock, sa, &socklen);
		if (!GEAR_VALID_SOCKET(client_sock)) {
			char *errstr;
			errstr = hyss_socket_strerror(hyss_socket_errno(), NULL, 0);
			hyss_cli_server_logf("Failed to accept a client (reason: %s)", errstr);
			efree(errstr);
			pefree(sa, 1);
			return SUCCESS;
		}
		if (SUCCESS != hyss_set_sock_blocking(client_sock, 0)) {
			pefree(sa, 1);
			closesocket(client_sock);
			return SUCCESS;
		}
		client = pemalloc(sizeof(hyss_cli_server_client), 1);
		if (FAILURE == hyss_cli_server_client_ctor(client, server, client_sock, sa, socklen)) {
			hyss_cli_server_logf("Failed to create a new request object");
			pefree(sa, 1);
			closesocket(client_sock);
			return SUCCESS;
		}
#if HYSS_DEBUG
		hyss_cli_server_logf("%s Accepted", client->addr_str);
#endif
		gear_hash_index_update_ptr(&server->clients, client_sock, client);
		hyss_cli_server_recv_event_read_request(server, client);
	} else {
		hyss_cli_server_client *client;
		if (NULL != (client = gear_hash_index_find_ptr(&server->clients, fd))) {
			if (event & POLLIN) {
				params->rhandler(server, client);
			}
			if (event & POLLOUT) {
				params->whandler(server, client);
			}
		}
	}
	return SUCCESS;
} /* }}} */

static void hyss_cli_server_do_event_for_each_fd(hyss_cli_server *server, int(*rhandler)(hyss_cli_server*, hyss_cli_server_client*), int(*whandler)(hyss_cli_server*, hyss_cli_server_client*)) /* {{{ */
{
	hyss_cli_server_do_event_for_each_fd_callback_params params = {
		server,
		rhandler,
		whandler
	};

	hyss_cli_server_poller_iter_on_active(&server->poller, &params, hyss_cli_server_do_event_for_each_fd_callback);
} /* }}} */

static int hyss_cli_server_do_event_loop(hyss_cli_server *server) /* {{{ */
{
	int retval = SUCCESS;
	while (server->is_running) {
		struct timeval tv = { 1, 0 };
		int n = hyss_cli_server_poller_poll(&server->poller, &tv);
		if (n > 0) {
			hyss_cli_server_do_event_for_each_fd(server,
					hyss_cli_server_recv_event_read_request,
					hyss_cli_server_send_event);
		} else if (n == 0) {
			/* do nothing */
		} else {
			int err = hyss_socket_errno();
			if (err != SOCK_EINTR) {
				char *errstr = hyss_socket_strerror(err, NULL, 0);
				hyss_cli_server_logf("%s", errstr);
				efree(errstr);
				retval = FAILURE;
				goto out;
			}
		}
	}
out:
	return retval;
} /* }}} */

static hyss_cli_server server;

static void hyss_cli_server_sigint_handler(int sig) /* {{{ */
{
	server.is_running = 0;
}
/* }}} */

int do_cli_server(int argc, char **argv) /* {{{ */
{
	char *hyss_optarg = NULL;
	int hyss_optind = 1;
	int c;
	const char *server_bind_address = NULL;
	extern const opt_struct OPTIONS[];
	const char *document_root = NULL;
#ifdef HYSS_WIN32
	char document_root_tmp[MAXPATHLEN];
	size_t k;
#endif
	const char *router = NULL;
	char document_root_buf[MAXPATHLEN];

	while ((c = hyss_getopt(argc, argv, OPTIONS, &hyss_optarg, &hyss_optind, 0, 2))!=-1) {
		switch (c) {
			case 'S':
				server_bind_address = hyss_optarg;
				break;
			case 't':
#ifndef HYSS_WIN32
				document_root = hyss_optarg;
#else
				k = strlen(hyss_optarg);
				if (k + 1 > MAXPATHLEN) {
					fprintf(stderr, "Document root path is too long.\n");
					return 1;
				}
				memmove(document_root_tmp, hyss_optarg, k + 1);
				/* Clean out any trailing garbage that might have been passed
					from a batch script. */
				do {
					document_root_tmp[k] = '\0';
					k--;
				} while ('"' == document_root_tmp[k] || ' ' == document_root_tmp[k]);
				document_root = document_root_tmp;
#endif
				break;
		}
	}

	if (document_root) {
		gear_stat_t sb;

		if (hyss_sys_stat(document_root, &sb)) {
			fprintf(stderr, "Directory %s does not exist.\n", document_root);
			return 1;
		}
		if (!S_ISDIR(sb.st_mode)) {
			fprintf(stderr, "%s is not a directory.\n", document_root);
			return 1;
		}
		if (VCWD_REALPATH(document_root, document_root_buf)) {
			document_root = document_root_buf;
		}
	} else {
		char *ret = NULL;

#if HAVE_GETCWD
		ret = VCWD_GETCWD(document_root_buf, MAXPATHLEN);
#elif HAVE_GETWD
		ret = VCWD_GETWD(document_root_buf);
#endif
		document_root = ret ? document_root_buf: ".";
	}

	if (argc > hyss_optind) {
		router = argv[hyss_optind];
	}

	if (FAILURE == hyss_cli_server_ctor(&server, server_bind_address, document_root, router)) {
		return 1;
	}
	sapi_capi.hyssinfo_as_text = 0;

	{
		char buf[52];

		if (hyss_cli_server_get_system_time(buf) != 0) {
			memmove(buf, "unknown time, can't be fetched", sizeof("unknown time, can't be fetched"));
		}

		printf("HYSS %s Development Server started at %s"
				"Listening on http://%s\n"
				"Document root is %s\n"
				"Press Ctrl-C to quit.\n",
				HYSS_VERSION, buf, server_bind_address, document_root);
	}

#if defined(HAVE_SIGNAL_H) && defined(SIGINT)
	signal(SIGINT, hyss_cli_server_sigint_handler);
	gear_signal_init();
#endif
	hyss_cli_server_do_event_loop(&server);
	hyss_cli_server_dtor(&server);
	return 0;
} /* }}} */

