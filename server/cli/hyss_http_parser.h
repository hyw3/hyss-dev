/* Copyright 2009,2010 Ryan Dahl <ry@tinyclouds.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
 
/* modified by Moriyoshi Koizumi to make it fit to HYSS source tree. */

#ifndef hyss_http_parser_h
#define hyss_http_parser_h
#ifdef __cplusplus
extern "C" {
#endif


#include <sys/types.h>
#if defined(_WIN32) && !defined(__MINGW32__)
# include <windows.h>
# include "config.w32.h"
#else
# include "hyss_config.h"
#endif

#include "hyss_stdint.h"

/* Compile with -DHYSS_HTTP_PARSER_STRICT=0 to make less checks, but run
 * faster
 */
#ifndef HYSS_HTTP_PARSER_STRICT
# define HYSS_HTTP_PARSER_STRICT 1
#else
# define HYSS_HTTP_PARSER_STRICT 0
#endif


/* Maximium header size allowed */
#define HYSS_HTTP_MAX_HEADER_SIZE (80*1024)


typedef struct hyss_http_parser hyss_http_parser;
typedef struct hyss_http_parser_settings hyss_http_parser_settings;


/* Callbacks should return non-zero to indicate an error. The parser will
 * then halt execution.
 *
 * The one exception is on_headers_complete. In a HYSS_HTTP_RESPONSE parser
 * returning '1' from on_headers_complete will tell the parser that it
 * should not expect a body. This is used when receiving a response to a
 * HEAD request which may contain 'Content-Length' or 'Transfer-Encoding:
 * chunked' headers that indicate the presence of a body.
 *
 * http_data_cb does not return data chunks. It will be call arbitrarally
 * many times for each string. E.G. you might get 10 callbacks for "on_path"
 * each providing just a few characters more data.
 */
typedef int (*hyss_http_data_cb) (hyss_http_parser*, const char *at, size_t length);
typedef int (*hyss_http_cb) (hyss_http_parser*);


/* Request Methods */
enum hyss_http_method
  { HYSS_HTTP_DELETE    = 0
  , HYSS_HTTP_GET
  , HYSS_HTTP_HEAD
  , HYSS_HTTP_POST
  , HYSS_HTTP_PUT
  , HYSS_HTTP_PATCH
  /* pathological */
  , HYSS_HTTP_CONNECT
  , HYSS_HTTP_OPTIONS
  , HYSS_HTTP_TRACE
  /* webdav */
  , HYSS_HTTP_COPY
  , HYSS_HTTP_LOCK
  , HYSS_HTTP_MKCOL
  , HYSS_HTTP_MOVE
  , HYSS_HTTP_MKCALENDAR
  , HYSS_HTTP_PROPFIND
  , HYSS_HTTP_PROPPATCH
  , HYSS_HTTP_SEARCH
  , HYSS_HTTP_UNLOCK
  /* subversion */
  , HYSS_HTTP_REPORT
  , HYSS_HTTP_MKACTIVITY
  , HYSS_HTTP_CHECKOUT
  , HYSS_HTTP_MERGE
  /* upnp */
  , HYSS_HTTP_MSEARCH
  , HYSS_HTTP_NOTIFY
  , HYSS_HTTP_SUBSCRIBE
  , HYSS_HTTP_UNSUBSCRIBE
  /* unknown, not implemented */
  , HYSS_HTTP_NOT_IMPLEMENTED
  };


enum hyss_http_parser_type { HYSS_HTTP_REQUEST, HYSS_HTTP_RESPONSE, HYSS_HTTP_BOTH };

enum state
  { s_dead = 1 /* important that this is > 0 */

  , s_start_req_or_res
  , s_res_or_resp_H
  , s_start_res
  , s_res_H
  , s_res_HT
  , s_res_HTT
  , s_res_HTTP
  , s_res_first_http_major
  , s_res_http_major
  , s_res_first_http_minor
  , s_res_http_minor
  , s_res_first_status_code
  , s_res_status_code
  , s_res_status
  , s_res_line_almost_done

  , s_start_req

  , s_req_method
  , s_req_spaces_before_url
  , s_req_schema
  , s_req_schema_slash
  , s_req_schema_slash_slash
  , s_req_host
  , s_req_port
  , s_req_path
  , s_req_query_string_start
  , s_req_query_string
  , s_req_fragment_start
  , s_req_fragment
  , s_req_http_start
  , s_req_http_H
  , s_req_http_HT
  , s_req_http_HTT
  , s_req_http_HTTP
  , s_req_first_http_major
  , s_req_http_major
  , s_req_first_http_minor
  , s_req_http_minor
  , s_req_line_almost_done

  , s_header_field_start
  , s_header_field
  , s_header_value_start
  , s_header_value

  , s_header_almost_done

  , s_headers_almost_done
  /* Important: 's_headers_almost_done' must be the last 'header' state. All
   * states beyond this must be 'body' states. It is used for overflow
   * checking. See the PARSING_HEADER() macro.
   */
  , s_chunk_size_start
  , s_chunk_size
  , s_chunk_size_almost_done
  , s_chunk_parameters
  , s_chunk_data
  , s_chunk_data_almost_done
  , s_chunk_data_done

  , s_body_identity
  , s_body_identity_eof
  };

struct hyss_http_parser {
  /** PRIVATE **/
  unsigned char type : 2;
  unsigned char flags : 6;
  unsigned char state;
  unsigned char header_state;
  unsigned char index;

  uint32_t nread;
  ssize_t  content_length;

  /** READ-ONLY **/
  unsigned short http_major;
  unsigned short http_minor;
  unsigned short status_code; /* responses only */
  unsigned char method;    /* requests only */

  /* 1 = Upgrade header was present and the parser has exited because of that.
   * 0 = No upgrade header present.
   * Should be checked when http_parser_execute() returns in addition to
   * error checking.
   */
  char upgrade;

  /** PUBLIC **/
  void *data; /* A pointer to get hook to the "connection" or "socket" object */
};


struct hyss_http_parser_settings {
  hyss_http_cb      on_message_begin;
  hyss_http_data_cb on_path;
  hyss_http_data_cb on_query_string;
  hyss_http_data_cb on_url;
  hyss_http_data_cb on_fragment;
  hyss_http_data_cb on_header_field;
  hyss_http_data_cb on_header_value;
  hyss_http_cb      on_headers_complete;
  hyss_http_data_cb on_body;
  hyss_http_cb      on_message_complete;
};


void hyss_http_parser_init(hyss_http_parser *parser, enum hyss_http_parser_type type);


size_t hyss_http_parser_execute(hyss_http_parser *parser,
                           const hyss_http_parser_settings *settings,
                           const char *data,
                           size_t len);


/* If hyss_http_should_keep_alive() in the on_headers_complete or
 * on_message_complete callback returns true, then this will be should be
 * the last message on the connection.
 * If you are the server, respond with the "Connection: close" header.
 * If you are the client, close the connection.
 */
int hyss_http_should_keep_alive(hyss_http_parser *parser);

/* Returns a string version of the HTTP method. */
const char *hyss_http_method_str(enum hyss_http_method);

#ifdef __cplusplus
}
#endif
#endif
