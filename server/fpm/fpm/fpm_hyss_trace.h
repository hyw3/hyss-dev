	/* (c) 2007,2008 Andrei Nigmatulin */

#ifndef FPM_HYSS_TRACE_H
#define FPM_HYSS_TRACE_H 1

struct fpm_child_s;

void fpm_hyss_trace(struct fpm_child_s *);

#endif
