	/* (c) 2007,2008 Andrei Nigmatulin */

#ifndef FPM_HYSS_H
#define FPM_HYSS_H 1

#include <hypbc.h>

#include "hyss.h"
#include "build-defs.h" /* for HYSS_ defines */
#include "fpm/fpm_conf.h"

#define FPM_HYSS_ICS_TO_EXPAND \
	{ \
		"error_log", \
		"extension_dir", \
		"mime_magic.magicfile", \
		"sendmail_path", \
		"session.cookie_path", \
		"session_pgsql.sem_file_name", \
		"soap.wsdl_cache_dir", \
		"uploadprogress.file.filename_template", \
		"xdebug.output_dir", \
		"xdebug.profiler_output_dir", \
		"xdebug.trace_output_dir", \
		"xmms.path", \
		"axis2.client_home", \
		"blenc.key_file", \
		"coin_acceptor.device", \
		NULL \
	}

struct fpm_worker_pool_s;

int fpm_hyss_init_child(struct fpm_worker_pool_s *wp);
char *fpm_hyss_script_filename(void);
char *fpm_hyss_request_uri(void);
char *fpm_hyss_request_method(void);
char *fpm_hyss_query_string(void);
char *fpm_hyss_auth_user(void);
size_t fpm_hyss_content_length(void);
void fpm_hyss_soft_quit();
int fpm_hyss_init_main();
int fpm_hyss_apply_defines_ex(struct key_value_s *kv, int mode);
int fpm_hyss_limit_extensions(char *path);
char* fpm_hyss_get_string_from_table(gear_string *table, char *key);

#endif
