fpm: $(SAPI_FPM_PATH)

$(SAPI_FPM_PATH): $(HYSS_GLOBAL_OBJS) $(HYSS_BINARY_OBJS) $(HYSS_FASTCGI_OBJS) $(HYSS_FPM_OBJS)
	$(BUILD_FPM)

install-fpm: $(SAPI_FPM_PATH)
	@echo "Installing HYSS FPM binary:        $(INSTALL_ROOT)$(sbindir)/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(sbindir)
	@$(mkinstalldirs) $(INSTALL_ROOT)$(localstatedir)/log
	@$(mkinstalldirs) $(INSTALL_ROOT)$(localstatedir)/run
	@$(INSTALL) -m 0755 $(SAPI_FPM_PATH) $(INSTALL_ROOT)$(sbindir)/$(program_prefix)hyss-fpm$(program_suffix)$(EXEEXT)

	@if test -f "$(INSTALL_ROOT)$(sysconfdir)/hyss-fpm.conf"; then \
		echo "Installing HYSS FPM defconfig:     skipping"; \
	else \
		echo "Installing HYSS FPM defconfig:     $(INSTALL_ROOT)$(sysconfdir)/" && \
		$(mkinstalldirs) $(INSTALL_ROOT)$(sysconfdir)/hyss-fpm.d; \
		$(INSTALL_DATA) server/fpm/hyss-fpm.conf $(INSTALL_ROOT)$(sysconfdir)/hyss-fpm.conf.default; \
		$(INSTALL_DATA) server/fpm/www.conf $(INSTALL_ROOT)$(sysconfdir)/hyss-fpm.d/www.conf.default; \
	fi

	@echo "Installing HYSS FPM man page:      $(INSTALL_ROOT)$(mandir)/man8/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(mandir)/man8
	@$(INSTALL_DATA) server/fpm/hyss-fpm.8 $(INSTALL_ROOT)$(mandir)/man8/hyss-fpm$(program_suffix).8

	@echo "Installing HYSS FPM status page:   $(INSTALL_ROOT)$(datadir)/fpm/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(datadir)/fpm
	@$(INSTALL_DATA) server/fpm/status.html $(INSTALL_ROOT)$(datadir)/fpm/status.html
