cgi: $(SAPI_CGI_PATH)

$(SAPI_CGI_PATH): $(HYSS_GLOBAL_OBJS) $(HYSS_BINARY_OBJS) $(HYSS_FASTCGI_OBJS) $(HYSS_CGI_OBJS)
	$(BUILD_CGI)

install-cgi: $(SAPI_CGI_PATH)
	@echo "Installing HYSS CGI binary:        $(INSTALL_ROOT)$(bindir)/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(bindir)
	@$(INSTALL) -m 0755 $(SAPI_CGI_PATH) $(INSTALL_ROOT)$(bindir)/$(program_prefix)hyss-cgi$(program_suffix)$(EXEEXT)
	@echo "Installing HYSS CGI man page:      $(INSTALL_ROOT)$(mandir)/man1/"
	@$(mkinstalldirs) $(INSTALL_ROOT)$(mandir)/man1
	@$(INSTALL_DATA) server/cgi/hyss-cgi.1 $(INSTALL_ROOT)$(mandir)/man1/$(program_prefix)hyss-cgi$(program_suffix).1
