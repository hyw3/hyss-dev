# HySS - Hyang Server Scripts
#
# Copyright (C) 2019-2020 Hyang Language Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

BEGIN {
	mode=0
	sources=""
}

mode == 0 && /^LTLIBRARY_SOURCES.*\\$/ {
	if (match($0, "[^=]*$")) {
	sources=substr($0, RSTART, RLENGTH-1)
	}
	mode=1
	next
}

mode == 0 && /^LTLIBRARY_SOURCES.*/ {
	if (match($0, "[^=]*$")) {
	sources=substr($0, RSTART, RLENGTH)
	}
}

mode == 1 && /.*\\$/ {
	sources=sources substr($0, 0, length - 1)
	next
}

mode == 1 {
	sources=sources $0
	mode=0
}

END {
	print sources
}
