# HySS - Hyang Server Scripts
#
# Copyright (C) 2019-2020 Hyang Language Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

BEGIN {
	orig_rs = RS;
	orig_fs = FS;
	RS=" ";
	capi_count = 0;
	SUBSEP=":";
}

function get_deps(capi_name, capi_dir,       depline, cmd)
{
	RS=orig_rs;
	FS="[(,) \t]+"
	cmd = "grep HYSS_ADD_EXTENSION_DEP " capi_dir "/config*.m4"
	while (cmd | getline) {
#		printf("GOT: %s,%s,%s,%s,%s\n", $1, $2, $3, $4, $5);
		if (!length($5)) {
			$5 = 0;
		}
		capi_deps[capi_name, $4] = $5;
	}
	close(cmd)
	RS=" ";
	FS=orig_fs;
}

function get_capi_index(name,  i)
{
	for (i in mods) {
		if (mods[i] == name) {
			return i;
		}
	}
	return -1;
}

function do_deps(capi_idx,        capi_name, capi_name_len, dep, ext, val, depidx)
{
	capi_name = mods[capi_idx];
	capi_name_len = length(capi_name);

	for (ext in capi_deps) {
		if (substr(ext, 0, capi_name_len+1) != capi_name SUBSEP) {
			continue;
		}
		val = capi_deps[ext];
		ext = substr(ext, capi_name_len+2, length(ext)-capi_name_len);

		depidx = get_capi_index(ext);
		if (depidx >= 0) {
			do_deps(depidx);
		}
	}
	printf("	hyssext_%s_ptr,@NEWLINE@", capi_name);
	delete mods[capi_idx];
}

function count(arr,       n, i)
{
	n = 0;
	for (i in arr)
		n++;
	return n;
}

/^[a-zA-Z0-9_;-]+/ {
	split($1, mod, ";");

	gsub("[^a-zA-Z0-9_-]", "", mod[1])
	mods[capi_count++] = mod[1]

	get_deps(mod[1], mod[2]);
}
END {
	out_count = 0;

	while (count(mods)) {
		for (i = 0; i <= capi_count - 1; i++) {
			if (i in mods) {
				do_deps(i);
			}
		}
	}
}
