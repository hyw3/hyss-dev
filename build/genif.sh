#! /bin/sh
# HySS - Hyang Server Scripts
#
# Copyright (C) 2019-2020 Hyang Language Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# replacement for genif.pl

infile=$1
shift
srcdir=$1
shift
extra_capi_ptrs=$1
shift
awk=$1
shift

if test -z "$infile" || test -z "$srcdir"; then
	echo "please supply infile and srcdir" >&2
	exit 1
fi

header_list=
olddir=`pwd`
cd $srcdir

capi_ptrs="$extra_capi_ptrs`echo $@ | $awk -f ./build/order_by_dep.awk`"

for ext in ${1+"$@"} ; do
	ext_dir=`echo "$ext" | cut -d ';' -f 2`
	header_list="$header_list $ext_dir/*.h*"
done

includes=`$awk -f ./build/print_include.awk $header_list`

cd $olddir

cat $infile | \
	sed \
	-e "s'@EXT_INCLUDE_CODE@'$includes'" \
	-e "s'@EXT_CAPI_PTRS@'$capi_ptrs'" \
	-e 's/@NEWLINE@/\
/g'
