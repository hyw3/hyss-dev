# HySS - Hyang Server Scripts
#
# Copyright (C) 2019-2020 Hyang Language Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
include generated_lists

TOUCH_FILES = mkinstalldirs install-sh missing

LT_TARGETS = ltmain.sh config.guess config.sub

config_h_in = main/hyss_config.h.in

targets = $(TOUCH_FILES) configure $(config_h_in)

HYSS_AUTOCONF ?= 'autoconf'
HYSS_AUTOHEADER ?= 'autoheader'

SUPPRESS_WARNINGS ?= 2>&1 | (egrep -v '(AC_PROG_CXXCPP was called before AC_PROG_CXX|defined in acinclude.m4 but never used)'||true)

all: $(targets)

$(config_h_in): configure
	@echo rebuilding $@
	@rm -f $@
	$(HYSS_AUTOHEADER) $(SUPPRESS_WARNINGS)

$(TOUCH_FILES):
	touch $(TOUCH_FILES)

aclocal.m4: configure.ac acinclude.m4
	@echo rebuilding $@
	cat acinclude.m4 ./build/libtool.m4 > $@

configure: aclocal.m4 configure.ac $(config_m4_files)
	@echo rebuilding $@
	@rm -f $@
	$(HYSS_AUTOCONF) -f $(SUPPRESS_WARNINGS)
