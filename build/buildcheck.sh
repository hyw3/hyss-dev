#! /bin/sh
# HySS - Hyang Server Scripts
#
# Copyright (C) 2019-2020 Hyang Language Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
echo "buildconf: checking installation..."

stamp=$1

# Allow the autoconf executable to be overridden by $HYSS_AUTOCONF.
if test -z "$HYSS_AUTOCONF"; then
  HYSS_AUTOCONF='autoconf'
fi

# autoconf 2.68 or newer
ac_version=`$HYSS_AUTOCONF --version 2>/dev/null|head -n 1|sed -e 's/^[^0-9]*//' -e 's/[a-z]* *$//'`
if test -z "$ac_version"; then
echo "buildconf: autoconf not found." >&2
echo "           You need autoconf version 2.68 or newer installed" >&2
echo "           to build HYSS from Git." >&2
exit 1
fi
IFS=.; set $ac_version; IFS=' '
if test "$1" = "2" -a "$2" -lt "68" || test "$1" -lt "2"; then
echo "buildconf: autoconf version $ac_version found." >&2
echo "           You need autoconf version 2.68 or newer installed" >&2
echo "           to build HYSS from Git." >&2
exit 1
else
echo "buildconf: autoconf version $ac_version (ok)"
fi

test -n "$stamp" && touch $stamp

exit 0
