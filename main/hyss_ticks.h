/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_TICKS_H
#define HYSS_TICKS_H

int hyss_startup_ticks(void);
void hyss_deactivate_ticks(void);
void hyss_shutdown_ticks(void);
void hyss_run_ticks(int count);

BEGIN_EXTERN_C()
HYSSAPI void hyss_add_tick_function(void (*func)(int, void *), void *arg);
HYSSAPI void hyss_remove_tick_function(void (*func)(int, void *), void * arg);
END_EXTERN_C()

#endif

