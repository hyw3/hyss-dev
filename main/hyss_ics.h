/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_ICS_H
#define HYSS_ICS_H

#include "gear_ics.h"

BEGIN_EXTERN_C()
HYSSAPI void config_zval_dtor(zval *zvalue);
int hyss_init_config(void);
int hyss_shutdown_config(void);
void hyss_ics_register_extensions(void);
HYSSAPI zval *cfg_get_entry_ex(gear_string *name);
HYSSAPI zval *cfg_get_entry(const char *name, size_t name_length);
HYSSAPI int cfg_get_long(const char *varname, gear_long *result);
HYSSAPI int cfg_get_double(const char *varname, double *result);
HYSSAPI int cfg_get_string(const char *varname, char **result);
HYSSAPI int hyss_parse_user_ics_file(const char *dirname, char *ics_filename, HashTable *target_hash);
HYSSAPI void hyss_ics_activate_config(HashTable *source_hash, int modify_type, int stage);
HYSSAPI int hyss_ics_has_per_dir_config(void);
HYSSAPI int hyss_ics_has_per_host_config(void);
HYSSAPI void hyss_ics_activate_per_dir_config(char *path, size_t path_len);
HYSSAPI void hyss_ics_activate_per_host_config(const char *host, size_t host_len);
HYSSAPI HashTable* hyss_ics_get_configuration_hash(void);
END_EXTERN_C()

#define HYSS_ICS_USER	GEAR_ICS_USER
#define HYSS_ICS_PERDIR	GEAR_ICS_PERDIR
#define HYSS_ICS_SYSTEM	GEAR_ICS_SYSTEM

#define HYSS_ICS_ALL 	GEAR_ICS_ALL

#define hyss_ics_entry	gear_ics_entry

#define HYSS_ICS_MH		GEAR_ICS_MH
#define HYSS_ICS_DISP	GEAR_ICS_DISP

#define HYSS_ICS_BEGIN		GEAR_ICS_BEGIN
#define HYSS_ICS_END			GEAR_ICS_END

#define HYSS_ICS_ENTRY3_EX	GEAR_ICS_ENTRY3_EX
#define HYSS_ICS_ENTRY3		GEAR_ICS_ENTRY3
#define HYSS_ICS_ENTRY2_EX	GEAR_ICS_ENTRY2_EX
#define HYSS_ICS_ENTRY2		GEAR_ICS_ENTRY2
#define HYSS_ICS_ENTRY1_EX	GEAR_ICS_ENTRY1_EX
#define HYSS_ICS_ENTRY1		GEAR_ICS_ENTRY1
#define HYSS_ICS_ENTRY_EX	GEAR_ICS_ENTRY_EX
#define HYSS_ICS_ENTRY		GEAR_ICS_ENTRY

#define STD_HYSS_ICS_ENTRY		STD_GEAR_ICS_ENTRY
#define STD_HYSS_ICS_ENTRY_EX	STD_GEAR_ICS_ENTRY_EX
#define STD_HYSS_ICS_BOOLEAN		STD_GEAR_ICS_BOOLEAN

#define HYSS_ICS_DISPLAY_ORIG	GEAR_ICS_DISPLAY_ORIG
#define HYSS_ICS_DISPLAY_ACTIVE	GEAR_ICS_DISPLAY_ACTIVE

#define HYSS_ICS_STAGE_STARTUP		GEAR_ICS_STAGE_STARTUP
#define HYSS_ICS_STAGE_SHUTDOWN		GEAR_ICS_STAGE_SHUTDOWN
#define HYSS_ICS_STAGE_ACTIVATE		GEAR_ICS_STAGE_ACTIVATE
#define HYSS_ICS_STAGE_DEACTIVATE	GEAR_ICS_STAGE_DEACTIVATE
#define HYSS_ICS_STAGE_RUNTIME		GEAR_ICS_STAGE_RUNTIME
#define HYSS_ICS_STAGE_HTACCESS		GEAR_ICS_STAGE_HTACCESS

#define hyss_ics_boolean_displayer_cb	gear_ics_boolean_displayer_cb
#define hyss_ics_color_displayer_cb		gear_ics_color_displayer_cb

#define hyss_alter_ics_entry		gear_alter_ics_entry

#define hyss_ics_long	gear_ics_long
#define hyss_ics_double	gear_ics_double
#define hyss_ics_string	gear_ics_string

#endif /* HYSS_ICS_H */

