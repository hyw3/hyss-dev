/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HYSS_NETWORK_H
#define _HYSS_NETWORK_H

#include <hyss.h>

#ifdef HYSS_WIN32
# include "win32/inet.h"
#else
# undef closesocket
# define closesocket close
# include <netinet/tcp.h>
#endif

#ifndef HAVE_SHUTDOWN
#undef shutdown
#define shutdown(s,n)	/* nothing */
#endif

#ifdef HYSS_WIN32
# ifdef EWOULDBLOCK
#  undef EWOULDBLOCK
# endif
# ifdef EINPROGRESS
#  undef EINPROGRESS
# endif
# define EWOULDBLOCK WSAEWOULDBLOCK
# define EINPROGRESS	WSAEWOULDBLOCK
# define fsync _commit
# define ftruncate(a, b) chsize(a, b)
#endif /* defined(HYSS_WIN32) */

#ifndef EWOULDBLOCK
# define EWOULDBLOCK EAGAIN
#endif

#ifdef HYSS_WIN32
#define hyss_socket_errno() WSAGetLastError()
#else
#define hyss_socket_errno() errno
#endif

/* like strerror, but caller must efree the returned string,
 * unless buf is not NULL.
 * Also works sensibly for win32 */
BEGIN_EXTERN_C()
HYSSAPI char *hyss_socket_strerror(long err, char *buf, size_t bufsize);
HYSSAPI gear_string *hyss_socket_error_str(long err);
END_EXTERN_C()

#ifdef HAVE_NETINET_IN_H
# include <netinet/in.h>
#endif

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#ifdef HAVE_GETHOSTBYNAME_R
#include <netdb.h>
#endif

/* These are here, rather than with the win32 counterparts above,
 * since <sys/socket.h> defines them. */
#ifndef SHUT_RD
# define SHUT_RD 0
# define SHUT_WR 1
# define SHUT_RDWR 2
#endif

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#ifdef HAVE_STDDEF_H
#include <stddef.h>
#endif

#ifdef HYSS_WIN32
typedef SOCKET hyss_socket_t;
#else
typedef int hyss_socket_t;
#endif

#ifdef HYSS_WIN32
# define SOCK_ERR INVALID_SOCKET
# define SOCK_CONN_ERR SOCKET_ERROR
# define SOCK_RECV_ERR SOCKET_ERROR
#else
# define SOCK_ERR -1
# define SOCK_CONN_ERR -1
# define SOCK_RECV_ERR -1
#endif

#define STREAM_SOCKOP_NONE                (1 << 0)
#define STREAM_SOCKOP_SO_REUSEPORT        (1 << 1)
#define STREAM_SOCKOP_SO_BROADCAST        (1 << 2)
#define STREAM_SOCKOP_IPV6_V6ONLY         (1 << 3)
#define STREAM_SOCKOP_IPV6_V6ONLY_ENABLED (1 << 4)
#define STREAM_SOCKOP_TCP_NODELAY         (1 << 5)


/* uncomment this to debug poll(2) emulation on systems that have poll(2) */
/* #define HYSS_USE_POLL_2_EMULATION 1 */

#if defined(HAVE_POLL)
# if defined(HAVE_POLL_H)
#  include <poll.h>
# elif defined(HAVE_SYS_POLL_H)
#  include <sys/poll.h>
# endif
typedef struct pollfd hyss_pollfd;
#else
typedef struct _hyss_pollfd {
	hyss_socket_t fd;
	short events;
	short revents;
} hyss_pollfd;

HYSSAPI int hyss_poll2(hyss_pollfd *ufds, unsigned int nfds, int timeout);

#ifndef POLLIN
# define POLLIN      0x0001    /* There is data to read */
# define POLLPRI     0x0002    /* There is urgent data to read */
# define POLLOUT     0x0004    /* Writing now will not block */
# define POLLERR     0x0008    /* Error condition */
# define POLLHUP     0x0010    /* Hung up */
# define POLLNVAL    0x0020    /* Invalid request: fd not open */
#endif

# ifndef HYSS_USE_POLL_2_EMULATION
#  define HYSS_USE_POLL_2_EMULATION 1
# endif
#endif

#define HYSS_POLLREADABLE	(POLLIN|POLLERR|POLLHUP)

#ifndef HYSS_USE_POLL_2_EMULATION
# define hyss_poll2(ufds, nfds, timeout)		poll(ufds, nfds, timeout)
#endif

/* timeval-to-timeout (for poll(2)) */
static inline int hyss_tvtoto(struct timeval *timeouttv)
{
	if (timeouttv) {
		return (timeouttv->tv_sec * 1000) + (timeouttv->tv_usec / 1000);
	}
	return -1;
}

/* hybrid select(2)/poll(2) for a single descriptor.
 * timeouttv follows same rules as select(2), but is reduced to millisecond accuracy.
 * Returns 0 on timeout, -1 on error, or the event mask (ala poll(2)).
 */
static inline int hyss_pollfd_for(hyss_socket_t fd, int events, struct timeval *timeouttv)
{
	hyss_pollfd p;
	int n;

	p.fd = fd;
	p.events = events;
	p.revents = 0;

	n = hyss_poll2(&p, 1, hyss_tvtoto(timeouttv));

	if (n > 0) {
		return p.revents;
	}

	return n;
}

static inline int hyss_pollfd_for_ms(hyss_socket_t fd, int events, int timeout)
{
	hyss_pollfd p;
	int n;

	p.fd = fd;
	p.events = events;
	p.revents = 0;

	n = hyss_poll2(&p, 1, timeout);

	if (n > 0) {
		return p.revents;
	}

	return n;
}

/* emit warning and suggestion for unsafe select(2) usage */
HYSSAPI void _hyss_emit_fd_setsize_warning(int max_fd);

#ifdef HYSS_WIN32
/* it is safe to FD_SET too many fd's under win32; the macro will simply ignore
 * descriptors that go beyond the default FD_SETSIZE */
# define HYSS_SAFE_FD_SET(fd, set)	FD_SET(fd, set)
# define HYSS_SAFE_FD_CLR(fd, set)	FD_CLR(fd, set)
# define HYSS_SAFE_FD_ISSET(fd, set)	FD_ISSET(fd, set)
# define HYSS_SAFE_MAX_FD(m, n)		do { if (n + 1 >= FD_SETSIZE) { _hyss_emit_fd_setsize_warning(n); }} while(0)
#else
# define HYSS_SAFE_FD_SET(fd, set)	do { if (fd < FD_SETSIZE) FD_SET(fd, set); } while(0)
# define HYSS_SAFE_FD_CLR(fd, set)	do { if (fd < FD_SETSIZE) FD_CLR(fd, set); } while(0)
# define HYSS_SAFE_FD_ISSET(fd, set)	((fd < FD_SETSIZE) && FD_ISSET(fd, set))
# define HYSS_SAFE_MAX_FD(m, n)		do { if (m >= FD_SETSIZE) { _hyss_emit_fd_setsize_warning(m); m = FD_SETSIZE - 1; }} while(0)
#endif


#define HYSS_SOCK_CHUNK_SIZE	8192

#ifdef HAVE_SOCKADDR_STORAGE
typedef struct sockaddr_storage hyss_sockaddr_storage;
#else
typedef struct {
#ifdef HAVE_SOCKADDR_SA_LEN
		unsigned char ss_len;
		unsigned char ss_family;
#else
        unsigned short ss_family;
#endif
        char info[126];
} hyss_sockaddr_storage;
#endif

BEGIN_EXTERN_C()
HYSSAPI int hyss_network_getaddresses(const char *host, int socktype, struct sockaddr ***sal, gear_string **error_string);
HYSSAPI void hyss_network_freeaddresses(struct sockaddr **sal);

HYSSAPI hyss_socket_t hyss_network_connect_socket_to_host(const char *host, unsigned short port,
		int socktype, int asynchronous, struct timeval *timeout, gear_string **error_string,
		int *error_code, char *bindto, unsigned short bindport, long sockopts
		);

HYSSAPI int hyss_network_connect_socket(hyss_socket_t sockfd,
		const struct sockaddr *addr,
		socklen_t addrlen,
		int asynchronous,
		struct timeval *timeout,
		gear_string **error_string,
		int *error_code);

#define hyss_connect_nonb(sock, addr, addrlen, timeout) \
	hyss_network_connect_socket((sock), (addr), (addrlen), 0, (timeout), NULL, NULL)

HYSSAPI hyss_socket_t hyss_network_bind_socket_to_local_addr(const char *host, unsigned port,
		int socktype, long sockopts, gear_string **error_string, int *error_code
		);

HYSSAPI hyss_socket_t hyss_network_accept_incoming(hyss_socket_t srvsock,
		gear_string **textaddr,
		struct sockaddr **addr,
		socklen_t *addrlen,
		struct timeval *timeout,
		gear_string **error_string,
		int *error_code,
		int tcp_nodelay
		);

HYSSAPI int hyss_network_get_sock_name(hyss_socket_t sock,
		gear_string **textaddr,
		struct sockaddr **addr,
		socklen_t *addrlen
		);

HYSSAPI int hyss_network_get_peer_name(hyss_socket_t sock,
		gear_string **textaddr,
		struct sockaddr **addr,
		socklen_t *addrlen
		);

HYSSAPI void hyss_any_addr(int family, hyss_sockaddr_storage *addr, unsigned short port);
HYSSAPI int hyss_sockaddr_size(hyss_sockaddr_storage *addr);
END_EXTERN_C()

struct _hyss_netstream_data_t	{
	hyss_socket_t socket;
	char is_blocked;
	struct timeval timeout;
	char timeout_event;
	size_t ownsize;
};
typedef struct _hyss_netstream_data_t hyss_netstream_data_t;
HYSSAPI extern const hyss_stream_ops hyss_stream_socket_ops;
extern const hyss_stream_ops hyss_stream_generic_socket_ops;
#define HYSS_STREAM_IS_SOCKET	(&hyss_stream_socket_ops)

BEGIN_EXTERN_C()
HYSSAPI hyss_stream *_hyss_stream_sock_open_from_socket(hyss_socket_t socket, const char *persistent_id STREAMS_DC );
/* open a connection to a host using hyss_hostconnect and return a stream */
HYSSAPI hyss_stream *_hyss_stream_sock_open_host(const char *host, unsigned short port,
		int socktype, struct timeval *timeout, const char *persistent_id STREAMS_DC);
HYSSAPI void hyss_network_populate_name_from_sockaddr(
		/* input address */
		struct sockaddr *sa, socklen_t sl,
		/* output readable address */
		gear_string **textaddr,
		/* output address */
		struct sockaddr **addr,
		socklen_t *addrlen
		);

HYSSAPI int hyss_network_parse_network_address_with_port(const char *addr,
		gear_long addrlen, struct sockaddr *sa, socklen_t *sl);

HYSSAPI struct hostent*	hyss_network_gethostbyname(char *name);

HYSSAPI int hyss_set_sock_blocking(hyss_socket_t socketd, int block);
END_EXTERN_C()

#define hyss_stream_sock_open_from_socket(socket, persistent)	_hyss_stream_sock_open_from_socket((socket), (persistent) STREAMS_CC)
#define hyss_stream_sock_open_host(host, port, socktype, timeout, persistent)	_hyss_stream_sock_open_host((host), (port), (socktype), (timeout), (persistent) STREAMS_CC)

/* {{{ memory debug */
#define hyss_stream_sock_open_from_socket_rel(socket, persistent)	_hyss_stream_sock_open_from_socket((socket), (persistent) STREAMS_REL_CC)
#define hyss_stream_sock_open_host_rel(host, port, socktype, timeout, persistent)	_hyss_stream_sock_open_host((host), (port), (socktype), (timeout), (persistent) STREAMS_REL_CC)
#define hyss_stream_sock_open_unix_rel(path, pathlen, persistent, timeval)	_hyss_stream_sock_open_unix((path), (pathlen), (persistent), (timeval) STREAMS_REL_CC)

/* }}} */

#ifndef MAXFQDNLEN
#define MAXFQDNLEN 255
#endif

#endif /* _HYSS_NETWORK_H */

