/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_ticks.h"

struct st_tick_function
{
	void (*func)(int, void *);
	void *arg;
};

int hyss_startup_ticks(void)
{
	gear_llist_init(&PG(tick_functions), sizeof(struct st_tick_function), NULL, 1);
	return SUCCESS;
}

void hyss_deactivate_ticks(void)
{
	gear_llist_clean(&PG(tick_functions));
}

void hyss_shutdown_ticks(void)
{
	gear_llist_destroy(&PG(tick_functions));
}

static int hyss_compare_tick_functions(void *elem1, void *elem2)
{
  struct st_tick_function *e1 = (struct st_tick_function *)elem1;
  struct st_tick_function *e2 = (struct st_tick_function *)elem2;
  return e1->func == e2->func && e1->arg == e2->arg;
}

HYSSAPI void hyss_add_tick_function(void (*func)(int, void*), void * arg)
{
	struct st_tick_function tmp = {func, arg};
	gear_llist_add_element(&PG(tick_functions), (void *)&tmp);
}

HYSSAPI void hyss_remove_tick_function(void (*func)(int, void *), void * arg)
{
	struct st_tick_function tmp = {func, arg};
	gear_llist_del_element(&PG(tick_functions), (void *)&tmp, (int(*)(void*, void*))hyss_compare_tick_functions);
}

static void hyss_tick_iterator(void *d, void *arg)
{
	struct st_tick_function *data = (struct st_tick_function *)d;
	data->func(*((int *)arg), data->arg);
}

void hyss_run_ticks(int count)
{
	gear_llist_apply_with_argument(&PG(tick_functions), (llist_apply_with_arg_func_t) hyss_tick_iterator, &count);
}

