/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_OUTPUT_H
#define HYSS_OUTPUT_H

#define HYSS_OUTPUT_NEWAPI 1

/* handler ops */
#define HYSS_OUTPUT_HANDLER_WRITE	0x00	/* standard passthru */
#define HYSS_OUTPUT_HANDLER_START	0x01	/* start */
#define HYSS_OUTPUT_HANDLER_CLEAN	0x02	/* restart */
#define HYSS_OUTPUT_HANDLER_FLUSH	0x04	/* pass along as much as possible */
#define HYSS_OUTPUT_HANDLER_FINAL	0x08	/* finalize */
#define HYSS_OUTPUT_HANDLER_CONT		HYSS_OUTPUT_HANDLER_WRITE
#define HYSS_OUTPUT_HANDLER_END		HYSS_OUTPUT_HANDLER_FINAL

/* handler types */
#define HYSS_OUTPUT_HANDLER_INTERNAL		0x0000
#define HYSS_OUTPUT_HANDLER_USER			0x0001

/* handler ability flags */
#define HYSS_OUTPUT_HANDLER_CLEANABLE	0x0010
#define HYSS_OUTPUT_HANDLER_FLUSHABLE	0x0020
#define HYSS_OUTPUT_HANDLER_REMOVABLE	0x0040
#define HYSS_OUTPUT_HANDLER_STDFLAGS		0x0070

/* handler status flags */
#define HYSS_OUTPUT_HANDLER_STARTED		0x1000
#define HYSS_OUTPUT_HANDLER_DISABLED		0x2000
#define HYSS_OUTPUT_HANDLER_PROCESSED	0x4000

/* handler op return values */
typedef enum _hyss_output_handler_status_t {
	HYSS_OUTPUT_HANDLER_FAILURE,
	HYSS_OUTPUT_HANDLER_SUCCESS,
	HYSS_OUTPUT_HANDLER_NO_DATA
} hyss_output_handler_status_t;

/* hyss_output_stack_pop() flags */
#define HYSS_OUTPUT_POP_TRY			0x000
#define HYSS_OUTPUT_POP_FORCE		0x001
#define HYSS_OUTPUT_POP_DISCARD		0x010
#define HYSS_OUTPUT_POP_SILENT		0x100

/* real global flags */
#define HYSS_OUTPUT_IMPLICITFLUSH		0x01
#define HYSS_OUTPUT_DISABLED				0x02
#define HYSS_OUTPUT_WRITTEN				0x04
#define HYSS_OUTPUT_SENT					0x08
/* supplementary flags for hyss_output_get_status() */
#define HYSS_OUTPUT_ACTIVE				0x10
#define HYSS_OUTPUT_LOCKED				0x20
/* output layer is ready to use */
#define HYSS_OUTPUT_ACTIVATED		0x100000

/* handler hooks */
typedef enum _hyss_output_handler_hook_t {
	HYSS_OUTPUT_HANDLER_HOOK_GET_OPAQ,
	HYSS_OUTPUT_HANDLER_HOOK_GET_FLAGS,
	HYSS_OUTPUT_HANDLER_HOOK_GET_LEVEL,
	HYSS_OUTPUT_HANDLER_HOOK_IMMUTABLE,
	HYSS_OUTPUT_HANDLER_HOOK_DISABLE,
	/* unused */
	HYSS_OUTPUT_HANDLER_HOOK_LAST
} hyss_output_handler_hook_t;

#define HYSS_OUTPUT_HANDLER_INITBUF_SIZE(s) \
( ((s) > 1) ? \
	(s) + HYSS_OUTPUT_HANDLER_ALIGNTO_SIZE - ((s) % (HYSS_OUTPUT_HANDLER_ALIGNTO_SIZE)) : \
	HYSS_OUTPUT_HANDLER_DEFAULT_SIZE \
)
#define HYSS_OUTPUT_HANDLER_ALIGNTO_SIZE		0x1000
#define HYSS_OUTPUT_HANDLER_DEFAULT_SIZE		0x4000

typedef struct _hyss_output_buffer {
	char *data;
	size_t size;
	size_t used;
	uint32_t free:1;
	uint32_t _reserved:31;
} hyss_output_buffer;

typedef struct _hyss_output_context {
	int op;
	hyss_output_buffer in;
	hyss_output_buffer out;
} hyss_output_context;

/* old-style, stateless callback */
typedef void (*hyss_output_handler_func_t)(char *output, size_t output_len, char **handled_output, size_t *handled_output_len, int mode);
/* new-style, opaque context callback */
typedef int (*hyss_output_handler_context_func_t)(void **handler_context, hyss_output_context *output_context);
/* output handler context dtor */
typedef void (*hyss_output_handler_context_dtor_t)(void *opaq);
/* conflict check callback */
typedef int (*hyss_output_handler_conflict_check_t)(const char *handler_name, size_t handler_name_len);
/* ctor for aliases */
typedef struct _hyss_output_handler *(*hyss_output_handler_alias_ctor_t)(const char *handler_name, size_t handler_name_len, size_t chunk_size, int flags);

typedef struct _hyss_output_handler_user_func_t {
	gear_fcall_info fci;
	gear_fcall_info_cache fcc;
	zval zoh;
} hyss_output_handler_user_func_t;

typedef struct _hyss_output_handler {
	gear_string *name;
	int flags;
	int level;
	size_t size;
	hyss_output_buffer buffer;

	void *opaq;
	void (*dtor)(void *opaq);

	union {
		hyss_output_handler_user_func_t *user;
		hyss_output_handler_context_func_t internal;
	} func;
} hyss_output_handler;

GEAR_BEGIN_CAPI_GLOBALS(output)
	gear_stack handlers;
	hyss_output_handler *active;
	hyss_output_handler *running;
	const char *output_start_filename;
	int output_start_lineno;
	int flags;
GEAR_END_CAPI_GLOBALS(output)

HYSSAPI GEAR_EXTERN_CAPI_GLOBALS(output)

/* there should not be a need to use OG() from outside of output.c */
#ifdef ZTS
# define OG(v) GEAR_PBCG(output_globals_id, gear_output_globals *, v)
#else
# define OG(v) (output_globals.v)
#endif

/* convenience macros */
#define HYSSWRITE(str, str_len)		hyss_output_write((str), (str_len))
#define HYSSWRITE_H(str, str_len)	hyss_output_write_unbuffered((str), (str_len))

#define PUTC(c)						hyss_output_write((const char *) &(c), 1)
#define PUTC_H(c)					hyss_output_write_unbuffered((const char *) &(c), 1)

#define PUTS(str)					do {				\
	const char *__str = (str);							\
	hyss_output_write(__str, strlen(__str));	\
} while (0)
#define PUTS_H(str)					do {							\
	const char *__str = (str);										\
	hyss_output_write_unbuffered(__str, strlen(__str));	\
} while (0)


BEGIN_EXTERN_C()

extern const char hyss_output_default_handler_name[sizeof("default output handler")];
extern const char hyss_output_devnull_handler_name[sizeof("null output handler")];

#define hyss_output_tearup() \
	hyss_output_startup(); \
	hyss_output_activate()
#define hyss_output_teardown() \
	hyss_output_end_all(); \
	hyss_output_deactivate(); \
	hyss_output_shutdown()

/* MINIT */
HYSSAPI void hyss_output_startup(void);
/* MSHUTDOWN */
HYSSAPI void hyss_output_shutdown(void);

HYSSAPI void hyss_output_register_constants(void);

/* RINIT */
HYSSAPI int hyss_output_activate(void);
/* RSHUTDOWN */
HYSSAPI void hyss_output_deactivate(void);

HYSSAPI void hyss_output_set_status(int status);
HYSSAPI int hyss_output_get_status(void);
HYSSAPI void hyss_output_set_implicit_flush(int flush);
HYSSAPI const char *hyss_output_get_start_filename(void);
HYSSAPI int hyss_output_get_start_lineno(void);

HYSSAPI size_t hyss_output_write_unbuffered(const char *str, size_t len);
HYSSAPI size_t hyss_output_write(const char *str, size_t len);

HYSSAPI int hyss_output_flush(void);
HYSSAPI void hyss_output_flush_all(void);
HYSSAPI int hyss_output_clean(void);
HYSSAPI void hyss_output_clean_all(void);
HYSSAPI int hyss_output_end(void);
HYSSAPI void hyss_output_end_all(void);
HYSSAPI int hyss_output_discard(void);
HYSSAPI void hyss_output_discard_all(void);

HYSSAPI int hyss_output_get_contents(zval *p);
HYSSAPI int hyss_output_get_length(zval *p);
HYSSAPI int hyss_output_get_level(void);
HYSSAPI hyss_output_handler* hyss_output_get_active_handler(void);

HYSSAPI int hyss_output_start_default(void);
HYSSAPI int hyss_output_start_devnull(void);

HYSSAPI int hyss_output_start_user(zval *output_handler, size_t chunk_size, int flags);
HYSSAPI int hyss_output_start_internal(const char *name, size_t name_len, hyss_output_handler_func_t output_handler, size_t chunk_size, int flags);

HYSSAPI hyss_output_handler *hyss_output_handler_create_user(zval *handler, size_t chunk_size, int flags);
HYSSAPI hyss_output_handler *hyss_output_handler_create_internal(const char *name, size_t name_len, hyss_output_handler_context_func_t handler, size_t chunk_size, int flags);

HYSSAPI void hyss_output_handler_set_context(hyss_output_handler *handler, void *opaq, void (*dtor)(void*));
HYSSAPI int hyss_output_handler_start(hyss_output_handler *handler);
HYSSAPI int hyss_output_handler_started(const char *name, size_t name_len);
HYSSAPI int hyss_output_handler_hook(hyss_output_handler_hook_t type, void *arg);
HYSSAPI void hyss_output_handler_dtor(hyss_output_handler *handler);
HYSSAPI void hyss_output_handler_free(hyss_output_handler **handler);

HYSSAPI int hyss_output_handler_conflict(const char *handler_new, size_t handler_new_len, const char *handler_set, size_t handler_set_len);
HYSSAPI int hyss_output_handler_conflict_register(const char *handler_name, size_t handler_name_len, hyss_output_handler_conflict_check_t check_func);
HYSSAPI int hyss_output_handler_reverse_conflict_register(const char *handler_name, size_t handler_name_len, hyss_output_handler_conflict_check_t check_func);

HYSSAPI hyss_output_handler_alias_ctor_t hyss_output_handler_alias(const char *handler_name, size_t handler_name_len);
HYSSAPI int hyss_output_handler_alias_register(const char *handler_name, size_t handler_name_len, hyss_output_handler_alias_ctor_t func);

END_EXTERN_C()


HYSS_FUNCTION(ob_start);
HYSS_FUNCTION(ob_flush);
HYSS_FUNCTION(ob_clean);
HYSS_FUNCTION(ob_end_flush);
HYSS_FUNCTION(ob_end_clean);
HYSS_FUNCTION(ob_get_flush);
HYSS_FUNCTION(ob_get_clean);
HYSS_FUNCTION(ob_get_contents);
HYSS_FUNCTION(ob_get_length);
HYSS_FUNCTION(ob_get_level);
HYSS_FUNCTION(ob_get_status);
HYSS_FUNCTION(ob_implicit_flush);
HYSS_FUNCTION(ob_list_handlers);

HYSS_FUNCTION(output_add_rewrite_var);
HYSS_FUNCTION(output_reset_rewrite_vars);

#endif

