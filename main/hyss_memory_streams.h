/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_MEMORY_STREAM_H
#define HYSS_MEMORY_STREAM_H

#include "hyss_streams.h"

#define HYSS_STREAM_MAX_MEM	2 * 1024 * 1024

#define TEMP_STREAM_DEFAULT     0x0
#define TEMP_STREAM_READONLY    0x1
#define TEMP_STREAM_TAKE_BUFFER 0x2
#define TEMP_STREAM_APPEND      0x4

#define hyss_stream_memory_create(mode) _hyss_stream_memory_create((mode) STREAMS_CC)
#define hyss_stream_memory_create_rel(mode) _hyss_stream_memory_create((mode) STREAMS_REL_CC)
#define hyss_stream_memory_open(mode, buf, length) _hyss_stream_memory_open((mode), (buf), (length) STREAMS_CC)
#define hyss_stream_memory_get_buffer(stream, length) _hyss_stream_memory_get_buffer((stream), (length) STREAMS_CC)

#define hyss_stream_temp_new() hyss_stream_temp_create(TEMP_STREAM_DEFAULT, HYSS_STREAM_MAX_MEM)
#define hyss_stream_temp_create(mode, max_memory_usage) _hyss_stream_temp_create((mode), (max_memory_usage) STREAMS_CC)
#define hyss_stream_temp_create_ex(mode, max_memory_usage, tmpdir) _hyss_stream_temp_create_ex((mode), (max_memory_usage), (tmpdir) STREAMS_CC)
#define hyss_stream_temp_create_rel(mode, max_memory_usage) _hyss_stream_temp_create((mode), (max_memory_usage) STREAMS_REL_CC)
#define hyss_stream_temp_open(mode, max_memory_usage, buf, length) _hyss_stream_temp_open((mode), (max_memory_usage), (buf), (length) STREAMS_CC)

BEGIN_EXTERN_C()

HYSSAPI hyss_stream *_hyss_stream_memory_create(int mode STREAMS_DC);
HYSSAPI hyss_stream *_hyss_stream_memory_open(int mode, char *buf, size_t length STREAMS_DC);
HYSSAPI char *_hyss_stream_memory_get_buffer(hyss_stream *stream, size_t *length STREAMS_DC);

HYSSAPI hyss_stream *_hyss_stream_temp_create(int mode, size_t max_memory_usage STREAMS_DC);
HYSSAPI hyss_stream *_hyss_stream_temp_create_ex(int mode, size_t max_memory_usage, const char *tmpdir STREAMS_DC);
HYSSAPI hyss_stream *_hyss_stream_temp_open(int mode, size_t max_memory_usage, char *buf, size_t length STREAMS_DC);

HYSSAPI int hyss_stream_mode_from_str(const char *mode);
HYSSAPI const char *_hyss_stream_mode_to_str(int mode);

END_EXTERN_C()

extern HYSSAPI const hyss_stream_ops hyss_stream_memory_ops;
extern HYSSAPI const hyss_stream_ops hyss_stream_temp_ops;
extern HYSSAPI const hyss_stream_ops hyss_stream_rfc2397_ops;
extern HYSSAPI const hyss_stream_wrapper hyss_stream_rfc2397_wrapper;

#define HYSS_STREAM_IS_MEMORY &hyss_stream_memory_ops
#define HYSS_STREAM_IS_TEMP   &hyss_stream_temp_ops

#endif

