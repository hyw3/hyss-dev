/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"

#ifndef HAVE_EXPLICIT_BZERO

#include <string.h>

HYSSAPI void hyss_explicit_bzero(void *dst, size_t siz)
{
#if HAVE_EXPLICIT_MEMSET
    explicit_memset(dst, 0, siz);
#elif defined(HYSS_WIN32)
	RtlSecureZeroMemory(dst, siz);
#elif defined(__GNUC__)
	memset(dst, 0, siz);
	asm __volatile__("" :: "r"(dst) : "memory");
#else
	size_t i = 0;
	volatile unsigned char *buf = (volatile unsigned char *)dst;

	for (; i < siz; i ++)
		buf[i] = 0;
#endif
}
#endif

