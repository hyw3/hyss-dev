/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_GLOBALS_H
#define HYSS_GLOBALS_H

#include "gear_globals.h"

typedef struct _hyss_core_globals hyss_core_globals;

#ifdef ZTS
# define PG(v) GEAR_PBCG(core_globals_id, hyss_core_globals *, v)
extern HYSSAPI int core_globals_id;
#else
# define PG(v) (core_globals.v)
extern GEAR_API struct _hyss_core_globals core_globals;
#endif

/* Error display modes */
#define HYSS_DISPLAY_ERRORS_STDOUT	1
#define HYSS_DISPLAY_ERRORS_STDERR	2

/* Track vars */
#define TRACK_VARS_POST		0
#define TRACK_VARS_GET		1
#define TRACK_VARS_COOKIE	2
#define TRACK_VARS_SERVER	3
#define TRACK_VARS_ENV		4
#define TRACK_VARS_FILES	5
#define TRACK_VARS_REQUEST	6

struct _hyss_tick_function_entry;

typedef struct _arg_separators {
	char *output;
	char *input;
} arg_separators;

struct _hyss_core_globals {
	gear_bool implicit_flush;

	gear_long output_buffering;

	gear_bool enable_dl;

	char *output_handler;

	char *unserialize_callback_func;
	gear_long serialize_precision;

	gear_long memory_limit;
	gear_long max_input_time;

	gear_bool track_errors;
	gear_bool display_errors;
	gear_bool display_startup_errors;
	gear_bool log_errors;
	gear_long      log_errors_max_len;
	gear_bool ignore_repeated_errors;
	gear_bool ignore_repeated_source;
	gear_bool report_memleaks;
	char *error_log;

	char *doc_root;
	char *user_dir;
	char *include_path;
	char *open_basedir;
	char *extension_dir;
	char *hyss_binary;
	char *sys_temp_dir;

	char *upload_tmp_dir;
	gear_long upload_max_filesize;

	char *error_append_string;
	char *error_prepend_string;

	char *auto_prepend_file;
	char *auto_append_file;

	char *input_encoding;
	char *internal_encoding;
	char *output_encoding;

	arg_separators arg_separator;

	char *variables_order;

	HashTable rfc1867_protected_variables;

	short connection_status;
	gear_bool ignore_user_abort;

	unsigned char header_is_being_sent;

	gear_llist tick_functions;

	zval http_globals[6];

	gear_bool expose_hyss;

	gear_bool register_argc_argv;
	gear_bool auto_globals_jit;

	char *docref_root;
	char *docref_ext;

	gear_bool html_errors;
	gear_bool xmlrpc_errors;

	gear_long xmlrpc_error_number;

	gear_bool activated_auto_globals[8];

	gear_bool cAPIs_activated;
	gear_bool file_uploads;
	gear_bool during_request_startup;
	gear_bool allow_url_fopen;
	gear_bool enable_post_data_reading;
	gear_bool report_gear_debug;

	int last_error_type;
	char *last_error_message;
	char *last_error_file;
	int  last_error_lineno;

	char *hyss_sys_temp_dir;

	char *disable_functions;
	char *disable_classes;
	gear_bool allow_url_include;
#ifdef HYSS_WIN32
	gear_bool com_initialized;
#endif
	gear_long max_input_nesting_level;
	gear_long max_input_vars;
	gear_bool in_user_include;

	char *user_ics_filename;
	gear_long user_ics_cache_ttl;

	char *request_order;

	gear_bool mail_x_header;
	char *mail_log;

	gear_bool in_error_log;

#ifdef HYSS_WIN32
	gear_bool windows_show_crt_warning;
#endif

	gear_long syslog_facility;
	char *syslog_ident;
	gear_bool have_called_openlog;
	gear_long syslog_filter;
};


#endif /* HYSS_GLOBALS_H */

