/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HYSS_WIN32
#include "config.w32.h"
#include <Ws2tcpip.h>
#endif

#if HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#endif

typedef hyss_stream *(hyss_stream_transport_factory_func)(const char *proto, size_t protolen,
		const char *resourcename, size_t resourcenamelen,
		const char *persistent_id, int options, int flags,
		struct timeval *timeout,
		hyss_stream_context *context STREAMS_DC);
typedef hyss_stream_transport_factory_func *hyss_stream_transport_factory;

BEGIN_EXTERN_C()
HYSSAPI int hyss_stream_xport_register(const char *protocol, hyss_stream_transport_factory factory);
HYSSAPI int hyss_stream_xport_unregister(const char *protocol);

#define STREAM_XPORT_CLIENT			0
#define STREAM_XPORT_SERVER			1

#define STREAM_XPORT_CONNECT		2
#define STREAM_XPORT_BIND			4
#define STREAM_XPORT_LISTEN			8
#define STREAM_XPORT_CONNECT_ASYNC	16

/* Open a client or server socket connection */
HYSSAPI hyss_stream *_hyss_stream_xport_create(const char *name, size_t namelen, int options,
		int flags, const char *persistent_id,
		struct timeval *timeout,
		hyss_stream_context *context,
		gear_string **error_string,
		int *error_code
		STREAMS_DC);

#define hyss_stream_xport_create(name, namelen, options, flags, persistent_id, timeout, context, estr, ecode) \
	_hyss_stream_xport_create(name, namelen, options, flags, persistent_id, timeout, context, estr, ecode STREAMS_CC)

/* Bind the stream to a local address */
HYSSAPI int hyss_stream_xport_bind(hyss_stream *stream,
		const char *name, size_t namelen,
		gear_string **error_text
		);

/* Connect to a remote address */
HYSSAPI int hyss_stream_xport_connect(hyss_stream *stream,
		const char *name, size_t namelen,
		int asynchronous,
		struct timeval *timeout,
		gear_string **error_text,
		int *error_code
		);

/* Prepare to listen */
HYSSAPI int hyss_stream_xport_listen(hyss_stream *stream,
		int backlog,
		gear_string **error_text
		);

/* Get the next client and their address as a string, or the underlying address
 * structure.  You must efree either of these if you request them */
HYSSAPI int hyss_stream_xport_accept(hyss_stream *stream, hyss_stream **client,
		gear_string **textaddr,
		void **addr, socklen_t *addrlen,
		struct timeval *timeout,
		gear_string **error_text
		);

/* Get the name of either the socket or it's peer */
HYSSAPI int hyss_stream_xport_get_name(hyss_stream *stream, int want_peer,
		gear_string **textaddr,
		void **addr, socklen_t *addrlen
		);

enum hyss_stream_xport_send_recv_flags {
	STREAM_OOB = 1,
	STREAM_PEEK = 2
};

/* Similar to recv() system call; read data from the stream, optionally
 * peeking, optionally retrieving OOB data */
HYSSAPI int hyss_stream_xport_recvfrom(hyss_stream *stream, char *buf, size_t buflen,
		int flags, void **addr, socklen_t *addrlen,
		gear_string **textaddr);

/* Similar to send() system call; send data to the stream, optionally
 * sending it as OOB data */
HYSSAPI int hyss_stream_xport_sendto(hyss_stream *stream, const char *buf, size_t buflen,
		int flags, void *addr, socklen_t addrlen);

typedef enum {
	STREAM_SHUT_RD,
	STREAM_SHUT_WR,
	STREAM_SHUT_RDWR
} stream_shutdown_t;

/* Similar to shutdown() system call; shut down part of a full-duplex
 * connection */
HYSSAPI int hyss_stream_xport_shutdown(hyss_stream *stream, stream_shutdown_t how);
END_EXTERN_C()


/* Structure definition for the set_option interface that the above functions wrap */

typedef struct _hyss_stream_xport_param {
	enum {
		STREAM_XPORT_OP_BIND, STREAM_XPORT_OP_CONNECT,
		STREAM_XPORT_OP_LISTEN, STREAM_XPORT_OP_ACCEPT,
		STREAM_XPORT_OP_CONNECT_ASYNC,
		STREAM_XPORT_OP_GET_NAME,
		STREAM_XPORT_OP_GET_PEER_NAME,
		STREAM_XPORT_OP_RECV,
		STREAM_XPORT_OP_SEND,
		STREAM_XPORT_OP_SHUTDOWN
	} op;
	unsigned int want_addr:1;
	unsigned int want_textaddr:1;
	unsigned int want_errortext:1;
	unsigned int how:2;

	struct {
		char *name;
		size_t namelen;
		struct timeval *timeout;
		struct sockaddr *addr;
		char *buf;
		size_t buflen;
		socklen_t addrlen;
		int backlog;
		int flags;
	} inputs;
	struct {
		hyss_stream *client;
		struct sockaddr *addr;
		socklen_t addrlen;
		gear_string *textaddr;
		gear_string *error_text;
		int returncode;
		int error_code;
	} outputs;
} hyss_stream_xport_param;

/* Because both client and server streams use the same mechanisms
   for encryption we use the LSB to denote clients.
*/
typedef enum {
	STREAM_CRYPTO_METHOD_SSLv2_CLIENT = (1 << 1 | 1),
	STREAM_CRYPTO_METHOD_SSLv3_CLIENT = (1 << 2 | 1),
	/* v23 no longer negotiates SSL2 or SSL3 */
	STREAM_CRYPTO_METHOD_SSLv23_CLIENT = ((1 << 3) | (1 << 4) | (1 << 5) | 1),
	STREAM_CRYPTO_METHOD_TLSv1_0_CLIENT = (1 << 3 | 1),
	STREAM_CRYPTO_METHOD_TLSv1_1_CLIENT = (1 << 4 | 1),
	STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT = (1 << 5 | 1),
	/* TLS equates to TLS_ANY as of HYSS 7.2 */
	STREAM_CRYPTO_METHOD_TLS_CLIENT = ((1 << 3) | (1 << 4) | (1 << 5) | 1),
	STREAM_CRYPTO_METHOD_TLS_ANY_CLIENT = ((1 << 3) | (1 << 4) | (1 << 5) | 1),
	STREAM_CRYPTO_METHOD_ANY_CLIENT = ((1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5) | 1),
	STREAM_CRYPTO_METHOD_SSLv2_SERVER = (1 << 1),
	STREAM_CRYPTO_METHOD_SSLv3_SERVER = (1 << 2),
	/* v23 no longer negotiates SSL2 or SSL3 */
	STREAM_CRYPTO_METHOD_SSLv23_SERVER = ((1 << 3) | (1 << 4) | (1 << 5)),
	STREAM_CRYPTO_METHOD_TLSv1_0_SERVER = (1 << 3),
	STREAM_CRYPTO_METHOD_TLSv1_1_SERVER = (1 << 4),
	STREAM_CRYPTO_METHOD_TLSv1_2_SERVER = (1 << 5),
	/* TLS equates to TLS_ANY as of HYSS 7.2 */
	STREAM_CRYPTO_METHOD_TLS_SERVER = ((1 << 3) | (1 << 4) | (1 << 5)),
	STREAM_CRYPTO_METHOD_TLS_ANY_SERVER = ((1 << 3) | (1 << 4) | (1 << 5)),
	STREAM_CRYPTO_METHOD_ANY_SERVER = ((1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5))
} hyss_stream_xport_crypt_method_t;

/* These functions provide crypto support on the underlying transport */

BEGIN_EXTERN_C()
HYSSAPI int hyss_stream_xport_crypto_setup(hyss_stream *stream, hyss_stream_xport_crypt_method_t crypto_method, hyss_stream *session_stream);
HYSSAPI int hyss_stream_xport_crypto_enable(hyss_stream *stream, int activate);
END_EXTERN_C()

typedef struct _hyss_stream_xport_crypto_param {
	struct {
		hyss_stream *session;
		int activate;
		hyss_stream_xport_crypt_method_t method;
	} inputs;
	struct {
		int returncode;
	} outputs;
	enum {
		STREAM_XPORT_CRYPTO_OP_SETUP,
		STREAM_XPORT_CRYPTO_OP_ENABLE
	} op;
} hyss_stream_xport_crypto_param;

BEGIN_EXTERN_C()
HYSSAPI HashTable *hyss_stream_xport_get_hash(void);
HYSSAPI hyss_stream_transport_factory_func hyss_stream_generic_socket_factory;
END_EXTERN_C()

