/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Memory Mapping interface for streams */
#include "hyss.h"
#include "hyss_streams_int.h"

HYSSAPI char *_hyss_stream_mmap_range(hyss_stream *stream, size_t offset, size_t length, hyss_stream_mmap_access_t mode, size_t *mapped_len)
{
	hyss_stream_mmap_range range;

	range.offset = offset;
	range.length = length;
	range.mode = mode;
	range.mapped = NULL;

	/* For now, we impose an arbitrary limit to avoid
	 * runaway swapping when large files are passed through. */
	if (length > 4 * 1024 * 1024) {
		return NULL;
	}

	if (HYSS_STREAM_OPTION_RETURN_OK == hyss_stream_set_option(stream, HYSS_STREAM_OPTION_MMAP_API, HYSS_STREAM_MMAP_MAP_RANGE, &range)) {
		if (mapped_len) {
			*mapped_len = range.length;
		}
		return range.mapped;
	}
	return NULL;
}

HYSSAPI int _hyss_stream_mmap_unmap(hyss_stream *stream)
{
	return hyss_stream_set_option(stream, HYSS_STREAM_OPTION_MMAP_API, HYSS_STREAM_MMAP_UNMAP, NULL) == HYSS_STREAM_OPTION_RETURN_OK ? 1 : 0;
}

HYSSAPI int _hyss_stream_mmap_unmap_ex(hyss_stream *stream, gear_off_t readden)
{
	int ret = 1;

	if (hyss_stream_seek(stream, readden, SEEK_CUR) != 0) {
		ret = 0;
	}
	if (hyss_stream_mmap_unmap(stream) == 0) {
		ret = 0;
	}

	return ret;
}

