/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "extslib/standard/file.h"
#include "streams/hyss_streams_int.h"
#include "hyss_network.h"

#if defined(HYSS_WIN32) || defined(__riscos__)
# undef AF_UNIX
#endif

#if defined(AF_UNIX)
#include <sys/un.h>
#endif

#ifndef MSG_DONTWAIT
# define MSG_DONTWAIT 0
#endif

#ifndef MSG_PEEK
# define MSG_PEEK 0
#endif

#ifdef HYSS_WIN32
/* send/recv family on windows expects int */
# define XP_SOCK_BUF_SIZE(sz) (((sz) > INT_MAX) ? INT_MAX : (int)(sz))
#else
# define XP_SOCK_BUF_SIZE(sz) (sz)
#endif

const hyss_stream_ops hyss_stream_generic_socket_ops;
HYSSAPI const hyss_stream_ops hyss_stream_socket_ops;
const hyss_stream_ops hyss_stream_udp_socket_ops;
#ifdef AF_UNIX
const hyss_stream_ops hyss_stream_unix_socket_ops;
const hyss_stream_ops hyss_stream_unixdg_socket_ops;
#endif


static int hyss_tcp_sockop_set_option(hyss_stream *stream, int option, int value, void *ptrparam);

/* {{{ Generic socket stream operations */
static size_t hyss_sockop_write(hyss_stream *stream, const char *buf, size_t count)
{
	hyss_netstream_data_t *sock = (hyss_netstream_data_t*)stream->abstract;
	int didwrite;
	struct timeval *ptimeout;

	if (!sock || sock->socket == -1) {
		return 0;
	}

	if (sock->timeout.tv_sec == -1)
		ptimeout = NULL;
	else
		ptimeout = &sock->timeout;

retry:
	didwrite = send(sock->socket, buf, XP_SOCK_BUF_SIZE(count), (sock->is_blocked && ptimeout) ? MSG_DONTWAIT : 0);

	if (didwrite <= 0) {
		int err = hyss_socket_errno();
		char *estr;

		if (sock->is_blocked && (err == EWOULDBLOCK || err == EAGAIN)) {
			int retval;

			sock->timeout_event = 0;

			do {
				retval = hyss_pollfd_for(sock->socket, POLLOUT, ptimeout);

				if (retval == 0) {
					sock->timeout_event = 1;
					break;
				}

				if (retval > 0) {
					/* writable now; retry */
					goto retry;
				}

				err = hyss_socket_errno();
			} while (err == EINTR);
		}
		estr = hyss_socket_strerror(err, NULL, 0);
		hyss_error_docref(NULL, E_NOTICE, "send of " GEAR_LONG_FMT " bytes failed with errno=%d %s",
				(gear_long)count, err, estr);
		efree(estr);
	}

	if (didwrite > 0) {
		hyss_stream_notify_progress_increment(HYSS_STREAM_CONTEXT(stream), didwrite, 0);
	}

	if (didwrite < 0) {
		didwrite = 0;
	}

	return didwrite;
}

static void hyss_sock_stream_wait_for_data(hyss_stream *stream, hyss_netstream_data_t *sock)
{
	int retval;
	struct timeval *ptimeout;

	if (!sock || sock->socket == -1) {
		return;
	}

	sock->timeout_event = 0;

	if (sock->timeout.tv_sec == -1)
		ptimeout = NULL;
	else
		ptimeout = &sock->timeout;

	while(1) {
		retval = hyss_pollfd_for(sock->socket, HYSS_POLLREADABLE, ptimeout);

		if (retval == 0)
			sock->timeout_event = 1;

		if (retval >= 0)
			break;

		if (hyss_socket_errno() != EINTR)
			break;
	}
}

static size_t hyss_sockop_read(hyss_stream *stream, char *buf, size_t count)
{
	hyss_netstream_data_t *sock = (hyss_netstream_data_t*)stream->abstract;
	ssize_t nr_bytes = 0;
	int err;

	if (!sock || sock->socket == -1) {
		return 0;
	}

	if (sock->is_blocked) {
		hyss_sock_stream_wait_for_data(stream, sock);
		if (sock->timeout_event)
			return 0;
	}

	nr_bytes = recv(sock->socket, buf, XP_SOCK_BUF_SIZE(count), (sock->is_blocked && sock->timeout.tv_sec != -1) ? MSG_DONTWAIT : 0);
	err = hyss_socket_errno();

	stream->eof = (nr_bytes == 0 || (nr_bytes == -1 && err != EWOULDBLOCK && err != EAGAIN));

	if (nr_bytes > 0) {
		hyss_stream_notify_progress_increment(HYSS_STREAM_CONTEXT(stream), nr_bytes, 0);
	}

	if (nr_bytes < 0) {
		nr_bytes = 0;
	}

	return nr_bytes;
}


static int hyss_sockop_close(hyss_stream *stream, int close_handle)
{
	hyss_netstream_data_t *sock = (hyss_netstream_data_t*)stream->abstract;
#ifdef HYSS_WIN32
	int n;
#endif

	if (!sock) {
		return 0;
	}

	if (close_handle) {

#ifdef HYSS_WIN32
		if (sock->socket == -1)
			sock->socket = SOCK_ERR;
#endif
		if (sock->socket != SOCK_ERR) {
#ifdef HYSS_WIN32
			/* prevent more data from coming in */
			shutdown(sock->socket, SHUT_RD);

			/* try to make sure that the OS sends all data before we close the connection.
			 * Essentially, we are waiting for the socket to become writeable, which means
			 * that all pending data has been sent.
			 * We use a small timeout which should encourage the OS to send the data,
			 * but at the same time avoid hanging indefinitely.
			 * */
			do {
				n = hyss_pollfd_for_ms(sock->socket, POLLOUT, 500);
			} while (n == -1 && hyss_socket_errno() == EINTR);
#endif
			closesocket(sock->socket);
			sock->socket = SOCK_ERR;
		}

	}

	pefree(sock, hyss_stream_is_persistent(stream));

	return 0;
}

static int hyss_sockop_flush(hyss_stream *stream)
{
#if 0
	hyss_netstream_data_t *sock = (hyss_netstream_data_t*)stream->abstract;
	return fsync(sock->socket);
#endif
	return 0;
}

static int hyss_sockop_stat(hyss_stream *stream, hyss_stream_statbuf *ssb)
{
#if GEAR_WIN32
	return 0;
#else
	hyss_netstream_data_t *sock = (hyss_netstream_data_t*)stream->abstract;

	return gear_fstat(sock->socket, &ssb->sb);
#endif
}

static inline int sock_sendto(hyss_netstream_data_t *sock, const char *buf, size_t buflen, int flags,
		struct sockaddr *addr, socklen_t addrlen
		)
{
	int ret;
	if (addr) {
		ret = sendto(sock->socket, buf, XP_SOCK_BUF_SIZE(buflen), flags, addr, XP_SOCK_BUF_SIZE(addrlen));

		return (ret == SOCK_CONN_ERR) ? -1 : ret;
	}
#ifdef HYSS_WIN32
	return ((ret = send(sock->socket, buf, buflen > INT_MAX ? INT_MAX : (int)buflen, flags)) == SOCK_CONN_ERR) ? -1 : ret;
#else
	return ((ret = send(sock->socket, buf, buflen, flags)) == SOCK_CONN_ERR) ? -1 : ret;
#endif
}

static inline int sock_recvfrom(hyss_netstream_data_t *sock, char *buf, size_t buflen, int flags,
		gear_string **textaddr,
		struct sockaddr **addr, socklen_t *addrlen
		)
{
	int ret;
	int want_addr = textaddr || addr;

	if (want_addr) {
		hyss_sockaddr_storage sa;
		socklen_t sl = sizeof(sa);
		ret = recvfrom(sock->socket, buf, XP_SOCK_BUF_SIZE(buflen), flags, (struct sockaddr*)&sa, &sl);
		ret = (ret == SOCK_CONN_ERR) ? -1 : ret;
		if (sl) {
			hyss_network_populate_name_from_sockaddr((struct sockaddr*)&sa, sl,
					textaddr, addr, addrlen);
		} else {
			if (textaddr) {
				*textaddr = ZSTR_EMPTY_ALLOC();
			}
			if (addr) {
				*addr = NULL;
				*addrlen = 0;
			}
		}
	} else {
		ret = recv(sock->socket, buf, XP_SOCK_BUF_SIZE(buflen), flags);
		ret = (ret == SOCK_CONN_ERR) ? -1 : ret;
	}

	return ret;
}

static int hyss_sockop_set_option(hyss_stream *stream, int option, int value, void *ptrparam)
{
	int oldmode, flags;
	hyss_netstream_data_t *sock = (hyss_netstream_data_t*)stream->abstract;
	hyss_stream_xport_param *xparam;

	if (!sock) {
		return HYSS_STREAM_OPTION_RETURN_NOTIMPL;
	}

	switch(option) {
		case HYSS_STREAM_OPTION_CHECK_LIVENESS:
			{
				struct timeval tv;
				char buf;
				int alive = 1;

				if (value == -1) {
					if (sock->timeout.tv_sec == -1) {
						tv.tv_sec = FG(default_socket_timeout);
						tv.tv_usec = 0;
					} else {
						tv = sock->timeout;
					}
				} else {
					tv.tv_sec = value;
					tv.tv_usec = 0;
				}

				if (sock->socket == -1) {
					alive = 0;
				} else if (hyss_pollfd_for(sock->socket, HYSS_POLLREADABLE|POLLPRI, &tv) > 0) {
#ifdef HYSS_WIN32
					int ret;
#else
					ssize_t ret;
#endif
					int err;

					ret = recv(sock->socket, &buf, sizeof(buf), MSG_PEEK);
					err = hyss_socket_errno();
					if (0 == ret || /* the counterpart did properly shutdown*/
						(0 > ret && err != EWOULDBLOCK && err != EAGAIN && err != EMSGSIZE)) { /* there was an unrecoverable error */
						alive = 0;
					}
				}
				return alive ? HYSS_STREAM_OPTION_RETURN_OK : HYSS_STREAM_OPTION_RETURN_ERR;
			}

		case HYSS_STREAM_OPTION_BLOCKING:
			oldmode = sock->is_blocked;
			if (SUCCESS == hyss_set_sock_blocking(sock->socket, value)) {
				sock->is_blocked = value;
				return oldmode;
			}
			return HYSS_STREAM_OPTION_RETURN_ERR;

		case HYSS_STREAM_OPTION_READ_TIMEOUT:
			sock->timeout = *(struct timeval*)ptrparam;
			sock->timeout_event = 0;
			return HYSS_STREAM_OPTION_RETURN_OK;

		case HYSS_STREAM_OPTION_META_DATA_API:
			add_assoc_bool((zval *)ptrparam, "timed_out", sock->timeout_event);
			add_assoc_bool((zval *)ptrparam, "blocked", sock->is_blocked);
			add_assoc_bool((zval *)ptrparam, "eof", stream->eof);
			return HYSS_STREAM_OPTION_RETURN_OK;

		case HYSS_STREAM_OPTION_XPORT_API:
			xparam = (hyss_stream_xport_param *)ptrparam;

			switch (xparam->op) {
				case STREAM_XPORT_OP_LISTEN:
					xparam->outputs.returncode = (listen(sock->socket, xparam->inputs.backlog) == 0) ?  0: -1;
					return HYSS_STREAM_OPTION_RETURN_OK;

				case STREAM_XPORT_OP_GET_NAME:
					xparam->outputs.returncode = hyss_network_get_sock_name(sock->socket,
							xparam->want_textaddr ? &xparam->outputs.textaddr : NULL,
							xparam->want_addr ? &xparam->outputs.addr : NULL,
							xparam->want_addr ? &xparam->outputs.addrlen : NULL
							);
					return HYSS_STREAM_OPTION_RETURN_OK;

				case STREAM_XPORT_OP_GET_PEER_NAME:
					xparam->outputs.returncode = hyss_network_get_peer_name(sock->socket,
							xparam->want_textaddr ? &xparam->outputs.textaddr : NULL,
							xparam->want_addr ? &xparam->outputs.addr : NULL,
							xparam->want_addr ? &xparam->outputs.addrlen : NULL
							);
					return HYSS_STREAM_OPTION_RETURN_OK;

				case STREAM_XPORT_OP_SEND:
					flags = 0;
					if ((xparam->inputs.flags & STREAM_OOB) == STREAM_OOB) {
						flags |= MSG_OOB;
					}
					xparam->outputs.returncode = sock_sendto(sock,
							xparam->inputs.buf, xparam->inputs.buflen,
							flags,
							xparam->inputs.addr,
							xparam->inputs.addrlen);
					if (xparam->outputs.returncode == -1) {
						char *err = hyss_socket_strerror(hyss_socket_errno(), NULL, 0);
						hyss_error_docref(NULL, E_WARNING,
						   	"%s\n", err);
						efree(err);
					}
					return HYSS_STREAM_OPTION_RETURN_OK;

				case STREAM_XPORT_OP_RECV:
					flags = 0;
					if ((xparam->inputs.flags & STREAM_OOB) == STREAM_OOB) {
						flags |= MSG_OOB;
					}
					if ((xparam->inputs.flags & STREAM_PEEK) == STREAM_PEEK) {
						flags |= MSG_PEEK;
					}
					xparam->outputs.returncode = sock_recvfrom(sock,
							xparam->inputs.buf, xparam->inputs.buflen,
							flags,
							xparam->want_textaddr ? &xparam->outputs.textaddr : NULL,
							xparam->want_addr ? &xparam->outputs.addr : NULL,
							xparam->want_addr ? &xparam->outputs.addrlen : NULL
							);
					return HYSS_STREAM_OPTION_RETURN_OK;


#ifdef HAVE_SHUTDOWN
# ifndef SHUT_RD
#  define SHUT_RD 0
# endif
# ifndef SHUT_WR
#  define SHUT_WR 1
# endif
# ifndef SHUT_RDWR
#  define SHUT_RDWR 2
# endif
				case STREAM_XPORT_OP_SHUTDOWN: {
					static const int shutdown_how[] = {SHUT_RD, SHUT_WR, SHUT_RDWR};

					xparam->outputs.returncode = shutdown(sock->socket, shutdown_how[xparam->how]);
					return HYSS_STREAM_OPTION_RETURN_OK;
				}
#endif

				default:
					return HYSS_STREAM_OPTION_RETURN_NOTIMPL;
			}

		default:
			return HYSS_STREAM_OPTION_RETURN_NOTIMPL;
	}
}

static int hyss_sockop_cast(hyss_stream *stream, int castas, void **ret)
{
	hyss_netstream_data_t *sock = (hyss_netstream_data_t*)stream->abstract;

	if (!sock) {
		return FAILURE;
	}

	switch(castas)	{
		case HYSS_STREAM_AS_STDIO:
			if (ret)	{
				*(FILE**)ret = fdopen(sock->socket, stream->mode);
				if (*ret)
					return SUCCESS;
				return FAILURE;
			}
			return SUCCESS;
		case HYSS_STREAM_AS_FD_FOR_SELECT:
		case HYSS_STREAM_AS_FD:
		case HYSS_STREAM_AS_SOCKETD:
			if (ret)
				*(hyss_socket_t *)ret = sock->socket;
			return SUCCESS;
		default:
			return FAILURE;
	}
}
/* }}} */

/* These may look identical, but we need them this way so that
 * we can determine which type of socket we are dealing with
 * by inspecting stream->ops.
 * A "useful" side-effect is that the user's scripts can then
 * make similar decisions using stream_get_meta_data.
 * */
const hyss_stream_ops hyss_stream_generic_socket_ops = {
	hyss_sockop_write, hyss_sockop_read,
	hyss_sockop_close, hyss_sockop_flush,
	"generic_socket",
	NULL, /* seek */
	hyss_sockop_cast,
	hyss_sockop_stat,
	hyss_sockop_set_option,
};


const hyss_stream_ops hyss_stream_socket_ops = {
	hyss_sockop_write, hyss_sockop_read,
	hyss_sockop_close, hyss_sockop_flush,
	"tcp_socket",
	NULL, /* seek */
	hyss_sockop_cast,
	hyss_sockop_stat,
	hyss_tcp_sockop_set_option,
};

const hyss_stream_ops hyss_stream_udp_socket_ops = {
	hyss_sockop_write, hyss_sockop_read,
	hyss_sockop_close, hyss_sockop_flush,
	"udp_socket",
	NULL, /* seek */
	hyss_sockop_cast,
	hyss_sockop_stat,
	hyss_tcp_sockop_set_option,
};

#ifdef AF_UNIX
const hyss_stream_ops hyss_stream_unix_socket_ops = {
	hyss_sockop_write, hyss_sockop_read,
	hyss_sockop_close, hyss_sockop_flush,
	"unix_socket",
	NULL, /* seek */
	hyss_sockop_cast,
	hyss_sockop_stat,
	hyss_tcp_sockop_set_option,
};
const hyss_stream_ops hyss_stream_unixdg_socket_ops = {
	hyss_sockop_write, hyss_sockop_read,
	hyss_sockop_close, hyss_sockop_flush,
	"udg_socket",
	NULL, /* seek */
	hyss_sockop_cast,
	hyss_sockop_stat,
	hyss_tcp_sockop_set_option,
};
#endif


/* network socket operations */

#ifdef AF_UNIX
static inline int parse_unix_address(hyss_stream_xport_param *xparam, struct sockaddr_un *unix_addr)
{
	memset(unix_addr, 0, sizeof(*unix_addr));
	unix_addr->sun_family = AF_UNIX;

	/* we need to be binary safe on systems that support an abstract
	 * namespace */
	if (xparam->inputs.namelen >= sizeof(unix_addr->sun_path)) {
		/* On linux, when the path begins with a NUL byte we are
		 * referring to an abstract namespace.  In theory we should
		 * allow an extra byte below, since we don't need the NULL.
		 * BUT, to get into this branch of code, the name is too long,
		 * so we don't care. */
		xparam->inputs.namelen = sizeof(unix_addr->sun_path) - 1;
		hyss_error_docref(NULL, E_NOTICE,
			"socket path exceeded the maximum allowed length of %lu bytes "
			"and was truncated", (unsigned long)sizeof(unix_addr->sun_path));
	}

	memcpy(unix_addr->sun_path, xparam->inputs.name, xparam->inputs.namelen);

	return 1;
}
#endif

static inline char *parse_ip_address_ex(const char *str, size_t str_len, int *portno, int get_err, gear_string **err)
{
	char *colon;
	char *host = NULL;

#ifdef HAVE_IPV6
	char *p;

	if (*(str) == '[' && str_len > 1) {
		/* IPV6 notation to specify raw address with port (i.e. [fe80::1]:80) */
		p = memchr(str + 1, ']', str_len - 2);
		if (!p || *(p + 1) != ':') {
			if (get_err) {
				*err = strpprintf(0, "Failed to parse IPv6 address \"%s\"", str);
			}
			return NULL;
		}
		*portno = atoi(p + 2);
		return estrndup(str + 1, p - str - 1);
	}
#endif
	if (str_len) {
		colon = memchr(str, ':', str_len - 1);
	} else {
		colon = NULL;
	}
	if (colon) {
		*portno = atoi(colon + 1);
		host = estrndup(str, colon - str);
	} else {
		if (get_err) {
			*err = strpprintf(0, "Failed to parse address \"%s\"", str);
		}
		return NULL;
	}

	return host;
}

static inline char *parse_ip_address(hyss_stream_xport_param *xparam, int *portno)
{
	return parse_ip_address_ex(xparam->inputs.name, xparam->inputs.namelen, portno, xparam->want_errortext, &xparam->outputs.error_text);
}

static inline int hyss_tcp_sockop_bind(hyss_stream *stream, hyss_netstream_data_t *sock,
		hyss_stream_xport_param *xparam)
{
	char *host = NULL;
	int portno, err;
	long sockopts = STREAM_SOCKOP_NONE;
	zval *tmpzval = NULL;

#ifdef AF_UNIX
	if (stream->ops == &hyss_stream_unix_socket_ops || stream->ops == &hyss_stream_unixdg_socket_ops) {
		struct sockaddr_un unix_addr;

		sock->socket = socket(PF_UNIX, stream->ops == &hyss_stream_unix_socket_ops ? SOCK_STREAM : SOCK_DGRAM, 0);

		if (sock->socket == SOCK_ERR) {
			if (xparam->want_errortext) {
				xparam->outputs.error_text = strpprintf(0, "Failed to create unix%s socket %s",
						stream->ops == &hyss_stream_unix_socket_ops ? "" : "datagram",
						strerror(errno));
			}
			return -1;
		}

		parse_unix_address(xparam, &unix_addr);

		return bind(sock->socket, (const struct sockaddr *)&unix_addr,
			(socklen_t) XtOffsetOf(struct sockaddr_un, sun_path) + xparam->inputs.namelen);
	}
#endif

	host = parse_ip_address(xparam, &portno);

	if (host == NULL) {
		return -1;
	}

#ifdef IPV6_V6ONLY
	if (HYSS_STREAM_CONTEXT(stream)
		&& (tmpzval = hyss_stream_context_get_option(HYSS_STREAM_CONTEXT(stream), "socket", "ipv6_v6only")) != NULL
		&& Z_TYPE_P(tmpzval) != IS_NULL
	) {
		sockopts |= STREAM_SOCKOP_IPV6_V6ONLY;
		sockopts |= STREAM_SOCKOP_IPV6_V6ONLY_ENABLED * gear_is_true(tmpzval);
	}
#endif

#ifdef SO_REUSEPORT
	if (HYSS_STREAM_CONTEXT(stream)
		&& (tmpzval = hyss_stream_context_get_option(HYSS_STREAM_CONTEXT(stream), "socket", "so_reuseport")) != NULL
		&& gear_is_true(tmpzval)
	) {
		sockopts |= STREAM_SOCKOP_SO_REUSEPORT;
	}
#endif

#ifdef SO_BROADCAST
	if (stream->ops == &hyss_stream_udp_socket_ops /* SO_BROADCAST is only applicable for UDP */
		&& HYSS_STREAM_CONTEXT(stream)
		&& (tmpzval = hyss_stream_context_get_option(HYSS_STREAM_CONTEXT(stream), "socket", "so_broadcast")) != NULL
		&& gear_is_true(tmpzval)
	) {
		sockopts |= STREAM_SOCKOP_SO_BROADCAST;
	}
#endif

	sock->socket = hyss_network_bind_socket_to_local_addr(host, portno,
			stream->ops == &hyss_stream_udp_socket_ops ? SOCK_DGRAM : SOCK_STREAM,
			sockopts,
			xparam->want_errortext ? &xparam->outputs.error_text : NULL,
			&err
			);

	if (host) {
		efree(host);
	}

	return sock->socket == -1 ? -1 : 0;
}

static inline int hyss_tcp_sockop_connect(hyss_stream *stream, hyss_netstream_data_t *sock,
		hyss_stream_xport_param *xparam)
{
	char *host = NULL, *bindto = NULL;
	int portno, bindport = 0;
	int err = 0;
	int ret;
	zval *tmpzval = NULL;
	long sockopts = STREAM_SOCKOP_NONE;

#ifdef AF_UNIX
	if (stream->ops == &hyss_stream_unix_socket_ops || stream->ops == &hyss_stream_unixdg_socket_ops) {
		struct sockaddr_un unix_addr;

		sock->socket = socket(PF_UNIX, stream->ops == &hyss_stream_unix_socket_ops ? SOCK_STREAM : SOCK_DGRAM, 0);

		if (sock->socket == SOCK_ERR) {
			if (xparam->want_errortext) {
				xparam->outputs.error_text = strpprintf(0, "Failed to create unix socket");
			}
			return -1;
		}

		parse_unix_address(xparam, &unix_addr);

		ret = hyss_network_connect_socket(sock->socket,
				(const struct sockaddr *)&unix_addr, (socklen_t) XtOffsetOf(struct sockaddr_un, sun_path) + xparam->inputs.namelen,
				xparam->op == STREAM_XPORT_OP_CONNECT_ASYNC, xparam->inputs.timeout,
				xparam->want_errortext ? &xparam->outputs.error_text : NULL,
				&err);

		xparam->outputs.error_code = err;

		goto out;
	}
#endif

	host = parse_ip_address(xparam, &portno);

	if (host == NULL) {
		return -1;
	}

	if (HYSS_STREAM_CONTEXT(stream) && (tmpzval = hyss_stream_context_get_option(HYSS_STREAM_CONTEXT(stream), "socket", "bindto")) != NULL) {
		if (Z_TYPE_P(tmpzval) != IS_STRING) {
			if (xparam->want_errortext) {
				xparam->outputs.error_text = strpprintf(0, "local_addr context option is not a string.");
			}
			efree(host);
			return -1;
		}
		bindto = parse_ip_address_ex(Z_STRVAL_P(tmpzval), Z_STRLEN_P(tmpzval), &bindport, xparam->want_errortext, &xparam->outputs.error_text);
	}

#ifdef SO_BROADCAST
	if (stream->ops == &hyss_stream_udp_socket_ops /* SO_BROADCAST is only applicable for UDP */
		&& HYSS_STREAM_CONTEXT(stream)
		&& (tmpzval = hyss_stream_context_get_option(HYSS_STREAM_CONTEXT(stream), "socket", "so_broadcast")) != NULL
		&& gear_is_true(tmpzval)
	) {
		sockopts |= STREAM_SOCKOP_SO_BROADCAST;
	}
#endif

	if (stream->ops != &hyss_stream_udp_socket_ops /* TCP_NODELAY is only applicable for TCP */
#ifdef AF_UNIX
		&& stream->ops != &hyss_stream_unix_socket_ops
		&& stream->ops != &hyss_stream_unixdg_socket_ops
#endif
		&& HYSS_STREAM_CONTEXT(stream)
		&& (tmpzval = hyss_stream_context_get_option(HYSS_STREAM_CONTEXT(stream), "socket", "tcp_nodelay")) != NULL
		&& gear_is_true(tmpzval)
	) {
		sockopts |= STREAM_SOCKOP_TCP_NODELAY;
	}

	/* Note: the test here for hyss_stream_udp_socket_ops is important, because we
	 * want the default to be TCP sockets so that the openssl extension can
	 * re-use this code. */

	sock->socket = hyss_network_connect_socket_to_host(host, portno,
			stream->ops == &hyss_stream_udp_socket_ops ? SOCK_DGRAM : SOCK_STREAM,
			xparam->op == STREAM_XPORT_OP_CONNECT_ASYNC,
			xparam->inputs.timeout,
			xparam->want_errortext ? &xparam->outputs.error_text : NULL,
			&err,
			bindto,
			bindport,
			sockopts
			);

	ret = sock->socket == -1 ? -1 : 0;
	xparam->outputs.error_code = err;

	if (host) {
		efree(host);
	}
	if (bindto) {
		efree(bindto);
	}

#ifdef AF_UNIX
out:
#endif

	if (ret >= 0 && xparam->op == STREAM_XPORT_OP_CONNECT_ASYNC && err == EINPROGRESS) {
		/* indicates pending connection */
		return 1;
	}

	return ret;
}

static inline int hyss_tcp_sockop_accept(hyss_stream *stream, hyss_netstream_data_t *sock,
		hyss_stream_xport_param *xparam STREAMS_DC)
{
	int clisock;
	gear_bool nodelay = 0;
	zval *tmpzval = NULL;

	xparam->outputs.client = NULL;

	if ((NULL != HYSS_STREAM_CONTEXT(stream)) &&
		(tmpzval = hyss_stream_context_get_option(HYSS_STREAM_CONTEXT(stream), "socket", "tcp_nodelay")) != NULL &&
		gear_is_true(tmpzval)) {
		nodelay = 1;
	}

	clisock = hyss_network_accept_incoming(sock->socket,
		xparam->want_textaddr ? &xparam->outputs.textaddr : NULL,
		xparam->want_addr ? &xparam->outputs.addr : NULL,
		xparam->want_addr ? &xparam->outputs.addrlen : NULL,
		xparam->inputs.timeout,
		xparam->want_errortext ? &xparam->outputs.error_text : NULL,
		&xparam->outputs.error_code,
		nodelay);

	if (clisock >= 0) {
		hyss_netstream_data_t *clisockdata = (hyss_netstream_data_t*) emalloc(sizeof(*clisockdata));

		memcpy(clisockdata, sock, sizeof(*clisockdata));
		clisockdata->socket = clisock;

		xparam->outputs.client = hyss_stream_alloc_rel(stream->ops, clisockdata, NULL, "r+");
		if (xparam->outputs.client) {
			xparam->outputs.client->ctx = stream->ctx;
			if (stream->ctx) {
				GC_ADDREF(stream->ctx);
			}
		}
	}

	return xparam->outputs.client == NULL ? -1 : 0;
}

static int hyss_tcp_sockop_set_option(hyss_stream *stream, int option, int value, void *ptrparam)
{
	hyss_netstream_data_t *sock = (hyss_netstream_data_t*)stream->abstract;
	hyss_stream_xport_param *xparam;

	switch(option) {
		case HYSS_STREAM_OPTION_XPORT_API:
			xparam = (hyss_stream_xport_param *)ptrparam;

			switch(xparam->op) {
				case STREAM_XPORT_OP_CONNECT:
				case STREAM_XPORT_OP_CONNECT_ASYNC:
					xparam->outputs.returncode = hyss_tcp_sockop_connect(stream, sock, xparam);
					return HYSS_STREAM_OPTION_RETURN_OK;

				case STREAM_XPORT_OP_BIND:
					xparam->outputs.returncode = hyss_tcp_sockop_bind(stream, sock, xparam);
					return HYSS_STREAM_OPTION_RETURN_OK;


				case STREAM_XPORT_OP_ACCEPT:
					xparam->outputs.returncode = hyss_tcp_sockop_accept(stream, sock, xparam STREAMS_CC);
					return HYSS_STREAM_OPTION_RETURN_OK;
				default:
					/* fall through */
					;
			}
	}
	return hyss_sockop_set_option(stream, option, value, ptrparam);
}


HYSSAPI hyss_stream *hyss_stream_generic_socket_factory(const char *proto, size_t protolen,
		const char *resourcename, size_t resourcenamelen,
		const char *persistent_id, int options, int flags,
		struct timeval *timeout,
		hyss_stream_context *context STREAMS_DC)
{
	hyss_stream *stream = NULL;
	hyss_netstream_data_t *sock;
	const hyss_stream_ops *ops;

	/* which type of socket ? */
	if (strncmp(proto, "tcp", protolen) == 0) {
		ops = &hyss_stream_socket_ops;
	} else if (strncmp(proto, "udp", protolen) == 0) {
		ops = &hyss_stream_udp_socket_ops;
	}
#ifdef AF_UNIX
	else if (strncmp(proto, "unix", protolen) == 0) {
		ops = &hyss_stream_unix_socket_ops;
	} else if (strncmp(proto, "udg", protolen) == 0) {
		ops = &hyss_stream_unixdg_socket_ops;
	}
#endif
	else {
		/* should never happen */
		return NULL;
	}

	sock = pemalloc(sizeof(hyss_netstream_data_t), persistent_id ? 1 : 0);
	memset(sock, 0, sizeof(hyss_netstream_data_t));

	sock->is_blocked = 1;
	sock->timeout.tv_sec = FG(default_socket_timeout);
	sock->timeout.tv_usec = 0;

	/* we don't know the socket until we have determined if we are binding or
	 * connecting */
	sock->socket = -1;

	stream = hyss_stream_alloc_rel(ops, sock, persistent_id, "r+");

	if (stream == NULL)	{
		pefree(sock, persistent_id ? 1 : 0);
		return NULL;
	}

	if (flags == 0) {
		return stream;
	}

	return stream;
}

