/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include "hyss.h"
#include "hyss_globals.h"
#include "hyss_network.h"
#include "hyss_open_temporary_file.h"
#include "extslib/standard/file.h"
#include <stddef.h>
#include <fcntl.h>

#include "hyss_streams_int.h"

/* Under BSD, emulate fopencookie using funopen */
#if defined(HAVE_FUNOPEN) && !defined(HAVE_FOPENCOOKIE)

/* NetBSD 6.0+ uses off_t instead of fpos_t in funopen */
# if defined(__NetBSD__) && (__NetBSD_Version__ >= 600000000)
#  define HYSS_FPOS_T off_t
# else
#  define HYSS_FPOS_T fpos_t
# endif

typedef struct {
	int (*reader)(void *, char *, int);
	int (*writer)(void *, const char *, int);
	HYSS_FPOS_T (*seeker)(void *, HYSS_FPOS_T, int);
	int (*closer)(void *);
} COOKIE_IO_FUNCTIONS_T;

FILE *fopencookie(void *cookie, const char *mode, COOKIE_IO_FUNCTIONS_T *funcs)
{
	return funopen(cookie, funcs->reader, funcs->writer, funcs->seeker, funcs->closer);
}
# define HAVE_FOPENCOOKIE 1
# define HYSS_EMULATE_FOPENCOOKIE 1
# define HYSS_STREAM_COOKIE_FUNCTIONS	&stream_cookie_functions
#elif defined(HAVE_FOPENCOOKIE)
# define HYSS_STREAM_COOKIE_FUNCTIONS	stream_cookie_functions
#endif

/* {{{ STDIO with fopencookie */
#if defined(HYSS_EMULATE_FOPENCOOKIE)
/* use our fopencookie emulation */
static int stream_cookie_reader(void *cookie, char *buffer, int size)
{
	int ret;

	ret = hyss_stream_read((hyss_stream*)cookie, buffer, size);
	return ret;
}

static int stream_cookie_writer(void *cookie, const char *buffer, int size)
{

	return hyss_stream_write((hyss_stream *)cookie, (char *)buffer, size);
}

static HYSS_FPOS_T stream_cookie_seeker(void *cookie, gear_off_t position, int whence)
{

	return (HYSS_FPOS_T)hyss_stream_seek((hyss_stream *)cookie, position, whence);
}

static int stream_cookie_closer(void *cookie)
{
	hyss_stream *stream = (hyss_stream*)cookie;

	/* prevent recursion */
	stream->fclose_stdiocast = HYSS_STREAM_FCLOSE_NONE;
	return hyss_stream_free(stream, HYSS_STREAM_FREE_CLOSE | HYSS_STREAM_FREE_KEEP_RSRC);
}
#elif defined(HAVE_FOPENCOOKIE)
static ssize_t stream_cookie_reader(void *cookie, char *buffer, size_t size)
{
	ssize_t ret;

	ret = hyss_stream_read(((hyss_stream *)cookie), buffer, size);
	return ret;
}

static ssize_t stream_cookie_writer(void *cookie, const char *buffer, size_t size)
{

	return hyss_stream_write(((hyss_stream *)cookie), (char *)buffer, size);
}

# ifdef COOKIE_SEEKER_USES_OFF64_T
static int stream_cookie_seeker(void *cookie, __off64_t *position, int whence)
{

	*position = hyss_stream_seek((hyss_stream *)cookie, (gear_off_t)*position, whence);

	if (*position == -1) {
		return -1;
	}
	return 0;
}
# else
static int stream_cookie_seeker(void *cookie, gear_off_t position, int whence)
{

	return hyss_stream_seek((hyss_stream *)cookie, position, whence);
}
# endif

static int stream_cookie_closer(void *cookie)
{
	hyss_stream *stream = (hyss_stream*)cookie;

	/* prevent recursion */
	stream->fclose_stdiocast = HYSS_STREAM_FCLOSE_NONE;
	return hyss_stream_free(stream, HYSS_STREAM_FREE_CLOSE | HYSS_STREAM_FREE_KEEP_RSRC);
}
#endif /* elif defined(HAVE_FOPENCOOKIE) */

#if HAVE_FOPENCOOKIE
static COOKIE_IO_FUNCTIONS_T stream_cookie_functions =
{
	stream_cookie_reader, stream_cookie_writer,
	stream_cookie_seeker, stream_cookie_closer
};
#else
/* TODO: use socketpair() to emulate fopencookie, as suggested by Hartmut ? */
#endif
/* }}} */

/* {{{ hyss_stream_mode_sanitize_fdopen_fopencookie
 * Result should have at least size 5, e.g. to write wbx+\0 */
void hyss_stream_mode_sanitize_fdopen_fopencookie(hyss_stream *stream, char *result)
{
	/* replace modes not supported by fdopen and fopencookie, but supported
	 * by HYSS's fread(), so that their calls won't fail */
	const char *cur_mode = stream->mode;
	int         has_plus = 0,
		        has_bin  = 0,
				i,
				res_curs = 0;

	if (cur_mode[0] == 'r' || cur_mode[0] == 'w' || cur_mode[0] == 'a') {
		result[res_curs++] = cur_mode[0];
	} else {
		/* assume cur_mode[0] is 'c' or 'x'; substitute by 'w', which should not
		 * truncate anything in fdopen/fopencookie */
		result[res_curs++] = 'w';

		/* x is allowed (at least by glibc & compat), but not as the 1st mode
		 * as in HYSS and in any case is (at best) ignored by fdopen and fopencookie */
	}

	/* assume current mode has at most length 4 (e.g. wbn+) */
	for (i = 1; i < 4 && cur_mode[i] != '\0'; i++) {
		if (cur_mode[i] == 'b') {
			has_bin = 1;
		} else if (cur_mode[i] == '+') {
			has_plus = 1;
		}
		/* ignore 'n', 't' or other stuff */
	}

	if (has_bin) {
		result[res_curs++] = 'b';
	}
	if (has_plus) {
		result[res_curs++] = '+';
	}

	result[res_curs] = '\0';
}
/* }}} */

/* {{{ hyss_stream_cast */
HYSSAPI int _hyss_stream_cast(hyss_stream *stream, int castas, void **ret, int show_err)
{
	int flags = castas & HYSS_STREAM_CAST_MASK;
	castas &= ~HYSS_STREAM_CAST_MASK;

	/* synchronize our buffer (if possible) */
	if (ret && castas != HYSS_STREAM_AS_FD_FOR_SELECT) {
		hyss_stream_flush(stream);
		if (stream->ops->seek && (stream->flags & HYSS_STREAM_FLAG_NO_SEEK) == 0) {
			gear_off_t dummy;

			stream->ops->seek(stream, stream->position, SEEK_SET, &dummy);
			stream->readpos = stream->writepos = 0;
		}
	}

	/* filtered streams can only be cast as stdio, and only when fopencookie is present */

	if (castas == HYSS_STREAM_AS_STDIO) {
		if (stream->stdiocast) {
			if (ret) {
				*(FILE**)ret = stream->stdiocast;
			}
			goto exit_success;
		}

		/* if the stream is a stdio stream let's give it a chance to respond
		 * first, to avoid doubling up the layers of stdio with an fopencookie */
		if (hyss_stream_is(stream, HYSS_STREAM_IS_STDIO) &&
			stream->ops->cast &&
			!hyss_stream_is_filtered(stream) &&
			stream->ops->cast(stream, castas, ret) == SUCCESS
		) {
			goto exit_success;
		}

#if HAVE_FOPENCOOKIE
		/* if just checking, say yes we can be a FILE*, but don't actually create it yet */
		if (ret == NULL) {
			goto exit_success;
		}

		{
			char fixed_mode[5];
			hyss_stream_mode_sanitize_fdopen_fopencookie(stream, fixed_mode);
			*(FILE**)ret = fopencookie(stream, fixed_mode, HYSS_STREAM_COOKIE_FUNCTIONS);
		}

		if (*ret != NULL) {
			gear_off_t pos;

			stream->fclose_stdiocast = HYSS_STREAM_FCLOSE_FOPENCOOKIE;

			/* If the stream position is not at the start, we need to force
			 * the stdio layer to believe it's real location. */
			pos = hyss_stream_tell(stream);
			if (pos > 0) {
				gear_fseek(*ret, pos, SEEK_SET);
			}

			goto exit_success;
		}

		/* must be either:
			a) programmer error
			b) no memory
			-> lets bail
		*/
		hyss_error_docref(NULL, E_ERROR, "fopencookie failed");
		return FAILURE;
#endif

		if (!hyss_stream_is_filtered(stream) && stream->ops->cast && stream->ops->cast(stream, castas, NULL) == SUCCESS) {
			if (FAILURE == stream->ops->cast(stream, castas, ret)) {
				return FAILURE;
			}
			goto exit_success;
		} else if (flags & HYSS_STREAM_CAST_TRY_HARD) {
			hyss_stream *newstream;

			newstream = hyss_stream_fopen_tmpfile();
			if (newstream) {
				int retcopy = hyss_stream_copy_to_stream_ex(stream, newstream, HYSS_STREAM_COPY_ALL, NULL);

				if (retcopy != SUCCESS) {
					hyss_stream_close(newstream);
				} else {
					int retcast = hyss_stream_cast(newstream, castas | flags, (void **)ret, show_err);

					if (retcast == SUCCESS) {
						rewind(*(FILE**)ret);
					}

					/* do some specialized cleanup */
					if ((flags & HYSS_STREAM_CAST_RELEASE)) {
						hyss_stream_free(stream, HYSS_STREAM_FREE_CLOSE_CASTED);
					}

					/* TODO: we probably should be setting .stdiocast and .fclose_stdiocast or
					 * we may be leaking the FILE*. Needs investigation, though. */
					return retcast;
				}
			}
		}
	}

	if (hyss_stream_is_filtered(stream)) {
		hyss_error_docref(NULL, E_WARNING, "cannot cast a filtered stream on this system");
		return FAILURE;
	} else if (stream->ops->cast && stream->ops->cast(stream, castas, ret) == SUCCESS) {
		goto exit_success;
	}

	if (show_err) {
		/* these names depend on the values of the HYSS_STREAM_AS_XXX defines in hyss_streams.h */
		static const char *cast_names[4] = {
			"STDIO FILE*",
			"File Descriptor",
			"Socket Descriptor",
			"select()able descriptor"
		};

		hyss_error_docref(NULL, E_WARNING, "cannot represent a stream of type %s as a %s", stream->ops->label, cast_names[castas]);
	}

	return FAILURE;

exit_success:

	if ((stream->writepos - stream->readpos) > 0 &&
		stream->fclose_stdiocast != HYSS_STREAM_FCLOSE_FOPENCOOKIE &&
		(flags & HYSS_STREAM_CAST_INTERNAL) == 0
	) {
		/* the data we have buffered will be lost to the third party library that
		 * will be accessing the stream.  Emit a warning so that the end-user will
		 * know that they should try something else */

		hyss_error_docref(NULL, E_WARNING, GEAR_LONG_FMT " bytes of buffered data lost during stream conversion!", (gear_long)(stream->writepos - stream->readpos));
	}

	if (castas == HYSS_STREAM_AS_STDIO && ret) {
		stream->stdiocast = *(FILE**)ret;
	}

	if (flags & HYSS_STREAM_CAST_RELEASE) {
		hyss_stream_free(stream, HYSS_STREAM_FREE_CLOSE_CASTED);
	}

	return SUCCESS;

}
/* }}} */

/* {{{ hyss_stream_open_wrapper_as_file */
HYSSAPI FILE * _hyss_stream_open_wrapper_as_file(char *path, char *mode, int options, gear_string **opened_path STREAMS_DC)
{
	FILE *fp = NULL;
	hyss_stream *stream = NULL;

	stream = hyss_stream_open_wrapper_rel(path, mode, options|STREAM_WILL_CAST, opened_path);

	if (stream == NULL) {
		return NULL;
	}

	if (hyss_stream_cast(stream, HYSS_STREAM_AS_STDIO|HYSS_STREAM_CAST_TRY_HARD|HYSS_STREAM_CAST_RELEASE, (void**)&fp, REPORT_ERRORS) == FAILURE) {
		hyss_stream_close(stream);
		if (opened_path && *opened_path) {
			gear_string_release_ex(*opened_path, 0);
		}
		return NULL;
	}
	return fp;
}
/* }}} */

/* {{{ hyss_stream_make_seekable */
HYSSAPI int _hyss_stream_make_seekable(hyss_stream *origstream, hyss_stream **newstream, int flags STREAMS_DC)
{
	if (newstream == NULL) {
		return HYSS_STREAM_FAILED;
	}
	*newstream = NULL;

	if (((flags & HYSS_STREAM_FORCE_CONVERSION) == 0) && origstream->ops->seek != NULL) {
		*newstream = origstream;
		return HYSS_STREAM_UNCHANGED;
	}

	/* Use a tmpfile and copy the old streams contents into it */

	if (flags & HYSS_STREAM_PREFER_STDIO) {
		*newstream = hyss_stream_fopen_tmpfile();
	} else {
		*newstream = hyss_stream_temp_new();
	}

	if (*newstream == NULL) {
		return HYSS_STREAM_FAILED;
	}

#if GEAR_DEBUG
	(*newstream)->open_filename = origstream->open_filename;
	(*newstream)->open_lineno = origstream->open_lineno;
#endif

	if (hyss_stream_copy_to_stream_ex(origstream, *newstream, HYSS_STREAM_COPY_ALL, NULL) != SUCCESS) {
		hyss_stream_close(*newstream);
		*newstream = NULL;
		return HYSS_STREAM_CRITICAL;
	}

	hyss_stream_close(origstream);
	hyss_stream_seek(*newstream, 0, SEEK_SET);

	return HYSS_STREAM_RELEASED;
}
/* }}} */

