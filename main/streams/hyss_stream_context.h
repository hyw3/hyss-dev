/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Stream context and status notification related definitions */

/* callback for status notifications */
typedef void (*hyss_stream_notification_func)(hyss_stream_context *context,
		int notifycode, int severity,
		char *xmsg, int xcode,
		size_t bytes_sofar, size_t bytes_max,
		void * ptr);

#define HYSS_STREAM_NOTIFIER_PROGRESS	1

/* Attempt to fetch context from the zval passed,
   If no context was passed, use the default context
   The default context has not yet been created, do it now. */
#define hyss_stream_context_from_zval(zcontext, nocontext) ( \
		(zcontext) ? gear_fetch_resource_ex(zcontext, "Stream-Context", hyss_le_stream_context()) : \
		(nocontext) ? NULL : \
		FG(default_context) ? FG(default_context) : \
		(FG(default_context) = hyss_stream_context_alloc()) )

#define hyss_stream_context_to_zval(context, zval) { ZVAL_RES(zval, (context)->res); GC_ADDREF((context)->res); }

typedef struct _hyss_stream_notifier hyss_stream_notifier;

struct _hyss_stream_notifier {
	hyss_stream_notification_func func;
	void (*dtor)(hyss_stream_notifier *notifier);
	zval ptr;
	int mask;
	size_t progress, progress_max; /* position for progress notification */
};

struct _hyss_stream_context {
	hyss_stream_notifier *notifier;
	zval options;	/* hash keyed by wrapper family or specific wrapper */
	gear_resource *res;	/* used for auto-cleanup */
};

BEGIN_EXTERN_C()
HYSSAPI void hyss_stream_context_free(hyss_stream_context *context);
HYSSAPI hyss_stream_context *hyss_stream_context_alloc(void);
HYSSAPI zval *hyss_stream_context_get_option(hyss_stream_context *context,
		const char *wrappername, const char *optionname);
HYSSAPI int hyss_stream_context_set_option(hyss_stream_context *context,
		const char *wrappername, const char *optionname, zval *optionvalue);

HYSSAPI hyss_stream_notifier *hyss_stream_notification_alloc(void);
HYSSAPI void hyss_stream_notification_free(hyss_stream_notifier *notifier);
END_EXTERN_C()

/* not all notification codes are implemented */
#define HYSS_STREAM_NOTIFY_RESOLVE		1
#define HYSS_STREAM_NOTIFY_CONNECT		2
#define HYSS_STREAM_NOTIFY_AUTH_REQUIRED		3
#define HYSS_STREAM_NOTIFY_MIME_TYPE_IS	4
#define HYSS_STREAM_NOTIFY_FILE_SIZE_IS	5
#define HYSS_STREAM_NOTIFY_REDIRECTED	6
#define HYSS_STREAM_NOTIFY_PROGRESS		7
#define HYSS_STREAM_NOTIFY_COMPLETED		8
#define HYSS_STREAM_NOTIFY_FAILURE		9
#define HYSS_STREAM_NOTIFY_AUTH_RESULT	10

#define HYSS_STREAM_NOTIFY_SEVERITY_INFO	0
#define HYSS_STREAM_NOTIFY_SEVERITY_WARN	1
#define HYSS_STREAM_NOTIFY_SEVERITY_ERR	2

BEGIN_EXTERN_C()
HYSSAPI void hyss_stream_notification_notify(hyss_stream_context *context, int notifycode, int severity,
		char *xmsg, int xcode, size_t bytes_sofar, size_t bytes_max, void * ptr);
HYSSAPI hyss_stream_context *hyss_stream_context_set(hyss_stream *stream, hyss_stream_context *context);
END_EXTERN_C()

#define hyss_stream_notify_info(context, code, xmsg, xcode)	do { if ((context) && (context)->notifier) { \
	hyss_stream_notification_notify((context), (code), HYSS_STREAM_NOTIFY_SEVERITY_INFO, \
				(xmsg), (xcode), 0, 0, NULL); } } while (0)

#define hyss_stream_notify_progress(context, bsofar, bmax) do { if ((context) && (context)->notifier) { \
	hyss_stream_notification_notify((context), HYSS_STREAM_NOTIFY_PROGRESS, HYSS_STREAM_NOTIFY_SEVERITY_INFO, \
			NULL, 0, (bsofar), (bmax), NULL); } } while(0)

#define hyss_stream_notify_progress_init(context, sofar, bmax) do { if ((context) && (context)->notifier) { \
	(context)->notifier->progress = (sofar); \
	(context)->notifier->progress_max = (bmax); \
	(context)->notifier->mask |= HYSS_STREAM_NOTIFIER_PROGRESS; \
	hyss_stream_notify_progress((context), (sofar), (bmax)); } } while (0)

#define hyss_stream_notify_progress_increment(context, dsofar, dmax) do { if ((context) && (context)->notifier && (context)->notifier->mask & HYSS_STREAM_NOTIFIER_PROGRESS) { \
	(context)->notifier->progress += (dsofar); \
	(context)->notifier->progress_max += (dmax); \
	hyss_stream_notify_progress((context), (context)->notifier->progress, (context)->notifier->progress_max); } } while (0)

#define hyss_stream_notify_file_size(context, file_size, xmsg, xcode) do { if ((context) && (context)->notifier) { \
	hyss_stream_notification_notify((context), HYSS_STREAM_NOTIFY_FILE_SIZE_IS, HYSS_STREAM_NOTIFY_SEVERITY_INFO, \
			(xmsg), (xcode), 0, (file_size), NULL); } } while(0)

#define hyss_stream_notify_error(context, code, xmsg, xcode) do { if ((context) && (context)->notifier) {\
	hyss_stream_notification_notify((context), (code), HYSS_STREAM_NOTIFY_SEVERITY_ERR, \
			(xmsg), (xcode), 0, 0, NULL); } } while(0)


