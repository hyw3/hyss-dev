/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_globals.h"
#include "hyss_network.h"
#include "hyss_open_temporary_file.h"
#include "extslib/standard/file.h"
#include <stddef.h>
#include <fcntl.h>

#include "hyss_streams_int.h"

/* Global filter hash, copied to FG(stream_filters) on registration of volatile filter */
static HashTable stream_filters_hash;

/* Should only be used during core initialization */
HYSSAPI HashTable *hyss_get_stream_filters_hash_global(void)
{
	return &stream_filters_hash;
}

/* Normal hash selection/retrieval call */
HYSSAPI HashTable *_hyss_get_stream_filters_hash(void)
{
	return (FG(stream_filters) ? FG(stream_filters) : &stream_filters_hash);
}

/* API for registering GLOBAL filters */
HYSSAPI int hyss_stream_filter_register_factory(const char *filterpattern, const hyss_stream_filter_factory *factory)
{
	int ret;
	gear_string *str = gear_string_init_interned(filterpattern, strlen(filterpattern), 1);
	ret = gear_hash_add_ptr(&stream_filters_hash, str, (void*)factory) ? SUCCESS : FAILURE;
	gear_string_release_ex(str, 1);
	return ret;
}

HYSSAPI int hyss_stream_filter_unregister_factory(const char *filterpattern)
{
	return gear_hash_str_del(&stream_filters_hash, filterpattern, strlen(filterpattern));
}

/* API for registering VOLATILE wrappers */
HYSSAPI int hyss_stream_filter_register_factory_volatile(gear_string *filterpattern, const hyss_stream_filter_factory *factory)
{
	if (!FG(stream_filters)) {
		ALLOC_HASHTABLE(FG(stream_filters));
		gear_hash_init(FG(stream_filters), gear_hash_num_elements(&stream_filters_hash) + 1, NULL, NULL, 0);
		gear_hash_copy(FG(stream_filters), &stream_filters_hash, NULL);
	}

	return gear_hash_add_ptr(FG(stream_filters), filterpattern, (void*)factory) ? SUCCESS : FAILURE;
}

/* Buckets */

HYSSAPI hyss_stream_bucket *hyss_stream_bucket_new(hyss_stream *stream, char *buf, size_t buflen, uint8_t own_buf, uint8_t buf_persistent)
{
	int is_persistent = hyss_stream_is_persistent(stream);
	hyss_stream_bucket *bucket;

	bucket = (hyss_stream_bucket*)pemalloc(sizeof(hyss_stream_bucket), is_persistent);
	bucket->next = bucket->prev = NULL;

	if (is_persistent && !buf_persistent) {
		/* all data in a persistent bucket must also be persistent */
		bucket->buf = pemalloc(buflen, 1);
		memcpy(bucket->buf, buf, buflen);
		bucket->buflen = buflen;
		bucket->own_buf = 1;
	} else {
		bucket->buf = buf;
		bucket->buflen = buflen;
		bucket->own_buf = own_buf;
	}
	bucket->is_persistent = is_persistent;
	bucket->refcount = 1;
	bucket->brigade = NULL;

	return bucket;
}

/* Given a bucket, returns a version of that bucket with a writeable buffer.
 * If the original bucket has a refcount of 1 and owns its buffer, then it
 * is returned unchanged.
 * Otherwise, a copy of the buffer is made.
 * In both cases, the original bucket is unlinked from its brigade.
 * If a copy is made, the original bucket is delref'd.
 * */
HYSSAPI hyss_stream_bucket *hyss_stream_bucket_make_writeable(hyss_stream_bucket *bucket)
{
	hyss_stream_bucket *retval;

	hyss_stream_bucket_unlink(bucket);

	if (bucket->refcount == 1 && bucket->own_buf) {
		return bucket;
	}

	retval = (hyss_stream_bucket*)pemalloc(sizeof(hyss_stream_bucket), bucket->is_persistent);
	memcpy(retval, bucket, sizeof(*retval));

	retval->buf = pemalloc(retval->buflen, retval->is_persistent);
	memcpy(retval->buf, bucket->buf, retval->buflen);

	retval->refcount = 1;
	retval->own_buf = 1;

	hyss_stream_bucket_delref(bucket);

	return retval;
}

HYSSAPI int hyss_stream_bucket_split(hyss_stream_bucket *in, hyss_stream_bucket **left, hyss_stream_bucket **right, size_t length)
{
	*left = (hyss_stream_bucket*)pecalloc(1, sizeof(hyss_stream_bucket), in->is_persistent);
	*right = (hyss_stream_bucket*)pecalloc(1, sizeof(hyss_stream_bucket), in->is_persistent);

	(*left)->buf = pemalloc(length, in->is_persistent);
	(*left)->buflen = length;
	memcpy((*left)->buf, in->buf, length);
	(*left)->refcount = 1;
	(*left)->own_buf = 1;
	(*left)->is_persistent = in->is_persistent;

	(*right)->buflen = in->buflen - length;
	(*right)->buf = pemalloc((*right)->buflen, in->is_persistent);
	memcpy((*right)->buf, in->buf + length, (*right)->buflen);
	(*right)->refcount = 1;
	(*right)->own_buf = 1;
	(*right)->is_persistent = in->is_persistent;

	return SUCCESS;
}

HYSSAPI void hyss_stream_bucket_delref(hyss_stream_bucket *bucket)
{
	if (--bucket->refcount == 0) {
		if (bucket->own_buf) {
			pefree(bucket->buf, bucket->is_persistent);
		}
		pefree(bucket, bucket->is_persistent);
	}
}

HYSSAPI void hyss_stream_bucket_prepend(hyss_stream_bucket_brigade *brigade, hyss_stream_bucket *bucket)
{
	bucket->next = brigade->head;
	bucket->prev = NULL;

	if (brigade->head) {
		brigade->head->prev = bucket;
	} else {
		brigade->tail = bucket;
	}
	brigade->head = bucket;
	bucket->brigade = brigade;
}

HYSSAPI void hyss_stream_bucket_append(hyss_stream_bucket_brigade *brigade, hyss_stream_bucket *bucket)
{
	if (brigade->tail == bucket) {
		return;
	}

	bucket->prev = brigade->tail;
	bucket->next = NULL;

	if (brigade->tail) {
		brigade->tail->next = bucket;
	} else {
		brigade->head = bucket;
	}
	brigade->tail = bucket;
	bucket->brigade = brigade;
}

HYSSAPI void hyss_stream_bucket_unlink(hyss_stream_bucket *bucket)
{
	if (bucket->prev) {
		bucket->prev->next = bucket->next;
	} else if (bucket->brigade) {
		bucket->brigade->head = bucket->next;
	}
	if (bucket->next) {
		bucket->next->prev = bucket->prev;
	} else if (bucket->brigade) {
		bucket->brigade->tail = bucket->prev;
	}
	bucket->brigade = NULL;
	bucket->next = bucket->prev = NULL;
}








/* We allow very simple pattern matching for filter factories:
 * if "convert.charset.utf-8/sjis" is requested, we search first for an exact
 * match. If that fails, we try "convert.charset.*", then "convert.*"
 * This means that we don't need to clog up the hashtable with a zillion
 * charsets (for example) but still be able to provide them all as filters */
HYSSAPI hyss_stream_filter *hyss_stream_filter_create(const char *filtername, zval *filterparams, uint8_t persistent)
{
	HashTable *filter_hash = (FG(stream_filters) ? FG(stream_filters) : &stream_filters_hash);
	const hyss_stream_filter_factory *factory = NULL;
	hyss_stream_filter *filter = NULL;
	size_t n;
	char *period;

	n = strlen(filtername);

	if (NULL != (factory = gear_hash_str_find_ptr(filter_hash, filtername, n))) {
		filter = factory->create_filter(filtername, filterparams, persistent);
	} else if ((period = strrchr(filtername, '.'))) {
		/* try a wildcard */
		char *wildname;

		wildname = safe_emalloc(1, n, 3);
		memcpy(wildname, filtername, n+1);
		period = wildname + (period - filtername);
		while (period && !filter) {
			*period = '\0';
			strncat(wildname, ".*", 2);
			if (NULL != (factory = gear_hash_str_find_ptr(filter_hash, wildname, strlen(wildname)))) {
				filter = factory->create_filter(filtername, filterparams, persistent);
			}

			*period = '\0';
			period = strrchr(wildname, '.');
		}
		efree(wildname);
	}

	if (filter == NULL) {
		/* TODO: these need correct docrefs */
		if (factory == NULL)
			hyss_error_docref(NULL, E_WARNING, "unable to locate filter \"%s\"", filtername);
		else
			hyss_error_docref(NULL, E_WARNING, "unable to create or locate filter \"%s\"", filtername);
	}

	return filter;
}

HYSSAPI hyss_stream_filter *_hyss_stream_filter_alloc(const hyss_stream_filter_ops *fops, void *abstract, uint8_t persistent STREAMS_DC)
{
	hyss_stream_filter *filter;

	filter = (hyss_stream_filter*) pemalloc_rel_orig(sizeof(hyss_stream_filter), persistent);
	memset(filter, 0, sizeof(hyss_stream_filter));

	filter->fops = fops;
	Z_PTR(filter->abstract) = abstract;
	filter->is_persistent = persistent;

	return filter;
}

HYSSAPI void hyss_stream_filter_free(hyss_stream_filter *filter)
{
	if (filter->fops->dtor)
		filter->fops->dtor(filter);
	pefree(filter, filter->is_persistent);
}

HYSSAPI int hyss_stream_filter_prepend_ex(hyss_stream_filter_chain *chain, hyss_stream_filter *filter)
{
	filter->next = chain->head;
	filter->prev = NULL;

	if (chain->head) {
		chain->head->prev = filter;
	} else {
		chain->tail = filter;
	}
	chain->head = filter;
	filter->chain = chain;

	return SUCCESS;
}

HYSSAPI void _hyss_stream_filter_prepend(hyss_stream_filter_chain *chain, hyss_stream_filter *filter)
{
	hyss_stream_filter_prepend_ex(chain, filter);
}

HYSSAPI int hyss_stream_filter_append_ex(hyss_stream_filter_chain *chain, hyss_stream_filter *filter)
{
	hyss_stream *stream = chain->stream;

	filter->prev = chain->tail;
	filter->next = NULL;
	if (chain->tail) {
		chain->tail->next = filter;
	} else {
		chain->head = filter;
	}
	chain->tail = filter;
	filter->chain = chain;

	if (&(stream->readfilters) == chain && (stream->writepos - stream->readpos) > 0) {
		/* Let's going ahead and wind anything in the buffer through this filter */
		hyss_stream_bucket_brigade brig_in = { NULL, NULL }, brig_out = { NULL, NULL };
		hyss_stream_bucket_brigade *brig_inp = &brig_in, *brig_outp = &brig_out;
		hyss_stream_filter_status_t status;
		hyss_stream_bucket *bucket;
		size_t consumed = 0;

		bucket = hyss_stream_bucket_new(stream, (char*) stream->readbuf + stream->readpos, stream->writepos - stream->readpos, 0, 0);
		hyss_stream_bucket_append(brig_inp, bucket);
		status = filter->fops->filter(stream, filter, brig_inp, brig_outp, &consumed, PSFS_FLAG_NORMAL);

		if (stream->readpos + consumed > (uint32_t)stream->writepos) {
			/* No behaving filter should cause this. */
			status = PSFS_ERR_FATAL;
		}

		switch (status) {
			case PSFS_ERR_FATAL:
				while (brig_in.head) {
					bucket = brig_in.head;
					hyss_stream_bucket_unlink(bucket);
					hyss_stream_bucket_delref(bucket);
				}
				while (brig_out.head) {
					bucket = brig_out.head;
					hyss_stream_bucket_unlink(bucket);
					hyss_stream_bucket_delref(bucket);
				}
				hyss_error_docref(NULL, E_WARNING, "Filter failed to process pre-buffered data");
				return FAILURE;
			case PSFS_FEED_ME:
				/* We don't actually need data yet,
				   leave this filter in a feed me state until data is needed.
				   Reset stream's internal read buffer since the filter is "holding" it. */
				stream->readpos = 0;
				stream->writepos = 0;
				break;
			case PSFS_PASS_ON:
				/* If any data is consumed, we cannot rely upon the existing read buffer,
				   as the filtered data must replace the existing data, so invalidate the cache */
				/* note that changes here should be reflected in
				   main/streams/streams.c::hyss_stream_fill_read_buffer */
				stream->writepos = 0;
				stream->readpos = 0;

				while (brig_outp->head) {
					bucket = brig_outp->head;
					/* Grow buffer to hold this bucket if need be.
					   TODO: See warning in main/stream/streams.c::hyss_stream_fill_read_buffer */
					if (stream->readbuflen - stream->writepos < bucket->buflen) {
						stream->readbuflen += bucket->buflen;
						stream->readbuf = perealloc(stream->readbuf, stream->readbuflen, stream->is_persistent);
					}
					memcpy(stream->readbuf + stream->writepos, bucket->buf, bucket->buflen);
					stream->writepos += bucket->buflen;

					hyss_stream_bucket_unlink(bucket);
					hyss_stream_bucket_delref(bucket);
				}
				break;
		}
	}

	return SUCCESS;
}

HYSSAPI void _hyss_stream_filter_append(hyss_stream_filter_chain *chain, hyss_stream_filter *filter)
{
	if (hyss_stream_filter_append_ex(chain, filter) != SUCCESS) {
		if (chain->head == filter) {
			chain->head = NULL;
			chain->tail = NULL;
		} else {
			filter->prev->next = NULL;
			chain->tail = filter->prev;
		}
	}
}

HYSSAPI int _hyss_stream_filter_flush(hyss_stream_filter *filter, int finish)
{
	hyss_stream_bucket_brigade brig_a = { NULL, NULL }, brig_b = { NULL, NULL }, *inp = &brig_a, *outp = &brig_b, *brig_temp;
	hyss_stream_bucket *bucket;
	hyss_stream_filter_chain *chain;
	hyss_stream_filter *current;
	hyss_stream *stream;
	size_t flushed_size = 0;
	long flags = (finish ? PSFS_FLAG_FLUSH_CLOSE : PSFS_FLAG_FLUSH_INC);

	if (!filter->chain || !filter->chain->stream) {
		/* Filter is not attached to a chain, or chain is somehow not part of a stream */
		return FAILURE;
	}

	chain = filter->chain;
	stream = chain->stream;

	for(current = filter; current; current = current->next) {
		hyss_stream_filter_status_t status;

		status = filter->fops->filter(stream, current, inp, outp, NULL, flags);
		if (status == PSFS_FEED_ME) {
			/* We've flushed the data far enough */
			return SUCCESS;
		}
		if (status == PSFS_ERR_FATAL) {
			return FAILURE;
		}
		/* Otherwise we have data available to PASS_ON
			Swap the brigades and continue */
		brig_temp = inp;
		inp = outp;
		outp = brig_temp;
		outp->head = NULL;
		outp->tail = NULL;

		flags = PSFS_FLAG_NORMAL;
	}

	/* Last filter returned data via PSFS_PASS_ON
		Do something with it */

	for(bucket = inp->head; bucket; bucket = bucket->next) {
		flushed_size += bucket->buflen;
	}

	if (flushed_size == 0) {
		/* Unlikely, but possible */
		return SUCCESS;
	}

	if (chain == &(stream->readfilters)) {
		/* Dump any newly flushed data to the read buffer */
		if (stream->readpos > 0) {
			/* Back the buffer up */
			memcpy(stream->readbuf, stream->readbuf + stream->readpos, stream->writepos - stream->readpos);
			stream->readpos = 0;
			stream->writepos -= stream->readpos;
		}
		if (flushed_size > (stream->readbuflen - stream->writepos)) {
			/* Grow the buffer */
			stream->readbuf = perealloc(stream->readbuf, stream->writepos + flushed_size + stream->chunk_size, stream->is_persistent);
		}
		while ((bucket = inp->head)) {
			memcpy(stream->readbuf + stream->writepos, bucket->buf, bucket->buflen);
			stream->writepos += bucket->buflen;
			hyss_stream_bucket_unlink(bucket);
			hyss_stream_bucket_delref(bucket);
		}
	} else if (chain == &(stream->writefilters)) {
		/* Send flushed data to the stream */
		while ((bucket = inp->head)) {
			stream->ops->write(stream, bucket->buf, bucket->buflen);
			hyss_stream_bucket_unlink(bucket);
			hyss_stream_bucket_delref(bucket);
		}
	}

	return SUCCESS;
}

HYSSAPI hyss_stream_filter *hyss_stream_filter_remove(hyss_stream_filter *filter, int call_dtor)
{
	if (filter->prev) {
		filter->prev->next = filter->next;
	} else {
		filter->chain->head = filter->next;
	}
	if (filter->next) {
		filter->next->prev = filter->prev;
	} else {
		filter->chain->tail = filter->prev;
	}

	if (filter->res) {
		gear_list_delete(filter->res);
	}

	if (call_dtor) {
		hyss_stream_filter_free(filter);
		return NULL;
	}
	return filter;
}

