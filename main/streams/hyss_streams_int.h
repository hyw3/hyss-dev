/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if GEAR_DEBUG

#define emalloc_rel_orig(size)	\
		( __hyss_stream_call_depth == 0 \
		? _emalloc((size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_RELAY_CC) \
		: _emalloc((size) GEAR_FILE_LINE_CC GEAR_FILE_LINE_ORIG_RELAY_CC) )

#define erealloc_rel_orig(ptr, size)	\
		( __hyss_stream_call_depth == 0 \
		? _erealloc((ptr), (size), 0 GEAR_FILE_LINE_CC GEAR_FILE_LINE_RELAY_CC) \
		: _erealloc((ptr), (size), 0 GEAR_FILE_LINE_CC GEAR_FILE_LINE_ORIG_RELAY_CC) )

#define pemalloc_rel_orig(size, persistent)	((persistent) ? malloc((size)) : emalloc_rel_orig((size)))
#define perealloc_rel_orig(ptr, size, persistent)	((persistent) ? realloc((ptr), (size)) : erealloc_rel_orig((ptr), (size)))
#else
# define pemalloc_rel_orig(size, persistent)				pemalloc((size), (persistent))
# define perealloc_rel_orig(ptr, size, persistent)			perealloc((ptr), (size), (persistent))
# define emalloc_rel_orig(size)								emalloc((size))
#endif

#define STREAM_DEBUG 0
#define STREAM_WRAPPER_PLAIN_FILES	((hyss_stream_wrapper*)-1)

#ifndef MAP_FAILED
#define MAP_FAILED ((void *) -1)
#endif

#define CHUNK_SIZE	8192

#ifdef HYSS_WIN32
# ifdef EWOULDBLOCK
#  undef EWOULDBLOCK
# endif
# define EWOULDBLOCK WSAEWOULDBLOCK
# ifdef EMSGSIZE
#  undef EMSGSIZE
# endif
# define EMSGSIZE WSAEMSGSIZE
#endif

/* This functions transforms the first char to 'w' if it's not 'r', 'a' or 'w'
 * and strips any subsequent chars except '+' and 'b'.
 * Use this to sanitize stream->mode if you call e.g. fdopen, fopencookie or
 * any other function that expects standard modes and you allow non-standard
 * ones. result should be a char[5]. */
void hyss_stream_mode_sanitize_fdopen_fopencookie(hyss_stream *stream, char *result);

void hyss_stream_tidy_wrapper_error_log(hyss_stream_wrapper *wrapper);
void hyss_stream_display_wrapper_errors(hyss_stream_wrapper *wrapper, const char *path, const char *caption);
