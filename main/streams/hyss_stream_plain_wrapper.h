/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* definitions for the plain files wrapper */

/* operations for a plain file; use the hyss_stream_fopen_XXX funcs below */
HYSSAPI extern hyss_stream_ops hyss_stream_stdio_ops;
HYSSAPI extern /*const*/ hyss_stream_wrapper hyss_plain_files_wrapper;

BEGIN_EXTERN_C()

/* like fopen, but returns a stream */
HYSSAPI hyss_stream *_hyss_stream_fopen(const char *filename, const char *mode, gear_string **opened_path, int options STREAMS_DC);
#define hyss_stream_fopen(filename, mode, opened)	_hyss_stream_fopen((filename), (mode), (opened), 0 STREAMS_CC)

HYSSAPI hyss_stream *_hyss_stream_fopen_with_path(const char *filename, const char *mode, const char *path, gear_string **opened_path, int options STREAMS_DC);
#define hyss_stream_fopen_with_path(filename, mode, path, opened)	_hyss_stream_fopen_with_path((filename), (mode), (path), (opened), 0 STREAMS_CC)

HYSSAPI hyss_stream *_hyss_stream_fopen_from_file(FILE *file, const char *mode STREAMS_DC);
#define hyss_stream_fopen_from_file(file, mode)	_hyss_stream_fopen_from_file((file), (mode) STREAMS_CC)

HYSSAPI hyss_stream *_hyss_stream_fopen_from_fd(int fd, const char *mode, const char *persistent_id STREAMS_DC);
#define hyss_stream_fopen_from_fd(fd, mode, persistent_id)	_hyss_stream_fopen_from_fd((fd), (mode), (persistent_id) STREAMS_CC)

HYSSAPI hyss_stream *_hyss_stream_fopen_from_pipe(FILE *file, const char *mode STREAMS_DC);
#define hyss_stream_fopen_from_pipe(file, mode)	_hyss_stream_fopen_from_pipe((file), (mode) STREAMS_CC)

HYSSAPI hyss_stream *_hyss_stream_fopen_tmpfile(int dummy STREAMS_DC);
#define hyss_stream_fopen_tmpfile()	_hyss_stream_fopen_tmpfile(0 STREAMS_CC)

HYSSAPI hyss_stream *_hyss_stream_fopen_temporary_file(const char *dir, const char *pfx, gear_string **opened_path STREAMS_DC);
#define hyss_stream_fopen_temporary_file(dir, pfx, opened_path)	_hyss_stream_fopen_temporary_file((dir), (pfx), (opened_path) STREAMS_CC)

/* This is a utility API for extensions that are opening a stream, converting it
 * to a FILE* and then closing it again.  Be warned that fileno() on the result
 * will most likely fail on systems with fopencookie. */
HYSSAPI FILE * _hyss_stream_open_wrapper_as_file(char * path, char * mode, int options, gear_string **opened_path STREAMS_DC);
#define hyss_stream_open_wrapper_as_file(path, mode, options, opened_path) _hyss_stream_open_wrapper_as_file((path), (mode), (options), (opened_path) STREAMS_CC)

/* parse standard "fopen" modes into open() flags */
HYSSAPI int hyss_stream_parse_fopen_modes(const char *mode, int *open_flags);

END_EXTERN_C()

