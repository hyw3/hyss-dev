/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_streams_int.h"

#ifdef HAVE_GLOB
# ifndef HYSS_WIN32
#  include <glob.h>
# else
#  include "win32/glob.h"
# endif
#endif

#ifdef HAVE_GLOB
#ifndef GLOB_ONLYDIR
#define GLOB_ONLYDIR (1<<30)
#define GLOB_FLAGMASK (~GLOB_ONLYDIR)
#else
#define GLOB_FLAGMASK (~0)
#endif

typedef struct {
	glob_t   glob;
	size_t   index;
	int      flags;
	char     *path;
	size_t   path_len;
	char     *pattern;
	size_t   pattern_len;
} glob_s_t;

HYSSAPI char* _hyss_glob_stream_get_path(hyss_stream *stream, int copy, size_t *plen STREAMS_DC) /* {{{ */
{
	glob_s_t *pglob = (glob_s_t *)stream->abstract;

	if (pglob && pglob->path) {
		if (plen) {
			*plen = pglob->path_len;
		}
		if (copy) {
			return estrndup(pglob->path, pglob->path_len);
		} else {
			return pglob->path;
		}
	} else {
		if (plen) {
			*plen = 0;
		}
		return NULL;
	}
}
/* }}} */

HYSSAPI char* _hyss_glob_stream_get_pattern(hyss_stream *stream, int copy, size_t *plen STREAMS_DC) /* {{{ */
{
	glob_s_t *pglob = (glob_s_t *)stream->abstract;

	if (pglob && pglob->pattern) {
		if (plen) {
			*plen = pglob->pattern_len;
		}
		if (copy) {
			return estrndup(pglob->pattern, pglob->pattern_len);
		} else {
			return pglob->pattern;
		}
	} else {
		if (plen) {
			*plen = 0;
		}
		return NULL;
	}
}
/* }}} */

HYSSAPI int _hyss_glob_stream_get_count(hyss_stream *stream, int *pflags STREAMS_DC) /* {{{ */
{
	glob_s_t *pglob = (glob_s_t *)stream->abstract;

	if (pglob) {
		if (pflags) {
			*pflags = pglob->flags;
		}
		return pglob->glob.gl_pathc;
	} else {
		if (pflags) {
			*pflags = 0;
		}
		return 0;
	}
}
/* }}} */

static void hyss_glob_stream_path_split(glob_s_t *pglob, const char *path, int get_path, const char **p_file) /* {{{ */
{
	const char *pos, *gpath = path;

	if ((pos = strrchr(path, '/')) != NULL) {
		path = pos+1;
	}
#ifdef HYSS_WIN32
	if ((pos = strrchr(path, '\\')) != NULL) {
		path = pos+1;
	}
#endif

	*p_file = path;

	if (get_path) {
		if (pglob->path) {
			efree(pglob->path);
		}
		if ((path - gpath) > 1) {
			path--;
		}
		pglob->path_len = path - gpath;
		pglob->path = estrndup(gpath, pglob->path_len);
	}
}
/* }}} */

static size_t hyss_glob_stream_read(hyss_stream *stream, char *buf, size_t count) /* {{{ */
{
	glob_s_t *pglob = (glob_s_t *)stream->abstract;
	hyss_stream_dirent *ent = (hyss_stream_dirent*)buf;
	const char *path;

	/* avoid problems if someone mis-uses the stream */
	if (count == sizeof(hyss_stream_dirent) && pglob) {
		if (pglob->index < (size_t)pglob->glob.gl_pathc) {
			hyss_glob_stream_path_split(pglob, pglob->glob.gl_pathv[pglob->index++], pglob->flags & GLOB_APPEND, &path);
			HYSS_STRLCPY(ent->d_name, path, sizeof(ent->d_name), strlen(path));
			return sizeof(hyss_stream_dirent);
		}
		pglob->index = pglob->glob.gl_pathc;
		if (pglob->path) {
			efree(pglob->path);
			pglob->path = NULL;
		}
	}

	return 0;
}
/* }}} */

static int hyss_glob_stream_close(hyss_stream *stream, int close_handle)  /* {{{ */
{
	glob_s_t *pglob = (glob_s_t *)stream->abstract;

	if (pglob) {
		pglob->index = 0;
		globfree(&pglob->glob);
		if (pglob->path) {
			efree(pglob->path);
		}
		if (pglob->pattern) {
			efree(pglob->pattern);
		}
	}
	efree(stream->abstract);
	return 0;
}
/* {{{ */

static int hyss_glob_stream_rewind(hyss_stream *stream, gear_off_t offset, int whence, gear_off_t *newoffs) /* {{{ */
{
	glob_s_t *pglob = (glob_s_t *)stream->abstract;

	if (pglob) {
		pglob->index = 0;
		if (pglob->path) {
			efree(pglob->path);
			pglob->path = NULL;
		}
	}
	return 0;
}
/* }}} */

const hyss_stream_ops  hyss_glob_stream_ops = {
	NULL, hyss_glob_stream_read,
	hyss_glob_stream_close, NULL,
	"glob",
	hyss_glob_stream_rewind,
	NULL, /* cast */
	NULL, /* stat */
	NULL  /* set_option */
};

 /* {{{ hyss_glob_stream_opener */
static hyss_stream *hyss_glob_stream_opener(hyss_stream_wrapper *wrapper, const char *path, const char *mode,
		int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC)
{
	glob_s_t *pglob;
	int ret;
	const char *tmp, *pos;

	if (!strncmp(path, "glob://", sizeof("glob://")-1)) {
		path += sizeof("glob://")-1;
		if (opened_path) {
			*opened_path = gear_string_init(path, strlen(path), 0);
		}
	}

	if (((options & STREAM_DISABLE_OPEN_BASEDIR) == 0) && hyss_check_open_basedir(path)) {
		return NULL;
	}

	pglob = ecalloc(sizeof(*pglob), 1);

	if (0 != (ret = glob(path, pglob->flags & GLOB_FLAGMASK, NULL, &pglob->glob))) {
#ifdef GLOB_NOMATCH
		if (GLOB_NOMATCH != ret)
#endif
		{
			efree(pglob);
			return NULL;
		}
	}

	pos = path;
	if ((tmp = strrchr(pos, '/')) != NULL) {
		pos = tmp+1;
	}
#ifdef HYSS_WIN32
	if ((tmp = strrchr(pos, '\\')) != NULL) {
		pos = tmp+1;
	}
#endif

	pglob->pattern_len = strlen(pos);
	pglob->pattern = estrndup(pos, pglob->pattern_len);

	pglob->flags |= GLOB_APPEND;

	if (pglob->glob.gl_pathc) {
		hyss_glob_stream_path_split(pglob, pglob->glob.gl_pathv[0], 1, &tmp);
	} else {
		hyss_glob_stream_path_split(pglob, path, 1, &tmp);
	}

	return hyss_stream_alloc(&hyss_glob_stream_ops, pglob, 0, mode);
}
/* }}} */

static const hyss_stream_wrapper_ops  hyss_glob_stream_wrapper_ops = {
	NULL,
	NULL,
	NULL,
	NULL,
	hyss_glob_stream_opener,
	"glob",
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

const hyss_stream_wrapper  hyss_glob_stream_wrapper = {
	&hyss_glob_stream_wrapper_ops,
	NULL,
	0
};
#endif /* HAVE_GLOB */

