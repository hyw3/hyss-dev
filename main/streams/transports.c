/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_streams_int.h"
#include "extslib/standard/file.h"

static HashTable xport_hash;

HYSSAPI HashTable *hyss_stream_xport_get_hash(void)
{
	return &xport_hash;
}

HYSSAPI int hyss_stream_xport_register(const char *protocol, hyss_stream_transport_factory factory)
{
	gear_string *str = gear_string_init_interned(protocol, strlen(protocol), 1);

	gear_hash_update_ptr(&xport_hash, str, factory);
	gear_string_release_ex(str, 1);
	return SUCCESS;
}

HYSSAPI int hyss_stream_xport_unregister(const char *protocol)
{
	return gear_hash_str_del(&xport_hash, protocol, strlen(protocol));
}

#define ERR_REPORT(out_err, fmt, arg) \
	if (out_err) { *out_err = strpprintf(0, fmt, arg); } \
	else { hyss_error_docref(NULL, E_WARNING, fmt, arg); }

#define ERR_RETURN(out_err, local_err, fmt) \
	if (out_err) { *out_err = local_err; } \
	else { hyss_error_docref(NULL, E_WARNING, fmt, local_err ? ZSTR_VAL(local_err) : "Unspecified error"); \
		if (local_err) { gear_string_release_ex(local_err, 0); local_err = NULL; } \
	}

HYSSAPI hyss_stream *_hyss_stream_xport_create(const char *name, size_t namelen, int options,
		int flags, const char *persistent_id,
		struct timeval *timeout,
		hyss_stream_context *context,
		gear_string **error_string,
		int *error_code
		STREAMS_DC)
{
	hyss_stream *stream = NULL;
	hyss_stream_transport_factory factory = NULL;
	const char *p, *protocol = NULL;
	size_t n = 0;
	int failed = 0;
	gear_string *error_text = NULL;
	struct timeval default_timeout = { 0, 0 };

	default_timeout.tv_sec = FG(default_socket_timeout);

	if (timeout == NULL) {
		timeout = &default_timeout;
	}

	/* check for a cached persistent socket */
	if (persistent_id) {
		switch(hyss_stream_from_persistent_id(persistent_id, &stream)) {
			case HYSS_STREAM_PERSISTENT_SUCCESS:
				/* use a 0 second timeout when checking if the socket
				 * has already died */
				if (HYSS_STREAM_OPTION_RETURN_OK == hyss_stream_set_option(stream, HYSS_STREAM_OPTION_CHECK_LIVENESS, 0, NULL)) {
					return stream;
				}
				/* dead - kill it */
				hyss_stream_pclose(stream);
				stream = NULL;

				/* fall through */

			case HYSS_STREAM_PERSISTENT_FAILURE:
			default:
				/* failed; get a new one */
				;
		}
	}

	for (p = name; isalnum((int)*p) || *p == '+' || *p == '-' || *p == '.'; p++) {
		n++;
	}

	if ((*p == ':') && (n > 1) && !strncmp("://", p, 3)) {
		protocol = name;
		name = p + 3;
		namelen -= n + 3;
	} else {
		protocol = "tcp";
		n = 3;
	}

	if (protocol) {
		if (NULL == (factory = gear_hash_str_find_ptr(&xport_hash, protocol, n))) {
			char wrapper_name[32];

			if (n >= sizeof(wrapper_name))
				n = sizeof(wrapper_name) - 1;
			HYSS_STRLCPY(wrapper_name, protocol, sizeof(wrapper_name), n);

			ERR_REPORT(error_string, "Unable to find the socket transport \"%s\" - did you forget to enable it when you configured HYSS?",
					wrapper_name);

			return NULL;
		}
	}

	if (factory == NULL) {
		/* should never happen */
		hyss_error_docref(NULL, E_WARNING, "Could not find a factory !?");
		return NULL;
	}

	stream = (factory)(protocol, n,
			(char*)name, namelen, persistent_id, options, flags, timeout,
			context STREAMS_REL_CC);

	if (stream) {
		hyss_stream_context_set(stream, context);

		if ((flags & STREAM_XPORT_SERVER) == 0) {
			/* client */

			if (flags & (STREAM_XPORT_CONNECT|STREAM_XPORT_CONNECT_ASYNC)) {
				if (-1 == hyss_stream_xport_connect(stream, name, namelen,
							flags & STREAM_XPORT_CONNECT_ASYNC ? 1 : 0,
							timeout, &error_text, error_code)) {

					ERR_RETURN(error_string, error_text, "connect() failed: %s");

					failed = 1;
				}
			}

		} else {
			/* server */
			if (flags & STREAM_XPORT_BIND) {
				if (0 != hyss_stream_xport_bind(stream, name, namelen, &error_text)) {
					ERR_RETURN(error_string, error_text, "bind() failed: %s");
					failed = 1;
				} else if (flags & STREAM_XPORT_LISTEN) {
					zval *zbacklog = NULL;
					int backlog = 32;

					if (HYSS_STREAM_CONTEXT(stream) && (zbacklog = hyss_stream_context_get_option(HYSS_STREAM_CONTEXT(stream), "socket", "backlog")) != NULL) {
						backlog = zval_get_long(zbacklog);
					}

					if (0 != hyss_stream_xport_listen(stream, backlog, &error_text)) {
						ERR_RETURN(error_string, error_text, "listen() failed: %s");
						failed = 1;
					}
				}
			}
		}
	}

	if (failed) {
		/* failure means that they don't get a stream to play with */
		if (persistent_id) {
			hyss_stream_pclose(stream);
		} else {
			hyss_stream_close(stream);
		}
		stream = NULL;
	}

	return stream;
}

/* Bind the stream to a local address */
HYSSAPI int hyss_stream_xport_bind(hyss_stream *stream,
		const char *name, size_t namelen,
		gear_string **error_text
		)
{
	hyss_stream_xport_param param;
	int ret;

	memset(&param, 0, sizeof(param));
	param.op = STREAM_XPORT_OP_BIND;
	param.inputs.name = (char*)name;
	param.inputs.namelen = namelen;
	param.want_errortext = error_text ? 1 : 0;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_XPORT_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		if (error_text) {
			*error_text = param.outputs.error_text;
		}

		return param.outputs.returncode;
	}

	return ret;
}

/* Connect to a remote address */
HYSSAPI int hyss_stream_xport_connect(hyss_stream *stream,
		const char *name, size_t namelen,
		int asynchronous,
		struct timeval *timeout,
		gear_string **error_text,
		int *error_code
		)
{
	hyss_stream_xport_param param;
	int ret;

	memset(&param, 0, sizeof(param));
	param.op = asynchronous ? STREAM_XPORT_OP_CONNECT_ASYNC: STREAM_XPORT_OP_CONNECT;
	param.inputs.name = (char*)name;
	param.inputs.namelen = namelen;
	param.inputs.timeout = timeout;

	param.want_errortext = error_text ? 1 : 0;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_XPORT_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		if (error_text) {
			*error_text = param.outputs.error_text;
		}
		if (error_code) {
			*error_code = param.outputs.error_code;
		}
		return param.outputs.returncode;
	}

	return ret;

}

/* Prepare to listen */
HYSSAPI int hyss_stream_xport_listen(hyss_stream *stream, int backlog, gear_string **error_text)
{
	hyss_stream_xport_param param;
	int ret;

	memset(&param, 0, sizeof(param));
	param.op = STREAM_XPORT_OP_LISTEN;
	param.inputs.backlog = backlog;
	param.want_errortext = error_text ? 1 : 0;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_XPORT_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		if (error_text) {
			*error_text = param.outputs.error_text;
		}

		return param.outputs.returncode;
	}

	return ret;
}

/* Get the next client and their address (as a string) */
HYSSAPI int hyss_stream_xport_accept(hyss_stream *stream, hyss_stream **client,
		gear_string **textaddr,
		void **addr, socklen_t *addrlen,
		struct timeval *timeout,
		gear_string **error_text
		)
{
	hyss_stream_xport_param param;
	int ret;

	memset(&param, 0, sizeof(param));

	param.op = STREAM_XPORT_OP_ACCEPT;
	param.inputs.timeout = timeout;
	param.want_addr = addr ? 1 : 0;
	param.want_textaddr = textaddr ? 1 : 0;
	param.want_errortext = error_text ? 1 : 0;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_XPORT_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		*client = param.outputs.client;
		if (addr) {
			*addr = param.outputs.addr;
			*addrlen = param.outputs.addrlen;
		}
		if (textaddr) {
			*textaddr = param.outputs.textaddr;
		}
		if (error_text) {
			*error_text = param.outputs.error_text;
		}

		return param.outputs.returncode;
	}
	return ret;
}

HYSSAPI int hyss_stream_xport_get_name(hyss_stream *stream, int want_peer,
		gear_string **textaddr,
		void **addr, socklen_t *addrlen
		)
{
	hyss_stream_xport_param param;
	int ret;

	memset(&param, 0, sizeof(param));

	param.op = want_peer ? STREAM_XPORT_OP_GET_PEER_NAME : STREAM_XPORT_OP_GET_NAME;
	param.want_addr = addr ? 1 : 0;
	param.want_textaddr = textaddr ? 1 : 0;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_XPORT_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		if (addr) {
			*addr = param.outputs.addr;
			*addrlen = param.outputs.addrlen;
		}
		if (textaddr) {
			*textaddr = param.outputs.textaddr;
		}

		return param.outputs.returncode;
	}
	return ret;
}

HYSSAPI int hyss_stream_xport_crypto_setup(hyss_stream *stream, hyss_stream_xport_crypt_method_t crypto_method, hyss_stream *session_stream)
{
	hyss_stream_xport_crypto_param param;
	int ret;

	memset(&param, 0, sizeof(param));
	param.op = STREAM_XPORT_CRYPTO_OP_SETUP;
	param.inputs.method = crypto_method;
	param.inputs.session = session_stream;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_CRYPTO_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		return param.outputs.returncode;
	}

	hyss_error_docref("streams.crypto", E_WARNING, "this stream does not support SSL/crypto");

	return ret;
}

HYSSAPI int hyss_stream_xport_crypto_enable(hyss_stream *stream, int activate)
{
	hyss_stream_xport_crypto_param param;
	int ret;

	memset(&param, 0, sizeof(param));
	param.op = STREAM_XPORT_CRYPTO_OP_ENABLE;
	param.inputs.activate = activate;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_CRYPTO_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		return param.outputs.returncode;
	}

	hyss_error_docref("streams.crypto", E_WARNING, "this stream does not support SSL/crypto");

	return ret;
}

/* Similar to recv() system call; read data from the stream, optionally
 * peeking, optionally retrieving OOB data */
HYSSAPI int hyss_stream_xport_recvfrom(hyss_stream *stream, char *buf, size_t buflen,
		int flags, void **addr, socklen_t *addrlen, gear_string **textaddr
		)
{
	hyss_stream_xport_param param;
	int ret = 0;
	int recvd_len = 0;
#if 0
	int oob;

	if (flags == 0 && addr == NULL) {
		return hyss_stream_read(stream, buf, buflen);
	}

	if (stream->readfilters.head) {
		hyss_error_docref(NULL, E_WARNING, "cannot peek or fetch OOB data from a filtered stream");
		return -1;
	}

	oob = (flags & STREAM_OOB) == STREAM_OOB;

	if (!oob && addr == NULL) {
		/* must be peeking at regular data; copy content from the buffer
		 * first, then adjust the pointer/len before handing off to the
		 * stream */
		recvd_len = stream->writepos - stream->readpos;
		if (recvd_len > buflen) {
			recvd_len = buflen;
		}
		if (recvd_len) {
			memcpy(buf, stream->readbuf, recvd_len);
			buf += recvd_len;
			buflen -= recvd_len;
		}
		/* if we filled their buffer, return */
		if (buflen == 0) {
			return recvd_len;
		}
	}
#endif

	/* otherwise, we are going to bypass the buffer */

	memset(&param, 0, sizeof(param));

	param.op = STREAM_XPORT_OP_RECV;
	param.want_addr = addr ? 1 : 0;
	param.want_textaddr = textaddr ? 1 : 0;
	param.inputs.buf = buf;
	param.inputs.buflen = buflen;
	param.inputs.flags = flags;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_XPORT_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		if (addr) {
			*addr = param.outputs.addr;
			*addrlen = param.outputs.addrlen;
		}
		if (textaddr) {
			*textaddr = param.outputs.textaddr;
		}
		return recvd_len + param.outputs.returncode;
	}
	return recvd_len ? recvd_len : -1;
}

/* Similar to send() system call; send data to the stream, optionally
 * sending it as OOB data */
HYSSAPI int hyss_stream_xport_sendto(hyss_stream *stream, const char *buf, size_t buflen,
		int flags, void *addr, socklen_t addrlen)
{
	hyss_stream_xport_param param;
	int ret = 0;
	int oob;

#if 0
	if (flags == 0 && addr == NULL) {
		return hyss_stream_write(stream, buf, buflen);
	}
#endif

	oob = (flags & STREAM_OOB) == STREAM_OOB;

	if ((oob || addr) && stream->writefilters.head) {
		hyss_error_docref(NULL, E_WARNING, "cannot write OOB data, or data to a targeted address on a filtered stream");
		return -1;
	}

	memset(&param, 0, sizeof(param));

	param.op = STREAM_XPORT_OP_SEND;
	param.want_addr = addr ? 1 : 0;
	param.inputs.buf = (char*)buf;
	param.inputs.buflen = buflen;
	param.inputs.flags = flags;
	param.inputs.addr = addr;
	param.inputs.addrlen = addrlen;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_XPORT_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		return param.outputs.returncode;
	}
	return -1;
}

/* Similar to shutdown() system call; shut down part of a full-duplex
 * connection */
HYSSAPI int hyss_stream_xport_shutdown(hyss_stream *stream, stream_shutdown_t how)
{
	hyss_stream_xport_param param;
	int ret = 0;

	memset(&param, 0, sizeof(param));

	param.op = STREAM_XPORT_OP_SHUTDOWN;
	param.how = how;

	ret = hyss_stream_set_option(stream, HYSS_STREAM_OPTION_XPORT_API, 0, &param);

	if (ret == HYSS_STREAM_OPTION_RETURN_OK) {
		return param.outputs.returncode;
	}
	return -1;
}

