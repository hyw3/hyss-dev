/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* The filter API works on the principle of "Bucket-Brigades".  This is
 * partially inspired by the cLHy method of doing things, although
 * it is intentially a light-weight implementation.
 *
 * Each stream can have a chain of filters for reading and another for writing.
 *
 * When data is written to the stream, it is placed into a bucket and placed at
 * the start of the input brigade.
 *
 * The first filter in the chain is invoked on the brigade and (depending on
 * it's return value), the next filter is invoked and so on.
 * */

#define HYSS_STREAM_FILTER_READ	0x0001
#define HYSS_STREAM_FILTER_WRITE	0x0002
#define HYSS_STREAM_FILTER_ALL	(HYSS_STREAM_FILTER_READ | HYSS_STREAM_FILTER_WRITE)

typedef struct _hyss_stream_bucket			hyss_stream_bucket;
typedef struct _hyss_stream_bucket_brigade	hyss_stream_bucket_brigade;

struct _hyss_stream_bucket {
	hyss_stream_bucket *next, *prev;
	hyss_stream_bucket_brigade *brigade;

	char *buf;
	size_t buflen;
	/* if non-zero, buf should be pefreed when the bucket is destroyed */
	uint8_t own_buf;
	uint8_t is_persistent;

	/* destroy this struct when refcount falls to zero */
	int refcount;
};

struct _hyss_stream_bucket_brigade {
	hyss_stream_bucket *head, *tail;
};

typedef enum {
	PSFS_ERR_FATAL,	/* error in data stream */
	PSFS_FEED_ME,	/* filter needs more data; stop processing chain until more is available */
	PSFS_PASS_ON	/* filter generated output buckets; pass them on to next in chain */
} hyss_stream_filter_status_t;

/* Buckets API. */
BEGIN_EXTERN_C()
HYSSAPI hyss_stream_bucket *hyss_stream_bucket_new(hyss_stream *stream, char *buf, size_t buflen, uint8_t own_buf, uint8_t buf_persistent);
HYSSAPI int hyss_stream_bucket_split(hyss_stream_bucket *in, hyss_stream_bucket **left, hyss_stream_bucket **right, size_t length);
HYSSAPI void hyss_stream_bucket_delref(hyss_stream_bucket *bucket);
#define hyss_stream_bucket_addref(bucket)	(bucket)->refcount++
HYSSAPI void hyss_stream_bucket_prepend(hyss_stream_bucket_brigade *brigade, hyss_stream_bucket *bucket);
HYSSAPI void hyss_stream_bucket_append(hyss_stream_bucket_brigade *brigade, hyss_stream_bucket *bucket);
HYSSAPI void hyss_stream_bucket_unlink(hyss_stream_bucket *bucket);
HYSSAPI hyss_stream_bucket *hyss_stream_bucket_make_writeable(hyss_stream_bucket *bucket);
END_EXTERN_C()

#define PSFS_FLAG_NORMAL		0	/* regular read/write */
#define PSFS_FLAG_FLUSH_INC		1	/* an incremental flush */
#define PSFS_FLAG_FLUSH_CLOSE	2	/* final flush prior to closing */

typedef struct _hyss_stream_filter_ops {

	hyss_stream_filter_status_t (*filter)(
			hyss_stream *stream,
			hyss_stream_filter *thisfilter,
			hyss_stream_bucket_brigade *buckets_in,
			hyss_stream_bucket_brigade *buckets_out,
			size_t *bytes_consumed,
			int flags
			);

	void (*dtor)(hyss_stream_filter *thisfilter);

	const char *label;

} hyss_stream_filter_ops;

typedef struct _hyss_stream_filter_chain {
	hyss_stream_filter *head, *tail;

	/* Owning stream */
	hyss_stream *stream;
} hyss_stream_filter_chain;

struct _hyss_stream_filter {
	const hyss_stream_filter_ops *fops;
	zval abstract; /* for use by filter implementation */
	hyss_stream_filter *next;
	hyss_stream_filter *prev;
	int is_persistent;

	/* link into stream and chain */
	hyss_stream_filter_chain *chain;

	/* buffered buckets */
	hyss_stream_bucket_brigade buffer;

	/* filters are auto_registered when they're applied */
	gear_resource *res;
};

/* stack filter onto a stream */
BEGIN_EXTERN_C()
HYSSAPI void _hyss_stream_filter_prepend(hyss_stream_filter_chain *chain, hyss_stream_filter *filter);
HYSSAPI int hyss_stream_filter_prepend_ex(hyss_stream_filter_chain *chain, hyss_stream_filter *filter);
HYSSAPI void _hyss_stream_filter_append(hyss_stream_filter_chain *chain, hyss_stream_filter *filter);
HYSSAPI int hyss_stream_filter_append_ex(hyss_stream_filter_chain *chain, hyss_stream_filter *filter);
HYSSAPI int _hyss_stream_filter_flush(hyss_stream_filter *filter, int finish);
HYSSAPI hyss_stream_filter *hyss_stream_filter_remove(hyss_stream_filter *filter, int call_dtor);
HYSSAPI void hyss_stream_filter_free(hyss_stream_filter *filter);
HYSSAPI hyss_stream_filter *_hyss_stream_filter_alloc(const hyss_stream_filter_ops *fops, void *abstract, uint8_t persistent STREAMS_DC);
END_EXTERN_C()
#define hyss_stream_filter_alloc(fops, thisptr, persistent) _hyss_stream_filter_alloc((fops), (thisptr), (persistent) STREAMS_CC)
#define hyss_stream_filter_alloc_rel(fops, thisptr, persistent) _hyss_stream_filter_alloc((fops), (thisptr), (persistent) STREAMS_REL_CC)
#define hyss_stream_filter_prepend(chain, filter) _hyss_stream_filter_prepend((chain), (filter))
#define hyss_stream_filter_append(chain, filter) _hyss_stream_filter_append((chain), (filter))
#define hyss_stream_filter_flush(filter, finish) _hyss_stream_filter_flush((filter), (finish))

#define hyss_stream_is_filtered(stream)	((stream)->readfilters.head || (stream)->writefilters.head)

typedef struct _hyss_stream_filter_factory {
	hyss_stream_filter *(*create_filter)(const char *filtername, zval *filterparams, uint8_t persistent);
} hyss_stream_filter_factory;

BEGIN_EXTERN_C()
HYSSAPI int hyss_stream_filter_register_factory(const char *filterpattern, const hyss_stream_filter_factory *factory);
HYSSAPI int hyss_stream_filter_unregister_factory(const char *filterpattern);
HYSSAPI int hyss_stream_filter_register_factory_volatile(gear_string *filterpattern, const hyss_stream_filter_factory *factory);
HYSSAPI hyss_stream_filter *hyss_stream_filter_create(const char *filtername, zval *filterparams, uint8_t persistent);
END_EXTERN_C()

