/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Memory Mapping interface for streams.
 * The intention is to provide a uniform interface over the most common
 * operations that are used within HYSS itself, rather than a complete
 * API for all memory mapping needs.
 *
 * ATM, we support only mmap(), but win32 memory mapping support will
 * follow soon.
 * */

typedef enum {
	/* Does the stream support mmap ? */
	HYSS_STREAM_MMAP_SUPPORTED,
	/* Request a range and offset to be mapped;
	 * while mapped, you MUST NOT use any read/write functions
	 * on the stream (win9x compatibility) */
	HYSS_STREAM_MMAP_MAP_RANGE,
	/* Unmap the last range that was mapped for the stream */
	HYSS_STREAM_MMAP_UNMAP
} hyss_stream_mmap_operation_t;

typedef enum {
	HYSS_STREAM_MAP_MODE_READONLY,
	HYSS_STREAM_MAP_MODE_READWRITE,
	HYSS_STREAM_MAP_MODE_SHARED_READONLY,
	HYSS_STREAM_MAP_MODE_SHARED_READWRITE
} hyss_stream_mmap_access_t;

typedef struct {
	/* requested offset and length.
	 * If length is 0, the whole file is mapped */
	size_t offset;
	size_t length;

	hyss_stream_mmap_access_t mode;

	/* returned mapped address */
	char *mapped;

} hyss_stream_mmap_range;

#define HYSS_STREAM_MMAP_ALL 0

#define hyss_stream_mmap_supported(stream)	(_hyss_stream_set_option((stream), HYSS_STREAM_OPTION_MMAP_API, HYSS_STREAM_MMAP_SUPPORTED, NULL) == 0 ? 1 : 0)

/* Returns 1 if the stream in its current state can be memory mapped,
 * 0 otherwise */
#define hyss_stream_mmap_possible(stream)			(!hyss_stream_is_filtered((stream)) && hyss_stream_mmap_supported((stream)))

BEGIN_EXTERN_C()
HYSSAPI char *_hyss_stream_mmap_range(hyss_stream *stream, size_t offset, size_t length, hyss_stream_mmap_access_t mode, size_t *mapped_len);
#define hyss_stream_mmap_range(stream, offset, length, mode, mapped_len)	_hyss_stream_mmap_range((stream), (offset), (length), (mode), (mapped_len))

/* un-maps the last mapped range */
HYSSAPI int _hyss_stream_mmap_unmap(hyss_stream *stream);
#define hyss_stream_mmap_unmap(stream)				_hyss_stream_mmap_unmap((stream))

HYSSAPI int _hyss_stream_mmap_unmap_ex(hyss_stream *stream, gear_off_t readden);
#define hyss_stream_mmap_unmap_ex(stream, readden)			_hyss_stream_mmap_unmap_ex((stream), (readden))
END_EXTERN_C()

