/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include "hyss.h"
#include "extslib/standard/base64.h"

HYSSAPI size_t hyss_url_decode(char *str, size_t len);

/* Memory streams use a dynamic memory buffer to emulate a stream.
 * You can use hyss_stream_memory_open to create a readonly stream
 * from an existing memory buffer.
 */

/* Temp streams are streams that uses memory streams as long their
 * size is less than a given memory amount. When a write operation
 * exceeds that limit the content is written to a temporary file.
 */

/* {{{ ------- MEMORY stream implementation -------*/

typedef struct {
	char        *data;
	size_t      fpos;
	size_t      fsize;
	size_t      smax;
	int			mode;
} hyss_stream_memory_data;


/* {{{ */
static size_t hyss_stream_memory_write(hyss_stream *stream, const char *buf, size_t count)
{
	hyss_stream_memory_data *ms = (hyss_stream_memory_data*)stream->abstract;
	assert(ms != NULL);

	if (ms->mode & TEMP_STREAM_READONLY) {
		return 0;
	} else if (ms->mode & TEMP_STREAM_APPEND) {
		ms->fpos = ms->fsize;
	}
	if (ms->fpos + count > ms->fsize) {
		char *tmp;
		if (!ms->data) {
			tmp = emalloc(ms->fpos + count);
		} else {
			tmp = erealloc(ms->data, ms->fpos + count);
		}
		ms->data = tmp;
		ms->fsize = ms->fpos + count;
	}
	if (!ms->data)
		count = 0;
	if (count) {
		assert(buf!= NULL);
		memcpy(ms->data+ms->fpos, (char*)buf, count);
		ms->fpos += count;
	}
	return count;
}
/* }}} */


/* {{{ */
static size_t hyss_stream_memory_read(hyss_stream *stream, char *buf, size_t count)
{
	hyss_stream_memory_data *ms = (hyss_stream_memory_data*)stream->abstract;
	assert(ms != NULL);

	if (ms->fpos == ms->fsize) {
		stream->eof = 1;
		count = 0;
	} else {
		if (ms->fpos + count >= ms->fsize) {
			count = ms->fsize - ms->fpos;
		}
		if (count) {
			assert(ms->data!= NULL);
			assert(buf!= NULL);
			memcpy(buf, ms->data+ms->fpos, count);
			ms->fpos += count;
		}
	}
	return count;
}
/* }}} */


/* {{{ */
static int hyss_stream_memory_close(hyss_stream *stream, int close_handle)
{
	hyss_stream_memory_data *ms = (hyss_stream_memory_data*)stream->abstract;
	assert(ms != NULL);

	if (ms->data && close_handle && ms->mode != TEMP_STREAM_READONLY) {
		efree(ms->data);
	}
	efree(ms);
	return 0;
}
/* }}} */


/* {{{ */
static int hyss_stream_memory_flush(hyss_stream *stream)
{
	/* nothing to do here */
	return 0;
}
/* }}} */


/* {{{ */
static int hyss_stream_memory_seek(hyss_stream *stream, gear_off_t offset, int whence, gear_off_t *newoffs)
{
	hyss_stream_memory_data *ms = (hyss_stream_memory_data*)stream->abstract;
	assert(ms != NULL);

	switch(whence) {
		case SEEK_CUR:
			if (offset < 0) {
				if (ms->fpos < (size_t)(-offset)) {
					ms->fpos = 0;
					*newoffs = -1;
					return -1;
				} else {
					ms->fpos = ms->fpos + offset;
					*newoffs = ms->fpos;
					stream->eof = 0;
					return 0;
				}
			} else {
				if (ms->fpos + (size_t)(offset) > ms->fsize) {
					ms->fpos = ms->fsize;
					*newoffs = -1;
					return -1;
				} else {
					ms->fpos = ms->fpos + offset;
					*newoffs = ms->fpos;
					stream->eof = 0;
					return 0;
				}
			}
		case SEEK_SET:
			if (ms->fsize < (size_t)(offset)) {
				ms->fpos = ms->fsize;
				*newoffs = -1;
				return -1;
			} else {
				ms->fpos = offset;
				*newoffs = ms->fpos;
				stream->eof = 0;
				return 0;
			}
		case SEEK_END:
			if (offset > 0) {
				ms->fpos = ms->fsize;
				*newoffs = -1;
				return -1;
			} else if (ms->fsize < (size_t)(-offset)) {
				ms->fpos = 0;
				*newoffs = -1;
				return -1;
			} else {
				ms->fpos = ms->fsize + offset;
				*newoffs = ms->fpos;
				stream->eof = 0;
				return 0;
			}
		default:
			*newoffs = ms->fpos;
			return -1;
	}
}
/* }}} */

/* {{{ */
static int hyss_stream_memory_cast(hyss_stream *stream, int castas, void **ret)
{
	return FAILURE;
}
/* }}} */

static int hyss_stream_memory_stat(hyss_stream *stream, hyss_stream_statbuf *ssb) /* {{{ */
{
	time_t timestamp = 0;
	hyss_stream_memory_data *ms = (hyss_stream_memory_data*)stream->abstract;
	assert(ms != NULL);

	memset(ssb, 0, sizeof(hyss_stream_statbuf));
	/* read-only across the board */

	ssb->sb.st_mode = ms->mode & TEMP_STREAM_READONLY ? 0444 : 0666;

	ssb->sb.st_size = ms->fsize;
	ssb->sb.st_mode |= S_IFREG; /* regular file */
	ssb->sb.st_mtime = timestamp;
	ssb->sb.st_atime = timestamp;
	ssb->sb.st_ctime = timestamp;
	ssb->sb.st_nlink = 1;
	ssb->sb.st_rdev = -1;
	/* this is only for APC, so use /dev/null device - no chance of conflict there! */
	ssb->sb.st_dev = 0xC;
	/* generate unique inode number for alias/filename, so no archys will conflict */
	ssb->sb.st_ino = 0;

#ifndef HYSS_WIN32
	ssb->sb.st_blksize = -1;
	ssb->sb.st_blocks = -1;
#endif

	return 0;
}
/* }}} */

static int hyss_stream_memory_set_option(hyss_stream *stream, int option, int value, void *ptrparam) /* {{{ */
{
	hyss_stream_memory_data *ms = (hyss_stream_memory_data*)stream->abstract;
	size_t newsize;

	switch(option) {
		case HYSS_STREAM_OPTION_TRUNCATE_API:
			switch (value) {
				case HYSS_STREAM_TRUNCATE_SUPPORTED:
					return HYSS_STREAM_OPTION_RETURN_OK;

				case HYSS_STREAM_TRUNCATE_SET_SIZE:
					if (ms->mode & TEMP_STREAM_READONLY) {
						return HYSS_STREAM_OPTION_RETURN_ERR;
					}
					newsize = *(size_t*)ptrparam;
					if (newsize <= ms->fsize) {
						if (newsize < ms->fpos) {
							ms->fpos = newsize;
						}
					} else {
						ms->data = erealloc(ms->data, newsize);
						memset(ms->data+ms->fsize, 0, newsize - ms->fsize);
						ms->fsize = newsize;
					}
					ms->fsize = newsize;
					return HYSS_STREAM_OPTION_RETURN_OK;
			}
		default:
			return HYSS_STREAM_OPTION_RETURN_NOTIMPL;
	}
}
/* }}} */

HYSSAPI const hyss_stream_ops	hyss_stream_memory_ops = {
	hyss_stream_memory_write, hyss_stream_memory_read,
	hyss_stream_memory_close, hyss_stream_memory_flush,
	"MEMORY",
	hyss_stream_memory_seek,
	hyss_stream_memory_cast,
	hyss_stream_memory_stat,
	hyss_stream_memory_set_option
};

/* {{{ */
HYSSAPI int hyss_stream_mode_from_str(const char *mode)
{
	if (strpbrk(mode, "a")) {
		return TEMP_STREAM_APPEND;
	} else if (strpbrk(mode, "w+")) {
		return TEMP_STREAM_DEFAULT;
	}
	return TEMP_STREAM_READONLY;
}
/* }}} */

/* {{{ */
HYSSAPI const char *_hyss_stream_mode_to_str(int mode)
{
	if (mode == TEMP_STREAM_READONLY) {
		return "rb";
	} else if (mode == TEMP_STREAM_APPEND) {
		return "a+b";
	}
	return "w+b";
}
/* }}} */

/* {{{ */
HYSSAPI hyss_stream *_hyss_stream_memory_create(int mode STREAMS_DC)
{
	hyss_stream_memory_data *self;
	hyss_stream *stream;

	self = emalloc(sizeof(*self));
	self->data = NULL;
	self->fpos = 0;
	self->fsize = 0;
	self->smax = ~0u;
	self->mode = mode;

	stream = hyss_stream_alloc_rel(&hyss_stream_memory_ops, self, 0, _hyss_stream_mode_to_str(mode));
	stream->flags |= HYSS_STREAM_FLAG_NO_BUFFER;
	return stream;
}
/* }}} */


/* {{{ */
HYSSAPI hyss_stream *_hyss_stream_memory_open(int mode, char *buf, size_t length STREAMS_DC)
{
	hyss_stream *stream;
	hyss_stream_memory_data *ms;

	if ((stream = hyss_stream_memory_create_rel(mode)) != NULL) {
		ms = (hyss_stream_memory_data*)stream->abstract;

		if (mode == TEMP_STREAM_READONLY || mode == TEMP_STREAM_TAKE_BUFFER) {
			/* use the buffer directly */
			ms->data = buf;
			ms->fsize = length;
		} else {
			if (length) {
				assert(buf != NULL);
				hyss_stream_write(stream, buf, length);
			}
		}
	}
	return stream;
}
/* }}} */


/* {{{ */
HYSSAPI char *_hyss_stream_memory_get_buffer(hyss_stream *stream, size_t *length STREAMS_DC)
{
	hyss_stream_memory_data *ms = (hyss_stream_memory_data*)stream->abstract;

	assert(ms != NULL);
	assert(length != 0);

	*length = ms->fsize;
	return ms->data;
}
/* }}} */

/* }}} */

/* {{{ ------- TEMP stream implementation -------*/

typedef struct {
	hyss_stream  *innerstream;
	size_t      smax;
	int			mode;
	zval        meta;
	char*		tmpdir;
} hyss_stream_temp_data;


/* {{{ */
static size_t hyss_stream_temp_write(hyss_stream *stream, const char *buf, size_t count)
{
	hyss_stream_temp_data *ts = (hyss_stream_temp_data*)stream->abstract;
	assert(ts != NULL);

	if (!ts->innerstream) {
		return -1;
	}
	if (hyss_stream_is(ts->innerstream, HYSS_STREAM_IS_MEMORY)) {
		size_t memsize;
		char *membuf = hyss_stream_memory_get_buffer(ts->innerstream, &memsize);

		if (memsize + count >= ts->smax) {
			hyss_stream *file = hyss_stream_fopen_temporary_file(ts->tmpdir, "hyss", NULL);
			if (file == NULL) {
				hyss_error_docref(NULL, E_WARNING, "Unable to create temporary file, Check permissions in temporary files directory.");
				return 0;
			}
			hyss_stream_write(file, membuf, memsize);
			hyss_stream_free_enclosed(ts->innerstream, HYSS_STREAM_FREE_CLOSE);
			ts->innerstream = file;
			hyss_stream_encloses(stream, ts->innerstream);
		}
	}
	return hyss_stream_write(ts->innerstream, buf, count);
}
/* }}} */


/* {{{ */
static size_t hyss_stream_temp_read(hyss_stream *stream, char *buf, size_t count)
{
	hyss_stream_temp_data *ts = (hyss_stream_temp_data*)stream->abstract;
	size_t got;

	assert(ts != NULL);

	if (!ts->innerstream) {
		return -1;
	}

	got = hyss_stream_read(ts->innerstream, buf, count);

	stream->eof = ts->innerstream->eof;

	return got;
}
/* }}} */


/* {{{ */
static int hyss_stream_temp_close(hyss_stream *stream, int close_handle)
{
	hyss_stream_temp_data *ts = (hyss_stream_temp_data*)stream->abstract;
	int ret;

	assert(ts != NULL);

	if (ts->innerstream) {
		ret = hyss_stream_free_enclosed(ts->innerstream, HYSS_STREAM_FREE_CLOSE | (close_handle ? 0 : HYSS_STREAM_FREE_PRESERVE_HANDLE));
	} else {
		ret = 0;
	}

	zval_ptr_dtor(&ts->meta);

	if (ts->tmpdir) {
		efree(ts->tmpdir);
	}

	efree(ts);

	return ret;
}
/* }}} */


/* {{{ */
static int hyss_stream_temp_flush(hyss_stream *stream)
{
	hyss_stream_temp_data *ts = (hyss_stream_temp_data*)stream->abstract;
	assert(ts != NULL);

	return ts->innerstream ? hyss_stream_flush(ts->innerstream) : -1;
}
/* }}} */


/* {{{ */
static int hyss_stream_temp_seek(hyss_stream *stream, gear_off_t offset, int whence, gear_off_t *newoffs)
{
	hyss_stream_temp_data *ts = (hyss_stream_temp_data*)stream->abstract;
	int ret;

	assert(ts != NULL);

	if (!ts->innerstream) {
		*newoffs = -1;
		return -1;
	}
	ret = hyss_stream_seek(ts->innerstream, offset, whence);
	*newoffs = hyss_stream_tell(ts->innerstream);
	stream->eof = ts->innerstream->eof;

	return ret;
}
/* }}} */

/* {{{ */
static int hyss_stream_temp_cast(hyss_stream *stream, int castas, void **ret)
{
	hyss_stream_temp_data *ts = (hyss_stream_temp_data*)stream->abstract;
	hyss_stream *file;
	size_t memsize;
	char *membuf;
	gear_off_t pos;

	assert(ts != NULL);

	if (!ts->innerstream) {
		return FAILURE;
	}
	if (hyss_stream_is(ts->innerstream, HYSS_STREAM_IS_STDIO)) {
		return hyss_stream_cast(ts->innerstream, castas, ret, 0);
	}

	/* we are still using a memory based backing. If they are if we can be
	 * a FILE*, say yes because we can perform the conversion.
	 * If they actually want to perform the conversion, we need to switch
	 * the memory stream to a tmpfile stream */

	if (ret == NULL && castas == HYSS_STREAM_AS_STDIO) {
		return SUCCESS;
	}

	/* say "no" to other stream forms */
	if (ret == NULL) {
		return FAILURE;
	}

	file = hyss_stream_fopen_tmpfile();
	if (file == NULL) {
		hyss_error_docref(NULL, E_WARNING, "Unable to create temporary file.");
		return FAILURE;
	}

	/* perform the conversion and then pass the request on to the innerstream */
	membuf = hyss_stream_memory_get_buffer(ts->innerstream, &memsize);
	hyss_stream_write(file, membuf, memsize);
	pos = hyss_stream_tell(ts->innerstream);

	hyss_stream_free_enclosed(ts->innerstream, HYSS_STREAM_FREE_CLOSE);
	ts->innerstream = file;
	hyss_stream_encloses(stream, ts->innerstream);
	hyss_stream_seek(ts->innerstream, pos, SEEK_SET);

	return hyss_stream_cast(ts->innerstream, castas, ret, 1);
}
/* }}} */

static int hyss_stream_temp_stat(hyss_stream *stream, hyss_stream_statbuf *ssb) /* {{{ */
{
	hyss_stream_temp_data *ts = (hyss_stream_temp_data*)stream->abstract;

	if (!ts || !ts->innerstream) {
		return -1;
	}
	return hyss_stream_stat(ts->innerstream, ssb);
}
/* }}} */

static int hyss_stream_temp_set_option(hyss_stream *stream, int option, int value, void *ptrparam) /* {{{ */
{
	hyss_stream_temp_data *ts = (hyss_stream_temp_data*)stream->abstract;

	switch(option) {
		case HYSS_STREAM_OPTION_META_DATA_API:
			if (Z_TYPE(ts->meta) != IS_UNDEF) {
				gear_hash_copy(Z_ARRVAL_P((zval*)ptrparam), Z_ARRVAL(ts->meta), zval_add_ref);
			}
			return HYSS_STREAM_OPTION_RETURN_OK;
		default:
			if (ts->innerstream) {
				return hyss_stream_set_option(ts->innerstream, option, value, ptrparam);
			}
			return HYSS_STREAM_OPTION_RETURN_NOTIMPL;
	}
}
/* }}} */

HYSSAPI const hyss_stream_ops	hyss_stream_temp_ops = {
	hyss_stream_temp_write, hyss_stream_temp_read,
	hyss_stream_temp_close, hyss_stream_temp_flush,
	"TEMP",
	hyss_stream_temp_seek,
	hyss_stream_temp_cast,
	hyss_stream_temp_stat,
	hyss_stream_temp_set_option
};

/* }}} */

/* {{{ _hyss_stream_temp_create_ex */
HYSSAPI hyss_stream *_hyss_stream_temp_create_ex(int mode, size_t max_memory_usage, const char *tmpdir STREAMS_DC)
{
	hyss_stream_temp_data *self;
	hyss_stream *stream;

	self = ecalloc(1, sizeof(*self));
	self->smax = max_memory_usage;
	self->mode = mode;
	ZVAL_UNDEF(&self->meta);
	if (tmpdir) {
		self->tmpdir = estrdup(tmpdir);
	}
	stream = hyss_stream_alloc_rel(&hyss_stream_temp_ops, self, 0, _hyss_stream_mode_to_str(mode));
	stream->flags |= HYSS_STREAM_FLAG_NO_BUFFER;
	self->innerstream = hyss_stream_memory_create_rel(mode);
	hyss_stream_encloses(stream, self->innerstream);

	return stream;
}
/* }}} */

/* {{{ _hyss_stream_temp_create */
HYSSAPI hyss_stream *_hyss_stream_temp_create(int mode, size_t max_memory_usage STREAMS_DC)
{
	return hyss_stream_temp_create_ex(mode, max_memory_usage, NULL);
}
/* }}} */

/* {{{ _hyss_stream_temp_open */
HYSSAPI hyss_stream *_hyss_stream_temp_open(int mode, size_t max_memory_usage, char *buf, size_t length STREAMS_DC)
{
	hyss_stream *stream;
	hyss_stream_temp_data *ts;
	gear_off_t newoffs;

	if ((stream = hyss_stream_temp_create_rel(mode, max_memory_usage)) != NULL) {
		if (length) {
			assert(buf != NULL);
			hyss_stream_temp_write(stream, buf, length);
			hyss_stream_temp_seek(stream, 0, SEEK_SET, &newoffs);
		}
		ts = (hyss_stream_temp_data*)stream->abstract;
		assert(ts != NULL);
		ts->mode = mode;
	}
	return stream;
}
/* }}} */

HYSSAPI const hyss_stream_ops hyss_stream_rfc2397_ops = {
	hyss_stream_temp_write, hyss_stream_temp_read,
	hyss_stream_temp_close, hyss_stream_temp_flush,
	"RFC2397",
	hyss_stream_temp_seek,
	hyss_stream_temp_cast,
	hyss_stream_temp_stat,
	hyss_stream_temp_set_option
};

static hyss_stream * hyss_stream_url_wrap_rfc2397(hyss_stream_wrapper *wrapper, const char *path,
												const char *mode, int options, gear_string **opened_path,
												hyss_stream_context *context STREAMS_DC) /* {{{ */
{
	hyss_stream *stream;
	hyss_stream_temp_data *ts;
	char *comma, *semi, *sep;
	size_t mlen, dlen, plen, vlen, ilen;
	gear_off_t newoffs;
	zval meta;
	int base64 = 0;
	gear_string *base64_comma = NULL;

	ZVAL_NULL(&meta);
	if (memcmp(path, "data:", 5)) {
		return NULL;
	}

	path += 5;
	dlen = strlen(path);

	if (dlen >= 2 && path[0] == '/' && path[1] == '/') {
		dlen -= 2;
		path += 2;
	}

	if ((comma = memchr(path, ',', dlen)) == NULL) {
		hyss_stream_wrapper_log_error(wrapper, options, "rfc2397: no comma in URL");
		return NULL;
	}

	if (comma != path) {
		/* meta info */
		mlen = comma - path;
		dlen -= mlen;
		semi = memchr(path, ';', mlen);
		sep = memchr(path, '/', mlen);

		if (!semi && !sep) {
			hyss_stream_wrapper_log_error(wrapper, options, "rfc2397: illegal media type");
			return NULL;
		}

		array_init(&meta);
		if (!semi) { /* there is only a mime type */
			add_assoc_stringl(&meta, "mediatype", (char *) path, mlen);
			mlen = 0;
		} else if (sep && sep < semi) { /* there is a mime type */
			plen = semi - path;
			add_assoc_stringl(&meta, "mediatype", (char *) path, plen);
			mlen -= plen;
			path += plen;
		} else if (semi != path || mlen != sizeof(";base64")-1 || memcmp(path, ";base64", sizeof(";base64")-1)) { /* must be error since parameters are only allowed after mediatype */
			zval_ptr_dtor(&meta);
			hyss_stream_wrapper_log_error(wrapper, options, "rfc2397: illegal media type");
			return NULL;
		}
		/* get parameters and potentially ';base64' */
		while(semi && (semi == path)) {
			path++;
			mlen--;
			sep = memchr(path, '=', mlen);
			semi = memchr(path, ';', mlen);
			if (!sep || (semi && semi < sep)) { /* must be ';base64' or failure */
				if (mlen != sizeof("base64")-1 || memcmp(path, "base64", sizeof("base64")-1)) {
					/* must be error since parameters are only allowed after mediatype and we have no '=' sign */
					zval_ptr_dtor(&meta);
					hyss_stream_wrapper_log_error(wrapper, options, "rfc2397: illegal parameter");
					return NULL;
				}
				base64 = 1;
				mlen -= sizeof("base64") - 1;
				path += sizeof("base64") - 1;
				break;
			}
			/* found parameter ... the heart of cs ppl lies in +1/-1 or was it +2 this time? */
			plen = sep - path;
			vlen = (semi ? (size_t)(semi - sep) : (mlen - plen)) - 1 /* '=' */;
			if (plen != sizeof("mediatype")-1 || memcmp(path, "mediatype", sizeof("mediatype")-1)) {
				add_assoc_stringl_ex(&meta, path, plen, sep + 1, vlen);
			}
			plen += vlen + 1;
			mlen -= plen;
			path += plen;
		}
		if (mlen) {
			zval_ptr_dtor(&meta);
			hyss_stream_wrapper_log_error(wrapper, options, "rfc2397: illegal URL");
			return NULL;
		}
	} else {
		array_init(&meta);
	}
	add_assoc_bool(&meta, "base64", base64);

	/* skip ',' */
	comma++;
	dlen--;

	if (base64) {
		base64_comma = hyss_base64_decode_ex((const unsigned char *)comma, dlen, 1);
		if (!base64_comma) {
			zval_ptr_dtor(&meta);
			hyss_stream_wrapper_log_error(wrapper, options, "rfc2397: unable to decode");
			return NULL;
		}
		comma = ZSTR_VAL(base64_comma);
		ilen = ZSTR_LEN(base64_comma);
	} else {
		comma = estrndup(comma, dlen);
		dlen = hyss_url_decode(comma, dlen);
		ilen = dlen;
	}

	if ((stream = hyss_stream_temp_create_rel(0, ~0u)) != NULL) {
		/* store data */
		hyss_stream_temp_write(stream, comma, ilen);
		hyss_stream_temp_seek(stream, 0, SEEK_SET, &newoffs);
		/* set special stream stuff (enforce exact mode) */
		vlen = strlen(mode);
		if (vlen >= sizeof(stream->mode)) {
			vlen = sizeof(stream->mode) - 1;
		}
		memcpy(stream->mode, mode, vlen);
		stream->mode[vlen] = '\0';
		stream->ops = &hyss_stream_rfc2397_ops;
		ts = (hyss_stream_temp_data*)stream->abstract;
		assert(ts != NULL);
		ts->mode = mode && mode[0] == 'r' && mode[1] != '+' ? TEMP_STREAM_READONLY : 0;
		ZVAL_COPY_VALUE(&ts->meta, &meta);
	}
	if (base64_comma) {
		gear_string_free(base64_comma);
	} else {
		efree(comma);
	}

	return stream;
}

HYSSAPI const hyss_stream_wrapper_ops hyss_stream_rfc2397_wops = {
	hyss_stream_url_wrap_rfc2397,
	NULL, /* close */
	NULL, /* fstat */
	NULL, /* stat */
	NULL, /* opendir */
	"RFC2397",
	NULL, /* unlink */
	NULL, /* rename */
	NULL, /* mkdir */
	NULL, /* rmdir */
	NULL, /* stream_metadata */
};

HYSSAPI const hyss_stream_wrapper hyss_stream_rfc2397_wrapper =	{
	&hyss_stream_rfc2397_wops,
	NULL,
	1, /* is_url */
};

