/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_SYSLOG_H
#define HYSS_SYSLOG_H

#include "hyss.h"

#ifdef HYSS_WIN32
#include "win32/syslog.h"
#else
#include <hyss_config.h>
#ifdef HAVE_SYSLOG_H
#include <syslog.h>
#endif
#endif

/* Syslog filters */
#define HYSS_SYSLOG_FILTER_ALL		0
#define HYSS_SYSLOG_FILTER_NO_CTRL	1
#define HYSS_SYSLOG_FILTER_ASCII		2

BEGIN_EXTERN_C()
HYSSAPI void hyss_syslog(int, const char *format, ...);
HYSSAPI void hyss_openlog(const char *, int, int);
END_EXTERN_C()

#endif

