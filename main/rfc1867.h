/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RFC1867_H
#define RFC1867_H

#include "SAPI.h"

#define MULTIPART_CONTENT_TYPE "multipart/form-data"
#define MULTIPART_EVENT_START		0
#define MULTIPART_EVENT_FORMDATA	1
#define MULTIPART_EVENT_FILE_START	2
#define MULTIPART_EVENT_FILE_DATA	3
#define MULTIPART_EVENT_FILE_END	4
#define MULTIPART_EVENT_END		5

typedef struct _multipart_event_start {
	size_t	content_length;
} multipart_event_start;

typedef struct _multipart_event_formdata {
	size_t	post_bytes_processed;
	char	*name;
	char	**value;
	size_t	length;
	size_t	*newlength;
} multipart_event_formdata;

typedef struct _multipart_event_file_start {
	size_t	post_bytes_processed;
	char	*name;
	char	**filename;
} multipart_event_file_start;

typedef struct _multipart_event_file_data {
	size_t	post_bytes_processed;
	gear_off_t	offset;
	char	*data;
	size_t	length;
	size_t	*newlength;
} multipart_event_file_data;

typedef struct _multipart_event_file_end {
	size_t	post_bytes_processed;
	char	*temp_filename;
	int	cancel_upload;
} multipart_event_file_end;

typedef struct _multipart_event_end {
	size_t	post_bytes_processed;
} multipart_event_end;

typedef int (*hyss_rfc1867_encoding_translation_t)(void);
typedef void (*hyss_rfc1867_get_detect_order_t)(const gear_encoding ***list, size_t *list_size);
typedef void (*hyss_rfc1867_set_input_encoding_t)(const gear_encoding *encoding);
typedef char* (*hyss_rfc1867_getword_t)(const gear_encoding *encoding, char **line, char stop);
typedef char* (*hyss_rfc1867_getword_conf_t)(const gear_encoding *encoding, char *str);
typedef char* (*hyss_rfc1867_basename_t)(const gear_encoding *encoding, char *str);

SAPI_API SAPI_POST_HANDLER_FUNC(rfc1867_post_handler);

HYSSAPI void destroy_uploaded_files_hash(void);
void hyss_rfc1867_register_constants(void);
extern HYSSAPI int (*hyss_rfc1867_callback)(unsigned int event, void *event_data, void **extra);

SAPI_API void hyss_rfc1867_set_multibyte_callbacks(
					hyss_rfc1867_encoding_translation_t encoding_translation,
					hyss_rfc1867_get_detect_order_t get_detect_order,
					hyss_rfc1867_set_input_encoding_t set_input_encoding,
					hyss_rfc1867_getword_t getword,
					hyss_rfc1867_getword_conf_t getword_conf,
					hyss_rfc1867_basename_t basename);

#endif /* RFC1867_H */

