/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FOPEN_WRAPPERS_H
#define FOPEN_WRAPPERS_H

BEGIN_EXTERN_C()
#include "hyss_globals.h"
#include "hyss_ics.h"

HYSSAPI int hyss_fopen_primary_script(gear_file_handle *file_handle);
HYSSAPI char *expand_filepath(const char *filepath, char *real_path);
HYSSAPI char *expand_filepath_ex(const char *filepath, char *real_path, const char *relative_to, size_t relative_to_len);
HYSSAPI char *expand_filepath_with_mode(const char *filepath, char *real_path, const char *relative_to, size_t relative_to_len, int use_realpath);

HYSSAPI int hyss_check_open_basedir(const char *path);
HYSSAPI int hyss_check_open_basedir_ex(const char *path, int warn);
HYSSAPI int hyss_check_specific_open_basedir(const char *basedir, const char *path);

/* OPENBASEDIR_CHECKPATH(filename) to ease merge between 6.x and 5.x */
#define OPENBASEDIR_CHECKPATH(filename) hyss_check_open_basedir(filename)

HYSSAPI int hyss_check_safe_mode_include_dir(const char *path);

HYSSAPI gear_string *hyss_resolve_path(const char *filename, size_t filename_len, const char *path);

HYSSAPI FILE *hyss_fopen_with_path(const char *filename, const char *mode, const char *path, gear_string **opened_path);

HYSSAPI char *hyss_strip_url_passwd(char *path);

HYSSAPI GEAR_ICS_MH(OnUpdateBaseDir);
END_EXTERN_C()

#endif

