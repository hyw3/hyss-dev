/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_H
#define HYSS_H

#ifdef HAVE_DMALLOC
#include <dmalloc.h>
#endif

#define HYSS_API_VERSION 20180731
#define HYSS_HAVE_STREAMS
#define YYDEBUG 0
#define HYSS_DEFAULT_CHARSET "UTF-8"

#include "hyss_version.h"
#include "gear.h"
#include "gear_sort.h"
#include "hyss_compat.h"

#include "gear_API.h"

#undef sprintf
#define sprintf hyss_sprintf

/* Operating system family definition */
#ifdef HYSS_WIN32
# define HYSS_OS_FAMILY			"Windows"
#elif defined(BSD) || defined(__DragonFly__) || defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__)
# define HYSS_OS_FAMILY			"BSD"
#elif defined(__APPLE__) || defined(__MACH__)
# define HYSS_OS_FAMILY			"Darwin"
#elif defined(__sun__)
# define HYSS_OS_FAMILY			"Solaris"
#elif defined(__linux__)
# define HYSS_OS_FAMILY			"Linux"
#else
# define HYSS_OS_FAMILY			"Unknown"
#endif

/* HYSS's DEBUG value must match Gear's GEAR_DEBUG value */
#undef HYSS_DEBUG
#define HYSS_DEBUG GEAR_DEBUG

#ifdef HYSS_WIN32
#	include "pbc_win32.h"
#	ifdef HYSS_EXPORTS
#		define HYSSAPI __declspec(dllexport)
#	else
#		define HYSSAPI __declspec(dllimport)
#	endif
#	define HYSS_DIR_SEPARATOR '\\'
#	define HYSS_EOL "\r\n"
#else
#	if defined(__GNUC__) && __GNUC__ >= 4
#		define HYSSAPI __attribute__ ((visibility("default")))
#	else
#		define HYSSAPI
#	endif
#	define THREAD_LS
#	define HYSS_DIR_SEPARATOR '/'
#	define HYSS_EOL "\n"
#endif

/* Windows specific defines */
#ifdef HYSS_WIN32
# define HYSS_PROG_SENDMAIL		"Built in mailer"
# define HAVE_DECLARED_TIMEZONE
# define WIN32_LEAN_AND_MEAN
# define NOOPENFILE

# include <io.h>
# include <malloc.h>
# include <direct.h>
# include <stdlib.h>
# include <stdio.h>
# include <stdarg.h>
# include <sys/types.h>
# include <process.h>

typedef int uid_t;
typedef int gid_t;
typedef char * caddr_t;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef int pid_t;

# ifndef HYSS_DEBUG
#  ifdef inline
#   undef inline
#  endif
#  define inline		__inline
# endif

# define M_TWOPI        (M_PI * 2.0)
# define off_t			_off_t

# define lstat(x, y)	hyss_sys_lstat(x, y)
# define chdir(path)	_chdir(path)
# define mkdir(a, b)	_mkdir(a)
# define rmdir(a)		_rmdir(a)
# define getpid			_getpid
# define hyss_sleep(t)	SleepEx(t*1000, TRUE)

# ifndef getcwd
#  define getcwd(a, b)	_getcwd(a, b)
# endif
#endif

#if HAVE_ASSERT_H
#if HYSS_DEBUG
#undef NDEBUG
#else
#ifndef NDEBUG
#define NDEBUG
#endif
#endif
#include <assert.h>
#else /* HAVE_ASSERT_H */
#define assert(expr) ((void) (0))
#endif /* HAVE_ASSERT_H */

/* #define CLHY 0 */

#if HAVE_UNIX_H
#include <unix.h>
#endif

#if HAVE_ALLOCA_H
#include <alloca.h>
#endif

#if HAVE_BUILD_DEFS_H
#include <build-defs.h>
#endif

/*
 * This is a fast version of strlcpy which should be used, if you
 * know the size of the destination buffer and if you know
 * the length of the source string.
 *
 * size is the allocated number of bytes of dst
 * src_size is the number of bytes excluding the NUL of src
 */

#define HYSS_STRLCPY(dst, src, size, src_size)	\
	{											\
		size_t hyss_str_len;						\
												\
		if (src_size >= size)					\
			hyss_str_len = size - 1;				\
		else									\
			hyss_str_len = src_size;				\
		memcpy(dst, src, hyss_str_len);			\
		dst[hyss_str_len] = '\0';				\
	}

#ifndef HAVE_STRLCPY
BEGIN_EXTERN_C()
HYSSAPI size_t hyss_strlcpy(char *dst, const char *src, size_t siz);
END_EXTERN_C()
#undef strlcpy
#define strlcpy hyss_strlcpy
#define HAVE_STRLCPY 1
#define USE_STRLCPY_HYSS_IMPL 1
#endif

#ifndef HAVE_STRLCAT
BEGIN_EXTERN_C()
HYSSAPI size_t hyss_strlcat(char *dst, const char *src, size_t siz);
END_EXTERN_C()
#undef strlcat
#define strlcat hyss_strlcat
#define HAVE_STRLCAT 1
#define USE_STRLCAT_HYSS_IMPL 1
#endif

#ifndef HAVE_EXPLICIT_BZERO
BEGIN_EXTERN_C()
HYSSAPI void hyss_explicit_bzero(void *dst, size_t siz);
END_EXTERN_C()
#undef explicit_bzero
#define explicit_bzero hyss_explicit_bzero
#endif

#ifndef HAVE_STRTOK_R
BEGIN_EXTERN_C()
char *strtok_r(char *s, const char *delim, char **last);
END_EXTERN_C()
#endif

#ifndef HAVE_SOCKLEN_T
# ifdef HYSS_WIN32
typedef int socklen_t;
# else
typedef unsigned int socklen_t;
# endif
#endif

#define CREATE_MUTEX(a, b)
#define SET_MUTEX(a)
#define FREE_MUTEX(a)

/*
 * Then the ODBC support can use both iodbc and Solid,
 * uncomment this.
 * #define HAVE_ODBC (HAVE_IODBC|HAVE_SOLID)
 */

#include <stdlib.h>
#include <ctype.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#if HAVE_STDARG_H
#include <stdarg.h>
#else
# if HAVE_SYS_VARARGS_H
# include <sys/varargs.h>
# endif
#endif

#include "hyss_stdint.h"

#include "gear_hash.h"
#include "gear_alloc.h"
#include "gear_stack.h"

#if STDC_HEADERS
# include <string.h>
#else
# ifndef HAVE_MEMCPY
#  define memcpy(d, s, n)	bcopy((s), (d), (n))
# endif
# ifndef HAVE_MEMMOVE
#  define memmove(d, s, n)	bcopy ((s), (d), (n))
# endif
#endif

#ifndef HAVE_STRERROR
char *strerror(int);
#endif

#if HAVE_PWD_H
# ifdef HYSS_WIN32
#include "win32/param.h"
# else
#include <pwd.h>
#include <sys/param.h>
# endif
#endif

#if HAVE_LIMITS_H
#include <limits.h>
#endif

#ifndef LONG_MAX
#define LONG_MAX 2147483647L
#endif

#ifndef LONG_MIN
#define LONG_MIN (- LONG_MAX - 1)
#endif

#ifndef INT_MAX
#define INT_MAX 2147483647
#endif

#ifndef INT_MIN
#define INT_MIN (- INT_MAX - 1)
#endif

/* double limits */
#include <float.h>
#if defined(DBL_MANT_DIG) && defined(DBL_MIN_EXP)
#define HYSS_DOUBLE_MAX_LENGTH (3 + DBL_MANT_DIG - DBL_MIN_EXP)
#else
#define HYSS_DOUBLE_MAX_LENGTH 1080
#endif

#define HYSS_GCC_VERSION GEAR_GCC_VERSION
#define HYSS_ATTRIBUTE_MALLOC GEAR_ATTRIBUTE_MALLOC
#define HYSS_ATTRIBUTE_FORMAT GEAR_ATTRIBUTE_FORMAT

BEGIN_EXTERN_C()
#include "snprintf.h"
END_EXTERN_C()
#include "spprintf.h"

#define EXEC_INPUT_BUF 4096

#define HYSS_MIME_TYPE "application/x-wwhy-hyss"

/* macros */
#define STR_PRINT(str)	((str)?(str):"")

#ifndef MAXPATHLEN
# ifdef HYSS_WIN32
#  include "win32/ioutil.h"
#  define MAXPATHLEN HYSS_WIN32_IOUTIL_MAXPATHLEN
# elif PATH_MAX
#  define MAXPATHLEN PATH_MAX
# elif defined(MAX_PATH)
#  define MAXPATHLEN MAX_PATH
# else
#  define MAXPATHLEN 256    /* Should be safe for any weird systems that do not define it */
# endif
#endif

#define hyss_ignore_value(x) GEAR_IGNORE_VALUE(x)

/* global variables */
#if !defined(HYSS_WIN32)
#define HYSS_SLEEP_NON_VOID
#define hyss_sleep sleep
extern char **environ;
#endif	/* !defined(HYSS_WIN32) */

#ifdef HYSS_PWRITE_64
ssize_t pwrite(int, void *, size_t, off64_t);
#endif

#ifdef HYSS_PREAD_64
ssize_t pread(int, void *, size_t, off64_t);
#endif

BEGIN_EXTERN_C()
void hysserror(char *error);
HYSSAPI size_t hyss_write(void *buf, size_t size);
HYSSAPI size_t hyss_printf(const char *format, ...) HYSS_ATTRIBUTE_FORMAT(printf, 1,
		2);
HYSSAPI int hyss_get_capi_initialized(void);
#ifdef HAVE_SYSLOG_H
#include "hyss_syslog.h"
#define hyss_log_err(msg) hyss_log_err_with_severity(msg, LOG_NOTICE)
#else
#define hyss_log_err(msg) hyss_log_err_with_severity(msg, 5)
#endif
HYSSAPI GEAR_COLD void hyss_log_err_with_severity(char *log_message, int syslog_type_int);
int Debug(char *format, ...) HYSS_ATTRIBUTE_FORMAT(printf, 1, 2);
int cfgparse(void);
END_EXTERN_C()

#define hyss_error gear_error
#define error_handling_t gear_error_handling_t

BEGIN_EXTERN_C()
static inline GEAR_ATTRIBUTE_DEPRECATED void hyss_set_error_handling(error_handling_t error_handling, gear_class_entry *exception_class)
{
	gear_replace_error_handling(error_handling, exception_class, NULL);
}
static inline GEAR_ATTRIBUTE_DEPRECATED void hyss_std_error_handling() {}

HYSSAPI GEAR_COLD void hyss_verror(const char *docref, const char *params, int type, const char *format, va_list args) HYSS_ATTRIBUTE_FORMAT(printf, 4, 0);

/* HYSSAPI void hyss_error(int type, const char *format, ...); */
HYSSAPI GEAR_COLD void hyss_error_docref0(const char *docref, int type, const char *format, ...)
	HYSS_ATTRIBUTE_FORMAT(printf, 3, 4);
HYSSAPI GEAR_COLD void hyss_error_docref1(const char *docref, const char *param1, int type, const char *format, ...)
	HYSS_ATTRIBUTE_FORMAT(printf, 4, 5);
HYSSAPI GEAR_COLD void hyss_error_docref2(const char *docref, const char *param1, const char *param2, int type, const char *format, ...)
	HYSS_ATTRIBUTE_FORMAT(printf, 5, 6);
#ifdef HYSS_WIN32
HYSSAPI GEAR_COLD void hyss_win32_docref2_from_error(DWORD error, const char *param1, const char *param2);
#endif
END_EXTERN_C()

#define hyss_error_docref hyss_error_docref0

#define gearerror hysserror
#define gearlex hysslex

#define hyssparse gearparse
#define hyssrestart gearrestart
#define hyssin gearin

#define hyss_memnstr gear_memnstr

/* functions */
BEGIN_EXTERN_C()
HYSSAPI extern int (*hyss_register_internal_extensions_func)(void);
HYSSAPI int hyss_register_internal_extensions(void);
HYSSAPI int hyss_mergesort(void *base, size_t nmemb, size_t size, int (*cmp)(const void *, const void *));
HYSSAPI void hyss_register_pre_request_shutdown(void (*func)(void *), void *userdata);
HYSSAPI void hyss_com_initialize(void);
HYSSAPI char *hyss_get_current_user(void);
END_EXTERN_C()

/* HYSS-named Gear macro wrappers */
#define HYSS_FN					GEAR_FN
#define HYSS_MN					GEAR_MN
#define HYSS_NAMED_FUNCTION		GEAR_NAMED_FUNCTION
#define HYSS_FUNCTION			GEAR_FUNCTION
#define HYSS_METHOD  			GEAR_METHOD

#define HYSS_RAW_NAMED_FE GEAR_RAW_NAMED_FE
#define HYSS_NAMED_FE	GEAR_NAMED_FE
#define HYSS_FE			GEAR_FE
#define HYSS_DEP_FE      GEAR_DEP_FE
#define HYSS_FALIAS		GEAR_FALIAS
#define HYSS_DEP_FALIAS	GEAR_DEP_FALIAS
#define HYSS_ME          GEAR_ME
#define HYSS_MALIAS      GEAR_MALIAS
#define HYSS_ABSTRACT_ME GEAR_ABSTRACT_ME
#define HYSS_ME_MAPPING  GEAR_ME_MAPPING
#define HYSS_FE_END      GEAR_FE_END

#define HYSS_CAPI_STARTUP_N	GEAR_CAPI_STARTUP_N
#define HYSS_CAPI_SHUTDOWN_N	GEAR_CAPI_SHUTDOWN_N
#define HYSS_CAPI_ACTIVATE_N	GEAR_CAPI_ACTIVATE_N
#define HYSS_CAPI_DEACTIVATE_N	GEAR_CAPI_DEACTIVATE_N
#define HYSS_CAPI_INFO_N		GEAR_CAPI_INFO_N

#define HYSS_CAPI_STARTUP_D	GEAR_CAPI_STARTUP_D
#define HYSS_CAPI_SHUTDOWN_D	GEAR_CAPI_SHUTDOWN_D
#define HYSS_CAPI_ACTIVATE_D	GEAR_CAPI_ACTIVATE_D
#define HYSS_CAPI_DEACTIVATE_D	GEAR_CAPI_DEACTIVATE_D
#define HYSS_CAPI_INFO_D		GEAR_CAPI_INFO_D

/* Compatibility macros */
#define HYSS_MINIT		GEAR_CAPI_STARTUP_N
#define HYSS_MSHUTDOWN	GEAR_CAPI_SHUTDOWN_N
#define HYSS_RINIT		GEAR_CAPI_ACTIVATE_N
#define HYSS_RSHUTDOWN	GEAR_CAPI_DEACTIVATE_N
#define HYSS_MINFO		GEAR_CAPI_INFO_N
#define HYSS_GINIT		GEAR_GINIT
#define HYSS_GSHUTDOWN	GEAR_GSHUTDOWN

#define HYSS_MINIT_FUNCTION		GEAR_CAPI_STARTUP_D
#define HYSS_MSHUTDOWN_FUNCTION	GEAR_CAPI_SHUTDOWN_D
#define HYSS_RINIT_FUNCTION		GEAR_CAPI_ACTIVATE_D
#define HYSS_RSHUTDOWN_FUNCTION	GEAR_CAPI_DEACTIVATE_D
#define HYSS_MINFO_FUNCTION		GEAR_CAPI_INFO_D
#define HYSS_GINIT_FUNCTION		GEAR_GINIT_FUNCTION
#define HYSS_GSHUTDOWN_FUNCTION	GEAR_GSHUTDOWN_FUNCTION

#define HYSS_CAPI_GLOBALS		GEAR_CAPI_GLOBALS


/* Output support */
#include "main/hyss_output.h"


#include "hyss_streams.h"
#include "hyss_memory_streams.h"
#include "fopen_wrappers.h"


/* Virtual current working directory support */
#include "gear_virtual_cwd.h"

#include "gear_constants.h"

/* connection status states */
#define HYSS_CONNECTION_NORMAL  0
#define HYSS_CONNECTION_ABORTED 1
#define HYSS_CONNECTION_TIMEOUT 2

#include "hyss_reentrancy.h"

/* Finding offsets of elements within structures.
 * Taken from the cLHy code, which in turn, was taken from X code...
 */

#ifndef XtOffset
#if defined(CRAY) || (defined(__arm) && !(defined(LINUX) || defined(__riscos__)))
#ifdef __STDC__
#define XtOffset(p_type, field) _Offsetof(p_type, field)
#else
#ifdef CRAY2
#define XtOffset(p_type, field) \
    (sizeof(int)*((unsigned int)&(((p_type)NULL)->field)))

#else /* !CRAY2 */

#define XtOffset(p_type, field) ((unsigned int)&(((p_type)NULL)->field))

#endif /* !CRAY2 */
#endif /* __STDC__ */
#else /* ! (CRAY || __arm) */

#define XtOffset(p_type, field) \
    ((gear_long) (((char *) (&(((p_type)NULL)->field))) - ((char *) NULL)))

#endif /* !CRAY */
#endif /* ! XtOffset */

#ifndef XtOffsetOf
#ifdef offsetof
#define XtOffsetOf(s_type, field) offsetof(s_type, field)
#else
#define XtOffsetOf(s_type, field) XtOffset(s_type*, field)
#endif
#endif /* !XtOffsetOf */

#endif

