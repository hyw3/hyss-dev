/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_VARIABLES_H
#define HYSS_VARIABLES_H

#include "hyss.h"
#include "SAPI.h"

#define PARSE_POST 0
#define PARSE_GET 1
#define PARSE_COOKIE 2
#define PARSE_STRING 3
#define PARSE_ENV 4
#define PARSE_SERVER 5
#define PARSE_SESSION 6

BEGIN_EXTERN_C()
void hyss_startup_auto_globals(void);
extern HYSSAPI void (*hyss_import_environment_variables)(zval *array_ptr);
HYSSAPI void hyss_register_variable(char *var, char *val, zval *track_vars_array);
/* binary-safe version */
HYSSAPI void hyss_register_variable_safe(char *var, char *val, size_t val_len, zval *track_vars_array);
HYSSAPI void hyss_register_variable_ex(char *var, zval *val, zval *track_vars_array);

HYSSAPI void hyss_build_argv(char *s, zval *track_vars_array);
HYSSAPI int hyss_hash_environment(void);
END_EXTERN_C()

#define NUM_TRACK_VARS	6

#endif /* HYSS_VARIABLES_H */

