/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes
 */

#define GEAR_INCLUDE_FULL_WINDOWS_HEADERS

#include "hyss.h"
#include <stdio.h>
#include <fcntl.h>
#ifdef HYSS_WIN32
#include "win32/time.h"
#include "win32/signal.h"
#include "win32/hyss_win32_globals.h"
#include "win32/winutil.h"
#include <process.h>
#endif
#if HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#if HAVE_SIGNAL_H
#include <signal.h>
#endif
#if HAVE_SETLOCALE
#include <locale.h>
#endif
#include "gear.h"
#include "gear_types.h"
#include "gear_extensions.h"
#include "hyss_ics.h"
#include "hyss_globals.h"
#include "hyss_main.h"
#include "hyss_syslog.h"
#include "fopen_wrappers.h"
#include "extslib/standard/hyss_standard.h"
#include "extslib/standard/hyss_string.h"
#include "extslib/date/hyss_date.h"
#include "hyss_variables.h"
#include "extslib/standard/credits.h"
#ifdef HYSS_WIN32
#include <io.h>
#include "win32/hyss_registry.h"
#include "extslib/standard/flock_compat.h"
#endif
#include "hyss_syslog.h"
#include "Gear/gear_exceptions.h"

#if HYSS_SIGCHILD
#include <sys/types.h>
#include <sys/wait.h>
#endif

#include "gear_compile.h"
#include "gear_execute.h"
#include "gear_highlight.h"
#include "gear_extensions.h"
#include "gear_ics.h"
#include "gear_dtrace.h"

#include "hyss_content_types.h"
#include "hyss_ticks.h"
#include "hyss_streams.h"
#include "hyss_open_temporary_file.h"

#include "SAPI.h"
#include "rfc1867.h"

#include "extslib/standard/html_tables.h"

#if HAVE_MMAP || defined(HYSS_WIN32)
# if HAVE_UNISTD_H
#  include <unistd.h>
#  if defined(_SC_PAGESIZE)
#    define REAL_PAGE_SIZE sysconf(_SC_PAGESIZE);
#  elif defined(_SC_PAGE_SIZE)
#    define REAL_PAGE_SIZE sysconf(_SC_PAGE_SIZE);
#  endif
# endif
# if HAVE_SYS_MMAN_H
#  include <sys/mman.h>
# endif
# ifndef REAL_PAGE_SIZE
#  ifdef PAGE_SIZE
#   define REAL_PAGE_SIZE PAGE_SIZE
#  else
#   define REAL_PAGE_SIZE 4096
#  endif
# endif
#endif
/* }}} */

HYSSAPI int (*hyss_register_internal_extensions_func)(void) = hyss_register_internal_extensions;

#ifndef ZTS
hyss_core_globals core_globals;
#else
HYSSAPI int core_globals_id;
#endif

#define SAFE_FILENAME(f) ((f)?(f):"-")

static char *get_safe_charset_hint(void) {
	GEAR_TLS char *lastHint = NULL;
	GEAR_TLS char *lastCodeset = NULL;
	char *hint = SG(default_charset);
	size_t len = strlen(hint);
	size_t i = 0;

	if (lastHint == SG(default_charset)) {
		return lastCodeset;
	}

	lastHint = hint;
	lastCodeset = NULL;

	for (i = 0; i < sizeof(charset_map)/sizeof(charset_map[0]); i++) {
		if (len == charset_map[i].codeset_len
			&& gear_binary_strcasecmp(hint, len, charset_map[i].codeset, len) == 0) {
			lastCodeset = (char*)charset_map[i].codeset;
			break;
		}
	}

	return lastCodeset;
}

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnSetFacility)
{
	const char *facility = ZSTR_VAL(new_value);

#ifdef LOG_AUTH
	if (!strcmp(facility, "LOG_AUTH") || !strcmp(facility, "auth") || !strcmp(facility, "security")) {
		PG(syslog_facility) = LOG_AUTH;
		return SUCCESS;
	}
#endif
#ifdef LOG_AUTHPRIV
	if (!strcmp(facility, "LOG_AUTHPRIV") || !strcmp(facility, "authpriv")) {
		PG(syslog_facility) = LOG_AUTHPRIV;
		return SUCCESS;
	}
#endif
#ifdef LOG_CRON
	if (!strcmp(facility, "LOG_CRON") || !strcmp(facility, "cron")) {
		PG(syslog_facility) = LOG_CRON;
		return SUCCESS;
	}
#endif
#ifdef LOG_DAEMON
	if (!strcmp(facility, "LOG_DAEMON") || !strcmp(facility, "daemon")) {
		PG(syslog_facility) = LOG_DAEMON;
		return SUCCESS;
	}
#endif
#ifdef LOG_FTP
	if (!strcmp(facility, "LOG_FTP") || !strcmp(facility, "ftp")) {
		PG(syslog_facility) = LOG_FTP;
		return SUCCESS;
	}
#endif
#ifdef LOG_KERN
	if (!strcmp(facility, "LOG_KERN") || !strcmp(facility, "kern")) {
		PG(syslog_facility) = LOG_KERN;
		return SUCCESS;
	}
#endif
#ifdef LOG_LPR
	if (!strcmp(facility, "LOG_LPR") || !strcmp(facility, "lpr")) {
		PG(syslog_facility) = LOG_LPR;
		return SUCCESS;
	}
#endif
#ifdef LOG_MAIL
	if (!strcmp(facility, "LOG_MAIL") || !strcmp(facility, "mail")) {
		PG(syslog_facility) = LOG_MAIL;
		return SUCCESS;
	}
#endif
#ifdef LOG_INTERNAL_MARK
	if (!strcmp(facility, "LOG_INTERNAL_MARK") || !strcmp(facility, "mark")) {
		PG(syslog_facility) = LOG_INTERNAL_MARK;
		return SUCCESS;
	}
#endif
#ifdef LOG_NEWS
	if (!strcmp(facility, "LOG_NEWS") || !strcmp(facility, "news")) {
		PG(syslog_facility) = LOG_NEWS;
		return SUCCESS;
	}
#endif
#ifdef LOG_SYSLOG
	if (!strcmp(facility, "LOG_SYSLOG") || !strcmp(facility, "syslog")) {
		PG(syslog_facility) = LOG_SYSLOG;
		return SUCCESS;
	}
#endif
#ifdef LOG_USER
	if (!strcmp(facility, "LOG_USER") || !strcmp(facility, "user")) {
		PG(syslog_facility) = LOG_USER;
		return SUCCESS;
	}
#endif
#ifdef LOG_UUCP
	if (!strcmp(facility, "LOG_UUCP") || !strcmp(facility, "uucp")) {
		PG(syslog_facility) = LOG_UUCP;
		return SUCCESS;
	}
#endif
#ifdef LOG_LOCAL0
	if (!strcmp(facility, "LOG_LOCAL0") || !strcmp(facility, "local0")) {
		PG(syslog_facility) = LOG_LOCAL0;
		return SUCCESS;
	}
#endif
#ifdef LOG_LOCAL1
	if (!strcmp(facility, "LOG_LOCAL1") || !strcmp(facility, "local1")) {
		PG(syslog_facility) = LOG_LOCAL1;
		return SUCCESS;
	}
#endif
#ifdef LOG_LOCAL2
	if (!strcmp(facility, "LOG_LOCAL2") || !strcmp(facility, "local2")) {
		PG(syslog_facility) = LOG_LOCAL2;
		return SUCCESS;
	}
#endif
#ifdef LOG_LOCAL3
	if (!strcmp(facility, "LOG_LOCAL3") || !strcmp(facility, "local3")) {
		PG(syslog_facility) = LOG_LOCAL3;
		return SUCCESS;
	}
#endif
#ifdef LOG_LOCAL4
	if (!strcmp(facility, "LOG_LOCAL4") || !strcmp(facility, "local4")) {
		PG(syslog_facility) = LOG_LOCAL4;
		return SUCCESS;
	}
#endif
#ifdef LOG_LOCAL5
	if (!strcmp(facility, "LOG_LOCAL5") || !strcmp(facility, "local5")) {
		PG(syslog_facility) = LOG_LOCAL5;
		return SUCCESS;
	}
#endif
#ifdef LOG_LOCAL6
	if (!strcmp(facility, "LOG_LOCAL6") || !strcmp(facility, "local6")) {
		PG(syslog_facility) = LOG_LOCAL6;
		return SUCCESS;
	}
#endif
#ifdef LOG_LOCAL7
	if (!strcmp(facility, "LOG_LOCAL7") || !strcmp(facility, "local7")) {
		PG(syslog_facility) = LOG_LOCAL7;
		return SUCCESS;
	}
#endif

	return FAILURE;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnSetPrecision)
{
	gear_long i;

	GEAR_ATOL(i, ZSTR_VAL(new_value));
	if (i >= -1) {
		EG(precision) = i;
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnSetSerializePrecision)
{
	gear_long i;

	GEAR_ATOL(i, ZSTR_VAL(new_value));
	if (i >= -1) {
		PG(serialize_precision) = i;
		return SUCCESS;
	} else {
		return FAILURE;
	}
}
/* }}} */


/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnChangeMemoryLimit)
{
	if (new_value) {
		PG(memory_limit) = gear_atol(ZSTR_VAL(new_value), ZSTR_LEN(new_value));
	} else {
		PG(memory_limit) = Z_L(1)<<30;		/* effectively, no limit */
	}
	return gear_set_memory_limit(PG(memory_limit));
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnSetLogFilter)
{
	const char *filter = ZSTR_VAL(new_value);

	if (!strcmp(filter, "all")) {
		PG(syslog_filter) = HYSS_SYSLOG_FILTER_ALL;
		return SUCCESS;
	}
	if (!strcmp(filter, "no-ctrl")) {
		PG(syslog_filter) = HYSS_SYSLOG_FILTER_NO_CTRL;
		return SUCCESS;
	}
	if (!strcmp(filter, "ascii")) {
		PG(syslog_filter) = HYSS_SYSLOG_FILTER_ASCII;
		return SUCCESS;
	}

	return FAILURE;
}
/* }}} */

/* {{{ hyss_disable_functions
 */
static void hyss_disable_functions(void)
{
	char *s = NULL, *e;

	if (!*(ICS_STR("disable_functions"))) {
		return;
	}

	e = PG(disable_functions) = strdup(ICS_STR("disable_functions"));
	if (e == NULL) {
		return;
	}
	while (*e) {
		switch (*e) {
			case ' ':
			case ',':
				if (s) {
					*e = '\0';
					gear_disable_function(s, e-s);
					s = NULL;
				}
				break;
			default:
				if (!s) {
					s = e;
				}
				break;
		}
		e++;
	}
	if (s) {
		gear_disable_function(s, e-s);
	}
}
/* }}} */

/* {{{ hyss_disable_classes
 */
static void hyss_disable_classes(void)
{
	char *s = NULL, *e;

	if (!*(ICS_STR("disable_classes"))) {
		return;
	}

	e = PG(disable_classes) = strdup(ICS_STR("disable_classes"));

	while (*e) {
		switch (*e) {
			case ' ':
			case ',':
				if (s) {
					*e = '\0';
					gear_disable_class(s, e-s);
					s = NULL;
				}
				break;
			default:
				if (!s) {
					s = e;
				}
				break;
		}
		e++;
	}
	if (s) {
		gear_disable_class(s, e-s);
	}
}
/* }}} */

/* {{{ hyss_binary_init
 */
static void hyss_binary_init(void)
{
	char *binary_location = NULL;
#ifdef HYSS_WIN32
	binary_location = (char *)malloc(MAXPATHLEN);
	if (binary_location && GetcAPIFileName(0, binary_location, MAXPATHLEN) == 0) {
		free(binary_location);
		PG(hyss_binary) = NULL;
	}
#else
	if (sapi_capi.executable_location) {
		binary_location = (char *)malloc(MAXPATHLEN);
		if (binary_location && !strchr(sapi_capi.executable_location, '/')) {
			char *envpath, *path;
			int found = 0;

			if ((envpath = getenv("PATH")) != NULL) {
				char *search_dir, search_path[MAXPATHLEN];
				char *last = NULL;
				gear_stat_t s;

				path = estrdup(envpath);
				search_dir = hyss_strtok_r(path, ":", &last);

				while (search_dir) {
					snprintf(search_path, MAXPATHLEN, "%s/%s", search_dir, sapi_capi.executable_location);
					if (VCWD_REALPATH(search_path, binary_location) && !VCWD_ACCESS(binary_location, X_OK) && VCWD_STAT(binary_location, &s) == 0 && S_ISREG(s.st_mode)) {
						found = 1;
						break;
					}
					search_dir = hyss_strtok_r(NULL, ":", &last);
				}
				efree(path);
			}
			if (!found) {
				free(binary_location);
				binary_location = NULL;
			}
		} else if (!VCWD_REALPATH(sapi_capi.executable_location, binary_location) || VCWD_ACCESS(binary_location, X_OK)) {
			free(binary_location);
			binary_location = NULL;
		}
	}
#endif
	PG(hyss_binary) = binary_location;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateTimeout)
{
	if (stage==HYSS_ICS_STAGE_STARTUP) {
		/* Don't set a timeout on startup, only per-request */
		GEAR_ATOL(EG(timeout_seconds), ZSTR_VAL(new_value));
		return SUCCESS;
	}
	gear_unset_timeout();
	GEAR_ATOL(EG(timeout_seconds), ZSTR_VAL(new_value));
	gear_set_timeout(EG(timeout_seconds), 0);
	return SUCCESS;
}
/* }}} */

/* {{{ hyss_get_display_errors_mode() helper function
 */
static int hyss_get_display_errors_mode(char *value, size_t value_length)
{
	int mode;

	if (!value) {
		return HYSS_DISPLAY_ERRORS_STDOUT;
	}

	if (value_length == 2 && !strcasecmp("on", value)) {
		mode = HYSS_DISPLAY_ERRORS_STDOUT;
	} else if (value_length == 3 && !strcasecmp("yes", value)) {
		mode = HYSS_DISPLAY_ERRORS_STDOUT;
	} else if (value_length == 4 && !strcasecmp("true", value)) {
		mode = HYSS_DISPLAY_ERRORS_STDOUT;
	} else if (value_length == 6 && !strcasecmp(value, "stderr")) {
		mode = HYSS_DISPLAY_ERRORS_STDERR;
	} else if (value_length == 6 && !strcasecmp(value, "stdout")) {
		mode = HYSS_DISPLAY_ERRORS_STDOUT;
	} else {
		GEAR_ATOL(mode, value);
		if (mode && mode != HYSS_DISPLAY_ERRORS_STDOUT && mode != HYSS_DISPLAY_ERRORS_STDERR) {
			mode = HYSS_DISPLAY_ERRORS_STDOUT;
		}
	}

	return mode;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateDisplayErrors)
{
	PG(display_errors) = (gear_bool) hyss_get_display_errors_mode(ZSTR_VAL(new_value), ZSTR_LEN(new_value));

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_ICS_DISP
 */
static HYSS_ICS_DISP(display_errors_mode)
{
	int mode, cgi_or_cli;
	size_t tmp_value_length;
	char *tmp_value;

	if (type == GEAR_ICS_DISPLAY_ORIG && ics_entry->modified) {
		tmp_value = (ics_entry->orig_value ? ZSTR_VAL(ics_entry->orig_value) : NULL );
		tmp_value_length = (ics_entry->orig_value? ZSTR_LEN(ics_entry->orig_value) : 0);
	} else if (ics_entry->value) {
		tmp_value = ZSTR_VAL(ics_entry->value);
		tmp_value_length = ZSTR_LEN(ics_entry->value);
	} else {
		tmp_value = NULL;
		tmp_value_length = 0;
	}

	mode = hyss_get_display_errors_mode(tmp_value, tmp_value_length);

	/* Display 'On' for other SAPIs instead of STDOUT or STDERR */
	cgi_or_cli = (!strcmp(sapi_capi.name, "cli") || !strcmp(sapi_capi.name, "cgi") || !strcmp(sapi_capi.name, "hyssdbg"));

	switch (mode) {
		case HYSS_DISPLAY_ERRORS_STDERR:
			if (cgi_or_cli ) {
				PUTS("STDERR");
			} else {
				PUTS("On");
			}
			break;

		case HYSS_DISPLAY_ERRORS_STDOUT:
			if (cgi_or_cli ) {
				PUTS("STDOUT");
			} else {
				PUTS("On");
			}
			break;

		default:
			PUTS("Off");
			break;
	}
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateDefaultCharset)
{
	if (new_value) {
		OnUpdateString(entry, new_value, mh_arg1, mh_arg2, mh_arg3, stage);
#ifdef HYSS_WIN32
		hyss_win32_cp_do_update(ZSTR_VAL(new_value));
#endif
	}
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateInternalEncoding)
{
	if (new_value) {
		OnUpdateString(entry, new_value, mh_arg1, mh_arg2, mh_arg3, stage);
#ifdef HYSS_WIN32
		hyss_win32_cp_do_update(ZSTR_VAL(new_value));
#endif
	}
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateInputEncoding)
{
	if (new_value) {
		OnUpdateString(entry, new_value, mh_arg1, mh_arg2, mh_arg3, stage);
#ifdef HYSS_WIN32
		hyss_win32_cp_do_update(NULL);
#endif
	}
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateOutputEncoding)
{
	if (new_value) {
		OnUpdateString(entry, new_value, mh_arg1, mh_arg2, mh_arg3, stage);
#ifdef HYSS_WIN32
		hyss_win32_cp_do_update(NULL);
#endif
	}
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateErrorLog)
{
	/* Only do the safemode/open_basedir check at runtime */
	if ((stage == HYSS_ICS_STAGE_RUNTIME || stage == HYSS_ICS_STAGE_HTACCESS) && new_value && strcmp(ZSTR_VAL(new_value), "syslog")) {
		if (PG(open_basedir) && hyss_check_open_basedir(ZSTR_VAL(new_value))) {
			return FAILURE;
		}
	}
	OnUpdateString(entry, new_value, mh_arg1, mh_arg2, mh_arg3, stage);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnUpdateMailLog)
{
	/* Only do the safemode/open_basedir check at runtime */
	if ((stage == HYSS_ICS_STAGE_RUNTIME || stage == HYSS_ICS_STAGE_HTACCESS) && new_value) {
		if (PG(open_basedir) && hyss_check_open_basedir(ZSTR_VAL(new_value))) {
			return FAILURE;
		}
	}
	OnUpdateString(entry, new_value, mh_arg1, mh_arg2, mh_arg3, stage);
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_ICS_MH
 */
static HYSS_ICS_MH(OnChangeMailForceExtra)
{
	/* Don't allow changing it in htaccess */
	if (stage == HYSS_ICS_STAGE_HTACCESS) {
			return FAILURE;
	}
	return SUCCESS;
}
/* }}} */

/* defined in browscap.c */
HYSS_ICS_MH(OnChangeBrowscap);


/* Need to be read from the environment (?):
 * HYSS_AUTO_PREPEND_FILE
 * HYSS_AUTO_APPEND_FILE
 * HYSS_DOCUMENT_ROOT
 * HYSS_USER_DIR
 * HYSS_INCLUDE_PATH
 */

 /* Windows use the internal mail */
#if defined(HYSS_WIN32)
# define DEFAULT_SENDMAIL_PATH NULL
#elif defined(HYSS_PROG_SENDMAIL)
# define DEFAULT_SENDMAIL_PATH HYSS_PROG_SENDMAIL " -t -i "
#else
# define DEFAULT_SENDMAIL_PATH "/usr/sbin/sendmail -t -i"
#endif

/* {{{ HYSS_ICS
 */
HYSS_ICS_BEGIN()
	HYSS_ICS_ENTRY_EX("highlight.comment",		HL_COMMENT_COLOR,	HYSS_ICS_ALL,	NULL,			hyss_ics_color_displayer_cb)
	HYSS_ICS_ENTRY_EX("highlight.default",		HL_DEFAULT_COLOR,	HYSS_ICS_ALL,	NULL,			hyss_ics_color_displayer_cb)
	HYSS_ICS_ENTRY_EX("highlight.html",			HL_HTML_COLOR,		HYSS_ICS_ALL,	NULL,			hyss_ics_color_displayer_cb)
	HYSS_ICS_ENTRY_EX("highlight.keyword",		HL_KEYWORD_COLOR,	HYSS_ICS_ALL,	NULL,			hyss_ics_color_displayer_cb)
	HYSS_ICS_ENTRY_EX("highlight.string",		HL_STRING_COLOR,	HYSS_ICS_ALL,	NULL,			hyss_ics_color_displayer_cb)

	STD_HYSS_ICS_ENTRY_EX("display_errors",		"1",		HYSS_ICS_ALL,		OnUpdateDisplayErrors,	display_errors,			hyss_core_globals,	core_globals, display_errors_mode)
	STD_HYSS_ICS_BOOLEAN("display_startup_errors",	"0",	HYSS_ICS_ALL,		OnUpdateBool,			display_startup_errors,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("enable_dl",			"1",		HYSS_ICS_SYSTEM,		OnUpdateBool,			enable_dl,				hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("expose_hyss",			"1",		HYSS_ICS_SYSTEM,		OnUpdateBool,			expose_hyss,				hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("docref_root", 			"", 		HYSS_ICS_ALL,		OnUpdateString,			docref_root,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("docref_ext",				"",			HYSS_ICS_ALL,		OnUpdateString,			docref_ext,				hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("html_errors",			"1",		HYSS_ICS_ALL,		OnUpdateBool,			html_errors,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("xmlrpc_errors",		"0",		HYSS_ICS_SYSTEM,		OnUpdateBool,			xmlrpc_errors,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("xmlrpc_error_number",	"0",		HYSS_ICS_ALL,		OnUpdateLong,			xmlrpc_error_number,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("max_input_time",			"-1",	HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateLong,			max_input_time,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("ignore_user_abort",	"0",		HYSS_ICS_ALL,		OnUpdateBool,			ignore_user_abort,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("implicit_flush",		"0",		HYSS_ICS_ALL,		OnUpdateBool,			implicit_flush,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("log_errors",			"0",		HYSS_ICS_ALL,		OnUpdateBool,			log_errors,				hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("log_errors_max_len",	 "1024",		HYSS_ICS_ALL,		OnUpdateLong,			log_errors_max_len,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("ignore_repeated_errors",	"0",	HYSS_ICS_ALL,		OnUpdateBool,			ignore_repeated_errors,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("ignore_repeated_source",	"0",	HYSS_ICS_ALL,		OnUpdateBool,			ignore_repeated_source,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("report_memleaks",		"1",		HYSS_ICS_ALL,		OnUpdateBool,			report_memleaks,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("report_gear_debug",	"1",		HYSS_ICS_ALL,		OnUpdateBool,			report_gear_debug,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("output_buffering",		"0",		HYSS_ICS_PERDIR|HYSS_ICS_SYSTEM,	OnUpdateLong,	output_buffering,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("output_handler",			NULL,		HYSS_ICS_PERDIR|HYSS_ICS_SYSTEM,	OnUpdateString,	output_handler,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("register_argc_argv",	"1",		HYSS_ICS_PERDIR|HYSS_ICS_SYSTEM,	OnUpdateBool,	register_argc_argv,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("auto_globals_jit",		"1",		HYSS_ICS_PERDIR|HYSS_ICS_SYSTEM,	OnUpdateBool,	auto_globals_jit,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_BOOLEAN("short_open_tag",	DEFAULT_SHORT_OPEN_TAG,	HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateBool,			short_tags,				gear_compiler_globals,	compiler_globals)
	STD_HYSS_ICS_BOOLEAN("track_errors",			"0",		HYSS_ICS_ALL,		OnUpdateBool,			track_errors,			hyss_core_globals,	core_globals)

	STD_HYSS_ICS_ENTRY("unserialize_callback_func",	NULL,	HYSS_ICS_ALL,		OnUpdateString,			unserialize_callback_func,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("serialize_precision",	"-1",	HYSS_ICS_ALL,		OnSetSerializePrecision,			serialize_precision,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("arg_separator.output",	"&",		HYSS_ICS_ALL,		OnUpdateStringUnempty,	arg_separator.output,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("arg_separator.input",	"&",		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,	OnUpdateStringUnempty,	arg_separator.input,	hyss_core_globals,	core_globals)

	STD_HYSS_ICS_ENTRY("auto_append_file",		NULL,		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateString,			auto_append_file,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("auto_prepend_file",		NULL,		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateString,			auto_prepend_file,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("doc_root",				NULL,		HYSS_ICS_SYSTEM,		OnUpdateStringUnempty,	doc_root,				hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("default_charset",		HYSS_DEFAULT_CHARSET,	HYSS_ICS_ALL,	OnUpdateDefaultCharset,			default_charset,		sapi_globals_struct, sapi_globals)
	STD_HYSS_ICS_ENTRY("default_mimetype",		SAPI_DEFAULT_MIMETYPE,	HYSS_ICS_ALL,	OnUpdateString,			default_mimetype,		sapi_globals_struct, sapi_globals)
	STD_HYSS_ICS_ENTRY("internal_encoding",		NULL,			HYSS_ICS_ALL,	OnUpdateInternalEncoding,	internal_encoding,	hyss_core_globals, core_globals)
	STD_HYSS_ICS_ENTRY("input_encoding",			NULL,			HYSS_ICS_ALL,	OnUpdateInputEncoding,				input_encoding,		hyss_core_globals, core_globals)
	STD_HYSS_ICS_ENTRY("output_encoding",		NULL,			HYSS_ICS_ALL,	OnUpdateOutputEncoding,				output_encoding,	hyss_core_globals, core_globals)
	STD_HYSS_ICS_ENTRY("error_log",				NULL,		HYSS_ICS_ALL,		OnUpdateErrorLog,			error_log,				hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("extension_dir",			HYSS_EXTENSION_DIR,		HYSS_ICS_SYSTEM,		OnUpdateStringUnempty,	extension_dir,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("sys_temp_dir",			NULL,		HYSS_ICS_SYSTEM,		OnUpdateStringUnempty,	sys_temp_dir,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("include_path",			HYSS_INCLUDE_PATH,		HYSS_ICS_ALL,		OnUpdateStringUnempty,	include_path,			hyss_core_globals,	core_globals)
	HYSS_ICS_ENTRY("max_execution_time",			"30",		HYSS_ICS_ALL,			OnUpdateTimeout)
	STD_HYSS_ICS_ENTRY("open_basedir",			NULL,		HYSS_ICS_ALL,		OnUpdateBaseDir,			open_basedir,			hyss_core_globals,	core_globals)

	STD_HYSS_ICS_BOOLEAN("file_uploads",			"1",		HYSS_ICS_SYSTEM,		OnUpdateBool,			file_uploads,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("upload_max_filesize",	"2M",		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateLong,			upload_max_filesize,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("post_max_size",			"8M",		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateLong,			post_max_size,			sapi_globals_struct,sapi_globals)
	STD_HYSS_ICS_ENTRY("upload_tmp_dir",			NULL,		HYSS_ICS_SYSTEM,		OnUpdateStringUnempty,	upload_tmp_dir,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("max_input_nesting_level", "64",		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateLongGEZero,	max_input_nesting_level,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("max_input_vars",			"1000",		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateLongGEZero,	max_input_vars,						hyss_core_globals,	core_globals)

	STD_HYSS_ICS_ENTRY("user_dir",				NULL,		HYSS_ICS_SYSTEM,		OnUpdateString,			user_dir,				hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("variables_order",		"EGPCS",	HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateStringUnempty,	variables_order,		hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("request_order",			NULL,		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateString,	request_order,		hyss_core_globals,	core_globals)

	STD_HYSS_ICS_ENTRY("error_append_string",	NULL,		HYSS_ICS_ALL,		OnUpdateString,			error_append_string,	hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("error_prepend_string",	NULL,		HYSS_ICS_ALL,		OnUpdateString,			error_prepend_string,	hyss_core_globals,	core_globals)

	HYSS_ICS_ENTRY("SMTP",						"localhost",HYSS_ICS_ALL,		NULL)
	HYSS_ICS_ENTRY("smtp_port",					"25",		HYSS_ICS_ALL,		NULL)
	STD_HYSS_ICS_BOOLEAN("mail.add_x_header",			"0",		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateBool,			mail_x_header,			hyss_core_globals,	core_globals)
	STD_HYSS_ICS_ENTRY("mail.log",					NULL,		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnUpdateMailLog,			mail_log,			hyss_core_globals,	core_globals)
	HYSS_ICS_ENTRY("browscap",					NULL,		HYSS_ICS_SYSTEM,		OnChangeBrowscap)
	HYSS_ICS_ENTRY("memory_limit",				"128M",		HYSS_ICS_ALL,		OnChangeMemoryLimit)
	HYSS_ICS_ENTRY("precision",					"14",		HYSS_ICS_ALL,		OnSetPrecision)
	HYSS_ICS_ENTRY("sendmail_from",				NULL,		HYSS_ICS_ALL,		NULL)
	HYSS_ICS_ENTRY("sendmail_path",	DEFAULT_SENDMAIL_PATH,	HYSS_ICS_SYSTEM,		NULL)
	HYSS_ICS_ENTRY("mail.force_extra_parameters",NULL,		HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		OnChangeMailForceExtra)
	HYSS_ICS_ENTRY("disable_functions",			"",			HYSS_ICS_SYSTEM,		NULL)
	HYSS_ICS_ENTRY("disable_classes",			"",			HYSS_ICS_SYSTEM,		NULL)
	HYSS_ICS_ENTRY("max_file_uploads",			"20",			HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,		NULL)

	STD_HYSS_ICS_BOOLEAN("allow_url_fopen",		"1",		HYSS_ICS_SYSTEM,		OnUpdateBool,		allow_url_fopen,		hyss_core_globals,		core_globals)
	STD_HYSS_ICS_BOOLEAN("allow_url_include",	"0",		HYSS_ICS_SYSTEM,		OnUpdateBool,		allow_url_include,		hyss_core_globals,		core_globals)
	STD_HYSS_ICS_BOOLEAN("enable_post_data_reading",	"1",	HYSS_ICS_SYSTEM|HYSS_ICS_PERDIR,	OnUpdateBool,	enable_post_data_reading,	hyss_core_globals,	core_globals)

	STD_HYSS_ICS_ENTRY("realpath_cache_size",	"4096K",	HYSS_ICS_SYSTEM,		OnUpdateLong,	realpath_cache_size_limit,	virtual_cwd_globals,	cwd_globals)
	STD_HYSS_ICS_ENTRY("realpath_cache_ttl",		"120",		HYSS_ICS_SYSTEM,		OnUpdateLong,	realpath_cache_ttl,			virtual_cwd_globals,	cwd_globals)

	STD_HYSS_ICS_ENTRY("user_ics.filename",		".user.ics",	HYSS_ICS_SYSTEM,		OnUpdateString,		user_ics_filename,	hyss_core_globals,		core_globals)
	STD_HYSS_ICS_ENTRY("user_ics.cache_ttl",		"300",			HYSS_ICS_SYSTEM,		OnUpdateLong,		user_ics_cache_ttl,	hyss_core_globals,		core_globals)
	STD_HYSS_ICS_ENTRY("hard_timeout",			"2",			HYSS_ICS_SYSTEM,		OnUpdateLong,		hard_timeout,		gear_executor_globals,	executor_globals)
#ifdef HYSS_WIN32
	STD_HYSS_ICS_BOOLEAN("windows.show_crt_warning",		"0",		HYSS_ICS_ALL,		OnUpdateBool,			windows_show_crt_warning,			hyss_core_globals,	core_globals)
#endif
	STD_HYSS_ICS_ENTRY("syslog.facility",		"LOG_USER",		HYSS_ICS_SYSTEM,		OnSetFacility,		syslog_facility,	hyss_core_globals,		core_globals)
	STD_HYSS_ICS_ENTRY("syslog.ident",		"hyss",			HYSS_ICS_SYSTEM,		OnUpdateString,		syslog_ident,		hyss_core_globals,		core_globals)
	STD_HYSS_ICS_ENTRY("syslog.filter",		"no-ctrl",		HYSS_ICS_ALL,		OnSetLogFilter,		syslog_filter,		hyss_core_globals, 		core_globals)
HYSS_ICS_END()
/* }}} */

/* True globals (no need for thread safety */
/* But don't make them a single int bitfield */
static int capi_initialized = 0;
static int capi_startup = 1;
static int capi_shutdown = 0;

/* {{{ hyss_during_capi_startup */
static int hyss_during_capi_startup(void)
{
	return capi_startup;
}
/* }}} */

/* {{{ hyss_during_capi_shutdown */
static int hyss_during_capi_shutdown(void)
{
	return capi_shutdown;
}
/* }}} */

/* {{{ hyss_get_capi_initialized
 */
HYSSAPI int hyss_get_capi_initialized(void)
{
	return capi_initialized;
}
/* }}} */

/* {{{ hyss_log_err_with_severity
 */
HYSSAPI GEAR_COLD void hyss_log_err_with_severity(char *log_message, int syslog_type_int)
{
	int fd = -1;
	time_t error_time;

	if (PG(in_error_log)) {
		/* prevent recursive invocation */
		return;
	}
	PG(in_error_log) = 1;

	/* Try to use the specified logging location. */
	if (PG(error_log) != NULL) {
#ifdef HAVE_SYSLOG_H
		if (!strcmp(PG(error_log), "syslog")) {
			hyss_syslog(syslog_type_int, "%s", log_message);
			PG(in_error_log) = 0;
			return;
		}
#endif
		fd = VCWD_OPEN_MODE(PG(error_log), O_CREAT | O_APPEND | O_WRONLY, 0644);
		if (fd != -1) {
			char *tmp;
			size_t len;
			gear_string *error_time_str;

			time(&error_time);
#ifdef ZTS
			if (!hyss_during_capi_startup()) {
				error_time_str = hyss_format_date("d-M-Y H:i:s e", 13, error_time, 1);
			} else {
				error_time_str = hyss_format_date("d-M-Y H:i:s e", 13, error_time, 0);
			}
#else
			error_time_str = hyss_format_date("d-M-Y H:i:s e", 13, error_time, 1);
#endif
			len = spprintf(&tmp, 0, "[%s] %s%s", ZSTR_VAL(error_time_str), log_message, HYSS_EOL);
#ifdef HYSS_WIN32
			hyss_flock(fd, 2);
			/* XXX should eventually write in a loop if len > UINT_MAX */
			hyss_ignore_value(write(fd, tmp, (unsigned)len));
#else
			hyss_ignore_value(write(fd, tmp, len));
#endif
			efree(tmp);
			gear_string_free(error_time_str);
			close(fd);
			PG(in_error_log) = 0;
			return;
		}
	}

	/* Otherwise fall back to the default logging location, if we have one */

	if (sapi_capi.log_message) {
		sapi_capi.log_message(log_message, syslog_type_int);
	}
	PG(in_error_log) = 0;
}
/* }}} */

/* {{{ hyss_write
   wrapper for cAPIs to use HYSSWRITE */
HYSSAPI size_t hyss_write(void *buf, size_t size)
{
	return HYSSWRITE(buf, size);
}
/* }}} */

/* {{{ hyss_printf
 */
HYSSAPI size_t hyss_printf(const char *format, ...)
{
	va_list args;
	size_t ret;
	char *buffer;
	size_t size;

	va_start(args, format);
	size = vspprintf(&buffer, 0, format, args);
	ret = HYSSWRITE(buffer, size);
	efree(buffer);
	va_end(args);

	return ret;
}
/* }}} */

/* {{{ hyss_verror */
/* hyss_verror is called from hyss_error_docref<n> functions.
 * Its purpose is to unify error messages and automatically generate clickable
 * html error messages if correcponding ics setting (html_errors) is activated.
 * 
 */
HYSSAPI GEAR_COLD void hyss_verror(const char *docref, const char *params, int type, const char *format, va_list args)
{
	gear_string *replace_buffer = NULL, *replace_origin = NULL;
	char *buffer = NULL, *docref_buf = NULL, *target = NULL;
	char *docref_target = "", *docref_root = "";
	char *p;
	int buffer_len = 0;
	const char *space = "";
	const char *class_name = "";
	const char *function;
	int origin_len;
	char *origin;
	char *message;
	int is_function = 0;

	/* get error text into buffer and escape for html if necessary */
	buffer_len = (int)vspprintf(&buffer, 0, format, args);

	if (PG(html_errors)) {
		replace_buffer = hyss_escape_html_entities((unsigned char*)buffer, buffer_len, 0, ENT_COMPAT, get_safe_charset_hint());
		/* Retry with substituting invalid chars on fail. */
		if (!replace_buffer || ZSTR_LEN(replace_buffer) < 1) {
			replace_buffer = hyss_escape_html_entities((unsigned char*)buffer, buffer_len, 0, ENT_COMPAT | ENT_HTML_SUBSTITUTE_ERRORS, get_safe_charset_hint());
		}

		efree(buffer);

		if (replace_buffer) {
			buffer = ZSTR_VAL(replace_buffer);
			buffer_len = (int)ZSTR_LEN(replace_buffer);
		} else {
			buffer = "";
			buffer_len = 0;
		}
	}

	/* which function caused the problem if any at all */
	if (hyss_during_capi_startup()) {
		function = "HYSS Startup";
	} else if (hyss_during_capi_shutdown()) {
		function = "HYSS Shutdown";
	} else if (EG(current_execute_data) &&
				EG(current_execute_data)->func &&
				GEAR_USER_CODE(EG(current_execute_data)->func->common.type) &&
				EG(current_execute_data)->opline &&
				EG(current_execute_data)->opline->opcode == GEAR_INCLUDE_OR_EVAL
	) {
		switch (EG(current_execute_data)->opline->extended_value) {
			case GEAR_EVAL:
				function = "eval";
				is_function = 1;
				break;
			case GEAR_INCLUDE:
				function = "include";
				is_function = 1;
				break;
			case GEAR_INCLUDE_ONCE:
				function = "include_once";
				is_function = 1;
				break;
			case GEAR_REQUIRE:
				function = "require";
				is_function = 1;
				break;
			case GEAR_REQUIRE_ONCE:
				function = "require_once";
				is_function = 1;
				break;
			default:
				function = "Unknown";
		}
	} else {
		function = get_active_function_name();
		if (!function || !strlen(function)) {
			function = "Unknown";
		} else {
			is_function = 1;
			class_name = get_active_class_name(&space);
		}
	}

	/* if we still have memory then format the origin */
	if (is_function) {
		origin_len = (int)spprintf(&origin, 0, "%s%s%s(%s)", class_name, space, function, params);
	} else {
		origin_len = (int)spprintf(&origin, 0, "%s", function);
	}

	if (PG(html_errors)) {
		replace_origin = hyss_escape_html_entities((unsigned char*)origin, origin_len, 0, ENT_COMPAT, get_safe_charset_hint());
		efree(origin);
		origin = ZSTR_VAL(replace_origin);
	}

	/* origin and buffer available, so lets come up with the error message */
	if (docref && docref[0] == '#') {
		docref_target = strchr(docref, '#');
		docref = NULL;
	}

	/* no docref given but function is known (the default) */
	if (!docref && is_function) {
		int doclen;
		while (*function == '_') {
			function++;
		}
		if (space[0] == '\0') {
			doclen = (int)spprintf(&docref_buf, 0, "function.%s", function);
		} else {
			doclen = (int)spprintf(&docref_buf, 0, "%s.%s", class_name, function);
		}
		while((p = strchr(docref_buf, '_')) != NULL) {
			*p = '-';
		}
		docref = hyss_strtolower(docref_buf, doclen);
	}

	/* we have a docref for a function AND
	 * - we show errors in html mode AND
	 * - the user wants to see the links
	 */
	if (docref && is_function && PG(html_errors) && strlen(PG(docref_root))) {
		if (strncmp(docref, "http://", 7)) {
			/* We don't have 'http://' so we use docref_root */

			char *ref;  /* temp copy for duplicated docref */

			docref_root = PG(docref_root);

			ref = estrdup(docref);
			if (docref_buf) {
				efree(docref_buf);
			}
			docref_buf = ref;
			/* strip of the target if any */
			p = strrchr(ref, '#');
			if (p) {
				target = estrdup(p);
				if (target) {
					docref_target = target;
					*p = '\0';
				}
			}
			/* add the extension if it is set in ics */
			if (PG(docref_ext) && strlen(PG(docref_ext))) {
				spprintf(&docref_buf, 0, "%s%s", ref, PG(docref_ext));
				efree(ref);
			}
			docref = docref_buf;
		}
		/* display html formatted or only show the additional links */
		if (PG(html_errors)) {
			spprintf(&message, 0, "%s [<a href='%s%s%s'>%s</a>]: %s", origin, docref_root, docref, docref_target, docref, buffer);
		} else {
			spprintf(&message, 0, "%s [%s%s%s]: %s", origin, docref_root, docref, docref_target, buffer);
		}
		if (target) {
			efree(target);
		}
	} else {
		spprintf(&message, 0, "%s: %s", origin, buffer);
	}
	if (replace_origin) {
		gear_string_free(replace_origin);
	} else {
		efree(origin);
	}
	if (docref_buf) {
		efree(docref_buf);
	}

	if (PG(track_errors) && capi_initialized && EG(active) &&
			(Z_TYPE(EG(user_error_handler)) == IS_UNDEF || !(EG(user_error_handler_error_reporting) & type))) {
		zval tmp;
		ZVAL_STRINGL(&tmp, buffer, buffer_len);
		if (EG(current_execute_data)) {
			if (gear_set_local_var_str("hyss_errormsg", sizeof("hyss_errormsg")-1, &tmp, 0) == FAILURE) {
				zval_ptr_dtor(&tmp);
			}
		} else {
			gear_hash_str_update_ind(&EG(symbol_table), "hyss_errormsg", sizeof("hyss_errormsg")-1, &tmp);
		}
	}
	if (replace_buffer) {
		gear_string_free(replace_buffer);
	} else {
		if (buffer_len > 0) {
			efree(buffer);
		}
	}

	hyss_error(type, "%s", message);
	efree(message);
}
/* }}} */

/* {{{ hyss_error_docref0 */

HYSSAPI GEAR_COLD void hyss_error_docref0(const char *docref, int type, const char *format, ...)
{
	va_list args;

	va_start(args, format);
	hyss_verror(docref, "", type, format, args);
	va_end(args);
}
/* }}} */

/* {{{ hyss_error_docref1 */

HYSSAPI GEAR_COLD void hyss_error_docref1(const char *docref, const char *param1, int type, const char *format, ...)
{
	va_list args;

	va_start(args, format);
	hyss_verror(docref, param1, type, format, args);
	va_end(args);
}
/* }}} */

/* {{{ hyss_error_docref2 */

HYSSAPI GEAR_COLD void hyss_error_docref2(const char *docref, const char *param1, const char *param2, int type, const char *format, ...)
{
	char *params;
	va_list args;

	spprintf(&params, 0, "%s,%s", param1, param2);
	va_start(args, format);
	hyss_verror(docref, params ? params : "...", type, format, args);
	va_end(args);
	if (params) {
		efree(params);
	}
}
/* }}} */

#ifdef HYSS_WIN32
#define HYSS_WIN32_ERROR_MSG_BUFFER_SIZE 512
HYSSAPI GEAR_COLD void hyss_win32_docref2_from_error(DWORD error, const char *param1, const char *param2) {
	if (error == 0) {
		hyss_error_docref2(NULL, param1, param2, E_WARNING, "%s", strerror(errno));
	} else {
		char buf[HYSS_WIN32_ERROR_MSG_BUFFER_SIZE + 1];
		size_t buf_len;

		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, error, 0, buf, HYSS_WIN32_ERROR_MSG_BUFFER_SIZE, NULL);
		buf_len = strlen(buf);
		if (buf_len >= 2) {
			buf[buf_len - 1] = '\0';
			buf[buf_len - 2] = '\0';
		}
		hyss_error_docref2(NULL, param1, param2, E_WARNING, "%s (code: %lu)", (char *)buf, error);
	}
}
#undef HYSS_WIN32_ERROR_MSG_BUFFER_SIZE
#endif

/* {{{ hyss_html_puts */
HYSSAPI void hyss_html_puts(const char *str, size_t size)
{
	gear_html_puts(str, size);
}
/* }}} */

/* {{{ hyss_error_cb
 extended error handling function */
static GEAR_COLD void hyss_error_cb(int type, const char *error_filename, const uint32_t error_lineno, const char *format, va_list args)
{
	char *buffer;
	int buffer_len, display;

	buffer_len = (int)vspprintf(&buffer, PG(log_errors_max_len), format, args);

	/* check for repeated errors to be ignored */
	if (PG(ignore_repeated_errors) && PG(last_error_message)) {
		/* no check for PG(last_error_file) is needed since it cannot
		 * be NULL if PG(last_error_message) is not NULL */
		if (strcmp(PG(last_error_message), buffer)
			|| (!PG(ignore_repeated_source)
				&& ((PG(last_error_lineno) != (int)error_lineno)
					|| strcmp(PG(last_error_file), error_filename)))) {
			display = 1;
		} else {
			display = 0;
		}
	} else {
		display = 1;
	}

	/* according to error handling mode, throw exception or show it */
	if (EG(error_handling) == EH_THROW) {
		switch (type) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_PARSE:
				/* fatal errors are real errors and cannot be made exceptions */
				break;
			case E_STRICT:
			case E_DEPRECATED:
			case E_USER_DEPRECATED:
				/* for the sake of BC to old damaged code */
				break;
			case E_NOTICE:
			case E_USER_NOTICE:
				/* notices are no errors and are not treated as such like E_WARNINGS */
				break;
			default:
				/* throw an exception if we are in EH_THROW mode
				 * but DO NOT overwrite a pending exception
				 */
				if (!EG(exception)) {
					gear_throw_error_exception(EG(exception_class), buffer, 0, type);
				}
				efree(buffer);
				return;
		}
	}

	/* store the error if it has changed */
	if (display) {
		if (PG(last_error_message)) {
			char *s = PG(last_error_message);
			PG(last_error_message) = NULL;
			free(s);
		}
		if (PG(last_error_file)) {
			char *s = PG(last_error_file);
			PG(last_error_file) = NULL;
			free(s);
		}
		if (!error_filename) {
			error_filename = "Unknown";
		}
		PG(last_error_type) = type;
		PG(last_error_message) = strdup(buffer);
		PG(last_error_file) = strdup(error_filename);
		PG(last_error_lineno) = error_lineno;
	}

	/* display/log the error if necessary */
	if (display && (EG(error_reporting) & type || (type & E_CORE))
		&& (PG(log_errors) || PG(display_errors) || (!capi_initialized))) {
		char *error_type_str;
		int syslog_type_int = LOG_NOTICE;

		switch (type) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
				error_type_str = "Fatal error";
				syslog_type_int = LOG_ERR;
				break;
			case E_RECOVERABLE_ERROR:
				error_type_str = "Recoverable fatal error";
				syslog_type_int = LOG_ERR;
				break;
			case E_WARNING:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_USER_WARNING:
				error_type_str = "Warning";
				syslog_type_int = LOG_WARNING;
				break;
			case E_PARSE:
				error_type_str = "Parse error";
				syslog_type_int = LOG_EMERG;
				break;
			case E_NOTICE:
			case E_USER_NOTICE:
				error_type_str = "Notice";
				syslog_type_int = LOG_NOTICE;
				break;
			case E_STRICT:
				error_type_str = "Strict Standards";
				syslog_type_int = LOG_INFO;
				break;
			case E_DEPRECATED:
			case E_USER_DEPRECATED:
				error_type_str = "Deprecated";
				syslog_type_int = LOG_INFO;
				break;
			default:
				error_type_str = "Unknown error";
				break;
		}

		if (!capi_initialized || PG(log_errors)) {
			char *log_buffer;
#ifdef HYSS_WIN32
			if (type == E_CORE_ERROR || type == E_CORE_WARNING) {
				syslog(LOG_ALERT, "HYSS %s: %s (%s)", error_type_str, buffer, GetCommandLine());
			}
#endif
			spprintf(&log_buffer, 0, "HYSS %s:  %s in %s on line %" PRIu32, error_type_str, buffer, error_filename, error_lineno);
			hyss_log_err_with_severity(log_buffer, syslog_type_int);
			efree(log_buffer);
		}

		if (PG(display_errors) && ((capi_initialized && !PG(during_request_startup)) || (PG(display_startup_errors)))) {
			if (PG(xmlrpc_errors)) {
				hyss_printf("<?xml version=\"1.0\"?><methodResponse><fault><value><struct><member><name>faultCode</name><value><int>" GEAR_LONG_FMT "</int></value></member><member><name>faultString</name><value><string>%s:%s in %s on line %" PRIu32 "</string></value></member></struct></value></fault></methodResponse>", PG(xmlrpc_error_number), error_type_str, buffer, error_filename, error_lineno);
			} else {
				char *prepend_string = ICS_STR("error_prepend_string");
				char *append_string = ICS_STR("error_append_string");

				if (PG(html_errors)) {
					if (type == E_ERROR || type == E_PARSE) {
						gear_string *buf = hyss_escape_html_entities((unsigned char*)buffer, buffer_len, 0, ENT_COMPAT, get_safe_charset_hint());
						hyss_printf("%s<br />\n<b>%s</b>:  %s in <b>%s</b> on line <b>%" PRIu32 "</b><br />\n%s", STR_PRINT(prepend_string), error_type_str, ZSTR_VAL(buf), error_filename, error_lineno, STR_PRINT(append_string));
						gear_string_free(buf);
					} else {
						hyss_printf("%s<br />\n<b>%s</b>:  %s in <b>%s</b> on line <b>%" PRIu32 "</b><br />\n%s", STR_PRINT(prepend_string), error_type_str, buffer, error_filename, error_lineno, STR_PRINT(append_string));
					}
				} else {
					/* Write CLI/CGI errors to stderr if display_errors = "stderr" */
					if ((!strcmp(sapi_capi.name, "cli") || !strcmp(sapi_capi.name, "cgi") || !strcmp(sapi_capi.name, "hyssdbg")) &&
						PG(display_errors) == HYSS_DISPLAY_ERRORS_STDERR
					) {
						fprintf(stderr, "%s: %s in %s on line %" PRIu32 "\n", error_type_str, buffer, error_filename, error_lineno);
#ifdef HYSS_WIN32
						fflush(stderr);
#endif
					} else {
						hyss_printf("%s\n%s: %s in %s on line %" PRIu32 "\n%s", STR_PRINT(prepend_string), error_type_str, buffer, error_filename, error_lineno, STR_PRINT(append_string));
					}
				}
			}
		}
#if GEAR_DEBUG
		if (PG(report_gear_debug)) {
			gear_bool trigger_break;

			switch (type) {
				case E_ERROR:
				case E_CORE_ERROR:
				case E_COMPILE_ERROR:
				case E_USER_ERROR:
					trigger_break=1;
					break;
				default:
					trigger_break=0;
					break;
			}
			gear_output_debug_string(trigger_break, "%s(%" PRIu32 ") : %s - %s", error_filename, error_lineno, error_type_str, buffer);
		}
#endif
	}

	/* Bail out if we can't recover */
	switch (type) {
		case E_CORE_ERROR:
			if(!capi_initialized) {
				/* bad error in cAPI startup - no way we can live with this */
				exit(-2);
			}
		/* no break - intentionally */
		case E_ERROR:
		case E_RECOVERABLE_ERROR:
		case E_PARSE:
		case E_COMPILE_ERROR:
		case E_USER_ERROR:
			EG(exit_status) = 255;
			if (capi_initialized) {
				if (!PG(display_errors) &&
				    !SG(headers_sent) &&
					SG(sapi_headers).http_response_code == 200
				) {
					sapi_header_line ctr = {0};

					ctr.line = "HTTP/1.0 500 Internal Server Error";
					ctr.line_len = sizeof("HTTP/1.0 500 Internal Server Error") - 1;
					sapi_header_op(SAPI_HEADER_REPLACE, &ctr);
				}
				/* the parser would return 1 (failure), we can bail out nicely */
				if (type != E_PARSE) {
					/* restore memory limit */
					gear_set_memory_limit(PG(memory_limit));
					efree(buffer);
					gear_objects_store_mark_destructed(&EG(objects_store));
					gear_bailout();
					return;
				}
			}
			break;
	}

	/* Log if necessary */
	if (!display) {
		efree(buffer);
		return;
	}

	if (PG(track_errors) && capi_initialized && EG(active)) {
		zval tmp;

		ZVAL_STRINGL(&tmp, buffer, buffer_len);
		if (EG(current_execute_data)) {
			if (gear_set_local_var_str("hyss_errormsg", sizeof("hyss_errormsg")-1, &tmp, 0) == FAILURE) {
				zval_ptr_dtor(&tmp);
			}
		} else {
			gear_hash_str_update_ind(&EG(symbol_table), "hyss_errormsg", sizeof("hyss_errormsg")-1, &tmp);
		}
	}

	efree(buffer);
}
/* }}} */

/* {{{ hyss_get_current_user
 */
HYSSAPI char *hyss_get_current_user(void)
{
	gear_stat_t *pstat;

	if (SG(request_info).current_user) {
		return SG(request_info).current_user;
	}

	/* FIXME: I need to have this somehow handled if
	USE_SAPI is defined, because cgi will also be
	interfaced in USE_SAPI */

	pstat = sapi_get_stat();

	if (!pstat) {
		return "";
	} else {
#ifdef HYSS_WIN32
		char *name = hyss_win32_get_username();
		int len;

		if (!name) {
			return "";
		}
		len = (int)strlen(name);
		name[len] = '\0';
		SG(request_info).current_user_length = len;
		SG(request_info).current_user = estrndup(name, len);
		free(name);
		return SG(request_info).current_user;
#else
		struct passwd *pwd;
#if defined(ZTS) && defined(HAVE_GETPWUID_R) && defined(_SC_GETPW_R_SIZE_MAX)
		struct passwd _pw;
		struct passwd *retpwptr = NULL;
		int pwbuflen = sysconf(_SC_GETPW_R_SIZE_MAX);
		char *pwbuf;

		if (pwbuflen < 1) {
			return "";
		}
		pwbuf = emalloc(pwbuflen);
		if (getpwuid_r(pstat->st_uid, &_pw, pwbuf, pwbuflen, &retpwptr) != 0) {
			efree(pwbuf);
			return "";
		}
		if (retpwptr == NULL) {
			efree(pwbuf);
			return "";
		}
		pwd = &_pw;
#else
		if ((pwd=getpwuid(pstat->st_uid))==NULL) {
			return "";
		}
#endif
		SG(request_info).current_user_length = strlen(pwd->pw_name);
		SG(request_info).current_user = estrndup(pwd->pw_name, SG(request_info).current_user_length);
#if defined(ZTS) && defined(HAVE_GETPWUID_R) && defined(_SC_GETPW_R_SIZE_MAX)
		efree(pwbuf);
#endif
		return SG(request_info).current_user;
#endif
	}
}
/* }}} */

/* {{{ proto bool set_time_limit(int seconds)
   Sets the maximum time a script can run */
HYSS_FUNCTION(set_time_limit)
{
	gear_long new_timeout;
	char *new_timeout_str;
	int new_timeout_strlen;
	gear_string *key;

	if (gear_parse_parameters(GEAR_NUM_ARGS(), "l", &new_timeout) == FAILURE) {
		return;
	}

	new_timeout_strlen = (int)gear_spprintf(&new_timeout_str, 0, GEAR_LONG_FMT, new_timeout);

	key = gear_string_init("max_execution_time", sizeof("max_execution_time")-1, 0);
	if (gear_alter_ics_entry_chars_ex(key, new_timeout_str, new_timeout_strlen, HYSS_ICS_USER, HYSS_ICS_STAGE_RUNTIME, 0) == SUCCESS) {
		RETVAL_TRUE;
	} else {
		RETVAL_FALSE;
	}
	gear_string_release_ex(key, 0);
	efree(new_timeout_str);
}
/* }}} */

/* {{{ hyss_fopen_wrapper_for_gear
 */
static FILE *hyss_fopen_wrapper_for_gear(const char *filename, gear_string **opened_path)
{
	return hyss_stream_open_wrapper_as_file((char *)filename, "rb", USE_PATH|IGNORE_URL_WIN|REPORT_ERRORS|STREAM_OPEN_FOR_INCLUDE, opened_path);
}
/* }}} */

static void hyss_gear_stream_closer(void *handle) /* {{{ */
{
	hyss_stream_close((hyss_stream*)handle);
}
/* }}} */

static void hyss_gear_stream_mmap_closer(void *handle) /* {{{ */
{
	hyss_stream_mmap_unmap((hyss_stream*)handle);
	hyss_gear_stream_closer(handle);
}
/* }}} */

static size_t hyss_gear_stream_fsizer(void *handle) /* {{{ */
{
	hyss_stream_statbuf  ssb;
	if (hyss_stream_stat((hyss_stream*)handle, &ssb) == 0) {
		return ssb.sb.st_size;
	}
	return 0;
}
/* }}} */

static int hyss_stream_open_for_gear(const char *filename, gear_file_handle *handle) /* {{{ */
{
	return hyss_stream_open_for_gear_ex(filename, handle, USE_PATH|REPORT_ERRORS|STREAM_OPEN_FOR_INCLUDE);
}
/* }}} */

HYSSAPI int hyss_stream_open_for_gear_ex(const char *filename, gear_file_handle *handle, int mode) /* {{{ */
{
	char *p;
	size_t len, mapped_len;
	hyss_stream *stream = hyss_stream_open_wrapper((char *)filename, "rb", mode, &handle->opened_path);

	if (stream) {
#if HAVE_MMAP || defined(HYSS_WIN32)
		size_t page_size = REAL_PAGE_SIZE;
#endif

		handle->filename = (char*)filename;
		handle->free_filename = 0;
		handle->handle.stream.handle  = stream;
		handle->handle.stream.reader  = (gear_stream_reader_t)_hyss_stream_read;
		handle->handle.stream.fsizer  = hyss_gear_stream_fsizer;
		handle->handle.stream.isatty  = 0;
		/* can we mmap immediately? */
		memset(&handle->handle.stream.mmap, 0, sizeof(handle->handle.stream.mmap));
		len = hyss_gear_stream_fsizer(stream);
		if (len != 0
#if HAVE_MMAP || defined(HYSS_WIN32)
		&& ((len - 1) % page_size) <= page_size - GEAR_MMAP_AHEAD
#endif
		&& hyss_stream_mmap_possible(stream)
		&& (p = hyss_stream_mmap_range(stream, 0, len, HYSS_STREAM_MAP_MODE_SHARED_READONLY, &mapped_len)) != NULL) {
			handle->handle.stream.closer   = hyss_gear_stream_mmap_closer;
			handle->handle.stream.mmap.buf = p;
			handle->handle.stream.mmap.len = mapped_len;
			handle->type = GEAR_HANDLE_MAPPED;
		} else {
			handle->handle.stream.closer = hyss_gear_stream_closer;
			handle->type = GEAR_HANDLE_STREAM;
		}
		/* suppress warning if this stream is not explicitly closed */
		hyss_stream_auto_cleanup(stream);

		return SUCCESS;
	}
	return FAILURE;
}
/* }}} */

static gear_string *hyss_resolve_path_for_gear(const char *filename, size_t filename_len) /* {{{ */
{
	return hyss_resolve_path(filename, filename_len, PG(include_path));
}
/* }}} */

/* {{{ hyss_get_configuration_directive_for_gear
 */
static zval *hyss_get_configuration_directive_for_gear(gear_string *name)
{
	return cfg_get_entry_ex(name);
}
/* }}} */

/* {{{ hyss_free_request_globals
 */
static void hyss_free_request_globals(void)
{
	if (PG(last_error_message)) {
		free(PG(last_error_message));
		PG(last_error_message) = NULL;
	}
	if (PG(last_error_file)) {
		free(PG(last_error_file));
		PG(last_error_file) = NULL;
	}
	if (PG(hyss_sys_temp_dir)) {
		efree(PG(hyss_sys_temp_dir));
		PG(hyss_sys_temp_dir) = NULL;
	}
}
/* }}} */

/* {{{ hyss_message_handler_for_gear
 */
static GEAR_COLD void hyss_message_handler_for_gear(gear_long message, const void *data)
{
	switch (message) {
		case ZMSG_FAILED_INCLUDE_FOPEN:
			hyss_error_docref("function.include", E_WARNING, "Failed opening '%s' for inclusion (include_path='%s')", hyss_strip_url_passwd((char *) data), STR_PRINT(PG(include_path)));
			break;
		case ZMSG_FAILED_REQUIRE_FOPEN:
			hyss_error_docref("function.require", E_COMPILE_ERROR, "Failed opening required '%s' (include_path='%s')", hyss_strip_url_passwd((char *) data), STR_PRINT(PG(include_path)));
			break;
		case ZMSG_FAILED_HIGHLIGHT_FOPEN:
			hyss_error_docref(NULL, E_WARNING, "Failed opening '%s' for highlighting", hyss_strip_url_passwd((char *) data));
			break;
		case ZMSG_MEMORY_LEAK_DETECTED:
		case ZMSG_MEMORY_LEAK_REPEATED:
#if GEAR_DEBUG
			if (EG(error_reporting) & E_WARNING) {
				char memory_leak_buf[1024];

				if (message==ZMSG_MEMORY_LEAK_DETECTED) {
					gear_leak_info *t = (gear_leak_info *) data;

					snprintf(memory_leak_buf, 512, "%s(%" PRIu32 ") :  Freeing " GEAR_ADDR_FMT " (%zu bytes), script=%s\n", t->filename, t->lineno, (size_t)t->addr, t->size, SAFE_FILENAME(SG(request_info).path_translated));
					if (t->orig_filename) {
						char relay_buf[512];

						snprintf(relay_buf, 512, "%s(%" PRIu32 ") : Actual location (location was relayed)\n", t->orig_filename, t->orig_lineno);
						strlcat(memory_leak_buf, relay_buf, sizeof(memory_leak_buf));
					}
				} else {
					unsigned long leak_count = (gear_uintptr_t) data;

					snprintf(memory_leak_buf, 512, "Last leak repeated %lu time%s\n", leak_count, (leak_count>1?"s":""));
				}
#	if defined(HYSS_WIN32)
				OutputDebugString(memory_leak_buf);
#	else
				fprintf(stderr, "%s", memory_leak_buf);
#	endif
			}
#endif
			break;
		case ZMSG_MEMORY_LEAKS_GRAND_TOTAL:
#if GEAR_DEBUG
			if (EG(error_reporting) & E_WARNING) {
				char memory_leak_buf[512];

				snprintf(memory_leak_buf, 512, "=== Total %d memory leaks detected ===\n", *((uint32_t *) data));
#	if defined(HYSS_WIN32)
				OutputDebugString(memory_leak_buf);
#	else
				fprintf(stderr, "%s", memory_leak_buf);
#	endif
			}
#endif
			break;
		case ZMSG_LOG_SCRIPT_NAME: {
				struct tm *ta, tmbuf;
				time_t curtime;
				char *datetime_str, asctimebuf[52];
				char memory_leak_buf[4096];

				time(&curtime);
				ta = hyss_localtime_r(&curtime, &tmbuf);
				datetime_str = hyss_asctime_r(ta, asctimebuf);
				if (datetime_str) {
					datetime_str[strlen(datetime_str)-1]=0;	/* get rid of the trailing newline */
					snprintf(memory_leak_buf, sizeof(memory_leak_buf), "[%s]  Script:  '%s'\n", datetime_str, SAFE_FILENAME(SG(request_info).path_translated));
				} else {
					snprintf(memory_leak_buf, sizeof(memory_leak_buf), "[null]  Script:  '%s'\n", SAFE_FILENAME(SG(request_info).path_translated));
				}
#	if defined(HYSS_WIN32)
				OutputDebugString(memory_leak_buf);
#	else
				fprintf(stderr, "%s", memory_leak_buf);
#	endif
			}
			break;
	}
}
/* }}} */


void hyss_on_timeout(int seconds)
{
	PG(connection_status) |= HYSS_CONNECTION_TIMEOUT;
}

#if HYSS_SIGCHILD
/* {{{ sigchld_handler
 */
static void sigchld_handler(int apar)
{
	int errno_save = errno;

	while (waitpid(-1, NULL, WNOHANG) > 0);
	signal(SIGCHLD, sigchld_handler);

	errno = errno_save;
}
/* }}} */
#endif

/* {{{ hyss_request_startup
 */
int hyss_request_startup(void)
{
	int retval = SUCCESS;

	gear_interned_strings_activate();

#ifdef HAVE_DTRACE
	DTRACE_REQUEST_STARTUP(SAFE_FILENAME(SG(request_info).path_translated), SAFE_FILENAME(SG(request_info).request_uri), (char *)SAFE_FILENAME(SG(request_info).request_method));
#endif /* HAVE_DTRACE */

#ifdef HYSS_WIN32
# if defined(ZTS)
	_configthreadlocale(_ENABLE_PER_THREAD_LOCALE);
# endif
	PG(com_initialized) = 0;
#endif

#if HYSS_SIGCHILD
	signal(SIGCHLD, sigchld_handler);
#endif

	gear_try {
		PG(in_error_log) = 0;
		PG(during_request_startup) = 1;

		hyss_output_activate();

		/* initialize global variables */
		PG(cAPIs_activated) = 0;
		PG(header_is_being_sent) = 0;
		PG(connection_status) = HYSS_CONNECTION_NORMAL;
		PG(in_user_include) = 0;

		gear_activate();
		sapi_activate();

#ifdef GEAR_SIGNALS
		gear_signal_activate();
#endif

		if (PG(max_input_time) == -1) {
			gear_set_timeout(EG(timeout_seconds), 1);
		} else {
			gear_set_timeout(PG(max_input_time), 1);
		}

		/* Disable realpath cache if an open_basedir is set */
		if (PG(open_basedir) && *PG(open_basedir)) {
			CWDG(realpath_cache_size_limit) = 0;
		}

		if (PG(expose_hyss)) {
			sapi_add_header(SAPI_HYSS_VERSION_HEADER, sizeof(SAPI_HYSS_VERSION_HEADER)-1, 1);
		}

		if (PG(output_handler) && PG(output_handler)[0]) {
			zval oh;

			ZVAL_STRING(&oh, PG(output_handler));
			hyss_output_start_user(&oh, 0, HYSS_OUTPUT_HANDLER_STDFLAGS);
			zval_ptr_dtor(&oh);
		} else if (PG(output_buffering)) {
			hyss_output_start_user(NULL, PG(output_buffering) > 1 ? PG(output_buffering) : 0, HYSS_OUTPUT_HANDLER_STDFLAGS);
		} else if (PG(implicit_flush)) {
			hyss_output_set_implicit_flush(1);
		}

		/* We turn this off in hyss_execute_script() */
		/* PG(during_request_startup) = 0; */

		hyss_hash_environment();
		gear_activate_capis();
		PG(cAPIs_activated)=1;
	} gear_catch {
		retval = FAILURE;
	} gear_end_try();

	SG(sapi_started) = 1;

	return retval;
}
/* }}} */

/* {{{ hyss_request_shutdown_for_exec
 */
void hyss_request_shutdown_for_exec(void *dummy)
{

	/* used to close fd's in the 3..255 range here, but it's problematic
	 */
	gear_interned_strings_deactivate();
	shutdown_memory_manager(1, 1);
}
/* }}} */

/* {{{ hyss_request_shutdown
 */
void hyss_request_shutdown(void *dummy)
{
	gear_bool report_memleaks;

	EG(flags) |= EG_FLAGS_IN_SHUTDOWN;

	report_memleaks = PG(report_memleaks);

	/* EG(current_execute_data) points into nirvana and therefore cannot be safely accessed
	 * inside gear_executor callback functions.
	 */
	EG(current_execute_data) = NULL;

	hyss_deactivate_ticks();

	/* 1. Call all possible shutdown functions registered with register_shutdown_function() */
	if (PG(cAPIs_activated)) gear_try {
		hyss_call_shutdown_functions();
	} gear_end_try();

	/* 2. Call all possible __destruct() functions */
	gear_try {
		gear_call_destructors();
	} gear_end_try();

	/* 3. Flush all output buffers */
	gear_try {
		gear_bool send_buffer = SG(request_info).headers_only ? 0 : 1;

		if (CG(unclean_shutdown) && PG(last_error_type) == E_ERROR &&
			(size_t)PG(memory_limit) < gear_memory_usage(1)
		) {
			send_buffer = 0;
		}

		if (!send_buffer) {
			hyss_output_discard_all();
		} else {
			hyss_output_end_all();
		}
	} gear_end_try();

	/* 4. Reset max_execution_time (no longer executing hyss code after response sent) */
	gear_try {
		gear_unset_timeout();
	} gear_end_try();

	/* 5. Call all extensions RSHUTDOWN functions */
	if (PG(cAPIs_activated)) {
		gear_deactivate_capis();
	}

	/* 6. Shutdown output layer (send the set HTTP headers, cleanup output handlers, etc.) */
	gear_try {
		hyss_output_deactivate();
	} gear_end_try();

	/* 7. Free shutdown functions */
	if (PG(cAPIs_activated)) {
		hyss_free_shutdown_functions();
	}

	/* 8. Destroy super-globals */
	gear_try {
		int i;

		for (i=0; i<NUM_TRACK_VARS; i++) {
			zval_ptr_dtor(&PG(http_globals)[i]);
		}
	} gear_end_try();

	/* 9. free request-bound globals */
	hyss_free_request_globals();

	/* 10. Shutdown scanner/executor/compiler and restore ics entries */
	gear_deactivate();

	/* 11. Call all extensions post-RSHUTDOWN functions */
	gear_try {
		gear_post_deactivate_capis();
	} gear_end_try();

	/* 12. SAPI related shutdown (free stuff) */
	gear_try {
		sapi_deactivate();
	} gear_end_try();

	/* 13. free virtual CWD memory */
	virtual_cwd_deactivate();

	/* 14. Destroy stream hashes */
	gear_try {
		hyss_shutdown_stream_hashes();
	} gear_end_try();

	/* 15. Free Willy (here be crashes) */
	gear_interned_strings_deactivate();
	gear_try {
		shutdown_memory_manager(CG(unclean_shutdown) || !report_memleaks, 0);
	} gear_end_try();

	/* 16. Reset max_execution_time */
	gear_try {
		gear_unset_timeout();
	} gear_end_try();

#ifdef HYSS_WIN32
	if (PG(com_initialized)) {
		CoUninitialize();
		PG(com_initialized) = 0;
	}
#endif

#ifdef HAVE_DTRACE
	DTRACE_REQUEST_SHUTDOWN(SAFE_FILENAME(SG(request_info).path_translated), SAFE_FILENAME(SG(request_info).request_uri), (char *)SAFE_FILENAME(SG(request_info).request_method));
#endif /* HAVE_DTRACE */
}
/* }}} */

/* {{{ hyss_com_initialize
 */
HYSSAPI void hyss_com_initialize(void)
{
#ifdef HYSS_WIN32
	if (!PG(com_initialized)) {
		if (CoInitialize(NULL) == S_OK) {
			PG(com_initialized) = 1;
		}
	}
#endif
}
/* }}} */

/* {{{ hyss_output_wrapper
 */
static size_t hyss_output_wrapper(const char *str, size_t str_length)
{
	return hyss_output_write(str, str_length);
}
/* }}} */

#ifdef ZTS
/* {{{ core_globals_ctor
 */
static void core_globals_ctor(hyss_core_globals *core_globals)
{
	memset(core_globals, 0, sizeof(*core_globals));
	hyss_startup_ticks();
}
/* }}} */
#endif

/* {{{ core_globals_dtor
 */
static void core_globals_dtor(hyss_core_globals *core_globals)
{
	if (core_globals->last_error_message) {
		free(core_globals->last_error_message);
	}
	if (core_globals->last_error_file) {
		free(core_globals->last_error_file);
	}
	if (core_globals->disable_functions) {
		free(core_globals->disable_functions);
	}
	if (core_globals->disable_classes) {
		free(core_globals->disable_classes);
	}
	if (core_globals->hyss_binary) {
		free(core_globals->hyss_binary);
	}

	hyss_shutdown_ticks();
}
/* }}} */

HYSS_MINFO_FUNCTION(hyss_core) { /* {{{ */
	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "HYSS Version", HYSS_VERSION);
	hyss_info_print_table_end();
	DISPLAY_ICS_ENTRIES();
}
/* }}} */

/* {{{ hyss_register_extensions
 */
int hyss_register_extensions(gear_capi_entry * const * ptr, int count)
{
	gear_capi_entry * const * end = ptr + count;

	while (ptr < end) {
		if (*ptr) {
			if (gear_register_internal_capi(*ptr)==NULL) {
				return FAILURE;
			}
		}
		ptr++;
	}
	return SUCCESS;
}

/* A very long time ago hyss_capi_startup() was refactored in a way
 * which broke calling it with more than one additional cAPI.
 * This alternative to hyss_register_extensions() works around that
 * by walking the shallower structure.
 *
 */
static int hyss_register_extensions_bc(gear_capi_entry *ptr, int count)
{
	while (count--) {
		if (gear_register_internal_capi(ptr++) == NULL) {
			return FAILURE;
 		}
	}
	return SUCCESS;
}
/* }}} */

#ifdef HYSS_WIN32
static _invalid_parameter_handler old_invalid_parameter_handler;

void dummy_invalid_parameter_handler(
		const wchar_t *expression,
		const wchar_t *function,
		const wchar_t *file,
		unsigned int   line,
		uintptr_t      pEwserved)
{
	static int called = 0;
	char buf[1024];
	int len;

	if (!called) {
			if(PG(windows_show_crt_warning)) {
			called = 1;
			if (function) {
				if (file) {
					len = _snprintf(buf, sizeof(buf)-1, "Invalid parameter detected in CRT function '%ws' (%ws:%u)", function, file, line);
				} else {
					len = _snprintf(buf, sizeof(buf)-1, "Invalid parameter detected in CRT function '%ws'", function);
				}
			} else {
				len = _snprintf(buf, sizeof(buf)-1, "Invalid CRT parameter detected (function not known)");
			}
			gear_error(E_WARNING, "%s", buf);
			called = 0;
		}
	}
}
#endif

/* {{{ hyss_capi_startup
 */
int hyss_capi_startup(sapi_capi_struct *sf, gear_capi_entry *additional_capis, uint32_t num_additional_capis)
{
	gear_utility_functions zuf;
	gear_utility_values zuv;
	int retval = SUCCESS, capi_number=0;	/* for REGISTER_ICS_ENTRIES() */
	char *hyss_os;
	gear_capi_entry *cAPI;

#ifdef HYSS_WIN32
	WORD wVersionRequested = MAKEWORD(2, 0);
	WSADATA wsaData;

	hyss_os = "WINNT";

	old_invalid_parameter_handler =
		_set_invalid_parameter_handler(dummy_invalid_parameter_handler);
	if (old_invalid_parameter_handler != NULL) {
		_set_invalid_parameter_handler(old_invalid_parameter_handler);
	}

	/* Disable the message box for assertions.*/
	_CrtSetReportMode(_CRT_ASSERT, 0);
#else
	hyss_os = HYSS_OS;
#endif

#ifdef ZTS
	(void)ts_resource(0);
#endif

#ifdef HYSS_WIN32
	if (!hyss_win32_init_random_bytes()) {
		fprintf(stderr, "\ncrypt algorithm provider initialization failed\n");
		return FAILURE;
	}
#endif

	capi_shutdown = 0;
	capi_startup = 1;
	sapi_initialize_empty_request();
	sapi_activate();

	if (capi_initialized) {
		return SUCCESS;
	}

	sapi_capi = *sf;

	hyss_output_startup();

#ifdef ZTS
	ts_allocate_id(&core_globals_id, sizeof(hyss_core_globals), (ts_allocate_ctor) core_globals_ctor, (ts_allocate_dtor) core_globals_dtor);
#ifdef HYSS_WIN32
	ts_allocate_id(&hyss_win32_core_globals_id, sizeof(hyss_win32_core_globals), (ts_allocate_ctor) hyss_win32_core_globals_ctor, (ts_allocate_dtor) hyss_win32_core_globals_dtor);
#endif
#else
	memset(&core_globals, 0, sizeof(core_globals));
	hyss_startup_ticks();
#endif
	gc_globals_ctor();

	zuf.error_function = hyss_error_cb;
	zuf.printf_function = hyss_printf;
	zuf.write_function = hyss_output_wrapper;
	zuf.fopen_function = hyss_fopen_wrapper_for_gear;
	zuf.message_handler = hyss_message_handler_for_gear;
	zuf.get_configuration_directive = hyss_get_configuration_directive_for_gear;
	zuf.ticks_function = hyss_run_ticks;
	zuf.on_timeout = hyss_on_timeout;
	zuf.stream_open_function = hyss_stream_open_for_gear;
	zuf.printf_to_smart_string_function = hyss_printf_to_smart_string;
	zuf.printf_to_smart_str_function = hyss_printf_to_smart_str;
	zuf.getenv_function = sapi_getenv;
	zuf.resolve_path_function = hyss_resolve_path_for_gear;
	gear_startup(&zuf, NULL);

#if HAVE_SETLOCALE
	setlocale(LC_CTYPE, "");
	gear_update_current_locale();
#endif

#if HAVE_TZSET
	tzset();
#endif

#ifdef HYSS_WIN32
	/* start up winsock services */
	if (WSAStartup(wVersionRequested, &wsaData) != 0) {
		hyss_printf("\nwinsock.dll unusable. %d\n", WSAGetLastError());
		return FAILURE;
	}
#endif

	le_index_ptr = gear_register_list_destructors_ex(NULL, NULL, "index pointer", 0);

	/* Register constants */
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_VERSION", HYSS_VERSION, sizeof(HYSS_VERSION)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_MAJOR_VERSION", HYSS_MAJOR_VERSION, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_MINOR_VERSION", HYSS_MINOR_VERSION, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_RELEASE_VERSION", HYSS_RELEASE_VERSION, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_EXTRA_VERSION", HYSS_EXTRA_VERSION, sizeof(HYSS_EXTRA_VERSION) - 1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_VERSION_ID", HYSS_VERSION_ID, CONST_PERSISTENT | CONST_CS);
#ifdef ZTS
	REGISTER_MAIN_LONG_CONSTANT("HYSS_ZTS", 1, CONST_PERSISTENT | CONST_CS);
#else
	REGISTER_MAIN_LONG_CONSTANT("HYSS_ZTS", 0, CONST_PERSISTENT | CONST_CS);
#endif
	REGISTER_MAIN_LONG_CONSTANT("HYSS_DEBUG", HYSS_DEBUG, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_OS", hyss_os, strlen(hyss_os), CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_OS_FAMILY", HYSS_OS_FAMILY, sizeof(HYSS_OS_FAMILY)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_SAPI", sapi_capi.name, strlen(sapi_capi.name), CONST_PERSISTENT | CONST_CS | CONST_NO_FILE_CACHE);
	REGISTER_MAIN_STRINGL_CONSTANT("DEFAULT_INCLUDE_PATH", HYSS_INCLUDE_PATH, sizeof(HYSS_INCLUDE_PATH)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HEXA_INSTALL_DIR", HEXA_INSTALLDIR, sizeof(HEXA_INSTALLDIR)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HEXA_EXTENSION_DIR", HYSS_EXTENSION_DIR, sizeof(HYSS_EXTENSION_DIR)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_EXTENSION_DIR", HYSS_EXTENSION_DIR, sizeof(HYSS_EXTENSION_DIR)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_PREFIX", HYSS_PREFIX, sizeof(HYSS_PREFIX)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_BINDIR", HYSS_BINDIR, sizeof(HYSS_BINDIR)-1, CONST_PERSISTENT | CONST_CS);
#ifndef HYSS_WIN32
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_MANDIR", HYSS_MANDIR, sizeof(HYSS_MANDIR)-1, CONST_PERSISTENT | CONST_CS);
#endif
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_LIBDIR", HYSS_LIBDIR, sizeof(HYSS_LIBDIR)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_DATADIR", HYSS_DATADIR, sizeof(HYSS_DATADIR)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_SYSCONFDIR", HYSS_SYSCONFDIR, sizeof(HYSS_SYSCONFDIR)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_LOCALSTATEDIR", HYSS_LOCALSTATEDIR, sizeof(HYSS_LOCALSTATEDIR)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_CONFIG_FILE_PATH", HYSS_CONFIG_FILE_PATH, strlen(HYSS_CONFIG_FILE_PATH), CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_CONFIG_FILE_SCAN_DIR", HYSS_CONFIG_FILE_SCAN_DIR, sizeof(HYSS_CONFIG_FILE_SCAN_DIR)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_SHLIB_SUFFIX", HYSS_SHLIB_SUFFIX, sizeof(HYSS_SHLIB_SUFFIX)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_STRINGL_CONSTANT("HYSS_EOL", HYSS_EOL, sizeof(HYSS_EOL)-1, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_MAXPATHLEN", MAXPATHLEN, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_INT_MAX", GEAR_LONG_MAX, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_INT_MIN", GEAR_LONG_MIN, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_INT_SIZE", SIZEOF_GEAR_LONG, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_FD_SETSIZE", FD_SETSIZE, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_FLOAT_DIG", DBL_DIG, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_DOUBLE_CONSTANT("HYSS_FLOAT_EPSILON", DBL_EPSILON, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_DOUBLE_CONSTANT("HYSS_FLOAT_MAX", DBL_MAX, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_DOUBLE_CONSTANT("HYSS_FLOAT_MIN", DBL_MIN, CONST_PERSISTENT | CONST_CS);

#ifdef HYSS_WIN32
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_VERSION_MAJOR",      EG(windows_version_info).dwMajorVersion, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_VERSION_MINOR",      EG(windows_version_info).dwMinorVersion, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_VERSION_BUILD",      EG(windows_version_info).dwBuildNumber, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_VERSION_PLATFORM",   EG(windows_version_info).dwPlatformId, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_VERSION_SP_MAJOR",   EG(windows_version_info).wServicePackMajor, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_VERSION_SP_MINOR",   EG(windows_version_info).wServicePackMinor, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_VERSION_SUITEMASK",  EG(windows_version_info).wSuiteMask, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_VERSION_PRODUCTTYPE", EG(windows_version_info).wProductType, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_NT_DOMAIN_CONTROLLER", VER_NT_DOMAIN_CONTROLLER, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_NT_SERVER", VER_NT_SERVER, CONST_PERSISTENT | CONST_CS);
	REGISTER_MAIN_LONG_CONSTANT("HYSS_WINDOWS_NT_WORKSTATION", VER_NT_WORKSTATION, CONST_PERSISTENT | CONST_CS);
#endif

	hyss_binary_init();
	if (PG(hyss_binary)) {
		REGISTER_MAIN_STRINGL_CONSTANT("HYSS_BINARY", PG(hyss_binary), strlen(PG(hyss_binary)), CONST_PERSISTENT | CONST_CS | CONST_NO_FILE_CACHE);
	} else {
		REGISTER_MAIN_STRINGL_CONSTANT("HYSS_BINARY", "", 0, CONST_PERSISTENT | CONST_CS | CONST_NO_FILE_CACHE);
	}

	hyss_output_register_constants();
	hyss_rfc1867_register_constants();

	/* this will read in hyss.ics, set up the configuration parameters,
	   load gear extensions and register hyss function extensions
	   to be loaded later */
	if (hyss_init_config() == FAILURE) {
		return FAILURE;
	}

	/* Register HYSS core ics entries */
	REGISTER_ICS_ENTRIES();

	/* Register Gear ics entries */
	gear_register_standard_ics_entries();

#ifdef GEAR_WIN32
	/* Until the current ics values was setup, the current cp is 65001.
		If the actual ics vaues are different, some stuff needs to be updated.
		It concerns at least main_cwd_state and there might be more. As we're
		still in the startup phase, lets use the chance and reinit the relevant
		item according to the current codepage. Still, if ics_set() is used
		later on, a more intelligent way to update such stuff is needed.
		Startup/shutdown routines could involve touching globals and thus
		can't always be used on demand. */
	if (!hyss_win32_cp_use_unicode()) {
		virtual_cwd_main_cwd_init(1);
	}
#endif

	/* Disable realpath cache if an open_basedir is set */
	if (PG(open_basedir) && *PG(open_basedir)) {
		CWDG(realpath_cache_size_limit) = 0;
	}

	PG(have_called_openlog) = 0;

	/* initialize stream wrappers registry
	 * (this uses configuration parameters from hyss.ics)
	 */
	if (hyss_init_stream_wrappers(capi_number) == FAILURE)	{
		hyss_printf("HYSS:  Unable to initialize stream url wrappers.\n");
		return FAILURE;
	}

	zuv.html_errors = 1;
	zuv.import_use_extension = ".hyss";
	zuv.import_use_extension_length = (uint32_t)strlen(zuv.import_use_extension);
	hyss_startup_auto_globals();
	gear_set_utility_values(&zuv);
	hyss_startup_sapi_content_types();

	/* startup extensions statically compiled in */
	if (hyss_register_internal_extensions_func() == FAILURE) {
		hyss_printf("Unable to start builtin cAPIs\n");
		return FAILURE;
	}

	/* start additional HYSS extensions */
	hyss_register_extensions_bc(additional_capis, num_additional_capis);

	/* load and startup extensions compiled as shared objects (aka DLLs)
	   as requested by hyss.ics entries
	   these are loaded after initialization of internal extensions
	   as extensions *might* rely on things from extslib/standard
	   which is always an internal extension and to be initialized
	   ahead of all other internals
	 */
	hyss_ics_register_extensions();
	gear_startup_capis();

	/* start Gear extensions */
	gear_startup_extensions();

	gear_collect_capi_handlers();

	/* register additional functions */
	if (sapi_capi.additional_functions) {
		if ((cAPI = gear_hash_str_find_ptr(&capi_registry, "standard", sizeof("standard")-1)) != NULL) {
			EG(current_capi) = cAPI;
			gear_register_functions(NULL, sapi_capi.additional_functions, NULL, CAPI_PERSISTENT);
			EG(current_capi) = NULL;
		}
	}

	/* disable certain classes and functions as requested by hyss.ics */
	hyss_disable_functions();
	hyss_disable_classes();

	/* make core report what it should */
	if ((cAPI = gear_hash_str_find_ptr(&capi_registry, "core", sizeof("core")-1)) != NULL) {
		cAPI->version = HYSS_VERSION;
		cAPI->info_func = HYSS_MINFO(hyss_core);
	}

	if (gear_post_startup() != SUCCESS) {
		return FAILURE;
	}

	capi_initialized = 1;

	/* Check for deprecated directives */
	/* NOTE: If you add anything here, remember to add it to Makefile.global! */
	{
		struct {
			const long error_level;
			const char *phrase;
			const char *directives[17]; /* Remember to change this if the number of directives change */
		} directives[2] = {
			{
				E_DEPRECATED,
				"Directive '%s' is deprecated",
				{
					"track_errors",
					NULL
				}
			},
			{
				E_CORE_ERROR,
				"Directive '%s' is no longer available in HYSS",
				{
					"allow_call_time_pass_reference",
					"asp_tags",
					"define_syslog_variables",
					"highlight.bg",
					"magic_quotes_gpc",
					"magic_quotes_runtime",
					"magic_quotes_sybase",
					"register_globals",
					"register_long_arrays",
					"safe_mode",
					"safe_mode_gid",
					"safe_mode_include_dir",
					"safe_mode_exec_dir",
					"safe_mode_allowed_env_vars",
					"safe_mode_protected_env_vars",
					"gear.ze1_compatibility_mode",
					NULL
				}
			}
		};

		unsigned int i;

		gear_try {
			/* 2 = Count of deprecation structs */
			for (i = 0; i < 2; i++) {
				const char **p = directives[i].directives;

				while(*p) {
					gear_long value;

					if (cfg_get_long((char*)*p, &value) == SUCCESS && value) {
						gear_error(directives[i].error_level, directives[i].phrase, *p);
					}

					++p;
				}
			}
		} gear_catch {
			retval = FAILURE;
		} gear_end_try();
	}

	virtual_cwd_deactivate();

	sapi_deactivate();
	capi_startup = 0;

	shutdown_memory_manager(1, 0);
 	virtual_cwd_activate();

	gear_interned_strings_switch_storage(1);

#if GEAR_RC_DEBUG
	gear_rc_debug = 1;
#endif

	/* we're done */
	return retval;
}
/* }}} */

void hyss_capi_shutdown_for_exec(void)
{
	/* used to close fd's in the range 3.255 here, but it's problematic */
}

/* {{{ hyss_capi_shutdown_wrapper
 */
int hyss_capi_shutdown_wrapper(sapi_capi_struct *sapi_globals)
{
	hyss_capi_shutdown();
	return SUCCESS;
}
/* }}} */

/* {{{ hyss_capi_shutdown
 */
void hyss_capi_shutdown(void)
{
	int capi_number=0;	/* for UNREGISTER_ICS_ENTRIES() */

	capi_shutdown = 1;

	if (!capi_initialized) {
		return;
	}

	gear_interned_strings_switch_storage(0);

#ifdef ZTS
	ts_free_worker_threads();
#endif

#if GEAR_RC_DEBUG
	gear_rc_debug = 0;
#endif

#ifdef HYSS_WIN32
	(void)hyss_win32_shutdown_random_bytes();
#endif

	sapi_flush();

	gear_shutdown();

#ifdef HYSS_WIN32
	/*close winsock */
	WSACleanup();
#endif

	/* Destroys filter & transport registries too */
	hyss_shutdown_stream_wrappers(capi_number);

	UNREGISTER_ICS_ENTRIES();

	/* close down the ics config */
	hyss_shutdown_config();

#ifndef ZTS
	gear_ics_shutdown();
	shutdown_memory_manager(CG(unclean_shutdown), 1);
#else
	gear_ics_global_shutdown();
#endif

	hyss_output_shutdown();

#ifndef ZTS
	gear_interned_strings_dtor();
#endif

	capi_initialized = 0;

#ifndef ZTS
	core_globals_dtor(&core_globals);
	gc_globals_dtor();
#else
	ts_free_id(core_globals_id);
#endif

#ifdef HYSS_WIN32
	if (old_invalid_parameter_handler == NULL) {
		_set_invalid_parameter_handler(old_invalid_parameter_handler);
	}
#endif
}
/* }}} */

/* {{{ hyss_execute_script
 */
HYSSAPI int hyss_execute_script(gear_file_handle *primary_file)
{
	gear_file_handle *prepend_file_p, *append_file_p;
	gear_file_handle prepend_file = {{0}, NULL, NULL, 0, 0}, append_file = {{0}, NULL, NULL, 0, 0};
#if HAVE_BROKEN_GETCWD
	volatile int old_cwd_fd = -1;
#else
	char *old_cwd;
	ALLOCA_FLAG(use_heap)
#endif
	int retval = 0;

	EG(exit_status) = 0;
#ifndef HAVE_BROKEN_GETCWD
# define OLD_CWD_SIZE 4096
	old_cwd = do_alloca(OLD_CWD_SIZE, use_heap);
	old_cwd[0] = '\0';
#endif

	gear_try {
		char realfile[MAXPATHLEN];

#ifdef HYSS_WIN32
		if(primary_file->filename) {
			UpdateIniFromRegistry((char*)primary_file->filename);
		}
#endif

		PG(during_request_startup) = 0;

		if (primary_file->filename && !(SG(options) & SAPI_OPTION_NO_CHDIR)) {
#if HAVE_BROKEN_GETCWD
			/* this looks nasty to me */
			old_cwd_fd = open(".", 0);
#else
			hyss_ignore_value(VCWD_GETCWD(old_cwd, OLD_CWD_SIZE-1));
#endif
			VCWD_CHDIR_FILE(primary_file->filename);
		}

 		/* Only lookup the real file path and add it to the included_files list if already opened
		 *   otherwise it will get opened and added to the included_files list in gear_execute_scripts
		 */
 		if (primary_file->filename &&
 		    strcmp("Standard input code", primary_file->filename) &&
 			primary_file->opened_path == NULL &&
 			primary_file->type != GEAR_HANDLE_FILENAME
		) {
			if (expand_filepath(primary_file->filename, realfile)) {
				primary_file->opened_path = gear_string_init(realfile, strlen(realfile), 0);
				gear_hash_add_empty_element(&EG(included_files), primary_file->opened_path);
			}
		}

		if (PG(auto_prepend_file) && PG(auto_prepend_file)[0]) {
			prepend_file.filename = PG(auto_prepend_file);
			prepend_file.opened_path = NULL;
			prepend_file.free_filename = 0;
			prepend_file.type = GEAR_HANDLE_FILENAME;
			prepend_file_p = &prepend_file;
		} else {
			prepend_file_p = NULL;
		}

		if (PG(auto_append_file) && PG(auto_append_file)[0]) {
			append_file.filename = PG(auto_append_file);
			append_file.opened_path = NULL;
			append_file.free_filename = 0;
			append_file.type = GEAR_HANDLE_FILENAME;
			append_file_p = &append_file;
		} else {
			append_file_p = NULL;
		}
		if (PG(max_input_time) != -1) {
#ifdef HYSS_WIN32
			gear_unset_timeout();
#endif
			gear_set_timeout(ICS_INT("max_execution_time"), 0);
		}

		/*
		   If cli primary file has shabang line and there is a prepend file,
		   the `start_lineno` will be used by prepend file but not primary file,
		   save it and restore after prepend file been executed.
		 */
		if (CG(start_lineno) && prepend_file_p) {
			int orig_start_lineno = CG(start_lineno);

			CG(start_lineno) = 0;
			if (gear_execute_scripts(GEAR_REQUIRE, NULL, 1, prepend_file_p) == SUCCESS) {
				CG(start_lineno) = orig_start_lineno;
				retval = (gear_execute_scripts(GEAR_REQUIRE, NULL, 2, primary_file, append_file_p) == SUCCESS);
			}
		} else {
			retval = (gear_execute_scripts(GEAR_REQUIRE, NULL, 3, prepend_file_p, primary_file, append_file_p) == SUCCESS);
		}
	} gear_end_try();

	if (EG(exception)) {
		gear_try {
			gear_exception_error(EG(exception), E_ERROR);
		} gear_end_try();
	}

#if HAVE_BROKEN_GETCWD
	if (old_cwd_fd != -1) {
		fchdir(old_cwd_fd);
		close(old_cwd_fd);
	}
#else
	if (old_cwd[0] != '\0') {
		hyss_ignore_value(VCWD_CHDIR(old_cwd));
	}
	free_alloca(old_cwd, use_heap);
#endif
	return retval;
}
/* }}} */

/* {{{ hyss_execute_simple_script
 */
HYSSAPI int hyss_execute_simple_script(gear_file_handle *primary_file, zval *ret)
{
	char *old_cwd;
	ALLOCA_FLAG(use_heap)

	EG(exit_status) = 0;
#define OLD_CWD_SIZE 4096
	old_cwd = do_alloca(OLD_CWD_SIZE, use_heap);
	old_cwd[0] = '\0';

	gear_try {
#ifdef HYSS_WIN32
		if(primary_file->filename) {
			UpdateIniFromRegistry((char*)primary_file->filename);
		}
#endif

		PG(during_request_startup) = 0;

		if (primary_file->filename && !(SG(options) & SAPI_OPTION_NO_CHDIR)) {
			hyss_ignore_value(VCWD_GETCWD(old_cwd, OLD_CWD_SIZE-1));
			VCWD_CHDIR_FILE(primary_file->filename);
		}
		gear_execute_scripts(GEAR_REQUIRE, ret, 1, primary_file);
	} gear_end_try();

	if (old_cwd[0] != '\0') {
		hyss_ignore_value(VCWD_CHDIR(old_cwd));
	}

	free_alloca(old_cwd, use_heap);
	return EG(exit_status);
}
/* }}} */

/* {{{ hyss_handle_aborted_connection
 */
HYSSAPI void hyss_handle_aborted_connection(void)
{

	PG(connection_status) = HYSS_CONNECTION_ABORTED;
	hyss_output_set_status(HYSS_OUTPUT_DISABLED);

	if (!PG(ignore_user_abort)) {
		gear_bailout();
	}
}
/* }}} */

/* {{{ hyss_handle_auth_data
 */
HYSSAPI int hyss_handle_auth_data(const char *auth)
{
	int ret = -1;

	if (auth && auth[0] != '\0' && strncmp(auth, "Basic ", 6) == 0) {
		char *pass;
		gear_string *user;

		user = hyss_base64_decode((const unsigned char*)auth + 6, strlen(auth) - 6);
		if (user) {
			pass = strchr(ZSTR_VAL(user), ':');
			if (pass) {
				*pass++ = '\0';
				SG(request_info).auth_user = estrndup(ZSTR_VAL(user), ZSTR_LEN(user));
				SG(request_info).auth_password = estrdup(pass);
				ret = 0;
			}
			gear_string_free(user);
		}
	}

	if (ret == -1) {
		SG(request_info).auth_user = SG(request_info).auth_password = NULL;
	} else {
		SG(request_info).auth_digest = NULL;
	}

	if (ret == -1 && auth && auth[0] != '\0' && strncmp(auth, "Digest ", 7) == 0) {
		SG(request_info).auth_digest = estrdup(auth + 7);
		ret = 0;
	}

	if (ret == -1) {
		SG(request_info).auth_digest = NULL;
	}

	return ret;
}
/* }}} */

/* {{{ hyss_lint_script
 */
HYSSAPI int hyss_lint_script(gear_file_handle *file)
{
	gear_op_array *op_array;
	int retval = FAILURE;

	gear_try {
		op_array = gear_compile_file(file, GEAR_INCLUDE);
		gear_destroy_file_handle(file);

		if (op_array) {
			destroy_op_array(op_array);
			efree(op_array);
			retval = SUCCESS;
		}
	} gear_end_try();
	if (EG(exception)) {
		gear_exception_error(EG(exception), E_ERROR);
	}

	return retval;
}
/* }}} */

