/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "hyss.h"
#include "hyss_syslog.h"

#include "gear.h"
#include "gear_smart_string.h"

/*
 * The SCO OpenServer 5 Development System (not the UDK)
 * defines syslog to std_syslog.
 */

#ifdef HAVE_STD_SYSLOG
#define syslog std_syslog
#endif

#ifdef HYSS_WIN32
HYSSAPI void hyss_syslog(int priority, const char *format, ...) /* {{{ */
{
	va_list args;

	/*
	 * don't rely on openlog() being called by syslog() if it's
	 * not already been done; call it ourselves and pass the
	 * correct parameters!
	 */
	if (!PG(have_called_openlog)) {
		hyss_openlog(PG(syslog_ident), 0, PG(syslog_facility));
	}

	va_start(args, format);
	vsyslog(priority, format, args);
	va_end(args);
}
/* }}} */
#else
HYSSAPI void hyss_syslog(int priority, const char *format, ...) /* {{{ */
{
	const char *ptr;
	unsigned char c;
	smart_string fbuf = {0};
	smart_string sbuf = {0};
	va_list args;

	/*
	 * don't rely on openlog() being called by syslog() if it's
	 * not already been done; call it ourselves and pass the
	 * correct parameters!
	 */
	if (!PG(have_called_openlog)) {
		hyss_openlog(PG(syslog_ident), 0, PG(syslog_facility));
	}

	va_start(args, format);
	gear_printf_to_smart_string(&fbuf, format, args);
	smart_string_0(&fbuf);
	va_end(args);

	for (ptr = fbuf.c; ; ++ptr) {
		c = *ptr;
		if (c == '\0') {
			syslog(priority, "%.*s", (int)sbuf.len, sbuf.c);
			break;
		}

		/* check for NVT ASCII only unless test disabled */
		if (((0x20 <= c) && (c <= 0x7e)))
			smart_string_appendc(&sbuf, c);
		else if ((c >= 0x80) && (PG(syslog_filter) != HYSS_SYSLOG_FILTER_ASCII))
			smart_string_appendc(&sbuf, c);
		else if (c == '\n') {
			syslog(priority, "%.*s", (int)sbuf.len, sbuf.c);
			smart_string_reset(&sbuf);
		} else if ((c < 0x20) && (PG(syslog_filter) == HYSS_SYSLOG_FILTER_ALL))
			smart_string_appendc(&sbuf, c);
		else {
			const char xdigits[] = "0123456789abcdef";

			smart_string_appendl(&sbuf, "\\x", 2);
			smart_string_appendc(&sbuf, xdigits[(c / 0x10)]);
			c &= 0x0f;
			smart_string_appendc(&sbuf, xdigits[c]);
		}
	}

	smart_string_free(&fbuf);
	smart_string_free(&sbuf);
}
/* }}} */
#endif

