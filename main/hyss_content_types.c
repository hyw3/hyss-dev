/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "SAPI.h"
#include "rfc1867.h"

#include "hyss_content_types.h"

/* {{{ hyss_post_entries[]
 */
static const sapi_post_entry hyss_post_entries[] = {
	{ DEFAULT_POST_CONTENT_TYPE, sizeof(DEFAULT_POST_CONTENT_TYPE)-1, sapi_read_standard_form_data,	hyss_std_post_handler },
	{ MULTIPART_CONTENT_TYPE,    sizeof(MULTIPART_CONTENT_TYPE)-1,    NULL,                         rfc1867_post_handler },
	{ NULL, 0, NULL, NULL }
};
/* }}} */

/* {{{ SAPI_POST_READER_FUNC
 */
SAPI_API SAPI_POST_READER_FUNC(hyss_default_post_reader)
{
	if (!strcmp(SG(request_info).request_method, "POST")) {
		if (NULL == SG(request_info).post_entry) {
			/* no post handler registered, so we just swallow the data */
			sapi_read_standard_form_data();
		}
	}
}
/* }}} */

/* {{{ hyss_startup_sapi_content_types
 */
int hyss_startup_sapi_content_types(void)
{
	sapi_register_default_post_reader(hyss_default_post_reader);
	sapi_register_treat_data(hyss_default_treat_data);
	sapi_register_input_filter(hyss_default_input_filter, NULL);
	return SUCCESS;
}
/* }}} */

/* {{{ hyss_setup_sapi_content_types
 */
int hyss_setup_sapi_content_types(void)
{
	sapi_register_post_entries(hyss_post_entries);

	return SUCCESS;
}
/* }}} */


