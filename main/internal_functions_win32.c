/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* {{{ includes
 */
#include "hyss.h"
#include "hyss_main.h"
#include "gear_capis.h"
#include "gear_compile.h"
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

#include "extslib/standard/dl.h"
#include "extslib/standard/file.h"
#include "extslib/standard/fsock.h"
#include "extslib/standard/head.h"
#include "extslib/standard/pack.h"
#include "extslib/standard/hyss_browscap.h"
#include "extslib/standard/hyss_crypt.h"
#include "extslib/standard/hyss_dir.h"
#include "extslib/standard/hyss_filestat.h"
#include "extslib/standard/hyss_mail.h"
#include "extslib/standard/hyss_ext_syslog.h"
#include "extslib/standard/hyss_standard.h"
#include "extslib/standard/hyss_lcg.h"
#include "extslib/standard/hyss_array.h"
#include "extslib/standard/hyss_assert.h"
#include "extslib/reflection/hyss_reflection.h"
#if HAVE_BCMATH
#include "extslib/bcmath/hyss_bcmath.h"
#endif
#if HAVE_CALENDAR
#include "extslib/calendar/hyss_calendar.h"
#endif
#if HAVE_CTYPE
#include "extslib/ctype/hyss_ctype.h"
#endif
#if HAVE_DATE
#include "extslib/date/hyss_date.h"
#endif
#if HAVE_FTP
#include "extslib/ftp/hyss_ftp.h"
#endif
#if HAVE_ICONV
#include "extslib/iconv/hyss_iconv.h"
#endif
#include "extslib/standard/reg.h"
#if HAVE_PCRE || HAVE_BUNDLED_PCRE
#include "extslib/pcre/hyss_pcre.h"
#endif
#if HAVE_UODBC
#include "extslib/odbc/hyss_odbc.h"
#endif
#if HAVE_HYSS_SESSION
#include "extslib/session/hyss_session.h"
#endif
#if HAVE_MBSTRING
#include "extslib/mbstring/mbstring.h"
#endif
#if HAVE_TOKENIZER
#include "extslib/tokenizer/hyss_tokenizer.h"
#endif
#if HAVE_ZLIB
#include "extslib/zlib/hyss_zlib.h"
#endif
#if HAVE_LIBXML
#include "extslib/libxml/hyss_libxml.h"
#if HAVE_DOM
#include "extslib/dom/hyss_dom.h"
#endif
#if HAVE_SIMPLEXML
#include "extslib/simplexml/hyss_simplexml.h"
#endif
#endif
#if HAVE_XML
#include "extslib/xml/hyss_xml.h"
#endif
#if HAVE_XML && HAVE_WDDX
#include "extslib/wddx/hyss_wddx.h"
#endif
#include "extslib/com_dotnet/hyss_com_dotnet.h"
#ifdef HAVE_SPL
#include "extslib/spl/hyss_spl.h"
#endif
#if HAVE_XML && HAVE_XMLREADER
#include "extslib/xmlreader/hyss_xmlreader.h"
#endif
#if HAVE_XML && HAVE_XMLWRITER
#include "extslib/xmlwriter/hyss_xmlwriter.h"
#endif
/* }}} */

/* {{{ hyss_builtin_extensions[]
 */
static gear_capi_entry * const hyss_builtin_extensions[] = {
	hyssext_standard_ptr
#if HAVE_BCMATH
	,hyssext_bcmath_ptr
#endif
#if HAVE_CALENDAR
	,hyssext_calendar_ptr
#endif
	,hyssext_com_dotnet_ptr
#if HAVE_CTYPE
	,hyssext_ctype_ptr
#endif
#if HAVE_DATE
	,hyssext_date_ptr
#endif
#if HAVE_FTP
	,hyssext_ftp_ptr
#endif
#if HAVE_HASH
	,hyssext_hash_ptr
#endif
#if HAVE_ICONV
	,hyssext_iconv_ptr
#endif
#if HAVE_MBSTRING
	,hyssext_mbstring_ptr
#endif
#if HAVE_UODBC
	,hyssext_odbc_ptr
#endif
#if HAVE_PCRE || HAVE_BUNDLED_PCRE
	,hyssext_pcre_ptr
#endif
	,hyssext_reflection_ptr
#if HAVE_HYSS_SESSION
	,hyssext_session_ptr
#endif
#if HAVE_TOKENIZER
	,hyssext_tokenizer_ptr
#endif
#if HAVE_ZLIB
	,hyssext_zlib_ptr
#endif
#if HAVE_LIBXML
	,hyssext_libxml_ptr
#if HAVE_DOM
	,hyssext_dom_ptr
#endif
#if HAVE_SIMPLEXML
	,hyssext_simplexml_ptr
#endif
#endif
#if HAVE_XML
	,hyssext_xml_ptr
#endif
#if HAVE_XML && HAVE_WDDX
	,hyssext_wddx_ptr
#endif
#if HAVE_SPL
	,hyssext_spl_ptr
#endif
#if HAVE_XML && HAVE_XMLREADER
	,hyssext_xmlreader_ptr
#endif
#if HAVE_XML && HAVE_XMLWRITER
	,hyssext_xmlwriter_ptr
#endif
};
/* }}} */

#define EXTCOUNT (sizeof(hyss_builtin_extensions)/sizeof(gear_capi_entry *))

HYSSAPI int hyss_register_internal_extensions(void)
{
	return hyss_register_extensions(hyss_builtin_extensions, EXTCOUNT);
}

