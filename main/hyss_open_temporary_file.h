/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_OPEN_TEMPORARY_FILE_H
#define HYSS_OPEN_TEMPORARY_FILE_H

#define HYSS_TMP_FILE_OPEN_BASEDIR_CHECK (1<<0)
#define HYSS_TMP_FILE_SILENT (1<<1)

BEGIN_EXTERN_C()
HYSSAPI FILE *hyss_open_temporary_file(const char *dir, const char *pfx, gear_string **opened_path_p);
HYSSAPI int hyss_open_temporary_fd_ex(const char *dir, const char *pfx, gear_string **opened_path_p, uint32_t flags);
HYSSAPI int hyss_open_temporary_fd(const char *dir, const char *pfx, gear_string **opened_path_p);
HYSSAPI const char *hyss_get_temporary_directory(void);
END_EXTERN_C()

#endif /* HYSS_OPEN_TEMPORARY_FILE_H */

