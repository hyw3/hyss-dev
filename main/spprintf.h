/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPPRINTF_H
#define SPPRINTF_H

#include "snprintf.h"
#include "gear_smart_str_public.h"
#include "gear_smart_string_public.h"

BEGIN_EXTERN_C()
HYSSAPI void hyss_printf_to_smart_string(smart_string *buf, const char *format, va_list ap);
HYSSAPI void hyss_printf_to_smart_str(smart_str *buf, const char *format, va_list ap);
END_EXTERN_C()

#define spprintf gear_spprintf
#define strpprintf gear_strpprintf
#define vspprintf gear_vspprintf
#define vstrpprintf gear_vstrpprintf

#endif /* SNPRINTF_H */

