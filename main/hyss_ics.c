/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "extslib/standard/info.h"
#include "gear_ics.h"
#include "gear_ics_scanner.h"
#include "hyss_ics.h"
#include "extslib/standard/dl.h"
#include "gear_extensions.h"
#include "gear_highlight.h"
#include "SAPI.h"
#include "hyss_main.h"
#include "hyss_scandir.h"
#ifdef HYSS_WIN32
#include "win32/hyss_registry.h"
#endif

#if HAVE_SCANDIR && HAVE_ALPHASORT && HAVE_DIRENT_H
#include <dirent.h>
#endif

#ifdef HYSS_WIN32
#define TRANSLATE_SLASHES_LOWER(path) \
	{ \
		char *tmp = path; \
		while (*tmp) { \
			if (*tmp == '\\') *tmp = '/'; \
			else *tmp = tolower(*tmp); \
				tmp++; \
		} \
	}
#else
#define TRANSLATE_SLASHES_LOWER(path)
#endif


typedef struct _hyss_extension_lists {
	gear_llist engine;
	gear_llist functions;
} hyss_extension_lists;

/* True globals */
static int is_special_section = 0;
static HashTable *active_ics_hash;
static HashTable configuration_hash;
static int has_per_dir_config = 0;
static int has_per_host_config = 0;
HYSSAPI char *hyss_ics_opened_path=NULL;
static hyss_extension_lists extension_lists;
HYSSAPI char *hyss_ics_scanned_path=NULL;
HYSSAPI char *hyss_ics_scanned_files=NULL;

/* {{{ hyss_ics_displayer_cb
 */
static void hyss_ics_displayer_cb(gear_ics_entry *ics_entry, int type)
{
	if (ics_entry->displayer) {
		ics_entry->displayer(ics_entry, type);
	} else {
		char *display_string;
		size_t display_string_length;
		int esc_html=0;

		if (type == GEAR_ICS_DISPLAY_ORIG && ics_entry->modified) {
			if (ics_entry->orig_value && ZSTR_VAL(ics_entry->orig_value)[0]) {
				display_string = ZSTR_VAL(ics_entry->orig_value);
				display_string_length = ZSTR_LEN(ics_entry->orig_value);
				esc_html = !sapi_capi.hyssinfo_as_text;
			} else {
				if (!sapi_capi.hyssinfo_as_text) {
					display_string = "<i>no value</i>";
					display_string_length = sizeof("<i>no value</i>") - 1;
				} else {
					display_string = "no value";
					display_string_length = sizeof("no value") - 1;
				}
			}
		} else if (ics_entry->value && ZSTR_VAL(ics_entry->value)[0]) {
			display_string = ZSTR_VAL(ics_entry->value);
			display_string_length = ZSTR_LEN(ics_entry->value);
			esc_html = !sapi_capi.hyssinfo_as_text;
		} else {
			if (!sapi_capi.hyssinfo_as_text) {
				display_string = "<i>no value</i>";
				display_string_length = sizeof("<i>no value</i>") - 1;
			} else {
				display_string = "no value";
				display_string_length = sizeof("no value") - 1;
			}
		}

		if (esc_html) {
			hyss_html_puts(display_string, display_string_length);
		} else {
			HYSSWRITE(display_string, display_string_length);
		}
	}
}
/* }}} */

/* {{{ hyss_ics_displayer
 */
static int hyss_ics_displayer(zval *el, void *arg)
{
	gear_ics_entry *ics_entry = (gear_ics_entry*)Z_PTR_P(el);
	int capi_number = *(int *)arg;

	if (ics_entry->capi_number != capi_number) {
		return 0;
	}
	if (!sapi_capi.hyssinfo_as_text) {
		PUTS("<tr>");
		PUTS("<td class=\"e\">");
		HYSSWRITE(ZSTR_VAL(ics_entry->name), ZSTR_LEN(ics_entry->name));
		PUTS("</td><td class=\"v\">");
		hyss_ics_displayer_cb(ics_entry, GEAR_ICS_DISPLAY_ACTIVE);
		PUTS("</td><td class=\"v\">");
		hyss_ics_displayer_cb(ics_entry, GEAR_ICS_DISPLAY_ORIG);
		PUTS("</td></tr>\n");
	} else {
		HYSSWRITE(ZSTR_VAL(ics_entry->name), ZSTR_LEN(ics_entry->name));
		PUTS(" => ");
		hyss_ics_displayer_cb(ics_entry, GEAR_ICS_DISPLAY_ACTIVE);
		PUTS(" => ");
		hyss_ics_displayer_cb(ics_entry, GEAR_ICS_DISPLAY_ORIG);
		PUTS("\n");
	}
	return 0;
}
/* }}} */

/* {{{ hyss_ics_available
 */
static int hyss_ics_available(zval *el, void *arg)
{
	gear_ics_entry *ics_entry = (gear_ics_entry *)Z_PTR_P(el);
	int *capi_number_available = (int *)arg;
	if (ics_entry->capi_number == *(int *)capi_number_available) {
		*(int *)capi_number_available = -1;
		return GEAR_HASH_APPLY_STOP;
	} else {
		return GEAR_HASH_APPLY_KEEP;
	}
}
/* }}} */

/* {{{ display_ics_entries
 */
HYSSAPI void display_ics_entries(gear_capi_entry *cAPI)
{
	int capi_number, capi_number_available;

	if (cAPI) {
		capi_number = cAPI->capi_number;
	} else {
		capi_number = 0;
	}
	capi_number_available = capi_number;
	gear_hash_apply_with_argument(EG(ics_directives), hyss_ics_available, &capi_number_available);
	if (capi_number_available == -1) {
		hyss_info_print_table_start();
		hyss_info_print_table_header(3, "Directive", "Local Value", "Master Value");
		gear_hash_apply_with_argument(EG(ics_directives), hyss_ics_displayer, (void *)&capi_number);
		hyss_info_print_table_end();
	}
}
/* }}} */

/* hyss.ics support */
#define HYSS_EXTENSION_TOKEN		"extension"
#define GEAR_EXTENSION_TOKEN	"gear_extension"

/* {{{ config_zval_dtor
 */
HYSSAPI void config_zval_dtor(zval *zvalue)
{
	if (Z_TYPE_P(zvalue) == IS_ARRAY) {
		gear_hash_destroy(Z_ARRVAL_P(zvalue));
		free(Z_ARR_P(zvalue));
	} else if (Z_TYPE_P(zvalue) == IS_STRING) {
		gear_string_release_ex(Z_STR_P(zvalue), 1);
	}
}
/* Reset / free active_ics_sectin global */
#define RESET_ACTIVE_ICS_HASH() do { \
	active_ics_hash = NULL;          \
	is_special_section = 0;          \
} while (0)
/* }}} */

/* {{{ hyss_ics_parser_cb
 */
static void hyss_ics_parser_cb(zval *arg1, zval *arg2, zval *arg3, int callback_type, HashTable *target_hash)
{
	zval *entry;
	HashTable *active_hash;
	char *extension_name;

	if (active_ics_hash) {
		active_hash = active_ics_hash;
	} else {
		active_hash = target_hash;
	}

	switch (callback_type) {
		case GEAR_ICS_PARSER_ENTRY: {
				if (!arg2) {
					/* bare string - nothing to do */
					break;
				}

				/* HYSS and Gear extensions are not added into configuration hash! */
				if (!is_special_section && !strcasecmp(Z_STRVAL_P(arg1), HYSS_EXTENSION_TOKEN)) { /* load HYSS extension */
					extension_name = estrndup(Z_STRVAL_P(arg2), Z_STRLEN_P(arg2));
					gear_llist_add_element(&extension_lists.functions, &extension_name);
				} else if (!is_special_section && !strcasecmp(Z_STRVAL_P(arg1), GEAR_EXTENSION_TOKEN)) { /* load Gear extension */
					extension_name = estrndup(Z_STRVAL_P(arg2), Z_STRLEN_P(arg2));
					gear_llist_add_element(&extension_lists.engine, &extension_name);

				/* All other entries are added into either configuration_hash or active ics section array */
				} else {
					/* Store in active hash */
					entry = gear_hash_update(active_hash, Z_STR_P(arg1), arg2);
					Z_STR_P(entry) = gear_string_dup(Z_STR_P(entry), 1);
				}
			}
			break;

		case GEAR_ICS_PARSER_POP_ENTRY: {
				zval option_arr;
				zval *find_arr;

				if (!arg2) {
					/* bare string - nothing to do */
					break;
				}

/* fprintf(stdout, "GEAR_ICS_PARSER_POP_ENTRY: %s[%s] = %s\n",Z_STRVAL_P(arg1), Z_STRVAL_P(arg3), Z_STRVAL_P(arg2)); */

				/* If option not found in hash or is not an array -> create array, otherwise add to existing array */
				if ((find_arr = gear_hash_find(active_hash, Z_STR_P(arg1))) == NULL || Z_TYPE_P(find_arr) != IS_ARRAY) {
					ZVAL_NEW_PERSISTENT_ARR(&option_arr);
					gear_hash_init(Z_ARRVAL(option_arr), 8, NULL, config_zval_dtor, 1);
					find_arr = gear_hash_update(active_hash, Z_STR_P(arg1), &option_arr);
				}

				/* arg3 is possible option offset name */
				if (arg3 && Z_STRLEN_P(arg3) > 0) {
					entry = gear_symtable_update(Z_ARRVAL_P(find_arr), Z_STR_P(arg3), arg2);
				} else {
					entry = gear_hash_next_index_insert(Z_ARRVAL_P(find_arr), arg2);
				}
				Z_STR_P(entry) = gear_string_dup(Z_STR_P(entry), 1);
			}
			break;

		case GEAR_ICS_PARSER_SECTION: { /* Create an array of entries of each section */

/* fprintf(stdout, "GEAR_ICS_PARSER_SECTION: %s\n",Z_STRVAL_P(arg1)); */

				char *key = NULL;
				size_t key_len;

				/* PATH sections */
				if (!gear_binary_strncasecmp(Z_STRVAL_P(arg1), Z_STRLEN_P(arg1), "PATH", sizeof("PATH") - 1, sizeof("PATH") - 1)) {
					key = Z_STRVAL_P(arg1);
					key = key + sizeof("PATH") - 1;
					key_len = Z_STRLEN_P(arg1) - sizeof("PATH") + 1;
					is_special_section = 1;
					has_per_dir_config = 1;

					/* make the path lowercase on Windows, for case insensitivity. Does nothing for other platforms */
					TRANSLATE_SLASHES_LOWER(key);

				/* HOST sections */
				} else if (!gear_binary_strncasecmp(Z_STRVAL_P(arg1), Z_STRLEN_P(arg1), "HOST", sizeof("HOST") - 1, sizeof("HOST") - 1)) {
					key = Z_STRVAL_P(arg1);
					key = key + sizeof("HOST") - 1;
					key_len = Z_STRLEN_P(arg1) - sizeof("HOST") + 1;
					is_special_section = 1;
					has_per_host_config = 1;
					gear_str_tolower(key, key_len); /* host names are case-insensitive. */

				} else {
					is_special_section = 0;
				}

				if (key && key_len > 0) {
					/* Strip any trailing slashes */
					while (key_len > 0 && (key[key_len - 1] == '/' || key[key_len - 1] == '\\')) {
						key_len--;
						key[key_len] = 0;
					}

					/* Strip any leading whitespace and '=' */
					while (*key && (
						*key == '=' ||
						*key == ' ' ||
						*key == '\t'
					)) {
						key++;
						key_len--;
					}

					/* Search for existing entry and if it does not exist create one */
					if ((entry = gear_hash_str_find(target_hash, key, key_len)) == NULL) {
						zval section_arr;

						ZVAL_NEW_PERSISTENT_ARR(&section_arr);
						gear_hash_init(Z_ARRVAL(section_arr), 8, NULL, (dtor_func_t) config_zval_dtor, 1);
						entry = gear_hash_str_update(target_hash, key, key_len, &section_arr);
					}
					if (Z_TYPE_P(entry) == IS_ARRAY) {
						active_ics_hash = Z_ARRVAL_P(entry);
					}
				}
			}
			break;
	}
}
/* }}} */

/* {{{ hyss_load_hyss_extension_cb
 */
static void hyss_load_hyss_extension_cb(void *arg)
{
#ifdef HAVE_LIBDL
	hyss_load_extension(*((char **) arg), CAPI_PERSISTENT, 0);
#endif
}
/* }}} */

/* {{{ hyss_load_gear_extension_cb
 */
#ifdef HAVE_LIBDL
static void hyss_load_gear_extension_cb(void *arg)
{
	char *filename = *((char **) arg);
	const size_t length = strlen(filename);

#ifndef HYSS_WIN32
	(void) length;
#endif

	if (IS_ABSOLUTE_PATH(filename, length)) {
		gear_load_extension(filename);
	} else {
		DL_HANDLE handle;
		char *libpath;
		char *extension_dir = ICS_STR("extension_dir");
		int slash_suffix = 0;
		char *err1, *err2;

		if (extension_dir && extension_dir[0]) {
			slash_suffix = IS_SLASH(extension_dir[strlen(extension_dir)-1]);
		}

		/* Try as filename first */
		if (slash_suffix) {
			spprintf(&libpath, 0, "%s%s", extension_dir, filename); /* SAFE */
		} else {
			spprintf(&libpath, 0, "%s%c%s", extension_dir, DEFAULT_SLASH, filename); /* SAFE */
		}

		handle = (DL_HANDLE)hyss_load_shlib(libpath, &err1);
		if (!handle) {
			/* If file does not exist, consider as extension name and build file name */
			char *orig_libpath = libpath;

			if (slash_suffix) {
				spprintf(&libpath, 0, "%s" HYSS_SHLIB_EXT_PREFIX "%s." HYSS_SHLIB_SUFFIX, extension_dir, filename); /* SAFE */
			} else {
				spprintf(&libpath, 0, "%s%c" HYSS_SHLIB_EXT_PREFIX "%s." HYSS_SHLIB_SUFFIX, extension_dir, DEFAULT_SLASH, filename); /* SAFE */
			}

			handle = (DL_HANDLE)hyss_load_shlib(libpath, &err2);
			if (!handle) {
				hyss_error(E_CORE_WARNING, "Failed loading Gear extension '%s' (tried: %s (%s), %s (%s))",
					filename, orig_libpath, err1, libpath, err2);
				efree(orig_libpath);
				efree(err1);
				efree(libpath);
				efree(err2);
				return;
			}

			efree(orig_libpath);
			efree(err1);
		}

		gear_load_extension_handle(handle, libpath);
		efree(libpath);
	}
}
#else
static void hyss_load_gear_extension_cb(void *arg) { }
#endif
/* }}} */

/* {{{ hyss_init_config
 */
int hyss_init_config(void)
{
	char *hyss_ics_file_name = NULL;
	char *hyss_ics_search_path = NULL;
	int hyss_ics_scanned_path_len;
	char *open_basedir;
	int free_ics_search_path = 0;
	gear_file_handle fh;
	gear_string *opened_path = NULL;

	gear_hash_init(&configuration_hash, 8, NULL, config_zval_dtor, 1);

	if (sapi_capi.ics_defaults) {
		sapi_capi.ics_defaults(&configuration_hash);
	}

	gear_llist_init(&extension_lists.engine, sizeof(char *), (llist_dtor_func_t) free_estring, 1);
	gear_llist_init(&extension_lists.functions, sizeof(char *), (llist_dtor_func_t) free_estring, 1);

	open_basedir = PG(open_basedir);

	if (sapi_capi.hyss_ics_path_override) {
		hyss_ics_file_name = sapi_capi.hyss_ics_path_override;
		hyss_ics_search_path = sapi_capi.hyss_ics_path_override;
		free_ics_search_path = 0;
	} else if (!sapi_capi.hyss_ics_ignore) {
		int search_path_size;
		char *default_location;
		char *env_location;
		static const char paths_separator[] = { GEAR_PATHS_SEPARATOR, 0 };
#ifdef HYSS_WIN32
		char *reg_location;
		char hyssrc_path[MAXPATHLEN];
#endif

		env_location = getenv("HYSSRC");

#ifdef HYSS_WIN32
		if (!env_location) {
			char dummybuf;
			int size;

			SetLastError(0);

			/*If the given buffer is not large enough to hold the data, the return value is
			the buffer size,  in characters, required to hold the string and its terminating
			null character. We use this return value to alloc the final buffer. */
			size = GetEnvironmentVariableA("HYSSRC", &dummybuf, 0);
			if (GetLastError() == ERROR_ENVVAR_NOT_FOUND) {
				/* The environment variable doesn't exist. */
				env_location = "";
			} else {
				if (size == 0) {
					env_location = "";
				} else {
					size = GetEnvironmentVariableA("HYSSRC", hyssrc_path, size);
					if (size == 0) {
						env_location = "";
					} else {
						env_location = hyssrc_path;
					}
				}
			}
		}
#else
		if (!env_location) {
			env_location = "";
		}
#endif
		/*
		 * Prepare search path
		 */

		search_path_size = MAXPATHLEN * 4 + (int)strlen(env_location) + 3 + 1;
		hyss_ics_search_path = (char *) emalloc(search_path_size);
		free_ics_search_path = 1;
		hyss_ics_search_path[0] = 0;

		/* Add environment location */
		if (env_location[0]) {
			if (*hyss_ics_search_path) {
				strlcat(hyss_ics_search_path, paths_separator, search_path_size);
			}
			strlcat(hyss_ics_search_path, env_location, search_path_size);
			hyss_ics_file_name = env_location;
		}

#ifdef HYSS_WIN32
		/* Add registry location */
		reg_location = GetIniPathFromRegistry();
		if (reg_location != NULL) {
			if (*hyss_ics_search_path) {
				strlcat(hyss_ics_search_path, paths_separator, search_path_size);
			}
			strlcat(hyss_ics_search_path, reg_location, search_path_size);
			efree(reg_location);
		}
#endif

		/* Add cwd (not with CLI) */
		if (!sapi_capi.hyss_ics_ignore_cwd) {
			if (*hyss_ics_search_path) {
				strlcat(hyss_ics_search_path, paths_separator, search_path_size);
			}
			strlcat(hyss_ics_search_path, ".", search_path_size);
		}

		if (PG(hyss_binary)) {
			char *separator_location, *binary_location;

			binary_location = estrdup(PG(hyss_binary));
			separator_location = strrchr(binary_location, DEFAULT_SLASH);

			if (separator_location && separator_location != binary_location) {
				*(separator_location) = 0;
			}
			if (*hyss_ics_search_path) {
				strlcat(hyss_ics_search_path, paths_separator, search_path_size);
			}
			strlcat(hyss_ics_search_path, binary_location, search_path_size);
			efree(binary_location);
		}

		/* Add default location */
#ifdef HYSS_WIN32
		default_location = (char *) emalloc(MAXPATHLEN + 1);

		if (0 < GetWindowsDirectory(default_location, MAXPATHLEN)) {
			if (*hyss_ics_search_path) {
				strlcat(hyss_ics_search_path, paths_separator, search_path_size);
			}
			strlcat(hyss_ics_search_path, default_location, search_path_size);
		}

		/* For people running under terminal services, GetWindowsDirectory will
		 * return their personal Windows directory, so lets add the system
		 * windows directory too */
		if (0 < GetSystemWindowsDirectory(default_location, MAXPATHLEN)) {
			if (*hyss_ics_search_path) {
				strlcat(hyss_ics_search_path, paths_separator, search_path_size);
			}
			strlcat(hyss_ics_search_path, default_location, search_path_size);
		}
		efree(default_location);

#else
		default_location = HYSS_CONFIG_FILE_PATH;
		if (*hyss_ics_search_path) {
			strlcat(hyss_ics_search_path, paths_separator, search_path_size);
		}
		strlcat(hyss_ics_search_path, default_location, search_path_size);
#endif
	}

	PG(open_basedir) = NULL;

	/*
	 * Find and open actual ics file
	 */

	memset(&fh, 0, sizeof(fh));

	/* If SAPI does not want to ignore all ics files OR an overriding file/path is given.
	 * This allows disabling scanning for ics files in the HYSS_CONFIG_FILE_SCAN_DIR but still
	 * load an optional ics file. */
	if (!sapi_capi.hyss_ics_ignore || sapi_capi.hyss_ics_path_override) {

		/* Check if hyss_ics_file_name is a file and can be opened */
		if (hyss_ics_file_name && hyss_ics_file_name[0]) {
			gear_stat_t statbuf;

			if (!VCWD_STAT(hyss_ics_file_name, &statbuf)) {
				if (!((statbuf.st_mode & S_IFMT) == S_IFDIR)) {
					fh.handle.fp = VCWD_FOPEN(hyss_ics_file_name, "r");
					if (fh.handle.fp) {
						fh.filename = expand_filepath(hyss_ics_file_name, NULL);
					}
				}
			}
		}

		/* Otherwise search for hyss-%sapi-cAPI-name%.ics file in search path */
		if (!fh.handle.fp) {
			const char *fmt = "hyss-%s.ics";
			char *ics_fname;
			spprintf(&ics_fname, 0, fmt, sapi_capi.name);
			fh.handle.fp = hyss_fopen_with_path(ics_fname, "r", hyss_ics_search_path, &opened_path);
			efree(ics_fname);
			if (fh.handle.fp) {
				fh.filename = ZSTR_VAL(opened_path);
			}
		}

		/* If still no ics file found, search for hyss.ics file in search path */
		if (!fh.handle.fp) {
			fh.handle.fp = hyss_fopen_with_path("hyss.ics", "r", hyss_ics_search_path, &opened_path);
			if (fh.handle.fp) {
				fh.filename = ZSTR_VAL(opened_path);
			}
		}
	}

	if (free_ics_search_path) {
		efree(hyss_ics_search_path);
	}

	PG(open_basedir) = open_basedir;

	if (fh.handle.fp) {
		fh.type = GEAR_HANDLE_FP;
		RESET_ACTIVE_ICS_HASH();

		gear_parse_ics_file(&fh, 1, GEAR_ICS_SCANNER_NORMAL, (gear_ics_parser_cb_t) hyss_ics_parser_cb, &configuration_hash);

		{
			zval tmp;

			ZVAL_NEW_STR(&tmp, gear_string_init(fh.filename, strlen(fh.filename), 1));
			gear_hash_str_update(&configuration_hash, "cfg_file_path", sizeof("cfg_file_path")-1, &tmp);
			if (opened_path) {
				gear_string_release_ex(opened_path, 0);
			} else {
				efree((char *)fh.filename);
			}
			hyss_ics_opened_path = gear_strndup(Z_STRVAL(tmp), Z_STRLEN(tmp));
		}
	}

	/* Check for HYSS_ICS_SCAN_DIR environment variable to override/set config file scan directory */
	hyss_ics_scanned_path = getenv("HYSS_ICS_SCAN_DIR");
	if (!hyss_ics_scanned_path) {
		/* Or fall back using possible --with-config-file-scan-dir setting (defaults to empty string!) */
		hyss_ics_scanned_path = HYSS_CONFIG_FILE_SCAN_DIR;
	}
	hyss_ics_scanned_path_len = (int)strlen(hyss_ics_scanned_path);

	/* Scan and parse any .ics files found in scan path if path not empty. */
	if (!sapi_capi.hyss_ics_ignore && hyss_ics_scanned_path_len) {
		struct dirent **namelist;
		int ndir, i;
		gear_stat_t sb;
		char ics_file[MAXPATHLEN];
		char *p;
		gear_file_handle fh2;
		gear_llist scanned_ics_list;
		gear_llist_element *element;
		int l, total_l = 0;
		char *bufpath, *debpath, *endpath;
		int lenpath;

		gear_llist_init(&scanned_ics_list, sizeof(char *), (llist_dtor_func_t) free_estring, 1);
		memset(&fh2, 0, sizeof(fh2));

		bufpath = estrdup(hyss_ics_scanned_path);
		for (debpath = bufpath ; debpath ; debpath=endpath) {
			endpath = strchr(debpath, DEFAULT_DIR_SEPARATOR);
			if (endpath) {
				*(endpath++) = 0;
			}
			if (!debpath[0]) {
				/* empty string means default builtin value
				   to allow "/foo/hyss.d:" or ":/foo/hyss.d" */
				debpath = HYSS_CONFIG_FILE_SCAN_DIR;
			}
			lenpath = (int)strlen(debpath);

			if (lenpath > 0 && (ndir = hyss_scandir(debpath, &namelist, 0, hyss_alphasort)) > 0) {

				for (i = 0; i < ndir; i++) {

					/* check for any file with .ics extension */
					if (!(p = strrchr(namelist[i]->d_name, '.')) || (p && strcmp(p, ".ics"))) {
						free(namelist[i]);
						continue;
					}
					/* Reset active ics section */
					RESET_ACTIVE_ICS_HASH();

					if (IS_SLASH(debpath[lenpath - 1])) {
						snprintf(ics_file, MAXPATHLEN, "%s%s", debpath, namelist[i]->d_name);
					} else {
						snprintf(ics_file, MAXPATHLEN, "%s%c%s", debpath, DEFAULT_SLASH, namelist[i]->d_name);
					}
					if (VCWD_STAT(ics_file, &sb) == 0) {
						if (S_ISREG(sb.st_mode)) {
							if ((fh2.handle.fp = VCWD_FOPEN(ics_file, "r"))) {
								fh2.filename = ics_file;
								fh2.type = GEAR_HANDLE_FP;

								if (gear_parse_ics_file(&fh2, 1, GEAR_ICS_SCANNER_NORMAL, (gear_ics_parser_cb_t) hyss_ics_parser_cb, &configuration_hash) == SUCCESS) {
									/* Here, add it to the list of ics files read */
									l = (int)strlen(ics_file);
									total_l += l + 2;
									p = estrndup(ics_file, l);
									gear_llist_add_element(&scanned_ics_list, &p);
								}
							}
						}
					}
					free(namelist[i]);
				}
				free(namelist);
			}
		}
		efree(bufpath);

		if (total_l) {
			int hyss_ics_scanned_files_len = (hyss_ics_scanned_files) ? (int)strlen(hyss_ics_scanned_files) + 1 : 0;
			hyss_ics_scanned_files = (char *) realloc(hyss_ics_scanned_files, hyss_ics_scanned_files_len + total_l + 1);
			if (!hyss_ics_scanned_files_len) {
				*hyss_ics_scanned_files = '\0';
			}
			total_l += hyss_ics_scanned_files_len;
			for (element = scanned_ics_list.head; element; element = element->next) {
				if (hyss_ics_scanned_files_len) {
					strlcat(hyss_ics_scanned_files, ",\n", total_l);
				}
				strlcat(hyss_ics_scanned_files, *(char **)element->data, total_l);
				strlcat(hyss_ics_scanned_files, element->next ? ",\n" : "\n", total_l);
			}
		}
		gear_llist_destroy(&scanned_ics_list);
	} else {
		/* Make sure an empty hyss_ics_scanned_path ends up as NULL */
		hyss_ics_scanned_path = NULL;
	}

	if (sapi_capi.ics_entries) {
		/* Reset active ics section */
		RESET_ACTIVE_ICS_HASH();
		gear_parse_ics_string(sapi_capi.ics_entries, 1, GEAR_ICS_SCANNER_NORMAL, (gear_ics_parser_cb_t) hyss_ics_parser_cb, &configuration_hash);
	}

	return SUCCESS;
}
/* }}} */

/* {{{ hyss_shutdown_config
 */
int hyss_shutdown_config(void)
{
	gear_hash_destroy(&configuration_hash);
	if (hyss_ics_opened_path) {
		free(hyss_ics_opened_path);
		hyss_ics_opened_path = NULL;
	}
	if (hyss_ics_scanned_files) {
		free(hyss_ics_scanned_files);
		hyss_ics_scanned_files = NULL;
	}
	return SUCCESS;
}
/* }}} */

/* {{{ hyss_ics_register_extensions
 */
void hyss_ics_register_extensions(void)
{
	gear_llist_apply(&extension_lists.engine, hyss_load_gear_extension_cb);
	gear_llist_apply(&extension_lists.functions, hyss_load_hyss_extension_cb);

	gear_llist_destroy(&extension_lists.engine);
	gear_llist_destroy(&extension_lists.functions);
}
/* }}} */

/* {{{ hyss_parse_user_ics_file
 */
HYSSAPI int hyss_parse_user_ics_file(const char *dirname, char *ics_filename, HashTable *target_hash)
{
	gear_stat_t sb;
	char ics_file[MAXPATHLEN];
	gear_file_handle fh;

	snprintf(ics_file, MAXPATHLEN, "%s%c%s", dirname, DEFAULT_SLASH, ics_filename);

	if (VCWD_STAT(ics_file, &sb) == 0) {
		if (S_ISREG(sb.st_mode)) {
			memset(&fh, 0, sizeof(fh));
			if ((fh.handle.fp = VCWD_FOPEN(ics_file, "r"))) {
				fh.filename = ics_file;
				fh.type = GEAR_HANDLE_FP;

				/* Reset active ics section */
				RESET_ACTIVE_ICS_HASH();

				if (gear_parse_ics_file(&fh, 1, GEAR_ICS_SCANNER_NORMAL, (gear_ics_parser_cb_t) hyss_ics_parser_cb, target_hash) == SUCCESS) {
					/* FIXME: Add parsed file to the list of user files read? */
					return SUCCESS;
				}
				return FAILURE;
			}
		}
	}
	return FAILURE;
}
/* }}} */

/* {{{ hyss_ics_activate_config
 */
HYSSAPI void hyss_ics_activate_config(HashTable *source_hash, int modify_type, int stage)
{
	gear_string *str;
	zval *data;

	/* Walk through config hash and alter matching ics entries using the values found in the hash */
	GEAR_HASH_FOREACH_STR_KEY_VAL(source_hash, str, data) {
		gear_alter_ics_entry_ex(str, Z_STR_P(data), modify_type, stage, 0);
	} GEAR_HASH_FOREACH_END();
}
/* }}} */

/* {{{ hyss_ics_has_per_dir_config
 */
HYSSAPI int hyss_ics_has_per_dir_config(void)
{
	return has_per_dir_config;
}
/* }}} */

/* {{{ hyss_ics_activate_per_dir_config
 */
HYSSAPI void hyss_ics_activate_per_dir_config(char *path, size_t path_len)
{
	zval *tmp2;
	char *ptr;

#ifdef HYSS_WIN32
	char path_bak[MAXPATHLEN];
#endif

#ifdef HYSS_WIN32
	/* MAX_PATH is \0-terminated, path_len == MAXPATHLEN would overrun path_bak */
	if (path_len >= MAXPATHLEN) {
#else
	if (path_len > MAXPATHLEN) {
#endif
		return;
	}

#ifdef HYSS_WIN32
	memcpy(path_bak, path, path_len);
	path_bak[path_len] = 0;
	TRANSLATE_SLASHES_LOWER(path_bak);
	path = path_bak;
#endif

	/* Walk through each directory in path and apply any found per-dir-system-configuration from configuration_hash */
	if (has_per_dir_config && path && path_len) {
		ptr = path + 1;
		while ((ptr = strchr(ptr, '/')) != NULL) {
			*ptr = 0;
			/* Search for source array matching the path from configuration_hash */
			if ((tmp2 = gear_hash_str_find(&configuration_hash, path, strlen(path))) != NULL) {
				hyss_ics_activate_config(Z_ARRVAL_P(tmp2), HYSS_ICS_SYSTEM, HYSS_ICS_STAGE_ACTIVATE);
			}
			*ptr = '/';
			ptr++;
		}
	}
}
/* }}} */

/* {{{ hyss_ics_has_per_host_config
 */
HYSSAPI int hyss_ics_has_per_host_config(void)
{
	return has_per_host_config;
}
/* }}} */

/* {{{ hyss_ics_activate_per_host_config
 */
HYSSAPI void hyss_ics_activate_per_host_config(const char *host, size_t host_len)
{
	zval *tmp;

	if (has_per_host_config && host && host_len) {
		/* Search for source array matching the host from configuration_hash */
		if ((tmp = gear_hash_str_find(&configuration_hash, host, host_len)) != NULL) {
			hyss_ics_activate_config(Z_ARRVAL_P(tmp), HYSS_ICS_SYSTEM, HYSS_ICS_STAGE_ACTIVATE);
		}
	}
}
/* }}} */

/* {{{ cfg_get_entry
 */
HYSSAPI zval *cfg_get_entry_ex(gear_string *name)
{
	return gear_hash_find(&configuration_hash, name);
}
/* }}} */

/* {{{ cfg_get_entry
 */
HYSSAPI zval *cfg_get_entry(const char *name, size_t name_length)
{
	return gear_hash_str_find(&configuration_hash, name, name_length);
}
/* }}} */

/* {{{ cfg_get_long
 */
HYSSAPI int cfg_get_long(const char *varname, gear_long *result)
{
	zval *tmp;

	if ((tmp = gear_hash_str_find(&configuration_hash, varname, strlen(varname))) == NULL) {
		*result = 0;
		return FAILURE;
	}
	*result = zval_get_long(tmp);
	return SUCCESS;
}
/* }}} */

/* {{{ cfg_get_double
 */
HYSSAPI int cfg_get_double(const char *varname, double *result)
{
	zval *tmp;

	if ((tmp = gear_hash_str_find(&configuration_hash, varname, strlen(varname))) == NULL) {
		*result = (double) 0;
		return FAILURE;
	}
	*result = zval_get_double(tmp);
	return SUCCESS;
}
/* }}} */

/* {{{ cfg_get_string
 */
HYSSAPI int cfg_get_string(const char *varname, char **result)
{
	zval *tmp;

	if ((tmp = gear_hash_str_find(&configuration_hash, varname, strlen(varname))) == NULL) {
		*result = NULL;
		return FAILURE;
	}
	*result = Z_STRVAL_P(tmp);
	return SUCCESS;
}
/* }}} */

HYSSAPI HashTable* hyss_ics_get_configuration_hash(void) /* {{{ */
{
	return &configuration_hash;
} /* }}} */

