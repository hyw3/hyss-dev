/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_MAIN_H
#define HYSS_MAIN_H

#include "gear_globals.h"
#include "hyss_globals.h"
#include "SAPI.h"

BEGIN_EXTERN_C()
HYSSAPI int hyss_request_startup(void);
HYSSAPI void hyss_request_shutdown(void *dummy);
HYSSAPI void hyss_request_shutdown_for_exec(void *dummy);
HYSSAPI int hyss_capi_startup(sapi_capi_struct *sf, gear_capi_entry *additional_capis, uint32_t num_additional_capis);
HYSSAPI void hyss_capi_shutdown(void);
HYSSAPI void hyss_capi_shutdown_for_exec(void);
HYSSAPI int hyss_capi_shutdown_wrapper(sapi_capi_struct *sapi_globals);

HYSSAPI int hyss_register_extensions(gear_capi_entry * const * ptr, int count);

HYSSAPI int hyss_execute_script(gear_file_handle *primary_file);
HYSSAPI int hyss_execute_simple_script(gear_file_handle *primary_file, zval *ret);
HYSSAPI int hyss_handle_special_queries(void);
HYSSAPI int hyss_lint_script(gear_file_handle *file);

HYSSAPI void hyss_handle_aborted_connection(void);
HYSSAPI int hyss_handle_auth_data(const char *auth);

HYSSAPI void hyss_html_puts(const char *str, size_t siz);
HYSSAPI int hyss_stream_open_for_gear_ex(const char *filename, gear_file_handle *handle, int mode);

/* environment cAPI */
extern int hyss_init_environ(void);
extern int hyss_shutdown_environ(void);
END_EXTERN_C()

#endif

