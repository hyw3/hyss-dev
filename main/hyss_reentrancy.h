/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_REENTRANCY_H
#define HYSS_REENTRANCY_H

#include "hyss.h"

#include <sys/types.h>
#ifdef HAVE_DIRENT_H
#include <dirent.h>
#endif
#include <time.h>

/* currently, HYSS does not check for these functions, but assumes
   that they are available on all systems. */

#define HAVE_LOCALTIME 1
#define HAVE_GMTIME 1
#define HAVE_ASCTIME 1
#define HAVE_CTIME 1

#if defined(HYSS_IRIX_TIME_R)
#undef HAVE_ASCTIME_R
#undef HAVE_CTIME_R
#endif

#if defined(HYSS_HPUX_TIME_R)
#undef HAVE_LOCALTIME_R
#undef HAVE_ASCTIME_R
#undef HAVE_CTIME_R
#undef HAVE_GMTIME_R
#endif

BEGIN_EXTERN_C()

#if defined(HAVE_POSIX_READDIR_R)
#define hyss_readdir_r readdir_r
#else
HYSSAPI int hyss_readdir_r(DIR *dirp, struct dirent *entry,
		struct dirent **result);
#endif

#if !defined(HAVE_LOCALTIME_R) && defined(HAVE_LOCALTIME)
#define HYSS_NEED_REENTRANCY 1
HYSSAPI struct tm *hyss_localtime_r(const time_t *const timep, struct tm *p_tm);
#else
#define hyss_localtime_r localtime_r
#ifdef MISSING_LOCALTIME_R_DECL
struct tm *localtime_r(const time_t *const timep, struct tm *p_tm);
#endif
#endif


#if !defined(HAVE_CTIME_R) && defined(HAVE_CTIME)
#define HYSS_NEED_REENTRANCY 1
HYSSAPI char *hyss_ctime_r(const time_t *clock, char *buf);
#else
#define hyss_ctime_r ctime_r
#ifdef MISSING_CTIME_R_DECL
char *ctime_r(const time_t *clock, char *buf);
#endif
#endif


#if !defined(HAVE_ASCTIME_R) && defined(HAVE_ASCTIME)
#define HYSS_NEED_REENTRANCY 1
HYSSAPI char *hyss_asctime_r(const struct tm *tm, char *buf);
#else
#define hyss_asctime_r asctime_r
#ifdef MISSING_ASCTIME_R_DECL
char *asctime_r(const struct tm *tm, char *buf);
#endif
#endif


#if !defined(HAVE_GMTIME_R) && defined(HAVE_GMTIME)
#define HYSS_NEED_REENTRANCY 1
HYSSAPI struct tm *hyss_gmtime_r(const time_t *const timep, struct tm *p_tm);
#else
#define hyss_gmtime_r gmtime_r
#ifdef MISSING_GMTIME_R_DECL
struct tm *hyss_gmtime_r(const time_t *const timep, struct tm *p_tm);
#endif
#endif

#if !defined(HAVE_STRTOK_R)
HYSSAPI char *hyss_strtok_r(char *s, const char *delim, char **last);
#else
#define hyss_strtok_r strtok_r
#ifdef MISSING_STRTOK_R_DECL
char *strtok_r(char *s, const char *delim, char **last);
#endif
#endif

#if !defined(HAVE_RAND_R)
HYSSAPI int hyss_rand_r(unsigned int *seed);
#else
#define hyss_rand_r rand_r
#endif

END_EXTERN_C()

#if !defined(ZTS)
#undef HYSS_NEED_REENTRANCY
#endif

#if defined(HYSS_NEED_REENTRANCY)
void reentrancy_startup(void);
void reentrancy_shutdown(void);
#else
#define reentrancy_startup()
#define reentrancy_shutdown()
#endif

#endif

