/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_STREAMS_H
#define HYSS_STREAMS_H

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include "gear.h"
#include "gear_stream.h"

BEGIN_EXTERN_C()
HYSSAPI int hyss_file_le_stream(void);
HYSSAPI int hyss_file_le_pstream(void);
HYSSAPI int hyss_file_le_stream_filter(void);
END_EXTERN_C()

/* {{{ Streams memory debugging stuff */

#if GEAR_DEBUG
/* these have more of a dependency on the definitions of the gear macros than
 * I would prefer, but doing it this way saves loads of idefs :-/ */
# define STREAMS_D			int __hyss_stream_call_depth GEAR_FILE_LINE_DC GEAR_FILE_LINE_ORIG_DC
# define STREAMS_C			0 GEAR_FILE_LINE_CC GEAR_FILE_LINE_EMPTY_CC
# define STREAMS_REL_C		__hyss_stream_call_depth + 1 GEAR_FILE_LINE_CC, \
	__hyss_stream_call_depth ? __gear_orig_filename : __gear_filename, \
	__hyss_stream_call_depth ? __gear_orig_lineno : __gear_lineno

# define STREAMS_DC		, STREAMS_D
# define STREAMS_CC		, STREAMS_C
# define STREAMS_REL_CC	, STREAMS_REL_C

#else
# define STREAMS_D
# define STREAMS_C
# define STREAMS_REL_C
# define STREAMS_DC
# define STREAMS_CC
# define STREAMS_REL_CC
#endif

/* these functions relay the file/line number information. They are depth aware, so they will pass
 * the ultimate ancestor, which is useful, because there can be several layers of calls */
#define hyss_stream_alloc_rel(ops, thisptr, persistent, mode) _hyss_stream_alloc((ops), (thisptr), (persistent), (mode) STREAMS_REL_CC)

#define hyss_stream_copy_to_mem_rel(src, maxlen, persistent) _hyss_stream_copy_to_mem((src), (buf), (maxlen), (persistent) STREAMS_REL_CC)

#define hyss_stream_fopen_rel(filename, mode, opened, options) _hyss_stream_fopen((filename), (mode), (opened), (options) STREAMS_REL_CC)

#define hyss_stream_fopen_with_path_rel(filename, mode, path, opened, options) _hyss_stream_fopen_with_path((filename), (mode), (path), (opened), (options) STREAMS_REL_CC)

#define hyss_stream_fopen_from_fd_rel(fd, mode, persistent_id)	 _hyss_stream_fopen_from_fd((fd), (mode), (persistent_id) STREAMS_REL_CC)
#define hyss_stream_fopen_from_file_rel(file, mode)	 _hyss_stream_fopen_from_file((file), (mode) STREAMS_REL_CC)

#define hyss_stream_fopen_from_pipe_rel(file, mode)	 _hyss_stream_fopen_from_pipe((file), (mode) STREAMS_REL_CC)

#define hyss_stream_fopen_tmpfile_rel()	_hyss_stream_fopen_tmpfile(0 STREAMS_REL_CC)

#define hyss_stream_fopen_temporary_file_rel(dir, pfx, opened_path)	_hyss_stream_fopen_temporary_file((dir), (pfx), (opened_path) STREAMS_REL_CC)

#define hyss_stream_open_wrapper_rel(path, mode, options, opened) _hyss_stream_open_wrapper_ex((path), (mode), (options), (opened), NULL STREAMS_REL_CC)
#define hyss_stream_open_wrapper_ex_rel(path, mode, options, opened, context) _hyss_stream_open_wrapper_ex((path), (mode), (options), (opened), (context) STREAMS_REL_CC)

#define hyss_stream_make_seekable_rel(origstream, newstream, flags) _hyss_stream_make_seekable((origstream), (newstream), (flags) STREAMS_REL_CC)

/* }}} */

/* The contents of the hyss_stream_ops and hyss_stream should only be accessed
 * using the functions/macros in this header.
 * If you need to get at something that doesn't have an API,
 * drop me a line <wez@thebrainroom.com> and we can sort out a way to do
 * it properly.
 *
 * The only exceptions to this rule are that stream implementations can use
 * the hyss_stream->abstract pointer to hold their context, and streams
 * opened via stream_open_wrappers can use the zval ptr in
 * hyss_stream->wrapperdata to hold meta data for hyss scripts to
 * retrieve using file_get_wrapper_data(). */

typedef struct _hyss_stream hyss_stream;
typedef struct _hyss_stream_wrapper hyss_stream_wrapper;
typedef struct _hyss_stream_context hyss_stream_context;
typedef struct _hyss_stream_filter hyss_stream_filter;

#include "streams/hyss_stream_context.h"
#include "streams/hyss_stream_filter_api.h"

typedef struct _hyss_stream_statbuf {
	gear_stat_t sb; /* regular info */
	/* extended info to go here some day: content-type etc. etc. */
} hyss_stream_statbuf;

typedef struct _hyss_stream_dirent {
	char d_name[MAXPATHLEN];
} hyss_stream_dirent;

/* operations on streams that are file-handles */
typedef struct _hyss_stream_ops  {
	/* stdio like functions - these are mandatory! */
	size_t (*write)(hyss_stream *stream, const char *buf, size_t count);
	size_t (*read)(hyss_stream *stream, char *buf, size_t count);
	int    (*close)(hyss_stream *stream, int close_handle);
	int    (*flush)(hyss_stream *stream);

	const char *label; /* label for this ops structure */

	/* these are optional */
	int (*seek)(hyss_stream *stream, gear_off_t offset, int whence, gear_off_t *newoffset);
	int (*cast)(hyss_stream *stream, int castas, void **ret);
	int (*stat)(hyss_stream *stream, hyss_stream_statbuf *ssb);
	int (*set_option)(hyss_stream *stream, int option, int value, void *ptrparam);
} hyss_stream_ops;

typedef struct _hyss_stream_wrapper_ops {
	/* open/create a wrapped stream */
	hyss_stream *(*stream_opener)(hyss_stream_wrapper *wrapper, const char *filename, const char *mode,
			int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC);
	/* close/destroy a wrapped stream */
	int (*stream_closer)(hyss_stream_wrapper *wrapper, hyss_stream *stream);
	/* stat a wrapped stream */
	int (*stream_stat)(hyss_stream_wrapper *wrapper, hyss_stream *stream, hyss_stream_statbuf *ssb);
	/* stat a URL */
	int (*url_stat)(hyss_stream_wrapper *wrapper, const char *url, int flags, hyss_stream_statbuf *ssb, hyss_stream_context *context);
	/* open a "directory" stream */
	hyss_stream *(*dir_opener)(hyss_stream_wrapper *wrapper, const char *filename, const char *mode,
			int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC);

	const char *label;

	/* delete a file */
	int (*unlink)(hyss_stream_wrapper *wrapper, const char *url, int options, hyss_stream_context *context);

	/* rename a file */
	int (*rename)(hyss_stream_wrapper *wrapper, const char *url_from, const char *url_to, int options, hyss_stream_context *context);

	/* Create/Remove directory */
	int (*stream_mkdir)(hyss_stream_wrapper *wrapper, const char *url, int mode, int options, hyss_stream_context *context);
	int (*stream_rmdir)(hyss_stream_wrapper *wrapper, const char *url, int options, hyss_stream_context *context);
	/* Metadata handling */
	int (*stream_metadata)(hyss_stream_wrapper *wrapper, const char *url, int options, void *value, hyss_stream_context *context);
} hyss_stream_wrapper_ops;

struct _hyss_stream_wrapper	{
	const hyss_stream_wrapper_ops *wops;	/* operations the wrapper can perform */
	void *abstract;					/* context for the wrapper */
	int is_url;						/* so that PG(allow_url_fopen) can be respected */
};

#define HYSS_STREAM_FLAG_NO_SEEK						0x1
#define HYSS_STREAM_FLAG_NO_BUFFER					0x2

#define HYSS_STREAM_FLAG_EOL_UNIX					0x0 /* also includes DOS */
#define HYSS_STREAM_FLAG_DETECT_EOL					0x4
#define HYSS_STREAM_FLAG_EOL_MAC						0x8

/* set this when the stream might represent "interactive" data.
 * When set, the read buffer will avoid certain operations that
 * might otherwise cause the read to block for much longer than
 * is strictly required. */
#define HYSS_STREAM_FLAG_AVOID_BLOCKING					0x10

#define HYSS_STREAM_FLAG_NO_CLOSE					0x20

#define HYSS_STREAM_FLAG_IS_DIR						0x40

#define HYSS_STREAM_FLAG_NO_FCLOSE					0x80

#define HYSS_STREAM_FLAG_WAS_WRITTEN					0x80000000

struct _hyss_stream  {
	const hyss_stream_ops *ops;
	void *abstract;			/* convenience pointer for abstraction */

	hyss_stream_filter_chain readfilters, writefilters;

	hyss_stream_wrapper *wrapper; /* which wrapper was used to open the stream */
	void *wrapperthis;		/* convenience pointer for a instance of a wrapper */
	zval wrapperdata;		/* fgetwrapperdata retrieves this */

	uint8_t is_persistent:1;
	uint8_t in_free:2;			/* to prevent recursion during free */
	uint8_t eof:1;
	uint8_t __exposed:1;	/* non-zero if exposed as a zval somewhere */

	/* so we know how to clean it up correctly.  This should be set to
	 * HYSS_STREAM_FCLOSE_XXX as appropriate */
	uint8_t fclose_stdiocast:2;

	uint8_t fgetss_state;		/* for fgetss to handle multiline tags */

	char mode[16];			/* "rwb" etc. ala stdio */

	uint32_t flags;	/* HYSS_STREAM_FLAG_XXX */

	gear_resource *res;		/* used for auto-cleanup */
	FILE *stdiocast;    /* cache this, otherwise we might leak! */
	char *orig_path;

	gear_resource *ctx;

	/* buffer */
	gear_off_t position; /* of underlying stream */
	unsigned char *readbuf;
	size_t readbuflen;
	gear_off_t readpos;
	gear_off_t writepos;

	/* how much data to read when filling buffer */
	size_t chunk_size;

#if GEAR_DEBUG
	const char *open_filename;
	uint32_t open_lineno;
#endif

	struct _hyss_stream *enclosing_stream; /* this is a private stream owned by enclosing_stream */
}; /* hyss_stream */

#define HYSS_STREAM_CONTEXT(stream) \
	((hyss_stream_context*) ((stream)->ctx ? ((stream)->ctx->ptr) : NULL))

/* state definitions when closing down; these are private to streams.c */
#define HYSS_STREAM_FCLOSE_NONE 0
#define HYSS_STREAM_FCLOSE_FDOPEN	1
#define HYSS_STREAM_FCLOSE_FOPENCOOKIE 2

/* allocate a new stream for a particular ops */
BEGIN_EXTERN_C()
HYSSAPI hyss_stream *_hyss_stream_alloc(const hyss_stream_ops *ops, void *abstract,
		const char *persistent_id, const char *mode STREAMS_DC);
END_EXTERN_C()
#define hyss_stream_alloc(ops, thisptr, persistent_id, mode)	_hyss_stream_alloc((ops), (thisptr), (persistent_id), (mode) STREAMS_CC)

#define hyss_stream_get_resource_id(stream)		((hyss_stream *)(stream))->res->handle
/* use this to tell the stream that it is OK if we don't explicitly close it */
#define hyss_stream_auto_cleanup(stream)	{ (stream)->__exposed = 1; }
/* use this to assign the stream to a zval and tell the stream that is
 * has been exported to the engine; it will expect to be closed automatically
 * when the resources are auto-destructed */
#define hyss_stream_to_zval(stream, zval)	{ ZVAL_RES(zval, (stream)->res); (stream)->__exposed = 1; }

#define hyss_stream_from_zval(xstr, pzval)	do { \
	if (((xstr) = (hyss_stream*)gear_fetch_resource2_ex((pzval), \
				"stream", hyss_file_le_stream(), hyss_file_le_pstream())) == NULL) { \
		RETURN_FALSE; \
	} \
} while (0)
#define hyss_stream_from_res(xstr, res)	do { \
	if (((xstr) = (hyss_stream*)gear_fetch_resource2((res), \
			   	"stream", hyss_file_le_stream(), hyss_file_le_pstream())) == NULL) { \
		RETURN_FALSE; \
	} \
} while (0)
#define hyss_stream_from_res_no_verify(xstr, pzval)	(xstr) = (hyss_stream*)gear_fetch_resource2((res), "stream", hyss_file_le_stream(), hyss_file_le_pstream())
#define hyss_stream_from_zval_no_verify(xstr, pzval)	(xstr) = (hyss_stream*)gear_fetch_resource2_ex((pzval), "stream", hyss_file_le_stream(), hyss_file_le_pstream())

BEGIN_EXTERN_C()
HYSSAPI hyss_stream *hyss_stream_encloses(hyss_stream *enclosing, hyss_stream *enclosed);
#define hyss_stream_free_enclosed(stream_enclosed, close_options) _hyss_stream_free_enclosed((stream_enclosed), (close_options))
HYSSAPI int _hyss_stream_free_enclosed(hyss_stream *stream_enclosed, int close_options);

HYSSAPI int hyss_stream_from_persistent_id(const char *persistent_id, hyss_stream **stream);
#define HYSS_STREAM_PERSISTENT_SUCCESS	0 /* id exists */
#define HYSS_STREAM_PERSISTENT_FAILURE	1 /* id exists but is not a stream! */
#define HYSS_STREAM_PERSISTENT_NOT_EXIST	2 /* id does not exist */

#define HYSS_STREAM_FREE_CALL_DTOR			1 /* call ops->close */
#define HYSS_STREAM_FREE_RELEASE_STREAM		2 /* pefree(stream) */
#define HYSS_STREAM_FREE_PRESERVE_HANDLE		4 /* tell ops->close to not close it's underlying handle */
#define HYSS_STREAM_FREE_RSRC_DTOR			8 /* called from the resource list dtor */
#define HYSS_STREAM_FREE_PERSISTENT			16 /* manually freeing a persistent connection */
#define HYSS_STREAM_FREE_IGNORE_ENCLOSING	32 /* don't close the enclosing stream instead */
#define HYSS_STREAM_FREE_KEEP_RSRC			64 /* keep associated gear_resource */
#define HYSS_STREAM_FREE_CLOSE				(HYSS_STREAM_FREE_CALL_DTOR | HYSS_STREAM_FREE_RELEASE_STREAM)
#define HYSS_STREAM_FREE_CLOSE_CASTED		(HYSS_STREAM_FREE_CLOSE | HYSS_STREAM_FREE_PRESERVE_HANDLE)
#define HYSS_STREAM_FREE_CLOSE_PERSISTENT	(HYSS_STREAM_FREE_CLOSE | HYSS_STREAM_FREE_PERSISTENT)

HYSSAPI int _hyss_stream_free(hyss_stream *stream, int close_options);
#define hyss_stream_free(stream, close_options)	_hyss_stream_free((stream), (close_options))
#define hyss_stream_close(stream)	_hyss_stream_free((stream), HYSS_STREAM_FREE_CLOSE)
#define hyss_stream_pclose(stream)	_hyss_stream_free((stream), HYSS_STREAM_FREE_CLOSE_PERSISTENT)

HYSSAPI int _hyss_stream_seek(hyss_stream *stream, gear_off_t offset, int whence);
#define hyss_stream_rewind(stream)	_hyss_stream_seek((stream), 0L, SEEK_SET)
#define hyss_stream_seek(stream, offset, whence)	_hyss_stream_seek((stream), (offset), (whence))

HYSSAPI gear_off_t _hyss_stream_tell(hyss_stream *stream);
#define hyss_stream_tell(stream)	_hyss_stream_tell((stream))

HYSSAPI size_t _hyss_stream_read(hyss_stream *stream, char *buf, size_t count);
#define hyss_stream_read(stream, buf, count)		_hyss_stream_read((stream), (buf), (count))

HYSSAPI size_t _hyss_stream_write(hyss_stream *stream, const char *buf, size_t count);
#define hyss_stream_write_string(stream, str)	_hyss_stream_write(stream, str, strlen(str))
#define hyss_stream_write(stream, buf, count)	_hyss_stream_write(stream, (buf), (count))

HYSSAPI void _hyss_stream_fill_read_buffer(hyss_stream *stream, size_t size);
#define hyss_stream_fill_read_buffer(stream, size)	_hyss_stream_fill_read_buffer((stream), (size))

HYSSAPI size_t _hyss_stream_printf(hyss_stream *stream, const char *fmt, ...) HYSS_ATTRIBUTE_FORMAT(printf, 2, 3);

/* hyss_stream_printf macro & function require */
#define hyss_stream_printf _hyss_stream_printf

HYSSAPI int _hyss_stream_eof(hyss_stream *stream);
#define hyss_stream_eof(stream)	_hyss_stream_eof((stream))

HYSSAPI int _hyss_stream_getc(hyss_stream *stream);
#define hyss_stream_getc(stream)	_hyss_stream_getc((stream))

HYSSAPI int _hyss_stream_putc(hyss_stream *stream, int c);
#define hyss_stream_putc(stream, c)	_hyss_stream_putc((stream), (c))

HYSSAPI int _hyss_stream_flush(hyss_stream *stream, int closing);
#define hyss_stream_flush(stream)	_hyss_stream_flush((stream), 0)

HYSSAPI char *_hyss_stream_get_line(hyss_stream *stream, char *buf, size_t maxlen, size_t *returned_len);
#define hyss_stream_gets(stream, buf, maxlen)	_hyss_stream_get_line((stream), (buf), (maxlen), NULL)

#define hyss_stream_get_line(stream, buf, maxlen, retlen) _hyss_stream_get_line((stream), (buf), (maxlen), (retlen))
HYSSAPI gear_string *hyss_stream_get_record(hyss_stream *stream, size_t maxlen, const char *delim, size_t delim_len);

/* CAREFUL! this is equivalent to puts NOT fputs! */
HYSSAPI int _hyss_stream_puts(hyss_stream *stream, const char *buf);
#define hyss_stream_puts(stream, buf)	_hyss_stream_puts((stream), (buf))

HYSSAPI int _hyss_stream_stat(hyss_stream *stream, hyss_stream_statbuf *ssb);
#define hyss_stream_stat(stream, ssb)	_hyss_stream_stat((stream), (ssb))

HYSSAPI int _hyss_stream_stat_path(const char *path, int flags, hyss_stream_statbuf *ssb, hyss_stream_context *context);
#define hyss_stream_stat_path(path, ssb)	_hyss_stream_stat_path((path), 0, (ssb), NULL)
#define hyss_stream_stat_path_ex(path, flags, ssb, context)	_hyss_stream_stat_path((path), (flags), (ssb), (context))

HYSSAPI int _hyss_stream_mkdir(const char *path, int mode, int options, hyss_stream_context *context);
#define hyss_stream_mkdir(path, mode, options, context)	_hyss_stream_mkdir(path, mode, options, context)

HYSSAPI int _hyss_stream_rmdir(const char *path, int options, hyss_stream_context *context);
#define hyss_stream_rmdir(path, options, context)	_hyss_stream_rmdir(path, options, context)

HYSSAPI hyss_stream *_hyss_stream_opendir(const char *path, int options, hyss_stream_context *context STREAMS_DC);
#define hyss_stream_opendir(path, options, context)	_hyss_stream_opendir((path), (options), (context) STREAMS_CC)
HYSSAPI hyss_stream_dirent *_hyss_stream_readdir(hyss_stream *dirstream, hyss_stream_dirent *ent);
#define hyss_stream_readdir(dirstream, dirent)	_hyss_stream_readdir((dirstream), (dirent))
#define hyss_stream_closedir(dirstream)	hyss_stream_close((dirstream))
#define hyss_stream_rewinddir(dirstream)	hyss_stream_rewind((dirstream))

HYSSAPI int hyss_stream_dirent_alphasort(const gear_string **a, const gear_string **b);
HYSSAPI int hyss_stream_dirent_alphasortr(const gear_string **a, const gear_string **b);

HYSSAPI int _hyss_stream_scandir(const char *dirname, gear_string **namelist[], int flags, hyss_stream_context *context,
			int (*compare) (const gear_string **a, const gear_string **b));
#define hyss_stream_scandir(dirname, namelist, context, compare) _hyss_stream_scandir((dirname), (namelist), 0, (context), (compare))

HYSSAPI int _hyss_stream_set_option(hyss_stream *stream, int option, int value, void *ptrparam);
#define hyss_stream_set_option(stream, option, value, ptrvalue)	_hyss_stream_set_option((stream), (option), (value), (ptrvalue))

#define hyss_stream_set_chunk_size(stream, size) _hyss_stream_set_option((stream), HYSS_STREAM_OPTION_SET_CHUNK_SIZE, (size), NULL)

END_EXTERN_C()


/* Flags for mkdir method in wrapper ops */
#define HYSS_STREAM_MKDIR_RECURSIVE	1
/* define REPORT ERRORS 8 (below) */

/* Flags for rmdir method in wrapper ops */
/* define REPORT_ERRORS 8 (below) */

/* Flags for url_stat method in wrapper ops */
#define HYSS_STREAM_URL_STAT_LINK	1
#define HYSS_STREAM_URL_STAT_QUIET	2
#define HYSS_STREAM_URL_STAT_NOCACHE	4

/* change the blocking mode of stream: value == 1 => blocking, value == 0 => non-blocking. */
#define HYSS_STREAM_OPTION_BLOCKING	1

/* change the buffering mode of stream. value is a HYSS_STREAM_BUFFER_XXXX value, ptrparam is a ptr to a size_t holding
 * the required buffer size */
#define HYSS_STREAM_OPTION_READ_BUFFER	2
#define HYSS_STREAM_OPTION_WRITE_BUFFER	3

#define HYSS_STREAM_BUFFER_NONE	0	/* unbuffered */
#define HYSS_STREAM_BUFFER_LINE	1	/* line buffered */
#define HYSS_STREAM_BUFFER_FULL	2	/* fully buffered */

/* set the timeout duration for reads on the stream. ptrparam is a pointer to a struct timeval * */
#define HYSS_STREAM_OPTION_READ_TIMEOUT	4
#define HYSS_STREAM_OPTION_SET_CHUNK_SIZE	5

/* set or release lock on a stream */
#define HYSS_STREAM_OPTION_LOCKING		6

/* whether or not locking is supported */
#define HYSS_STREAM_LOCK_SUPPORTED		1

#define hyss_stream_supports_lock(stream)	(_hyss_stream_set_option((stream), HYSS_STREAM_OPTION_LOCKING, 0, (void *) HYSS_STREAM_LOCK_SUPPORTED) == 0 ? 1 : 0)
#define hyss_stream_lock(stream, mode)		_hyss_stream_set_option((stream), HYSS_STREAM_OPTION_LOCKING, (mode), (void *) NULL)

/* option code used by the hyss_stream_xport_XXX api */
#define HYSS_STREAM_OPTION_XPORT_API			7 /* see hyss_stream_transport.h */
#define HYSS_STREAM_OPTION_CRYPTO_API		8 /* see hyss_stream_transport.h */
#define HYSS_STREAM_OPTION_MMAP_API			9 /* see hyss_stream_mmap.h */
#define HYSS_STREAM_OPTION_TRUNCATE_API		10

#define HYSS_STREAM_TRUNCATE_SUPPORTED	0
#define HYSS_STREAM_TRUNCATE_SET_SIZE	1	/* ptrparam is a pointer to a size_t */

#define hyss_stream_truncate_supported(stream)	(_hyss_stream_set_option((stream), HYSS_STREAM_OPTION_TRUNCATE_API, HYSS_STREAM_TRUNCATE_SUPPORTED, NULL) == HYSS_STREAM_OPTION_RETURN_OK ? 1 : 0)

BEGIN_EXTERN_C()
HYSSAPI int _hyss_stream_truncate_set_size(hyss_stream *stream, size_t newsize);
#define hyss_stream_truncate_set_size(stream, size)	_hyss_stream_truncate_set_size((stream), (size))
END_EXTERN_C()

#define HYSS_STREAM_OPTION_META_DATA_API		11 /* ptrparam is a zval* to which to add meta data information */
#define hyss_stream_populate_meta_data(stream, zv)	(_hyss_stream_set_option((stream), HYSS_STREAM_OPTION_META_DATA_API, 0, zv) == HYSS_STREAM_OPTION_RETURN_OK ? 1 : 0)

/* Check if the stream is still "live"; for sockets/pipes this means the socket
 * is still connected; for files, this does not really have meaning */
#define HYSS_STREAM_OPTION_CHECK_LIVENESS	12 /* no parameters */

/* Enable/disable blocking reads on anonymous pipes on Windows. */
#define HYSS_STREAM_OPTION_PIPE_BLOCKING 13

#define HYSS_STREAM_OPTION_RETURN_OK			 0 /* option set OK */
#define HYSS_STREAM_OPTION_RETURN_ERR		-1 /* problem setting option */
#define HYSS_STREAM_OPTION_RETURN_NOTIMPL	-2 /* underlying stream does not implement; streams can handle it instead */

/* copy up to maxlen bytes from src to dest.  If maxlen is HYSS_STREAM_COPY_ALL,
 * copy until eof(src). */
#define HYSS_STREAM_COPY_ALL		((size_t)-1)

BEGIN_EXTERN_C()
GEAR_ATTRIBUTE_DEPRECATED
HYSSAPI size_t _hyss_stream_copy_to_stream(hyss_stream *src, hyss_stream *dest, size_t maxlen STREAMS_DC);
#define hyss_stream_copy_to_stream(src, dest, maxlen)	_hyss_stream_copy_to_stream((src), (dest), (maxlen) STREAMS_CC)
HYSSAPI int _hyss_stream_copy_to_stream_ex(hyss_stream *src, hyss_stream *dest, size_t maxlen, size_t *len STREAMS_DC);
#define hyss_stream_copy_to_stream_ex(src, dest, maxlen, len)	_hyss_stream_copy_to_stream_ex((src), (dest), (maxlen), (len) STREAMS_CC)


/* read all data from stream and put into a buffer. Caller must free buffer
 * when done. */
HYSSAPI gear_string *_hyss_stream_copy_to_mem(hyss_stream *src, size_t maxlen, int persistent STREAMS_DC);
#define hyss_stream_copy_to_mem(src, maxlen, persistent) _hyss_stream_copy_to_mem((src), (maxlen), (persistent) STREAMS_CC)

/* output all data from a stream */
HYSSAPI size_t _hyss_stream_passthru(hyss_stream * src STREAMS_DC);
#define hyss_stream_passthru(stream)	_hyss_stream_passthru((stream) STREAMS_CC)
END_EXTERN_C()

#include "streams/hyss_stream_transport.h"
#include "streams/hyss_stream_plain_wrapper.h"
#include "streams/hyss_stream_glob_wrapper.h"
#include "streams/hyss_stream_userspace.h"
#include "streams/hyss_stream_mmap.h"

/* coerce the stream into some other form */
/* cast as a stdio FILE * */
#define HYSS_STREAM_AS_STDIO     0
/* cast as a POSIX fd or socketd */
#define HYSS_STREAM_AS_FD		1
/* cast as a socketd */
#define HYSS_STREAM_AS_SOCKETD	2
/* cast as fd/socket for select purposes */
#define HYSS_STREAM_AS_FD_FOR_SELECT 3

/* try really, really hard to make sure the cast happens (avoid using this flag if possible) */
#define HYSS_STREAM_CAST_TRY_HARD	0x80000000
#define HYSS_STREAM_CAST_RELEASE		0x40000000	/* stream becomes invalid on success */
#define HYSS_STREAM_CAST_INTERNAL	0x20000000	/* stream cast for internal use */
#define HYSS_STREAM_CAST_MASK		(HYSS_STREAM_CAST_TRY_HARD | HYSS_STREAM_CAST_RELEASE | HYSS_STREAM_CAST_INTERNAL)
BEGIN_EXTERN_C()
HYSSAPI int _hyss_stream_cast(hyss_stream *stream, int castas, void **ret, int show_err);
END_EXTERN_C()
/* use this to check if a stream can be cast into another form */
#define hyss_stream_can_cast(stream, as)	_hyss_stream_cast((stream), (as), NULL, 0)
#define hyss_stream_cast(stream, as, ret, show_err)	_hyss_stream_cast((stream), (as), (ret), (show_err))

/* use this to check if a stream is of a particular type:
 * HYSSAPI int hyss_stream_is(hyss_stream *stream, hyss_stream_ops *ops); */
#define hyss_stream_is(stream, anops)		((stream)->ops == anops)
#define HYSS_STREAM_IS_STDIO &hyss_stream_stdio_ops

#define hyss_stream_is_persistent(stream)	(stream)->is_persistent

/* Wrappers support */

#define IGNORE_PATH                     0x00000000
#define USE_PATH                        0x00000001
#define IGNORE_URL                      0x00000002
#define REPORT_ERRORS                   0x00000008

/* If you don't need to write to the stream, but really need to
 * be able to seek, use this flag in your options. */
#define STREAM_MUST_SEEK                0x00000010
/* If you are going to end up casting the stream into a FILE* or
 * a socket, pass this flag and the streams/wrappers will not use
 * buffering mechanisms while reading the headers, so that HTTP
 * wrapped streams will work consistently.
 * If you omit this flag, streams will use buffering and should end
 * up working more optimally.
 * */
#define STREAM_WILL_CAST                0x00000020

/* this flag applies to hyss_stream_locate_url_wrapper */
#define STREAM_LOCATE_WRAPPERS_ONLY     0x00000040

/* this flag is only used by include/require functions */
#define STREAM_OPEN_FOR_INCLUDE         0x00000080

/* this flag tells streams to ONLY open urls */
#define STREAM_USE_URL                  0x00000100

/* this flag is used when only the headers from HTTP request are to be fetched */
#define STREAM_ONLY_GET_HEADERS         0x00000200

/* don't apply open_basedir checks */
#define STREAM_DISABLE_OPEN_BASEDIR     0x00000400

/* get (or create) a persistent version of the stream */
#define STREAM_OPEN_PERSISTENT          0x00000800

/* use glob stream for directory open in plain files stream */
#define STREAM_USE_GLOB_DIR_OPEN        0x00001000

/* don't check allow_url_fopen and allow_url_include */
#define STREAM_DISABLE_URL_PROTECTION   0x00002000

/* assume the path passed in exists and is fully expanded, avoiding syscalls */
#define STREAM_ASSUME_REALPATH          0x00004000

/* Allow blocking reads on anonymous pipes on Windows. */
#define STREAM_USE_BLOCKING_PIPE        0x00008000

/* Antique - no longer has meaning */
#define IGNORE_URL_WIN 0

int hyss_init_stream_wrappers(int capi_number);
int hyss_shutdown_stream_wrappers(int capi_number);
void hyss_shutdown_stream_hashes(void);
HYSS_RSHUTDOWN_FUNCTION(streams);

BEGIN_EXTERN_C()
HYSSAPI int hyss_register_url_stream_wrapper(const char *protocol, const hyss_stream_wrapper *wrapper);
HYSSAPI int hyss_unregister_url_stream_wrapper(const char *protocol);
HYSSAPI int hyss_register_url_stream_wrapper_volatile(gear_string *protocol, hyss_stream_wrapper *wrapper);
HYSSAPI int hyss_unregister_url_stream_wrapper_volatile(gear_string *protocol);
HYSSAPI hyss_stream *_hyss_stream_open_wrapper_ex(const char *path, const char *mode, int options, gear_string **opened_path, hyss_stream_context *context STREAMS_DC);
HYSSAPI hyss_stream_wrapper *hyss_stream_locate_url_wrapper(const char *path, const char **path_for_open, int options);
HYSSAPI const char *hyss_stream_locate_eol(hyss_stream *stream, gear_string *buf);

#define hyss_stream_open_wrapper(path, mode, options, opened)	_hyss_stream_open_wrapper_ex((path), (mode), (options), (opened), NULL STREAMS_CC)
#define hyss_stream_open_wrapper_ex(path, mode, options, opened, context)	_hyss_stream_open_wrapper_ex((path), (mode), (options), (opened), (context) STREAMS_CC)

/* pushes an error message onto the stack for a wrapper instance */
HYSSAPI void hyss_stream_wrapper_log_error(const hyss_stream_wrapper *wrapper, int options, const char *fmt, ...) HYSS_ATTRIBUTE_FORMAT(printf, 3, 4);

#define HYSS_STREAM_UNCHANGED	0 /* orig stream was seekable anyway */
#define HYSS_STREAM_RELEASED		1 /* newstream should be used; origstream is no longer valid */
#define HYSS_STREAM_FAILED		2 /* an error occurred while attempting conversion */
#define HYSS_STREAM_CRITICAL		3 /* an error occurred; origstream is in an unknown state; you should close origstream */
#define HYSS_STREAM_NO_PREFERENCE	0
#define HYSS_STREAM_PREFER_STDIO		1
#define HYSS_STREAM_FORCE_CONVERSION	2
/* DO NOT call this on streams that are referenced by resources! */
HYSSAPI int _hyss_stream_make_seekable(hyss_stream *origstream, hyss_stream **newstream, int flags STREAMS_DC);
#define hyss_stream_make_seekable(origstream, newstream, flags)	_hyss_stream_make_seekable((origstream), (newstream), (flags) STREAMS_CC)

/* Give other cAPIs access to the url_stream_wrappers_hash and stream_filters_hash */
HYSSAPI HashTable *_hyss_stream_get_url_stream_wrappers_hash(void);
#define hyss_stream_get_url_stream_wrappers_hash()	_hyss_stream_get_url_stream_wrappers_hash()
HYSSAPI HashTable *hyss_stream_get_url_stream_wrappers_hash_global(void);
HYSSAPI HashTable *_hyss_get_stream_filters_hash(void);
#define hyss_get_stream_filters_hash()	_hyss_get_stream_filters_hash()
HYSSAPI HashTable *hyss_get_stream_filters_hash_global(void);
extern const hyss_stream_wrapper_ops *hyss_stream_user_wrapper_ops;
END_EXTERN_C()
#endif

/* Definitions for user streams */
#define HYSS_STREAM_IS_URL		1

/* Stream metadata definitions */
/* Create if referred resource does not exist */
#define HYSS_STREAM_META_TOUCH		1
#define HYSS_STREAM_META_OWNER_NAME	2
#define HYSS_STREAM_META_OWNER		3
#define HYSS_STREAM_META_GROUP_NAME	4
#define HYSS_STREAM_META_GROUP		5
#define HYSS_STREAM_META_ACCESS		6

